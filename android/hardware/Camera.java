package android.hardware;

import android.app.ActivityThread;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.media.IAudioService;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RSIllegalArgumentException;
import android.renderscript.RenderScript;
import android.renderscript.Type;
import android.system.OsConstants;
import android.text.TextUtils;
import android.util.Log;
import android.util.SeempLog;
import android.view.Surface;
import android.view.SurfaceHolder;
import com.android.internal.app.IAppOpsCallback;
import com.android.internal.app.IAppOpsService;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

@Deprecated
public class Camera {
  private boolean mFaceDetectionRunning = false;
  
  private final Object mAutoFocusCallbackLock = new Object();
  
  private final OplusCameraStatisticsManager mCameraStatisticsManager = OplusCameraStatisticsManager.getInstance();
  
  private final Object mShutterSoundLock = new Object();
  
  private boolean mHasAppOpsPlayAudio = true;
  
  private boolean mShutterSoundEnabledFromApp = true;
  
  public static final String ACTION_NEW_PICTURE = "android.hardware.action.NEW_PICTURE";
  
  public static final String ACTION_NEW_VIDEO = "android.hardware.action.NEW_VIDEO";
  
  public static final int CAMERA_ERROR_DISABLED = 3;
  
  public static final int CAMERA_ERROR_EVICTED = 2;
  
  public static final int CAMERA_ERROR_SERVER_DIED = 100;
  
  public static final int CAMERA_ERROR_UNKNOWN = 1;
  
  private static final int CAMERA_FACE_DETECTION_HW = 0;
  
  private static final int CAMERA_FACE_DETECTION_SW = 1;
  
  public static final int CAMERA_HAL_API_VERSION_1_0 = 256;
  
  private static final int CAMERA_HAL_API_VERSION_NORMAL_CONNECT = -2;
  
  private static final int CAMERA_HAL_API_VERSION_UNSPECIFIED = -1;
  
  private static final int CAMERA_MSG_COMPRESSED_IMAGE = 256;
  
  private static final int CAMERA_MSG_ERROR = 1;
  
  private static final int CAMERA_MSG_FOCUS = 4;
  
  private static final int CAMERA_MSG_FOCUS_MOVE = 2048;
  
  private static final int CAMERA_MSG_META_DATA = 8192;
  
  private static final int CAMERA_MSG_POSTVIEW_FRAME = 64;
  
  private static final int CAMERA_MSG_PREVIEW_FRAME = 16;
  
  private static final int CAMERA_MSG_PREVIEW_METADATA = 1024;
  
  private static final int CAMERA_MSG_RAW_IMAGE = 128;
  
  private static final int CAMERA_MSG_RAW_IMAGE_NOTIFY = 512;
  
  private static final int CAMERA_MSG_SHUTTER = 2;
  
  private static final int CAMERA_MSG_STATS_DATA = 4096;
  
  private static final int CAMERA_MSG_VIDEO_FRAME = 32;
  
  private static final int CAMERA_MSG_ZOOM = 8;
  
  private static final int EACCESS = -13;
  
  private static final int EBUSY = -16;
  
  private static final int EINVAL = -22;
  
  private static final int ENODEV = -19;
  
  private static final int ENOSYS = -38;
  
  private static final int EOPNOTSUPP = -95;
  
  private static final int EUSERS = -87;
  
  private static final int NO_ERROR = 0;
  
  private static final String TAG = "Camera";
  
  private IAppOpsService mAppOps;
  
  private IAppOpsCallback mAppOpsCallback;
  
  private AutoFocusCallback mAutoFocusCallback;
  
  private AutoFocusMoveCallback mAutoFocusMoveCallback;
  
  private CameraDataCallback mCameraDataCallback;
  
  private CameraMetaDataCallback mCameraMetaDataCallback;
  
  private ErrorCallback mDetailedErrorCallback;
  
  private ErrorCallback mErrorCallback;
  
  private EventHandler mEventHandler;
  
  private FaceDetectionListener mFaceListener;
  
  private PictureCallback mJpegCallback;
  
  private long mNativeContext;
  
  private boolean mOneShot;
  
  private PictureCallback mPostviewCallback;
  
  private PreviewCallback mPreviewCallback;
  
  private PictureCallback mRawImageCallback;
  
  private ShutterCallback mShutterCallback;
  
  private boolean mUsingPreviewAllocation;
  
  private boolean mWithBuffer;
  
  private OnZoomChangeListener mZoomListener;
  
  public static int getNumberOfCameras() {
    int i = 0;
    String str1 = ActivityThread.currentOpPackageName();
    String str2 = SystemProperties.get("vendor.camera.aux.packagelist");
    int j = i;
    if (str2.length() > 0) {
      TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(',');
      simpleStringSplitter.setString(str2);
      Iterator<String> iterator = simpleStringSplitter.iterator();
      while (true) {
        j = i;
        if (iterator.hasNext()) {
          String str = iterator.next();
          if (str1.equals(str)) {
            j = 1;
            break;
          } 
          continue;
        } 
        break;
      } 
    } 
    int k = _getNumberOfCameras();
    i = k;
    if (j == 0) {
      i = k;
      if (k > 2)
        i = 2; 
    } 
    return i;
  }
  
  public static void getCameraInfo(int paramInt, CameraInfo paramCameraInfo) {
    if (paramInt < getNumberOfCameras()) {
      _getCameraInfo(paramInt, paramCameraInfo);
      IBinder iBinder = ServiceManager.getService("audio");
      IAudioService iAudioService = IAudioService.Stub.asInterface(iBinder);
      try {
        if (iAudioService.isCameraSoundForced())
          paramCameraInfo.canDisableShutterSound = false; 
      } catch (RemoteException remoteException) {
        Log.e("Camera", "Audio service is unavailable for queries");
      } 
      return;
    } 
    throw new RuntimeException("Unknown camera ID");
  }
  
  @Deprecated
  public static class CameraInfo {
    public static final int CAMERA_FACING_BACK = 0;
    
    public static final int CAMERA_FACING_FRONT = 1;
    
    public static final int CAMERA_SUPPORT_MODE_NONZSL = 3;
    
    public static final int CAMERA_SUPPORT_MODE_ZSL = 2;
    
    public boolean canDisableShutterSound;
    
    public int facing;
    
    public int orientation;
  }
  
  public static Camera open(int paramInt) {
    return new Camera(paramInt);
  }
  
  public static Camera open() {
    int i = getNumberOfCameras();
    CameraInfo cameraInfo = new CameraInfo();
    for (byte b = 0; b < i; b++) {
      getCameraInfo(b, cameraInfo);
      if (cameraInfo.facing == 0)
        return new Camera(b); 
    } 
    return null;
  }
  
  public static Camera openLegacy(int paramInt1, int paramInt2) {
    if (paramInt2 >= 256)
      return new Camera(paramInt1, paramInt2); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid HAL version ");
    stringBuilder.append(paramInt2);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private Camera(int paramInt1, int paramInt2) {
    paramInt2 = cameraInitVersion(paramInt1, paramInt2);
    if (checkInitErrors(paramInt2)) {
      if (paramInt2 != -OsConstants.EACCES) {
        if (paramInt2 != 19) {
          if (paramInt2 != 38) {
            if (paramInt2 != 95) {
              if (paramInt2 != 22) {
                if (paramInt2 != 16) {
                  if (paramInt2 == 87)
                    throw new RuntimeException("Camera initialization failed because the max number of camera devices were already opened"); 
                  throw new RuntimeException("Unknown camera error");
                } 
                throw new RuntimeException("Camera initialization failed because the camera device was already opened");
              } 
              throw new RuntimeException("Camera initialization failed because the input arugments are invalid");
            } 
            throw new RuntimeException("Camera initialization failed because the hal version is not supported by this device");
          } 
          throw new RuntimeException("Camera initialization failed because some methods are not implemented");
        } 
        throw new RuntimeException("Camera initialization failed");
      } 
      throw new RuntimeException("Fail to connect to camera service");
    } 
    this.mCameraStatisticsManager.setConnectTime(System.currentTimeMillis());
    this.mCameraStatisticsManager.addInfo(paramInt1, 0L);
  }
  
  private int cameraInitVersion(int paramInt1, int paramInt2) {
    this.mShutterCallback = null;
    this.mRawImageCallback = null;
    this.mJpegCallback = null;
    this.mPreviewCallback = null;
    this.mPostviewCallback = null;
    this.mUsingPreviewAllocation = false;
    this.mZoomListener = null;
    this.mCameraDataCallback = null;
    this.mCameraMetaDataCallback = null;
    Looper looper = Looper.myLooper();
    if (looper != null) {
      this.mEventHandler = new EventHandler(this, looper);
    } else {
      looper = Looper.getMainLooper();
      if (looper != null) {
        this.mEventHandler = new EventHandler(this, looper);
      } else {
        this.mEventHandler = null;
      } 
    } 
    String str1 = ActivityThread.currentOpPackageName();
    String str2 = SystemProperties.get("vendor.camera.hal1.packagelist", "");
    int i = paramInt2;
    if (str2.length() > 0) {
      TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(',');
      simpleStringSplitter.setString(str2);
      Iterator<String> iterator = simpleStringSplitter.iterator();
      while (true) {
        i = paramInt2;
        if (iterator.hasNext()) {
          str2 = iterator.next();
          if (str1.equals(str2)) {
            i = 256;
            break;
          } 
          continue;
        } 
        break;
      } 
    } 
    return native_setup(new WeakReference<>(this), paramInt1, i, str1);
  }
  
  private int cameraInitNormal(int paramInt) {
    return cameraInitVersion(paramInt, -2);
  }
  
  public int cameraInitUnspecified(int paramInt) {
    return cameraInitVersion(paramInt, -1);
  }
  
  Camera(int paramInt) {
    if (paramInt < getNumberOfCameras()) {
      int i = cameraInitNormal(paramInt);
      if (checkInitErrors(i)) {
        if (i != -OsConstants.EACCES) {
          if (i == 19)
            throw new RuntimeException("Camera initialization failed"); 
          throw new RuntimeException("Unknown camera error");
        } 
        throw new RuntimeException("Fail to connect to camera service");
      } 
      this.mCameraStatisticsManager.setConnectTime(System.currentTimeMillis());
      this.mCameraStatisticsManager.addInfo(paramInt, 0L);
      initAppOps();
      return;
    } 
    throw new RuntimeException("Unknown camera ID");
  }
  
  public static boolean checkInitErrors(int paramInt) {
    boolean bool;
    if (paramInt != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static Camera openUninitialized() {
    return new Camera();
  }
  
  private void initAppOps() {
    IBinder iBinder = ServiceManager.getService("appops");
    this.mAppOps = IAppOpsService.Stub.asInterface(iBinder);
    updateAppOpsPlayAudio();
    this.mAppOpsCallback = (IAppOpsCallback)new IAppOpsCallbackWrapper(this);
    try {
      IAppOpsService iAppOpsService = this.mAppOps;
      String str = ActivityThread.currentPackageName();
      IAppOpsCallback iAppOpsCallback = this.mAppOpsCallback;
      iAppOpsService.startWatchingMode(28, str, iAppOpsCallback);
    } catch (RemoteException remoteException) {
      Log.e("Camera", "Error registering appOps callback", (Throwable)remoteException);
      this.mHasAppOpsPlayAudio = false;
    } 
  }
  
  private void releaseAppOps() {
    try {
      if (this.mAppOps != null)
        this.mAppOps.stopWatchingMode(this.mAppOpsCallback); 
    } catch (Exception exception) {}
  }
  
  protected void finalize() {
    release();
  }
  
  public final void release() {
    native_release();
    this.mFaceDetectionRunning = false;
    releaseAppOps();
    long l = System.currentTimeMillis();
    this.mCameraStatisticsManager.setDisconnectTime(l);
    OplusCameraStatisticsManager oplusCameraStatisticsManager = this.mCameraStatisticsManager;
    oplusCameraStatisticsManager.addInfo(oplusCameraStatisticsManager.getCameraId(), l);
  }
  
  public final void setPreviewDisplay(SurfaceHolder paramSurfaceHolder) throws IOException {
    if (paramSurfaceHolder != null) {
      setPreviewSurface(paramSurfaceHolder.getSurface());
    } else {
      setPreviewSurface((Surface)null);
    } 
  }
  
  public final void stopPreview() {
    Parameters parameters = null;
    try {
      Parameters parameters1 = getParameters();
    } catch (Exception exception) {
      Log.e("Camera", "failure in getParameters");
    } 
    this.mCameraStatisticsManager.addPreviewInfo(parameters);
    _stopPreview();
    this.mFaceDetectionRunning = false;
    this.mShutterCallback = null;
    this.mRawImageCallback = null;
    this.mPostviewCallback = null;
    this.mJpegCallback = null;
    synchronized (this.mAutoFocusCallbackLock) {
      this.mAutoFocusCallback = null;
      this.mAutoFocusMoveCallback = null;
      return;
    } 
  }
  
  public final void setPreviewCallback(PreviewCallback paramPreviewCallback) {
    boolean bool;
    SeempLog.record(66);
    this.mPreviewCallback = paramPreviewCallback;
    this.mOneShot = false;
    this.mWithBuffer = false;
    if (paramPreviewCallback != null)
      this.mUsingPreviewAllocation = false; 
    if (paramPreviewCallback != null) {
      bool = true;
    } else {
      bool = false;
    } 
    setHasPreviewCallback(bool, false);
  }
  
  public final void setOneShotPreviewCallback(PreviewCallback paramPreviewCallback) {
    SeempLog.record(68);
    this.mPreviewCallback = paramPreviewCallback;
    boolean bool = true;
    this.mOneShot = true;
    this.mWithBuffer = false;
    if (paramPreviewCallback != null)
      this.mUsingPreviewAllocation = false; 
    if (paramPreviewCallback == null)
      bool = false; 
    setHasPreviewCallback(bool, false);
  }
  
  public final void setPreviewCallbackWithBuffer(PreviewCallback paramPreviewCallback) {
    SeempLog.record(67);
    this.mPreviewCallback = paramPreviewCallback;
    boolean bool = false;
    this.mOneShot = false;
    this.mWithBuffer = true;
    if (paramPreviewCallback != null)
      this.mUsingPreviewAllocation = false; 
    if (paramPreviewCallback != null)
      bool = true; 
    setHasPreviewCallback(bool, true);
  }
  
  public final void addCallbackBuffer(byte[] paramArrayOfbyte) {
    _addCallbackBuffer(paramArrayOfbyte, 16);
  }
  
  public final void addRawImageCallbackBuffer(byte[] paramArrayOfbyte) {
    addCallbackBuffer(paramArrayOfbyte, 128);
  }
  
  private final void addCallbackBuffer(byte[] paramArrayOfbyte, int paramInt) {
    if (paramInt == 16 || paramInt == 128) {
      _addCallbackBuffer(paramArrayOfbyte, paramInt);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported message type: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public final Allocation createPreviewAllocation(RenderScript paramRenderScript, int paramInt) throws RSIllegalArgumentException {
    Parameters parameters = getParameters();
    Size size = parameters.getPreviewSize();
    Element.DataType dataType = Element.DataType.UNSIGNED_8;
    Element.DataKind dataKind = Element.DataKind.PIXEL_YUV;
    Type.Builder builder = new Type.Builder(paramRenderScript, Element.createPixel(paramRenderScript, dataType, dataKind));
    builder.setYuvFormat(842094169);
    builder.setX(size.width);
    builder.setY(size.height);
    return Allocation.createTyped(paramRenderScript, builder.create(), paramInt | 0x20);
  }
  
  public final void setPreviewCallbackAllocation(Allocation paramAllocation) throws IOException {
    StringBuilder stringBuilder1, stringBuilder2;
    Parameters parameters = null;
    if (paramAllocation != null) {
      Surface surface;
      parameters = getParameters();
      Size size = parameters.getPreviewSize();
      if (size.width == paramAllocation.getType().getX()) {
        int i = size.height;
        if (i == paramAllocation.getType().getY()) {
          if ((paramAllocation.getUsage() & 0x20) != 0) {
            if (paramAllocation.getType().getElement().getDataKind() == Element.DataKind.PIXEL_YUV) {
              surface = paramAllocation.getSurface();
              this.mUsingPreviewAllocation = true;
            } else {
              throw new IllegalArgumentException("Allocation is not of a YUV type");
            } 
          } else {
            throw new IllegalArgumentException("Allocation usage does not include USAGE_IO_INPUT");
          } 
        } else {
          stringBuilder2 = new StringBuilder();
          stringBuilder2.append("Allocation dimensions don't match preview dimensions: Allocation is ");
          stringBuilder2.append(surface.getType().getX());
          stringBuilder2.append(", ");
          stringBuilder2.append(surface.getType().getY());
          stringBuilder2.append(". Preview is ");
          stringBuilder2.append(size.width);
          stringBuilder2.append(", ");
          stringBuilder2.append(size.height);
          throw new IllegalArgumentException(stringBuilder2.toString());
        } 
      } else {
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append("Allocation dimensions don't match preview dimensions: Allocation is ");
        stringBuilder2.append(surface.getType().getX());
        stringBuilder2.append(", ");
        stringBuilder2.append(surface.getType().getY());
        stringBuilder2.append(". Preview is ");
        stringBuilder2.append(size.width);
        stringBuilder2.append(", ");
        stringBuilder2.append(size.height);
        throw new IllegalArgumentException(stringBuilder2.toString());
      } 
    } else {
      this.mUsingPreviewAllocation = false;
      stringBuilder1 = stringBuilder2;
    } 
    setPreviewCallbackSurface((Surface)stringBuilder1);
  }
  
  class EventHandler extends Handler {
    private final Camera mCamera;
    
    final Camera this$0;
    
    public EventHandler(Camera param1Camera1, Looper param1Looper) {
      super(param1Looper);
      this.mCamera = param1Camera1;
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      boolean bool1 = true, bool2 = true, bool3 = true;
      if (i != 1) {
        if (i != 2) {
          if (i != 4) {
            if (i != 8) {
              if (i != 16) {
                if (i != 64) {
                  if (i != 128) {
                    if (i != 256) {
                      if (i != 1024) {
                        if (i != 2048) {
                          if (i != 4096) {
                            if (i != 8192) {
                              StringBuilder stringBuilder1 = new StringBuilder();
                              stringBuilder1.append("Unknown message type ");
                              stringBuilder1.append(param1Message.what);
                              Log.e("Camera", stringBuilder1.toString());
                              return;
                            } 
                            if (Camera.this.mCameraMetaDataCallback != null)
                              Camera.this.mCameraMetaDataCallback.onCameraMetaData((byte[])param1Message.obj, this.mCamera); 
                            return;
                          } 
                          int[] arrayOfInt = new int[257];
                          for (i = 0; i < 257; i++)
                            arrayOfInt[i] = Camera.byteToInt((byte[])param1Message.obj, i * 4); 
                          if (Camera.this.mCameraDataCallback != null)
                            Camera.this.mCameraDataCallback.onCameraData(arrayOfInt, this.mCamera); 
                          return;
                        } 
                        if (Camera.this.mAutoFocusMoveCallback != null) {
                          Camera.AutoFocusMoveCallback autoFocusMoveCallback = Camera.this.mAutoFocusMoveCallback;
                          bool2 = bool3;
                          if (param1Message.arg1 == 0)
                            bool2 = false; 
                          autoFocusMoveCallback.onAutoFocusMoving(bool2, this.mCamera);
                        } 
                        return;
                      } 
                      if (Camera.this.mFaceListener != null) {
                        Camera.this.mFaceListener.onFaceDetection((Camera.Face[])param1Message.obj, this.mCamera);
                        null = (Camera.Face[])param1Message.obj;
                        if (null != null)
                          Camera.this.mCameraStatisticsManager.setCurFaceCount(null.length); 
                      } 
                      return;
                    } 
                    if (Camera.this.mJpegCallback != null)
                      Camera.this.mJpegCallback.onPictureTaken((byte[])((Message)null).obj, this.mCamera); 
                    return;
                  } 
                  if (Camera.this.mRawImageCallback != null)
                    Camera.this.mRawImageCallback.onPictureTaken((byte[])((Message)null).obj, this.mCamera); 
                  return;
                } 
                if (Camera.this.mPostviewCallback != null)
                  Camera.this.mPostviewCallback.onPictureTaken((byte[])((Message)null).obj, this.mCamera); 
                return;
              } 
              Camera.PreviewCallback previewCallback = Camera.this.mPreviewCallback;
              if (previewCallback != null) {
                if (Camera.this.mOneShot) {
                  Camera.access$302(Camera.this, null);
                } else if (!Camera.this.mWithBuffer) {
                  Camera.this.setHasPreviewCallback(true, false);
                } 
                previewCallback.onPreviewFrame((byte[])((Message)null).obj, this.mCamera);
              } 
              return;
            } 
            if (Camera.this.mZoomListener != null) {
              Camera.OnZoomChangeListener onZoomChangeListener = Camera.this.mZoomListener;
              i = ((Message)null).arg1;
              if (((Message)null).arg2 != 0) {
                bool2 = bool1;
              } else {
                bool2 = false;
              } 
              onZoomChangeListener.onZoomChange(i, bool2, this.mCamera);
            } 
            return;
          } 
          synchronized (Camera.this.mAutoFocusCallbackLock) {
            Camera.AutoFocusCallback autoFocusCallback = Camera.this.mAutoFocusCallback;
            if (autoFocusCallback != null) {
              if (((Message)null).arg1 == 0)
                bool2 = false; 
              autoFocusCallback.onAutoFocus(bool2, this.mCamera);
            } 
            return;
          } 
        } 
        if (Camera.this.mShutterCallback != null)
          Camera.this.mShutterCallback.onShutter(); 
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error ");
      stringBuilder.append(param1Message.arg1);
      Log.e("Camera", stringBuilder.toString());
      if (Camera.this.mDetailedErrorCallback != null) {
        Camera.this.mDetailedErrorCallback.onError(param1Message.arg1, this.mCamera);
      } else if (Camera.this.mErrorCallback != null) {
        if (param1Message.arg1 == 3) {
          Camera.this.mErrorCallback.onError(2, this.mCamera);
        } else {
          Camera.this.mErrorCallback.onError(param1Message.arg1, this.mCamera);
        } 
      } 
    }
  }
  
  private static void postEventFromNative(Object paramObject1, int paramInt1, int paramInt2, int paramInt3, Object paramObject2) {
    paramObject1 = ((WeakReference<Camera>)paramObject1).get();
    if (paramObject1 == null)
      return; 
    EventHandler eventHandler = ((Camera)paramObject1).mEventHandler;
    if (eventHandler != null) {
      paramObject2 = eventHandler.obtainMessage(paramInt1, paramInt2, paramInt3, paramObject2);
      ((Camera)paramObject1).mEventHandler.sendMessage((Message)paramObject2);
    } 
  }
  
  public final void autoFocus(AutoFocusCallback paramAutoFocusCallback) {
    synchronized (this.mAutoFocusCallbackLock) {
      this.mAutoFocusCallback = paramAutoFocusCallback;
      native_autoFocus();
      return;
    } 
  }
  
  public final void cancelAutoFocus() {
    synchronized (this.mAutoFocusCallbackLock) {
      this.mAutoFocusCallback = null;
      native_cancelAutoFocus();
      this.mEventHandler.removeMessages(4);
      return;
    } 
  }
  
  public void setAutoFocusMoveCallback(AutoFocusMoveCallback paramAutoFocusMoveCallback) {
    boolean bool;
    this.mAutoFocusMoveCallback = paramAutoFocusMoveCallback;
    if (paramAutoFocusMoveCallback != null) {
      bool = true;
    } else {
      bool = false;
    } 
    enableFocusMoveCallback(bool);
  }
  
  public final void takePicture(ShutterCallback paramShutterCallback, PictureCallback paramPictureCallback1, PictureCallback paramPictureCallback2) {
    SeempLog.record(65);
    takePicture(paramShutterCallback, paramPictureCallback1, null, paramPictureCallback2);
  }
  
  public final void takePicture(ShutterCallback paramShutterCallback, PictureCallback paramPictureCallback1, PictureCallback paramPictureCallback2, PictureCallback paramPictureCallback3) {
    Parameters parameters;
    SeempLog.record(65);
    this.mShutterCallback = paramShutterCallback;
    this.mRawImageCallback = paramPictureCallback1;
    this.mPostviewCallback = paramPictureCallback2;
    this.mJpegCallback = paramPictureCallback3;
    paramShutterCallback = null;
    try {
      Parameters parameters1 = getParameters();
    } catch (Exception exception) {
      Log.e("Camera", "failure in getParameters");
    } 
    this.mCameraStatisticsManager.addCaptureInfo(parameters);
    int i = 0;
    if (this.mShutterCallback != null)
      i = 0x0 | 0x2; 
    int j = i;
    if (this.mRawImageCallback != null)
      j = i | 0x80; 
    i = j;
    if (this.mPostviewCallback != null)
      i = j | 0x40; 
    j = i;
    if (this.mJpegCallback != null)
      j = i | 0x100; 
    native_takePicture(j);
    this.mFaceDetectionRunning = false;
  }
  
  public final boolean enableShutterSound(boolean paramBoolean) {
    boolean bool1 = true, bool2 = true;
    IBinder iBinder = ServiceManager.getService("audio");
    null = IAudioService.Stub.asInterface(iBinder);
    try {
      boolean bool = null.isCameraSoundForced();
      bool1 = bool2;
      if (bool)
        bool1 = false; 
    } catch (RemoteException remoteException) {
      Log.e("Camera", "Audio service is unavailable for queries");
    } 
    if (!paramBoolean && !bool1)
      return false; 
    synchronized (this.mShutterSoundLock) {
      this.mShutterSoundEnabledFromApp = paramBoolean;
      boolean bool = _enableShutterSound(paramBoolean);
      if (paramBoolean && !this.mHasAppOpsPlayAudio) {
        Log.i("Camera", "Shutter sound is not allowed by AppOpsManager");
        if (bool1)
          _enableShutterSound(false); 
      } 
      return bool;
    } 
  }
  
  public final boolean disableShutterSound() {
    return _enableShutterSound(false);
  }
  
  class IAppOpsCallbackWrapper extends IAppOpsCallback.Stub {
    private final WeakReference<Camera> mWeakCamera;
    
    IAppOpsCallbackWrapper(Camera this$0) {
      this.mWeakCamera = new WeakReference<>(this$0);
    }
    
    public void opChanged(int param1Int1, int param1Int2, String param1String) {
      if (param1Int1 == 28) {
        Camera camera = this.mWeakCamera.get();
        if (camera != null)
          camera.updateAppOpsPlayAudio(); 
      } 
    }
  }
  
  private void updateAppOpsPlayAudio() {
    synchronized (this.mShutterSoundLock) {
      boolean bool = this.mHasAppOpsPlayAudio;
      int i = 1;
      try {
        boolean bool1;
        if (this.mAppOps != null) {
          IAppOpsService iAppOpsService = this.mAppOps;
          i = Process.myUid();
          String str = ActivityThread.currentPackageName();
          i = iAppOpsService.checkAudioOperation(28, 13, i, str);
        } 
        if (i == 0) {
          bool1 = true;
        } else {
          bool1 = false;
        } 
        this.mHasAppOpsPlayAudio = bool1;
      } catch (RemoteException remoteException) {
        Log.e("Camera", "AppOpsService check audio operation failed");
        this.mHasAppOpsPlayAudio = false;
      } 
      if (bool != this.mHasAppOpsPlayAudio)
        if (!this.mHasAppOpsPlayAudio) {
          IBinder iBinder = ServiceManager.getService("audio");
          IAudioService iAudioService = IAudioService.Stub.asInterface(iBinder);
          try {
            boolean bool1 = iAudioService.isCameraSoundForced();
            if (bool1)
              return; 
          } catch (RemoteException remoteException) {
            Log.e("Camera", "Audio service is unavailable for queries");
          } 
          _enableShutterSound(false);
        } else {
          enableShutterSound(this.mShutterSoundEnabledFromApp);
        }  
      return;
    } 
  }
  
  public final void setZoomChangeListener(OnZoomChangeListener paramOnZoomChangeListener) {
    this.mZoomListener = paramOnZoomChangeListener;
  }
  
  public final void setFaceDetectionListener(FaceDetectionListener paramFaceDetectionListener) {
    this.mFaceListener = paramFaceDetectionListener;
  }
  
  public final void startFaceDetection() {
    if (!this.mFaceDetectionRunning) {
      _startFaceDetection(0);
      this.mFaceDetectionRunning = true;
      return;
    } 
    throw new RuntimeException("Face detection is already running");
  }
  
  public final void stopFaceDetection() {
    _stopFaceDetection();
    this.mFaceDetectionRunning = false;
  }
  
  @Deprecated
  public static class Face {
    public int blinkDetected;
    
    public int faceRecognised;
    
    public int id;
    
    public Point leftEye;
    
    public Point mouth;
    
    public Rect rect;
    
    public Point rightEye;
    
    public int score;
    
    public int smileDegree;
    
    public int smileScore;
    
    public Face() {
      this.id = -1;
      this.leftEye = null;
      this.rightEye = null;
      this.mouth = null;
      this.smileDegree = 0;
      this.smileScore = 0;
      this.blinkDetected = 0;
      this.faceRecognised = 0;
    }
  }
  
  public final void setErrorCallback(ErrorCallback paramErrorCallback) {
    this.mErrorCallback = paramErrorCallback;
  }
  
  public final void setDetailedErrorCallback(ErrorCallback paramErrorCallback) {
    this.mDetailedErrorCallback = paramErrorCallback;
  }
  
  public void setParameters(Parameters paramParameters) {
    if (this.mUsingPreviewAllocation) {
      Size size1 = paramParameters.getPreviewSize();
      Size size2 = getParameters().getPreviewSize();
      if (size1.width != size2.width || size1.height != size2.height)
        throw new IllegalStateException("Cannot change preview size while a preview allocation is configured."); 
    } 
    native_setParameters(paramParameters.flatten());
  }
  
  public Parameters getParameters() {
    Parameters parameters = new Parameters();
    String str = native_getParameters();
    parameters.unflatten(str);
    return parameters;
  }
  
  public int getWBCurrentCCT() {
    Parameters parameters = new Parameters();
    String str = native_getParameters();
    parameters.unflatten(str);
    int i = 0;
    if (parameters.getWBCurrentCCT() != null)
      i = Integer.parseInt(parameters.getWBCurrentCCT()); 
    return i;
  }
  
  public static Parameters getEmptyParameters() {
    Camera camera = new Camera();
    Objects.requireNonNull(camera);
    return new Parameters();
  }
  
  private static int byteToInt(byte[] paramArrayOfbyte, int paramInt) {
    int i = 0;
    for (byte b = 0; b < 4; b++)
      i += (paramArrayOfbyte[3 - b + paramInt] & 0xFF) << (3 - b) * 8; 
    return i;
  }
  
  public final void setHistogramMode(CameraDataCallback paramCameraDataCallback) {
    boolean bool;
    this.mCameraDataCallback = paramCameraDataCallback;
    if (paramCameraDataCallback != null) {
      bool = true;
    } else {
      bool = false;
    } 
    native_setHistogramMode(bool);
  }
  
  public final void sendHistogramData() {
    native_sendHistogramData();
  }
  
  public final void setMetadataCb(CameraMetaDataCallback paramCameraMetaDataCallback) {
    boolean bool;
    this.mCameraMetaDataCallback = paramCameraMetaDataCallback;
    if (paramCameraMetaDataCallback != null) {
      bool = true;
    } else {
      bool = false;
    } 
    native_setMetadataCb(bool);
  }
  
  public final void sendMetaData() {
    native_sendMetaData();
  }
  
  public final void setLongshot(boolean paramBoolean) {
    native_setLongshot(paramBoolean);
  }
  
  public class Coordinate {
    final Camera this$0;
    
    public int xCoordinate;
    
    public int yCoordinate;
    
    public Coordinate(int param1Int1, int param1Int2) {
      this.xCoordinate = param1Int1;
      this.yCoordinate = param1Int2;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof Coordinate;
      boolean bool1 = false;
      if (!bool)
        return false; 
      param1Object = param1Object;
      bool = bool1;
      if (this.xCoordinate == ((Coordinate)param1Object).xCoordinate) {
        bool = bool1;
        if (this.yCoordinate == ((Coordinate)param1Object).yCoordinate)
          bool = true; 
      } 
      return bool;
    }
  }
  
  public int getCurrentFocusPosition() {
    Parameters parameters = new Parameters();
    String str = native_getParameters();
    parameters.unflatten(str);
    int i = -1;
    if (parameters.getCurrentFocusPosition() != null)
      i = Integer.parseInt(parameters.getCurrentFocusPosition()); 
    return i;
  }
  
  public static Parameters getParametersCopy(Parameters paramParameters) {
    if (paramParameters != null) {
      Camera camera = paramParameters.getOuter();
      Objects.requireNonNull(camera);
      Parameters parameters = new Parameters();
      parameters.copyFrom(paramParameters);
      return parameters;
    } 
    throw new NullPointerException("parameters must not be null");
  }
  
  Camera() {}
  
  private final native void _addCallbackBuffer(byte[] paramArrayOfbyte, int paramInt);
  
  private final native boolean _enableShutterSound(boolean paramBoolean);
  
  private static native void _getCameraInfo(int paramInt, CameraInfo paramCameraInfo);
  
  public static native int _getNumberOfCameras();
  
  private final native void _startFaceDetection(int paramInt);
  
  private final native void _stopFaceDetection();
  
  private final native void _stopPreview();
  
  private native void enableFocusMoveCallback(int paramInt);
  
  private final native void native_autoFocus();
  
  private final native void native_cancelAutoFocus();
  
  private final native String native_getParameters();
  
  private final native void native_release();
  
  private final native void native_sendHistogramData();
  
  private final native void native_sendMetaData();
  
  private final native void native_setHistogramMode(boolean paramBoolean);
  
  private final native void native_setLongshot(boolean paramBoolean);
  
  private final native void native_setMetadataCb(boolean paramBoolean);
  
  private final native void native_setParameters(String paramString);
  
  private final native int native_setup(Object paramObject, int paramInt1, int paramInt2, String paramString);
  
  private final native void native_takePicture(int paramInt);
  
  private final native void setHasPreviewCallback(boolean paramBoolean1, boolean paramBoolean2);
  
  private final native void setPreviewCallbackSurface(Surface paramSurface);
  
  public final native int getAudioRestriction();
  
  public final native void lock();
  
  public final native boolean previewEnabled();
  
  public final native void reconnect() throws IOException;
  
  public final native void setAudioRestriction(int paramInt);
  
  public final native void setDisplayOrientation(int paramInt);
  
  public final native void setPreviewSurface(Surface paramSurface) throws IOException;
  
  public final native void setPreviewTexture(SurfaceTexture paramSurfaceTexture) throws IOException;
  
  public final native void startPreview();
  
  public final native void startSmoothZoom(int paramInt);
  
  public final native void stopSmoothZoom();
  
  public final native void unlock();
  
  @Deprecated
  public class Size {
    public int height;
    
    final Camera this$0;
    
    public int width;
    
    public Size(int param1Int1, int param1Int2) {
      this.width = param1Int1;
      this.height = param1Int2;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof Size;
      boolean bool1 = false;
      if (!bool)
        return false; 
      param1Object = param1Object;
      bool = bool1;
      if (this.width == ((Size)param1Object).width) {
        bool = bool1;
        if (this.height == ((Size)param1Object).height)
          bool = true; 
      } 
      return bool;
    }
    
    public int hashCode() {
      return this.width * 32713 + this.height;
    }
  }
  
  @Deprecated
  public static class Area {
    public Rect rect;
    
    public int weight;
    
    public Area(Rect param1Rect, int param1Int) {
      this.rect = param1Rect;
      this.weight = param1Int;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof Area;
      boolean bool1 = false;
      if (!bool)
        return false; 
      param1Object = param1Object;
      Rect rect = this.rect;
      if (rect == null) {
        if (((Area)param1Object).rect != null)
          return false; 
      } else if (!rect.equals(((Area)param1Object).rect)) {
        return false;
      } 
      if (this.weight == ((Area)param1Object).weight)
        bool1 = true; 
      return bool1;
    }
  }
  
  @Deprecated
  public class Parameters {
    public static final String AE_BRACKET = "AE-Bracket";
    
    public static final String AE_BRACKET_HDR = "HDR";
    
    public static final String AE_BRACKET_HDR_OFF = "Off";
    
    public static final String ANTIBANDING_50HZ = "50hz";
    
    public static final String ANTIBANDING_60HZ = "60hz";
    
    public static final String ANTIBANDING_AUTO = "auto";
    
    public static final String ANTIBANDING_OFF = "off";
    
    public static final String AUTO_EXPOSURE_CENTER_WEIGHTED = "center-weighted";
    
    public static final String AUTO_EXPOSURE_FRAME_AVG = "frame-average";
    
    public static final String AUTO_EXPOSURE_SPOT_METERING = "spot-metering";
    
    public static final String CONTINUOUS_AF_OFF = "caf-off";
    
    public static final String CONTINUOUS_AF_ON = "caf-on";
    
    public static final String DENOISE_OFF = "denoise-off";
    
    public static final String DENOISE_ON = "denoise-on";
    
    public static final String EFFECT_AQUA = "aqua";
    
    public static final String EFFECT_BLACKBOARD = "blackboard";
    
    public static final String EFFECT_MONO = "mono";
    
    public static final String EFFECT_NEGATIVE = "negative";
    
    public static final String EFFECT_NONE = "none";
    
    public static final String EFFECT_POSTERIZE = "posterize";
    
    public static final String EFFECT_SEPIA = "sepia";
    
    public static final String EFFECT_SOLARIZE = "solarize";
    
    public static final String EFFECT_WHITEBOARD = "whiteboard";
    
    public static final String FACE_DETECTION_OFF = "off";
    
    public static final String FACE_DETECTION_ON = "on";
    
    private static final String FALSE = "false";
    
    public static final String FLASH_MODE_AUTO = "auto";
    
    public static final String FLASH_MODE_OFF = "off";
    
    public static final String FLASH_MODE_ON = "on";
    
    public static final String FLASH_MODE_RED_EYE = "red-eye";
    
    public static final String FLASH_MODE_TORCH = "torch";
    
    public static final int FOCUS_DISTANCE_FAR_INDEX = 2;
    
    public static final int FOCUS_DISTANCE_NEAR_INDEX = 0;
    
    public static final int FOCUS_DISTANCE_OPTIMAL_INDEX = 1;
    
    public static final String FOCUS_MODE_AUTO = "auto";
    
    public static final String FOCUS_MODE_CONTINUOUS_PICTURE = "continuous-picture";
    
    public static final String FOCUS_MODE_CONTINUOUS_VIDEO = "continuous-video";
    
    public static final String FOCUS_MODE_EDOF = "edof";
    
    public static final String FOCUS_MODE_FIXED = "fixed";
    
    public static final String FOCUS_MODE_INFINITY = "infinity";
    
    public static final String FOCUS_MODE_MACRO = "macro";
    
    public static final String FOCUS_MODE_MANUAL_POSITION = "manual";
    
    public static final String FOCUS_MODE_NORMAL = "normal";
    
    public static final String HISTOGRAM_DISABLE = "disable";
    
    public static final String HISTOGRAM_ENABLE = "enable";
    
    public static final String ISO_100 = "ISO100";
    
    public static final String ISO_1600 = "ISO1600";
    
    public static final String ISO_200 = "ISO200";
    
    public static final String ISO_3200 = "ISO3200";
    
    public static final String ISO_400 = "ISO400";
    
    public static final String ISO_800 = "ISO800";
    
    public static final String ISO_AUTO = "auto";
    
    public static final String ISO_HJR = "ISO_HJR";
    
    private static final String KEY_ANTIBANDING = "antibanding";
    
    private static final String KEY_AUTO_EXPOSURE_LOCK = "auto-exposure-lock";
    
    private static final String KEY_AUTO_EXPOSURE_LOCK_SUPPORTED = "auto-exposure-lock-supported";
    
    private static final String KEY_AUTO_WHITEBALANCE_LOCK = "auto-whitebalance-lock";
    
    private static final String KEY_AUTO_WHITEBALANCE_LOCK_SUPPORTED = "auto-whitebalance-lock-supported";
    
    private static final String KEY_EFFECT = "effect";
    
    private static final String KEY_EXPOSURE_COMPENSATION = "exposure-compensation";
    
    private static final String KEY_EXPOSURE_COMPENSATION_STEP = "exposure-compensation-step";
    
    private static final String KEY_FLASH_MODE = "flash-mode";
    
    private static final String KEY_FOCAL_LENGTH = "focal-length";
    
    private static final String KEY_FOCUS_AREAS = "focus-areas";
    
    private static final String KEY_FOCUS_DISTANCES = "focus-distances";
    
    private static final String KEY_FOCUS_MODE = "focus-mode";
    
    private static final String KEY_GPS_ALTITUDE = "gps-altitude";
    
    private static final String KEY_GPS_LATITUDE = "gps-latitude";
    
    private static final String KEY_GPS_LONGITUDE = "gps-longitude";
    
    private static final String KEY_GPS_PROCESSING_METHOD = "gps-processing-method";
    
    private static final String KEY_GPS_TIMESTAMP = "gps-timestamp";
    
    private static final String KEY_HORIZONTAL_VIEW_ANGLE = "horizontal-view-angle";
    
    private static final String KEY_JPEG_QUALITY = "jpeg-quality";
    
    private static final String KEY_JPEG_THUMBNAIL_HEIGHT = "jpeg-thumbnail-height";
    
    private static final String KEY_JPEG_THUMBNAIL_QUALITY = "jpeg-thumbnail-quality";
    
    private static final String KEY_JPEG_THUMBNAIL_SIZE = "jpeg-thumbnail-size";
    
    private static final String KEY_JPEG_THUMBNAIL_WIDTH = "jpeg-thumbnail-width";
    
    private static final String KEY_MAX_EXPOSURE_COMPENSATION = "max-exposure-compensation";
    
    private static final String KEY_MAX_NUM_DETECTED_FACES_HW = "max-num-detected-faces-hw";
    
    private static final String KEY_MAX_NUM_DETECTED_FACES_SW = "max-num-detected-faces-sw";
    
    private static final String KEY_MAX_NUM_FOCUS_AREAS = "max-num-focus-areas";
    
    private static final String KEY_MAX_NUM_METERING_AREAS = "max-num-metering-areas";
    
    private static final String KEY_MAX_ZOOM = "max-zoom";
    
    private static final String KEY_METERING_AREAS = "metering-areas";
    
    private static final String KEY_MIN_EXPOSURE_COMPENSATION = "min-exposure-compensation";
    
    private static final String KEY_PICTURE_FORMAT = "picture-format";
    
    private static final String KEY_PICTURE_SIZE = "picture-size";
    
    private static final String KEY_PREFERRED_PREVIEW_SIZE_FOR_VIDEO = "preferred-preview-size-for-video";
    
    private static final String KEY_PREVIEW_FORMAT = "preview-format";
    
    private static final String KEY_PREVIEW_FPS_RANGE = "preview-fps-range";
    
    private static final String KEY_PREVIEW_FRAME_RATE = "preview-frame-rate";
    
    private static final String KEY_PREVIEW_SIZE = "preview-size";
    
    public static final String KEY_QC_AE_BRACKET_HDR = "ae-bracket-hdr";
    
    private static final String KEY_QC_AUTO_EXPOSURE = "auto-exposure";
    
    private static final String KEY_QC_AUTO_HDR_ENABLE = "auto-hdr-enable";
    
    private static final String KEY_QC_CAMERA_MODE = "camera-mode";
    
    private static final String KEY_QC_CONTINUOUS_AF = "continuous-af";
    
    private static final String KEY_QC_CONTRAST = "contrast";
    
    private static final String KEY_QC_DENOISE = "denoise";
    
    private static final String KEY_QC_EXIF_DATETIME = "exif-datetime";
    
    private static final String KEY_QC_EXPOSURE_TIME = "exposure-time";
    
    private static final String KEY_QC_FACE_DETECTION = "face-detection";
    
    private static final String KEY_QC_GPS_ALTITUDE_REF = "gps-altitude-ref";
    
    private static final String KEY_QC_GPS_LATITUDE_REF = "gps-latitude-ref";
    
    private static final String KEY_QC_GPS_LONGITUDE_REF = "gps-longitude-ref";
    
    private static final String KEY_QC_GPS_STATUS = "gps-status";
    
    private static final String KEY_QC_HFR_SIZE = "hfr-size";
    
    private static final String KEY_QC_HISTOGRAM = "histogram";
    
    private static final String KEY_QC_ISO_MODE = "iso";
    
    private static final String KEY_QC_LENSSHADE = "lensshade";
    
    private static final String KEY_QC_MANUAL_FOCUS_POSITION = "manual-focus-position";
    
    private static final String KEY_QC_MANUAL_FOCUS_POS_TYPE = "manual-focus-pos-type";
    
    private static final String KEY_QC_MAX_CONTRAST = "max-contrast";
    
    private static final String KEY_QC_MAX_EXPOSURE_TIME = "max-exposure-time";
    
    private static final String KEY_QC_MAX_SATURATION = "max-saturation";
    
    private static final String KEY_QC_MAX_SHARPNESS = "max-sharpness";
    
    private static final String KEY_QC_MAX_WB_CCT = "max-wb-cct";
    
    private static final String KEY_QC_MEMORY_COLOR_ENHANCEMENT = "mce";
    
    private static final String KEY_QC_MIN_EXPOSURE_TIME = "min-exposure-time";
    
    private static final String KEY_QC_MIN_WB_CCT = "min-wb-cct";
    
    private static final String KEY_QC_POWER_MODE = "power-mode";
    
    private static final String KEY_QC_POWER_MODE_SUPPORTED = "power-mode-supported";
    
    private static final String KEY_QC_PREVIEW_FRAME_RATE_AUTO_MODE = "frame-rate-auto";
    
    private static final String KEY_QC_PREVIEW_FRAME_RATE_FIXED_MODE = "frame-rate-fixed";
    
    private static final String KEY_QC_PREVIEW_FRAME_RATE_MODE = "preview-frame-rate-mode";
    
    private static final String KEY_QC_REDEYE_REDUCTION = "redeye-reduction";
    
    private static final String KEY_QC_SATURATION = "saturation";
    
    private static final String KEY_QC_SCENE_DETECT = "scene-detect";
    
    private static final String KEY_QC_SELECTABLE_ZONE_AF = "selectable-zone-af";
    
    private static final String KEY_QC_SHARPNESS = "sharpness";
    
    private static final String KEY_QC_SKIN_TONE_ENHANCEMENT = "skinToneEnhancement";
    
    private static final String KEY_QC_TOUCH_AF_AEC = "touch-af-aec";
    
    private static final String KEY_QC_TOUCH_INDEX_AEC = "touch-index-aec";
    
    private static final String KEY_QC_TOUCH_INDEX_AF = "touch-index-af";
    
    private static final String KEY_QC_VIDEO_HDR = "video-hdr";
    
    private static final String KEY_QC_VIDEO_HIGH_FRAME_RATE = "video-hfr";
    
    private static final String KEY_QC_VIDEO_ROTATION = "video-rotation";
    
    private static final String KEY_QC_WB_MANUAL_CCT = "wb-manual-cct";
    
    private static final String KEY_QC_ZSL = "zsl";
    
    private static final String KEY_RECORDING_HINT = "recording-hint";
    
    private static final String KEY_ROTATION = "rotation";
    
    private static final String KEY_SCENE_MODE = "scene-mode";
    
    private static final String KEY_SMOOTH_ZOOM_SUPPORTED = "smooth-zoom-supported";
    
    private static final String KEY_VERTICAL_VIEW_ANGLE = "vertical-view-angle";
    
    private static final String KEY_VIDEO_SIZE = "video-size";
    
    private static final String KEY_VIDEO_SNAPSHOT_SUPPORTED = "video-snapshot-supported";
    
    private static final String KEY_VIDEO_STABILIZATION = "video-stabilization";
    
    private static final String KEY_VIDEO_STABILIZATION_SUPPORTED = "video-stabilization-supported";
    
    private static final String KEY_WHITE_BALANCE = "whitebalance";
    
    private static final String KEY_ZOOM = "zoom";
    
    private static final String KEY_ZOOM_RATIOS = "zoom-ratios";
    
    private static final String KEY_ZOOM_SUPPORTED = "zoom-supported";
    
    public static final String LENSSHADE_DISABLE = "disable";
    
    public static final String LENSSHADE_ENABLE = "enable";
    
    public static final String LOW_POWER = "Low_Power";
    
    private static final int MANUAL_FOCUS_POS_TYPE_DAC = 1;
    
    private static final int MANUAL_FOCUS_POS_TYPE_INDEX = 0;
    
    public static final String MCE_DISABLE = "disable";
    
    public static final String MCE_ENABLE = "enable";
    
    public static final String NORMAL_POWER = "Normal_Power";
    
    private static final String PIXEL_FORMAT_BAYER_RGGB = "bayer-rggb";
    
    private static final String PIXEL_FORMAT_JPEG = "jpeg";
    
    private static final String PIXEL_FORMAT_NV12 = "nv12";
    
    private static final String PIXEL_FORMAT_RAW = "raw";
    
    private static final String PIXEL_FORMAT_RGB565 = "rgb565";
    
    private static final String PIXEL_FORMAT_YUV420P = "yuv420p";
    
    private static final String PIXEL_FORMAT_YUV420SP = "yuv420sp";
    
    private static final String PIXEL_FORMAT_YUV420SP_ADRENO = "yuv420sp-adreno";
    
    private static final String PIXEL_FORMAT_YUV422I = "yuv422i-yuyv";
    
    private static final String PIXEL_FORMAT_YUV422SP = "yuv422sp";
    
    private static final String PIXEL_FORMAT_YV12 = "yv12";
    
    public static final int PREVIEW_FPS_MAX_INDEX = 1;
    
    public static final int PREVIEW_FPS_MIN_INDEX = 0;
    
    public static final String REDEYE_REDUCTION_DISABLE = "disable";
    
    public static final String REDEYE_REDUCTION_ENABLE = "enable";
    
    public static final String SCENE_DETECT_OFF = "off";
    
    public static final String SCENE_DETECT_ON = "on";
    
    public static final String SCENE_MODE_ACTION = "action";
    
    public static final String SCENE_MODE_ASD = "asd";
    
    public static final String SCENE_MODE_AUTO = "auto";
    
    public static final String SCENE_MODE_BACKLIGHT = "backlight";
    
    public static final String SCENE_MODE_BARCODE = "barcode";
    
    public static final String SCENE_MODE_BEACH = "beach";
    
    public static final String SCENE_MODE_CANDLELIGHT = "candlelight";
    
    public static final String SCENE_MODE_FIREWORKS = "fireworks";
    
    public static final String SCENE_MODE_FLOWERS = "flowers";
    
    public static final String SCENE_MODE_HDR = "hdr";
    
    public static final String SCENE_MODE_LANDSCAPE = "landscape";
    
    public static final String SCENE_MODE_NIGHT = "night";
    
    public static final String SCENE_MODE_NIGHT_PORTRAIT = "night-portrait";
    
    public static final String SCENE_MODE_PARTY = "party";
    
    public static final String SCENE_MODE_PORTRAIT = "portrait";
    
    public static final String SCENE_MODE_SNOW = "snow";
    
    public static final String SCENE_MODE_SPORTS = "sports";
    
    public static final String SCENE_MODE_STEADYPHOTO = "steadyphoto";
    
    public static final String SCENE_MODE_SUNSET = "sunset";
    
    public static final String SCENE_MODE_THEATRE = "theatre";
    
    public static final String SELECTABLE_ZONE_AF_AUTO = "auto";
    
    public static final String SELECTABLE_ZONE_AF_CENTER_WEIGHTED = "center-weighted";
    
    public static final String SELECTABLE_ZONE_AF_FRAME_AVERAGE = "frame-average";
    
    public static final String SELECTABLE_ZONE_AF_SPOTMETERING = "spot-metering";
    
    public static final String SKIN_TONE_ENHANCEMENT_DISABLE = "disable";
    
    public static final String SKIN_TONE_ENHANCEMENT_ENABLE = "enable";
    
    private static final String SUPPORTED_VALUES_SUFFIX = "-values";
    
    public static final String TOUCH_AF_AEC_OFF = "touch-off";
    
    public static final String TOUCH_AF_AEC_ON = "touch-on";
    
    private static final String TRUE = "true";
    
    public static final String VIDEO_HFR_2X = "60";
    
    public static final String VIDEO_HFR_3X = "90";
    
    public static final String VIDEO_HFR_4X = "120";
    
    public static final String VIDEO_HFR_OFF = "off";
    
    public static final String VIDEO_ROTATION_0 = "0";
    
    public static final String VIDEO_ROTATION_180 = "180";
    
    public static final String VIDEO_ROTATION_270 = "270";
    
    public static final String VIDEO_ROTATION_90 = "90";
    
    public static final String WHITE_BALANCE_AUTO = "auto";
    
    public static final String WHITE_BALANCE_CLOUDY_DAYLIGHT = "cloudy-daylight";
    
    public static final String WHITE_BALANCE_DAYLIGHT = "daylight";
    
    public static final String WHITE_BALANCE_FLUORESCENT = "fluorescent";
    
    public static final String WHITE_BALANCE_INCANDESCENT = "incandescent";
    
    public static final String WHITE_BALANCE_MANUAL_CCT = "manual-cct";
    
    public static final String WHITE_BALANCE_SHADE = "shade";
    
    public static final String WHITE_BALANCE_TWILIGHT = "twilight";
    
    public static final String WHITE_BALANCE_WARM_FLUORESCENT = "warm-fluorescent";
    
    public static final String ZSL_OFF = "off";
    
    public static final String ZSL_ON = "on";
    
    private final LinkedHashMap<String, String> mMap = new LinkedHashMap<>(64);
    
    final Camera this$0;
    
    public void copyFrom(Parameters param1Parameters) {
      if (param1Parameters != null) {
        this.mMap.putAll(param1Parameters.mMap);
        return;
      } 
      throw new NullPointerException("other must not be null");
    }
    
    private Camera getOuter() {
      return Camera.this;
    }
    
    public boolean same(Parameters param1Parameters) {
      boolean bool = true;
      if (this == param1Parameters)
        return true; 
      if (param1Parameters == null || !this.mMap.equals(param1Parameters.mMap))
        bool = false; 
      return bool;
    }
    
    @Deprecated
    public void dump() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("dump: size=");
      stringBuilder.append(this.mMap.size());
      Log.e("Camera", stringBuilder.toString());
      for (String str : this.mMap.keySet()) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("dump: ");
        stringBuilder1.append(str);
        stringBuilder1.append("=");
        stringBuilder1.append(this.mMap.get(str));
        Log.e("Camera", stringBuilder1.toString());
      } 
    }
    
    public String flatten() {
      StringBuilder stringBuilder = new StringBuilder(128);
      for (String str : this.mMap.keySet()) {
        stringBuilder.append(str);
        stringBuilder.append("=");
        stringBuilder.append(this.mMap.get(str));
        stringBuilder.append(";");
      } 
      stringBuilder.deleteCharAt(stringBuilder.length() - 1);
      return stringBuilder.toString();
    }
    
    public void unflatten(String param1String) {
      this.mMap.clear();
      TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(';');
      simpleStringSplitter.setString(param1String);
      for (String str : simpleStringSplitter) {
        int i = str.indexOf('=');
        if (i == -1)
          continue; 
        param1String = str.substring(0, i);
        str = str.substring(i + 1);
        this.mMap.put(param1String, str);
      } 
    }
    
    public void remove(String param1String) {
      this.mMap.remove(param1String);
    }
    
    public void set(String param1String1, String param1String2) {
      StringBuilder stringBuilder1, stringBuilder2;
      if (param1String1.indexOf('=') != -1 || param1String1.indexOf(';') != -1 || param1String1.indexOf(false) != -1) {
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append("Key \"");
        stringBuilder2.append(param1String1);
        stringBuilder2.append("\" contains invalid character (= or ; or \\0)");
        Log.e("Camera", stringBuilder2.toString());
        return;
      } 
      if (stringBuilder2.indexOf('=') != -1 || stringBuilder2.indexOf(';') != -1 || stringBuilder2.indexOf(false) != -1) {
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Value \"");
        stringBuilder1.append((String)stringBuilder2);
        stringBuilder1.append("\" contains invalid character (= or ; or \\0)");
        Log.e("Camera", stringBuilder1.toString());
        return;
      } 
      put((String)stringBuilder1, (String)stringBuilder2);
    }
    
    public void set(String param1String, int param1Int) {
      put(param1String, Integer.toString(param1Int));
    }
    
    private void put(String param1String1, String param1String2) {
      this.mMap.remove(param1String1);
      this.mMap.put(param1String1, param1String2);
    }
    
    private void set(String param1String, List<Camera.Area> param1List) {
      if (param1List == null) {
        set(param1String, "(0,0,0,0,0)");
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        for (byte b = 0; b < param1List.size(); b++) {
          Camera.Area area = param1List.get(b);
          Rect rect = area.rect;
          stringBuilder.append('(');
          stringBuilder.append(rect.left);
          stringBuilder.append(',');
          stringBuilder.append(rect.top);
          stringBuilder.append(',');
          stringBuilder.append(rect.right);
          stringBuilder.append(',');
          stringBuilder.append(rect.bottom);
          stringBuilder.append(',');
          stringBuilder.append(area.weight);
          stringBuilder.append(')');
          if (b != param1List.size() - 1)
            stringBuilder.append(','); 
        } 
        set(param1String, stringBuilder.toString());
      } 
    }
    
    public String get(String param1String) {
      return this.mMap.get(param1String);
    }
    
    public int getInt(String param1String) {
      return Integer.parseInt(this.mMap.get(param1String));
    }
    
    public void setPreviewSize(int param1Int1, int param1Int2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(Integer.toString(param1Int1));
      stringBuilder.append("x");
      stringBuilder.append(Integer.toString(param1Int2));
      String str = stringBuilder.toString();
      set("preview-size", str);
    }
    
    public Camera.Size getPreviewSize() {
      String str = get("preview-size");
      return strToSize(str);
    }
    
    public List<Camera.Size> getSupportedPreviewSizes() {
      String str = get("preview-size-values");
      return splitSize(str);
    }
    
    public List<Camera.Size> getSupportedVideoSizes() {
      String str = get("video-size-values");
      return splitSize(str);
    }
    
    public Camera.Size getPreferredPreviewSizeForVideo() {
      String str = get("preferred-preview-size-for-video");
      return strToSize(str);
    }
    
    public void setJpegThumbnailSize(int param1Int1, int param1Int2) {
      set("jpeg-thumbnail-width", param1Int1);
      set("jpeg-thumbnail-height", param1Int2);
    }
    
    public Camera.Size getJpegThumbnailSize() {
      Camera camera = Camera.this;
      int i = getInt("jpeg-thumbnail-width");
      return 
        new Camera.Size(i, getInt("jpeg-thumbnail-height"));
    }
    
    public List<Camera.Size> getSupportedJpegThumbnailSizes() {
      String str = get("jpeg-thumbnail-size-values");
      return splitSize(str);
    }
    
    public void setJpegThumbnailQuality(int param1Int) {
      set("jpeg-thumbnail-quality", param1Int);
    }
    
    public int getJpegThumbnailQuality() {
      return getInt("jpeg-thumbnail-quality");
    }
    
    public void setJpegQuality(int param1Int) {
      set("jpeg-quality", param1Int);
    }
    
    public int getJpegQuality() {
      return getInt("jpeg-quality");
    }
    
    @Deprecated
    public void setPreviewFrameRate(int param1Int) {
      set("preview-frame-rate", param1Int);
    }
    
    @Deprecated
    public int getPreviewFrameRate() {
      return getInt("preview-frame-rate");
    }
    
    @Deprecated
    public List<Integer> getSupportedPreviewFrameRates() {
      String str = get("preview-frame-rate-values");
      return splitInt(str);
    }
    
    public void setPreviewFpsRange(int param1Int1, int param1Int2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("");
      stringBuilder.append(param1Int1);
      stringBuilder.append(",");
      stringBuilder.append(param1Int2);
      set("preview-fps-range", stringBuilder.toString());
    }
    
    public void getPreviewFpsRange(int[] param1ArrayOfint) {
      if (param1ArrayOfint != null && param1ArrayOfint.length == 2) {
        splitInt(get("preview-fps-range"), param1ArrayOfint);
        return;
      } 
      throw new IllegalArgumentException("range must be an array with two elements.");
    }
    
    public List<int[]> getSupportedPreviewFpsRange() {
      String str = get("preview-fps-range-values");
      return (List<int[]>)splitRange(str);
    }
    
    public void setPreviewFormat(int param1Int) {
      String str = cameraFormatForPixelFormat(param1Int);
      if (str != null) {
        set("preview-format", str);
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid pixel_format=");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public int getPreviewFormat() {
      return pixelFormatForCameraFormat(get("preview-format"));
    }
    
    public List<Integer> getSupportedPreviewFormats() {
      str = get("preview-format-values");
      ArrayList<Integer> arrayList = new ArrayList();
      for (String str : split(str)) {
        int i = pixelFormatForCameraFormat(str);
        if (i == 0)
          continue; 
        arrayList.add(Integer.valueOf(i));
      } 
      return arrayList;
    }
    
    public void setPictureSize(int param1Int1, int param1Int2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(Integer.toString(param1Int1));
      stringBuilder.append("x");
      stringBuilder.append(Integer.toString(param1Int2));
      String str = stringBuilder.toString();
      set("picture-size", str);
    }
    
    public Camera.Size getPictureSize() {
      String str = get("picture-size");
      return strToSize(str);
    }
    
    public List<Camera.Size> getSupportedPictureSizes() {
      String str = get("picture-size-values");
      return splitSize(str);
    }
    
    public void setPictureFormat(int param1Int) {
      String str = cameraFormatForPixelFormat(param1Int);
      if (str != null) {
        set("picture-format", str);
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid pixel_format=");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public int getPictureFormat() {
      return pixelFormatForCameraFormat(get("picture-format"));
    }
    
    public List<Integer> getSupportedPictureFormats() {
      str = get("picture-format-values");
      ArrayList<Integer> arrayList = new ArrayList();
      for (String str : split(str)) {
        int i = pixelFormatForCameraFormat(str);
        if (i == 0)
          continue; 
        arrayList.add(Integer.valueOf(i));
      } 
      return arrayList;
    }
    
    private String cameraFormatForPixelFormat(int param1Int) {
      if (param1Int != 4) {
        if (param1Int != 20) {
          if (param1Int != 256) {
            if (param1Int != 842094169) {
              if (param1Int != 16) {
                if (param1Int != 17)
                  return null; 
                return "yuv420sp";
              } 
              return "yuv422sp";
            } 
            return "yuv420p";
          } 
          return "jpeg";
        } 
        return "yuv422i-yuyv";
      } 
      return "rgb565";
    }
    
    private int pixelFormatForCameraFormat(String param1String) {
      if (param1String == null)
        return 0; 
      if (param1String.equals("yuv422sp"))
        return 16; 
      if (param1String.equals("yuv420sp"))
        return 17; 
      if (param1String.equals("yuv422i-yuyv"))
        return 20; 
      if (param1String.equals("yuv420p"))
        return 842094169; 
      if (param1String.equals("rgb565"))
        return 4; 
      if (param1String.equals("jpeg"))
        return 256; 
      return 0;
    }
    
    public void setRotation(int param1Int) {
      if (param1Int == 0 || param1Int == 90 || param1Int == 180 || param1Int == 270) {
        set("rotation", Integer.toString(param1Int));
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid rotation=");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public void setGpsLatitude(double param1Double) {
      set("gps-latitude", Double.toString(param1Double));
    }
    
    public void setGpsLongitude(double param1Double) {
      set("gps-longitude", Double.toString(param1Double));
    }
    
    public void setGpsAltitude(double param1Double) {
      set("gps-altitude", Double.toString(param1Double));
    }
    
    public void setGpsTimestamp(long param1Long) {
      set("gps-timestamp", Long.toString(param1Long));
    }
    
    public void setGpsProcessingMethod(String param1String) {
      set("gps-processing-method", param1String);
    }
    
    public void removeGpsData() {
      remove("gps-latitude-ref");
      remove("gps-latitude");
      remove("gps-longitude-ref");
      remove("gps-longitude");
      remove("gps-altitude-ref");
      remove("gps-altitude");
      remove("gps-timestamp");
      remove("gps-processing-method");
    }
    
    public String getWhiteBalance() {
      return get("whitebalance");
    }
    
    public void setWhiteBalance(String param1String) {
      String str = get("whitebalance");
      if (same(param1String, str))
        return; 
      set("whitebalance", param1String);
      set("auto-whitebalance-lock", "false");
    }
    
    public List<String> getSupportedWhiteBalance() {
      String str = get("whitebalance-values");
      return split(str);
    }
    
    public String getColorEffect() {
      return get("effect");
    }
    
    public void setColorEffect(String param1String) {
      set("effect", param1String);
    }
    
    public List<String> getSupportedColorEffects() {
      String str = get("effect-values");
      return split(str);
    }
    
    public String getAntibanding() {
      return get("antibanding");
    }
    
    public void setAntibanding(String param1String) {
      set("antibanding", param1String);
    }
    
    public List<String> getSupportedAntibanding() {
      String str = get("antibanding-values");
      return split(str);
    }
    
    public String getSceneMode() {
      return get("scene-mode");
    }
    
    public void setSceneMode(String param1String) {
      set("scene-mode", param1String);
    }
    
    public List<String> getSupportedSceneModes() {
      String str = get("scene-mode-values");
      return split(str);
    }
    
    public String getFlashMode() {
      return get("flash-mode");
    }
    
    public void setFlashMode(String param1String) {
      set("flash-mode", param1String);
    }
    
    public List<String> getSupportedFlashModes() {
      String str = get("flash-mode-values");
      return split(str);
    }
    
    public String getFocusMode() {
      return get("focus-mode");
    }
    
    public void setFocusMode(String param1String) {
      set("focus-mode", param1String);
    }
    
    public List<String> getSupportedFocusModes() {
      String str = get("focus-mode-values");
      return split(str);
    }
    
    public float getFocalLength() {
      return Float.parseFloat(get("focal-length"));
    }
    
    public float getHorizontalViewAngle() {
      return Float.parseFloat(get("horizontal-view-angle"));
    }
    
    public float getVerticalViewAngle() {
      return Float.parseFloat(get("vertical-view-angle"));
    }
    
    public int getExposureCompensation() {
      return getInt("exposure-compensation", 0);
    }
    
    public void setExposureCompensation(int param1Int) {
      set("exposure-compensation", param1Int);
    }
    
    public int getMaxExposureCompensation() {
      return getInt("max-exposure-compensation", 0);
    }
    
    public int getMinExposureCompensation() {
      return getInt("min-exposure-compensation", 0);
    }
    
    public float getExposureCompensationStep() {
      return getFloat("exposure-compensation-step", 0.0F);
    }
    
    public void setAutoExposureLock(boolean param1Boolean) {
      String str;
      if (param1Boolean) {
        str = "true";
      } else {
        str = "false";
      } 
      set("auto-exposure-lock", str);
    }
    
    public boolean getAutoExposureLock() {
      String str = get("auto-exposure-lock");
      return "true".equals(str);
    }
    
    public boolean isAutoExposureLockSupported() {
      String str = get("auto-exposure-lock-supported");
      return "true".equals(str);
    }
    
    public void setAutoWhiteBalanceLock(boolean param1Boolean) {
      String str;
      if (param1Boolean) {
        str = "true";
      } else {
        str = "false";
      } 
      set("auto-whitebalance-lock", str);
    }
    
    public boolean getAutoWhiteBalanceLock() {
      String str = get("auto-whitebalance-lock");
      return "true".equals(str);
    }
    
    public boolean isAutoWhiteBalanceLockSupported() {
      String str = get("auto-whitebalance-lock-supported");
      return "true".equals(str);
    }
    
    public int getZoom() {
      return getInt("zoom", 0);
    }
    
    public void setZoom(int param1Int) {
      set("zoom", param1Int);
    }
    
    public boolean isZoomSupported() {
      String str = get("zoom-supported");
      return "true".equals(str);
    }
    
    public int getMaxZoom() {
      return getInt("max-zoom", 0);
    }
    
    public List<Integer> getZoomRatios() {
      return splitInt(get("zoom-ratios"));
    }
    
    public boolean isSmoothZoomSupported() {
      String str = get("smooth-zoom-supported");
      return "true".equals(str);
    }
    
    public void getFocusDistances(float[] param1ArrayOffloat) {
      if (param1ArrayOffloat != null && param1ArrayOffloat.length == 3) {
        splitFloat(get("focus-distances"), param1ArrayOffloat);
        return;
      } 
      throw new IllegalArgumentException("output must be a float array with three elements.");
    }
    
    public int getMaxNumFocusAreas() {
      return getInt("max-num-focus-areas", 0);
    }
    
    public List<Camera.Area> getFocusAreas() {
      return splitArea(get("focus-areas"));
    }
    
    public void setFocusAreas(List<Camera.Area> param1List) {
      set("focus-areas", param1List);
    }
    
    public int getMaxNumMeteringAreas() {
      return getInt("max-num-metering-areas", 0);
    }
    
    public List<Camera.Area> getMeteringAreas() {
      return splitArea(get("metering-areas"));
    }
    
    public void setMeteringAreas(List<Camera.Area> param1List) {
      set("metering-areas", param1List);
    }
    
    public int getMaxNumDetectedFaces() {
      return getInt("max-num-detected-faces-hw", 0);
    }
    
    public void setRecordingHint(boolean param1Boolean) {
      String str;
      if (param1Boolean) {
        str = "true";
      } else {
        str = "false";
      } 
      set("recording-hint", str);
    }
    
    public boolean isVideoSnapshotSupported() {
      String str = get("video-snapshot-supported");
      return "true".equals(str);
    }
    
    public void setVideoStabilization(boolean param1Boolean) {
      String str;
      if (param1Boolean) {
        str = "true";
      } else {
        str = "false";
      } 
      set("video-stabilization", str);
    }
    
    public boolean getVideoStabilization() {
      String str = get("video-stabilization");
      return "true".equals(str);
    }
    
    public boolean isVideoStabilizationSupported() {
      String str = get("video-stabilization-supported");
      return "true".equals(str);
    }
    
    private ArrayList<String> split(String param1String) {
      if (param1String == null)
        return null; 
      TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(',');
      simpleStringSplitter.setString(param1String);
      ArrayList<String> arrayList = new ArrayList();
      for (String str : simpleStringSplitter)
        arrayList.add(str); 
      return arrayList;
    }
    
    private ArrayList<Integer> splitInt(String param1String) {
      if (param1String == null)
        return null; 
      TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(',');
      simpleStringSplitter.setString(param1String);
      ArrayList<Integer> arrayList = new ArrayList();
      for (String str : simpleStringSplitter)
        arrayList.add(Integer.valueOf(Integer.parseInt(str))); 
      if (arrayList.size() == 0)
        return null; 
      return arrayList;
    }
    
    private void splitInt(String param1String, int[] param1ArrayOfint) {
      if (param1String == null)
        return; 
      TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(',');
      simpleStringSplitter.setString(param1String);
      byte b = 0;
      for (String str : simpleStringSplitter) {
        param1ArrayOfint[b] = Integer.parseInt(str);
        b++;
      } 
    }
    
    private void splitFloat(String param1String, float[] param1ArrayOffloat) {
      if (param1String == null)
        return; 
      TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(',');
      simpleStringSplitter.setString(param1String);
      byte b = 0;
      for (String str : simpleStringSplitter) {
        param1ArrayOffloat[b] = Float.parseFloat(str);
        b++;
      } 
    }
    
    private float getFloat(String param1String, float param1Float) {
      try {
        return Float.parseFloat(this.mMap.get(param1String));
      } catch (NumberFormatException numberFormatException) {
        return param1Float;
      } 
    }
    
    private int getInt(String param1String, int param1Int) {
      try {
        return Integer.parseInt(this.mMap.get(param1String));
      } catch (NumberFormatException numberFormatException) {
        return param1Int;
      } 
    }
    
    private ArrayList<Camera.Size> splitSize(String param1String) {
      if (param1String == null)
        return null; 
      TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(',');
      simpleStringSplitter.setString(param1String);
      ArrayList<Camera.Size> arrayList = new ArrayList();
      for (String str : simpleStringSplitter) {
        Camera.Size size = strToSize(str);
        if (size != null)
          arrayList.add(size); 
      } 
      if (arrayList.size() == 0)
        return null; 
      return arrayList;
    }
    
    private Camera.Size strToSize(String param1String) {
      if (param1String == null)
        return null; 
      int i = param1String.indexOf('x');
      if (i != -1) {
        String str1 = param1String.substring(0, i);
        String str2 = param1String.substring(i + 1);
        Camera camera = Camera.this;
        i = Integer.parseInt(str1);
        return 
          new Camera.Size(i, Integer.parseInt(str2));
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid size parameter string=");
      stringBuilder.append(param1String);
      Log.e("Camera", stringBuilder.toString());
      return null;
    }
    
    private ArrayList<int[]> splitRange(String param1String) {
      int j;
      if (param1String == null || param1String.charAt(0) != '(' || 
        param1String.charAt(param1String.length() - 1) != ')') {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid range list string=");
        stringBuilder.append(param1String);
        Log.e("Camera", stringBuilder.toString());
        return null;
      } 
      ArrayList<int[]> arrayList = new ArrayList();
      int i = 1;
      do {
        int[] arrayOfInt = new int[2];
        int k = param1String.indexOf("),(", i);
        j = k;
        if (k == -1)
          j = param1String.length() - 1; 
        splitInt(param1String.substring(i, j), arrayOfInt);
        arrayList.add(arrayOfInt);
        i = j + 3;
      } while (j != param1String.length() - 1);
      if (arrayList.size() == 0)
        return null; 
      return arrayList;
    }
    
    private ArrayList<Camera.Area> splitArea(String param1String) {
      int j;
      if (param1String == null || param1String.charAt(0) != '(' || 
        param1String.charAt(param1String.length() - 1) != ')') {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid area string=");
        stringBuilder.append(param1String);
        Log.e("Camera", stringBuilder.toString());
        return null;
      } 
      ArrayList<Camera.Area> arrayList = new ArrayList();
      int i = 1;
      int[] arrayOfInt = new int[5];
      do {
        int k = param1String.indexOf("),(", i);
        j = k;
        if (k == -1)
          j = param1String.length() - 1; 
        splitInt(param1String.substring(i, j), arrayOfInt);
        Rect rect = new Rect(arrayOfInt[0], arrayOfInt[1], arrayOfInt[2], arrayOfInt[3]);
        arrayList.add(new Camera.Area(rect, arrayOfInt[4]));
        i = j + 3;
      } while (j != param1String.length() - 1);
      if (arrayList.size() == 0)
        return null; 
      if (arrayList.size() == 1) {
        Camera.Area area = arrayList.get(0);
        Rect rect = area.rect;
        if (rect.left == 0 && rect.top == 0 && rect.right == 0 && rect.bottom == 0 && area.weight == 0)
          return null; 
      } 
      return arrayList;
    }
    
    private boolean same(String param1String1, String param1String2) {
      if (param1String1 == null && param1String2 == null)
        return true; 
      if (param1String1 != null && param1String1.equals(param1String2))
        return true; 
      return false;
    }
    
    public List<Camera.Size> getSupportedHfrSizes() {
      String str = get("hfr-size-values");
      return splitSize(str);
    }
    
    public List<String> getSupportedTouchAfAec() {
      String str = get("touch-af-aec-values");
      return split(str);
    }
    
    public List<String> getSupportedPreviewFrameRateModes() {
      String str = get("preview-frame-rate-mode-values");
      return split(str);
    }
    
    public List<String> getSupportedSceneDetectModes() {
      String str = get("scene-detect-values");
      return split(str);
    }
    
    public List<String> getSupportedIsoValues() {
      String str = get("iso-values");
      return split(str);
    }
    
    public List<String> getSupportedLensShadeModes() {
      String str = get("lensshade-values");
      return split(str);
    }
    
    public List<String> getSupportedHistogramModes() {
      String str = get("histogram-values");
      return split(str);
    }
    
    public List<String> getSupportedSkinToneEnhancementModes() {
      String str = get("skinToneEnhancement-values");
      return split(str);
    }
    
    public List<String> getSupportedAutoexposure() {
      String str = get("auto-exposure-values");
      return split(str);
    }
    
    public List<String> getSupportedMemColorEnhanceModes() {
      String str = get("mce-values");
      return split(str);
    }
    
    public List<String> getSupportedZSLModes() {
      String str = get("zsl-values");
      return split(str);
    }
    
    public List<String> getSupportedVideoHDRModes() {
      String str = get("video-hdr-values");
      return split(str);
    }
    
    public List<String> getSupportedVideoHighFrameRateModes() {
      String str = get("video-hfr-values");
      return split(str);
    }
    
    public List<String> getSupportedContinuousAfModes() {
      String str = get("continuous-af-values");
      return split(str);
    }
    
    public List<String> getSupportedDenoiseModes() {
      String str = get("denoise-values");
      return split(str);
    }
    
    public List<String> getSupportedSelectableZoneAf() {
      String str = get("selectable-zone-af-values");
      return split(str);
    }
    
    public List<String> getSupportedFaceDetectionModes() {
      String str = get("face-detection-values");
      return split(str);
    }
    
    public List<String> getSupportedRedeyeReductionModes() {
      String str = get("redeye-reduction-values");
      return split(str);
    }
    
    public void setGpsAltitudeRef(double param1Double) {
      set("gps-altitude-ref", Double.toString(param1Double));
    }
    
    public void setGpsStatus(double param1Double) {
      set("gps-status", Double.toString(param1Double));
    }
    
    public void setTouchIndexAec(int param1Int1, int param1Int2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(Integer.toString(param1Int1));
      stringBuilder.append("x");
      stringBuilder.append(Integer.toString(param1Int2));
      String str = stringBuilder.toString();
      set("touch-index-aec", str);
    }
    
    public Camera.Coordinate getTouchIndexAec() {
      String str = get("touch-index-aec");
      return strToCoordinate(str);
    }
    
    public void setTouchIndexAf(int param1Int1, int param1Int2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(Integer.toString(param1Int1));
      stringBuilder.append("x");
      stringBuilder.append(Integer.toString(param1Int2));
      String str = stringBuilder.toString();
      set("touch-index-af", str);
    }
    
    public Camera.Coordinate getTouchIndexAf() {
      String str = get("touch-index-af");
      return strToCoordinate(str);
    }
    
    public void setSharpness(int param1Int) {
      if (param1Int >= 0 && param1Int <= getMaxSharpness()) {
        set("sharpness", String.valueOf(param1Int));
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid Sharpness ");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public void setContrast(int param1Int) {
      if (param1Int >= 0 && param1Int <= getMaxContrast()) {
        set("contrast", String.valueOf(param1Int));
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid Contrast ");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public void setSaturation(int param1Int) {
      if (param1Int >= 0 && param1Int <= getMaxSaturation()) {
        set("saturation", String.valueOf(param1Int));
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid Saturation ");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public boolean isPowerModeSupported() {
      String str = get("power-mode-supported");
      return "true".equals(str);
    }
    
    public int getSharpness() {
      return getInt("sharpness");
    }
    
    public int getMaxSharpness() {
      return getInt("max-sharpness");
    }
    
    public int getContrast() {
      return getInt("contrast");
    }
    
    public int getMaxContrast() {
      return getInt("max-contrast");
    }
    
    public int getSaturation() {
      return getInt("saturation");
    }
    
    public int getMaxSaturation() {
      return getInt("max-saturation");
    }
    
    public void setGpsLatitudeRef(String param1String) {
      set("gps-latitude-ref", param1String);
    }
    
    public void setGpsLongitudeRef(String param1String) {
      set("gps-longitude-ref", param1String);
    }
    
    public void setExifDateTime(String param1String) {
      set("exif-datetime", param1String);
    }
    
    public String getTouchAfAec() {
      return get("touch-af-aec");
    }
    
    public void setTouchAfAec(String param1String) {
      set("touch-af-aec", param1String);
    }
    
    public String getRedeyeReductionMode() {
      return get("redeye-reduction");
    }
    
    public void setRedeyeReductionMode(String param1String) {
      set("redeye-reduction", param1String);
    }
    
    public String getPreviewFrameRateMode() {
      return get("preview-frame-rate-mode");
    }
    
    public void setPreviewFrameRateMode(String param1String) {
      set("preview-frame-rate-mode", param1String);
    }
    
    public String getSceneDetectMode() {
      return get("scene-detect");
    }
    
    public void setSceneDetectMode(String param1String) {
      set("scene-detect", param1String);
    }
    
    public String getAEBracket() {
      return get("ae-bracket-hdr");
    }
    
    public void setPowerMode(String param1String) {
      set("power-mode", param1String);
    }
    
    public String getPowerMode() {
      return get("power-mode");
    }
    
    public void setAEBracket(String param1String) {
      set("ae-bracket-hdr", param1String);
    }
    
    public String getISOValue() {
      return get("iso");
    }
    
    public void setISOValue(String param1String) {
      set("iso", param1String);
    }
    
    public void setExposureTime(int param1Int) {
      set("exposure-time", Integer.toString(param1Int));
    }
    
    public String getExposureTime() {
      return get("exposure-time");
    }
    
    public String getMinExposureTime() {
      return get("min-exposure-time");
    }
    
    public String getMaxExposureTime() {
      return get("max-exposure-time");
    }
    
    public String getLensShade() {
      return get("lensshade");
    }
    
    public void setLensShade(String param1String) {
      set("lensshade", param1String);
    }
    
    public String getAutoExposure() {
      return get("auto-exposure");
    }
    
    public void setAutoExposure(String param1String) {
      set("auto-exposure", param1String);
    }
    
    public String getMemColorEnhance() {
      return get("mce");
    }
    
    public void setMemColorEnhance(String param1String) {
      set("mce", param1String);
    }
    
    public void setWBManualCCT(int param1Int) {
      set("wb-manual-cct", Integer.toString(param1Int));
    }
    
    public String getWBMinCCT() {
      return get("min-wb-cct");
    }
    
    public String getMaxWBCCT() {
      return get("max-wb-cct");
    }
    
    public String getWBCurrentCCT() {
      return get("wb-manual-cct");
    }
    
    public String getZSLMode() {
      return get("zsl");
    }
    
    public void setZSLMode(String param1String) {
      set("zsl", param1String);
    }
    
    public void setAutoHDRMode(String param1String) {
      set("auto-hdr-enable", param1String);
    }
    
    public String getCameraMode() {
      return get("camera-mode");
    }
    
    public void setCameraMode(int param1Int) {
      set("camera-mode", param1Int);
    }
    
    public void setFocusPosition(int param1Int1, int param1Int2) {
      set("manual-focus-pos-type", Integer.toString(param1Int1));
      set("manual-focus-position", Integer.toString(param1Int2));
    }
    
    public String getCurrentFocusPosition() {
      return get("manual-focus-position");
    }
    
    public String getVideoHighFrameRate() {
      return get("video-hfr");
    }
    
    public void setVideoHighFrameRate(String param1String) {
      set("video-hfr", param1String);
    }
    
    public String getVideoHDRMode() {
      return get("video-hdr");
    }
    
    public void setVideoHDRMode(String param1String) {
      set("video-hdr", param1String);
    }
    
    public String getDenoise() {
      return get("denoise");
    }
    
    public String getContinuousAf() {
      return get("continuous-af");
    }
    
    public void setDenoise(String param1String) {
      set("denoise", param1String);
    }
    
    public void setContinuousAf(String param1String) {
      set("continuous-af", param1String);
    }
    
    public String getSelectableZoneAf() {
      return get("selectable-zone-af");
    }
    
    public void setSelectableZoneAf(String param1String) {
      set("selectable-zone-af", param1String);
    }
    
    public String getFaceDetectionMode() {
      return get("face-detection");
    }
    
    public void setFaceDetectionMode(String param1String) {
      set("face-detection", param1String);
    }
    
    public String getVideoRotation() {
      return get("video-rotation");
    }
    
    public void setVideoRotation(String param1String) {
      set("video-rotation", param1String);
    }
    
    public List<String> getSupportedVideoRotationValues() {
      String str = get("video-rotation-values");
      return split(str);
    }
    
    private ArrayList<Camera.Coordinate> splitCoordinate(String param1String) {
      if (param1String == null)
        return null; 
      TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(',');
      simpleStringSplitter.setString(param1String);
      ArrayList<Camera.Coordinate> arrayList = new ArrayList();
      for (String str : simpleStringSplitter) {
        Camera.Coordinate coordinate = strToCoordinate(str);
        if (coordinate != null)
          arrayList.add(coordinate); 
      } 
      if (arrayList.size() == 0)
        return null; 
      return arrayList;
    }
    
    private Camera.Coordinate strToCoordinate(String param1String) {
      if (param1String == null)
        return null; 
      int i = param1String.indexOf('x');
      if (i != -1) {
        String str = param1String.substring(0, i);
        param1String = param1String.substring(i + 1);
        Camera camera = Camera.this;
        i = Integer.parseInt(str);
        return 
          new Camera.Coordinate(i, Integer.parseInt(param1String));
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid Coordinate parameter string=");
      stringBuilder.append(param1String);
      Log.e("Camera", stringBuilder.toString());
      return null;
    }
    
    private Parameters() {}
  }
  
  @Deprecated
  public static interface AutoFocusCallback {
    void onAutoFocus(boolean param1Boolean, Camera param1Camera);
  }
  
  @Deprecated
  public static interface AutoFocusMoveCallback {
    void onAutoFocusMoving(boolean param1Boolean, Camera param1Camera);
  }
  
  public static interface CameraDataCallback {
    void onCameraData(int[] param1ArrayOfint, Camera param1Camera);
  }
  
  public static interface CameraMetaDataCallback {
    void onCameraMetaData(byte[] param1ArrayOfbyte, Camera param1Camera);
  }
  
  @Deprecated
  public static interface ErrorCallback {
    void onError(int param1Int, Camera param1Camera);
  }
  
  @Deprecated
  public static interface FaceDetectionListener {
    void onFaceDetection(Camera.Face[] param1ArrayOfFace, Camera param1Camera);
  }
  
  @Deprecated
  public static interface OnZoomChangeListener {
    void onZoomChange(int param1Int, boolean param1Boolean, Camera param1Camera);
  }
  
  @Deprecated
  public static interface PictureCallback {
    void onPictureTaken(byte[] param1ArrayOfbyte, Camera param1Camera);
  }
  
  @Deprecated
  public static interface PreviewCallback {
    void onPreviewFrame(byte[] param1ArrayOfbyte, Camera param1Camera);
  }
  
  @Deprecated
  public static interface ShutterCallback {
    void onShutter();
  }
}
