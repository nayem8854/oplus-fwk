package android.hardware.fingerprint;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;

public final class EngineeringInfo implements Parcelable {
  private int[] mKey = null;
  
  private String[] mValue = null;
  
  public EngineeringInfo(int paramInt, String paramString) {
    HashMap<Object, Object> hashMap = new HashMap<>();
    this.mLength = 1;
    int[] arrayOfInt = new int[1];
    String[] arrayOfString = new String[1];
    arrayOfInt[0] = paramInt;
    arrayOfString[0] = paramString;
    hashMap.put(Integer.valueOf(paramInt), paramString);
  }
  
  public EngineeringInfo(int paramInt, ArrayList<Integer> paramArrayList, ArrayList<String> paramArrayList1) {
    this.mEngineeringInfoMap = new HashMap<>();
    this.mLength = paramInt;
    this.mKey = new int[paramInt];
    this.mValue = new String[paramInt];
    for (paramInt = 0; paramInt < this.mLength; paramInt++) {
      this.mKey[paramInt] = ((Integer)paramArrayList.get(paramInt)).intValue();
      this.mValue[paramInt] = paramArrayList1.get(paramInt);
    } 
  }
  
  private EngineeringInfo(Parcel paramParcel) {
    this.mEngineeringInfoMap = new HashMap<>();
    this.mLength = paramParcel.readInt();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("mLength = ");
    stringBuilder.append(this.mLength);
    Log.d("EngineeringInfo", stringBuilder.toString());
    int i = this.mLength;
    if (i > 0) {
      this.mKey = new int[i];
      for (i = 0; i < this.mLength; i++)
        this.mKey[i] = paramParcel.readInt(); 
    } 
    this.mValue = paramParcel.readStringArray();
    for (i = 0; i < this.mLength; i++)
      this.mEngineeringInfoMap.put(Integer.valueOf(this.mKey[i]), this.mValue[i]); 
  }
  
  public EngineeringInfo(HashMap<Integer, String> paramHashMap) {
    this.mEngineeringInfoMap = new HashMap<>();
    this.mEngineeringInfoMap = paramHashMap;
    this.mLength = paramHashMap.size();
    Integer[] arrayOfInteger = (Integer[])this.mEngineeringInfoMap.keySet().toArray();
    String[] arrayOfString = (String[])this.mEngineeringInfoMap.values().toArray();
    for (byte b = 0; b < this.mLength; b++) {
      this.mKey[b] = arrayOfInteger[b].intValue();
      this.mValue[b] = arrayOfString[b];
    } 
  }
  
  class EngineeringParameterGroup {
    public static final int BAD_PIXEL_NUM = 4;
    
    public static final int IMAGE_QUALITY = 1;
    
    public static final int IMAGE_SNR = 3;
    
    public static final int LOCAL_BAD_PIXEL_NUM = 5;
    
    public static final int LOCAL_BIG_PIXEL_NUM = 8;
    
    public static final int M_ALL_TILT_ANGLE = 6;
    
    public static final int M_BLOCK_TILT_ANGLE_MAX = 7;
    
    public static final int SNR_SUCCESSED = 2;
    
    public static final int SUCCESSED = 0;
    
    final EngineeringInfo this$0;
  }
  
  class EngineeringInfoAcquireAction {
    public static final int FINGERPRINT_GET_BAD_PIXELS = 2;
    
    public static final int FINGERPRINT_GET_IMAGET_QUALITY = 1;
    
    public static final int FINGERPRINT_GET_IMAGE_SNR = 0;
    
    public static final int FINGERPRINT_GET_UNLOCK_TIME = 1000;
    
    public static final int FINGERPRINT_SELF_TEST = 3;
    
    final EngineeringInfo this$0;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public int getLength() {
    return this.mLength;
  }
  
  public int[] getKey() {
    for (byte b = 0; b < this.mLength; b++) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("mKey[");
      stringBuilder.append(b);
      stringBuilder.append("] = ");
      stringBuilder.append(this.mKey[b]);
      Log.d("EngineeringInfo", stringBuilder.toString());
    } 
    return this.mKey;
  }
  
  public String[] getValue() {
    for (byte b = 0; b < this.mLength; b++) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("mValue[");
      stringBuilder.append(b);
      stringBuilder.append("] = ");
      stringBuilder.append(this.mValue[b]);
      Log.d("EngineeringInfo", stringBuilder.toString());
    } 
    return this.mValue;
  }
  
  public HashMap<Integer, String> getEngineeringInfoMap() {
    return this.mEngineeringInfoMap;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeIntArray(this.mKey);
    paramParcel.writeStringArray(this.mValue);
  }
  
  public static final Parcelable.Creator<EngineeringInfo> CREATOR = new Parcelable.Creator<EngineeringInfo>() {
      public EngineeringInfo createFromParcel(Parcel param1Parcel) {
        return new EngineeringInfo(param1Parcel);
      }
      
      public EngineeringInfo[] newArray(int param1Int) {
        return new EngineeringInfo[param1Int];
      }
    };
  
  private HashMap<Integer, String> mEngineeringInfoMap;
  
  private int mLength;
}
