package android.hardware.fingerprint;

import android.hardware.biometrics.IBiometricServiceLockoutResetCallback;
import android.hardware.biometrics.IBiometricServiceReceiverInternal;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IFingerprintService extends IInterface {
  void addClientActiveCallback(IFingerprintClientActiveCallback paramIFingerprintClientActiveCallback) throws RemoteException;
  
  void addLockoutResetCallback(IBiometricServiceLockoutResetCallback paramIBiometricServiceLockoutResetCallback) throws RemoteException;
  
  byte[] alipayInvokeCommand(byte[] paramArrayOfbyte) throws RemoteException;
  
  void authenticate(IBinder paramIBinder, long paramLong, int paramInt1, IFingerprintServiceReceiver paramIFingerprintServiceReceiver, int paramInt2, String paramString) throws RemoteException;
  
  void cancelAuthentication(IBinder paramIBinder, String paramString) throws RemoteException;
  
  void cancelAuthenticationFromService(IBinder paramIBinder, String paramString, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) throws RemoteException;
  
  void cancelEnrollment(IBinder paramIBinder, String paramString) throws RemoteException;
  
  void cancelGetEngineeringInfo(IBinder paramIBinder, String paramString, int paramInt) throws RemoteException;
  
  void cancelTouchEventListener(IBinder paramIBinder, String paramString) throws RemoteException;
  
  int continueEnroll() throws RemoteException;
  
  int continueIdentify(IBinder paramIBinder) throws RemoteException;
  
  void enroll(IBinder paramIBinder, byte[] paramArrayOfbyte, int paramInt1, IFingerprintServiceReceiver paramIFingerprintServiceReceiver, int paramInt2, String paramString) throws RemoteException;
  
  void enumerate(IBinder paramIBinder, int paramInt, IFingerprintServiceReceiver paramIFingerprintServiceReceiver, String paramString) throws RemoteException;
  
  void finishUnLockedScreen(boolean paramBoolean, String paramString) throws RemoteException;
  
  int getAlikeyStatus() throws RemoteException;
  
  long getAuthenticatorId(int paramInt) throws RemoteException;
  
  int getCurrentIconStatus() throws RemoteException;
  
  int getEngineeringInfo(IBinder paramIBinder, String paramString, int paramInt1, IFingerprintServiceReceiver paramIFingerprintServiceReceiver, int paramInt2) throws RemoteException;
  
  List<Fingerprint> getEnrolledFingerprints(int paramInt, String paramString) throws RemoteException;
  
  int getEnrollmentTotalTimes(IBinder paramIBinder) throws RemoteException;
  
  int getFailedAttempts(String paramString) throws RemoteException;
  
  byte[] getFingerprintAuthToken(String paramString) throws RemoteException;
  
  long getLockoutAttemptDeadline(String paramString) throws RemoteException;
  
  boolean hasEnrolledFingerprints(int paramInt, String paramString) throws RemoteException;
  
  void hideFingerprintIcon(int paramInt, String paramString) throws RemoteException;
  
  void initConfiguredStrength(int paramInt) throws RemoteException;
  
  boolean isClientActive() throws RemoteException;
  
  boolean isHardwareDetected(String paramString) throws RemoteException;
  
  int pauseEnroll() throws RemoteException;
  
  int pauseIdentify(IBinder paramIBinder) throws RemoteException;
  
  int postEnroll(IBinder paramIBinder) throws RemoteException;
  
  long preEnroll(IBinder paramIBinder) throws RemoteException;
  
  void prepareForAuthentication(IBinder paramIBinder, long paramLong, int paramInt1, IBiometricServiceReceiverInternal paramIBiometricServiceReceiverInternal, String paramString, int paramInt2, int paramInt3, int paramInt4, int paramInt5) throws RemoteException;
  
  int regsiterFingerprintCmdCallback(IFingerprintCommandCallback paramIFingerprintCommandCallback) throws RemoteException;
  
  int regsiterOpticalFingerprintListener(IOpticalFingerprintListener paramIOpticalFingerprintListener) throws RemoteException;
  
  void remove(IBinder paramIBinder, int paramInt1, int paramInt2, int paramInt3, IFingerprintServiceReceiver paramIFingerprintServiceReceiver, String paramString) throws RemoteException;
  
  void removeClientActiveCallback(IFingerprintClientActiveCallback paramIFingerprintClientActiveCallback) throws RemoteException;
  
  void rename(int paramInt1, int paramInt2, String paramString) throws RemoteException;
  
  void resetTimeout(byte[] paramArrayOfbyte) throws RemoteException;
  
  int sendFingerprintCmd(int paramInt, byte[] paramArrayOfbyte) throws RemoteException;
  
  void setActiveUser(int paramInt) throws RemoteException;
  
  void setFingerKeymode(IBinder paramIBinder, int paramInt1, String paramString, int paramInt2) throws RemoteException;
  
  void setFingerprintEnabled(boolean paramBoolean) throws RemoteException;
  
  void setTouchEventListener(IBinder paramIBinder, IFingerprintServiceReceiver paramIFingerprintServiceReceiver, int paramInt, String paramString) throws RemoteException;
  
  void showFingerprintIcon(String paramString) throws RemoteException;
  
  void startPreparedClient(int paramInt) throws RemoteException;
  
  int touchDown() throws RemoteException;
  
  int touchUp() throws RemoteException;
  
  int unregsiterFingerprintCmdCallback(IFingerprintCommandCallback paramIFingerprintCommandCallback) throws RemoteException;
  
  int unregsiterOpticalFingerprintListener() throws RemoteException;
  
  class Default implements IFingerprintService {
    public void authenticate(IBinder param1IBinder, long param1Long, int param1Int1, IFingerprintServiceReceiver param1IFingerprintServiceReceiver, int param1Int2, String param1String) throws RemoteException {}
    
    public void prepareForAuthentication(IBinder param1IBinder, long param1Long, int param1Int1, IBiometricServiceReceiverInternal param1IBiometricServiceReceiverInternal, String param1String, int param1Int2, int param1Int3, int param1Int4, int param1Int5) throws RemoteException {}
    
    public void startPreparedClient(int param1Int) throws RemoteException {}
    
    public void cancelAuthentication(IBinder param1IBinder, String param1String) throws RemoteException {}
    
    public void cancelAuthenticationFromService(IBinder param1IBinder, String param1String, int param1Int1, int param1Int2, int param1Int3, boolean param1Boolean) throws RemoteException {}
    
    public void enroll(IBinder param1IBinder, byte[] param1ArrayOfbyte, int param1Int1, IFingerprintServiceReceiver param1IFingerprintServiceReceiver, int param1Int2, String param1String) throws RemoteException {}
    
    public void cancelEnrollment(IBinder param1IBinder, String param1String) throws RemoteException {}
    
    public void remove(IBinder param1IBinder, int param1Int1, int param1Int2, int param1Int3, IFingerprintServiceReceiver param1IFingerprintServiceReceiver, String param1String) throws RemoteException {}
    
    public void rename(int param1Int1, int param1Int2, String param1String) throws RemoteException {}
    
    public List<Fingerprint> getEnrolledFingerprints(int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public boolean isHardwareDetected(String param1String) throws RemoteException {
      return false;
    }
    
    public long preEnroll(IBinder param1IBinder) throws RemoteException {
      return 0L;
    }
    
    public int postEnroll(IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public boolean hasEnrolledFingerprints(int param1Int, String param1String) throws RemoteException {
      return false;
    }
    
    public long getAuthenticatorId(int param1Int) throws RemoteException {
      return 0L;
    }
    
    public void resetTimeout(byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void addLockoutResetCallback(IBiometricServiceLockoutResetCallback param1IBiometricServiceLockoutResetCallback) throws RemoteException {}
    
    public void setActiveUser(int param1Int) throws RemoteException {}
    
    public int getEngineeringInfo(IBinder param1IBinder, String param1String, int param1Int1, IFingerprintServiceReceiver param1IFingerprintServiceReceiver, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public void cancelGetEngineeringInfo(IBinder param1IBinder, String param1String, int param1Int) throws RemoteException {}
    
    public int getEnrollmentTotalTimes(IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public int pauseEnroll() throws RemoteException {
      return 0;
    }
    
    public int continueEnroll() throws RemoteException {
      return 0;
    }
    
    public void setTouchEventListener(IBinder param1IBinder, IFingerprintServiceReceiver param1IFingerprintServiceReceiver, int param1Int, String param1String) throws RemoteException {}
    
    public void cancelTouchEventListener(IBinder param1IBinder, String param1String) throws RemoteException {}
    
    public void finishUnLockedScreen(boolean param1Boolean, String param1String) throws RemoteException {}
    
    public int getAlikeyStatus() throws RemoteException {
      return 0;
    }
    
    public void setFingerprintEnabled(boolean param1Boolean) throws RemoteException {}
    
    public int pauseIdentify(IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public int continueIdentify(IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public long getLockoutAttemptDeadline(String param1String) throws RemoteException {
      return 0L;
    }
    
    public int getFailedAttempts(String param1String) throws RemoteException {
      return 0;
    }
    
    public void enumerate(IBinder param1IBinder, int param1Int, IFingerprintServiceReceiver param1IFingerprintServiceReceiver, String param1String) throws RemoteException {}
    
    public boolean isClientActive() throws RemoteException {
      return false;
    }
    
    public void addClientActiveCallback(IFingerprintClientActiveCallback param1IFingerprintClientActiveCallback) throws RemoteException {}
    
    public void removeClientActiveCallback(IFingerprintClientActiveCallback param1IFingerprintClientActiveCallback) throws RemoteException {}
    
    public void initConfiguredStrength(int param1Int) throws RemoteException {}
    
    public byte[] alipayInvokeCommand(byte[] param1ArrayOfbyte) throws RemoteException {
      return null;
    }
    
    public int touchDown() throws RemoteException {
      return 0;
    }
    
    public int touchUp() throws RemoteException {
      return 0;
    }
    
    public int sendFingerprintCmd(int param1Int, byte[] param1ArrayOfbyte) throws RemoteException {
      return 0;
    }
    
    public byte[] getFingerprintAuthToken(String param1String) throws RemoteException {
      return null;
    }
    
    public int getCurrentIconStatus() throws RemoteException {
      return 0;
    }
    
    public int regsiterFingerprintCmdCallback(IFingerprintCommandCallback param1IFingerprintCommandCallback) throws RemoteException {
      return 0;
    }
    
    public int unregsiterFingerprintCmdCallback(IFingerprintCommandCallback param1IFingerprintCommandCallback) throws RemoteException {
      return 0;
    }
    
    public int regsiterOpticalFingerprintListener(IOpticalFingerprintListener param1IOpticalFingerprintListener) throws RemoteException {
      return 0;
    }
    
    public int unregsiterOpticalFingerprintListener() throws RemoteException {
      return 0;
    }
    
    public void showFingerprintIcon(String param1String) throws RemoteException {}
    
    public void hideFingerprintIcon(int param1Int, String param1String) throws RemoteException {}
    
    public void setFingerKeymode(IBinder param1IBinder, int param1Int1, String param1String, int param1Int2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IFingerprintService {
    private static final String DESCRIPTOR = "android.hardware.fingerprint.IFingerprintService";
    
    static final int TRANSACTION_addClientActiveCallback = 35;
    
    static final int TRANSACTION_addLockoutResetCallback = 17;
    
    static final int TRANSACTION_alipayInvokeCommand = 38;
    
    static final int TRANSACTION_authenticate = 1;
    
    static final int TRANSACTION_cancelAuthentication = 4;
    
    static final int TRANSACTION_cancelAuthenticationFromService = 5;
    
    static final int TRANSACTION_cancelEnrollment = 7;
    
    static final int TRANSACTION_cancelGetEngineeringInfo = 20;
    
    static final int TRANSACTION_cancelTouchEventListener = 25;
    
    static final int TRANSACTION_continueEnroll = 23;
    
    static final int TRANSACTION_continueIdentify = 30;
    
    static final int TRANSACTION_enroll = 6;
    
    static final int TRANSACTION_enumerate = 33;
    
    static final int TRANSACTION_finishUnLockedScreen = 26;
    
    static final int TRANSACTION_getAlikeyStatus = 27;
    
    static final int TRANSACTION_getAuthenticatorId = 15;
    
    static final int TRANSACTION_getCurrentIconStatus = 43;
    
    static final int TRANSACTION_getEngineeringInfo = 19;
    
    static final int TRANSACTION_getEnrolledFingerprints = 10;
    
    static final int TRANSACTION_getEnrollmentTotalTimes = 21;
    
    static final int TRANSACTION_getFailedAttempts = 32;
    
    static final int TRANSACTION_getFingerprintAuthToken = 42;
    
    static final int TRANSACTION_getLockoutAttemptDeadline = 31;
    
    static final int TRANSACTION_hasEnrolledFingerprints = 14;
    
    static final int TRANSACTION_hideFingerprintIcon = 49;
    
    static final int TRANSACTION_initConfiguredStrength = 37;
    
    static final int TRANSACTION_isClientActive = 34;
    
    static final int TRANSACTION_isHardwareDetected = 11;
    
    static final int TRANSACTION_pauseEnroll = 22;
    
    static final int TRANSACTION_pauseIdentify = 29;
    
    static final int TRANSACTION_postEnroll = 13;
    
    static final int TRANSACTION_preEnroll = 12;
    
    static final int TRANSACTION_prepareForAuthentication = 2;
    
    static final int TRANSACTION_regsiterFingerprintCmdCallback = 44;
    
    static final int TRANSACTION_regsiterOpticalFingerprintListener = 46;
    
    static final int TRANSACTION_remove = 8;
    
    static final int TRANSACTION_removeClientActiveCallback = 36;
    
    static final int TRANSACTION_rename = 9;
    
    static final int TRANSACTION_resetTimeout = 16;
    
    static final int TRANSACTION_sendFingerprintCmd = 41;
    
    static final int TRANSACTION_setActiveUser = 18;
    
    static final int TRANSACTION_setFingerKeymode = 50;
    
    static final int TRANSACTION_setFingerprintEnabled = 28;
    
    static final int TRANSACTION_setTouchEventListener = 24;
    
    static final int TRANSACTION_showFingerprintIcon = 48;
    
    static final int TRANSACTION_startPreparedClient = 3;
    
    static final int TRANSACTION_touchDown = 39;
    
    static final int TRANSACTION_touchUp = 40;
    
    static final int TRANSACTION_unregsiterFingerprintCmdCallback = 45;
    
    static final int TRANSACTION_unregsiterOpticalFingerprintListener = 47;
    
    public Stub() {
      attachInterface(this, "android.hardware.fingerprint.IFingerprintService");
    }
    
    public static IFingerprintService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.fingerprint.IFingerprintService");
      if (iInterface != null && iInterface instanceof IFingerprintService)
        return (IFingerprintService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 50:
          return "setFingerKeymode";
        case 49:
          return "hideFingerprintIcon";
        case 48:
          return "showFingerprintIcon";
        case 47:
          return "unregsiterOpticalFingerprintListener";
        case 46:
          return "regsiterOpticalFingerprintListener";
        case 45:
          return "unregsiterFingerprintCmdCallback";
        case 44:
          return "regsiterFingerprintCmdCallback";
        case 43:
          return "getCurrentIconStatus";
        case 42:
          return "getFingerprintAuthToken";
        case 41:
          return "sendFingerprintCmd";
        case 40:
          return "touchUp";
        case 39:
          return "touchDown";
        case 38:
          return "alipayInvokeCommand";
        case 37:
          return "initConfiguredStrength";
        case 36:
          return "removeClientActiveCallback";
        case 35:
          return "addClientActiveCallback";
        case 34:
          return "isClientActive";
        case 33:
          return "enumerate";
        case 32:
          return "getFailedAttempts";
        case 31:
          return "getLockoutAttemptDeadline";
        case 30:
          return "continueIdentify";
        case 29:
          return "pauseIdentify";
        case 28:
          return "setFingerprintEnabled";
        case 27:
          return "getAlikeyStatus";
        case 26:
          return "finishUnLockedScreen";
        case 25:
          return "cancelTouchEventListener";
        case 24:
          return "setTouchEventListener";
        case 23:
          return "continueEnroll";
        case 22:
          return "pauseEnroll";
        case 21:
          return "getEnrollmentTotalTimes";
        case 20:
          return "cancelGetEngineeringInfo";
        case 19:
          return "getEngineeringInfo";
        case 18:
          return "setActiveUser";
        case 17:
          return "addLockoutResetCallback";
        case 16:
          return "resetTimeout";
        case 15:
          return "getAuthenticatorId";
        case 14:
          return "hasEnrolledFingerprints";
        case 13:
          return "postEnroll";
        case 12:
          return "preEnroll";
        case 11:
          return "isHardwareDetected";
        case 10:
          return "getEnrolledFingerprints";
        case 9:
          return "rename";
        case 8:
          return "remove";
        case 7:
          return "cancelEnrollment";
        case 6:
          return "enroll";
        case 5:
          return "cancelAuthenticationFromService";
        case 4:
          return "cancelAuthentication";
        case 3:
          return "startPreparedClient";
        case 2:
          return "prepareForAuthentication";
        case 1:
          break;
      } 
      return "authenticate";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        String str7;
        IOpticalFingerprintListener iOpticalFingerprintListener;
        IFingerprintCommandCallback iFingerprintCommandCallback;
        String str6;
        byte[] arrayOfByte2;
        IFingerprintClientActiveCallback iFingerprintClientActiveCallback;
        String str5;
        IBinder iBinder3;
        String str4;
        IBinder iBinder2;
        IBiometricServiceLockoutResetCallback iBiometricServiceLockoutResetCallback;
        byte[] arrayOfByte1;
        String str3;
        IBinder iBinder1;
        String str2;
        List<Fingerprint> list;
        IBinder iBinder9;
        IFingerprintServiceReceiver iFingerprintServiceReceiver4;
        IBinder iBinder8;
        IFingerprintServiceReceiver iFingerprintServiceReceiver3;
        IBinder iBinder7;
        String str9;
        IFingerprintServiceReceiver iFingerprintServiceReceiver2;
        IBinder iBinder6;
        IFingerprintServiceReceiver iFingerprintServiceReceiver1;
        String str8;
        IBinder iBinder5;
        IBiometricServiceReceiverInternal iBiometricServiceReceiverInternal;
        String str12;
        IBinder iBinder11;
        String str11;
        IFingerprintServiceReceiver iFingerprintServiceReceiver6;
        IBinder iBinder10;
        String str10;
        IBinder iBinder13;
        byte[] arrayOfByte3;
        IBinder iBinder12;
        int m, n, i1;
        boolean bool4 = false, bool5 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 50:
            param1Parcel1.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iBinder9 = param1Parcel1.readStrongBinder();
            param1Int2 = param1Parcel1.readInt();
            str12 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            setFingerKeymode(iBinder9, param1Int2, str12, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 49:
            param1Parcel1.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            param1Int1 = param1Parcel1.readInt();
            str7 = param1Parcel1.readString();
            hideFingerprintIcon(param1Int1, str7);
            param1Parcel2.writeNoException();
            return true;
          case 48:
            str7.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            str7 = str7.readString();
            showFingerprintIcon(str7);
            param1Parcel2.writeNoException();
            return true;
          case 47:
            str7.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            param1Int1 = unregsiterOpticalFingerprintListener();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 46:
            str7.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iOpticalFingerprintListener = IOpticalFingerprintListener.Stub.asInterface(str7.readStrongBinder());
            param1Int1 = regsiterOpticalFingerprintListener(iOpticalFingerprintListener);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 45:
            iOpticalFingerprintListener.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iFingerprintCommandCallback = IFingerprintCommandCallback.Stub.asInterface(iOpticalFingerprintListener.readStrongBinder());
            param1Int1 = unregsiterFingerprintCmdCallback(iFingerprintCommandCallback);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 44:
            iFingerprintCommandCallback.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iFingerprintCommandCallback = IFingerprintCommandCallback.Stub.asInterface(iFingerprintCommandCallback.readStrongBinder());
            param1Int1 = regsiterFingerprintCmdCallback(iFingerprintCommandCallback);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 43:
            iFingerprintCommandCallback.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            param1Int1 = getCurrentIconStatus();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 42:
            iFingerprintCommandCallback.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            str6 = iFingerprintCommandCallback.readString();
            arrayOfByte2 = getFingerprintAuthToken(str6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte2);
            return true;
          case 41:
            arrayOfByte2.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            param1Int1 = arrayOfByte2.readInt();
            arrayOfByte2 = arrayOfByte2.createByteArray();
            param1Int1 = sendFingerprintCmd(param1Int1, arrayOfByte2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 40:
            arrayOfByte2.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            param1Int1 = touchUp();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 39:
            arrayOfByte2.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            param1Int1 = touchDown();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 38:
            arrayOfByte2.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            arrayOfByte2 = arrayOfByte2.createByteArray();
            arrayOfByte2 = alipayInvokeCommand(arrayOfByte2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte2);
            return true;
          case 37:
            arrayOfByte2.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            param1Int1 = arrayOfByte2.readInt();
            initConfiguredStrength(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 36:
            arrayOfByte2.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iFingerprintClientActiveCallback = IFingerprintClientActiveCallback.Stub.asInterface(arrayOfByte2.readStrongBinder());
            removeClientActiveCallback(iFingerprintClientActiveCallback);
            param1Parcel2.writeNoException();
            return true;
          case 35:
            iFingerprintClientActiveCallback.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iFingerprintClientActiveCallback = IFingerprintClientActiveCallback.Stub.asInterface(iFingerprintClientActiveCallback.readStrongBinder());
            addClientActiveCallback(iFingerprintClientActiveCallback);
            param1Parcel2.writeNoException();
            return true;
          case 34:
            iFingerprintClientActiveCallback.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            bool3 = isClientActive();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 33:
            iFingerprintClientActiveCallback.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iBinder11 = iFingerprintClientActiveCallback.readStrongBinder();
            k = iFingerprintClientActiveCallback.readInt();
            iFingerprintServiceReceiver4 = IFingerprintServiceReceiver.Stub.asInterface(iFingerprintClientActiveCallback.readStrongBinder());
            str5 = iFingerprintClientActiveCallback.readString();
            enumerate(iBinder11, k, iFingerprintServiceReceiver4, str5);
            param1Parcel2.writeNoException();
            return true;
          case 32:
            str5.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            str5 = str5.readString();
            k = getFailedAttempts(str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 31:
            str5.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            str5 = str5.readString();
            l = getLockoutAttemptDeadline(str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 30:
            str5.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iBinder3 = str5.readStrongBinder();
            k = continueIdentify(iBinder3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 29:
            iBinder3.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iBinder3 = iBinder3.readStrongBinder();
            k = pauseIdentify(iBinder3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 28:
            iBinder3.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            bool4 = bool5;
            if (iBinder3.readInt() != 0)
              bool4 = true; 
            setFingerprintEnabled(bool4);
            param1Parcel2.writeNoException();
            return true;
          case 27:
            iBinder3.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            k = getAlikeyStatus();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 26:
            iBinder3.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            if (iBinder3.readInt() != 0)
              bool4 = true; 
            str4 = iBinder3.readString();
            finishUnLockedScreen(bool4, str4);
            return true;
          case 25:
            str4.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iBinder8 = str4.readStrongBinder();
            str4 = str4.readString();
            cancelTouchEventListener(iBinder8, str4);
            param1Parcel2.writeNoException();
            return true;
          case 24:
            str4.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iBinder11 = str4.readStrongBinder();
            iFingerprintServiceReceiver3 = IFingerprintServiceReceiver.Stub.asInterface(str4.readStrongBinder());
            k = str4.readInt();
            str4 = str4.readString();
            setTouchEventListener(iBinder11, iFingerprintServiceReceiver3, k, str4);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            str4.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            k = continueEnroll();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 22:
            str4.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            k = pauseEnroll();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 21:
            str4.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iBinder2 = str4.readStrongBinder();
            k = getEnrollmentTotalTimes(iBinder2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 20:
            iBinder2.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iBinder7 = iBinder2.readStrongBinder();
            str11 = iBinder2.readString();
            k = iBinder2.readInt();
            cancelGetEngineeringInfo(iBinder7, str11, k);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            iBinder2.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iBinder13 = iBinder2.readStrongBinder();
            str9 = iBinder2.readString();
            param1Int2 = iBinder2.readInt();
            iFingerprintServiceReceiver6 = IFingerprintServiceReceiver.Stub.asInterface(iBinder2.readStrongBinder());
            k = iBinder2.readInt();
            k = getEngineeringInfo(iBinder13, str9, param1Int2, iFingerprintServiceReceiver6, k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 18:
            iBinder2.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            k = iBinder2.readInt();
            setActiveUser(k);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            iBinder2.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iBiometricServiceLockoutResetCallback = IBiometricServiceLockoutResetCallback.Stub.asInterface(iBinder2.readStrongBinder());
            addLockoutResetCallback(iBiometricServiceLockoutResetCallback);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            iBiometricServiceLockoutResetCallback.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            arrayOfByte1 = iBiometricServiceLockoutResetCallback.createByteArray();
            resetTimeout(arrayOfByte1);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            arrayOfByte1.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            k = arrayOfByte1.readInt();
            l = getAuthenticatorId(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 14:
            arrayOfByte1.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            k = arrayOfByte1.readInt();
            str3 = arrayOfByte1.readString();
            bool2 = hasEnrolledFingerprints(k, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 13:
            str3.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iBinder1 = str3.readStrongBinder();
            j = postEnroll(iBinder1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 12:
            iBinder1.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iBinder1 = iBinder1.readStrongBinder();
            l = preEnroll(iBinder1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 11:
            iBinder1.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            str2 = iBinder1.readString();
            bool1 = isHardwareDetected(str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 10:
            str2.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            i = str2.readInt();
            str2 = str2.readString();
            list = getEnrolledFingerprints(i, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 9:
            list.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            i = list.readInt();
            param1Int2 = list.readInt();
            str1 = list.readString();
            rename(i, param1Int2, str1);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            str1.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iBinder10 = str1.readStrongBinder();
            i = str1.readInt();
            param1Int2 = str1.readInt();
            m = str1.readInt();
            iFingerprintServiceReceiver2 = IFingerprintServiceReceiver.Stub.asInterface(str1.readStrongBinder());
            str1 = str1.readString();
            remove(iBinder10, i, param1Int2, m, iFingerprintServiceReceiver2, str1);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            str1.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iBinder6 = str1.readStrongBinder();
            str1 = str1.readString();
            cancelEnrollment(iBinder6, str1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            str1.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iBinder10 = str1.readStrongBinder();
            arrayOfByte3 = str1.createByteArray();
            i = str1.readInt();
            iFingerprintServiceReceiver1 = IFingerprintServiceReceiver.Stub.asInterface(str1.readStrongBinder());
            param1Int2 = str1.readInt();
            str1 = str1.readString();
            enroll(iBinder10, arrayOfByte3, i, iFingerprintServiceReceiver1, param1Int2, str1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            str1.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iBinder10 = str1.readStrongBinder();
            str8 = str1.readString();
            i = str1.readInt();
            param1Int2 = str1.readInt();
            m = str1.readInt();
            if (str1.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            cancelAuthenticationFromService(iBinder10, str8, i, param1Int2, m, bool4);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str1.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iBinder5 = str1.readStrongBinder();
            str1 = str1.readString();
            cancelAuthentication(iBinder5, str1);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            str1.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            i = str1.readInt();
            startPreparedClient(i);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str1.enforceInterface("android.hardware.fingerprint.IFingerprintService");
            iBinder12 = str1.readStrongBinder();
            l = str1.readLong();
            n = str1.readInt();
            iBiometricServiceReceiverInternal = IBiometricServiceReceiverInternal.Stub.asInterface(str1.readStrongBinder());
            str10 = str1.readString();
            param1Int2 = str1.readInt();
            i = str1.readInt();
            i1 = str1.readInt();
            m = str1.readInt();
            prepareForAuthentication(iBinder12, l, n, iBiometricServiceReceiverInternal, str10, param1Int2, i, i1, m);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.hardware.fingerprint.IFingerprintService");
        IBinder iBinder4 = str1.readStrongBinder();
        long l = str1.readLong();
        param1Int2 = str1.readInt();
        IFingerprintServiceReceiver iFingerprintServiceReceiver5 = IFingerprintServiceReceiver.Stub.asInterface(str1.readStrongBinder());
        int i = str1.readInt();
        String str1 = str1.readString();
        authenticate(iBinder4, l, param1Int2, iFingerprintServiceReceiver5, i, str1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.hardware.fingerprint.IFingerprintService");
      return true;
    }
    
    private static class Proxy implements IFingerprintService {
      public static IFingerprintService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.fingerprint.IFingerprintService";
      }
      
      public void authenticate(IBinder param2IBinder, long param2Long, int param2Int1, IFingerprintServiceReceiver param2IFingerprintServiceReceiver, int param2Int2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          try {
            parcel1.writeStrongBinder(param2IBinder);
            try {
              parcel1.writeLong(param2Long);
              try {
                IBinder iBinder;
                parcel1.writeInt(param2Int1);
                if (param2IFingerprintServiceReceiver != null) {
                  iBinder = param2IFingerprintServiceReceiver.asBinder();
                } else {
                  iBinder = null;
                } 
                parcel1.writeStrongBinder(iBinder);
                try {
                  parcel1.writeInt(param2Int2);
                  parcel1.writeString(param2String);
                  boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
                  if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
                    IFingerprintService.Stub.getDefaultImpl().authenticate(param2IBinder, param2Long, param2Int1, param2IFingerprintServiceReceiver, param2Int2, param2String);
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } 
                  parcel2.readException();
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IBinder;
      }
      
      public void prepareForAuthentication(IBinder param2IBinder, long param2Long, int param2Int1, IBiometricServiceReceiverInternal param2IBiometricServiceReceiverInternal, String param2String, int param2Int2, int param2Int3, int param2Int4, int param2Int5) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          try {
            parcel1.writeStrongBinder(param2IBinder);
            parcel1.writeLong(param2Long);
            try {
              IBinder iBinder;
              parcel1.writeInt(param2Int1);
              if (param2IBiometricServiceReceiverInternal != null) {
                iBinder = param2IBiometricServiceReceiverInternal.asBinder();
              } else {
                iBinder = null;
              } 
              parcel1.writeStrongBinder(iBinder);
              parcel1.writeString(param2String);
              parcel1.writeInt(param2Int2);
              parcel1.writeInt(param2Int3);
              parcel1.writeInt(param2Int4);
              parcel1.writeInt(param2Int5);
              boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
              if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
                IFingerprintService.Stub.getDefaultImpl().prepareForAuthentication(param2IBinder, param2Long, param2Int1, param2IBiometricServiceReceiverInternal, param2String, param2Int2, param2Int3, param2Int4, param2Int5);
                parcel2.recycle();
                parcel1.recycle();
                return;
              } 
              parcel2.readException();
              parcel2.recycle();
              parcel1.recycle();
              return;
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IBinder;
      }
      
      public void startPreparedClient(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
            IFingerprintService.Stub.getDefaultImpl().startPreparedClient(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelAuthentication(IBinder param2IBinder, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
            IFingerprintService.Stub.getDefaultImpl().cancelAuthentication(param2IBinder, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelAuthenticationFromService(IBinder param2IBinder, String param2String, int param2Int1, int param2Int2, int param2Int3, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          try {
            parcel1.writeStrongBinder(param2IBinder);
            try {
              parcel1.writeString(param2String);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  parcel1.writeInt(param2Int2);
                  try {
                    boolean bool;
                    parcel1.writeInt(param2Int3);
                    if (param2Boolean) {
                      bool = true;
                    } else {
                      bool = false;
                    } 
                    parcel1.writeInt(bool);
                    try {
                      boolean bool1 = this.mRemote.transact(5, parcel1, parcel2, 0);
                      if (!bool1 && IFingerprintService.Stub.getDefaultImpl() != null) {
                        IFingerprintService.Stub.getDefaultImpl().cancelAuthenticationFromService(param2IBinder, param2String, param2Int1, param2Int2, param2Int3, param2Boolean);
                        parcel2.recycle();
                        parcel1.recycle();
                        return;
                      } 
                      parcel2.readException();
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IBinder;
      }
      
      public void enroll(IBinder param2IBinder, byte[] param2ArrayOfbyte, int param2Int1, IFingerprintServiceReceiver param2IFingerprintServiceReceiver, int param2Int2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          try {
            parcel1.writeStrongBinder(param2IBinder);
            try {
              parcel1.writeByteArray(param2ArrayOfbyte);
              try {
                IBinder iBinder;
                parcel1.writeInt(param2Int1);
                if (param2IFingerprintServiceReceiver != null) {
                  iBinder = param2IFingerprintServiceReceiver.asBinder();
                } else {
                  iBinder = null;
                } 
                parcel1.writeStrongBinder(iBinder);
                try {
                  parcel1.writeInt(param2Int2);
                  try {
                    parcel1.writeString(param2String);
                    try {
                      boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
                      if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
                        IFingerprintService.Stub.getDefaultImpl().enroll(param2IBinder, param2ArrayOfbyte, param2Int1, param2IFingerprintServiceReceiver, param2Int2, param2String);
                        parcel2.recycle();
                        parcel1.recycle();
                        return;
                      } 
                      parcel2.readException();
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IBinder;
      }
      
      public void cancelEnrollment(IBinder param2IBinder, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
            IFingerprintService.Stub.getDefaultImpl().cancelEnrollment(param2IBinder, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void remove(IBinder param2IBinder, int param2Int1, int param2Int2, int param2Int3, IFingerprintServiceReceiver param2IFingerprintServiceReceiver, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          try {
            parcel1.writeStrongBinder(param2IBinder);
            try {
              parcel1.writeInt(param2Int1);
              try {
                parcel1.writeInt(param2Int2);
                try {
                  IBinder iBinder;
                  parcel1.writeInt(param2Int3);
                  if (param2IFingerprintServiceReceiver != null) {
                    iBinder = param2IFingerprintServiceReceiver.asBinder();
                  } else {
                    iBinder = null;
                  } 
                  parcel1.writeStrongBinder(iBinder);
                  try {
                    parcel1.writeString(param2String);
                    try {
                      boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
                      if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
                        IFingerprintService.Stub.getDefaultImpl().remove(param2IBinder, param2Int1, param2Int2, param2Int3, param2IFingerprintServiceReceiver, param2String);
                        parcel2.recycle();
                        parcel1.recycle();
                        return;
                      } 
                      parcel2.readException();
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IBinder;
      }
      
      public void rename(int param2Int1, int param2Int2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
            IFingerprintService.Stub.getDefaultImpl().rename(param2Int1, param2Int2, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<Fingerprint> getEnrolledFingerprints(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null)
            return IFingerprintService.Stub.getDefaultImpl().getEnrolledFingerprints(param2Int, param2String); 
          parcel2.readException();
          return parcel2.createTypedArrayList(Fingerprint.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isHardwareDetected(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(11, parcel1, parcel2, 0);
          if (!bool2 && IFingerprintService.Stub.getDefaultImpl() != null) {
            bool1 = IFingerprintService.Stub.getDefaultImpl().isHardwareDetected(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long preEnroll(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null)
            return IFingerprintService.Stub.getDefaultImpl().preEnroll(param2IBinder); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int postEnroll(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null)
            return IFingerprintService.Stub.getDefaultImpl().postEnroll(param2IBinder); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasEnrolledFingerprints(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(14, parcel1, parcel2, 0);
          if (!bool2 && IFingerprintService.Stub.getDefaultImpl() != null) {
            bool1 = IFingerprintService.Stub.getDefaultImpl().hasEnrolledFingerprints(param2Int, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getAuthenticatorId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null)
            return IFingerprintService.Stub.getDefaultImpl().getAuthenticatorId(param2Int); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resetTimeout(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
            IFingerprintService.Stub.getDefaultImpl().resetTimeout(param2ArrayOfbyte);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addLockoutResetCallback(IBiometricServiceLockoutResetCallback param2IBiometricServiceLockoutResetCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          if (param2IBiometricServiceLockoutResetCallback != null) {
            iBinder = param2IBiometricServiceLockoutResetCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
            IFingerprintService.Stub.getDefaultImpl().addLockoutResetCallback(param2IBiometricServiceLockoutResetCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setActiveUser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
            IFingerprintService.Stub.getDefaultImpl().setActiveUser(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getEngineeringInfo(IBinder param2IBinder, String param2String, int param2Int1, IFingerprintServiceReceiver param2IFingerprintServiceReceiver, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          if (param2IFingerprintServiceReceiver != null) {
            iBinder = param2IFingerprintServiceReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
            param2Int1 = IFingerprintService.Stub.getDefaultImpl().getEngineeringInfo(param2IBinder, param2String, param2Int1, param2IFingerprintServiceReceiver, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelGetEngineeringInfo(IBinder param2IBinder, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
            IFingerprintService.Stub.getDefaultImpl().cancelGetEngineeringInfo(param2IBinder, param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getEnrollmentTotalTimes(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null)
            return IFingerprintService.Stub.getDefaultImpl().getEnrollmentTotalTimes(param2IBinder); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int pauseEnroll() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null)
            return IFingerprintService.Stub.getDefaultImpl().pauseEnroll(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int continueEnroll() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null)
            return IFingerprintService.Stub.getDefaultImpl().continueEnroll(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTouchEventListener(IBinder param2IBinder, IFingerprintServiceReceiver param2IFingerprintServiceReceiver, int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2IFingerprintServiceReceiver != null) {
            iBinder = param2IFingerprintServiceReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
            IFingerprintService.Stub.getDefaultImpl().setTouchEventListener(param2IBinder, param2IFingerprintServiceReceiver, param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelTouchEventListener(IBinder param2IBinder, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
            IFingerprintService.Stub.getDefaultImpl().cancelTouchEventListener(param2IBinder, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void finishUnLockedScreen(boolean param2Boolean, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeString(param2String);
          boolean bool1 = this.mRemote.transact(26, parcel, null, 1);
          if (!bool1 && IFingerprintService.Stub.getDefaultImpl() != null) {
            IFingerprintService.Stub.getDefaultImpl().finishUnLockedScreen(param2Boolean, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public int getAlikeyStatus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null)
            return IFingerprintService.Stub.getDefaultImpl().getAlikeyStatus(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setFingerprintEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool1 && IFingerprintService.Stub.getDefaultImpl() != null) {
            IFingerprintService.Stub.getDefaultImpl().setFingerprintEnabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int pauseIdentify(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null)
            return IFingerprintService.Stub.getDefaultImpl().pauseIdentify(param2IBinder); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int continueIdentify(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null)
            return IFingerprintService.Stub.getDefaultImpl().continueIdentify(param2IBinder); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getLockoutAttemptDeadline(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null)
            return IFingerprintService.Stub.getDefaultImpl().getLockoutAttemptDeadline(param2String); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getFailedAttempts(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null)
            return IFingerprintService.Stub.getDefaultImpl().getFailedAttempts(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enumerate(IBinder param2IBinder, int param2Int, IFingerprintServiceReceiver param2IFingerprintServiceReceiver, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          if (param2IFingerprintServiceReceiver != null) {
            iBinder = param2IFingerprintServiceReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
            IFingerprintService.Stub.getDefaultImpl().enumerate(param2IBinder, param2Int, param2IFingerprintServiceReceiver, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isClientActive() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(34, parcel1, parcel2, 0);
          if (!bool2 && IFingerprintService.Stub.getDefaultImpl() != null) {
            bool1 = IFingerprintService.Stub.getDefaultImpl().isClientActive();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addClientActiveCallback(IFingerprintClientActiveCallback param2IFingerprintClientActiveCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          if (param2IFingerprintClientActiveCallback != null) {
            iBinder = param2IFingerprintClientActiveCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
            IFingerprintService.Stub.getDefaultImpl().addClientActiveCallback(param2IFingerprintClientActiveCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeClientActiveCallback(IFingerprintClientActiveCallback param2IFingerprintClientActiveCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          if (param2IFingerprintClientActiveCallback != null) {
            iBinder = param2IFingerprintClientActiveCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
            IFingerprintService.Stub.getDefaultImpl().removeClientActiveCallback(param2IFingerprintClientActiveCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void initConfiguredStrength(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
            IFingerprintService.Stub.getDefaultImpl().initConfiguredStrength(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] alipayInvokeCommand(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
            param2ArrayOfbyte = IFingerprintService.Stub.getDefaultImpl().alipayInvokeCommand(param2ArrayOfbyte);
            return param2ArrayOfbyte;
          } 
          parcel2.readException();
          param2ArrayOfbyte = parcel2.createByteArray();
          return param2ArrayOfbyte;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int touchDown() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null)
            return IFingerprintService.Stub.getDefaultImpl().touchDown(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int touchUp() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null)
            return IFingerprintService.Stub.getDefaultImpl().touchUp(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int sendFingerprintCmd(int param2Int, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeInt(param2Int);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
            param2Int = IFingerprintService.Stub.getDefaultImpl().sendFingerprintCmd(param2Int, param2ArrayOfbyte);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getFingerprintAuthToken(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null)
            return IFingerprintService.Stub.getDefaultImpl().getFingerprintAuthToken(param2String); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCurrentIconStatus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          boolean bool = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null)
            return IFingerprintService.Stub.getDefaultImpl().getCurrentIconStatus(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int regsiterFingerprintCmdCallback(IFingerprintCommandCallback param2IFingerprintCommandCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          if (param2IFingerprintCommandCallback != null) {
            iBinder = param2IFingerprintCommandCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null)
            return IFingerprintService.Stub.getDefaultImpl().regsiterFingerprintCmdCallback(param2IFingerprintCommandCallback); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int unregsiterFingerprintCmdCallback(IFingerprintCommandCallback param2IFingerprintCommandCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          if (param2IFingerprintCommandCallback != null) {
            iBinder = param2IFingerprintCommandCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null)
            return IFingerprintService.Stub.getDefaultImpl().unregsiterFingerprintCmdCallback(param2IFingerprintCommandCallback); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int regsiterOpticalFingerprintListener(IOpticalFingerprintListener param2IOpticalFingerprintListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          if (param2IOpticalFingerprintListener != null) {
            iBinder = param2IOpticalFingerprintListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null)
            return IFingerprintService.Stub.getDefaultImpl().regsiterOpticalFingerprintListener(param2IOpticalFingerprintListener); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int unregsiterOpticalFingerprintListener() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null)
            return IFingerprintService.Stub.getDefaultImpl().unregsiterOpticalFingerprintListener(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void showFingerprintIcon(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(48, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
            IFingerprintService.Stub.getDefaultImpl().showFingerprintIcon(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void hideFingerprintIcon(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(49, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
            IFingerprintService.Stub.getDefaultImpl().hideFingerprintIcon(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setFingerKeymode(IBinder param2IBinder, int param2Int1, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(50, parcel1, parcel2, 0);
          if (!bool && IFingerprintService.Stub.getDefaultImpl() != null) {
            IFingerprintService.Stub.getDefaultImpl().setFingerKeymode(param2IBinder, param2Int1, param2String, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IFingerprintService param1IFingerprintService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IFingerprintService != null) {
          Proxy.sDefaultImpl = param1IFingerprintService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IFingerprintService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
