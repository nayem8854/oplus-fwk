package android.hardware.fingerprint;

public interface IOplusFingerprintManagerEx {
  public static abstract class OpticalFingerprintListener {
    public void onOpticalFingerprintUpdate(int param1Int) {}
  }
  
  default int regsiterOpticalFingerprintListener(OpticalFingerprintListener paramOpticalFingerprintListener) {
    return -1;
  }
  
  default void showFingerprintIcon() {}
  
  default long getLockoutAttemptDeadline() {
    return -1L;
  }
  
  default void hideFingerprintIcon() {}
  
  default int getFailedAttempts() {
    return -1;
  }
}
