package android.hardware.fingerprint;

import android.os.IBinder;
import android.os.ServiceManager;

public class OplusBaseFingerprintManager {
  public static final String FINGERPRINT_DESCRIPTOR = "android.hardware.fingerprint.IFingerprintService";
  
  public static final int GET_FINGERPRINT_LIST_WITH_USERID = 10003;
  
  private static final int OPLUS_CALL_TRANSACTION_INDEX = 10000;
  
  private static final int OPLUS_FIRST_CALL_TRANSACTION = 10001;
  
  public static final int SET_FINGERPRINT_FLAGS = 10002;
  
  protected IBinder mRemote;
  
  public OplusBaseFingerprintManager() {
    ensureRemoteFingerprintService();
  }
  
  protected void ensureRemoteFingerprintService() {
    if (this.mRemote == null)
      this.mRemote = ServiceManager.getService("fingerprint"); 
  }
}
