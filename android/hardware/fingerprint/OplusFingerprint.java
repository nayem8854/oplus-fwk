package android.hardware.fingerprint;

import android.os.Parcel;
import android.os.Parcelable;

public final class OplusFingerprint implements Parcelable {
  private CharSequence mName;
  
  private int mGroupId;
  
  private int mFingerFlags = 0;
  
  private long mDeviceId;
  
  private int mBiometricId;
  
  public OplusFingerprint(CharSequence paramCharSequence, int paramInt1, int paramInt2, long paramLong, int paramInt3) {
    this.mName = paramCharSequence;
    this.mBiometricId = paramInt2;
    this.mDeviceId = paramLong;
    this.mGroupId = paramInt1;
    this.mFingerFlags = paramInt3;
  }
  
  private OplusFingerprint(Parcel paramParcel) {
    this.mName = paramParcel.readString();
    this.mBiometricId = paramParcel.readInt();
    this.mDeviceId = paramParcel.readLong();
    this.mGroupId = paramParcel.readInt();
    this.mFingerFlags = paramParcel.readInt();
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{flag=");
    stringBuilder.append(Integer.toHexString(this.mFingerFlags));
    stringBuilder.append(",mGroupId=");
    stringBuilder.append(this.mGroupId);
    stringBuilder.append(",fingerId=");
    stringBuilder.append(this.mBiometricId);
    stringBuilder.append(",deviceId=");
    stringBuilder.append(this.mDeviceId);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int getGroupId() {
    return this.mGroupId;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(getName().toString());
    paramParcel.writeInt(getBiometricId());
    paramParcel.writeLong(getDeviceId());
    paramParcel.writeInt(this.mGroupId);
    paramParcel.writeInt(this.mFingerFlags);
  }
  
  public static final Parcelable.Creator<OplusFingerprint> CREATOR = new Parcelable.Creator<OplusFingerprint>() {
      public OplusFingerprint createFromParcel(Parcel param1Parcel) {
        return new OplusFingerprint(param1Parcel);
      }
      
      public OplusFingerprint[] newArray(int param1Int) {
        return new OplusFingerprint[param1Int];
      }
    };
  
  public int getFingerFlags() {
    return this.mFingerFlags;
  }
  
  public CharSequence getName() {
    return this.mName;
  }
  
  public int getBiometricId() {
    return this.mBiometricId;
  }
  
  public long getDeviceId() {
    return this.mDeviceId;
  }
}
