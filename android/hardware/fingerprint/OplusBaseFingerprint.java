package android.hardware.fingerprint;

import android.hardware.biometrics.BiometricAuthenticator;
import android.os.Parcel;

public abstract class OplusBaseFingerprint extends BiometricAuthenticator.Identifier {
  protected int mFingerFlags = 0;
  
  public OplusBaseFingerprint(CharSequence paramCharSequence, int paramInt, long paramLong) {
    super(paramCharSequence, paramInt, paramLong);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{flag=");
    stringBuilder.append(Integer.toHexString(this.mFingerFlags));
    stringBuilder.append(",fingerId=");
    stringBuilder.append(getBiometricId());
    stringBuilder.append(",deviceId=");
    stringBuilder.append(getDeviceId());
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mFingerFlags = paramParcel.readInt();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mFingerFlags);
  }
  
  public void setFingerFlags(int paramInt) {
    this.mFingerFlags = paramInt;
  }
  
  public int getFingerFlags() {
    return this.mFingerFlags;
  }
}
