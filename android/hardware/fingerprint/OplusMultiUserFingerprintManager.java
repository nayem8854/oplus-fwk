package android.hardware.fingerprint;

import android.os.Parcel;
import android.os.RemoteException;
import android.util.Singleton;
import java.util.ArrayList;
import java.util.List;

public class OplusMultiUserFingerprintManager extends OplusBaseFingerprintManager {
  public static final int FLAG_FINGERPRINT_SECOND_SYSTEM = 268435456;
  
  private static final Singleton<OplusMultiUserFingerprintManager> sOplusMultiUserSingleton = new Singleton<OplusMultiUserFingerprintManager>() {
      protected OplusMultiUserFingerprintManager create() {
        return new OplusMultiUserFingerprintManager();
      }
    };
  
  public static OplusMultiUserFingerprintManager getInstance() {
    return (OplusMultiUserFingerprintManager)sOplusMultiUserSingleton.get();
  }
  
  public List<OplusFingerprint> getEnrolledFingerprints(int paramInt) throws RemoteException {
    ensureRemoteFingerprintService();
    new ArrayList();
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10003, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.createTypedArrayList(OplusFingerprint.CREATOR);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int setFingerprintFlags(int paramInt1, int paramInt2, int paramInt3, int paramInt4) throws RemoteException {
    ensureRemoteFingerprintService();
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintService");
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      parcel1.writeInt(paramInt3);
      parcel1.writeInt(paramInt4);
      this.mRemote.transact(10002, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt1 = parcel2.readInt();
      return paramInt1;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
}
