package android.hardware.fingerprint;

import android.content.Context;

public class OplusFingerprintManagerEx {
  private static final String TAG = "OplusFingerprintManagerEx";
  
  private Context mContext;
  
  private FingerprintManager mFingerprintManager = null;
  
  public OplusFingerprintManagerEx(Context paramContext) {
    this.mContext = paramContext;
    this.mFingerprintManager = (FingerprintManager)paramContext.getSystemService("fingerprint");
  }
  
  public static class FingerprintImageInfoBase {
    public int mQuality;
    
    public int mScore;
    
    public int mType;
    
    public FingerprintImageInfoBase(int param1Int1, int param1Int2, int param1Int3) {
      this.mType = param1Int1;
      this.mQuality = param1Int2;
      this.mScore = param1Int3;
    }
  }
  
  class FingerprintInputCallbackEx extends FingerprintManager.FingerprintInputCallback {}
  
  class MonitorEventCallbackEx extends FingerprintManager.MonitorEventCallback {}
  
  public long getLockoutAttemptDeadline() {
    FingerprintManager fingerprintManager = this.mFingerprintManager;
    if (fingerprintManager != null)
      return fingerprintManager.getLockoutAttemptDeadline(); 
    return -1L;
  }
  
  public int getFailedAttempts() {
    FingerprintManager fingerprintManager = this.mFingerprintManager;
    if (fingerprintManager != null)
      return fingerprintManager.getFailedAttempts(); 
    return -1;
  }
  
  public int getCurrentIconStatus() {
    FingerprintManager fingerprintManager = this.mFingerprintManager;
    if (fingerprintManager != null)
      return fingerprintManager.getCurrentIconStatus(); 
    return -1;
  }
  
  public byte[] alipayInvokeCommand(byte[] paramArrayOfbyte) {
    FingerprintManager fingerprintManager = this.mFingerprintManager;
    if (fingerprintManager != null)
      return fingerprintManager.alipayInvokeCommand(paramArrayOfbyte); 
    return null;
  }
  
  public byte[] getFingerprintAuthToken() {
    FingerprintManager fingerprintManager = this.mFingerprintManager;
    if (fingerprintManager != null)
      return fingerprintManager.getFingerprintAuthToken(); 
    return null;
  }
  
  public int touchDown() {
    FingerprintManager fingerprintManager = this.mFingerprintManager;
    if (fingerprintManager != null)
      return fingerprintManager.touchDown(); 
    return -1;
  }
  
  public int touchUp() {
    FingerprintManager fingerprintManager = this.mFingerprintManager;
    if (fingerprintManager != null)
      return fingerprintManager.touchUp(); 
    return -1;
  }
  
  public int sendFingerprintCmd(int paramInt, byte[] paramArrayOfbyte) {
    FingerprintManager fingerprintManager = this.mFingerprintManager;
    if (fingerprintManager != null)
      return fingerprintManager.sendFingerprintCmd(paramInt, paramArrayOfbyte); 
    return -1;
  }
  
  public boolean pauseEnroll() {
    FingerprintManager fingerprintManager = this.mFingerprintManager;
    if (fingerprintManager != null)
      return fingerprintManager.pauseEnroll(); 
    return true;
  }
  
  public boolean continueEnroll() {
    FingerprintManager fingerprintManager = this.mFingerprintManager;
    if (fingerprintManager != null)
      return fingerprintManager.continueEnroll(); 
    return true;
  }
  
  public int getEnrollmentTotalTimes() {
    FingerprintManager fingerprintManager = this.mFingerprintManager;
    if (fingerprintManager != null)
      return fingerprintManager.getEnrollmentTotalTimes(); 
    return 0;
  }
  
  public void pauseIdentify() {
    FingerprintManager fingerprintManager = this.mFingerprintManager;
    if (fingerprintManager != null)
      fingerprintManager.pauseIdentify(); 
  }
  
  public void continueIdentify() {
    FingerprintManager fingerprintManager = this.mFingerprintManager;
    if (fingerprintManager != null)
      fingerprintManager.continueIdentify(); 
  }
  
  public int getAlikeyStatus() {
    FingerprintManager fingerprintManager = this.mFingerprintManager;
    if (fingerprintManager != null)
      fingerprintManager.getAlikeyStatus(); 
    return -1;
  }
}
