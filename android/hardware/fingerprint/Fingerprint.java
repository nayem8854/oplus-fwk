package android.hardware.fingerprint;

import android.os.Parcel;
import android.os.Parcelable;

public final class Fingerprint extends OplusBaseFingerprint {
  public Fingerprint(CharSequence paramCharSequence, int paramInt1, int paramInt2, long paramLong) {
    super(paramCharSequence, paramInt2, paramLong);
    this.mGroupId = paramInt1;
  }
  
  private Fingerprint(Parcel paramParcel) {
    super(paramParcel.readString(), paramParcel.readInt(), paramParcel.readLong());
    this.mGroupId = paramParcel.readInt();
    readFromParcel(paramParcel);
  }
  
  public int getGroupId() {
    return this.mGroupId;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(getName().toString());
    paramParcel.writeInt(getBiometricId());
    paramParcel.writeLong(getDeviceId());
    paramParcel.writeInt(this.mGroupId);
    super.writeToParcel(paramParcel, paramInt);
  }
  
  public static final Parcelable.Creator<Fingerprint> CREATOR = (Parcelable.Creator<Fingerprint>)new Object();
  
  private int mGroupId;
}
