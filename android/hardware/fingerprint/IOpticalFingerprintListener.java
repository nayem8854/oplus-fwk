package android.hardware.fingerprint;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOpticalFingerprintListener extends IInterface {
  void onOpticalFingerprintUpdate(int paramInt) throws RemoteException;
  
  class Default implements IOpticalFingerprintListener {
    public void onOpticalFingerprintUpdate(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOpticalFingerprintListener {
    private static final String DESCRIPTOR = "android.hardware.fingerprint.IOpticalFingerprintListener";
    
    static final int TRANSACTION_onOpticalFingerprintUpdate = 1;
    
    public Stub() {
      attachInterface(this, "android.hardware.fingerprint.IOpticalFingerprintListener");
    }
    
    public static IOpticalFingerprintListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.fingerprint.IOpticalFingerprintListener");
      if (iInterface != null && iInterface instanceof IOpticalFingerprintListener)
        return (IOpticalFingerprintListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onOpticalFingerprintUpdate";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.hardware.fingerprint.IOpticalFingerprintListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.hardware.fingerprint.IOpticalFingerprintListener");
      param1Int1 = param1Parcel1.readInt();
      onOpticalFingerprintUpdate(param1Int1);
      return true;
    }
    
    private static class Proxy implements IOpticalFingerprintListener {
      public static IOpticalFingerprintListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.fingerprint.IOpticalFingerprintListener";
      }
      
      public void onOpticalFingerprintUpdate(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.fingerprint.IOpticalFingerprintListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOpticalFingerprintListener.Stub.getDefaultImpl() != null) {
            IOpticalFingerprintListener.Stub.getDefaultImpl().onOpticalFingerprintUpdate(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOpticalFingerprintListener param1IOpticalFingerprintListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOpticalFingerprintListener != null) {
          Proxy.sDefaultImpl = param1IOpticalFingerprintListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOpticalFingerprintListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
