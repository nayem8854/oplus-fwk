package android.hardware.fingerprint;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.hardware.biometrics.BiometricAuthenticator;
import android.hardware.biometrics.BiometricFingerprintConstants;
import android.hardware.biometrics.IBiometricServiceLockoutResetCallback;
import android.os.Binder;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.security.identity.IdentityCredential;
import android.util.Slog;
import java.security.Signature;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.Mac;

@Deprecated
public class FingerprintManager implements BiometricAuthenticator, BiometricFingerprintConstants, IOplusFingerprintManagerEx {
  private String mKeyguardPackageName = "com.android.systemui";
  
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private IBinder mToken = (IBinder)new Binder();
  
  private static final int MSG_ACQUIRED = 101;
  
  private static final int MSG_AUTHENTICATION_FAILED = 103;
  
  private static final int MSG_AUTHENTICATION_SUCCEEDED = 102;
  
  private static final int MSG_ENGINEERING_INFO = 1005;
  
  private static final int MSG_ENROLL_RESULT = 100;
  
  private static final int MSG_ENUMERATED = 106;
  
  private static final int MSG_ERROR = 104;
  
  private static final int MSG_IMAGE_INFO_ACQUIRED = 1004;
  
  private static final int MSG_MONITOR_EVENT_TRIGGERED = 1003;
  
  private static final int MSG_REMOVED = 105;
  
  private static final int MSG_TOUCHDOWN_EVNET = 1001;
  
  private static final int MSG_TOUCHUP_EVNET = 1002;
  
  public static final int OPTICAL_FINGERPRINT_HIDE = 0;
  
  public static final int OPTICAL_FINGERPRINT_SHOW = 1;
  
  private static final String TAG = "FingerprintManager";
  
  private AuthenticationCallback mAuthenticationCallback;
  
  private Context mContext;
  
  private CryptoObject mCryptoObject;
  
  private EngineeringInfoCallback mEngineeringInfoCallback;
  
  private EnrollmentCallback mEnrollmentCallback;
  
  private EnumerateCallback mEnumerateCallback;
  
  private FingerprintInputCallback mFingerprintInputCallback;
  
  private Handler mHandler;
  
  private MonitorEventCallback mMonitorEventCallback;
  
  private RemovalCallback mRemovalCallback;
  
  private Fingerprint mRemovalFingerprint;
  
  private IFingerprintService mService;
  
  private IFingerprintServiceReceiver mServiceReceiver;
  
  private class OnEnrollCancelListener implements CancellationSignal.OnCancelListener {
    final FingerprintManager this$0;
    
    private OnEnrollCancelListener() {}
    
    public void onCancel() {
      FingerprintManager.this.cancelEnrollment();
    }
  }
  
  private class OnAuthenticationCancelListener implements CancellationSignal.OnCancelListener {
    private android.hardware.biometrics.CryptoObject mCrypto;
    
    final FingerprintManager this$0;
    
    public OnAuthenticationCancelListener(android.hardware.biometrics.CryptoObject param1CryptoObject) {
      this.mCrypto = param1CryptoObject;
    }
    
    public void onCancel() {
      Slog.w("FingerprintManager", "onCancel");
      FingerprintManager.this.cancelAuthentication(this.mCrypto);
    }
  }
  
  @Deprecated
  public static final class CryptoObject extends android.hardware.biometrics.CryptoObject {
    public CryptoObject(Signature param1Signature) {
      super(param1Signature);
    }
    
    public CryptoObject(Cipher param1Cipher) {
      super(param1Cipher);
    }
    
    public CryptoObject(Mac param1Mac) {
      super(param1Mac);
    }
    
    public Signature getSignature() {
      return super.getSignature();
    }
    
    public Cipher getCipher() {
      return super.getCipher();
    }
    
    public Mac getMac() {
      return super.getMac();
    }
    
    public IdentityCredential getIdentityCredential() {
      return super.getIdentityCredential();
    }
  }
  
  @Deprecated
  class AuthenticationResult {
    private FingerprintManager.CryptoObject mCryptoObject;
    
    private Fingerprint mFingerprint;
    
    private boolean mIsStrongBiometric;
    
    private int mUserId;
    
    public AuthenticationResult(FingerprintManager this$0, Fingerprint param1Fingerprint, int param1Int, boolean param1Boolean) {
      this.mCryptoObject = (FingerprintManager.CryptoObject)this$0;
      this.mFingerprint = param1Fingerprint;
      this.mUserId = param1Int;
      this.mIsStrongBiometric = param1Boolean;
    }
    
    public FingerprintManager.CryptoObject getCryptoObject() {
      return this.mCryptoObject;
    }
    
    public Fingerprint getFingerprint() {
      return this.mFingerprint;
    }
    
    public int getUserId() {
      return this.mUserId;
    }
    
    public boolean isStrongBiometric() {
      return this.mIsStrongBiometric;
    }
  }
  
  @Deprecated
  public static abstract class AuthenticationCallback extends BiometricAuthenticator.AuthenticationCallback {
    public void onAuthenticationError(int param1Int, CharSequence param1CharSequence) {}
    
    public void onAuthenticationHelp(int param1Int, CharSequence param1CharSequence) {}
    
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult param1AuthenticationResult) {}
    
    public void onAuthenticationFailed() {}
    
    public void onAuthenticationAcquired(int param1Int) {}
    
    public void onImageInfoAcquired(OplusFingerprintManagerEx.FingerprintImageInfoBase param1FingerprintImageInfoBase) {}
    
    public void onTouchDown() {}
    
    public void onTouchUp() {}
  }
  
  public class FingerprintImageInfo extends OplusFingerprintManagerEx.FingerprintImageInfoBase {
    final FingerprintManager this$0;
    
    public FingerprintImageInfo(int param1Int1, int param1Int2, int param1Int3) {
      super(param1Int1, param1Int2, param1Int3);
    }
  }
  
  class EnrollmentCallback {
    public void onEnrollmentError(int param1Int, CharSequence param1CharSequence) {}
    
    public void onEnrollmentHelp(int param1Int, CharSequence param1CharSequence) {}
    
    public void onEnrollmentProgress(int param1Int) {}
    
    public void onTouchUp() {}
  }
  
  class RemovalCallback {
    public void onRemovalError(Fingerprint param1Fingerprint, int param1Int, CharSequence param1CharSequence) {}
    
    public void onRemovalSucceeded(Fingerprint param1Fingerprint, int param1Int) {}
  }
  
  class EnumerateCallback {
    public void onEnumerateError(int param1Int, CharSequence param1CharSequence) {}
    
    public void onEnumerate(Fingerprint param1Fingerprint) {}
  }
  
  class LockoutResetCallback {
    public void onLockoutReset() {}
  }
  
  class OpticalFingerprintListener {
    public void onOpticalFingerprintUpdate(int param1Int) {}
  }
  
  @Deprecated
  public void authenticate(CryptoObject paramCryptoObject, CancellationSignal paramCancellationSignal, int paramInt, AuthenticationCallback paramAuthenticationCallback, Handler paramHandler) {
    authenticate(paramCryptoObject, paramCancellationSignal, paramInt, paramAuthenticationCallback, paramHandler, this.mContext.getUserId());
  }
  
  @Deprecated
  public void authenticateFido(long paramLong, CancellationSignal paramCancellationSignal, int paramInt, Handler paramHandler, AuthenticationCallback paramAuthenticationCallback) {
    int i = this.mContext.getUserId();
    if (paramAuthenticationCallback != null) {
      if (paramCancellationSignal != null) {
        if (paramCancellationSignal.isCanceled()) {
          Slog.w("FingerprintManager", "authentication already canceled");
          return;
        } 
        paramCancellationSignal.setOnCancelListener(new OnAuthenticationCancelListener(null));
      } 
      if (this.mService != null)
        try {
          useHandler(paramHandler);
          this.mFingerprintInputCallback = null;
          this.mAuthenticationCallback = paramAuthenticationCallback;
          IFingerprintService iFingerprintService = this.mService;
          IBinder iBinder = this.mToken;
          IFingerprintServiceReceiver iFingerprintServiceReceiver = this.mServiceReceiver;
          Context context = this.mContext;
          String str = context.getOpPackageName();
          iFingerprintService.authenticate(iBinder, paramLong, i, iFingerprintServiceReceiver, paramInt, str);
        } catch (RemoteException remoteException) {
          Slog.w("FingerprintManager", "Remote exception while authenticating: ", (Throwable)remoteException);
          if (paramAuthenticationCallback != null) {
            Context context = this.mContext;
            String str = getErrorString(context, 1, 0);
            paramAuthenticationCallback.onAuthenticationError(1, str);
          } 
        }  
      return;
    } 
    throw new IllegalArgumentException("Must supply an authentication callback");
  }
  
  private void useHandler(Handler paramHandler) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("[useHandler] handler = ");
      stringBuilder.append(paramHandler);
      Slog.d("FingerprintManager", stringBuilder.toString());
    } 
    if (this.mKeyguardPackageName.equals(this.mContext.getOpPackageName())) {
      if (paramHandler != null) {
        if (DEBUG)
          Slog.d("FingerprintManager", "[useHandler] Keyguard Handler"); 
        this.mHandler = new MyHandler(paramHandler.getLooper(), null, true);
      } else {
        if (DEBUG)
          Slog.d("FingerprintManager", "[useHandler] New Handler for keyguard"); 
        this.mHandler = new MyHandler(this.mContext.getMainLooper(), null, true);
      } 
      return;
    } 
    if (paramHandler != null) {
      this.mHandler = new MyHandler(paramHandler.getLooper());
    } else if (this.mHandler.getLooper() != this.mContext.getMainLooper()) {
      this.mHandler = new MyHandler(this.mContext.getMainLooper());
    } 
  }
  
  public void authenticate(CryptoObject paramCryptoObject, CancellationSignal paramCancellationSignal, int paramInt1, AuthenticationCallback paramAuthenticationCallback, Handler paramHandler, int paramInt2) {
    if (paramAuthenticationCallback != null) {
      if (paramCancellationSignal != null) {
        if (paramCancellationSignal.isCanceled()) {
          Slog.w("FingerprintManager", "authentication already canceled");
          return;
        } 
        paramCancellationSignal.setOnCancelListener(new OnAuthenticationCancelListener(paramCryptoObject));
      } 
      if (this.mService != null)
        try {
          long l;
          useHandler(paramHandler);
          this.mFingerprintInputCallback = null;
          this.mAuthenticationCallback = paramAuthenticationCallback;
          this.mCryptoObject = paramCryptoObject;
          if (paramCryptoObject != null) {
            l = paramCryptoObject.getOpId();
          } else {
            l = 0L;
          } 
          IFingerprintService iFingerprintService = this.mService;
          IBinder iBinder = this.mToken;
          IFingerprintServiceReceiver iFingerprintServiceReceiver = this.mServiceReceiver;
          Context context = this.mContext;
          String str = context.getOpPackageName();
          iFingerprintService.authenticate(iBinder, l, paramInt2, iFingerprintServiceReceiver, paramInt1, str);
        } catch (RemoteException remoteException) {
          Slog.w("FingerprintManager", "Remote exception while authenticating: ", (Throwable)remoteException);
          if (paramAuthenticationCallback != null) {
            Context context = this.mContext;
            String str = getErrorString(context, 1, 0);
            paramAuthenticationCallback.onAuthenticationError(1, str);
          } 
        }  
      return;
    } 
    throw new IllegalArgumentException("Must supply an authentication callback");
  }
  
  public void enroll(byte[] paramArrayOfbyte, CancellationSignal paramCancellationSignal, int paramInt1, int paramInt2, EnrollmentCallback paramEnrollmentCallback) {
    int i = paramInt2;
    if (paramInt2 == -2)
      i = getCurrentUserId(); 
    if (paramEnrollmentCallback != null) {
      if (paramCancellationSignal != null) {
        if (paramCancellationSignal.isCanceled()) {
          Slog.w("FingerprintManager", "enrollment already canceled");
          return;
        } 
        paramCancellationSignal.setOnCancelListener(new OnEnrollCancelListener());
      } 
      IFingerprintService iFingerprintService = this.mService;
      if (iFingerprintService != null)
        try {
          this.mEnrollmentCallback = paramEnrollmentCallback;
          IBinder iBinder = this.mToken;
          IFingerprintServiceReceiver iFingerprintServiceReceiver = this.mServiceReceiver;
          Context context = this.mContext;
          String str = context.getOpPackageName();
          iFingerprintService.enroll(iBinder, paramArrayOfbyte, i, iFingerprintServiceReceiver, paramInt1, str);
        } catch (RemoteException remoteException) {
          Slog.w("FingerprintManager", "Remote exception in enroll: ", (Throwable)remoteException);
          if (paramEnrollmentCallback != null) {
            Context context = this.mContext;
            String str = getErrorString(context, 1, 0);
            paramEnrollmentCallback.onEnrollmentError(1, str);
          } 
        }  
      return;
    } 
    throw new IllegalArgumentException("Must supply an enrollment callback");
  }
  
  public long preEnroll() {
    long l1 = 0L;
    IFingerprintService iFingerprintService = this.mService;
    long l2 = l1;
    if (iFingerprintService != null)
      try {
        l2 = iFingerprintService.preEnroll(this.mToken);
      } catch (RemoteException remoteException) {
        Slog.w("FingerprintManager", "Remote exception in pre enroll: ", (Throwable)remoteException);
        l2 = l1;
      }  
    return l2;
  }
  
  public boolean pauseEnroll() {
    boolean bool;
    byte b = 0;
    IFingerprintService iFingerprintService = this.mService;
    int i = b;
    if (iFingerprintService != null)
      try {
        i = iFingerprintService.pauseEnroll();
      } catch (RemoteException remoteException) {
        Slog.w("FingerprintManager", "Remote exception in pauseEnroll: ", (Throwable)remoteException);
        i = b;
      }  
    if (i < 0) {
      bool = false;
    } else {
      bool = true;
    } 
    return bool;
  }
  
  public boolean continueEnroll() {
    boolean bool;
    byte b = 0;
    IFingerprintService iFingerprintService = this.mService;
    int i = b;
    if (iFingerprintService != null)
      try {
        i = iFingerprintService.continueEnroll();
      } catch (RemoteException remoteException) {
        Slog.w("FingerprintManager", "Remote exception in continueEnroll: ", (Throwable)remoteException);
        i = b;
      }  
    if (i < 0) {
      bool = false;
    } else {
      bool = true;
    } 
    return bool;
  }
  
  public int postEnroll() {
    byte b = 0;
    IFingerprintService iFingerprintService = this.mService;
    int i = b;
    if (iFingerprintService != null)
      try {
        i = iFingerprintService.postEnroll(this.mToken);
      } catch (RemoteException remoteException) {
        Slog.w("FingerprintManager", "Remote exception in post enroll: ", (Throwable)remoteException);
        i = b;
      }  
    return i;
  }
  
  public void setActiveUser(int paramInt) {
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null)
      try {
        iFingerprintService.setActiveUser(paramInt);
      } catch (RemoteException remoteException) {
        Slog.w("FingerprintManager", "Remote exception in setActiveUser: ", (Throwable)remoteException);
      }  
  }
  
  public void remove(Fingerprint paramFingerprint, int paramInt, RemovalCallback paramRemovalCallback) {
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null)
      try {
        this.mRemovalCallback = paramRemovalCallback;
        this.mRemovalFingerprint = paramFingerprint;
        iFingerprintService.remove(this.mToken, paramFingerprint.getBiometricId(), paramFingerprint.getGroupId(), paramInt, this.mServiceReceiver, this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        Slog.w("FingerprintManager", "Remote exception in remove: ", (Throwable)remoteException);
        if (paramRemovalCallback != null) {
          Context context = this.mContext;
          String str = getErrorString(context, 1, 0);
          paramRemovalCallback.onRemovalError(paramFingerprint, 1, str);
        } 
      }  
  }
  
  public void enumerate(int paramInt, EnumerateCallback paramEnumerateCallback) {
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null)
      try {
        this.mEnumerateCallback = paramEnumerateCallback;
        iFingerprintService.enumerate(this.mToken, paramInt, this.mServiceReceiver, this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        Slog.w("FingerprintManager", "Remote exception in enumerate: ", (Throwable)remoteException);
        if (paramEnumerateCallback != null) {
          Context context = this.mContext;
          String str = getErrorString(context, 1, 0);
          paramEnumerateCallback.onEnumerateError(1, str);
        } 
      }  
  }
  
  public void rename(int paramInt1, int paramInt2, String paramString) {
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null) {
      try {
        iFingerprintService.rename(paramInt1, paramInt2, paramString);
      } catch (RemoteException remoteException) {
        Slog.w("FingerprintManager", "Remote exception in rename(): ", (Throwable)remoteException);
      } 
    } else {
      Slog.w("FingerprintManager", "rename(): Service not connected!");
    } 
  }
  
  public List<Fingerprint> getEnrolledFingerprints(int paramInt) {
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null)
      try {
        return iFingerprintService.getEnrolledFingerprints(paramInt, this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        Slog.w("FingerprintManager", "Remote exception in getEnrolledFingerprints: ", (Throwable)remoteException);
      }  
    return null;
  }
  
  public List<Fingerprint> getEnrolledFingerprints() {
    return getEnrolledFingerprints(this.mContext.getUserId());
  }
  
  public boolean hasEnrolledTemplates() {
    return hasEnrolledFingerprints();
  }
  
  public boolean hasEnrolledTemplates(int paramInt) {
    return hasEnrolledFingerprints(paramInt);
  }
  
  @Deprecated
  public boolean hasEnrolledFingerprints() {
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null)
      try {
        Context context = this.mContext;
        int i = context.getUserId();
        String str = this.mContext.getOpPackageName();
        return iFingerprintService.hasEnrolledFingerprints(i, str);
      } catch (RemoteException remoteException) {
        Slog.w("FingerprintManager", "Remote exception in hasEnrolledFingerprints: ", (Throwable)remoteException);
      }  
    return false;
  }
  
  public boolean hasEnrolledFingerprints(int paramInt) {
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null)
      try {
        return iFingerprintService.hasEnrolledFingerprints(paramInt, this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        Slog.w("FingerprintManager", "Remote exception in hasEnrolledFingerprints: ", (Throwable)remoteException);
      }  
    return false;
  }
  
  @Deprecated
  public boolean isHardwareDetected() {
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null) {
      try {
        return iFingerprintService.isHardwareDetected(this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        Slog.w("FingerprintManager", "Remote exception in isFingerprintHardwareDetected: ", (Throwable)remoteException);
      } 
    } else {
      Slog.w("FingerprintManager", "isFingerprintHardwareDetected(): Service not connected!");
    } 
    return false;
  }
  
  public void addLockoutResetCallback(LockoutResetCallback paramLockoutResetCallback) {
    if (this.mService != null) {
      try {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("[addLockoutResetCallback] opPackageName = ");
        stringBuilder.append(this.mContext.getOpPackageName());
        Slog.d("FingerprintManager", stringBuilder.toString());
        PowerManager powerManager = this.mContext.<PowerManager>getSystemService(PowerManager.class);
        IFingerprintService iFingerprintService = this.mService;
        Object object = new Object();
        super(this, powerManager, paramLockoutResetCallback);
        iFingerprintService.addLockoutResetCallback((IBiometricServiceLockoutResetCallback)object);
      } catch (RemoteException remoteException) {
        Slog.w("FingerprintManager", "Remote exception in addLockoutResetCallback(): ", (Throwable)remoteException);
      } 
    } else {
      Slog.w("FingerprintManager", "addLockoutResetCallback(): Service not connected!");
    } 
  }
  
  public void setFingerKeymode(int paramInt) {
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null)
      try {
        iFingerprintService.setFingerKeymode(this.mToken, UserHandle.myUserId(), this.mContext.getOpPackageName(), paramInt);
      } catch (RemoteException remoteException) {
        Slog.e("FingerprintManager", "Remote exception in setFingerKeymode ", (Throwable)remoteException);
      }  
  }
  
  class FingerprintInputCallback {
    public void onTouchDown() {}
  }
  
  public int getEnrollmentTotalTimes() {
    byte b = 0;
    IFingerprintService iFingerprintService = this.mService;
    int i = b;
    if (iFingerprintService != null)
      try {
        i = iFingerprintService.getEnrollmentTotalTimes(this.mToken);
      } catch (RemoteException remoteException) {
        Slog.w("FingerprintManager", "Remote exception in enroll: ", (Throwable)remoteException);
        i = b;
      }  
    return i;
  }
  
  private class OnTouchEventMonitorCancelListener implements CancellationSignal.OnCancelListener {
    final FingerprintManager this$0;
    
    private OnTouchEventMonitorCancelListener() {}
    
    public void onCancel() {
      FingerprintManager.this.cancelTouchEventListener();
    }
  }
  
  public void setTouchEventListener(FingerprintInputCallback paramFingerprintInputCallback, CancellationSignal paramCancellationSignal) {
    if (paramFingerprintInputCallback != null) {
      if (paramCancellationSignal != null) {
        if (paramCancellationSignal.isCanceled()) {
          Slog.w("FingerprintManager", "setTouchEventListener already canceled");
          return;
        } 
        paramCancellationSignal.setOnCancelListener(new OnTouchEventMonitorCancelListener());
      } 
      IFingerprintService iFingerprintService = this.mService;
      if (iFingerprintService != null) {
        this.mAuthenticationCallback = null;
        this.mFingerprintInputCallback = paramFingerprintInputCallback;
        try {
          iFingerprintService.setTouchEventListener(this.mToken, this.mServiceReceiver, UserHandle.myUserId(), this.mContext.getOpPackageName());
        } catch (RemoteException remoteException) {
          Slog.e("FingerprintManager", "Remote exception in setTouchEventListener(): ", (Throwable)remoteException);
        } 
      } 
      return;
    } 
    throw new IllegalArgumentException("Must supply an setTouchEventListener callback");
  }
  
  public void cancelTouchEventListener() {
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null)
      try {
        iFingerprintService.cancelTouchEventListener(this.mToken, this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        if (DEBUG)
          Slog.w("FingerprintManager", "Remote exception while canceling touchevent"); 
      }  
  }
  
  public void setMonitorEventListener(MonitorEventCallback paramMonitorEventCallback) {
    if (paramMonitorEventCallback != null) {
      if (this.mService != null)
        this.mMonitorEventCallback = paramMonitorEventCallback; 
      return;
    } 
    throw new IllegalArgumentException("Must supply an setMonitorEventListener callback");
  }
  
  public void pauseIdentify() {
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null)
      try {
        iFingerprintService.pauseIdentify(this.mToken);
      } catch (RemoteException remoteException) {
        Slog.w("FingerprintManager", "Remote exception in pauseIdentify: ", (Throwable)remoteException);
      }  
  }
  
  public void continueIdentify() {
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null)
      try {
        iFingerprintService.continueIdentify(this.mToken);
      } catch (RemoteException remoteException) {
        Slog.w("FingerprintManager", "Remote exception in continueIdentify: ", (Throwable)remoteException);
      }  
  }
  
  public void finishUnLockedScreen(boolean paramBoolean) {
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null) {
      try {
        iFingerprintService.finishUnLockedScreen(paramBoolean, this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        Slog.e("FingerprintManager", "Remote exception in finishUnLockedScreen(): ", (Throwable)remoteException);
      } 
    } else {
      Slog.w("FingerprintManager", "finishUnLockedScreen(): Service not connected!");
    } 
  }
  
  class MonitorEventCallback {
    public void onMonitorEventTriggered(int param1Int, String param1String) {}
  }
  
  public int getAlikeyStatus() {
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null) {
      try {
        return iFingerprintService.getAlikeyStatus();
      } catch (RemoteException remoteException) {
        Slog.v("FingerprintManager", "Remote exception in getAlikeyStatus(): ", (Throwable)remoteException);
      } 
    } else {
      Slog.w("FingerprintManager", "getAlikeyStatus(): Service not connected!");
    } 
    return -1;
  }
  
  public int getEngineeringInfo(EngineeringInfoCallback paramEngineeringInfoCallback, int paramInt) {
    if (paramEngineeringInfoCallback != null) {
      IFingerprintService iFingerprintService = this.mService;
      if (iFingerprintService != null) {
        try {
          this.mEngineeringInfoCallback = paramEngineeringInfoCallback;
          return iFingerprintService.getEngineeringInfo(this.mToken, this.mContext.getOpPackageName(), UserHandle.myUserId(), this.mServiceReceiver, paramInt);
        } catch (RemoteException remoteException) {
          Slog.v("FingerprintManager", "Remote exception in getEngineeringInfo(): ", (Throwable)remoteException);
        } 
      } else {
        Slog.w("FingerprintManager", "getEngineeringInfo(): Service not connected!");
      } 
      return -1;
    } 
    throw new IllegalArgumentException("Must supply an getEngineeringInfo callback");
  }
  
  public void cancelGetEngineeringInfo(int paramInt) {
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null) {
      try {
        iFingerprintService.cancelGetEngineeringInfo(this.mToken, this.mContext.getOpPackageName(), paramInt);
        this.mEngineeringInfoCallback = null;
      } catch (RemoteException remoteException) {
        Slog.v("FingerprintManager", "Remote exception in cancelgetEngineeringInfo(): ", (Throwable)remoteException);
      } 
    } else {
      Slog.w("FingerprintManager", "cancelgetEngineeringInfo(): Service not connected!");
    } 
  }
  
  public byte[] alipayInvokeCommand(byte[] paramArrayOfbyte) {
    byte[] arrayOfByte = null;
    RemoteException remoteException = null;
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null) {
      try {
        paramArrayOfbyte = iFingerprintService.alipayInvokeCommand(paramArrayOfbyte);
      } catch (RemoteException remoteException1) {
        Slog.v("FingerprintManager", "Remote exception in alipayInvokeCommand(): ", (Throwable)remoteException1);
        remoteException1 = remoteException;
      } 
    } else {
      Slog.w("FingerprintManager", "alipayInvokeCommand(): Service not connected!");
      paramArrayOfbyte = arrayOfByte;
    } 
    return paramArrayOfbyte;
  }
  
  public int touchDown() {
    int i = -1;
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null) {
      try {
        int j = iFingerprintService.touchDown();
      } catch (RemoteException remoteException) {
        Slog.v("FingerprintManager", "Remote exception in touchDown(): ", (Throwable)remoteException);
      } 
    } else {
      Slog.w("FingerprintManager", "touchDown(): Service not connected!");
    } 
    return i;
  }
  
  public int touchUp() {
    int i = -1;
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null) {
      try {
        int j = iFingerprintService.touchUp();
      } catch (RemoteException remoteException) {
        Slog.v("FingerprintManager", "Remote exception in touchUp(): ", (Throwable)remoteException);
      } 
    } else {
      Slog.w("FingerprintManager", "touchUp(): Service not connected!");
    } 
    return i;
  }
  
  public int sendFingerprintCmd(int paramInt, byte[] paramArrayOfbyte) {
    byte b = -1;
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null) {
      try {
        paramInt = iFingerprintService.sendFingerprintCmd(paramInt, paramArrayOfbyte);
      } catch (RemoteException remoteException) {
        Slog.v("FingerprintManager", "Remote exception in sendFingerprintCmd(): ", (Throwable)remoteException);
        paramInt = b;
      } 
    } else {
      Slog.w("FingerprintManager", "sendFingerprintCmd(): Service not connected!");
      paramInt = b;
    } 
    return paramInt;
  }
  
  public byte[] getFingerprintAuthToken() {
    RemoteException remoteException2;
    byte[] arrayOfByte1 = null, arrayOfByte2 = null;
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null) {
      try {
        arrayOfByte2 = arrayOfByte1 = iFingerprintService.getFingerprintAuthToken(this.mContext.getOpPackageName());
      } catch (RemoteException remoteException1) {
        Slog.v("FingerprintManager", "Remote exception in getFingerprintAuthToken(): ", (Throwable)remoteException1);
      } 
    } else {
      Slog.w("FingerprintManager", "getFingerprintAuthToken(): Service not connected!");
      remoteException2 = remoteException1;
    } 
    return (byte[])remoteException2;
  }
  
  public int regsiterFingerprintCmdCallback(FingerprintCommandCallback paramFingerprintCommandCallback) {
    int i = -1;
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null) {
      try {
        Object object = new Object();
        super(this, paramFingerprintCommandCallback);
        int j = iFingerprintService.regsiterFingerprintCmdCallback((IFingerprintCommandCallback)object);
      } catch (RemoteException remoteException) {
        Slog.v("FingerprintManager", "Remote exception in regsiterFingerprintCmdCallback(): ", (Throwable)remoteException);
      } 
    } else {
      Slog.w("FingerprintManager", "regsiterFingerprintCmdCallback(): Service not connected!");
    } 
    return i;
  }
  
  public int unregsiterFingerprintCmdCallback(FingerprintCommandCallback paramFingerprintCommandCallback) {
    int i = -1;
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null) {
      try {
        Object object = new Object();
        super(this, paramFingerprintCommandCallback);
        int j = iFingerprintService.unregsiterFingerprintCmdCallback((IFingerprintCommandCallback)object);
      } catch (RemoteException remoteException) {
        Slog.v("FingerprintManager", "Remote exception in unregsiterFingerprintCmdCallback(): ", (Throwable)remoteException);
      } 
    } else {
      Slog.w("FingerprintManager", "unregsiterFingerprintCmdCallback(): Service not connected!");
    } 
    return i;
  }
  
  public int regsiterOpticalFingerprintListener(IOplusFingerprintManagerEx.OpticalFingerprintListener paramOpticalFingerprintListener) {
    int i = -1;
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null) {
      try {
        Object object = new Object();
        super(this, paramOpticalFingerprintListener);
        int j = iFingerprintService.regsiterOpticalFingerprintListener((IOpticalFingerprintListener)object);
      } catch (RemoteException remoteException) {
        Slog.v("FingerprintManager", "Remote exception in regsiterOpticalFingerprintListener(): ", (Throwable)remoteException);
      } 
    } else {
      Slog.w("FingerprintManager", "regsiterOpticalFingerprintListener(): Service not connected!");
    } 
    return i;
  }
  
  public int regsiterOpticalFingerprintListener(OpticalFingerprintListener paramOpticalFingerprintListener) {
    int i = -1;
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null) {
      try {
        Object object = new Object();
        super(this, paramOpticalFingerprintListener);
        int j = iFingerprintService.regsiterOpticalFingerprintListener((IOpticalFingerprintListener)object);
      } catch (RemoteException remoteException) {
        Slog.v("FingerprintManager", "Remote exception in regsiterOpticalFingerprintListener(): ", (Throwable)remoteException);
      } 
    } else {
      Slog.w("FingerprintManager", "regsiterOpticalFingerprintListener(): Service not connected!");
    } 
    return i;
  }
  
  public int unregsiterOpticalFingerprintListener() {
    int i = -1;
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null) {
      try {
        int j = iFingerprintService.unregsiterOpticalFingerprintListener();
      } catch (RemoteException remoteException) {
        Slog.v("FingerprintManager", "Remote exception in unregsiterOpticalFingerprintListener(): ", (Throwable)remoteException);
      } 
    } else {
      Slog.w("FingerprintManager", "unregsiterOpticalFingerprintListener(): Service not connected!");
    } 
    return i;
  }
  
  private class MyHandler extends Handler {
    final FingerprintManager this$0;
    
    private MyHandler(Context param1Context) {
      super(param1Context.getMainLooper());
    }
    
    private MyHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    private MyHandler(Looper param1Looper, Handler.Callback param1Callback, boolean param1Boolean) {
      super(param1Looper, param1Callback, param1Boolean);
    }
    
    public void handleMessage(Message param1Message) {
      Fingerprint fingerprint1;
      FingerprintManager fingerprintManager1, fingerprintManager2;
      Fingerprint fingerprint2;
      long l;
      int j, k;
      boolean bool;
      int i = param1Message.what;
      switch (i) {
        default:
          switch (i) {
            default:
              return;
            case 1005:
              FingerprintManager.this.sendEngineeringInfo((EngineeringInfo)param1Message.obj);
            case 1004:
              FingerprintManager.this.sendImageInfo((OplusFingerprintManagerEx.FingerprintImageInfoBase)param1Message.obj);
            case 1003:
              FingerprintManager.this.sendMonitorEventTriggered(param1Message.arg2, (String)param1Message.obj);
            case 1002:
              FingerprintManager.this.sendTouchUpEvent(((Long)param1Message.obj).longValue(), param1Message.arg1, param1Message.arg2);
            case 1001:
              break;
          } 
          FingerprintManager.this.sendTouchDownEvent(((Long)param1Message.obj).longValue(), param1Message.arg1, param1Message.arg2);
        case 106:
          FingerprintManager.this.sendEnumeratedResult(((Long)param1Message.obj).longValue(), param1Message.arg1, param1Message.arg2);
        case 105:
          fingerprint1 = (Fingerprint)param1Message.obj;
          fingerprintManager2 = FingerprintManager.this;
          l = fingerprint1.getDeviceId();
          j = fingerprint1.getBiometricId();
          i = fingerprint1.getGroupId();
          k = param1Message.arg1;
          fingerprintManager2.sendRemovedResult(l, j, i, k);
        case 104:
          FingerprintManager.this.sendErrorResult(((Long)param1Message.obj).longValue(), param1Message.arg1, param1Message.arg2);
        case 103:
          FingerprintManager.this.sendAuthenticatedFailed();
        case 102:
          fingerprintManager1 = FingerprintManager.this;
          fingerprint2 = (Fingerprint)param1Message.obj;
          j = param1Message.arg1;
          i = param1Message.arg2;
          bool = true;
          if (i != 1)
            bool = false; 
          fingerprintManager1.sendAuthenticatedSucceeded(fingerprint2, j, bool);
        case 101:
          FingerprintManager.this.sendAcquiredResult(((Long)param1Message.obj).longValue(), param1Message.arg1, param1Message.arg2);
        case 100:
          break;
      } 
      FingerprintManager.this.sendEnrollResult((Fingerprint)param1Message.obj, param1Message.arg1);
    }
  }
  
  private void sendEngineeringInfo(EngineeringInfo paramEngineeringInfo) {
    EngineeringInfoCallback engineeringInfoCallback = this.mEngineeringInfoCallback;
    if (engineeringInfoCallback != null)
      engineeringInfoCallback.onEngineeringInfoUpdated(paramEngineeringInfo); 
  }
  
  private void sendTouchDownEvent(long paramLong, int paramInt1, int paramInt2) {
    FingerprintInputCallback fingerprintInputCallback = this.mFingerprintInputCallback;
    if (fingerprintInputCallback != null)
      fingerprintInputCallback.onTouchDown(); 
    AuthenticationCallback authenticationCallback = this.mAuthenticationCallback;
    if (authenticationCallback != null)
      authenticationCallback.onTouchDown(); 
  }
  
  private void sendTouchUpEvent(long paramLong, int paramInt1, int paramInt2) {
    EnrollmentCallback enrollmentCallback = this.mEnrollmentCallback;
    if (enrollmentCallback != null)
      enrollmentCallback.onTouchUp(); 
    AuthenticationCallback authenticationCallback = this.mAuthenticationCallback;
    if (authenticationCallback != null)
      authenticationCallback.onTouchUp(); 
  }
  
  private void sendMonitorEventTriggered(int paramInt, String paramString) {
    MonitorEventCallback monitorEventCallback = this.mMonitorEventCallback;
    if (monitorEventCallback != null)
      monitorEventCallback.onMonitorEventTriggered(paramInt, paramString); 
  }
  
  private void sendImageInfo(OplusFingerprintManagerEx.FingerprintImageInfoBase paramFingerprintImageInfoBase) {
    AuthenticationCallback authenticationCallback = this.mAuthenticationCallback;
    if (authenticationCallback != null)
      authenticationCallback.onImageInfoAcquired(paramFingerprintImageInfoBase); 
  }
  
  private void sendRemovedResult(long paramLong, int paramInt1, int paramInt2, int paramInt3) {
    if (this.mRemovalCallback != null) {
      int i = this.mRemovalFingerprint.getBiometricId();
      int j = this.mRemovalFingerprint.getGroupId();
      if (i != 0 && paramInt1 != 0 && paramInt1 != i) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Finger id didn't match: ");
        stringBuilder.append(paramInt1);
        stringBuilder.append(" != ");
        stringBuilder.append(i);
        Slog.w("FingerprintManager", stringBuilder.toString());
        return;
      } 
      if (paramInt2 != j) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Group id didn't match: ");
        stringBuilder.append(paramInt2);
        stringBuilder.append(" != ");
        stringBuilder.append(j);
        Slog.w("FingerprintManager", stringBuilder.toString());
        return;
      } 
      if (DEBUG)
        Slog.d("FingerprintManager", "onRemovalSucceeded"); 
      this.mRemovalCallback.onRemovalSucceeded(new Fingerprint(null, paramInt2, paramInt1, paramLong), paramInt3);
    } 
  }
  
  private void sendRemovedResult(Fingerprint paramFingerprint, int paramInt) {
    StringBuilder stringBuilder;
    if (this.mRemovalCallback == null)
      return; 
    if (paramFingerprint == null) {
      Slog.e("FingerprintManager", "Received MSG_REMOVED, but fingerprint is null");
      return;
    } 
    int i = paramFingerprint.getBiometricId();
    int j = this.mRemovalFingerprint.getBiometricId();
    if (j != 0 && i != 0 && i != j) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Finger id didn't match: ");
      stringBuilder.append(i);
      stringBuilder.append(" != ");
      stringBuilder.append(j);
      Slog.w("FingerprintManager", stringBuilder.toString());
      return;
    } 
    j = stringBuilder.getGroupId();
    i = this.mRemovalFingerprint.getGroupId();
    if (j != i) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Group id didn't match: ");
      stringBuilder.append(j);
      stringBuilder.append(" != ");
      stringBuilder.append(i);
      Slog.w("FingerprintManager", stringBuilder.toString());
      return;
    } 
    this.mRemovalCallback.onRemovalSucceeded((Fingerprint)stringBuilder, paramInt);
  }
  
  private void sendEnumeratedResult(long paramLong, int paramInt1, int paramInt2) {
    EnumerateCallback enumerateCallback = this.mEnumerateCallback;
    if (enumerateCallback != null)
      enumerateCallback.onEnumerate(new Fingerprint(null, paramInt2, paramInt1, paramLong)); 
  }
  
  private void sendEnrollResult(Fingerprint paramFingerprint, int paramInt) {
    EnrollmentCallback enrollmentCallback = this.mEnrollmentCallback;
    if (enrollmentCallback != null)
      enrollmentCallback.onEnrollmentProgress(paramInt); 
  }
  
  private void sendAuthenticatedSucceeded(Fingerprint paramFingerprint, int paramInt, boolean paramBoolean) {
    if (DEBUG)
      Slog.d("FingerprintManager", "sendAuthenticatedSucceeded"); 
    if (this.mAuthenticationCallback != null) {
      AuthenticationResult authenticationResult = new AuthenticationResult(this.mCryptoObject, paramFingerprint, paramInt, paramBoolean);
      this.mAuthenticationCallback.onAuthenticationSucceeded(authenticationResult);
    } 
  }
  
  private void sendAuthenticatedFailed() {
    if (DEBUG)
      Slog.d("FingerprintManager", "sendAuthenticatedFailed"); 
    AuthenticationCallback authenticationCallback = this.mAuthenticationCallback;
    if (authenticationCallback != null)
      authenticationCallback.onAuthenticationFailed(); 
  }
  
  private void sendAcquiredResult(long paramLong, int paramInt1, int paramInt2) {
    if (DEBUG)
      Slog.d("FingerprintManager", "sendAcquiredResult"); 
    AuthenticationCallback authenticationCallback = this.mAuthenticationCallback;
    if (authenticationCallback != null)
      authenticationCallback.onAuthenticationAcquired(paramInt1); 
    String str = getAcquiredString(this.mContext, paramInt1, paramInt2);
    if (str == null)
      return; 
    if (paramInt1 == 6) {
      paramInt2 += 1000;
    } else {
      paramInt2 = paramInt1;
    } 
    EnrollmentCallback enrollmentCallback = this.mEnrollmentCallback;
    if (enrollmentCallback != null) {
      enrollmentCallback.onEnrollmentHelp(paramInt2, str);
    } else {
      AuthenticationCallback authenticationCallback1 = this.mAuthenticationCallback;
      if (authenticationCallback1 != null && 
        paramInt1 != 7)
        authenticationCallback1.onAuthenticationHelp(paramInt2, str); 
    } 
  }
  
  private void sendErrorResult(long paramLong, int paramInt1, int paramInt2) {
    int i;
    if (paramInt1 == 8) {
      i = paramInt2 + 1000;
    } else {
      i = paramInt1;
    } 
    EnrollmentCallback enrollmentCallback = this.mEnrollmentCallback;
    if (enrollmentCallback != null) {
      Context context = this.mContext;
      String str = getErrorString(context, paramInt1, paramInt2);
      enrollmentCallback.onEnrollmentError(i, str);
    } else {
      AuthenticationCallback authenticationCallback = this.mAuthenticationCallback;
      if (authenticationCallback != null) {
        Context context = this.mContext;
        String str = getErrorString(context, paramInt1, paramInt2);
        authenticationCallback.onAuthenticationError(i, str);
      } else {
        RemovalCallback removalCallback = this.mRemovalCallback;
        if (removalCallback != null) {
          Fingerprint fingerprint = this.mRemovalFingerprint;
          Context context = this.mContext;
          String str = getErrorString(context, paramInt1, paramInt2);
          removalCallback.onRemovalError(fingerprint, i, str);
        } else {
          EnumerateCallback enumerateCallback = this.mEnumerateCallback;
          if (enumerateCallback != null) {
            Context context = this.mContext;
            String str = getErrorString(context, paramInt1, paramInt2);
            enumerateCallback.onEnumerateError(i, str);
          } 
        } 
      } 
    } 
  }
  
  private int getCurrentUserId() {
    try {
      return (ActivityManager.getService().getCurrentUser()).id;
    } catch (RemoteException remoteException) {
      Slog.w("FingerprintManager", "Failed to get current user id\n");
      return -10000;
    } 
  }
  
  private void cancelEnrollment() {
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null)
      try {
        iFingerprintService.cancelEnrollment(this.mToken, this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        if (DEBUG)
          Slog.w("FingerprintManager", "Remote exception while canceling enrollment"); 
      }  
  }
  
  private void cancelAuthentication(android.hardware.biometrics.CryptoObject paramCryptoObject) {
    if (this.mService != null)
      try {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("cancelAuthentication package:");
        stringBuilder.append(this.mContext.getOpPackageName());
        Slog.w("FingerprintManager", stringBuilder.toString());
        this.mService.cancelAuthentication(this.mToken, this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        if (DEBUG)
          Slog.w("FingerprintManager", "Remote exception while canceling enrollment"); 
      }  
  }
  
  public static String getErrorString(Context paramContext, int paramInt1, int paramInt2) {
    StringBuilder stringBuilder;
    String[] arrayOfString;
    switch (paramInt1) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid error message: ");
        stringBuilder.append(paramInt1);
        stringBuilder.append(", ");
        stringBuilder.append(paramInt2);
        Slog.w("FingerprintManager", stringBuilder.toString());
        return null;
      case 15:
        return stringBuilder.getString(17040236);
      case 12:
        return stringBuilder.getString(17040231);
      case 11:
        return stringBuilder.getString(17040234);
      case 10:
        return stringBuilder.getString(17040239);
      case 9:
        return stringBuilder.getString(17040233);
      case 8:
        arrayOfString = stringBuilder.getResources().getStringArray(17236105);
        if (paramInt2 < arrayOfString.length)
          return arrayOfString[paramInt2]; 
      case 7:
        return arrayOfString.getString(17040232);
      case 5:
        return arrayOfString.getString(17040229);
      case 4:
        return arrayOfString.getString(17040235);
      case 3:
        return arrayOfString.getString(17040237);
      case 2:
        return arrayOfString.getString(17040238);
      case 1:
        break;
    } 
    return arrayOfString.getString(17040230);
  }
  
  public static String getAcquiredString(Context paramContext, int paramInt1, int paramInt2) {
    if (paramInt1 != 1001) {
      if (paramInt1 != 1002) {
        StringBuilder stringBuilder;
        String[] arrayOfString;
        switch (paramInt1) {
          default:
            stringBuilder = new StringBuilder();
            stringBuilder.append("Invalid acquired message: ");
            stringBuilder.append(paramInt1);
            stringBuilder.append(", ");
            stringBuilder.append(paramInt2);
            Slog.w("FingerprintManager", stringBuilder.toString());
            return null;
          case 7:
            return null;
          case 6:
            arrayOfString = stringBuilder.getResources().getStringArray(17236104);
            if (paramInt2 < arrayOfString.length)
              return arrayOfString[paramInt2]; 
          case 5:
            return arrayOfString.getString(17040226);
          case 4:
            return arrayOfString.getString(17040227);
          case 3:
            return arrayOfString.getString(17040223);
          case 2:
            return arrayOfString.getString(17040224);
          case 1:
            return arrayOfString.getString(17040225);
          case 0:
            break;
        } 
        return null;
      } 
      return "already enrolled finger";
    } 
    return "acquared too similar";
  }
  
  public FingerprintManager(Context paramContext, IFingerprintService paramIFingerprintService) {
    this.mServiceReceiver = (IFingerprintServiceReceiver)new Object(this);
    this.mContext = paramContext;
    this.mService = paramIFingerprintService;
    if (paramIFingerprintService == null)
      Slog.v("FingerprintManager", "FingerprintManagerService was null"); 
    this.mHandler = new MyHandler(paramContext);
    ComponentName componentName = ComponentName.unflattenFromString(paramContext.getResources().getString(17039923));
    this.mKeyguardPackageName = componentName.getPackageName();
  }
  
  public long getLockoutAttemptDeadline() {
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null) {
      try {
        return iFingerprintService.getLockoutAttemptDeadline(this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        Slog.v("FingerprintManager", "Remote exception in getLockoutAttemptDeadline(): ", (Throwable)remoteException);
      } 
    } else {
      Slog.w("FingerprintManager", "getLockoutAttemptDeadline(): Service not connected!");
    } 
    return -1L;
  }
  
  public int getFailedAttempts() {
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null) {
      try {
        return iFingerprintService.getFailedAttempts(this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        Slog.v("FingerprintManager", "Remote exception in getFailedAttempts(): ", (Throwable)remoteException);
      } 
    } else {
      Slog.w("FingerprintManager", "getFailedAttempts(): Service not connected!");
    } 
    return -1;
  }
  
  public void showFingerprintIcon() {
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null) {
      try {
        iFingerprintService.showFingerprintIcon(this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        Slog.v("FingerprintManager", "Remote exception in showFingerprintIcon(): ", (Throwable)remoteException);
      } 
    } else {
      Slog.w("FingerprintManager", "showFingerprintIcon(): Service not connected!");
    } 
  }
  
  public void hideFingerprintIcon() {
    if (isFingerprintIconHided())
      return; 
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null) {
      try {
        iFingerprintService.hideFingerprintIcon(0, this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        Slog.v("FingerprintManager", "Remote exception in hideFingerprintIcon(): ", (Throwable)remoteException);
      } 
    } else {
      Slog.w("FingerprintManager", "hideFingerprintIcon(): Service not connected!");
    } 
  }
  
  private boolean isFingerprintIconHided() {
    if (getCurrentIconStatus() == 0)
      return true; 
    return false;
  }
  
  public int getCurrentIconStatus() {
    IFingerprintService iFingerprintService = this.mService;
    if (iFingerprintService != null) {
      try {
        return iFingerprintService.getCurrentIconStatus();
      } catch (RemoteException remoteException) {
        Slog.v("FingerprintManager", "Remote exception in updateOpticalFingerprintIcon(): ", (Throwable)remoteException);
      } 
    } else {
      Slog.w("FingerprintManager", "updateOpticalFingerprintIcon(): Service not connected!");
    } 
    return -1;
  }
  
  class EngineeringInfoCallback {
    public abstract void onEngineeringInfoUpdated(EngineeringInfo param1EngineeringInfo);
    
    public abstract void onError(int param1Int, CharSequence param1CharSequence);
  }
  
  class FingerprintCommandCallback {
    public abstract void onFingerprintCmd(int param1Int, byte[] param1ArrayOfbyte);
  }
}
