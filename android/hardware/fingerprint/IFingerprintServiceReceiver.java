package android.hardware.fingerprint;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IFingerprintServiceReceiver extends IInterface {
  void onAcquired(long paramLong, int paramInt1, int paramInt2) throws RemoteException;
  
  void onAuthenticationFailed(long paramLong) throws RemoteException;
  
  void onAuthenticationSucceeded(long paramLong, Fingerprint paramFingerprint, int paramInt, boolean paramBoolean) throws RemoteException;
  
  void onEngineeringInfoUpdated(EngineeringInfo paramEngineeringInfo) throws RemoteException;
  
  void onEnrollResult(long paramLong, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void onEnumerated(long paramLong, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void onError(long paramLong, int paramInt1, int paramInt2) throws RemoteException;
  
  void onImageInfoAcquired(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void onMonitorEventTriggered(int paramInt, String paramString) throws RemoteException;
  
  void onRemoved(long paramLong, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void onTouchDown(long paramLong) throws RemoteException;
  
  void onTouchUp(long paramLong) throws RemoteException;
  
  class Default implements IFingerprintServiceReceiver {
    public void onEnrollResult(long param1Long, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void onAcquired(long param1Long, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onAuthenticationSucceeded(long param1Long, Fingerprint param1Fingerprint, int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void onAuthenticationFailed(long param1Long) throws RemoteException {}
    
    public void onError(long param1Long, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onRemoved(long param1Long, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void onTouchDown(long param1Long) throws RemoteException {}
    
    public void onTouchUp(long param1Long) throws RemoteException {}
    
    public void onMonitorEventTriggered(int param1Int, String param1String) throws RemoteException {}
    
    public void onImageInfoAcquired(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void onEngineeringInfoUpdated(EngineeringInfo param1EngineeringInfo) throws RemoteException {}
    
    public void onEnumerated(long param1Long, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IFingerprintServiceReceiver {
    private static final String DESCRIPTOR = "android.hardware.fingerprint.IFingerprintServiceReceiver";
    
    static final int TRANSACTION_onAcquired = 2;
    
    static final int TRANSACTION_onAuthenticationFailed = 4;
    
    static final int TRANSACTION_onAuthenticationSucceeded = 3;
    
    static final int TRANSACTION_onEngineeringInfoUpdated = 11;
    
    static final int TRANSACTION_onEnrollResult = 1;
    
    static final int TRANSACTION_onEnumerated = 12;
    
    static final int TRANSACTION_onError = 5;
    
    static final int TRANSACTION_onImageInfoAcquired = 10;
    
    static final int TRANSACTION_onMonitorEventTriggered = 9;
    
    static final int TRANSACTION_onRemoved = 6;
    
    static final int TRANSACTION_onTouchDown = 7;
    
    static final int TRANSACTION_onTouchUp = 8;
    
    public Stub() {
      attachInterface(this, "android.hardware.fingerprint.IFingerprintServiceReceiver");
    }
    
    public static IFingerprintServiceReceiver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.fingerprint.IFingerprintServiceReceiver");
      if (iInterface != null && iInterface instanceof IFingerprintServiceReceiver)
        return (IFingerprintServiceReceiver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 12:
          return "onEnumerated";
        case 11:
          return "onEngineeringInfoUpdated";
        case 10:
          return "onImageInfoAcquired";
        case 9:
          return "onMonitorEventTriggered";
        case 8:
          return "onTouchUp";
        case 7:
          return "onTouchDown";
        case 6:
          return "onRemoved";
        case 5:
          return "onError";
        case 4:
          return "onAuthenticationFailed";
        case 3:
          return "onAuthenticationSucceeded";
        case 2:
          return "onAcquired";
        case 1:
          break;
      } 
      return "onEnrollResult";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String str;
        Fingerprint fingerprint;
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 12:
            param1Parcel1.enforceInterface("android.hardware.fingerprint.IFingerprintServiceReceiver");
            l = param1Parcel1.readLong();
            i = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            onEnumerated(l, i, param1Int2, param1Int1);
            return true;
          case 11:
            param1Parcel1.enforceInterface("android.hardware.fingerprint.IFingerprintServiceReceiver");
            if (param1Parcel1.readInt() != 0) {
              EngineeringInfo engineeringInfo = (EngineeringInfo)EngineeringInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onEngineeringInfoUpdated((EngineeringInfo)param1Parcel1);
            return true;
          case 10:
            param1Parcel1.enforceInterface("android.hardware.fingerprint.IFingerprintServiceReceiver");
            i = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            onImageInfoAcquired(i, param1Int1, param1Int2);
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.hardware.fingerprint.IFingerprintServiceReceiver");
            param1Int1 = param1Parcel1.readInt();
            str = param1Parcel1.readString();
            onMonitorEventTriggered(param1Int1, str);
            return true;
          case 8:
            str.enforceInterface("android.hardware.fingerprint.IFingerprintServiceReceiver");
            l = str.readLong();
            onTouchUp(l);
            return true;
          case 7:
            str.enforceInterface("android.hardware.fingerprint.IFingerprintServiceReceiver");
            l = str.readLong();
            onTouchDown(l);
            return true;
          case 6:
            str.enforceInterface("android.hardware.fingerprint.IFingerprintServiceReceiver");
            l = str.readLong();
            param1Int1 = str.readInt();
            i = str.readInt();
            param1Int2 = str.readInt();
            onRemoved(l, param1Int1, i, param1Int2);
            return true;
          case 5:
            str.enforceInterface("android.hardware.fingerprint.IFingerprintServiceReceiver");
            l = str.readLong();
            param1Int1 = str.readInt();
            param1Int2 = str.readInt();
            onError(l, param1Int1, param1Int2);
            return true;
          case 4:
            str.enforceInterface("android.hardware.fingerprint.IFingerprintServiceReceiver");
            l = str.readLong();
            onAuthenticationFailed(l);
            return true;
          case 3:
            str.enforceInterface("android.hardware.fingerprint.IFingerprintServiceReceiver");
            l = str.readLong();
            if (str.readInt() != 0) {
              fingerprint = (Fingerprint)Fingerprint.CREATOR.createFromParcel((Parcel)str);
            } else {
              fingerprint = null;
            } 
            param1Int1 = str.readInt();
            if (str.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            onAuthenticationSucceeded(l, fingerprint, param1Int1, bool);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str.enforceInterface("android.hardware.fingerprint.IFingerprintServiceReceiver");
            l = str.readLong();
            param1Int2 = str.readInt();
            param1Int1 = str.readInt();
            onAcquired(l, param1Int2, param1Int1);
            return true;
          case 1:
            break;
        } 
        str.enforceInterface("android.hardware.fingerprint.IFingerprintServiceReceiver");
        long l = str.readLong();
        param1Int2 = str.readInt();
        int i = str.readInt();
        param1Int1 = str.readInt();
        onEnrollResult(l, param1Int2, i, param1Int1);
        return true;
      } 
      param1Parcel2.writeString("android.hardware.fingerprint.IFingerprintServiceReceiver");
      return true;
    }
    
    private static class Proxy implements IFingerprintServiceReceiver {
      public static IFingerprintServiceReceiver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.fingerprint.IFingerprintServiceReceiver";
      }
      
      public void onEnrollResult(long param2Long, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.fingerprint.IFingerprintServiceReceiver");
          parcel.writeLong(param2Long);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IFingerprintServiceReceiver.Stub.getDefaultImpl() != null) {
            IFingerprintServiceReceiver.Stub.getDefaultImpl().onEnrollResult(param2Long, param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAcquired(long param2Long, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.fingerprint.IFingerprintServiceReceiver");
          parcel.writeLong(param2Long);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IFingerprintServiceReceiver.Stub.getDefaultImpl() != null) {
            IFingerprintServiceReceiver.Stub.getDefaultImpl().onAcquired(param2Long, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAuthenticationSucceeded(long param2Long, Fingerprint param2Fingerprint, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.fingerprint.IFingerprintServiceReceiver");
          parcel1.writeLong(param2Long);
          boolean bool = true;
          if (param2Fingerprint != null) {
            parcel1.writeInt(1);
            param2Fingerprint.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool1 && IFingerprintServiceReceiver.Stub.getDefaultImpl() != null) {
            IFingerprintServiceReceiver.Stub.getDefaultImpl().onAuthenticationSucceeded(param2Long, param2Fingerprint, param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onAuthenticationFailed(long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.fingerprint.IFingerprintServiceReceiver");
          parcel.writeLong(param2Long);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IFingerprintServiceReceiver.Stub.getDefaultImpl() != null) {
            IFingerprintServiceReceiver.Stub.getDefaultImpl().onAuthenticationFailed(param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onError(long param2Long, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.fingerprint.IFingerprintServiceReceiver");
          parcel.writeLong(param2Long);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IFingerprintServiceReceiver.Stub.getDefaultImpl() != null) {
            IFingerprintServiceReceiver.Stub.getDefaultImpl().onError(param2Long, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRemoved(long param2Long, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.fingerprint.IFingerprintServiceReceiver");
          parcel.writeLong(param2Long);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IFingerprintServiceReceiver.Stub.getDefaultImpl() != null) {
            IFingerprintServiceReceiver.Stub.getDefaultImpl().onRemoved(param2Long, param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTouchDown(long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.fingerprint.IFingerprintServiceReceiver");
          parcel.writeLong(param2Long);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IFingerprintServiceReceiver.Stub.getDefaultImpl() != null) {
            IFingerprintServiceReceiver.Stub.getDefaultImpl().onTouchDown(param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTouchUp(long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.fingerprint.IFingerprintServiceReceiver");
          parcel.writeLong(param2Long);
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IFingerprintServiceReceiver.Stub.getDefaultImpl() != null) {
            IFingerprintServiceReceiver.Stub.getDefaultImpl().onTouchUp(param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onMonitorEventTriggered(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.fingerprint.IFingerprintServiceReceiver");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IFingerprintServiceReceiver.Stub.getDefaultImpl() != null) {
            IFingerprintServiceReceiver.Stub.getDefaultImpl().onMonitorEventTriggered(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onImageInfoAcquired(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.fingerprint.IFingerprintServiceReceiver");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IFingerprintServiceReceiver.Stub.getDefaultImpl() != null) {
            IFingerprintServiceReceiver.Stub.getDefaultImpl().onImageInfoAcquired(param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onEngineeringInfoUpdated(EngineeringInfo param2EngineeringInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.fingerprint.IFingerprintServiceReceiver");
          if (param2EngineeringInfo != null) {
            parcel.writeInt(1);
            param2EngineeringInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IFingerprintServiceReceiver.Stub.getDefaultImpl() != null) {
            IFingerprintServiceReceiver.Stub.getDefaultImpl().onEngineeringInfoUpdated(param2EngineeringInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onEnumerated(long param2Long, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.fingerprint.IFingerprintServiceReceiver");
          parcel.writeLong(param2Long);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(12, parcel, null, 1);
          if (!bool && IFingerprintServiceReceiver.Stub.getDefaultImpl() != null) {
            IFingerprintServiceReceiver.Stub.getDefaultImpl().onEnumerated(param2Long, param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IFingerprintServiceReceiver param1IFingerprintServiceReceiver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IFingerprintServiceReceiver != null) {
          Proxy.sDefaultImpl = param1IFingerprintServiceReceiver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IFingerprintServiceReceiver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
