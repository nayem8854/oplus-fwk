package android.hardware.fingerprint;

import com.oplus.reflect.RefClass;
import com.oplus.reflect.RefMethod;

public class OplusMirrorFingerprintManager {
  public static Class<?> TYPE = RefClass.load(OplusMirrorFingerprintManager.class, FingerprintManager.class);
  
  public static RefMethod<Integer> getFailedAttempts;
  
  public static RefMethod<Long> getLockoutAttemptDeadline;
  
  public static RefMethod<Void> hideFingerprintIcon;
  
  public static RefMethod<Void> showFingerprintIcon;
}
