package android.hardware;

import android.app.ActivityManager;
import android.app.ActivityThread;
import android.app.Application;
import android.util.Log;

public class OplusCameraUtils {
  private static final String OPPO_CAMERA_PACKNAME_ACTIVITY = "oppo.camera.packname.activity";
  
  private static final String TAG = "OplusCameraUtils";
  
  public static String getActivityName() {
    Application application = ActivityThread.currentApplication();
    ActivityManager activityManager = (ActivityManager)application.getApplicationContext().getSystemService("activity");
    if (activityManager.getAppTasks().isEmpty() || (
      (ActivityManager.AppTask)activityManager.getAppTasks().get(0)).getTaskInfo() == null || 
      (((ActivityManager.AppTask)activityManager.getAppTasks().get(0)).getTaskInfo()).topActivity == null)
      return ""; 
    String str = (((ActivityManager.AppTask)activityManager.getAppTasks().get(0)).getTaskInfo()).topActivity.getClassName();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("current activityName: ");
    stringBuilder.append(str);
    Log.i("OplusCameraUtils", stringBuilder.toString());
    return str;
  }
  
  public static String getComponentName() {
    String str1 = ActivityThread.currentOpPackageName();
    String str2 = "";
    try {
      String str = getActivityName();
    } catch (RuntimeException runtimeException) {
      runtimeException.printStackTrace();
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str1);
    stringBuilder.append('/');
    stringBuilder.append(str2);
    String str3 = stringBuilder.toString();
    stringBuilder = new StringBuilder();
    stringBuilder.append("getComponentName, componentName: ");
    stringBuilder.append(str3);
    stringBuilder.append(", packageName:");
    stringBuilder.append(str1);
    stringBuilder.append(", activityName:");
    stringBuilder.append(str2);
    Log.i("OplusCameraUtils", stringBuilder.toString());
    return str3;
  }
}
