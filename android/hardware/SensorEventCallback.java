package android.hardware;

public abstract class SensorEventCallback implements SensorEventListener2 {
  public void onSensorChanged(SensorEvent paramSensorEvent) {}
  
  public void onAccuracyChanged(Sensor paramSensor, int paramInt) {}
  
  public void onFlushCompleted(Sensor paramSensor) {}
  
  public void onSensorAdditionalInfo(SensorAdditionalInfo paramSensorAdditionalInfo) {}
}
