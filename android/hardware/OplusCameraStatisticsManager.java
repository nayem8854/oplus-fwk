package android.hardware;

import android.app.ActivityThread;
import android.graphics.Rect;
import android.util.Log;
import java.util.HashMap;
import java.util.List;
import oplus.util.OplusStatistics;

public final class OplusCameraStatisticsManager {
  private static OplusCameraStatisticsManager sInstance = new OplusCameraStatisticsManager();
  
  private long mConnectTime = 0L;
  
  private long mDisconnectTime = 0L;
  
  private int mCameraId = 0;
  
  private int mCurFaceCount = -1;
  
  private static final String TAG = "OplusCameraStatisticsManager";
  
  public static OplusCameraStatisticsManager getInstance() {
    // Byte code:
    //   0: ldc android/hardware/OplusCameraStatisticsManager
    //   2: monitorenter
    //   3: getstatic android/hardware/OplusCameraStatisticsManager.sInstance : Landroid/hardware/OplusCameraStatisticsManager;
    //   6: astore_0
    //   7: ldc android/hardware/OplusCameraStatisticsManager
    //   9: monitorexit
    //   10: aload_0
    //   11: areturn
    //   12: astore_0
    //   13: ldc android/hardware/OplusCameraStatisticsManager
    //   15: monitorexit
    //   16: aload_0
    //   17: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #41	-> 3
    //   #41	-> 12
    // Exception table:
    //   from	to	target	type
    //   3	7	12	finally
  }
  
  public void setConnectTime(long paramLong) {
    this.mConnectTime = paramLong;
  }
  
  public void setDisconnectTime(long paramLong) {
    this.mDisconnectTime = paramLong;
  }
  
  public void setCameraId(int paramInt) {
    this.mCameraId = paramInt;
  }
  
  public int getCameraId() {
    return this.mCameraId;
  }
  
  public void setCurFaceCount(int paramInt) {
    this.mCurFaceCount = paramInt;
  }
  
  public void addInfo(int paramInt, long paramLong) {
    if (this.mConnectTime > 1L) {
      this.mCameraId = paramInt;
      HashMap<Object, Object> hashMap = new HashMap<>();
      hashMap.put("pkgName", ActivityThread.currentOpPackageName());
      hashMap.put("cameraId", String.valueOf(paramInt));
      hashMap.put("apLevel", "1");
      hashMap.put("halLevel", "1");
      hashMap.put("connentTime", String.valueOf(this.mConnectTime));
      if (paramLong > 1L) {
        hashMap.put("disconnectTime", String.valueOf(paramLong));
        hashMap.put("timeCost", String.valueOf(paramLong - this.mConnectTime));
        this.mConnectTime = 0L;
      } 
      OplusStatistics.onCommon(ActivityThread.currentApplication().getApplicationContext(), "2012002", "openCamera", hashMap, false);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("addInfo, eventMap: ");
      stringBuilder.append(hashMap.toString());
      Log.d("OplusCameraStatisticsManager", stringBuilder.toString());
    } 
  }
  
  public void addPreviewInfo(Camera.Parameters paramParameters) {
    if (paramParameters == null || this.mConnectTime < 1L)
      return; 
    try {
      HashMap<Object, Object> hashMap = new HashMap<>();
      this();
      Camera.Size size = paramParameters.getPreviewSize();
      List<Camera.Area> list = paramParameters.getFocusAreas();
      long l = System.currentTimeMillis();
      hashMap.put("pkgName", ActivityThread.currentOpPackageName());
      hashMap.put("camera_id", String.valueOf(this.mCameraId));
      hashMap.put("apLevel", "1");
      hashMap.put("halLevel", "1");
      if (l > 1L)
        hashMap.put("preview_time", String.valueOf(l - this.mConnectTime)); 
      if (size != null) {
        hashMap.put("width", String.valueOf(size.width));
        hashMap.put("height", String.valueOf(size.height));
      } 
      if (list != null && list.size() > 0) {
        Rect rect = ((Camera.Area)list.get(0)).rect;
        if (rect != null) {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append(String.valueOf((rect.left + rect.right) / 2));
          stringBuilder1.append(",");
          int i = (rect.top + rect.bottom) / 2;
          stringBuilder1.append(String.valueOf(i));
          String str = stringBuilder1.toString();
          hashMap.put("touchxy_value", str);
        } 
      } 
      hashMap.put("face_count", String.valueOf(this.mCurFaceCount));
      OplusStatistics.onCommon(ActivityThread.currentApplication().getApplicationContext(), "2012002", "preview", hashMap, false);
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("addPreviewInfo, eventMap: ");
      stringBuilder.append(hashMap.toString());
      Log.d("OplusCameraStatisticsManager", stringBuilder.toString());
    } catch (Exception exception) {
      Log.e("OplusCameraStatisticsManager", "failure in addPreviewInfo");
    } 
  }
  
  public void addCaptureInfo(Camera.Parameters paramParameters) {
    if (paramParameters == null)
      return; 
    try {
      HashMap<Object, Object> hashMap = new HashMap<>();
      this();
      Camera.Size size = paramParameters.getPictureSize();
      int i = paramParameters.getZoom();
      String str = paramParameters.getFlashMode();
      List<Camera.Area> list = paramParameters.getFocusAreas();
      hashMap.put("pkgName", ActivityThread.currentOpPackageName());
      hashMap.put("camera_id", String.valueOf(this.mCameraId));
      hashMap.put("apLevel", "1");
      hashMap.put("halLevel", "1");
      int j = this.mCameraId;
      if (j == 0) {
        hashMap.put("rear_front", "rear");
      } else {
        hashMap.put("rear_front", "front");
      } 
      if (size != null) {
        hashMap.put("width", String.valueOf(size.width));
        hashMap.put("height", String.valueOf(size.height));
      } 
      hashMap.put("zoom", String.valueOf(i));
      hashMap.put("iso_value", paramParameters.get("iso"));
      hashMap.put("exp_value", paramParameters.get("exposure-time"));
      if (str != null) {
        boolean bool = str.equals("off");
        if (bool) {
          hashMap.put("flash_trigger", "0");
        } else if (str.equals("on")) {
          hashMap.put("flash_trigger", "1");
        } else if (str.equals("torch")) {
          hashMap.put("flash_trigger", "2");
        } 
      } 
      if (list != null && list.size() > 0) {
        Rect rect = ((Camera.Area)list.get(0)).rect;
        if (rect != null) {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append(String.valueOf((rect.left + rect.right) / 2));
          stringBuilder1.append(",");
          i = (rect.top + rect.bottom) / 2;
          stringBuilder1.append(String.valueOf(i));
          String str1 = stringBuilder1.toString();
          hashMap.put("touchxy_value", str1);
        } 
      } 
      hashMap.put("face_count", String.valueOf(this.mCurFaceCount));
      OplusStatistics.onCommon(ActivityThread.currentApplication().getApplicationContext(), "2012002", "photograph", hashMap, false);
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("addCaptureInfo, eventMap: ");
      stringBuilder.append(hashMap.toString());
      Log.d("OplusCameraStatisticsManager", stringBuilder.toString());
    } catch (Exception exception) {
      Log.e("OplusCameraStatisticsManager", "failure in addCaptureInfo");
    } 
  }
}
