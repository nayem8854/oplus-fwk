package android.hardware.hdmi;

import android.annotation.SystemApi;
import android.os.RemoteException;
import android.util.Log;

@SystemApi
public final class HdmiPlaybackClient extends HdmiClient {
  private static final int ADDR_TV = 0;
  
  private static final String TAG = "HdmiPlaybackClient";
  
  HdmiPlaybackClient(IHdmiControlService paramIHdmiControlService) {
    super(paramIHdmiControlService);
  }
  
  public void oneTouchPlay(OneTouchPlayCallback paramOneTouchPlayCallback) {
    try {
      this.mService.oneTouchPlay(getCallbackWrapper(paramOneTouchPlayCallback));
    } catch (RemoteException remoteException) {
      Log.e("HdmiPlaybackClient", "oneTouchPlay threw exception ", (Throwable)remoteException);
    } 
  }
  
  public int getDeviceType() {
    return 4;
  }
  
  public void queryDisplayStatus(DisplayStatusCallback paramDisplayStatusCallback) {
    try {
      this.mService.queryDisplayStatus(getCallbackWrapper(paramDisplayStatusCallback));
    } catch (RemoteException remoteException) {
      Log.e("HdmiPlaybackClient", "queryDisplayStatus threw exception ", (Throwable)remoteException);
    } 
  }
  
  public void sendStandby() {
    try {
      this.mService.sendStandby(getDeviceType(), HdmiDeviceInfo.idForCecDevice(0));
    } catch (RemoteException remoteException) {
      Log.e("HdmiPlaybackClient", "sendStandby threw exception ", (Throwable)remoteException);
    } 
  }
  
  private IHdmiControlCallback getCallbackWrapper(OneTouchPlayCallback paramOneTouchPlayCallback) {
    return (IHdmiControlCallback)new Object(this, paramOneTouchPlayCallback);
  }
  
  private IHdmiControlCallback getCallbackWrapper(DisplayStatusCallback paramDisplayStatusCallback) {
    return (IHdmiControlCallback)new Object(this, paramDisplayStatusCallback);
  }
  
  class DisplayStatusCallback {
    public abstract void onComplete(int param1Int);
  }
  
  class OneTouchPlayCallback {
    public abstract void onComplete(int param1Int);
  }
}
