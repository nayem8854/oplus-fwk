package android.hardware.hdmi;

import android.annotation.SystemApi;
import android.os.RemoteException;
import android.util.Log;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executor;

@SystemApi
public class HdmiSwitchClient extends HdmiClient {
  private static final String TAG = "HdmiSwitchClient";
  
  HdmiSwitchClient(IHdmiControlService paramIHdmiControlService) {
    super(paramIHdmiControlService);
  }
  
  private static IHdmiControlCallback getCallbackWrapper(OnSelectListener paramOnSelectListener) {
    return (IHdmiControlCallback)new Object(paramOnSelectListener);
  }
  
  public int getDeviceType() {
    return 6;
  }
  
  public void selectDevice(int paramInt, OnSelectListener paramOnSelectListener) {
    Objects.requireNonNull(paramOnSelectListener);
    try {
      this.mService.deviceSelect(paramInt, getCallbackWrapper(paramOnSelectListener));
      return;
    } catch (RemoteException remoteException) {
      Log.e("HdmiSwitchClient", "failed to select device: ", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void selectPort(int paramInt, OnSelectListener paramOnSelectListener) {
    Objects.requireNonNull(paramOnSelectListener);
    try {
      this.mService.portSelect(paramInt, getCallbackWrapper(paramOnSelectListener));
      return;
    } catch (RemoteException remoteException) {
      Log.e("HdmiSwitchClient", "failed to select port: ", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void selectDevice(int paramInt, Executor paramExecutor, OnSelectListener paramOnSelectListener) {
    Objects.requireNonNull(paramOnSelectListener);
    try {
      IHdmiControlService iHdmiControlService = this.mService;
      Object object = new Object();
      super(this, paramExecutor, paramOnSelectListener);
      iHdmiControlService.deviceSelect(paramInt, (IHdmiControlCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("HdmiSwitchClient", "failed to select device: ", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void selectPort(int paramInt, Executor paramExecutor, OnSelectListener paramOnSelectListener) {
    Objects.requireNonNull(paramOnSelectListener);
    try {
      IHdmiControlService iHdmiControlService = this.mService;
      Object object = new Object();
      super(this, paramExecutor, paramOnSelectListener);
      iHdmiControlService.portSelect(paramInt, (IHdmiControlCallback)object);
      return;
    } catch (RemoteException remoteException) {
      Log.e("HdmiSwitchClient", "failed to select port: ", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<HdmiDeviceInfo> getDeviceList() {
    try {
      return this.mService.getDeviceList();
    } catch (RemoteException remoteException) {
      Log.e("TAG", "Failed to call getDeviceList():", (Throwable)remoteException);
      return Collections.emptyList();
    } 
  }
  
  public List<HdmiPortInfo> getPortInfo() {
    try {
      return this.mService.getPortInfo();
    } catch (RemoteException remoteException) {
      Log.e("TAG", "Failed to call getPortInfo():", (Throwable)remoteException);
      return Collections.emptyList();
    } 
  }
  
  @SystemApi
  class OnSelectListener {
    public abstract void onSelect(int param1Int);
  }
}
