package android.hardware.hdmi;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IHdmiMhlVendorCommandListener extends IInterface {
  void onReceived(int paramInt1, int paramInt2, int paramInt3, byte[] paramArrayOfbyte) throws RemoteException;
  
  class Default implements IHdmiMhlVendorCommandListener {
    public void onReceived(int param1Int1, int param1Int2, int param1Int3, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IHdmiMhlVendorCommandListener {
    private static final String DESCRIPTOR = "android.hardware.hdmi.IHdmiMhlVendorCommandListener";
    
    static final int TRANSACTION_onReceived = 1;
    
    public Stub() {
      attachInterface(this, "android.hardware.hdmi.IHdmiMhlVendorCommandListener");
    }
    
    public static IHdmiMhlVendorCommandListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.hdmi.IHdmiMhlVendorCommandListener");
      if (iInterface != null && iInterface instanceof IHdmiMhlVendorCommandListener)
        return (IHdmiMhlVendorCommandListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onReceived";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.hardware.hdmi.IHdmiMhlVendorCommandListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.hardware.hdmi.IHdmiMhlVendorCommandListener");
      param1Int1 = param1Parcel1.readInt();
      int i = param1Parcel1.readInt();
      param1Int2 = param1Parcel1.readInt();
      byte[] arrayOfByte = param1Parcel1.createByteArray();
      onReceived(param1Int1, i, param1Int2, arrayOfByte);
      return true;
    }
    
    private static class Proxy implements IHdmiMhlVendorCommandListener {
      public static IHdmiMhlVendorCommandListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.hdmi.IHdmiMhlVendorCommandListener";
      }
      
      public void onReceived(int param2Int1, int param2Int2, int param2Int3, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.hdmi.IHdmiMhlVendorCommandListener");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          parcel.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IHdmiMhlVendorCommandListener.Stub.getDefaultImpl() != null) {
            IHdmiMhlVendorCommandListener.Stub.getDefaultImpl().onReceived(param2Int1, param2Int2, param2Int3, param2ArrayOfbyte);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IHdmiMhlVendorCommandListener param1IHdmiMhlVendorCommandListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IHdmiMhlVendorCommandListener != null) {
          Proxy.sDefaultImpl = param1IHdmiMhlVendorCommandListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IHdmiMhlVendorCommandListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
