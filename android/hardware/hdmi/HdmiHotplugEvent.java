package android.hardware.hdmi;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class HdmiHotplugEvent implements Parcelable {
  public HdmiHotplugEvent(int paramInt, boolean paramBoolean) {
    this.mPort = paramInt;
    this.mConnected = paramBoolean;
  }
  
  public int getPort() {
    return this.mPort;
  }
  
  public boolean isConnected() {
    return this.mConnected;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mPort);
    paramParcel.writeByte((byte)this.mConnected);
  }
  
  public static final Parcelable.Creator<HdmiHotplugEvent> CREATOR = new Parcelable.Creator<HdmiHotplugEvent>() {
      public HdmiHotplugEvent createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        byte b = param1Parcel.readByte();
        boolean bool = true;
        if (b != 1)
          bool = false; 
        return new HdmiHotplugEvent(i, bool);
      }
      
      public HdmiHotplugEvent[] newArray(int param1Int) {
        return new HdmiHotplugEvent[param1Int];
      }
    };
  
  private final boolean mConnected;
  
  private final int mPort;
}
