package android.hardware.hdmi;

import java.util.List;

public final class HdmiControlServiceWrapper {
  private List<HdmiPortInfo> mInfoList = null;
  
  private int[] mTypes = null;
  
  public HdmiControlManager createHdmiControlManager() {
    return new HdmiControlManager(this.mInterface);
  }
  
  private final IHdmiControlService mInterface = (IHdmiControlService)new Object(this);
  
  public static final int DEVICE_PURE_CEC_SWITCH = 6;
  
  public void setPortInfo(List<HdmiPortInfo> paramList) {
    this.mInfoList = paramList;
  }
  
  public void setDeviceTypes(int[] paramArrayOfint) {
    this.mTypes = paramArrayOfint;
  }
  
  public List<HdmiPortInfo> getPortInfo() {
    return this.mInfoList;
  }
  
  public int[] getSupportedTypes() {
    return this.mTypes;
  }
  
  public HdmiDeviceInfo getActiveSource() {
    return null;
  }
  
  public void oneTouchPlay(IHdmiControlCallback paramIHdmiControlCallback) {}
  
  public void queryDisplayStatus(IHdmiControlCallback paramIHdmiControlCallback) {}
  
  public void addHdmiControlStatusChangeListener(IHdmiControlStatusChangeListener paramIHdmiControlStatusChangeListener) {}
  
  public void removeHdmiControlStatusChangeListener(IHdmiControlStatusChangeListener paramIHdmiControlStatusChangeListener) {}
  
  public void addHotplugEventListener(IHdmiHotplugEventListener paramIHdmiHotplugEventListener) {}
  
  public void removeHotplugEventListener(IHdmiHotplugEventListener paramIHdmiHotplugEventListener) {}
  
  public void addDeviceEventListener(IHdmiDeviceEventListener paramIHdmiDeviceEventListener) {}
  
  public void deviceSelect(int paramInt, IHdmiControlCallback paramIHdmiControlCallback) {}
  
  public void portSelect(int paramInt, IHdmiControlCallback paramIHdmiControlCallback) {}
  
  public void sendKeyEvent(int paramInt1, int paramInt2, boolean paramBoolean) {}
  
  public void sendVolumeKeyEvent(int paramInt1, int paramInt2, boolean paramBoolean) {}
  
  public boolean canChangeSystemAudioMode() {
    return true;
  }
  
  public boolean getSystemAudioMode() {
    return true;
  }
  
  public int getPhysicalAddress() {
    return 65535;
  }
  
  public void setSystemAudioMode(boolean paramBoolean, IHdmiControlCallback paramIHdmiControlCallback) {}
  
  public void addSystemAudioModeChangeListener(IHdmiSystemAudioModeChangeListener paramIHdmiSystemAudioModeChangeListener) {}
  
  public void removeSystemAudioModeChangeListener(IHdmiSystemAudioModeChangeListener paramIHdmiSystemAudioModeChangeListener) {}
  
  public void setArcMode(boolean paramBoolean) {}
  
  public void setProhibitMode(boolean paramBoolean) {}
  
  public void setSystemAudioVolume(int paramInt1, int paramInt2, int paramInt3) {}
  
  public void setSystemAudioMute(boolean paramBoolean) {}
  
  public void setInputChangeListener(IHdmiInputChangeListener paramIHdmiInputChangeListener) {}
  
  public List<HdmiDeviceInfo> getInputDevices() {
    return null;
  }
  
  public List<HdmiDeviceInfo> getDeviceList() {
    return null;
  }
  
  public void powerOffRemoteDevice(int paramInt1, int paramInt2) {}
  
  public void powerOnRemoteDevice(int paramInt1, int paramInt2) {}
  
  public void askRemoteDeviceToBecomeActiveSource(int paramInt) {}
  
  public void sendVendorCommand(int paramInt1, int paramInt2, byte[] paramArrayOfbyte, boolean paramBoolean) {}
  
  public void addVendorCommandListener(IHdmiVendorCommandListener paramIHdmiVendorCommandListener, int paramInt) {}
  
  public void sendStandby(int paramInt1, int paramInt2) {}
  
  public void setHdmiRecordListener(IHdmiRecordListener paramIHdmiRecordListener) {}
  
  public void startOneTouchRecord(int paramInt, byte[] paramArrayOfbyte) {}
  
  public void stopOneTouchRecord(int paramInt) {}
  
  public void startTimerRecording(int paramInt1, int paramInt2, byte[] paramArrayOfbyte) {}
  
  public void clearTimerRecording(int paramInt1, int paramInt2, byte[] paramArrayOfbyte) {}
  
  public void sendMhlVendorCommand(int paramInt1, int paramInt2, int paramInt3, byte[] paramArrayOfbyte) {}
  
  public void addHdmiMhlVendorCommandListener(IHdmiMhlVendorCommandListener paramIHdmiMhlVendorCommandListener) {}
  
  public void setStandbyMode(boolean paramBoolean) {}
  
  public void setHdmiCecVolumeControlEnabled(boolean paramBoolean) {}
  
  public boolean isHdmiCecVolumeControlEnabled() {
    return true;
  }
  
  public void reportAudioStatus(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) {}
  
  public void setSystemAudioModeOnForAudioOnlySource() {}
  
  public void addHdmiCecVolumeControlFeatureListener(IHdmiCecVolumeControlFeatureListener paramIHdmiCecVolumeControlFeatureListener) {}
  
  public void removeHdmiCecVolumeControlFeatureListener(IHdmiCecVolumeControlFeatureListener paramIHdmiCecVolumeControlFeatureListener) {}
}
