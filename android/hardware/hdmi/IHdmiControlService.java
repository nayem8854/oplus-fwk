package android.hardware.hdmi;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IHdmiControlService extends IInterface {
  void addDeviceEventListener(IHdmiDeviceEventListener paramIHdmiDeviceEventListener) throws RemoteException;
  
  void addHdmiCecVolumeControlFeatureListener(IHdmiCecVolumeControlFeatureListener paramIHdmiCecVolumeControlFeatureListener) throws RemoteException;
  
  void addHdmiControlStatusChangeListener(IHdmiControlStatusChangeListener paramIHdmiControlStatusChangeListener) throws RemoteException;
  
  void addHdmiMhlVendorCommandListener(IHdmiMhlVendorCommandListener paramIHdmiMhlVendorCommandListener) throws RemoteException;
  
  void addHotplugEventListener(IHdmiHotplugEventListener paramIHdmiHotplugEventListener) throws RemoteException;
  
  void addSystemAudioModeChangeListener(IHdmiSystemAudioModeChangeListener paramIHdmiSystemAudioModeChangeListener) throws RemoteException;
  
  void addVendorCommandListener(IHdmiVendorCommandListener paramIHdmiVendorCommandListener, int paramInt) throws RemoteException;
  
  void askRemoteDeviceToBecomeActiveSource(int paramInt) throws RemoteException;
  
  boolean canChangeSystemAudioMode() throws RemoteException;
  
  void clearTimerRecording(int paramInt1, int paramInt2, byte[] paramArrayOfbyte) throws RemoteException;
  
  void deviceSelect(int paramInt, IHdmiControlCallback paramIHdmiControlCallback) throws RemoteException;
  
  HdmiDeviceInfo getActiveSource() throws RemoteException;
  
  List<HdmiDeviceInfo> getDeviceList() throws RemoteException;
  
  List<HdmiDeviceInfo> getInputDevices() throws RemoteException;
  
  int getPhysicalAddress() throws RemoteException;
  
  List<HdmiPortInfo> getPortInfo() throws RemoteException;
  
  int[] getSupportedTypes() throws RemoteException;
  
  boolean getSystemAudioMode() throws RemoteException;
  
  boolean isHdmiCecVolumeControlEnabled() throws RemoteException;
  
  void oneTouchPlay(IHdmiControlCallback paramIHdmiControlCallback) throws RemoteException;
  
  void portSelect(int paramInt, IHdmiControlCallback paramIHdmiControlCallback) throws RemoteException;
  
  void powerOffRemoteDevice(int paramInt1, int paramInt2) throws RemoteException;
  
  void powerOnRemoteDevice(int paramInt1, int paramInt2) throws RemoteException;
  
  void queryDisplayStatus(IHdmiControlCallback paramIHdmiControlCallback) throws RemoteException;
  
  void removeHdmiCecVolumeControlFeatureListener(IHdmiCecVolumeControlFeatureListener paramIHdmiCecVolumeControlFeatureListener) throws RemoteException;
  
  void removeHdmiControlStatusChangeListener(IHdmiControlStatusChangeListener paramIHdmiControlStatusChangeListener) throws RemoteException;
  
  void removeHotplugEventListener(IHdmiHotplugEventListener paramIHdmiHotplugEventListener) throws RemoteException;
  
  void removeSystemAudioModeChangeListener(IHdmiSystemAudioModeChangeListener paramIHdmiSystemAudioModeChangeListener) throws RemoteException;
  
  void reportAudioStatus(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) throws RemoteException;
  
  void sendKeyEvent(int paramInt1, int paramInt2, boolean paramBoolean) throws RemoteException;
  
  void sendMhlVendorCommand(int paramInt1, int paramInt2, int paramInt3, byte[] paramArrayOfbyte) throws RemoteException;
  
  void sendStandby(int paramInt1, int paramInt2) throws RemoteException;
  
  void sendVendorCommand(int paramInt1, int paramInt2, byte[] paramArrayOfbyte, boolean paramBoolean) throws RemoteException;
  
  void sendVolumeKeyEvent(int paramInt1, int paramInt2, boolean paramBoolean) throws RemoteException;
  
  void setArcMode(boolean paramBoolean) throws RemoteException;
  
  void setHdmiCecVolumeControlEnabled(boolean paramBoolean) throws RemoteException;
  
  void setHdmiRecordListener(IHdmiRecordListener paramIHdmiRecordListener) throws RemoteException;
  
  void setInputChangeListener(IHdmiInputChangeListener paramIHdmiInputChangeListener) throws RemoteException;
  
  void setProhibitMode(boolean paramBoolean) throws RemoteException;
  
  void setStandbyMode(boolean paramBoolean) throws RemoteException;
  
  void setSystemAudioMode(boolean paramBoolean, IHdmiControlCallback paramIHdmiControlCallback) throws RemoteException;
  
  void setSystemAudioModeOnForAudioOnlySource() throws RemoteException;
  
  void setSystemAudioMute(boolean paramBoolean) throws RemoteException;
  
  void setSystemAudioVolume(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void startOneTouchRecord(int paramInt, byte[] paramArrayOfbyte) throws RemoteException;
  
  void startTimerRecording(int paramInt1, int paramInt2, byte[] paramArrayOfbyte) throws RemoteException;
  
  void stopOneTouchRecord(int paramInt) throws RemoteException;
  
  class Default implements IHdmiControlService {
    public int[] getSupportedTypes() throws RemoteException {
      return null;
    }
    
    public HdmiDeviceInfo getActiveSource() throws RemoteException {
      return null;
    }
    
    public void oneTouchPlay(IHdmiControlCallback param1IHdmiControlCallback) throws RemoteException {}
    
    public void queryDisplayStatus(IHdmiControlCallback param1IHdmiControlCallback) throws RemoteException {}
    
    public void addHdmiControlStatusChangeListener(IHdmiControlStatusChangeListener param1IHdmiControlStatusChangeListener) throws RemoteException {}
    
    public void removeHdmiControlStatusChangeListener(IHdmiControlStatusChangeListener param1IHdmiControlStatusChangeListener) throws RemoteException {}
    
    public void addHdmiCecVolumeControlFeatureListener(IHdmiCecVolumeControlFeatureListener param1IHdmiCecVolumeControlFeatureListener) throws RemoteException {}
    
    public void removeHdmiCecVolumeControlFeatureListener(IHdmiCecVolumeControlFeatureListener param1IHdmiCecVolumeControlFeatureListener) throws RemoteException {}
    
    public void addHotplugEventListener(IHdmiHotplugEventListener param1IHdmiHotplugEventListener) throws RemoteException {}
    
    public void removeHotplugEventListener(IHdmiHotplugEventListener param1IHdmiHotplugEventListener) throws RemoteException {}
    
    public void addDeviceEventListener(IHdmiDeviceEventListener param1IHdmiDeviceEventListener) throws RemoteException {}
    
    public void deviceSelect(int param1Int, IHdmiControlCallback param1IHdmiControlCallback) throws RemoteException {}
    
    public void portSelect(int param1Int, IHdmiControlCallback param1IHdmiControlCallback) throws RemoteException {}
    
    public void sendKeyEvent(int param1Int1, int param1Int2, boolean param1Boolean) throws RemoteException {}
    
    public void sendVolumeKeyEvent(int param1Int1, int param1Int2, boolean param1Boolean) throws RemoteException {}
    
    public List<HdmiPortInfo> getPortInfo() throws RemoteException {
      return null;
    }
    
    public boolean canChangeSystemAudioMode() throws RemoteException {
      return false;
    }
    
    public boolean getSystemAudioMode() throws RemoteException {
      return false;
    }
    
    public int getPhysicalAddress() throws RemoteException {
      return 0;
    }
    
    public void setSystemAudioMode(boolean param1Boolean, IHdmiControlCallback param1IHdmiControlCallback) throws RemoteException {}
    
    public void addSystemAudioModeChangeListener(IHdmiSystemAudioModeChangeListener param1IHdmiSystemAudioModeChangeListener) throws RemoteException {}
    
    public void removeSystemAudioModeChangeListener(IHdmiSystemAudioModeChangeListener param1IHdmiSystemAudioModeChangeListener) throws RemoteException {}
    
    public void setArcMode(boolean param1Boolean) throws RemoteException {}
    
    public void setProhibitMode(boolean param1Boolean) throws RemoteException {}
    
    public void setSystemAudioVolume(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void setSystemAudioMute(boolean param1Boolean) throws RemoteException {}
    
    public void setInputChangeListener(IHdmiInputChangeListener param1IHdmiInputChangeListener) throws RemoteException {}
    
    public List<HdmiDeviceInfo> getInputDevices() throws RemoteException {
      return null;
    }
    
    public List<HdmiDeviceInfo> getDeviceList() throws RemoteException {
      return null;
    }
    
    public void powerOffRemoteDevice(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void powerOnRemoteDevice(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void askRemoteDeviceToBecomeActiveSource(int param1Int) throws RemoteException {}
    
    public void sendVendorCommand(int param1Int1, int param1Int2, byte[] param1ArrayOfbyte, boolean param1Boolean) throws RemoteException {}
    
    public void addVendorCommandListener(IHdmiVendorCommandListener param1IHdmiVendorCommandListener, int param1Int) throws RemoteException {}
    
    public void sendStandby(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void setHdmiRecordListener(IHdmiRecordListener param1IHdmiRecordListener) throws RemoteException {}
    
    public void startOneTouchRecord(int param1Int, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void stopOneTouchRecord(int param1Int) throws RemoteException {}
    
    public void startTimerRecording(int param1Int1, int param1Int2, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void clearTimerRecording(int param1Int1, int param1Int2, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void sendMhlVendorCommand(int param1Int1, int param1Int2, int param1Int3, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void addHdmiMhlVendorCommandListener(IHdmiMhlVendorCommandListener param1IHdmiMhlVendorCommandListener) throws RemoteException {}
    
    public void setStandbyMode(boolean param1Boolean) throws RemoteException {}
    
    public void setHdmiCecVolumeControlEnabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isHdmiCecVolumeControlEnabled() throws RemoteException {
      return false;
    }
    
    public void reportAudioStatus(int param1Int1, int param1Int2, int param1Int3, boolean param1Boolean) throws RemoteException {}
    
    public void setSystemAudioModeOnForAudioOnlySource() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IHdmiControlService {
    private static final String DESCRIPTOR = "android.hardware.hdmi.IHdmiControlService";
    
    static final int TRANSACTION_addDeviceEventListener = 11;
    
    static final int TRANSACTION_addHdmiCecVolumeControlFeatureListener = 7;
    
    static final int TRANSACTION_addHdmiControlStatusChangeListener = 5;
    
    static final int TRANSACTION_addHdmiMhlVendorCommandListener = 42;
    
    static final int TRANSACTION_addHotplugEventListener = 9;
    
    static final int TRANSACTION_addSystemAudioModeChangeListener = 21;
    
    static final int TRANSACTION_addVendorCommandListener = 34;
    
    static final int TRANSACTION_askRemoteDeviceToBecomeActiveSource = 32;
    
    static final int TRANSACTION_canChangeSystemAudioMode = 17;
    
    static final int TRANSACTION_clearTimerRecording = 40;
    
    static final int TRANSACTION_deviceSelect = 12;
    
    static final int TRANSACTION_getActiveSource = 2;
    
    static final int TRANSACTION_getDeviceList = 29;
    
    static final int TRANSACTION_getInputDevices = 28;
    
    static final int TRANSACTION_getPhysicalAddress = 19;
    
    static final int TRANSACTION_getPortInfo = 16;
    
    static final int TRANSACTION_getSupportedTypes = 1;
    
    static final int TRANSACTION_getSystemAudioMode = 18;
    
    static final int TRANSACTION_isHdmiCecVolumeControlEnabled = 45;
    
    static final int TRANSACTION_oneTouchPlay = 3;
    
    static final int TRANSACTION_portSelect = 13;
    
    static final int TRANSACTION_powerOffRemoteDevice = 30;
    
    static final int TRANSACTION_powerOnRemoteDevice = 31;
    
    static final int TRANSACTION_queryDisplayStatus = 4;
    
    static final int TRANSACTION_removeHdmiCecVolumeControlFeatureListener = 8;
    
    static final int TRANSACTION_removeHdmiControlStatusChangeListener = 6;
    
    static final int TRANSACTION_removeHotplugEventListener = 10;
    
    static final int TRANSACTION_removeSystemAudioModeChangeListener = 22;
    
    static final int TRANSACTION_reportAudioStatus = 46;
    
    static final int TRANSACTION_sendKeyEvent = 14;
    
    static final int TRANSACTION_sendMhlVendorCommand = 41;
    
    static final int TRANSACTION_sendStandby = 35;
    
    static final int TRANSACTION_sendVendorCommand = 33;
    
    static final int TRANSACTION_sendVolumeKeyEvent = 15;
    
    static final int TRANSACTION_setArcMode = 23;
    
    static final int TRANSACTION_setHdmiCecVolumeControlEnabled = 44;
    
    static final int TRANSACTION_setHdmiRecordListener = 36;
    
    static final int TRANSACTION_setInputChangeListener = 27;
    
    static final int TRANSACTION_setProhibitMode = 24;
    
    static final int TRANSACTION_setStandbyMode = 43;
    
    static final int TRANSACTION_setSystemAudioMode = 20;
    
    static final int TRANSACTION_setSystemAudioModeOnForAudioOnlySource = 47;
    
    static final int TRANSACTION_setSystemAudioMute = 26;
    
    static final int TRANSACTION_setSystemAudioVolume = 25;
    
    static final int TRANSACTION_startOneTouchRecord = 37;
    
    static final int TRANSACTION_startTimerRecording = 39;
    
    static final int TRANSACTION_stopOneTouchRecord = 38;
    
    public Stub() {
      attachInterface(this, "android.hardware.hdmi.IHdmiControlService");
    }
    
    public static IHdmiControlService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.hdmi.IHdmiControlService");
      if (iInterface != null && iInterface instanceof IHdmiControlService)
        return (IHdmiControlService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 47:
          return "setSystemAudioModeOnForAudioOnlySource";
        case 46:
          return "reportAudioStatus";
        case 45:
          return "isHdmiCecVolumeControlEnabled";
        case 44:
          return "setHdmiCecVolumeControlEnabled";
        case 43:
          return "setStandbyMode";
        case 42:
          return "addHdmiMhlVendorCommandListener";
        case 41:
          return "sendMhlVendorCommand";
        case 40:
          return "clearTimerRecording";
        case 39:
          return "startTimerRecording";
        case 38:
          return "stopOneTouchRecord";
        case 37:
          return "startOneTouchRecord";
        case 36:
          return "setHdmiRecordListener";
        case 35:
          return "sendStandby";
        case 34:
          return "addVendorCommandListener";
        case 33:
          return "sendVendorCommand";
        case 32:
          return "askRemoteDeviceToBecomeActiveSource";
        case 31:
          return "powerOnRemoteDevice";
        case 30:
          return "powerOffRemoteDevice";
        case 29:
          return "getDeviceList";
        case 28:
          return "getInputDevices";
        case 27:
          return "setInputChangeListener";
        case 26:
          return "setSystemAudioMute";
        case 25:
          return "setSystemAudioVolume";
        case 24:
          return "setProhibitMode";
        case 23:
          return "setArcMode";
        case 22:
          return "removeSystemAudioModeChangeListener";
        case 21:
          return "addSystemAudioModeChangeListener";
        case 20:
          return "setSystemAudioMode";
        case 19:
          return "getPhysicalAddress";
        case 18:
          return "getSystemAudioMode";
        case 17:
          return "canChangeSystemAudioMode";
        case 16:
          return "getPortInfo";
        case 15:
          return "sendVolumeKeyEvent";
        case 14:
          return "sendKeyEvent";
        case 13:
          return "portSelect";
        case 12:
          return "deviceSelect";
        case 11:
          return "addDeviceEventListener";
        case 10:
          return "removeHotplugEventListener";
        case 9:
          return "addHotplugEventListener";
        case 8:
          return "removeHdmiCecVolumeControlFeatureListener";
        case 7:
          return "addHdmiCecVolumeControlFeatureListener";
        case 6:
          return "removeHdmiControlStatusChangeListener";
        case 5:
          return "addHdmiControlStatusChangeListener";
        case 4:
          return "queryDisplayStatus";
        case 3:
          return "oneTouchPlay";
        case 2:
          return "getActiveSource";
        case 1:
          break;
      } 
      return "getSupportedTypes";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int j;
        boolean bool1;
        int i;
        IHdmiMhlVendorCommandListener iHdmiMhlVendorCommandListener;
        byte[] arrayOfByte1;
        IHdmiRecordListener iHdmiRecordListener;
        List<HdmiDeviceInfo> list1;
        IHdmiInputChangeListener iHdmiInputChangeListener;
        IHdmiSystemAudioModeChangeListener iHdmiSystemAudioModeChangeListener;
        IHdmiControlCallback iHdmiControlCallback3;
        List<HdmiPortInfo> list;
        IHdmiControlCallback iHdmiControlCallback2;
        IHdmiDeviceEventListener iHdmiDeviceEventListener;
        IHdmiHotplugEventListener iHdmiHotplugEventListener;
        IHdmiCecVolumeControlFeatureListener iHdmiCecVolumeControlFeatureListener;
        IHdmiControlStatusChangeListener iHdmiControlStatusChangeListener;
        IHdmiControlCallback iHdmiControlCallback1;
        HdmiDeviceInfo hdmiDeviceInfo;
        int k;
        IHdmiVendorCommandListener iHdmiVendorCommandListener;
        byte[] arrayOfByte2;
        boolean bool3 = false, bool4 = false, bool5 = false, bool6 = false, bool7 = false, bool8 = false, bool9 = false, bool10 = false, bool11 = false, bool12 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 47:
            param1Parcel1.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            setSystemAudioModeOnForAudioOnlySource();
            param1Parcel2.writeNoException();
            return true;
          case 46:
            param1Parcel1.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            k = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0)
              bool12 = true; 
            reportAudioStatus(param1Int1, param1Int2, k, bool12);
            param1Parcel2.writeNoException();
            return true;
          case 45:
            param1Parcel1.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            bool2 = isHdmiCecVolumeControlEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 44:
            param1Parcel1.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            bool12 = bool3;
            if (param1Parcel1.readInt() != 0)
              bool12 = true; 
            setHdmiCecVolumeControlEnabled(bool12);
            param1Parcel2.writeNoException();
            return true;
          case 43:
            param1Parcel1.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            bool12 = bool4;
            if (param1Parcel1.readInt() != 0)
              bool12 = true; 
            setStandbyMode(bool12);
            param1Parcel2.writeNoException();
            return true;
          case 42:
            param1Parcel1.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            iHdmiMhlVendorCommandListener = IHdmiMhlVendorCommandListener.Stub.asInterface(param1Parcel1.readStrongBinder());
            addHdmiMhlVendorCommandListener(iHdmiMhlVendorCommandListener);
            param1Parcel2.writeNoException();
            return true;
          case 41:
            iHdmiMhlVendorCommandListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            param1Int2 = iHdmiMhlVendorCommandListener.readInt();
            j = iHdmiMhlVendorCommandListener.readInt();
            k = iHdmiMhlVendorCommandListener.readInt();
            arrayOfByte1 = iHdmiMhlVendorCommandListener.createByteArray();
            sendMhlVendorCommand(param1Int2, j, k, arrayOfByte1);
            param1Parcel2.writeNoException();
            return true;
          case 40:
            arrayOfByte1.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            j = arrayOfByte1.readInt();
            param1Int2 = arrayOfByte1.readInt();
            arrayOfByte1 = arrayOfByte1.createByteArray();
            clearTimerRecording(j, param1Int2, arrayOfByte1);
            param1Parcel2.writeNoException();
            return true;
          case 39:
            arrayOfByte1.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            j = arrayOfByte1.readInt();
            param1Int2 = arrayOfByte1.readInt();
            arrayOfByte1 = arrayOfByte1.createByteArray();
            startTimerRecording(j, param1Int2, arrayOfByte1);
            param1Parcel2.writeNoException();
            return true;
          case 38:
            arrayOfByte1.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            j = arrayOfByte1.readInt();
            stopOneTouchRecord(j);
            param1Parcel2.writeNoException();
            return true;
          case 37:
            arrayOfByte1.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            j = arrayOfByte1.readInt();
            arrayOfByte1 = arrayOfByte1.createByteArray();
            startOneTouchRecord(j, arrayOfByte1);
            param1Parcel2.writeNoException();
            return true;
          case 36:
            arrayOfByte1.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            iHdmiRecordListener = IHdmiRecordListener.Stub.asInterface(arrayOfByte1.readStrongBinder());
            setHdmiRecordListener(iHdmiRecordListener);
            param1Parcel2.writeNoException();
            return true;
          case 35:
            iHdmiRecordListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            j = iHdmiRecordListener.readInt();
            param1Int2 = iHdmiRecordListener.readInt();
            sendStandby(j, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 34:
            iHdmiRecordListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            iHdmiVendorCommandListener = IHdmiVendorCommandListener.Stub.asInterface(iHdmiRecordListener.readStrongBinder());
            j = iHdmiRecordListener.readInt();
            addVendorCommandListener(iHdmiVendorCommandListener, j);
            param1Parcel2.writeNoException();
            return true;
          case 33:
            iHdmiRecordListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            param1Int2 = iHdmiRecordListener.readInt();
            j = iHdmiRecordListener.readInt();
            arrayOfByte2 = iHdmiRecordListener.createByteArray();
            bool12 = bool5;
            if (iHdmiRecordListener.readInt() != 0)
              bool12 = true; 
            sendVendorCommand(param1Int2, j, arrayOfByte2, bool12);
            param1Parcel2.writeNoException();
            return true;
          case 32:
            iHdmiRecordListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            j = iHdmiRecordListener.readInt();
            askRemoteDeviceToBecomeActiveSource(j);
            param1Parcel2.writeNoException();
            return true;
          case 31:
            iHdmiRecordListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            j = iHdmiRecordListener.readInt();
            param1Int2 = iHdmiRecordListener.readInt();
            powerOnRemoteDevice(j, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 30:
            iHdmiRecordListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            j = iHdmiRecordListener.readInt();
            param1Int2 = iHdmiRecordListener.readInt();
            powerOffRemoteDevice(j, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 29:
            iHdmiRecordListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            list1 = getDeviceList();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list1);
            return true;
          case 28:
            list1.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            list1 = getInputDevices();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list1);
            return true;
          case 27:
            list1.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            iHdmiInputChangeListener = IHdmiInputChangeListener.Stub.asInterface(list1.readStrongBinder());
            setInputChangeListener(iHdmiInputChangeListener);
            param1Parcel2.writeNoException();
            return true;
          case 26:
            iHdmiInputChangeListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            bool12 = bool6;
            if (iHdmiInputChangeListener.readInt() != 0)
              bool12 = true; 
            setSystemAudioMute(bool12);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            iHdmiInputChangeListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            j = iHdmiInputChangeListener.readInt();
            k = iHdmiInputChangeListener.readInt();
            param1Int2 = iHdmiInputChangeListener.readInt();
            setSystemAudioVolume(j, k, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 24:
            iHdmiInputChangeListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            bool12 = bool7;
            if (iHdmiInputChangeListener.readInt() != 0)
              bool12 = true; 
            setProhibitMode(bool12);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            iHdmiInputChangeListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            bool12 = bool8;
            if (iHdmiInputChangeListener.readInt() != 0)
              bool12 = true; 
            setArcMode(bool12);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            iHdmiInputChangeListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            iHdmiSystemAudioModeChangeListener = IHdmiSystemAudioModeChangeListener.Stub.asInterface(iHdmiInputChangeListener.readStrongBinder());
            removeSystemAudioModeChangeListener(iHdmiSystemAudioModeChangeListener);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            iHdmiSystemAudioModeChangeListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            iHdmiSystemAudioModeChangeListener = IHdmiSystemAudioModeChangeListener.Stub.asInterface(iHdmiSystemAudioModeChangeListener.readStrongBinder());
            addSystemAudioModeChangeListener(iHdmiSystemAudioModeChangeListener);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            iHdmiSystemAudioModeChangeListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            bool12 = bool9;
            if (iHdmiSystemAudioModeChangeListener.readInt() != 0)
              bool12 = true; 
            iHdmiControlCallback3 = IHdmiControlCallback.Stub.asInterface(iHdmiSystemAudioModeChangeListener.readStrongBinder());
            setSystemAudioMode(bool12, iHdmiControlCallback3);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            iHdmiControlCallback3.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            j = getPhysicalAddress();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 18:
            iHdmiControlCallback3.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            bool1 = getSystemAudioMode();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 17:
            iHdmiControlCallback3.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            bool1 = canChangeSystemAudioMode();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 16:
            iHdmiControlCallback3.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            list = getPortInfo();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 15:
            list.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            param1Int2 = list.readInt();
            i = list.readInt();
            bool12 = bool10;
            if (list.readInt() != 0)
              bool12 = true; 
            sendVolumeKeyEvent(param1Int2, i, bool12);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            list.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            i = list.readInt();
            param1Int2 = list.readInt();
            bool12 = bool11;
            if (list.readInt() != 0)
              bool12 = true; 
            sendKeyEvent(i, param1Int2, bool12);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            list.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            i = list.readInt();
            iHdmiControlCallback2 = IHdmiControlCallback.Stub.asInterface(list.readStrongBinder());
            portSelect(i, iHdmiControlCallback2);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            iHdmiControlCallback2.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            i = iHdmiControlCallback2.readInt();
            iHdmiControlCallback2 = IHdmiControlCallback.Stub.asInterface(iHdmiControlCallback2.readStrongBinder());
            deviceSelect(i, iHdmiControlCallback2);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            iHdmiControlCallback2.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            iHdmiDeviceEventListener = IHdmiDeviceEventListener.Stub.asInterface(iHdmiControlCallback2.readStrongBinder());
            addDeviceEventListener(iHdmiDeviceEventListener);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            iHdmiDeviceEventListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            iHdmiHotplugEventListener = IHdmiHotplugEventListener.Stub.asInterface(iHdmiDeviceEventListener.readStrongBinder());
            removeHotplugEventListener(iHdmiHotplugEventListener);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            iHdmiHotplugEventListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            iHdmiHotplugEventListener = IHdmiHotplugEventListener.Stub.asInterface(iHdmiHotplugEventListener.readStrongBinder());
            addHotplugEventListener(iHdmiHotplugEventListener);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            iHdmiHotplugEventListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            iHdmiCecVolumeControlFeatureListener = IHdmiCecVolumeControlFeatureListener.Stub.asInterface(iHdmiHotplugEventListener.readStrongBinder());
            removeHdmiCecVolumeControlFeatureListener(iHdmiCecVolumeControlFeatureListener);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            iHdmiCecVolumeControlFeatureListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            iHdmiCecVolumeControlFeatureListener = IHdmiCecVolumeControlFeatureListener.Stub.asInterface(iHdmiCecVolumeControlFeatureListener.readStrongBinder());
            addHdmiCecVolumeControlFeatureListener(iHdmiCecVolumeControlFeatureListener);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            iHdmiCecVolumeControlFeatureListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            iHdmiControlStatusChangeListener = IHdmiControlStatusChangeListener.Stub.asInterface(iHdmiCecVolumeControlFeatureListener.readStrongBinder());
            removeHdmiControlStatusChangeListener(iHdmiControlStatusChangeListener);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            iHdmiControlStatusChangeListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            iHdmiControlStatusChangeListener = IHdmiControlStatusChangeListener.Stub.asInterface(iHdmiControlStatusChangeListener.readStrongBinder());
            addHdmiControlStatusChangeListener(iHdmiControlStatusChangeListener);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            iHdmiControlStatusChangeListener.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            iHdmiControlCallback1 = IHdmiControlCallback.Stub.asInterface(iHdmiControlStatusChangeListener.readStrongBinder());
            queryDisplayStatus(iHdmiControlCallback1);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            iHdmiControlCallback1.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            iHdmiControlCallback1 = IHdmiControlCallback.Stub.asInterface(iHdmiControlCallback1.readStrongBinder());
            oneTouchPlay(iHdmiControlCallback1);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            iHdmiControlCallback1.enforceInterface("android.hardware.hdmi.IHdmiControlService");
            hdmiDeviceInfo = getActiveSource();
            param1Parcel2.writeNoException();
            if (hdmiDeviceInfo != null) {
              param1Parcel2.writeInt(1);
              hdmiDeviceInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        hdmiDeviceInfo.enforceInterface("android.hardware.hdmi.IHdmiControlService");
        int[] arrayOfInt = getSupportedTypes();
        param1Parcel2.writeNoException();
        param1Parcel2.writeIntArray(arrayOfInt);
        return true;
      } 
      param1Parcel2.writeString("android.hardware.hdmi.IHdmiControlService");
      return true;
    }
    
    private static class Proxy implements IHdmiControlService {
      public static IHdmiControlService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.hdmi.IHdmiControlService";
      }
      
      public int[] getSupportedTypes() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null)
            return IHdmiControlService.Stub.getDefaultImpl().getSupportedTypes(); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public HdmiDeviceInfo getActiveSource() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          HdmiDeviceInfo hdmiDeviceInfo;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            hdmiDeviceInfo = IHdmiControlService.Stub.getDefaultImpl().getActiveSource();
            return hdmiDeviceInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            hdmiDeviceInfo = (HdmiDeviceInfo)HdmiDeviceInfo.CREATOR.createFromParcel(parcel2);
          } else {
            hdmiDeviceInfo = null;
          } 
          return hdmiDeviceInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void oneTouchPlay(IHdmiControlCallback param2IHdmiControlCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2IHdmiControlCallback != null) {
            iBinder = param2IHdmiControlCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().oneTouchPlay(param2IHdmiControlCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void queryDisplayStatus(IHdmiControlCallback param2IHdmiControlCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2IHdmiControlCallback != null) {
            iBinder = param2IHdmiControlCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().queryDisplayStatus(param2IHdmiControlCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addHdmiControlStatusChangeListener(IHdmiControlStatusChangeListener param2IHdmiControlStatusChangeListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2IHdmiControlStatusChangeListener != null) {
            iBinder = param2IHdmiControlStatusChangeListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().addHdmiControlStatusChangeListener(param2IHdmiControlStatusChangeListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeHdmiControlStatusChangeListener(IHdmiControlStatusChangeListener param2IHdmiControlStatusChangeListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2IHdmiControlStatusChangeListener != null) {
            iBinder = param2IHdmiControlStatusChangeListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().removeHdmiControlStatusChangeListener(param2IHdmiControlStatusChangeListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addHdmiCecVolumeControlFeatureListener(IHdmiCecVolumeControlFeatureListener param2IHdmiCecVolumeControlFeatureListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2IHdmiCecVolumeControlFeatureListener != null) {
            iBinder = param2IHdmiCecVolumeControlFeatureListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().addHdmiCecVolumeControlFeatureListener(param2IHdmiCecVolumeControlFeatureListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeHdmiCecVolumeControlFeatureListener(IHdmiCecVolumeControlFeatureListener param2IHdmiCecVolumeControlFeatureListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2IHdmiCecVolumeControlFeatureListener != null) {
            iBinder = param2IHdmiCecVolumeControlFeatureListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().removeHdmiCecVolumeControlFeatureListener(param2IHdmiCecVolumeControlFeatureListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addHotplugEventListener(IHdmiHotplugEventListener param2IHdmiHotplugEventListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2IHdmiHotplugEventListener != null) {
            iBinder = param2IHdmiHotplugEventListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().addHotplugEventListener(param2IHdmiHotplugEventListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeHotplugEventListener(IHdmiHotplugEventListener param2IHdmiHotplugEventListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2IHdmiHotplugEventListener != null) {
            iBinder = param2IHdmiHotplugEventListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().removeHotplugEventListener(param2IHdmiHotplugEventListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addDeviceEventListener(IHdmiDeviceEventListener param2IHdmiDeviceEventListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2IHdmiDeviceEventListener != null) {
            iBinder = param2IHdmiDeviceEventListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().addDeviceEventListener(param2IHdmiDeviceEventListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deviceSelect(int param2Int, IHdmiControlCallback param2IHdmiControlCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          parcel1.writeInt(param2Int);
          if (param2IHdmiControlCallback != null) {
            iBinder = param2IHdmiControlCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().deviceSelect(param2Int, param2IHdmiControlCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void portSelect(int param2Int, IHdmiControlCallback param2IHdmiControlCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          parcel1.writeInt(param2Int);
          if (param2IHdmiControlCallback != null) {
            iBinder = param2IHdmiControlCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().portSelect(param2Int, param2IHdmiControlCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendKeyEvent(int param2Int1, int param2Int2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool1 && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().sendKeyEvent(param2Int1, param2Int2, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendVolumeKeyEvent(int param2Int1, int param2Int2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool1 && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().sendVolumeKeyEvent(param2Int1, param2Int2, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<HdmiPortInfo> getPortInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null)
            return IHdmiControlService.Stub.getDefaultImpl().getPortInfo(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(HdmiPortInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean canChangeSystemAudioMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(17, parcel1, parcel2, 0);
          if (!bool2 && IHdmiControlService.Stub.getDefaultImpl() != null) {
            bool1 = IHdmiControlService.Stub.getDefaultImpl().canChangeSystemAudioMode();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getSystemAudioMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(18, parcel1, parcel2, 0);
          if (!bool2 && IHdmiControlService.Stub.getDefaultImpl() != null) {
            bool1 = IHdmiControlService.Stub.getDefaultImpl().getSystemAudioMode();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPhysicalAddress() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null)
            return IHdmiControlService.Stub.getDefaultImpl().getPhysicalAddress(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSystemAudioMode(boolean param2Boolean, IHdmiControlCallback param2IHdmiControlCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          if (param2IHdmiControlCallback != null) {
            iBinder = param2IHdmiControlCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool1 = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool1 && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().setSystemAudioMode(param2Boolean, param2IHdmiControlCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addSystemAudioModeChangeListener(IHdmiSystemAudioModeChangeListener param2IHdmiSystemAudioModeChangeListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2IHdmiSystemAudioModeChangeListener != null) {
            iBinder = param2IHdmiSystemAudioModeChangeListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().addSystemAudioModeChangeListener(param2IHdmiSystemAudioModeChangeListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeSystemAudioModeChangeListener(IHdmiSystemAudioModeChangeListener param2IHdmiSystemAudioModeChangeListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2IHdmiSystemAudioModeChangeListener != null) {
            iBinder = param2IHdmiSystemAudioModeChangeListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().removeSystemAudioModeChangeListener(param2IHdmiSystemAudioModeChangeListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setArcMode(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool1 && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().setArcMode(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setProhibitMode(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool1 && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().setProhibitMode(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSystemAudioVolume(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().setSystemAudioVolume(param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSystemAudioMute(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool1 && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().setSystemAudioMute(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInputChangeListener(IHdmiInputChangeListener param2IHdmiInputChangeListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2IHdmiInputChangeListener != null) {
            iBinder = param2IHdmiInputChangeListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().setInputChangeListener(param2IHdmiInputChangeListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<HdmiDeviceInfo> getInputDevices() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null)
            return IHdmiControlService.Stub.getDefaultImpl().getInputDevices(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(HdmiDeviceInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<HdmiDeviceInfo> getDeviceList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null)
            return IHdmiControlService.Stub.getDefaultImpl().getDeviceList(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(HdmiDeviceInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void powerOffRemoteDevice(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().powerOffRemoteDevice(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void powerOnRemoteDevice(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().powerOnRemoteDevice(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void askRemoteDeviceToBecomeActiveSource(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().askRemoteDeviceToBecomeActiveSource(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendVendorCommand(int param2Int1, int param2Int2, byte[] param2ArrayOfbyte, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeByteArray(param2ArrayOfbyte);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool1 && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().sendVendorCommand(param2Int1, param2Int2, param2ArrayOfbyte, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addVendorCommandListener(IHdmiVendorCommandListener param2IHdmiVendorCommandListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2IHdmiVendorCommandListener != null) {
            iBinder = param2IHdmiVendorCommandListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().addVendorCommandListener(param2IHdmiVendorCommandListener, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendStandby(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().sendStandby(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setHdmiRecordListener(IHdmiRecordListener param2IHdmiRecordListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2IHdmiRecordListener != null) {
            iBinder = param2IHdmiRecordListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().setHdmiRecordListener(param2IHdmiRecordListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startOneTouchRecord(int param2Int, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          parcel1.writeInt(param2Int);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().startOneTouchRecord(param2Int, param2ArrayOfbyte);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopOneTouchRecord(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().stopOneTouchRecord(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startTimerRecording(int param2Int1, int param2Int2, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().startTimerRecording(param2Int1, param2Int2, param2ArrayOfbyte);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearTimerRecording(int param2Int1, int param2Int2, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().clearTimerRecording(param2Int1, param2Int2, param2ArrayOfbyte);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendMhlVendorCommand(int param2Int1, int param2Int2, int param2Int3, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().sendMhlVendorCommand(param2Int1, param2Int2, param2Int3, param2ArrayOfbyte);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addHdmiMhlVendorCommandListener(IHdmiMhlVendorCommandListener param2IHdmiMhlVendorCommandListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2IHdmiMhlVendorCommandListener != null) {
            iBinder = param2IHdmiMhlVendorCommandListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().addHdmiMhlVendorCommandListener(param2IHdmiMhlVendorCommandListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setStandbyMode(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool1 && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().setStandbyMode(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setHdmiCecVolumeControlEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool1 && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().setHdmiCecVolumeControlEnabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isHdmiCecVolumeControlEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(45, parcel1, parcel2, 0);
          if (!bool2 && IHdmiControlService.Stub.getDefaultImpl() != null) {
            bool1 = IHdmiControlService.Stub.getDefaultImpl().isHdmiCecVolumeControlEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportAudioStatus(int param2Int1, int param2Int2, int param2Int3, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool1 && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().reportAudioStatus(param2Int1, param2Int2, param2Int3, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSystemAudioModeOnForAudioOnlySource() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.hdmi.IHdmiControlService");
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && IHdmiControlService.Stub.getDefaultImpl() != null) {
            IHdmiControlService.Stub.getDefaultImpl().setSystemAudioModeOnForAudioOnlySource();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IHdmiControlService param1IHdmiControlService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IHdmiControlService != null) {
          Proxy.sDefaultImpl = param1IHdmiControlService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IHdmiControlService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
