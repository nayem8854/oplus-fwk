package android.hardware.hdmi;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IHdmiSystemAudioModeChangeListener extends IInterface {
  void onStatusChanged(boolean paramBoolean) throws RemoteException;
  
  class Default implements IHdmiSystemAudioModeChangeListener {
    public void onStatusChanged(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IHdmiSystemAudioModeChangeListener {
    private static final String DESCRIPTOR = "android.hardware.hdmi.IHdmiSystemAudioModeChangeListener";
    
    static final int TRANSACTION_onStatusChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.hardware.hdmi.IHdmiSystemAudioModeChangeListener");
    }
    
    public static IHdmiSystemAudioModeChangeListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.hdmi.IHdmiSystemAudioModeChangeListener");
      if (iInterface != null && iInterface instanceof IHdmiSystemAudioModeChangeListener)
        return (IHdmiSystemAudioModeChangeListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onStatusChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.hardware.hdmi.IHdmiSystemAudioModeChangeListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.hardware.hdmi.IHdmiSystemAudioModeChangeListener");
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      onStatusChanged(bool);
      return true;
    }
    
    private static class Proxy implements IHdmiSystemAudioModeChangeListener {
      public static IHdmiSystemAudioModeChangeListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.hdmi.IHdmiSystemAudioModeChangeListener";
      }
      
      public void onStatusChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.hardware.hdmi.IHdmiSystemAudioModeChangeListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && IHdmiSystemAudioModeChangeListener.Stub.getDefaultImpl() != null) {
            IHdmiSystemAudioModeChangeListener.Stub.getDefaultImpl().onStatusChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IHdmiSystemAudioModeChangeListener param1IHdmiSystemAudioModeChangeListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IHdmiSystemAudioModeChangeListener != null) {
          Proxy.sDefaultImpl = param1IHdmiSystemAudioModeChangeListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IHdmiSystemAudioModeChangeListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
