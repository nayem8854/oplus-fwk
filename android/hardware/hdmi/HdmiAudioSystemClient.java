package android.hardware.hdmi;

import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;

public final class HdmiAudioSystemClient extends HdmiClient {
  private static final int REPORT_AUDIO_STATUS_INTERVAL_MS = 500;
  
  private static final String TAG = "HdmiAudioSystemClient";
  
  private boolean mCanSendAudioStatus = true;
  
  private final Handler mHandler;
  
  private boolean mLastIsMute;
  
  private int mLastMaxVolume;
  
  private int mLastVolume;
  
  private boolean mPendingReportAudioStatus;
  
  public HdmiAudioSystemClient(IHdmiControlService paramIHdmiControlService) {
    this(paramIHdmiControlService, null);
  }
  
  public HdmiAudioSystemClient(IHdmiControlService paramIHdmiControlService, Handler paramHandler) {
    super(paramIHdmiControlService);
    if (paramHandler == null)
      paramHandler = new Handler(Looper.getMainLooper()); 
    this.mHandler = paramHandler;
  }
  
  public int getDeviceType() {
    return 5;
  }
  
  public void sendReportAudioStatusCecCommand(boolean paramBoolean1, int paramInt1, int paramInt2, boolean paramBoolean2) {
    if (paramBoolean1) {
      try {
        this.mService.reportAudioStatus(getDeviceType(), paramInt1, paramInt2, paramBoolean2);
      } catch (RemoteException remoteException) {}
      return;
    } 
    this.mLastVolume = paramInt1;
    this.mLastMaxVolume = paramInt2;
    this.mLastIsMute = paramBoolean2;
    if (this.mCanSendAudioStatus) {
      try {
        this.mService.reportAudioStatus(getDeviceType(), paramInt1, paramInt2, paramBoolean2);
        this.mCanSendAudioStatus = false;
        Handler handler = this.mHandler;
        Object object = new Object();
        super(this);
        handler.postDelayed((Runnable)object, 500L);
      } catch (RemoteException remoteException) {}
    } else {
      this.mPendingReportAudioStatus = true;
    } 
  }
  
  public void setSystemAudioMode(boolean paramBoolean, SetSystemAudioModeCallback paramSetSystemAudioModeCallback) {}
  
  public void setSystemAudioModeOnForAudioOnlySource() {
    try {
      this.mService.setSystemAudioModeOnForAudioOnlySource();
    } catch (RemoteException remoteException) {
      Log.d("HdmiAudioSystemClient", "Failed to set System Audio Mode on for Audio Only source");
    } 
  }
  
  class SetSystemAudioModeCallback {
    public abstract void onComplete(int param1Int);
  }
}
