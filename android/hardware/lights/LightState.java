package android.hardware.lights;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class LightState implements Parcelable {
  public LightState(int paramInt) {
    this.mColor = paramInt;
  }
  
  private LightState(Parcel paramParcel) {
    this.mColor = paramParcel.readInt();
  }
  
  public int getColor() {
    return this.mColor;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mColor);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<LightState> CREATOR = new Parcelable.Creator<LightState>() {
      public LightState createFromParcel(Parcel param1Parcel) {
        return new LightState(param1Parcel);
      }
      
      public LightState[] newArray(int param1Int) {
        return new LightState[param1Int];
      }
    };
  
  private final int mColor;
}
