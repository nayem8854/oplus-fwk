package android.hardware.cabc;

import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.Log;
import com.android.internal.cabc.ICabcManager;

public class CabcManager {
  private static final String CABC_SERVICE = "cabc";
  
  private static boolean DEBUG = false;
  
  public static final int OFF_MODE = 0;
  
  public static final int PIC_MODE = 2;
  
  private static final String PROP_LOG_CABC = "persist.sys.assert.panic";
  
  private static final String TAG = "CabcManager";
  
  public static final int UI_MODE = 1;
  
  public static final int VIDEO_MODE = 3;
  
  private static CabcManager sCabcManagerInstance = null;
  
  private int mMode = 3;
  
  private ICabcManager mService;
  
  private CabcManager() {
    init();
  }
  
  private void init() {
    DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
    if (this.mService == null) {
      IBinder iBinder = ServiceManager.getService("cabc");
      this.mService = ICabcManager.Stub.asInterface(iBinder);
    } 
  }
  
  public static CabcManager getCabcManagerInstance() {
    if (sCabcManagerInstance == null)
      sCabcManagerInstance = new CabcManager(); 
    return sCabcManagerInstance;
  }
  
  public void setMode(int paramInt) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setMode, new mode:");
      stringBuilder.append(paramInt);
      Log.d("CabcManager", stringBuilder.toString());
    } 
    try {
      if (this.mService != null) {
        this.mService.setMode(paramInt);
      } else {
        Log.e("CabcManager", "setMode failed: mService is null!");
      } 
      return;
    } catch (RemoteException remoteException) {
      throw new RuntimeException("system server dead ?", remoteException);
    } 
  }
  
  public int getMode() {
    try {
      if (this.mService != null)
        return this.mService.getMode(); 
      Log.e("CabcManager", "getMode failed: mService is null!");
      return 3;
    } catch (RemoteException remoteException) {
      throw new RuntimeException("system server dead ?", remoteException);
    } 
  }
  
  public void closeCabc() {
    if (DEBUG)
      Log.d("CabcManager", "closeCabc."); 
    try {
      if (this.mService != null) {
        this.mService.closeCabc();
      } else {
        Log.e("CabcManager", "closeCabc failed: mService is null!");
      } 
      return;
    } catch (RemoteException remoteException) {
      throw new RuntimeException("system server dead ?", remoteException);
    } 
  }
  
  public void openCabc() {
    if (DEBUG)
      Log.d("CabcManager", "openCabc."); 
    try {
      if (this.mService != null) {
        this.mService.openCabc();
      } else {
        Log.e("CabcManager", "openCabc failed: mService is null!");
      } 
      return;
    } catch (RemoteException remoteException) {
      throw new RuntimeException("system server dead ?", remoteException);
    } 
  }
}
