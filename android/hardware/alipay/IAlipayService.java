package android.hardware.alipay;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAlipayService extends IInterface {
  byte[] alipayFaceInvokeCommand(byte[] paramArrayOfbyte) throws RemoteException;
  
  byte[] alipayInvokeCommand(byte[] paramArrayOfbyte) throws RemoteException;
  
  void authenticate(IBinder paramIBinder, String paramString, int paramInt, IAlipayAuthenticatorCallback paramIAlipayAuthenticatorCallback) throws RemoteException;
  
  int cancel(String paramString) throws RemoteException;
  
  void enroll(IBinder paramIBinder, String paramString, int paramInt, IAlipayAuthenticatorCallback paramIAlipayAuthenticatorCallback) throws RemoteException;
  
  String getDeviceModel() throws RemoteException;
  
  int getFingerprintIconDiameter() throws RemoteException;
  
  int getFingerprintIconExternalCircleXY(String paramString) throws RemoteException;
  
  int getSupportBIOTypes() throws RemoteException;
  
  int getSupportIFAAVersion() throws RemoteException;
  
  void upgrade(String paramString) throws RemoteException;
  
  class Default implements IAlipayService {
    public byte[] alipayInvokeCommand(byte[] param1ArrayOfbyte) throws RemoteException {
      return null;
    }
    
    public byte[] alipayFaceInvokeCommand(byte[] param1ArrayOfbyte) throws RemoteException {
      return null;
    }
    
    public void authenticate(IBinder param1IBinder, String param1String, int param1Int, IAlipayAuthenticatorCallback param1IAlipayAuthenticatorCallback) throws RemoteException {}
    
    public void enroll(IBinder param1IBinder, String param1String, int param1Int, IAlipayAuthenticatorCallback param1IAlipayAuthenticatorCallback) throws RemoteException {}
    
    public int cancel(String param1String) throws RemoteException {
      return 0;
    }
    
    public void upgrade(String param1String) throws RemoteException {}
    
    public int getSupportBIOTypes() throws RemoteException {
      return 0;
    }
    
    public int getSupportIFAAVersion() throws RemoteException {
      return 0;
    }
    
    public String getDeviceModel() throws RemoteException {
      return null;
    }
    
    public int getFingerprintIconDiameter() throws RemoteException {
      return 0;
    }
    
    public int getFingerprintIconExternalCircleXY(String param1String) throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAlipayService {
    private static final String DESCRIPTOR = "android.hardware.alipay.IAlipayService";
    
    static final int TRANSACTION_alipayFaceInvokeCommand = 2;
    
    static final int TRANSACTION_alipayInvokeCommand = 1;
    
    static final int TRANSACTION_authenticate = 3;
    
    static final int TRANSACTION_cancel = 5;
    
    static final int TRANSACTION_enroll = 4;
    
    static final int TRANSACTION_getDeviceModel = 9;
    
    static final int TRANSACTION_getFingerprintIconDiameter = 10;
    
    static final int TRANSACTION_getFingerprintIconExternalCircleXY = 11;
    
    static final int TRANSACTION_getSupportBIOTypes = 7;
    
    static final int TRANSACTION_getSupportIFAAVersion = 8;
    
    static final int TRANSACTION_upgrade = 6;
    
    public Stub() {
      attachInterface(this, "android.hardware.alipay.IAlipayService");
    }
    
    public static IAlipayService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.alipay.IAlipayService");
      if (iInterface != null && iInterface instanceof IAlipayService)
        return (IAlipayService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 11:
          return "getFingerprintIconExternalCircleXY";
        case 10:
          return "getFingerprintIconDiameter";
        case 9:
          return "getDeviceModel";
        case 8:
          return "getSupportIFAAVersion";
        case 7:
          return "getSupportBIOTypes";
        case 6:
          return "upgrade";
        case 5:
          return "cancel";
        case 4:
          return "enroll";
        case 3:
          return "authenticate";
        case 2:
          return "alipayFaceInvokeCommand";
        case 1:
          break;
      } 
      return "alipayInvokeCommand";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String str1;
        IAlipayAuthenticatorCallback iAlipayAuthenticatorCallback;
        IBinder iBinder;
        String str2;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 11:
            param1Parcel1.enforceInterface("android.hardware.alipay.IAlipayService");
            str1 = param1Parcel1.readString();
            param1Int1 = getFingerprintIconExternalCircleXY(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 10:
            str1.enforceInterface("android.hardware.alipay.IAlipayService");
            param1Int1 = getFingerprintIconDiameter();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 9:
            str1.enforceInterface("android.hardware.alipay.IAlipayService");
            str1 = getDeviceModel();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 8:
            str1.enforceInterface("android.hardware.alipay.IAlipayService");
            param1Int1 = getSupportIFAAVersion();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 7:
            str1.enforceInterface("android.hardware.alipay.IAlipayService");
            param1Int1 = getSupportBIOTypes();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 6:
            str1.enforceInterface("android.hardware.alipay.IAlipayService");
            str1 = str1.readString();
            upgrade(str1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            str1.enforceInterface("android.hardware.alipay.IAlipayService");
            str1 = str1.readString();
            param1Int1 = cancel(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 4:
            str1.enforceInterface("android.hardware.alipay.IAlipayService");
            iBinder = str1.readStrongBinder();
            str2 = str1.readString();
            param1Int1 = str1.readInt();
            iAlipayAuthenticatorCallback = IAlipayAuthenticatorCallback.Stub.asInterface(str1.readStrongBinder());
            enroll(iBinder, str2, param1Int1, iAlipayAuthenticatorCallback);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            iAlipayAuthenticatorCallback.enforceInterface("android.hardware.alipay.IAlipayService");
            iBinder = iAlipayAuthenticatorCallback.readStrongBinder();
            str2 = iAlipayAuthenticatorCallback.readString();
            param1Int1 = iAlipayAuthenticatorCallback.readInt();
            iAlipayAuthenticatorCallback = IAlipayAuthenticatorCallback.Stub.asInterface(iAlipayAuthenticatorCallback.readStrongBinder());
            authenticate(iBinder, str2, param1Int1, iAlipayAuthenticatorCallback);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            iAlipayAuthenticatorCallback.enforceInterface("android.hardware.alipay.IAlipayService");
            arrayOfByte = iAlipayAuthenticatorCallback.createByteArray();
            arrayOfByte = alipayFaceInvokeCommand(arrayOfByte);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte);
            return true;
          case 1:
            break;
        } 
        arrayOfByte.enforceInterface("android.hardware.alipay.IAlipayService");
        byte[] arrayOfByte = arrayOfByte.createByteArray();
        arrayOfByte = alipayInvokeCommand(arrayOfByte);
        param1Parcel2.writeNoException();
        param1Parcel2.writeByteArray(arrayOfByte);
        return true;
      } 
      param1Parcel2.writeString("android.hardware.alipay.IAlipayService");
      return true;
    }
    
    private static class Proxy implements IAlipayService {
      public static IAlipayService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.alipay.IAlipayService";
      }
      
      public byte[] alipayInvokeCommand(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.alipay.IAlipayService");
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IAlipayService.Stub.getDefaultImpl() != null) {
            param2ArrayOfbyte = IAlipayService.Stub.getDefaultImpl().alipayInvokeCommand(param2ArrayOfbyte);
            return param2ArrayOfbyte;
          } 
          parcel2.readException();
          param2ArrayOfbyte = parcel2.createByteArray();
          return param2ArrayOfbyte;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] alipayFaceInvokeCommand(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.alipay.IAlipayService");
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IAlipayService.Stub.getDefaultImpl() != null) {
            param2ArrayOfbyte = IAlipayService.Stub.getDefaultImpl().alipayFaceInvokeCommand(param2ArrayOfbyte);
            return param2ArrayOfbyte;
          } 
          parcel2.readException();
          param2ArrayOfbyte = parcel2.createByteArray();
          return param2ArrayOfbyte;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void authenticate(IBinder param2IBinder, String param2String, int param2Int, IAlipayAuthenticatorCallback param2IAlipayAuthenticatorCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.alipay.IAlipayService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2IAlipayAuthenticatorCallback != null) {
            iBinder = param2IAlipayAuthenticatorCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IAlipayService.Stub.getDefaultImpl() != null) {
            IAlipayService.Stub.getDefaultImpl().authenticate(param2IBinder, param2String, param2Int, param2IAlipayAuthenticatorCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enroll(IBinder param2IBinder, String param2String, int param2Int, IAlipayAuthenticatorCallback param2IAlipayAuthenticatorCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.alipay.IAlipayService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2IAlipayAuthenticatorCallback != null) {
            iBinder = param2IAlipayAuthenticatorCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IAlipayService.Stub.getDefaultImpl() != null) {
            IAlipayService.Stub.getDefaultImpl().enroll(param2IBinder, param2String, param2Int, param2IAlipayAuthenticatorCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int cancel(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.alipay.IAlipayService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IAlipayService.Stub.getDefaultImpl() != null)
            return IAlipayService.Stub.getDefaultImpl().cancel(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void upgrade(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.alipay.IAlipayService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IAlipayService.Stub.getDefaultImpl() != null) {
            IAlipayService.Stub.getDefaultImpl().upgrade(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSupportBIOTypes() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.alipay.IAlipayService");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IAlipayService.Stub.getDefaultImpl() != null)
            return IAlipayService.Stub.getDefaultImpl().getSupportBIOTypes(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSupportIFAAVersion() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.alipay.IAlipayService");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IAlipayService.Stub.getDefaultImpl() != null)
            return IAlipayService.Stub.getDefaultImpl().getSupportIFAAVersion(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDeviceModel() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.alipay.IAlipayService");
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IAlipayService.Stub.getDefaultImpl() != null)
            return IAlipayService.Stub.getDefaultImpl().getDeviceModel(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getFingerprintIconDiameter() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.alipay.IAlipayService");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IAlipayService.Stub.getDefaultImpl() != null)
            return IAlipayService.Stub.getDefaultImpl().getFingerprintIconDiameter(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getFingerprintIconExternalCircleXY(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.alipay.IAlipayService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IAlipayService.Stub.getDefaultImpl() != null)
            return IAlipayService.Stub.getDefaultImpl().getFingerprintIconExternalCircleXY(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAlipayService param1IAlipayService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAlipayService != null) {
          Proxy.sDefaultImpl = param1IAlipayService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAlipayService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
