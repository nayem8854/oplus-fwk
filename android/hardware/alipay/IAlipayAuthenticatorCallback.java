package android.hardware.alipay;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAlipayAuthenticatorCallback extends IInterface {
  void onAuthenticationError(int paramInt) throws RemoteException;
  
  void onAuthenticationFailed(int paramInt) throws RemoteException;
  
  void onAuthenticationStatus(int paramInt) throws RemoteException;
  
  void onAuthenticationSucceeded() throws RemoteException;
  
  class Default implements IAlipayAuthenticatorCallback {
    public void onAuthenticationError(int param1Int) throws RemoteException {}
    
    public void onAuthenticationStatus(int param1Int) throws RemoteException {}
    
    public void onAuthenticationSucceeded() throws RemoteException {}
    
    public void onAuthenticationFailed(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAlipayAuthenticatorCallback {
    private static final String DESCRIPTOR = "android.hardware.alipay.IAlipayAuthenticatorCallback";
    
    static final int TRANSACTION_onAuthenticationError = 1;
    
    static final int TRANSACTION_onAuthenticationFailed = 4;
    
    static final int TRANSACTION_onAuthenticationStatus = 2;
    
    static final int TRANSACTION_onAuthenticationSucceeded = 3;
    
    public Stub() {
      attachInterface(this, "android.hardware.alipay.IAlipayAuthenticatorCallback");
    }
    
    public static IAlipayAuthenticatorCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.alipay.IAlipayAuthenticatorCallback");
      if (iInterface != null && iInterface instanceof IAlipayAuthenticatorCallback)
        return (IAlipayAuthenticatorCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onAuthenticationFailed";
          } 
          return "onAuthenticationSucceeded";
        } 
        return "onAuthenticationStatus";
      } 
      return "onAuthenticationError";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.hardware.alipay.IAlipayAuthenticatorCallback");
              return true;
            } 
            param1Parcel1.enforceInterface("android.hardware.alipay.IAlipayAuthenticatorCallback");
            param1Int1 = param1Parcel1.readInt();
            onAuthenticationFailed(param1Int1);
            return true;
          } 
          param1Parcel1.enforceInterface("android.hardware.alipay.IAlipayAuthenticatorCallback");
          onAuthenticationSucceeded();
          return true;
        } 
        param1Parcel1.enforceInterface("android.hardware.alipay.IAlipayAuthenticatorCallback");
        param1Int1 = param1Parcel1.readInt();
        onAuthenticationStatus(param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.hardware.alipay.IAlipayAuthenticatorCallback");
      param1Int1 = param1Parcel1.readInt();
      onAuthenticationError(param1Int1);
      return true;
    }
    
    private static class Proxy implements IAlipayAuthenticatorCallback {
      public static IAlipayAuthenticatorCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.alipay.IAlipayAuthenticatorCallback";
      }
      
      public void onAuthenticationError(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.alipay.IAlipayAuthenticatorCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IAlipayAuthenticatorCallback.Stub.getDefaultImpl() != null) {
            IAlipayAuthenticatorCallback.Stub.getDefaultImpl().onAuthenticationError(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAuthenticationStatus(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.alipay.IAlipayAuthenticatorCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IAlipayAuthenticatorCallback.Stub.getDefaultImpl() != null) {
            IAlipayAuthenticatorCallback.Stub.getDefaultImpl().onAuthenticationStatus(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAuthenticationSucceeded() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.alipay.IAlipayAuthenticatorCallback");
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IAlipayAuthenticatorCallback.Stub.getDefaultImpl() != null) {
            IAlipayAuthenticatorCallback.Stub.getDefaultImpl().onAuthenticationSucceeded();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAuthenticationFailed(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.alipay.IAlipayAuthenticatorCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IAlipayAuthenticatorCallback.Stub.getDefaultImpl() != null) {
            IAlipayAuthenticatorCallback.Stub.getDefaultImpl().onAuthenticationFailed(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAlipayAuthenticatorCallback param1IAlipayAuthenticatorCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAlipayAuthenticatorCallback != null) {
          Proxy.sDefaultImpl = param1IAlipayAuthenticatorCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAlipayAuthenticatorCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
