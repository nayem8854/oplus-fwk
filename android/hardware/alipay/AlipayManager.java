package android.hardware.alipay;

import android.content.Context;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.util.Slog;

public class AlipayManager {
  public static final int AUTH_TYPE_FACE = 4;
  
  public static final int AUTH_TYPE_FINGERPRINT = 1;
  
  public static final int AUTH_TYPE_IRIS = 2;
  
  public static final int AUTH_TYPE_NOT_SUPPORT = 0;
  
  public static final int AUTH_TYPE_OPTICAL_FINGERPRINT = 17;
  
  public static final int OPLUS_DEFAULT_FINGERPRINT_ICON_DIAMETER = 190;
  
  public static final int OPLUS_DEFAULT_FINGERPRINT_ICON_LOCATION_X = 445;
  
  public static final int OPLUS_DEFAULT_FINGERPRINT_ICON_LOCATION_Y = 1967;
  
  public static final String OPLUS_DEFAULT_MODEL = "OPLUS-Default";
  
  private static final String TAG = "AlipayManager";
  
  private Context mContext;
  
  private IAlipayService mService;
  
  private IBinder mToken = (IBinder)new Binder();
  
  public AlipayManager(Context paramContext, IAlipayService paramIAlipayService) {
    this.mContext = paramContext;
    this.mService = paramIAlipayService;
    if (paramIAlipayService == null)
      Slog.v("AlipayManager", "AlipayService was null"); 
  }
  
  public byte[] alipayInvokeCommand(byte[] paramArrayOfbyte) {
    byte[] arrayOfByte = null;
    RemoteException remoteException = null;
    IAlipayService iAlipayService = this.mService;
    if (iAlipayService != null) {
      try {
        paramArrayOfbyte = iAlipayService.alipayInvokeCommand(paramArrayOfbyte);
      } catch (RemoteException remoteException1) {
        Log.v("AlipayManager", "Remote exception in alipayInvokeCommand(): ", (Throwable)remoteException1);
        remoteException1 = remoteException;
      } 
    } else {
      Log.w("AlipayManager", "alipayInvokeCommand(): Service not connected!");
      paramArrayOfbyte = arrayOfByte;
    } 
    return paramArrayOfbyte;
  }
  
  public byte[] alipayFaceInvokeCommand(byte[] paramArrayOfbyte) {
    byte[] arrayOfByte = null;
    RemoteException remoteException = null;
    IAlipayService iAlipayService = this.mService;
    if (iAlipayService != null) {
      try {
        paramArrayOfbyte = iAlipayService.alipayFaceInvokeCommand(paramArrayOfbyte);
      } catch (RemoteException remoteException1) {
        Log.v("AlipayManager", "Remote exception in alipayFaceInvokeCommand(): ", (Throwable)remoteException1);
        remoteException1 = remoteException;
      } 
    } else {
      Log.w("AlipayManager", "alipayFaceInvokeCommand(): Service not connected!");
      paramArrayOfbyte = arrayOfByte;
    } 
    return paramArrayOfbyte;
  }
  
  public void authenticate(String paramString, int paramInt, IAlipayAuthenticatorCallback paramIAlipayAuthenticatorCallback) {
    IAlipayService iAlipayService = this.mService;
    if (iAlipayService != null) {
      try {
        iAlipayService.authenticate(this.mToken, paramString, paramInt, paramIAlipayAuthenticatorCallback);
      } catch (RemoteException remoteException) {
        Log.v("AlipayManager", "Remote exception in authenticate(): ", (Throwable)remoteException);
      } 
    } else {
      Log.w("AlipayManager", "authenticate(): Service not connected!");
    } 
  }
  
  public void enroll(String paramString, int paramInt, IAlipayAuthenticatorCallback paramIAlipayAuthenticatorCallback) {
    IAlipayService iAlipayService = this.mService;
    if (iAlipayService != null) {
      try {
        iAlipayService.enroll(this.mToken, paramString, paramInt, paramIAlipayAuthenticatorCallback);
      } catch (RemoteException remoteException) {
        Log.v("AlipayManager", "Remote exception in enroll(): ", (Throwable)remoteException);
      } 
    } else {
      Log.w("AlipayManager", "enroll(): Service not connected!");
    } 
  }
  
  public int cancel(String paramString) {
    int i = -1;
    IAlipayService iAlipayService = this.mService;
    if (iAlipayService != null) {
      try {
        int j = iAlipayService.cancel(paramString);
      } catch (RemoteException remoteException) {
        Log.v("AlipayManager", "Remote exception in cancel(): ", (Throwable)remoteException);
      } 
    } else {
      Log.w("AlipayManager", "cancel(): Service not connected!");
    } 
    return i;
  }
  
  public void upgrade(String paramString) {
    IAlipayService iAlipayService = this.mService;
    if (iAlipayService != null) {
      try {
        iAlipayService.upgrade(paramString);
      } catch (RemoteException remoteException) {
        Log.v("AlipayManager", "Remote exception in upgrade(): ", (Throwable)remoteException);
      } 
    } else {
      Log.w("AlipayManager", "upgrade(): Service not connected!");
    } 
  }
  
  public int getSupportBIOTypes() {
    int i = 0, j = 0;
    IAlipayService iAlipayService = this.mService;
    if (iAlipayService != null) {
      try {
        j = i = iAlipayService.getSupportBIOTypes();
      } catch (RemoteException remoteException) {
        Log.v("AlipayManager", "Remote exception in getSupportBIOTypes(): ", (Throwable)remoteException);
      } 
    } else {
      Log.w("AlipayManager", "Service not connected!");
      j = i;
    } 
    return j;
  }
  
  public int getSupportIFAAVersion() {
    int i = 0, j = 0;
    IAlipayService iAlipayService = this.mService;
    if (iAlipayService != null) {
      try {
        j = i = iAlipayService.getSupportIFAAVersion();
      } catch (RemoteException remoteException) {
        Log.v("AlipayManager", "Remote exception in getSupportIFAAVersion(): ", (Throwable)remoteException);
      } 
    } else {
      Log.w("AlipayManager", "Service not connected!");
      j = i;
    } 
    return j;
  }
  
  public String getDeviceModel() {
    String str = "OPLUS-Default";
    IAlipayService iAlipayService = this.mService;
    if (iAlipayService != null) {
      try {
        String str1 = iAlipayService.getDeviceModel();
      } catch (RemoteException remoteException) {
        Log.v("AlipayManager", "Remote exception in getDeviceModel(): ", (Throwable)remoteException);
      } 
    } else {
      Log.w("AlipayManager", "Service not connected!");
    } 
    return str;
  }
  
  public int getFingerprintIconDiameter() {
    int i = 190;
    IAlipayService iAlipayService = this.mService;
    if (iAlipayService != null) {
      try {
        int j = iAlipayService.getFingerprintIconDiameter();
      } catch (RemoteException remoteException) {
        Log.v("AlipayManager", "Remote exception in getFingerprintIconDiameter(): ", (Throwable)remoteException);
      } 
    } else {
      Log.w("AlipayManager", "Service not connected!");
    } 
    return i;
  }
  
  public int getFingerprintIconExternalCircleXY(String paramString) {
    int i;
    if ("X".equals(paramString)) {
      i = 445;
    } else {
      i = 1967;
    } 
    IAlipayService iAlipayService = this.mService;
    if (iAlipayService != null) {
      try {
        int j = iAlipayService.getFingerprintIconExternalCircleXY(paramString);
      } catch (RemoteException remoteException) {
        Log.v("AlipayManager", "Remote exception in getFingerprintIconExternalCircleXY(): ", (Throwable)remoteException);
      } 
    } else {
      Log.w("AlipayManager", "Service not connected!");
    } 
    return i;
  }
}
