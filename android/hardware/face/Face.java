package android.hardware.face;

import android.hardware.biometrics.BiometricAuthenticator;
import android.os.Parcel;
import android.os.Parcelable;

public final class Face extends BiometricAuthenticator.Identifier {
  public Face(CharSequence paramCharSequence, int paramInt, long paramLong) {
    super(paramCharSequence, paramInt, paramLong);
  }
  
  private Face(Parcel paramParcel) {
    super(paramParcel.readString(), paramParcel.readInt(), paramParcel.readLong());
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(getName().toString());
    paramParcel.writeInt(getBiometricId());
    paramParcel.writeLong(getDeviceId());
  }
  
  public static final Parcelable.Creator<Face> CREATOR = (Parcelable.Creator<Face>)new Object();
}
