package android.hardware.face;

public enum ClientMode {
  AUTHEN,
  ENGINEERING_INFO,
  ENROLL,
  NONE(0),
  REMOVE(0),
  UPDATE_FEATURE(0);
  
  private static final ClientMode[] $VALUES;
  
  final int mode;
  
  static {
    ENROLL = new ClientMode("ENROLL", 1, 1);
    AUTHEN = new ClientMode("AUTHEN", 2, 2);
    REMOVE = new ClientMode("REMOVE", 3, 3);
    ENGINEERING_INFO = new ClientMode("ENGINEERING_INFO", 4, 4);
    ClientMode clientMode = new ClientMode("UPDATE_FEATURE", 5, 5);
    $VALUES = new ClientMode[] { NONE, ENROLL, AUTHEN, REMOVE, ENGINEERING_INFO, clientMode };
  }
  
  ClientMode(int paramInt1) {
    this.mode = paramInt1;
  }
  
  public int getValue() {
    return this.mode;
  }
}
