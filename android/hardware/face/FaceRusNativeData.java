package android.hardware.face;

import android.os.Parcel;
import android.os.Parcelable;

public class FaceRusNativeData implements Parcelable {
  public FaceRusNativeData() {}
  
  public FaceRusNativeData(float paramFloat) {
    this.mHacknessThreshold = paramFloat;
  }
  
  public FaceRusNativeData(Parcel paramParcel) {
    this.mHacknessThreshold = paramParcel.readFloat();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeFloat(this.mHacknessThreshold);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("hacknessThreshold: ");
    stringBuilder.append(this.mHacknessThreshold);
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<FaceRusNativeData> CREATOR = new Parcelable.Creator<FaceRusNativeData>() {
      public FaceRusNativeData createFromParcel(Parcel param1Parcel) {
        return new FaceRusNativeData(param1Parcel);
      }
      
      public FaceRusNativeData[] newArray(int param1Int) {
        return new FaceRusNativeData[param1Int];
      }
    };
  
  private static final String TAG = "FaceRusNativeData";
  
  public float mHacknessThreshold;
}
