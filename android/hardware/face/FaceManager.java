package android.hardware.face;

import android.app.ActivityManager;
import android.content.Context;
import android.hardware.biometrics.BiometricAuthenticator;
import android.hardware.biometrics.BiometricFaceConstants;
import android.hardware.biometrics.CryptoObject;
import android.hardware.biometrics.IBiometricServiceLockoutResetCallback;
import android.hardware.biometrics.OplusTimeUtils;
import android.os.Binder;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.Trace;
import android.os.UserHandle;
import android.util.Log;
import android.util.Slog;
import com.android.internal.os.SomeArgs;
import java.util.List;

public class FaceManager extends OplusBaseFaceManager implements BiometricAuthenticator, BiometricFaceConstants {
  private IBinder mToken = (IBinder)new Binder();
  
  private SetFeatureCallback mSetFeatureCallback;
  
  private IFaceServiceReceiver mServiceReceiver = (IFaceServiceReceiver)new Object(this);
  
  private IFaceService mService;
  
  private Face mRemovalFace;
  
  private RemovalCallback mRemovalCallback;
  
  private Handler mHandler;
  
  private GetFeatureCallback mGetFeatureCallback;
  
  private EnrollmentCallback mEnrollmentCallback;
  
  private CryptoObject mCryptoObject;
  
  private final Context mContext;
  
  private AuthenticationCallback mAuthenticationCallback;
  
  private static final String TAG = "FaceManager";
  
  private static final int MSG_SET_FEATURE_COMPLETED = 107;
  
  private static final int MSG_REMOVED = 105;
  
  private static final int MSG_PROGRESS_CHANGED = 999;
  
  private static final int MSG_GET_FEATURE_COMPLETED = 106;
  
  private static final int MSG_ERROR = 104;
  
  private static final int MSG_ENROLL_RESULT = 100;
  
  private static final int MSG_AUTHENTICATION_SUCCEEDED = 102;
  
  private static final int MSG_AUTHENTICATION_FAILED = 103;
  
  private static final int MSG_ACQUIRED = 101;
  
  private static final boolean DEBUG = true;
  
  public FaceManager(Context paramContext, IFaceService paramIFaceService) {
    super(paramContext, paramIFaceService);
    this.mContext = paramContext;
    this.mService = paramIFaceService;
    if (paramIFaceService == null)
      Slog.v("FaceManager", "FaceAuthenticationManagerService was null"); 
    this.mHandler = new MyHandler(paramContext);
  }
  
  public void authenticate(CryptoObject paramCryptoObject, CancellationSignal paramCancellationSignal, int paramInt, AuthenticationCallback paramAuthenticationCallback, Handler paramHandler) {
    authenticate(paramCryptoObject, paramCancellationSignal, paramInt, paramAuthenticationCallback, paramHandler, this.mContext.getUserId());
  }
  
  private void useHandler(Handler paramHandler) {
    if (paramHandler != null) {
      this.mHandler = new MyHandler(paramHandler.getLooper());
    } else if (this.mHandler.getLooper() != this.mContext.getMainLooper()) {
      this.mHandler = new MyHandler(this.mContext.getMainLooper());
    } 
  }
  
  public void authenticate(CryptoObject paramCryptoObject, CancellationSignal paramCancellationSignal, int paramInt1, AuthenticationCallback paramAuthenticationCallback, Handler paramHandler, int paramInt2) {
    if (paramAuthenticationCallback != null) {
      if (paramCancellationSignal != null) {
        if (paramCancellationSignal.isCanceled()) {
          Log.w("FaceManager", "authentication already canceled");
          return;
        } 
        paramCancellationSignal.setOnCancelListener(new OnAuthenticationCancelListener(paramCryptoObject));
      } 
      if (this.mService != null) {
        try {
          long l;
          useHandler(paramHandler);
          this.mAuthenticationCallback = paramAuthenticationCallback;
          this.mCryptoObject = paramCryptoObject;
          if (paramCryptoObject != null) {
            l = paramCryptoObject.getOpId();
          } else {
            l = 0L;
          } 
          Trace.beginSection("FaceManager#authenticate");
          Slog.d("FaceManager", "FaceManager#authenticate");
          OplusTimeUtils.stopCalculateTime("FaceManager", "FaceManager#authenticate");
          IFaceService iFaceService = this.mService;
          IBinder iBinder = this.mToken;
          IFaceServiceReceiver iFaceServiceReceiver = this.mServiceReceiver;
          Context context = this.mContext;
          String str = context.getOpPackageName();
          iFaceService.authenticate(iBinder, l, paramInt2, iFaceServiceReceiver, paramInt1, str);
        } catch (RemoteException remoteException) {
          Log.w("FaceManager", "Remote exception while authenticating: ", (Throwable)remoteException);
          if (paramAuthenticationCallback != null) {
            Context context = this.mContext;
            String str = getErrorString(context, 1, 0);
            paramAuthenticationCallback.onAuthenticationError(1, str);
          } 
        } finally {}
        Trace.endSection();
      } 
      return;
    } 
    throw new IllegalArgumentException("Must supply an authentication callback");
  }
  
  public void enroll(int paramInt, byte[] paramArrayOfbyte, CancellationSignal paramCancellationSignal, EnrollmentCallback paramEnrollmentCallback, int[] paramArrayOfint) {
    if (paramEnrollmentCallback != null) {
      if (paramCancellationSignal != null) {
        if (paramCancellationSignal.isCanceled()) {
          Log.w("FaceManager", "enrollment already canceled");
          return;
        } 
        paramCancellationSignal.setOnCancelListener(new OnEnrollCancelListener());
      } 
      if (this.mService != null) {
        try {
          this.mEnrollmentCallback = paramEnrollmentCallback;
          Trace.beginSection("FaceManager#enroll");
          Slog.d("FaceManager", "FaceManager#enroll");
          IFaceService iFaceService = this.mService;
          IBinder iBinder = this.mToken;
          IFaceServiceReceiver iFaceServiceReceiver = this.mServiceReceiver;
          Context context = this.mContext;
          String str = context.getOpPackageName();
          iFaceService.enroll(paramInt, iBinder, paramArrayOfbyte, iFaceServiceReceiver, str, paramArrayOfint);
        } catch (RemoteException remoteException) {
          Log.w("FaceManager", "Remote exception in enroll: ", (Throwable)remoteException);
          if (paramEnrollmentCallback != null) {
            Context context = this.mContext;
            String str = getErrorString(context, 1, 0);
            paramEnrollmentCallback.onEnrollmentError(1, str);
          } 
        } finally {}
        Trace.endSection();
      } 
      return;
    } 
    throw new IllegalArgumentException("Must supply an enrollment callback");
  }
  
  public long generateChallenge() {
    long l = 0L;
    IFaceService iFaceService = this.mService;
    if (iFaceService != null)
      try {
        l = iFaceService.generateChallenge(this.mToken);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    return l;
  }
  
  public int revokeChallenge() {
    int i = 0;
    IFaceService iFaceService = this.mService;
    if (iFaceService != null)
      try {
        i = iFaceService.revokeChallenge(this.mToken);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    return i;
  }
  
  public void setFeature(int paramInt1, int paramInt2, boolean paramBoolean, byte[] paramArrayOfbyte, SetFeatureCallback paramSetFeatureCallback) {
    IFaceService iFaceService = this.mService;
    if (iFaceService != null)
      try {
        this.mSetFeatureCallback = paramSetFeatureCallback;
        IFaceServiceReceiver iFaceServiceReceiver = this.mServiceReceiver;
        Context context = this.mContext;
        String str = context.getOpPackageName();
        iFaceService.setFeature(paramInt1, paramInt2, paramBoolean, paramArrayOfbyte, iFaceServiceReceiver, str);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
  }
  
  public void getFeature(int paramInt1, int paramInt2, GetFeatureCallback paramGetFeatureCallback) {
    IFaceService iFaceService = this.mService;
    if (iFaceService != null)
      try {
        this.mGetFeatureCallback = paramGetFeatureCallback;
        iFaceService.getFeature(paramInt1, paramInt2, this.mServiceReceiver, this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
  }
  
  public void userActivity() {
    IFaceService iFaceService = this.mService;
    if (iFaceService != null)
      try {
        iFaceService.userActivity();
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
  }
  
  public void setActiveUser(int paramInt) {
    IFaceService iFaceService = this.mService;
    if (iFaceService != null)
      try {
        iFaceService.setActiveUser(paramInt);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
  }
  
  public void remove(Face paramFace, int paramInt, RemovalCallback paramRemovalCallback) {
    if (this.mService != null)
      try {
        this.mRemovalCallback = paramRemovalCallback;
        this.mRemovalFace = paramFace;
        Slog.d("FaceManager", "FaceManager#remove");
        IFaceService iFaceService = this.mService;
        IBinder iBinder = this.mToken;
        int i = paramFace.getBiometricId();
        IFaceServiceReceiver iFaceServiceReceiver = this.mServiceReceiver;
        Context context = this.mContext;
        String str = context.getOpPackageName();
        iFaceService.remove(iBinder, i, paramInt, iFaceServiceReceiver, str);
      } catch (RemoteException remoteException) {
        Log.w("FaceManager", "Remote exception in remove: ", (Throwable)remoteException);
        if (paramRemovalCallback != null) {
          Context context = this.mContext;
          String str = getErrorString(context, 1, 0);
          paramRemovalCallback.onRemovalError(paramFace, 1, str);
        } 
      }  
  }
  
  public List<Face> getEnrolledFaces(int paramInt) {
    IFaceService iFaceService = this.mService;
    if (iFaceService != null)
      try {
        return iFaceService.getEnrolledFaces(paramInt, this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    return null;
  }
  
  public List<Face> getEnrolledFaces() {
    return getEnrolledFaces(UserHandle.myUserId());
  }
  
  public boolean hasEnrolledTemplates() {
    IFaceService iFaceService = this.mService;
    if (iFaceService != null)
      try {
        int i = UserHandle.myUserId();
        String str = this.mContext.getOpPackageName();
        return iFaceService.hasEnrolledFaces(i, str);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    return false;
  }
  
  public boolean hasEnrolledTemplates(int paramInt) {
    IFaceService iFaceService = this.mService;
    if (iFaceService != null)
      try {
        return iFaceService.hasEnrolledFaces(paramInt, this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    return false;
  }
  
  public boolean isHardwareDetected() {
    IFaceService iFaceService = this.mService;
    if (iFaceService != null)
      try {
        return iFaceService.isHardwareDetected(this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    Log.w("FaceManager", "isFaceHardwareDetected(): Service not connected!");
    return false;
  }
  
  public void addLockoutResetCallback(LockoutResetCallback paramLockoutResetCallback) {
    if (this.mService != null) {
      try {
        PowerManager powerManager = this.mContext.<PowerManager>getSystemService(PowerManager.class);
        IFaceService iFaceService = this.mService;
        Object object = new Object();
        super(this, powerManager, paramLockoutResetCallback);
        iFaceService.addLockoutResetCallback((IBiometricServiceLockoutResetCallback)object);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } else {
      Log.w("FaceManager", "addLockoutResetCallback(): Service not connected!");
    } 
  }
  
  private int getCurrentUserId() {
    try {
      return (ActivityManager.getService().getCurrentUser()).id;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private void cancelEnrollment() {
    if (this.mService != null)
      try {
        Slog.d("FaceManager", "FaceManager#cancelEnrollment");
        this.mService.cancelEnrollment(this.mToken);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
  }
  
  private void cancelAuthentication(CryptoObject paramCryptoObject) {
    if (this.mService != null)
      try {
        Slog.d("FaceManager", "FaceManager#cancelAuthentication");
        this.mService.cancelAuthentication(this.mToken, this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
  }
  
  public static String getErrorString(Context paramContext, int paramInt1, int paramInt2) {
    StringBuilder stringBuilder;
    String[] arrayOfString;
    switch (paramInt1) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid error message: ");
        stringBuilder.append(paramInt1);
        stringBuilder.append(", ");
        stringBuilder.append(paramInt2);
        Slog.w("FaceManager", stringBuilder.toString());
        return "";
      case 15:
        return stringBuilder.getString(17040197);
      case 12:
        return stringBuilder.getString(17040192);
      case 11:
        return stringBuilder.getString(17040196);
      case 10:
        return stringBuilder.getString(17040200);
      case 9:
        return stringBuilder.getString(17040194);
      case 8:
        arrayOfString = stringBuilder.getResources().getStringArray(17236103);
        if (paramInt2 < arrayOfString.length)
          return arrayOfString[paramInt2]; 
      case 7:
        return arrayOfString.getString(17040193);
      case 5:
        return arrayOfString.getString(17040190);
      case 4:
        return arrayOfString.getString(17040195);
      case 3:
        return arrayOfString.getString(17040198);
      case 2:
        return arrayOfString.getString(17040199);
      case 1:
        break;
    } 
    return arrayOfString.getString(17040191);
  }
  
  public static String getAcquiredString(Context paramContext, int paramInt1, int paramInt2) {
    StringBuilder stringBuilder;
    String[] arrayOfString;
    switch (paramInt1) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid acquired message: ");
        stringBuilder.append(paramInt1);
        stringBuilder.append(", ");
        stringBuilder.append(paramInt2);
        Slog.w("FaceManager", stringBuilder.toString());
        return null;
      case 22:
        arrayOfString = stringBuilder.getResources().getStringArray(17236102);
        if (paramInt2 < arrayOfString.length)
          return arrayOfString[paramInt2]; 
      case 21:
        return arrayOfString.getString(17040175);
      case 20:
        return null;
      case 19:
        return arrayOfString.getString(17040170);
      case 18:
        return arrayOfString.getString(17040174);
      case 17:
        return arrayOfString.getString(17040176);
      case 16:
        return arrayOfString.getString(17040171);
      case 15:
        return arrayOfString.getString(17040187);
      case 14:
        return arrayOfString.getString(17040180);
      case 13:
        return arrayOfString.getString(17040173);
      case 12:
        return arrayOfString.getString(17040185);
      case 11:
        return arrayOfString.getString(17040169);
      case 10:
        return arrayOfString.getString(17040172);
      case 9:
        return arrayOfString.getString(17040183);
      case 8:
        return arrayOfString.getString(17040186);
      case 7:
        return arrayOfString.getString(17040184);
      case 6:
        return arrayOfString.getString(17040182);
      case 5:
        return arrayOfString.getString(17040181);
      case 4:
        return arrayOfString.getString(17040178);
      case 3:
        return arrayOfString.getString(17040179);
      case 2:
        return arrayOfString.getString(17040177);
      case 1:
        return arrayOfString.getString(17040168);
      case 0:
        break;
    } 
    return null;
  }
  
  public static int getMappedAcquiredInfo(int paramInt1, int paramInt2) {
    if (paramInt1 != 22) {
      switch (paramInt1) {
        default:
          return 0;
        case 10:
        case 11:
        case 12:
        case 13:
          return 2;
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
          return 1;
        case 1:
        case 2:
        case 3:
          break;
      } 
      return 2;
    } 
    return paramInt2 + 1000;
  }
  
  class AuthenticationResult {
    private CryptoObject mCryptoObject;
    
    private Face mFace;
    
    private boolean mIsStrongBiometric;
    
    private int mUserId;
    
    public AuthenticationResult(FaceManager this$0, Face param1Face, int param1Int, boolean param1Boolean) {
      this.mCryptoObject = (CryptoObject)this$0;
      this.mFace = param1Face;
      this.mUserId = param1Int;
      this.mIsStrongBiometric = param1Boolean;
    }
    
    public CryptoObject getCryptoObject() {
      return this.mCryptoObject;
    }
    
    public Face getFace() {
      return this.mFace;
    }
    
    public int getUserId() {
      return this.mUserId;
    }
    
    public boolean isStrongBiometric() {
      return this.mIsStrongBiometric;
    }
  }
  
  public static abstract class AuthenticationCallback extends BiometricAuthenticator.AuthenticationCallback {
    public void onAuthenticationError(int param1Int, CharSequence param1CharSequence) {}
    
    public void onAuthenticationHelp(int param1Int, CharSequence param1CharSequence) {}
    
    public void onAuthenticationSucceeded(FaceManager.AuthenticationResult param1AuthenticationResult) {}
    
    public void onAuthenticationFailed() {}
    
    public void onAuthenticationAcquired(int param1Int) {}
    
    public void onProgressChanged(int param1Int) {}
  }
  
  class EnrollmentCallback {
    public void onEnrollmentError(int param1Int, CharSequence param1CharSequence) {}
    
    public void onEnrollmentHelp(int param1Int, CharSequence param1CharSequence) {}
    
    public void onEnrollmentProgress(int param1Int) {}
    
    public void onProgressChanged(int param1Int) {}
  }
  
  class RemovalCallback {
    public void onRemovalError(Face param1Face, int param1Int, CharSequence param1CharSequence) {}
    
    public void onRemovalSucceeded(Face param1Face, int param1Int) {}
  }
  
  class LockoutResetCallback {
    public void onLockoutReset() {}
  }
  
  class SetFeatureCallback {
    public abstract void onCompleted(boolean param1Boolean, int param1Int);
  }
  
  class GetFeatureCallback {
    public abstract void onCompleted(boolean param1Boolean1, int param1Int, boolean param1Boolean2);
  }
  
  private class OnEnrollCancelListener implements CancellationSignal.OnCancelListener {
    final FaceManager this$0;
    
    private OnEnrollCancelListener() {}
    
    public void onCancel() {
      FaceManager.this.cancelEnrollment();
    }
  }
  
  private class OnAuthenticationCancelListener implements CancellationSignal.OnCancelListener {
    private CryptoObject mCrypto;
    
    final FaceManager this$0;
    
    OnAuthenticationCancelListener(CryptoObject param1CryptoObject) {
      this.mCrypto = param1CryptoObject;
    }
    
    public void onCancel() {
      FaceManager.this.cancelAuthentication(this.mCrypto);
    }
  }
  
  private class MyHandler extends Handler {
    final FaceManager this$0;
    
    private MyHandler(Context param1Context) {
      super(param1Context.getMainLooper());
    }
    
    private MyHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      FaceManager faceManager;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("FaceManager#handleMessage: ");
      stringBuilder.append(Integer.toString(param1Message.what));
      Trace.beginSection(stringBuilder.toString());
      int i = param1Message.what;
      if (i != 999) {
        Boolean bool;
        FaceManager faceManager1;
        SomeArgs someArgs;
        Face face;
        boolean bool1;
        boolean bool2;
        int j;
        switch (i) {
          default:
            stringBuilder = new StringBuilder();
            stringBuilder.append("Unknown message: ");
            stringBuilder.append(param1Message.what);
            Log.w("FaceManager", stringBuilder.toString());
            break;
          case 107:
            FaceManager.this.sendSetFeatureCompleted(((Boolean)param1Message.obj).booleanValue(), param1Message.arg1);
            break;
          case 106:
            someArgs = (SomeArgs)param1Message.obj;
            faceManager = FaceManager.this;
            bool1 = ((Boolean)someArgs.arg1).booleanValue();
            i = someArgs.argi1;
            bool = (Boolean)someArgs.arg2;
            bool2 = bool.booleanValue();
            faceManager.sendGetFeatureCompleted(bool1, i, bool2);
            someArgs.recycle();
            break;
          case 105:
            FaceManager.this.sendRemovedResult((Face)((Message)faceManager).obj, ((Message)faceManager).arg1);
            break;
          case 104:
            FaceManager.this.sendErrorResult(((Long)((Message)faceManager).obj).longValue(), ((Message)faceManager).arg1, ((Message)faceManager).arg2);
            break;
          case 103:
            FaceManager.this.sendAuthenticatedFailed();
            break;
          case 102:
            faceManager1 = FaceManager.this;
            face = (Face)((Message)faceManager).obj;
            i = ((Message)faceManager).arg1;
            j = ((Message)faceManager).arg2;
            bool2 = true;
            if (j != 1)
              bool2 = false; 
            faceManager1.sendAuthenticatedSucceeded(face, i, bool2);
            break;
          case 101:
            FaceManager.this.sendAcquiredResult(((Long)((Message)faceManager).obj).longValue(), ((Message)faceManager).arg1, ((Message)faceManager).arg2);
            break;
          case 100:
            FaceManager.this.sendEnrollResult((Face)((Message)faceManager).obj, ((Message)faceManager).arg1);
            break;
        } 
      } else {
        FaceManager.this.sendProgressResult(((Long)((Message)faceManager).obj).longValue(), ((Message)faceManager).arg1);
      } 
      Trace.endSection();
    }
  }
  
  private void sendSetFeatureCompleted(boolean paramBoolean, int paramInt) {
    SetFeatureCallback setFeatureCallback = this.mSetFeatureCallback;
    if (setFeatureCallback == null)
      return; 
    setFeatureCallback.onCompleted(paramBoolean, paramInt);
  }
  
  private void sendGetFeatureCompleted(boolean paramBoolean1, int paramInt, boolean paramBoolean2) {
    GetFeatureCallback getFeatureCallback = this.mGetFeatureCallback;
    if (getFeatureCallback == null)
      return; 
    getFeatureCallback.onCompleted(paramBoolean1, paramInt, paramBoolean2);
  }
  
  private void sendRemovedResult(Face paramFace, int paramInt) {
    RemovalCallback removalCallback = this.mRemovalCallback;
    if (removalCallback == null)
      return; 
    if (paramFace == null) {
      Log.e("FaceManager", "Received MSG_REMOVED, but face is null");
      return;
    } 
    removalCallback.onRemovalSucceeded(paramFace, paramInt);
  }
  
  private void sendErrorResult(long paramLong, int paramInt1, int paramInt2) {
    int i;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("sendErrorResult and errMsgId = ");
    stringBuilder.append(paramInt1);
    Slog.d("FaceManager", stringBuilder.toString());
    if (paramInt1 == 8) {
      i = paramInt2 + 1000;
    } else {
      i = paramInt1;
    } 
    EnrollmentCallback enrollmentCallback = this.mEnrollmentCallback;
    if (enrollmentCallback != null) {
      Context context = this.mContext;
      String str = getErrorString(context, paramInt1, paramInt2);
      enrollmentCallback.onEnrollmentError(i, str);
    } else {
      AuthenticationCallback authenticationCallback = this.mAuthenticationCallback;
      if (authenticationCallback != null) {
        Context context = this.mContext;
        String str = getErrorString(context, paramInt1, paramInt2);
        authenticationCallback.onAuthenticationError(i, str);
      } else {
        RemovalCallback removalCallback = this.mRemovalCallback;
        if (removalCallback != null) {
          Face face = this.mRemovalFace;
          Context context = this.mContext;
          String str = getErrorString(context, paramInt1, paramInt2);
          removalCallback.onRemovalError(face, i, str);
        } 
      } 
    } 
  }
  
  private void sendEnrollResult(Face paramFace, int paramInt) {
    if (this.mEnrollmentCallback != null) {
      Slog.d("FaceManager", "sendEnrollResult!");
      this.mEnrollmentCallback.onEnrollmentProgress(paramInt);
    } 
  }
  
  private void sendAuthenticatedSucceeded(Face paramFace, int paramInt, boolean paramBoolean) {
    if (this.mAuthenticationCallback != null) {
      Slog.d("FaceManager", "sendAuthenticatedSucceeded!");
      AuthenticationResult authenticationResult = new AuthenticationResult(this.mCryptoObject, paramFace, paramInt, paramBoolean);
      this.mAuthenticationCallback.onAuthenticationSucceeded(authenticationResult);
    } 
  }
  
  private void sendAuthenticatedFailed() {
    if (this.mAuthenticationCallback != null) {
      Slog.d("FaceManager", "sendAuthenticatedFailed!");
      this.mAuthenticationCallback.onAuthenticationFailed();
    } 
  }
  
  private void sendAcquiredResult(long paramLong, int paramInt1, int paramInt2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("sendAcquiredResult and acquireInfo = ");
    stringBuilder.append(paramInt1);
    Slog.d("FaceManager", stringBuilder.toString());
    AuthenticationCallback authenticationCallback = this.mAuthenticationCallback;
    if (authenticationCallback != null)
      authenticationCallback.onAuthenticationAcquired(paramInt1); 
    String str = getAcquiredString(this.mContext, paramInt1, paramInt2);
    if (paramInt1 == 22)
      paramInt1 = paramInt2 + 1000; 
    EnrollmentCallback enrollmentCallback = this.mEnrollmentCallback;
    if (enrollmentCallback != null) {
      enrollmentCallback.onEnrollmentHelp(paramInt1, str);
    } else {
      AuthenticationCallback authenticationCallback1 = this.mAuthenticationCallback;
      if (authenticationCallback1 != null && str != null)
        authenticationCallback1.onAuthenticationHelp(paramInt1, str); 
    } 
  }
  
  protected void sendProgressResult(long paramLong, int paramInt) {
    super.sendProgressResult(paramLong, paramInt);
  }
  
  protected AuthenticationCallback getAuthenticationCallback() {
    return this.mAuthenticationCallback;
  }
  
  protected EnrollmentCallback getEnrollmentCallback() {
    return this.mEnrollmentCallback;
  }
  
  protected void setCryptoObject(CryptoObject paramCryptoObject) {
    this.mCryptoObject = paramCryptoObject;
  }
  
  protected void setHandler(Handler paramHandler) {
    useHandler(paramHandler);
  }
  
  protected void setAuthenticationCallback(AuthenticationCallback paramAuthenticationCallback) {
    this.mAuthenticationCallback = paramAuthenticationCallback;
  }
  
  protected IFaceServiceReceiver getServiceReceiver() {
    return this.mServiceReceiver;
  }
}
