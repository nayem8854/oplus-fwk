package android.hardware.face;

import android.graphics.Rect;
import android.hardware.biometrics.IBiometricServiceLockoutResetCallback;
import android.hardware.biometrics.IBiometricServiceReceiverInternal;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.Surface;
import java.util.List;

public interface IFaceService extends IInterface {
  void addLockoutResetCallback(IBiometricServiceLockoutResetCallback paramIBiometricServiceLockoutResetCallback) throws RemoteException;
  
  void authenticate(IBinder paramIBinder, long paramLong, int paramInt1, IFaceServiceReceiver paramIFaceServiceReceiver, int paramInt2, String paramString) throws RemoteException;
  
  void authenticateAON(IBinder paramIBinder, long paramLong, int paramInt1, IFaceServiceReceiver paramIFaceServiceReceiver, int paramInt2, String paramString, byte[] paramArrayOfbyte) throws RemoteException;
  
  void cancelAONAuthentication(IBinder paramIBinder, String paramString) throws RemoteException;
  
  void cancelAuthentication(IBinder paramIBinder, String paramString) throws RemoteException;
  
  void cancelAuthenticationFromService(IBinder paramIBinder, String paramString, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) throws RemoteException;
  
  void cancelEnrollment(IBinder paramIBinder) throws RemoteException;
  
  void enroll(int paramInt, IBinder paramIBinder, byte[] paramArrayOfbyte, IFaceServiceReceiver paramIFaceServiceReceiver, String paramString, int[] paramArrayOfint) throws RemoteException;
  
  void enumerate(IBinder paramIBinder, int paramInt, IFaceServiceReceiver paramIFaceServiceReceiver) throws RemoteException;
  
  long generateChallenge(IBinder paramIBinder) throws RemoteException;
  
  long getAuthenticatorId(int paramInt) throws RemoteException;
  
  List<Face> getEnrolledFaces(int paramInt, String paramString) throws RemoteException;
  
  int getFaceProcessMemory() throws RemoteException;
  
  int getFailedAttempts(String paramString) throws RemoteException;
  
  void getFeature(int paramInt1, int paramInt2, IFaceServiceReceiver paramIFaceServiceReceiver, String paramString) throws RemoteException;
  
  long getLockoutAttemptDeadline(String paramString) throws RemoteException;
  
  int getPreviewHeight() throws RemoteException;
  
  int getPreviewWidth() throws RemoteException;
  
  boolean hasEnrolledFaces(int paramInt, String paramString) throws RemoteException;
  
  void initConfiguredStrength(int paramInt) throws RemoteException;
  
  boolean isHardwareDetected(String paramString) throws RemoteException;
  
  void prepareForAuthentication(boolean paramBoolean, IBinder paramIBinder, long paramLong, int paramInt1, IBiometricServiceReceiverInternal paramIBiometricServiceReceiverInternal, String paramString, int paramInt2, int paramInt3, int paramInt4, int paramInt5) throws RemoteException;
  
  void remove(IBinder paramIBinder, int paramInt1, int paramInt2, IFaceServiceReceiver paramIFaceServiceReceiver, String paramString) throws RemoteException;
  
  void rename(int paramInt, String paramString) throws RemoteException;
  
  void resetFaceDaemon() throws RemoteException;
  
  void resetLockout(byte[] paramArrayOfbyte) throws RemoteException;
  
  int revokeChallenge(IBinder paramIBinder) throws RemoteException;
  
  int sendFaceCmd(int paramInt, byte[] paramArrayOfbyte) throws RemoteException;
  
  void setActiveUser(int paramInt) throws RemoteException;
  
  void setFeature(int paramInt1, int paramInt2, boolean paramBoolean, byte[] paramArrayOfbyte, IFaceServiceReceiver paramIFaceServiceReceiver, String paramString) throws RemoteException;
  
  void setPreviewFrame(IBinder paramIBinder, Rect paramRect) throws RemoteException;
  
  int setPreviewSurface(IBinder paramIBinder, Surface paramSurface) throws RemoteException;
  
  void startPreparedClient(int paramInt) throws RemoteException;
  
  void userActivity() throws RemoteException;
  
  class Default implements IFaceService {
    public void authenticate(IBinder param1IBinder, long param1Long, int param1Int1, IFaceServiceReceiver param1IFaceServiceReceiver, int param1Int2, String param1String) throws RemoteException {}
    
    public void prepareForAuthentication(boolean param1Boolean, IBinder param1IBinder, long param1Long, int param1Int1, IBiometricServiceReceiverInternal param1IBiometricServiceReceiverInternal, String param1String, int param1Int2, int param1Int3, int param1Int4, int param1Int5) throws RemoteException {}
    
    public void startPreparedClient(int param1Int) throws RemoteException {}
    
    public void cancelAuthentication(IBinder param1IBinder, String param1String) throws RemoteException {}
    
    public void cancelAuthenticationFromService(IBinder param1IBinder, String param1String, int param1Int1, int param1Int2, int param1Int3, boolean param1Boolean) throws RemoteException {}
    
    public void enroll(int param1Int, IBinder param1IBinder, byte[] param1ArrayOfbyte, IFaceServiceReceiver param1IFaceServiceReceiver, String param1String, int[] param1ArrayOfint) throws RemoteException {}
    
    public void cancelEnrollment(IBinder param1IBinder) throws RemoteException {}
    
    public void remove(IBinder param1IBinder, int param1Int1, int param1Int2, IFaceServiceReceiver param1IFaceServiceReceiver, String param1String) throws RemoteException {}
    
    public void rename(int param1Int, String param1String) throws RemoteException {}
    
    public List<Face> getEnrolledFaces(int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public boolean isHardwareDetected(String param1String) throws RemoteException {
      return false;
    }
    
    public long generateChallenge(IBinder param1IBinder) throws RemoteException {
      return 0L;
    }
    
    public int revokeChallenge(IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public boolean hasEnrolledFaces(int param1Int, String param1String) throws RemoteException {
      return false;
    }
    
    public long getAuthenticatorId(int param1Int) throws RemoteException {
      return 0L;
    }
    
    public void resetLockout(byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void addLockoutResetCallback(IBiometricServiceLockoutResetCallback param1IBiometricServiceLockoutResetCallback) throws RemoteException {}
    
    public void setActiveUser(int param1Int) throws RemoteException {}
    
    public void enumerate(IBinder param1IBinder, int param1Int, IFaceServiceReceiver param1IFaceServiceReceiver) throws RemoteException {}
    
    public void setFeature(int param1Int1, int param1Int2, boolean param1Boolean, byte[] param1ArrayOfbyte, IFaceServiceReceiver param1IFaceServiceReceiver, String param1String) throws RemoteException {}
    
    public void getFeature(int param1Int1, int param1Int2, IFaceServiceReceiver param1IFaceServiceReceiver, String param1String) throws RemoteException {}
    
    public void userActivity() throws RemoteException {}
    
    public void initConfiguredStrength(int param1Int) throws RemoteException {}
    
    public void setPreviewFrame(IBinder param1IBinder, Rect param1Rect) throws RemoteException {}
    
    public int setPreviewSurface(IBinder param1IBinder, Surface param1Surface) throws RemoteException {
      return 0;
    }
    
    public long getLockoutAttemptDeadline(String param1String) throws RemoteException {
      return 0L;
    }
    
    public int getFailedAttempts(String param1String) throws RemoteException {
      return 0;
    }
    
    public int getPreviewWidth() throws RemoteException {
      return 0;
    }
    
    public int getPreviewHeight() throws RemoteException {
      return 0;
    }
    
    public void resetFaceDaemon() throws RemoteException {}
    
    public int getFaceProcessMemory() throws RemoteException {
      return 0;
    }
    
    public int sendFaceCmd(int param1Int, byte[] param1ArrayOfbyte) throws RemoteException {
      return 0;
    }
    
    public void authenticateAON(IBinder param1IBinder, long param1Long, int param1Int1, IFaceServiceReceiver param1IFaceServiceReceiver, int param1Int2, String param1String, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void cancelAONAuthentication(IBinder param1IBinder, String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IFaceService {
    private static final String DESCRIPTOR = "android.hardware.face.IFaceService";
    
    static final int TRANSACTION_addLockoutResetCallback = 17;
    
    static final int TRANSACTION_authenticate = 1;
    
    static final int TRANSACTION_authenticateAON = 33;
    
    static final int TRANSACTION_cancelAONAuthentication = 34;
    
    static final int TRANSACTION_cancelAuthentication = 4;
    
    static final int TRANSACTION_cancelAuthenticationFromService = 5;
    
    static final int TRANSACTION_cancelEnrollment = 7;
    
    static final int TRANSACTION_enroll = 6;
    
    static final int TRANSACTION_enumerate = 19;
    
    static final int TRANSACTION_generateChallenge = 12;
    
    static final int TRANSACTION_getAuthenticatorId = 15;
    
    static final int TRANSACTION_getEnrolledFaces = 10;
    
    static final int TRANSACTION_getFaceProcessMemory = 31;
    
    static final int TRANSACTION_getFailedAttempts = 27;
    
    static final int TRANSACTION_getFeature = 21;
    
    static final int TRANSACTION_getLockoutAttemptDeadline = 26;
    
    static final int TRANSACTION_getPreviewHeight = 29;
    
    static final int TRANSACTION_getPreviewWidth = 28;
    
    static final int TRANSACTION_hasEnrolledFaces = 14;
    
    static final int TRANSACTION_initConfiguredStrength = 23;
    
    static final int TRANSACTION_isHardwareDetected = 11;
    
    static final int TRANSACTION_prepareForAuthentication = 2;
    
    static final int TRANSACTION_remove = 8;
    
    static final int TRANSACTION_rename = 9;
    
    static final int TRANSACTION_resetFaceDaemon = 30;
    
    static final int TRANSACTION_resetLockout = 16;
    
    static final int TRANSACTION_revokeChallenge = 13;
    
    static final int TRANSACTION_sendFaceCmd = 32;
    
    static final int TRANSACTION_setActiveUser = 18;
    
    static final int TRANSACTION_setFeature = 20;
    
    static final int TRANSACTION_setPreviewFrame = 24;
    
    static final int TRANSACTION_setPreviewSurface = 25;
    
    static final int TRANSACTION_startPreparedClient = 3;
    
    static final int TRANSACTION_userActivity = 22;
    
    public Stub() {
      attachInterface(this, "android.hardware.face.IFaceService");
    }
    
    public static IFaceService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.face.IFaceService");
      if (iInterface != null && iInterface instanceof IFaceService)
        return (IFaceService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 34:
          return "cancelAONAuthentication";
        case 33:
          return "authenticateAON";
        case 32:
          return "sendFaceCmd";
        case 31:
          return "getFaceProcessMemory";
        case 30:
          return "resetFaceDaemon";
        case 29:
          return "getPreviewHeight";
        case 28:
          return "getPreviewWidth";
        case 27:
          return "getFailedAttempts";
        case 26:
          return "getLockoutAttemptDeadline";
        case 25:
          return "setPreviewSurface";
        case 24:
          return "setPreviewFrame";
        case 23:
          return "initConfiguredStrength";
        case 22:
          return "userActivity";
        case 21:
          return "getFeature";
        case 20:
          return "setFeature";
        case 19:
          return "enumerate";
        case 18:
          return "setActiveUser";
        case 17:
          return "addLockoutResetCallback";
        case 16:
          return "resetLockout";
        case 15:
          return "getAuthenticatorId";
        case 14:
          return "hasEnrolledFaces";
        case 13:
          return "revokeChallenge";
        case 12:
          return "generateChallenge";
        case 11:
          return "isHardwareDetected";
        case 10:
          return "getEnrolledFaces";
        case 9:
          return "rename";
        case 8:
          return "remove";
        case 7:
          return "cancelEnrollment";
        case 6:
          return "enroll";
        case 5:
          return "cancelAuthenticationFromService";
        case 4:
          return "cancelAuthentication";
        case 3:
          return "startPreparedClient";
        case 2:
          return "prepareForAuthentication";
        case 1:
          break;
      } 
      return "authenticate";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int j;
        boolean bool1;
        String str6;
        byte[] arrayOfByte2;
        String str5;
        IFaceServiceReceiver iFaceServiceReceiver1;
        IBiometricServiceLockoutResetCallback iBiometricServiceLockoutResetCallback;
        byte[] arrayOfByte1;
        String str4;
        IBinder iBinder2;
        String str3;
        List<Face> list;
        String str2;
        IBinder iBinder1;
        int[] arrayOfInt;
        IBinder iBinder5;
        IFaceServiceReceiver iFaceServiceReceiver3;
        byte[] arrayOfByte3;
        IBinder iBinder4;
        String str7;
        IBinder iBinder3;
        IFaceServiceReceiver iFaceServiceReceiver4;
        IBinder iBinder6;
        IBiometricServiceReceiverInternal iBiometricServiceReceiverInternal;
        String str9;
        IFaceServiceReceiver iFaceServiceReceiver5;
        byte[] arrayOfByte4;
        String str8;
        boolean bool;
        IFaceServiceReceiver iFaceServiceReceiver6;
        int k, m, n;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 34:
            param1Parcel1.enforceInterface("android.hardware.face.IFaceService");
            iBinder5 = param1Parcel1.readStrongBinder();
            str6 = param1Parcel1.readString();
            cancelAONAuthentication(iBinder5, str6);
            param1Parcel2.writeNoException();
            return true;
          case 33:
            str6.enforceInterface("android.hardware.face.IFaceService");
            iBinder5 = str6.readStrongBinder();
            l = str6.readLong();
            param1Int1 = str6.readInt();
            iFaceServiceReceiver4 = IFaceServiceReceiver.Stub.asInterface(str6.readStrongBinder());
            param1Int2 = str6.readInt();
            str9 = str6.readString();
            arrayOfByte2 = str6.createByteArray();
            authenticateAON(iBinder5, l, param1Int1, iFaceServiceReceiver4, param1Int2, str9, arrayOfByte2);
            param1Parcel2.writeNoException();
            return true;
          case 32:
            arrayOfByte2.enforceInterface("android.hardware.face.IFaceService");
            param1Int1 = arrayOfByte2.readInt();
            arrayOfByte2 = arrayOfByte2.createByteArray();
            param1Int1 = sendFaceCmd(param1Int1, arrayOfByte2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 31:
            arrayOfByte2.enforceInterface("android.hardware.face.IFaceService");
            param1Int1 = getFaceProcessMemory();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 30:
            arrayOfByte2.enforceInterface("android.hardware.face.IFaceService");
            resetFaceDaemon();
            param1Parcel2.writeNoException();
            return true;
          case 29:
            arrayOfByte2.enforceInterface("android.hardware.face.IFaceService");
            param1Int1 = getPreviewHeight();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 28:
            arrayOfByte2.enforceInterface("android.hardware.face.IFaceService");
            param1Int1 = getPreviewWidth();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 27:
            arrayOfByte2.enforceInterface("android.hardware.face.IFaceService");
            str5 = arrayOfByte2.readString();
            param1Int1 = getFailedAttempts(str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 26:
            str5.enforceInterface("android.hardware.face.IFaceService");
            str5 = str5.readString();
            l = getLockoutAttemptDeadline(str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 25:
            str5.enforceInterface("android.hardware.face.IFaceService");
            iBinder5 = str5.readStrongBinder();
            if (str5.readInt() != 0) {
              Surface surface = (Surface)Surface.CREATOR.createFromParcel((Parcel)str5);
            } else {
              str5 = null;
            } 
            param1Int1 = setPreviewSurface(iBinder5, (Surface)str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 24:
            str5.enforceInterface("android.hardware.face.IFaceService");
            iBinder5 = str5.readStrongBinder();
            if (str5.readInt() != 0) {
              Rect rect = (Rect)Rect.CREATOR.createFromParcel((Parcel)str5);
            } else {
              str5 = null;
            } 
            setPreviewFrame(iBinder5, (Rect)str5);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            str5.enforceInterface("android.hardware.face.IFaceService");
            param1Int1 = str5.readInt();
            initConfiguredStrength(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            str5.enforceInterface("android.hardware.face.IFaceService");
            userActivity();
            param1Parcel2.writeNoException();
            return true;
          case 21:
            str5.enforceInterface("android.hardware.face.IFaceService");
            param1Int1 = str5.readInt();
            param1Int2 = str5.readInt();
            iFaceServiceReceiver3 = IFaceServiceReceiver.Stub.asInterface(str5.readStrongBinder());
            str5 = str5.readString();
            getFeature(param1Int1, param1Int2, iFaceServiceReceiver3, str5);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            str5.enforceInterface("android.hardware.face.IFaceService");
            param1Int2 = str5.readInt();
            param1Int1 = str5.readInt();
            if (str5.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            arrayOfByte3 = str5.createByteArray();
            iFaceServiceReceiver5 = IFaceServiceReceiver.Stub.asInterface(str5.readStrongBinder());
            str5 = str5.readString();
            setFeature(param1Int2, param1Int1, bool, arrayOfByte3, iFaceServiceReceiver5, str5);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            str5.enforceInterface("android.hardware.face.IFaceService");
            iBinder4 = str5.readStrongBinder();
            param1Int1 = str5.readInt();
            iFaceServiceReceiver1 = IFaceServiceReceiver.Stub.asInterface(str5.readStrongBinder());
            enumerate(iBinder4, param1Int1, iFaceServiceReceiver1);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            iFaceServiceReceiver1.enforceInterface("android.hardware.face.IFaceService");
            param1Int1 = iFaceServiceReceiver1.readInt();
            setActiveUser(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            iFaceServiceReceiver1.enforceInterface("android.hardware.face.IFaceService");
            iBiometricServiceLockoutResetCallback = IBiometricServiceLockoutResetCallback.Stub.asInterface(iFaceServiceReceiver1.readStrongBinder());
            addLockoutResetCallback(iBiometricServiceLockoutResetCallback);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            iBiometricServiceLockoutResetCallback.enforceInterface("android.hardware.face.IFaceService");
            arrayOfByte1 = iBiometricServiceLockoutResetCallback.createByteArray();
            resetLockout(arrayOfByte1);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            arrayOfByte1.enforceInterface("android.hardware.face.IFaceService");
            param1Int1 = arrayOfByte1.readInt();
            l = getAuthenticatorId(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 14:
            arrayOfByte1.enforceInterface("android.hardware.face.IFaceService");
            param1Int1 = arrayOfByte1.readInt();
            str4 = arrayOfByte1.readString();
            bool2 = hasEnrolledFaces(param1Int1, str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 13:
            str4.enforceInterface("android.hardware.face.IFaceService");
            iBinder2 = str4.readStrongBinder();
            j = revokeChallenge(iBinder2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 12:
            iBinder2.enforceInterface("android.hardware.face.IFaceService");
            iBinder2 = iBinder2.readStrongBinder();
            l = generateChallenge(iBinder2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 11:
            iBinder2.enforceInterface("android.hardware.face.IFaceService");
            str3 = iBinder2.readString();
            bool1 = isHardwareDetected(str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 10:
            str3.enforceInterface("android.hardware.face.IFaceService");
            i = str3.readInt();
            str3 = str3.readString();
            list = getEnrolledFaces(i, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 9:
            list.enforceInterface("android.hardware.face.IFaceService");
            i = list.readInt();
            str2 = list.readString();
            rename(i, str2);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            str2.enforceInterface("android.hardware.face.IFaceService");
            iBinder4 = str2.readStrongBinder();
            param1Int2 = str2.readInt();
            i = str2.readInt();
            iFaceServiceReceiver5 = IFaceServiceReceiver.Stub.asInterface(str2.readStrongBinder());
            str2 = str2.readString();
            remove(iBinder4, param1Int2, i, iFaceServiceReceiver5, str2);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            str2.enforceInterface("android.hardware.face.IFaceService");
            iBinder1 = str2.readStrongBinder();
            cancelEnrollment(iBinder1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            iBinder1.enforceInterface("android.hardware.face.IFaceService");
            i = iBinder1.readInt();
            iBinder6 = iBinder1.readStrongBinder();
            arrayOfByte4 = iBinder1.createByteArray();
            iFaceServiceReceiver6 = IFaceServiceReceiver.Stub.asInterface(iBinder1.readStrongBinder());
            str7 = iBinder1.readString();
            arrayOfInt = iBinder1.createIntArray();
            enroll(i, iBinder6, arrayOfByte4, iFaceServiceReceiver6, str7, arrayOfInt);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            arrayOfInt.enforceInterface("android.hardware.face.IFaceService");
            iBinder3 = arrayOfInt.readStrongBinder();
            str8 = arrayOfInt.readString();
            k = arrayOfInt.readInt();
            param1Int2 = arrayOfInt.readInt();
            i = arrayOfInt.readInt();
            if (arrayOfInt.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            cancelAuthenticationFromService(iBinder3, str8, k, param1Int2, i, bool);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            arrayOfInt.enforceInterface("android.hardware.face.IFaceService");
            iBinder3 = arrayOfInt.readStrongBinder();
            str1 = arrayOfInt.readString();
            cancelAuthentication(iBinder3, str1);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            str1.enforceInterface("android.hardware.face.IFaceService");
            i = str1.readInt();
            startPreparedClient(i);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str1.enforceInterface("android.hardware.face.IFaceService");
            if (str1.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            iBinder3 = str1.readStrongBinder();
            l = str1.readLong();
            m = str1.readInt();
            iBiometricServiceReceiverInternal = IBiometricServiceReceiverInternal.Stub.asInterface(str1.readStrongBinder());
            str8 = str1.readString();
            n = str1.readInt();
            k = str1.readInt();
            param1Int2 = str1.readInt();
            i = str1.readInt();
            prepareForAuthentication(bool, iBinder3, l, m, iBiometricServiceReceiverInternal, str8, n, k, param1Int2, i);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.hardware.face.IFaceService");
        IBinder iBinder7 = str1.readStrongBinder();
        long l = str1.readLong();
        int i = str1.readInt();
        IFaceServiceReceiver iFaceServiceReceiver2 = IFaceServiceReceiver.Stub.asInterface(str1.readStrongBinder());
        param1Int2 = str1.readInt();
        String str1 = str1.readString();
        authenticate(iBinder7, l, i, iFaceServiceReceiver2, param1Int2, str1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.hardware.face.IFaceService");
      return true;
    }
    
    private static class Proxy implements IFaceService {
      public static IFaceService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.face.IFaceService";
      }
      
      public void authenticate(IBinder param2IBinder, long param2Long, int param2Int1, IFaceServiceReceiver param2IFaceServiceReceiver, int param2Int2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          try {
            parcel1.writeStrongBinder(param2IBinder);
            try {
              parcel1.writeLong(param2Long);
              try {
                IBinder iBinder;
                parcel1.writeInt(param2Int1);
                if (param2IFaceServiceReceiver != null) {
                  iBinder = param2IFaceServiceReceiver.asBinder();
                } else {
                  iBinder = null;
                } 
                parcel1.writeStrongBinder(iBinder);
                try {
                  parcel1.writeInt(param2Int2);
                  parcel1.writeString(param2String);
                  boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
                  if (!bool && IFaceService.Stub.getDefaultImpl() != null) {
                    IFaceService.Stub.getDefaultImpl().authenticate(param2IBinder, param2Long, param2Int1, param2IFaceServiceReceiver, param2Int2, param2String);
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } 
                  parcel2.readException();
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IBinder;
      }
      
      public void prepareForAuthentication(boolean param2Boolean, IBinder param2IBinder, long param2Long, int param2Int1, IBiometricServiceReceiverInternal param2IBiometricServiceReceiverInternal, String param2String, int param2Int2, int param2Int3, int param2Int4, int param2Int5) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          try {
            IBinder iBinder;
            parcel1.writeStrongBinder(param2IBinder);
            parcel1.writeLong(param2Long);
            parcel1.writeInt(param2Int1);
            if (param2IBiometricServiceReceiverInternal != null) {
              iBinder = param2IBiometricServiceReceiverInternal.asBinder();
            } else {
              iBinder = null;
            } 
            parcel1.writeStrongBinder(iBinder);
            parcel1.writeString(param2String);
            parcel1.writeInt(param2Int2);
            parcel1.writeInt(param2Int3);
            parcel1.writeInt(param2Int4);
            parcel1.writeInt(param2Int5);
            boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
            if (!bool1 && IFaceService.Stub.getDefaultImpl() != null) {
              IFaceService.Stub.getDefaultImpl().prepareForAuthentication(param2Boolean, param2IBinder, param2Long, param2Int1, param2IBiometricServiceReceiverInternal, param2String, param2Int2, param2Int3, param2Int4, param2Int5);
              parcel2.recycle();
              parcel1.recycle();
              return;
            } 
            parcel2.readException();
            parcel2.recycle();
            parcel1.recycle();
            return;
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IBinder;
      }
      
      public void startPreparedClient(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null) {
            IFaceService.Stub.getDefaultImpl().startPreparedClient(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelAuthentication(IBinder param2IBinder, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null) {
            IFaceService.Stub.getDefaultImpl().cancelAuthentication(param2IBinder, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelAuthenticationFromService(IBinder param2IBinder, String param2String, int param2Int1, int param2Int2, int param2Int3, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          try {
            parcel1.writeStrongBinder(param2IBinder);
            try {
              parcel1.writeString(param2String);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  parcel1.writeInt(param2Int2);
                  try {
                    boolean bool;
                    parcel1.writeInt(param2Int3);
                    if (param2Boolean) {
                      bool = true;
                    } else {
                      bool = false;
                    } 
                    parcel1.writeInt(bool);
                    try {
                      boolean bool1 = this.mRemote.transact(5, parcel1, parcel2, 0);
                      if (!bool1 && IFaceService.Stub.getDefaultImpl() != null) {
                        IFaceService.Stub.getDefaultImpl().cancelAuthenticationFromService(param2IBinder, param2String, param2Int1, param2Int2, param2Int3, param2Boolean);
                        parcel2.recycle();
                        parcel1.recycle();
                        return;
                      } 
                      parcel2.readException();
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IBinder;
      }
      
      public void enroll(int param2Int, IBinder param2IBinder, byte[] param2ArrayOfbyte, IFaceServiceReceiver param2IFaceServiceReceiver, String param2String, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          try {
            parcel1.writeInt(param2Int);
            try {
              parcel1.writeStrongBinder(param2IBinder);
              try {
                IBinder iBinder;
                parcel1.writeByteArray(param2ArrayOfbyte);
                if (param2IFaceServiceReceiver != null) {
                  iBinder = param2IFaceServiceReceiver.asBinder();
                } else {
                  iBinder = null;
                } 
                parcel1.writeStrongBinder(iBinder);
                try {
                  parcel1.writeString(param2String);
                  try {
                    parcel1.writeIntArray(param2ArrayOfint);
                    try {
                      boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
                      if (!bool && IFaceService.Stub.getDefaultImpl() != null) {
                        IFaceService.Stub.getDefaultImpl().enroll(param2Int, param2IBinder, param2ArrayOfbyte, param2IFaceServiceReceiver, param2String, param2ArrayOfint);
                        parcel2.recycle();
                        parcel1.recycle();
                        return;
                      } 
                      parcel2.readException();
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IBinder;
      }
      
      public void cancelEnrollment(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null) {
            IFaceService.Stub.getDefaultImpl().cancelEnrollment(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void remove(IBinder param2IBinder, int param2Int1, int param2Int2, IFaceServiceReceiver param2IFaceServiceReceiver, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2IFaceServiceReceiver != null) {
            iBinder = param2IFaceServiceReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null) {
            IFaceService.Stub.getDefaultImpl().remove(param2IBinder, param2Int1, param2Int2, param2IFaceServiceReceiver, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void rename(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null) {
            IFaceService.Stub.getDefaultImpl().rename(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<Face> getEnrolledFaces(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null)
            return IFaceService.Stub.getDefaultImpl().getEnrolledFaces(param2Int, param2String); 
          parcel2.readException();
          return parcel2.createTypedArrayList(Face.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isHardwareDetected(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(11, parcel1, parcel2, 0);
          if (!bool2 && IFaceService.Stub.getDefaultImpl() != null) {
            bool1 = IFaceService.Stub.getDefaultImpl().isHardwareDetected(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long generateChallenge(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null)
            return IFaceService.Stub.getDefaultImpl().generateChallenge(param2IBinder); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int revokeChallenge(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null)
            return IFaceService.Stub.getDefaultImpl().revokeChallenge(param2IBinder); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasEnrolledFaces(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(14, parcel1, parcel2, 0);
          if (!bool2 && IFaceService.Stub.getDefaultImpl() != null) {
            bool1 = IFaceService.Stub.getDefaultImpl().hasEnrolledFaces(param2Int, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getAuthenticatorId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null)
            return IFaceService.Stub.getDefaultImpl().getAuthenticatorId(param2Int); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resetLockout(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null) {
            IFaceService.Stub.getDefaultImpl().resetLockout(param2ArrayOfbyte);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addLockoutResetCallback(IBiometricServiceLockoutResetCallback param2IBiometricServiceLockoutResetCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          if (param2IBiometricServiceLockoutResetCallback != null) {
            iBinder = param2IBiometricServiceLockoutResetCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null) {
            IFaceService.Stub.getDefaultImpl().addLockoutResetCallback(param2IBiometricServiceLockoutResetCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setActiveUser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null) {
            IFaceService.Stub.getDefaultImpl().setActiveUser(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enumerate(IBinder param2IBinder, int param2Int, IFaceServiceReceiver param2IFaceServiceReceiver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          if (param2IFaceServiceReceiver != null) {
            iBinder = param2IFaceServiceReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null) {
            IFaceService.Stub.getDefaultImpl().enumerate(param2IBinder, param2Int, param2IFaceServiceReceiver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setFeature(int param2Int1, int param2Int2, boolean param2Boolean, byte[] param2ArrayOfbyte, IFaceServiceReceiver param2IFaceServiceReceiver, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          try {
            parcel1.writeInt(param2Int1);
            try {
              boolean bool;
              parcel1.writeInt(param2Int2);
              if (param2Boolean) {
                bool = true;
              } else {
                bool = false;
              } 
              parcel1.writeInt(bool);
              try {
                IBinder iBinder;
                parcel1.writeByteArray(param2ArrayOfbyte);
                if (param2IFaceServiceReceiver != null) {
                  iBinder = param2IFaceServiceReceiver.asBinder();
                } else {
                  iBinder = null;
                } 
                parcel1.writeStrongBinder(iBinder);
                try {
                  parcel1.writeString(param2String);
                  try {
                    boolean bool1 = this.mRemote.transact(20, parcel1, parcel2, 0);
                    if (!bool1 && IFaceService.Stub.getDefaultImpl() != null) {
                      IFaceService.Stub.getDefaultImpl().setFeature(param2Int1, param2Int2, param2Boolean, param2ArrayOfbyte, param2IFaceServiceReceiver, param2String);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2ArrayOfbyte;
      }
      
      public void getFeature(int param2Int1, int param2Int2, IFaceServiceReceiver param2IFaceServiceReceiver, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2IFaceServiceReceiver != null) {
            iBinder = param2IFaceServiceReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null) {
            IFaceService.Stub.getDefaultImpl().getFeature(param2Int1, param2Int2, param2IFaceServiceReceiver, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void userActivity() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null) {
            IFaceService.Stub.getDefaultImpl().userActivity();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void initConfiguredStrength(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null) {
            IFaceService.Stub.getDefaultImpl().initConfiguredStrength(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPreviewFrame(IBinder param2IBinder, Rect param2Rect) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Rect != null) {
            parcel1.writeInt(1);
            param2Rect.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null) {
            IFaceService.Stub.getDefaultImpl().setPreviewFrame(param2IBinder, param2Rect);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setPreviewSurface(IBinder param2IBinder, Surface param2Surface) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Surface != null) {
            parcel1.writeInt(1);
            param2Surface.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null)
            return IFaceService.Stub.getDefaultImpl().setPreviewSurface(param2IBinder, param2Surface); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getLockoutAttemptDeadline(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null)
            return IFaceService.Stub.getDefaultImpl().getLockoutAttemptDeadline(param2String); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getFailedAttempts(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null)
            return IFaceService.Stub.getDefaultImpl().getFailedAttempts(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPreviewWidth() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null)
            return IFaceService.Stub.getDefaultImpl().getPreviewWidth(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPreviewHeight() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null)
            return IFaceService.Stub.getDefaultImpl().getPreviewHeight(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resetFaceDaemon() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null) {
            IFaceService.Stub.getDefaultImpl().resetFaceDaemon();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getFaceProcessMemory() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null)
            return IFaceService.Stub.getDefaultImpl().getFaceProcessMemory(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int sendFaceCmd(int param2Int, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeInt(param2Int);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null) {
            param2Int = IFaceService.Stub.getDefaultImpl().sendFaceCmd(param2Int, param2ArrayOfbyte);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void authenticateAON(IBinder param2IBinder, long param2Long, int param2Int1, IFaceServiceReceiver param2IFaceServiceReceiver, int param2Int2, String param2String, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          try {
            parcel1.writeStrongBinder(param2IBinder);
            try {
              parcel1.writeLong(param2Long);
              try {
                IBinder iBinder;
                parcel1.writeInt(param2Int1);
                if (param2IFaceServiceReceiver != null) {
                  iBinder = param2IFaceServiceReceiver.asBinder();
                } else {
                  iBinder = null;
                } 
                parcel1.writeStrongBinder(iBinder);
                parcel1.writeInt(param2Int2);
                parcel1.writeString(param2String);
                parcel1.writeByteArray(param2ArrayOfbyte);
                boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
                if (!bool && IFaceService.Stub.getDefaultImpl() != null) {
                  IFaceService.Stub.getDefaultImpl().authenticateAON(param2IBinder, param2Long, param2Int1, param2IFaceServiceReceiver, param2Int2, param2String, param2ArrayOfbyte);
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } 
                parcel2.readException();
                parcel2.recycle();
                parcel1.recycle();
                return;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IBinder;
      }
      
      public void cancelAONAuthentication(IBinder param2IBinder, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && IFaceService.Stub.getDefaultImpl() != null) {
            IFaceService.Stub.getDefaultImpl().cancelAONAuthentication(param2IBinder, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IFaceService param1IFaceService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IFaceService != null) {
          Proxy.sDefaultImpl = param1IFaceService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IFaceService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
