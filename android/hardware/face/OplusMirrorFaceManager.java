package android.hardware.face;

import com.oplus.reflect.RefClass;
import com.oplus.reflect.RefMethod;

public class OplusMirrorFaceManager {
  public static Class<?> TYPE = RefClass.load(OplusMirrorFaceManager.class, FaceManager.class);
  
  public static RefMethod<Integer> getFailedAttempts;
  
  public static RefMethod<Long> getLockoutAttemptDeadline;
}
