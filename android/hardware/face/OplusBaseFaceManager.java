package android.hardware.face;

import android.content.Context;
import android.graphics.Rect;
import android.hardware.biometrics.CryptoObject;
import android.os.Binder;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.util.Slog;
import android.view.Surface;

public abstract class OplusBaseFaceManager {
  private static final boolean DEBUG = true;
  
  public static final int FACE_START_BY_AON = 11;
  
  public static final int FACE_START_BY_FACE_RETRY = 10;
  
  public static final int FACE_START_BY_FINGERPRINT_TOUCH = 8;
  
  public static final int FACE_START_BY_OTHER_WAY = 7;
  
  public static final int FACE_START_BY_USER_SWITCHED = 9;
  
  public static final int FACE_START_CLICK_LOCKICON = 2;
  
  public static final int FACE_START_ENTER_INTO_SECURITY_CONTAINER = 1;
  
  public static final int FACE_START_GESTURE_DOUBLE_TAP_SCREEN = 5;
  
  public static final int FACE_START_GESTURE_LIFT_HAND = 6;
  
  public static final int FACE_START_OCCLUDED_CHANGE_TO_FALSE = 3;
  
  public static final int FACE_START_SCREEN_ON = 0;
  
  public static final int FACE_START_SLIDE_UP_INTO_BIOMETRIC_BOUNCER = 4;
  
  private static final String TAG = "OplusBaseFaceManager";
  
  private FaceManager.AuthenticationCallback mAuthenticationCallback;
  
  private final Context mContext;
  
  private FaceManager.EnrollmentCallback mEnrollmentCallback;
  
  private IFaceService mService;
  
  private IBinder mToken = (IBinder)new Binder();
  
  public OplusBaseFaceManager(Context paramContext, IFaceService paramIFaceService) {
    this.mContext = paramContext;
    this.mService = paramIFaceService;
    if (paramIFaceService == null)
      Slog.v("OplusBaseFaceManager", "FaceAuthenticationManagerService was null"); 
  }
  
  public void setPreviewFrame(Rect paramRect) {
    IFaceService iFaceService = this.mService;
    if (iFaceService != null)
      try {
        iFaceService.setPreviewFrame(this.mToken, paramRect);
      } catch (RemoteException remoteException) {
        Log.w("OplusBaseFaceManager", "Remote exception in setPreviewFrame: ", (Throwable)remoteException);
      }  
  }
  
  public int setPreviewSurface(Surface paramSurface) {
    int i = -1;
    IFaceService iFaceService = this.mService;
    if (iFaceService != null)
      try {
        i = iFaceService.setPreviewSurface(this.mToken, paramSurface);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    return i;
  }
  
  public int getPreviewWidth() {
    IFaceService iFaceService = this.mService;
    if (iFaceService != null)
      try {
        return iFaceService.getPreviewWidth();
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    return 0;
  }
  
  public int getPreviewHeight() {
    IFaceService iFaceService = this.mService;
    if (iFaceService != null)
      try {
        return iFaceService.getPreviewHeight();
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    return 0;
  }
  
  public void resetFaceDaemon() {
    IFaceService iFaceService = this.mService;
    if (iFaceService != null) {
      try {
        iFaceService.resetFaceDaemon();
      } catch (RemoteException remoteException) {
        Log.v("OplusBaseFaceManager", "Remote exception in resetFaceDaemon: ", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusBaseFaceManager", "resetFaceDaemon: Service not connected!");
    } 
  }
  
  public int getFaceProcessMemory() {
    IFaceService iFaceService = this.mService;
    if (iFaceService != null)
      try {
        return iFaceService.getFaceProcessMemory();
      } catch (RemoteException remoteException) {
        Log.v("OplusBaseFaceManager", "Remote exception in getFaceProcessMemory: ", (Throwable)remoteException);
        return -1;
      }  
    Log.w("OplusBaseFaceManager", "getFaceProcessMemory: Service not connected!");
    return -1;
  }
  
  public long getLockoutAttemptDeadline() {
    IFaceService iFaceService = this.mService;
    if (iFaceService != null) {
      try {
        return iFaceService.getLockoutAttemptDeadline(this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        Log.v("OplusBaseFaceManager", "Remote exception in getLockoutAttemptDeadline(): ", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusBaseFaceManager", "getLockoutAttemptDeadline(): Service not connected!");
    } 
    return -1L;
  }
  
  public int getFailedAttempts() {
    IFaceService iFaceService = this.mService;
    if (iFaceService != null) {
      try {
        return iFaceService.getFailedAttempts(this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        Log.v("OplusBaseFaceManager", "Remote exception in getFailedAttempts(): ", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusBaseFaceManager", "getFailedAttempts(): Service not connected!");
    } 
    return -1;
  }
  
  protected void sendProgressResult(long paramLong, int paramInt) {
    this.mEnrollmentCallback = getEnrollmentCallback();
    this.mAuthenticationCallback = getAuthenticationCallback();
    Slog.d("OplusBaseFaceManager", "sendProgressResult");
    FaceManager.EnrollmentCallback enrollmentCallback = this.mEnrollmentCallback;
    if (enrollmentCallback != null) {
      enrollmentCallback.onProgressChanged(paramInt);
    } else {
      FaceManager.AuthenticationCallback authenticationCallback = this.mAuthenticationCallback;
      if (authenticationCallback != null)
        authenticationCallback.onProgressChanged(paramInt); 
    } 
  }
  
  public int sendFaceCmd(int paramInt, byte[] paramArrayOfbyte) {
    byte b = -1;
    IFaceService iFaceService = this.mService;
    if (iFaceService != null) {
      try {
        paramInt = iFaceService.sendFaceCmd(paramInt, paramArrayOfbyte);
      } catch (RemoteException remoteException) {
        Log.v("OplusBaseFaceManager", "Remote exception in sendFaceCmd(): ", (Throwable)remoteException);
        paramInt = b;
      } 
    } else {
      Log.w("OplusBaseFaceManager", "sendFaceCmd(): Service not connected!");
      paramInt = b;
    } 
    return paramInt;
  }
  
  public void authenticateAON(CryptoObject paramCryptoObject, CancellationSignal paramCancellationSignal, int paramInt1, FaceManager.AuthenticationCallback paramAuthenticationCallback, int paramInt2, byte[] paramArrayOfbyte, Handler paramHandler) {
    if (paramAuthenticationCallback != null) {
      if (paramCancellationSignal != null) {
        if (paramCancellationSignal.isCanceled()) {
          Log.w("OplusBaseFaceManager", "aonAuthentication already canceled");
          return;
        } 
        paramCancellationSignal.setOnCancelListener(new OnAONAuthenticationCancelListener(paramCryptoObject));
      } 
      if (this.mService != null)
        try {
          long l;
          setHandler(paramHandler);
          setAuthenticationCallback(paramAuthenticationCallback);
          setCryptoObject(paramCryptoObject);
          if (paramCryptoObject != null) {
            l = paramCryptoObject.getOpId();
          } else {
            l = 0L;
          } 
          IFaceService iFaceService = this.mService;
          IBinder iBinder = this.mToken;
          IFaceServiceReceiver iFaceServiceReceiver = getServiceReceiver();
          Context context = this.mContext;
          String str = context.getOpPackageName();
          iFaceService.authenticateAON(iBinder, l, paramInt2, iFaceServiceReceiver, paramInt1, str, paramArrayOfbyte);
        } catch (RemoteException remoteException) {
          Log.w("OplusBaseFaceManager", "Remote exception while authenticating: ", (Throwable)remoteException);
          if (paramAuthenticationCallback != null)
            paramAuthenticationCallback.onAuthenticationError(1, this.mContext.getString(17040191)); 
        }  
      return;
    } 
    throw new IllegalArgumentException("Must supply an authentication callback");
  }
  
  private void cancelAONAuthentication(CryptoObject paramCryptoObject) {
    if (this.mService != null)
      try {
        Slog.d("OplusBaseFaceManager", "FaceManager#cancelAONAuthentication");
        this.mService.cancelAuthentication(this.mToken, this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
  }
  
  protected abstract FaceManager.AuthenticationCallback getAuthenticationCallback();
  
  protected abstract FaceManager.EnrollmentCallback getEnrollmentCallback();
  
  protected abstract IFaceServiceReceiver getServiceReceiver();
  
  protected abstract void setAuthenticationCallback(FaceManager.AuthenticationCallback paramAuthenticationCallback);
  
  protected abstract void setCryptoObject(CryptoObject paramCryptoObject);
  
  protected abstract void setHandler(Handler paramHandler);
  
  class OnAONAuthenticationCancelListener implements CancellationSignal.OnCancelListener {
    private CryptoObject mCrypto;
    
    final OplusBaseFaceManager this$0;
    
    OnAONAuthenticationCancelListener(CryptoObject param1CryptoObject) {
      this.mCrypto = param1CryptoObject;
    }
    
    public void onCancel() {
      OplusBaseFaceManager.this.cancelAONAuthentication(this.mCrypto);
    }
  }
}
