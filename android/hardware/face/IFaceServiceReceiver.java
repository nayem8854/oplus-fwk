package android.hardware.face;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IFaceServiceReceiver extends IInterface {
  void onAcquired(long paramLong, int paramInt1, int paramInt2) throws RemoteException;
  
  void onAuthenticationFailed(long paramLong) throws RemoteException;
  
  void onAuthenticationSucceeded(long paramLong, Face paramFace, int paramInt, boolean paramBoolean) throws RemoteException;
  
  void onEnrollResult(long paramLong, int paramInt1, int paramInt2) throws RemoteException;
  
  void onEnumerated(long paramLong, int paramInt1, int paramInt2) throws RemoteException;
  
  void onError(long paramLong, int paramInt1, int paramInt2) throws RemoteException;
  
  void onFeatureGet(boolean paramBoolean1, int paramInt, boolean paramBoolean2) throws RemoteException;
  
  void onFeatureSet(boolean paramBoolean, int paramInt) throws RemoteException;
  
  void onProgressChanged(long paramLong, int paramInt) throws RemoteException;
  
  void onRemoved(long paramLong, int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements IFaceServiceReceiver {
    public void onEnrollResult(long param1Long, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onAcquired(long param1Long, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onAuthenticationSucceeded(long param1Long, Face param1Face, int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void onAuthenticationFailed(long param1Long) throws RemoteException {}
    
    public void onError(long param1Long, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onRemoved(long param1Long, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onEnumerated(long param1Long, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onFeatureSet(boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public void onFeatureGet(boolean param1Boolean1, int param1Int, boolean param1Boolean2) throws RemoteException {}
    
    public void onProgressChanged(long param1Long, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IFaceServiceReceiver {
    private static final String DESCRIPTOR = "android.hardware.face.IFaceServiceReceiver";
    
    static final int TRANSACTION_onAcquired = 2;
    
    static final int TRANSACTION_onAuthenticationFailed = 4;
    
    static final int TRANSACTION_onAuthenticationSucceeded = 3;
    
    static final int TRANSACTION_onEnrollResult = 1;
    
    static final int TRANSACTION_onEnumerated = 7;
    
    static final int TRANSACTION_onError = 5;
    
    static final int TRANSACTION_onFeatureGet = 9;
    
    static final int TRANSACTION_onFeatureSet = 8;
    
    static final int TRANSACTION_onProgressChanged = 10;
    
    static final int TRANSACTION_onRemoved = 6;
    
    public Stub() {
      attachInterface(this, "android.hardware.face.IFaceServiceReceiver");
    }
    
    public static IFaceServiceReceiver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.face.IFaceServiceReceiver");
      if (iInterface != null && iInterface instanceof IFaceServiceReceiver)
        return (IFaceServiceReceiver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 10:
          return "onProgressChanged";
        case 9:
          return "onFeatureGet";
        case 8:
          return "onFeatureSet";
        case 7:
          return "onEnumerated";
        case 6:
          return "onRemoved";
        case 5:
          return "onError";
        case 4:
          return "onAuthenticationFailed";
        case 3:
          return "onAuthenticationSucceeded";
        case 2:
          return "onAcquired";
        case 1:
          break;
      } 
      return "onEnrollResult";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        Face face;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 10:
            param1Parcel1.enforceInterface("android.hardware.face.IFaceServiceReceiver");
            l = param1Parcel1.readLong();
            param1Int1 = param1Parcel1.readInt();
            onProgressChanged(l, param1Int1);
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.hardware.face.IFaceServiceReceiver");
            if (param1Parcel1.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0)
              bool2 = true; 
            onFeatureGet(bool1, param1Int1, bool2);
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.hardware.face.IFaceServiceReceiver");
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            param1Int1 = param1Parcel1.readInt();
            onFeatureSet(bool1, param1Int1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.hardware.face.IFaceServiceReceiver");
            l = param1Parcel1.readLong();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            onEnumerated(l, param1Int2, param1Int1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.hardware.face.IFaceServiceReceiver");
            l = param1Parcel1.readLong();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            onRemoved(l, param1Int2, param1Int1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.hardware.face.IFaceServiceReceiver");
            l = param1Parcel1.readLong();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            onError(l, param1Int2, param1Int1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.hardware.face.IFaceServiceReceiver");
            l = param1Parcel1.readLong();
            onAuthenticationFailed(l);
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.hardware.face.IFaceServiceReceiver");
            l = param1Parcel1.readLong();
            if (param1Parcel1.readInt() != 0) {
              face = (Face)Face.CREATOR.createFromParcel(param1Parcel1);
            } else {
              face = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            onAuthenticationSucceeded(l, face, param1Int1, bool1);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            param1Parcel1.enforceInterface("android.hardware.face.IFaceServiceReceiver");
            l = param1Parcel1.readLong();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            onAcquired(l, param1Int2, param1Int1);
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("android.hardware.face.IFaceServiceReceiver");
        long l = param1Parcel1.readLong();
        param1Int1 = param1Parcel1.readInt();
        param1Int2 = param1Parcel1.readInt();
        onEnrollResult(l, param1Int1, param1Int2);
        return true;
      } 
      param1Parcel2.writeString("android.hardware.face.IFaceServiceReceiver");
      return true;
    }
    
    private static class Proxy implements IFaceServiceReceiver {
      public static IFaceServiceReceiver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.face.IFaceServiceReceiver";
      }
      
      public void onEnrollResult(long param2Long, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.face.IFaceServiceReceiver");
          parcel.writeLong(param2Long);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IFaceServiceReceiver.Stub.getDefaultImpl() != null) {
            IFaceServiceReceiver.Stub.getDefaultImpl().onEnrollResult(param2Long, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAcquired(long param2Long, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.face.IFaceServiceReceiver");
          parcel.writeLong(param2Long);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IFaceServiceReceiver.Stub.getDefaultImpl() != null) {
            IFaceServiceReceiver.Stub.getDefaultImpl().onAcquired(param2Long, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAuthenticationSucceeded(long param2Long, Face param2Face, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.face.IFaceServiceReceiver");
          parcel1.writeLong(param2Long);
          boolean bool = true;
          if (param2Face != null) {
            parcel1.writeInt(1);
            param2Face.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool1 && IFaceServiceReceiver.Stub.getDefaultImpl() != null) {
            IFaceServiceReceiver.Stub.getDefaultImpl().onAuthenticationSucceeded(param2Long, param2Face, param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onAuthenticationFailed(long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.face.IFaceServiceReceiver");
          parcel.writeLong(param2Long);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IFaceServiceReceiver.Stub.getDefaultImpl() != null) {
            IFaceServiceReceiver.Stub.getDefaultImpl().onAuthenticationFailed(param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onError(long param2Long, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.face.IFaceServiceReceiver");
          parcel.writeLong(param2Long);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IFaceServiceReceiver.Stub.getDefaultImpl() != null) {
            IFaceServiceReceiver.Stub.getDefaultImpl().onError(param2Long, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRemoved(long param2Long, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.face.IFaceServiceReceiver");
          parcel.writeLong(param2Long);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IFaceServiceReceiver.Stub.getDefaultImpl() != null) {
            IFaceServiceReceiver.Stub.getDefaultImpl().onRemoved(param2Long, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onEnumerated(long param2Long, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.face.IFaceServiceReceiver");
          parcel.writeLong(param2Long);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IFaceServiceReceiver.Stub.getDefaultImpl() != null) {
            IFaceServiceReceiver.Stub.getDefaultImpl().onEnumerated(param2Long, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onFeatureSet(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.hardware.face.IFaceServiceReceiver");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(8, parcel, null, 1);
          if (!bool1 && IFaceServiceReceiver.Stub.getDefaultImpl() != null) {
            IFaceServiceReceiver.Stub.getDefaultImpl().onFeatureSet(param2Boolean, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onFeatureGet(boolean param2Boolean1, int param2Int, boolean param2Boolean2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.face.IFaceServiceReceiver");
          boolean bool1 = false;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel.writeInt(bool2);
          parcel.writeInt(param2Int);
          boolean bool2 = bool1;
          if (param2Boolean2)
            bool2 = true; 
          parcel.writeInt(bool2);
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IFaceServiceReceiver.Stub.getDefaultImpl() != null) {
            IFaceServiceReceiver.Stub.getDefaultImpl().onFeatureGet(param2Boolean1, param2Int, param2Boolean2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onProgressChanged(long param2Long, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.face.IFaceServiceReceiver");
          parcel.writeLong(param2Long);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IFaceServiceReceiver.Stub.getDefaultImpl() != null) {
            IFaceServiceReceiver.Stub.getDefaultImpl().onProgressChanged(param2Long, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IFaceServiceReceiver param1IFaceServiceReceiver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IFaceServiceReceiver != null) {
          Proxy.sDefaultImpl = param1IFaceServiceReceiver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IFaceServiceReceiver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
