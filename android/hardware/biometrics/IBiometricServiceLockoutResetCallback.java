package android.hardware.biometrics;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.IRemoteCallback;
import android.os.Parcel;
import android.os.RemoteException;

public interface IBiometricServiceLockoutResetCallback extends IInterface {
  void onLockoutReset(long paramLong, IRemoteCallback paramIRemoteCallback) throws RemoteException;
  
  class Default implements IBiometricServiceLockoutResetCallback {
    public void onLockoutReset(long param1Long, IRemoteCallback param1IRemoteCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IBiometricServiceLockoutResetCallback {
    private static final String DESCRIPTOR = "android.hardware.biometrics.IBiometricServiceLockoutResetCallback";
    
    static final int TRANSACTION_onLockoutReset = 1;
    
    public Stub() {
      attachInterface(this, "android.hardware.biometrics.IBiometricServiceLockoutResetCallback");
    }
    
    public static IBiometricServiceLockoutResetCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.biometrics.IBiometricServiceLockoutResetCallback");
      if (iInterface != null && iInterface instanceof IBiometricServiceLockoutResetCallback)
        return (IBiometricServiceLockoutResetCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onLockoutReset";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.hardware.biometrics.IBiometricServiceLockoutResetCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.hardware.biometrics.IBiometricServiceLockoutResetCallback");
      long l = param1Parcel1.readLong();
      IRemoteCallback iRemoteCallback = IRemoteCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
      onLockoutReset(l, iRemoteCallback);
      return true;
    }
    
    private static class Proxy implements IBiometricServiceLockoutResetCallback {
      public static IBiometricServiceLockoutResetCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.biometrics.IBiometricServiceLockoutResetCallback";
      }
      
      public void onLockoutReset(long param2Long, IRemoteCallback param2IRemoteCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.hardware.biometrics.IBiometricServiceLockoutResetCallback");
          parcel.writeLong(param2Long);
          if (param2IRemoteCallback != null) {
            iBinder = param2IRemoteCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IBiometricServiceLockoutResetCallback.Stub.getDefaultImpl() != null) {
            IBiometricServiceLockoutResetCallback.Stub.getDefaultImpl().onLockoutReset(param2Long, param2IRemoteCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IBiometricServiceLockoutResetCallback param1IBiometricServiceLockoutResetCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IBiometricServiceLockoutResetCallback != null) {
          Proxy.sDefaultImpl = param1IBiometricServiceLockoutResetCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IBiometricServiceLockoutResetCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
