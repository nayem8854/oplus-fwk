package android.hardware.biometrics;

import android.os.OplusSystemProperties;
import android.os.SystemClock;
import android.util.Slog;
import java.util.concurrent.ConcurrentHashMap;

public class OplusTimeUtils {
  private static boolean mCanCalculateTime;
  
  private static ConcurrentHashMap<String, Long> mTimeHashMap = new ConcurrentHashMap<>();
  
  private static boolean canCalculateTime() {
    String str = OplusSystemProperties.get("persist.sys.biometrics.debug", "");
    boolean bool = str.contains("on");
    return bool;
  }
  
  public static void startCalculateTime(String paramString1, String paramString2) {
    if (canCalculateTime())
      mTimeHashMap.put(paramString2, Long.valueOf(SystemClock.uptimeMillis())); 
  }
  
  public static void stopCalculateTime(String paramString1, String paramString2) {
    if (mCanCalculateTime && mTimeHashMap.get(paramString2) != null) {
      long l = SystemClock.uptimeMillis() - ((Long)mTimeHashMap.get(paramString2)).longValue();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString2);
      stringBuilder.append(", TimeConsuming = ");
      stringBuilder.append(l);
      Slog.d(paramString1, stringBuilder.toString());
      mTimeHashMap.put(paramString2, Long.valueOf(l));
    } 
  }
  
  public static void printTotalTime(String paramString1, String paramString2) {
    if (canCalculateTime()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("totalTime of all mode = ");
      stringBuilder.append(mTimeHashMap);
      Slog.d(paramString1, stringBuilder.toString());
    } 
  }
}
