package android.hardware.biometrics;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IBiometricService extends IInterface {
  void authenticate(IBinder paramIBinder, long paramLong, int paramInt1, IBiometricServiceReceiver paramIBiometricServiceReceiver, String paramString, Bundle paramBundle, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  int canAuthenticate(String paramString, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void cancelAuthentication(IBinder paramIBinder, String paramString, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  long[] getAuthenticatorIds(int paramInt) throws RemoteException;
  
  boolean hasEnrolledBiometrics(int paramInt, String paramString) throws RemoteException;
  
  void onReadyForAuthentication(int paramInt1, boolean paramBoolean, int paramInt2) throws RemoteException;
  
  void registerAuthenticator(int paramInt1, int paramInt2, int paramInt3, IBiometricAuthenticator paramIBiometricAuthenticator) throws RemoteException;
  
  void registerEnabledOnKeyguardCallback(IBiometricEnabledOnKeyguardCallback paramIBiometricEnabledOnKeyguardCallback, int paramInt) throws RemoteException;
  
  void resetLockout(byte[] paramArrayOfbyte) throws RemoteException;
  
  void setActiveUser(int paramInt) throws RemoteException;
  
  class Default implements IBiometricService {
    public void authenticate(IBinder param1IBinder, long param1Long, int param1Int1, IBiometricServiceReceiver param1IBiometricServiceReceiver, String param1String, Bundle param1Bundle, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {}
    
    public void cancelAuthentication(IBinder param1IBinder, String param1String, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public int canAuthenticate(String param1String, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return 0;
    }
    
    public boolean hasEnrolledBiometrics(int param1Int, String param1String) throws RemoteException {
      return false;
    }
    
    public void registerAuthenticator(int param1Int1, int param1Int2, int param1Int3, IBiometricAuthenticator param1IBiometricAuthenticator) throws RemoteException {}
    
    public void registerEnabledOnKeyguardCallback(IBiometricEnabledOnKeyguardCallback param1IBiometricEnabledOnKeyguardCallback, int param1Int) throws RemoteException {}
    
    public void setActiveUser(int param1Int) throws RemoteException {}
    
    public void onReadyForAuthentication(int param1Int1, boolean param1Boolean, int param1Int2) throws RemoteException {}
    
    public void resetLockout(byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public long[] getAuthenticatorIds(int param1Int) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IBiometricService {
    private static final String DESCRIPTOR = "android.hardware.biometrics.IBiometricService";
    
    static final int TRANSACTION_authenticate = 1;
    
    static final int TRANSACTION_canAuthenticate = 3;
    
    static final int TRANSACTION_cancelAuthentication = 2;
    
    static final int TRANSACTION_getAuthenticatorIds = 10;
    
    static final int TRANSACTION_hasEnrolledBiometrics = 4;
    
    static final int TRANSACTION_onReadyForAuthentication = 8;
    
    static final int TRANSACTION_registerAuthenticator = 5;
    
    static final int TRANSACTION_registerEnabledOnKeyguardCallback = 6;
    
    static final int TRANSACTION_resetLockout = 9;
    
    static final int TRANSACTION_setActiveUser = 7;
    
    public Stub() {
      attachInterface(this, "android.hardware.biometrics.IBiometricService");
    }
    
    public static IBiometricService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.biometrics.IBiometricService");
      if (iInterface != null && iInterface instanceof IBiometricService)
        return (IBiometricService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 10:
          return "getAuthenticatorIds";
        case 9:
          return "resetLockout";
        case 8:
          return "onReadyForAuthentication";
        case 7:
          return "setActiveUser";
        case 6:
          return "registerEnabledOnKeyguardCallback";
        case 5:
          return "registerAuthenticator";
        case 4:
          return "hasEnrolledBiometrics";
        case 3:
          return "canAuthenticate";
        case 2:
          return "cancelAuthentication";
        case 1:
          break;
      } 
      return "authenticate";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        long[] arrayOfLong;
        byte[] arrayOfByte;
        IBiometricAuthenticator iBiometricAuthenticator;
        String str1;
        boolean bool1;
        IBiometricEnabledOnKeyguardCallback iBiometricEnabledOnKeyguardCallback;
        String str2;
        IBinder iBinder1;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 10:
            param1Parcel1.enforceInterface("android.hardware.biometrics.IBiometricService");
            param1Int1 = param1Parcel1.readInt();
            arrayOfLong = getAuthenticatorIds(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLongArray(arrayOfLong);
            return true;
          case 9:
            arrayOfLong.enforceInterface("android.hardware.biometrics.IBiometricService");
            arrayOfByte = arrayOfLong.createByteArray();
            resetLockout(arrayOfByte);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            arrayOfByte.enforceInterface("android.hardware.biometrics.IBiometricService");
            param1Int1 = arrayOfByte.readInt();
            if (arrayOfByte.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            param1Int2 = arrayOfByte.readInt();
            onReadyForAuthentication(param1Int1, bool1, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            arrayOfByte.enforceInterface("android.hardware.biometrics.IBiometricService");
            param1Int1 = arrayOfByte.readInt();
            setActiveUser(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            arrayOfByte.enforceInterface("android.hardware.biometrics.IBiometricService");
            iBiometricEnabledOnKeyguardCallback = IBiometricEnabledOnKeyguardCallback.Stub.asInterface(arrayOfByte.readStrongBinder());
            param1Int1 = arrayOfByte.readInt();
            registerEnabledOnKeyguardCallback(iBiometricEnabledOnKeyguardCallback, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            arrayOfByte.enforceInterface("android.hardware.biometrics.IBiometricService");
            j = arrayOfByte.readInt();
            param1Int1 = arrayOfByte.readInt();
            param1Int2 = arrayOfByte.readInt();
            iBiometricAuthenticator = IBiometricAuthenticator.Stub.asInterface(arrayOfByte.readStrongBinder());
            registerAuthenticator(j, param1Int1, param1Int2, iBiometricAuthenticator);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            iBiometricAuthenticator.enforceInterface("android.hardware.biometrics.IBiometricService");
            param1Int1 = iBiometricAuthenticator.readInt();
            str1 = iBiometricAuthenticator.readString();
            bool = hasEnrolledBiometrics(param1Int1, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 3:
            str1.enforceInterface("android.hardware.biometrics.IBiometricService");
            str2 = str1.readString();
            param1Int2 = str1.readInt();
            j = str1.readInt();
            i = str1.readInt();
            i = canAuthenticate(str2, param1Int2, j, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 2:
            str1.enforceInterface("android.hardware.biometrics.IBiometricService");
            iBinder1 = str1.readStrongBinder();
            str2 = str1.readString();
            i = str1.readInt();
            param1Int2 = str1.readInt();
            j = str1.readInt();
            cancelAuthentication(iBinder1, str2, i, param1Int2, j);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.hardware.biometrics.IBiometricService");
        IBinder iBinder2 = str1.readStrongBinder();
        long l = str1.readLong();
        int k = str1.readInt();
        IBiometricServiceReceiver iBiometricServiceReceiver = IBiometricServiceReceiver.Stub.asInterface(str1.readStrongBinder());
        String str3 = str1.readString();
        if (str1.readInt() != 0) {
          Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str1);
        } else {
          str2 = null;
        } 
        int i = str1.readInt();
        param1Int2 = str1.readInt();
        int j = str1.readInt();
        authenticate(iBinder2, l, k, iBiometricServiceReceiver, str3, (Bundle)str2, i, param1Int2, j);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.hardware.biometrics.IBiometricService");
      return true;
    }
    
    private static class Proxy implements IBiometricService {
      public static IBiometricService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.biometrics.IBiometricService";
      }
      
      public void authenticate(IBinder param2IBinder, long param2Long, int param2Int1, IBiometricServiceReceiver param2IBiometricServiceReceiver, String param2String, Bundle param2Bundle, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.biometrics.IBiometricService");
          try {
            IBinder iBinder;
            parcel1.writeStrongBinder(param2IBinder);
            parcel1.writeLong(param2Long);
            parcel1.writeInt(param2Int1);
            if (param2IBiometricServiceReceiver != null) {
              iBinder = param2IBiometricServiceReceiver.asBinder();
            } else {
              iBinder = null;
            } 
            parcel1.writeStrongBinder(iBinder);
            parcel1.writeString(param2String);
            if (param2Bundle != null) {
              parcel1.writeInt(1);
              param2Bundle.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            parcel1.writeInt(param2Int2);
            parcel1.writeInt(param2Int3);
            parcel1.writeInt(param2Int4);
            boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
            if (!bool && IBiometricService.Stub.getDefaultImpl() != null) {
              IBiometricService.Stub.getDefaultImpl().authenticate(param2IBinder, param2Long, param2Int1, param2IBiometricServiceReceiver, param2String, param2Bundle, param2Int2, param2Int3, param2Int4);
              parcel2.recycle();
              parcel1.recycle();
              return;
            } 
            parcel2.readException();
            parcel2.recycle();
            parcel1.recycle();
            return;
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IBinder;
      }
      
      public void cancelAuthentication(IBinder param2IBinder, String param2String, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.biometrics.IBiometricService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IBiometricService.Stub.getDefaultImpl() != null) {
            IBiometricService.Stub.getDefaultImpl().cancelAuthentication(param2IBinder, param2String, param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int canAuthenticate(String param2String, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.biometrics.IBiometricService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IBiometricService.Stub.getDefaultImpl() != null) {
            param2Int1 = IBiometricService.Stub.getDefaultImpl().canAuthenticate(param2String, param2Int1, param2Int2, param2Int3);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasEnrolledBiometrics(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.biometrics.IBiometricService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IBiometricService.Stub.getDefaultImpl() != null) {
            bool1 = IBiometricService.Stub.getDefaultImpl().hasEnrolledBiometrics(param2Int, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerAuthenticator(int param2Int1, int param2Int2, int param2Int3, IBiometricAuthenticator param2IBiometricAuthenticator) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.biometrics.IBiometricService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          if (param2IBiometricAuthenticator != null) {
            iBinder = param2IBiometricAuthenticator.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IBiometricService.Stub.getDefaultImpl() != null) {
            IBiometricService.Stub.getDefaultImpl().registerAuthenticator(param2Int1, param2Int2, param2Int3, param2IBiometricAuthenticator);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerEnabledOnKeyguardCallback(IBiometricEnabledOnKeyguardCallback param2IBiometricEnabledOnKeyguardCallback, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.biometrics.IBiometricService");
          if (param2IBiometricEnabledOnKeyguardCallback != null) {
            iBinder = param2IBiometricEnabledOnKeyguardCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IBiometricService.Stub.getDefaultImpl() != null) {
            IBiometricService.Stub.getDefaultImpl().registerEnabledOnKeyguardCallback(param2IBiometricEnabledOnKeyguardCallback, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setActiveUser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.biometrics.IBiometricService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IBiometricService.Stub.getDefaultImpl() != null) {
            IBiometricService.Stub.getDefaultImpl().setActiveUser(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onReadyForAuthentication(int param2Int1, boolean param2Boolean, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.hardware.biometrics.IBiometricService");
          parcel1.writeInt(param2Int1);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int2);
          boolean bool1 = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool1 && IBiometricService.Stub.getDefaultImpl() != null) {
            IBiometricService.Stub.getDefaultImpl().onReadyForAuthentication(param2Int1, param2Boolean, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resetLockout(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.biometrics.IBiometricService");
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IBiometricService.Stub.getDefaultImpl() != null) {
            IBiometricService.Stub.getDefaultImpl().resetLockout(param2ArrayOfbyte);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long[] getAuthenticatorIds(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.biometrics.IBiometricService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IBiometricService.Stub.getDefaultImpl() != null)
            return IBiometricService.Stub.getDefaultImpl().getAuthenticatorIds(param2Int); 
          parcel2.readException();
          return parcel2.createLongArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IBiometricService param1IBiometricService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IBiometricService != null) {
          Proxy.sDefaultImpl = param1IBiometricService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IBiometricService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
