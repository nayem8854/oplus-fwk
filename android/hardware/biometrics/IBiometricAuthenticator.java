package android.hardware.biometrics;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IBiometricAuthenticator extends IInterface {
  void cancelAuthenticationFromService(IBinder paramIBinder, String paramString, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) throws RemoteException;
  
  long getAuthenticatorId(int paramInt) throws RemoteException;
  
  boolean hasEnrolledTemplates(int paramInt, String paramString) throws RemoteException;
  
  boolean isHardwareDetected(String paramString) throws RemoteException;
  
  void prepareForAuthentication(boolean paramBoolean, IBinder paramIBinder, long paramLong, int paramInt1, IBiometricServiceReceiverInternal paramIBiometricServiceReceiverInternal, String paramString, int paramInt2, int paramInt3, int paramInt4, int paramInt5) throws RemoteException;
  
  void resetLockout(byte[] paramArrayOfbyte) throws RemoteException;
  
  void setActiveUser(int paramInt) throws RemoteException;
  
  void startPreparedClient(int paramInt) throws RemoteException;
  
  class Default implements IBiometricAuthenticator {
    public void prepareForAuthentication(boolean param1Boolean, IBinder param1IBinder, long param1Long, int param1Int1, IBiometricServiceReceiverInternal param1IBiometricServiceReceiverInternal, String param1String, int param1Int2, int param1Int3, int param1Int4, int param1Int5) throws RemoteException {}
    
    public void startPreparedClient(int param1Int) throws RemoteException {}
    
    public void cancelAuthenticationFromService(IBinder param1IBinder, String param1String, int param1Int1, int param1Int2, int param1Int3, boolean param1Boolean) throws RemoteException {}
    
    public boolean isHardwareDetected(String param1String) throws RemoteException {
      return false;
    }
    
    public boolean hasEnrolledTemplates(int param1Int, String param1String) throws RemoteException {
      return false;
    }
    
    public void resetLockout(byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void setActiveUser(int param1Int) throws RemoteException {}
    
    public long getAuthenticatorId(int param1Int) throws RemoteException {
      return 0L;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IBiometricAuthenticator {
    private static final String DESCRIPTOR = "android.hardware.biometrics.IBiometricAuthenticator";
    
    static final int TRANSACTION_cancelAuthenticationFromService = 3;
    
    static final int TRANSACTION_getAuthenticatorId = 8;
    
    static final int TRANSACTION_hasEnrolledTemplates = 5;
    
    static final int TRANSACTION_isHardwareDetected = 4;
    
    static final int TRANSACTION_prepareForAuthentication = 1;
    
    static final int TRANSACTION_resetLockout = 6;
    
    static final int TRANSACTION_setActiveUser = 7;
    
    static final int TRANSACTION_startPreparedClient = 2;
    
    public Stub() {
      attachInterface(this, "android.hardware.biometrics.IBiometricAuthenticator");
    }
    
    public static IBiometricAuthenticator asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.biometrics.IBiometricAuthenticator");
      if (iInterface != null && iInterface instanceof IBiometricAuthenticator)
        return (IBiometricAuthenticator)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "getAuthenticatorId";
        case 7:
          return "setActiveUser";
        case 6:
          return "resetLockout";
        case 5:
          return "hasEnrolledTemplates";
        case 4:
          return "isHardwareDetected";
        case 3:
          return "cancelAuthenticationFromService";
        case 2:
          return "startPreparedClient";
        case 1:
          break;
      } 
      return "prepareForAuthentication";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        byte[] arrayOfByte;
        String str1, str2;
        boolean bool1;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("android.hardware.biometrics.IBiometricAuthenticator");
            param1Int1 = param1Parcel1.readInt();
            l = getAuthenticatorId(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.hardware.biometrics.IBiometricAuthenticator");
            param1Int1 = param1Parcel1.readInt();
            setActiveUser(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.hardware.biometrics.IBiometricAuthenticator");
            arrayOfByte = param1Parcel1.createByteArray();
            resetLockout(arrayOfByte);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            arrayOfByte.enforceInterface("android.hardware.biometrics.IBiometricAuthenticator");
            param1Int1 = arrayOfByte.readInt();
            str1 = arrayOfByte.readString();
            bool = hasEnrolledTemplates(param1Int1, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 4:
            str1.enforceInterface("android.hardware.biometrics.IBiometricAuthenticator");
            str1 = str1.readString();
            bool = isHardwareDetected(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 3:
            str1.enforceInterface("android.hardware.biometrics.IBiometricAuthenticator");
            iBinder = str1.readStrongBinder();
            str2 = str1.readString();
            param1Int2 = str1.readInt();
            j = str1.readInt();
            i = str1.readInt();
            if (str1.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            cancelAuthenticationFromService(iBinder, str2, param1Int2, j, i, bool1);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str1.enforceInterface("android.hardware.biometrics.IBiometricAuthenticator");
            i = str1.readInt();
            startPreparedClient(i);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.hardware.biometrics.IBiometricAuthenticator");
        if (str1.readInt() != 0) {
          bool1 = true;
        } else {
          bool1 = false;
        } 
        IBinder iBinder = str1.readStrongBinder();
        long l = str1.readLong();
        param1Int2 = str1.readInt();
        IBiometricServiceReceiverInternal iBiometricServiceReceiverInternal = IBiometricServiceReceiverInternal.Stub.asInterface(str1.readStrongBinder());
        String str3 = str1.readString();
        int j = str1.readInt();
        int k = str1.readInt();
        int i = str1.readInt();
        int m = str1.readInt();
        prepareForAuthentication(bool1, iBinder, l, param1Int2, iBiometricServiceReceiverInternal, str3, j, k, i, m);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.hardware.biometrics.IBiometricAuthenticator");
      return true;
    }
    
    private static class Proxy implements IBiometricAuthenticator {
      public static IBiometricAuthenticator sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.biometrics.IBiometricAuthenticator";
      }
      
      public void prepareForAuthentication(boolean param2Boolean, IBinder param2IBinder, long param2Long, int param2Int1, IBiometricServiceReceiverInternal param2IBiometricServiceReceiverInternal, String param2String, int param2Int2, int param2Int3, int param2Int4, int param2Int5) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.biometrics.IBiometricAuthenticator");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int1);
          if (param2IBiometricServiceReceiverInternal != null) {
            iBinder = param2IBiometricServiceReceiverInternal.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeInt(param2Int4);
          parcel1.writeInt(param2Int5);
          boolean bool1 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool1 && IBiometricAuthenticator.Stub.getDefaultImpl() != null) {
            IBiometricAuthenticator.Stub.getDefaultImpl().prepareForAuthentication(param2Boolean, param2IBinder, param2Long, param2Int1, param2IBiometricServiceReceiverInternal, param2String, param2Int2, param2Int3, param2Int4, param2Int5);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startPreparedClient(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.biometrics.IBiometricAuthenticator");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IBiometricAuthenticator.Stub.getDefaultImpl() != null) {
            IBiometricAuthenticator.Stub.getDefaultImpl().startPreparedClient(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelAuthenticationFromService(IBinder param2IBinder, String param2String, int param2Int1, int param2Int2, int param2Int3, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.biometrics.IBiometricAuthenticator");
          try {
            parcel1.writeStrongBinder(param2IBinder);
            try {
              parcel1.writeString(param2String);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  parcel1.writeInt(param2Int2);
                  try {
                    boolean bool;
                    parcel1.writeInt(param2Int3);
                    if (param2Boolean) {
                      bool = true;
                    } else {
                      bool = false;
                    } 
                    parcel1.writeInt(bool);
                    try {
                      boolean bool1 = this.mRemote.transact(3, parcel1, parcel2, 0);
                      if (!bool1 && IBiometricAuthenticator.Stub.getDefaultImpl() != null) {
                        IBiometricAuthenticator.Stub.getDefaultImpl().cancelAuthenticationFromService(param2IBinder, param2String, param2Int1, param2Int2, param2Int3, param2Boolean);
                        parcel2.recycle();
                        parcel1.recycle();
                        return;
                      } 
                      parcel2.readException();
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IBinder;
      }
      
      public boolean isHardwareDetected(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.biometrics.IBiometricAuthenticator");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IBiometricAuthenticator.Stub.getDefaultImpl() != null) {
            bool1 = IBiometricAuthenticator.Stub.getDefaultImpl().isHardwareDetected(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasEnrolledTemplates(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.biometrics.IBiometricAuthenticator");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IBiometricAuthenticator.Stub.getDefaultImpl() != null) {
            bool1 = IBiometricAuthenticator.Stub.getDefaultImpl().hasEnrolledTemplates(param2Int, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resetLockout(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.biometrics.IBiometricAuthenticator");
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IBiometricAuthenticator.Stub.getDefaultImpl() != null) {
            IBiometricAuthenticator.Stub.getDefaultImpl().resetLockout(param2ArrayOfbyte);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setActiveUser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.biometrics.IBiometricAuthenticator");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IBiometricAuthenticator.Stub.getDefaultImpl() != null) {
            IBiometricAuthenticator.Stub.getDefaultImpl().setActiveUser(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getAuthenticatorId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.biometrics.IBiometricAuthenticator");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IBiometricAuthenticator.Stub.getDefaultImpl() != null)
            return IBiometricAuthenticator.Stub.getDefaultImpl().getAuthenticatorId(param2Int); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IBiometricAuthenticator param1IBiometricAuthenticator) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IBiometricAuthenticator != null) {
          Proxy.sDefaultImpl = param1IBiometricAuthenticator;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IBiometricAuthenticator getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
