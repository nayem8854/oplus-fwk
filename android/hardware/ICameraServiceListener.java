package android.hardware;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ICameraServiceListener extends IInterface {
  public static final int STATUS_ENUMERATING = 2;
  
  public static final int STATUS_NOT_AVAILABLE = -2;
  
  public static final int STATUS_NOT_PRESENT = 0;
  
  public static final int STATUS_PRESENT = 1;
  
  public static final int STATUS_UNKNOWN = -1;
  
  public static final int TORCH_STATUS_AVAILABLE_OFF = 1;
  
  public static final int TORCH_STATUS_AVAILABLE_ON = 2;
  
  public static final int TORCH_STATUS_NOT_AVAILABLE = 0;
  
  public static final int TORCH_STATUS_UNKNOWN = -1;
  
  void onCameraAccessPrioritiesChanged() throws RemoteException;
  
  void onCameraClosed(String paramString) throws RemoteException;
  
  void onCameraOpened(String paramString1, String paramString2) throws RemoteException;
  
  void onPhysicalCameraStatusChanged(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  void onStatusChanged(int paramInt, String paramString) throws RemoteException;
  
  void onTorchStatusChanged(int paramInt, String paramString) throws RemoteException;
  
  class Default implements ICameraServiceListener {
    public void onStatusChanged(int param1Int, String param1String) throws RemoteException {}
    
    public void onPhysicalCameraStatusChanged(int param1Int, String param1String1, String param1String2) throws RemoteException {}
    
    public void onTorchStatusChanged(int param1Int, String param1String) throws RemoteException {}
    
    public void onCameraAccessPrioritiesChanged() throws RemoteException {}
    
    public void onCameraOpened(String param1String1, String param1String2) throws RemoteException {}
    
    public void onCameraClosed(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICameraServiceListener {
    private static final String DESCRIPTOR = "android.hardware.ICameraServiceListener";
    
    static final int TRANSACTION_onCameraAccessPrioritiesChanged = 4;
    
    static final int TRANSACTION_onCameraClosed = 6;
    
    static final int TRANSACTION_onCameraOpened = 5;
    
    static final int TRANSACTION_onPhysicalCameraStatusChanged = 2;
    
    static final int TRANSACTION_onStatusChanged = 1;
    
    static final int TRANSACTION_onTorchStatusChanged = 3;
    
    public Stub() {
      attachInterface(this, "android.hardware.ICameraServiceListener");
    }
    
    public static ICameraServiceListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.ICameraServiceListener");
      if (iInterface != null && iInterface instanceof ICameraServiceListener)
        return (ICameraServiceListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "onCameraClosed";
        case 5:
          return "onCameraOpened";
        case 4:
          return "onCameraAccessPrioritiesChanged";
        case 3:
          return "onTorchStatusChanged";
        case 2:
          return "onPhysicalCameraStatusChanged";
        case 1:
          break;
      } 
      return "onStatusChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("android.hardware.ICameraServiceListener");
            str1 = param1Parcel1.readString();
            onCameraClosed(str1);
            return true;
          case 5:
            str1.enforceInterface("android.hardware.ICameraServiceListener");
            str = str1.readString();
            str1 = str1.readString();
            onCameraOpened(str, str1);
            return true;
          case 4:
            str1.enforceInterface("android.hardware.ICameraServiceListener");
            onCameraAccessPrioritiesChanged();
            return true;
          case 3:
            str1.enforceInterface("android.hardware.ICameraServiceListener");
            param1Int1 = str1.readInt();
            str1 = str1.readString();
            onTorchStatusChanged(param1Int1, str1);
            return true;
          case 2:
            str1.enforceInterface("android.hardware.ICameraServiceListener");
            param1Int1 = str1.readInt();
            str = str1.readString();
            str1 = str1.readString();
            onPhysicalCameraStatusChanged(param1Int1, str, str1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.hardware.ICameraServiceListener");
        param1Int1 = str1.readInt();
        String str1 = str1.readString();
        onStatusChanged(param1Int1, str1);
        return true;
      } 
      str.writeString("android.hardware.ICameraServiceListener");
      return true;
    }
    
    class Proxy implements ICameraServiceListener {
      public static ICameraServiceListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(ICameraServiceListener.Stub this$0) {
        this.mRemote = (IBinder)this$0;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.ICameraServiceListener";
      }
      
      public void onStatusChanged(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.ICameraServiceListener");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ICameraServiceListener.Stub.getDefaultImpl() != null) {
            ICameraServiceListener.Stub.getDefaultImpl().onStatusChanged(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPhysicalCameraStatusChanged(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.ICameraServiceListener");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && ICameraServiceListener.Stub.getDefaultImpl() != null) {
            ICameraServiceListener.Stub.getDefaultImpl().onPhysicalCameraStatusChanged(param2Int, param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTorchStatusChanged(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.ICameraServiceListener");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && ICameraServiceListener.Stub.getDefaultImpl() != null) {
            ICameraServiceListener.Stub.getDefaultImpl().onTorchStatusChanged(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCameraAccessPrioritiesChanged() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.ICameraServiceListener");
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && ICameraServiceListener.Stub.getDefaultImpl() != null) {
            ICameraServiceListener.Stub.getDefaultImpl().onCameraAccessPrioritiesChanged();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCameraOpened(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.ICameraServiceListener");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && ICameraServiceListener.Stub.getDefaultImpl() != null) {
            ICameraServiceListener.Stub.getDefaultImpl().onCameraOpened(param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCameraClosed(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.ICameraServiceListener");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && ICameraServiceListener.Stub.getDefaultImpl() != null) {
            ICameraServiceListener.Stub.getDefaultImpl().onCameraClosed(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICameraServiceListener param1ICameraServiceListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICameraServiceListener != null) {
          Proxy.sDefaultImpl = param1ICameraServiceListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICameraServiceListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
