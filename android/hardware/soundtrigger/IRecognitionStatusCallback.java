package android.hardware.soundtrigger;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IRecognitionStatusCallback extends IInterface {
  void onError(int paramInt) throws RemoteException;
  
  void onGenericSoundTriggerDetected(SoundTrigger.GenericRecognitionEvent paramGenericRecognitionEvent) throws RemoteException;
  
  void onKeyphraseDetected(SoundTrigger.KeyphraseRecognitionEvent paramKeyphraseRecognitionEvent) throws RemoteException;
  
  void onRecognitionPaused() throws RemoteException;
  
  void onRecognitionResumed() throws RemoteException;
  
  class Default implements IRecognitionStatusCallback {
    public void onKeyphraseDetected(SoundTrigger.KeyphraseRecognitionEvent param1KeyphraseRecognitionEvent) throws RemoteException {}
    
    public void onGenericSoundTriggerDetected(SoundTrigger.GenericRecognitionEvent param1GenericRecognitionEvent) throws RemoteException {}
    
    public void onError(int param1Int) throws RemoteException {}
    
    public void onRecognitionPaused() throws RemoteException {}
    
    public void onRecognitionResumed() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRecognitionStatusCallback {
    private static final String DESCRIPTOR = "android.hardware.soundtrigger.IRecognitionStatusCallback";
    
    static final int TRANSACTION_onError = 3;
    
    static final int TRANSACTION_onGenericSoundTriggerDetected = 2;
    
    static final int TRANSACTION_onKeyphraseDetected = 1;
    
    static final int TRANSACTION_onRecognitionPaused = 4;
    
    static final int TRANSACTION_onRecognitionResumed = 5;
    
    public Stub() {
      attachInterface(this, "android.hardware.soundtrigger.IRecognitionStatusCallback");
    }
    
    public static IRecognitionStatusCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.soundtrigger.IRecognitionStatusCallback");
      if (iInterface != null && iInterface instanceof IRecognitionStatusCallback)
        return (IRecognitionStatusCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "onRecognitionResumed";
            } 
            return "onRecognitionPaused";
          } 
          return "onError";
        } 
        return "onGenericSoundTriggerDetected";
      } 
      return "onKeyphraseDetected";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.hardware.soundtrigger.IRecognitionStatusCallback");
                return true;
              } 
              param1Parcel1.enforceInterface("android.hardware.soundtrigger.IRecognitionStatusCallback");
              onRecognitionResumed();
              return true;
            } 
            param1Parcel1.enforceInterface("android.hardware.soundtrigger.IRecognitionStatusCallback");
            onRecognitionPaused();
            return true;
          } 
          param1Parcel1.enforceInterface("android.hardware.soundtrigger.IRecognitionStatusCallback");
          param1Int1 = param1Parcel1.readInt();
          onError(param1Int1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.hardware.soundtrigger.IRecognitionStatusCallback");
        if (param1Parcel1.readInt() != 0) {
          SoundTrigger.GenericRecognitionEvent genericRecognitionEvent = SoundTrigger.GenericRecognitionEvent.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onGenericSoundTriggerDetected((SoundTrigger.GenericRecognitionEvent)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.hardware.soundtrigger.IRecognitionStatusCallback");
      if (param1Parcel1.readInt() != 0) {
        SoundTrigger.KeyphraseRecognitionEvent keyphraseRecognitionEvent = SoundTrigger.KeyphraseRecognitionEvent.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onKeyphraseDetected((SoundTrigger.KeyphraseRecognitionEvent)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IRecognitionStatusCallback {
      public static IRecognitionStatusCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.soundtrigger.IRecognitionStatusCallback";
      }
      
      public void onKeyphraseDetected(SoundTrigger.KeyphraseRecognitionEvent param2KeyphraseRecognitionEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.soundtrigger.IRecognitionStatusCallback");
          if (param2KeyphraseRecognitionEvent != null) {
            parcel.writeInt(1);
            param2KeyphraseRecognitionEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IRecognitionStatusCallback.Stub.getDefaultImpl() != null) {
            IRecognitionStatusCallback.Stub.getDefaultImpl().onKeyphraseDetected(param2KeyphraseRecognitionEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onGenericSoundTriggerDetected(SoundTrigger.GenericRecognitionEvent param2GenericRecognitionEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.soundtrigger.IRecognitionStatusCallback");
          if (param2GenericRecognitionEvent != null) {
            parcel.writeInt(1);
            param2GenericRecognitionEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IRecognitionStatusCallback.Stub.getDefaultImpl() != null) {
            IRecognitionStatusCallback.Stub.getDefaultImpl().onGenericSoundTriggerDetected(param2GenericRecognitionEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onError(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.soundtrigger.IRecognitionStatusCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IRecognitionStatusCallback.Stub.getDefaultImpl() != null) {
            IRecognitionStatusCallback.Stub.getDefaultImpl().onError(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRecognitionPaused() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.soundtrigger.IRecognitionStatusCallback");
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IRecognitionStatusCallback.Stub.getDefaultImpl() != null) {
            IRecognitionStatusCallback.Stub.getDefaultImpl().onRecognitionPaused();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRecognitionResumed() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.soundtrigger.IRecognitionStatusCallback");
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IRecognitionStatusCallback.Stub.getDefaultImpl() != null) {
            IRecognitionStatusCallback.Stub.getDefaultImpl().onRecognitionResumed();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRecognitionStatusCallback param1IRecognitionStatusCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRecognitionStatusCallback != null) {
          Proxy.sDefaultImpl = param1IRecognitionStatusCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRecognitionStatusCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
