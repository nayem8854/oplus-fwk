package android.hardware.soundtrigger;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.text.TextUtils;
import android.util.ArraySet;
import android.util.AttributeSet;
import android.util.Slog;
import android.util.Xml;
import com.android.internal.R;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class KeyphraseEnrollmentInfo {
  public static final String ACTION_MANAGE_VOICE_KEYPHRASES = "com.android.intent.action.MANAGE_VOICE_KEYPHRASES";
  
  public static final String EXTRA_VOICE_KEYPHRASE_ACTION = "com.android.intent.extra.VOICE_KEYPHRASE_ACTION";
  
  public static final String EXTRA_VOICE_KEYPHRASE_HINT_TEXT = "com.android.intent.extra.VOICE_KEYPHRASE_HINT_TEXT";
  
  public static final String EXTRA_VOICE_KEYPHRASE_LOCALE = "com.android.intent.extra.VOICE_KEYPHRASE_LOCALE";
  
  public static final int MANAGE_ACTION_ENROLL = 0;
  
  public static final int MANAGE_ACTION_RE_ENROLL = 1;
  
  public static final int MANAGE_ACTION_UN_ENROLL = 2;
  
  private static final String TAG = "KeyphraseEnrollmentInfo";
  
  private static final String VOICE_KEYPHRASE_META_DATA = "android.voice_enrollment";
  
  private final Map<KeyphraseMetadata, String> mKeyphrasePackageMap;
  
  private final KeyphraseMetadata[] mKeyphrases;
  
  private String mParseError;
  
  public KeyphraseEnrollmentInfo(PackageManager paramPackageManager) {
    Objects.requireNonNull(paramPackageManager);
    List list = paramPackageManager.queryIntentServices(new Intent("com.android.intent.action.MANAGE_VOICE_KEYPHRASES"), 65536);
    if (list == null || list.isEmpty()) {
      this.mParseError = "No enrollment applications found";
      this.mKeyphrasePackageMap = Collections.emptyMap();
      this.mKeyphrases = null;
      return;
    } 
    LinkedList<String> linkedList = new LinkedList();
    this.mKeyphrasePackageMap = new HashMap<>();
    for (ResolveInfo resolveInfo : list) {
      try {
        ApplicationInfo applicationInfo = paramPackageManager.getApplicationInfo(resolveInfo.serviceInfo.packageName, 128);
        if ((applicationInfo.privateFlags & 0x8) == 0) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append(applicationInfo.packageName);
          stringBuilder.append(" is not a privileged system app");
          Slog.w("KeyphraseEnrollmentInfo", stringBuilder.toString());
          continue;
        } 
        if (!"android.permission.MANAGE_VOICE_KEYPHRASES".equals(applicationInfo.permission)) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append(applicationInfo.packageName);
          stringBuilder.append(" does not require MANAGE_VOICE_KEYPHRASES");
          Slog.w("KeyphraseEnrollmentInfo", stringBuilder.toString());
          continue;
        } 
        KeyphraseMetadata keyphraseMetadata = getKeyphraseMetadataFromApplicationInfo(paramPackageManager, applicationInfo, linkedList);
        if (keyphraseMetadata != null)
          this.mKeyphrasePackageMap.put(keyphraseMetadata, applicationInfo.packageName); 
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("error parsing voice enrollment meta-data for ");
        stringBuilder.append(resolveInfo.serviceInfo.packageName);
        String str = stringBuilder.toString();
        stringBuilder = new StringBuilder();
        stringBuilder.append(str);
        stringBuilder.append(": ");
        stringBuilder.append(nameNotFoundException);
        linkedList.add(stringBuilder.toString());
        Slog.w("KeyphraseEnrollmentInfo", str, (Throwable)nameNotFoundException);
      } 
    } 
    if (this.mKeyphrasePackageMap.isEmpty()) {
      linkedList.add("No suitable enrollment application found");
      Slog.w("KeyphraseEnrollmentInfo", "No suitable enrollment application found");
      this.mKeyphrases = null;
    } else {
      this.mKeyphrases = (KeyphraseMetadata[])this.mKeyphrasePackageMap.keySet().toArray((Object[])new KeyphraseMetadata[0]);
    } 
    if (!linkedList.isEmpty())
      this.mParseError = TextUtils.join("\n", linkedList); 
  }
  
  private KeyphraseMetadata getKeyphraseMetadataFromApplicationInfo(PackageManager paramPackageManager, ApplicationInfo paramApplicationInfo, List<String> paramList) {
    KeyphraseMetadata keyphraseMetadata1;
    XmlResourceParser xmlResourceParser1 = null, xmlResourceParser2 = null;
    String str = paramApplicationInfo.packageName;
    KeyphraseMetadata keyphraseMetadata2 = null;
    KeyphraseMetadata keyphraseMetadata3 = keyphraseMetadata2;
    try {
      String str2, str1;
      XmlResourceParser xmlResourceParser = paramApplicationInfo.loadXmlMetaData(paramPackageManager, "android.voice_enrollment");
      if (xmlResourceParser == null) {
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        keyphraseMetadata3 = keyphraseMetadata2;
        StringBuilder stringBuilder = new StringBuilder();
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        keyphraseMetadata3 = keyphraseMetadata2;
        this();
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        keyphraseMetadata3 = keyphraseMetadata2;
        stringBuilder.append("No android.voice_enrollment meta-data for ");
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        keyphraseMetadata3 = keyphraseMetadata2;
        stringBuilder.append(str);
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        keyphraseMetadata3 = keyphraseMetadata2;
        str2 = stringBuilder.toString();
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        keyphraseMetadata3 = keyphraseMetadata2;
        paramList.add(str2);
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        keyphraseMetadata3 = keyphraseMetadata2;
        Slog.w("KeyphraseEnrollmentInfo", str2);
        if (xmlResourceParser != null)
          xmlResourceParser.close(); 
        return null;
      } 
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      keyphraseMetadata3 = keyphraseMetadata2;
      Resources resources = str2.getResourcesForApplication(paramApplicationInfo);
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      keyphraseMetadata3 = keyphraseMetadata2;
      AttributeSet attributeSet = Xml.asAttributeSet((XmlPullParser)xmlResourceParser);
      while (true) {
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        keyphraseMetadata3 = keyphraseMetadata2;
        int i = xmlResourceParser.next();
        if (i != 1 && i != 2)
          continue; 
        break;
      } 
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      keyphraseMetadata3 = keyphraseMetadata2;
      String str3 = xmlResourceParser.getName();
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      keyphraseMetadata3 = keyphraseMetadata2;
      if (!"voice-enrollment-application".equals(str3)) {
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        keyphraseMetadata3 = keyphraseMetadata2;
        StringBuilder stringBuilder = new StringBuilder();
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        keyphraseMetadata3 = keyphraseMetadata2;
        this();
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        keyphraseMetadata3 = keyphraseMetadata2;
        stringBuilder.append("Meta-data does not start with voice-enrollment-application tag for ");
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        keyphraseMetadata3 = keyphraseMetadata2;
        stringBuilder.append(str);
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        keyphraseMetadata3 = keyphraseMetadata2;
        str1 = stringBuilder.toString();
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        keyphraseMetadata3 = keyphraseMetadata2;
        paramList.add(str1);
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        keyphraseMetadata3 = keyphraseMetadata2;
        Slog.w("KeyphraseEnrollmentInfo", str1);
        if (xmlResourceParser != null)
          xmlResourceParser.close(); 
        return null;
      } 
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      keyphraseMetadata3 = keyphraseMetadata2;
      TypedArray typedArray = resources.obtainAttributes((AttributeSet)str1, R.styleable.VoiceEnrollmentApplication);
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      keyphraseMetadata3 = keyphraseMetadata2;
      KeyphraseMetadata keyphraseMetadata = getKeyphraseFromTypedArray(typedArray, str, paramList);
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      keyphraseMetadata3 = keyphraseMetadata;
      typedArray.recycle();
      keyphraseMetadata1 = keyphraseMetadata;
      if (xmlResourceParser != null) {
        xmlResourceParser1 = xmlResourceParser;
      } else {
        return keyphraseMetadata1;
      } 
      xmlResourceParser1.close();
      keyphraseMetadata1 = keyphraseMetadata;
    } catch (XmlPullParserException|android.content.pm.PackageManager.NameNotFoundException|java.io.IOException xmlPullParserException) {
      KeyphraseMetadata keyphraseMetadata;
      xmlResourceParser2 = xmlResourceParser1;
      StringBuilder stringBuilder1 = new StringBuilder();
      xmlResourceParser2 = xmlResourceParser1;
      this();
      xmlResourceParser2 = xmlResourceParser1;
      stringBuilder1.append("Error parsing keyphrase enrollment meta-data for ");
      xmlResourceParser2 = xmlResourceParser1;
      stringBuilder1.append(str);
      xmlResourceParser2 = xmlResourceParser1;
      String str1 = stringBuilder1.toString();
      xmlResourceParser2 = xmlResourceParser1;
      StringBuilder stringBuilder2 = new StringBuilder();
      xmlResourceParser2 = xmlResourceParser1;
      this();
      xmlResourceParser2 = xmlResourceParser1;
      stringBuilder2.append(str1);
      xmlResourceParser2 = xmlResourceParser1;
      stringBuilder2.append(": ");
      xmlResourceParser2 = xmlResourceParser1;
      stringBuilder2.append(xmlPullParserException);
      xmlResourceParser2 = xmlResourceParser1;
      paramList.add(stringBuilder2.toString());
      xmlResourceParser2 = xmlResourceParser1;
      Slog.w("KeyphraseEnrollmentInfo", str1, (Throwable)xmlPullParserException);
      keyphraseMetadata1 = keyphraseMetadata3;
      if (xmlResourceParser1 != null) {
        keyphraseMetadata = keyphraseMetadata3;
      } else {
        return keyphraseMetadata1;
      } 
      xmlResourceParser1.close();
      keyphraseMetadata1 = keyphraseMetadata;
    } finally {}
    return keyphraseMetadata1;
  }
  
  private KeyphraseMetadata getKeyphraseFromTypedArray(TypedArray paramTypedArray, String paramString, List<String> paramList) {
    String str1;
    int i = 0, j = paramTypedArray.getInt(0, -1);
    if (j <= 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("No valid searchKeyphraseId specified in meta-data for ");
      stringBuilder.append(paramString);
      str1 = stringBuilder.toString();
      paramList.add(str1);
      Slog.w("KeyphraseEnrollmentInfo", str1);
      return null;
    } 
    String str2 = str1.getString(1);
    if (str2 == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("No valid searchKeyphrase specified in meta-data for ");
      stringBuilder.append(paramString);
      str1 = stringBuilder.toString();
      paramList.add(str1);
      Slog.w("KeyphraseEnrollmentInfo", str1);
      return null;
    } 
    String str3 = str1.getString(2);
    if (str3 == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("No valid searchKeyphraseSupportedLocales specified in meta-data for ");
      stringBuilder.append(paramString);
      String str = stringBuilder.toString();
      paramList.add(str);
      Slog.w("KeyphraseEnrollmentInfo", str);
      return null;
    } 
    ArraySet arraySet = new ArraySet();
    if (!TextUtils.isEmpty(str3))
      try {
        String[] arrayOfString = str3.split(",");
        for (int k = arrayOfString.length; i < k; ) {
          str3 = arrayOfString[i];
          arraySet.add(Locale.forLanguageTag(str3));
          i++;
        } 
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Error reading searchKeyphraseSupportedLocales from meta-data for ");
        stringBuilder.append(paramString);
        str1 = stringBuilder.toString();
        paramList.add(str1);
        Slog.w("KeyphraseEnrollmentInfo", str1);
        return null;
      }  
    i = str1.getInt(3, -1);
    if (i < 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("No valid searchKeyphraseRecognitionFlags specified in meta-data for ");
      stringBuilder.append(paramString);
      String str = stringBuilder.toString();
      paramList.add(str);
      Slog.w("KeyphraseEnrollmentInfo", str);
      return null;
    } 
    return new KeyphraseMetadata(j, str2, (Set<Locale>)arraySet, i);
  }
  
  public String getParseError() {
    return this.mParseError;
  }
  
  public Collection<KeyphraseMetadata> listKeyphraseMetadata() {
    return Arrays.asList(this.mKeyphrases);
  }
  
  public Intent getManageKeyphraseIntent(int paramInt, String paramString, Locale paramLocale) {
    Objects.requireNonNull(paramString);
    Objects.requireNonNull(paramLocale);
    Map<KeyphraseMetadata, String> map = this.mKeyphrasePackageMap;
    if (map == null || map.isEmpty()) {
      Slog.w("KeyphraseEnrollmentInfo", "No enrollment application exists");
      return null;
    } 
    KeyphraseMetadata keyphraseMetadata = getKeyphraseMetadata(paramString, paramLocale);
    if (keyphraseMetadata != null) {
      Intent intent2 = new Intent("com.android.intent.action.MANAGE_VOICE_KEYPHRASES");
      Map<KeyphraseMetadata, String> map1 = this.mKeyphrasePackageMap;
      Intent intent1 = intent2.setPackage(map1.get(keyphraseMetadata));
      null = intent1.putExtra("com.android.intent.extra.VOICE_KEYPHRASE_HINT_TEXT", paramString);
      null = null.putExtra("com.android.intent.extra.VOICE_KEYPHRASE_LOCALE", paramLocale.toLanguageTag());
      return null.putExtra("com.android.intent.extra.VOICE_KEYPHRASE_ACTION", paramInt);
    } 
    return null;
  }
  
  public KeyphraseMetadata getKeyphraseMetadata(String paramString, Locale paramLocale) {
    Objects.requireNonNull(paramString);
    Objects.requireNonNull(paramLocale);
    KeyphraseMetadata[] arrayOfKeyphraseMetadata = this.mKeyphrases;
    if (arrayOfKeyphraseMetadata != null && arrayOfKeyphraseMetadata.length > 0) {
      int i;
      byte b;
      for (i = arrayOfKeyphraseMetadata.length, b = 0; b < i; ) {
        KeyphraseMetadata keyphraseMetadata = arrayOfKeyphraseMetadata[b];
        if (keyphraseMetadata.supportsPhrase(paramString) && 
          keyphraseMetadata.supportsLocale(paramLocale))
          return keyphraseMetadata; 
        b++;
      } 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("No enrollment application supports the given keyphrase/locale: '");
    stringBuilder.append(paramString);
    stringBuilder.append("'/");
    stringBuilder.append(paramLocale);
    Slog.w("KeyphraseEnrollmentInfo", stringBuilder.toString());
    return null;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("KeyphraseEnrollmentInfo [KeyphrasePackageMap=");
    stringBuilder.append(this.mKeyphrasePackageMap.toString());
    stringBuilder.append(", ParseError=");
    stringBuilder.append(this.mParseError);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ManageActions {}
}
