package android.hardware.soundtrigger;

import android.annotation.NonNull;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.ArraySet;
import com.android.internal.util.AnnotationValidations;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

public final class KeyphraseMetadata implements Parcelable {
  public KeyphraseMetadata(int paramInt1, String paramString, Set<Locale> paramSet, int paramInt2) {
    this.mId = paramInt1;
    this.mKeyphrase = paramString;
    this.mSupportedLocales = new ArraySet(paramSet);
    this.mRecognitionModeFlags = paramInt2;
  }
  
  public int getId() {
    return this.mId;
  }
  
  public String getKeyphrase() {
    return this.mKeyphrase;
  }
  
  public Set<Locale> getSupportedLocales() {
    return (Set<Locale>)this.mSupportedLocales;
  }
  
  public int getRecognitionModeFlags() {
    return this.mRecognitionModeFlags;
  }
  
  public boolean supportsPhrase(String paramString) {
    return (getKeyphrase().isEmpty() || getKeyphrase().equalsIgnoreCase(paramString));
  }
  
  public boolean supportsLocale(Locale paramLocale) {
    return (getSupportedLocales().isEmpty() || getSupportedLocales().contains(paramLocale));
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("KeyphraseMetadata { id = ");
    stringBuilder.append(this.mId);
    stringBuilder.append(", keyphrase = ");
    stringBuilder.append(this.mKeyphrase);
    stringBuilder.append(", supportedLocales = ");
    stringBuilder.append(this.mSupportedLocales);
    stringBuilder.append(", recognitionModeFlags = ");
    stringBuilder.append(this.mRecognitionModeFlags);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mId == ((KeyphraseMetadata)paramObject).mId) {
      String str1 = this.mKeyphrase, str2 = ((KeyphraseMetadata)paramObject).mKeyphrase;
      if (Objects.equals(str1, str2)) {
        ArraySet<Locale> arraySet1 = this.mSupportedLocales, arraySet2 = ((KeyphraseMetadata)paramObject).mSupportedLocales;
        if (Objects.equals(arraySet1, arraySet2) && this.mRecognitionModeFlags == ((KeyphraseMetadata)paramObject).mRecognitionModeFlags)
          return null; 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = this.mId;
    int j = Objects.hashCode(this.mKeyphrase);
    int k = Objects.hashCode(this.mSupportedLocales);
    int m = this.mRecognitionModeFlags;
    return (((1 * 31 + i) * 31 + j) * 31 + k) * 31 + m;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mId);
    paramParcel.writeString(this.mKeyphrase);
    paramParcel.writeArraySet((ArraySet)this.mSupportedLocales);
    paramParcel.writeInt(this.mRecognitionModeFlags);
  }
  
  public int describeContents() {
    return 0;
  }
  
  KeyphraseMetadata(Parcel paramParcel) {
    int i = paramParcel.readInt();
    String str = paramParcel.readString();
    ArraySet<? extends Object> arraySet = paramParcel.readArraySet(null);
    int j = paramParcel.readInt();
    this.mId = i;
    this.mKeyphrase = str;
    AnnotationValidations.validate(NonNull.class, null, str);
    this.mSupportedLocales = (ArraySet)arraySet;
    AnnotationValidations.validate(NonNull.class, null, arraySet);
    this.mRecognitionModeFlags = j;
  }
  
  public static final Parcelable.Creator<KeyphraseMetadata> CREATOR = new Parcelable.Creator<KeyphraseMetadata>() {
      public KeyphraseMetadata[] newArray(int param1Int) {
        return new KeyphraseMetadata[param1Int];
      }
      
      public KeyphraseMetadata createFromParcel(Parcel param1Parcel) {
        return new KeyphraseMetadata(param1Parcel);
      }
    };
  
  private final int mId;
  
  private final String mKeyphrase;
  
  private final int mRecognitionModeFlags;
  
  private final ArraySet<Locale> mSupportedLocales;
  
  @Deprecated
  private void __metadata() {}
}
