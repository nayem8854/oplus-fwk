package android.hardware.soundtrigger;

import android.media.AudioFormat;
import android.media.audio.common.AudioConfig;
import android.media.soundtrigger_middleware.ConfidenceLevel;
import android.media.soundtrigger_middleware.ModelParameterRange;
import android.media.soundtrigger_middleware.Phrase;
import android.media.soundtrigger_middleware.PhraseRecognitionEvent;
import android.media.soundtrigger_middleware.PhraseRecognitionExtra;
import android.media.soundtrigger_middleware.PhraseSoundModel;
import android.media.soundtrigger_middleware.RecognitionConfig;
import android.media.soundtrigger_middleware.RecognitionEvent;
import android.media.soundtrigger_middleware.SoundModel;
import android.media.soundtrigger_middleware.SoundTriggerModuleDescriptor;
import android.media.soundtrigger_middleware.SoundTriggerModuleProperties;
import android.os.SharedMemory;
import android.system.ErrnoException;
import java.io.FileDescriptor;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.UUID;

class ConversionUtil {
  public static SoundTrigger.ModuleProperties aidl2apiModuleDescriptor(SoundTriggerModuleDescriptor paramSoundTriggerModuleDescriptor) {
    SoundTriggerModuleProperties soundTriggerModuleProperties = paramSoundTriggerModuleDescriptor.properties;
    int i = paramSoundTriggerModuleDescriptor.handle;
    String str1 = soundTriggerModuleProperties.implementor, str2 = soundTriggerModuleProperties.description, str3 = soundTriggerModuleProperties.uuid;
    int j = soundTriggerModuleProperties.version;
    String str4 = soundTriggerModuleProperties.supportedModelArch;
    int k = soundTriggerModuleProperties.maxSoundModels, m = soundTriggerModuleProperties.maxKeyPhrases, n = soundTriggerModuleProperties.maxUsers, i1 = soundTriggerModuleProperties.recognitionModes;
    i1 = aidl2apiRecognitionModes(i1);
    boolean bool1 = soundTriggerModuleProperties.captureTransition;
    int i2 = soundTriggerModuleProperties.maxBufferMs;
    boolean bool2 = soundTriggerModuleProperties.concurrentCapture;
    int i3 = soundTriggerModuleProperties.powerConsumptionMw;
    boolean bool3 = soundTriggerModuleProperties.triggerInEvent;
    int i4 = soundTriggerModuleProperties.audioCapabilities;
    return new SoundTrigger.ModuleProperties(i, str1, str2, str3, j, str4, k, m, n, i1, bool1, i2, bool2, i3, bool3, aidl2apiAudioCapabilities(i4));
  }
  
  public static int aidl2apiRecognitionModes(int paramInt) {
    int i = 0;
    if ((paramInt & 0x1) != 0)
      i = false | true; 
    int j = i;
    if ((paramInt & 0x2) != 0)
      j = i | 0x2; 
    i = j;
    if ((paramInt & 0x4) != 0)
      i = j | 0x4; 
    j = i;
    if ((paramInt & 0x8) != 0)
      j = i | 0x8; 
    return j;
  }
  
  public static int api2aidlRecognitionModes(int paramInt) {
    int i = 0;
    if ((paramInt & 0x1) != 0)
      i = false | true; 
    int j = i;
    if ((paramInt & 0x2) != 0)
      j = i | 0x2; 
    i = j;
    if ((paramInt & 0x4) != 0)
      i = j | 0x4; 
    j = i;
    if ((paramInt & 0x8) != 0)
      j = i | 0x8; 
    return j;
  }
  
  public static SoundModel api2aidlGenericSoundModel(SoundTrigger.GenericSoundModel paramGenericSoundModel) {
    return api2aidlSoundModel(paramGenericSoundModel);
  }
  
  public static SoundModel api2aidlSoundModel(SoundTrigger.SoundModel paramSoundModel) {
    SoundModel soundModel = new SoundModel();
    soundModel.type = paramSoundModel.getType();
    soundModel.uuid = api2aidlUuid(paramSoundModel.getUuid());
    soundModel.vendorUuid = api2aidlUuid(paramSoundModel.getVendorUuid());
    soundModel.data = byteArrayToSharedMemory(paramSoundModel.getData(), "SoundTrigger SoundModel");
    soundModel.dataSize = (paramSoundModel.getData()).length;
    return soundModel;
  }
  
  public static String api2aidlUuid(UUID paramUUID) {
    return paramUUID.toString();
  }
  
  public static PhraseSoundModel api2aidlPhraseSoundModel(SoundTrigger.KeyphraseSoundModel paramKeyphraseSoundModel) {
    PhraseSoundModel phraseSoundModel = new PhraseSoundModel();
    phraseSoundModel.common = api2aidlSoundModel(paramKeyphraseSoundModel);
    phraseSoundModel.phrases = new Phrase[(paramKeyphraseSoundModel.getKeyphrases()).length];
    for (byte b = 0; b < (paramKeyphraseSoundModel.getKeyphrases()).length; b++)
      phraseSoundModel.phrases[b] = api2aidlPhrase(paramKeyphraseSoundModel.getKeyphrases()[b]); 
    return phraseSoundModel;
  }
  
  public static Phrase api2aidlPhrase(SoundTrigger.Keyphrase paramKeyphrase) {
    Phrase phrase = new Phrase();
    phrase.id = paramKeyphrase.getId();
    phrase.recognitionModes = api2aidlRecognitionModes(paramKeyphrase.getRecognitionModes());
    phrase.users = Arrays.copyOf(paramKeyphrase.getUsers(), (paramKeyphrase.getUsers()).length);
    phrase.locale = paramKeyphrase.getLocale().toLanguageTag();
    phrase.text = paramKeyphrase.getText();
    return phrase;
  }
  
  public static RecognitionConfig api2aidlRecognitionConfig(SoundTrigger.RecognitionConfig paramRecognitionConfig) {
    RecognitionConfig recognitionConfig = new RecognitionConfig();
    recognitionConfig.captureRequested = paramRecognitionConfig.captureRequested;
    recognitionConfig.phraseRecognitionExtras = new PhraseRecognitionExtra[paramRecognitionConfig.keyphrases.length];
    for (byte b = 0; b < paramRecognitionConfig.keyphrases.length; b++)
      recognitionConfig.phraseRecognitionExtras[b] = api2aidlPhraseRecognitionExtra(paramRecognitionConfig.keyphrases[b]); 
    recognitionConfig.data = Arrays.copyOf(paramRecognitionConfig.data, paramRecognitionConfig.data.length);
    recognitionConfig.audioCapabilities = api2aidlAudioCapabilities(paramRecognitionConfig.audioCapabilities);
    return recognitionConfig;
  }
  
  public static PhraseRecognitionExtra api2aidlPhraseRecognitionExtra(SoundTrigger.KeyphraseRecognitionExtra paramKeyphraseRecognitionExtra) {
    PhraseRecognitionExtra phraseRecognitionExtra = new PhraseRecognitionExtra();
    phraseRecognitionExtra.id = paramKeyphraseRecognitionExtra.id;
    phraseRecognitionExtra.recognitionModes = api2aidlRecognitionModes(paramKeyphraseRecognitionExtra.recognitionModes);
    phraseRecognitionExtra.confidenceLevel = paramKeyphraseRecognitionExtra.coarseConfidenceLevel;
    phraseRecognitionExtra.levels = new ConfidenceLevel[paramKeyphraseRecognitionExtra.confidenceLevels.length];
    for (byte b = 0; b < paramKeyphraseRecognitionExtra.confidenceLevels.length; b++)
      phraseRecognitionExtra.levels[b] = api2aidlConfidenceLevel(paramKeyphraseRecognitionExtra.confidenceLevels[b]); 
    return phraseRecognitionExtra;
  }
  
  public static SoundTrigger.KeyphraseRecognitionExtra aidl2apiPhraseRecognitionExtra(PhraseRecognitionExtra paramPhraseRecognitionExtra) {
    SoundTrigger.ConfidenceLevel[] arrayOfConfidenceLevel = new SoundTrigger.ConfidenceLevel[paramPhraseRecognitionExtra.levels.length];
    int i;
    for (i = 0; i < paramPhraseRecognitionExtra.levels.length; i++)
      arrayOfConfidenceLevel[i] = aidl2apiConfidenceLevel(paramPhraseRecognitionExtra.levels[i]); 
    i = paramPhraseRecognitionExtra.id;
    int j = paramPhraseRecognitionExtra.recognitionModes;
    return 
      new SoundTrigger.KeyphraseRecognitionExtra(i, aidl2apiRecognitionModes(j), paramPhraseRecognitionExtra.confidenceLevel, arrayOfConfidenceLevel);
  }
  
  public static ConfidenceLevel api2aidlConfidenceLevel(SoundTrigger.ConfidenceLevel paramConfidenceLevel) {
    ConfidenceLevel confidenceLevel = new ConfidenceLevel();
    confidenceLevel.levelPercent = paramConfidenceLevel.confidenceLevel;
    confidenceLevel.userId = paramConfidenceLevel.userId;
    return confidenceLevel;
  }
  
  public static SoundTrigger.ConfidenceLevel aidl2apiConfidenceLevel(ConfidenceLevel paramConfidenceLevel) {
    return new SoundTrigger.ConfidenceLevel(paramConfidenceLevel.userId, paramConfidenceLevel.levelPercent);
  }
  
  public static SoundTrigger.RecognitionEvent aidl2apiRecognitionEvent(int paramInt, RecognitionEvent paramRecognitionEvent) {
    AudioFormat audioFormat = aidl2apiAudioFormatWithDefault(paramRecognitionEvent.audioConfig);
    return new SoundTrigger.GenericRecognitionEvent(paramRecognitionEvent.status, paramInt, paramRecognitionEvent.captureAvailable, paramRecognitionEvent.captureSession, paramRecognitionEvent.captureDelayMs, paramRecognitionEvent.capturePreambleMs, paramRecognitionEvent.triggerInData, audioFormat, paramRecognitionEvent.data);
  }
  
  public static SoundTrigger.RecognitionEvent aidl2apiPhraseRecognitionEvent(int paramInt, PhraseRecognitionEvent paramPhraseRecognitionEvent) {
    SoundTrigger.KeyphraseRecognitionExtra[] arrayOfKeyphraseRecognitionExtra = new SoundTrigger.KeyphraseRecognitionExtra[paramPhraseRecognitionEvent.phraseExtras.length];
    for (byte b = 0; b < paramPhraseRecognitionEvent.phraseExtras.length; b++)
      arrayOfKeyphraseRecognitionExtra[b] = aidl2apiPhraseRecognitionExtra(paramPhraseRecognitionEvent.phraseExtras[b]); 
    AudioFormat audioFormat = aidl2apiAudioFormatWithDefault(paramPhraseRecognitionEvent.common.audioConfig);
    return new SoundTrigger.KeyphraseRecognitionEvent(paramPhraseRecognitionEvent.common.status, paramInt, paramPhraseRecognitionEvent.common.captureAvailable, paramPhraseRecognitionEvent.common.captureSession, paramPhraseRecognitionEvent.common.captureDelayMs, paramPhraseRecognitionEvent.common.capturePreambleMs, paramPhraseRecognitionEvent.common.triggerInData, audioFormat, paramPhraseRecognitionEvent.common.data, arrayOfKeyphraseRecognitionExtra);
  }
  
  public static AudioFormat aidl2apiAudioFormat(AudioConfig paramAudioConfig) {
    AudioFormat.Builder builder = new AudioFormat.Builder();
    builder.setSampleRate(paramAudioConfig.sampleRateHz);
    builder.setChannelMask(aidl2apiChannelInMask(paramAudioConfig.channelMask));
    builder.setEncoding(aidl2apiEncoding(paramAudioConfig.format));
    return builder.build();
  }
  
  public static AudioFormat aidl2apiAudioFormatWithDefault(AudioConfig paramAudioConfig) {
    if (paramAudioConfig != null)
      return aidl2apiAudioFormat(paramAudioConfig); 
    return (new AudioFormat.Builder()).build();
  }
  
  public static int aidl2apiEncoding(int paramInt) {
    if (paramInt != 167772160) {
      if (paramInt != 167772161) {
        switch (paramInt) {
          default:
            switch (paramInt) {
              default:
                switch (paramInt) {
                  default:
                    return 0;
                  case 603979776:
                  case 603979777:
                  case 603979778:
                  case 603979779:
                    break;
                } 
                return 19;
              case 570425344:
                return 17;
              case 234881024:
                return 14;
              case 218103808:
                return 13;
              case 201326592:
                return 8;
              case 184549376:
                return 7;
              case 150994944:
                return 5;
              case 67109632:
                return 16;
              case 67109376:
                return 15;
              case 67109120:
                return 12;
              case 67108880:
                return 11;
              case 67108866:
                return 10;
              case 16777216:
                break;
            } 
            return 9;
          case 3:
          case 4:
          case 5:
          case 6:
            return 4;
          case 2:
            return 3;
          case 1:
            return 2;
          case 0:
            break;
        } 
        return 1;
      } 
      return 18;
    } 
    return 6;
  }
  
  public static int api2aidlModelParameter(int paramInt) {
    if (paramInt != 0)
      return -1; 
    return 0;
  }
  
  public static int aidl2apiChannelInMask(int paramInt) {
    return paramInt;
  }
  
  public static SoundTrigger.ModelParamRange aidl2apiModelParameterRange(ModelParameterRange paramModelParameterRange) {
    if (paramModelParameterRange == null)
      return null; 
    return new SoundTrigger.ModelParamRange(paramModelParameterRange.minInclusive, paramModelParameterRange.maxInclusive);
  }
  
  public static int aidl2apiAudioCapabilities(int paramInt) {
    int i = 0;
    if ((paramInt & 0x1) != 0)
      i = false | true; 
    int j = i;
    if ((paramInt & 0x2) != 0)
      j = i | 0x2; 
    return j;
  }
  
  public static int api2aidlAudioCapabilities(int paramInt) {
    int i = 0;
    if ((paramInt & 0x1) != 0)
      i = false | true; 
    int j = i;
    if ((paramInt & 0x2) != 0)
      j = i | 0x2; 
    return j;
  }
  
  private static FileDescriptor byteArrayToSharedMemory(byte[] paramArrayOfbyte, String paramString) {
    if (paramArrayOfbyte.length == 0)
      return null; 
    if (paramString == null)
      paramString = ""; 
    try {
      SharedMemory sharedMemory = SharedMemory.create(paramString, paramArrayOfbyte.length);
      ByteBuffer byteBuffer = sharedMemory.mapReadWrite();
      byteBuffer.put(paramArrayOfbyte);
      SharedMemory.unmap(byteBuffer);
      return sharedMemory.getFileDescriptor();
    } catch (ErrnoException errnoException) {
      throw new RuntimeException(errnoException);
    } 
  }
}
