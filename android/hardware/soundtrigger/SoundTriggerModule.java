package android.hardware.soundtrigger;

import android.media.soundtrigger_middleware.ISoundTriggerCallback;
import android.media.soundtrigger_middleware.ISoundTriggerMiddlewareService;
import android.media.soundtrigger_middleware.ISoundTriggerModule;
import android.media.soundtrigger_middleware.PhraseRecognitionEvent;
import android.media.soundtrigger_middleware.PhraseSoundModel;
import android.media.soundtrigger_middleware.RecognitionConfig;
import android.media.soundtrigger_middleware.RecognitionEvent;
import android.media.soundtrigger_middleware.SoundModel;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;

public class SoundTriggerModule {
  private static final int EVENT_RECOGNITION = 1;
  
  private static final int EVENT_SERVICE_DIED = 2;
  
  private static final int EVENT_SERVICE_STATE_CHANGE = 3;
  
  private static final String TAG = "SoundTriggerModule";
  
  private EventHandlerDelegate mEventHandlerDelegate;
  
  private int mId;
  
  private ISoundTriggerModule mService;
  
  SoundTriggerModule(ISoundTriggerMiddlewareService paramISoundTriggerMiddlewareService, int paramInt, SoundTrigger.StatusListener paramStatusListener, Looper paramLooper) throws RemoteException {
    this.mId = paramInt;
    EventHandlerDelegate eventHandlerDelegate = new EventHandlerDelegate(paramStatusListener, paramLooper);
    ISoundTriggerModule iSoundTriggerModule = paramISoundTriggerMiddlewareService.attach(paramInt, eventHandlerDelegate);
    iSoundTriggerModule.asBinder().linkToDeath(this.mEventHandlerDelegate, 0);
  }
  
  protected void finalize() {
    detach();
  }
  
  public void detach() {
    /* monitor enter ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
    try {
      if (this.mService != null) {
        this.mService.asBinder().unlinkToDeath(this.mEventHandlerDelegate, 0);
        this.mService.detach();
        this.mService = null;
      } 
    } catch (Exception exception) {
      SoundTrigger.handleException(exception);
    } finally {
      Exception exception;
    } 
    /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
  }
  
  public int loadSoundModel(SoundTrigger.SoundModel paramSoundModel, int[] paramArrayOfint) {
    /* monitor enter ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
    try {
      SoundModel soundModel;
      if (paramSoundModel instanceof SoundTrigger.GenericSoundModel) {
        soundModel = ConversionUtil.api2aidlGenericSoundModel((SoundTrigger.GenericSoundModel)paramSoundModel);
        paramArrayOfint[0] = this.mService.loadModel(soundModel);
        /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
        return 0;
      } 
      if (soundModel instanceof SoundTrigger.KeyphraseSoundModel) {
        PhraseSoundModel phraseSoundModel = ConversionUtil.api2aidlPhraseSoundModel((SoundTrigger.KeyphraseSoundModel)soundModel);
        paramArrayOfint[0] = this.mService.loadPhraseModel(phraseSoundModel);
        /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
        return 0;
      } 
      int i = SoundTrigger.STATUS_BAD_VALUE;
      /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
      return i;
    } catch (Exception exception) {
      int i = SoundTrigger.handleException(exception);
      /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
      return i;
    } finally {}
    /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
    throw paramSoundModel;
  }
  
  public int unloadSoundModel(int paramInt) {
    Exception exception;
    /* monitor enter ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
    try {
      this.mService.unloadModel(paramInt);
      /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
      return 0;
    } catch (Exception null) {
      paramInt = SoundTrigger.handleException(exception);
      /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
      return paramInt;
    } finally {}
    /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
    throw exception;
  }
  
  public int startRecognition(int paramInt, SoundTrigger.RecognitionConfig paramRecognitionConfig) {
    /* monitor enter ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
    try {
      ISoundTriggerModule iSoundTriggerModule = this.mService;
      RecognitionConfig recognitionConfig = ConversionUtil.api2aidlRecognitionConfig(paramRecognitionConfig);
      iSoundTriggerModule.startRecognition(paramInt, recognitionConfig);
      /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
      return 0;
    } catch (Exception exception) {
      paramInt = SoundTrigger.handleException(exception);
      /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
      return paramInt;
    } finally {}
    /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
    throw paramRecognitionConfig;
  }
  
  public int stopRecognition(int paramInt) {
    Exception exception;
    /* monitor enter ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
    try {
      this.mService.stopRecognition(paramInt);
      /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
      return 0;
    } catch (Exception null) {
      paramInt = SoundTrigger.handleException(exception);
      /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
      return paramInt;
    } finally {}
    /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
    throw exception;
  }
  
  public int getModelState(int paramInt) {
    Exception exception;
    /* monitor enter ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
    try {
      this.mService.forceRecognitionEvent(paramInt);
      /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
      return 0;
    } catch (Exception null) {
      paramInt = SoundTrigger.handleException(exception);
      /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
      return paramInt;
    } finally {}
    /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
    throw exception;
  }
  
  public int setParameter(int paramInt1, int paramInt2, int paramInt3) {
    Exception exception;
    /* monitor enter ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
    try {
      ISoundTriggerModule iSoundTriggerModule = this.mService;
      paramInt2 = ConversionUtil.api2aidlModelParameter(paramInt2);
      iSoundTriggerModule.setModelParameter(paramInt1, paramInt2, paramInt3);
      /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
      return 0;
    } catch (Exception null) {
      paramInt1 = SoundTrigger.handleException(exception);
      /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
      return paramInt1;
    } finally {}
    /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
    throw exception;
  }
  
  public int getParameter(int paramInt1, int paramInt2) {
    Exception exception;
    /* monitor enter ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
    try {
      ISoundTriggerModule iSoundTriggerModule = this.mService;
      paramInt2 = ConversionUtil.api2aidlModelParameter(paramInt2);
      paramInt1 = iSoundTriggerModule.getModelParameter(paramInt1, paramInt2);
      /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
      return paramInt1;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } finally {}
    /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
    throw exception;
  }
  
  public SoundTrigger.ModelParamRange queryParameter(int paramInt1, int paramInt2) {
    Exception exception;
    /* monitor enter ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
    try {
      ISoundTriggerModule iSoundTriggerModule = this.mService;
      paramInt2 = ConversionUtil.api2aidlModelParameter(paramInt2);
      SoundTrigger.ModelParamRange modelParamRange = ConversionUtil.aidl2apiModelParameterRange(iSoundTriggerModule.queryModelParameterSupport(paramInt1, paramInt2));
      /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
      return modelParamRange;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } finally {}
    /* monitor exit ThisExpression{ObjectType{android/hardware/soundtrigger/SoundTriggerModule}} */
    throw exception;
  }
  
  class EventHandlerDelegate extends ISoundTriggerCallback.Stub implements IBinder.DeathRecipient {
    private final Handler mHandler;
    
    final SoundTriggerModule this$0;
    
    EventHandlerDelegate(SoundTrigger.StatusListener param1StatusListener, Looper param1Looper) {
      this.mHandler = (Handler)new Object(this, param1Looper, SoundTriggerModule.this, param1StatusListener);
    }
    
    public void onRecognition(int param1Int, RecognitionEvent param1RecognitionEvent) throws RemoteException {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mHandler : Landroid/os/Handler;
      //   6: astore_3
      //   7: iload_1
      //   8: aload_2
      //   9: invokestatic aidl2apiRecognitionEvent : (ILandroid/media/soundtrigger_middleware/RecognitionEvent;)Landroid/hardware/soundtrigger/SoundTrigger$RecognitionEvent;
      //   12: astore_2
      //   13: aload_3
      //   14: iconst_1
      //   15: aload_2
      //   16: invokevirtual obtainMessage : (ILjava/lang/Object;)Landroid/os/Message;
      //   19: astore_2
      //   20: aload_0
      //   21: getfield mHandler : Landroid/os/Handler;
      //   24: aload_2
      //   25: invokevirtual sendMessage : (Landroid/os/Message;)Z
      //   28: pop
      //   29: aload_0
      //   30: monitorexit
      //   31: return
      //   32: astore_2
      //   33: aload_0
      //   34: monitorexit
      //   35: aload_2
      //   36: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #322	-> 2
      //   #323	-> 7
      //   #322	-> 13
      //   #324	-> 20
      //   #325	-> 29
      //   #321	-> 32
      // Exception table:
      //   from	to	target	type
      //   2	7	32	finally
      //   7	13	32	finally
      //   13	20	32	finally
      //   20	29	32	finally
    }
    
    public void onPhraseRecognition(int param1Int, PhraseRecognitionEvent param1PhraseRecognitionEvent) throws RemoteException {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mHandler : Landroid/os/Handler;
      //   6: astore_3
      //   7: iload_1
      //   8: aload_2
      //   9: invokestatic aidl2apiPhraseRecognitionEvent : (ILandroid/media/soundtrigger_middleware/PhraseRecognitionEvent;)Landroid/hardware/soundtrigger/SoundTrigger$RecognitionEvent;
      //   12: astore_2
      //   13: aload_3
      //   14: iconst_1
      //   15: aload_2
      //   16: invokevirtual obtainMessage : (ILjava/lang/Object;)Landroid/os/Message;
      //   19: astore_2
      //   20: aload_0
      //   21: getfield mHandler : Landroid/os/Handler;
      //   24: aload_2
      //   25: invokevirtual sendMessage : (Landroid/os/Message;)Z
      //   28: pop
      //   29: aload_0
      //   30: monitorexit
      //   31: return
      //   32: astore_2
      //   33: aload_0
      //   34: monitorexit
      //   35: aload_2
      //   36: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #330	-> 2
      //   #331	-> 7
      //   #330	-> 13
      //   #332	-> 20
      //   #333	-> 29
      //   #329	-> 32
      // Exception table:
      //   from	to	target	type
      //   2	7	32	finally
      //   7	13	32	finally
      //   13	20	32	finally
      //   20	29	32	finally
    }
    
    public void onRecognitionAvailabilityChange(boolean param1Boolean) throws RemoteException {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mHandler : Landroid/os/Handler;
      //   6: astore_2
      //   7: iload_1
      //   8: ifeq -> 16
      //   11: iconst_0
      //   12: istore_3
      //   13: goto -> 18
      //   16: iconst_1
      //   17: istore_3
      //   18: aload_2
      //   19: iconst_3
      //   20: iload_3
      //   21: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   24: invokevirtual obtainMessage : (ILjava/lang/Object;)Landroid/os/Message;
      //   27: astore_2
      //   28: aload_0
      //   29: getfield mHandler : Landroid/os/Handler;
      //   32: aload_2
      //   33: invokevirtual sendMessage : (Landroid/os/Message;)Z
      //   36: pop
      //   37: aload_0
      //   38: monitorexit
      //   39: return
      //   40: astore_2
      //   41: aload_0
      //   42: monitorexit
      //   43: aload_2
      //   44: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #338	-> 2
      //   #339	-> 7
      //   #340	-> 16
      //   #339	-> 18
      //   #338	-> 18
      //   #341	-> 28
      //   #342	-> 37
      //   #337	-> 40
      // Exception table:
      //   from	to	target	type
      //   2	7	40	finally
      //   18	28	40	finally
      //   28	37	40	finally
    }
    
    public void onModuleDied() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mHandler : Landroid/os/Handler;
      //   6: iconst_2
      //   7: invokevirtual obtainMessage : (I)Landroid/os/Message;
      //   10: astore_1
      //   11: aload_0
      //   12: getfield mHandler : Landroid/os/Handler;
      //   15: aload_1
      //   16: invokevirtual sendMessage : (Landroid/os/Message;)Z
      //   19: pop
      //   20: aload_0
      //   21: monitorexit
      //   22: return
      //   23: astore_1
      //   24: aload_0
      //   25: monitorexit
      //   26: aload_1
      //   27: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #346	-> 2
      //   #347	-> 11
      //   #348	-> 20
      //   #345	-> 23
      // Exception table:
      //   from	to	target	type
      //   2	11	23	finally
      //   11	20	23	finally
    }
    
    public void binderDied() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mHandler : Landroid/os/Handler;
      //   6: iconst_2
      //   7: invokevirtual obtainMessage : (I)Landroid/os/Message;
      //   10: astore_1
      //   11: aload_0
      //   12: getfield mHandler : Landroid/os/Handler;
      //   15: aload_1
      //   16: invokevirtual sendMessage : (Landroid/os/Message;)Z
      //   19: pop
      //   20: aload_0
      //   21: monitorexit
      //   22: return
      //   23: astore_1
      //   24: aload_0
      //   25: monitorexit
      //   26: aload_1
      //   27: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #352	-> 2
      //   #353	-> 11
      //   #354	-> 20
      //   #351	-> 23
      // Exception table:
      //   from	to	target	type
      //   2	11	23	finally
      //   11	20	23	finally
    }
  }
}
