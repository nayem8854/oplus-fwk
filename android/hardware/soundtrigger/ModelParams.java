package android.hardware.soundtrigger;

public @interface ModelParams {
  public static final int INVALID = -1;
  
  public static final int THRESHOLD_FACTOR = 0;
}
