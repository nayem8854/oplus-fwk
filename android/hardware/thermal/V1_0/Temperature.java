package android.hardware.thermal.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class Temperature {
  public int type = 0;
  
  public String name = new String();
  
  public float currentValue = 0.0F;
  
  public float throttlingThreshold = 0.0F;
  
  public float shutdownThreshold = 0.0F;
  
  public float vrThrottlingThreshold = 0.0F;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != Temperature.class)
      return false; 
    paramObject = paramObject;
    if (this.type != ((Temperature)paramObject).type)
      return false; 
    if (!HidlSupport.deepEquals(this.name, ((Temperature)paramObject).name))
      return false; 
    if (this.currentValue != ((Temperature)paramObject).currentValue)
      return false; 
    if (this.throttlingThreshold != ((Temperature)paramObject).throttlingThreshold)
      return false; 
    if (this.shutdownThreshold != ((Temperature)paramObject).shutdownThreshold)
      return false; 
    if (this.vrThrottlingThreshold != ((Temperature)paramObject).vrThrottlingThreshold)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.type;
    int j = HidlSupport.deepHashCode(Integer.valueOf(i));
    String str = this.name;
    int k = HidlSupport.deepHashCode(str);
    float f = this.currentValue;
    i = HidlSupport.deepHashCode(Float.valueOf(f));
    f = this.throttlingThreshold;
    int m = HidlSupport.deepHashCode(Float.valueOf(f));
    f = this.shutdownThreshold;
    int n = HidlSupport.deepHashCode(Float.valueOf(f));
    f = this.vrThrottlingThreshold;
    int i1 = HidlSupport.deepHashCode(Float.valueOf(f));
    return Objects.hash(new Object[] { Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(i), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".type = ");
    stringBuilder.append(TemperatureType.toString(this.type));
    stringBuilder.append(", .name = ");
    stringBuilder.append(this.name);
    stringBuilder.append(", .currentValue = ");
    stringBuilder.append(this.currentValue);
    stringBuilder.append(", .throttlingThreshold = ");
    stringBuilder.append(this.throttlingThreshold);
    stringBuilder.append(", .shutdownThreshold = ");
    stringBuilder.append(this.shutdownThreshold);
    stringBuilder.append(", .vrThrottlingThreshold = ");
    stringBuilder.append(this.vrThrottlingThreshold);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(40L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<Temperature> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<Temperature> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 40);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      Temperature temperature = new Temperature();
      temperature.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 40));
      arrayList.add(temperature);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.type = paramHwBlob.getInt32(paramLong + 0L);
    String str = paramHwBlob.getString(paramLong + 8L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 8L + 0L, false);
    this.currentValue = paramHwBlob.getFloat(paramLong + 24L);
    this.throttlingThreshold = paramHwBlob.getFloat(paramLong + 28L);
    this.shutdownThreshold = paramHwBlob.getFloat(paramLong + 32L);
    this.vrThrottlingThreshold = paramHwBlob.getFloat(paramLong + 36L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(40);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<Temperature> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 40);
    for (byte b = 0; b < i; b++)
      ((Temperature)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 40)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.type);
    paramHwBlob.putString(8L + paramLong, this.name);
    paramHwBlob.putFloat(24L + paramLong, this.currentValue);
    paramHwBlob.putFloat(28L + paramLong, this.throttlingThreshold);
    paramHwBlob.putFloat(32L + paramLong, this.shutdownThreshold);
    paramHwBlob.putFloat(36L + paramLong, this.vrThrottlingThreshold);
  }
}
