package android.hardware.thermal.V1_0;

import java.util.ArrayList;

public final class CoolingType {
  public static final int FAN_RPM = 0;
  
  public static final String toString(int paramInt) {
    if (paramInt == 0)
      return "FAN_RPM"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    arrayList.add("FAN_RPM");
    if (paramInt != 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((0x0 ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
