package android.hardware.thermal.V1_0;

import java.util.ArrayList;

public final class TemperatureType {
  public static final int BATTERY = 2;
  
  public static final int CPU = 0;
  
  public static final int GPU = 1;
  
  public static final int SKIN = 3;
  
  public static final int UNKNOWN = -1;
  
  public static final String toString(int paramInt) {
    if (paramInt == -1)
      return "UNKNOWN"; 
    if (paramInt == 0)
      return "CPU"; 
    if (paramInt == 1)
      return "GPU"; 
    if (paramInt == 2)
      return "BATTERY"; 
    if (paramInt == 3)
      return "SKIN"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    if ((paramInt & 0xFFFFFFFF) == -1) {
      arrayList.add("UNKNOWN");
      i = 0x0 | 0xFFFFFFFF;
    } 
    arrayList.add("CPU");
    int j = i;
    if ((paramInt & 0x1) == 1) {
      arrayList.add("GPU");
      j = i | 0x1;
    } 
    i = j;
    if ((paramInt & 0x2) == 2) {
      arrayList.add("BATTERY");
      i = j | 0x2;
    } 
    j = i;
    if ((paramInt & 0x3) == 3) {
      arrayList.add("SKIN");
      j = i | 0x3;
    } 
    if (paramInt != j) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((j ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
