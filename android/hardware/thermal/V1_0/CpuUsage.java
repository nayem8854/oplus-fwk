package android.hardware.thermal.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CpuUsage {
  public String name = new String();
  
  public long active = 0L;
  
  public long total = 0L;
  
  public boolean isOnline = false;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CpuUsage.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.name, ((CpuUsage)paramObject).name))
      return false; 
    if (this.active != ((CpuUsage)paramObject).active)
      return false; 
    if (this.total != ((CpuUsage)paramObject).total)
      return false; 
    if (this.isOnline != ((CpuUsage)paramObject).isOnline)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    String str = this.name;
    int i = HidlSupport.deepHashCode(str);
    long l = this.active;
    int j = HidlSupport.deepHashCode(Long.valueOf(l));
    l = this.total;
    int k = HidlSupport.deepHashCode(Long.valueOf(l));
    boolean bool = this.isOnline;
    int m = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".name = ");
    stringBuilder.append(this.name);
    stringBuilder.append(", .active = ");
    stringBuilder.append(this.active);
    stringBuilder.append(", .total = ");
    stringBuilder.append(this.total);
    stringBuilder.append(", .isOnline = ");
    stringBuilder.append(this.isOnline);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(40L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CpuUsage> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CpuUsage> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 40);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CpuUsage cpuUsage = new CpuUsage();
      cpuUsage.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 40));
      arrayList.add(cpuUsage);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    String str = paramHwBlob.getString(paramLong + 0L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 0L + 0L, false);
    this.active = paramHwBlob.getInt64(16L + paramLong);
    this.total = paramHwBlob.getInt64(24L + paramLong);
    this.isOnline = paramHwBlob.getBool(32L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(40);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CpuUsage> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 40);
    for (byte b = 0; b < i; b++)
      ((CpuUsage)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 40)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putString(0L + paramLong, this.name);
    paramHwBlob.putInt64(16L + paramLong, this.active);
    paramHwBlob.putInt64(24L + paramLong, this.total);
    paramHwBlob.putBool(32L + paramLong, this.isOnline);
  }
}
