package android.hardware.thermal.V2_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public final class TemperatureThreshold {
  public int type = 0;
  
  public String name = new String();
  
  public float[] hotThrottlingThresholds = new float[7];
  
  public float[] coldThrottlingThresholds = new float[7];
  
  public float vrThrottlingThreshold = 0.0F;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != TemperatureThreshold.class)
      return false; 
    paramObject = paramObject;
    if (this.type != ((TemperatureThreshold)paramObject).type)
      return false; 
    if (!HidlSupport.deepEquals(this.name, ((TemperatureThreshold)paramObject).name))
      return false; 
    if (!HidlSupport.deepEquals(this.hotThrottlingThresholds, ((TemperatureThreshold)paramObject).hotThrottlingThresholds))
      return false; 
    if (!HidlSupport.deepEquals(this.coldThrottlingThresholds, ((TemperatureThreshold)paramObject).coldThrottlingThresholds))
      return false; 
    if (this.vrThrottlingThreshold != ((TemperatureThreshold)paramObject).vrThrottlingThreshold)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.type;
    int j = HidlSupport.deepHashCode(Integer.valueOf(i));
    String str = this.name;
    int k = HidlSupport.deepHashCode(str);
    float[] arrayOfFloat = this.hotThrottlingThresholds;
    int m = HidlSupport.deepHashCode(arrayOfFloat);
    arrayOfFloat = this.coldThrottlingThresholds;
    int n = HidlSupport.deepHashCode(arrayOfFloat);
    float f = this.vrThrottlingThreshold;
    i = HidlSupport.deepHashCode(Float.valueOf(f));
    return Objects.hash(new Object[] { Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".type = ");
    stringBuilder.append(TemperatureType.toString(this.type));
    stringBuilder.append(", .name = ");
    stringBuilder.append(this.name);
    stringBuilder.append(", .hotThrottlingThresholds = ");
    stringBuilder.append(Arrays.toString(this.hotThrottlingThresholds));
    stringBuilder.append(", .coldThrottlingThresholds = ");
    stringBuilder.append(Arrays.toString(this.coldThrottlingThresholds));
    stringBuilder.append(", .vrThrottlingThreshold = ");
    stringBuilder.append(this.vrThrottlingThreshold);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(88L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<TemperatureThreshold> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<TemperatureThreshold> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 88);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      TemperatureThreshold temperatureThreshold = new TemperatureThreshold();
      temperatureThreshold.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 88));
      arrayList.add(temperatureThreshold);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.type = paramHwBlob.getInt32(paramLong + 0L);
    String str = paramHwBlob.getString(paramLong + 8L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 8L + 0L, false);
    paramHwBlob.copyToFloatArray(paramLong + 24L, this.hotThrottlingThresholds, 7);
    paramHwBlob.copyToFloatArray(paramLong + 52L, this.coldThrottlingThresholds, 7);
    this.vrThrottlingThreshold = paramHwBlob.getFloat(paramLong + 80L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(88);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<TemperatureThreshold> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 88);
    for (byte b = 0; b < i; b++)
      ((TemperatureThreshold)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 88)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.type);
    paramHwBlob.putString(8L + paramLong, this.name);
    float[] arrayOfFloat = this.hotThrottlingThresholds;
    if (arrayOfFloat != null && arrayOfFloat.length == 7) {
      paramHwBlob.putFloatArray(24L + paramLong, arrayOfFloat);
      arrayOfFloat = this.coldThrottlingThresholds;
      if (arrayOfFloat != null && arrayOfFloat.length == 7) {
        paramHwBlob.putFloatArray(52L + paramLong, arrayOfFloat);
        paramHwBlob.putFloat(80L + paramLong, this.vrThrottlingThreshold);
        return;
      } 
      throw new IllegalArgumentException("Array element is not of the expected length");
    } 
    throw new IllegalArgumentException("Array element is not of the expected length");
  }
}
