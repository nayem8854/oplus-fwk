package android.hardware.thermal.V2_0;

import java.util.ArrayList;

public final class CoolingType {
  public static final int BATTERY = 1;
  
  public static final int COMPONENT = 6;
  
  public static final int CPU = 2;
  
  public static final int FAN = 0;
  
  public static final int GPU = 3;
  
  public static final int MODEM = 4;
  
  public static final int NPU = 5;
  
  public static final String toString(int paramInt) {
    if (paramInt == 0)
      return "FAN"; 
    if (paramInt == 1)
      return "BATTERY"; 
    if (paramInt == 2)
      return "CPU"; 
    if (paramInt == 3)
      return "GPU"; 
    if (paramInt == 4)
      return "MODEM"; 
    if (paramInt == 5)
      return "NPU"; 
    if (paramInt == 6)
      return "COMPONENT"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    arrayList.add("FAN");
    if ((paramInt & 0x1) == 1) {
      arrayList.add("BATTERY");
      i = false | true;
    } 
    int j = i;
    if ((paramInt & 0x2) == 2) {
      arrayList.add("CPU");
      j = i | 0x2;
    } 
    i = j;
    if ((paramInt & 0x3) == 3) {
      arrayList.add("GPU");
      i = j | 0x3;
    } 
    j = i;
    if ((paramInt & 0x4) == 4) {
      arrayList.add("MODEM");
      j = i | 0x4;
    } 
    i = j;
    if ((paramInt & 0x5) == 5) {
      arrayList.add("NPU");
      i = j | 0x5;
    } 
    j = i;
    if ((paramInt & 0x6) == 6) {
      arrayList.add("COMPONENT");
      j = i | 0x6;
    } 
    if (paramInt != j) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((j ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
