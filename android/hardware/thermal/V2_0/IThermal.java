package android.hardware.thermal.V2_0;

import android.hardware.thermal.V1_0.CoolingDevice;
import android.hardware.thermal.V1_0.CpuUsage;
import android.hardware.thermal.V1_0.IThermal;
import android.hardware.thermal.V1_0.Temperature;
import android.hardware.thermal.V1_0.ThermalStatus;
import android.internal.hidl.base.V1_0.DebugInfo;
import android.os.HidlSupport;
import android.os.HwBinder;
import android.os.HwBlob;
import android.os.HwParcel;
import android.os.IHwBinder;
import android.os.IHwInterface;
import android.os.NativeHandle;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public interface IThermal extends IThermal {
  public static final String kInterfaceName = "android.hardware.thermal@2.0::IThermal";
  
  static IThermal asInterface(IHwBinder paramIHwBinder) {
    if (paramIHwBinder == null)
      return null; 
    IHwInterface iHwInterface = paramIHwBinder.queryLocalInterface("android.hardware.thermal@2.0::IThermal");
    if (iHwInterface != null && iHwInterface instanceof IThermal)
      return (IThermal)iHwInterface; 
    Proxy proxy = new Proxy(paramIHwBinder);
    try {
      for (String str : proxy.interfaceChain()) {
        boolean bool = str.equals("android.hardware.thermal@2.0::IThermal");
        if (bool)
          return proxy; 
      } 
    } catch (RemoteException remoteException) {}
    return null;
  }
  
  static IThermal castFrom(IHwInterface paramIHwInterface) {
    if (paramIHwInterface == null) {
      paramIHwInterface = null;
    } else {
      paramIHwInterface = asInterface(paramIHwInterface.asBinder());
    } 
    return (IThermal)paramIHwInterface;
  }
  
  static IThermal getService(String paramString, boolean paramBoolean) throws RemoteException {
    return asInterface(HwBinder.getService("android.hardware.thermal@2.0::IThermal", paramString, paramBoolean));
  }
  
  static IThermal getService(boolean paramBoolean) throws RemoteException {
    return getService("default", paramBoolean);
  }
  
  static IThermal getService(String paramString) throws RemoteException {
    return asInterface(HwBinder.getService("android.hardware.thermal@2.0::IThermal", paramString));
  }
  
  static IThermal getService() throws RemoteException {
    return getService("default");
  }
  
  IHwBinder asBinder();
  
  void debug(NativeHandle paramNativeHandle, ArrayList<String> paramArrayList) throws RemoteException;
  
  void getCurrentCoolingDevices(boolean paramBoolean, int paramInt, getCurrentCoolingDevicesCallback paramgetCurrentCoolingDevicesCallback) throws RemoteException;
  
  void getCurrentTemperatures(boolean paramBoolean, int paramInt, getCurrentTemperaturesCallback paramgetCurrentTemperaturesCallback) throws RemoteException;
  
  DebugInfo getDebugInfo() throws RemoteException;
  
  ArrayList<byte[]> getHashChain() throws RemoteException;
  
  void getTemperatureThresholds(boolean paramBoolean, int paramInt, getTemperatureThresholdsCallback paramgetTemperatureThresholdsCallback) throws RemoteException;
  
  ArrayList<String> interfaceChain() throws RemoteException;
  
  String interfaceDescriptor() throws RemoteException;
  
  boolean linkToDeath(IHwBinder.DeathRecipient paramDeathRecipient, long paramLong) throws RemoteException;
  
  void notifySyspropsChanged() throws RemoteException;
  
  void ping() throws RemoteException;
  
  ThermalStatus registerThermalChangedCallback(IThermalChangedCallback paramIThermalChangedCallback, boolean paramBoolean, int paramInt) throws RemoteException;
  
  void setHALInstrumentation() throws RemoteException;
  
  boolean unlinkToDeath(IHwBinder.DeathRecipient paramDeathRecipient) throws RemoteException;
  
  ThermalStatus unregisterThermalChangedCallback(IThermalChangedCallback paramIThermalChangedCallback) throws RemoteException;
  
  class Proxy implements IThermal {
    private IHwBinder mRemote;
    
    public Proxy(IThermal this$0) {
      Objects.requireNonNull(this$0);
      this.mRemote = (IHwBinder)this$0;
    }
    
    public IHwBinder asBinder() {
      return this.mRemote;
    }
    
    public String toString() {
      try {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(interfaceDescriptor());
        stringBuilder.append("@Proxy");
        return stringBuilder.toString();
      } catch (RemoteException remoteException) {
        return "[class or subclass of android.hardware.thermal@2.0::IThermal]@Proxy";
      } 
    }
    
    public final boolean equals(Object param1Object) {
      return HidlSupport.interfacesEqual(this, param1Object);
    }
    
    public final int hashCode() {
      return asBinder().hashCode();
    }
    
    public void getTemperatures(IThermal.getTemperaturesCallback param1getTemperaturesCallback) throws RemoteException {
      HwParcel hwParcel1 = new HwParcel();
      hwParcel1.writeInterfaceToken("android.hardware.thermal@1.0::IThermal");
      HwParcel hwParcel2 = new HwParcel();
      try {
        this.mRemote.transact(1, hwParcel1, hwParcel2, 0);
        hwParcel2.verifySuccess();
        hwParcel1.releaseTemporaryStorage();
        ThermalStatus thermalStatus = new ThermalStatus();
        this();
        thermalStatus.readFromParcel(hwParcel2);
        ArrayList<Temperature> arrayList = Temperature.readVectorFromParcel(hwParcel2);
        param1getTemperaturesCallback.onValues(thermalStatus, arrayList);
        return;
      } finally {
        hwParcel2.release();
      } 
    }
    
    public void getCpuUsages(IThermal.getCpuUsagesCallback param1getCpuUsagesCallback) throws RemoteException {
      HwParcel hwParcel1 = new HwParcel();
      hwParcel1.writeInterfaceToken("android.hardware.thermal@1.0::IThermal");
      HwParcel hwParcel2 = new HwParcel();
      try {
        this.mRemote.transact(2, hwParcel1, hwParcel2, 0);
        hwParcel2.verifySuccess();
        hwParcel1.releaseTemporaryStorage();
        ThermalStatus thermalStatus = new ThermalStatus();
        this();
        thermalStatus.readFromParcel(hwParcel2);
        ArrayList<CpuUsage> arrayList = CpuUsage.readVectorFromParcel(hwParcel2);
        param1getCpuUsagesCallback.onValues(thermalStatus, arrayList);
        return;
      } finally {
        hwParcel2.release();
      } 
    }
    
    public void getCoolingDevices(IThermal.getCoolingDevicesCallback param1getCoolingDevicesCallback) throws RemoteException {
      HwParcel hwParcel1 = new HwParcel();
      hwParcel1.writeInterfaceToken("android.hardware.thermal@1.0::IThermal");
      HwParcel hwParcel2 = new HwParcel();
      try {
        this.mRemote.transact(3, hwParcel1, hwParcel2, 0);
        hwParcel2.verifySuccess();
        hwParcel1.releaseTemporaryStorage();
        ThermalStatus thermalStatus = new ThermalStatus();
        this();
        thermalStatus.readFromParcel(hwParcel2);
        ArrayList<CoolingDevice> arrayList = CoolingDevice.readVectorFromParcel(hwParcel2);
        param1getCoolingDevicesCallback.onValues(thermalStatus, arrayList);
        return;
      } finally {
        hwParcel2.release();
      } 
    }
    
    public void getCurrentTemperatures(boolean param1Boolean, int param1Int, IThermal.getCurrentTemperaturesCallback param1getCurrentTemperaturesCallback) throws RemoteException {
      HwParcel hwParcel1 = new HwParcel();
      hwParcel1.writeInterfaceToken("android.hardware.thermal@2.0::IThermal");
      hwParcel1.writeBool(param1Boolean);
      hwParcel1.writeInt32(param1Int);
      HwParcel hwParcel2 = new HwParcel();
      try {
        this.mRemote.transact(4, hwParcel1, hwParcel2, 0);
        hwParcel2.verifySuccess();
        hwParcel1.releaseTemporaryStorage();
        ThermalStatus thermalStatus = new ThermalStatus();
        this();
        thermalStatus.readFromParcel(hwParcel2);
        ArrayList<Temperature> arrayList = Temperature.readVectorFromParcel(hwParcel2);
        param1getCurrentTemperaturesCallback.onValues(thermalStatus, arrayList);
        return;
      } finally {
        hwParcel2.release();
      } 
    }
    
    public void getTemperatureThresholds(boolean param1Boolean, int param1Int, IThermal.getTemperatureThresholdsCallback param1getTemperatureThresholdsCallback) throws RemoteException {
      HwParcel hwParcel1 = new HwParcel();
      hwParcel1.writeInterfaceToken("android.hardware.thermal@2.0::IThermal");
      hwParcel1.writeBool(param1Boolean);
      hwParcel1.writeInt32(param1Int);
      HwParcel hwParcel2 = new HwParcel();
      try {
        this.mRemote.transact(5, hwParcel1, hwParcel2, 0);
        hwParcel2.verifySuccess();
        hwParcel1.releaseTemporaryStorage();
        ThermalStatus thermalStatus = new ThermalStatus();
        this();
        thermalStatus.readFromParcel(hwParcel2);
        ArrayList<TemperatureThreshold> arrayList = TemperatureThreshold.readVectorFromParcel(hwParcel2);
        param1getTemperatureThresholdsCallback.onValues(thermalStatus, arrayList);
        return;
      } finally {
        hwParcel2.release();
      } 
    }
    
    public ThermalStatus registerThermalChangedCallback(IThermalChangedCallback param1IThermalChangedCallback, boolean param1Boolean, int param1Int) throws RemoteException {
      IHwBinder iHwBinder;
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.thermal@2.0::IThermal");
      if (param1IThermalChangedCallback == null) {
        param1IThermalChangedCallback = null;
      } else {
        iHwBinder = param1IThermalChangedCallback.asBinder();
      } 
      null.writeStrongBinder(iHwBinder);
      null.writeBool(param1Boolean);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(6, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ThermalStatus thermalStatus = new ThermalStatus();
        this();
        thermalStatus.readFromParcel(hwParcel);
        return thermalStatus;
      } finally {
        hwParcel.release();
      } 
    }
    
    public ThermalStatus unregisterThermalChangedCallback(IThermalChangedCallback param1IThermalChangedCallback) throws RemoteException {
      IHwBinder iHwBinder;
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.thermal@2.0::IThermal");
      if (param1IThermalChangedCallback == null) {
        param1IThermalChangedCallback = null;
      } else {
        iHwBinder = param1IThermalChangedCallback.asBinder();
      } 
      null.writeStrongBinder(iHwBinder);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(7, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ThermalStatus thermalStatus = new ThermalStatus();
        this();
        thermalStatus.readFromParcel(hwParcel);
        return thermalStatus;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getCurrentCoolingDevices(boolean param1Boolean, int param1Int, IThermal.getCurrentCoolingDevicesCallback param1getCurrentCoolingDevicesCallback) throws RemoteException {
      HwParcel hwParcel1 = new HwParcel();
      hwParcel1.writeInterfaceToken("android.hardware.thermal@2.0::IThermal");
      hwParcel1.writeBool(param1Boolean);
      hwParcel1.writeInt32(param1Int);
      HwParcel hwParcel2 = new HwParcel();
      try {
        this.mRemote.transact(8, hwParcel1, hwParcel2, 0);
        hwParcel2.verifySuccess();
        hwParcel1.releaseTemporaryStorage();
        ThermalStatus thermalStatus = new ThermalStatus();
        this();
        thermalStatus.readFromParcel(hwParcel2);
        ArrayList<CoolingDevice> arrayList = CoolingDevice.readVectorFromParcel(hwParcel2);
        param1getCurrentCoolingDevicesCallback.onValues(thermalStatus, arrayList);
        return;
      } finally {
        hwParcel2.release();
      } 
    }
    
    public ArrayList<String> interfaceChain() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256067662, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readStringVector();
      } finally {
        hwParcel.release();
      } 
    }
    
    public void debug(NativeHandle param1NativeHandle, ArrayList<String> param1ArrayList) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hidl.base@1.0::IBase");
      hwParcel2.writeNativeHandle(param1NativeHandle);
      hwParcel2.writeStringVector(param1ArrayList);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(256131655, hwParcel2, hwParcel1, 0);
        hwParcel1.verifySuccess();
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public String interfaceDescriptor() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256136003, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readString();
      } finally {
        hwParcel.release();
      } 
    }
    
    public ArrayList<byte[]> getHashChain() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256398152, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ArrayList<byte[]> arrayList = new ArrayList();
        this();
        HwBlob hwBlob1 = hwParcel.readBuffer(16L);
        int i = hwBlob1.getInt32(8L);
        long l1 = (i * 32);
        long l2 = hwBlob1.handle();
        HwBlob hwBlob2 = hwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
        arrayList.clear();
        for (byte b = 0; b < i; b++) {
          byte[] arrayOfByte = new byte[32];
          l1 = (b * 32);
          hwBlob2.copyToInt8Array(l1, arrayOfByte, 32);
          arrayList.add(arrayOfByte);
        } 
        return arrayList;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setHALInstrumentation() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256462420, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public boolean linkToDeath(IHwBinder.DeathRecipient param1DeathRecipient, long param1Long) throws RemoteException {
      return this.mRemote.linkToDeath(param1DeathRecipient, param1Long);
    }
    
    public void ping() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256921159, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public DebugInfo getDebugInfo() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(257049926, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        DebugInfo debugInfo = new DebugInfo();
        this();
        debugInfo.readFromParcel(hwParcel);
        return debugInfo;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void notifySyspropsChanged() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(257120595, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public boolean unlinkToDeath(IHwBinder.DeathRecipient param1DeathRecipient) throws RemoteException {
      return this.mRemote.unlinkToDeath(param1DeathRecipient);
    }
  }
  
  class Stub extends HwBinder implements IThermal {
    public IHwBinder asBinder() {
      return this;
    }
    
    public final ArrayList<String> interfaceChain() {
      return new ArrayList<>(Arrays.asList(new String[] { "android.hardware.thermal@2.0::IThermal", "android.hardware.thermal@1.0::IThermal", "android.hidl.base@1.0::IBase" }));
    }
    
    public void debug(NativeHandle param1NativeHandle, ArrayList<String> param1ArrayList) {}
    
    public final String interfaceDescriptor() {
      return "android.hardware.thermal@2.0::IThermal";
    }
    
    public final ArrayList<byte[]> getHashChain() {
      return (ArrayList)new ArrayList<>(Arrays.asList((byte[])new byte[][] { { 
                -67, -120, -76, -122, 57, -54, -29, 9, -126, 2, 
                16, 36, -30, 35, 113, 7, 108, 118, -6, -88, 
                70, 110, 56, -54, 89, -123, 41, 69, 43, 97, 
                -114, -82 }, { 
                -105, -15, -20, 68, 96, 67, -68, 90, 102, 69, 
                -73, 69, 41, -90, 39, 100, -106, -67, -77, 94, 
                10, -18, 65, -19, -91, 92, -71, 45, 81, -21, 
                120, 2 }, { 
                -20, Byte.MAX_VALUE, -41, -98, -48, 45, -6, -123, -68, 73, 
                -108, 38, -83, -82, 62, -66, 35, -17, 5, 36, 
                -13, -51, 105, 87, 19, -109, 36, -72, 59, 24, 
                -54, 76 } }));
    }
    
    public final void setHALInstrumentation() {}
    
    public final boolean linkToDeath(IHwBinder.DeathRecipient param1DeathRecipient, long param1Long) {
      return true;
    }
    
    public final void ping() {}
    
    public final DebugInfo getDebugInfo() {
      DebugInfo debugInfo = new DebugInfo();
      debugInfo.pid = HidlSupport.getPidIfSharable();
      debugInfo.ptr = 0L;
      debugInfo.arch = 0;
      return debugInfo;
    }
    
    public final void notifySyspropsChanged() {
      HwBinder.enableInstrumentation();
    }
    
    public final boolean unlinkToDeath(IHwBinder.DeathRecipient param1DeathRecipient) {
      return true;
    }
    
    public IHwInterface queryLocalInterface(String param1String) {
      if ("android.hardware.thermal@2.0::IThermal".equals(param1String))
        return this; 
      return null;
    }
    
    public void registerAsService(String param1String) throws RemoteException {
      registerService(param1String);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(interfaceDescriptor());
      stringBuilder.append("@Stub");
      return stringBuilder.toString();
    }
    
    public void onTransact(int param1Int1, HwParcel param1HwParcel1, HwParcel param1HwParcel2, int param1Int2) throws RemoteException {
      DebugInfo debugInfo;
      ArrayList<byte[]> arrayList1;
      String str;
      ArrayList<String> arrayList;
      IThermalChangedCallback iThermalChangedCallback1;
      ThermalStatus thermalStatus;
      HwBlob hwBlob1, hwBlob2;
      NativeHandle nativeHandle;
      IThermalChangedCallback iThermalChangedCallback2;
      boolean bool;
      switch (param1Int1) {
        default:
          switch (param1Int1) {
            default:
              return;
            case 257120595:
              param1HwParcel1.enforceInterface("android.hidl.base@1.0::IBase");
              notifySyspropsChanged();
            case 257049926:
              param1HwParcel1.enforceInterface("android.hidl.base@1.0::IBase");
              debugInfo = getDebugInfo();
              param1HwParcel2.writeStatus(0);
              debugInfo.writeToParcel(param1HwParcel2);
              param1HwParcel2.send();
            case 256921159:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              ping();
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.send();
            case 256462420:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              setHALInstrumentation();
            case 256398152:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              arrayList1 = getHashChain();
              param1HwParcel2.writeStatus(0);
              hwBlob1 = new HwBlob(16);
              param1Int2 = arrayList1.size();
              hwBlob1.putInt32(8L, param1Int2);
              hwBlob1.putBool(12L, false);
              hwBlob2 = new HwBlob(param1Int2 * 32);
              for (param1Int1 = 0; param1Int1 < param1Int2; ) {
                long l = (param1Int1 * 32);
                byte[] arrayOfByte = arrayList1.get(param1Int1);
                if (arrayOfByte != null && arrayOfByte.length == 32) {
                  hwBlob2.putInt8Array(l, arrayOfByte);
                  param1Int1++;
                } 
                throw new IllegalArgumentException("Array element is not of the expected length");
              } 
              hwBlob1.putBlob(0L, hwBlob2);
              param1HwParcel2.writeBuffer(hwBlob1);
              param1HwParcel2.send();
            case 256136003:
              arrayList1.enforceInterface("android.hidl.base@1.0::IBase");
              str = interfaceDescriptor();
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.writeString(str);
              param1HwParcel2.send();
            case 256131655:
              str.enforceInterface("android.hidl.base@1.0::IBase");
              nativeHandle = str.readNativeHandle();
              arrayList = str.readStringVector();
              debug(nativeHandle, arrayList);
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.send();
            case 256067662:
              break;
          } 
          arrayList.enforceInterface("android.hidl.base@1.0::IBase");
          arrayList = interfaceChain();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeStringVector(arrayList);
          param1HwParcel2.send();
        case 8:
          arrayList.enforceInterface("android.hardware.thermal@2.0::IThermal");
          bool = arrayList.readBool();
          param1Int1 = arrayList.readInt32();
          getCurrentCoolingDevices(bool, param1Int1, (IThermal.getCurrentCoolingDevicesCallback)new Object(this, param1HwParcel2));
        case 7:
          arrayList.enforceInterface("android.hardware.thermal@2.0::IThermal");
          iThermalChangedCallback1 = IThermalChangedCallback.asInterface(arrayList.readStrongBinder());
          thermalStatus = unregisterThermalChangedCallback(iThermalChangedCallback1);
          param1HwParcel2.writeStatus(0);
          thermalStatus.writeToParcel(param1HwParcel2);
          param1HwParcel2.send();
        case 6:
          thermalStatus.enforceInterface("android.hardware.thermal@2.0::IThermal");
          iThermalChangedCallback2 = IThermalChangedCallback.asInterface(thermalStatus.readStrongBinder());
          bool = thermalStatus.readBool();
          param1Int1 = thermalStatus.readInt32();
          thermalStatus = registerThermalChangedCallback(iThermalChangedCallback2, bool, param1Int1);
          param1HwParcel2.writeStatus(0);
          thermalStatus.writeToParcel(param1HwParcel2);
          param1HwParcel2.send();
        case 5:
          thermalStatus.enforceInterface("android.hardware.thermal@2.0::IThermal");
          bool = thermalStatus.readBool();
          param1Int1 = thermalStatus.readInt32();
          getTemperatureThresholds(bool, param1Int1, (IThermal.getTemperatureThresholdsCallback)new Object(this, param1HwParcel2));
        case 4:
          thermalStatus.enforceInterface("android.hardware.thermal@2.0::IThermal");
          bool = thermalStatus.readBool();
          param1Int1 = thermalStatus.readInt32();
          getCurrentTemperatures(bool, param1Int1, (IThermal.getCurrentTemperaturesCallback)new Object(this, param1HwParcel2));
        case 3:
          thermalStatus.enforceInterface("android.hardware.thermal@1.0::IThermal");
          getCoolingDevices((IThermal.getCoolingDevicesCallback)new Object(this, param1HwParcel2));
        case 2:
          thermalStatus.enforceInterface("android.hardware.thermal@1.0::IThermal");
          getCpuUsages((IThermal.getCpuUsagesCallback)new Object(this, param1HwParcel2));
        case 1:
          break;
      } 
      thermalStatus.enforceInterface("android.hardware.thermal@1.0::IThermal");
      getTemperatures((IThermal.getTemperaturesCallback)new Object(this, param1HwParcel2));
    }
  }
  
  @FunctionalInterface
  class getCurrentCoolingDevicesCallback {
    public abstract void onValues(ThermalStatus param1ThermalStatus, ArrayList<CoolingDevice> param1ArrayList);
  }
  
  @FunctionalInterface
  class getCurrentTemperaturesCallback {
    public abstract void onValues(ThermalStatus param1ThermalStatus, ArrayList<Temperature> param1ArrayList);
  }
  
  @FunctionalInterface
  class getTemperatureThresholdsCallback {
    public abstract void onValues(ThermalStatus param1ThermalStatus, ArrayList<TemperatureThreshold> param1ArrayList);
  }
}
