package android.hardware.thermal.V2_0;

import java.util.ArrayList;

public final class ThrottlingSeverity {
  public static final int CRITICAL = 4;
  
  public static final int EMERGENCY = 5;
  
  public static final int LIGHT = 1;
  
  public static final int MODERATE = 2;
  
  public static final int NONE = 0;
  
  public static final int SEVERE = 3;
  
  public static final int SHUTDOWN = 6;
  
  public static final String toString(int paramInt) {
    if (paramInt == 0)
      return "NONE"; 
    if (paramInt == 1)
      return "LIGHT"; 
    if (paramInt == 2)
      return "MODERATE"; 
    if (paramInt == 3)
      return "SEVERE"; 
    if (paramInt == 4)
      return "CRITICAL"; 
    if (paramInt == 5)
      return "EMERGENCY"; 
    if (paramInt == 6)
      return "SHUTDOWN"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    arrayList.add("NONE");
    if ((paramInt & 0x1) == 1) {
      arrayList.add("LIGHT");
      i = false | true;
    } 
    int j = i;
    if ((paramInt & 0x2) == 2) {
      arrayList.add("MODERATE");
      j = i | 0x2;
    } 
    i = j;
    if ((paramInt & 0x3) == 3) {
      arrayList.add("SEVERE");
      i = j | 0x3;
    } 
    int k = i;
    if ((paramInt & 0x4) == 4) {
      arrayList.add("CRITICAL");
      k = i | 0x4;
    } 
    j = k;
    if ((paramInt & 0x5) == 5) {
      arrayList.add("EMERGENCY");
      j = k | 0x5;
    } 
    i = j;
    if ((paramInt & 0x6) == 6) {
      arrayList.add("SHUTDOWN");
      i = j | 0x6;
    } 
    if (paramInt != i) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((i ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
