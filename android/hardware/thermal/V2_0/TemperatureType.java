package android.hardware.thermal.V2_0;

import java.util.ArrayList;

public final class TemperatureType {
  public static final int BATTERY = 2;
  
  public static final int BCL_CURRENT = 7;
  
  public static final int BCL_PERCENTAGE = 8;
  
  public static final int BCL_VOLTAGE = 6;
  
  public static final int CPU = 0;
  
  public static final int GPU = 1;
  
  public static final int NPU = 9;
  
  public static final int POWER_AMPLIFIER = 5;
  
  public static final int SKIN = 3;
  
  public static final int UNKNOWN = -1;
  
  public static final int USB_PORT = 4;
  
  public static final String toString(int paramInt) {
    if (paramInt == -1)
      return "UNKNOWN"; 
    if (paramInt == 0)
      return "CPU"; 
    if (paramInt == 1)
      return "GPU"; 
    if (paramInt == 2)
      return "BATTERY"; 
    if (paramInt == 3)
      return "SKIN"; 
    if (paramInt == 4)
      return "USB_PORT"; 
    if (paramInt == 5)
      return "POWER_AMPLIFIER"; 
    if (paramInt == 6)
      return "BCL_VOLTAGE"; 
    if (paramInt == 7)
      return "BCL_CURRENT"; 
    if (paramInt == 8)
      return "BCL_PERCENTAGE"; 
    if (paramInt == 9)
      return "NPU"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    if ((paramInt & 0xFFFFFFFF) == -1) {
      arrayList.add("UNKNOWN");
      i = 0x0 | 0xFFFFFFFF;
    } 
    arrayList.add("CPU");
    int j = i;
    if ((paramInt & 0x1) == 1) {
      arrayList.add("GPU");
      j = i | 0x1;
    } 
    i = j;
    if ((paramInt & 0x2) == 2) {
      arrayList.add("BATTERY");
      i = j | 0x2;
    } 
    j = i;
    if ((paramInt & 0x3) == 3) {
      arrayList.add("SKIN");
      j = i | 0x3;
    } 
    i = j;
    if ((paramInt & 0x4) == 4) {
      arrayList.add("USB_PORT");
      i = j | 0x4;
    } 
    j = i;
    if ((paramInt & 0x5) == 5) {
      arrayList.add("POWER_AMPLIFIER");
      j = i | 0x5;
    } 
    i = j;
    if ((paramInt & 0x6) == 6) {
      arrayList.add("BCL_VOLTAGE");
      i = j | 0x6;
    } 
    j = i;
    if ((paramInt & 0x7) == 7) {
      arrayList.add("BCL_CURRENT");
      j = i | 0x7;
    } 
    i = j;
    if ((paramInt & 0x8) == 8) {
      arrayList.add("BCL_PERCENTAGE");
      i = j | 0x8;
    } 
    j = i;
    if ((paramInt & 0x9) == 9) {
      arrayList.add("NPU");
      j = i | 0x9;
    } 
    if (paramInt != j) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((j ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
