package android.hardware;

public interface SensorEventListener {
  void onAccuracyChanged(Sensor paramSensor, int paramInt);
  
  void onSensorChanged(SensorEvent paramSensorEvent);
}
