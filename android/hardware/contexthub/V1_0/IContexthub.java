package android.hardware.contexthub.V1_0;

import android.internal.hidl.base.V1_0.DebugInfo;
import android.internal.hidl.base.V1_0.IBase;
import android.os.HidlSupport;
import android.os.HwBinder;
import android.os.HwBlob;
import android.os.HwParcel;
import android.os.IHwBinder;
import android.os.IHwInterface;
import android.os.NativeHandle;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public interface IContexthub extends IBase {
  public static final String kInterfaceName = "android.hardware.contexthub@1.0::IContexthub";
  
  static IContexthub asInterface(IHwBinder paramIHwBinder) {
    if (paramIHwBinder == null)
      return null; 
    IHwInterface iHwInterface = paramIHwBinder.queryLocalInterface("android.hardware.contexthub@1.0::IContexthub");
    if (iHwInterface != null && iHwInterface instanceof IContexthub)
      return (IContexthub)iHwInterface; 
    Proxy proxy = new Proxy(paramIHwBinder);
    try {
      for (String str : proxy.interfaceChain()) {
        boolean bool = str.equals("android.hardware.contexthub@1.0::IContexthub");
        if (bool)
          return proxy; 
      } 
    } catch (RemoteException remoteException) {}
    return null;
  }
  
  static IContexthub castFrom(IHwInterface paramIHwInterface) {
    IContexthub iContexthub;
    if (paramIHwInterface == null) {
      paramIHwInterface = null;
    } else {
      iContexthub = asInterface(paramIHwInterface.asBinder());
    } 
    return iContexthub;
  }
  
  static IContexthub getService(String paramString, boolean paramBoolean) throws RemoteException {
    return asInterface(HwBinder.getService("android.hardware.contexthub@1.0::IContexthub", paramString, paramBoolean));
  }
  
  static IContexthub getService(boolean paramBoolean) throws RemoteException {
    return getService("default", paramBoolean);
  }
  
  static IContexthub getService(String paramString) throws RemoteException {
    return asInterface(HwBinder.getService("android.hardware.contexthub@1.0::IContexthub", paramString));
  }
  
  static IContexthub getService() throws RemoteException {
    return getService("default");
  }
  
  IHwBinder asBinder();
  
  void debug(NativeHandle paramNativeHandle, ArrayList<String> paramArrayList) throws RemoteException;
  
  int disableNanoApp(int paramInt1, long paramLong, int paramInt2) throws RemoteException;
  
  int enableNanoApp(int paramInt1, long paramLong, int paramInt2) throws RemoteException;
  
  DebugInfo getDebugInfo() throws RemoteException;
  
  ArrayList<byte[]> getHashChain() throws RemoteException;
  
  ArrayList<ContextHub> getHubs() throws RemoteException;
  
  ArrayList<String> interfaceChain() throws RemoteException;
  
  String interfaceDescriptor() throws RemoteException;
  
  boolean linkToDeath(IHwBinder.DeathRecipient paramDeathRecipient, long paramLong) throws RemoteException;
  
  int loadNanoApp(int paramInt1, NanoAppBinary paramNanoAppBinary, int paramInt2) throws RemoteException;
  
  void notifySyspropsChanged() throws RemoteException;
  
  void ping() throws RemoteException;
  
  int queryApps(int paramInt) throws RemoteException;
  
  int registerCallback(int paramInt, IContexthubCallback paramIContexthubCallback) throws RemoteException;
  
  int sendMessageToHub(int paramInt, ContextHubMsg paramContextHubMsg) throws RemoteException;
  
  void setHALInstrumentation() throws RemoteException;
  
  boolean unlinkToDeath(IHwBinder.DeathRecipient paramDeathRecipient) throws RemoteException;
  
  int unloadNanoApp(int paramInt1, long paramLong, int paramInt2) throws RemoteException;
  
  class Proxy implements IContexthub {
    private IHwBinder mRemote;
    
    public Proxy(IContexthub this$0) {
      Objects.requireNonNull(this$0);
      this.mRemote = (IHwBinder)this$0;
    }
    
    public IHwBinder asBinder() {
      return this.mRemote;
    }
    
    public String toString() {
      try {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(interfaceDescriptor());
        stringBuilder.append("@Proxy");
        return stringBuilder.toString();
      } catch (RemoteException remoteException) {
        return "[class or subclass of android.hardware.contexthub@1.0::IContexthub]@Proxy";
      } 
    }
    
    public final boolean equals(Object param1Object) {
      return HidlSupport.interfacesEqual((IHwInterface)this, param1Object);
    }
    
    public final int hashCode() {
      return asBinder().hashCode();
    }
    
    public ArrayList<ContextHub> getHubs() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.contexthub@1.0::IContexthub");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(1, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return ContextHub.readVectorFromParcel(hwParcel);
      } finally {
        hwParcel.release();
      } 
    }
    
    public int registerCallback(int param1Int, IContexthubCallback param1IContexthubCallback) throws RemoteException {
      IHwBinder iHwBinder;
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.contexthub@1.0::IContexthub");
      null.writeInt32(param1Int);
      if (param1IContexthubCallback == null) {
        param1IContexthubCallback = null;
      } else {
        iHwBinder = param1IContexthubCallback.asBinder();
      } 
      null.writeStrongBinder(iHwBinder);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(2, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        param1Int = hwParcel.readInt32();
        return param1Int;
      } finally {
        hwParcel.release();
      } 
    }
    
    public int sendMessageToHub(int param1Int, ContextHubMsg param1ContextHubMsg) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.contexthub@1.0::IContexthub");
      null.writeInt32(param1Int);
      param1ContextHubMsg.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(3, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        param1Int = hwParcel.readInt32();
        return param1Int;
      } finally {
        hwParcel.release();
      } 
    }
    
    public int loadNanoApp(int param1Int1, NanoAppBinary param1NanoAppBinary, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.contexthub@1.0::IContexthub");
      null.writeInt32(param1Int1);
      param1NanoAppBinary.writeToParcel(null);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(4, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        param1Int1 = hwParcel.readInt32();
        return param1Int1;
      } finally {
        hwParcel.release();
      } 
    }
    
    public int unloadNanoApp(int param1Int1, long param1Long, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.contexthub@1.0::IContexthub");
      null.writeInt32(param1Int1);
      null.writeInt64(param1Long);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(5, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        param1Int1 = hwParcel.readInt32();
        return param1Int1;
      } finally {
        hwParcel.release();
      } 
    }
    
    public int enableNanoApp(int param1Int1, long param1Long, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.contexthub@1.0::IContexthub");
      null.writeInt32(param1Int1);
      null.writeInt64(param1Long);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(6, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        param1Int1 = hwParcel.readInt32();
        return param1Int1;
      } finally {
        hwParcel.release();
      } 
    }
    
    public int disableNanoApp(int param1Int1, long param1Long, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.contexthub@1.0::IContexthub");
      null.writeInt32(param1Int1);
      null.writeInt64(param1Long);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(7, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        param1Int1 = hwParcel.readInt32();
        return param1Int1;
      } finally {
        hwParcel.release();
      } 
    }
    
    public int queryApps(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.contexthub@1.0::IContexthub");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(8, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        param1Int = hwParcel.readInt32();
        return param1Int;
      } finally {
        hwParcel.release();
      } 
    }
    
    public ArrayList<String> interfaceChain() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256067662, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readStringVector();
      } finally {
        hwParcel.release();
      } 
    }
    
    public void debug(NativeHandle param1NativeHandle, ArrayList<String> param1ArrayList) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hidl.base@1.0::IBase");
      hwParcel2.writeNativeHandle(param1NativeHandle);
      hwParcel2.writeStringVector(param1ArrayList);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(256131655, hwParcel2, hwParcel1, 0);
        hwParcel1.verifySuccess();
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public String interfaceDescriptor() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256136003, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readString();
      } finally {
        hwParcel.release();
      } 
    }
    
    public ArrayList<byte[]> getHashChain() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256398152, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ArrayList<byte[]> arrayList = new ArrayList();
        this();
        HwBlob hwBlob1 = hwParcel.readBuffer(16L);
        int i = hwBlob1.getInt32(8L);
        long l1 = (i * 32);
        long l2 = hwBlob1.handle();
        HwBlob hwBlob2 = hwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
        arrayList.clear();
        for (byte b = 0; b < i; b++) {
          byte[] arrayOfByte = new byte[32];
          l1 = (b * 32);
          hwBlob2.copyToInt8Array(l1, arrayOfByte, 32);
          arrayList.add(arrayOfByte);
        } 
        return arrayList;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setHALInstrumentation() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256462420, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public boolean linkToDeath(IHwBinder.DeathRecipient param1DeathRecipient, long param1Long) throws RemoteException {
      return this.mRemote.linkToDeath(param1DeathRecipient, param1Long);
    }
    
    public void ping() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256921159, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public DebugInfo getDebugInfo() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(257049926, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        DebugInfo debugInfo = new DebugInfo();
        this();
        debugInfo.readFromParcel(hwParcel);
        return debugInfo;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void notifySyspropsChanged() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(257120595, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public boolean unlinkToDeath(IHwBinder.DeathRecipient param1DeathRecipient) throws RemoteException {
      return this.mRemote.unlinkToDeath(param1DeathRecipient);
    }
  }
  
  class Stub extends HwBinder implements IContexthub {
    public IHwBinder asBinder() {
      return (IHwBinder)this;
    }
    
    public final ArrayList<String> interfaceChain() {
      return new ArrayList<>(Arrays.asList(new String[] { "android.hardware.contexthub@1.0::IContexthub", "android.hidl.base@1.0::IBase" }));
    }
    
    public void debug(NativeHandle param1NativeHandle, ArrayList<String> param1ArrayList) {}
    
    public final String interfaceDescriptor() {
      return "android.hardware.contexthub@1.0::IContexthub";
    }
    
    public final ArrayList<byte[]> getHashChain() {
      return (ArrayList)new ArrayList<>(Arrays.asList((byte[])new byte[][] { { 
                -91, -82, 15, -24, 102, Byte.MAX_VALUE, 11, 26, -16, -101, 
                19, -25, 45, 41, 96, 15, 78, -77, -123, 59, 
                37, 112, 121, -60, 90, -103, -74, -12, -93, 54, 
                6, 73 }, { 
                -20, Byte.MAX_VALUE, -41, -98, -48, 45, -6, -123, -68, 73, 
                -108, 38, -83, -82, 62, -66, 35, -17, 5, 36, 
                -13, -51, 105, 87, 19, -109, 36, -72, 59, 24, 
                -54, 76 } }));
    }
    
    public final void setHALInstrumentation() {}
    
    public final boolean linkToDeath(IHwBinder.DeathRecipient param1DeathRecipient, long param1Long) {
      return true;
    }
    
    public final void ping() {}
    
    public final DebugInfo getDebugInfo() {
      DebugInfo debugInfo = new DebugInfo();
      debugInfo.pid = HidlSupport.getPidIfSharable();
      debugInfo.ptr = 0L;
      debugInfo.arch = 0;
      return debugInfo;
    }
    
    public final void notifySyspropsChanged() {
      HwBinder.enableInstrumentation();
    }
    
    public final boolean unlinkToDeath(IHwBinder.DeathRecipient param1DeathRecipient) {
      return true;
    }
    
    public IHwInterface queryLocalInterface(String param1String) {
      if ("android.hardware.contexthub@1.0::IContexthub".equals(param1String))
        return (IHwInterface)this; 
      return null;
    }
    
    public void registerAsService(String param1String) throws RemoteException {
      registerService(param1String);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(interfaceDescriptor());
      stringBuilder.append("@Stub");
      return stringBuilder.toString();
    }
    
    public void onTransact(int param1Int1, HwParcel param1HwParcel1, HwParcel param1HwParcel2, int param1Int2) throws RemoteException {
      DebugInfo debugInfo;
      HwBlob hwBlob1;
      String str;
      ArrayList<String> arrayList1;
      IContexthubCallback iContexthubCallback;
      ArrayList<byte[]> arrayList2;
      HwBlob hwBlob2;
      long l;
      NativeHandle nativeHandle;
      NanoAppBinary nanoAppBinary;
      ContextHubMsg contextHubMsg;
      switch (param1Int1) {
        default:
          switch (param1Int1) {
            default:
              return;
            case 257120595:
              param1HwParcel1.enforceInterface("android.hidl.base@1.0::IBase");
              notifySyspropsChanged();
            case 257049926:
              param1HwParcel1.enforceInterface("android.hidl.base@1.0::IBase");
              debugInfo = getDebugInfo();
              param1HwParcel2.writeStatus(0);
              debugInfo.writeToParcel(param1HwParcel2);
              param1HwParcel2.send();
            case 256921159:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              ping();
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.send();
            case 256462420:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              setHALInstrumentation();
            case 256398152:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              arrayList2 = getHashChain();
              param1HwParcel2.writeStatus(0);
              hwBlob2 = new HwBlob(16);
              param1Int2 = arrayList2.size();
              hwBlob2.putInt32(8L, param1Int2);
              hwBlob2.putBool(12L, false);
              hwBlob1 = new HwBlob(param1Int2 * 32);
              for (param1Int1 = 0; param1Int1 < param1Int2; ) {
                long l1 = (param1Int1 * 32);
                byte[] arrayOfByte = arrayList2.get(param1Int1);
                if (arrayOfByte != null && arrayOfByte.length == 32) {
                  hwBlob1.putInt8Array(l1, arrayOfByte);
                  param1Int1++;
                } 
                throw new IllegalArgumentException("Array element is not of the expected length");
              } 
              hwBlob2.putBlob(0L, hwBlob1);
              param1HwParcel2.writeBuffer(hwBlob2);
              param1HwParcel2.send();
            case 256136003:
              hwBlob1.enforceInterface("android.hidl.base@1.0::IBase");
              str = interfaceDescriptor();
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.writeString(str);
              param1HwParcel2.send();
            case 256131655:
              str.enforceInterface("android.hidl.base@1.0::IBase");
              nativeHandle = str.readNativeHandle();
              arrayList1 = str.readStringVector();
              debug(nativeHandle, arrayList1);
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.send();
            case 256067662:
              break;
          } 
          arrayList1.enforceInterface("android.hidl.base@1.0::IBase");
          arrayList1 = interfaceChain();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeStringVector(arrayList1);
          param1HwParcel2.send();
        case 8:
          arrayList1.enforceInterface("android.hardware.contexthub@1.0::IContexthub");
          param1Int1 = arrayList1.readInt32();
          param1Int1 = queryApps(param1Int1);
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeInt32(param1Int1);
          param1HwParcel2.send();
        case 7:
          arrayList1.enforceInterface("android.hardware.contexthub@1.0::IContexthub");
          param1Int2 = arrayList1.readInt32();
          l = arrayList1.readInt64();
          param1Int1 = arrayList1.readInt32();
          param1Int1 = disableNanoApp(param1Int2, l, param1Int1);
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeInt32(param1Int1);
          param1HwParcel2.send();
        case 6:
          arrayList1.enforceInterface("android.hardware.contexthub@1.0::IContexthub");
          param1Int1 = arrayList1.readInt32();
          l = arrayList1.readInt64();
          param1Int2 = arrayList1.readInt32();
          param1Int1 = enableNanoApp(param1Int1, l, param1Int2);
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeInt32(param1Int1);
          param1HwParcel2.send();
        case 5:
          arrayList1.enforceInterface("android.hardware.contexthub@1.0::IContexthub");
          param1Int1 = arrayList1.readInt32();
          l = arrayList1.readInt64();
          param1Int2 = arrayList1.readInt32();
          param1Int1 = unloadNanoApp(param1Int1, l, param1Int2);
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeInt32(param1Int1);
          param1HwParcel2.send();
        case 4:
          arrayList1.enforceInterface("android.hardware.contexthub@1.0::IContexthub");
          param1Int1 = arrayList1.readInt32();
          nanoAppBinary = new NanoAppBinary();
          nanoAppBinary.readFromParcel((HwParcel)arrayList1);
          param1Int2 = arrayList1.readInt32();
          param1Int1 = loadNanoApp(param1Int1, nanoAppBinary, param1Int2);
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeInt32(param1Int1);
          param1HwParcel2.send();
        case 3:
          arrayList1.enforceInterface("android.hardware.contexthub@1.0::IContexthub");
          param1Int1 = arrayList1.readInt32();
          contextHubMsg = new ContextHubMsg();
          contextHubMsg.readFromParcel((HwParcel)arrayList1);
          param1Int1 = sendMessageToHub(param1Int1, contextHubMsg);
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeInt32(param1Int1);
          param1HwParcel2.send();
        case 2:
          arrayList1.enforceInterface("android.hardware.contexthub@1.0::IContexthub");
          param1Int1 = arrayList1.readInt32();
          iContexthubCallback = IContexthubCallback.asInterface(arrayList1.readStrongBinder());
          param1Int1 = registerCallback(param1Int1, iContexthubCallback);
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeInt32(param1Int1);
          param1HwParcel2.send();
        case 1:
          break;
      } 
      iContexthubCallback.enforceInterface("android.hardware.contexthub@1.0::IContexthub");
      ArrayList<ContextHub> arrayList = getHubs();
      param1HwParcel2.writeStatus(0);
      ContextHub.writeVectorToParcel(param1HwParcel2, arrayList);
      param1HwParcel2.send();
    }
  }
}
