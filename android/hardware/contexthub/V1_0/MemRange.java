package android.hardware.contexthub.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class MemRange {
  public int totalBytes = 0;
  
  public int freeBytes = 0;
  
  public int type = 0;
  
  public int flags;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != MemRange.class)
      return false; 
    paramObject = paramObject;
    if (this.totalBytes != ((MemRange)paramObject).totalBytes)
      return false; 
    if (this.freeBytes != ((MemRange)paramObject).freeBytes)
      return false; 
    if (this.type != ((MemRange)paramObject).type)
      return false; 
    if (!HidlSupport.deepEquals(Integer.valueOf(this.flags), Integer.valueOf(((MemRange)paramObject).flags)))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.totalBytes;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.freeBytes;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.type;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    int m = this.flags;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".totalBytes = ");
    stringBuilder.append(this.totalBytes);
    stringBuilder.append(", .freeBytes = ");
    stringBuilder.append(this.freeBytes);
    stringBuilder.append(", .type = ");
    stringBuilder.append(HubMemoryType.toString(this.type));
    stringBuilder.append(", .flags = ");
    stringBuilder.append(HubMemoryFlag.dumpBitfield(this.flags));
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<MemRange> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<MemRange> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 16);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      MemRange memRange = new MemRange();
      memRange.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 16));
      arrayList.add(memRange);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.totalBytes = paramHwBlob.getInt32(0L + paramLong);
    this.freeBytes = paramHwBlob.getInt32(4L + paramLong);
    this.type = paramHwBlob.getInt32(8L + paramLong);
    this.flags = paramHwBlob.getInt32(12L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(16);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<MemRange> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 16);
    for (byte b = 0; b < i; b++)
      ((MemRange)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 16)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.totalBytes);
    paramHwBlob.putInt32(4L + paramLong, this.freeBytes);
    paramHwBlob.putInt32(8L + paramLong, this.type);
    paramHwBlob.putInt32(12L + paramLong, this.flags);
  }
}
