package android.hardware.contexthub.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class ContextHub {
  public String name = new String();
  
  public String vendor = new String();
  
  public String toolchain = new String();
  
  public int platformVersion = 0;
  
  public int toolchainVersion = 0;
  
  public int hubId = 0;
  
  public float peakMips = 0.0F;
  
  public float stoppedPowerDrawMw = 0.0F;
  
  public float sleepPowerDrawMw = 0.0F;
  
  public float peakPowerDrawMw = 0.0F;
  
  public ArrayList<PhysicalSensor> connectedSensors = new ArrayList<>();
  
  public int maxSupportedMsgLen = 0;
  
  public long chrePlatformId = 0L;
  
  public byte chreApiMajorVersion = 0;
  
  public byte chreApiMinorVersion = 0;
  
  public short chrePatchVersion = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != ContextHub.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.name, ((ContextHub)paramObject).name))
      return false; 
    if (!HidlSupport.deepEquals(this.vendor, ((ContextHub)paramObject).vendor))
      return false; 
    if (!HidlSupport.deepEquals(this.toolchain, ((ContextHub)paramObject).toolchain))
      return false; 
    if (this.platformVersion != ((ContextHub)paramObject).platformVersion)
      return false; 
    if (this.toolchainVersion != ((ContextHub)paramObject).toolchainVersion)
      return false; 
    if (this.hubId != ((ContextHub)paramObject).hubId)
      return false; 
    if (this.peakMips != ((ContextHub)paramObject).peakMips)
      return false; 
    if (this.stoppedPowerDrawMw != ((ContextHub)paramObject).stoppedPowerDrawMw)
      return false; 
    if (this.sleepPowerDrawMw != ((ContextHub)paramObject).sleepPowerDrawMw)
      return false; 
    if (this.peakPowerDrawMw != ((ContextHub)paramObject).peakPowerDrawMw)
      return false; 
    if (!HidlSupport.deepEquals(this.connectedSensors, ((ContextHub)paramObject).connectedSensors))
      return false; 
    if (this.maxSupportedMsgLen != ((ContextHub)paramObject).maxSupportedMsgLen)
      return false; 
    if (this.chrePlatformId != ((ContextHub)paramObject).chrePlatformId)
      return false; 
    if (this.chreApiMajorVersion != ((ContextHub)paramObject).chreApiMajorVersion)
      return false; 
    if (this.chreApiMinorVersion != ((ContextHub)paramObject).chreApiMinorVersion)
      return false; 
    if (this.chrePatchVersion != ((ContextHub)paramObject).chrePatchVersion)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    String str = this.name;
    int i = HidlSupport.deepHashCode(str);
    str = this.vendor;
    int j = HidlSupport.deepHashCode(str);
    str = this.toolchain;
    int k = HidlSupport.deepHashCode(str), m = this.platformVersion;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    int n = this.toolchainVersion;
    n = HidlSupport.deepHashCode(Integer.valueOf(n));
    int i1 = this.hubId;
    int i2 = HidlSupport.deepHashCode(Integer.valueOf(i1));
    float f = this.peakMips;
    i1 = HidlSupport.deepHashCode(Float.valueOf(f));
    f = this.stoppedPowerDrawMw;
    int i3 = HidlSupport.deepHashCode(Float.valueOf(f));
    f = this.sleepPowerDrawMw;
    int i4 = HidlSupport.deepHashCode(Float.valueOf(f));
    f = this.peakPowerDrawMw;
    int i5 = HidlSupport.deepHashCode(Float.valueOf(f));
    ArrayList<PhysicalSensor> arrayList = this.connectedSensors;
    int i6 = HidlSupport.deepHashCode(arrayList), i7 = this.maxSupportedMsgLen;
    i7 = HidlSupport.deepHashCode(Integer.valueOf(i7));
    long l = this.chrePlatformId;
    int i8 = HidlSupport.deepHashCode(Long.valueOf(l));
    byte b = this.chreApiMajorVersion;
    int i9 = HidlSupport.deepHashCode(Byte.valueOf(b));
    b = this.chreApiMinorVersion;
    int i10 = HidlSupport.deepHashCode(Byte.valueOf(b));
    short s = this.chrePatchVersion;
    int i11 = HidlSupport.deepHashCode(Short.valueOf(s));
    return Objects.hash(new Object[] { 
          Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i2), Integer.valueOf(i1), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5), 
          Integer.valueOf(i6), Integer.valueOf(i7), Integer.valueOf(i8), Integer.valueOf(i9), Integer.valueOf(i10), Integer.valueOf(i11) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".name = ");
    stringBuilder.append(this.name);
    stringBuilder.append(", .vendor = ");
    stringBuilder.append(this.vendor);
    stringBuilder.append(", .toolchain = ");
    stringBuilder.append(this.toolchain);
    stringBuilder.append(", .platformVersion = ");
    stringBuilder.append(this.platformVersion);
    stringBuilder.append(", .toolchainVersion = ");
    stringBuilder.append(this.toolchainVersion);
    stringBuilder.append(", .hubId = ");
    stringBuilder.append(this.hubId);
    stringBuilder.append(", .peakMips = ");
    stringBuilder.append(this.peakMips);
    stringBuilder.append(", .stoppedPowerDrawMw = ");
    stringBuilder.append(this.stoppedPowerDrawMw);
    stringBuilder.append(", .sleepPowerDrawMw = ");
    stringBuilder.append(this.sleepPowerDrawMw);
    stringBuilder.append(", .peakPowerDrawMw = ");
    stringBuilder.append(this.peakPowerDrawMw);
    stringBuilder.append(", .connectedSensors = ");
    stringBuilder.append(this.connectedSensors);
    stringBuilder.append(", .maxSupportedMsgLen = ");
    stringBuilder.append(this.maxSupportedMsgLen);
    stringBuilder.append(", .chrePlatformId = ");
    stringBuilder.append(this.chrePlatformId);
    stringBuilder.append(", .chreApiMajorVersion = ");
    stringBuilder.append(this.chreApiMajorVersion);
    stringBuilder.append(", .chreApiMinorVersion = ");
    stringBuilder.append(this.chreApiMinorVersion);
    stringBuilder.append(", .chrePatchVersion = ");
    stringBuilder.append(this.chrePatchVersion);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(120L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<ContextHub> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<ContextHub> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 120);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      ContextHub contextHub = new ContextHub();
      contextHub.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 120));
      arrayList.add(contextHub);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    String str = paramHwBlob.getString(paramLong + 0L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 0L + 0L, false);
    this.vendor = str = paramHwBlob.getString(paramLong + 16L);
    l2 = ((str.getBytes()).length + 1);
    l1 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 16L + 0L, false);
    this.toolchain = str = paramHwBlob.getString(paramLong + 32L);
    l2 = ((str.getBytes()).length + 1);
    l1 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 32L + 0L, false);
    this.platformVersion = paramHwBlob.getInt32(paramLong + 48L);
    this.toolchainVersion = paramHwBlob.getInt32(paramLong + 52L);
    this.hubId = paramHwBlob.getInt32(paramLong + 56L);
    this.peakMips = paramHwBlob.getFloat(paramLong + 60L);
    this.stoppedPowerDrawMw = paramHwBlob.getFloat(paramLong + 64L);
    this.sleepPowerDrawMw = paramHwBlob.getFloat(paramLong + 68L);
    this.peakPowerDrawMw = paramHwBlob.getFloat(paramLong + 72L);
    int i = paramHwBlob.getInt32(paramLong + 80L + 8L);
    l2 = (i * 96);
    l1 = paramHwBlob.handle();
    HwBlob hwBlob = paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 80L + 0L, true);
    this.connectedSensors.clear();
    for (byte b = 0; b < i; b++) {
      PhysicalSensor physicalSensor = new PhysicalSensor();
      physicalSensor.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 96));
      this.connectedSensors.add(physicalSensor);
    } 
    this.maxSupportedMsgLen = paramHwBlob.getInt32(paramLong + 96L);
    this.chrePlatformId = paramHwBlob.getInt64(paramLong + 104L);
    this.chreApiMajorVersion = paramHwBlob.getInt8(paramLong + 112L);
    this.chreApiMinorVersion = paramHwBlob.getInt8(paramLong + 113L);
    this.chrePatchVersion = paramHwBlob.getInt16(paramLong + 114L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(120);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<ContextHub> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 120);
    for (byte b = 0; b < i; b++)
      ((ContextHub)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 120)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putString(paramLong + 0L, this.name);
    paramHwBlob.putString(16L + paramLong, this.vendor);
    paramHwBlob.putString(32L + paramLong, this.toolchain);
    paramHwBlob.putInt32(48L + paramLong, this.platformVersion);
    paramHwBlob.putInt32(52L + paramLong, this.toolchainVersion);
    paramHwBlob.putInt32(56L + paramLong, this.hubId);
    paramHwBlob.putFloat(60L + paramLong, this.peakMips);
    paramHwBlob.putFloat(64L + paramLong, this.stoppedPowerDrawMw);
    paramHwBlob.putFloat(68L + paramLong, this.sleepPowerDrawMw);
    paramHwBlob.putFloat(72L + paramLong, this.peakPowerDrawMw);
    int i = this.connectedSensors.size();
    paramHwBlob.putInt32(paramLong + 80L + 8L, i);
    paramHwBlob.putBool(paramLong + 80L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 96);
    for (byte b = 0; b < i; b++)
      ((PhysicalSensor)this.connectedSensors.get(b)).writeEmbeddedToBlob(hwBlob, (b * 96)); 
    paramHwBlob.putBlob(80L + paramLong + 0L, hwBlob);
    paramHwBlob.putInt32(96L + paramLong, this.maxSupportedMsgLen);
    paramHwBlob.putInt64(104L + paramLong, this.chrePlatformId);
    paramHwBlob.putInt8(112L + paramLong, this.chreApiMajorVersion);
    paramHwBlob.putInt8(113L + paramLong, this.chreApiMinorVersion);
    paramHwBlob.putInt16(114L + paramLong, this.chrePatchVersion);
  }
}
