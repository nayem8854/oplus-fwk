package android.hardware.contexthub.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class PhysicalSensor {
  public int sensorType = 0;
  
  public String type = new String();
  
  public String name = new String();
  
  public String vendor = new String();
  
  public int version = 0;
  
  public int fifoReservedCount = 0;
  
  public int fifoMaxCount = 0;
  
  public long minDelayMs = 0L;
  
  public long maxDelayMs = 0L;
  
  public float peakPowerMw = 0.0F;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != PhysicalSensor.class)
      return false; 
    paramObject = paramObject;
    if (this.sensorType != ((PhysicalSensor)paramObject).sensorType)
      return false; 
    if (!HidlSupport.deepEquals(this.type, ((PhysicalSensor)paramObject).type))
      return false; 
    if (!HidlSupport.deepEquals(this.name, ((PhysicalSensor)paramObject).name))
      return false; 
    if (!HidlSupport.deepEquals(this.vendor, ((PhysicalSensor)paramObject).vendor))
      return false; 
    if (this.version != ((PhysicalSensor)paramObject).version)
      return false; 
    if (this.fifoReservedCount != ((PhysicalSensor)paramObject).fifoReservedCount)
      return false; 
    if (this.fifoMaxCount != ((PhysicalSensor)paramObject).fifoMaxCount)
      return false; 
    if (this.minDelayMs != ((PhysicalSensor)paramObject).minDelayMs)
      return false; 
    if (this.maxDelayMs != ((PhysicalSensor)paramObject).maxDelayMs)
      return false; 
    if (this.peakPowerMw != ((PhysicalSensor)paramObject).peakPowerMw)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.sensorType;
    int j = HidlSupport.deepHashCode(Integer.valueOf(i));
    String str = this.type;
    i = HidlSupport.deepHashCode(str);
    str = this.name;
    int k = HidlSupport.deepHashCode(str);
    str = this.vendor;
    int m = HidlSupport.deepHashCode(str), n = this.version;
    n = HidlSupport.deepHashCode(Integer.valueOf(n));
    int i1 = this.fifoReservedCount;
    i1 = HidlSupport.deepHashCode(Integer.valueOf(i1));
    int i2 = this.fifoMaxCount;
    i2 = HidlSupport.deepHashCode(Integer.valueOf(i2));
    long l = this.minDelayMs;
    int i3 = HidlSupport.deepHashCode(Long.valueOf(l));
    l = this.maxDelayMs;
    int i4 = HidlSupport.deepHashCode(Long.valueOf(l));
    float f = this.peakPowerMw;
    int i5 = HidlSupport.deepHashCode(Float.valueOf(f));
    return Objects.hash(new Object[] { Integer.valueOf(j), Integer.valueOf(i), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".sensorType = ");
    stringBuilder.append(SensorType.toString(this.sensorType));
    stringBuilder.append(", .type = ");
    stringBuilder.append(this.type);
    stringBuilder.append(", .name = ");
    stringBuilder.append(this.name);
    stringBuilder.append(", .vendor = ");
    stringBuilder.append(this.vendor);
    stringBuilder.append(", .version = ");
    stringBuilder.append(this.version);
    stringBuilder.append(", .fifoReservedCount = ");
    stringBuilder.append(this.fifoReservedCount);
    stringBuilder.append(", .fifoMaxCount = ");
    stringBuilder.append(this.fifoMaxCount);
    stringBuilder.append(", .minDelayMs = ");
    stringBuilder.append(this.minDelayMs);
    stringBuilder.append(", .maxDelayMs = ");
    stringBuilder.append(this.maxDelayMs);
    stringBuilder.append(", .peakPowerMw = ");
    stringBuilder.append(this.peakPowerMw);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(96L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<PhysicalSensor> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<PhysicalSensor> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 96);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      PhysicalSensor physicalSensor = new PhysicalSensor();
      physicalSensor.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 96));
      arrayList.add(physicalSensor);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.sensorType = paramHwBlob.getInt32(paramLong + 0L);
    String str = paramHwBlob.getString(paramLong + 8L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 8L + 0L, false);
    this.name = str = paramHwBlob.getString(paramLong + 24L);
    l1 = ((str.getBytes()).length + 1);
    l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 24L + 0L, false);
    this.vendor = str = paramHwBlob.getString(paramLong + 40L);
    l1 = ((str.getBytes()).length + 1);
    l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 40L + 0L, false);
    this.version = paramHwBlob.getInt32(paramLong + 56L);
    this.fifoReservedCount = paramHwBlob.getInt32(paramLong + 60L);
    this.fifoMaxCount = paramHwBlob.getInt32(paramLong + 64L);
    this.minDelayMs = paramHwBlob.getInt64(paramLong + 72L);
    this.maxDelayMs = paramHwBlob.getInt64(paramLong + 80L);
    this.peakPowerMw = paramHwBlob.getFloat(paramLong + 88L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(96);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<PhysicalSensor> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 96);
    for (byte b = 0; b < i; b++)
      ((PhysicalSensor)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 96)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.sensorType);
    paramHwBlob.putString(8L + paramLong, this.type);
    paramHwBlob.putString(24L + paramLong, this.name);
    paramHwBlob.putString(40L + paramLong, this.vendor);
    paramHwBlob.putInt32(56L + paramLong, this.version);
    paramHwBlob.putInt32(60L + paramLong, this.fifoReservedCount);
    paramHwBlob.putInt32(64L + paramLong, this.fifoMaxCount);
    paramHwBlob.putInt64(72L + paramLong, this.minDelayMs);
    paramHwBlob.putInt64(80L + paramLong, this.maxDelayMs);
    paramHwBlob.putFloat(88L + paramLong, this.peakPowerMw);
  }
}
