package android.internal.hidl.base.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class DebugInfo {
  public static final class Architecture {
    public static final int IS_32BIT = 2;
    
    public static final int IS_64BIT = 1;
    
    public static final int UNKNOWN = 0;
    
    public static final String toString(int param1Int) {
      if (param1Int == 0)
        return "UNKNOWN"; 
      if (param1Int == 1)
        return "IS_64BIT"; 
      if (param1Int == 2)
        return "IS_32BIT"; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString(param1Int));
      return stringBuilder.toString();
    }
    
    public static final String dumpBitfield(int param1Int) {
      ArrayList<String> arrayList = new ArrayList();
      int i = 0;
      arrayList.add("UNKNOWN");
      if ((param1Int & 0x1) == 1) {
        arrayList.add("IS_64BIT");
        i = false | true;
      } 
      int j = i;
      if ((param1Int & 0x2) == 2) {
        arrayList.add("IS_32BIT");
        j = i | 0x2;
      } 
      if (param1Int != j) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("0x");
        stringBuilder.append(Integer.toHexString((j ^ 0xFFFFFFFF) & param1Int));
        arrayList.add(stringBuilder.toString());
      } 
      return String.join(" | ", (Iterable)arrayList);
    }
  }
  
  public int pid = 0;
  
  public long ptr = 0L;
  
  public int arch = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != DebugInfo.class)
      return false; 
    paramObject = paramObject;
    if (this.pid != ((DebugInfo)paramObject).pid)
      return false; 
    if (this.ptr != ((DebugInfo)paramObject).ptr)
      return false; 
    if (this.arch != ((DebugInfo)paramObject).arch)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.pid;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    long l = this.ptr;
    int j = HidlSupport.deepHashCode(Long.valueOf(l)), k = this.arch;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".pid = ");
    stringBuilder.append(this.pid);
    stringBuilder.append(", .ptr = ");
    stringBuilder.append(this.ptr);
    stringBuilder.append(", .arch = ");
    stringBuilder.append(Architecture.toString(this.arch));
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(24L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<DebugInfo> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<DebugInfo> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 24);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      DebugInfo debugInfo = new DebugInfo();
      debugInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 24));
      arrayList.add(debugInfo);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.pid = paramHwBlob.getInt32(0L + paramLong);
    this.ptr = paramHwBlob.getInt64(8L + paramLong);
    this.arch = paramHwBlob.getInt32(16L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(24);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<DebugInfo> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 24);
    for (byte b = 0; b < i; b++)
      ((DebugInfo)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 24)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.pid);
    paramHwBlob.putInt64(8L + paramLong, this.ptr);
    paramHwBlob.putInt32(16L + paramLong, this.arch);
  }
}
