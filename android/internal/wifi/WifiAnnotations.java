package android.internal.wifi;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class WifiAnnotations {
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Bandwidth {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ChannelWidth {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Cipher {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface KeyMgmt {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Protocol {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ScanType {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface WifiBandBasic {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface WifiStandard {}
}
