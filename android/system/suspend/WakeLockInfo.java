package android.system.suspend;

import android.os.Parcel;
import android.os.Parcelable;

public class WakeLockInfo implements Parcelable {
  public static final Parcelable.Creator<WakeLockInfo> CREATOR = new Parcelable.Creator<WakeLockInfo>() {
      public WakeLockInfo createFromParcel(Parcel param1Parcel) {
        WakeLockInfo wakeLockInfo = new WakeLockInfo();
        wakeLockInfo.readFromParcel(param1Parcel);
        return wakeLockInfo;
      }
      
      public WakeLockInfo[] newArray(int param1Int) {
        return new WakeLockInfo[param1Int];
      }
    };
  
  public long activeCount;
  
  public long activeTime;
  
  public long eventCount;
  
  public long expireCount;
  
  public boolean isActive;
  
  public boolean isKernelWakelock;
  
  public long lastChange;
  
  public long maxTime;
  
  public String name;
  
  public int pid;
  
  public long preventSuspendTime;
  
  public long totalTime;
  
  public long wakeupCount;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    paramInt = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeString(this.name);
    paramParcel.writeLong(this.activeCount);
    paramParcel.writeLong(this.lastChange);
    paramParcel.writeLong(this.maxTime);
    paramParcel.writeLong(this.totalTime);
    paramParcel.writeInt(this.isActive);
    paramParcel.writeLong(this.activeTime);
    paramParcel.writeInt(this.isKernelWakelock);
    paramParcel.writeInt(this.pid);
    paramParcel.writeLong(this.eventCount);
    paramParcel.writeLong(this.expireCount);
    paramParcel.writeLong(this.preventSuspendTime);
    paramParcel.writeLong(this.wakeupCount);
    int i = paramParcel.dataPosition();
    paramParcel.setDataPosition(paramInt);
    paramParcel.writeInt(i - paramInt);
    paramParcel.setDataPosition(i);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      boolean bool2;
      this.name = paramParcel.readString();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.activeCount = paramParcel.readLong();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.lastChange = paramParcel.readLong();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.maxTime = paramParcel.readLong();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.totalTime = paramParcel.readLong();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      k = paramParcel.readInt();
      boolean bool1 = true;
      if (k != 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.isActive = bool2;
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.activeTime = paramParcel.readLong();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      if (paramParcel.readInt() != 0) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      this.isKernelWakelock = bool2;
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.pid = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.eventCount = paramParcel.readLong();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.expireCount = paramParcel.readLong();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.preventSuspendTime = paramParcel.readLong();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.wakeupCount = paramParcel.readLong();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
