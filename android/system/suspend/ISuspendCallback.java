package android.system.suspend;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISuspendCallback extends IInterface {
  void notifyWakeup(boolean paramBoolean) throws RemoteException;
  
  class Default implements ISuspendCallback {
    public void notifyWakeup(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISuspendCallback {
    private static final String DESCRIPTOR = "android$system$suspend$ISuspendCallback".replace('$', '.');
    
    static final int TRANSACTION_notifyWakeup = 1;
    
    public Stub() {
      attachInterface(this, DESCRIPTOR);
    }
    
    public static ISuspendCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface(DESCRIPTOR);
      if (iInterface != null && iInterface instanceof ISuspendCallback)
        return (ISuspendCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      String str = DESCRIPTOR;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString(str);
        return true;
      } 
      param1Parcel1.enforceInterface(str);
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      notifyWakeup(bool);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements ISuspendCallback {
      public static ISuspendCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return ISuspendCallback.Stub.DESCRIPTOR;
      }
      
      public void notifyWakeup(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken(ISuspendCallback.Stub.DESCRIPTOR);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool1 && ISuspendCallback.Stub.getDefaultImpl() != null) {
            ISuspendCallback.Stub.getDefaultImpl().notifyWakeup(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISuspendCallback param1ISuspendCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISuspendCallback != null) {
          Proxy.sDefaultImpl = param1ISuspendCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISuspendCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
