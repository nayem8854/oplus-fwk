package android.system.suspend;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public interface ISuspendControlService extends IInterface {
  boolean enableAutosuspend() throws RemoteException;
  
  boolean forceSuspend() throws RemoteException;
  
  WakeLockInfo[] getWakeLockStats() throws RemoteException;
  
  boolean registerCallback(ISuspendCallback paramISuspendCallback) throws RemoteException;
  
  class Default implements ISuspendControlService {
    public boolean enableAutosuspend() throws RemoteException {
      return false;
    }
    
    public boolean registerCallback(ISuspendCallback param1ISuspendCallback) throws RemoteException {
      return false;
    }
    
    public boolean forceSuspend() throws RemoteException {
      return false;
    }
    
    public WakeLockInfo[] getWakeLockStats() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISuspendControlService {
    private static final String DESCRIPTOR = "android$system$suspend$ISuspendControlService".replace('$', '.');
    
    static final int TRANSACTION_enableAutosuspend = 1;
    
    static final int TRANSACTION_forceSuspend = 3;
    
    static final int TRANSACTION_getWakeLockStats = 4;
    
    static final int TRANSACTION_registerCallback = 2;
    
    public Stub() {
      attachInterface(this, DESCRIPTOR);
    }
    
    public static ISuspendControlService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface(DESCRIPTOR);
      if (iInterface != null && iInterface instanceof ISuspendControlService)
        return (ISuspendControlService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ISuspendCallback iSuspendCallback;
      String str = DESCRIPTOR;
      if (param1Int1 != 1) {
        WakeLockInfo[] arrayOfWakeLockInfo;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString(str);
              return true;
            } 
            param1Parcel1.enforceInterface(str);
            arrayOfWakeLockInfo = getWakeLockStats();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray((Parcelable[])arrayOfWakeLockInfo, 1);
            return true;
          } 
          arrayOfWakeLockInfo.enforceInterface(str);
          boolean bool2 = forceSuspend();
          param1Parcel2.writeNoException();
          param1Parcel2.writeInt(bool2);
          return true;
        } 
        arrayOfWakeLockInfo.enforceInterface(str);
        iSuspendCallback = ISuspendCallback.Stub.asInterface(arrayOfWakeLockInfo.readStrongBinder());
        boolean bool1 = registerCallback(iSuspendCallback);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      iSuspendCallback.enforceInterface(str);
      boolean bool = enableAutosuspend();
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements ISuspendControlService {
      public static ISuspendControlService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return ISuspendControlService.Stub.DESCRIPTOR;
      }
      
      public boolean enableAutosuspend() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken(ISuspendControlService.Stub.DESCRIPTOR);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && ISuspendControlService.Stub.getDefaultImpl() != null) {
            bool1 = ISuspendControlService.Stub.getDefaultImpl().enableAutosuspend();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean registerCallback(ISuspendCallback param2ISuspendCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken(ISuspendControlService.Stub.DESCRIPTOR);
          if (param2ISuspendCallback != null) {
            iBinder = param2ISuspendCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && ISuspendControlService.Stub.getDefaultImpl() != null) {
            bool1 = ISuspendControlService.Stub.getDefaultImpl().registerCallback(param2ISuspendCallback);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean forceSuspend() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken(ISuspendControlService.Stub.DESCRIPTOR);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && ISuspendControlService.Stub.getDefaultImpl() != null) {
            bool1 = ISuspendControlService.Stub.getDefaultImpl().forceSuspend();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public WakeLockInfo[] getWakeLockStats() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken(ISuspendControlService.Stub.DESCRIPTOR);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ISuspendControlService.Stub.getDefaultImpl() != null)
            return ISuspendControlService.Stub.getDefaultImpl().getWakeLockStats(); 
          parcel2.readException();
          return (WakeLockInfo[])parcel2.createTypedArray(WakeLockInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISuspendControlService param1ISuspendControlService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISuspendControlService != null) {
          Proxy.sDefaultImpl = param1ISuspendControlService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISuspendControlService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
