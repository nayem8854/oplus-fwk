package android.permissionpresenterservice;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.permission.IRuntimePermissionPresenter;
import android.content.pm.permission.RuntimePermissionPresentationInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteCallback;
import com.android.internal.util.Preconditions;
import com.android.internal.util.function.TriConsumer;
import com.android.internal.util.function.pooled.PooledLambda;
import java.util.List;

@SystemApi
@Deprecated
public abstract class RuntimePermissionPresenterService extends Service {
  private static final String KEY_RESULT = "android.content.pm.permission.RuntimePermissionPresenter.key.result";
  
  public static final String SERVICE_INTERFACE = "android.permissionpresenterservice.RuntimePermissionPresenterService";
  
  private Handler mHandler;
  
  public final void attachBaseContext(Context paramContext) {
    super.attachBaseContext(paramContext);
    this.mHandler = new Handler(paramContext.getMainLooper());
  }
  
  public final IBinder onBind(Intent paramIntent) {
    return (IBinder)new IRuntimePermissionPresenter.Stub() {
        final RuntimePermissionPresenterService this$0;
        
        public void getAppPermissions(String param1String, RemoteCallback param1RemoteCallback) {
          Preconditions.checkNotNull(param1String, "packageName");
          Preconditions.checkNotNull(param1RemoteCallback, "callback");
          Handler handler = RuntimePermissionPresenterService.this.mHandler;
          -$.Lambda.RuntimePermissionPresenterService.null.hIxcH5_fyEVhEY0Z-wjDuhvJriA hIxcH5_fyEVhEY0Z-wjDuhvJriA = _$$Lambda$RuntimePermissionPresenterService$1$hIxcH5_fyEVhEY0Z_wjDuhvJriA.INSTANCE;
          RuntimePermissionPresenterService runtimePermissionPresenterService = RuntimePermissionPresenterService.this;
          Message message = PooledLambda.obtainMessage((TriConsumer)hIxcH5_fyEVhEY0Z-wjDuhvJriA, runtimePermissionPresenterService, param1String, param1RemoteCallback);
          handler.sendMessage(message);
        }
      };
  }
  
  private void getAppPermissions(String paramString, RemoteCallback paramRemoteCallback) {
    List<RuntimePermissionPresentationInfo> list = onGetAppPermissions(paramString);
    if (list != null && !list.isEmpty()) {
      Bundle bundle = new Bundle();
      bundle.putParcelableList("android.content.pm.permission.RuntimePermissionPresenter.key.result", (List)list);
      paramRemoteCallback.sendResult(bundle);
    } else {
      paramRemoteCallback.sendResult(null);
    } 
  }
  
  public abstract List<RuntimePermissionPresentationInfo> onGetAppPermissions(String paramString);
}
