package android.appwidget;

import android.util.ArraySet;

public abstract class AppWidgetManagerInternal {
  public abstract ArraySet<String> getHostedWidgetPackages(int paramInt);
  
  public abstract void unlockUser(int paramInt);
}
