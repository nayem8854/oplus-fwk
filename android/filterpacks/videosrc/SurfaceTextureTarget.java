package android.filterpacks.videosrc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.GLFrame;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.GenerateFinalPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.core.ShaderProgram;
import android.filterfw.geometry.Point;
import android.filterfw.geometry.Quad;
import android.graphics.SurfaceTexture;
import android.util.Log;

public class SurfaceTextureTarget extends Filter {
  private final int RENDERMODE_STRETCH = 0;
  
  private final int RENDERMODE_FIT = 1;
  
  private final int RENDERMODE_FILL_CROP = 2;
  
  private final int RENDERMODE_CUSTOMIZE = 3;
  
  @GenerateFieldPort(hasDefault = true, name = "sourceQuad")
  private Quad mSourceQuad = new Quad(new Point(0.0F, 1.0F), new Point(1.0F, 1.0F), new Point(0.0F, 0.0F), new Point(1.0F, 0.0F));
  
  @GenerateFieldPort(hasDefault = true, name = "targetQuad")
  private Quad mTargetQuad = new Quad(new Point(0.0F, 0.0F), new Point(1.0F, 0.0F), new Point(0.0F, 1.0F), new Point(1.0F, 1.0F));
  
  private int mRenderMode = 1;
  
  private float mAspectRatio = 1.0F;
  
  private static final String TAG = "SurfaceTextureTarget";
  
  private boolean mLogVerbose;
  
  private ShaderProgram mProgram;
  
  @GenerateFieldPort(hasDefault = true, name = "renderMode")
  private String mRenderModeString;
  
  private GLFrame mScreen;
  
  @GenerateFinalPort(name = "height")
  private int mScreenHeight;
  
  @GenerateFinalPort(name = "width")
  private int mScreenWidth;
  
  private int mSurfaceId;
  
  @GenerateFinalPort(name = "surfaceTexture")
  private SurfaceTexture mSurfaceTexture;
  
  public SurfaceTextureTarget(String paramString) {
    super(paramString);
    this.mLogVerbose = Log.isLoggable("SurfaceTextureTarget", 2);
  }
  
  public void setupPorts() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mSurfaceTexture : Landroid/graphics/SurfaceTexture;
    //   6: ifnull -> 23
    //   9: aload_0
    //   10: ldc_w 'frame'
    //   13: iconst_3
    //   14: invokestatic create : (I)Landroid/filterfw/core/MutableFrameFormat;
    //   17: invokevirtual addMaskedInputPort : (Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V
    //   20: aload_0
    //   21: monitorexit
    //   22: return
    //   23: new java/lang/RuntimeException
    //   26: astore_1
    //   27: aload_1
    //   28: ldc_w 'Null SurfaceTexture passed to SurfaceTextureTarget'
    //   31: invokespecial <init> : (Ljava/lang/String;)V
    //   34: aload_1
    //   35: athrow
    //   36: astore_1
    //   37: aload_0
    //   38: monitorexit
    //   39: aload_1
    //   40: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #105	-> 2
    //   #110	-> 9
    //   #111	-> 20
    //   #106	-> 23
    //   #104	-> 36
    // Exception table:
    //   from	to	target	type
    //   2	9	36	finally
    //   9	20	36	finally
    //   23	36	36	finally
  }
  
  public void updateRenderMode() {
    if (this.mLogVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("updateRenderMode. Thread: ");
      stringBuilder.append(Thread.currentThread());
      Log.v("SurfaceTextureTarget", stringBuilder.toString());
    } 
    String str = this.mRenderModeString;
    if (str != null)
      if (str.equals("stretch")) {
        this.mRenderMode = 0;
      } else if (this.mRenderModeString.equals("fit")) {
        this.mRenderMode = 1;
      } else if (this.mRenderModeString.equals("fill_crop")) {
        this.mRenderMode = 2;
      } else if (this.mRenderModeString.equals("customize")) {
        this.mRenderMode = 3;
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown render mode '");
        stringBuilder.append(this.mRenderModeString);
        stringBuilder.append("'!");
        throw new RuntimeException(stringBuilder.toString());
      }  
    updateTargetRect();
  }
  
  public void prepare(FilterContext paramFilterContext) {
    if (this.mLogVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Prepare. Thread: ");
      stringBuilder.append(Thread.currentThread());
      Log.v("SurfaceTextureTarget", stringBuilder.toString());
    } 
    ShaderProgram shaderProgram = ShaderProgram.createIdentity(paramFilterContext);
    shaderProgram.setSourceRect(0.0F, 1.0F, 1.0F, -1.0F);
    this.mProgram.setClearColor(0.0F, 0.0F, 0.0F);
    updateRenderMode();
    MutableFrameFormat mutableFrameFormat = new MutableFrameFormat(2, 3);
    mutableFrameFormat.setBytesPerSample(4);
    mutableFrameFormat.setDimensions(this.mScreenWidth, this.mScreenHeight);
    this.mScreen = (GLFrame)paramFilterContext.getFrameManager().newBoundFrame(mutableFrameFormat, 101, 0L);
  }
  
  public void open(FilterContext paramFilterContext) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mSurfaceTexture : Landroid/graphics/SurfaceTexture;
    //   6: ifnull -> 79
    //   9: aload_1
    //   10: invokevirtual getGLEnvironment : ()Landroid/filterfw/core/GLEnvironment;
    //   13: aload_0
    //   14: getfield mSurfaceTexture : Landroid/graphics/SurfaceTexture;
    //   17: aload_0
    //   18: getfield mScreenWidth : I
    //   21: aload_0
    //   22: getfield mScreenHeight : I
    //   25: invokevirtual registerSurfaceTexture : (Landroid/graphics/SurfaceTexture;II)I
    //   28: istore_2
    //   29: aload_0
    //   30: iload_2
    //   31: putfield mSurfaceId : I
    //   34: iload_2
    //   35: ifle -> 41
    //   38: aload_0
    //   39: monitorexit
    //   40: return
    //   41: new java/lang/RuntimeException
    //   44: astore_1
    //   45: new java/lang/StringBuilder
    //   48: astore_3
    //   49: aload_3
    //   50: invokespecial <init> : ()V
    //   53: aload_3
    //   54: ldc 'Could not register SurfaceTexture: '
    //   56: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   59: pop
    //   60: aload_3
    //   61: aload_0
    //   62: getfield mSurfaceTexture : Landroid/graphics/SurfaceTexture;
    //   65: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   68: pop
    //   69: aload_1
    //   70: aload_3
    //   71: invokevirtual toString : ()Ljava/lang/String;
    //   74: invokespecial <init> : (Ljava/lang/String;)V
    //   77: aload_1
    //   78: athrow
    //   79: ldc 'SurfaceTextureTarget'
    //   81: ldc 'SurfaceTexture is null!!'
    //   83: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   86: pop
    //   87: new java/lang/RuntimeException
    //   90: astore_1
    //   91: new java/lang/StringBuilder
    //   94: astore_3
    //   95: aload_3
    //   96: invokespecial <init> : ()V
    //   99: aload_3
    //   100: ldc 'Could not register SurfaceTexture: '
    //   102: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   105: pop
    //   106: aload_3
    //   107: aload_0
    //   108: getfield mSurfaceTexture : Landroid/graphics/SurfaceTexture;
    //   111: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   114: pop
    //   115: aload_1
    //   116: aload_3
    //   117: invokevirtual toString : ()Ljava/lang/String;
    //   120: invokespecial <init> : (Ljava/lang/String;)V
    //   123: aload_1
    //   124: athrow
    //   125: astore_1
    //   126: aload_0
    //   127: monitorexit
    //   128: aload_1
    //   129: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #155	-> 2
    //   #159	-> 9
    //   #161	-> 34
    //   #164	-> 38
    //   #162	-> 41
    //   #156	-> 79
    //   #157	-> 87
    //   #154	-> 125
    // Exception table:
    //   from	to	target	type
    //   2	9	125	finally
    //   9	34	125	finally
    //   41	79	125	finally
    //   79	87	125	finally
    //   87	125	125	finally
  }
  
  public void close(FilterContext paramFilterContext) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mSurfaceId : I
    //   6: ifle -> 25
    //   9: aload_1
    //   10: invokevirtual getGLEnvironment : ()Landroid/filterfw/core/GLEnvironment;
    //   13: aload_0
    //   14: getfield mSurfaceId : I
    //   17: invokevirtual unregisterSurfaceId : (I)V
    //   20: aload_0
    //   21: iconst_m1
    //   22: putfield mSurfaceId : I
    //   25: aload_0
    //   26: monitorexit
    //   27: return
    //   28: astore_1
    //   29: aload_0
    //   30: monitorexit
    //   31: aload_1
    //   32: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #176	-> 2
    //   #177	-> 9
    //   #178	-> 20
    //   #180	-> 25
    //   #175	-> 28
    // Exception table:
    //   from	to	target	type
    //   2	9	28	finally
    //   9	20	28	finally
    //   20	25	28	finally
  }
  
  public void disconnect(FilterContext paramFilterContext) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mLogVerbose : Z
    //   6: ifeq -> 17
    //   9: ldc 'SurfaceTextureTarget'
    //   11: ldc 'disconnect'
    //   13: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   16: pop
    //   17: aload_0
    //   18: getfield mSurfaceTexture : Landroid/graphics/SurfaceTexture;
    //   21: ifnonnull -> 35
    //   24: ldc 'SurfaceTextureTarget'
    //   26: ldc 'SurfaceTexture is already null. Nothing to disconnect.'
    //   28: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   31: pop
    //   32: aload_0
    //   33: monitorexit
    //   34: return
    //   35: aload_0
    //   36: aconst_null
    //   37: putfield mSurfaceTexture : Landroid/graphics/SurfaceTexture;
    //   40: aload_0
    //   41: getfield mSurfaceId : I
    //   44: ifle -> 63
    //   47: aload_1
    //   48: invokevirtual getGLEnvironment : ()Landroid/filterfw/core/GLEnvironment;
    //   51: aload_0
    //   52: getfield mSurfaceId : I
    //   55: invokevirtual unregisterSurfaceId : (I)V
    //   58: aload_0
    //   59: iconst_m1
    //   60: putfield mSurfaceId : I
    //   63: aload_0
    //   64: monitorexit
    //   65: return
    //   66: astore_1
    //   67: aload_0
    //   68: monitorexit
    //   69: aload_1
    //   70: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #186	-> 2
    //   #187	-> 17
    //   #188	-> 24
    //   #189	-> 32
    //   #191	-> 35
    //   #197	-> 40
    //   #198	-> 47
    //   #199	-> 58
    //   #201	-> 63
    //   #185	-> 66
    // Exception table:
    //   from	to	target	type
    //   2	17	66	finally
    //   17	24	66	finally
    //   24	32	66	finally
    //   35	40	66	finally
    //   40	47	66	finally
    //   47	58	66	finally
    //   58	63	66	finally
  }
  
  public void process(FilterContext paramFilterContext) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mSurfaceId : I
    //   6: istore_2
    //   7: iload_2
    //   8: ifgt -> 14
    //   11: aload_0
    //   12: monitorexit
    //   13: return
    //   14: aload_1
    //   15: invokevirtual getGLEnvironment : ()Landroid/filterfw/core/GLEnvironment;
    //   18: astore_3
    //   19: aload_0
    //   20: ldc_w 'frame'
    //   23: invokevirtual pullInput : (Ljava/lang/String;)Landroid/filterfw/core/Frame;
    //   26: astore #4
    //   28: iconst_0
    //   29: istore_2
    //   30: aload #4
    //   32: invokevirtual getFormat : ()Landroid/filterfw/core/FrameFormat;
    //   35: invokevirtual getWidth : ()I
    //   38: i2f
    //   39: aload #4
    //   41: invokevirtual getFormat : ()Landroid/filterfw/core/FrameFormat;
    //   44: invokevirtual getHeight : ()I
    //   47: i2f
    //   48: fdiv
    //   49: fstore #5
    //   51: fload #5
    //   53: aload_0
    //   54: getfield mAspectRatio : F
    //   57: fcmpl
    //   58: ifeq -> 157
    //   61: aload_0
    //   62: getfield mLogVerbose : Z
    //   65: ifeq -> 147
    //   68: new java/lang/StringBuilder
    //   71: astore #6
    //   73: aload #6
    //   75: invokespecial <init> : ()V
    //   78: aload #6
    //   80: ldc_w 'Process. New aspect ratio: '
    //   83: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   86: pop
    //   87: aload #6
    //   89: fload #5
    //   91: invokevirtual append : (F)Ljava/lang/StringBuilder;
    //   94: pop
    //   95: aload #6
    //   97: ldc_w ', previously: '
    //   100: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   103: pop
    //   104: aload #6
    //   106: aload_0
    //   107: getfield mAspectRatio : F
    //   110: invokevirtual append : (F)Ljava/lang/StringBuilder;
    //   113: pop
    //   114: aload #6
    //   116: ldc_w '. Thread: '
    //   119: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   122: pop
    //   123: aload #6
    //   125: invokestatic currentThread : ()Ljava/lang/Thread;
    //   128: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   131: pop
    //   132: aload #6
    //   134: invokevirtual toString : ()Ljava/lang/String;
    //   137: astore #6
    //   139: ldc 'SurfaceTextureTarget'
    //   141: aload #6
    //   143: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   146: pop
    //   147: aload_0
    //   148: fload #5
    //   150: putfield mAspectRatio : F
    //   153: aload_0
    //   154: invokespecial updateTargetRect : ()V
    //   157: aload #4
    //   159: invokevirtual getFormat : ()Landroid/filterfw/core/FrameFormat;
    //   162: invokevirtual getTarget : ()I
    //   165: istore #7
    //   167: iload #7
    //   169: iconst_3
    //   170: if_icmpeq -> 189
    //   173: aload_1
    //   174: invokevirtual getFrameManager : ()Landroid/filterfw/core/FrameManager;
    //   177: aload #4
    //   179: iconst_3
    //   180: invokevirtual duplicateFrameToTarget : (Landroid/filterfw/core/Frame;I)Landroid/filterfw/core/Frame;
    //   183: astore_1
    //   184: iconst_1
    //   185: istore_2
    //   186: goto -> 192
    //   189: aload #4
    //   191: astore_1
    //   192: aload_3
    //   193: aload_0
    //   194: getfield mSurfaceId : I
    //   197: invokevirtual activateSurfaceWithId : (I)V
    //   200: aload_0
    //   201: getfield mProgram : Landroid/filterfw/core/ShaderProgram;
    //   204: aload_1
    //   205: aload_0
    //   206: getfield mScreen : Landroid/filterfw/core/GLFrame;
    //   209: invokevirtual process : (Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V
    //   212: aload_3
    //   213: aload #4
    //   215: invokevirtual getTimestamp : ()J
    //   218: invokevirtual setSurfaceTimestamp : (J)V
    //   221: aload_3
    //   222: invokevirtual swapBuffers : ()V
    //   225: iload_2
    //   226: ifeq -> 234
    //   229: aload_1
    //   230: invokevirtual release : ()Landroid/filterfw/core/Frame;
    //   233: pop
    //   234: aload_0
    //   235: monitorexit
    //   236: return
    //   237: astore_1
    //   238: aload_0
    //   239: monitorexit
    //   240: aload_1
    //   241: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #206	-> 2
    //   #207	-> 11
    //   #209	-> 14
    //   #212	-> 19
    //   #213	-> 28
    //   #215	-> 30
    //   #216	-> 30
    //   #217	-> 51
    //   #218	-> 61
    //   #219	-> 68
    //   #220	-> 123
    //   #219	-> 139
    //   #222	-> 147
    //   #223	-> 153
    //   #227	-> 157
    //   #228	-> 157
    //   #229	-> 167
    //   #230	-> 173
    //   #232	-> 184
    //   #234	-> 189
    //   #238	-> 192
    //   #241	-> 200
    //   #243	-> 212
    //   #246	-> 221
    //   #248	-> 225
    //   #249	-> 229
    //   #251	-> 234
    //   #205	-> 237
    // Exception table:
    //   from	to	target	type
    //   2	7	237	finally
    //   14	19	237	finally
    //   19	28	237	finally
    //   30	51	237	finally
    //   51	61	237	finally
    //   61	68	237	finally
    //   68	123	237	finally
    //   123	139	237	finally
    //   139	147	237	finally
    //   147	153	237	finally
    //   153	157	237	finally
    //   157	167	237	finally
    //   173	184	237	finally
    //   192	200	237	finally
    //   200	212	237	finally
    //   212	221	237	finally
    //   221	225	237	finally
    //   229	234	237	finally
  }
  
  public void fieldPortValueUpdated(String paramString, FilterContext paramFilterContext) {
    if (this.mLogVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("FPVU. Thread: ");
      stringBuilder.append(Thread.currentThread());
      Log.v("SurfaceTextureTarget", stringBuilder.toString());
    } 
    updateRenderMode();
  }
  
  public void tearDown(FilterContext paramFilterContext) {
    GLFrame gLFrame = this.mScreen;
    if (gLFrame != null)
      gLFrame.release(); 
  }
  
  private void updateTargetRect() {
    if (this.mLogVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("updateTargetRect. Thread: ");
      stringBuilder.append(Thread.currentThread());
      Log.v("SurfaceTextureTarget", stringBuilder.toString());
    } 
    int i = this.mScreenWidth;
    if (i > 0) {
      int j = this.mScreenHeight;
      if (j > 0 && this.mProgram != null) {
        float f1 = i / j;
        float f2 = f1 / this.mAspectRatio;
        if (this.mLogVerbose) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("UTR. screen w = ");
          stringBuilder.append(this.mScreenWidth);
          stringBuilder.append(" x screen h = ");
          stringBuilder.append(this.mScreenHeight);
          stringBuilder.append(" Screen AR: ");
          stringBuilder.append(f1);
          stringBuilder.append(", frame AR: ");
          stringBuilder.append(this.mAspectRatio);
          stringBuilder.append(", relative AR: ");
          stringBuilder.append(f2);
          Log.v("SurfaceTextureTarget", stringBuilder.toString());
        } 
        if (f2 == 1.0F && this.mRenderMode != 3) {
          this.mProgram.setTargetRect(0.0F, 0.0F, 1.0F, 1.0F);
          this.mProgram.setClearsOutput(false);
        } else {
          j = this.mRenderMode;
          if (j != 0) {
            if (j != 1) {
              if (j != 2) {
                if (j == 3)
                  this.mProgram.setSourceRegion(this.mSourceQuad); 
              } else {
                if (f2 > 1.0F) {
                  this.mTargetQuad.p0.set(0.0F, 0.5F - f2 * 0.5F);
                  this.mTargetQuad.p1.set(1.0F, 0.5F - f2 * 0.5F);
                  this.mTargetQuad.p2.set(0.0F, f2 * 0.5F + 0.5F);
                  this.mTargetQuad.p3.set(1.0F, f2 * 0.5F + 0.5F);
                } else {
                  this.mTargetQuad.p0.set(0.5F - 0.5F / f2, 0.0F);
                  this.mTargetQuad.p1.set(0.5F / f2 + 0.5F, 0.0F);
                  this.mTargetQuad.p2.set(0.5F - 0.5F / f2, 1.0F);
                  this.mTargetQuad.p3.set(0.5F / f2 + 0.5F, 1.0F);
                } 
                this.mProgram.setClearsOutput(true);
              } 
            } else {
              if (f2 > 1.0F) {
                this.mTargetQuad.p0.set(0.5F - 0.5F / f2, 0.0F);
                this.mTargetQuad.p1.set(0.5F / f2 + 0.5F, 0.0F);
                this.mTargetQuad.p2.set(0.5F - 0.5F / f2, 1.0F);
                this.mTargetQuad.p3.set(0.5F / f2 + 0.5F, 1.0F);
              } else {
                this.mTargetQuad.p0.set(0.0F, 0.5F - f2 * 0.5F);
                this.mTargetQuad.p1.set(1.0F, 0.5F - f2 * 0.5F);
                this.mTargetQuad.p2.set(0.0F, f2 * 0.5F + 0.5F);
                this.mTargetQuad.p3.set(1.0F, f2 * 0.5F + 0.5F);
              } 
              this.mProgram.setClearsOutput(true);
            } 
          } else {
            this.mTargetQuad.p0.set(0.0F, 0.0F);
            this.mTargetQuad.p1.set(1.0F, 0.0F);
            this.mTargetQuad.p2.set(0.0F, 1.0F);
            this.mTargetQuad.p3.set(1.0F, 1.0F);
            this.mProgram.setClearsOutput(false);
          } 
          if (this.mLogVerbose) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("UTR. quad: ");
            stringBuilder.append(this.mTargetQuad);
            Log.v("SurfaceTextureTarget", stringBuilder.toString());
          } 
          this.mProgram.setTargetRegion(this.mTargetQuad);
        } 
      } 
    } 
  }
}
