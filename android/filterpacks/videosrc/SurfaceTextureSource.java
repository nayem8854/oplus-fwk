package android.filterpacks.videosrc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.GLFrame;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.GenerateFinalPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;
import android.graphics.SurfaceTexture;
import android.opengl.Matrix;
import android.os.ConditionVariable;
import android.util.Log;

public class SurfaceTextureSource extends Filter {
  @GenerateFieldPort(hasDefault = true, name = "waitForNewFrame")
  private boolean mWaitForNewFrame = true;
  
  @GenerateFieldPort(hasDefault = true, name = "waitTimeout")
  private int mWaitTimeout = 1000;
  
  @GenerateFieldPort(hasDefault = true, name = "closeOnTimeout")
  private boolean mCloseOnTimeout = false;
  
  private static final float[] mSourceCoords = new float[] { 
      0.0F, 1.0F, 0.0F, 1.0F, 1.0F, 1.0F, 0.0F, 1.0F, 0.0F, 0.0F, 
      0.0F, 1.0F, 1.0F, 0.0F, 0.0F, 1.0F };
  
  private final String mRenderShader = "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n";
  
  private static final String TAG = "SurfaceTextureSource";
  
  private static final boolean mLogVerbose;
  
  private boolean mFirstFrame;
  
  private ShaderProgram mFrameExtractor;
  
  private float[] mFrameTransform;
  
  @GenerateFieldPort(name = "height")
  private int mHeight;
  
  private float[] mMappedCoords;
  
  private GLFrame mMediaFrame;
  
  private ConditionVariable mNewFrameAvailable;
  
  private MutableFrameFormat mOutputFormat;
  
  @GenerateFinalPort(name = "sourceListener")
  private SurfaceTextureSourceListener mSourceListener;
  
  private SurfaceTexture mSurfaceTexture;
  
  @GenerateFieldPort(name = "width")
  private int mWidth;
  
  private SurfaceTexture.OnFrameAvailableListener onFrameAvailableListener;
  
  static {
    mLogVerbose = Log.isLoggable("SurfaceTextureSource", 2);
  }
  
  public SurfaceTextureSource(String paramString) {
    super(paramString);
    this.onFrameAvailableListener = new SurfaceTexture.OnFrameAvailableListener() {
        final SurfaceTextureSource this$0;
        
        public void onFrameAvailable(SurfaceTexture param1SurfaceTexture) {
          if (SurfaceTextureSource.mLogVerbose)
            Log.v("SurfaceTextureSource", "New frame from SurfaceTexture"); 
          SurfaceTextureSource.this.mNewFrameAvailable.open();
        }
      };
    this.mNewFrameAvailable = new ConditionVariable();
    this.mFrameTransform = new float[16];
    this.mMappedCoords = new float[16];
  }
  
  public void setupPorts() {
    addOutputPort("video", ImageFormat.create(3, 3));
  }
  
  private void createFormats() {
    this.mOutputFormat = ImageFormat.create(this.mWidth, this.mHeight, 3, 3);
  }
  
  protected void prepare(FilterContext paramFilterContext) {
    if (mLogVerbose)
      Log.v("SurfaceTextureSource", "Preparing SurfaceTextureSource"); 
    createFormats();
    this.mMediaFrame = (GLFrame)paramFilterContext.getFrameManager().newBoundFrame(this.mOutputFormat, 104, 0L);
    this.mFrameExtractor = new ShaderProgram(paramFilterContext, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n");
  }
  
  public void open(FilterContext paramFilterContext) {
    if (mLogVerbose)
      Log.v("SurfaceTextureSource", "Opening SurfaceTextureSource"); 
    SurfaceTexture surfaceTexture = new SurfaceTexture(this.mMediaFrame.getTextureId());
    surfaceTexture.setOnFrameAvailableListener(this.onFrameAvailableListener);
    this.mSourceListener.onSurfaceTextureSourceReady(this.mSurfaceTexture);
    this.mFirstFrame = true;
  }
  
  public void process(FilterContext paramFilterContext) {
    if (mLogVerbose)
      Log.v("SurfaceTextureSource", "Processing new frame"); 
    if (this.mWaitForNewFrame || this.mFirstFrame) {
      int i = this.mWaitTimeout;
      if (i != 0) {
        boolean bool = this.mNewFrameAvailable.block(i);
        if (!bool) {
          if (this.mCloseOnTimeout) {
            if (mLogVerbose)
              Log.v("SurfaceTextureSource", "Timeout waiting for a new frame. Closing."); 
            closeOutputPort("video");
            return;
          } 
          throw new RuntimeException("Timeout waiting for new frame");
        } 
      } else {
        this.mNewFrameAvailable.block();
      } 
      this.mNewFrameAvailable.close();
      this.mFirstFrame = false;
    } 
    this.mSurfaceTexture.updateTexImage();
    this.mSurfaceTexture.getTransformMatrix(this.mFrameTransform);
    Matrix.multiplyMM(this.mMappedCoords, 0, this.mFrameTransform, 0, mSourceCoords, 0);
    ShaderProgram shaderProgram = this.mFrameExtractor;
    float[] arrayOfFloat = this.mMappedCoords;
    shaderProgram.setSourceRegion(arrayOfFloat[0], arrayOfFloat[1], arrayOfFloat[4], arrayOfFloat[5], arrayOfFloat[8], arrayOfFloat[9], arrayOfFloat[12], arrayOfFloat[13]);
    Frame frame = paramFilterContext.getFrameManager().newFrame(this.mOutputFormat);
    this.mFrameExtractor.process(this.mMediaFrame, frame);
    frame.setTimestamp(this.mSurfaceTexture.getTimestamp());
    pushOutput("video", frame);
    frame.release();
  }
  
  public void close(FilterContext paramFilterContext) {
    if (mLogVerbose)
      Log.v("SurfaceTextureSource", "SurfaceTextureSource closed"); 
    this.mSourceListener.onSurfaceTextureSourceReady(null);
    this.mSurfaceTexture.release();
    this.mSurfaceTexture = null;
  }
  
  public void tearDown(FilterContext paramFilterContext) {
    GLFrame gLFrame = this.mMediaFrame;
    if (gLFrame != null)
      gLFrame.release(); 
  }
  
  public void fieldPortValueUpdated(String paramString, FilterContext paramFilterContext) {
    if (paramString.equals("width") || paramString.equals("height"))
      this.mOutputFormat.setDimensions(this.mWidth, this.mHeight); 
  }
  
  class SurfaceTextureSourceListener {
    public abstract void onSurfaceTextureSourceReady(SurfaceTexture param1SurfaceTexture);
  }
}
