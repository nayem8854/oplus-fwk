package android.filterpacks.text;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.format.ObjectFormat;
import java.util.Locale;

public class ToUpperCase extends Filter {
  private FrameFormat mOutputFormat;
  
  public ToUpperCase(String paramString) {
    super(paramString);
  }
  
  public void setupPorts() {
    MutableFrameFormat mutableFrameFormat = ObjectFormat.fromClass(String.class, 1);
    addMaskedInputPort("mixedcase", mutableFrameFormat);
    addOutputPort("uppercase", this.mOutputFormat);
  }
  
  public void process(FilterContext paramFilterContext) {
    Frame frame2 = pullInput("mixedcase");
    String str = (String)frame2.getObjectValue();
    Frame frame1 = paramFilterContext.getFrameManager().newFrame(this.mOutputFormat);
    frame1.setObjectValue(str.toUpperCase(Locale.getDefault()));
    pushOutput("uppercase", frame1);
  }
}
