package android.filterpacks.performance;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.format.ObjectFormat;
import android.os.SystemClock;

public class ThroughputFilter extends Filter {
  @GenerateFieldPort(hasDefault = true, name = "period")
  private int mPeriod = 5;
  
  private long mLastTime = 0L;
  
  private int mTotalFrameCount = 0;
  
  private int mPeriodFrameCount = 0;
  
  private FrameFormat mOutputFormat;
  
  public ThroughputFilter(String paramString) {
    super(paramString);
  }
  
  public void setupPorts() {
    addInputPort("frame");
    this.mOutputFormat = ObjectFormat.fromClass(Throughput.class, 1);
    addOutputBasedOnInput("frame", "frame");
    addOutputPort("throughput", this.mOutputFormat);
  }
  
  public FrameFormat getOutputFormat(String paramString, FrameFormat paramFrameFormat) {
    return paramFrameFormat;
  }
  
  public void open(FilterContext paramFilterContext) {
    this.mTotalFrameCount = 0;
    this.mPeriodFrameCount = 0;
    this.mLastTime = 0L;
  }
  
  public void process(FilterContext paramFilterContext) {
    Frame frame = pullInput("frame");
    pushOutput("frame", frame);
    this.mTotalFrameCount++;
    this.mPeriodFrameCount++;
    if (this.mLastTime == 0L)
      this.mLastTime = SystemClock.elapsedRealtime(); 
    long l = SystemClock.elapsedRealtime();
    if (l - this.mLastTime >= (this.mPeriod * 1000)) {
      FrameFormat frameFormat = frame.getFormat();
      int i = frameFormat.getWidth(), j = frameFormat.getHeight();
      Throughput throughput = new Throughput(this.mTotalFrameCount, this.mPeriodFrameCount, this.mPeriod, i * j);
      Frame frame1 = paramFilterContext.getFrameManager().newFrame(this.mOutputFormat);
      frame1.setObjectValue(throughput);
      pushOutput("throughput", frame1);
      this.mLastTime = l;
      this.mPeriodFrameCount = 0;
    } 
  }
}
