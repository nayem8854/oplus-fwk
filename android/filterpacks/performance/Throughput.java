package android.filterpacks.performance;

public class Throughput {
  private final int mPeriodFrames;
  
  private final int mPeriodTime;
  
  private final int mPixels;
  
  private final int mTotalFrames;
  
  public Throughput(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mTotalFrames = paramInt1;
    this.mPeriodFrames = paramInt2;
    this.mPeriodTime = paramInt3;
    this.mPixels = paramInt4;
  }
  
  public int getTotalFrameCount() {
    return this.mTotalFrames;
  }
  
  public int getPeriodFrameCount() {
    return this.mPeriodFrames;
  }
  
  public int getPeriodTime() {
    return this.mPeriodTime;
  }
  
  public float getFramesPerSecond() {
    return this.mPeriodFrames / this.mPeriodTime;
  }
  
  public float getNanosPerPixel() {
    double d = this.mPeriodTime / this.mPeriodFrames;
    return (float)(d * 1000000.0D / this.mPixels);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(getFramesPerSecond());
    stringBuilder.append(" FPS");
    return stringBuilder.toString();
  }
}
