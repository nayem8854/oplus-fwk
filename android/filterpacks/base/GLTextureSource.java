package android.filterpacks.base;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.format.ImageFormat;

public class GLTextureSource extends Filter {
  private Frame mFrame;
  
  @GenerateFieldPort(name = "height")
  private int mHeight;
  
  @GenerateFieldPort(hasDefault = true, name = "repeatFrame")
  private boolean mRepeatFrame = false;
  
  @GenerateFieldPort(name = "texId")
  private int mTexId;
  
  @GenerateFieldPort(hasDefault = true, name = "timestamp")
  private long mTimestamp = -1L;
  
  @GenerateFieldPort(name = "width")
  private int mWidth;
  
  public GLTextureSource(String paramString) {
    super(paramString);
  }
  
  public void setupPorts() {
    addOutputPort("frame", ImageFormat.create(3, 3));
  }
  
  public void fieldPortValueUpdated(String paramString, FilterContext paramFilterContext) {
    Frame frame = this.mFrame;
    if (frame != null) {
      frame.release();
      this.mFrame = null;
    } 
  }
  
  public void process(FilterContext paramFilterContext) {
    if (this.mFrame == null) {
      MutableFrameFormat mutableFrameFormat = ImageFormat.create(this.mWidth, this.mHeight, 3, 3);
      Frame frame = paramFilterContext.getFrameManager().newBoundFrame(mutableFrameFormat, 100, this.mTexId);
      frame.setTimestamp(this.mTimestamp);
    } 
    pushOutput("frame", this.mFrame);
    if (!this.mRepeatFrame)
      closeOutputPort("frame"); 
  }
  
  public void tearDown(FilterContext paramFilterContext) {
    Frame frame = this.mFrame;
    if (frame != null)
      frame.release(); 
  }
}
