package android.filterpacks.base;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.format.ImageFormat;

public class GLTextureTarget extends Filter {
  @GenerateFieldPort(name = "texId")
  private int mTexId;
  
  public GLTextureTarget(String paramString) {
    super(paramString);
  }
  
  public void setupPorts() {
    addMaskedInputPort("frame", ImageFormat.create(3));
  }
  
  public void process(FilterContext paramFilterContext) {
    Frame frame2 = pullInput("frame");
    int i = frame2.getFormat().getWidth();
    int j = frame2.getFormat().getHeight();
    MutableFrameFormat mutableFrameFormat = ImageFormat.create(i, j, 3, 3);
    Frame frame1 = paramFilterContext.getFrameManager().newBoundFrame(mutableFrameFormat, 100, this.mTexId);
    frame1.setDataFromFrame(frame2);
    frame1.release();
  }
}
