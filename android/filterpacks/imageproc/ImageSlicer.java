package android.filterpacks.imageproc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.Program;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;

public class ImageSlicer extends Filter {
  private int mInputHeight;
  
  private int mInputWidth;
  
  private Frame mOriginalFrame;
  
  private int mOutputHeight;
  
  private int mOutputWidth;
  
  @GenerateFieldPort(name = "padSize")
  private int mPadSize;
  
  private Program mProgram;
  
  private int mSliceHeight;
  
  private int mSliceIndex;
  
  private int mSliceWidth;
  
  @GenerateFieldPort(name = "xSlices")
  private int mXSlices;
  
  @GenerateFieldPort(name = "ySlices")
  private int mYSlices;
  
  public ImageSlicer(String paramString) {
    super(paramString);
    this.mSliceIndex = 0;
  }
  
  public void setupPorts() {
    addMaskedInputPort("image", ImageFormat.create(3, 3));
    addOutputBasedOnInput("image", "image");
  }
  
  public FrameFormat getOutputFormat(String paramString, FrameFormat paramFrameFormat) {
    return paramFrameFormat;
  }
  
  private void calcOutputFormatForInput(Frame paramFrame) {
    this.mInputWidth = paramFrame.getFormat().getWidth();
    int i = paramFrame.getFormat().getHeight();
    int j = this.mInputWidth, k = this.mXSlices;
    this.mSliceWidth = j = (j + k - 1) / k;
    k = this.mYSlices;
    this.mSliceHeight = k = (i + k - 1) / k;
    i = this.mPadSize;
    this.mOutputWidth = j + i * 2;
    this.mOutputHeight = k + i * 2;
  }
  
  public void process(FilterContext paramFilterContext) {
    if (this.mSliceIndex == 0) {
      Frame frame1 = pullInput("image");
      calcOutputFormatForInput(frame1);
    } 
    FrameFormat frameFormat = this.mOriginalFrame.getFormat();
    frameFormat = frameFormat.mutableCopy();
    frameFormat.setDimensions(this.mOutputWidth, this.mOutputHeight);
    Frame frame = paramFilterContext.getFrameManager().newFrame(frameFormat);
    if (this.mProgram == null)
      this.mProgram = ShaderProgram.createIdentity(paramFilterContext); 
    int i = this.mSliceIndex, j = this.mXSlices;
    int k = i / j;
    int m = this.mSliceWidth, n = this.mPadSize;
    float f1 = (m * i % j - n);
    m = this.mInputWidth;
    f1 /= m;
    float f2 = (this.mSliceHeight * k - n);
    k = this.mInputHeight;
    f2 /= k;
    ((ShaderProgram)this.mProgram).setSourceRect(f1, f2, this.mOutputWidth / m, this.mOutputHeight / k);
    this.mProgram.process(this.mOriginalFrame, frame);
    this.mSliceIndex = k = this.mSliceIndex + 1;
    if (k == this.mXSlices * this.mYSlices) {
      this.mSliceIndex = 0;
      this.mOriginalFrame.release();
      setWaitsOnInputPort("image", true);
    } else {
      this.mOriginalFrame.retain();
      setWaitsOnInputPort("image", false);
    } 
    pushOutput("image", frame);
    frame.release();
  }
}
