package android.filterpacks.imageproc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.Program;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;
import android.filterfw.geometry.Point;
import android.filterfw.geometry.Quad;

public class RotateFilter extends Filter {
  @GenerateFieldPort(hasDefault = true, name = "tile_size")
  private int mTileSize = 640;
  
  private int mWidth = 0;
  
  private int mHeight = 0;
  
  private int mTarget = 0;
  
  @GenerateFieldPort(name = "angle")
  private int mAngle;
  
  private int mOutputHeight;
  
  private int mOutputWidth;
  
  private Program mProgram;
  
  public RotateFilter(String paramString) {
    super(paramString);
  }
  
  public void setupPorts() {
    addMaskedInputPort("image", ImageFormat.create(3));
    addOutputBasedOnInput("image", "image");
  }
  
  public void initProgram(FilterContext paramFilterContext, int paramInt) {
    if (paramInt == 3) {
      ShaderProgram shaderProgram = ShaderProgram.createIdentity(paramFilterContext);
      shaderProgram.setMaximumTileSize(this.mTileSize);
      shaderProgram.setClearsOutput(true);
      this.mProgram = shaderProgram;
      this.mTarget = paramInt;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Filter Sharpen does not support frames of target ");
    stringBuilder.append(paramInt);
    stringBuilder.append("!");
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public void fieldPortValueUpdated(String paramString, FilterContext paramFilterContext) {
    if (this.mProgram != null)
      updateParameters(); 
  }
  
  public void process(FilterContext paramFilterContext) {
    Frame frame2 = pullInput("image");
    FrameFormat frameFormat = frame2.getFormat();
    if (this.mProgram == null || frameFormat.getTarget() != this.mTarget)
      initProgram(paramFilterContext, frameFormat.getTarget()); 
    if (frameFormat.getWidth() != this.mWidth || frameFormat.getHeight() != this.mHeight) {
      this.mWidth = frameFormat.getWidth();
      int i = frameFormat.getHeight();
      this.mOutputWidth = this.mWidth;
      this.mOutputHeight = i;
      updateParameters();
    } 
    frameFormat = ImageFormat.create(this.mOutputWidth, this.mOutputHeight, 3, 3);
    Frame frame1 = paramFilterContext.getFrameManager().newFrame(frameFormat);
    this.mProgram.process(frame2, frame1);
    pushOutput("image", frame1);
    frame1.release();
  }
  
  private void updateParameters() {
    int i = this.mAngle;
    if (i % 90 == 0) {
      float f2, f1 = -1.0F;
      if (i % 180 == 0) {
        f2 = 0.0F;
        if (i % 360 == 0)
          f1 = 1.0F; 
      } else {
        if ((i + 90) % 360 != 0)
          f1 = 1.0F; 
        this.mOutputWidth = this.mHeight;
        this.mOutputHeight = this.mWidth;
        float f = 0.0F;
        f2 = f1;
        f1 = f;
      } 
      Point point1 = new Point((-f1 + f2 + 1.0F) * 0.5F, (-f2 - f1 + 1.0F) * 0.5F);
      Point point2 = new Point((f1 + f2 + 1.0F) * 0.5F, (f2 - f1 + 1.0F) * 0.5F);
      Point point3 = new Point((-f1 - f2 + 1.0F) * 0.5F, (-f2 + f1 + 1.0F) * 0.5F);
      Point point4 = new Point((f1 - f2 + 1.0F) * 0.5F, (f2 + f1 + 1.0F) * 0.5F);
      Quad quad = new Quad(point1, point2, point3, point4);
      ((ShaderProgram)this.mProgram).setTargetRegion(quad);
      return;
    } 
    throw new RuntimeException("degree has to be multiply of 90.");
  }
}
