package android.filterpacks.imageproc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.core.Program;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import res.Hex;

public class AutoFixFilter extends Filter {
  private static long[] $d2j$hex$7e904607$decode_J(String src) {
    byte[] d = Hex.decode_B(src);
    ByteBuffer b = ByteBuffer.wrap(d);
    b.order(ByteOrder.LITTLE_ENDIAN);
    LongBuffer s = b.asLongBuffer();
    long[] data = new long[d.length / 8];
    s.get(data);
    return data;
  }
  
  private static int[] $d2j$hex$7e904607$decode_I(String src) {
    byte[] d = Hex.decode_B(src);
    ByteBuffer b = ByteBuffer.wrap(d);
    b.order(ByteOrder.LITTLE_ENDIAN);
    IntBuffer s = b.asIntBuffer();
    int[] data = new int[d.length / 4];
    s.get(data);
    return data;
  }
  
  private static short[] $d2j$hex$7e904607$decode_S(String src) {
    byte[] d = Hex.decode_B(src);
    ByteBuffer b = ByteBuffer.wrap(d);
    b.order(ByteOrder.LITTLE_ENDIAN);
    ShortBuffer s = b.asShortBuffer();
    short[] data = new short[d.length / 2];
    s.get(data);
    return data;
  }
  
  @GenerateFieldPort(hasDefault = true, name = "tile_size")
  private int mTileSize = 640;
  
  private static final int[] normal_cdf = $d2j$hex$7e904607$decode_I("090000002100000032000000400000004b000000540000005c000000630000006a00000070000000750000007a0000007e00000082000000860000008a0000008e0000009100000094000000960000009a0000009d0000009f000000a2000000a4000000a6000000a9000000aa000000ad000000af000000b1000000b3000000b4000000b6000000b8000000ba000000bc000000bd000000be000000c0000000c2000000c3000000c5000000c6000000c7000000c8000000ca000000cb000000cd000000ce000000cf000000d0000000d1000000d2000000d4000000d5000000d6000000d7000000d8000000d9000000da000000db000000dc000000dd000000de000000df000000e0000000e1000000e2000000e3000000e4000000e5000000e5000000e6000000e7000000e8000000e9000000ea000000eb000000ec000000ec000000ed000000ee000000ef000000ef000000f0000000f0000000f2000000f2000000f3000000f4000000f5000000f5000000f6000000f7000000f7000000f8000000f9000000f9000000fa000000fa000000fb000000fc000000fd000000fd000000fe000000ff000000ff0000000001000000010000010100000201000002010000030100000301000003010000040100000501000006010000060100000701000007010000080100000801000009010000090100000a0100000b0100000b0100000c0100000c0100000d0100000d0100000d0100000e0100000e0100000f0100001001000010010000110100001101000012010000120100001301000013010000140100001401000015010000150100001501000016010000160100001701000017010000170100001801000018010000190100001a0100001a0100001a0100001b0100001b0100001c0100001c0100001d0100001d0100001d0100001e0100001e0100001f0100001f01000020010000200100002001000021010000210100002101000022010000220100002201000023010000240100002401000024010000250100002501000026010000260100002601000027010000270100002801000028010000280100002901000029010000290100002a0100002a0100002a0100002b0100002b0100002b0100002b0100002c0100002c0100002d0100002d0100002e0100002e0100002e0100002f0100002f0100003001000030010000300100003101000031010000310100003201000032010000320100003301000033010000330100003401000034010000340100003501000035010000350100003501000036010000360100003601000036010000370100003801000038010000380100003901000039010000390100003a0100003a0100003a0100003b0100003b0100003b0100003b0100003c0100003c0100003c0100003d0100003d0100003d0100003e0100003e0100003e0100003f0100003f0100003f0100003f0100003f010000400100004001000040010000410100004101000042010000420100004201000043010000430100004301000043010000440100004401000044010000450100004501000045010000450100004601000046010000460100004701000047010000470100004701000048010000480100004801000049010000490100004901000049010000490100004a0100004a0100004a0100004a0100004b0100004b0100004c0100004c0100004c0100004d0100004d0100004d0100004d0100004e0100004e0100004e0100004e0100004f0100004f0100004f01000050010000500100005001000050010000510100005101000051010000510100005201000052010000520100005301000053010000530100005301000053010000530100005401000054010000540100005401000055010000550100005601000056010000560100005601000057010000570100005701000058010000580100005801000058010000590100005901000059010000590100005a0100005a0100005a0100005a0100005b0100005b0100005b0100005b0100005c0100005c0100005c0100005c0100005d0100005d0100005d0100005d0100005d0100005d0100005e0100005e0100005e0100005e0100005f0100005f01000060010000600100006001000060010000610100006101000061010000610100006201000062010000620100006201000063010000630100006301000063010000640100006401000064010000640100006501000065010000650100006501000066010000660100006601000066010000670100006701000067010000670100006701000067010000670100006801000068010000680100006801000069010000690100006a0100006a0100006a0100006a0100006b0100006b0100006b0100006b0100006c0100006c0100006c0100006c0100006d0100006d0100006d0100006d0100006e0100006e0100006e0100006e0100006e0100006f0100006f0100006f0100006f0100007001000070010000700100007001000071010000710100007101000071010000710100007101000072010000720100007201000072010000720100007301000073010000740100007401000074010000740100007501000075010000750100007501000076010000760100007601000076010000760100007701000077010000770100007701000078010000780100007801000078010000790100007901000079010000790100007a0100007a0100007a0100007a0100007a0100007b0100007b0100007b0100007b0100007b0100007b0100007c0100007c0100007c0100007c0100007d0100007d0100007d0100007e0100007e0100007e0100007e0100007f0100007f0100007f0100007f010000800100008001000080010000800100008101000081010000810100008101000081010000820100008201000082010000820100008301000083010000830100008301000084010000840100008401000084010000840100008501000085010000850100008501000085010000850100008601000086010000860100008601000087010000870100008801000088010000880100008801000088010000890100008901000089010000890100008a0100008a0100008a0100008a0100008b0100008b0100008b0100008b0100008c0100008c0100008c0100008c0100008c0100008d0100008d0100008d0100008d0100008e0100008e0100008e0100008e0100008f0100008f0100008f0100008f0100008f0100008f0100009001000090010000900100009001000090010000910100009101000092010000920100009201000092010000930100009301000093010000930100009401000094010000940100009401000095010000950100009501000095010000960100009601000096010000960100009601000097010000970100009701000097010000980100009801000098010000980100009901000099010000990100009901000099010000990100009a0100009a0100009a0100009a0100009b0100009b0100009c0100009c0100009c0100009c0100009d0100009d0100009d0100009d0100009e0100009e0100009e0100009e0100009f0100009f0100009f0100009f010000a0010000a0010000a0010000a0010000a1010000a1010000a1010000a1010000a2010000a2010000a2010000a2010000a3010000a3010000a3010000a3010000a3010000a3010000a4010000a4010000a4010000a4010000a5010000a5010000a6010000a6010000a6010000a6010000a7010000a7010000a7010000a7010000a8010000a8010000a8010000a9010000a9010000a9010000a9010000aa010000aa010000aa010000aa010000ab010000ab010000ab010000ab010000ac010000ac010000ac010000ad010000ad010000ad010000ad010000ad010000ad010000ae010000ae010000ae010000ae010000af010000af010000b0010000b0010000b0010000b1010000b1010000b1010000b1010000b2010000b2010000b2010000b3010000b3010000b3010000b3010000b4010000b4010000b4010000b4010000b5010000b5010000b5010000b6010000b6010000b6010000b6010000b7010000b7010000b7010000b7010000b7010000b8010000b8010000b8010000b9010000b9010000ba010000ba010000ba010000bb010000bb010000bb010000bb010000bc010000bc010000bc010000bd010000bd010000bd010000be010000be010000be010000be010000bf010000bf010000bf010000c0010000c0010000c0010000c1010000c1010000c1010000c1010000c1010000c2010000c2010000c2010000c3010000c3010000c4010000c4010000c4010000c5010000c5010000c5010000c6010000c6010000c6010000c7010000c7010000c7010000c8010000c8010000c8010000c9010000c9010000c9010000ca010000ca010000ca010000cb010000cb010000cb010000cb010000cc010000cc010000cc010000cd010000cd010000ce010000ce010000ce010000cf010000cf010000cf010000d0010000d0010000d1010000d1010000d1010000d2010000d2010000d2010000d3010000d3010000d3010000d4010000d4010000d5010000d5010000d5010000d5010000d6010000d6010000d6010000d7010000d8010000d8010000d8010000d9010000d9010000da010000da010000da010000db010000db010000dc010000dc010000dc010000dd010000dd010000de010000de010000de010000df010000df010000df010000e0010000e0010000e0010000e1010000e2010000e2010000e3010000e3010000e4010000e4010000e4010000e5010000e5010000e6010000e6010000e7010000e7010000e8010000e8010000e8010000e9010000e9010000e9010000ea010000ea010000eb010000ec010000ec010000ed010000ed010000ee010000ee010000ef010000ef010000f0010000f0010000f1010000f1010000f2010000f2010000f3010000f3010000f3010000f4010000f5010000f6010000f6010000f7010000f7010000f8010000f8010000f9010000f9010000fa010000fb010000fb010000fc010000fc010000fd010000fd010000fe010000fe010000ff0100000002000001020000010200000202000003020000030200000402000005020000050200000602000007020000070200000702000008020000090200000a0200000b0200000c0200000c0200000d0200000e0200000e0200000f0200001002000011020000110200001202000013020000140200001502000016020000170200001702000018020000190200001a0200001b0200001b0200001c0200001e0200001f020000200200002102000022020000230200002402000025020000250200002602000028020000290200002a0200002b0200002c0200002e0200002f0200002f020000310200003202000034020000350200003602000038020000390200003a0200003c0200003e0200003f0200004102000042020000430200004602000047020000490200004b0200004d0200004e020000510200005302000055020000570200005a0200005c0200005f0200006102000064020000670200006a0200006c0200007002000074020000770200007b0200007f02000084020000890200008e020000930200009a020000a1020000a8020000b2020000bc020000ca020000");
  
  private static byte[] $d2j$hex$7e904607$decode_B(String src) {
    char[] d = src.toCharArray();
    byte[] ret = new byte[src.length() / 2];
    for (int i = 0; i < ret.length; i++) {
      char h = d[2 * i];
      char l = d[2 * i + 1];
      int hh = 0;
      if (h >= '0' && h <= '9') {
        hh = h - 48;
      } else if (h >= 'a' && h <= 'f') {
        hh = h - 97 + 10;
      } else if (h >= 'A' && h <= 'F') {
        hh = h - 65 + 10;
      } else {
        throw new RuntimeException();
      } 
      int ll = 0;
      if (l >= '0' && l <= '9') {
        ll = h - 48;
      } else if (l >= 'a' && l <= 'f') {
        ll = h - 97 + 10;
      } else if (l >= 'A' && l <= 'F') {
        ll = h - 65 + 10;
      } else {
        throw new RuntimeException();
      } 
      d[i] = (char)(hh << 4 | ll);
    } 
    return ret;
  }
  
  private final String mAutoFixShader = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform sampler2D tex_sampler_2;\nuniform float scale;\nuniform float shift_scale;\nuniform float hist_offset;\nuniform float hist_scale;\nuniform float density_offset;\nuniform float density_scale;\nvarying vec2 v_texcoord;\nvoid main() {\n  const vec3 weights = vec3(0.33333, 0.33333, 0.33333);\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float energy = dot(color.rgb, weights);\n  float mask_value = energy - 0.5;\n  float alpha;\n  if (mask_value > 0.0) {\n    alpha = (pow(2.0 * mask_value, 1.5) - 1.0) * scale + 1.0;\n  } else { \n    alpha = (pow(2.0 * mask_value, 2.0) - 1.0) * scale + 1.0;\n  }\n  float index = energy * hist_scale + hist_offset;\n  vec4 temp = texture2D(tex_sampler_1, vec2(index, 0.5));\n  float value = temp.g + temp.r * shift_scale;\n  index = value * density_scale + density_offset;\n  temp = texture2D(tex_sampler_2, vec2(index, 0.5));\n  value = temp.g + temp.r * shift_scale;\n  float dst_energy = energy * alpha + value * (1.0 - alpha);\n  float max_energy = energy / max(color.r, max(color.g, color.b));\n  if (dst_energy > max_energy) {\n    dst_energy = max_energy;\n  }\n  if (energy == 0.0) {\n    gl_FragColor = color;\n  } else {\n    gl_FragColor = vec4(color.rgb * dst_energy / energy, color.a);\n  }\n}\n";
  
  private int mWidth = 0;
  
  private int mHeight = 0;
  
  private int mTarget = 0;
  
  private Frame mDensityFrame;
  
  private Frame mHistFrame;
  
  private Program mNativeProgram;
  
  @GenerateFieldPort(name = "scale")
  private float mScale;
  
  private Program mShaderProgram;
  
  public AutoFixFilter(String paramString) {
    super(paramString);
  }
  
  public void setupPorts() {
    addMaskedInputPort("image", ImageFormat.create(3));
    addOutputBasedOnInput("image", "image");
  }
  
  public FrameFormat getOutputFormat(String paramString, FrameFormat paramFrameFormat) {
    return paramFrameFormat;
  }
  
  public void initProgram(FilterContext paramFilterContext, int paramInt) {
    if (paramInt == 3) {
      ShaderProgram shaderProgram = new ShaderProgram(paramFilterContext, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform sampler2D tex_sampler_2;\nuniform float scale;\nuniform float shift_scale;\nuniform float hist_offset;\nuniform float hist_scale;\nuniform float density_offset;\nuniform float density_scale;\nvarying vec2 v_texcoord;\nvoid main() {\n  const vec3 weights = vec3(0.33333, 0.33333, 0.33333);\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float energy = dot(color.rgb, weights);\n  float mask_value = energy - 0.5;\n  float alpha;\n  if (mask_value > 0.0) {\n    alpha = (pow(2.0 * mask_value, 1.5) - 1.0) * scale + 1.0;\n  } else { \n    alpha = (pow(2.0 * mask_value, 2.0) - 1.0) * scale + 1.0;\n  }\n  float index = energy * hist_scale + hist_offset;\n  vec4 temp = texture2D(tex_sampler_1, vec2(index, 0.5));\n  float value = temp.g + temp.r * shift_scale;\n  index = value * density_scale + density_offset;\n  temp = texture2D(tex_sampler_2, vec2(index, 0.5));\n  value = temp.g + temp.r * shift_scale;\n  float dst_energy = energy * alpha + value * (1.0 - alpha);\n  float max_energy = energy / max(color.r, max(color.g, color.b));\n  if (dst_energy > max_energy) {\n    dst_energy = max_energy;\n  }\n  if (energy == 0.0) {\n    gl_FragColor = color;\n  } else {\n    gl_FragColor = vec4(color.rgb * dst_energy / energy, color.a);\n  }\n}\n");
      shaderProgram.setMaximumTileSize(this.mTileSize);
      this.mShaderProgram = shaderProgram;
      this.mTarget = paramInt;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Filter Sharpen does not support frames of target ");
    stringBuilder.append(paramInt);
    stringBuilder.append("!");
    throw new RuntimeException(stringBuilder.toString());
  }
  
  private void initParameters() {
    this.mShaderProgram.setHostValue("shift_scale", Float.valueOf(0.00390625F));
    this.mShaderProgram.setHostValue("hist_offset", Float.valueOf(6.527415E-4F));
    this.mShaderProgram.setHostValue("hist_scale", Float.valueOf(0.99869454F));
    this.mShaderProgram.setHostValue("density_offset", Float.valueOf(4.8828125E-4F));
    this.mShaderProgram.setHostValue("density_scale", Float.valueOf(0.99902344F));
    this.mShaderProgram.setHostValue("scale", Float.valueOf(this.mScale));
  }
  
  protected void prepare(FilterContext paramFilterContext) {
    int[] arrayOfInt = new int[1024];
    for (byte b = 0; b < 'Ѐ'; b++) {
      long l = normal_cdf[b] * 65535L / 766L;
      arrayOfInt[b] = (int)l;
    } 
    MutableFrameFormat mutableFrameFormat = ImageFormat.create(1024, 1, 3, 3);
    Frame frame = paramFilterContext.getFrameManager().newFrame(mutableFrameFormat);
    frame.setInts(arrayOfInt);
  }
  
  public void tearDown(FilterContext paramFilterContext) {
    Frame frame = this.mDensityFrame;
    if (frame != null) {
      frame.release();
      this.mDensityFrame = null;
    } 
    frame = this.mHistFrame;
    if (frame != null) {
      frame.release();
      this.mHistFrame = null;
    } 
  }
  
  public void fieldPortValueUpdated(String paramString, FilterContext paramFilterContext) {
    Program program = this.mShaderProgram;
    if (program != null)
      program.setHostValue("scale", Float.valueOf(this.mScale)); 
  }
  
  public void process(FilterContext paramFilterContext) {
    Frame frame2 = pullInput("image");
    FrameFormat frameFormat = frame2.getFormat();
    if (this.mShaderProgram == null || frameFormat.getTarget() != this.mTarget) {
      initProgram(paramFilterContext, frameFormat.getTarget());
      initParameters();
    } 
    if (frameFormat.getWidth() != this.mWidth || frameFormat.getHeight() != this.mHeight) {
      this.mWidth = frameFormat.getWidth();
      int i = frameFormat.getHeight();
      createHistogramFrame(paramFilterContext, this.mWidth, i, frame2.getInts());
    } 
    Frame frame4 = paramFilterContext.getFrameManager().newFrame(frameFormat);
    Frame frame1 = this.mHistFrame, frame3 = this.mDensityFrame;
    this.mShaderProgram.process(new Frame[] { frame2, frame1, frame3 }, frame4);
    pushOutput("image", frame4);
    frame4.release();
  }
  
  private void createHistogramFrame(FilterContext paramFilterContext, int paramInt1, int paramInt2, int[] paramArrayOfint) {
    int[] arrayOfInt = new int[766];
    int i = (int)(paramInt2 * 0.05F);
    int j = (int)(paramInt1 * 0.05F);
    int k;
    for (k = i; k < paramInt2 - i; k++) {
      for (int m = j; m < paramInt1 - j; m++) {
        int n = k * paramInt1 + m;
        n = (paramArrayOfint[n] & 0xFF) + (paramArrayOfint[n] >> 8 & 0xFF) + (paramArrayOfint[n] >> 16 & 0xFF);
        arrayOfInt[n] = arrayOfInt[n] + 1;
      } 
    } 
    for (k = 1; k < 766; k++)
      arrayOfInt[k] = arrayOfInt[k] + arrayOfInt[k - 1]; 
    for (k = 0; k < 766; k++) {
      long l = arrayOfInt[k] * 65535L / ((paramInt1 - j * 2) * (paramInt2 - i * 2));
      arrayOfInt[k] = (int)l;
    } 
    MutableFrameFormat mutableFrameFormat = ImageFormat.create(766, 1, 3, 3);
    Frame frame2 = this.mHistFrame;
    if (frame2 != null)
      frame2.release(); 
    Frame frame1 = paramFilterContext.getFrameManager().newFrame(mutableFrameFormat);
    frame1.setInts(arrayOfInt);
  }
}
