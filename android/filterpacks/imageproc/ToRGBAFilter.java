package android.filterpacks.imageproc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.core.NativeProgram;
import android.filterfw.core.Program;

public class ToRGBAFilter extends Filter {
  private int mInputBPP;
  
  private FrameFormat mLastFormat = null;
  
  private Program mProgram;
  
  public ToRGBAFilter(String paramString) {
    super(paramString);
  }
  
  public void setupPorts() {
    MutableFrameFormat mutableFrameFormat = new MutableFrameFormat(2, 2);
    mutableFrameFormat.setDimensionCount(2);
    addMaskedInputPort("image", mutableFrameFormat);
    addOutputBasedOnInput("image", "image");
  }
  
  public FrameFormat getOutputFormat(String paramString, FrameFormat paramFrameFormat) {
    return getConvertedFormat(paramFrameFormat);
  }
  
  public FrameFormat getConvertedFormat(FrameFormat paramFrameFormat) {
    paramFrameFormat = paramFrameFormat.mutableCopy();
    paramFrameFormat.setMetaValue("colorspace", Integer.valueOf(3));
    paramFrameFormat.setBytesPerSample(4);
    return paramFrameFormat;
  }
  
  public void createProgram(FilterContext paramFilterContext, FrameFormat paramFrameFormat) {
    this.mInputBPP = paramFrameFormat.getBytesPerSample();
    FrameFormat frameFormat = this.mLastFormat;
    if (frameFormat != null && frameFormat.getBytesPerSample() == this.mInputBPP)
      return; 
    this.mLastFormat = paramFrameFormat;
    int i = this.mInputBPP;
    if (i != 1) {
      if (i == 3) {
        this.mProgram = new NativeProgram("filterpack_imageproc", "rgb_to_rgba");
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unsupported BytesPerPixel: ");
        stringBuilder.append(this.mInputBPP);
        stringBuilder.append("!");
        throw new RuntimeException(stringBuilder.toString());
      } 
    } else {
      this.mProgram = new NativeProgram("filterpack_imageproc", "gray_to_rgba");
    } 
  }
  
  public void process(FilterContext paramFilterContext) {
    Frame frame2 = pullInput("image");
    createProgram(paramFilterContext, frame2.getFormat());
    Frame frame1 = paramFilterContext.getFrameManager().newFrame(getConvertedFormat(frame2.getFormat()));
    this.mProgram.process(frame2, frame1);
    pushOutput("image", frame1);
    frame1.release();
  }
}
