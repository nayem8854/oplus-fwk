package android.filterpacks.imageproc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.format.ImageFormat;
import android.graphics.Bitmap;

public class BitmapSource extends Filter {
  @GenerateFieldPort(name = "bitmap")
  private Bitmap mBitmap;
  
  private Frame mImageFrame;
  
  @GenerateFieldPort(hasDefault = true, name = "recycleBitmap")
  private boolean mRecycleBitmap = true;
  
  @GenerateFieldPort(hasDefault = true, name = "repeatFrame")
  boolean mRepeatFrame = false;
  
  private int mTarget;
  
  @GenerateFieldPort(name = "target")
  String mTargetString;
  
  public BitmapSource(String paramString) {
    super(paramString);
  }
  
  public void setupPorts() {
    MutableFrameFormat mutableFrameFormat = ImageFormat.create(3, 0);
    addOutputPort("image", mutableFrameFormat);
  }
  
  public void loadImage(FilterContext paramFilterContext) {
    this.mTarget = FrameFormat.readTargetString(this.mTargetString);
    int i = this.mBitmap.getWidth();
    Bitmap bitmap = this.mBitmap;
    int j = bitmap.getHeight(), k = this.mTarget;
    MutableFrameFormat mutableFrameFormat = ImageFormat.create(i, j, 3, k);
    Frame frame = paramFilterContext.getFrameManager().newFrame(mutableFrameFormat);
    frame.setBitmap(this.mBitmap);
    this.mImageFrame.setTimestamp(-1L);
    if (this.mRecycleBitmap)
      this.mBitmap.recycle(); 
    this.mBitmap = null;
  }
  
  public void fieldPortValueUpdated(String paramString, FilterContext paramFilterContext) {
    if (paramString.equals("bitmap") || paramString.equals("target")) {
      Frame frame = this.mImageFrame;
      if (frame != null) {
        frame.release();
        this.mImageFrame = null;
      } 
    } 
  }
  
  public void process(FilterContext paramFilterContext) {
    if (this.mImageFrame == null)
      loadImage(paramFilterContext); 
    pushOutput("image", this.mImageFrame);
    if (!this.mRepeatFrame)
      closeOutputPort("image"); 
  }
  
  public void tearDown(FilterContext paramFilterContext) {
    Frame frame = this.mImageFrame;
    if (frame != null) {
      frame.release();
      this.mImageFrame = null;
    } 
  }
}
