package android.filterpacks.imageproc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.GLFrame;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.core.Program;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;

public class ResizeFilter extends Filter {
  @GenerateFieldPort(hasDefault = true, name = "keepAspectRatio")
  private boolean mKeepAspectRatio = false;
  
  @GenerateFieldPort(hasDefault = true, name = "generateMipMap")
  private boolean mGenerateMipMap = false;
  
  private FrameFormat mLastFormat = null;
  
  private int mInputChannels;
  
  @GenerateFieldPort(name = "oheight")
  private int mOHeight;
  
  @GenerateFieldPort(name = "owidth")
  private int mOWidth;
  
  private MutableFrameFormat mOutputFormat;
  
  private Program mProgram;
  
  public ResizeFilter(String paramString) {
    super(paramString);
  }
  
  public void setupPorts() {
    addMaskedInputPort("image", ImageFormat.create(3));
    addOutputBasedOnInput("image", "image");
  }
  
  public FrameFormat getOutputFormat(String paramString, FrameFormat paramFrameFormat) {
    return paramFrameFormat;
  }
  
  protected void createProgram(FilterContext paramFilterContext, FrameFormat paramFrameFormat) {
    FrameFormat frameFormat = this.mLastFormat;
    if (frameFormat != null && frameFormat.getTarget() == paramFrameFormat.getTarget())
      return; 
    this.mLastFormat = paramFrameFormat;
    int i = paramFrameFormat.getTarget();
    if (i != 2) {
      if (i == 3) {
        ShaderProgram shaderProgram = ShaderProgram.createIdentity(paramFilterContext);
        this.mProgram = shaderProgram;
        return;
      } 
      throw new RuntimeException("ResizeFilter could not create suitable program!");
    } 
    throw new RuntimeException("Native ResizeFilter not implemented yet!");
  }
  
  public void process(FilterContext paramFilterContext) {
    Frame frame1 = pullInput("image");
    createProgram(paramFilterContext, frame1.getFormat());
    MutableFrameFormat mutableFrameFormat = frame1.getFormat().mutableCopy();
    if (this.mKeepAspectRatio) {
      FrameFormat frameFormat = frame1.getFormat();
      this.mOHeight = this.mOWidth * frameFormat.getHeight() / frameFormat.getWidth();
    } 
    mutableFrameFormat.setDimensions(this.mOWidth, this.mOHeight);
    Frame frame2 = paramFilterContext.getFrameManager().newFrame(mutableFrameFormat);
    if (this.mGenerateMipMap) {
      GLFrame gLFrame = (GLFrame)paramFilterContext.getFrameManager().newFrame(frame1.getFormat());
      gLFrame.setTextureParameter(10241, 9985);
      gLFrame.setDataFromFrame(frame1);
      gLFrame.generateMipMap();
      this.mProgram.process(gLFrame, frame2);
      gLFrame.release();
    } else {
      this.mProgram.process(frame1, frame2);
    } 
    pushOutput("image", frame2);
    frame2.release();
  }
}
