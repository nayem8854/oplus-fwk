package android.filterpacks.imageproc;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.FrameFormat;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.core.Program;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;

public class ImageStitcher extends Filter {
  private int mImageHeight;
  
  private int mImageWidth;
  
  private int mInputHeight;
  
  private int mInputWidth;
  
  private Frame mOutputFrame;
  
  @GenerateFieldPort(name = "padSize")
  private int mPadSize;
  
  private Program mProgram;
  
  private int mSliceHeight;
  
  private int mSliceIndex;
  
  private int mSliceWidth;
  
  @GenerateFieldPort(name = "xSlices")
  private int mXSlices;
  
  @GenerateFieldPort(name = "ySlices")
  private int mYSlices;
  
  public ImageStitcher(String paramString) {
    super(paramString);
    this.mSliceIndex = 0;
  }
  
  public void setupPorts() {
    addMaskedInputPort("image", ImageFormat.create(3, 3));
    addOutputBasedOnInput("image", "image");
  }
  
  public FrameFormat getOutputFormat(String paramString, FrameFormat paramFrameFormat) {
    return paramFrameFormat;
  }
  
  private FrameFormat calcOutputFormatForInput(FrameFormat paramFrameFormat) {
    MutableFrameFormat mutableFrameFormat = paramFrameFormat.mutableCopy();
    this.mInputWidth = paramFrameFormat.getWidth();
    int i = paramFrameFormat.getHeight();
    int j = this.mInputWidth, k = this.mPadSize;
    this.mSliceWidth = j -= k * 2;
    this.mSliceHeight = k = i - k * 2;
    this.mImageWidth = i = j * this.mXSlices;
    this.mImageHeight = j = k * this.mYSlices;
    mutableFrameFormat.setDimensions(i, j);
    return mutableFrameFormat;
  }
  
  public void process(FilterContext paramFilterContext) {
    Frame frame = pullInput("image");
    FrameFormat frameFormat = frame.getFormat();
    if (this.mSliceIndex == 0) {
      this.mOutputFrame = paramFilterContext.getFrameManager().newFrame(calcOutputFormatForInput(frameFormat));
    } else if (frameFormat.getWidth() != this.mInputWidth || 
      frameFormat.getHeight() != this.mInputHeight) {
      throw new RuntimeException("Image size should not change.");
    } 
    if (this.mProgram == null)
      this.mProgram = ShaderProgram.createIdentity(paramFilterContext); 
    int i = this.mPadSize;
    float f1 = i / this.mInputWidth;
    float f2 = i / this.mInputHeight;
    int j = this.mSliceIndex, k = this.mXSlices, m = this.mSliceWidth;
    i = j % k * m;
    k = j / k * this.mSliceHeight;
    float f3 = Math.min(m, this.mImageWidth - i);
    float f4 = Math.min(this.mSliceHeight, this.mImageHeight - k);
    ((ShaderProgram)this.mProgram).setSourceRect(f1, f2, f3 / this.mInputWidth, f4 / this.mInputHeight);
    ShaderProgram shaderProgram = (ShaderProgram)this.mProgram;
    f1 = i;
    i = this.mImageWidth;
    f2 = f1 / i;
    f1 = k;
    m = this.mImageHeight;
    shaderProgram.setTargetRect(f2, f1 / m, f3 / i, f4 / m);
    this.mProgram.process(frame, this.mOutputFrame);
    this.mSliceIndex = i = this.mSliceIndex + 1;
    if (i == this.mXSlices * this.mYSlices) {
      pushOutput("image", this.mOutputFrame);
      this.mOutputFrame.release();
      this.mSliceIndex = 0;
    } 
  }
}
