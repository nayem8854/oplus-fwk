package android.filterpacks.videosink;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterContext;
import android.filterfw.core.Frame;
import android.filterfw.core.GLEnvironment;
import android.filterfw.core.GLFrame;
import android.filterfw.core.GenerateFieldPort;
import android.filterfw.core.MutableFrameFormat;
import android.filterfw.core.ShaderProgram;
import android.filterfw.format.ImageFormat;
import android.filterfw.geometry.Point;
import android.filterfw.geometry.Quad;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.IOException;

public class MediaEncoderFilter extends Filter {
  @GenerateFieldPort(hasDefault = true, name = "recording")
  private boolean mRecording = true;
  
  @GenerateFieldPort(hasDefault = true, name = "outputFile")
  private String mOutputFile = new String("/sdcard/MediaEncoderOut.mp4");
  
  @GenerateFieldPort(hasDefault = true, name = "outputFileDescriptor")
  private FileDescriptor mFd = null;
  
  @GenerateFieldPort(hasDefault = true, name = "audioSource")
  private int mAudioSource = -1;
  
  @GenerateFieldPort(hasDefault = true, name = "infoListener")
  private MediaRecorder.OnInfoListener mInfoListener = null;
  
  @GenerateFieldPort(hasDefault = true, name = "errorListener")
  private MediaRecorder.OnErrorListener mErrorListener = null;
  
  @GenerateFieldPort(hasDefault = true, name = "recordingDoneListener")
  private OnRecordingDoneListener mRecordingDoneListener = null;
  
  @GenerateFieldPort(hasDefault = true, name = "orientationHint")
  private int mOrientationHint = 0;
  
  @GenerateFieldPort(hasDefault = true, name = "recordingProfile")
  private CamcorderProfile mProfile = null;
  
  @GenerateFieldPort(hasDefault = true, name = "width")
  private int mWidth = 0;
  
  @GenerateFieldPort(hasDefault = true, name = "height")
  private int mHeight = 0;
  
  @GenerateFieldPort(hasDefault = true, name = "framerate")
  private int mFps = 30;
  
  @GenerateFieldPort(hasDefault = true, name = "outputFormat")
  private int mOutputFormat = 2;
  
  @GenerateFieldPort(hasDefault = true, name = "videoEncoder")
  private int mVideoEncoder = 2;
  
  @GenerateFieldPort(hasDefault = true, name = "maxFileSize")
  private long mMaxFileSize = 0L;
  
  @GenerateFieldPort(hasDefault = true, name = "maxDurationMs")
  private int mMaxDurationMs = 0;
  
  @GenerateFieldPort(hasDefault = true, name = "timelapseRecordingIntervalUs")
  private long mTimeBetweenTimeLapseFrameCaptureUs = 0L;
  
  private boolean mRecordingActive = false;
  
  private long mTimestampNs = 0L;
  
  private long mLastTimeLapseFrameRealTimestampNs = 0L;
  
  private int mNumFramesEncoded = 0;
  
  private boolean mCaptureTimeLapse = false;
  
  private static final int NO_AUDIO_SOURCE = -1;
  
  private static final String TAG = "MediaEncoderFilter";
  
  private boolean mLogVerbose;
  
  private MediaRecorder mMediaRecorder;
  
  private ShaderProgram mProgram;
  
  private GLFrame mScreen;
  
  @GenerateFieldPort(hasDefault = true, name = "inputRegion")
  private Quad mSourceRegion;
  
  private int mSurfaceId;
  
  public MediaEncoderFilter(String paramString) {
    super(paramString);
    Point point2 = new Point(0.0F, 0.0F);
    Point point3 = new Point(1.0F, 0.0F);
    Point point1 = new Point(0.0F, 1.0F);
    Point point4 = new Point(1.0F, 1.0F);
    this.mSourceRegion = new Quad(point2, point3, point1, point4);
    this.mLogVerbose = Log.isLoggable("MediaEncoderFilter", 2);
  }
  
  public void setupPorts() {
    addMaskedInputPort("videoframe", ImageFormat.create(3, 3));
  }
  
  public void fieldPortValueUpdated(String paramString, FilterContext paramFilterContext) {
    if (this.mLogVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Port ");
      stringBuilder.append(paramString);
      stringBuilder.append(" has been updated");
      Log.v("MediaEncoderFilter", stringBuilder.toString());
    } 
    if (paramString.equals("recording"))
      return; 
    if (paramString.equals("inputRegion")) {
      if (isOpen())
        updateSourceRegion(); 
      return;
    } 
    if (!isOpen() || !this.mRecordingActive)
      return; 
    throw new RuntimeException("Cannot change recording parameters when the filter is recording!");
  }
  
  private void updateSourceRegion() {
    Quad quad = new Quad();
    quad.p0 = this.mSourceRegion.p2;
    quad.p1 = this.mSourceRegion.p3;
    quad.p2 = this.mSourceRegion.p0;
    quad.p3 = this.mSourceRegion.p1;
    this.mProgram.setSourceRegion(quad);
  }
  
  private void updateMediaRecorderParams() {
    boolean bool;
    if (this.mTimeBetweenTimeLapseFrameCaptureUs > 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mCaptureTimeLapse = bool;
    this.mMediaRecorder.setVideoSource(2);
    if (!this.mCaptureTimeLapse) {
      int i = this.mAudioSource;
      if (i != -1)
        this.mMediaRecorder.setAudioSource(i); 
    } 
    CamcorderProfile camcorderProfile = this.mProfile;
    if (camcorderProfile != null) {
      this.mMediaRecorder.setProfile(camcorderProfile);
      this.mFps = this.mProfile.videoFrameRate;
      int i = this.mWidth;
      if (i > 0) {
        int j = this.mHeight;
        if (j > 0)
          this.mMediaRecorder.setVideoSize(i, j); 
      } 
    } else {
      this.mMediaRecorder.setOutputFormat(this.mOutputFormat);
      this.mMediaRecorder.setVideoEncoder(this.mVideoEncoder);
      this.mMediaRecorder.setVideoSize(this.mWidth, this.mHeight);
      this.mMediaRecorder.setVideoFrameRate(this.mFps);
    } 
    this.mMediaRecorder.setOrientationHint(this.mOrientationHint);
    this.mMediaRecorder.setOnInfoListener(this.mInfoListener);
    this.mMediaRecorder.setOnErrorListener(this.mErrorListener);
    FileDescriptor fileDescriptor = this.mFd;
    if (fileDescriptor != null) {
      this.mMediaRecorder.setOutputFile(fileDescriptor);
    } else {
      this.mMediaRecorder.setOutputFile(this.mOutputFile);
    } 
    try {
      this.mMediaRecorder.setMaxFileSize(this.mMaxFileSize);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Setting maxFileSize on MediaRecorder unsuccessful! ");
      stringBuilder.append(exception.getMessage());
      String str = stringBuilder.toString();
      Log.w("MediaEncoderFilter", str);
    } 
    this.mMediaRecorder.setMaxDuration(this.mMaxDurationMs);
  }
  
  public void prepare(FilterContext paramFilterContext) {
    if (this.mLogVerbose)
      Log.v("MediaEncoderFilter", "Preparing"); 
    this.mProgram = ShaderProgram.createIdentity(paramFilterContext);
    this.mRecordingActive = false;
  }
  
  public void open(FilterContext paramFilterContext) {
    if (this.mLogVerbose)
      Log.v("MediaEncoderFilter", "Opening"); 
    updateSourceRegion();
    if (this.mRecording)
      startRecording(paramFilterContext); 
  }
  
  private void startRecording(FilterContext paramFilterContext) {
    int i, j;
    if (this.mLogVerbose)
      Log.v("MediaEncoderFilter", "Starting recording"); 
    MutableFrameFormat mutableFrameFormat = new MutableFrameFormat(2, 3);
    mutableFrameFormat.setBytesPerSample(4);
    if (this.mWidth > 0 && this.mHeight > 0) {
      i = 1;
    } else {
      i = 0;
    } 
    CamcorderProfile camcorderProfile = this.mProfile;
    if (camcorderProfile != null && !i) {
      j = camcorderProfile.videoFrameWidth;
      i = this.mProfile.videoFrameHeight;
    } else {
      j = this.mWidth;
      i = this.mHeight;
    } 
    mutableFrameFormat.setDimensions(j, i);
    this.mScreen = (GLFrame)paramFilterContext.getFrameManager().newBoundFrame(mutableFrameFormat, 101, 0L);
    this.mMediaRecorder = new MediaRecorder();
    updateMediaRecorderParams();
    try {
      this.mMediaRecorder.prepare();
      this.mMediaRecorder.start();
      if (this.mLogVerbose)
        Log.v("MediaEncoderFilter", "Open: registering surface from Mediarecorder"); 
      GLEnvironment gLEnvironment = paramFilterContext.getGLEnvironment();
      MediaRecorder mediaRecorder = this.mMediaRecorder;
      this.mSurfaceId = gLEnvironment.registerSurfaceFromMediaRecorder(mediaRecorder);
      this.mNumFramesEncoded = 0;
      this.mRecordingActive = true;
      return;
    } catch (IllegalStateException illegalStateException) {
      throw illegalStateException;
    } catch (IOException iOException) {
      throw new RuntimeException("IOException inMediaRecorder.prepare()!", iOException);
    } catch (Exception exception) {
      throw new RuntimeException("Unknown Exception inMediaRecorder.prepare()!", exception);
    } 
  }
  
  public boolean skipFrameAndModifyTimestamp(long paramLong) {
    int i = this.mNumFramesEncoded;
    if (i == 0) {
      this.mLastTimeLapseFrameRealTimestampNs = paramLong;
      this.mTimestampNs = paramLong;
      if (this.mLogVerbose) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("timelapse: FIRST frame, last real t= ");
        stringBuilder.append(this.mLastTimeLapseFrameRealTimestampNs);
        stringBuilder.append(", setting t = ");
        stringBuilder.append(this.mTimestampNs);
        Log.v("MediaEncoderFilter", stringBuilder.toString());
      } 
      return false;
    } 
    if (i >= 2 && paramLong < this.mLastTimeLapseFrameRealTimestampNs + this.mTimeBetweenTimeLapseFrameCaptureUs * 1000L) {
      if (this.mLogVerbose)
        Log.v("MediaEncoderFilter", "timelapse: skipping intermediate frame"); 
      return true;
    } 
    if (this.mLogVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("timelapse: encoding frame, Timestamp t = ");
      stringBuilder.append(paramLong);
      stringBuilder.append(", last real t= ");
      stringBuilder.append(this.mLastTimeLapseFrameRealTimestampNs);
      stringBuilder.append(", interval = ");
      stringBuilder.append(this.mTimeBetweenTimeLapseFrameCaptureUs);
      Log.v("MediaEncoderFilter", stringBuilder.toString());
    } 
    this.mLastTimeLapseFrameRealTimestampNs = paramLong;
    this.mTimestampNs += 1000000000L / this.mFps;
    if (this.mLogVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("timelapse: encoding frame, setting t = ");
      stringBuilder.append(this.mTimestampNs);
      stringBuilder.append(", delta t = ");
      stringBuilder.append(1000000000L / this.mFps);
      stringBuilder.append(", fps = ");
      stringBuilder.append(this.mFps);
      Log.v("MediaEncoderFilter", stringBuilder.toString());
    } 
    return false;
  }
  
  public void process(FilterContext paramFilterContext) {
    GLEnvironment gLEnvironment = paramFilterContext.getGLEnvironment();
    Frame frame = pullInput("videoframe");
    if (!this.mRecordingActive && this.mRecording)
      startRecording(paramFilterContext); 
    if (this.mRecordingActive && !this.mRecording)
      stopRecording(paramFilterContext); 
    if (!this.mRecordingActive)
      return; 
    if (this.mCaptureTimeLapse) {
      if (skipFrameAndModifyTimestamp(frame.getTimestamp()))
        return; 
    } else {
      this.mTimestampNs = frame.getTimestamp();
    } 
    gLEnvironment.activateSurfaceWithId(this.mSurfaceId);
    this.mProgram.process(frame, this.mScreen);
    gLEnvironment.setSurfaceTimestamp(this.mTimestampNs);
    gLEnvironment.swapBuffers();
    this.mNumFramesEncoded++;
  }
  
  private void stopRecording(FilterContext paramFilterContext) {
    if (this.mLogVerbose)
      Log.v("MediaEncoderFilter", "Stopping recording"); 
    this.mRecordingActive = false;
    this.mNumFramesEncoded = 0;
    GLEnvironment gLEnvironment = paramFilterContext.getGLEnvironment();
    if (this.mLogVerbose)
      Log.v("MediaEncoderFilter", String.format("Unregistering surface %d", new Object[] { Integer.valueOf(this.mSurfaceId) })); 
    gLEnvironment.unregisterSurfaceId(this.mSurfaceId);
    try {
      this.mMediaRecorder.stop();
      this.mMediaRecorder.release();
      this.mMediaRecorder = null;
      this.mScreen.release();
      this.mScreen = null;
      OnRecordingDoneListener onRecordingDoneListener = this.mRecordingDoneListener;
      if (onRecordingDoneListener != null)
        onRecordingDoneListener.onRecordingDone(); 
      return;
    } catch (RuntimeException runtimeException) {
      throw new MediaRecorderStopException("MediaRecorder.stop() failed!", runtimeException);
    } 
  }
  
  public void close(FilterContext paramFilterContext) {
    if (this.mLogVerbose)
      Log.v("MediaEncoderFilter", "Closing"); 
    if (this.mRecordingActive)
      stopRecording(paramFilterContext); 
  }
  
  public void tearDown(FilterContext paramFilterContext) {
    MediaRecorder mediaRecorder = this.mMediaRecorder;
    if (mediaRecorder != null)
      mediaRecorder.release(); 
    GLFrame gLFrame = this.mScreen;
    if (gLFrame != null)
      gLFrame.release(); 
  }
  
  class OnRecordingDoneListener {
    public abstract void onRecordingDone();
  }
}
