package android.print;

import android.content.Context;
import android.content.pm.ParceledListSlice;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.ArrayMap;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class PrinterDiscoverySession {
  private static final String LOG_TAG = "PrinterDiscoverySession";
  
  private static final int MSG_PRINTERS_ADDED = 1;
  
  private static final int MSG_PRINTERS_REMOVED = 2;
  
  private final Handler mHandler;
  
  private boolean mIsPrinterDiscoveryStarted;
  
  private OnPrintersChangeListener mListener;
  
  private IPrinterDiscoveryObserver mObserver;
  
  private final IPrintManager mPrintManager;
  
  private final LinkedHashMap<PrinterId, PrinterInfo> mPrinters = new LinkedHashMap<>();
  
  private final int mUserId;
  
  PrinterDiscoverySession(IPrintManager paramIPrintManager, Context paramContext, int paramInt) {
    this.mPrintManager = paramIPrintManager;
    this.mUserId = paramInt;
    this.mHandler = new SessionHandler(paramContext.getMainLooper());
    PrinterDiscoveryObserver printerDiscoveryObserver = new PrinterDiscoveryObserver(this);
    try {
      this.mPrintManager.createPrinterDiscoverySession(printerDiscoveryObserver, this.mUserId);
    } catch (RemoteException remoteException) {
      Log.e("PrinterDiscoverySession", "Error creating printer discovery session", (Throwable)remoteException);
    } 
  }
  
  public final void startPrinterDiscovery(List<PrinterId> paramList) {
    if (isDestroyed()) {
      Log.w("PrinterDiscoverySession", "Ignoring start printers discovery - session destroyed");
      return;
    } 
    if (!this.mIsPrinterDiscoveryStarted) {
      this.mIsPrinterDiscoveryStarted = true;
      try {
        this.mPrintManager.startPrinterDiscovery(this.mObserver, paramList, this.mUserId);
      } catch (RemoteException remoteException) {
        Log.e("PrinterDiscoverySession", "Error starting printer discovery", (Throwable)remoteException);
      } 
    } 
  }
  
  public final void stopPrinterDiscovery() {
    if (isDestroyed()) {
      Log.w("PrinterDiscoverySession", "Ignoring stop printers discovery - session destroyed");
      return;
    } 
    if (this.mIsPrinterDiscoveryStarted) {
      this.mIsPrinterDiscoveryStarted = false;
      try {
        this.mPrintManager.stopPrinterDiscovery(this.mObserver, this.mUserId);
      } catch (RemoteException remoteException) {
        Log.e("PrinterDiscoverySession", "Error stopping printer discovery", (Throwable)remoteException);
      } 
    } 
  }
  
  public final void startPrinterStateTracking(PrinterId paramPrinterId) {
    if (isDestroyed()) {
      Log.w("PrinterDiscoverySession", "Ignoring start printer state tracking - session destroyed");
      return;
    } 
    try {
      this.mPrintManager.startPrinterStateTracking(paramPrinterId, this.mUserId);
    } catch (RemoteException remoteException) {
      Log.e("PrinterDiscoverySession", "Error starting printer state tracking", (Throwable)remoteException);
    } 
  }
  
  public final void stopPrinterStateTracking(PrinterId paramPrinterId) {
    if (isDestroyed()) {
      Log.w("PrinterDiscoverySession", "Ignoring stop printer state tracking - session destroyed");
      return;
    } 
    try {
      this.mPrintManager.stopPrinterStateTracking(paramPrinterId, this.mUserId);
    } catch (RemoteException remoteException) {
      Log.e("PrinterDiscoverySession", "Error stopping printer state tracking", (Throwable)remoteException);
    } 
  }
  
  public final void validatePrinters(List<PrinterId> paramList) {
    if (isDestroyed()) {
      Log.w("PrinterDiscoverySession", "Ignoring validate printers - session destroyed");
      return;
    } 
    try {
      this.mPrintManager.validatePrinters(paramList, this.mUserId);
    } catch (RemoteException remoteException) {
      Log.e("PrinterDiscoverySession", "Error validating printers", (Throwable)remoteException);
    } 
  }
  
  public final void destroy() {
    if (isDestroyed())
      Log.w("PrinterDiscoverySession", "Ignoring destroy - session destroyed"); 
    destroyNoCheck();
  }
  
  public final List<PrinterInfo> getPrinters() {
    if (isDestroyed()) {
      Log.w("PrinterDiscoverySession", "Ignoring get printers - session destroyed");
      return Collections.emptyList();
    } 
    return new ArrayList<>(this.mPrinters.values());
  }
  
  public final boolean isDestroyed() {
    throwIfNotCalledOnMainThread();
    return isDestroyedNoCheck();
  }
  
  public final boolean isPrinterDiscoveryStarted() {
    throwIfNotCalledOnMainThread();
    return this.mIsPrinterDiscoveryStarted;
  }
  
  public final void setOnPrintersChangeListener(OnPrintersChangeListener paramOnPrintersChangeListener) {
    throwIfNotCalledOnMainThread();
    this.mListener = paramOnPrintersChangeListener;
  }
  
  protected final void finalize() throws Throwable {
    if (!isDestroyedNoCheck()) {
      Log.e("PrinterDiscoverySession", "Destroying leaked printer discovery session");
      destroyNoCheck();
    } 
    super.finalize();
  }
  
  private boolean isDestroyedNoCheck() {
    boolean bool;
    if (this.mObserver == null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void destroyNoCheck() {
    stopPrinterDiscovery();
    try {
      this.mPrintManager.destroyPrinterDiscoverySession(this.mObserver, this.mUserId);
      this.mObserver = null;
      this.mPrinters.clear();
    } catch (RemoteException remoteException) {
      Log.e("PrinterDiscoverySession", "Error destroying printer discovery session", (Throwable)remoteException);
      this.mObserver = null;
      this.mPrinters.clear();
    } finally {
      Exception exception;
    } 
  }
  
  private void handlePrintersAdded(List<PrinterInfo> paramList) {
    if (isDestroyed())
      return; 
    if (this.mPrinters.isEmpty()) {
      int j = paramList.size();
      for (byte b1 = 0; b1 < j; b1++) {
        PrinterInfo printerInfo = paramList.get(b1);
        this.mPrinters.put(printerInfo.getId(), printerInfo);
      } 
      notifyOnPrintersChanged();
      return;
    } 
    ArrayMap arrayMap = new ArrayMap();
    int i = paramList.size();
    for (byte b = 0; b < i; b++) {
      PrinterInfo printerInfo = paramList.get(b);
      arrayMap.put(printerInfo.getId(), printerInfo);
    } 
    for (PrinterId printerId : this.mPrinters.keySet()) {
      PrinterInfo printerInfo = (PrinterInfo)arrayMap.remove(printerId);
      if (printerInfo != null)
        this.mPrinters.put(printerId, printerInfo); 
    } 
    this.mPrinters.putAll((Map<? extends PrinterId, ? extends PrinterInfo>)arrayMap);
    notifyOnPrintersChanged();
  }
  
  private void handlePrintersRemoved(List<PrinterId> paramList) {
    if (isDestroyed())
      return; 
    boolean bool = false;
    int i = paramList.size();
    for (byte b = 0; b < i; b++) {
      PrinterId printerId = paramList.get(b);
      if (this.mPrinters.remove(printerId) != null)
        bool = true; 
    } 
    if (bool)
      notifyOnPrintersChanged(); 
  }
  
  private void notifyOnPrintersChanged() {
    OnPrintersChangeListener onPrintersChangeListener = this.mListener;
    if (onPrintersChangeListener != null)
      onPrintersChangeListener.onPrintersChanged(); 
  }
  
  private static void throwIfNotCalledOnMainThread() {
    if (Looper.getMainLooper().isCurrentThread())
      return; 
    throw new IllegalAccessError("must be called from the main thread");
  }
  
  class SessionHandler extends Handler {
    final PrinterDiscoverySession this$0;
    
    public SessionHandler(Looper param1Looper) {
      super(param1Looper, null, false);
    }
    
    public void handleMessage(Message param1Message) {
      List list;
      int i = param1Message.what;
      if (i != 1) {
        if (i == 2) {
          list = (List)param1Message.obj;
          PrinterDiscoverySession.this.handlePrintersRemoved(list);
        } 
      } else {
        list = (List)((Message)list).obj;
        PrinterDiscoverySession.this.handlePrintersAdded(list);
      } 
    }
  }
  
  class PrinterDiscoveryObserver extends IPrinterDiscoveryObserver.Stub {
    private final WeakReference<PrinterDiscoverySession> mWeakSession;
    
    public PrinterDiscoveryObserver(PrinterDiscoverySession this$0) {
      this.mWeakSession = new WeakReference<>(this$0);
    }
    
    public void onPrintersAdded(ParceledListSlice param1ParceledListSlice) {
      PrinterDiscoverySession printerDiscoverySession = this.mWeakSession.get();
      if (printerDiscoverySession != null) {
        Handler handler = printerDiscoverySession.mHandler;
        List list = param1ParceledListSlice.getList();
        Message message = handler.obtainMessage(1, list);
        message.sendToTarget();
      } 
    }
    
    public void onPrintersRemoved(ParceledListSlice param1ParceledListSlice) {
      PrinterDiscoverySession printerDiscoverySession = this.mWeakSession.get();
      if (printerDiscoverySession != null) {
        Handler handler = printerDiscoverySession.mHandler;
        List list = param1ParceledListSlice.getList();
        Message message = handler.obtainMessage(2, list);
        message.sendToTarget();
      } 
    }
  }
  
  public static interface OnPrintersChangeListener {
    void onPrintersChanged();
  }
}
