package android.print;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPrintServicesChangeListener extends IInterface {
  void onPrintServicesChanged() throws RemoteException;
  
  class Default implements IPrintServicesChangeListener {
    public void onPrintServicesChanged() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPrintServicesChangeListener {
    private static final String DESCRIPTOR = "android.print.IPrintServicesChangeListener";
    
    static final int TRANSACTION_onPrintServicesChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.print.IPrintServicesChangeListener");
    }
    
    public static IPrintServicesChangeListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.print.IPrintServicesChangeListener");
      if (iInterface != null && iInterface instanceof IPrintServicesChangeListener)
        return (IPrintServicesChangeListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onPrintServicesChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.print.IPrintServicesChangeListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.print.IPrintServicesChangeListener");
      onPrintServicesChanged();
      return true;
    }
    
    private static class Proxy implements IPrintServicesChangeListener {
      public static IPrintServicesChangeListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.print.IPrintServicesChangeListener";
      }
      
      public void onPrintServicesChanged() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintServicesChangeListener");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IPrintServicesChangeListener.Stub.getDefaultImpl() != null) {
            IPrintServicesChangeListener.Stub.getDefaultImpl().onPrintServicesChanged();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPrintServicesChangeListener param1IPrintServicesChangeListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPrintServicesChangeListener != null) {
          Proxy.sDefaultImpl = param1IPrintServicesChangeListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPrintServicesChangeListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
