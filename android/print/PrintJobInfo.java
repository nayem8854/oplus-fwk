package android.print;

import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;

public final class PrintJobInfo implements Parcelable {
  public PrintJobInfo() {
    this.mProgress = -1.0F;
  }
  
  public PrintJobInfo(PrintJobInfo paramPrintJobInfo) {
    this.mId = paramPrintJobInfo.mId;
    this.mLabel = paramPrintJobInfo.mLabel;
    this.mPrinterId = paramPrintJobInfo.mPrinterId;
    this.mPrinterName = paramPrintJobInfo.mPrinterName;
    this.mState = paramPrintJobInfo.mState;
    this.mAppId = paramPrintJobInfo.mAppId;
    this.mTag = paramPrintJobInfo.mTag;
    this.mCreationTime = paramPrintJobInfo.mCreationTime;
    this.mCopies = paramPrintJobInfo.mCopies;
    this.mPageRanges = paramPrintJobInfo.mPageRanges;
    this.mAttributes = paramPrintJobInfo.mAttributes;
    this.mDocumentInfo = paramPrintJobInfo.mDocumentInfo;
    this.mProgress = paramPrintJobInfo.mProgress;
    this.mStatus = paramPrintJobInfo.mStatus;
    this.mStatusRes = paramPrintJobInfo.mStatusRes;
    this.mStatusResAppPackageName = paramPrintJobInfo.mStatusResAppPackageName;
    this.mCanceling = paramPrintJobInfo.mCanceling;
    this.mAdvancedOptions = paramPrintJobInfo.mAdvancedOptions;
  }
  
  private PrintJobInfo(Parcel paramParcel) {
    boolean bool;
    this.mId = paramParcel.<PrintJobId>readParcelable(null);
    this.mLabel = paramParcel.readString();
    this.mPrinterId = paramParcel.<PrinterId>readParcelable(null);
    this.mPrinterName = paramParcel.readString();
    this.mState = paramParcel.readInt();
    this.mAppId = paramParcel.readInt();
    this.mTag = paramParcel.readString();
    this.mCreationTime = paramParcel.readLong();
    this.mCopies = paramParcel.readInt();
    Parcelable[] arrayOfParcelable = paramParcel.readParcelableArray(null);
    if (arrayOfParcelable != null) {
      this.mPageRanges = new PageRange[arrayOfParcelable.length];
      for (byte b = 0; b < arrayOfParcelable.length; b++)
        this.mPageRanges[b] = (PageRange)arrayOfParcelable[b]; 
    } 
    this.mAttributes = paramParcel.<PrintAttributes>readParcelable(null);
    this.mDocumentInfo = paramParcel.<PrintDocumentInfo>readParcelable(null);
    this.mProgress = paramParcel.readFloat();
    this.mStatus = paramParcel.readCharSequence();
    this.mStatusRes = paramParcel.readInt();
    this.mStatusResAppPackageName = paramParcel.readCharSequence();
    if (paramParcel.readInt() == 1) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mCanceling = bool;
    Bundle bundle = paramParcel.readBundle();
    if (bundle != null)
      Preconditions.checkArgument(bundle.containsKey(null) ^ true); 
  }
  
  public PrintJobId getId() {
    return this.mId;
  }
  
  public void setId(PrintJobId paramPrintJobId) {
    this.mId = paramPrintJobId;
  }
  
  public String getLabel() {
    return this.mLabel;
  }
  
  public void setLabel(String paramString) {
    this.mLabel = paramString;
  }
  
  public PrinterId getPrinterId() {
    return this.mPrinterId;
  }
  
  public void setPrinterId(PrinterId paramPrinterId) {
    this.mPrinterId = paramPrinterId;
  }
  
  public String getPrinterName() {
    return this.mPrinterName;
  }
  
  public void setPrinterName(String paramString) {
    this.mPrinterName = paramString;
  }
  
  public int getState() {
    return this.mState;
  }
  
  public void setState(int paramInt) {
    this.mState = paramInt;
  }
  
  public void setProgress(float paramFloat) {
    Preconditions.checkArgumentInRange(paramFloat, 0.0F, 1.0F, "progress");
    this.mProgress = paramFloat;
  }
  
  public void setStatus(CharSequence paramCharSequence) {
    this.mStatusRes = 0;
    this.mStatusResAppPackageName = null;
    this.mStatus = paramCharSequence;
  }
  
  public void setStatus(int paramInt, CharSequence paramCharSequence) {
    this.mStatus = null;
    this.mStatusRes = paramInt;
    this.mStatusResAppPackageName = paramCharSequence;
  }
  
  public int getAppId() {
    return this.mAppId;
  }
  
  public void setAppId(int paramInt) {
    this.mAppId = paramInt;
  }
  
  public String getTag() {
    return this.mTag;
  }
  
  public void setTag(String paramString) {
    this.mTag = paramString;
  }
  
  public long getCreationTime() {
    return this.mCreationTime;
  }
  
  public void setCreationTime(long paramLong) {
    if (paramLong >= 0L) {
      this.mCreationTime = paramLong;
      return;
    } 
    throw new IllegalArgumentException("creationTime must be non-negative.");
  }
  
  public int getCopies() {
    return this.mCopies;
  }
  
  public void setCopies(int paramInt) {
    if (paramInt >= 1) {
      this.mCopies = paramInt;
      return;
    } 
    throw new IllegalArgumentException("Copies must be more than one.");
  }
  
  public PageRange[] getPages() {
    return this.mPageRanges;
  }
  
  public void setPages(PageRange[] paramArrayOfPageRange) {
    this.mPageRanges = paramArrayOfPageRange;
  }
  
  public PrintAttributes getAttributes() {
    return this.mAttributes;
  }
  
  public void setAttributes(PrintAttributes paramPrintAttributes) {
    this.mAttributes = paramPrintAttributes;
  }
  
  public PrintDocumentInfo getDocumentInfo() {
    return this.mDocumentInfo;
  }
  
  public void setDocumentInfo(PrintDocumentInfo paramPrintDocumentInfo) {
    this.mDocumentInfo = paramPrintDocumentInfo;
  }
  
  public boolean isCancelling() {
    return this.mCanceling;
  }
  
  public void setCancelling(boolean paramBoolean) {
    this.mCanceling = paramBoolean;
  }
  
  public boolean shouldStayAwake() {
    if (!this.mCanceling) {
      int i = this.mState;
      return (i == 3 || i == 2);
    } 
    return true;
  }
  
  public boolean hasAdvancedOption(String paramString) {
    boolean bool;
    Bundle bundle = this.mAdvancedOptions;
    if (bundle != null && bundle.containsKey(paramString)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String getAdvancedStringOption(String paramString) {
    Bundle bundle = this.mAdvancedOptions;
    if (bundle != null)
      return bundle.getString(paramString); 
    return null;
  }
  
  public int getAdvancedIntOption(String paramString) {
    Bundle bundle = this.mAdvancedOptions;
    if (bundle != null)
      return bundle.getInt(paramString); 
    return 0;
  }
  
  public Bundle getAdvancedOptions() {
    return this.mAdvancedOptions;
  }
  
  public void setAdvancedOptions(Bundle paramBundle) {
    this.mAdvancedOptions = paramBundle;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.mId, paramInt);
    paramParcel.writeString(this.mLabel);
    paramParcel.writeParcelable(this.mPrinterId, paramInt);
    paramParcel.writeString(this.mPrinterName);
    paramParcel.writeInt(this.mState);
    paramParcel.writeInt(this.mAppId);
    paramParcel.writeString(this.mTag);
    paramParcel.writeLong(this.mCreationTime);
    paramParcel.writeInt(this.mCopies);
    paramParcel.writeParcelableArray(this.mPageRanges, paramInt);
    paramParcel.writeParcelable(this.mAttributes, paramInt);
    paramParcel.writeParcelable(this.mDocumentInfo, 0);
    paramParcel.writeFloat(this.mProgress);
    paramParcel.writeCharSequence(this.mStatus);
    paramParcel.writeInt(this.mStatusRes);
    paramParcel.writeCharSequence(this.mStatusResAppPackageName);
    paramParcel.writeInt(this.mCanceling);
    paramParcel.writeBundle(this.mAdvancedOptions);
  }
  
  public String toString() {
    boolean bool;
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("PrintJobInfo{");
    stringBuilder1.append("label: ");
    stringBuilder1.append(this.mLabel);
    stringBuilder1.append(", id: ");
    stringBuilder1.append(this.mId);
    stringBuilder1.append(", state: ");
    stringBuilder1.append(stateToString(this.mState));
    StringBuilder stringBuilder4 = new StringBuilder();
    stringBuilder4.append(", printer: ");
    stringBuilder4.append(this.mPrinterId);
    stringBuilder1.append(stringBuilder4.toString());
    stringBuilder1.append(", tag: ");
    stringBuilder1.append(this.mTag);
    stringBuilder4 = new StringBuilder();
    stringBuilder4.append(", creationTime: ");
    stringBuilder4.append(this.mCreationTime);
    stringBuilder1.append(stringBuilder4.toString());
    stringBuilder1.append(", copies: ");
    stringBuilder1.append(this.mCopies);
    StringBuilder stringBuilder5 = new StringBuilder();
    stringBuilder5.append(", attributes: ");
    PrintAttributes printAttributes = this.mAttributes;
    CharSequence charSequence2 = null;
    if (printAttributes != null) {
      String str = printAttributes.toString();
    } else {
      printAttributes = null;
    } 
    stringBuilder5.append((String)printAttributes);
    String str3 = stringBuilder5.toString();
    stringBuilder1.append(str3);
    stringBuilder5 = new StringBuilder();
    stringBuilder5.append(", documentInfo: ");
    PrintDocumentInfo printDocumentInfo = this.mDocumentInfo;
    if (printDocumentInfo != null) {
      String str = printDocumentInfo.toString();
    } else {
      printDocumentInfo = null;
    } 
    stringBuilder5.append((String)printDocumentInfo);
    String str2 = stringBuilder5.toString();
    stringBuilder1.append(str2);
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append(", cancelling: ");
    stringBuilder3.append(this.mCanceling);
    stringBuilder1.append(stringBuilder3.toString());
    stringBuilder5 = new StringBuilder();
    stringBuilder5.append(", pages: ");
    PageRange[] arrayOfPageRange = this.mPageRanges;
    if (arrayOfPageRange != null) {
      String str = Arrays.toString((Object[])arrayOfPageRange);
    } else {
      arrayOfPageRange = null;
    } 
    stringBuilder5.append((String)arrayOfPageRange);
    String str1 = stringBuilder5.toString();
    stringBuilder1.append(str1);
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", hasAdvancedOptions: ");
    if (this.mAdvancedOptions != null) {
      bool = true;
    } else {
      bool = false;
    } 
    stringBuilder2.append(bool);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", progress: ");
    stringBuilder2.append(this.mProgress);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder5 = new StringBuilder();
    stringBuilder5.append(", status: ");
    CharSequence charSequence1 = this.mStatus;
    if (charSequence1 != null) {
      charSequence1 = charSequence1.toString();
    } else {
      charSequence1 = null;
    } 
    stringBuilder5.append((String)charSequence1);
    charSequence1 = stringBuilder5.toString();
    stringBuilder1.append((String)charSequence1);
    charSequence1 = new StringBuilder();
    charSequence1.append(", statusRes: ");
    charSequence1.append(this.mStatusRes);
    stringBuilder1.append(charSequence1.toString());
    stringBuilder5 = new StringBuilder();
    stringBuilder5.append(", statusResAppPackageName: ");
    charSequence1 = this.mStatusResAppPackageName;
    if (charSequence1 != null) {
      charSequence1 = charSequence1.toString();
    } else {
      charSequence1 = charSequence2;
    } 
    stringBuilder5.append((String)charSequence1);
    charSequence1 = stringBuilder5.toString();
    stringBuilder1.append((String)charSequence1);
    stringBuilder1.append("}");
    return stringBuilder1.toString();
  }
  
  public static String stateToString(int paramInt) {
    switch (paramInt) {
      default:
        return "STATE_UNKNOWN";
      case 7:
        return "STATE_CANCELED";
      case 6:
        return "STATE_FAILED";
      case 5:
        return "STATE_COMPLETED";
      case 4:
        return "STATE_BLOCKED";
      case 3:
        return "STATE_STARTED";
      case 2:
        return "STATE_QUEUED";
      case 1:
        break;
    } 
    return "STATE_CREATED";
  }
  
  public float getProgress() {
    return this.mProgress;
  }
  
  public CharSequence getStatus(PackageManager paramPackageManager) {
    if (this.mStatusRes == 0)
      return this.mStatus; 
    try {
      Resources resources = paramPackageManager.getResourcesForApplication(this.mStatusResAppPackageName.toString());
      int i = this.mStatusRes;
      return 
        resources.getString(i);
    } catch (android.content.pm.PackageManager.NameNotFoundException|android.content.res.Resources.NotFoundException nameNotFoundException) {
      return null;
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class State implements Annotation {}
  
  class Builder {
    private final PrintJobInfo mPrototype;
    
    public Builder(PrintJobInfo this$0) {
      if (this$0 != null) {
        this$0 = new PrintJobInfo(this$0);
      } else {
        this$0 = new PrintJobInfo();
      } 
      this.mPrototype = this$0;
    }
    
    public void setCopies(int param1Int) {
      PrintJobInfo.access$002(this.mPrototype, param1Int);
    }
    
    public void setAttributes(PrintAttributes param1PrintAttributes) {
      PrintJobInfo.access$102(this.mPrototype, param1PrintAttributes);
    }
    
    public void setPages(PageRange[] param1ArrayOfPageRange) {
      PrintJobInfo.access$202(this.mPrototype, param1ArrayOfPageRange);
    }
    
    public void setProgress(float param1Float) {
      Preconditions.checkArgumentInRange(param1Float, 0.0F, 1.0F, "progress");
      PrintJobInfo.access$302(this.mPrototype, param1Float);
    }
    
    public void setStatus(CharSequence param1CharSequence) {
      PrintJobInfo.access$402(this.mPrototype, param1CharSequence);
    }
    
    public void putAdvancedOption(String param1String1, String param1String2) {
      Preconditions.checkNotNull(param1String1, "key cannot be null");
      if (this.mPrototype.mAdvancedOptions == null)
        PrintJobInfo.access$502(this.mPrototype, new Bundle()); 
      this.mPrototype.mAdvancedOptions.putString(param1String1, param1String2);
    }
    
    public void putAdvancedOption(String param1String, int param1Int) {
      if (this.mPrototype.mAdvancedOptions == null)
        PrintJobInfo.access$502(this.mPrototype, new Bundle()); 
      this.mPrototype.mAdvancedOptions.putInt(param1String, param1Int);
    }
    
    public PrintJobInfo build() {
      return this.mPrototype;
    }
  }
  
  public static final Parcelable.Creator<PrintJobInfo> CREATOR = new Parcelable.Creator<PrintJobInfo>() {
      public PrintJobInfo createFromParcel(Parcel param1Parcel) {
        return new PrintJobInfo(param1Parcel);
      }
      
      public PrintJobInfo[] newArray(int param1Int) {
        return new PrintJobInfo[param1Int];
      }
    };
  
  public static final int STATE_ANY = -1;
  
  public static final int STATE_ANY_ACTIVE = -3;
  
  public static final int STATE_ANY_SCHEDULED = -4;
  
  public static final int STATE_ANY_VISIBLE_TO_CLIENTS = -2;
  
  public static final int STATE_BLOCKED = 4;
  
  public static final int STATE_CANCELED = 7;
  
  public static final int STATE_COMPLETED = 5;
  
  public static final int STATE_CREATED = 1;
  
  public static final int STATE_FAILED = 6;
  
  public static final int STATE_QUEUED = 2;
  
  public static final int STATE_STARTED = 3;
  
  private Bundle mAdvancedOptions;
  
  private int mAppId;
  
  private PrintAttributes mAttributes;
  
  private boolean mCanceling;
  
  private int mCopies;
  
  private long mCreationTime;
  
  private PrintDocumentInfo mDocumentInfo;
  
  private PrintJobId mId;
  
  private String mLabel;
  
  private PageRange[] mPageRanges;
  
  private PrinterId mPrinterId;
  
  private String mPrinterName;
  
  private float mProgress;
  
  private int mState;
  
  private CharSequence mStatus;
  
  private int mStatusRes;
  
  private CharSequence mStatusResAppPackageName;
  
  private String mTag;
}
