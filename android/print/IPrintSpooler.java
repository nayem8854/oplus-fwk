package android.print;

import android.content.ComponentName;
import android.graphics.drawable.Icon;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

public interface IPrintSpooler extends IInterface {
  void clearCustomPrinterIconCache(IPrintSpoolerCallbacks paramIPrintSpoolerCallbacks, int paramInt) throws RemoteException;
  
  void createPrintJob(PrintJobInfo paramPrintJobInfo) throws RemoteException;
  
  void getCustomPrinterIcon(PrinterId paramPrinterId, IPrintSpoolerCallbacks paramIPrintSpoolerCallbacks, int paramInt) throws RemoteException;
  
  void getPrintJobInfo(PrintJobId paramPrintJobId, IPrintSpoolerCallbacks paramIPrintSpoolerCallbacks, int paramInt1, int paramInt2) throws RemoteException;
  
  void getPrintJobInfos(IPrintSpoolerCallbacks paramIPrintSpoolerCallbacks, ComponentName paramComponentName, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void onCustomPrinterIconLoaded(PrinterId paramPrinterId, Icon paramIcon, IPrintSpoolerCallbacks paramIPrintSpoolerCallbacks, int paramInt) throws RemoteException;
  
  void pruneApprovedPrintServices(List<ComponentName> paramList) throws RemoteException;
  
  void removeObsoletePrintJobs() throws RemoteException;
  
  void setClient(IPrintSpoolerClient paramIPrintSpoolerClient) throws RemoteException;
  
  void setPrintJobCancelling(PrintJobId paramPrintJobId, boolean paramBoolean) throws RemoteException;
  
  void setPrintJobState(PrintJobId paramPrintJobId, int paramInt1, String paramString, IPrintSpoolerCallbacks paramIPrintSpoolerCallbacks, int paramInt2) throws RemoteException;
  
  void setPrintJobTag(PrintJobId paramPrintJobId, String paramString, IPrintSpoolerCallbacks paramIPrintSpoolerCallbacks, int paramInt) throws RemoteException;
  
  void setProgress(PrintJobId paramPrintJobId, float paramFloat) throws RemoteException;
  
  void setStatus(PrintJobId paramPrintJobId, CharSequence paramCharSequence) throws RemoteException;
  
  void setStatusRes(PrintJobId paramPrintJobId, int paramInt, CharSequence paramCharSequence) throws RemoteException;
  
  void writePrintJobData(ParcelFileDescriptor paramParcelFileDescriptor, PrintJobId paramPrintJobId) throws RemoteException;
  
  class Default implements IPrintSpooler {
    public void removeObsoletePrintJobs() throws RemoteException {}
    
    public void getPrintJobInfos(IPrintSpoolerCallbacks param1IPrintSpoolerCallbacks, ComponentName param1ComponentName, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void getPrintJobInfo(PrintJobId param1PrintJobId, IPrintSpoolerCallbacks param1IPrintSpoolerCallbacks, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void createPrintJob(PrintJobInfo param1PrintJobInfo) throws RemoteException {}
    
    public void setPrintJobState(PrintJobId param1PrintJobId, int param1Int1, String param1String, IPrintSpoolerCallbacks param1IPrintSpoolerCallbacks, int param1Int2) throws RemoteException {}
    
    public void setProgress(PrintJobId param1PrintJobId, float param1Float) throws RemoteException {}
    
    public void setStatus(PrintJobId param1PrintJobId, CharSequence param1CharSequence) throws RemoteException {}
    
    public void setStatusRes(PrintJobId param1PrintJobId, int param1Int, CharSequence param1CharSequence) throws RemoteException {}
    
    public void onCustomPrinterIconLoaded(PrinterId param1PrinterId, Icon param1Icon, IPrintSpoolerCallbacks param1IPrintSpoolerCallbacks, int param1Int) throws RemoteException {}
    
    public void getCustomPrinterIcon(PrinterId param1PrinterId, IPrintSpoolerCallbacks param1IPrintSpoolerCallbacks, int param1Int) throws RemoteException {}
    
    public void clearCustomPrinterIconCache(IPrintSpoolerCallbacks param1IPrintSpoolerCallbacks, int param1Int) throws RemoteException {}
    
    public void setPrintJobTag(PrintJobId param1PrintJobId, String param1String, IPrintSpoolerCallbacks param1IPrintSpoolerCallbacks, int param1Int) throws RemoteException {}
    
    public void writePrintJobData(ParcelFileDescriptor param1ParcelFileDescriptor, PrintJobId param1PrintJobId) throws RemoteException {}
    
    public void setClient(IPrintSpoolerClient param1IPrintSpoolerClient) throws RemoteException {}
    
    public void setPrintJobCancelling(PrintJobId param1PrintJobId, boolean param1Boolean) throws RemoteException {}
    
    public void pruneApprovedPrintServices(List<ComponentName> param1List) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPrintSpooler {
    private static final String DESCRIPTOR = "android.print.IPrintSpooler";
    
    static final int TRANSACTION_clearCustomPrinterIconCache = 11;
    
    static final int TRANSACTION_createPrintJob = 4;
    
    static final int TRANSACTION_getCustomPrinterIcon = 10;
    
    static final int TRANSACTION_getPrintJobInfo = 3;
    
    static final int TRANSACTION_getPrintJobInfos = 2;
    
    static final int TRANSACTION_onCustomPrinterIconLoaded = 9;
    
    static final int TRANSACTION_pruneApprovedPrintServices = 16;
    
    static final int TRANSACTION_removeObsoletePrintJobs = 1;
    
    static final int TRANSACTION_setClient = 14;
    
    static final int TRANSACTION_setPrintJobCancelling = 15;
    
    static final int TRANSACTION_setPrintJobState = 5;
    
    static final int TRANSACTION_setPrintJobTag = 12;
    
    static final int TRANSACTION_setProgress = 6;
    
    static final int TRANSACTION_setStatus = 7;
    
    static final int TRANSACTION_setStatusRes = 8;
    
    static final int TRANSACTION_writePrintJobData = 13;
    
    public Stub() {
      attachInterface(this, "android.print.IPrintSpooler");
    }
    
    public static IPrintSpooler asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.print.IPrintSpooler");
      if (iInterface != null && iInterface instanceof IPrintSpooler)
        return (IPrintSpooler)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 16:
          return "pruneApprovedPrintServices";
        case 15:
          return "setPrintJobCancelling";
        case 14:
          return "setClient";
        case 13:
          return "writePrintJobData";
        case 12:
          return "setPrintJobTag";
        case 11:
          return "clearCustomPrinterIconCache";
        case 10:
          return "getCustomPrinterIcon";
        case 9:
          return "onCustomPrinterIconLoaded";
        case 8:
          return "setStatusRes";
        case 7:
          return "setStatus";
        case 6:
          return "setProgress";
        case 5:
          return "setPrintJobState";
        case 4:
          return "createPrintJob";
        case 3:
          return "getPrintJobInfo";
        case 2:
          return "getPrintJobInfos";
        case 1:
          break;
      } 
      return "removeObsoletePrintJobs";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IPrintSpoolerCallbacks iPrintSpoolerCallbacks;
      if (param1Int1 != 1598968902) {
        ArrayList<?> arrayList;
        IPrintSpoolerClient iPrintSpoolerClient;
        boolean bool;
        String str1;
        IPrintSpoolerCallbacks iPrintSpoolerCallbacks1, iPrintSpoolerCallbacks3;
        String str2;
        IPrintSpoolerCallbacks iPrintSpoolerCallbacks2;
        float f;
        int i;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 16:
            param1Parcel1.enforceInterface("android.print.IPrintSpooler");
            arrayList = param1Parcel1.createTypedArrayList(ComponentName.CREATOR);
            pruneApprovedPrintServices((List)arrayList);
            return true;
          case 15:
            arrayList.enforceInterface("android.print.IPrintSpooler");
            if (arrayList.readInt() != 0) {
              PrintJobId printJobId = PrintJobId.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              param1Parcel2 = null;
            } 
            if (arrayList.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            setPrintJobCancelling((PrintJobId)param1Parcel2, bool);
            return true;
          case 14:
            arrayList.enforceInterface("android.print.IPrintSpooler");
            iPrintSpoolerClient = IPrintSpoolerClient.Stub.asInterface(arrayList.readStrongBinder());
            setClient(iPrintSpoolerClient);
            return true;
          case 13:
            iPrintSpoolerClient.enforceInterface("android.print.IPrintSpooler");
            if (iPrintSpoolerClient.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)iPrintSpoolerClient);
            } else {
              param1Parcel2 = null;
            } 
            if (iPrintSpoolerClient.readInt() != 0) {
              PrintJobId printJobId = PrintJobId.CREATOR.createFromParcel((Parcel)iPrintSpoolerClient);
            } else {
              iPrintSpoolerClient = null;
            } 
            writePrintJobData((ParcelFileDescriptor)param1Parcel2, (PrintJobId)iPrintSpoolerClient);
            return true;
          case 12:
            iPrintSpoolerClient.enforceInterface("android.print.IPrintSpooler");
            if (iPrintSpoolerClient.readInt() != 0) {
              PrintJobId printJobId = PrintJobId.CREATOR.createFromParcel((Parcel)iPrintSpoolerClient);
            } else {
              param1Parcel2 = null;
            } 
            str1 = iPrintSpoolerClient.readString();
            iPrintSpoolerCallbacks3 = IPrintSpoolerCallbacks.Stub.asInterface(iPrintSpoolerClient.readStrongBinder());
            param1Int1 = iPrintSpoolerClient.readInt();
            setPrintJobTag((PrintJobId)param1Parcel2, str1, iPrintSpoolerCallbacks3, param1Int1);
            return true;
          case 11:
            iPrintSpoolerClient.enforceInterface("android.print.IPrintSpooler");
            iPrintSpoolerCallbacks = IPrintSpoolerCallbacks.Stub.asInterface(iPrintSpoolerClient.readStrongBinder());
            param1Int1 = iPrintSpoolerClient.readInt();
            clearCustomPrinterIconCache(iPrintSpoolerCallbacks, param1Int1);
            return true;
          case 10:
            iPrintSpoolerClient.enforceInterface("android.print.IPrintSpooler");
            if (iPrintSpoolerClient.readInt() != 0) {
              PrinterId printerId = PrinterId.CREATOR.createFromParcel((Parcel)iPrintSpoolerClient);
            } else {
              iPrintSpoolerCallbacks = null;
            } 
            iPrintSpoolerCallbacks3 = IPrintSpoolerCallbacks.Stub.asInterface(iPrintSpoolerClient.readStrongBinder());
            param1Int1 = iPrintSpoolerClient.readInt();
            getCustomPrinterIcon((PrinterId)iPrintSpoolerCallbacks, iPrintSpoolerCallbacks3, param1Int1);
            return true;
          case 9:
            iPrintSpoolerClient.enforceInterface("android.print.IPrintSpooler");
            if (iPrintSpoolerClient.readInt() != 0) {
              PrinterId printerId = PrinterId.CREATOR.createFromParcel((Parcel)iPrintSpoolerClient);
            } else {
              iPrintSpoolerCallbacks = null;
            } 
            if (iPrintSpoolerClient.readInt() != 0) {
              Icon icon = Icon.CREATOR.createFromParcel((Parcel)iPrintSpoolerClient);
            } else {
              iPrintSpoolerCallbacks3 = null;
            } 
            iPrintSpoolerCallbacks1 = IPrintSpoolerCallbacks.Stub.asInterface(iPrintSpoolerClient.readStrongBinder());
            param1Int1 = iPrintSpoolerClient.readInt();
            onCustomPrinterIconLoaded((PrinterId)iPrintSpoolerCallbacks, (Icon)iPrintSpoolerCallbacks3, iPrintSpoolerCallbacks1, param1Int1);
            return true;
          case 8:
            iPrintSpoolerClient.enforceInterface("android.print.IPrintSpooler");
            if (iPrintSpoolerClient.readInt() != 0) {
              PrintJobId printJobId = PrintJobId.CREATOR.createFromParcel((Parcel)iPrintSpoolerClient);
            } else {
              iPrintSpoolerCallbacks = null;
            } 
            param1Int1 = iPrintSpoolerClient.readInt();
            if (iPrintSpoolerClient.readInt() != 0) {
              CharSequence charSequence = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)iPrintSpoolerClient);
            } else {
              iPrintSpoolerClient = null;
            } 
            setStatusRes((PrintJobId)iPrintSpoolerCallbacks, param1Int1, (CharSequence)iPrintSpoolerClient);
            return true;
          case 7:
            iPrintSpoolerClient.enforceInterface("android.print.IPrintSpooler");
            if (iPrintSpoolerClient.readInt() != 0) {
              PrintJobId printJobId = PrintJobId.CREATOR.createFromParcel((Parcel)iPrintSpoolerClient);
            } else {
              iPrintSpoolerCallbacks = null;
            } 
            if (iPrintSpoolerClient.readInt() != 0) {
              CharSequence charSequence = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)iPrintSpoolerClient);
            } else {
              iPrintSpoolerClient = null;
            } 
            setStatus((PrintJobId)iPrintSpoolerCallbacks, (CharSequence)iPrintSpoolerClient);
            return true;
          case 6:
            iPrintSpoolerClient.enforceInterface("android.print.IPrintSpooler");
            if (iPrintSpoolerClient.readInt() != 0) {
              PrintJobId printJobId = PrintJobId.CREATOR.createFromParcel((Parcel)iPrintSpoolerClient);
            } else {
              iPrintSpoolerCallbacks = null;
            } 
            f = iPrintSpoolerClient.readFloat();
            setProgress((PrintJobId)iPrintSpoolerCallbacks, f);
            return true;
          case 5:
            iPrintSpoolerClient.enforceInterface("android.print.IPrintSpooler");
            if (iPrintSpoolerClient.readInt() != 0) {
              PrintJobId printJobId = PrintJobId.CREATOR.createFromParcel((Parcel)iPrintSpoolerClient);
            } else {
              iPrintSpoolerCallbacks = null;
            } 
            param1Int1 = iPrintSpoolerClient.readInt();
            str2 = iPrintSpoolerClient.readString();
            iPrintSpoolerCallbacks1 = IPrintSpoolerCallbacks.Stub.asInterface(iPrintSpoolerClient.readStrongBinder());
            param1Int2 = iPrintSpoolerClient.readInt();
            setPrintJobState((PrintJobId)iPrintSpoolerCallbacks, param1Int1, str2, iPrintSpoolerCallbacks1, param1Int2);
            return true;
          case 4:
            iPrintSpoolerClient.enforceInterface("android.print.IPrintSpooler");
            if (iPrintSpoolerClient.readInt() != 0) {
              PrintJobInfo printJobInfo = PrintJobInfo.CREATOR.createFromParcel((Parcel)iPrintSpoolerClient);
            } else {
              iPrintSpoolerClient = null;
            } 
            createPrintJob((PrintJobInfo)iPrintSpoolerClient);
            return true;
          case 3:
            iPrintSpoolerClient.enforceInterface("android.print.IPrintSpooler");
            if (iPrintSpoolerClient.readInt() != 0) {
              PrintJobId printJobId = PrintJobId.CREATOR.createFromParcel((Parcel)iPrintSpoolerClient);
            } else {
              iPrintSpoolerCallbacks = null;
            } 
            iPrintSpoolerCallbacks2 = IPrintSpoolerCallbacks.Stub.asInterface(iPrintSpoolerClient.readStrongBinder());
            param1Int1 = iPrintSpoolerClient.readInt();
            param1Int2 = iPrintSpoolerClient.readInt();
            getPrintJobInfo((PrintJobId)iPrintSpoolerCallbacks, iPrintSpoolerCallbacks2, param1Int1, param1Int2);
            return true;
          case 2:
            iPrintSpoolerClient.enforceInterface("android.print.IPrintSpooler");
            iPrintSpoolerCallbacks2 = IPrintSpoolerCallbacks.Stub.asInterface(iPrintSpoolerClient.readStrongBinder());
            if (iPrintSpoolerClient.readInt() != 0) {
              ComponentName componentName = ComponentName.CREATOR.createFromParcel((Parcel)iPrintSpoolerClient);
            } else {
              iPrintSpoolerCallbacks = null;
            } 
            param1Int1 = iPrintSpoolerClient.readInt();
            param1Int2 = iPrintSpoolerClient.readInt();
            i = iPrintSpoolerClient.readInt();
            getPrintJobInfos(iPrintSpoolerCallbacks2, (ComponentName)iPrintSpoolerCallbacks, param1Int1, param1Int2, i);
            return true;
          case 1:
            break;
        } 
        iPrintSpoolerClient.enforceInterface("android.print.IPrintSpooler");
        removeObsoletePrintJobs();
        return true;
      } 
      iPrintSpoolerCallbacks.writeString("android.print.IPrintSpooler");
      return true;
    }
    
    private static class Proxy implements IPrintSpooler {
      public static IPrintSpooler sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.print.IPrintSpooler";
      }
      
      public void removeObsoletePrintJobs() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintSpooler");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpooler.Stub.getDefaultImpl() != null) {
            IPrintSpooler.Stub.getDefaultImpl().removeObsoletePrintJobs();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getPrintJobInfos(IPrintSpoolerCallbacks param2IPrintSpoolerCallbacks, ComponentName param2ComponentName, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.print.IPrintSpooler");
          if (param2IPrintSpoolerCallbacks != null) {
            iBinder = param2IPrintSpoolerCallbacks.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ComponentName != null) {
            parcel.writeInt(1);
            param2ComponentName.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpooler.Stub.getDefaultImpl() != null) {
            IPrintSpooler.Stub.getDefaultImpl().getPrintJobInfos(param2IPrintSpoolerCallbacks, param2ComponentName, param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getPrintJobInfo(PrintJobId param2PrintJobId, IPrintSpoolerCallbacks param2IPrintSpoolerCallbacks, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.print.IPrintSpooler");
          if (param2PrintJobId != null) {
            parcel.writeInt(1);
            param2PrintJobId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IPrintSpoolerCallbacks != null) {
            iBinder = param2IPrintSpoolerCallbacks.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpooler.Stub.getDefaultImpl() != null) {
            IPrintSpooler.Stub.getDefaultImpl().getPrintJobInfo(param2PrintJobId, param2IPrintSpoolerCallbacks, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void createPrintJob(PrintJobInfo param2PrintJobInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintSpooler");
          if (param2PrintJobInfo != null) {
            parcel.writeInt(1);
            param2PrintJobInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpooler.Stub.getDefaultImpl() != null) {
            IPrintSpooler.Stub.getDefaultImpl().createPrintJob(param2PrintJobInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setPrintJobState(PrintJobId param2PrintJobId, int param2Int1, String param2String, IPrintSpoolerCallbacks param2IPrintSpoolerCallbacks, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.print.IPrintSpooler");
          if (param2PrintJobId != null) {
            parcel.writeInt(1);
            param2PrintJobId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int1);
          parcel.writeString(param2String);
          if (param2IPrintSpoolerCallbacks != null) {
            iBinder = param2IPrintSpoolerCallbacks.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpooler.Stub.getDefaultImpl() != null) {
            IPrintSpooler.Stub.getDefaultImpl().setPrintJobState(param2PrintJobId, param2Int1, param2String, param2IPrintSpoolerCallbacks, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setProgress(PrintJobId param2PrintJobId, float param2Float) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintSpooler");
          if (param2PrintJobId != null) {
            parcel.writeInt(1);
            param2PrintJobId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpooler.Stub.getDefaultImpl() != null) {
            IPrintSpooler.Stub.getDefaultImpl().setProgress(param2PrintJobId, param2Float);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setStatus(PrintJobId param2PrintJobId, CharSequence param2CharSequence) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintSpooler");
          if (param2PrintJobId != null) {
            parcel.writeInt(1);
            param2PrintJobId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2CharSequence != null) {
            parcel.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpooler.Stub.getDefaultImpl() != null) {
            IPrintSpooler.Stub.getDefaultImpl().setStatus(param2PrintJobId, param2CharSequence);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setStatusRes(PrintJobId param2PrintJobId, int param2Int, CharSequence param2CharSequence) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintSpooler");
          if (param2PrintJobId != null) {
            parcel.writeInt(1);
            param2PrintJobId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          if (param2CharSequence != null) {
            parcel.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpooler.Stub.getDefaultImpl() != null) {
            IPrintSpooler.Stub.getDefaultImpl().setStatusRes(param2PrintJobId, param2Int, param2CharSequence);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCustomPrinterIconLoaded(PrinterId param2PrinterId, Icon param2Icon, IPrintSpoolerCallbacks param2IPrintSpoolerCallbacks, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.print.IPrintSpooler");
          if (param2PrinterId != null) {
            parcel.writeInt(1);
            param2PrinterId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Icon != null) {
            parcel.writeInt(1);
            param2Icon.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IPrintSpoolerCallbacks != null) {
            iBinder = param2IPrintSpoolerCallbacks.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpooler.Stub.getDefaultImpl() != null) {
            IPrintSpooler.Stub.getDefaultImpl().onCustomPrinterIconLoaded(param2PrinterId, param2Icon, param2IPrintSpoolerCallbacks, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getCustomPrinterIcon(PrinterId param2PrinterId, IPrintSpoolerCallbacks param2IPrintSpoolerCallbacks, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.print.IPrintSpooler");
          if (param2PrinterId != null) {
            parcel.writeInt(1);
            param2PrinterId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IPrintSpoolerCallbacks != null) {
            iBinder = param2IPrintSpoolerCallbacks.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpooler.Stub.getDefaultImpl() != null) {
            IPrintSpooler.Stub.getDefaultImpl().getCustomPrinterIcon(param2PrinterId, param2IPrintSpoolerCallbacks, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void clearCustomPrinterIconCache(IPrintSpoolerCallbacks param2IPrintSpoolerCallbacks, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.print.IPrintSpooler");
          if (param2IPrintSpoolerCallbacks != null) {
            iBinder = param2IPrintSpoolerCallbacks.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(11, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpooler.Stub.getDefaultImpl() != null) {
            IPrintSpooler.Stub.getDefaultImpl().clearCustomPrinterIconCache(param2IPrintSpoolerCallbacks, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setPrintJobTag(PrintJobId param2PrintJobId, String param2String, IPrintSpoolerCallbacks param2IPrintSpoolerCallbacks, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.print.IPrintSpooler");
          if (param2PrintJobId != null) {
            parcel.writeInt(1);
            param2PrintJobId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeString(param2String);
          if (param2IPrintSpoolerCallbacks != null) {
            iBinder = param2IPrintSpoolerCallbacks.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(12, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpooler.Stub.getDefaultImpl() != null) {
            IPrintSpooler.Stub.getDefaultImpl().setPrintJobTag(param2PrintJobId, param2String, param2IPrintSpoolerCallbacks, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void writePrintJobData(ParcelFileDescriptor param2ParcelFileDescriptor, PrintJobId param2PrintJobId) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintSpooler");
          if (param2ParcelFileDescriptor != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2PrintJobId != null) {
            parcel.writeInt(1);
            param2PrintJobId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(13, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpooler.Stub.getDefaultImpl() != null) {
            IPrintSpooler.Stub.getDefaultImpl().writePrintJobData(param2ParcelFileDescriptor, param2PrintJobId);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setClient(IPrintSpoolerClient param2IPrintSpoolerClient) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.print.IPrintSpooler");
          if (param2IPrintSpoolerClient != null) {
            iBinder = param2IPrintSpoolerClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(14, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpooler.Stub.getDefaultImpl() != null) {
            IPrintSpooler.Stub.getDefaultImpl().setClient(param2IPrintSpoolerClient);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setPrintJobCancelling(PrintJobId param2PrintJobId, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintSpooler");
          boolean bool = false;
          if (param2PrintJobId != null) {
            parcel.writeInt(1);
            param2PrintJobId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(15, parcel, (Parcel)null, 1);
          if (!bool1 && IPrintSpooler.Stub.getDefaultImpl() != null) {
            IPrintSpooler.Stub.getDefaultImpl().setPrintJobCancelling(param2PrintJobId, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void pruneApprovedPrintServices(List<ComponentName> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintSpooler");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(16, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpooler.Stub.getDefaultImpl() != null) {
            IPrintSpooler.Stub.getDefaultImpl().pruneApprovedPrintServices(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPrintSpooler param1IPrintSpooler) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPrintSpooler != null) {
          Proxy.sDefaultImpl = param1IPrintSpooler;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPrintSpooler getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
