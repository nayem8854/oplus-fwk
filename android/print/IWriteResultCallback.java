package android.print;

import android.os.Binder;
import android.os.IBinder;
import android.os.ICancellationSignal;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.text.TextUtils;

public interface IWriteResultCallback extends IInterface {
  void onWriteCanceled(int paramInt) throws RemoteException;
  
  void onWriteFailed(CharSequence paramCharSequence, int paramInt) throws RemoteException;
  
  void onWriteFinished(PageRange[] paramArrayOfPageRange, int paramInt) throws RemoteException;
  
  void onWriteStarted(ICancellationSignal paramICancellationSignal, int paramInt) throws RemoteException;
  
  class Default implements IWriteResultCallback {
    public void onWriteStarted(ICancellationSignal param1ICancellationSignal, int param1Int) throws RemoteException {}
    
    public void onWriteFinished(PageRange[] param1ArrayOfPageRange, int param1Int) throws RemoteException {}
    
    public void onWriteFailed(CharSequence param1CharSequence, int param1Int) throws RemoteException {}
    
    public void onWriteCanceled(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWriteResultCallback {
    private static final String DESCRIPTOR = "android.print.IWriteResultCallback";
    
    static final int TRANSACTION_onWriteCanceled = 4;
    
    static final int TRANSACTION_onWriteFailed = 3;
    
    static final int TRANSACTION_onWriteFinished = 2;
    
    static final int TRANSACTION_onWriteStarted = 1;
    
    public Stub() {
      attachInterface(this, "android.print.IWriteResultCallback");
    }
    
    public static IWriteResultCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.print.IWriteResultCallback");
      if (iInterface != null && iInterface instanceof IWriteResultCallback)
        return (IWriteResultCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onWriteCanceled";
          } 
          return "onWriteFailed";
        } 
        return "onWriteFinished";
      } 
      return "onWriteStarted";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.print.IWriteResultCallback");
              return true;
            } 
            param1Parcel1.enforceInterface("android.print.IWriteResultCallback");
            param1Int1 = param1Parcel1.readInt();
            onWriteCanceled(param1Int1);
            return true;
          } 
          param1Parcel1.enforceInterface("android.print.IWriteResultCallback");
          if (param1Parcel1.readInt() != 0) {
            CharSequence charSequence = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel2 = null;
          } 
          param1Int1 = param1Parcel1.readInt();
          onWriteFailed((CharSequence)param1Parcel2, param1Int1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.print.IWriteResultCallback");
        PageRange[] arrayOfPageRange = param1Parcel1.<PageRange>createTypedArray(PageRange.CREATOR);
        param1Int1 = param1Parcel1.readInt();
        onWriteFinished(arrayOfPageRange, param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.print.IWriteResultCallback");
      ICancellationSignal iCancellationSignal = ICancellationSignal.Stub.asInterface(param1Parcel1.readStrongBinder());
      param1Int1 = param1Parcel1.readInt();
      onWriteStarted(iCancellationSignal, param1Int1);
      return true;
    }
    
    private static class Proxy implements IWriteResultCallback {
      public static IWriteResultCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.print.IWriteResultCallback";
      }
      
      public void onWriteStarted(ICancellationSignal param2ICancellationSignal, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.print.IWriteResultCallback");
          if (param2ICancellationSignal != null) {
            iBinder = param2ICancellationSignal.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IWriteResultCallback.Stub.getDefaultImpl() != null) {
            IWriteResultCallback.Stub.getDefaultImpl().onWriteStarted(param2ICancellationSignal, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onWriteFinished(PageRange[] param2ArrayOfPageRange, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IWriteResultCallback");
          parcel.writeTypedArray(param2ArrayOfPageRange, 0);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IWriteResultCallback.Stub.getDefaultImpl() != null) {
            IWriteResultCallback.Stub.getDefaultImpl().onWriteFinished(param2ArrayOfPageRange, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onWriteFailed(CharSequence param2CharSequence, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IWriteResultCallback");
          if (param2CharSequence != null) {
            parcel.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IWriteResultCallback.Stub.getDefaultImpl() != null) {
            IWriteResultCallback.Stub.getDefaultImpl().onWriteFailed(param2CharSequence, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onWriteCanceled(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IWriteResultCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IWriteResultCallback.Stub.getDefaultImpl() != null) {
            IWriteResultCallback.Stub.getDefaultImpl().onWriteCanceled(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IWriteResultCallback param1IWriteResultCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWriteResultCallback != null) {
          Proxy.sDefaultImpl = param1IWriteResultCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWriteResultCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
