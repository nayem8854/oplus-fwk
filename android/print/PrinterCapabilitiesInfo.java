package android.print;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.IntConsumer;

public final class PrinterCapabilitiesInfo implements Parcelable {
  public static final Parcelable.Creator<PrinterCapabilitiesInfo> CREATOR;
  
  private static final PrintAttributes.Margins DEFAULT_MARGINS = new PrintAttributes.Margins(0, 0, 0, 0);
  
  public static final int DEFAULT_UNDEFINED = -1;
  
  private static final int PROPERTY_COLOR_MODE = 2;
  
  private static final int PROPERTY_COUNT = 4;
  
  private static final int PROPERTY_DUPLEX_MODE = 3;
  
  private static final int PROPERTY_MEDIA_SIZE = 0;
  
  private static final int PROPERTY_RESOLUTION = 1;
  
  private int mColorModes;
  
  private final int[] mDefaults;
  
  private int mDuplexModes;
  
  private List<PrintAttributes.MediaSize> mMediaSizes;
  
  private PrintAttributes.Margins mMinMargins = DEFAULT_MARGINS;
  
  private List<PrintAttributes.Resolution> mResolutions;
  
  public PrinterCapabilitiesInfo() {
    int[] arrayOfInt = new int[4];
    Arrays.fill(arrayOfInt, -1);
  }
  
  private PrinterCapabilitiesInfo(Parcel paramParcel) {
    this.mDefaults = new int[4];
    this.mMinMargins = (PrintAttributes.Margins)Preconditions.checkNotNull(readMargins(paramParcel));
    readMediaSizes(paramParcel);
    readResolutions(paramParcel);
    int i = paramParcel.readInt();
    enforceValidMask(i, (IntConsumer)_$$Lambda$PrinterCapabilitiesInfo$2mJhwjGC7Dgi0vwDsnG83V2s6sE.INSTANCE);
    this.mDuplexModes = i = paramParcel.readInt();
    enforceValidMask(i, (IntConsumer)_$$Lambda$PrinterCapabilitiesInfo$TL1SYHyXTbqj2Nseol9bDJQOn3U.INSTANCE);
    readDefaults(paramParcel);
    i = this.mMediaSizes.size();
    int[] arrayOfInt = this.mDefaults;
    boolean bool1 = false;
    if (i > arrayOfInt[0]) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2);
    boolean bool2 = bool1;
    if (this.mResolutions.size() > this.mDefaults[1])
      bool2 = true; 
    Preconditions.checkArgument(bool2);
  }
  
  public PrinterCapabilitiesInfo(PrinterCapabilitiesInfo paramPrinterCapabilitiesInfo) {
    this.mDefaults = new int[4];
    copyFrom(paramPrinterCapabilitiesInfo);
  }
  
  public void copyFrom(PrinterCapabilitiesInfo paramPrinterCapabilitiesInfo) {
    if (this == paramPrinterCapabilitiesInfo)
      return; 
    this.mMinMargins = paramPrinterCapabilitiesInfo.mMinMargins;
    if (paramPrinterCapabilitiesInfo.mMediaSizes != null) {
      List<PrintAttributes.MediaSize> list = this.mMediaSizes;
      if (list != null) {
        list.clear();
        this.mMediaSizes.addAll(paramPrinterCapabilitiesInfo.mMediaSizes);
      } else {
        this.mMediaSizes = new ArrayList<>(paramPrinterCapabilitiesInfo.mMediaSizes);
      } 
    } else {
      this.mMediaSizes = null;
    } 
    if (paramPrinterCapabilitiesInfo.mResolutions != null) {
      List<PrintAttributes.Resolution> list = this.mResolutions;
      if (list != null) {
        list.clear();
        this.mResolutions.addAll(paramPrinterCapabilitiesInfo.mResolutions);
      } else {
        this.mResolutions = new ArrayList<>(paramPrinterCapabilitiesInfo.mResolutions);
      } 
    } else {
      this.mResolutions = null;
    } 
    this.mColorModes = paramPrinterCapabilitiesInfo.mColorModes;
    this.mDuplexModes = paramPrinterCapabilitiesInfo.mDuplexModes;
    int i = paramPrinterCapabilitiesInfo.mDefaults.length;
    for (byte b = 0; b < i; b++)
      this.mDefaults[b] = paramPrinterCapabilitiesInfo.mDefaults[b]; 
  }
  
  public List<PrintAttributes.MediaSize> getMediaSizes() {
    return Collections.unmodifiableList(this.mMediaSizes);
  }
  
  public List<PrintAttributes.Resolution> getResolutions() {
    return Collections.unmodifiableList(this.mResolutions);
  }
  
  public PrintAttributes.Margins getMinMargins() {
    return this.mMinMargins;
  }
  
  public int getColorModes() {
    return this.mColorModes;
  }
  
  public int getDuplexModes() {
    return this.mDuplexModes;
  }
  
  public PrintAttributes getDefaults() {
    PrintAttributes.Builder builder = new PrintAttributes.Builder();
    builder.setMinMargins(this.mMinMargins);
    int i = this.mDefaults[0];
    if (i >= 0)
      builder.setMediaSize(this.mMediaSizes.get(i)); 
    i = this.mDefaults[1];
    if (i >= 0)
      builder.setResolution(this.mResolutions.get(i)); 
    i = this.mDefaults[2];
    if (i > 0)
      builder.setColorMode(i); 
    i = this.mDefaults[3];
    if (i > 0)
      builder.setDuplexMode(i); 
    return builder.build();
  }
  
  private static void enforceValidMask(int paramInt, IntConsumer paramIntConsumer) {
    while (paramInt > 0) {
      int i = 1 << Integer.numberOfTrailingZeros(paramInt);
      paramInt &= i ^ 0xFFFFFFFF;
      paramIntConsumer.accept(i);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeMargins(this.mMinMargins, paramParcel);
    writeMediaSizes(paramParcel);
    writeResolutions(paramParcel);
    paramParcel.writeInt(this.mColorModes);
    paramParcel.writeInt(this.mDuplexModes);
    writeDefaults(paramParcel);
  }
  
  public int hashCode() {
    int j, k;
    PrintAttributes.Margins margins = this.mMinMargins;
    int i = 0;
    if (margins == null) {
      j = 0;
    } else {
      j = margins.hashCode();
    } 
    List<PrintAttributes.MediaSize> list1 = this.mMediaSizes;
    if (list1 == null) {
      k = 0;
    } else {
      k = list1.hashCode();
    } 
    List<PrintAttributes.Resolution> list = this.mResolutions;
    if (list != null)
      i = list.hashCode(); 
    int m = this.mColorModes;
    int n = this.mDuplexModes;
    int i1 = Arrays.hashCode(this.mDefaults);
    return (((((1 * 31 + j) * 31 + k) * 31 + i) * 31 + m) * 31 + n) * 31 + i1;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    PrintAttributes.Margins margins = this.mMinMargins;
    if (margins == null) {
      if (((PrinterCapabilitiesInfo)paramObject).mMinMargins != null)
        return false; 
    } else if (!margins.equals(((PrinterCapabilitiesInfo)paramObject).mMinMargins)) {
      return false;
    } 
    List<PrintAttributes.MediaSize> list1 = this.mMediaSizes;
    if (list1 == null) {
      if (((PrinterCapabilitiesInfo)paramObject).mMediaSizes != null)
        return false; 
    } else if (!list1.equals(((PrinterCapabilitiesInfo)paramObject).mMediaSizes)) {
      return false;
    } 
    List<PrintAttributes.Resolution> list = this.mResolutions;
    if (list == null) {
      if (((PrinterCapabilitiesInfo)paramObject).mResolutions != null)
        return false; 
    } else if (!list.equals(((PrinterCapabilitiesInfo)paramObject).mResolutions)) {
      return false;
    } 
    if (this.mColorModes != ((PrinterCapabilitiesInfo)paramObject).mColorModes)
      return false; 
    if (this.mDuplexModes != ((PrinterCapabilitiesInfo)paramObject).mDuplexModes)
      return false; 
    if (!Arrays.equals(this.mDefaults, ((PrinterCapabilitiesInfo)paramObject).mDefaults))
      return false; 
    return true;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("PrinterInfo{");
    stringBuilder.append("minMargins=");
    stringBuilder.append(this.mMinMargins);
    stringBuilder.append(", mediaSizes=");
    stringBuilder.append(this.mMediaSizes);
    stringBuilder.append(", resolutions=");
    stringBuilder.append(this.mResolutions);
    stringBuilder.append(", colorModes=");
    stringBuilder.append(colorModesToString());
    stringBuilder.append(", duplexModes=");
    stringBuilder.append(duplexModesToString());
    stringBuilder.append("\"}");
    return stringBuilder.toString();
  }
  
  private String colorModesToString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append('[');
    int i = this.mColorModes;
    while (i != 0) {
      int j = 1 << Integer.numberOfTrailingZeros(i);
      i &= j ^ 0xFFFFFFFF;
      if (stringBuilder.length() > 1)
        stringBuilder.append(", "); 
      stringBuilder.append(PrintAttributes.colorModeToString(j));
    } 
    stringBuilder.append(']');
    return stringBuilder.toString();
  }
  
  private String duplexModesToString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append('[');
    int i = this.mDuplexModes;
    while (i != 0) {
      int j = 1 << Integer.numberOfTrailingZeros(i);
      i &= j ^ 0xFFFFFFFF;
      if (stringBuilder.length() > 1)
        stringBuilder.append(", "); 
      stringBuilder.append(PrintAttributes.duplexModeToString(j));
    } 
    stringBuilder.append(']');
    return stringBuilder.toString();
  }
  
  private void writeMediaSizes(Parcel paramParcel) {
    List<PrintAttributes.MediaSize> list = this.mMediaSizes;
    if (list == null) {
      paramParcel.writeInt(0);
      return;
    } 
    int i = list.size();
    paramParcel.writeInt(i);
    for (byte b = 0; b < i; b++)
      ((PrintAttributes.MediaSize)this.mMediaSizes.get(b)).writeToParcel(paramParcel); 
  }
  
  private void readMediaSizes(Parcel paramParcel) {
    int i = paramParcel.readInt();
    if (i > 0 && this.mMediaSizes == null)
      this.mMediaSizes = new ArrayList<>(); 
    for (byte b = 0; b < i; b++)
      this.mMediaSizes.add(PrintAttributes.MediaSize.createFromParcel(paramParcel)); 
  }
  
  private void writeResolutions(Parcel paramParcel) {
    List<PrintAttributes.Resolution> list = this.mResolutions;
    if (list == null) {
      paramParcel.writeInt(0);
      return;
    } 
    int i = list.size();
    paramParcel.writeInt(i);
    for (byte b = 0; b < i; b++)
      ((PrintAttributes.Resolution)this.mResolutions.get(b)).writeToParcel(paramParcel); 
  }
  
  private void readResolutions(Parcel paramParcel) {
    int i = paramParcel.readInt();
    if (i > 0 && this.mResolutions == null)
      this.mResolutions = new ArrayList<>(); 
    for (byte b = 0; b < i; b++)
      this.mResolutions.add(PrintAttributes.Resolution.createFromParcel(paramParcel)); 
  }
  
  private void writeMargins(PrintAttributes.Margins paramMargins, Parcel paramParcel) {
    if (paramMargins == null) {
      paramParcel.writeInt(0);
    } else {
      paramParcel.writeInt(1);
      paramMargins.writeToParcel(paramParcel);
    } 
  }
  
  private PrintAttributes.Margins readMargins(Parcel paramParcel) {
    if (paramParcel.readInt() == 1) {
      PrintAttributes.Margins margins = PrintAttributes.Margins.createFromParcel(paramParcel);
    } else {
      paramParcel = null;
    } 
    return (PrintAttributes.Margins)paramParcel;
  }
  
  private void readDefaults(Parcel paramParcel) {
    int i = paramParcel.readInt();
    for (byte b = 0; b < i; b++)
      this.mDefaults[b] = paramParcel.readInt(); 
  }
  
  private void writeDefaults(Parcel paramParcel) {
    int i = this.mDefaults.length;
    paramParcel.writeInt(i);
    for (byte b = 0; b < i; b++)
      paramParcel.writeInt(this.mDefaults[b]); 
  }
  
  class Builder {
    private final PrinterCapabilitiesInfo mPrototype;
    
    public Builder(PrinterCapabilitiesInfo this$0) {
      if (this$0 != null) {
        this.mPrototype = new PrinterCapabilitiesInfo();
        return;
      } 
      throw new IllegalArgumentException("printerId cannot be null.");
    }
    
    public Builder addMediaSize(PrintAttributes.MediaSize param1MediaSize, boolean param1Boolean) {
      if (this.mPrototype.mMediaSizes == null)
        PrinterCapabilitiesInfo.access$002(this.mPrototype, new ArrayList()); 
      int i = this.mPrototype.mMediaSizes.size();
      this.mPrototype.mMediaSizes.add(param1MediaSize);
      if (param1Boolean) {
        throwIfDefaultAlreadySpecified(0);
        this.mPrototype.mDefaults[0] = i;
      } 
      return this;
    }
    
    public Builder addResolution(PrintAttributes.Resolution param1Resolution, boolean param1Boolean) {
      if (this.mPrototype.mResolutions == null)
        PrinterCapabilitiesInfo.access$202(this.mPrototype, new ArrayList()); 
      int i = this.mPrototype.mResolutions.size();
      this.mPrototype.mResolutions.add(param1Resolution);
      if (param1Boolean) {
        throwIfDefaultAlreadySpecified(1);
        this.mPrototype.mDefaults[1] = i;
      } 
      return this;
    }
    
    public Builder setMinMargins(PrintAttributes.Margins param1Margins) {
      if (param1Margins != null) {
        PrinterCapabilitiesInfo.access$302(this.mPrototype, param1Margins);
        return this;
      } 
      throw new IllegalArgumentException("margins cannot be null");
    }
    
    public Builder setColorModes(int param1Int1, int param1Int2) {
      PrinterCapabilitiesInfo.enforceValidMask(param1Int1, (IntConsumer)_$$Lambda$PrinterCapabilitiesInfo$Builder$dbsSt8pZfd6hqZ6hGCnpzhPK6Uk.INSTANCE);
      PrintAttributes.enforceValidColorMode(param1Int2);
      PrinterCapabilitiesInfo.access$502(this.mPrototype, param1Int1);
      this.mPrototype.mDefaults[2] = param1Int2;
      return this;
    }
    
    public Builder setDuplexModes(int param1Int1, int param1Int2) {
      PrinterCapabilitiesInfo.enforceValidMask(param1Int1, (IntConsumer)_$$Lambda$PrinterCapabilitiesInfo$Builder$gsgXbNHGWpWENdPzemgHcCY8HnE.INSTANCE);
      PrintAttributes.enforceValidDuplexMode(param1Int2);
      PrinterCapabilitiesInfo.access$602(this.mPrototype, param1Int1);
      this.mPrototype.mDefaults[3] = param1Int2;
      return this;
    }
    
    public PrinterCapabilitiesInfo build() {
      if (this.mPrototype.mMediaSizes != null && !this.mPrototype.mMediaSizes.isEmpty()) {
        if (this.mPrototype.mDefaults[0] != -1) {
          if (this.mPrototype.mResolutions != null && !this.mPrototype.mResolutions.isEmpty()) {
            if (this.mPrototype.mDefaults[1] != -1) {
              if (this.mPrototype.mColorModes != 0) {
                if (this.mPrototype.mDefaults[2] != -1) {
                  if (this.mPrototype.mDuplexModes == 0)
                    setDuplexModes(1, 1); 
                  if (this.mPrototype.mMinMargins != null)
                    return this.mPrototype; 
                  throw new IllegalArgumentException("margins cannot be null");
                } 
                throw new IllegalStateException("No default color mode specified.");
              } 
              throw new IllegalStateException("No color mode specified.");
            } 
            throw new IllegalStateException("No default resolution specified.");
          } 
          throw new IllegalStateException("No resolution specified.");
        } 
        throw new IllegalStateException("No default media size specified.");
      } 
      throw new IllegalStateException("No media size specified.");
    }
    
    private void throwIfDefaultAlreadySpecified(int param1Int) {
      if (this.mPrototype.mDefaults[param1Int] == -1)
        return; 
      throw new IllegalArgumentException("Default already specified.");
    }
  }
  
  static {
    CREATOR = new Parcelable.Creator<PrinterCapabilitiesInfo>() {
        public PrinterCapabilitiesInfo createFromParcel(Parcel param1Parcel) {
          return new PrinterCapabilitiesInfo(param1Parcel);
        }
        
        public PrinterCapabilitiesInfo[] newArray(int param1Int) {
          return new PrinterCapabilitiesInfo[param1Int];
        }
      };
  }
}
