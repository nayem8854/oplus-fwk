package android.print;

import android.app.PendingIntent;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class PrinterInfo implements Parcelable {
  private PrinterInfo(PrinterId paramPrinterId, String paramString1, int paramInt1, int paramInt2, boolean paramBoolean, String paramString2, PendingIntent paramPendingIntent, PrinterCapabilitiesInfo paramPrinterCapabilitiesInfo, int paramInt3) {
    this.mId = paramPrinterId;
    this.mName = paramString1;
    this.mStatus = paramInt1;
    this.mIconResourceId = paramInt2;
    this.mHasCustomPrinterIcon = paramBoolean;
    this.mDescription = paramString2;
    this.mInfoIntent = paramPendingIntent;
    this.mCapabilities = paramPrinterCapabilitiesInfo;
    this.mCustomPrinterIconGen = paramInt3;
  }
  
  public PrinterId getId() {
    return this.mId;
  }
  
  public Drawable loadIcon(Context paramContext) {
    Drawable drawable2;
    PrintManager printManager1 = null;
    PackageManager packageManager = paramContext.getPackageManager();
    PrintManager printManager2 = printManager1;
    if (this.mHasCustomPrinterIcon) {
      printManager2 = (PrintManager)paramContext.getSystemService("print");
      Icon icon = printManager2.getCustomPrinterIcon(this.mId);
      printManager2 = printManager1;
      if (icon != null)
        drawable2 = icon.loadDrawable(paramContext); 
    } 
    Drawable drawable1 = drawable2;
    if (drawable2 == null) {
      drawable1 = drawable2;
      try {
        String str = this.mId.getServiceName().getPackageName();
        drawable1 = drawable2;
        PackageInfo packageInfo = packageManager.getPackageInfo(str, 0);
        drawable1 = drawable2;
        ApplicationInfo applicationInfo = packageInfo.applicationInfo;
        Drawable drawable = drawable2;
        drawable1 = drawable2;
        if (this.mIconResourceId != 0) {
          drawable1 = drawable2;
          drawable = packageManager.getDrawable(str, this.mIconResourceId, applicationInfo);
        } 
        drawable2 = drawable;
        if (drawable == null) {
          drawable1 = drawable;
          drawable2 = applicationInfo.loadIcon(packageManager);
        } 
        drawable1 = drawable2;
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {}
    } 
    return drawable1;
  }
  
  public boolean getHasCustomPrinterIcon() {
    return this.mHasCustomPrinterIcon;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public int getStatus() {
    return this.mStatus;
  }
  
  public String getDescription() {
    return this.mDescription;
  }
  
  public PendingIntent getInfoIntent() {
    return this.mInfoIntent;
  }
  
  public PrinterCapabilitiesInfo getCapabilities() {
    return this.mCapabilities;
  }
  
  private static PrinterId checkPrinterId(PrinterId paramPrinterId) {
    return (PrinterId)Preconditions.checkNotNull(paramPrinterId, "printerId cannot be null.");
  }
  
  private static int checkStatus(int paramInt) {
    if (paramInt == 1 || paramInt == 2 || paramInt == 3)
      return paramInt; 
    throw new IllegalArgumentException("status is invalid.");
  }
  
  private static String checkName(String paramString) {
    return (String)Preconditions.checkStringNotEmpty(paramString, "name cannot be empty.");
  }
  
  private PrinterInfo(Parcel paramParcel) {
    boolean bool;
    this.mId = checkPrinterId(paramParcel.<PrinterId>readParcelable(null));
    this.mName = checkName(paramParcel.readString());
    this.mStatus = checkStatus(paramParcel.readInt());
    this.mDescription = paramParcel.readString();
    this.mCapabilities = paramParcel.<PrinterCapabilitiesInfo>readParcelable(null);
    this.mIconResourceId = paramParcel.readInt();
    if (paramParcel.readByte() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mHasCustomPrinterIcon = bool;
    this.mCustomPrinterIconGen = paramParcel.readInt();
    this.mInfoIntent = paramParcel.<PendingIntent>readParcelable(null);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.mId, paramInt);
    paramParcel.writeString(this.mName);
    paramParcel.writeInt(this.mStatus);
    paramParcel.writeString(this.mDescription);
    paramParcel.writeParcelable(this.mCapabilities, paramInt);
    paramParcel.writeInt(this.mIconResourceId);
    paramParcel.writeByte((byte)this.mHasCustomPrinterIcon);
    paramParcel.writeInt(this.mCustomPrinterIconGen);
    paramParcel.writeParcelable((Parcelable)this.mInfoIntent, paramInt);
  }
  
  public int hashCode() {
    byte b1, b2;
    int i = this.mId.hashCode();
    int j = this.mName.hashCode();
    int k = this.mStatus;
    String str = this.mDescription;
    int m = 0;
    if (str != null) {
      b1 = str.hashCode();
    } else {
      b1 = 0;
    } 
    PrinterCapabilitiesInfo printerCapabilitiesInfo = this.mCapabilities;
    if (printerCapabilitiesInfo != null) {
      b2 = printerCapabilitiesInfo.hashCode();
    } else {
      b2 = 0;
    } 
    int n = this.mIconResourceId;
    boolean bool = this.mHasCustomPrinterIcon;
    int i1 = this.mCustomPrinterIconGen;
    PendingIntent pendingIntent = this.mInfoIntent;
    if (pendingIntent != null)
      m = pendingIntent.hashCode(); 
    return ((((((((1 * 31 + i) * 31 + j) * 31 + k) * 31 + b1) * 31 + b2) * 31 + n) * 31 + bool) * 31 + i1) * 31 + m;
  }
  
  public boolean equalsIgnoringStatus(PrinterInfo paramPrinterInfo) {
    if (!this.mId.equals(paramPrinterInfo.mId))
      return false; 
    if (!this.mName.equals(paramPrinterInfo.mName))
      return false; 
    if (!TextUtils.equals(this.mDescription, paramPrinterInfo.mDescription))
      return false; 
    PrinterCapabilitiesInfo printerCapabilitiesInfo = this.mCapabilities;
    if (printerCapabilitiesInfo == null) {
      if (paramPrinterInfo.mCapabilities != null)
        return false; 
    } else if (!printerCapabilitiesInfo.equals(paramPrinterInfo.mCapabilities)) {
      return false;
    } 
    if (this.mIconResourceId != paramPrinterInfo.mIconResourceId)
      return false; 
    if (this.mHasCustomPrinterIcon != paramPrinterInfo.mHasCustomPrinterIcon)
      return false; 
    if (this.mCustomPrinterIconGen != paramPrinterInfo.mCustomPrinterIconGen)
      return false; 
    PendingIntent pendingIntent = this.mInfoIntent;
    if (pendingIntent == null) {
      if (paramPrinterInfo.mInfoIntent != null)
        return false; 
    } else if (!pendingIntent.equals(paramPrinterInfo.mInfoIntent)) {
      return false;
    } 
    return true;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (!equalsIgnoringStatus((PrinterInfo)paramObject))
      return false; 
    if (this.mStatus != ((PrinterInfo)paramObject).mStatus)
      return false; 
    return true;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("PrinterInfo{");
    stringBuilder.append("id=");
    stringBuilder.append(this.mId);
    stringBuilder.append(", name=");
    stringBuilder.append(this.mName);
    stringBuilder.append(", status=");
    stringBuilder.append(this.mStatus);
    stringBuilder.append(", description=");
    stringBuilder.append(this.mDescription);
    stringBuilder.append(", capabilities=");
    stringBuilder.append(this.mCapabilities);
    stringBuilder.append(", iconResId=");
    stringBuilder.append(this.mIconResourceId);
    stringBuilder.append(", hasCustomPrinterIcon=");
    stringBuilder.append(this.mHasCustomPrinterIcon);
    stringBuilder.append(", customPrinterIconGen=");
    stringBuilder.append(this.mCustomPrinterIconGen);
    stringBuilder.append(", infoIntent=");
    stringBuilder.append(this.mInfoIntent);
    stringBuilder.append("\"}");
    return stringBuilder.toString();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Status implements Annotation {}
  
  class Builder {
    private PrinterCapabilitiesInfo mCapabilities;
    
    private int mCustomPrinterIconGen;
    
    private String mDescription;
    
    private boolean mHasCustomPrinterIcon;
    
    private int mIconResourceId;
    
    private PendingIntent mInfoIntent;
    
    private String mName;
    
    private PrinterId mPrinterId;
    
    private int mStatus;
    
    public Builder(PrinterInfo this$0, String param1String, int param1Int) {
      this.mPrinterId = PrinterInfo.checkPrinterId((PrinterId)this$0);
      this.mName = PrinterInfo.checkName(param1String);
      this.mStatus = PrinterInfo.checkStatus(param1Int);
    }
    
    public Builder(PrinterInfo this$0) {
      this.mPrinterId = this$0.mId;
      this.mName = this$0.mName;
      this.mStatus = this$0.mStatus;
      this.mIconResourceId = this$0.mIconResourceId;
      this.mHasCustomPrinterIcon = this$0.mHasCustomPrinterIcon;
      this.mDescription = this$0.mDescription;
      this.mInfoIntent = this$0.mInfoIntent;
      this.mCapabilities = this$0.mCapabilities;
      this.mCustomPrinterIconGen = this$0.mCustomPrinterIconGen;
    }
    
    public Builder setStatus(int param1Int) {
      this.mStatus = PrinterInfo.checkStatus(param1Int);
      return this;
    }
    
    public Builder setIconResourceId(int param1Int) {
      this.mIconResourceId = Preconditions.checkArgumentNonnegative(param1Int, "iconResourceId can't be negative");
      return this;
    }
    
    public Builder setHasCustomPrinterIcon(boolean param1Boolean) {
      this.mHasCustomPrinterIcon = param1Boolean;
      return this;
    }
    
    public Builder setName(String param1String) {
      this.mName = PrinterInfo.checkName(param1String);
      return this;
    }
    
    public Builder setDescription(String param1String) {
      this.mDescription = param1String;
      return this;
    }
    
    public Builder setInfoIntent(PendingIntent param1PendingIntent) {
      this.mInfoIntent = param1PendingIntent;
      return this;
    }
    
    public Builder setCapabilities(PrinterCapabilitiesInfo param1PrinterCapabilitiesInfo) {
      this.mCapabilities = param1PrinterCapabilitiesInfo;
      return this;
    }
    
    public PrinterInfo build() {
      return new PrinterInfo(this.mPrinterId, this.mName, this.mStatus, this.mIconResourceId, this.mHasCustomPrinterIcon, this.mDescription, this.mInfoIntent, this.mCapabilities, this.mCustomPrinterIconGen);
    }
    
    public Builder incCustomPrinterIconGen() {
      this.mCustomPrinterIconGen++;
      return this;
    }
  }
  
  public static final Parcelable.Creator<PrinterInfo> CREATOR = new Parcelable.Creator<PrinterInfo>() {
      public PrinterInfo createFromParcel(Parcel param1Parcel) {
        return new PrinterInfo(param1Parcel);
      }
      
      public PrinterInfo[] newArray(int param1Int) {
        return new PrinterInfo[param1Int];
      }
    };
  
  public static final int STATUS_BUSY = 2;
  
  public static final int STATUS_IDLE = 1;
  
  public static final int STATUS_UNAVAILABLE = 3;
  
  private final PrinterCapabilitiesInfo mCapabilities;
  
  private final int mCustomPrinterIconGen;
  
  private final String mDescription;
  
  private final boolean mHasCustomPrinterIcon;
  
  private final int mIconResourceId;
  
  private final PrinterId mId;
  
  private final PendingIntent mInfoIntent;
  
  private final String mName;
  
  private final int mStatus;
}
