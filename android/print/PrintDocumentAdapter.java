package android.print;

import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;

public abstract class PrintDocumentAdapter {
  public static final String EXTRA_PRINT_PREVIEW = "EXTRA_PRINT_PREVIEW";
  
  public abstract void onWrite(PageRange[] paramArrayOfPageRange, ParcelFileDescriptor paramParcelFileDescriptor, CancellationSignal paramCancellationSignal, WriteResultCallback paramWriteResultCallback);
  
  public void onStart() {}
  
  public abstract void onLayout(PrintAttributes paramPrintAttributes1, PrintAttributes paramPrintAttributes2, CancellationSignal paramCancellationSignal, LayoutResultCallback paramLayoutResultCallback, Bundle paramBundle);
  
  public void onFinish() {}
  
  public static abstract class WriteResultCallback {
    public void onWriteFinished(PageRange[] param1ArrayOfPageRange) {}
    
    public void onWriteFailed(CharSequence param1CharSequence) {}
    
    public void onWriteCancelled() {}
  }
  
  public static abstract class LayoutResultCallback {
    public void onLayoutFinished(PrintDocumentInfo param1PrintDocumentInfo, boolean param1Boolean) {}
    
    public void onLayoutFailed(CharSequence param1CharSequence) {}
    
    public void onLayoutCancelled() {}
  }
}
