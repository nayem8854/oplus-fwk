package android.print;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPrintDocumentAdapterObserver extends IInterface {
  void onDestroy() throws RemoteException;
  
  class Default implements IPrintDocumentAdapterObserver {
    public void onDestroy() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPrintDocumentAdapterObserver {
    private static final String DESCRIPTOR = "android.print.IPrintDocumentAdapterObserver";
    
    static final int TRANSACTION_onDestroy = 1;
    
    public Stub() {
      attachInterface(this, "android.print.IPrintDocumentAdapterObserver");
    }
    
    public static IPrintDocumentAdapterObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.print.IPrintDocumentAdapterObserver");
      if (iInterface != null && iInterface instanceof IPrintDocumentAdapterObserver)
        return (IPrintDocumentAdapterObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onDestroy";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.print.IPrintDocumentAdapterObserver");
        return true;
      } 
      param1Parcel1.enforceInterface("android.print.IPrintDocumentAdapterObserver");
      onDestroy();
      return true;
    }
    
    private static class Proxy implements IPrintDocumentAdapterObserver {
      public static IPrintDocumentAdapterObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.print.IPrintDocumentAdapterObserver";
      }
      
      public void onDestroy() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintDocumentAdapterObserver");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IPrintDocumentAdapterObserver.Stub.getDefaultImpl() != null) {
            IPrintDocumentAdapterObserver.Stub.getDefaultImpl().onDestroy();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPrintDocumentAdapterObserver param1IPrintDocumentAdapterObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPrintDocumentAdapterObserver != null) {
          Proxy.sDefaultImpl = param1IPrintDocumentAdapterObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPrintDocumentAdapterObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
