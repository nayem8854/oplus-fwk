package android.print;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.OperationCanceledException;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class PrintFileDocumentAdapter extends PrintDocumentAdapter {
  private static final String LOG_TAG = "PrintedFileDocAdapter";
  
  private final Context mContext;
  
  private final PrintDocumentInfo mDocumentInfo;
  
  private final File mFile;
  
  private WriteFileAsyncTask mWriteFileAsyncTask;
  
  public PrintFileDocumentAdapter(Context paramContext, File paramFile, PrintDocumentInfo paramPrintDocumentInfo) {
    if (paramFile != null) {
      if (paramPrintDocumentInfo != null) {
        this.mContext = paramContext;
        this.mFile = paramFile;
        this.mDocumentInfo = paramPrintDocumentInfo;
        return;
      } 
      throw new IllegalArgumentException("documentInfo cannot be null!");
    } 
    throw new IllegalArgumentException("File cannot be null!");
  }
  
  public void onLayout(PrintAttributes paramPrintAttributes1, PrintAttributes paramPrintAttributes2, CancellationSignal paramCancellationSignal, PrintDocumentAdapter.LayoutResultCallback paramLayoutResultCallback, Bundle paramBundle) {
    paramLayoutResultCallback.onLayoutFinished(this.mDocumentInfo, false);
  }
  
  public void onWrite(PageRange[] paramArrayOfPageRange, ParcelFileDescriptor paramParcelFileDescriptor, CancellationSignal paramCancellationSignal, PrintDocumentAdapter.WriteResultCallback paramWriteResultCallback) {
    WriteFileAsyncTask writeFileAsyncTask = new WriteFileAsyncTask(paramParcelFileDescriptor, paramCancellationSignal, paramWriteResultCallback);
    writeFileAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[])null);
  }
  
  private final class WriteFileAsyncTask extends AsyncTask<Void, Void, Void> {
    private final CancellationSignal mCancellationSignal;
    
    private final ParcelFileDescriptor mDestination;
    
    private final PrintDocumentAdapter.WriteResultCallback mResultCallback;
    
    final PrintFileDocumentAdapter this$0;
    
    public WriteFileAsyncTask(ParcelFileDescriptor param1ParcelFileDescriptor, CancellationSignal param1CancellationSignal, PrintDocumentAdapter.WriteResultCallback param1WriteResultCallback) {
      this.mDestination = param1ParcelFileDescriptor;
      this.mResultCallback = param1WriteResultCallback;
      this.mCancellationSignal = param1CancellationSignal;
      param1CancellationSignal.setOnCancelListener(new CancellationSignal.OnCancelListener() {
            final PrintFileDocumentAdapter.WriteFileAsyncTask this$1;
            
            final PrintFileDocumentAdapter val$this$0;
            
            public void onCancel() {
              PrintFileDocumentAdapter.WriteFileAsyncTask.this.cancel(true);
            }
          });
    }
    
    protected Void doInBackground(Void... param1VarArgs) {
      try {
        FileInputStream fileInputStream = new FileInputStream();
        this(PrintFileDocumentAdapter.this.mFile);
        try {
          FileOutputStream fileOutputStream = new FileOutputStream();
          this(this.mDestination.getFileDescriptor());
        } finally {
          try {
            fileInputStream.close();
          } finally {
            fileInputStream = null;
          } 
        } 
      } catch (OperationCanceledException operationCanceledException) {
      
      } catch (IOException iOException) {
        Log.e("PrintedFileDocAdapter", "Error writing data!", iOException);
        this.mResultCallback.onWriteFailed(PrintFileDocumentAdapter.this.mContext.getString(17041594));
      } 
      return null;
    }
    
    protected void onPostExecute(Void param1Void) {
      this.mResultCallback.onWriteFinished(new PageRange[] { PageRange.ALL_PAGES });
    }
    
    protected void onCancelled(Void param1Void) {
      this.mResultCallback.onWriteFailed(PrintFileDocumentAdapter.this.mContext.getString(17041593));
    }
  }
  
  class null implements CancellationSignal.OnCancelListener {
    final PrintFileDocumentAdapter.WriteFileAsyncTask this$1;
    
    final PrintFileDocumentAdapter val$this$0;
    
    public void onCancel() {
      this.this$1.cancel(true);
    }
  }
}
