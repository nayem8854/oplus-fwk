package android.print;

import android.content.Context;
import android.content.Loader;
import android.os.Handler;
import android.os.Message;
import android.printservice.recommendation.RecommendationInfo;
import com.android.internal.util.Preconditions;
import java.util.List;

public class PrintServiceRecommendationsLoader extends Loader<List<RecommendationInfo>> {
  private final Handler mHandler;
  
  private PrintManager.PrintServiceRecommendationsChangeListener mListener;
  
  private final PrintManager mPrintManager;
  
  public PrintServiceRecommendationsLoader(PrintManager paramPrintManager, Context paramContext) {
    super((Context)Preconditions.checkNotNull(paramContext));
    this.mHandler = new MyHandler();
    this.mPrintManager = (PrintManager)Preconditions.checkNotNull(paramPrintManager);
  }
  
  protected void onForceLoad() {
    queueNewResult();
  }
  
  private void queueNewResult() {
    Message message = this.mHandler.obtainMessage(0);
    message.obj = this.mPrintManager.getPrintServiceRecommendations();
    this.mHandler.sendMessage(message);
  }
  
  protected void onStartLoading() {
    PrintManager.PrintServiceRecommendationsChangeListener printServiceRecommendationsChangeListener = new PrintManager.PrintServiceRecommendationsChangeListener() {
        final PrintServiceRecommendationsLoader this$0;
        
        public void onPrintServiceRecommendationsChanged() {
          PrintServiceRecommendationsLoader.this.queueNewResult();
        }
      };
    this.mPrintManager.addPrintServiceRecommendationsChangeListener(printServiceRecommendationsChangeListener, null);
    deliverResult(this.mPrintManager.getPrintServiceRecommendations());
  }
  
  protected void onStopLoading() {
    PrintManager.PrintServiceRecommendationsChangeListener printServiceRecommendationsChangeListener = this.mListener;
    if (printServiceRecommendationsChangeListener != null) {
      this.mPrintManager.removePrintServiceRecommendationsChangeListener(printServiceRecommendationsChangeListener);
      this.mListener = null;
    } 
    this.mHandler.removeMessages(0);
  }
  
  protected void onReset() {
    onStopLoading();
  }
  
  private class MyHandler extends Handler {
    final PrintServiceRecommendationsLoader this$0;
    
    public MyHandler() {
      super(PrintServiceRecommendationsLoader.this.getContext().getMainLooper());
    }
    
    public void handleMessage(Message param1Message) {
      if (PrintServiceRecommendationsLoader.this.isStarted())
        PrintServiceRecommendationsLoader.this.deliverResult(param1Message.obj); 
    }
  }
}
