package android.print;

import android.annotation.SystemApi;
import android.app.Activity;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.IntentSender;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.ICancellationSignal;
import android.os.Looper;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.printservice.PrintServiceInfo;
import android.printservice.recommendation.IRecommendationsChangeListener;
import android.printservice.recommendation.RecommendationInfo;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import com.android.internal.os.SomeArgs;
import com.android.internal.util.Preconditions;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import libcore.io.IoUtils;

public final class PrintManager {
  public static final String ACTION_PRINT_DIALOG = "android.print.PRINT_DIALOG";
  
  public static final int ALL_SERVICES = 3;
  
  public static final int APP_ID_ANY = -2;
  
  private static final boolean DEBUG = false;
  
  public static final int DISABLED_SERVICES = 2;
  
  @SystemApi
  public static final int ENABLED_SERVICES = 1;
  
  public static final String EXTRA_PRINT_DIALOG_INTENT = "android.print.intent.extra.EXTRA_PRINT_DIALOG_INTENT";
  
  public static final String EXTRA_PRINT_DOCUMENT_ADAPTER = "android.print.intent.extra.EXTRA_PRINT_DOCUMENT_ADAPTER";
  
  public static final String EXTRA_PRINT_JOB = "android.print.intent.extra.EXTRA_PRINT_JOB";
  
  private static final String LOG_TAG = "PrintManager";
  
  private static final int MSG_NOTIFY_PRINT_JOB_STATE_CHANGED = 1;
  
  public static final String PRINT_SPOOLER_PACKAGE_NAME = "com.android.printspooler";
  
  private final int mAppId;
  
  private final Context mContext;
  
  private final Handler mHandler;
  
  private Map<PrintJobStateChangeListener, PrintJobStateChangeListenerWrapper> mPrintJobStateChangeListeners;
  
  private Map<PrintServiceRecommendationsChangeListener, PrintServiceRecommendationsChangeListenerWrapper> mPrintServiceRecommendationsChangeListeners;
  
  private Map<PrintServicesChangeListener, PrintServicesChangeListenerWrapper> mPrintServicesChangeListeners;
  
  private final IPrintManager mService;
  
  private final int mUserId;
  
  public PrintManager(Context paramContext, IPrintManager paramIPrintManager, int paramInt1, int paramInt2) {
    this.mContext = paramContext;
    this.mService = paramIPrintManager;
    this.mUserId = paramInt1;
    this.mAppId = paramInt2;
    this.mHandler = (Handler)new Object(this, paramContext.getMainLooper(), null, false);
  }
  
  public PrintManager getGlobalPrintManagerForUser(int paramInt) {
    IPrintManager iPrintManager = this.mService;
    if (iPrintManager == null) {
      Log.w("PrintManager", "Feature android.software.print not available");
      return null;
    } 
    return new PrintManager(this.mContext, iPrintManager, paramInt, -2);
  }
  
  PrintJobInfo getPrintJobInfo(PrintJobId paramPrintJobId) {
    try {
      return this.mService.getPrintJobInfo(paramPrintJobId, this.mAppId, this.mUserId);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void addPrintJobStateChangeListener(PrintJobStateChangeListener paramPrintJobStateChangeListener) {
    if (this.mService == null) {
      Log.w("PrintManager", "Feature android.software.print not available");
      return;
    } 
    if (this.mPrintJobStateChangeListeners == null)
      this.mPrintJobStateChangeListeners = (Map<PrintJobStateChangeListener, PrintJobStateChangeListenerWrapper>)new ArrayMap(); 
    PrintJobStateChangeListenerWrapper printJobStateChangeListenerWrapper = new PrintJobStateChangeListenerWrapper(paramPrintJobStateChangeListener, this.mHandler);
    try {
      this.mService.addPrintJobStateChangeListener(printJobStateChangeListenerWrapper, this.mAppId, this.mUserId);
      this.mPrintJobStateChangeListeners.put(paramPrintJobStateChangeListener, printJobStateChangeListenerWrapper);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void removePrintJobStateChangeListener(PrintJobStateChangeListener paramPrintJobStateChangeListener) {
    if (this.mService == null) {
      Log.w("PrintManager", "Feature android.software.print not available");
      return;
    } 
    Map<PrintJobStateChangeListener, PrintJobStateChangeListenerWrapper> map = this.mPrintJobStateChangeListeners;
    if (map == null)
      return; 
    PrintJobStateChangeListenerWrapper printJobStateChangeListenerWrapper = map.remove(paramPrintJobStateChangeListener);
    if (printJobStateChangeListenerWrapper == null)
      return; 
    if (this.mPrintJobStateChangeListeners.isEmpty())
      this.mPrintJobStateChangeListeners = null; 
    printJobStateChangeListenerWrapper.destroy();
    try {
      this.mService.removePrintJobStateChangeListener(printJobStateChangeListenerWrapper, this.mUserId);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public PrintJob getPrintJob(PrintJobId paramPrintJobId) {
    IPrintManager iPrintManager = this.mService;
    if (iPrintManager == null) {
      Log.w("PrintManager", "Feature android.software.print not available");
      return null;
    } 
    try {
      PrintJobInfo printJobInfo = iPrintManager.getPrintJobInfo(paramPrintJobId, this.mAppId, this.mUserId);
      if (printJobInfo != null)
        return new PrintJob(printJobInfo, this); 
      return null;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Icon getCustomPrinterIcon(PrinterId paramPrinterId) {
    IPrintManager iPrintManager = this.mService;
    if (iPrintManager == null) {
      Log.w("PrintManager", "Feature android.software.print not available");
      return null;
    } 
    try {
      return iPrintManager.getCustomPrinterIcon(paramPrinterId, this.mUserId);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<PrintJob> getPrintJobs() {
    IPrintManager iPrintManager = this.mService;
    if (iPrintManager == null) {
      Log.w("PrintManager", "Feature android.software.print not available");
      return Collections.emptyList();
    } 
    try {
      List<PrintJobInfo> list = iPrintManager.getPrintJobInfos(this.mAppId, this.mUserId);
      if (list == null)
        return Collections.emptyList(); 
      int i = list.size();
      ArrayList<PrintJob> arrayList = new ArrayList();
      this(i);
      for (byte b = 0; b < i; b++) {
        PrintJob printJob = new PrintJob();
        this(list.get(b), this);
        arrayList.add(printJob);
      } 
      return arrayList;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  void cancelPrintJob(PrintJobId paramPrintJobId) {
    IPrintManager iPrintManager = this.mService;
    if (iPrintManager == null) {
      Log.w("PrintManager", "Feature android.software.print not available");
      return;
    } 
    try {
      iPrintManager.cancelPrintJob(paramPrintJobId, this.mAppId, this.mUserId);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  void restartPrintJob(PrintJobId paramPrintJobId) {
    IPrintManager iPrintManager = this.mService;
    if (iPrintManager == null) {
      Log.w("PrintManager", "Feature android.software.print not available");
      return;
    } 
    try {
      iPrintManager.restartPrintJob(paramPrintJobId, this.mAppId, this.mUserId);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public PrintJob print(String paramString, PrintDocumentAdapter paramPrintDocumentAdapter, PrintAttributes paramPrintAttributes) {
    if (this.mService == null) {
      Log.w("PrintManager", "Feature android.software.print not available");
      return null;
    } 
    if (this.mContext instanceof Activity) {
      if (!TextUtils.isEmpty(paramString)) {
        if (paramPrintDocumentAdapter != null) {
          PrintDocumentAdapterDelegate printDocumentAdapterDelegate = new PrintDocumentAdapterDelegate((Activity)this.mContext, paramPrintDocumentAdapter);
          try {
            IPrintManager iPrintManager = this.mService;
            Context context = this.mContext;
            String str = context.getPackageName();
            int i = this.mAppId, j = this.mUserId;
            Bundle bundle = iPrintManager.print(paramString, printDocumentAdapterDelegate, paramPrintAttributes, str, i, j);
            if (bundle != null) {
              PrintJobInfo printJobInfo = bundle.<PrintJobInfo>getParcelable("android.print.intent.extra.EXTRA_PRINT_JOB");
              IntentSender intentSender = bundle.<IntentSender>getParcelable("android.print.intent.extra.EXTRA_PRINT_DIALOG_INTENT");
              if (printJobInfo == null || intentSender == null)
                return null; 
              try {
                this.mContext.startIntentSender(intentSender, null, 0, 0, 0);
                return new PrintJob(printJobInfo, this);
              } catch (android.content.IntentSender.SendIntentException sendIntentException) {
                Log.e("PrintManager", "Couldn't start print job config activity.", (Throwable)sendIntentException);
              } 
            } 
            return null;
          } catch (RemoteException remoteException) {
            throw remoteException.rethrowFromSystemServer();
          } 
        } 
        throw new IllegalArgumentException("documentAdapter cannot be null");
      } 
      throw new IllegalArgumentException("printJobName cannot be empty");
    } 
    throw new IllegalStateException("Can print only from an activity");
  }
  
  @SystemApi
  public void addPrintServicesChangeListener(PrintServicesChangeListener paramPrintServicesChangeListener, Handler paramHandler) {
    Preconditions.checkNotNull(paramPrintServicesChangeListener);
    Handler handler = paramHandler;
    if (paramHandler == null)
      handler = this.mHandler; 
    if (this.mService == null) {
      Log.w("PrintManager", "Feature android.software.print not available");
      return;
    } 
    if (this.mPrintServicesChangeListeners == null)
      this.mPrintServicesChangeListeners = (Map<PrintServicesChangeListener, PrintServicesChangeListenerWrapper>)new ArrayMap(); 
    PrintServicesChangeListenerWrapper printServicesChangeListenerWrapper = new PrintServicesChangeListenerWrapper(paramPrintServicesChangeListener, handler);
    try {
      this.mService.addPrintServicesChangeListener(printServicesChangeListenerWrapper, this.mUserId);
      this.mPrintServicesChangeListeners.put(paramPrintServicesChangeListener, printServicesChangeListenerWrapper);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void removePrintServicesChangeListener(PrintServicesChangeListener paramPrintServicesChangeListener) {
    Preconditions.checkNotNull(paramPrintServicesChangeListener);
    if (this.mService == null) {
      Log.w("PrintManager", "Feature android.software.print not available");
      return;
    } 
    Map<PrintServicesChangeListener, PrintServicesChangeListenerWrapper> map = this.mPrintServicesChangeListeners;
    if (map == null)
      return; 
    PrintServicesChangeListenerWrapper printServicesChangeListenerWrapper = map.remove(paramPrintServicesChangeListener);
    if (printServicesChangeListenerWrapper == null)
      return; 
    if (this.mPrintServicesChangeListeners.isEmpty())
      this.mPrintServicesChangeListeners = null; 
    printServicesChangeListenerWrapper.destroy();
    try {
      this.mService.removePrintServicesChangeListener(printServicesChangeListenerWrapper, this.mUserId);
    } catch (RemoteException remoteException) {
      Log.e("PrintManager", "Error removing print services change listener", (Throwable)remoteException);
    } 
  }
  
  @SystemApi
  public List<PrintServiceInfo> getPrintServices(int paramInt) {
    Preconditions.checkFlagsArgument(paramInt, 3);
    try {
      List<PrintServiceInfo> list = this.mService.getPrintServices(paramInt, this.mUserId);
      if (list != null)
        return list; 
      return Collections.emptyList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void addPrintServiceRecommendationsChangeListener(PrintServiceRecommendationsChangeListener paramPrintServiceRecommendationsChangeListener, Handler paramHandler) {
    Preconditions.checkNotNull(paramPrintServiceRecommendationsChangeListener);
    Handler handler = paramHandler;
    if (paramHandler == null)
      handler = this.mHandler; 
    if (this.mService == null) {
      Log.w("PrintManager", "Feature android.software.print not available");
      return;
    } 
    if (this.mPrintServiceRecommendationsChangeListeners == null)
      this.mPrintServiceRecommendationsChangeListeners = (Map<PrintServiceRecommendationsChangeListener, PrintServiceRecommendationsChangeListenerWrapper>)new ArrayMap(); 
    PrintServiceRecommendationsChangeListenerWrapper printServiceRecommendationsChangeListenerWrapper = new PrintServiceRecommendationsChangeListenerWrapper(paramPrintServiceRecommendationsChangeListener, handler);
    try {
      this.mService.addPrintServiceRecommendationsChangeListener(printServiceRecommendationsChangeListenerWrapper, this.mUserId);
      this.mPrintServiceRecommendationsChangeListeners.put(paramPrintServiceRecommendationsChangeListener, printServiceRecommendationsChangeListenerWrapper);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void removePrintServiceRecommendationsChangeListener(PrintServiceRecommendationsChangeListener paramPrintServiceRecommendationsChangeListener) {
    Preconditions.checkNotNull(paramPrintServiceRecommendationsChangeListener);
    if (this.mService == null) {
      Log.w("PrintManager", "Feature android.software.print not available");
      return;
    } 
    Map<PrintServiceRecommendationsChangeListener, PrintServiceRecommendationsChangeListenerWrapper> map = this.mPrintServiceRecommendationsChangeListeners;
    if (map == null)
      return; 
    PrintServiceRecommendationsChangeListenerWrapper printServiceRecommendationsChangeListenerWrapper = map.remove(paramPrintServiceRecommendationsChangeListener);
    if (printServiceRecommendationsChangeListenerWrapper == null)
      return; 
    if (this.mPrintServiceRecommendationsChangeListeners.isEmpty())
      this.mPrintServiceRecommendationsChangeListeners = null; 
    printServiceRecommendationsChangeListenerWrapper.destroy();
    try {
      this.mService.removePrintServiceRecommendationsChangeListener(printServiceRecommendationsChangeListenerWrapper, this.mUserId);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public List<RecommendationInfo> getPrintServiceRecommendations() {
    try {
      IPrintManager iPrintManager = this.mService;
      int i = this.mUserId;
      List<RecommendationInfo> list = iPrintManager.getPrintServiceRecommendations(i);
      if (list != null)
        return list; 
      return Collections.emptyList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public PrinterDiscoverySession createPrinterDiscoverySession() {
    IPrintManager iPrintManager = this.mService;
    if (iPrintManager == null) {
      Log.w("PrintManager", "Feature android.software.print not available");
      return null;
    } 
    return new PrinterDiscoverySession(iPrintManager, this.mContext, this.mUserId);
  }
  
  public void setPrintServiceEnabled(ComponentName paramComponentName, boolean paramBoolean) {
    IPrintManager iPrintManager = this.mService;
    if (iPrintManager == null) {
      Log.w("PrintManager", "Feature android.software.print not available");
      return;
    } 
    try {
      iPrintManager.setPrintServiceEnabled(paramComponentName, paramBoolean, this.mUserId);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error enabling or disabling ");
      stringBuilder.append(paramComponentName);
      Log.e("PrintManager", stringBuilder.toString(), (Throwable)remoteException);
    } 
  }
  
  class PrintDocumentAdapterDelegate extends IPrintDocumentAdapter.Stub implements Application.ActivityLifecycleCallbacks {
    private Activity mActivity;
    
    private PrintDocumentAdapter mDocumentAdapter;
    
    private Handler mHandler;
    
    private final Object mLock = new Object();
    
    private IPrintDocumentAdapterObserver mObserver;
    
    private DestroyableCallback mPendingCallback;
    
    public PrintDocumentAdapterDelegate(PrintManager this$0, PrintDocumentAdapter param1PrintDocumentAdapter) {
      if (!this$0.isFinishing()) {
        this.mActivity = (Activity)this$0;
        this.mDocumentAdapter = param1PrintDocumentAdapter;
        this.mHandler = new MyHandler(this.mActivity.getMainLooper());
        this.mActivity.getApplication().registerActivityLifecycleCallbacks(this);
        return;
      } 
      throw new IllegalStateException("Cannot start printing for finishing activity");
    }
    
    public void setObserver(IPrintDocumentAdapterObserver param1IPrintDocumentAdapterObserver) {
      synchronized (this.mLock) {
        this.mObserver = param1IPrintDocumentAdapterObserver;
        boolean bool = isDestroyedLocked();
        if (bool && param1IPrintDocumentAdapterObserver != null)
          try {
            param1IPrintDocumentAdapterObserver.onDestroy();
          } catch (RemoteException remoteException) {
            Log.e("PrintManager", "Error announcing destroyed state", (Throwable)remoteException);
          }  
        return;
      } 
    }
    
    public void start() {
      synchronized (this.mLock) {
        if (!isDestroyedLocked()) {
          Message message = this.mHandler.obtainMessage(1, this.mDocumentAdapter);
          message.sendToTarget();
        } 
        return;
      } 
    }
    
    public void layout(PrintAttributes param1PrintAttributes1, PrintAttributes param1PrintAttributes2, ILayoutResultCallback param1ILayoutResultCallback, Bundle param1Bundle, int param1Int) {
      ICancellationSignal iCancellationSignal = CancellationSignal.createTransport();
      try {
        param1ILayoutResultCallback.onLayoutStarted(iCancellationSignal, param1Int);
        synchronized (this.mLock) {
          if (isDestroyedLocked())
            return; 
          CancellationSignal cancellationSignal = CancellationSignal.fromTransport(iCancellationSignal);
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = this.mDocumentAdapter;
          someArgs.arg2 = param1PrintAttributes1;
          someArgs.arg3 = param1PrintAttributes2;
          someArgs.arg4 = cancellationSignal;
          MyLayoutResultCallback myLayoutResultCallback = new MyLayoutResultCallback();
          this(this, param1ILayoutResultCallback, param1Int);
          someArgs.arg5 = myLayoutResultCallback;
          someArgs.arg6 = param1Bundle;
          this.mHandler.obtainMessage(2, someArgs).sendToTarget();
          return;
        } 
      } catch (RemoteException remoteException) {
        Log.e("PrintManager", "Error notifying for layout start", (Throwable)remoteException);
        return;
      } 
    }
    
    public void write(PageRange[] param1ArrayOfPageRange, ParcelFileDescriptor param1ParcelFileDescriptor, IWriteResultCallback param1IWriteResultCallback, int param1Int) {
      ICancellationSignal iCancellationSignal = CancellationSignal.createTransport();
      try {
        param1IWriteResultCallback.onWriteStarted(iCancellationSignal, param1Int);
        synchronized (this.mLock) {
          if (isDestroyedLocked())
            return; 
          CancellationSignal cancellationSignal = CancellationSignal.fromTransport(iCancellationSignal);
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = this.mDocumentAdapter;
          someArgs.arg2 = param1ArrayOfPageRange;
          someArgs.arg3 = param1ParcelFileDescriptor;
          someArgs.arg4 = cancellationSignal;
          MyWriteResultCallback myWriteResultCallback = new MyWriteResultCallback();
          this(this, param1IWriteResultCallback, param1ParcelFileDescriptor, param1Int);
          someArgs.arg5 = myWriteResultCallback;
          this.mHandler.obtainMessage(3, someArgs).sendToTarget();
          return;
        } 
      } catch (RemoteException remoteException) {
        Log.e("PrintManager", "Error notifying for write start", (Throwable)remoteException);
        return;
      } 
    }
    
    public void finish() {
      synchronized (this.mLock) {
        if (!isDestroyedLocked()) {
          Message message = this.mHandler.obtainMessage(4, this.mDocumentAdapter);
          message.sendToTarget();
        } 
        return;
      } 
    }
    
    public void kill(String param1String) {
      synchronized (this.mLock) {
        if (!isDestroyedLocked()) {
          Message message = this.mHandler.obtainMessage(5, param1String);
          message.sendToTarget();
        } 
        return;
      } 
    }
    
    public void onActivityPaused(Activity param1Activity) {}
    
    public void onActivityCreated(Activity param1Activity, Bundle param1Bundle) {}
    
    public void onActivityStarted(Activity param1Activity) {}
    
    public void onActivityResumed(Activity param1Activity) {}
    
    public void onActivityStopped(Activity param1Activity) {}
    
    public void onActivitySaveInstanceState(Activity param1Activity, Bundle param1Bundle) {}
    
    public void onActivityDestroyed(Activity param1Activity) {
      IPrintDocumentAdapterObserver iPrintDocumentAdapterObserver = null;
      synchronized (this.mLock) {
        if (param1Activity == this.mActivity) {
          iPrintDocumentAdapterObserver = this.mObserver;
          destroyLocked();
        } 
        if (iPrintDocumentAdapterObserver != null)
          try {
            iPrintDocumentAdapterObserver.onDestroy();
          } catch (RemoteException remoteException) {
            Log.e("PrintManager", "Error announcing destroyed state", (Throwable)remoteException);
          }  
        return;
      } 
    }
    
    private boolean isDestroyedLocked() {
      boolean bool;
      if (this.mActivity == null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private void destroyLocked() {
      this.mActivity.getApplication().unregisterActivityLifecycleCallbacks(this);
      this.mActivity = null;
      this.mDocumentAdapter = null;
      this.mHandler.removeMessages(1);
      this.mHandler.removeMessages(2);
      this.mHandler.removeMessages(3);
      this.mHandler.removeMessages(4);
      this.mHandler = null;
      this.mObserver = null;
      DestroyableCallback destroyableCallback = this.mPendingCallback;
      if (destroyableCallback != null) {
        destroyableCallback.destroy();
        this.mPendingCallback = null;
      } 
    }
    
    class DestroyableCallback {
      public abstract void destroy();
    }
    
    class MyHandler extends Handler {
      public static final int MSG_ON_FINISH = 4;
      
      public static final int MSG_ON_KILL = 5;
      
      public static final int MSG_ON_LAYOUT = 2;
      
      public static final int MSG_ON_START = 1;
      
      public static final int MSG_ON_WRITE = 3;
      
      final PrintManager.PrintDocumentAdapterDelegate this$0;
      
      public MyHandler(Looper param2Looper) {
        super(param2Looper, null, true);
      }
      
      public void handleMessage(Message param2Message) {
        SomeArgs someArgs;
        int i = param2Message.what;
        if (i != 1) {
          CancellationSignal cancellationSignal;
          if (i != 2) {
            if (i != 3) {
              String str;
              if (i != 4) {
                if (i != 5) {
                  StringBuilder stringBuilder = new StringBuilder();
                  stringBuilder.append("Unknown message: ");
                  stringBuilder.append(param2Message.what);
                  throw new IllegalArgumentException(stringBuilder.toString());
                } 
                str = (String)param2Message.obj;
                throw new RuntimeException(str);
              } 
              ((PrintDocumentAdapter)((Message)str).obj).onFinish();
              synchronized (PrintManager.PrintDocumentAdapterDelegate.this.mLock) {
                PrintManager.PrintDocumentAdapterDelegate.this.destroyLocked();
              } 
            } else {
              SomeArgs someArgs1 = (SomeArgs)param2Message.obj;
              PrintDocumentAdapter printDocumentAdapter = (PrintDocumentAdapter)someArgs1.arg1;
              PageRange[] arrayOfPageRange = (PageRange[])someArgs1.arg2;
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)someArgs1.arg3;
              cancellationSignal = (CancellationSignal)someArgs1.arg4;
              PrintDocumentAdapter.WriteResultCallback writeResultCallback = (PrintDocumentAdapter.WriteResultCallback)someArgs1.arg5;
              someArgs1.recycle();
              printDocumentAdapter.onWrite(arrayOfPageRange, parcelFileDescriptor, cancellationSignal, writeResultCallback);
            } 
          } else {
            someArgs = (SomeArgs)((Message)cancellationSignal).obj;
            PrintDocumentAdapter printDocumentAdapter = (PrintDocumentAdapter)someArgs.arg1;
            PrintAttributes printAttributes1 = (PrintAttributes)someArgs.arg2;
            PrintAttributes printAttributes2 = (PrintAttributes)someArgs.arg3;
            CancellationSignal cancellationSignal1 = (CancellationSignal)someArgs.arg4;
            PrintDocumentAdapter.LayoutResultCallback layoutResultCallback = (PrintDocumentAdapter.LayoutResultCallback)someArgs.arg5;
            Bundle bundle = (Bundle)someArgs.arg6;
            someArgs.recycle();
            printDocumentAdapter.onLayout(printAttributes1, printAttributes2, cancellationSignal1, layoutResultCallback, bundle);
          } 
        } else {
          ((PrintDocumentAdapter)((Message)someArgs).obj).onStart();
        } 
      }
    }
    
    class MyLayoutResultCallback extends PrintDocumentAdapter.LayoutResultCallback implements DestroyableCallback {
      private ILayoutResultCallback mCallback;
      
      private final int mSequence;
      
      final PrintManager.PrintDocumentAdapterDelegate this$0;
      
      public MyLayoutResultCallback(ILayoutResultCallback param2ILayoutResultCallback, int param2Int) {
        this.mCallback = param2ILayoutResultCallback;
        this.mSequence = param2Int;
      }
      
      public void onLayoutFinished(PrintDocumentInfo param2PrintDocumentInfo, boolean param2Boolean) {
        synchronized (PrintManager.PrintDocumentAdapterDelegate.this.mLock) {
          ILayoutResultCallback iLayoutResultCallback = this.mCallback;
          if (iLayoutResultCallback == null) {
            Log.e("PrintManager", "PrintDocumentAdapter is destroyed. Did you finish the printing activity before print completion or did you invoke a callback after finish?");
            return;
          } 
          if (param2PrintDocumentInfo != null) {
            try {
              iLayoutResultCallback.onLayoutFinished(param2PrintDocumentInfo, param2Boolean, this.mSequence);
            } catch (RemoteException remoteException) {
              Log.e("PrintManager", "Error calling onLayoutFinished", (Throwable)remoteException);
            } finally {}
            destroy();
            return;
          } 
          NullPointerException nullPointerException = new NullPointerException();
          this("document info cannot be null");
          throw nullPointerException;
        } 
      }
      
      public void onLayoutFailed(CharSequence param2CharSequence) {
        synchronized (PrintManager.PrintDocumentAdapterDelegate.this.mLock) {
          ILayoutResultCallback iLayoutResultCallback = this.mCallback;
          if (iLayoutResultCallback == null) {
            Log.e("PrintManager", "PrintDocumentAdapter is destroyed. Did you finish the printing activity before print completion or did you invoke a callback after finish?");
            return;
          } 
          try {
            iLayoutResultCallback.onLayoutFailed(param2CharSequence, this.mSequence);
            destroy();
          } catch (RemoteException remoteException) {
            Log.e("PrintManager", "Error calling onLayoutFailed", (Throwable)remoteException);
            destroy();
          } finally {}
          return;
        } 
      }
      
      public void onLayoutCancelled() {
        synchronized (PrintManager.PrintDocumentAdapterDelegate.this.mLock) {
          ILayoutResultCallback iLayoutResultCallback = this.mCallback;
          if (iLayoutResultCallback == null) {
            Log.e("PrintManager", "PrintDocumentAdapter is destroyed. Did you finish the printing activity before print completion or did you invoke a callback after finish?");
            return;
          } 
          try {
            iLayoutResultCallback.onLayoutCanceled(this.mSequence);
            destroy();
          } catch (RemoteException remoteException) {
            Log.e("PrintManager", "Error calling onLayoutFailed", (Throwable)remoteException);
            destroy();
          } finally {}
          return;
        } 
      }
      
      public void destroy() {
        synchronized (PrintManager.PrintDocumentAdapterDelegate.this.mLock) {
          this.mCallback = null;
          PrintManager.PrintDocumentAdapterDelegate.access$202(PrintManager.PrintDocumentAdapterDelegate.this, null);
          return;
        } 
      }
    }
    
    class MyWriteResultCallback extends PrintDocumentAdapter.WriteResultCallback implements DestroyableCallback {
      private IWriteResultCallback mCallback;
      
      private ParcelFileDescriptor mFd;
      
      private final int mSequence;
      
      final PrintManager.PrintDocumentAdapterDelegate this$0;
      
      public MyWriteResultCallback(IWriteResultCallback param2IWriteResultCallback, ParcelFileDescriptor param2ParcelFileDescriptor, int param2Int) {
        this.mFd = param2ParcelFileDescriptor;
        this.mSequence = param2Int;
        this.mCallback = param2IWriteResultCallback;
      }
      
      public void onWriteFinished(PageRange[] param2ArrayOfPageRange) {
        synchronized (PrintManager.PrintDocumentAdapterDelegate.this.mLock) {
          IWriteResultCallback iWriteResultCallback = this.mCallback;
          if (iWriteResultCallback == null) {
            Log.e("PrintManager", "PrintDocumentAdapter is destroyed. Did you finish the printing activity before print completion or did you invoke a callback after finish?");
            return;
          } 
          if (param2ArrayOfPageRange != null)
            try {
              int i = param2ArrayOfPageRange.length;
              if (i != 0) {
                try {
                  iWriteResultCallback.onWriteFinished(param2ArrayOfPageRange, this.mSequence);
                } catch (RemoteException remoteException) {
                  Log.e("PrintManager", "Error calling onWriteFinished", (Throwable)remoteException);
                } 
                return;
              } 
              IllegalArgumentException illegalArgumentException1 = new IllegalArgumentException();
              this("pages cannot be empty");
              throw illegalArgumentException1;
            } finally {
              destroy();
            }  
          IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
          this("pages cannot be null");
          throw illegalArgumentException;
        } 
      }
      
      public void onWriteFailed(CharSequence param2CharSequence) {
        synchronized (PrintManager.PrintDocumentAdapterDelegate.this.mLock) {
          IWriteResultCallback iWriteResultCallback = this.mCallback;
          if (iWriteResultCallback == null) {
            Log.e("PrintManager", "PrintDocumentAdapter is destroyed. Did you finish the printing activity before print completion or did you invoke a callback after finish?");
            return;
          } 
          try {
            iWriteResultCallback.onWriteFailed(param2CharSequence, this.mSequence);
            destroy();
          } catch (RemoteException remoteException) {
            Log.e("PrintManager", "Error calling onWriteFailed", (Throwable)remoteException);
            destroy();
          } finally {}
          return;
        } 
      }
      
      public void onWriteCancelled() {
        synchronized (PrintManager.PrintDocumentAdapterDelegate.this.mLock) {
          IWriteResultCallback iWriteResultCallback = this.mCallback;
          if (iWriteResultCallback == null) {
            Log.e("PrintManager", "PrintDocumentAdapter is destroyed. Did you finish the printing activity before print completion or did you invoke a callback after finish?");
            return;
          } 
          try {
            iWriteResultCallback.onWriteCanceled(this.mSequence);
            destroy();
          } catch (RemoteException remoteException) {
            Log.e("PrintManager", "Error calling onWriteCanceled", (Throwable)remoteException);
            destroy();
          } finally {}
          return;
        } 
      }
      
      public void destroy() {
        synchronized (PrintManager.PrintDocumentAdapterDelegate.this.mLock) {
          IoUtils.closeQuietly(this.mFd);
          this.mCallback = null;
          this.mFd = null;
          PrintManager.PrintDocumentAdapterDelegate.access$202(PrintManager.PrintDocumentAdapterDelegate.this, null);
          return;
        } 
      }
    }
  }
  
  class PrintJobStateChangeListenerWrapper extends IPrintJobStateChangeListener.Stub {
    private final WeakReference<Handler> mWeakHandler;
    
    private final WeakReference<PrintManager.PrintJobStateChangeListener> mWeakListener;
    
    public PrintJobStateChangeListenerWrapper(PrintManager this$0, Handler param1Handler) {
      this.mWeakListener = new WeakReference(this$0);
      this.mWeakHandler = new WeakReference<>(param1Handler);
    }
    
    public void onPrintJobStateChanged(PrintJobId param1PrintJobId) {
      Handler handler = this.mWeakHandler.get();
      PrintManager.PrintJobStateChangeListener printJobStateChangeListener = this.mWeakListener.get();
      if (handler != null && printJobStateChangeListener != null) {
        SomeArgs someArgs = SomeArgs.obtain();
        someArgs.arg1 = this;
        someArgs.arg2 = param1PrintJobId;
        Message message = handler.obtainMessage(1, someArgs);
        message.sendToTarget();
      } 
    }
    
    public void destroy() {
      this.mWeakListener.clear();
    }
    
    public PrintManager.PrintJobStateChangeListener getListener() {
      return this.mWeakListener.get();
    }
  }
  
  class PrintServicesChangeListenerWrapper extends IPrintServicesChangeListener.Stub {
    private final WeakReference<Handler> mWeakHandler;
    
    private final WeakReference<PrintManager.PrintServicesChangeListener> mWeakListener;
    
    public PrintServicesChangeListenerWrapper(PrintManager this$0, Handler param1Handler) {
      this.mWeakListener = new WeakReference(this$0);
      this.mWeakHandler = new WeakReference<>(param1Handler);
    }
    
    public void onPrintServicesChanged() {
      Handler handler = this.mWeakHandler.get();
      PrintManager.PrintServicesChangeListener printServicesChangeListener = this.mWeakListener.get();
      if (handler != null && printServicesChangeListener != null) {
        Objects.requireNonNull(printServicesChangeListener);
        handler.post(new _$$Lambda$c2Elb5E1w2yc6lr236iX_RUAL5Q(printServicesChangeListener));
      } 
    }
    
    public void destroy() {
      this.mWeakListener.clear();
    }
  }
  
  class PrintServiceRecommendationsChangeListenerWrapper extends IRecommendationsChangeListener.Stub {
    private final WeakReference<Handler> mWeakHandler;
    
    private final WeakReference<PrintManager.PrintServiceRecommendationsChangeListener> mWeakListener;
    
    public PrintServiceRecommendationsChangeListenerWrapper(PrintManager this$0, Handler param1Handler) {
      this.mWeakListener = new WeakReference(this$0);
      this.mWeakHandler = new WeakReference<>(param1Handler);
    }
    
    public void onRecommendationsChanged() {
      Handler handler = this.mWeakHandler.get();
      PrintManager.PrintServiceRecommendationsChangeListener printServiceRecommendationsChangeListener = this.mWeakListener.get();
      if (handler != null && printServiceRecommendationsChangeListener != null) {
        Objects.requireNonNull(printServiceRecommendationsChangeListener);
        handler.post(new _$$Lambda$KZ41E_yXUNYMY9k_Xeus1UG_cS8(printServiceRecommendationsChangeListener));
      } 
    }
    
    public void destroy() {
      this.mWeakListener.clear();
    }
  }
  
  private static interface DestroyableCallback {
    void destroy();
  }
  
  public static interface PrintJobStateChangeListener {
    void onPrintJobStateChanged(PrintJobId param1PrintJobId);
  }
  
  @SystemApi
  public static interface PrintServiceRecommendationsChangeListener {
    void onPrintServiceRecommendationsChanged();
  }
  
  @SystemApi
  public static interface PrintServicesChangeListener {
    void onPrintServicesChanged();
  }
}
