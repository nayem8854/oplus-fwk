package android.print;

import android.content.ComponentName;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPrintSpoolerClient extends IInterface {
  void onAllPrintJobsForServiceHandled(ComponentName paramComponentName) throws RemoteException;
  
  void onAllPrintJobsHandled() throws RemoteException;
  
  void onPrintJobQueued(PrintJobInfo paramPrintJobInfo) throws RemoteException;
  
  void onPrintJobStateChanged(PrintJobInfo paramPrintJobInfo) throws RemoteException;
  
  class Default implements IPrintSpoolerClient {
    public void onPrintJobQueued(PrintJobInfo param1PrintJobInfo) throws RemoteException {}
    
    public void onAllPrintJobsForServiceHandled(ComponentName param1ComponentName) throws RemoteException {}
    
    public void onAllPrintJobsHandled() throws RemoteException {}
    
    public void onPrintJobStateChanged(PrintJobInfo param1PrintJobInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPrintSpoolerClient {
    private static final String DESCRIPTOR = "android.print.IPrintSpoolerClient";
    
    static final int TRANSACTION_onAllPrintJobsForServiceHandled = 2;
    
    static final int TRANSACTION_onAllPrintJobsHandled = 3;
    
    static final int TRANSACTION_onPrintJobQueued = 1;
    
    static final int TRANSACTION_onPrintJobStateChanged = 4;
    
    public Stub() {
      attachInterface(this, "android.print.IPrintSpoolerClient");
    }
    
    public static IPrintSpoolerClient asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.print.IPrintSpoolerClient");
      if (iInterface != null && iInterface instanceof IPrintSpoolerClient)
        return (IPrintSpoolerClient)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onPrintJobStateChanged";
          } 
          return "onAllPrintJobsHandled";
        } 
        return "onAllPrintJobsForServiceHandled";
      } 
      return "onPrintJobQueued";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.print.IPrintSpoolerClient");
              return true;
            } 
            param1Parcel1.enforceInterface("android.print.IPrintSpoolerClient");
            if (param1Parcel1.readInt() != 0) {
              PrintJobInfo printJobInfo = PrintJobInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onPrintJobStateChanged((PrintJobInfo)param1Parcel1);
            return true;
          } 
          param1Parcel1.enforceInterface("android.print.IPrintSpoolerClient");
          onAllPrintJobsHandled();
          return true;
        } 
        param1Parcel1.enforceInterface("android.print.IPrintSpoolerClient");
        if (param1Parcel1.readInt() != 0) {
          ComponentName componentName = ComponentName.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onAllPrintJobsForServiceHandled((ComponentName)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.print.IPrintSpoolerClient");
      if (param1Parcel1.readInt() != 0) {
        PrintJobInfo printJobInfo = PrintJobInfo.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onPrintJobQueued((PrintJobInfo)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IPrintSpoolerClient {
      public static IPrintSpoolerClient sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.print.IPrintSpoolerClient";
      }
      
      public void onPrintJobQueued(PrintJobInfo param2PrintJobInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintSpoolerClient");
          if (param2PrintJobInfo != null) {
            parcel.writeInt(1);
            param2PrintJobInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpoolerClient.Stub.getDefaultImpl() != null) {
            IPrintSpoolerClient.Stub.getDefaultImpl().onPrintJobQueued(param2PrintJobInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAllPrintJobsForServiceHandled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintSpoolerClient");
          if (param2ComponentName != null) {
            parcel.writeInt(1);
            param2ComponentName.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpoolerClient.Stub.getDefaultImpl() != null) {
            IPrintSpoolerClient.Stub.getDefaultImpl().onAllPrintJobsForServiceHandled(param2ComponentName);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAllPrintJobsHandled() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintSpoolerClient");
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpoolerClient.Stub.getDefaultImpl() != null) {
            IPrintSpoolerClient.Stub.getDefaultImpl().onAllPrintJobsHandled();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPrintJobStateChanged(PrintJobInfo param2PrintJobInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintSpoolerClient");
          if (param2PrintJobInfo != null) {
            parcel.writeInt(1);
            param2PrintJobInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpoolerClient.Stub.getDefaultImpl() != null) {
            IPrintSpoolerClient.Stub.getDefaultImpl().onPrintJobStateChanged(param2PrintJobInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPrintSpoolerClient param1IPrintSpoolerClient) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPrintSpoolerClient != null) {
          Proxy.sDefaultImpl = param1IPrintSpoolerClient;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPrintSpoolerClient getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
