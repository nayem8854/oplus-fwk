package android.print;

import android.content.pm.ParceledListSlice;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPrinterDiscoveryObserver extends IInterface {
  void onPrintersAdded(ParceledListSlice paramParceledListSlice) throws RemoteException;
  
  void onPrintersRemoved(ParceledListSlice paramParceledListSlice) throws RemoteException;
  
  class Default implements IPrinterDiscoveryObserver {
    public void onPrintersAdded(ParceledListSlice param1ParceledListSlice) throws RemoteException {}
    
    public void onPrintersRemoved(ParceledListSlice param1ParceledListSlice) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPrinterDiscoveryObserver {
    private static final String DESCRIPTOR = "android.print.IPrinterDiscoveryObserver";
    
    static final int TRANSACTION_onPrintersAdded = 1;
    
    static final int TRANSACTION_onPrintersRemoved = 2;
    
    public Stub() {
      attachInterface(this, "android.print.IPrinterDiscoveryObserver");
    }
    
    public static IPrinterDiscoveryObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.print.IPrinterDiscoveryObserver");
      if (iInterface != null && iInterface instanceof IPrinterDiscoveryObserver)
        return (IPrinterDiscoveryObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onPrintersRemoved";
      } 
      return "onPrintersAdded";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.print.IPrinterDiscoveryObserver");
          return true;
        } 
        param1Parcel1.enforceInterface("android.print.IPrinterDiscoveryObserver");
        if (param1Parcel1.readInt() != 0) {
          ParceledListSlice parceledListSlice = ParceledListSlice.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onPrintersRemoved((ParceledListSlice)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.print.IPrinterDiscoveryObserver");
      if (param1Parcel1.readInt() != 0) {
        ParceledListSlice parceledListSlice = ParceledListSlice.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onPrintersAdded((ParceledListSlice)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IPrinterDiscoveryObserver {
      public static IPrinterDiscoveryObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.print.IPrinterDiscoveryObserver";
      }
      
      public void onPrintersAdded(ParceledListSlice param2ParceledListSlice) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrinterDiscoveryObserver");
          if (param2ParceledListSlice != null) {
            parcel.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IPrinterDiscoveryObserver.Stub.getDefaultImpl() != null) {
            IPrinterDiscoveryObserver.Stub.getDefaultImpl().onPrintersAdded(param2ParceledListSlice);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPrintersRemoved(ParceledListSlice param2ParceledListSlice) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrinterDiscoveryObserver");
          if (param2ParceledListSlice != null) {
            parcel.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IPrinterDiscoveryObserver.Stub.getDefaultImpl() != null) {
            IPrinterDiscoveryObserver.Stub.getDefaultImpl().onPrintersRemoved(param2ParceledListSlice);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPrinterDiscoveryObserver param1IPrinterDiscoveryObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPrinterDiscoveryObserver != null) {
          Proxy.sDefaultImpl = param1IPrinterDiscoveryObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPrinterDiscoveryObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
