package android.print;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;
import java.util.UUID;

public final class PrintJobId implements Parcelable {
  public PrintJobId() {
    this(UUID.randomUUID().toString());
  }
  
  public PrintJobId(String paramString) {
    this.mValue = paramString;
  }
  
  public int hashCode() {
    int i = this.mValue.hashCode();
    return 1 * 31 + i;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (!this.mValue.equals(((PrintJobId)paramObject).mValue))
      return false; 
    return true;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mValue);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String flattenToString() {
    return this.mValue;
  }
  
  public static PrintJobId unflattenFromString(String paramString) {
    return new PrintJobId(paramString);
  }
  
  public static final Parcelable.Creator<PrintJobId> CREATOR = new Parcelable.Creator<PrintJobId>() {
      public PrintJobId createFromParcel(Parcel param1Parcel) {
        return new PrintJobId((String)Preconditions.checkNotNull(param1Parcel.readString()));
      }
      
      public PrintJobId[] newArray(int param1Int) {
        return new PrintJobId[param1Int];
      }
    };
  
  private final String mValue;
}
