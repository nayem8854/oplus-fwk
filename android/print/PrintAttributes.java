package android.print;

import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Map;

public final class PrintAttributes implements Parcelable {
  public static final int COLOR_MODE_COLOR = 2;
  
  public static final int COLOR_MODE_MONOCHROME = 1;
  
  PrintAttributes() {}
  
  private PrintAttributes(Parcel paramParcel) {
    Margins margins;
    int i = paramParcel.readInt();
    MediaSize mediaSize1 = null;
    if (i == 1) {
      mediaSize2 = MediaSize.createFromParcel(paramParcel);
    } else {
      mediaSize2 = null;
    } 
    this.mMediaSize = mediaSize2;
    if (paramParcel.readInt() == 1) {
      Resolution resolution = Resolution.createFromParcel(paramParcel);
    } else {
      mediaSize2 = null;
    } 
    this.mResolution = (Resolution)mediaSize2;
    MediaSize mediaSize2 = mediaSize1;
    if (paramParcel.readInt() == 1)
      margins = Margins.createFromParcel(paramParcel); 
    this.mMinMargins = margins;
    this.mColorMode = i = paramParcel.readInt();
    if (i != 0)
      enforceValidColorMode(i); 
    this.mDuplexMode = i = paramParcel.readInt();
    if (i != 0)
      enforceValidDuplexMode(i); 
  }
  
  public MediaSize getMediaSize() {
    return this.mMediaSize;
  }
  
  public void setMediaSize(MediaSize paramMediaSize) {
    this.mMediaSize = paramMediaSize;
  }
  
  public Resolution getResolution() {
    return this.mResolution;
  }
  
  public void setResolution(Resolution paramResolution) {
    this.mResolution = paramResolution;
  }
  
  public Margins getMinMargins() {
    return this.mMinMargins;
  }
  
  public void setMinMargins(Margins paramMargins) {
    this.mMinMargins = paramMargins;
  }
  
  public int getColorMode() {
    return this.mColorMode;
  }
  
  public void setColorMode(int paramInt) {
    enforceValidColorMode(paramInt);
    this.mColorMode = paramInt;
  }
  
  public boolean isPortrait() {
    return this.mMediaSize.isPortrait();
  }
  
  public int getDuplexMode() {
    return this.mDuplexMode;
  }
  
  public void setDuplexMode(int paramInt) {
    enforceValidDuplexMode(paramInt);
    this.mDuplexMode = paramInt;
  }
  
  public PrintAttributes asPortrait() {
    if (isPortrait())
      return this; 
    PrintAttributes printAttributes = new PrintAttributes();
    printAttributes.setMediaSize(getMediaSize().asPortrait());
    Resolution resolution1 = getResolution();
    String str1 = resolution1.getId();
    String str2 = resolution1.getLabel();
    int i = resolution1.getVerticalDpi();
    Resolution resolution2 = new Resolution(str1, str2, i, resolution1.getHorizontalDpi());
    printAttributes.setResolution(resolution2);
    printAttributes.setMinMargins(getMinMargins());
    printAttributes.setColorMode(getColorMode());
    printAttributes.setDuplexMode(getDuplexMode());
    return printAttributes;
  }
  
  public PrintAttributes asLandscape() {
    if (!isPortrait())
      return this; 
    PrintAttributes printAttributes = new PrintAttributes();
    printAttributes.setMediaSize(getMediaSize().asLandscape());
    Resolution resolution1 = getResolution();
    String str1 = resolution1.getId();
    String str2 = resolution1.getLabel();
    int i = resolution1.getVerticalDpi();
    Resolution resolution2 = new Resolution(str1, str2, i, resolution1.getHorizontalDpi());
    printAttributes.setResolution(resolution2);
    printAttributes.setMinMargins(getMinMargins());
    printAttributes.setColorMode(getColorMode());
    printAttributes.setDuplexMode(getDuplexMode());
    return printAttributes;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (this.mMediaSize != null) {
      paramParcel.writeInt(1);
      this.mMediaSize.writeToParcel(paramParcel);
    } else {
      paramParcel.writeInt(0);
    } 
    if (this.mResolution != null) {
      paramParcel.writeInt(1);
      this.mResolution.writeToParcel(paramParcel);
    } else {
      paramParcel.writeInt(0);
    } 
    if (this.mMinMargins != null) {
      paramParcel.writeInt(1);
      this.mMinMargins.writeToParcel(paramParcel);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeInt(this.mColorMode);
    paramParcel.writeInt(this.mDuplexMode);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public int hashCode() {
    int m, n, i = this.mColorMode;
    int j = this.mDuplexMode;
    Margins margins = this.mMinMargins;
    int k = 0;
    if (margins == null) {
      m = 0;
    } else {
      m = margins.hashCode();
    } 
    MediaSize mediaSize = this.mMediaSize;
    if (mediaSize == null) {
      n = 0;
    } else {
      n = mediaSize.hashCode();
    } 
    Resolution resolution = this.mResolution;
    if (resolution != null)
      k = resolution.hashCode(); 
    return ((((1 * 31 + i) * 31 + j) * 31 + m) * 31 + n) * 31 + k;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mColorMode != ((PrintAttributes)paramObject).mColorMode)
      return false; 
    if (this.mDuplexMode != ((PrintAttributes)paramObject).mDuplexMode)
      return false; 
    Margins margins = this.mMinMargins;
    if (margins == null) {
      if (((PrintAttributes)paramObject).mMinMargins != null)
        return false; 
    } else if (!margins.equals(((PrintAttributes)paramObject).mMinMargins)) {
      return false;
    } 
    MediaSize mediaSize = this.mMediaSize;
    if (mediaSize == null) {
      if (((PrintAttributes)paramObject).mMediaSize != null)
        return false; 
    } else if (!mediaSize.equals(((PrintAttributes)paramObject).mMediaSize)) {
      return false;
    } 
    Resolution resolution = this.mResolution;
    if (resolution == null) {
      if (((PrintAttributes)paramObject).mResolution != null)
        return false; 
    } else if (!resolution.equals(((PrintAttributes)paramObject).mResolution)) {
      return false;
    } 
    return true;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("PrintAttributes{");
    stringBuilder.append("mediaSize: ");
    stringBuilder.append(this.mMediaSize);
    if (this.mMediaSize != null) {
      String str;
      stringBuilder.append(", orientation: ");
      if (this.mMediaSize.isPortrait()) {
        str = "portrait";
      } else {
        str = "landscape";
      } 
      stringBuilder.append(str);
    } else {
      stringBuilder.append(", orientation: ");
      stringBuilder.append("null");
    } 
    stringBuilder.append(", resolution: ");
    stringBuilder.append(this.mResolution);
    stringBuilder.append(", minMargins: ");
    stringBuilder.append(this.mMinMargins);
    stringBuilder.append(", colorMode: ");
    stringBuilder.append(colorModeToString(this.mColorMode));
    stringBuilder.append(", duplexMode: ");
    stringBuilder.append(duplexModeToString(this.mDuplexMode));
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void clear() {
    this.mMediaSize = null;
    this.mResolution = null;
    this.mMinMargins = null;
    this.mColorMode = 0;
    this.mDuplexMode = 0;
  }
  
  public void copyFrom(PrintAttributes paramPrintAttributes) {
    this.mMediaSize = paramPrintAttributes.mMediaSize;
    this.mResolution = paramPrintAttributes.mResolution;
    this.mMinMargins = paramPrintAttributes.mMinMargins;
    this.mColorMode = paramPrintAttributes.mColorMode;
    this.mDuplexMode = paramPrintAttributes.mDuplexMode;
  }
  
  class MediaSize {
    public static final MediaSize ISO_A0;
    
    public static final MediaSize ISO_A1;
    
    public static final MediaSize ISO_A10;
    
    public static final MediaSize ISO_A2;
    
    public static final MediaSize ISO_A3;
    
    public static final MediaSize ISO_A4;
    
    public static final MediaSize ISO_A5;
    
    public static final MediaSize ISO_A6;
    
    public static final MediaSize ISO_A7;
    
    public static final MediaSize ISO_A8;
    
    public static final MediaSize ISO_A9;
    
    public static final MediaSize ISO_B0;
    
    public static final MediaSize ISO_B1;
    
    public static final MediaSize ISO_B10;
    
    public static final MediaSize ISO_B2;
    
    public static final MediaSize ISO_B3;
    
    public static final MediaSize ISO_B4;
    
    public static final MediaSize ISO_B5;
    
    public static final MediaSize ISO_B6;
    
    public static final MediaSize ISO_B7;
    
    public static final MediaSize ISO_B8;
    
    public static final MediaSize ISO_B9;
    
    public static final MediaSize ISO_C0;
    
    public static final MediaSize ISO_C1;
    
    public static final MediaSize ISO_C10;
    
    public static final MediaSize ISO_C2;
    
    public static final MediaSize ISO_C3;
    
    public static final MediaSize ISO_C4;
    
    public static final MediaSize ISO_C5;
    
    public static final MediaSize ISO_C6;
    
    public static final MediaSize ISO_C7;
    
    public static final MediaSize ISO_C8;
    
    public static final MediaSize ISO_C9;
    
    public static final MediaSize JIS_B0;
    
    public static final MediaSize JIS_B1;
    
    public static final MediaSize JIS_B10;
    
    public static final MediaSize JIS_B2;
    
    public static final MediaSize JIS_B3;
    
    public static final MediaSize JIS_B4;
    
    public static final MediaSize JIS_B5;
    
    public static final MediaSize JIS_B6;
    
    public static final MediaSize JIS_B7;
    
    public static final MediaSize JIS_B8;
    
    public static final MediaSize JIS_B9;
    
    public static final MediaSize JIS_EXEC;
    
    public static final MediaSize JPN_CHOU2;
    
    public static final MediaSize JPN_CHOU3;
    
    public static final MediaSize JPN_CHOU4;
    
    public static final MediaSize JPN_HAGAKI;
    
    public static final MediaSize JPN_KAHU;
    
    public static final MediaSize JPN_KAKU2;
    
    public static final MediaSize JPN_OUFUKU;
    
    public static final MediaSize JPN_YOU4;
    
    private static final String LOG_TAG = "MediaSize";
    
    public static final MediaSize NA_FOOLSCAP;
    
    public static final MediaSize NA_GOVT_LETTER;
    
    public static final MediaSize NA_INDEX_3X5;
    
    public static final MediaSize NA_INDEX_4X6;
    
    public static final MediaSize NA_INDEX_5X8;
    
    public static final MediaSize NA_JUNIOR_LEGAL;
    
    public static final MediaSize NA_LEDGER;
    
    public static final MediaSize NA_LEGAL;
    
    public static final MediaSize NA_LETTER;
    
    public static final MediaSize NA_MONARCH;
    
    public static final MediaSize NA_QUARTO;
    
    public static final MediaSize NA_TABLOID;
    
    public static final MediaSize OM_DAI_PA_KAI;
    
    public static final MediaSize OM_JUURO_KU_KAI;
    
    public static final MediaSize OM_PA_KAI;
    
    public static final MediaSize PRC_1;
    
    public static final MediaSize PRC_10;
    
    public static final MediaSize PRC_16K;
    
    public static final MediaSize PRC_2;
    
    public static final MediaSize PRC_3;
    
    public static final MediaSize PRC_4;
    
    public static final MediaSize PRC_5;
    
    public static final MediaSize PRC_6;
    
    public static final MediaSize PRC_7;
    
    public static final MediaSize PRC_8;
    
    public static final MediaSize PRC_9;
    
    public static final MediaSize ROC_16K;
    
    public static final MediaSize ROC_8K;
    
    public static final MediaSize UNKNOWN_LANDSCAPE;
    
    public static final MediaSize UNKNOWN_PORTRAIT = new MediaSize("android", 17040623, 1, 2147483647);
    
    private static final Map<String, MediaSize> sIdToMediaSizeMap = (Map<String, MediaSize>)new ArrayMap();
    
    private final int mHeightMils;
    
    private final String mId;
    
    public final String mLabel;
    
    public final int mLabelResId;
    
    public final String mPackageName;
    
    private final int mWidthMils;
    
    static {
      UNKNOWN_LANDSCAPE = new MediaSize("android", 17040622, 2147483647, 1);
      ISO_A0 = new MediaSize("android", 17040557, 33110, 46810);
      ISO_A1 = new MediaSize("android", 17040558, 23390, 33110);
      ISO_A2 = new MediaSize("android", 17040560, 16540, 23390);
      ISO_A3 = new MediaSize("android", 17040561, 11690, 16540);
      ISO_A4 = new MediaSize("android", 17040562, 8270, 11690);
      ISO_A5 = new MediaSize("android", 17040563, 5830, 8270);
      ISO_A6 = new MediaSize("android", 17040564, 4130, 5830);
      ISO_A7 = new MediaSize("android", 17040565, 2910, 4130);
      ISO_A8 = new MediaSize("android", 17040566, 2050, 2910);
      ISO_A9 = new MediaSize("android", 17040567, 1460, 2050);
      ISO_A10 = new MediaSize("android", 17040559, 1020, 1460);
      ISO_B0 = new MediaSize("android", 17040568, 39370, 55670);
      ISO_B1 = new MediaSize("android", 17040569, 27830, 39370);
      ISO_B2 = new MediaSize("android", 17040571, 19690, 27830);
      ISO_B3 = new MediaSize("android", 17040572, 13900, 19690);
      ISO_B4 = new MediaSize("android", 17040573, 9840, 13900);
      ISO_B5 = new MediaSize("android", 17040574, 6930, 9840);
      ISO_B6 = new MediaSize("android", 17040575, 4920, 6930);
      ISO_B7 = new MediaSize("android", 17040576, 3460, 4920);
      ISO_B8 = new MediaSize("android", 17040577, 2440, 3460);
      ISO_B9 = new MediaSize("android", 17040578, 1730, 2440);
      ISO_B10 = new MediaSize("android", 17040570, 1220, 1730);
      ISO_C0 = new MediaSize("android", 17040579, 36100, 51060);
      ISO_C1 = new MediaSize("android", 17040580, 25510, 36100);
      ISO_C2 = new MediaSize("android", 17040582, 18030, 25510);
      ISO_C3 = new MediaSize("android", 17040583, 12760, 18030);
      ISO_C4 = new MediaSize("android", 17040584, 9020, 12760);
      ISO_C5 = new MediaSize("android", 17040585, 6380, 9020);
      ISO_C6 = new MediaSize("android", 17040586, 4490, 6380);
      ISO_C7 = new MediaSize("android", 17040587, 3190, 4490);
      ISO_C8 = new MediaSize("android", 17040588, 2240, 3190);
      ISO_C9 = new MediaSize("android", 17040589, 1570, 2240);
      ISO_C10 = new MediaSize("android", 17040581, 1100, 1570);
      NA_LETTER = new MediaSize("android", 17040618, 8500, 11000);
      NA_GOVT_LETTER = new MediaSize("android", 17040611, 8000, 10500);
      NA_LEGAL = new MediaSize("android", 17040617, 8500, 14000);
      NA_JUNIOR_LEGAL = new MediaSize("android", 17040615, 8000, 5000);
      NA_LEDGER = new MediaSize("android", 17040616, 17000, 11000);
      NA_TABLOID = new MediaSize("android", 17040621, 11000, 17000);
      NA_INDEX_3X5 = new MediaSize("android", 17040612, 3000, 5000);
      NA_INDEX_4X6 = new MediaSize("android", 17040613, 4000, 6000);
      NA_INDEX_5X8 = new MediaSize("android", 17040614, 5000, 8000);
      NA_MONARCH = new MediaSize("android", 17040619, 7250, 10500);
      NA_QUARTO = new MediaSize("android", 17040620, 8000, 10000);
      NA_FOOLSCAP = new MediaSize("android", 17040610, 8000, 13000);
      ROC_8K = new MediaSize("android", 17040556, 10629, 15354);
      ROC_16K = new MediaSize("android", 17040555, 7677, 10629);
      PRC_1 = new MediaSize("android", 17040544, 4015, 6496);
      PRC_2 = new MediaSize("android", 17040547, 4015, 6929);
      PRC_3 = new MediaSize("android", 17040548, 4921, 6929);
      PRC_4 = new MediaSize("android", 17040549, 4330, 8189);
      PRC_5 = new MediaSize("android", 17040550, 4330, 8661);
      PRC_6 = new MediaSize("android", 17040551, 4724, 12599);
      PRC_7 = new MediaSize("android", 17040552, 6299, 9055);
      PRC_8 = new MediaSize("android", 17040553, 4724, 12165);
      PRC_9 = new MediaSize("android", 17040554, 9016, 12756);
      PRC_10 = new MediaSize("android", 17040545, 12756, 18032);
      PRC_16K = new MediaSize("android", 17040546, 5749, 8465);
      OM_PA_KAI = new MediaSize("android", 17040543, 10512, 15315);
      OM_DAI_PA_KAI = new MediaSize("android", 17040541, 10827, 15551);
      OM_JUURO_KU_KAI = new MediaSize("android", 17040542, 7796, 10827);
      JIS_B10 = new MediaSize("android", 17040596, 1259, 1772);
      JIS_B9 = new MediaSize("android", 17040604, 1772, 2520);
      JIS_B8 = new MediaSize("android", 17040603, 2520, 3583);
      JIS_B7 = new MediaSize("android", 17040602, 3583, 5049);
      JIS_B6 = new MediaSize("android", 17040601, 5049, 7165);
      JIS_B5 = new MediaSize("android", 17040600, 7165, 10118);
      JIS_B4 = new MediaSize("android", 17040599, 10118, 14331);
      JIS_B3 = new MediaSize("android", 17040598, 14331, 20276);
      JIS_B2 = new MediaSize("android", 17040597, 20276, 28661);
      JIS_B1 = new MediaSize("android", 17040595, 28661, 40551);
      JIS_B0 = new MediaSize("android", 17040594, 40551, 57323);
      JIS_EXEC = new MediaSize("android", 17040605, 8504, 12992);
      JPN_CHOU4 = new MediaSize("android", 17040592, 3543, 8071);
      JPN_CHOU3 = new MediaSize("android", 17040591, 4724, 9252);
      JPN_CHOU2 = new MediaSize("android", 17040590, 4374, 5748);
      JPN_HAGAKI = new MediaSize("android", 17040593, 3937, 5827);
      JPN_OUFUKU = new MediaSize("android", 17040608, 5827, 7874);
      JPN_KAHU = new MediaSize("android", 17040606, 9449, 12681);
      JPN_KAKU2 = new MediaSize("android", 17040607, 9449, 13071);
      JPN_YOU4 = new MediaSize("android", 17040609, 4134, 9252);
    }
    
    public MediaSize(String param1String1, int param1Int1, int param1Int2, int param1Int3) {
      this((String)this$0, null, param1String1, param1Int2, param1Int3, param1Int1);
      sIdToMediaSizeMap.put(this.mId, this);
    }
    
    public MediaSize(String param1String1, int param1Int1, int param1Int2) {
      this((String)this$0, param1String1, null, param1Int1, param1Int2, 0);
    }
    
    public static ArraySet<MediaSize> getAllPredefinedSizes() {
      ArraySet<MediaSize> arraySet = new ArraySet(sIdToMediaSizeMap.values());
      arraySet.remove(UNKNOWN_PORTRAIT);
      arraySet.remove(UNKNOWN_LANDSCAPE);
      return arraySet;
    }
    
    public MediaSize(String param1String1, String param1String2, int param1Int1, int param1Int2, int param1Int3) {
      this.mPackageName = param1String2;
      this.mId = (String)Preconditions.checkStringNotEmpty((CharSequence)this$0, "id cannot be empty.");
      this.mLabelResId = param1Int3;
      this.mWidthMils = Preconditions.checkArgumentPositive(param1Int1, "widthMils cannot be less than or equal to zero.");
      this.mHeightMils = Preconditions.checkArgumentPositive(param1Int2, "heightMils cannot be less than or equal to zero.");
      this.mLabel = param1String1;
      boolean bool = TextUtils.isEmpty(param1String1);
      boolean bool1 = true;
      if (!TextUtils.isEmpty(param1String2) && param1Int3 != 0) {
        param1Int1 = 1;
      } else {
        param1Int1 = 0;
      } 
      if ((bool ^ true) == param1Int1)
        bool1 = false; 
      Preconditions.checkArgument(bool1, "label cannot be empty.");
    }
    
    public String getId() {
      return this.mId;
    }
    
    public String getLabel(PackageManager param1PackageManager) {
      if (!TextUtils.isEmpty(this.mPackageName) && this.mLabelResId > 0)
        try {
          Resources resources = param1PackageManager.getResourcesForApplication(this.mPackageName);
          int i = this.mLabelResId;
          return 
            resources.getString(i);
        } catch (android.content.res.Resources.NotFoundException|android.content.pm.PackageManager.NameNotFoundException notFoundException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Could not load resouce");
          stringBuilder.append(this.mLabelResId);
          stringBuilder.append(" from package ");
          stringBuilder.append(this.mPackageName);
          Log.w("MediaSize", stringBuilder.toString());
        }  
      return this.mLabel;
    }
    
    public int getWidthMils() {
      return this.mWidthMils;
    }
    
    public int getHeightMils() {
      return this.mHeightMils;
    }
    
    public boolean isPortrait() {
      boolean bool;
      if (this.mHeightMils >= this.mWidthMils) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public MediaSize asPortrait() {
      if (isPortrait())
        return this; 
      String str1 = this.mId, str2 = this.mLabel, str3 = this.mPackageName;
      int i = this.mWidthMils, j = this.mHeightMils;
      i = Math.min(i, j);
      j = this.mWidthMils;
      int k = this.mHeightMils;
      return new MediaSize(str2, str3, i, Math.max(j, k), this.mLabelResId);
    }
    
    public MediaSize asLandscape() {
      if (!isPortrait())
        return this; 
      String str1 = this.mId, str2 = this.mLabel, str3 = this.mPackageName;
      int i = this.mWidthMils, j = this.mHeightMils;
      j = Math.max(i, j);
      int k = this.mWidthMils;
      i = this.mHeightMils;
      return new MediaSize(str2, str3, j, Math.min(k, i), this.mLabelResId);
    }
    
    void writeToParcel(Parcel param1Parcel) {
      param1Parcel.writeString(this.mId);
      param1Parcel.writeString(this.mLabel);
      param1Parcel.writeString(this.mPackageName);
      param1Parcel.writeInt(this.mWidthMils);
      param1Parcel.writeInt(this.mHeightMils);
      param1Parcel.writeInt(this.mLabelResId);
    }
    
    static MediaSize createFromParcel(Parcel param1Parcel) {
      String str1 = param1Parcel.readString();
      String str2 = param1Parcel.readString();
      String str3 = param1Parcel.readString();
      int i = param1Parcel.readInt();
      int j = param1Parcel.readInt();
      return new MediaSize(str2, str3, i, j, param1Parcel.readInt());
    }
    
    public int hashCode() {
      int i = this.mWidthMils;
      int j = this.mHeightMils;
      return (1 * 31 + i) * 31 + j;
    }
    
    public boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null)
        return false; 
      if (getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      if (this.mWidthMils != ((MediaSize)param1Object).mWidthMils)
        return false; 
      if (this.mHeightMils != ((MediaSize)param1Object).mHeightMils)
        return false; 
      return true;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("MediaSize{");
      stringBuilder.append("id: ");
      stringBuilder.append(this.mId);
      stringBuilder.append(", label: ");
      stringBuilder.append(this.mLabel);
      stringBuilder.append(", packageName: ");
      stringBuilder.append(this.mPackageName);
      stringBuilder.append(", heightMils: ");
      stringBuilder.append(this.mHeightMils);
      stringBuilder.append(", widthMils: ");
      stringBuilder.append(this.mWidthMils);
      stringBuilder.append(", labelResId: ");
      stringBuilder.append(this.mLabelResId);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public static MediaSize getStandardMediaSizeById(String param1String) {
      return sIdToMediaSizeMap.get(param1String);
    }
  }
  
  class Resolution {
    private final int mHorizontalDpi;
    
    private final String mId;
    
    private final String mLabel;
    
    private final int mVerticalDpi;
    
    public Resolution(PrintAttributes this$0, String param1String1, int param1Int1, int param1Int2) {
      if (!TextUtils.isEmpty((CharSequence)this$0)) {
        if (!TextUtils.isEmpty(param1String1)) {
          if (param1Int1 > 0) {
            if (param1Int2 > 0) {
              this.mId = (String)this$0;
              this.mLabel = param1String1;
              this.mHorizontalDpi = param1Int1;
              this.mVerticalDpi = param1Int2;
              return;
            } 
            throw new IllegalArgumentException("verticalDpi cannot be less than or equal to zero.");
          } 
          throw new IllegalArgumentException("horizontalDpi cannot be less than or equal to zero.");
        } 
        throw new IllegalArgumentException("label cannot be empty.");
      } 
      throw new IllegalArgumentException("id cannot be empty.");
    }
    
    public String getId() {
      return this.mId;
    }
    
    public String getLabel() {
      return this.mLabel;
    }
    
    public int getHorizontalDpi() {
      return this.mHorizontalDpi;
    }
    
    public int getVerticalDpi() {
      return this.mVerticalDpi;
    }
    
    void writeToParcel(Parcel param1Parcel) {
      param1Parcel.writeString(this.mId);
      param1Parcel.writeString(this.mLabel);
      param1Parcel.writeInt(this.mHorizontalDpi);
      param1Parcel.writeInt(this.mVerticalDpi);
    }
    
    static Resolution createFromParcel(Parcel param1Parcel) {
      String str1 = param1Parcel.readString();
      String str2 = param1Parcel.readString();
      int i = param1Parcel.readInt();
      return new Resolution(str1, str2, i, param1Parcel.readInt());
    }
    
    public int hashCode() {
      int i = this.mHorizontalDpi;
      int j = this.mVerticalDpi;
      return (1 * 31 + i) * 31 + j;
    }
    
    public boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null)
        return false; 
      if (getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      if (this.mHorizontalDpi != ((Resolution)param1Object).mHorizontalDpi)
        return false; 
      if (this.mVerticalDpi != ((Resolution)param1Object).mVerticalDpi)
        return false; 
      return true;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Resolution{");
      stringBuilder.append("id: ");
      stringBuilder.append(this.mId);
      stringBuilder.append(", label: ");
      stringBuilder.append(this.mLabel);
      stringBuilder.append(", horizontalDpi: ");
      stringBuilder.append(this.mHorizontalDpi);
      stringBuilder.append(", verticalDpi: ");
      stringBuilder.append(this.mVerticalDpi);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  class Margins {
    public static final Margins NO_MARGINS = new Margins(0, 0, 0, 0);
    
    private final int mBottomMils;
    
    private final int mLeftMils;
    
    private final int mRightMils;
    
    private final int mTopMils;
    
    public Margins(PrintAttributes this$0, int param1Int1, int param1Int2, int param1Int3) {
      this.mTopMils = param1Int1;
      this.mLeftMils = this$0;
      this.mRightMils = param1Int2;
      this.mBottomMils = param1Int3;
    }
    
    public int getLeftMils() {
      return this.mLeftMils;
    }
    
    public int getTopMils() {
      return this.mTopMils;
    }
    
    public int getRightMils() {
      return this.mRightMils;
    }
    
    public int getBottomMils() {
      return this.mBottomMils;
    }
    
    void writeToParcel(Parcel param1Parcel) {
      param1Parcel.writeInt(this.mLeftMils);
      param1Parcel.writeInt(this.mTopMils);
      param1Parcel.writeInt(this.mRightMils);
      param1Parcel.writeInt(this.mBottomMils);
    }
    
    static Margins createFromParcel(Parcel param1Parcel) {
      int i = param1Parcel.readInt();
      int j = param1Parcel.readInt();
      int k = param1Parcel.readInt();
      return new Margins(i, j, k, param1Parcel.readInt());
    }
    
    public int hashCode() {
      int i = this.mBottomMils;
      int j = this.mLeftMils;
      int k = this.mRightMils;
      int m = this.mTopMils;
      return (((1 * 31 + i) * 31 + j) * 31 + k) * 31 + m;
    }
    
    public boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null)
        return false; 
      if (getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      if (this.mBottomMils != ((Margins)param1Object).mBottomMils)
        return false; 
      if (this.mLeftMils != ((Margins)param1Object).mLeftMils)
        return false; 
      if (this.mRightMils != ((Margins)param1Object).mRightMils)
        return false; 
      if (this.mTopMils != ((Margins)param1Object).mTopMils)
        return false; 
      return true;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Margins{");
      stringBuilder.append("leftMils: ");
      stringBuilder.append(this.mLeftMils);
      stringBuilder.append(", topMils: ");
      stringBuilder.append(this.mTopMils);
      stringBuilder.append(", rightMils: ");
      stringBuilder.append(this.mRightMils);
      stringBuilder.append(", bottomMils: ");
      stringBuilder.append(this.mBottomMils);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  static String colorModeToString(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2)
        return "COLOR_MODE_UNKNOWN"; 
      return "COLOR_MODE_COLOR";
    } 
    return "COLOR_MODE_MONOCHROME";
  }
  
  static String duplexModeToString(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 4)
          return "DUPLEX_MODE_UNKNOWN"; 
        return "DUPLEX_MODE_SHORT_EDGE";
      } 
      return "DUPLEX_MODE_LONG_EDGE";
    } 
    return "DUPLEX_MODE_NONE";
  }
  
  static void enforceValidColorMode(int paramInt) {
    if ((paramInt & 0x3) != 0 && Integer.bitCount(paramInt) == 1)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("invalid color mode: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  static void enforceValidDuplexMode(int paramInt) {
    if ((paramInt & 0x7) != 0 && Integer.bitCount(paramInt) == 1)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("invalid duplex mode: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  class Builder {
    private final PrintAttributes mAttributes = new PrintAttributes();
    
    public Builder setMediaSize(PrintAttributes.MediaSize param1MediaSize) {
      this.mAttributes.setMediaSize(param1MediaSize);
      return this;
    }
    
    public Builder setResolution(PrintAttributes.Resolution param1Resolution) {
      this.mAttributes.setResolution(param1Resolution);
      return this;
    }
    
    public Builder setMinMargins(PrintAttributes.Margins param1Margins) {
      this.mAttributes.setMinMargins(param1Margins);
      return this;
    }
    
    public Builder setColorMode(int param1Int) {
      this.mAttributes.setColorMode(param1Int);
      return this;
    }
    
    public Builder setDuplexMode(int param1Int) {
      this.mAttributes.setDuplexMode(param1Int);
      return this;
    }
    
    public PrintAttributes build() {
      return this.mAttributes;
    }
  }
  
  public static final Parcelable.Creator<PrintAttributes> CREATOR = new Parcelable.Creator<PrintAttributes>() {
      public PrintAttributes createFromParcel(Parcel param1Parcel) {
        return new PrintAttributes(param1Parcel);
      }
      
      public PrintAttributes[] newArray(int param1Int) {
        return new PrintAttributes[param1Int];
      }
    };
  
  public static final int DUPLEX_MODE_LONG_EDGE = 2;
  
  public static final int DUPLEX_MODE_NONE = 1;
  
  public static final int DUPLEX_MODE_SHORT_EDGE = 4;
  
  private static final int VALID_COLOR_MODES = 3;
  
  private static final int VALID_DUPLEX_MODES = 7;
  
  private int mColorMode;
  
  private int mDuplexMode;
  
  private MediaSize mMediaSize;
  
  private Margins mMinMargins;
  
  private Resolution mResolution;
  
  @Retention(RetentionPolicy.SOURCE)
  class ColorMode implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class DuplexMode implements Annotation {}
}
