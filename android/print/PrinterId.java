package android.print;

import android.content.ComponentName;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;

public final class PrinterId implements Parcelable {
  public PrinterId(ComponentName paramComponentName, String paramString) {
    this.mServiceName = paramComponentName;
    this.mLocalId = paramString;
  }
  
  private PrinterId(Parcel paramParcel) {
    this.mServiceName = (ComponentName)Preconditions.checkNotNull(paramParcel.<ComponentName>readParcelable(null));
    this.mLocalId = (String)Preconditions.checkNotNull(paramParcel.readString());
  }
  
  public ComponentName getServiceName() {
    return this.mServiceName;
  }
  
  public String getLocalId() {
    return this.mLocalId;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.mServiceName, paramInt);
    paramParcel.writeString(this.mLocalId);
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (!this.mServiceName.equals(((PrinterId)paramObject).mServiceName))
      return false; 
    if (!this.mLocalId.equals(((PrinterId)paramObject).mLocalId))
      return false; 
    return true;
  }
  
  public int hashCode() {
    int i = this.mServiceName.hashCode();
    int j = this.mLocalId.hashCode();
    return (1 * 31 + i) * 31 + j;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("PrinterId{");
    stringBuilder.append("serviceName=");
    stringBuilder.append(this.mServiceName.flattenToString());
    stringBuilder.append(", localId=");
    stringBuilder.append(this.mLocalId);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<PrinterId> CREATOR = new Parcelable.Creator<PrinterId>() {
      public PrinterId createFromParcel(Parcel param1Parcel) {
        return new PrinterId(param1Parcel);
      }
      
      public PrinterId[] newArray(int param1Int) {
        return new PrinterId[param1Int];
      }
    };
  
  private final String mLocalId;
  
  private final ComponentName mServiceName;
}
