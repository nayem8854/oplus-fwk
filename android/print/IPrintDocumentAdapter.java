package android.print;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

public interface IPrintDocumentAdapter extends IInterface {
  void finish() throws RemoteException;
  
  void kill(String paramString) throws RemoteException;
  
  void layout(PrintAttributes paramPrintAttributes1, PrintAttributes paramPrintAttributes2, ILayoutResultCallback paramILayoutResultCallback, Bundle paramBundle, int paramInt) throws RemoteException;
  
  void setObserver(IPrintDocumentAdapterObserver paramIPrintDocumentAdapterObserver) throws RemoteException;
  
  void start() throws RemoteException;
  
  void write(PageRange[] paramArrayOfPageRange, ParcelFileDescriptor paramParcelFileDescriptor, IWriteResultCallback paramIWriteResultCallback, int paramInt) throws RemoteException;
  
  class Default implements IPrintDocumentAdapter {
    public void setObserver(IPrintDocumentAdapterObserver param1IPrintDocumentAdapterObserver) throws RemoteException {}
    
    public void start() throws RemoteException {}
    
    public void layout(PrintAttributes param1PrintAttributes1, PrintAttributes param1PrintAttributes2, ILayoutResultCallback param1ILayoutResultCallback, Bundle param1Bundle, int param1Int) throws RemoteException {}
    
    public void write(PageRange[] param1ArrayOfPageRange, ParcelFileDescriptor param1ParcelFileDescriptor, IWriteResultCallback param1IWriteResultCallback, int param1Int) throws RemoteException {}
    
    public void finish() throws RemoteException {}
    
    public void kill(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPrintDocumentAdapter {
    private static final String DESCRIPTOR = "android.print.IPrintDocumentAdapter";
    
    static final int TRANSACTION_finish = 5;
    
    static final int TRANSACTION_kill = 6;
    
    static final int TRANSACTION_layout = 3;
    
    static final int TRANSACTION_setObserver = 1;
    
    static final int TRANSACTION_start = 2;
    
    static final int TRANSACTION_write = 4;
    
    public Stub() {
      attachInterface(this, "android.print.IPrintDocumentAdapter");
    }
    
    public static IPrintDocumentAdapter asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.print.IPrintDocumentAdapter");
      if (iInterface != null && iInterface instanceof IPrintDocumentAdapter)
        return (IPrintDocumentAdapter)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "kill";
        case 5:
          return "finish";
        case 4:
          return "write";
        case 3:
          return "layout";
        case 2:
          return "start";
        case 1:
          break;
      } 
      return "setObserver";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String str;
        PageRange[] arrayOfPageRange;
        IWriteResultCallback iWriteResultCallback;
        ILayoutResultCallback iLayoutResultCallback;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("android.print.IPrintDocumentAdapter");
            str = param1Parcel1.readString();
            kill(str);
            return true;
          case 5:
            str.enforceInterface("android.print.IPrintDocumentAdapter");
            finish();
            return true;
          case 4:
            str.enforceInterface("android.print.IPrintDocumentAdapter");
            arrayOfPageRange = str.<PageRange>createTypedArray(PageRange.CREATOR);
            if (str.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)str);
            } else {
              param1Parcel2 = null;
            } 
            iWriteResultCallback = IWriteResultCallback.Stub.asInterface(str.readStrongBinder());
            param1Int1 = str.readInt();
            write(arrayOfPageRange, (ParcelFileDescriptor)param1Parcel2, iWriteResultCallback, param1Int1);
            return true;
          case 3:
            str.enforceInterface("android.print.IPrintDocumentAdapter");
            if (str.readInt() != 0) {
              PrintAttributes printAttributes = PrintAttributes.CREATOR.createFromParcel((Parcel)str);
            } else {
              param1Parcel2 = null;
            } 
            if (str.readInt() != 0) {
              PrintAttributes printAttributes = PrintAttributes.CREATOR.createFromParcel((Parcel)str);
            } else {
              arrayOfPageRange = null;
            } 
            iLayoutResultCallback = ILayoutResultCallback.Stub.asInterface(str.readStrongBinder());
            if (str.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)str);
            } else {
              iWriteResultCallback = null;
            } 
            param1Int1 = str.readInt();
            layout((PrintAttributes)param1Parcel2, (PrintAttributes)arrayOfPageRange, iLayoutResultCallback, (Bundle)iWriteResultCallback, param1Int1);
            return true;
          case 2:
            str.enforceInterface("android.print.IPrintDocumentAdapter");
            start();
            return true;
          case 1:
            break;
        } 
        str.enforceInterface("android.print.IPrintDocumentAdapter");
        IPrintDocumentAdapterObserver iPrintDocumentAdapterObserver = IPrintDocumentAdapterObserver.Stub.asInterface(str.readStrongBinder());
        setObserver(iPrintDocumentAdapterObserver);
        return true;
      } 
      param1Parcel2.writeString("android.print.IPrintDocumentAdapter");
      return true;
    }
    
    private static class Proxy implements IPrintDocumentAdapter {
      public static IPrintDocumentAdapter sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.print.IPrintDocumentAdapter";
      }
      
      public void setObserver(IPrintDocumentAdapterObserver param2IPrintDocumentAdapterObserver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.print.IPrintDocumentAdapter");
          if (param2IPrintDocumentAdapterObserver != null) {
            iBinder = param2IPrintDocumentAdapterObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IPrintDocumentAdapter.Stub.getDefaultImpl() != null) {
            IPrintDocumentAdapter.Stub.getDefaultImpl().setObserver(param2IPrintDocumentAdapterObserver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void start() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintDocumentAdapter");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IPrintDocumentAdapter.Stub.getDefaultImpl() != null) {
            IPrintDocumentAdapter.Stub.getDefaultImpl().start();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void layout(PrintAttributes param2PrintAttributes1, PrintAttributes param2PrintAttributes2, ILayoutResultCallback param2ILayoutResultCallback, Bundle param2Bundle, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.print.IPrintDocumentAdapter");
          if (param2PrintAttributes1 != null) {
            parcel.writeInt(1);
            param2PrintAttributes1.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2PrintAttributes2 != null) {
            parcel.writeInt(1);
            param2PrintAttributes2.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ILayoutResultCallback != null) {
            iBinder = param2ILayoutResultCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IPrintDocumentAdapter.Stub.getDefaultImpl() != null) {
            IPrintDocumentAdapter.Stub.getDefaultImpl().layout(param2PrintAttributes1, param2PrintAttributes2, param2ILayoutResultCallback, param2Bundle, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void write(PageRange[] param2ArrayOfPageRange, ParcelFileDescriptor param2ParcelFileDescriptor, IWriteResultCallback param2IWriteResultCallback, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.print.IPrintDocumentAdapter");
          parcel.writeTypedArray(param2ArrayOfPageRange, 0);
          if (param2ParcelFileDescriptor != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IWriteResultCallback != null) {
            iBinder = param2IWriteResultCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IPrintDocumentAdapter.Stub.getDefaultImpl() != null) {
            IPrintDocumentAdapter.Stub.getDefaultImpl().write(param2ArrayOfPageRange, param2ParcelFileDescriptor, param2IWriteResultCallback, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void finish() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintDocumentAdapter");
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IPrintDocumentAdapter.Stub.getDefaultImpl() != null) {
            IPrintDocumentAdapter.Stub.getDefaultImpl().finish();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void kill(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintDocumentAdapter");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IPrintDocumentAdapter.Stub.getDefaultImpl() != null) {
            IPrintDocumentAdapter.Stub.getDefaultImpl().kill(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPrintDocumentAdapter param1IPrintDocumentAdapter) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPrintDocumentAdapter != null) {
          Proxy.sDefaultImpl = param1IPrintDocumentAdapter;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPrintDocumentAdapter getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
