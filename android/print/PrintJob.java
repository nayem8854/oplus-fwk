package android.print;

import java.util.Objects;

public final class PrintJob {
  private PrintJobInfo mCachedInfo;
  
  private final PrintManager mPrintManager;
  
  PrintJob(PrintJobInfo paramPrintJobInfo, PrintManager paramPrintManager) {
    this.mCachedInfo = paramPrintJobInfo;
    this.mPrintManager = paramPrintManager;
  }
  
  public PrintJobId getId() {
    return this.mCachedInfo.getId();
  }
  
  public PrintJobInfo getInfo() {
    if (isInImmutableState())
      return this.mCachedInfo; 
    PrintJobInfo printJobInfo = this.mPrintManager.getPrintJobInfo(this.mCachedInfo.getId());
    if (printJobInfo != null)
      this.mCachedInfo = printJobInfo; 
    return this.mCachedInfo;
  }
  
  public void cancel() {
    int i = getInfo().getState();
    if (i == 2 || i == 3 || i == 4 || i == 6)
      this.mPrintManager.cancelPrintJob(this.mCachedInfo.getId()); 
  }
  
  public void restart() {
    if (isFailed())
      this.mPrintManager.restartPrintJob(this.mCachedInfo.getId()); 
  }
  
  public boolean isQueued() {
    boolean bool;
    if (getInfo().getState() == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isStarted() {
    boolean bool;
    if (getInfo().getState() == 3) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isBlocked() {
    boolean bool;
    if (getInfo().getState() == 4) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isCompleted() {
    boolean bool;
    if (getInfo().getState() == 5) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isFailed() {
    boolean bool;
    if (getInfo().getState() == 6) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isCancelled() {
    boolean bool;
    if (getInfo().getState() == 7) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isInImmutableState() {
    int i = this.mCachedInfo.getState();
    return (i == 5 || i == 7);
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    return Objects.equals(this.mCachedInfo.getId(), ((PrintJob)paramObject).mCachedInfo.getId());
  }
  
  public int hashCode() {
    PrintJobId printJobId = this.mCachedInfo.getId();
    if (printJobId == null)
      return 0; 
    return printJobId.hashCode();
  }
}
