package android.print;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class PrintDocumentInfo implements Parcelable {
  public static final int CONTENT_TYPE_DOCUMENT = 0;
  
  public static final int CONTENT_TYPE_PHOTO = 1;
  
  public static final int CONTENT_TYPE_UNKNOWN = -1;
  
  private PrintDocumentInfo() {}
  
  private PrintDocumentInfo(PrintDocumentInfo paramPrintDocumentInfo) {
    this.mName = paramPrintDocumentInfo.mName;
    this.mPageCount = paramPrintDocumentInfo.mPageCount;
    this.mContentType = paramPrintDocumentInfo.mContentType;
    this.mDataSize = paramPrintDocumentInfo.mDataSize;
  }
  
  private PrintDocumentInfo(Parcel paramParcel) {
    boolean bool;
    this.mName = (String)Preconditions.checkStringNotEmpty(paramParcel.readString());
    int i = paramParcel.readInt();
    if (i == -1 || i > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool);
    this.mContentType = paramParcel.readInt();
    this.mDataSize = Preconditions.checkArgumentNonnegative(paramParcel.readLong());
  }
  
  public String getName() {
    return this.mName;
  }
  
  public int getPageCount() {
    return this.mPageCount;
  }
  
  public int getContentType() {
    return this.mContentType;
  }
  
  public long getDataSize() {
    return this.mDataSize;
  }
  
  public void setDataSize(long paramLong) {
    this.mDataSize = paramLong;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mName);
    paramParcel.writeInt(this.mPageCount);
    paramParcel.writeInt(this.mContentType);
    paramParcel.writeLong(this.mDataSize);
  }
  
  public int hashCode() {
    byte b;
    String str = this.mName;
    if (str != null) {
      b = str.hashCode();
    } else {
      b = 0;
    } 
    int i = this.mContentType;
    int j = this.mPageCount;
    long l = this.mDataSize;
    int k = (int)l;
    int m = (int)(l >> 32L);
    return ((((1 * 31 + b) * 31 + i) * 31 + j) * 31 + k) * 31 + m;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (!TextUtils.equals(this.mName, ((PrintDocumentInfo)paramObject).mName))
      return false; 
    if (this.mContentType != ((PrintDocumentInfo)paramObject).mContentType)
      return false; 
    if (this.mPageCount != ((PrintDocumentInfo)paramObject).mPageCount)
      return false; 
    if (this.mDataSize != ((PrintDocumentInfo)paramObject).mDataSize)
      return false; 
    return true;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("PrintDocumentInfo{");
    stringBuilder.append("name=");
    stringBuilder.append(this.mName);
    stringBuilder.append(", pageCount=");
    stringBuilder.append(this.mPageCount);
    stringBuilder.append(", contentType=");
    stringBuilder.append(contentTypeToString(this.mContentType));
    stringBuilder.append(", dataSize=");
    stringBuilder.append(this.mDataSize);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  private String contentTypeToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1)
        return "CONTENT_TYPE_UNKNOWN"; 
      return "CONTENT_TYPE_PHOTO";
    } 
    return "CONTENT_TYPE_DOCUMENT";
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ContentType implements Annotation {}
  
  class Builder {
    private final PrintDocumentInfo mPrototype;
    
    public Builder(PrintDocumentInfo this$0) {
      if (!TextUtils.isEmpty((CharSequence)this$0)) {
        PrintDocumentInfo printDocumentInfo = new PrintDocumentInfo();
        PrintDocumentInfo.access$102(printDocumentInfo, (String)this$0);
        return;
      } 
      throw new IllegalArgumentException("name cannot be empty");
    }
    
    public Builder setPageCount(int param1Int) {
      if (param1Int >= 0 || param1Int == -1) {
        PrintDocumentInfo.access$202(this.mPrototype, param1Int);
        return this;
      } 
      throw new IllegalArgumentException("pageCount must be greater than or equal to zero or DocumentInfo#PAGE_COUNT_UNKNOWN");
    }
    
    public Builder setContentType(int param1Int) {
      PrintDocumentInfo.access$302(this.mPrototype, param1Int);
      return this;
    }
    
    public PrintDocumentInfo build() {
      if (this.mPrototype.mPageCount == 0)
        PrintDocumentInfo.access$202(this.mPrototype, -1); 
      return new PrintDocumentInfo(this.mPrototype);
    }
  }
  
  public static final Parcelable.Creator<PrintDocumentInfo> CREATOR = new Parcelable.Creator<PrintDocumentInfo>() {
      public PrintDocumentInfo createFromParcel(Parcel param1Parcel) {
        return new PrintDocumentInfo(param1Parcel);
      }
      
      public PrintDocumentInfo[] newArray(int param1Int) {
        return new PrintDocumentInfo[param1Int];
      }
    };
  
  public static final int PAGE_COUNT_UNKNOWN = -1;
  
  private int mContentType;
  
  private long mDataSize;
  
  private String mName;
  
  private int mPageCount;
}
