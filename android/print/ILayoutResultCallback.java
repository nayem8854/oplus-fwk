package android.print;

import android.os.Binder;
import android.os.IBinder;
import android.os.ICancellationSignal;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.text.TextUtils;

public interface ILayoutResultCallback extends IInterface {
  void onLayoutCanceled(int paramInt) throws RemoteException;
  
  void onLayoutFailed(CharSequence paramCharSequence, int paramInt) throws RemoteException;
  
  void onLayoutFinished(PrintDocumentInfo paramPrintDocumentInfo, boolean paramBoolean, int paramInt) throws RemoteException;
  
  void onLayoutStarted(ICancellationSignal paramICancellationSignal, int paramInt) throws RemoteException;
  
  class Default implements ILayoutResultCallback {
    public void onLayoutStarted(ICancellationSignal param1ICancellationSignal, int param1Int) throws RemoteException {}
    
    public void onLayoutFinished(PrintDocumentInfo param1PrintDocumentInfo, boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public void onLayoutFailed(CharSequence param1CharSequence, int param1Int) throws RemoteException {}
    
    public void onLayoutCanceled(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ILayoutResultCallback {
    private static final String DESCRIPTOR = "android.print.ILayoutResultCallback";
    
    static final int TRANSACTION_onLayoutCanceled = 4;
    
    static final int TRANSACTION_onLayoutFailed = 3;
    
    static final int TRANSACTION_onLayoutFinished = 2;
    
    static final int TRANSACTION_onLayoutStarted = 1;
    
    public Stub() {
      attachInterface(this, "android.print.ILayoutResultCallback");
    }
    
    public static ILayoutResultCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.print.ILayoutResultCallback");
      if (iInterface != null && iInterface instanceof ILayoutResultCallback)
        return (ILayoutResultCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onLayoutCanceled";
          } 
          return "onLayoutFailed";
        } 
        return "onLayoutFinished";
      } 
      return "onLayoutStarted";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        boolean bool;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.print.ILayoutResultCallback");
              return true;
            } 
            param1Parcel1.enforceInterface("android.print.ILayoutResultCallback");
            param1Int1 = param1Parcel1.readInt();
            onLayoutCanceled(param1Int1);
            return true;
          } 
          param1Parcel1.enforceInterface("android.print.ILayoutResultCallback");
          if (param1Parcel1.readInt() != 0) {
            CharSequence charSequence = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel2 = null;
          } 
          param1Int1 = param1Parcel1.readInt();
          onLayoutFailed((CharSequence)param1Parcel2, param1Int1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.print.ILayoutResultCallback");
        if (param1Parcel1.readInt() != 0) {
          PrintDocumentInfo printDocumentInfo = PrintDocumentInfo.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel2 = null;
        } 
        if (param1Parcel1.readInt() != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        param1Int1 = param1Parcel1.readInt();
        onLayoutFinished((PrintDocumentInfo)param1Parcel2, bool, param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.print.ILayoutResultCallback");
      ICancellationSignal iCancellationSignal = ICancellationSignal.Stub.asInterface(param1Parcel1.readStrongBinder());
      param1Int1 = param1Parcel1.readInt();
      onLayoutStarted(iCancellationSignal, param1Int1);
      return true;
    }
    
    private static class Proxy implements ILayoutResultCallback {
      public static ILayoutResultCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.print.ILayoutResultCallback";
      }
      
      public void onLayoutStarted(ICancellationSignal param2ICancellationSignal, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.print.ILayoutResultCallback");
          if (param2ICancellationSignal != null) {
            iBinder = param2ICancellationSignal.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ILayoutResultCallback.Stub.getDefaultImpl() != null) {
            ILayoutResultCallback.Stub.getDefaultImpl().onLayoutStarted(param2ICancellationSignal, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onLayoutFinished(PrintDocumentInfo param2PrintDocumentInfo, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.ILayoutResultCallback");
          boolean bool = false;
          if (param2PrintDocumentInfo != null) {
            parcel.writeInt(1);
            param2PrintDocumentInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          parcel.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool1 && ILayoutResultCallback.Stub.getDefaultImpl() != null) {
            ILayoutResultCallback.Stub.getDefaultImpl().onLayoutFinished(param2PrintDocumentInfo, param2Boolean, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onLayoutFailed(CharSequence param2CharSequence, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.ILayoutResultCallback");
          if (param2CharSequence != null) {
            parcel.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && ILayoutResultCallback.Stub.getDefaultImpl() != null) {
            ILayoutResultCallback.Stub.getDefaultImpl().onLayoutFailed(param2CharSequence, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onLayoutCanceled(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.ILayoutResultCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && ILayoutResultCallback.Stub.getDefaultImpl() != null) {
            ILayoutResultCallback.Stub.getDefaultImpl().onLayoutCanceled(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ILayoutResultCallback param1ILayoutResultCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ILayoutResultCallback != null) {
          Proxy.sDefaultImpl = param1ILayoutResultCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ILayoutResultCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
