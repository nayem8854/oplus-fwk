package android.print;

import android.content.ComponentName;
import android.graphics.drawable.Icon;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.printservice.PrintServiceInfo;
import android.printservice.recommendation.IRecommendationsChangeListener;
import android.printservice.recommendation.RecommendationInfo;
import java.util.ArrayList;
import java.util.List;

public interface IPrintManager extends IInterface {
  void addPrintJobStateChangeListener(IPrintJobStateChangeListener paramIPrintJobStateChangeListener, int paramInt1, int paramInt2) throws RemoteException;
  
  void addPrintServiceRecommendationsChangeListener(IRecommendationsChangeListener paramIRecommendationsChangeListener, int paramInt) throws RemoteException;
  
  void addPrintServicesChangeListener(IPrintServicesChangeListener paramIPrintServicesChangeListener, int paramInt) throws RemoteException;
  
  void cancelPrintJob(PrintJobId paramPrintJobId, int paramInt1, int paramInt2) throws RemoteException;
  
  void createPrinterDiscoverySession(IPrinterDiscoveryObserver paramIPrinterDiscoveryObserver, int paramInt) throws RemoteException;
  
  void destroyPrinterDiscoverySession(IPrinterDiscoveryObserver paramIPrinterDiscoveryObserver, int paramInt) throws RemoteException;
  
  boolean getBindInstantServiceAllowed(int paramInt) throws RemoteException;
  
  Icon getCustomPrinterIcon(PrinterId paramPrinterId, int paramInt) throws RemoteException;
  
  PrintJobInfo getPrintJobInfo(PrintJobId paramPrintJobId, int paramInt1, int paramInt2) throws RemoteException;
  
  List<PrintJobInfo> getPrintJobInfos(int paramInt1, int paramInt2) throws RemoteException;
  
  List<RecommendationInfo> getPrintServiceRecommendations(int paramInt) throws RemoteException;
  
  List<PrintServiceInfo> getPrintServices(int paramInt1, int paramInt2) throws RemoteException;
  
  Bundle print(String paramString1, IPrintDocumentAdapter paramIPrintDocumentAdapter, PrintAttributes paramPrintAttributes, String paramString2, int paramInt1, int paramInt2) throws RemoteException;
  
  void removePrintJobStateChangeListener(IPrintJobStateChangeListener paramIPrintJobStateChangeListener, int paramInt) throws RemoteException;
  
  void removePrintServiceRecommendationsChangeListener(IRecommendationsChangeListener paramIRecommendationsChangeListener, int paramInt) throws RemoteException;
  
  void removePrintServicesChangeListener(IPrintServicesChangeListener paramIPrintServicesChangeListener, int paramInt) throws RemoteException;
  
  void restartPrintJob(PrintJobId paramPrintJobId, int paramInt1, int paramInt2) throws RemoteException;
  
  void setBindInstantServiceAllowed(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setPrintServiceEnabled(ComponentName paramComponentName, boolean paramBoolean, int paramInt) throws RemoteException;
  
  void startPrinterDiscovery(IPrinterDiscoveryObserver paramIPrinterDiscoveryObserver, List<PrinterId> paramList, int paramInt) throws RemoteException;
  
  void startPrinterStateTracking(PrinterId paramPrinterId, int paramInt) throws RemoteException;
  
  void stopPrinterDiscovery(IPrinterDiscoveryObserver paramIPrinterDiscoveryObserver, int paramInt) throws RemoteException;
  
  void stopPrinterStateTracking(PrinterId paramPrinterId, int paramInt) throws RemoteException;
  
  void validatePrinters(List<PrinterId> paramList, int paramInt) throws RemoteException;
  
  class Default implements IPrintManager {
    public List<PrintJobInfo> getPrintJobInfos(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public PrintJobInfo getPrintJobInfo(PrintJobId param1PrintJobId, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public Bundle print(String param1String1, IPrintDocumentAdapter param1IPrintDocumentAdapter, PrintAttributes param1PrintAttributes, String param1String2, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public void cancelPrintJob(PrintJobId param1PrintJobId, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void restartPrintJob(PrintJobId param1PrintJobId, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void addPrintJobStateChangeListener(IPrintJobStateChangeListener param1IPrintJobStateChangeListener, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void removePrintJobStateChangeListener(IPrintJobStateChangeListener param1IPrintJobStateChangeListener, int param1Int) throws RemoteException {}
    
    public void addPrintServicesChangeListener(IPrintServicesChangeListener param1IPrintServicesChangeListener, int param1Int) throws RemoteException {}
    
    public void removePrintServicesChangeListener(IPrintServicesChangeListener param1IPrintServicesChangeListener, int param1Int) throws RemoteException {}
    
    public List<PrintServiceInfo> getPrintServices(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public void setPrintServiceEnabled(ComponentName param1ComponentName, boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public void addPrintServiceRecommendationsChangeListener(IRecommendationsChangeListener param1IRecommendationsChangeListener, int param1Int) throws RemoteException {}
    
    public void removePrintServiceRecommendationsChangeListener(IRecommendationsChangeListener param1IRecommendationsChangeListener, int param1Int) throws RemoteException {}
    
    public List<RecommendationInfo> getPrintServiceRecommendations(int param1Int) throws RemoteException {
      return null;
    }
    
    public void createPrinterDiscoverySession(IPrinterDiscoveryObserver param1IPrinterDiscoveryObserver, int param1Int) throws RemoteException {}
    
    public void startPrinterDiscovery(IPrinterDiscoveryObserver param1IPrinterDiscoveryObserver, List<PrinterId> param1List, int param1Int) throws RemoteException {}
    
    public void stopPrinterDiscovery(IPrinterDiscoveryObserver param1IPrinterDiscoveryObserver, int param1Int) throws RemoteException {}
    
    public void validatePrinters(List<PrinterId> param1List, int param1Int) throws RemoteException {}
    
    public void startPrinterStateTracking(PrinterId param1PrinterId, int param1Int) throws RemoteException {}
    
    public Icon getCustomPrinterIcon(PrinterId param1PrinterId, int param1Int) throws RemoteException {
      return null;
    }
    
    public void stopPrinterStateTracking(PrinterId param1PrinterId, int param1Int) throws RemoteException {}
    
    public void destroyPrinterDiscoverySession(IPrinterDiscoveryObserver param1IPrinterDiscoveryObserver, int param1Int) throws RemoteException {}
    
    public boolean getBindInstantServiceAllowed(int param1Int) throws RemoteException {
      return false;
    }
    
    public void setBindInstantServiceAllowed(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPrintManager {
    private static final String DESCRIPTOR = "android.print.IPrintManager";
    
    static final int TRANSACTION_addPrintJobStateChangeListener = 6;
    
    static final int TRANSACTION_addPrintServiceRecommendationsChangeListener = 12;
    
    static final int TRANSACTION_addPrintServicesChangeListener = 8;
    
    static final int TRANSACTION_cancelPrintJob = 4;
    
    static final int TRANSACTION_createPrinterDiscoverySession = 15;
    
    static final int TRANSACTION_destroyPrinterDiscoverySession = 22;
    
    static final int TRANSACTION_getBindInstantServiceAllowed = 23;
    
    static final int TRANSACTION_getCustomPrinterIcon = 20;
    
    static final int TRANSACTION_getPrintJobInfo = 2;
    
    static final int TRANSACTION_getPrintJobInfos = 1;
    
    static final int TRANSACTION_getPrintServiceRecommendations = 14;
    
    static final int TRANSACTION_getPrintServices = 10;
    
    static final int TRANSACTION_print = 3;
    
    static final int TRANSACTION_removePrintJobStateChangeListener = 7;
    
    static final int TRANSACTION_removePrintServiceRecommendationsChangeListener = 13;
    
    static final int TRANSACTION_removePrintServicesChangeListener = 9;
    
    static final int TRANSACTION_restartPrintJob = 5;
    
    static final int TRANSACTION_setBindInstantServiceAllowed = 24;
    
    static final int TRANSACTION_setPrintServiceEnabled = 11;
    
    static final int TRANSACTION_startPrinterDiscovery = 16;
    
    static final int TRANSACTION_startPrinterStateTracking = 19;
    
    static final int TRANSACTION_stopPrinterDiscovery = 17;
    
    static final int TRANSACTION_stopPrinterStateTracking = 21;
    
    static final int TRANSACTION_validatePrinters = 18;
    
    public Stub() {
      attachInterface(this, "android.print.IPrintManager");
    }
    
    public static IPrintManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.print.IPrintManager");
      if (iInterface != null && iInterface instanceof IPrintManager)
        return (IPrintManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 24:
          return "setBindInstantServiceAllowed";
        case 23:
          return "getBindInstantServiceAllowed";
        case 22:
          return "destroyPrinterDiscoverySession";
        case 21:
          return "stopPrinterStateTracking";
        case 20:
          return "getCustomPrinterIcon";
        case 19:
          return "startPrinterStateTracking";
        case 18:
          return "validatePrinters";
        case 17:
          return "stopPrinterDiscovery";
        case 16:
          return "startPrinterDiscovery";
        case 15:
          return "createPrinterDiscoverySession";
        case 14:
          return "getPrintServiceRecommendations";
        case 13:
          return "removePrintServiceRecommendationsChangeListener";
        case 12:
          return "addPrintServiceRecommendationsChangeListener";
        case 11:
          return "setPrintServiceEnabled";
        case 10:
          return "getPrintServices";
        case 9:
          return "removePrintServicesChangeListener";
        case 8:
          return "addPrintServicesChangeListener";
        case 7:
          return "removePrintJobStateChangeListener";
        case 6:
          return "addPrintJobStateChangeListener";
        case 5:
          return "restartPrintJob";
        case 4:
          return "cancelPrintJob";
        case 3:
          return "print";
        case 2:
          return "getPrintJobInfo";
        case 1:
          break;
      } 
      return "getPrintJobInfos";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        Icon icon;
        List<RecommendationInfo> list1;
        Bundle bundle;
        PrintJobInfo printJobInfo;
        IPrinterDiscoveryObserver iPrinterDiscoveryObserver3;
        ArrayList<PrinterId> arrayList2;
        IPrinterDiscoveryObserver iPrinterDiscoveryObserver2;
        ArrayList<PrinterId> arrayList1;
        IPrinterDiscoveryObserver iPrinterDiscoveryObserver1;
        IRecommendationsChangeListener iRecommendationsChangeListener;
        IPrintServicesChangeListener iPrintServicesChangeListener;
        IPrintJobStateChangeListener iPrintJobStateChangeListener;
        IPrinterDiscoveryObserver iPrinterDiscoveryObserver4;
        String str1;
        IPrintDocumentAdapter iPrintDocumentAdapter;
        String str2;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 24:
            param1Parcel1.enforceInterface("android.print.IPrintManager");
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0)
              bool2 = true; 
            setBindInstantServiceAllowed(param1Int1, bool2);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            param1Parcel1.enforceInterface("android.print.IPrintManager");
            param1Int1 = param1Parcel1.readInt();
            bool = getBindInstantServiceAllowed(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 22:
            param1Parcel1.enforceInterface("android.print.IPrintManager");
            iPrinterDiscoveryObserver3 = IPrinterDiscoveryObserver.Stub.asInterface(param1Parcel1.readStrongBinder());
            i = param1Parcel1.readInt();
            destroyPrinterDiscoverySession(iPrinterDiscoveryObserver3, i);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            param1Parcel1.enforceInterface("android.print.IPrintManager");
            if (param1Parcel1.readInt() != 0) {
              PrinterId printerId = PrinterId.CREATOR.createFromParcel(param1Parcel1);
            } else {
              iPrinterDiscoveryObserver3 = null;
            } 
            i = param1Parcel1.readInt();
            stopPrinterStateTracking((PrinterId)iPrinterDiscoveryObserver3, i);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            param1Parcel1.enforceInterface("android.print.IPrintManager");
            if (param1Parcel1.readInt() != 0) {
              PrinterId printerId = PrinterId.CREATOR.createFromParcel(param1Parcel1);
            } else {
              iPrinterDiscoveryObserver3 = null;
            } 
            i = param1Parcel1.readInt();
            icon = getCustomPrinterIcon((PrinterId)iPrinterDiscoveryObserver3, i);
            param1Parcel2.writeNoException();
            if (icon != null) {
              param1Parcel2.writeInt(1);
              icon.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 19:
            icon.enforceInterface("android.print.IPrintManager");
            if (icon.readInt() != 0) {
              PrinterId printerId = PrinterId.CREATOR.createFromParcel((Parcel)icon);
            } else {
              iPrinterDiscoveryObserver3 = null;
            } 
            i = icon.readInt();
            startPrinterStateTracking((PrinterId)iPrinterDiscoveryObserver3, i);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            icon.enforceInterface("android.print.IPrintManager");
            arrayList2 = icon.createTypedArrayList(PrinterId.CREATOR);
            i = icon.readInt();
            validatePrinters(arrayList2, i);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            icon.enforceInterface("android.print.IPrintManager");
            iPrinterDiscoveryObserver2 = IPrinterDiscoveryObserver.Stub.asInterface(icon.readStrongBinder());
            i = icon.readInt();
            stopPrinterDiscovery(iPrinterDiscoveryObserver2, i);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            icon.enforceInterface("android.print.IPrintManager");
            iPrinterDiscoveryObserver4 = IPrinterDiscoveryObserver.Stub.asInterface(icon.readStrongBinder());
            arrayList1 = icon.createTypedArrayList(PrinterId.CREATOR);
            i = icon.readInt();
            startPrinterDiscovery(iPrinterDiscoveryObserver4, arrayList1, i);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            icon.enforceInterface("android.print.IPrintManager");
            iPrinterDiscoveryObserver1 = IPrinterDiscoveryObserver.Stub.asInterface(icon.readStrongBinder());
            i = icon.readInt();
            createPrinterDiscoverySession(iPrinterDiscoveryObserver1, i);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            icon.enforceInterface("android.print.IPrintManager");
            i = icon.readInt();
            list1 = getPrintServiceRecommendations(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list1);
            return true;
          case 13:
            list1.enforceInterface("android.print.IPrintManager");
            iRecommendationsChangeListener = IRecommendationsChangeListener.Stub.asInterface(list1.readStrongBinder());
            i = list1.readInt();
            removePrintServiceRecommendationsChangeListener(iRecommendationsChangeListener, i);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            list1.enforceInterface("android.print.IPrintManager");
            iRecommendationsChangeListener = IRecommendationsChangeListener.Stub.asInterface(list1.readStrongBinder());
            i = list1.readInt();
            addPrintServiceRecommendationsChangeListener(iRecommendationsChangeListener, i);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            list1.enforceInterface("android.print.IPrintManager");
            if (list1.readInt() != 0) {
              ComponentName componentName = ComponentName.CREATOR.createFromParcel((Parcel)list1);
            } else {
              iRecommendationsChangeListener = null;
            } 
            bool2 = bool1;
            if (list1.readInt() != 0)
              bool2 = true; 
            i = list1.readInt();
            setPrintServiceEnabled((ComponentName)iRecommendationsChangeListener, bool2, i);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            list1.enforceInterface("android.print.IPrintManager");
            param1Int2 = list1.readInt();
            i = list1.readInt();
            list1 = (List)getPrintServices(param1Int2, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list1);
            return true;
          case 9:
            list1.enforceInterface("android.print.IPrintManager");
            iPrintServicesChangeListener = IPrintServicesChangeListener.Stub.asInterface(list1.readStrongBinder());
            i = list1.readInt();
            removePrintServicesChangeListener(iPrintServicesChangeListener, i);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            list1.enforceInterface("android.print.IPrintManager");
            iPrintServicesChangeListener = IPrintServicesChangeListener.Stub.asInterface(list1.readStrongBinder());
            i = list1.readInt();
            addPrintServicesChangeListener(iPrintServicesChangeListener, i);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            list1.enforceInterface("android.print.IPrintManager");
            iPrintJobStateChangeListener = IPrintJobStateChangeListener.Stub.asInterface(list1.readStrongBinder());
            i = list1.readInt();
            removePrintJobStateChangeListener(iPrintJobStateChangeListener, i);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            list1.enforceInterface("android.print.IPrintManager");
            iPrintJobStateChangeListener = IPrintJobStateChangeListener.Stub.asInterface(list1.readStrongBinder());
            i = list1.readInt();
            param1Int2 = list1.readInt();
            addPrintJobStateChangeListener(iPrintJobStateChangeListener, i, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            list1.enforceInterface("android.print.IPrintManager");
            if (list1.readInt() != 0) {
              PrintJobId printJobId = PrintJobId.CREATOR.createFromParcel((Parcel)list1);
            } else {
              iPrintJobStateChangeListener = null;
            } 
            i = list1.readInt();
            param1Int2 = list1.readInt();
            restartPrintJob((PrintJobId)iPrintJobStateChangeListener, i, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            list1.enforceInterface("android.print.IPrintManager");
            if (list1.readInt() != 0) {
              PrintJobId printJobId = PrintJobId.CREATOR.createFromParcel((Parcel)list1);
            } else {
              iPrintJobStateChangeListener = null;
            } 
            i = list1.readInt();
            param1Int2 = list1.readInt();
            cancelPrintJob((PrintJobId)iPrintJobStateChangeListener, i, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            list1.enforceInterface("android.print.IPrintManager");
            str1 = list1.readString();
            iPrintDocumentAdapter = IPrintDocumentAdapter.Stub.asInterface(list1.readStrongBinder());
            if (list1.readInt() != 0) {
              PrintAttributes printAttributes = PrintAttributes.CREATOR.createFromParcel((Parcel)list1);
            } else {
              iPrintJobStateChangeListener = null;
            } 
            str2 = list1.readString();
            param1Int2 = list1.readInt();
            i = list1.readInt();
            bundle = print(str1, iPrintDocumentAdapter, (PrintAttributes)iPrintJobStateChangeListener, str2, param1Int2, i);
            param1Parcel2.writeNoException();
            if (bundle != null) {
              param1Parcel2.writeInt(1);
              bundle.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 2:
            bundle.enforceInterface("android.print.IPrintManager");
            if (bundle.readInt() != 0) {
              PrintJobId printJobId = PrintJobId.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              iPrintJobStateChangeListener = null;
            } 
            i = bundle.readInt();
            param1Int2 = bundle.readInt();
            printJobInfo = getPrintJobInfo((PrintJobId)iPrintJobStateChangeListener, i, param1Int2);
            param1Parcel2.writeNoException();
            if (printJobInfo != null) {
              param1Parcel2.writeInt(1);
              printJobInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        printJobInfo.enforceInterface("android.print.IPrintManager");
        int i = printJobInfo.readInt();
        param1Int2 = printJobInfo.readInt();
        List<PrintJobInfo> list = getPrintJobInfos(i, param1Int2);
        param1Parcel2.writeNoException();
        param1Parcel2.writeTypedList(list);
        return true;
      } 
      param1Parcel2.writeString("android.print.IPrintManager");
      return true;
    }
    
    private static class Proxy implements IPrintManager {
      public static IPrintManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.print.IPrintManager";
      }
      
      public List<PrintJobInfo> getPrintJobInfos(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IPrintManager.Stub.getDefaultImpl() != null)
            return IPrintManager.Stub.getDefaultImpl().getPrintJobInfos(param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.createTypedArrayList(PrintJobInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PrintJobInfo getPrintJobInfo(PrintJobId param2PrintJobId, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          if (param2PrintJobId != null) {
            parcel1.writeInt(1);
            param2PrintJobId.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IPrintManager.Stub.getDefaultImpl() != null)
            return IPrintManager.Stub.getDefaultImpl().getPrintJobInfo(param2PrintJobId, param2Int1, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            PrintJobInfo printJobInfo = PrintJobInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2PrintJobId = null;
          } 
          return (PrintJobInfo)param2PrintJobId;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle print(String param2String1, IPrintDocumentAdapter param2IPrintDocumentAdapter, PrintAttributes param2PrintAttributes, String param2String2, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          try {
            IBinder iBinder;
            parcel1.writeString(param2String1);
            if (param2IPrintDocumentAdapter != null) {
              iBinder = param2IPrintDocumentAdapter.asBinder();
            } else {
              iBinder = null;
            } 
            parcel1.writeStrongBinder(iBinder);
            if (param2PrintAttributes != null) {
              parcel1.writeInt(1);
              param2PrintAttributes.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  parcel1.writeInt(param2Int2);
                  try {
                    boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
                    if (!bool && IPrintManager.Stub.getDefaultImpl() != null) {
                      Bundle bundle = IPrintManager.Stub.getDefaultImpl().print(param2String1, param2IPrintDocumentAdapter, param2PrintAttributes, param2String2, param2Int1, param2Int2);
                      parcel2.recycle();
                      parcel1.recycle();
                      return bundle;
                    } 
                    parcel2.readException();
                    if (parcel2.readInt() != 0) {
                      Bundle bundle = Bundle.CREATOR.createFromParcel(parcel2);
                    } else {
                      param2String1 = null;
                    } 
                    parcel2.recycle();
                    parcel1.recycle();
                    return (Bundle)param2String1;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void cancelPrintJob(PrintJobId param2PrintJobId, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          if (param2PrintJobId != null) {
            parcel1.writeInt(1);
            param2PrintJobId.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IPrintManager.Stub.getDefaultImpl() != null) {
            IPrintManager.Stub.getDefaultImpl().cancelPrintJob(param2PrintJobId, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void restartPrintJob(PrintJobId param2PrintJobId, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          if (param2PrintJobId != null) {
            parcel1.writeInt(1);
            param2PrintJobId.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IPrintManager.Stub.getDefaultImpl() != null) {
            IPrintManager.Stub.getDefaultImpl().restartPrintJob(param2PrintJobId, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addPrintJobStateChangeListener(IPrintJobStateChangeListener param2IPrintJobStateChangeListener, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          if (param2IPrintJobStateChangeListener != null) {
            iBinder = param2IPrintJobStateChangeListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IPrintManager.Stub.getDefaultImpl() != null) {
            IPrintManager.Stub.getDefaultImpl().addPrintJobStateChangeListener(param2IPrintJobStateChangeListener, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removePrintJobStateChangeListener(IPrintJobStateChangeListener param2IPrintJobStateChangeListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          if (param2IPrintJobStateChangeListener != null) {
            iBinder = param2IPrintJobStateChangeListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IPrintManager.Stub.getDefaultImpl() != null) {
            IPrintManager.Stub.getDefaultImpl().removePrintJobStateChangeListener(param2IPrintJobStateChangeListener, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addPrintServicesChangeListener(IPrintServicesChangeListener param2IPrintServicesChangeListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          if (param2IPrintServicesChangeListener != null) {
            iBinder = param2IPrintServicesChangeListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IPrintManager.Stub.getDefaultImpl() != null) {
            IPrintManager.Stub.getDefaultImpl().addPrintServicesChangeListener(param2IPrintServicesChangeListener, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removePrintServicesChangeListener(IPrintServicesChangeListener param2IPrintServicesChangeListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          if (param2IPrintServicesChangeListener != null) {
            iBinder = param2IPrintServicesChangeListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IPrintManager.Stub.getDefaultImpl() != null) {
            IPrintManager.Stub.getDefaultImpl().removePrintServicesChangeListener(param2IPrintServicesChangeListener, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<PrintServiceInfo> getPrintServices(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IPrintManager.Stub.getDefaultImpl() != null)
            return IPrintManager.Stub.getDefaultImpl().getPrintServices(param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.createTypedArrayList(PrintServiceInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPrintServiceEnabled(ComponentName param2ComponentName, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool1 && IPrintManager.Stub.getDefaultImpl() != null) {
            IPrintManager.Stub.getDefaultImpl().setPrintServiceEnabled(param2ComponentName, param2Boolean, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addPrintServiceRecommendationsChangeListener(IRecommendationsChangeListener param2IRecommendationsChangeListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          if (param2IRecommendationsChangeListener != null) {
            iBinder = param2IRecommendationsChangeListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IPrintManager.Stub.getDefaultImpl() != null) {
            IPrintManager.Stub.getDefaultImpl().addPrintServiceRecommendationsChangeListener(param2IRecommendationsChangeListener, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removePrintServiceRecommendationsChangeListener(IRecommendationsChangeListener param2IRecommendationsChangeListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          if (param2IRecommendationsChangeListener != null) {
            iBinder = param2IRecommendationsChangeListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IPrintManager.Stub.getDefaultImpl() != null) {
            IPrintManager.Stub.getDefaultImpl().removePrintServiceRecommendationsChangeListener(param2IRecommendationsChangeListener, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<RecommendationInfo> getPrintServiceRecommendations(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IPrintManager.Stub.getDefaultImpl() != null)
            return IPrintManager.Stub.getDefaultImpl().getPrintServiceRecommendations(param2Int); 
          parcel2.readException();
          return parcel2.createTypedArrayList(RecommendationInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void createPrinterDiscoverySession(IPrinterDiscoveryObserver param2IPrinterDiscoveryObserver, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          if (param2IPrinterDiscoveryObserver != null) {
            iBinder = param2IPrinterDiscoveryObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IPrintManager.Stub.getDefaultImpl() != null) {
            IPrintManager.Stub.getDefaultImpl().createPrinterDiscoverySession(param2IPrinterDiscoveryObserver, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startPrinterDiscovery(IPrinterDiscoveryObserver param2IPrinterDiscoveryObserver, List<PrinterId> param2List, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          if (param2IPrinterDiscoveryObserver != null) {
            iBinder = param2IPrinterDiscoveryObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeTypedList(param2List);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IPrintManager.Stub.getDefaultImpl() != null) {
            IPrintManager.Stub.getDefaultImpl().startPrinterDiscovery(param2IPrinterDiscoveryObserver, param2List, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopPrinterDiscovery(IPrinterDiscoveryObserver param2IPrinterDiscoveryObserver, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          if (param2IPrinterDiscoveryObserver != null) {
            iBinder = param2IPrinterDiscoveryObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IPrintManager.Stub.getDefaultImpl() != null) {
            IPrintManager.Stub.getDefaultImpl().stopPrinterDiscovery(param2IPrinterDiscoveryObserver, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void validatePrinters(List<PrinterId> param2List, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          parcel1.writeTypedList(param2List);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IPrintManager.Stub.getDefaultImpl() != null) {
            IPrintManager.Stub.getDefaultImpl().validatePrinters(param2List, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startPrinterStateTracking(PrinterId param2PrinterId, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          if (param2PrinterId != null) {
            parcel1.writeInt(1);
            param2PrinterId.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IPrintManager.Stub.getDefaultImpl() != null) {
            IPrintManager.Stub.getDefaultImpl().startPrinterStateTracking(param2PrinterId, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Icon getCustomPrinterIcon(PrinterId param2PrinterId, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          if (param2PrinterId != null) {
            parcel1.writeInt(1);
            param2PrinterId.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IPrintManager.Stub.getDefaultImpl() != null)
            return IPrintManager.Stub.getDefaultImpl().getCustomPrinterIcon(param2PrinterId, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Icon icon = Icon.CREATOR.createFromParcel(parcel2);
          } else {
            param2PrinterId = null;
          } 
          return (Icon)param2PrinterId;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopPrinterStateTracking(PrinterId param2PrinterId, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          if (param2PrinterId != null) {
            parcel1.writeInt(1);
            param2PrinterId.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IPrintManager.Stub.getDefaultImpl() != null) {
            IPrintManager.Stub.getDefaultImpl().stopPrinterStateTracking(param2PrinterId, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void destroyPrinterDiscoverySession(IPrinterDiscoveryObserver param2IPrinterDiscoveryObserver, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          if (param2IPrinterDiscoveryObserver != null) {
            iBinder = param2IPrinterDiscoveryObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IPrintManager.Stub.getDefaultImpl() != null) {
            IPrintManager.Stub.getDefaultImpl().destroyPrinterDiscoverySession(param2IPrinterDiscoveryObserver, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getBindInstantServiceAllowed(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(23, parcel1, parcel2, 0);
          if (!bool2 && IPrintManager.Stub.getDefaultImpl() != null) {
            bool1 = IPrintManager.Stub.getDefaultImpl().getBindInstantServiceAllowed(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setBindInstantServiceAllowed(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.print.IPrintManager");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool1 && IPrintManager.Stub.getDefaultImpl() != null) {
            IPrintManager.Stub.getDefaultImpl().setBindInstantServiceAllowed(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPrintManager param1IPrintManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPrintManager != null) {
          Proxy.sDefaultImpl = param1IPrintManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPrintManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
