package android.print;

import android.graphics.drawable.Icon;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IPrintSpoolerCallbacks extends IInterface {
  void customPrinterIconCacheCleared(int paramInt) throws RemoteException;
  
  void onCancelPrintJobResult(boolean paramBoolean, int paramInt) throws RemoteException;
  
  void onCustomPrinterIconCached(int paramInt) throws RemoteException;
  
  void onGetCustomPrinterIconResult(Icon paramIcon, int paramInt) throws RemoteException;
  
  void onGetPrintJobInfoResult(PrintJobInfo paramPrintJobInfo, int paramInt) throws RemoteException;
  
  void onGetPrintJobInfosResult(List<PrintJobInfo> paramList, int paramInt) throws RemoteException;
  
  void onSetPrintJobStateResult(boolean paramBoolean, int paramInt) throws RemoteException;
  
  void onSetPrintJobTagResult(boolean paramBoolean, int paramInt) throws RemoteException;
  
  class Default implements IPrintSpoolerCallbacks {
    public void onGetPrintJobInfosResult(List<PrintJobInfo> param1List, int param1Int) throws RemoteException {}
    
    public void onCancelPrintJobResult(boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public void onSetPrintJobStateResult(boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public void onSetPrintJobTagResult(boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public void onGetPrintJobInfoResult(PrintJobInfo param1PrintJobInfo, int param1Int) throws RemoteException {}
    
    public void onGetCustomPrinterIconResult(Icon param1Icon, int param1Int) throws RemoteException {}
    
    public void onCustomPrinterIconCached(int param1Int) throws RemoteException {}
    
    public void customPrinterIconCacheCleared(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPrintSpoolerCallbacks {
    private static final String DESCRIPTOR = "android.print.IPrintSpoolerCallbacks";
    
    static final int TRANSACTION_customPrinterIconCacheCleared = 8;
    
    static final int TRANSACTION_onCancelPrintJobResult = 2;
    
    static final int TRANSACTION_onCustomPrinterIconCached = 7;
    
    static final int TRANSACTION_onGetCustomPrinterIconResult = 6;
    
    static final int TRANSACTION_onGetPrintJobInfoResult = 5;
    
    static final int TRANSACTION_onGetPrintJobInfosResult = 1;
    
    static final int TRANSACTION_onSetPrintJobStateResult = 3;
    
    static final int TRANSACTION_onSetPrintJobTagResult = 4;
    
    public Stub() {
      attachInterface(this, "android.print.IPrintSpoolerCallbacks");
    }
    
    public static IPrintSpoolerCallbacks asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.print.IPrintSpoolerCallbacks");
      if (iInterface != null && iInterface instanceof IPrintSpoolerCallbacks)
        return (IPrintSpoolerCallbacks)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "customPrinterIconCacheCleared";
        case 7:
          return "onCustomPrinterIconCached";
        case 6:
          return "onGetCustomPrinterIconResult";
        case 5:
          return "onGetPrintJobInfoResult";
        case 4:
          return "onSetPrintJobTagResult";
        case 3:
          return "onSetPrintJobStateResult";
        case 2:
          return "onCancelPrintJobResult";
        case 1:
          break;
      } 
      return "onGetPrintJobInfosResult";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ArrayList<PrintJobInfo> arrayList;
      if (param1Int1 != 1598968902) {
        boolean bool1 = false, bool2 = false, bool3 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("android.print.IPrintSpoolerCallbacks");
            param1Int1 = param1Parcel1.readInt();
            customPrinterIconCacheCleared(param1Int1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.print.IPrintSpoolerCallbacks");
            param1Int1 = param1Parcel1.readInt();
            onCustomPrinterIconCached(param1Int1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.print.IPrintSpoolerCallbacks");
            if (param1Parcel1.readInt() != 0) {
              Icon icon = Icon.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            onGetCustomPrinterIconResult((Icon)param1Parcel2, param1Int1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.print.IPrintSpoolerCallbacks");
            if (param1Parcel1.readInt() != 0) {
              PrintJobInfo printJobInfo = PrintJobInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            onGetPrintJobInfoResult((PrintJobInfo)param1Parcel2, param1Int1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.print.IPrintSpoolerCallbacks");
            if (param1Parcel1.readInt() != 0)
              bool3 = true; 
            param1Int1 = param1Parcel1.readInt();
            onSetPrintJobTagResult(bool3, param1Int1);
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.print.IPrintSpoolerCallbacks");
            bool3 = bool1;
            if (param1Parcel1.readInt() != 0)
              bool3 = true; 
            param1Int1 = param1Parcel1.readInt();
            onSetPrintJobStateResult(bool3, param1Int1);
            return true;
          case 2:
            param1Parcel1.enforceInterface("android.print.IPrintSpoolerCallbacks");
            bool3 = bool2;
            if (param1Parcel1.readInt() != 0)
              bool3 = true; 
            param1Int1 = param1Parcel1.readInt();
            onCancelPrintJobResult(bool3, param1Int1);
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("android.print.IPrintSpoolerCallbacks");
        arrayList = param1Parcel1.createTypedArrayList(PrintJobInfo.CREATOR);
        param1Int1 = param1Parcel1.readInt();
        onGetPrintJobInfosResult(arrayList, param1Int1);
        return true;
      } 
      arrayList.writeString("android.print.IPrintSpoolerCallbacks");
      return true;
    }
    
    private static class Proxy implements IPrintSpoolerCallbacks {
      public static IPrintSpoolerCallbacks sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.print.IPrintSpoolerCallbacks";
      }
      
      public void onGetPrintJobInfosResult(List<PrintJobInfo> param2List, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintSpoolerCallbacks");
          parcel.writeTypedList(param2List);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpoolerCallbacks.Stub.getDefaultImpl() != null) {
            IPrintSpoolerCallbacks.Stub.getDefaultImpl().onGetPrintJobInfosResult(param2List, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCancelPrintJobResult(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.print.IPrintSpoolerCallbacks");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool1 && IPrintSpoolerCallbacks.Stub.getDefaultImpl() != null) {
            IPrintSpoolerCallbacks.Stub.getDefaultImpl().onCancelPrintJobResult(param2Boolean, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSetPrintJobStateResult(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.print.IPrintSpoolerCallbacks");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool1 && IPrintSpoolerCallbacks.Stub.getDefaultImpl() != null) {
            IPrintSpoolerCallbacks.Stub.getDefaultImpl().onSetPrintJobStateResult(param2Boolean, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSetPrintJobTagResult(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.print.IPrintSpoolerCallbacks");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool1 && IPrintSpoolerCallbacks.Stub.getDefaultImpl() != null) {
            IPrintSpoolerCallbacks.Stub.getDefaultImpl().onSetPrintJobTagResult(param2Boolean, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onGetPrintJobInfoResult(PrintJobInfo param2PrintJobInfo, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintSpoolerCallbacks");
          if (param2PrintJobInfo != null) {
            parcel.writeInt(1);
            param2PrintJobInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpoolerCallbacks.Stub.getDefaultImpl() != null) {
            IPrintSpoolerCallbacks.Stub.getDefaultImpl().onGetPrintJobInfoResult(param2PrintJobInfo, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onGetCustomPrinterIconResult(Icon param2Icon, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintSpoolerCallbacks");
          if (param2Icon != null) {
            parcel.writeInt(1);
            param2Icon.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpoolerCallbacks.Stub.getDefaultImpl() != null) {
            IPrintSpoolerCallbacks.Stub.getDefaultImpl().onGetCustomPrinterIconResult(param2Icon, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCustomPrinterIconCached(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintSpoolerCallbacks");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpoolerCallbacks.Stub.getDefaultImpl() != null) {
            IPrintSpoolerCallbacks.Stub.getDefaultImpl().onCustomPrinterIconCached(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void customPrinterIconCacheCleared(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintSpoolerCallbacks");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && IPrintSpoolerCallbacks.Stub.getDefaultImpl() != null) {
            IPrintSpoolerCallbacks.Stub.getDefaultImpl().customPrinterIconCacheCleared(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPrintSpoolerCallbacks param1IPrintSpoolerCallbacks) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPrintSpoolerCallbacks != null) {
          Proxy.sDefaultImpl = param1IPrintSpoolerCallbacks;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPrintSpoolerCallbacks getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
