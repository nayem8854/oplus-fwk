package android.print;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPrintJobStateChangeListener extends IInterface {
  void onPrintJobStateChanged(PrintJobId paramPrintJobId) throws RemoteException;
  
  class Default implements IPrintJobStateChangeListener {
    public void onPrintJobStateChanged(PrintJobId param1PrintJobId) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPrintJobStateChangeListener {
    private static final String DESCRIPTOR = "android.print.IPrintJobStateChangeListener";
    
    static final int TRANSACTION_onPrintJobStateChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.print.IPrintJobStateChangeListener");
    }
    
    public static IPrintJobStateChangeListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.print.IPrintJobStateChangeListener");
      if (iInterface != null && iInterface instanceof IPrintJobStateChangeListener)
        return (IPrintJobStateChangeListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onPrintJobStateChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.print.IPrintJobStateChangeListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.print.IPrintJobStateChangeListener");
      if (param1Parcel1.readInt() != 0) {
        PrintJobId printJobId = PrintJobId.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onPrintJobStateChanged((PrintJobId)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IPrintJobStateChangeListener {
      public static IPrintJobStateChangeListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.print.IPrintJobStateChangeListener";
      }
      
      public void onPrintJobStateChanged(PrintJobId param2PrintJobId) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.print.IPrintJobStateChangeListener");
          if (param2PrintJobId != null) {
            parcel.writeInt(1);
            param2PrintJobId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IPrintJobStateChangeListener.Stub.getDefaultImpl() != null) {
            IPrintJobStateChangeListener.Stub.getDefaultImpl().onPrintJobStateChanged(param2PrintJobId);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPrintJobStateChangeListener param1IPrintJobStateChangeListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPrintJobStateChangeListener != null) {
          Proxy.sDefaultImpl = param1IPrintJobStateChangeListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPrintJobStateChangeListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
