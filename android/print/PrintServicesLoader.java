package android.print;

import android.content.Context;
import android.content.Loader;
import android.os.Handler;
import android.os.Message;
import android.printservice.PrintServiceInfo;
import com.android.internal.util.Preconditions;
import java.util.List;

public class PrintServicesLoader extends Loader<List<PrintServiceInfo>> {
  private final Handler mHandler;
  
  private PrintManager.PrintServicesChangeListener mListener;
  
  private final PrintManager mPrintManager;
  
  private final int mSelectionFlags;
  
  public PrintServicesLoader(PrintManager paramPrintManager, Context paramContext, int paramInt) {
    super((Context)Preconditions.checkNotNull(paramContext));
    this.mHandler = new MyHandler();
    this.mPrintManager = (PrintManager)Preconditions.checkNotNull(paramPrintManager);
    this.mSelectionFlags = Preconditions.checkFlagsArgument(paramInt, 3);
  }
  
  protected void onForceLoad() {
    queueNewResult();
  }
  
  private void queueNewResult() {
    Message message = this.mHandler.obtainMessage(0);
    message.obj = this.mPrintManager.getPrintServices(this.mSelectionFlags);
    this.mHandler.sendMessage(message);
  }
  
  protected void onStartLoading() {
    PrintManager.PrintServicesChangeListener printServicesChangeListener = new PrintManager.PrintServicesChangeListener() {
        final PrintServicesLoader this$0;
        
        public void onPrintServicesChanged() {
          PrintServicesLoader.this.queueNewResult();
        }
      };
    this.mPrintManager.addPrintServicesChangeListener(printServicesChangeListener, null);
    deliverResult(this.mPrintManager.getPrintServices(this.mSelectionFlags));
  }
  
  protected void onStopLoading() {
    PrintManager.PrintServicesChangeListener printServicesChangeListener = this.mListener;
    if (printServicesChangeListener != null) {
      this.mPrintManager.removePrintServicesChangeListener(printServicesChangeListener);
      this.mListener = null;
    } 
    this.mHandler.removeMessages(0);
  }
  
  protected void onReset() {
    onStopLoading();
  }
  
  private class MyHandler extends Handler {
    final PrintServicesLoader this$0;
    
    public MyHandler() {
      super(PrintServicesLoader.this.getContext().getMainLooper());
    }
    
    public void handleMessage(Message param1Message) {
      if (PrintServicesLoader.this.isStarted())
        PrintServicesLoader.this.deliverResult(param1Message.obj); 
    }
  }
}
