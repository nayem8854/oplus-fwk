package android.print;

import android.os.Parcel;
import android.os.Parcelable;

public final class PageRange implements Parcelable {
  public static final PageRange ALL_PAGES;
  
  public static final PageRange[] ALL_PAGES_ARRAY;
  
  static {
    PageRange pageRange = new PageRange(0, 2147483647);
    ALL_PAGES_ARRAY = new PageRange[] { pageRange };
  }
  
  public PageRange(int paramInt1, int paramInt2) {
    if (paramInt1 >= 0) {
      if (paramInt2 >= 0) {
        if (paramInt1 <= paramInt2) {
          this.mStart = paramInt1;
          this.mEnd = paramInt2;
          return;
        } 
        throw new IllegalArgumentException("start must be lesser than end.");
      } 
      throw new IllegalArgumentException("end cannot be less than zero.");
    } 
    throw new IllegalArgumentException("start cannot be less than zero.");
  }
  
  private PageRange(Parcel paramParcel) {
    this(paramParcel.readInt(), paramParcel.readInt());
  }
  
  public int getStart() {
    return this.mStart;
  }
  
  public int getEnd() {
    return this.mEnd;
  }
  
  public boolean contains(int paramInt) {
    boolean bool;
    if (paramInt >= this.mStart && paramInt <= this.mEnd) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getSize() {
    return this.mEnd - this.mStart + 1;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mStart);
    paramParcel.writeInt(this.mEnd);
  }
  
  public int hashCode() {
    int i = this.mEnd;
    int j = this.mStart;
    return (1 * 31 + i) * 31 + j;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mEnd != ((PageRange)paramObject).mEnd)
      return false; 
    if (this.mStart != ((PageRange)paramObject).mStart)
      return false; 
    return true;
  }
  
  public String toString() {
    if (this.mStart == 0 && this.mEnd == Integer.MAX_VALUE)
      return "PageRange[<all pages>]"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("PageRange[");
    int i = this.mStart;
    stringBuilder.append(i);
    stringBuilder.append(" - ");
    i = this.mEnd;
    stringBuilder.append(i);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<PageRange> CREATOR = new Parcelable.Creator<PageRange>() {
      public PageRange createFromParcel(Parcel param1Parcel) {
        return new PageRange(param1Parcel);
      }
      
      public PageRange[] newArray(int param1Int) {
        return new PageRange[param1Int];
      }
    };
  
  private final int mEnd;
  
  private final int mStart;
}
