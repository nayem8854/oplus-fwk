package android.print.pdf;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.pdf.PdfDocument;
import android.print.PrintAttributes;

public class PrintedPdfDocument extends PdfDocument {
  private static final int MILS_PER_INCH = 1000;
  
  private static final int POINTS_IN_INCH = 72;
  
  private final Rect mContentRect;
  
  private final int mPageHeight;
  
  private final int mPageWidth;
  
  public PrintedPdfDocument(Context paramContext, PrintAttributes paramPrintAttributes) {
    PrintAttributes.MediaSize mediaSize = paramPrintAttributes.getMediaSize();
    this.mPageWidth = (int)(mediaSize.getWidthMils() / 1000.0F * 72.0F);
    this.mPageHeight = (int)(mediaSize.getHeightMils() / 1000.0F * 72.0F);
    PrintAttributes.Margins margins = paramPrintAttributes.getMinMargins();
    int i = (int)(margins.getLeftMils() / 1000.0F * 72.0F);
    int j = (int)(margins.getTopMils() / 1000.0F * 72.0F);
    int k = (int)(margins.getRightMils() / 1000.0F * 72.0F);
    int m = (int)(margins.getBottomMils() / 1000.0F * 72.0F);
    this.mContentRect = new Rect(i, j, this.mPageWidth - k, this.mPageHeight - m);
  }
  
  public PdfDocument.Page startPage(int paramInt) {
    PdfDocument.PageInfo.Builder builder = new PdfDocument.PageInfo.Builder(this.mPageWidth, this.mPageHeight, paramInt);
    Rect rect = this.mContentRect;
    builder = builder.setContentRect(rect);
    PdfDocument.PageInfo pageInfo = builder.create();
    return startPage(pageInfo);
  }
  
  public int getPageWidth() {
    return this.mPageWidth;
  }
  
  public int getPageHeight() {
    return this.mPageHeight;
  }
  
  public Rect getPageContentRect() {
    return this.mContentRect;
  }
}
