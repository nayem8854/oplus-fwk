package android.util;

import com.android.internal.util.ArrayUtils;
import com.android.internal.util.GrowingArrayUtils;
import libcore.util.EmptyArray;

public class SparseArray<E> implements Cloneable {
  private static final Object DELETED = new Object();
  
  private boolean mGarbage = false;
  
  private int[] mKeys;
  
  private int mSize;
  
  private Object[] mValues;
  
  public SparseArray() {
    this(10);
  }
  
  public SparseArray(int paramInt) {
    if (paramInt == 0) {
      this.mKeys = EmptyArray.INT;
      this.mValues = EmptyArray.OBJECT;
    } else {
      Object[] arrayOfObject = ArrayUtils.newUnpaddedObjectArray(paramInt);
      this.mKeys = new int[arrayOfObject.length];
    } 
    this.mSize = 0;
  }
  
  public SparseArray<E> clone() {
    SparseArray<E> sparseArray = null;
    try {
      SparseArray<E> sparseArray1 = (SparseArray)super.clone();
      sparseArray = sparseArray1;
      sparseArray1.mKeys = (int[])this.mKeys.clone();
      sparseArray = sparseArray1;
      sparseArray1.mValues = (Object[])this.mValues.clone();
      sparseArray = sparseArray1;
    } catch (CloneNotSupportedException cloneNotSupportedException) {}
    return sparseArray;
  }
  
  public boolean contains(int paramInt) {
    boolean bool;
    if (indexOfKey(paramInt) >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public E get(int paramInt) {
    return get(paramInt, null);
  }
  
  public E get(int paramInt, E paramE) {
    paramInt = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt);
    if (paramInt >= 0) {
      Object[] arrayOfObject = this.mValues;
      if (arrayOfObject[paramInt] != DELETED)
        return (E)arrayOfObject[paramInt]; 
    } 
    return paramE;
  }
  
  public void delete(int paramInt) {
    paramInt = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt);
    if (paramInt >= 0) {
      Object arrayOfObject[] = this.mValues, object1 = arrayOfObject[paramInt], object2 = DELETED;
      if (object1 != object2) {
        arrayOfObject[paramInt] = object2;
        this.mGarbage = true;
      } 
    } 
  }
  
  public E removeReturnOld(int paramInt) {
    paramInt = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt);
    if (paramInt >= 0) {
      Object arrayOfObject[] = this.mValues, object1 = arrayOfObject[paramInt], object2 = DELETED;
      if (object1 != object2) {
        object1 = arrayOfObject[paramInt];
        arrayOfObject[paramInt] = object2;
        this.mGarbage = true;
        return (E)object1;
      } 
    } 
    return null;
  }
  
  public void remove(int paramInt) {
    delete(paramInt);
  }
  
  public void removeAt(int paramInt) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds) {
      Object arrayOfObject[] = this.mValues, object1 = arrayOfObject[paramInt], object2 = DELETED;
      if (object1 != object2) {
        arrayOfObject[paramInt] = object2;
        this.mGarbage = true;
      } 
      return;
    } 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public void removeAtRange(int paramInt1, int paramInt2) {
    paramInt2 = Math.min(this.mSize, paramInt1 + paramInt2);
    for (; paramInt1 < paramInt2; paramInt1++)
      removeAt(paramInt1); 
  }
  
  private void gc() {
    int i = this.mSize;
    int j = 0;
    int[] arrayOfInt = this.mKeys;
    Object[] arrayOfObject = this.mValues;
    for (int k = 0; k < i; k++, j = m) {
      Object object = arrayOfObject[k];
      int m = j;
      if (object != DELETED) {
        if (k != j) {
          arrayOfInt[j] = arrayOfInt[k];
          arrayOfObject[j] = object;
          arrayOfObject[k] = null;
        } 
        m = j + 1;
      } 
    } 
    this.mGarbage = false;
    this.mSize = j;
  }
  
  public void put(int paramInt, E paramE) {
    int i = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt);
    if (i >= 0) {
      this.mValues[i] = paramE;
    } else {
      int j = i ^ 0xFFFFFFFF;
      if (j < this.mSize) {
        Object[] arrayOfObject = this.mValues;
        if (arrayOfObject[j] == DELETED) {
          this.mKeys[j] = paramInt;
          arrayOfObject[j] = paramE;
          return;
        } 
      } 
      i = j;
      if (this.mGarbage) {
        i = j;
        if (this.mSize >= this.mKeys.length) {
          gc();
          i = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt) ^ 0xFFFFFFFF;
        } 
      } 
      this.mKeys = GrowingArrayUtils.insert(this.mKeys, this.mSize, i, paramInt);
      this.mValues = GrowingArrayUtils.insert(this.mValues, this.mSize, i, paramE);
      this.mSize++;
    } 
  }
  
  public int size() {
    if (this.mGarbage)
      gc(); 
    return this.mSize;
  }
  
  public int keyAt(int paramInt) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds) {
      if (this.mGarbage)
        gc(); 
      return this.mKeys[paramInt];
    } 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public E valueAt(int paramInt) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds) {
      if (this.mGarbage)
        gc(); 
      Object[] arrayOfObject = this.mValues;
      if (arrayOfObject[paramInt] == DELETED)
        return null; 
      return (E)arrayOfObject[paramInt];
    } 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public void setValueAt(int paramInt, E paramE) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds) {
      if (this.mGarbage)
        gc(); 
      this.mValues[paramInt] = paramE;
      return;
    } 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public int indexOfKey(int paramInt) {
    if (this.mGarbage)
      gc(); 
    return ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt);
  }
  
  public int indexOfValue(E paramE) {
    if (this.mGarbage)
      gc(); 
    for (byte b = 0; b < this.mSize; b++) {
      if (this.mValues[b] == paramE)
        return b; 
    } 
    return -1;
  }
  
  public int indexOfValueByValue(E paramE) {
    if (this.mGarbage)
      gc(); 
    for (byte b = 0; b < this.mSize; b++) {
      if (paramE == null) {
        if (this.mValues[b] == null)
          return b; 
      } else if (paramE.equals(this.mValues[b])) {
        return b;
      } 
    } 
    return -1;
  }
  
  public void clear() {
    int i = this.mSize;
    Object[] arrayOfObject = this.mValues;
    for (byte b = 0; b < i; b++)
      arrayOfObject[b] = null; 
    this.mSize = 0;
    this.mGarbage = false;
  }
  
  public void append(int paramInt, E paramE) {
    int i = this.mSize;
    if (i != 0 && paramInt <= this.mKeys[i - 1]) {
      put(paramInt, paramE);
      return;
    } 
    if (this.mGarbage && this.mSize >= this.mKeys.length)
      gc(); 
    this.mKeys = GrowingArrayUtils.append(this.mKeys, this.mSize, paramInt);
    this.mValues = GrowingArrayUtils.append(this.mValues, this.mSize, paramE);
    this.mSize++;
  }
  
  public String toString() {
    if (size() <= 0)
      return "{}"; 
    StringBuilder stringBuilder = new StringBuilder(this.mSize * 28);
    stringBuilder.append('{');
    for (byte b = 0; b < this.mSize; b++) {
      if (b > 0)
        stringBuilder.append(", "); 
      int i = keyAt(b);
      stringBuilder.append(i);
      stringBuilder.append('=');
      E e = valueAt(b);
      if (e != this) {
        stringBuilder.append(e);
      } else {
        stringBuilder.append("(this Map)");
      } 
    } 
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
}
