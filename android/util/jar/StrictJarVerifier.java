package android.util.jar;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.jar.Attributes;
import sun.security.jca.Providers;
import sun.security.pkcs.PKCS7;
import sun.security.pkcs.SignerInfo;

class StrictJarVerifier {
  private static final String[] DIGEST_ALGORITHMS = new String[] { "SHA-512", "SHA-384", "SHA-256", "SHA1" };
  
  private final Hashtable<String, HashMap<String, Attributes>> signatures = new Hashtable<>(5);
  
  private final Hashtable<String, Certificate[]> certificates = (Hashtable)new Hashtable<>(5);
  
  private final Hashtable<String, Certificate[][]> verifiedEntries = (Hashtable)new Hashtable<>();
  
  private static final String SF_ATTRIBUTE_ANDROID_APK_SIGNED_NAME = "X-Android-APK-Signed";
  
  private final String jarName;
  
  private final int mainAttributesEnd;
  
  private final StrictJarManifest manifest;
  
  private final HashMap<String, byte[]> metaEntries;
  
  private final boolean signatureSchemeRollbackProtectionsEnforced;
  
  class VerifierEntry extends OutputStream {
    private final Certificate[][] certChains;
    
    private final MessageDigest digest;
    
    private final byte[] hash;
    
    private final String name;
    
    private final Hashtable<String, Certificate[][]> verifiedEntries;
    
    VerifierEntry(StrictJarVerifier this$0, MessageDigest param1MessageDigest, byte[] param1ArrayOfbyte, Certificate[][] param1ArrayOfCertificate, Hashtable<String, Certificate[][]> param1Hashtable) {
      this.name = (String)this$0;
      this.digest = param1MessageDigest;
      this.hash = param1ArrayOfbyte;
      this.certChains = param1ArrayOfCertificate;
      this.verifiedEntries = param1Hashtable;
    }
    
    public void write(int param1Int) {
      this.digest.update((byte)param1Int);
    }
    
    public void write(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) {
      this.digest.update(param1ArrayOfbyte, param1Int1, param1Int2);
    }
    
    void verify() {
      byte[] arrayOfByte = this.digest.digest();
      if (StrictJarVerifier.verifyMessageDigest(arrayOfByte, this.hash)) {
        this.verifiedEntries.put(this.name, this.certChains);
        return;
      } 
      String str = this.name;
      throw StrictJarVerifier.invalidDigest("META-INF/MANIFEST.MF", str, str);
    }
  }
  
  private static SecurityException invalidDigest(String paramString1, String paramString2, String paramString3) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString1);
    stringBuilder.append(" has invalid digest for ");
    stringBuilder.append(paramString2);
    stringBuilder.append(" in ");
    stringBuilder.append(paramString3);
    throw new SecurityException(stringBuilder.toString());
  }
  
  private static SecurityException failedVerification(String paramString1, String paramString2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString1);
    stringBuilder.append(" failed verification of ");
    stringBuilder.append(paramString2);
    throw new SecurityException(stringBuilder.toString());
  }
  
  private static SecurityException failedVerification(String paramString1, String paramString2, Throwable paramThrowable) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString1);
    stringBuilder.append(" failed verification of ");
    stringBuilder.append(paramString2);
    throw new SecurityException(stringBuilder.toString(), paramThrowable);
  }
  
  StrictJarVerifier(String paramString, StrictJarManifest paramStrictJarManifest, HashMap<String, byte[]> paramHashMap, boolean paramBoolean) {
    this.jarName = paramString;
    this.manifest = paramStrictJarManifest;
    this.metaEntries = paramHashMap;
    this.mainAttributesEnd = paramStrictJarManifest.getMainAttributesEnd();
    this.signatureSchemeRollbackProtectionsEnforced = paramBoolean;
  }
  
  VerifierEntry initEntry(String paramString) {
    if (this.manifest == null || this.signatures.isEmpty())
      return null; 
    Attributes attributes = this.manifest.getAttributes(paramString);
    if (attributes == null)
      return null; 
    ArrayList<Certificate[]> arrayList = new ArrayList();
    Iterator<Map.Entry> iterator = this.signatures.entrySet().iterator();
    while (iterator.hasNext()) {
      Map.Entry entry = iterator.next();
      HashMap hashMap = (HashMap)entry.getValue();
      if (hashMap.get(paramString) != null) {
        String str = (String)entry.getKey();
        Certificate[] arrayOfCertificate1 = this.certificates.get(str);
        if (arrayOfCertificate1 != null)
          arrayList.add(arrayOfCertificate1); 
      } 
    } 
    if (arrayList.isEmpty())
      return null; 
    Certificate[][] arrayOfCertificate = arrayList.<Certificate[]>toArray(new Certificate[arrayList.size()][]);
    byte b = 0;
    while (true) {
      String[] arrayOfString = DIGEST_ALGORITHMS;
      if (b < arrayOfString.length) {
        String str2 = arrayOfString[b];
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str2);
        stringBuilder.append("-Digest");
        String str1 = attributes.getValue(stringBuilder.toString());
        if (str1 != null) {
          byte[] arrayOfByte = str1.getBytes(StandardCharsets.ISO_8859_1);
          try {
            MessageDigest messageDigest = MessageDigest.getInstance(str2);
            Hashtable<String, Certificate[][]> hashtable = this.verifiedEntries;
            try {
              return new VerifierEntry(paramString, messageDigest, arrayOfByte, arrayOfCertificate, hashtable);
            } catch (NoSuchAlgorithmException noSuchAlgorithmException) {}
          } catch (NoSuchAlgorithmException noSuchAlgorithmException) {}
        } 
        b++;
        continue;
      } 
      break;
    } 
    return null;
  }
  
  void addMetaEntry(String paramString, byte[] paramArrayOfbyte) {
    this.metaEntries.put(paramString.toUpperCase(Locale.US), paramArrayOfbyte);
  }
  
  boolean readCertificates() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield metaEntries : Ljava/util/HashMap;
    //   6: invokevirtual isEmpty : ()Z
    //   9: istore_1
    //   10: iload_1
    //   11: ifeq -> 18
    //   14: aload_0
    //   15: monitorexit
    //   16: iconst_0
    //   17: ireturn
    //   18: aload_0
    //   19: getfield metaEntries : Ljava/util/HashMap;
    //   22: invokevirtual keySet : ()Ljava/util/Set;
    //   25: invokeinterface iterator : ()Ljava/util/Iterator;
    //   30: astore_2
    //   31: aload_2
    //   32: invokeinterface hasNext : ()Z
    //   37: ifeq -> 94
    //   40: aload_2
    //   41: invokeinterface next : ()Ljava/lang/Object;
    //   46: checkcast java/lang/String
    //   49: astore_3
    //   50: aload_3
    //   51: ldc_w '.DSA'
    //   54: invokevirtual endsWith : (Ljava/lang/String;)Z
    //   57: ifne -> 80
    //   60: aload_3
    //   61: ldc_w '.RSA'
    //   64: invokevirtual endsWith : (Ljava/lang/String;)Z
    //   67: ifne -> 80
    //   70: aload_3
    //   71: ldc_w '.EC'
    //   74: invokevirtual endsWith : (Ljava/lang/String;)Z
    //   77: ifeq -> 91
    //   80: aload_0
    //   81: aload_3
    //   82: invokespecial verifyCertificate : (Ljava/lang/String;)V
    //   85: aload_2
    //   86: invokeinterface remove : ()V
    //   91: goto -> 31
    //   94: aload_0
    //   95: monitorexit
    //   96: iconst_1
    //   97: ireturn
    //   98: astore_3
    //   99: aload_0
    //   100: monitorexit
    //   101: aload_3
    //   102: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #292	-> 2
    //   #293	-> 14
    //   #296	-> 18
    //   #297	-> 31
    //   #298	-> 40
    //   #299	-> 50
    //   #300	-> 80
    //   #301	-> 85
    //   #303	-> 91
    //   #304	-> 94
    //   #291	-> 98
    // Exception table:
    //   from	to	target	type
    //   2	10	98	finally
    //   18	31	98	finally
    //   31	40	98	finally
    //   40	50	98	finally
    //   50	80	98	finally
    //   80	85	98	finally
    //   85	91	98	finally
  }
  
  static Certificate[] verifyBytes(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws GeneralSecurityException {
    Object object1 = null, object2 = null;
    try {
      Object object = Providers.startJarVerification();
      object2 = object;
      object1 = object;
      PKCS7 pKCS7 = new PKCS7();
      object2 = object;
      object1 = object;
      this(paramArrayOfbyte1);
      object2 = object;
      object1 = object;
      SignerInfo[] arrayOfSignerInfo = pKCS7.verify(paramArrayOfbyte2);
      if (arrayOfSignerInfo != null) {
        object2 = object;
        object1 = object;
        if (arrayOfSignerInfo.length != 0) {
          SignerInfo signerInfo = arrayOfSignerInfo[0];
          object2 = object;
          object1 = object;
          ArrayList<X509Certificate> arrayList = signerInfo.getCertificateChain(pKCS7);
          if (arrayList != null) {
            object2 = object;
            object1 = object;
            if (!arrayList.isEmpty()) {
              object2 = object;
              object1 = object;
              X509Certificate[] arrayOfX509Certificate = new X509Certificate[arrayList.size()];
              object2 = object;
              object1 = object;
              Certificate[] arrayOfCertificate = arrayList.<Certificate>toArray((Certificate[])arrayOfX509Certificate);
              Providers.stopJarVerification(object);
              return arrayOfCertificate;
            } 
            object2 = object;
            object1 = object;
            GeneralSecurityException generalSecurityException2 = new GeneralSecurityException();
            object2 = object;
            object1 = object;
            this("Verified SignerInfo certificate chain is emtpy");
            object2 = object;
            object1 = object;
            throw generalSecurityException2;
          } 
          object2 = object;
          object1 = object;
          GeneralSecurityException generalSecurityException1 = new GeneralSecurityException();
          object2 = object;
          object1 = object;
          this("Failed to find verified SignerInfo certificate chain");
          object2 = object;
          object1 = object;
          throw generalSecurityException1;
        } 
      } 
      object2 = object;
      object1 = object;
      GeneralSecurityException generalSecurityException = new GeneralSecurityException();
      object2 = object;
      object1 = object;
      this("Failed to verify signature: no verified SignerInfos");
      object2 = object;
      object1 = object;
      throw generalSecurityException;
    } catch (IOException iOException) {
      object2 = object1;
      GeneralSecurityException generalSecurityException = new GeneralSecurityException();
      object2 = object1;
      this("IO exception verifying jar cert", iOException);
      object2 = object1;
      throw generalSecurityException;
    } finally {}
    Providers.stopJarVerification(object2);
    throw paramArrayOfbyte1;
  }
  
  private void verifyCertificate(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString.substring(0, paramString.lastIndexOf('.')));
    stringBuilder.append(".SF");
    String str = stringBuilder.toString();
    byte[] arrayOfByte2 = this.metaEntries.get(str);
    if (arrayOfByte2 == null)
      return; 
    byte[] arrayOfByte1 = this.metaEntries.get("META-INF/MANIFEST.MF");
    if (arrayOfByte1 == null)
      return; 
    byte[] arrayOfByte3 = this.metaEntries.get(paramString);
    try {
      Certificate[] arrayOfCertificate = verifyBytes(arrayOfByte3, arrayOfByte2);
      if (arrayOfCertificate != null)
        try {
          this.certificates.put(str, arrayOfCertificate);
        } catch (GeneralSecurityException generalSecurityException) {} 
      Attributes attributes = new Attributes();
      HashMap<Object, Object> hashMap = new HashMap<>();
      try {
        StrictJarManifestReader strictJarManifestReader = new StrictJarManifestReader();
        this(arrayOfByte2, attributes);
        strictJarManifestReader.readEntries((Map)hashMap, null);
        if (this.signatureSchemeRollbackProtectionsEnforced) {
          String str2 = attributes.getValue("X-Android-APK-Signed");
          if (str2 != null) {
            int j;
            boolean bool2;
            byte b = 0;
            boolean bool1 = false;
            StringTokenizer stringTokenizer = new StringTokenizer(str2, ",");
            while (true) {
              j = b;
              bool2 = bool1;
              if (stringTokenizer.hasMoreTokens()) {
                String str3 = stringTokenizer.nextToken().trim();
                if (str3.isEmpty())
                  continue; 
                try {
                  j = Integer.parseInt(str3);
                  if (j == 2) {
                    j = 1;
                    bool2 = bool1;
                    break;
                  } 
                  if (j == 3) {
                    bool2 = true;
                    j = b;
                    break;
                  } 
                } catch (Exception exception) {}
                continue;
              } 
              break;
            } 
            if (j == 0) {
              if (bool2) {
                StringBuilder stringBuilder1 = new StringBuilder();
                stringBuilder1.append(str);
                stringBuilder1.append(" indicates ");
                stringBuilder1.append(this.jarName);
                stringBuilder1.append(" is signed using APK Signature Scheme v3, but no such signature was found. Signature stripped?");
                throw new SecurityException(stringBuilder1.toString());
              } 
            } else {
              StringBuilder stringBuilder1 = new StringBuilder();
              stringBuilder1.append(str);
              stringBuilder1.append(" indicates ");
              stringBuilder1.append(this.jarName);
              stringBuilder1.append(" is signed using APK Signature Scheme v2, but no such signature was found. Signature stripped?");
              throw new SecurityException(stringBuilder1.toString());
            } 
          } 
        } 
        if (attributes.get(Attributes.Name.SIGNATURE_VERSION) == null)
          return; 
        boolean bool = false;
        String str1 = attributes.getValue("Created-By");
        if (str1 != null)
          if (str1.indexOf("signtool") != -1) {
            bool = true;
          } else {
            bool = false;
          }  
        int i = this.mainAttributesEnd;
        if (i > 0 && !bool)
          if (!verify(attributes, "-Digest-Manifest-Main-Attributes", arrayOfByte1, 0, i, false, true))
            throw failedVerification(this.jarName, str);  
        if (bool) {
          str1 = "-Digest";
        } else {
          str1 = "-Digest-Manifest";
        } 
        if (!verify(attributes, str1, arrayOfByte1, 0, arrayOfByte1.length, false, false)) {
          Iterator<Map.Entry> iterator = hashMap.entrySet().iterator();
          byte[] arrayOfByte = arrayOfByte1;
          arrayOfByte1 = arrayOfByte3;
          Attributes attributes1 = attributes;
          while (iterator.hasNext()) {
            Map.Entry entry = iterator.next();
            StrictJarManifest.Chunk chunk = this.manifest.getChunk((String)entry.getKey());
            if (chunk == null)
              return; 
            if (verify((Attributes)entry.getValue(), "-Digest", arrayOfByte, chunk.start, chunk.end, bool, false))
              continue; 
            throw invalidDigest(str, (String)entry.getKey(), this.jarName);
          } 
        } 
        this.metaEntries.put(str, null);
        this.signatures.put(str, hashMap);
        return;
      } catch (IOException iOException) {
        return;
      } 
    } catch (GeneralSecurityException generalSecurityException) {
      throw failedVerification(this.jarName, str, generalSecurityException);
    } 
  }
  
  boolean isSignedJar() {
    boolean bool;
    if (this.certificates.size() > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean verify(Attributes paramAttributes, String paramString, byte[] paramArrayOfbyte, int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2) {
    byte b = 0;
    while (true) {
      String[] arrayOfString = DIGEST_ALGORITHMS;
      if (b < arrayOfString.length) {
        String str2 = arrayOfString[b];
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str2);
        stringBuilder.append(paramString);
        String str1 = paramAttributes.getValue(stringBuilder.toString());
        if (str1 != null)
          try {
            MessageDigest messageDigest = MessageDigest.getInstance(str2);
            if (paramBoolean1 && paramArrayOfbyte[paramInt2 - 1] == 10 && paramArrayOfbyte[paramInt2 - 2] == 10) {
              messageDigest.update(paramArrayOfbyte, paramInt1, paramInt2 - 1 - paramInt1);
            } else {
              messageDigest.update(paramArrayOfbyte, paramInt1, paramInt2 - paramInt1);
            } 
            byte[] arrayOfByte1 = messageDigest.digest();
            byte[] arrayOfByte2 = str1.getBytes(StandardCharsets.ISO_8859_1);
            return verifyMessageDigest(arrayOfByte1, arrayOfByte2);
          } catch (NoSuchAlgorithmException noSuchAlgorithmException) {} 
        b++;
        continue;
      } 
      break;
    } 
    return paramBoolean2;
  }
  
  private static boolean verifyMessageDigest(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) {
    try {
      paramArrayOfbyte2 = Base64.getDecoder().decode(paramArrayOfbyte2);
      return MessageDigest.isEqual(paramArrayOfbyte1, paramArrayOfbyte2);
    } catch (IllegalArgumentException illegalArgumentException) {
      return false;
    } 
  }
  
  Certificate[][] getCertificateChains(String paramString) {
    return this.verifiedEntries.get(paramString);
  }
  
  void removeMetaEntries() {
    this.metaEntries.clear();
  }
}
