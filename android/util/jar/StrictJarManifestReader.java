package android.util.jar;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.Attributes;

class StrictJarManifestReader {
  private final HashMap<String, Attributes.Name> attributeNameCache = new HashMap<>();
  
  private final ByteArrayOutputStream valueBuffer = new ByteArrayOutputStream(80);
  
  private int consecutiveLineBreaks = 0;
  
  private final byte[] buf;
  
  private final int endOfMainSection;
  
  private Attributes.Name name;
  
  private int pos;
  
  private String value;
  
  public StrictJarManifestReader(byte[] paramArrayOfbyte, Attributes paramAttributes) throws IOException {
    this.buf = paramArrayOfbyte;
    while (readHeader())
      paramAttributes.put(this.name, this.value); 
    this.endOfMainSection = this.pos;
  }
  
  public void readEntries(Map<String, Attributes> paramMap, Map<String, StrictJarManifest.Chunk> paramMap1) throws IOException {
    int i = this.pos;
    while (readHeader()) {
      if (StrictJarManifest.ATTRIBUTE_NAME_NAME.equals(this.name)) {
        String str = this.value;
        Attributes attributes1 = paramMap.get(str);
        Attributes attributes2 = attributes1;
        if (attributes1 == null)
          attributes2 = new Attributes(12); 
        while (readHeader())
          attributes2.put(this.name, this.value); 
        int j = i;
        if (paramMap1 != null)
          if (paramMap1.get(str) == null) {
            paramMap1.put(str, new StrictJarManifest.Chunk(i, this.pos));
            j = this.pos;
          } else {
            throw new IOException("A jar verifier does not support more than one entry with the same name");
          }  
        paramMap.put(str, attributes2);
        i = j;
        continue;
      } 
      throw new IOException("Entry is not named");
    } 
  }
  
  public int getEndOfMainSection() {
    return this.endOfMainSection;
  }
  
  private boolean readHeader() throws IOException {
    int i = this.consecutiveLineBreaks;
    boolean bool = true;
    if (i > 1) {
      this.consecutiveLineBreaks = 0;
      return false;
    } 
    readName();
    this.consecutiveLineBreaks = 0;
    readValue();
    if (this.consecutiveLineBreaks <= 0)
      bool = false; 
    return bool;
  }
  
  private void readName() throws IOException {
    int i = this.pos;
    while (true) {
      int j = this.pos;
      byte[] arrayOfByte = this.buf;
      if (j < arrayOfByte.length) {
        this.pos = j + 1;
        if (arrayOfByte[j] != 58)
          continue; 
        String str = new String(this.buf, i, this.pos - i - 1, StandardCharsets.US_ASCII);
        byte[] arrayOfByte1 = this.buf;
        i = this.pos;
        this.pos = i + 1;
        if (arrayOfByte1[i] == 32)
          try {
            Attributes.Name name = this.attributeNameCache.get(str);
            if (name == null) {
              name = new Attributes.Name();
              this(str);
              this.name = name;
              this.attributeNameCache.put(str, name);
            } 
            return;
          } catch (IllegalArgumentException illegalArgumentException) {
            throw new IOException(illegalArgumentException.getMessage());
          }  
        throw new IOException(String.format("Invalid value for attribute '%s'", new Object[] { illegalArgumentException }));
      } 
      break;
    } 
  }
  
  private void readValue() throws IOException {
    boolean bool = false;
    int i = this.pos;
    int j = this.pos;
    this.valueBuffer.reset();
    while (true) {
      int k = this.pos;
      byte[] arrayOfByte = this.buf;
      if (k < arrayOfByte.length) {
        this.pos = k + 1;
        k = arrayOfByte[k];
        if (k != 0) {
          if (k != 10) {
            if (k != 13) {
              if (k == 32)
                if (this.consecutiveLineBreaks == 1) {
                  this.valueBuffer.write(arrayOfByte, i, j - i);
                  i = this.pos;
                  this.consecutiveLineBreaks = 0;
                  continue;
                }  
              if (this.consecutiveLineBreaks >= 1) {
                this.pos--;
                break;
              } 
              j = this.pos;
              continue;
            } 
            bool = true;
            this.consecutiveLineBreaks++;
            continue;
          } 
          if (bool) {
            bool = false;
            continue;
          } 
          this.consecutiveLineBreaks++;
          continue;
        } 
        throw new IOException("NUL character in a manifest");
      } 
      break;
    } 
    this.valueBuffer.write(this.buf, i, j - i);
    this.value = this.valueBuffer.toString(StandardCharsets.UTF_8.name());
  }
}
