package android.util.jar;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.jar.Attributes;
import libcore.io.Streams;

public class StrictJarManifest implements Cloneable {
  static final Attributes.Name ATTRIBUTE_NAME_NAME;
  
  static final int LINE_LENGTH_LIMIT = 72;
  
  private static final byte[] LINE_SEPARATOR = new byte[] { 13, 10 };
  
  private static final byte[] VALUE_SEPARATOR = new byte[] { 58, 32 };
  
  private HashMap<String, Chunk> chunks;
  
  private final HashMap<String, Attributes> entries;
  
  private final Attributes mainAttributes;
  
  private int mainEnd;
  
  static {
    ATTRIBUTE_NAME_NAME = new Attributes.Name("Name");
  }
  
  static final class Chunk {
    final int end;
    
    final int start;
    
    Chunk(int param1Int1, int param1Int2) {
      this.start = param1Int1;
      this.end = param1Int2;
    }
  }
  
  public StrictJarManifest() {
    this.entries = new HashMap<>();
    this.mainAttributes = new Attributes();
  }
  
  public StrictJarManifest(InputStream paramInputStream) throws IOException {
    this();
    read(Streams.readFully(paramInputStream));
  }
  
  public StrictJarManifest(StrictJarManifest paramStrictJarManifest) {
    this.mainAttributes = (Attributes)paramStrictJarManifest.mainAttributes.clone();
    this.entries = (HashMap<String, Attributes>)((HashMap)paramStrictJarManifest.getEntries()).clone();
  }
  
  StrictJarManifest(byte[] paramArrayOfbyte, boolean paramBoolean) throws IOException {
    this();
    if (paramBoolean)
      this.chunks = new HashMap<>(); 
    read(paramArrayOfbyte);
  }
  
  public void clear() {
    this.entries.clear();
    this.mainAttributes.clear();
  }
  
  public Attributes getAttributes(String paramString) {
    return getEntries().get(paramString);
  }
  
  public Map<String, Attributes> getEntries() {
    return this.entries;
  }
  
  public Attributes getMainAttributes() {
    return this.mainAttributes;
  }
  
  public Object clone() {
    return new StrictJarManifest(this);
  }
  
  public void write(OutputStream paramOutputStream) throws IOException {
    write(this, paramOutputStream);
  }
  
  public void read(InputStream paramInputStream) throws IOException {
    read(Streams.readFullyNoClose(paramInputStream));
  }
  
  private void read(byte[] paramArrayOfbyte) throws IOException {
    if (paramArrayOfbyte.length == 0)
      return; 
    StrictJarManifestReader strictJarManifestReader = new StrictJarManifestReader(paramArrayOfbyte, this.mainAttributes);
    this.mainEnd = strictJarManifestReader.getEndOfMainSection();
    strictJarManifestReader.readEntries(this.entries, this.chunks);
  }
  
  public int hashCode() {
    return this.mainAttributes.hashCode() ^ getEntries().hashCode();
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != getClass())
      return false; 
    if (!this.mainAttributes.equals(((StrictJarManifest)paramObject).mainAttributes))
      return false; 
    return getEntries().equals(((StrictJarManifest)paramObject).getEntries());
  }
  
  Chunk getChunk(String paramString) {
    return this.chunks.get(paramString);
  }
  
  void removeChunks() {
    this.chunks = null;
  }
  
  int getMainAttributesEnd() {
    return this.mainEnd;
  }
  
  static void write(StrictJarManifest paramStrictJarManifest, OutputStream paramOutputStream) throws IOException {
    CharsetEncoder charsetEncoder = StandardCharsets.UTF_8.newEncoder();
    ByteBuffer byteBuffer = ByteBuffer.allocate(72);
    Attributes.Name name = Attributes.Name.MANIFEST_VERSION;
    String str1 = paramStrictJarManifest.mainAttributes.getValue(name);
    String str2 = str1;
    if (str1 == null) {
      name = Attributes.Name.SIGNATURE_VERSION;
      str2 = paramStrictJarManifest.mainAttributes.getValue(name);
    } 
    if (str2 != null) {
      writeEntry(paramOutputStream, name, str2, charsetEncoder, byteBuffer);
      Iterator<Attributes.Name> iterator1 = paramStrictJarManifest.mainAttributes.keySet().iterator();
      while (iterator1.hasNext()) {
        Attributes.Name name1 = iterator1.next();
        if (!name1.equals(name))
          writeEntry(paramOutputStream, name1, paramStrictJarManifest.mainAttributes.getValue(name1), charsetEncoder, byteBuffer); 
      } 
    } 
    paramOutputStream.write(LINE_SEPARATOR);
    Iterator<String> iterator = paramStrictJarManifest.getEntries().keySet().iterator();
    while (iterator.hasNext()) {
      str2 = iterator.next();
      writeEntry(paramOutputStream, ATTRIBUTE_NAME_NAME, str2, charsetEncoder, byteBuffer);
      Attributes attributes = paramStrictJarManifest.entries.get(str2);
      Iterator<Attributes.Name> iterator1 = attributes.keySet().iterator();
      while (iterator1.hasNext()) {
        Attributes.Name name1 = iterator1.next();
        writeEntry(paramOutputStream, name1, attributes.getValue(name1), charsetEncoder, byteBuffer);
      } 
      paramOutputStream.write(LINE_SEPARATOR);
    } 
  }
  
  private static void writeEntry(OutputStream paramOutputStream, Attributes.Name paramName, String paramString, CharsetEncoder paramCharsetEncoder, ByteBuffer paramByteBuffer) throws IOException {
    String str = paramName.toString();
    paramOutputStream.write(str.getBytes(StandardCharsets.US_ASCII));
    paramOutputStream.write(VALUE_SEPARATOR);
    paramCharsetEncoder.reset();
    paramByteBuffer.clear().limit(72 - str.length() - 2);
    CharBuffer charBuffer = CharBuffer.wrap(paramString);
    while (true) {
      CoderResult coderResult2 = paramCharsetEncoder.encode(charBuffer, paramByteBuffer, true);
      CoderResult coderResult1 = coderResult2;
      if (CoderResult.UNDERFLOW == coderResult2)
        coderResult1 = paramCharsetEncoder.flush(paramByteBuffer); 
      paramOutputStream.write(paramByteBuffer.array(), paramByteBuffer.arrayOffset(), paramByteBuffer.position());
      paramOutputStream.write(LINE_SEPARATOR);
      if (CoderResult.UNDERFLOW == coderResult1)
        return; 
      paramOutputStream.write(32);
      paramByteBuffer.clear().limit(71);
    } 
  }
}
