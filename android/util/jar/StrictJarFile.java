package android.util.jar;

import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import dalvik.system.CloseGuard;
import java.io.FileDescriptor;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.Certificate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;
import java.util.zip.ZipEntry;
import libcore.io.IoBridge;
import libcore.io.IoUtils;
import libcore.io.Streams;

public final class StrictJarFile {
  private boolean closed;
  
  private final FileDescriptor fd;
  
  private final CloseGuard guard = CloseGuard.get();
  
  private final boolean isSigned;
  
  private final StrictJarManifest manifest;
  
  private final long nativeHandle;
  
  private final StrictJarVerifier verifier;
  
  public StrictJarFile(String paramString) throws IOException, SecurityException {
    this(paramString, true, true);
  }
  
  public StrictJarFile(FileDescriptor paramFileDescriptor) throws IOException, SecurityException {
    this(paramFileDescriptor, true, true);
  }
  
  public StrictJarFile(FileDescriptor paramFileDescriptor, boolean paramBoolean1, boolean paramBoolean2) throws IOException, SecurityException {
    this(stringBuilder.toString(), paramFileDescriptor, paramBoolean1, paramBoolean2);
  }
  
  public StrictJarFile(String paramString, boolean paramBoolean1, boolean paramBoolean2) throws IOException, SecurityException {
    this(paramString, IoBridge.open(paramString, OsConstants.O_RDONLY), paramBoolean1, paramBoolean2);
  }
  
  private StrictJarFile(String paramString, FileDescriptor paramFileDescriptor, boolean paramBoolean1, boolean paramBoolean2) throws IOException, SecurityException {
    this.nativeHandle = nativeOpenJarFile(paramString, paramFileDescriptor.getInt$());
    this.fd = paramFileDescriptor;
    boolean bool = false;
    if (paramBoolean1)
      try {
        HashMap<String, byte[]> hashMap = getMetaEntries();
        StrictJarManifest strictJarManifest = new StrictJarManifest();
        this(hashMap.get("META-INF/MANIFEST.MF"), true);
        this.manifest = strictJarManifest;
        StrictJarVerifier strictJarVerifier = new StrictJarVerifier();
        this(paramString, this.manifest, hashMap, paramBoolean2);
        this.verifier = strictJarVerifier;
        Set<String> set = this.manifest.getEntries().keySet();
        for (Iterator<String> iterator = set.iterator(); iterator.hasNext(); ) {
          String str = iterator.next();
          if (findEntry(str) != null)
            continue; 
          SecurityException securityException = new SecurityException();
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("File ");
          stringBuilder.append(str);
          stringBuilder.append(" in manifest does not exist");
          this(stringBuilder.toString());
          throw securityException;
        } 
        paramBoolean1 = bool;
        if (this.verifier.readCertificates()) {
          paramBoolean1 = bool;
          if (this.verifier.isSignedJar())
            paramBoolean1 = true; 
        } 
        this.isSigned = paramBoolean1;
        this.guard.open("close");
        return;
      } catch (IOException|SecurityException iOException) {
        nativeClose(this.nativeHandle);
        IoUtils.closeQuietly(paramFileDescriptor);
        this.closed = true;
        throw iOException;
      }  
    this.isSigned = false;
    this.manifest = null;
    this.verifier = null;
    this.guard.open("close");
  }
  
  public StrictJarManifest getManifest() {
    return this.manifest;
  }
  
  public Iterator<ZipEntry> iterator() throws IOException {
    return new EntryIterator(this.nativeHandle, "");
  }
  
  public ZipEntry findEntry(String paramString) {
    return nativeFindEntry(this.nativeHandle, paramString);
  }
  
  public Certificate[][] getCertificateChains(ZipEntry paramZipEntry) {
    if (this.isSigned)
      return this.verifier.getCertificateChains(paramZipEntry.getName()); 
    return null;
  }
  
  @Deprecated
  public Certificate[] getCertificates(ZipEntry paramZipEntry) {
    if (this.isSigned) {
      Certificate[][] arrayOfCertificate = this.verifier.getCertificateChains(paramZipEntry.getName());
      int i = 0;
      int j;
      byte b;
      for (j = arrayOfCertificate.length, b = 0; b < j; ) {
        Certificate[] arrayOfCertificate2 = arrayOfCertificate[b];
        i += arrayOfCertificate2.length;
        b++;
      } 
      Certificate[] arrayOfCertificate1 = new Certificate[i];
      i = 0;
      for (j = arrayOfCertificate.length, b = 0; b < j; ) {
        Certificate[] arrayOfCertificate2 = arrayOfCertificate[b];
        System.arraycopy(arrayOfCertificate2, 0, arrayOfCertificate1, i, arrayOfCertificate2.length);
        i += arrayOfCertificate2.length;
        b++;
      } 
      return arrayOfCertificate1;
    } 
    return null;
  }
  
  public InputStream getInputStream(ZipEntry paramZipEntry) {
    InputStream inputStream = getZipInputStream(paramZipEntry);
    if (this.isSigned) {
      StrictJarVerifier.VerifierEntry verifierEntry = this.verifier.initEntry(paramZipEntry.getName());
      if (verifierEntry == null)
        return inputStream; 
      return new JarFileInputStream(inputStream, paramZipEntry.getSize(), verifierEntry);
    } 
    return inputStream;
  }
  
  public void close() throws IOException {
    if (!this.closed) {
      CloseGuard closeGuard = this.guard;
      if (closeGuard != null)
        closeGuard.close(); 
      nativeClose(this.nativeHandle);
      IoUtils.closeQuietly(this.fd);
      this.closed = true;
    } 
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.guard != null)
        this.guard.warnIfOpen(); 
      close();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  private InputStream getZipInputStream(ZipEntry paramZipEntry) {
    if (paramZipEntry.getMethod() == 0) {
      FileDescriptor fileDescriptor1 = this.fd;
      long l = paramZipEntry.getDataOffset();
      return 
        new FDStream(fileDescriptor1, l, paramZipEntry.getDataOffset() + paramZipEntry.getSize());
    } 
    FileDescriptor fileDescriptor = this.fd;
    FDStream fDStream = new FDStream(fileDescriptor, paramZipEntry.getDataOffset(), paramZipEntry.getDataOffset() + paramZipEntry.getCompressedSize());
    int i = Math.max(1024, (int)Math.min(paramZipEntry.getSize(), 65535L));
    return new ZipInflaterInputStream(fDStream, new Inflater(true), i, paramZipEntry);
  }
  
  static final class EntryIterator implements Iterator<ZipEntry> {
    private final long iterationHandle;
    
    private ZipEntry nextEntry;
    
    EntryIterator(long param1Long, String param1String) throws IOException {
      this.iterationHandle = StrictJarFile.nativeStartIteration(param1Long, param1String);
    }
    
    public ZipEntry next() {
      if (this.nextEntry != null) {
        ZipEntry zipEntry = this.nextEntry;
        this.nextEntry = null;
        return zipEntry;
      } 
      return StrictJarFile.nativeNextEntry(this.iterationHandle);
    }
    
    public boolean hasNext() {
      if (this.nextEntry != null)
        return true; 
      ZipEntry zipEntry = StrictJarFile.nativeNextEntry(this.iterationHandle);
      if (zipEntry == null)
        return false; 
      this.nextEntry = zipEntry;
      return true;
    }
    
    public void remove() {
      throw new UnsupportedOperationException();
    }
  }
  
  private HashMap<String, byte[]> getMetaEntries() throws IOException {
    HashMap<Object, Object> hashMap = new HashMap<>();
    EntryIterator entryIterator = new EntryIterator(this.nativeHandle, "META-INF/");
    while (entryIterator.hasNext()) {
      ZipEntry zipEntry = entryIterator.next();
      hashMap.put(zipEntry.getName(), Streams.readFully(getInputStream(zipEntry)));
    } 
    return (HashMap)hashMap;
  }
  
  private static native void nativeClose(long paramLong);
  
  private static native ZipEntry nativeFindEntry(long paramLong, String paramString);
  
  private static native ZipEntry nativeNextEntry(long paramLong);
  
  private static native long nativeOpenJarFile(String paramString, int paramInt) throws IOException;
  
  private static native long nativeStartIteration(long paramLong, String paramString);
  
  static final class JarFileInputStream extends FilterInputStream {
    private long count;
    
    private boolean done = false;
    
    private final StrictJarVerifier.VerifierEntry entry;
    
    JarFileInputStream(InputStream param1InputStream, long param1Long, StrictJarVerifier.VerifierEntry param1VerifierEntry) {
      super(param1InputStream);
      this.entry = param1VerifierEntry;
      this.count = param1Long;
    }
    
    public int read() throws IOException {
      if (this.done)
        return -1; 
      if (this.count > 0L) {
        int i = super.read();
        if (i != -1) {
          this.entry.write(i);
          this.count--;
        } else {
          this.count = 0L;
        } 
        if (this.count == 0L) {
          this.done = true;
          this.entry.verify();
        } 
        return i;
      } 
      this.done = true;
      this.entry.verify();
      return -1;
    }
    
    public int read(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      if (this.done)
        return -1; 
      if (this.count > 0L) {
        int i = super.read(param1ArrayOfbyte, param1Int1, param1Int2);
        if (i != -1) {
          param1Int2 = i;
          long l = this.count;
          int j = param1Int2;
          if (l < param1Int2)
            j = (int)l; 
          this.entry.write(param1ArrayOfbyte, param1Int1, j);
          this.count -= j;
        } else {
          this.count = 0L;
        } 
        if (this.count == 0L) {
          this.done = true;
          this.entry.verify();
        } 
        return i;
      } 
      this.done = true;
      this.entry.verify();
      return -1;
    }
    
    public int available() throws IOException {
      if (this.done)
        return 0; 
      return super.available();
    }
    
    public long skip(long param1Long) throws IOException {
      return Streams.skipByReading(this, param1Long);
    }
  }
  
  public static class ZipInflaterInputStream extends InflaterInputStream {
    private long bytesRead = 0L;
    
    private boolean closed;
    
    private final ZipEntry entry;
    
    public ZipInflaterInputStream(InputStream param1InputStream, Inflater param1Inflater, int param1Int, ZipEntry param1ZipEntry) {
      super(param1InputStream, param1Inflater, param1Int);
      this.entry = param1ZipEntry;
    }
    
    public int read(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      try {
        param1Int1 = super.read(param1ArrayOfbyte, param1Int1, param1Int2);
        if (param1Int1 == -1) {
          if (this.entry.getSize() != this.bytesRead) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Size mismatch on inflated file: ");
            stringBuilder.append(this.bytesRead);
            stringBuilder.append(" vs ");
            ZipEntry zipEntry = this.entry;
            stringBuilder.append(zipEntry.getSize());
            throw new IOException(stringBuilder.toString());
          } 
        } else {
          this.bytesRead += param1Int1;
        } 
        return param1Int1;
      } catch (IOException iOException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Error reading data for ");
        stringBuilder.append(this.entry.getName());
        stringBuilder.append(" near offset ");
        stringBuilder.append(this.bytesRead);
        throw new IOException(stringBuilder.toString(), iOException);
      } 
    }
    
    public int available() throws IOException {
      boolean bool = this.closed;
      int i = 0;
      if (bool)
        return 0; 
      if (super.available() != 0)
        i = (int)(this.entry.getSize() - this.bytesRead); 
      return i;
    }
    
    public void close() throws IOException {
      super.close();
      this.closed = true;
    }
  }
  
  public static class FDStream extends InputStream {
    private long endOffset;
    
    private final FileDescriptor fd;
    
    private long offset;
    
    public FDStream(FileDescriptor param1FileDescriptor, long param1Long1, long param1Long2) {
      this.fd = param1FileDescriptor;
      this.offset = param1Long1;
      this.endOffset = param1Long2;
    }
    
    public int available() throws IOException {
      boolean bool;
      if (this.offset < this.endOffset) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int read() throws IOException {
      return Streams.readSingleByte(this);
    }
    
    public int read(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      synchronized (this.fd) {
        long l1 = this.endOffset, l2 = this.offset;
        l1 -= l2;
        int i = param1Int2;
        if (param1Int2 > l1)
          i = (int)l1; 
        try {
          Os.lseek(this.fd, this.offset, OsConstants.SEEK_SET);
          param1Int1 = IoBridge.read(this.fd, param1ArrayOfbyte, param1Int1, i);
          if (param1Int1 > 0) {
            this.offset += param1Int1;
            return param1Int1;
          } 
          return -1;
        } catch (ErrnoException errnoException) {
          IOException iOException = new IOException();
          this((Throwable)errnoException);
          throw iOException;
        } 
      } 
    }
    
    public long skip(long param1Long) throws IOException {
      long l1 = this.endOffset, l2 = this.offset, l3 = param1Long;
      if (param1Long > l1 - l2)
        l3 = l1 - l2; 
      this.offset += l3;
      return l3;
    }
  }
}
