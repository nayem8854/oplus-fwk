package android.util;

import android.content.Context;
import android.os.SystemProperties;
import android.provider.Settings;
import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;

public class FeatureFlagUtils {
  private static final Map<String, String> DEFAULT_FLAGS;
  
  public static final String DYNAMIC_SYSTEM = "settings_dynamic_system";
  
  public static final String FFLAG_OVERRIDE_PREFIX = "sys.fflag.override.";
  
  public static final String FFLAG_PREFIX = "sys.fflag.";
  
  public static final String HEARING_AID_SETTINGS = "settings_bluetooth_hearing_aid";
  
  public static final String PERSIST_PREFIX = "persist.sys.fflag.override.";
  
  public static final String SCREENRECORD_LONG_PRESS = "settings_screenrecord_long_press";
  
  public static final String SEAMLESS_TRANSFER = "settings_seamless_transfer";
  
  public static final String SETTINGS_DO_NOT_RESTORE_PRESERVED = "settings_do_not_restore_preserved";
  
  public static final String SETTINGS_FUSE_FLAG = "settings_fuse";
  
  public static final String SETTINGS_WIFITRACKER2 = "settings_wifitracker2";
  
  static {
    HashMap<Object, Object> hashMap = new HashMap<>();
    hashMap.put("settings_audio_switcher", "true");
    DEFAULT_FLAGS.put("settings_systemui_theme", "true");
    DEFAULT_FLAGS.put("settings_fuse", "true");
    DEFAULT_FLAGS.put("settings_dynamic_system", "false");
    DEFAULT_FLAGS.put("settings_seamless_transfer", "false");
    DEFAULT_FLAGS.put("settings_bluetooth_hearing_aid", "false");
    DEFAULT_FLAGS.put("settings_screenrecord_long_press", "false");
    DEFAULT_FLAGS.put("settings_wifi_details_datausage_header", "false");
    DEFAULT_FLAGS.put("settings_skip_direction_mutable", "true");
    DEFAULT_FLAGS.put("settings_wifitracker2", "true");
    DEFAULT_FLAGS.put("settings_controller_loading_enhancement", "true");
    DEFAULT_FLAGS.put("settings_conditionals", "false");
    DEFAULT_FLAGS.put("settings_do_not_restore_preserved", "true");
    DEFAULT_FLAGS.put("settings_tether_all_in_one", "false");
  }
  
  public static boolean isEnabled(Context paramContext, String paramString) {
    if (paramContext != null) {
      String str1 = Settings.Global.getString(paramContext.getContentResolver(), paramString);
      if (!TextUtils.isEmpty(str1))
        return Boolean.parseBoolean(str1); 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("sys.fflag.override.");
    stringBuilder.append(paramString);
    String str = SystemProperties.get(stringBuilder.toString());
    if (!TextUtils.isEmpty(str))
      return Boolean.parseBoolean(str); 
    str = getAllFeatureFlags().get(paramString);
    return Boolean.parseBoolean(str);
  }
  
  public static void setEnabled(Context paramContext, String paramString, boolean paramBoolean) {
    String str;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("sys.fflag.override.");
    stringBuilder.append(paramString);
    paramString = stringBuilder.toString();
    if (paramBoolean) {
      str = "true";
    } else {
      str = "false";
    } 
    SystemProperties.set(paramString, str);
  }
  
  public static Map<String, String> getAllFeatureFlags() {
    return DEFAULT_FLAGS;
  }
}
