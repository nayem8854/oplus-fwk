package android.util;

import com.android.internal.util.Preconditions;

public final class Size {
  private final int mHeight;
  
  private final int mWidth;
  
  public Size(int paramInt1, int paramInt2) {
    this.mWidth = paramInt1;
    this.mHeight = paramInt2;
  }
  
  public int getWidth() {
    return this.mWidth;
  }
  
  public int getHeight() {
    return this.mHeight;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    if (paramObject == null)
      return false; 
    if (this == paramObject)
      return true; 
    if (paramObject instanceof Size) {
      paramObject = paramObject;
      boolean bool1 = bool;
      if (this.mWidth == ((Size)paramObject).mWidth) {
        bool1 = bool;
        if (this.mHeight == ((Size)paramObject).mHeight)
          bool1 = true; 
      } 
      return bool1;
    } 
    return false;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.mWidth);
    stringBuilder.append("x");
    stringBuilder.append(this.mHeight);
    return stringBuilder.toString();
  }
  
  private static NumberFormatException invalidSize(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid Size: \"");
    stringBuilder.append(paramString);
    stringBuilder.append("\"");
    throw new NumberFormatException(stringBuilder.toString());
  }
  
  public static Size parseSize(String paramString) throws NumberFormatException {
    Preconditions.checkNotNull(paramString, "string must not be null");
    int i = paramString.indexOf('*');
    int j = i;
    if (i < 0)
      j = paramString.indexOf('x'); 
    if (j >= 0)
      try {
        i = Integer.parseInt(paramString.substring(0, j));
        return 
          new Size(i, Integer.parseInt(paramString.substring(j + 1)));
      } catch (NumberFormatException numberFormatException) {
        throw invalidSize(paramString);
      }  
    throw invalidSize(paramString);
  }
  
  public int hashCode() {
    int i = this.mHeight, j = this.mWidth;
    return i ^ (j >>> 16 | j << 16);
  }
}
