package android.util;

import java.io.PrintWriter;

public class PrintWriterPrinter implements Printer {
  private final PrintWriter mPW;
  
  public PrintWriterPrinter(PrintWriter paramPrintWriter) {
    this.mPW = paramPrintWriter;
  }
  
  public void println(String paramString) {
    this.mPW.println(paramString);
  }
}
