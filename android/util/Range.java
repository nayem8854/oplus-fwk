package android.util;

import android.hardware.camera2.utils.HashCodeHelpers;
import com.android.internal.util.Preconditions;

public final class Range<T extends Comparable<? super T>> {
  private final T mLower;
  
  private final T mUpper;
  
  public Range(T paramT1, T paramT2) {
    this.mLower = (T)Preconditions.checkNotNull(paramT1, "lower must not be null");
    this.mUpper = (T)Preconditions.checkNotNull(paramT2, "upper must not be null");
    if (paramT1.compareTo(paramT2) <= 0)
      return; 
    throw new IllegalArgumentException("lower must be less than or equal to upper");
  }
  
  public static <T extends Comparable<? super T>> Range<T> create(T paramT1, T paramT2) {
    return new Range<>(paramT1, paramT2);
  }
  
  public T getLower() {
    return this.mLower;
  }
  
  public T getUpper() {
    return this.mUpper;
  }
  
  public boolean contains(T paramT) {
    boolean bool2;
    Preconditions.checkNotNull(paramT, "value must not be null");
    int i = paramT.compareTo(this.mLower);
    boolean bool1 = true;
    if (i >= 0) {
      i = 1;
    } else {
      i = 0;
    } 
    if (paramT.compareTo(this.mUpper) <= 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (i == 0 || !bool2)
      bool1 = false; 
    return bool1;
  }
  
  public boolean contains(Range<T> paramRange) {
    boolean bool2;
    Preconditions.checkNotNull(paramRange, "value must not be null");
    int i = paramRange.mLower.compareTo(this.mLower);
    boolean bool1 = true;
    if (i >= 0) {
      i = 1;
    } else {
      i = 0;
    } 
    if (paramRange.mUpper.compareTo(this.mUpper) <= 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (i == 0 || !bool2)
      bool1 = false; 
    return bool1;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    if (paramObject == null)
      return false; 
    if (this == paramObject)
      return true; 
    if (paramObject instanceof Range) {
      paramObject = paramObject;
      boolean bool1 = bool;
      if (this.mLower.equals(((Range)paramObject).mLower)) {
        bool1 = bool;
        if (this.mUpper.equals(((Range)paramObject).mUpper))
          bool1 = true; 
      } 
      return bool1;
    } 
    return false;
  }
  
  public T clamp(T paramT) {
    Preconditions.checkNotNull(paramT, "value must not be null");
    if (paramT.compareTo(this.mLower) < 0)
      return this.mLower; 
    if (paramT.compareTo(this.mUpper) > 0)
      return this.mUpper; 
    return paramT;
  }
  
  public Range<T> intersect(Range<T> paramRange) {
    T t1, t2;
    Preconditions.checkNotNull(paramRange, "range must not be null");
    int i = paramRange.mLower.compareTo(this.mLower);
    int j = paramRange.mUpper.compareTo(this.mUpper);
    if (i <= 0 && j >= 0)
      return this; 
    if (i >= 0 && j <= 0)
      return paramRange; 
    if (i <= 0) {
      t2 = this.mLower;
    } else {
      t2 = paramRange.mLower;
    } 
    if (j >= 0) {
      t1 = this.mUpper;
    } else {
      t1 = ((Range)t1).mUpper;
    } 
    return create(t2, t1);
  }
  
  public Range<T> intersect(T paramT1, T paramT2) {
    Preconditions.checkNotNull(paramT1, "lower must not be null");
    Preconditions.checkNotNull(paramT2, "upper must not be null");
    int i = paramT1.compareTo(this.mLower);
    int j = paramT2.compareTo(this.mUpper);
    if (i <= 0 && j >= 0)
      return this; 
    if (i <= 0)
      paramT1 = this.mLower; 
    if (j >= 0)
      paramT2 = this.mUpper; 
    return create(paramT1, paramT2);
  }
  
  public Range<T> extend(Range<T> paramRange) {
    T t1, t2;
    Preconditions.checkNotNull(paramRange, "range must not be null");
    int i = paramRange.mLower.compareTo(this.mLower);
    int j = paramRange.mUpper.compareTo(this.mUpper);
    if (i <= 0 && j >= 0)
      return paramRange; 
    if (i >= 0 && j <= 0)
      return this; 
    if (i >= 0) {
      t2 = this.mLower;
    } else {
      t2 = paramRange.mLower;
    } 
    if (j <= 0) {
      t1 = this.mUpper;
    } else {
      t1 = ((Range)t1).mUpper;
    } 
    return create(t2, t1);
  }
  
  public Range<T> extend(T paramT1, T paramT2) {
    Preconditions.checkNotNull(paramT1, "lower must not be null");
    Preconditions.checkNotNull(paramT2, "upper must not be null");
    int i = paramT1.compareTo(this.mLower);
    int j = paramT2.compareTo(this.mUpper);
    if (i >= 0 && j <= 0)
      return this; 
    if (i >= 0)
      paramT1 = this.mLower; 
    if (j <= 0)
      paramT2 = this.mUpper; 
    return create(paramT1, paramT2);
  }
  
  public Range<T> extend(T paramT) {
    Preconditions.checkNotNull(paramT, "value must not be null");
    return extend(paramT, paramT);
  }
  
  public String toString() {
    return String.format("[%s, %s]", new Object[] { this.mLower, this.mUpper });
  }
  
  public int hashCode() {
    return HashCodeHelpers.hashCodeGeneric((Object[])new Comparable[] { (Comparable)this.mLower, (Comparable)this.mUpper });
  }
}
