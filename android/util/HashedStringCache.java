package android.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.storage.StorageManager;
import android.text.TextUtils;
import java.io.File;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class HashedStringCache {
  private static final long DAYS_TO_MILLIS = 86400000L;
  
  private static final boolean DEBUG = false;
  
  private static final int HASH_CACHE_SIZE = 100;
  
  private static final int HASH_LENGTH = 8;
  
  static final String HASH_SALT = "_hash_salt";
  
  static final String HASH_SALT_DATE = "_hash_salt_date";
  
  static final String HASH_SALT_GEN = "_hash_salt_gen";
  
  private static final int MAX_SALT_DAYS = 100;
  
  private static final String TAG = "HashedStringCache";
  
  private static final Charset UTF_8;
  
  private static HashedStringCache sHashedStringCache = null;
  
  private final MessageDigest mDigester;
  
  private final LruCache<String, String> mHashes;
  
  static {
    UTF_8 = Charset.forName("UTF-8");
  }
  
  private final Object mPreferenceLock = new Object();
  
  private byte[] mSalt;
  
  private int mSaltGen;
  
  private final SecureRandom mSecureRandom;
  
  private SharedPreferences mSharedPreferences;
  
  private HashedStringCache() {
    this.mHashes = new LruCache<>(100);
    this.mSecureRandom = new SecureRandom();
    try {
      this.mDigester = MessageDigest.getInstance("MD5");
      return;
    } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
      throw new RuntimeException(noSuchAlgorithmException);
    } 
  }
  
  public static HashedStringCache getInstance() {
    if (sHashedStringCache == null)
      sHashedStringCache = new HashedStringCache(); 
    return sHashedStringCache;
  }
  
  public HashResult hashString(Context paramContext, String paramString1, String paramString2, int paramInt) {
    if (paramInt == -1 || paramContext == null || 
      TextUtils.isEmpty(paramString2) || TextUtils.isEmpty(paramString1))
      return null; 
    populateSaltValues(paramContext, paramString1, paramInt);
    String str2 = this.mHashes.get(paramString2);
    if (str2 != null)
      return new HashResult(str2, this.mSaltGen); 
    this.mDigester.reset();
    this.mDigester.update(this.mSalt);
    this.mDigester.update(paramString2.getBytes(UTF_8));
    byte[] arrayOfByte = this.mDigester.digest();
    paramInt = Math.min(8, arrayOfByte.length);
    String str1 = Base64.encodeToString(arrayOfByte, 0, paramInt, 3);
    this.mHashes.put(paramString2, str1);
    return new HashResult(str1, this.mSaltGen);
  }
  
  private boolean checkNeedsNewSalt(String paramString, int paramInt, long paramLong) {
    boolean bool1 = true;
    if (paramLong == 0L || paramInt < -1)
      return true; 
    int i = paramInt;
    if (paramInt > 100)
      i = 100; 
    long l = System.currentTimeMillis();
    paramLong = l - paramLong;
    boolean bool2 = bool1;
    if (paramLong < i * 86400000L)
      if (paramLong < 0L) {
        bool2 = bool1;
      } else {
        bool2 = false;
      }  
    return bool2;
  }
  
  private void populateSaltValues(Context paramContext, String paramString, int paramInt) {
    synchronized (this.mPreferenceLock) {
      SharedPreferences sharedPreferences = getHashSharedPreferences(paramContext);
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append(paramString);
      stringBuilder.append("_hash_salt_date");
      long l = sharedPreferences.getLong(stringBuilder.toString(), 0L);
      boolean bool = checkNeedsNewSalt(paramString, paramInt, l);
      if (bool)
        this.mHashes.evictAll(); 
      if (this.mSalt == null || bool) {
        sharedPreferences = this.mSharedPreferences;
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(paramString);
        stringBuilder.append("_hash_salt");
        String str = sharedPreferences.getString(stringBuilder.toString(), null);
        SharedPreferences sharedPreferences1 = this.mSharedPreferences;
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append(paramString);
        stringBuilder1.append("_hash_salt_gen");
        this.mSaltGen = sharedPreferences1.getInt(stringBuilder1.toString(), 0);
        if (str == null || bool) {
          this.mSaltGen++;
          byte[] arrayOfByte = new byte[16];
          this.mSecureRandom.nextBytes(arrayOfByte);
          str = Base64.encodeToString(arrayOfByte, 3);
          SharedPreferences.Editor editor = this.mSharedPreferences.edit();
          stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append(paramString);
          stringBuilder1.append("_hash_salt");
          String str2 = stringBuilder1.toString();
          editor = editor.putString(str2, str);
          StringBuilder stringBuilder3 = new StringBuilder();
          this();
          stringBuilder3.append(paramString);
          stringBuilder3.append("_hash_salt_gen");
          String str1 = stringBuilder3.toString();
          paramInt = this.mSaltGen;
          editor = editor.putInt(str1, paramInt);
          StringBuilder stringBuilder2 = new StringBuilder();
          this();
          stringBuilder2.append(paramString);
          stringBuilder2.append("_hash_salt_date");
          paramString = stringBuilder2.toString();
          editor.putLong(paramString, System.currentTimeMillis()).apply();
        } 
        this.mSalt = str.getBytes(UTF_8);
      } 
      return;
    } 
  }
  
  private SharedPreferences getHashSharedPreferences(Context paramContext) {
    String str1 = StorageManager.UUID_PRIVATE_INTERNAL;
    int i = paramContext.getUserId();
    String str2 = paramContext.getPackageName();
    File file = new File(new File(Environment.getDataUserCePackageDirectory(str1, i, str2), "shared_prefs"), "hashed_cache.xml");
    return paramContext.getSharedPreferences(file, 0);
  }
  
  public class HashResult {
    public String hashedString;
    
    public int saltGeneration;
    
    final HashedStringCache this$0;
    
    public HashResult(String param1String, int param1Int) {
      this.hashedString = param1String;
      this.saltGeneration = param1Int;
    }
  }
}
