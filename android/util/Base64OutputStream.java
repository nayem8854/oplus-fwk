package android.util;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Base64OutputStream extends FilterOutputStream {
  private final int flags;
  
  private final Base64.Coder coder;
  
  private byte[] buffer = null;
  
  private int bpos = 0;
  
  private static byte[] EMPTY = new byte[0];
  
  public Base64OutputStream(OutputStream paramOutputStream, int paramInt) {
    this(paramOutputStream, paramInt, true);
  }
  
  public Base64OutputStream(OutputStream paramOutputStream, int paramInt, boolean paramBoolean) {
    super(paramOutputStream);
    this.flags = paramInt;
    if (paramBoolean) {
      this.coder = new Base64.Encoder(paramInt, null);
    } else {
      this.coder = new Base64.Decoder(paramInt, null);
    } 
  }
  
  public void write(int paramInt) throws IOException {
    if (this.buffer == null)
      this.buffer = new byte[1024]; 
    int i = this.bpos;
    byte[] arrayOfByte = this.buffer;
    if (i >= arrayOfByte.length) {
      internalWrite(arrayOfByte, 0, i, false);
      this.bpos = 0;
    } 
    arrayOfByte = this.buffer;
    i = this.bpos;
    this.bpos = i + 1;
    arrayOfByte[i] = (byte)paramInt;
  }
  
  private void flushBuffer() throws IOException {
    int i = this.bpos;
    if (i > 0) {
      internalWrite(this.buffer, 0, i, false);
      this.bpos = 0;
    } 
  }
  
  public void write(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws IOException {
    if (paramInt2 <= 0)
      return; 
    flushBuffer();
    internalWrite(paramArrayOfbyte, paramInt1, paramInt2, false);
  }
  
  public void close() throws IOException {
    iOException = null;
    try {
      flushBuffer();
      internalWrite(EMPTY, 0, 0, true);
    } catch (IOException iOException) {}
    try {
      if ((this.flags & 0x10) == 0) {
        this.out.close();
      } else {
        this.out.flush();
      } 
    } catch (IOException iOException1) {
      if (iOException == null) {
        iOException = iOException1;
      } else {
        iOException.addSuppressed(iOException1);
      } 
    } 
    if (iOException == null)
      return; 
    throw iOException;
  }
  
  private void internalWrite(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, boolean paramBoolean) throws IOException {
    Base64.Coder coder = this.coder;
    coder.output = embiggen(coder.output, this.coder.maxOutputSize(paramInt2));
    if (this.coder.process(paramArrayOfbyte, paramInt1, paramInt2, paramBoolean)) {
      this.out.write(this.coder.output, 0, this.coder.op);
      return;
    } 
    throw new Base64DataException("bad base-64");
  }
  
  private byte[] embiggen(byte[] paramArrayOfbyte, int paramInt) {
    if (paramArrayOfbyte == null || paramArrayOfbyte.length < paramInt)
      return new byte[paramInt]; 
    return paramArrayOfbyte;
  }
}
