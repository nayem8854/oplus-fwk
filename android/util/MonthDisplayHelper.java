package android.util;

import java.util.Calendar;

public class MonthDisplayHelper {
  private Calendar mCalendar;
  
  private int mNumDaysInMonth;
  
  private int mNumDaysInPrevMonth;
  
  private int mOffset;
  
  private final int mWeekStartDay;
  
  public MonthDisplayHelper(int paramInt1, int paramInt2, int paramInt3) {
    if (paramInt3 >= 1 && paramInt3 <= 7) {
      this.mWeekStartDay = paramInt3;
      Calendar calendar = Calendar.getInstance();
      calendar.set(1, paramInt1);
      this.mCalendar.set(2, paramInt2);
      this.mCalendar.set(5, 1);
      this.mCalendar.set(11, 0);
      this.mCalendar.set(12, 0);
      this.mCalendar.set(13, 0);
      this.mCalendar.getTimeInMillis();
      recalculate();
      return;
    } 
    throw new IllegalArgumentException();
  }
  
  public MonthDisplayHelper(int paramInt1, int paramInt2) {
    this(paramInt1, paramInt2, 1);
  }
  
  public int getYear() {
    return this.mCalendar.get(1);
  }
  
  public int getMonth() {
    return this.mCalendar.get(2);
  }
  
  public int getWeekStartDay() {
    return this.mWeekStartDay;
  }
  
  public int getFirstDayOfMonth() {
    return this.mCalendar.get(7);
  }
  
  public int getNumberOfDaysInMonth() {
    return this.mNumDaysInMonth;
  }
  
  public int getOffset() {
    return this.mOffset;
  }
  
  public int[] getDigitsForRow(int paramInt) {
    if (paramInt >= 0 && paramInt <= 5) {
      int[] arrayOfInt = new int[7];
      for (byte b = 0; b < 7; b++)
        arrayOfInt[b] = getDayAt(paramInt, b); 
      return arrayOfInt;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("row ");
    stringBuilder.append(paramInt);
    stringBuilder.append(" out of range (0-5)");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public int getDayAt(int paramInt1, int paramInt2) {
    if (paramInt1 == 0) {
      int i = this.mOffset;
      if (paramInt2 < i)
        return this.mNumDaysInPrevMonth + paramInt2 - i + 1; 
    } 
    paramInt1 = paramInt1 * 7 + paramInt2 - this.mOffset + 1;
    paramInt2 = this.mNumDaysInMonth;
    if (paramInt1 > paramInt2)
      paramInt1 -= paramInt2; 
    return paramInt1;
  }
  
  public int getRowOf(int paramInt) {
    return (this.mOffset + paramInt - 1) / 7;
  }
  
  public int getColumnOf(int paramInt) {
    return (this.mOffset + paramInt - 1) % 7;
  }
  
  public void previousMonth() {
    this.mCalendar.add(2, -1);
    recalculate();
  }
  
  public void nextMonth() {
    this.mCalendar.add(2, 1);
    recalculate();
  }
  
  public boolean isWithinCurrentMonth(int paramInt1, int paramInt2) {
    if (paramInt1 < 0 || paramInt2 < 0 || paramInt1 > 5 || paramInt2 > 6)
      return false; 
    if (paramInt1 == 0 && paramInt2 < this.mOffset)
      return false; 
    int i = this.mOffset;
    if (paramInt1 * 7 + paramInt2 - i + 1 > this.mNumDaysInMonth)
      return false; 
    return true;
  }
  
  private void recalculate() {
    this.mNumDaysInMonth = this.mCalendar.getActualMaximum(5);
    this.mCalendar.add(2, -1);
    this.mNumDaysInPrevMonth = this.mCalendar.getActualMaximum(5);
    this.mCalendar.add(2, 1);
    int i = getFirstDayOfMonth();
    int j = i - this.mWeekStartDay;
    i = j;
    if (j < 0)
      i = j + 7; 
    this.mOffset = i;
  }
}
