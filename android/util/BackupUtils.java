package android.util;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class BackupUtils {
  public static final int NOT_NULL = 1;
  
  public static final int NULL = 0;
  
  public static class BadVersionException extends Exception {
    public BadVersionException(String param1String) {
      super(param1String);
    }
  }
  
  public static String readString(DataInputStream paramDataInputStream) throws IOException {
    if (paramDataInputStream.readByte() == 1) {
      String str = paramDataInputStream.readUTF();
    } else {
      paramDataInputStream = null;
    } 
    return (String)paramDataInputStream;
  }
  
  public static void writeString(DataOutputStream paramDataOutputStream, String paramString) throws IOException {
    if (paramString != null) {
      paramDataOutputStream.writeByte(1);
      paramDataOutputStream.writeUTF(paramString);
    } else {
      paramDataOutputStream.writeByte(0);
    } 
  }
}
