package android.util;

public class TypedValue {
  public int type;
  
  public CharSequence string;
  
  public int sourceResourceId;
  
  public int resourceId;
  
  public int density;
  
  public int data;
  
  public int changingConfigurations = -1;
  
  public int assetCookie;
  
  public static final int TYPE_STRING = 3;
  
  public static final int TYPE_REFERENCE = 1;
  
  public static final int TYPE_NULL = 0;
  
  public static final int TYPE_LAST_INT = 31;
  
  public static final int TYPE_LAST_COLOR_INT = 31;
  
  public static final int TYPE_INT_HEX = 17;
  
  public static final int TYPE_INT_DEC = 16;
  
  public static final int TYPE_INT_COLOR_RGB8 = 29;
  
  public static final int TYPE_INT_COLOR_RGB4 = 31;
  
  public static final int TYPE_INT_COLOR_ARGB8 = 28;
  
  public static final int TYPE_INT_COLOR_ARGB4 = 30;
  
  public static final int TYPE_INT_BOOLEAN = 18;
  
  public static final int TYPE_FRACTION = 6;
  
  public static final int TYPE_FLOAT = 4;
  
  public static final int TYPE_FIRST_INT = 16;
  
  public static final int TYPE_FIRST_COLOR_INT = 28;
  
  public static final int TYPE_DIMENSION = 5;
  
  public static final int TYPE_ATTRIBUTE = 2;
  
  public final float getFloat() {
    return Float.intBitsToFloat(this.data);
  }
  
  private static final float[] RADIX_MULTS = new float[] { 0.00390625F, 3.0517578E-5F, 1.1920929E-7F, 4.656613E-10F };
  
  private static final float MANTISSA_MULT = 0.00390625F;
  
  private static final String[] FRACTION_UNIT_STRS;
  
  private static final String[] DIMENSION_UNIT_STRS;
  
  public static final int DENSITY_NONE = 65535;
  
  public static final int DENSITY_DEFAULT = 0;
  
  public static final int DATA_NULL_UNDEFINED = 0;
  
  public static final int DATA_NULL_EMPTY = 1;
  
  public static final int COMPLEX_UNIT_SP = 2;
  
  public static final int COMPLEX_UNIT_SHIFT = 0;
  
  public static final int COMPLEX_UNIT_PX = 0;
  
  public static final int COMPLEX_UNIT_PT = 3;
  
  public static final int COMPLEX_UNIT_MM = 5;
  
  public static final int COMPLEX_UNIT_MASK = 15;
  
  public static final int COMPLEX_UNIT_IN = 4;
  
  public static final int COMPLEX_UNIT_FRACTION_PARENT = 1;
  
  public static final int COMPLEX_UNIT_FRACTION = 0;
  
  public static final int COMPLEX_UNIT_DIP = 1;
  
  public static final int COMPLEX_RADIX_SHIFT = 4;
  
  public static final int COMPLEX_RADIX_MASK = 3;
  
  public static final int COMPLEX_RADIX_8p15 = 2;
  
  public static final int COMPLEX_RADIX_23p0 = 0;
  
  public static final int COMPLEX_RADIX_16p7 = 1;
  
  public static final int COMPLEX_RADIX_0p23 = 3;
  
  public static final int COMPLEX_MANTISSA_SHIFT = 8;
  
  public static final int COMPLEX_MANTISSA_MASK = 16777215;
  
  public boolean isColorType() {
    boolean bool;
    int i = this.type;
    if (i >= 28 && i <= 31) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static float complexToFloat(int paramInt) {
    return (paramInt & 0xFFFFFF00) * RADIX_MULTS[paramInt >> 4 & 0x3];
  }
  
  public static float complexToDimension(int paramInt, DisplayMetrics paramDisplayMetrics) {
    float f = complexToFloat(paramInt);
    return applyDimension(paramInt >> 0 & 0xF, f, paramDisplayMetrics);
  }
  
  public static int complexToDimensionPixelOffset(int paramInt, DisplayMetrics paramDisplayMetrics) {
    float f = complexToFloat(paramInt);
    return (int)applyDimension(paramInt >> 0 & 0xF, f, paramDisplayMetrics);
  }
  
  public static int complexToDimensionPixelSize(int paramInt, DisplayMetrics paramDisplayMetrics) {
    float f1 = complexToFloat(paramInt);
    float f2 = applyDimension(paramInt >> 0 & 0xF, f1, paramDisplayMetrics);
    if (f2 >= 0.0F) {
      f2 = 0.5F + f2;
    } else {
      f2 -= 0.5F;
    } 
    paramInt = (int)f2;
    if (paramInt != 0)
      return paramInt; 
    if (f1 == 0.0F)
      return 0; 
    if (f1 > 0.0F)
      return 1; 
    return -1;
  }
  
  @Deprecated
  public static float complexToDimensionNoisy(int paramInt, DisplayMetrics paramDisplayMetrics) {
    return complexToDimension(paramInt, paramDisplayMetrics);
  }
  
  public int getComplexUnit() {
    return this.data >> 0 & 0xF;
  }
  
  public static float applyDimension(int paramInt, float paramFloat, DisplayMetrics paramDisplayMetrics) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt != 4) {
              if (paramInt != 5)
                return 0.0F; 
              return paramDisplayMetrics.xdpi * paramFloat * 0.03937008F;
            } 
            return paramDisplayMetrics.xdpi * paramFloat;
          } 
          return paramDisplayMetrics.xdpi * paramFloat * 0.013888889F;
        } 
        return paramDisplayMetrics.scaledDensity * paramFloat;
      } 
      return paramDisplayMetrics.density * paramFloat;
    } 
    return paramFloat;
  }
  
  public float getDimension(DisplayMetrics paramDisplayMetrics) {
    return complexToDimension(this.data, paramDisplayMetrics);
  }
  
  public static float complexToFraction(int paramInt, float paramFloat1, float paramFloat2) {
    int i = paramInt >> 0 & 0xF;
    if (i != 0) {
      if (i != 1)
        return 0.0F; 
      return complexToFloat(paramInt) * paramFloat2;
    } 
    return complexToFloat(paramInt) * paramFloat1;
  }
  
  public float getFraction(float paramFloat1, float paramFloat2) {
    return complexToFraction(this.data, paramFloat1, paramFloat2);
  }
  
  public final CharSequence coerceToString() {
    int i = this.type;
    if (i == 3)
      return this.string; 
    return coerceToString(i, this.data);
  }
  
  static {
    DIMENSION_UNIT_STRS = new String[] { "px", "dip", "sp", "pt", "in", "mm" };
    FRACTION_UNIT_STRS = new String[] { "%", "%p" };
  }
  
  public static final String coerceToString(int paramInt1, int paramInt2) {
    if (paramInt1 != 0) {
      if (paramInt1 != 1) {
        if (paramInt1 != 2) {
          if (paramInt1 != 4) {
            if (paramInt1 != 5) {
              if (paramInt1 != 6) {
                if (paramInt1 != 17) {
                  String str;
                  if (paramInt1 != 18) {
                    if (paramInt1 >= 28 && paramInt1 <= 31) {
                      StringBuilder stringBuilder5 = new StringBuilder();
                      stringBuilder5.append("#");
                      stringBuilder5.append(Integer.toHexString(paramInt2));
                      return stringBuilder5.toString();
                    } 
                    if (paramInt1 >= 16 && paramInt1 <= 31)
                      return Integer.toString(paramInt2); 
                    return null;
                  } 
                  if (paramInt2 != 0) {
                    str = "true";
                  } else {
                    str = "false";
                  } 
                  return str;
                } 
                StringBuilder stringBuilder4 = new StringBuilder();
                stringBuilder4.append("0x");
                stringBuilder4.append(Integer.toHexString(paramInt2));
                return stringBuilder4.toString();
              } 
              StringBuilder stringBuilder3 = new StringBuilder();
              stringBuilder3.append(Float.toString(complexToFloat(paramInt2) * 100.0F));
              stringBuilder3.append(FRACTION_UNIT_STRS[paramInt2 >> 0 & 0xF]);
              return stringBuilder3.toString();
            } 
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append(Float.toString(complexToFloat(paramInt2)));
            stringBuilder2.append(DIMENSION_UNIT_STRS[paramInt2 >> 0 & 0xF]);
            return stringBuilder2.toString();
          } 
          return Float.toString(Float.intBitsToFloat(paramInt2));
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("?");
        stringBuilder1.append(paramInt2);
        return stringBuilder1.toString();
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("@");
      stringBuilder.append(paramInt2);
      return stringBuilder.toString();
    } 
    return null;
  }
  
  public void setTo(TypedValue paramTypedValue) {
    this.type = paramTypedValue.type;
    this.string = paramTypedValue.string;
    this.data = paramTypedValue.data;
    this.assetCookie = paramTypedValue.assetCookie;
    this.resourceId = paramTypedValue.resourceId;
    this.density = paramTypedValue.density;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("TypedValue{t=0x");
    stringBuilder.append(Integer.toHexString(this.type));
    stringBuilder.append("/d=0x");
    stringBuilder.append(Integer.toHexString(this.data));
    if (this.type == 3) {
      stringBuilder.append(" \"");
      CharSequence charSequence = this.string;
      if (charSequence == null)
        charSequence = "<null>"; 
      stringBuilder.append(charSequence);
      stringBuilder.append("\"");
    } 
    if (this.assetCookie != 0) {
      stringBuilder.append(" a=");
      stringBuilder.append(this.assetCookie);
    } 
    if (this.resourceId != 0) {
      stringBuilder.append(" r=0x");
      stringBuilder.append(Integer.toHexString(this.resourceId));
    } 
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
}
