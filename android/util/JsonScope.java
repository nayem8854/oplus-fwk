package android.util;

enum JsonScope {
  CLOSED, DANGLING_NAME, EMPTY_ARRAY, EMPTY_DOCUMENT, EMPTY_OBJECT, NONEMPTY_ARRAY, NONEMPTY_DOCUMENT, NONEMPTY_OBJECT;
  
  private static final JsonScope[] $VALUES;
  
  static {
    EMPTY_OBJECT = new JsonScope("EMPTY_OBJECT", 2);
    DANGLING_NAME = new JsonScope("DANGLING_NAME", 3);
    NONEMPTY_OBJECT = new JsonScope("NONEMPTY_OBJECT", 4);
    EMPTY_DOCUMENT = new JsonScope("EMPTY_DOCUMENT", 5);
    NONEMPTY_DOCUMENT = new JsonScope("NONEMPTY_DOCUMENT", 6);
    JsonScope jsonScope = new JsonScope("CLOSED", 7);
    $VALUES = new JsonScope[] { EMPTY_ARRAY, NONEMPTY_ARRAY, EMPTY_OBJECT, DANGLING_NAME, NONEMPTY_OBJECT, EMPTY_DOCUMENT, NONEMPTY_DOCUMENT, jsonScope };
  }
}
