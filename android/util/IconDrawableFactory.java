package android.util;

import android.app.ActivityThread;
import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.res.IOplusThemeManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.UserHandle;
import android.os.UserManager;
import com.oplus.multiapp.OplusMultiAppManager;

public class IconDrawableFactory {
  protected final Context mContext;
  
  protected final boolean mEmbedShadow;
  
  protected final LauncherIcons mLauncherIcons;
  
  protected final PackageManager mPm;
  
  protected final UserManager mUm;
  
  private IconDrawableFactory(Context paramContext, boolean paramBoolean) {
    this.mContext = paramContext;
    this.mPm = paramContext.getPackageManager();
    this.mUm = (UserManager)paramContext.getSystemService(UserManager.class);
    this.mLauncherIcons = new LauncherIcons(paramContext);
    this.mEmbedShadow = paramBoolean;
  }
  
  protected boolean needsBadging(ApplicationInfo paramApplicationInfo, int paramInt) {
    return (paramApplicationInfo.isInstantApp() || this.mUm.isManagedProfile(paramInt));
  }
  
  public Drawable getBadgedIcon(ApplicationInfo paramApplicationInfo) {
    return getBadgedIcon(paramApplicationInfo, UserHandle.getUserId(paramApplicationInfo.uid));
  }
  
  public Drawable getBadgedIcon(ApplicationInfo paramApplicationInfo, int paramInt) {
    return getBadgedIcon((PackageItemInfo)paramApplicationInfo, paramApplicationInfo, paramInt);
  }
  
  public Drawable getBadgedIcon(PackageItemInfo paramPackageItemInfo, ApplicationInfo paramApplicationInfo, int paramInt) {
    Drawable arrayOfDrawable1[], drawable2, drawable1 = this.mPm.loadUnbadgedItemIcon(paramPackageItemInfo, paramApplicationInfo);
    if (!this.mEmbedShadow && !needsBadging(paramApplicationInfo, paramInt))
      return drawable1; 
    Drawable drawable3 = drawable1;
    if (!((IOplusThemeManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusThemeManager.DEFAULT, new Object[0])).isOplusIcons())
      drawable3 = getShadowedIcon(drawable1); 
    drawable1 = drawable3;
    if (paramApplicationInfo.isInstantApp()) {
      int i = Resources.getSystem().getColor(17170810, null);
      drawable1 = this.mLauncherIcons.getBadgedDrawable(drawable3, 17302457, i);
    } 
    if (OplusMultiAppManager.getInstance().isMultiAppUserId(paramInt)) {
      Resources resources = ActivityThread.currentActivityThread().getApplication().getResources();
      drawable2 = resources.getDrawable(201850903);
      if (drawable1 == null) {
        arrayOfDrawable1 = new Drawable[1];
        arrayOfDrawable1[0] = drawable2;
      } else {
        arrayOfDrawable1 = new Drawable[] { (Drawable)arrayOfDrawable1, drawable2 };
      } 
      return (Drawable)new LayerDrawable(arrayOfDrawable1);
    } 
    Drawable[] arrayOfDrawable2 = arrayOfDrawable1;
    if (this.mUm.hasBadge(paramInt)) {
      LauncherIcons launcherIcons = this.mLauncherIcons;
      UserManager userManager = this.mUm;
      int i = userManager.getUserIconBadgeResId(paramInt);
      userManager = this.mUm;
      paramInt = userManager.getUserBadgeColor(paramInt);
      drawable2 = launcherIcons.getBadgedDrawable((Drawable)arrayOfDrawable1, i, paramInt);
    } 
    return drawable2;
  }
  
  public Drawable getShadowedIcon(Drawable paramDrawable) {
    return this.mLauncherIcons.wrapIconDrawableWithShadow(paramDrawable);
  }
  
  public static IconDrawableFactory newInstance(Context paramContext) {
    return new IconDrawableFactory(paramContext, true);
  }
  
  public static IconDrawableFactory newInstance(Context paramContext, boolean paramBoolean) {
    return new IconDrawableFactory(paramContext, paramBoolean);
  }
}
