package android.util;

public abstract class Property<T, V> {
  private final String mName;
  
  private final Class<V> mType;
  
  public static <T, V> Property<T, V> of(Class<T> paramClass, Class<V> paramClass1, String paramString) {
    return new ReflectiveProperty<>(paramClass, paramClass1, paramString);
  }
  
  public Property(Class<V> paramClass, String paramString) {
    this.mName = paramString;
    this.mType = paramClass;
  }
  
  public boolean isReadOnly() {
    return false;
  }
  
  public void set(T paramT, V paramV) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Property ");
    stringBuilder.append(getName());
    stringBuilder.append(" is read-only");
    throw new UnsupportedOperationException(stringBuilder.toString());
  }
  
  public String getName() {
    return this.mName;
  }
  
  public Class<V> getType() {
    return this.mType;
  }
  
  public abstract V get(T paramT);
}
