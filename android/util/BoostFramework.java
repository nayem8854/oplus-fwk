package android.util;

import android.content.Context;
import android.os.SystemProperties;
import android.view.Surface;
import dalvik.system.PathClassLoader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class BoostFramework {
  public static final int MPCTLV3_GPU_IS_APP_BG = 1115832320;
  
  public static final int MPCTLV3_GPU_IS_APP_FG = 1115815936;
  
  private static final String PERFORMANCE_CLASS = "com.qualcomm.qti.Performance";
  
  private static final String PERFORMANCE_JAR = "/system/framework/QPerformance.jar";
  
  private static final String TAG = "BoostFramework";
  
  public static final int UXE_EVENT_BINDAPP = 2;
  
  public static final int UXE_EVENT_DISPLAYED_ACT = 3;
  
  public static final int UXE_EVENT_GAME = 5;
  
  public static final int UXE_EVENT_KILL = 4;
  
  public static final int UXE_EVENT_PKG_INSTALL = 8;
  
  public static final int UXE_EVENT_PKG_UNINSTALL = 7;
  
  public static final int UXE_EVENT_SUB_LAUNCH = 6;
  
  public static final int UXE_TRIGGER = 1;
  
  private static final String UXPERFORMANCE_CLASS = "com.qualcomm.qti.UxPerformance";
  
  private static final String UXPERFORMANCE_JAR = "/system/framework/UxPerformance.jar";
  
  public static final int VENDOR_FEEDBACK_LAUNCH_END_POINT = 5634;
  
  public static final int VENDOR_FEEDBACK_WORKLOAD_TYPE = 5633;
  
  public static final int VENDOR_HINT_ACTIVITY_BOOST = 4228;
  
  public static final int VENDOR_HINT_ANIM_BOOST = 4227;
  
  public static final int VENDOR_HINT_APP_UPDATE = 4242;
  
  public static final int VENDOR_HINT_BOOST_RENDERTHREAD = 4246;
  
  public static final int VENDOR_HINT_DRAG_BOOST = 4231;
  
  public static final int VENDOR_HINT_FIRST_DRAW = 4162;
  
  public static final int VENDOR_HINT_FIRST_LAUNCH_BOOST = 4225;
  
  public static final int VENDOR_HINT_KILL = 4243;
  
  public static final int VENDOR_HINT_MTP_BOOST = 4230;
  
  public static final int VENDOR_HINT_PACKAGE_INSTALL_BOOST = 4232;
  
  public static final int VENDOR_HINT_PERFORMANCE_MODE = 4241;
  
  public static final int VENDOR_HINT_ROTATION_ANIM_BOOST = 4240;
  
  public static final int VENDOR_HINT_ROTATION_LATENCY_BOOST = 4233;
  
  public static final int VENDOR_HINT_SCROLL_BOOST = 4224;
  
  public static final int VENDOR_HINT_SUBSEQ_LAUNCH_BOOST = 4226;
  
  public static final int VENDOR_HINT_TAP_EVENT = 4163;
  
  public static final int VENDOR_HINT_TOUCH_BOOST = 4229;
  
  private static Method sAcqAndReleaseFunc;
  
  private static Method sAcquireFunc;
  
  private static Method sFeedbackFunc;
  
  private static Method sIOPStart;
  
  private static Method sIOPStop;
  
  private static boolean sIsLoaded = false;
  
  private static Class<?> sPerfClass = null;
  
  private static Method sPerfGetPropFunc;
  
  private static Method sPerfHintFunc;
  
  private static Method sReleaseFunc;
  
  private static Method sReleaseHandlerFunc;
  
  private static Method sUXEngineEvents;
  
  private static Method sUXEngineTrigger;
  
  private static Method sUxIOPStart;
  
  private static boolean sUxIsLoaded;
  
  private static Class<?> sUxPerfClass;
  
  static {
    sAcquireFunc = null;
    sPerfHintFunc = null;
    sReleaseFunc = null;
    sReleaseHandlerFunc = null;
    sFeedbackFunc = null;
    sPerfGetPropFunc = null;
    sAcqAndReleaseFunc = null;
    sIOPStart = null;
    sIOPStop = null;
    sUXEngineEvents = null;
    sUXEngineTrigger = null;
    sUxIsLoaded = false;
    sUxPerfClass = null;
    sUxIOPStart = null;
  }
  
  private Object mPerf = null;
  
  private Object mUxPerf = null;
  
  public class Scroll {
    public static final int HORIZONTAL = 2;
    
    public static final int PANEL_VIEW = 3;
    
    public static final int PREFILING = 4;
    
    public static final int VERTICAL = 1;
    
    final BoostFramework this$0;
  }
  
  public class Launch {
    public static final int BOOST_GAME = 4;
    
    public static final int BOOST_V1 = 1;
    
    public static final int BOOST_V2 = 2;
    
    public static final int BOOST_V3 = 3;
    
    public static final int RESERVED_1 = 5;
    
    public static final int RESERVED_2 = 6;
    
    public static final int TYPE_ATTACH_APPLICATION = 103;
    
    public static final int TYPE_SERVICE_START = 100;
    
    public static final int TYPE_START_APP_FROM_BG = 102;
    
    public static final int TYPE_START_PROC = 101;
    
    final BoostFramework this$0;
  }
  
  public class Draw {
    public static final int EVENT_TYPE_V1 = 1;
    
    final BoostFramework this$0;
  }
  
  public class WorkloadType {
    public static final int APP = 1;
    
    public static final int BROWSER = 3;
    
    public static final int GAME = 2;
    
    public static final int NOT_KNOWN = 0;
    
    public static final int PREPROAPP = 4;
    
    final BoostFramework this$0;
  }
  
  public BoostFramework() {
    initFunctions();
    try {
      if (sPerfClass != null)
        this.mPerf = sPerfClass.newInstance(); 
      if (sUxPerfClass != null)
        this.mUxPerf = sUxPerfClass.newInstance(); 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("BoostFramework() : Exception_2 = ");
      stringBuilder.append(exception);
      Log.e("BoostFramework", stringBuilder.toString());
    } 
  }
  
  public BoostFramework(Context paramContext) {
    this(paramContext, false);
  }
  
  public BoostFramework(Context paramContext, boolean paramBoolean) {
    initFunctions();
    try {
      if (sPerfClass != null) {
        Constructor<?> constructor = sPerfClass.getConstructor(new Class[] { Context.class });
        if (constructor != null)
          this.mPerf = constructor.newInstance(new Object[] { paramContext }); 
      } 
      if (sUxPerfClass != null)
        if (paramBoolean) {
          Constructor<?> constructor = sUxPerfClass.getConstructor(new Class[] { Context.class });
          if (constructor != null)
            this.mUxPerf = constructor.newInstance(new Object[] { paramContext }); 
        } else {
          this.mUxPerf = sUxPerfClass.newInstance();
        }  
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("BoostFramework() : Exception_3 = ");
      stringBuilder.append(exception);
      Log.e("BoostFramework", stringBuilder.toString());
    } 
  }
  
  public BoostFramework(boolean paramBoolean) {
    initFunctions();
    try {
      if (sPerfClass != null) {
        Constructor<?> constructor = sPerfClass.getConstructor(new Class[] { boolean.class });
        if (constructor != null)
          this.mPerf = constructor.newInstance(new Object[] { Boolean.valueOf(paramBoolean) }); 
      } 
      if (sUxPerfClass != null)
        this.mUxPerf = sUxPerfClass.newInstance(); 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("BoostFramework() : Exception_5 = ");
      stringBuilder.append(exception);
      Log.e("BoostFramework", stringBuilder.toString());
    } 
  }
  
  private void initFunctions() {
    // Byte code:
    //   0: ldc android/util/BoostFramework
    //   2: monitorenter
    //   3: getstatic android/util/BoostFramework.sIsLoaded : Z
    //   6: istore_1
    //   7: iload_1
    //   8: ifne -> 504
    //   11: ldc 'com.qualcomm.qti.Performance'
    //   13: invokestatic forName : (Ljava/lang/String;)Ljava/lang/Class;
    //   16: putstatic android/util/BoostFramework.sPerfClass : Ljava/lang/Class;
    //   19: getstatic java/lang/Integer.TYPE : Ljava/lang/Class;
    //   22: astore_2
    //   23: getstatic android/util/BoostFramework.sPerfClass : Ljava/lang/Class;
    //   26: ldc 'perfLockAcquire'
    //   28: iconst_2
    //   29: anewarray java/lang/Class
    //   32: dup
    //   33: iconst_0
    //   34: aload_2
    //   35: aastore
    //   36: dup
    //   37: iconst_1
    //   38: ldc [I
    //   40: aastore
    //   41: invokevirtual getMethod : (Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   44: putstatic android/util/BoostFramework.sAcquireFunc : Ljava/lang/reflect/Method;
    //   47: getstatic java/lang/Integer.TYPE : Ljava/lang/Class;
    //   50: astore_2
    //   51: getstatic java/lang/Integer.TYPE : Ljava/lang/Class;
    //   54: astore_3
    //   55: getstatic java/lang/Integer.TYPE : Ljava/lang/Class;
    //   58: astore #4
    //   60: getstatic android/util/BoostFramework.sPerfClass : Ljava/lang/Class;
    //   63: ldc 'perfHint'
    //   65: iconst_4
    //   66: anewarray java/lang/Class
    //   69: dup
    //   70: iconst_0
    //   71: aload_2
    //   72: aastore
    //   73: dup
    //   74: iconst_1
    //   75: ldc java/lang/String
    //   77: aastore
    //   78: dup
    //   79: iconst_2
    //   80: aload_3
    //   81: aastore
    //   82: dup
    //   83: iconst_3
    //   84: aload #4
    //   86: aastore
    //   87: invokevirtual getMethod : (Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   90: putstatic android/util/BoostFramework.sPerfHintFunc : Ljava/lang/reflect/Method;
    //   93: getstatic android/util/BoostFramework.sPerfClass : Ljava/lang/Class;
    //   96: ldc 'perfLockRelease'
    //   98: iconst_0
    //   99: anewarray java/lang/Class
    //   102: invokevirtual getMethod : (Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   105: putstatic android/util/BoostFramework.sReleaseFunc : Ljava/lang/reflect/Method;
    //   108: getstatic java/lang/Integer.TYPE : Ljava/lang/Class;
    //   111: astore_2
    //   112: getstatic android/util/BoostFramework.sPerfClass : Ljava/lang/Class;
    //   115: ldc 'perfLockReleaseHandler'
    //   117: iconst_1
    //   118: anewarray java/lang/Class
    //   121: dup
    //   122: iconst_0
    //   123: aload_2
    //   124: aastore
    //   125: invokevirtual getDeclaredMethod : (Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   128: putstatic android/util/BoostFramework.sReleaseHandlerFunc : Ljava/lang/reflect/Method;
    //   131: getstatic java/lang/Integer.TYPE : Ljava/lang/Class;
    //   134: astore_2
    //   135: getstatic android/util/BoostFramework.sPerfClass : Ljava/lang/Class;
    //   138: ldc 'perfGetFeedback'
    //   140: iconst_2
    //   141: anewarray java/lang/Class
    //   144: dup
    //   145: iconst_0
    //   146: aload_2
    //   147: aastore
    //   148: dup
    //   149: iconst_1
    //   150: ldc java/lang/String
    //   152: aastore
    //   153: invokevirtual getMethod : (Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   156: putstatic android/util/BoostFramework.sFeedbackFunc : Ljava/lang/reflect/Method;
    //   159: getstatic java/lang/Integer.TYPE : Ljava/lang/Class;
    //   162: astore_2
    //   163: getstatic android/util/BoostFramework.sPerfClass : Ljava/lang/Class;
    //   166: ldc 'perfIOPrefetchStart'
    //   168: iconst_3
    //   169: anewarray java/lang/Class
    //   172: dup
    //   173: iconst_0
    //   174: aload_2
    //   175: aastore
    //   176: dup
    //   177: iconst_1
    //   178: ldc java/lang/String
    //   180: aastore
    //   181: dup
    //   182: iconst_2
    //   183: ldc java/lang/String
    //   185: aastore
    //   186: invokevirtual getDeclaredMethod : (Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   189: putstatic android/util/BoostFramework.sIOPStart : Ljava/lang/reflect/Method;
    //   192: getstatic android/util/BoostFramework.sPerfClass : Ljava/lang/Class;
    //   195: ldc 'perfIOPrefetchStop'
    //   197: iconst_0
    //   198: anewarray java/lang/Class
    //   201: invokevirtual getDeclaredMethod : (Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   204: putstatic android/util/BoostFramework.sIOPStop : Ljava/lang/reflect/Method;
    //   207: getstatic android/util/BoostFramework.sPerfClass : Ljava/lang/Class;
    //   210: ldc_w 'perfGetProp'
    //   213: iconst_2
    //   214: anewarray java/lang/Class
    //   217: dup
    //   218: iconst_0
    //   219: ldc java/lang/String
    //   221: aastore
    //   222: dup
    //   223: iconst_1
    //   224: ldc java/lang/String
    //   226: aastore
    //   227: invokevirtual getMethod : (Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   230: putstatic android/util/BoostFramework.sPerfGetPropFunc : Ljava/lang/reflect/Method;
    //   233: getstatic java/lang/Integer.TYPE : Ljava/lang/Class;
    //   236: astore #4
    //   238: getstatic java/lang/Integer.TYPE : Ljava/lang/Class;
    //   241: astore_2
    //   242: getstatic java/lang/Integer.TYPE : Ljava/lang/Class;
    //   245: astore_3
    //   246: getstatic java/lang/Integer.TYPE : Ljava/lang/Class;
    //   249: astore #5
    //   251: getstatic android/util/BoostFramework.sPerfClass : Ljava/lang/Class;
    //   254: ldc_w 'perfLockAcqAndRelease'
    //   257: iconst_5
    //   258: anewarray java/lang/Class
    //   261: dup
    //   262: iconst_0
    //   263: aload #4
    //   265: aastore
    //   266: dup
    //   267: iconst_1
    //   268: aload_2
    //   269: aastore
    //   270: dup
    //   271: iconst_2
    //   272: aload_3
    //   273: aastore
    //   274: dup
    //   275: iconst_3
    //   276: aload #5
    //   278: aastore
    //   279: dup
    //   280: iconst_4
    //   281: ldc [I
    //   283: aastore
    //   284: invokevirtual getMethod : (Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   287: putstatic android/util/BoostFramework.sAcqAndReleaseFunc : Ljava/lang/reflect/Method;
    //   290: getstatic java/lang/Integer.TYPE : Ljava/lang/Class;
    //   293: astore #4
    //   295: getstatic java/lang/Integer.TYPE : Ljava/lang/Class;
    //   298: astore_3
    //   299: getstatic java/lang/Integer.TYPE : Ljava/lang/Class;
    //   302: astore_2
    //   303: getstatic android/util/BoostFramework.sPerfClass : Ljava/lang/Class;
    //   306: ldc_w 'perfUXEngine_events'
    //   309: iconst_5
    //   310: anewarray java/lang/Class
    //   313: dup
    //   314: iconst_0
    //   315: aload #4
    //   317: aastore
    //   318: dup
    //   319: iconst_1
    //   320: aload_3
    //   321: aastore
    //   322: dup
    //   323: iconst_2
    //   324: ldc java/lang/String
    //   326: aastore
    //   327: dup
    //   328: iconst_3
    //   329: aload_2
    //   330: aastore
    //   331: dup
    //   332: iconst_4
    //   333: ldc java/lang/String
    //   335: aastore
    //   336: invokevirtual getDeclaredMethod : (Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   339: putstatic android/util/BoostFramework.sUXEngineEvents : Ljava/lang/reflect/Method;
    //   342: getstatic java/lang/Integer.TYPE : Ljava/lang/Class;
    //   345: astore_2
    //   346: getstatic android/util/BoostFramework.sPerfClass : Ljava/lang/Class;
    //   349: ldc_w 'perfUXEngine_trigger'
    //   352: iconst_1
    //   353: anewarray java/lang/Class
    //   356: dup
    //   357: iconst_0
    //   358: aload_2
    //   359: aastore
    //   360: invokevirtual getDeclaredMethod : (Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   363: putstatic android/util/BoostFramework.sUXEngineTrigger : Ljava/lang/reflect/Method;
    //   366: goto -> 379
    //   369: astore_2
    //   370: ldc 'BoostFramework'
    //   372: ldc_w 'BoostFramework() : Exception_4 = PreferredApps not supported'
    //   375: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   378: pop
    //   379: iconst_1
    //   380: putstatic android/util/BoostFramework.sIsLoaded : Z
    //   383: goto -> 421
    //   386: astore #4
    //   388: new java/lang/StringBuilder
    //   391: astore_2
    //   392: aload_2
    //   393: invokespecial <init> : ()V
    //   396: aload_2
    //   397: ldc_w 'BoostFramework() : Exception_1 = '
    //   400: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   403: pop
    //   404: aload_2
    //   405: aload #4
    //   407: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   410: pop
    //   411: ldc 'BoostFramework'
    //   413: aload_2
    //   414: invokevirtual toString : ()Ljava/lang/String;
    //   417: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   420: pop
    //   421: ldc 'com.qualcomm.qti.UxPerformance'
    //   423: invokestatic forName : (Ljava/lang/String;)Ljava/lang/Class;
    //   426: putstatic android/util/BoostFramework.sUxPerfClass : Ljava/lang/Class;
    //   429: getstatic java/lang/Integer.TYPE : Ljava/lang/Class;
    //   432: astore_2
    //   433: getstatic android/util/BoostFramework.sUxPerfClass : Ljava/lang/Class;
    //   436: ldc 'perfIOPrefetchStart'
    //   438: iconst_3
    //   439: anewarray java/lang/Class
    //   442: dup
    //   443: iconst_0
    //   444: aload_2
    //   445: aastore
    //   446: dup
    //   447: iconst_1
    //   448: ldc java/lang/String
    //   450: aastore
    //   451: dup
    //   452: iconst_2
    //   453: ldc java/lang/String
    //   455: aastore
    //   456: invokevirtual getDeclaredMethod : (Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   459: putstatic android/util/BoostFramework.sUxIOPStart : Ljava/lang/reflect/Method;
    //   462: iconst_1
    //   463: putstatic android/util/BoostFramework.sUxIsLoaded : Z
    //   466: goto -> 504
    //   469: astore #4
    //   471: new java/lang/StringBuilder
    //   474: astore_2
    //   475: aload_2
    //   476: invokespecial <init> : ()V
    //   479: aload_2
    //   480: ldc_w 'BoostFramework() Ux Perf: Exception = '
    //   483: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   486: pop
    //   487: aload_2
    //   488: aload #4
    //   490: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   493: pop
    //   494: ldc 'BoostFramework'
    //   496: aload_2
    //   497: invokevirtual toString : ()Ljava/lang/String;
    //   500: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   503: pop
    //   504: ldc android/util/BoostFramework
    //   506: monitorexit
    //   507: return
    //   508: astore_2
    //   509: ldc android/util/BoostFramework
    //   511: monitorexit
    //   512: aload_2
    //   513: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #212	-> 0
    //   #213	-> 3
    //   #215	-> 11
    //   #217	-> 19
    //   #218	-> 23
    //   #220	-> 47
    //   #221	-> 60
    //   #223	-> 93
    //   #224	-> 93
    //   #226	-> 108
    //   #227	-> 112
    //   #229	-> 131
    //   #230	-> 135
    //   #232	-> 159
    //   #233	-> 163
    //   #235	-> 192
    //   #236	-> 192
    //   #238	-> 207
    //   #239	-> 207
    //   #241	-> 233
    //   #242	-> 251
    //   #245	-> 290
    //   #246	-> 303
    //   #249	-> 342
    //   #250	-> 346
    //   #254	-> 366
    //   #252	-> 369
    //   #253	-> 370
    //   #256	-> 379
    //   #260	-> 383
    //   #258	-> 386
    //   #259	-> 388
    //   #264	-> 421
    //   #266	-> 429
    //   #267	-> 433
    //   #269	-> 462
    //   #273	-> 466
    //   #271	-> 469
    //   #272	-> 471
    //   #275	-> 504
    //   #276	-> 507
    //   #275	-> 508
    // Exception table:
    //   from	to	target	type
    //   3	7	508	finally
    //   11	19	386	java/lang/Exception
    //   11	19	508	finally
    //   19	23	386	java/lang/Exception
    //   19	23	508	finally
    //   23	47	386	java/lang/Exception
    //   23	47	508	finally
    //   47	60	386	java/lang/Exception
    //   47	60	508	finally
    //   60	93	386	java/lang/Exception
    //   60	93	508	finally
    //   93	108	386	java/lang/Exception
    //   93	108	508	finally
    //   108	112	386	java/lang/Exception
    //   108	112	508	finally
    //   112	131	386	java/lang/Exception
    //   112	131	508	finally
    //   131	135	386	java/lang/Exception
    //   131	135	508	finally
    //   135	159	386	java/lang/Exception
    //   135	159	508	finally
    //   159	163	386	java/lang/Exception
    //   159	163	508	finally
    //   163	192	386	java/lang/Exception
    //   163	192	508	finally
    //   192	207	386	java/lang/Exception
    //   192	207	508	finally
    //   207	233	386	java/lang/Exception
    //   207	233	508	finally
    //   233	251	386	java/lang/Exception
    //   233	251	508	finally
    //   251	290	386	java/lang/Exception
    //   251	290	508	finally
    //   290	303	369	java/lang/Exception
    //   290	303	508	finally
    //   303	342	369	java/lang/Exception
    //   303	342	508	finally
    //   342	346	369	java/lang/Exception
    //   342	346	508	finally
    //   346	366	369	java/lang/Exception
    //   346	366	508	finally
    //   370	379	386	java/lang/Exception
    //   370	379	508	finally
    //   379	383	386	java/lang/Exception
    //   379	383	508	finally
    //   388	421	508	finally
    //   421	429	469	java/lang/Exception
    //   421	429	508	finally
    //   429	433	469	java/lang/Exception
    //   429	433	508	finally
    //   433	462	469	java/lang/Exception
    //   433	462	508	finally
    //   462	466	469	java/lang/Exception
    //   462	466	508	finally
    //   471	504	508	finally
    //   504	507	508	finally
    //   509	512	508	finally
  }
  
  public int perfLockAcquire(int paramInt, int... paramVarArgs) {
    byte b = -1;
    int i = b;
    try {
      if (sAcquireFunc != null) {
        Object object = sAcquireFunc.invoke(this.mPerf, new Object[] { Integer.valueOf(paramInt), paramVarArgs });
        i = ((Integer)object).intValue();
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Exception ");
      stringBuilder.append(exception);
      Log.e("BoostFramework", stringBuilder.toString());
      i = b;
    } 
    return i;
  }
  
  public int perfLockRelease() {
    byte b = -1;
    int i = b;
    try {
      if (sReleaseFunc != null) {
        Object object = sReleaseFunc.invoke(this.mPerf, new Object[0]);
        i = ((Integer)object).intValue();
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Exception ");
      stringBuilder.append(exception);
      Log.e("BoostFramework", stringBuilder.toString());
      i = b;
    } 
    return i;
  }
  
  public int perfLockReleaseHandler(int paramInt) {
    byte b = -1;
    int i = b;
    try {
      if (sReleaseHandlerFunc != null) {
        Object object = sReleaseHandlerFunc.invoke(this.mPerf, new Object[] { Integer.valueOf(paramInt) });
        i = ((Integer)object).intValue();
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Exception ");
      stringBuilder.append(exception);
      Log.e("BoostFramework", stringBuilder.toString());
      i = b;
    } 
    return i;
  }
  
  public int perfHint(int paramInt, String paramString) {
    return perfHint(paramInt, paramString, -1, -1);
  }
  
  public int perfHint(int paramInt1, String paramString, int paramInt2) {
    return perfHint(paramInt1, paramString, paramInt2, -1);
  }
  
  public int perfHint(int paramInt1, String paramString, int paramInt2, int paramInt3) {
    byte b = -1;
    int i = b;
    try {
      if (sPerfHintFunc != null) {
        Object object = sPerfHintFunc.invoke(this.mPerf, new Object[] { Integer.valueOf(paramInt1), paramString, Integer.valueOf(paramInt2), Integer.valueOf(paramInt3) });
        i = ((Integer)object).intValue();
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Exception ");
      stringBuilder.append(exception);
      Log.e("BoostFramework", stringBuilder.toString());
      i = b;
    } 
    return i;
  }
  
  public int perfGetFeedback(int paramInt, String paramString) {
    byte b = -1;
    int i = b;
    try {
      if (sFeedbackFunc != null) {
        Object object = sFeedbackFunc.invoke(this.mPerf, new Object[] { Integer.valueOf(paramInt), paramString });
        i = ((Integer)object).intValue();
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Exception ");
      stringBuilder.append(exception);
      Log.e("BoostFramework", stringBuilder.toString());
      i = b;
    } 
    return i;
  }
  
  public int perfIOPrefetchStart(int paramInt, String paramString1, String paramString2) {
    byte b2, b1 = -1;
    try {
      Object object = sIOPStart.invoke(this.mPerf, new Object[] { Integer.valueOf(paramInt), paramString1, paramString2 });
      b2 = ((Integer)object).intValue();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Exception ");
      stringBuilder.append(exception);
      Log.e("BoostFramework", stringBuilder.toString());
      b2 = b1;
    } 
    try {
      Object object = sUxIOPStart.invoke(this.mUxPerf, new Object[] { Integer.valueOf(paramInt), paramString1, paramString2 });
      paramInt = ((Integer)object).intValue();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Ux Perf Exception ");
      stringBuilder.append(exception);
      Log.e("BoostFramework", stringBuilder.toString());
      paramInt = b2;
    } 
    return paramInt;
  }
  
  public int perfIOPrefetchStop() {
    byte b2, b1 = -1;
    try {
      Object object = sIOPStop.invoke(this.mPerf, new Object[0]);
      b2 = ((Integer)object).intValue();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Exception ");
      stringBuilder.append(exception);
      Log.e("BoostFramework", stringBuilder.toString());
      b2 = b1;
    } 
    return b2;
  }
  
  public int perfUXEngine_events(int paramInt1, int paramInt2, String paramString, int paramInt3) {
    return perfUXEngine_events(paramInt1, paramInt2, paramString, paramInt3, null);
  }
  
  public int perfUXEngine_events(int paramInt1, int paramInt2, String paramString1, int paramInt3, String paramString2) {
    byte b = -1;
    try {
      if (sUXEngineEvents == null)
        return -1; 
      Object object = sUXEngineEvents.invoke(this.mPerf, new Object[] { Integer.valueOf(paramInt1), Integer.valueOf(paramInt2), paramString1, Integer.valueOf(paramInt3), paramString2 });
      paramInt1 = ((Integer)object).intValue();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Exception ");
      stringBuilder.append(exception);
      Log.e("BoostFramework", stringBuilder.toString());
      paramInt1 = b;
    } 
    return paramInt1;
  }
  
  public String perfUXEngine_trigger(int paramInt) {
    Object object = null;
    try {
      if (sUXEngineTrigger == null)
        return null; 
      Object object1 = sUXEngineTrigger.invoke(this.mPerf, new Object[] { Integer.valueOf(paramInt) });
      object = object1 = object1;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Exception ");
      stringBuilder.append(exception);
      Log.e("BoostFramework", stringBuilder.toString());
    } 
    return (String)object;
  }
  
  public String perfGetProp(String paramString1, String paramString2) {
    Object object;
    String str = "";
    try {
      if (sPerfGetPropFunc != null) {
        object = sPerfGetPropFunc.invoke(this.mPerf, new Object[] { paramString1, paramString2 });
        object = object;
      } else {
        paramString1 = paramString2;
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Exception ");
      stringBuilder.append(exception);
      Log.e("BoostFramework", stringBuilder.toString());
      object = str;
    } 
    return (String)object;
  }
  
  public int perfLockAcqAndRelease(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int... paramVarArgs) {
    byte b = -1;
    int i = b;
    try {
      if (sAcqAndReleaseFunc != null) {
        Object object = sAcqAndReleaseFunc.invoke(this.mPerf, new Object[] { Integer.valueOf(paramInt1), Integer.valueOf(paramInt2), Integer.valueOf(paramInt3), Integer.valueOf(paramInt4), paramVarArgs });
        i = ((Integer)object).intValue();
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Exception ");
      stringBuilder.append(exception);
      Log.e("BoostFramework", stringBuilder.toString());
      i = b;
    } 
    return i;
  }
  
  public static class ScrollOptimizer {
    public static final int FLING_END = 0;
    
    public static final int FLING_START = 1;
    
    private static final String QXPERFORMANCE_JAR = "/system/framework/QXPerformance.jar";
    
    private static final String SCROLL_OPT_CLASS = "com.qualcomm.qti.QXPerformance.ScrollOptimizer";
    
    private static final String SCROLL_OPT_PROP = "ro.vendor.perf.scroll_opt";
    
    private static Method sGetAdjustedAnimationClock;
    
    private static Method sGetFrameDelay;
    
    private static boolean sQXIsLoaded = false;
    
    private static Class<?> sQXPerfClass = null;
    
    private static boolean sScrollOptEnable = false;
    
    private static Method sSetFlingFlag;
    
    private static Method sSetFrameInterval = null;
    
    private static Method sSetMotionType;
    
    private static Method sSetSurface = null;
    
    private static Method sSetUITaskStatus;
    
    private static Method sSetVsyncTime;
    
    private static Method sShouldUseVsync;
    
    static {
      sSetMotionType = null;
      sSetVsyncTime = null;
      sSetUITaskStatus = null;
      sSetFlingFlag = null;
      sShouldUseVsync = null;
      sGetFrameDelay = null;
      sGetAdjustedAnimationClock = null;
    }
    
    private static void initQXPerfFuncs() {
      if (sQXIsLoaded)
        return; 
      try {
        boolean bool = SystemProperties.getBoolean("ro.vendor.perf.scroll_opt", false);
        if (!bool) {
          sQXIsLoaded = true;
          return;
        } 
        PathClassLoader pathClassLoader = new PathClassLoader();
        this("/system/framework/QXPerformance.jar", ClassLoader.getSystemClassLoader());
        sQXPerfClass = pathClassLoader.loadClass("com.qualcomm.qti.QXPerformance.ScrollOptimizer");
        Class<long> clazz5 = long.class;
        sSetFrameInterval = sQXPerfClass.getMethod("setFrameInterval", new Class[] { clazz5 });
        sSetSurface = sQXPerfClass.getMethod("setSurface", new Class[] { Surface.class });
        Class<int> clazz4 = int.class;
        sSetMotionType = sQXPerfClass.getMethod("setMotionType", new Class[] { clazz4 });
        Class<long> clazz3 = long.class;
        sSetVsyncTime = sQXPerfClass.getMethod("setVsyncTime", new Class[] { clazz3 });
        Class<boolean> clazz = boolean.class;
        sSetUITaskStatus = sQXPerfClass.getMethod("setUITaskStatus", new Class[] { clazz });
        Class<int> clazz2 = int.class;
        sSetFlingFlag = sQXPerfClass.getMethod("setFlingFlag", new Class[] { clazz2 });
        sShouldUseVsync = sQXPerfClass.getMethod("shouldUseVsync", new Class[0]);
        Class<long> clazz1 = long.class;
        sGetFrameDelay = sQXPerfClass.getMethod("getFrameDelay", new Class[] { clazz1 });
        clazz1 = long.class;
        sGetAdjustedAnimationClock = sQXPerfClass.getMethod("getAdjustedAnimationClock", new Class[] { clazz1 });
        sQXIsLoaded = true;
      } catch (Exception exception) {
        Log.e("BoostFramework", "initQXPerfFuncs failed");
        exception.printStackTrace();
      } 
    }
    
    public static void setFrameInterval(long param1Long) {
      initQXPerfFuncs();
      if (sScrollOptEnable) {
        Method method = sSetFrameInterval;
        if (method != null)
          try {
            method.invoke(null, new Object[] { Long.valueOf(param1Long) });
          } catch (Exception exception) {
            exception.printStackTrace();
          }  
      } 
    }
    
    public static void setSurface(Surface param1Surface) {
      if (sScrollOptEnable) {
        Method method = sSetSurface;
        if (method != null)
          try {
            method.invoke(null, new Object[] { param1Surface });
          } catch (Exception exception) {
            exception.printStackTrace();
          }  
      } 
    }
    
    public static void setMotionType(int param1Int) {
      if (sScrollOptEnable) {
        Method method = sSetMotionType;
        if (method != null)
          try {
            method.invoke(null, new Object[] { Integer.valueOf(param1Int) });
          } catch (Exception exception) {
            exception.printStackTrace();
          }  
      } 
    }
    
    public static void setVsyncTime(long param1Long) {
      if (sScrollOptEnable) {
        Method method = sSetVsyncTime;
        if (method != null)
          try {
            method.invoke(null, new Object[] { Long.valueOf(param1Long) });
          } catch (Exception exception) {
            exception.printStackTrace();
          }  
      } 
    }
    
    public static void setUITaskStatus(boolean param1Boolean) {
      if (sScrollOptEnable) {
        Method method = sSetUITaskStatus;
        if (method != null)
          try {
            method.invoke(null, new Object[] { Boolean.valueOf(param1Boolean) });
          } catch (Exception exception) {
            exception.printStackTrace();
          }  
      } 
    }
    
    public static void setFlingFlag(int param1Int) {
      if (sScrollOptEnable) {
        Method method = sSetFlingFlag;
        if (method != null)
          try {
            method.invoke(null, new Object[] { Integer.valueOf(param1Int) });
          } catch (Exception exception) {
            exception.printStackTrace();
          }  
      } 
    }
    
    public static boolean shouldUseVsync(boolean param1Boolean) {
      boolean bool = param1Boolean;
      if (sScrollOptEnable) {
        Method method = sShouldUseVsync;
        bool = param1Boolean;
        if (method != null)
          try {
            Object object = method.invoke(null, new Object[0]);
            bool = ((Boolean)object).booleanValue();
          } catch (Exception exception) {
            exception.printStackTrace();
            bool = param1Boolean;
          }  
      } 
      return bool;
    }
    
    public static long getFrameDelay(long param1Long1, long param1Long2) {
      long l = param1Long1;
      if (sScrollOptEnable) {
        Method method = sGetFrameDelay;
        l = param1Long1;
        if (method != null)
          try {
            Object object = method.invoke(null, new Object[] { Long.valueOf(param1Long2) });
            l = ((Long)object).longValue();
          } catch (Exception exception) {
            exception.printStackTrace();
            l = param1Long1;
          }  
      } 
      return l;
    }
    
    public static long getAdjustedAnimationClock(long param1Long) {
      long l1 = param1Long;
      long l2 = l1;
      if (sScrollOptEnable) {
        Method method = sGetAdjustedAnimationClock;
        l2 = l1;
        if (method != null)
          try {
            Object object = method.invoke(null, new Object[] { Long.valueOf(param1Long) });
            l2 = ((Long)object).longValue();
          } catch (Exception exception) {
            exception.printStackTrace();
            l2 = l1;
          }  
      } 
      return l2;
    }
  }
}
