package android.util;

import android.icu.util.TimeZone;
import android.os.SystemClock;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import libcore.timezone.CountryTimeZones;
import libcore.timezone.TimeZoneFinder;
import libcore.timezone.ZoneInfoDb;

public class TimeUtils {
  public static final int HUNDRED_DAY_FIELD_LEN = 19;
  
  public static final long NANOS_PER_MS = 1000000L;
  
  private static final int SECONDS_PER_DAY = 86400;
  
  private static final int SECONDS_PER_HOUR = 3600;
  
  private static final int SECONDS_PER_MINUTE = 60;
  
  public static final SimpleDateFormat sDumpDateFormat;
  
  private static char[] sFormatStr;
  
  private static final Object sFormatSync;
  
  private static SimpleDateFormat sLoggingFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  
  private static char[] sTmpFormatStr;
  
  static {
    sDumpDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    sFormatSync = new Object();
    sFormatStr = new char[29];
    sTmpFormatStr = new char[29];
  }
  
  public static TimeZone getTimeZone(int paramInt, boolean paramBoolean, long paramLong, String paramString) {
    TimeZone timeZone = getIcuTimeZone(paramInt, paramBoolean, paramLong, paramString);
    if (timeZone != null) {
      TimeZone timeZone1 = TimeZone.getTimeZone(timeZone.getID());
    } else {
      timeZone = null;
    } 
    return (TimeZone)timeZone;
  }
  
  private static TimeZone getIcuTimeZone(int paramInt, boolean paramBoolean, long paramLong, String paramString) {
    TimeZone timeZone1;
    CountryTimeZones countryTimeZones2 = null;
    if (paramString == null)
      return null; 
    TimeZone timeZone2 = TimeZone.getDefault();
    CountryTimeZones countryTimeZones1 = TimeZoneFinder.getInstance().lookupCountryTimeZones(paramString);
    if (countryTimeZones1 == null)
      return null; 
    CountryTimeZones.OffsetResult offsetResult = countryTimeZones1.lookupByOffsetWithBias(paramLong, timeZone2, paramInt, paramBoolean);
    countryTimeZones1 = countryTimeZones2;
    if (offsetResult != null)
      timeZone1 = offsetResult.getTimeZone(); 
    return timeZone1;
  }
  
  public static List<String> getTimeZoneIdsForCountryCode(String paramString) {
    if (paramString != null) {
      TimeZoneFinder timeZoneFinder = TimeZoneFinder.getInstance();
      CountryTimeZones countryTimeZones = timeZoneFinder.lookupCountryTimeZones(paramString.toLowerCase());
      if (countryTimeZones == null)
        return null; 
      ArrayList<String> arrayList = new ArrayList();
      for (CountryTimeZones.TimeZoneMapping timeZoneMapping : countryTimeZones.getTimeZoneMappings()) {
        if (timeZoneMapping.isShownInPicker())
          arrayList.add(timeZoneMapping.getTimeZoneId()); 
      } 
      return Collections.unmodifiableList(arrayList);
    } 
    throw new NullPointerException("countryCode == null");
  }
  
  public static String getTimeZoneDatabaseVersion() {
    return ZoneInfoDb.getInstance().getVersion();
  }
  
  private static int accumField(int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3) {
    if (paramInt1 > 999) {
      paramInt3 = 0;
      while (paramInt1 != 0) {
        paramInt3++;
        paramInt1 /= 10;
      } 
      return paramInt3 + paramInt2;
    } 
    if (paramInt1 > 99 || (paramBoolean && paramInt3 >= 3))
      return paramInt2 + 3; 
    if (paramInt1 > 9 || (paramBoolean && paramInt3 >= 2))
      return paramInt2 + 2; 
    if (paramBoolean || paramInt1 > 0)
      return paramInt2 + 1; 
    return 0;
  }
  
  private static int printFieldLocked(char[] paramArrayOfchar, int paramInt1, char paramChar, int paramInt2, boolean paramBoolean, int paramInt3) {
    // Byte code:
    //   0: iload #4
    //   2: ifne -> 12
    //   5: iload_3
    //   6: istore #6
    //   8: iload_1
    //   9: ifle -> 226
    //   12: iload_1
    //   13: sipush #999
    //   16: if_icmple -> 92
    //   19: iconst_0
    //   20: istore #5
    //   22: iload_1
    //   23: ifeq -> 63
    //   26: getstatic android/util/TimeUtils.sTmpFormatStr : [C
    //   29: astore #7
    //   31: iload #5
    //   33: aload #7
    //   35: arraylength
    //   36: if_icmpge -> 63
    //   39: aload #7
    //   41: iload #5
    //   43: iload_1
    //   44: bipush #10
    //   46: irem
    //   47: bipush #48
    //   49: iadd
    //   50: i2c
    //   51: castore
    //   52: iinc #5, 1
    //   55: iload_1
    //   56: bipush #10
    //   58: idiv
    //   59: istore_1
    //   60: goto -> 22
    //   63: iload #5
    //   65: iconst_1
    //   66: isub
    //   67: istore_1
    //   68: iload_1
    //   69: iflt -> 89
    //   72: aload_0
    //   73: iload_3
    //   74: getstatic android/util/TimeUtils.sTmpFormatStr : [C
    //   77: iload_1
    //   78: caload
    //   79: castore
    //   80: iinc #3, 1
    //   83: iinc #1, -1
    //   86: goto -> 68
    //   89: goto -> 217
    //   92: iload #4
    //   94: ifeq -> 103
    //   97: iload #5
    //   99: iconst_3
    //   100: if_icmpge -> 115
    //   103: iload_1
    //   104: istore #6
    //   106: iload_3
    //   107: istore #8
    //   109: iload_1
    //   110: bipush #99
    //   112: if_icmple -> 144
    //   115: iload_1
    //   116: bipush #100
    //   118: idiv
    //   119: istore #6
    //   121: aload_0
    //   122: iload_3
    //   123: iload #6
    //   125: bipush #48
    //   127: iadd
    //   128: i2c
    //   129: castore
    //   130: iload_3
    //   131: iconst_1
    //   132: iadd
    //   133: istore #8
    //   135: iload_1
    //   136: iload #6
    //   138: bipush #100
    //   140: imul
    //   141: isub
    //   142: istore #6
    //   144: iload #4
    //   146: ifeq -> 155
    //   149: iload #5
    //   151: iconst_2
    //   152: if_icmpge -> 175
    //   155: iload #6
    //   157: bipush #9
    //   159: if_icmpgt -> 175
    //   162: iload #6
    //   164: istore #5
    //   166: iload #8
    //   168: istore_1
    //   169: iload_3
    //   170: iload #8
    //   172: if_icmpeq -> 204
    //   175: iload #6
    //   177: bipush #10
    //   179: idiv
    //   180: istore_3
    //   181: aload_0
    //   182: iload #8
    //   184: iload_3
    //   185: bipush #48
    //   187: iadd
    //   188: i2c
    //   189: castore
    //   190: iload #8
    //   192: iconst_1
    //   193: iadd
    //   194: istore_1
    //   195: iload #6
    //   197: iload_3
    //   198: bipush #10
    //   200: imul
    //   201: isub
    //   202: istore #5
    //   204: aload_0
    //   205: iload_1
    //   206: iload #5
    //   208: bipush #48
    //   210: iadd
    //   211: i2c
    //   212: castore
    //   213: iload_1
    //   214: iconst_1
    //   215: iadd
    //   216: istore_3
    //   217: aload_0
    //   218: iload_3
    //   219: iload_2
    //   220: castore
    //   221: iload_3
    //   222: iconst_1
    //   223: iadd
    //   224: istore #6
    //   226: iload #6
    //   228: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #178	-> 0
    //   #179	-> 12
    //   #180	-> 12
    //   #181	-> 19
    //   #182	-> 22
    //   #183	-> 39
    //   #184	-> 39
    //   #185	-> 52
    //   #186	-> 55
    //   #187	-> 60
    //   #188	-> 63
    //   #189	-> 68
    //   #190	-> 72
    //   #191	-> 80
    //   #192	-> 83
    //   #194	-> 89
    //   #195	-> 92
    //   #196	-> 115
    //   #197	-> 121
    //   #198	-> 130
    //   #199	-> 135
    //   #201	-> 144
    //   #202	-> 175
    //   #203	-> 181
    //   #204	-> 190
    //   #205	-> 195
    //   #207	-> 204
    //   #208	-> 213
    //   #210	-> 217
    //   #211	-> 221
    //   #213	-> 226
  }
  
  private static int formatDurationLocked(long paramLong, int paramInt) {
    byte b1;
    boolean bool1, bool2;
    int m;
    if (sFormatStr.length < paramInt)
      sFormatStr = new char[paramInt]; 
    char[] arrayOfChar = sFormatStr;
    if (paramLong == 0L) {
      byte b = 0;
      while (b < paramInt - 1) {
        arrayOfChar[b] = ' ';
        b++;
      } 
      arrayOfChar[b] = '0';
      return b + 1;
    } 
    if (paramLong > 0L) {
      b1 = 43;
    } else {
      paramLong = -paramLong;
      b1 = 45;
    } 
    int j = (int)(paramLong % 1000L);
    int i = (int)Math.floor((paramLong / 1000L));
    if (i >= 86400) {
      k = i / 86400;
      i -= 86400 * k;
    } else {
      k = 0;
    } 
    if (i >= 3600) {
      bool1 = i / 3600;
      i -= bool1 * 3600;
    } else {
      bool1 = false;
    } 
    if (i >= 60) {
      bool2 = i / 60;
      m = i - bool2 * 60;
    } else {
      bool2 = false;
      m = i;
    } 
    int n = 0;
    boolean bool3 = false;
    byte b2 = 3;
    boolean bool4 = false;
    if (paramInt != 0) {
      i = accumField(k, 1, false, 0);
      if (i > 0)
        bool4 = true; 
      i += accumField(bool1, 1, bool4, 2);
      if (i > 0) {
        bool4 = true;
      } else {
        bool4 = false;
      } 
      i += accumField(bool2, 1, bool4, 2);
      if (i > 0) {
        bool4 = true;
      } else {
        bool4 = false;
      } 
      int i1 = i + accumField(m, 1, bool4, 2);
      if (i1 > 0) {
        i = 3;
      } else {
        i = 0;
      } 
      i1 += accumField(j, 2, true, i) + 1;
      i = bool3;
      while (true) {
        n = i;
        if (i1 < paramInt) {
          arrayOfChar[i] = ' ';
          i++;
          i1++;
          continue;
        } 
        break;
      } 
    } 
    arrayOfChar[n] = b1;
    n++;
    if (paramInt != 0) {
      paramInt = 1;
    } else {
      paramInt = 0;
    } 
    boolean bool5 = true;
    byte b3 = 2;
    int k = printFieldLocked(arrayOfChar, k, 'd', n, false, 0);
    if (k != n) {
      bool4 = true;
    } else {
      bool4 = false;
    } 
    if (paramInt != 0) {
      i = 2;
    } else {
      i = 0;
    } 
    k = printFieldLocked(arrayOfChar, bool1, 'h', k, bool4, i);
    if (k != n) {
      bool4 = true;
    } else {
      bool4 = false;
    } 
    if (paramInt != 0) {
      i = 2;
    } else {
      i = 0;
    } 
    k = printFieldLocked(arrayOfChar, bool2, 'm', k, bool4, i);
    if (k != n) {
      bool4 = bool5;
    } else {
      bool4 = false;
    } 
    if (paramInt != 0) {
      i = b3;
    } else {
      i = 0;
    } 
    i = printFieldLocked(arrayOfChar, m, 's', k, bool4, i);
    if (paramInt != 0 && i != n) {
      paramInt = b2;
    } else {
      paramInt = 0;
    } 
    paramInt = printFieldLocked(arrayOfChar, j, 'm', i, true, paramInt);
    arrayOfChar[paramInt] = 's';
    return paramInt + 1;
  }
  
  public static void formatDuration(long paramLong, StringBuilder paramStringBuilder) {
    synchronized (sFormatSync) {
      int i = formatDurationLocked(paramLong, 0);
      paramStringBuilder.append(sFormatStr, 0, i);
      return;
    } 
  }
  
  public static void formatDuration(long paramLong, StringBuilder paramStringBuilder, int paramInt) {
    synchronized (sFormatSync) {
      paramInt = formatDurationLocked(paramLong, paramInt);
      paramStringBuilder.append(sFormatStr, 0, paramInt);
      return;
    } 
  }
  
  public static void formatDuration(long paramLong, PrintWriter paramPrintWriter, int paramInt) {
    synchronized (sFormatSync) {
      paramInt = formatDurationLocked(paramLong, paramInt);
      String str = new String();
      this(sFormatStr, 0, paramInt);
      paramPrintWriter.print(str);
      return;
    } 
  }
  
  public static String formatDuration(long paramLong) {
    synchronized (sFormatSync) {
      int i = formatDurationLocked(paramLong, 0);
      String str = new String();
      this(sFormatStr, 0, i);
      return str;
    } 
  }
  
  public static void formatDuration(long paramLong, PrintWriter paramPrintWriter) {
    formatDuration(paramLong, paramPrintWriter, 0);
  }
  
  public static void formatDuration(long paramLong1, long paramLong2, PrintWriter paramPrintWriter) {
    if (paramLong1 == 0L) {
      paramPrintWriter.print("--");
      return;
    } 
    formatDuration(paramLong1 - paramLong2, paramPrintWriter, 0);
  }
  
  public static String formatUptime(long paramLong) {
    return formatTime(paramLong, SystemClock.uptimeMillis());
  }
  
  public static String formatRealtime(long paramLong) {
    return formatTime(paramLong, SystemClock.elapsedRealtime());
  }
  
  public static String formatTime(long paramLong1, long paramLong2) {
    paramLong2 = paramLong1 - paramLong2;
    if (paramLong2 > 0L) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramLong1);
      stringBuilder1.append(" (in ");
      stringBuilder1.append(paramLong2);
      stringBuilder1.append(" ms)");
      return stringBuilder1.toString();
    } 
    if (paramLong2 < 0L) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramLong1);
      stringBuilder1.append(" (");
      stringBuilder1.append(-paramLong2);
      stringBuilder1.append(" ms ago)");
      return stringBuilder1.toString();
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramLong1);
    stringBuilder.append(" (now)");
    return stringBuilder.toString();
  }
  
  public static String logTimeOfDay(long paramLong) {
    Calendar calendar = Calendar.getInstance();
    if (paramLong >= 0L) {
      calendar.setTimeInMillis(paramLong);
      return String.format("%tm-%td %tH:%tM:%tS.%tL", new Object[] { calendar, calendar, calendar, calendar, calendar, calendar });
    } 
    return Long.toString(paramLong);
  }
  
  public static String formatForLogging(long paramLong) {
    if (paramLong <= 0L)
      return "unknown"; 
    return sLoggingFormat.format(new Date(paramLong));
  }
  
  public static void dumpTime(PrintWriter paramPrintWriter, long paramLong) {
    paramPrintWriter.print(sDumpDateFormat.format(new Date(paramLong)));
  }
  
  public static boolean isTimeBetween(LocalTime paramLocalTime1, LocalTime paramLocalTime2, LocalTime paramLocalTime3) {
    if (!paramLocalTime1.isBefore(paramLocalTime2) || !paramLocalTime1.isAfter(paramLocalTime3))
      if (!paramLocalTime1.isBefore(paramLocalTime3) || !paramLocalTime1.isBefore(paramLocalTime2) || !paramLocalTime2.isBefore(paramLocalTime3))
        if (!paramLocalTime1.isAfter(paramLocalTime3) || !paramLocalTime1.isAfter(paramLocalTime2) || !paramLocalTime2.isBefore(paramLocalTime3))
          return true;   
    return false;
  }
  
  public static void dumpTimeWithDelta(PrintWriter paramPrintWriter, long paramLong1, long paramLong2) {
    paramPrintWriter.print(sDumpDateFormat.format(new Date(paramLong1)));
    if (paramLong1 == paramLong2) {
      paramPrintWriter.print(" (now)");
    } else {
      paramPrintWriter.print(" (");
      formatDuration(paramLong1, paramLong2, paramPrintWriter);
      paramPrintWriter.print(")");
    } 
  }
}
