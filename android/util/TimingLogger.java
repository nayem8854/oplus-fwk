package android.util;

import android.os.SystemClock;
import java.util.ArrayList;

@Deprecated
public class TimingLogger {
  private boolean mDisabled;
  
  private String mLabel;
  
  ArrayList<String> mSplitLabels;
  
  ArrayList<Long> mSplits;
  
  private String mTag;
  
  public TimingLogger(String paramString1, String paramString2) {
    reset(paramString1, paramString2);
  }
  
  public void reset(String paramString1, String paramString2) {
    this.mTag = paramString1;
    this.mLabel = paramString2;
    reset();
  }
  
  public void reset() {
    int i = Log.isLoggable(this.mTag, 2) ^ true;
    if (i != 0)
      return; 
    ArrayList<Long> arrayList = this.mSplits;
    if (arrayList == null) {
      this.mSplits = new ArrayList<>();
      this.mSplitLabels = new ArrayList<>();
    } else {
      arrayList.clear();
      this.mSplitLabels.clear();
    } 
    addSplit(null);
  }
  
  public void addSplit(String paramString) {
    if (this.mDisabled)
      return; 
    long l = SystemClock.elapsedRealtime();
    this.mSplits.add(Long.valueOf(l));
    this.mSplitLabels.add(paramString);
  }
  
  public void dumpToLog() {
    if (this.mDisabled)
      return; 
    String str1 = this.mTag;
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(this.mLabel);
    stringBuilder2.append(": begin");
    Log.d(str1, stringBuilder2.toString());
    long l1 = ((Long)this.mSplits.get(0)).longValue();
    long l2 = l1;
    for (byte b = 1; b < this.mSplits.size(); b++) {
      l2 = ((Long)this.mSplits.get(b)).longValue();
      String str = this.mSplitLabels.get(b);
      long l = ((Long)this.mSplits.get(b - 1)).longValue();
      str1 = this.mTag;
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append(this.mLabel);
      stringBuilder2.append(":      ");
      stringBuilder2.append(l2 - l);
      stringBuilder2.append(" ms, ");
      stringBuilder2.append(str);
      Log.d(str1, stringBuilder2.toString());
    } 
    String str2 = this.mTag;
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(this.mLabel);
    stringBuilder1.append(": end, ");
    stringBuilder1.append(l2 - l1);
    stringBuilder1.append(" ms");
    Log.d(str2, stringBuilder1.toString());
  }
}
