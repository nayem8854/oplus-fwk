package android.util;

import android.annotation.SystemApi;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

public class EventLog {
  private static final String COMMENT_PATTERN = "^\\s*(#.*)?$";
  
  private static final String TAG = "EventLog";
  
  private static final String TAGS_FILE = "/system/etc/event-log-tags";
  
  private static final String TAG_PATTERN = "^\\s*(\\d+)\\s+(\\w+)\\s*(\\(.*\\))?\\s*$";
  
  private static HashMap<String, Integer> sTagCodes = null;
  
  private static HashMap<Integer, String> sTagNames = null;
  
  public static final class Event {
    private static final byte FLOAT_TYPE = 4;
    
    private static final int HEADER_SIZE_OFFSET = 2;
    
    private static final byte INT_TYPE = 0;
    
    private static final int LENGTH_OFFSET = 0;
    
    private static final byte LIST_TYPE = 3;
    
    private static final byte LONG_TYPE = 1;
    
    private static final int NANOSECONDS_OFFSET = 16;
    
    private static final int PROCESS_OFFSET = 4;
    
    private static final int SECONDS_OFFSET = 12;
    
    private static final byte STRING_TYPE = 2;
    
    private static final int TAG_LENGTH = 4;
    
    private static final int THREAD_OFFSET = 8;
    
    private static final int UID_OFFSET = 24;
    
    private static final int V1_PAYLOAD_START = 20;
    
    private final ByteBuffer mBuffer;
    
    private Exception mLastWtf;
    
    Event(byte[] param1ArrayOfbyte) {
      ByteBuffer byteBuffer = ByteBuffer.wrap(param1ArrayOfbyte);
      byteBuffer.order(ByteOrder.nativeOrder());
    }
    
    public int getProcessId() {
      return this.mBuffer.getInt(4);
    }
    
    @SystemApi
    public int getUid() {
      try {
        return this.mBuffer.getInt(24);
      } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
        return -1;
      } 
    }
    
    public int getThreadId() {
      return this.mBuffer.getInt(8);
    }
    
    public long getTimeNanos() {
      long l1 = this.mBuffer.getInt(12);
      ByteBuffer byteBuffer = this.mBuffer;
      long l2 = byteBuffer.getInt(16);
      return l1 * 1000000000L + l2;
    }
    
    public int getTag() {
      return this.mBuffer.getInt(getHeaderSize());
    }
    
    private int getHeaderSize() {
      short s = this.mBuffer.getShort(2);
      if (s != 0)
        return s; 
      return 20;
    }
    
    public Object getData() {
      Object object;
      /* monitor enter ThisExpression{InnerObjectType{ObjectType{android/util/EventLog}.Landroid/util/EventLog$Event;}} */
      try {
        int i = getHeaderSize();
        this.mBuffer.limit(this.mBuffer.getShort(0) + i);
        int j = this.mBuffer.limit();
        if (i + 4 >= j) {
          /* monitor exit ThisExpression{InnerObjectType{ObjectType{android/util/EventLog}.Landroid/util/EventLog$Event;}} */
          return null;
        } 
        this.mBuffer.position(i + 4);
        object = decodeObject();
        /* monitor exit ThisExpression{InnerObjectType{ObjectType{android/util/EventLog}.Landroid/util/EventLog$Event;}} */
        return object;
      } catch (IllegalArgumentException null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Illegal entry payload: tag=");
        stringBuilder.append(getTag());
        Log.wtf("EventLog", stringBuilder.toString(), (Throwable)object);
        this.mLastWtf = (Exception)object;
        /* monitor exit ThisExpression{InnerObjectType{ObjectType{android/util/EventLog}.Landroid/util/EventLog$Event;}} */
        return null;
      } catch (BufferUnderflowException bufferUnderflowException) {
        object = new StringBuilder();
        this();
        object.append("Truncated entry payload: tag=");
        object.append(getTag());
        Log.wtf("EventLog", object.toString(), bufferUnderflowException);
        this.mLastWtf = bufferUnderflowException;
        /* monitor exit ThisExpression{InnerObjectType{ObjectType{android/util/EventLog}.Landroid/util/EventLog$Event;}} */
        return null;
      } finally {}
      /* monitor exit ThisExpression{InnerObjectType{ObjectType{android/util/EventLog}.Landroid/util/EventLog$Event;}} */
      throw object;
    }
    
    public Event withNewData(Object param1Object) {
      param1Object = encodeObject(param1Object);
      if (param1Object.length <= 65531) {
        int i = getHeaderSize();
        byte[] arrayOfByte = new byte[i + 4 + param1Object.length];
        System.arraycopy(this.mBuffer.array(), 0, arrayOfByte, 0, i + 4);
        System.arraycopy(param1Object, 0, arrayOfByte, i + 4, param1Object.length);
        Event event = new Event(arrayOfByte);
        event.mBuffer.putShort(0, (short)(param1Object.length + 4));
        return event;
      } 
      throw new IllegalArgumentException("Payload too long");
    }
    
    private Object decodeObject() {
      byte b = this.mBuffer.get();
      if (b != 0) {
        if (b != 1) {
          if (b != 2) {
            int i;
            if (b != 3) {
              if (b == 4)
                return Float.valueOf(this.mBuffer.getFloat()); 
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Unknown entry type: ");
              stringBuilder.append(b);
              throw new IllegalArgumentException(stringBuilder.toString());
            } 
            byte b1 = this.mBuffer.get();
            b = b1;
            if (b1 < 0)
              i = b1 + 256; 
            Object[] arrayOfObject = new Object[i];
            for (b1 = 0; b1 < i; ) {
              arrayOfObject[b1] = decodeObject();
              b1++;
            } 
            return arrayOfObject;
          } 
          try {
            int j = this.mBuffer.getInt();
            int i = this.mBuffer.position();
            this.mBuffer.position(i + j);
            return new String(this.mBuffer.array(), i, j, "UTF-8");
          } catch (UnsupportedEncodingException unsupportedEncodingException) {
            Log.wtf("EventLog", "UTF-8 is not supported", unsupportedEncodingException);
            this.mLastWtf = unsupportedEncodingException;
            return null;
          } 
        } 
        return Long.valueOf(this.mBuffer.getLong());
      } 
      return Integer.valueOf(this.mBuffer.getInt());
    }
    
    private static byte[] encodeObject(Object param1Object) {
      if (param1Object == null)
        return new byte[0]; 
      if (param1Object instanceof Integer) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(5);
        byteBuffer = byteBuffer.order(ByteOrder.nativeOrder());
        byteBuffer = byteBuffer.put((byte)0);
        param1Object = param1Object;
        param1Object = byteBuffer.putInt(param1Object.intValue());
        return param1Object.array();
      } 
      if (param1Object instanceof Long) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(9);
        byteBuffer = byteBuffer.order(ByteOrder.nativeOrder());
        byteBuffer = byteBuffer.put((byte)1);
        param1Object = param1Object;
        param1Object = byteBuffer.putLong(param1Object.longValue());
        return param1Object.array();
      } 
      if (param1Object instanceof Float) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(5);
        byteBuffer = byteBuffer.order(ByteOrder.nativeOrder());
        byteBuffer = byteBuffer.put((byte)4);
        param1Object = param1Object;
        param1Object = byteBuffer.putFloat(param1Object.floatValue());
        return param1Object.array();
      } 
      if (param1Object instanceof String) {
        byte[] arrayOfByte;
        param1Object = param1Object;
        try {
          param1Object = param1Object.getBytes("UTF-8");
        } catch (UnsupportedEncodingException unsupportedEncodingException) {
          arrayOfByte = new byte[0];
        } 
        ByteBuffer byteBuffer2 = ByteBuffer.allocate(arrayOfByte.length + 5);
        byteBuffer2 = byteBuffer2.order(ByteOrder.nativeOrder());
        byteBuffer2 = byteBuffer2.put((byte)2);
        int i = arrayOfByte.length;
        byteBuffer2 = byteBuffer2.putInt(i);
        ByteBuffer byteBuffer1 = byteBuffer2.put(arrayOfByte);
        return byteBuffer1.array();
      } 
      if (param1Object instanceof Object[]) {
        param1Object = param1Object;
        if (param1Object.length <= 255) {
          byte[][] arrayOfByte = new byte[param1Object.length][];
          int i = 0;
          byte b;
          for (b = 0; b < param1Object.length; b++) {
            arrayOfByte[b] = encodeObject(param1Object[b]);
            i += (arrayOfByte[b]).length;
          } 
          ByteBuffer byteBuffer = ByteBuffer.allocate(i + 2);
          byteBuffer = byteBuffer.order(ByteOrder.nativeOrder());
          byteBuffer = byteBuffer.put((byte)3);
          byte b1 = (byte)param1Object.length;
          byteBuffer = byteBuffer.put(b1);
          for (b = 0; b < param1Object.length; b++)
            byteBuffer.put(arrayOfByte[b]); 
          return byteBuffer.array();
        } 
        throw new IllegalArgumentException("Object array too long");
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown object type ");
      stringBuilder.append(param1Object);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public static Event fromBytes(byte[] param1ArrayOfbyte) {
      return new Event(param1ArrayOfbyte);
    }
    
    public byte[] getBytes() {
      byte[] arrayOfByte = this.mBuffer.array();
      return Arrays.copyOf(arrayOfByte, arrayOfByte.length);
    }
    
    public Exception getLastError() {
      return this.mLastWtf;
    }
    
    public void clearError() {
      this.mLastWtf = null;
    }
    
    public boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      return Arrays.equals(this.mBuffer.array(), ((Event)param1Object).mBuffer.array());
    }
    
    public int hashCode() {
      return Arrays.hashCode(this.mBuffer.array());
    }
  }
  
  public static String getTagName(int paramInt) {
    readTagsFile();
    return sTagNames.get(Integer.valueOf(paramInt));
  }
  
  public static int getTagCode(String paramString) {
    byte b;
    readTagsFile();
    Integer integer = sTagCodes.get(paramString);
    if (integer != null) {
      b = integer.intValue();
    } else {
      b = -1;
    } 
    return b;
  }
  
  private static void readTagsFile() {
    // Byte code:
    //   0: ldc android/util/EventLog
    //   2: monitorenter
    //   3: getstatic android/util/EventLog.sTagCodes : Ljava/util/HashMap;
    //   6: ifnull -> 21
    //   9: getstatic android/util/EventLog.sTagNames : Ljava/util/HashMap;
    //   12: astore_0
    //   13: aload_0
    //   14: ifnull -> 21
    //   17: ldc android/util/EventLog
    //   19: monitorexit
    //   20: return
    //   21: new java/util/HashMap
    //   24: astore_0
    //   25: aload_0
    //   26: invokespecial <init> : ()V
    //   29: aload_0
    //   30: putstatic android/util/EventLog.sTagCodes : Ljava/util/HashMap;
    //   33: new java/util/HashMap
    //   36: astore_0
    //   37: aload_0
    //   38: invokespecial <init> : ()V
    //   41: aload_0
    //   42: putstatic android/util/EventLog.sTagNames : Ljava/util/HashMap;
    //   45: ldc '^\s*(#.*)?$'
    //   47: invokestatic compile : (Ljava/lang/String;)Ljava/util/regex/Pattern;
    //   50: astore_1
    //   51: ldc '^\s*(\d+)\s+(\w+)\s*(\(.*\))?\s*$'
    //   53: invokestatic compile : (Ljava/lang/String;)Ljava/util/regex/Pattern;
    //   56: astore_2
    //   57: aconst_null
    //   58: astore_3
    //   59: aconst_null
    //   60: astore #4
    //   62: aload #4
    //   64: astore_0
    //   65: aload_3
    //   66: astore #5
    //   68: new java/io/BufferedReader
    //   71: astore #6
    //   73: aload #4
    //   75: astore_0
    //   76: aload_3
    //   77: astore #5
    //   79: new java/io/FileReader
    //   82: astore #7
    //   84: aload #4
    //   86: astore_0
    //   87: aload_3
    //   88: astore #5
    //   90: aload #7
    //   92: ldc '/system/etc/event-log-tags'
    //   94: invokespecial <init> : (Ljava/lang/String;)V
    //   97: aload #4
    //   99: astore_0
    //   100: aload_3
    //   101: astore #5
    //   103: aload #6
    //   105: aload #7
    //   107: sipush #256
    //   110: invokespecial <init> : (Ljava/io/Reader;I)V
    //   113: aload #6
    //   115: astore_0
    //   116: aload #6
    //   118: astore #5
    //   120: aload #6
    //   122: invokevirtual readLine : ()Ljava/lang/String;
    //   125: astore #4
    //   127: aload #4
    //   129: ifnull -> 399
    //   132: aload #6
    //   134: astore_0
    //   135: aload #6
    //   137: astore #5
    //   139: aload_1
    //   140: aload #4
    //   142: invokevirtual matcher : (Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   145: invokevirtual matches : ()Z
    //   148: ifeq -> 154
    //   151: goto -> 113
    //   154: aload #6
    //   156: astore_0
    //   157: aload #6
    //   159: astore #5
    //   161: aload_2
    //   162: aload #4
    //   164: invokevirtual matcher : (Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   167: astore_3
    //   168: aload #6
    //   170: astore_0
    //   171: aload #6
    //   173: astore #5
    //   175: aload_3
    //   176: invokevirtual matches : ()Z
    //   179: ifne -> 252
    //   182: aload #6
    //   184: astore_0
    //   185: aload #6
    //   187: astore #5
    //   189: new java/lang/StringBuilder
    //   192: astore_3
    //   193: aload #6
    //   195: astore_0
    //   196: aload #6
    //   198: astore #5
    //   200: aload_3
    //   201: invokespecial <init> : ()V
    //   204: aload #6
    //   206: astore_0
    //   207: aload #6
    //   209: astore #5
    //   211: aload_3
    //   212: ldc 'Bad entry in /system/etc/event-log-tags: '
    //   214: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   217: pop
    //   218: aload #6
    //   220: astore_0
    //   221: aload #6
    //   223: astore #5
    //   225: aload_3
    //   226: aload #4
    //   228: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   231: pop
    //   232: aload #6
    //   234: astore_0
    //   235: aload #6
    //   237: astore #5
    //   239: ldc 'EventLog'
    //   241: aload_3
    //   242: invokevirtual toString : ()Ljava/lang/String;
    //   245: invokestatic wtf : (Ljava/lang/String;Ljava/lang/String;)I
    //   248: pop
    //   249: goto -> 113
    //   252: aload #6
    //   254: astore_0
    //   255: aload #6
    //   257: astore #5
    //   259: aload_3
    //   260: iconst_1
    //   261: invokevirtual group : (I)Ljava/lang/String;
    //   264: invokestatic parseInt : (Ljava/lang/String;)I
    //   267: istore #8
    //   269: aload #6
    //   271: astore_0
    //   272: aload #6
    //   274: astore #5
    //   276: aload_3
    //   277: iconst_2
    //   278: invokevirtual group : (I)Ljava/lang/String;
    //   281: astore_3
    //   282: aload #6
    //   284: astore_0
    //   285: aload #6
    //   287: astore #5
    //   289: getstatic android/util/EventLog.sTagCodes : Ljava/util/HashMap;
    //   292: aload_3
    //   293: iload #8
    //   295: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   298: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   301: pop
    //   302: aload #6
    //   304: astore_0
    //   305: aload #6
    //   307: astore #5
    //   309: getstatic android/util/EventLog.sTagNames : Ljava/util/HashMap;
    //   312: iload #8
    //   314: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   317: aload_3
    //   318: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   321: pop
    //   322: goto -> 396
    //   325: astore #7
    //   327: aload #6
    //   329: astore_0
    //   330: aload #6
    //   332: astore #5
    //   334: new java/lang/StringBuilder
    //   337: astore_3
    //   338: aload #6
    //   340: astore_0
    //   341: aload #6
    //   343: astore #5
    //   345: aload_3
    //   346: invokespecial <init> : ()V
    //   349: aload #6
    //   351: astore_0
    //   352: aload #6
    //   354: astore #5
    //   356: aload_3
    //   357: ldc 'Error in /system/etc/event-log-tags: '
    //   359: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   362: pop
    //   363: aload #6
    //   365: astore_0
    //   366: aload #6
    //   368: astore #5
    //   370: aload_3
    //   371: aload #4
    //   373: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   376: pop
    //   377: aload #6
    //   379: astore_0
    //   380: aload #6
    //   382: astore #5
    //   384: ldc 'EventLog'
    //   386: aload_3
    //   387: invokevirtual toString : ()Ljava/lang/String;
    //   390: aload #7
    //   392: invokestatic wtf : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   395: pop
    //   396: goto -> 113
    //   399: aload #6
    //   401: invokevirtual close : ()V
    //   404: goto -> 445
    //   407: astore_0
    //   408: goto -> 445
    //   411: astore #5
    //   413: goto -> 449
    //   416: astore #6
    //   418: aload #5
    //   420: astore_0
    //   421: ldc 'EventLog'
    //   423: ldc 'Error reading /system/etc/event-log-tags'
    //   425: aload #6
    //   427: invokestatic wtf : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   430: pop
    //   431: aload #5
    //   433: ifnull -> 445
    //   436: aload #5
    //   438: invokevirtual close : ()V
    //   441: goto -> 445
    //   444: astore_0
    //   445: ldc android/util/EventLog
    //   447: monitorexit
    //   448: return
    //   449: aload_0
    //   450: ifnull -> 461
    //   453: aload_0
    //   454: invokevirtual close : ()V
    //   457: goto -> 461
    //   460: astore_0
    //   461: aload #5
    //   463: athrow
    //   464: astore_0
    //   465: ldc android/util/EventLog
    //   467: monitorexit
    //   468: aload_0
    //   469: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #419	-> 3
    //   #421	-> 21
    //   #422	-> 33
    //   #424	-> 45
    //   #425	-> 51
    //   #426	-> 57
    //   #430	-> 62
    //   #431	-> 113
    //   #432	-> 132
    //   #434	-> 154
    //   #435	-> 168
    //   #436	-> 182
    //   #437	-> 249
    //   #441	-> 252
    //   #442	-> 269
    //   #443	-> 282
    //   #444	-> 302
    //   #447	-> 322
    //   #445	-> 325
    //   #446	-> 327
    //   #448	-> 396
    //   #453	-> 399
    //   #454	-> 408
    //   #453	-> 411
    //   #449	-> 416
    //   #450	-> 418
    //   #453	-> 431
    //   #455	-> 445
    //   #453	-> 449
    //   #454	-> 461
    //   #418	-> 464
    // Exception table:
    //   from	to	target	type
    //   3	13	464	finally
    //   21	33	464	finally
    //   33	45	464	finally
    //   45	51	464	finally
    //   51	57	464	finally
    //   68	73	416	java/io/IOException
    //   68	73	411	finally
    //   79	84	416	java/io/IOException
    //   79	84	411	finally
    //   90	97	416	java/io/IOException
    //   90	97	411	finally
    //   103	113	416	java/io/IOException
    //   103	113	411	finally
    //   120	127	416	java/io/IOException
    //   120	127	411	finally
    //   139	151	416	java/io/IOException
    //   139	151	411	finally
    //   161	168	416	java/io/IOException
    //   161	168	411	finally
    //   175	182	416	java/io/IOException
    //   175	182	411	finally
    //   189	193	416	java/io/IOException
    //   189	193	411	finally
    //   200	204	416	java/io/IOException
    //   200	204	411	finally
    //   211	218	416	java/io/IOException
    //   211	218	411	finally
    //   225	232	416	java/io/IOException
    //   225	232	411	finally
    //   239	249	416	java/io/IOException
    //   239	249	411	finally
    //   259	269	325	java/lang/NumberFormatException
    //   259	269	416	java/io/IOException
    //   259	269	411	finally
    //   276	282	325	java/lang/NumberFormatException
    //   276	282	416	java/io/IOException
    //   276	282	411	finally
    //   289	302	325	java/lang/NumberFormatException
    //   289	302	416	java/io/IOException
    //   289	302	411	finally
    //   309	322	325	java/lang/NumberFormatException
    //   309	322	416	java/io/IOException
    //   309	322	411	finally
    //   334	338	416	java/io/IOException
    //   334	338	411	finally
    //   345	349	416	java/io/IOException
    //   345	349	411	finally
    //   356	363	416	java/io/IOException
    //   356	363	411	finally
    //   370	377	416	java/io/IOException
    //   370	377	411	finally
    //   384	396	416	java/io/IOException
    //   384	396	411	finally
    //   399	404	407	java/io/IOException
    //   399	404	464	finally
    //   421	431	411	finally
    //   436	441	444	java/io/IOException
    //   436	441	464	finally
    //   453	457	460	java/io/IOException
    //   453	457	464	finally
    //   461	464	464	finally
  }
  
  public static native void readEvents(int[] paramArrayOfint, Collection<Event> paramCollection) throws IOException;
  
  @SystemApi
  public static native void readEventsOnWrapping(int[] paramArrayOfint, long paramLong, Collection<Event> paramCollection) throws IOException;
  
  public static native int writeEvent(int paramInt, float paramFloat);
  
  public static native int writeEvent(int paramInt1, int paramInt2);
  
  public static native int writeEvent(int paramInt, long paramLong);
  
  public static native int writeEvent(int paramInt, String paramString);
  
  public static native int writeEvent(int paramInt, Object... paramVarArgs);
}
