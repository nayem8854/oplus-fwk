package android.util;

import com.android.internal.util.Preconditions;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;

public final class Rational extends Number implements Comparable<Rational> {
  public static final Rational NEGATIVE_INFINITY;
  
  public static final Rational NaN = new Rational(0, 0);
  
  public static final Rational POSITIVE_INFINITY = new Rational(1, 0);
  
  public static final Rational ZERO;
  
  private static final long serialVersionUID = 1L;
  
  private final int mDenominator;
  
  private final int mNumerator;
  
  static {
    NEGATIVE_INFINITY = new Rational(-1, 0);
    ZERO = new Rational(0, 1);
  }
  
  public Rational(int paramInt1, int paramInt2) {
    int i = paramInt1, j = paramInt2;
    if (paramInt2 < 0) {
      i = -paramInt1;
      j = -paramInt2;
    } 
    if (j == 0 && i > 0) {
      this.mNumerator = 1;
      this.mDenominator = 0;
    } else if (j == 0 && i < 0) {
      this.mNumerator = -1;
      this.mDenominator = 0;
    } else if (j == 0 && i == 0) {
      this.mNumerator = 0;
      this.mDenominator = 0;
    } else if (i == 0) {
      this.mNumerator = 0;
      this.mDenominator = 1;
    } else {
      paramInt1 = gcd(i, j);
      this.mNumerator = i / paramInt1;
      this.mDenominator = j / paramInt1;
    } 
  }
  
  public int getNumerator() {
    return this.mNumerator;
  }
  
  public int getDenominator() {
    return this.mDenominator;
  }
  
  public boolean isNaN() {
    boolean bool;
    if (this.mDenominator == 0 && this.mNumerator == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isInfinite() {
    boolean bool;
    if (this.mNumerator != 0 && this.mDenominator == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isFinite() {
    boolean bool;
    if (this.mDenominator != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isZero() {
    boolean bool;
    if (isFinite() && this.mNumerator == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isPosInf() {
    boolean bool;
    if (this.mDenominator == 0 && this.mNumerator > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isNegInf() {
    boolean bool;
    if (this.mDenominator == 0 && this.mNumerator < 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool;
    if (paramObject instanceof Rational && equals((Rational)paramObject)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean equals(Rational paramRational) {
    boolean bool;
    if (this.mNumerator == paramRational.mNumerator && this.mDenominator == paramRational.mDenominator) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String toString() {
    if (isNaN())
      return "NaN"; 
    if (isPosInf())
      return "Infinity"; 
    if (isNegInf())
      return "-Infinity"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.mNumerator);
    stringBuilder.append("/");
    stringBuilder.append(this.mDenominator);
    return stringBuilder.toString();
  }
  
  public float toFloat() {
    return floatValue();
  }
  
  public int hashCode() {
    int i = this.mNumerator;
    return this.mDenominator ^ (i >>> 16 | i << 16);
  }
  
  public static int gcd(int paramInt1, int paramInt2) {
    int i = paramInt1;
    paramInt1 = paramInt2;
    while (paramInt1 != 0) {
      paramInt2 = i % paramInt1;
      i = paramInt1;
      paramInt1 = paramInt2;
    } 
    return Math.abs(i);
  }
  
  public double doubleValue() {
    double d1 = this.mNumerator;
    double d2 = this.mDenominator;
    return d1 / d2;
  }
  
  public float floatValue() {
    float f1 = this.mNumerator;
    float f2 = this.mDenominator;
    return f1 / f2;
  }
  
  public int intValue() {
    if (isPosInf())
      return Integer.MAX_VALUE; 
    if (isNegInf())
      return Integer.MIN_VALUE; 
    if (isNaN())
      return 0; 
    return this.mNumerator / this.mDenominator;
  }
  
  public long longValue() {
    if (isPosInf())
      return Long.MAX_VALUE; 
    if (isNegInf())
      return Long.MIN_VALUE; 
    if (isNaN())
      return 0L; 
    return (this.mNumerator / this.mDenominator);
  }
  
  public short shortValue() {
    return (short)intValue();
  }
  
  public int compareTo(Rational paramRational) {
    Preconditions.checkNotNull(paramRational, "another must not be null");
    if (equals(paramRational))
      return 0; 
    if (isNaN())
      return 1; 
    if (paramRational.isNaN())
      return -1; 
    if (isPosInf() || paramRational.isNegInf())
      return 1; 
    if (isNegInf() || paramRational.isPosInf())
      return -1; 
    long l1 = this.mNumerator * paramRational.mDenominator;
    long l2 = paramRational.mNumerator * this.mDenominator;
    if (l1 < l2)
      return -1; 
    if (l1 > l2)
      return 1; 
    return 0;
  }
  
  private void readObject(ObjectInputStream paramObjectInputStream) throws IOException, ClassNotFoundException {
    paramObjectInputStream.defaultReadObject();
    int i = this.mNumerator;
    if (i == 0) {
      int k = this.mDenominator;
      if (k == 1 || k == 0)
        return; 
      throw new InvalidObjectException("Rational must be deserialized from a reduced form for zero values");
    } 
    int j = this.mDenominator;
    if (j == 0) {
      if (i == 1 || i == -1)
        return; 
      throw new InvalidObjectException("Rational must be deserialized from a reduced form for infinity values");
    } 
    if (gcd(i, j) <= 1)
      return; 
    throw new InvalidObjectException("Rational must be deserialized from a reduced form for finite values");
  }
  
  private static NumberFormatException invalidRational(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid Rational: \"");
    stringBuilder.append(paramString);
    stringBuilder.append("\"");
    throw new NumberFormatException(stringBuilder.toString());
  }
  
  public static Rational parseRational(String paramString) throws NumberFormatException {
    Preconditions.checkNotNull(paramString, "string must not be null");
    if (paramString.equals("NaN"))
      return NaN; 
    if (paramString.equals("Infinity"))
      return POSITIVE_INFINITY; 
    if (paramString.equals("-Infinity"))
      return NEGATIVE_INFINITY; 
    int i = paramString.indexOf(':');
    int j = i;
    if (i < 0)
      j = paramString.indexOf('/'); 
    if (j >= 0)
      try {
        i = Integer.parseInt(paramString.substring(0, j));
        return 
          new Rational(i, Integer.parseInt(paramString.substring(j + 1)));
      } catch (NumberFormatException numberFormatException) {
        throw invalidRational(paramString);
      }  
    throw invalidRational(paramString);
  }
}
