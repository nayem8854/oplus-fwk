package android.util;

import com.android.internal.util.ArrayUtils;
import com.android.internal.util.Preconditions;
import java.util.Arrays;
import libcore.util.EmptyArray;

public class IntArray implements Cloneable {
  private static final int MIN_CAPACITY_INCREMENT = 12;
  
  private int mSize;
  
  private int[] mValues;
  
  private IntArray(int[] paramArrayOfint, int paramInt) {
    this.mValues = paramArrayOfint;
    this.mSize = Preconditions.checkArgumentInRange(paramInt, 0, paramArrayOfint.length, "size");
  }
  
  public IntArray() {
    this(10);
  }
  
  public IntArray(int paramInt) {
    if (paramInt == 0) {
      this.mValues = EmptyArray.INT;
    } else {
      this.mValues = ArrayUtils.newUnpaddedIntArray(paramInt);
    } 
    this.mSize = 0;
  }
  
  public static IntArray wrap(int[] paramArrayOfint) {
    return new IntArray(paramArrayOfint, paramArrayOfint.length);
  }
  
  public static IntArray fromArray(int[] paramArrayOfint, int paramInt) {
    return wrap(Arrays.copyOf(paramArrayOfint, paramInt));
  }
  
  public void resize(int paramInt) {
    Preconditions.checkArgumentNonnegative(paramInt);
    int[] arrayOfInt = this.mValues;
    if (paramInt <= arrayOfInt.length) {
      Arrays.fill(arrayOfInt, paramInt, arrayOfInt.length, 0);
    } else {
      ensureCapacity(paramInt - this.mSize);
    } 
    this.mSize = paramInt;
  }
  
  public void add(int paramInt) {
    add(this.mSize, paramInt);
  }
  
  public void add(int paramInt1, int paramInt2) {
    ensureCapacity(1);
    int i = this.mSize, j = i - paramInt1;
    this.mSize = ++i;
    ArrayUtils.checkBounds(i, paramInt1);
    if (j != 0) {
      int[] arrayOfInt = this.mValues;
      System.arraycopy(arrayOfInt, paramInt1, arrayOfInt, paramInt1 + 1, j);
    } 
    this.mValues[paramInt1] = paramInt2;
  }
  
  public int binarySearch(int paramInt) {
    return ContainerHelpers.binarySearch(this.mValues, this.mSize, paramInt);
  }
  
  public void addAll(IntArray paramIntArray) {
    int i = paramIntArray.mSize;
    ensureCapacity(i);
    System.arraycopy(paramIntArray.mValues, 0, this.mValues, this.mSize, i);
    this.mSize += i;
  }
  
  private void ensureCapacity(int paramInt) {
    int i = this.mSize;
    int j = i + paramInt;
    if (j >= this.mValues.length) {
      if (i < 6) {
        paramInt = 12;
      } else {
        paramInt = i >> 1;
      } 
      paramInt += i;
      if (paramInt <= j)
        paramInt = j; 
      int[] arrayOfInt = ArrayUtils.newUnpaddedIntArray(paramInt);
      System.arraycopy(this.mValues, 0, arrayOfInt, 0, i);
      this.mValues = arrayOfInt;
    } 
  }
  
  public void clear() {
    this.mSize = 0;
  }
  
  public IntArray clone() throws CloneNotSupportedException {
    IntArray intArray = (IntArray)super.clone();
    intArray.mValues = (int[])this.mValues.clone();
    return intArray;
  }
  
  public int get(int paramInt) {
    ArrayUtils.checkBounds(this.mSize, paramInt);
    return this.mValues[paramInt];
  }
  
  public void set(int paramInt1, int paramInt2) {
    ArrayUtils.checkBounds(this.mSize, paramInt1);
    this.mValues[paramInt1] = paramInt2;
  }
  
  public int indexOf(int paramInt) {
    int i = this.mSize;
    for (byte b = 0; b < i; b++) {
      if (this.mValues[b] == paramInt)
        return b; 
    } 
    return -1;
  }
  
  public void remove(int paramInt) {
    ArrayUtils.checkBounds(this.mSize, paramInt);
    int[] arrayOfInt = this.mValues;
    System.arraycopy(arrayOfInt, paramInt + 1, arrayOfInt, paramInt, this.mSize - paramInt - 1);
    this.mSize--;
  }
  
  public int size() {
    return this.mSize;
  }
  
  public int[] toArray() {
    return Arrays.copyOf(this.mValues, this.mSize);
  }
}
