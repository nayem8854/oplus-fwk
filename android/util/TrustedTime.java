package android.util;

public interface TrustedTime {
  @Deprecated
  long currentTimeMillis();
  
  @Deprecated
  boolean forceRefresh();
  
  boolean forceSync();
  
  @Deprecated
  long getCacheAge();
  
  @Deprecated
  boolean hasCache();
}
