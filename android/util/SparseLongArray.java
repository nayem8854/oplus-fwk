package android.util;

import com.android.internal.util.ArrayUtils;
import com.android.internal.util.GrowingArrayUtils;
import libcore.util.EmptyArray;

public class SparseLongArray implements Cloneable {
  private int[] mKeys;
  
  private int mSize;
  
  private long[] mValues;
  
  public SparseLongArray() {
    this(10);
  }
  
  public SparseLongArray(int paramInt) {
    if (paramInt == 0) {
      this.mKeys = EmptyArray.INT;
      this.mValues = EmptyArray.LONG;
    } else {
      long[] arrayOfLong = ArrayUtils.newUnpaddedLongArray(paramInt);
      this.mKeys = new int[arrayOfLong.length];
    } 
    this.mSize = 0;
  }
  
  public SparseLongArray clone() {
    SparseLongArray sparseLongArray = null;
    try {
      SparseLongArray sparseLongArray1 = (SparseLongArray)super.clone();
      sparseLongArray = sparseLongArray1;
      sparseLongArray1.mKeys = (int[])this.mKeys.clone();
      sparseLongArray = sparseLongArray1;
      sparseLongArray1.mValues = (long[])this.mValues.clone();
      sparseLongArray = sparseLongArray1;
    } catch (CloneNotSupportedException cloneNotSupportedException) {}
    return sparseLongArray;
  }
  
  public long get(int paramInt) {
    return get(paramInt, 0L);
  }
  
  public long get(int paramInt, long paramLong) {
    paramInt = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt);
    if (paramInt < 0)
      return paramLong; 
    return this.mValues[paramInt];
  }
  
  public void delete(int paramInt) {
    paramInt = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt);
    if (paramInt >= 0)
      removeAt(paramInt); 
  }
  
  public void removeAtRange(int paramInt1, int paramInt2) {
    paramInt2 = Math.min(paramInt2, this.mSize - paramInt1);
    int[] arrayOfInt = this.mKeys;
    System.arraycopy(arrayOfInt, paramInt1 + paramInt2, arrayOfInt, paramInt1, this.mSize - paramInt1 + paramInt2);
    long[] arrayOfLong = this.mValues;
    System.arraycopy(arrayOfLong, paramInt1 + paramInt2, arrayOfLong, paramInt1, this.mSize - paramInt1 + paramInt2);
    this.mSize -= paramInt2;
  }
  
  public void removeAt(int paramInt) {
    int[] arrayOfInt = this.mKeys;
    System.arraycopy(arrayOfInt, paramInt + 1, arrayOfInt, paramInt, this.mSize - paramInt + 1);
    long[] arrayOfLong = this.mValues;
    System.arraycopy(arrayOfLong, paramInt + 1, arrayOfLong, paramInt, this.mSize - paramInt + 1);
    this.mSize--;
  }
  
  public void put(int paramInt, long paramLong) {
    int i = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt);
    if (i >= 0) {
      this.mValues[i] = paramLong;
    } else {
      i ^= 0xFFFFFFFF;
      this.mKeys = GrowingArrayUtils.insert(this.mKeys, this.mSize, i, paramInt);
      this.mValues = GrowingArrayUtils.insert(this.mValues, this.mSize, i, paramLong);
      this.mSize++;
    } 
  }
  
  public int size() {
    return this.mSize;
  }
  
  public int keyAt(int paramInt) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds)
      return this.mKeys[paramInt]; 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public long valueAt(int paramInt) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds)
      return this.mValues[paramInt]; 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public int indexOfKey(int paramInt) {
    return ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt);
  }
  
  public int indexOfValue(long paramLong) {
    for (byte b = 0; b < this.mSize; b++) {
      if (this.mValues[b] == paramLong)
        return b; 
    } 
    return -1;
  }
  
  public void clear() {
    this.mSize = 0;
  }
  
  public void append(int paramInt, long paramLong) {
    int i = this.mSize;
    if (i != 0 && paramInt <= this.mKeys[i - 1]) {
      put(paramInt, paramLong);
      return;
    } 
    this.mKeys = GrowingArrayUtils.append(this.mKeys, this.mSize, paramInt);
    this.mValues = GrowingArrayUtils.append(this.mValues, this.mSize, paramLong);
    this.mSize++;
  }
  
  public String toString() {
    if (size() <= 0)
      return "{}"; 
    StringBuilder stringBuilder = new StringBuilder(this.mSize * 28);
    stringBuilder.append('{');
    for (byte b = 0; b < this.mSize; b++) {
      if (b > 0)
        stringBuilder.append(", "); 
      int i = keyAt(b);
      stringBuilder.append(i);
      stringBuilder.append('=');
      long l = valueAt(b);
      stringBuilder.append(l);
    } 
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
}
