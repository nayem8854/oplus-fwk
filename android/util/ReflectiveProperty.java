package android.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class ReflectiveProperty<T, V> extends Property<T, V> {
  private static final String PREFIX_GET = "get";
  
  private static final String PREFIX_IS = "is";
  
  private static final String PREFIX_SET = "set";
  
  private Field mField;
  
  private Method mGetter;
  
  private Method mSetter;
  
  public ReflectiveProperty(Class<T> paramClass, Class<V> paramClass1, String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: aload_2
    //   2: aload_3
    //   3: invokespecial <init> : (Ljava/lang/Class;Ljava/lang/String;)V
    //   6: aload_3
    //   7: iconst_0
    //   8: invokevirtual charAt : (I)C
    //   11: invokestatic toUpperCase : (C)C
    //   14: istore #4
    //   16: aload_3
    //   17: iconst_1
    //   18: invokevirtual substring : (I)Ljava/lang/String;
    //   21: astore #5
    //   23: new java/lang/StringBuilder
    //   26: dup
    //   27: invokespecial <init> : ()V
    //   30: astore #6
    //   32: aload #6
    //   34: iload #4
    //   36: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   39: pop
    //   40: aload #6
    //   42: aload #5
    //   44: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   47: pop
    //   48: aload #6
    //   50: invokevirtual toString : ()Ljava/lang/String;
    //   53: astore #6
    //   55: new java/lang/StringBuilder
    //   58: dup
    //   59: invokespecial <init> : ()V
    //   62: astore #5
    //   64: aload #5
    //   66: ldc 'get'
    //   68: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   71: pop
    //   72: aload #5
    //   74: aload #6
    //   76: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   79: pop
    //   80: aload #5
    //   82: invokevirtual toString : ()Ljava/lang/String;
    //   85: astore #5
    //   87: aload_0
    //   88: aload_1
    //   89: aload #5
    //   91: aconst_null
    //   92: checkcast [Ljava/lang/Class;
    //   95: invokevirtual getMethod : (Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   98: putfield mGetter : Ljava/lang/reflect/Method;
    //   101: goto -> 152
    //   104: astore #5
    //   106: new java/lang/StringBuilder
    //   109: dup
    //   110: invokespecial <init> : ()V
    //   113: astore #5
    //   115: aload #5
    //   117: ldc 'is'
    //   119: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   122: pop
    //   123: aload #5
    //   125: aload #6
    //   127: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   130: pop
    //   131: aload #5
    //   133: invokevirtual toString : ()Ljava/lang/String;
    //   136: astore #5
    //   138: aload_0
    //   139: aload_1
    //   140: aload #5
    //   142: aconst_null
    //   143: checkcast [Ljava/lang/Class;
    //   146: invokevirtual getMethod : (Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   149: putfield mGetter : Ljava/lang/reflect/Method;
    //   152: aload_0
    //   153: getfield mGetter : Ljava/lang/reflect/Method;
    //   156: invokevirtual getReturnType : ()Ljava/lang/Class;
    //   159: astore_3
    //   160: aload_0
    //   161: aload_2
    //   162: aload_3
    //   163: invokespecial typesMatch : (Ljava/lang/Class;Ljava/lang/Class;)Z
    //   166: ifeq -> 218
    //   169: new java/lang/StringBuilder
    //   172: dup
    //   173: invokespecial <init> : ()V
    //   176: astore_2
    //   177: aload_2
    //   178: ldc 'set'
    //   180: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   183: pop
    //   184: aload_2
    //   185: aload #6
    //   187: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   190: pop
    //   191: aload_2
    //   192: invokevirtual toString : ()Ljava/lang/String;
    //   195: astore_2
    //   196: aload_0
    //   197: aload_1
    //   198: aload_2
    //   199: iconst_1
    //   200: anewarray java/lang/Class
    //   203: dup
    //   204: iconst_0
    //   205: aload_3
    //   206: aastore
    //   207: invokevirtual getMethod : (Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   210: putfield mSetter : Ljava/lang/reflect/Method;
    //   213: goto -> 217
    //   216: astore_1
    //   217: return
    //   218: new java/lang/StringBuilder
    //   221: dup
    //   222: invokespecial <init> : ()V
    //   225: astore_1
    //   226: aload_1
    //   227: ldc 'Underlying type ('
    //   229: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   232: pop
    //   233: aload_1
    //   234: aload_3
    //   235: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   238: pop
    //   239: aload_1
    //   240: ldc ') does not match Property type ('
    //   242: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   245: pop
    //   246: aload_1
    //   247: aload_2
    //   248: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   251: pop
    //   252: aload_1
    //   253: ldc ')'
    //   255: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   258: pop
    //   259: new android/util/NoSuchPropertyException
    //   262: dup
    //   263: aload_1
    //   264: invokevirtual toString : ()Ljava/lang/String;
    //   267: invokespecial <init> : (Ljava/lang/String;)V
    //   270: athrow
    //   271: astore #6
    //   273: aload_1
    //   274: aload_3
    //   275: invokevirtual getField : (Ljava/lang/String;)Ljava/lang/reflect/Field;
    //   278: astore_1
    //   279: aload_0
    //   280: aload_1
    //   281: putfield mField : Ljava/lang/reflect/Field;
    //   284: aload_1
    //   285: invokevirtual getType : ()Ljava/lang/Class;
    //   288: astore #6
    //   290: aload_0
    //   291: aload_2
    //   292: aload #6
    //   294: invokespecial typesMatch : (Ljava/lang/Class;Ljava/lang/Class;)Z
    //   297: ifeq -> 301
    //   300: return
    //   301: new android/util/NoSuchPropertyException
    //   304: astore #5
    //   306: new java/lang/StringBuilder
    //   309: astore_1
    //   310: aload_1
    //   311: invokespecial <init> : ()V
    //   314: aload_1
    //   315: ldc 'Underlying type ('
    //   317: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   320: pop
    //   321: aload_1
    //   322: aload #6
    //   324: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   327: pop
    //   328: aload_1
    //   329: ldc ') does not match Property type ('
    //   331: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   334: pop
    //   335: aload_1
    //   336: aload_2
    //   337: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   340: pop
    //   341: aload_1
    //   342: ldc ')'
    //   344: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   347: pop
    //   348: aload #5
    //   350: aload_1
    //   351: invokevirtual toString : ()Ljava/lang/String;
    //   354: invokespecial <init> : (Ljava/lang/String;)V
    //   357: aload #5
    //   359: athrow
    //   360: astore_1
    //   361: new java/lang/StringBuilder
    //   364: dup
    //   365: invokespecial <init> : ()V
    //   368: astore_1
    //   369: aload_1
    //   370: ldc 'No accessor method or field found for property with name '
    //   372: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   375: pop
    //   376: aload_1
    //   377: aload_3
    //   378: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   381: pop
    //   382: new android/util/NoSuchPropertyException
    //   385: dup
    //   386: aload_1
    //   387: invokevirtual toString : ()Ljava/lang/String;
    //   390: invokespecial <init> : (Ljava/lang/String;)V
    //   393: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #47	-> 0
    //   #48	-> 6
    //   #49	-> 16
    //   #50	-> 23
    //   #51	-> 55
    //   #53	-> 87
    //   #75	-> 101
    //   #54	-> 104
    //   #56	-> 106
    //   #58	-> 138
    //   #74	-> 152
    //   #76	-> 152
    //   #78	-> 160
    //   #82	-> 169
    //   #84	-> 196
    //   #87	-> 213
    //   #85	-> 216
    //   #88	-> 217
    //   #79	-> 218
    //   #59	-> 271
    //   #62	-> 273
    //   #63	-> 284
    //   #64	-> 290
    //   #68	-> 300
    //   #65	-> 301
    //   #69	-> 360
    //   #71	-> 361
    // Exception table:
    //   from	to	target	type
    //   87	101	104	java/lang/NoSuchMethodException
    //   138	152	271	java/lang/NoSuchMethodException
    //   196	213	216	java/lang/NoSuchMethodException
    //   273	284	360	java/lang/NoSuchFieldException
    //   284	290	360	java/lang/NoSuchFieldException
    //   290	300	360	java/lang/NoSuchFieldException
    //   301	360	360	java/lang/NoSuchFieldException
  }
  
  private boolean typesMatch(Class<V> paramClass, Class paramClass1) {
    // Byte code:
    //   0: iconst_1
    //   1: istore_3
    //   2: aload_2
    //   3: aload_1
    //   4: if_acmpeq -> 153
    //   7: aload_2
    //   8: invokevirtual isPrimitive : ()Z
    //   11: ifeq -> 151
    //   14: aload_2
    //   15: getstatic java/lang/Float.TYPE : Ljava/lang/Class;
    //   18: if_acmpne -> 30
    //   21: iload_3
    //   22: istore #4
    //   24: aload_1
    //   25: ldc java/lang/Float
    //   27: if_acmpeq -> 148
    //   30: aload_2
    //   31: getstatic java/lang/Integer.TYPE : Ljava/lang/Class;
    //   34: if_acmpne -> 46
    //   37: iload_3
    //   38: istore #4
    //   40: aload_1
    //   41: ldc java/lang/Integer
    //   43: if_acmpeq -> 148
    //   46: aload_2
    //   47: getstatic java/lang/Boolean.TYPE : Ljava/lang/Class;
    //   50: if_acmpne -> 62
    //   53: iload_3
    //   54: istore #4
    //   56: aload_1
    //   57: ldc java/lang/Boolean
    //   59: if_acmpeq -> 148
    //   62: aload_2
    //   63: getstatic java/lang/Long.TYPE : Ljava/lang/Class;
    //   66: if_acmpne -> 78
    //   69: iload_3
    //   70: istore #4
    //   72: aload_1
    //   73: ldc java/lang/Long
    //   75: if_acmpeq -> 148
    //   78: aload_2
    //   79: getstatic java/lang/Double.TYPE : Ljava/lang/Class;
    //   82: if_acmpne -> 94
    //   85: iload_3
    //   86: istore #4
    //   88: aload_1
    //   89: ldc java/lang/Double
    //   91: if_acmpeq -> 148
    //   94: aload_2
    //   95: getstatic java/lang/Short.TYPE : Ljava/lang/Class;
    //   98: if_acmpne -> 110
    //   101: iload_3
    //   102: istore #4
    //   104: aload_1
    //   105: ldc java/lang/Short
    //   107: if_acmpeq -> 148
    //   110: aload_2
    //   111: getstatic java/lang/Byte.TYPE : Ljava/lang/Class;
    //   114: if_acmpne -> 126
    //   117: iload_3
    //   118: istore #4
    //   120: aload_1
    //   121: ldc java/lang/Byte
    //   123: if_acmpeq -> 148
    //   126: aload_2
    //   127: getstatic java/lang/Character.TYPE : Ljava/lang/Class;
    //   130: if_acmpne -> 145
    //   133: aload_1
    //   134: ldc java/lang/Character
    //   136: if_acmpne -> 145
    //   139: iload_3
    //   140: istore #4
    //   142: goto -> 148
    //   145: iconst_0
    //   146: istore #4
    //   148: iload #4
    //   150: ireturn
    //   151: iconst_0
    //   152: ireturn
    //   153: iconst_1
    //   154: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #98	-> 0
    //   #99	-> 7
    //   #100	-> 14
    //   #109	-> 151
    //   #111	-> 153
  }
  
  public void set(T paramT, V paramV) {
    Method method = this.mSetter;
    if (method != null) {
      try {
        method.invoke(paramT, new Object[] { paramV });
      } catch (IllegalAccessException illegalAccessException) {
        throw new AssertionError();
      } catch (InvocationTargetException invocationTargetException) {
        throw new RuntimeException(invocationTargetException.getCause());
      } 
    } else {
      Field field = this.mField;
      if (field != null)
        try {
          field.set(invocationTargetException, paramV);
          return;
        } catch (IllegalAccessException illegalAccessException) {
          throw new AssertionError();
        }  
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Property ");
      stringBuilder.append(getName());
      stringBuilder.append(" is read-only");
      throw new UnsupportedOperationException(stringBuilder.toString());
    } 
  }
  
  public V get(T paramT) {
    Method method = this.mGetter;
    if (method != null)
      try {
        return (V)method.invoke(paramT, (Object[])null);
      } catch (IllegalAccessException illegalAccessException) {
        throw new AssertionError();
      } catch (InvocationTargetException invocationTargetException) {
        throw new RuntimeException(invocationTargetException.getCause());
      }  
    Field field = this.mField;
    if (field != null)
      try {
        return (V)field.get(invocationTargetException);
      } catch (IllegalAccessException illegalAccessException) {
        throw new AssertionError();
      }  
    throw new AssertionError();
  }
  
  public boolean isReadOnly() {
    boolean bool;
    if (this.mSetter == null && this.mField == null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
