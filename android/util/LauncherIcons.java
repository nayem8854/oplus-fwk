package android.util;

import android.app.ActivityThread;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableWrapper;
import android.graphics.drawable.LayerDrawable;

public final class LauncherIcons {
  private static final int AMBIENT_SHADOW_ALPHA = 30;
  
  private static final float ICON_SIZE_BLUR_FACTOR = 0.010416667F;
  
  private static final float ICON_SIZE_KEY_SHADOW_DELTA_FACTOR = 0.020833334F;
  
  private static final int KEY_SHADOW_ALPHA = 61;
  
  private final int mIconSize;
  
  private final Resources mRes;
  
  private final SparseArray<Bitmap> mShadowCache = new SparseArray<>();
  
  public LauncherIcons(Context paramContext) {
    Resources resources = paramContext.getResources();
    this.mIconSize = resources.getDimensionPixelSize(17104896);
  }
  
  public Drawable wrapIconDrawableWithShadow(Drawable paramDrawable) {
    if (!(paramDrawable instanceof AdaptiveIconDrawable))
      return paramDrawable; 
    Bitmap bitmap = getShadowBitmap((AdaptiveIconDrawable)paramDrawable);
    return (Drawable)new ShadowDrawable(bitmap, paramDrawable);
  }
  
  private Bitmap getShadowBitmap(AdaptiveIconDrawable paramAdaptiveIconDrawable) {
    int i = Math.max(this.mIconSize, paramAdaptiveIconDrawable.getIntrinsicHeight());
    synchronized (this.mShadowCache) {
      Bitmap bitmap = this.mShadowCache.get(i);
      if (bitmap != null)
        return bitmap; 
      paramAdaptiveIconDrawable.setBounds(0, 0, i, i);
      float f1 = i * 0.010416667F;
      float f2 = i * 0.020833334F;
      int j = (int)(i + f1 * 2.0F + f2);
      null = Bitmap.createBitmap(j, j, Bitmap.Config.ARGB_8888);
      Canvas canvas = new Canvas(null);
      canvas.translate(f2 / 2.0F + f1, f1);
      Paint paint = new Paint(1);
      paint.setColor(0);
      paint.setShadowLayer(f1, 0.0F, 0.0F, 503316480);
      canvas.drawPath(paramAdaptiveIconDrawable.getIconMask(), paint);
      canvas.translate(0.0F, f2);
      paint.setShadowLayer(f1, 0.0F, 0.0F, 1023410176);
      canvas.drawPath(paramAdaptiveIconDrawable.getIconMask(), paint);
      canvas.setBitmap(null);
      synchronized (this.mShadowCache) {
        this.mShadowCache.put(i, null);
        return null;
      } 
    } 
  }
  
  public Drawable getBadgeDrawable(int paramInt1, int paramInt2) {
    return getBadgedDrawable(null, paramInt1, paramInt2);
  }
  
  public Drawable getBadgedDrawable(Drawable paramDrawable, int paramInt1, int paramInt2) {
    Drawable[] arrayOfDrawable;
    Resources resources = ActivityThread.currentActivityThread().getApplication().getResources();
    Drawable drawable2 = resources.getDrawable(17302390);
    Drawable drawable3 = resources.getDrawable(17302389);
    drawable3 = drawable3.getConstantState().newDrawable().mutate();
    Drawable drawable1 = resources.getDrawable(paramInt1);
    drawable1.setTint(paramInt2);
    if (paramDrawable == null) {
      arrayOfDrawable = new Drawable[3];
      arrayOfDrawable[0] = drawable2;
      arrayOfDrawable[1] = drawable3;
      arrayOfDrawable[2] = drawable1;
    } else {
      arrayOfDrawable = new Drawable[] { (Drawable)arrayOfDrawable, drawable2, drawable3, drawable1 };
    } 
    return (Drawable)new LayerDrawable(arrayOfDrawable);
  }
  
  class ShadowDrawable extends DrawableWrapper {
    final MyConstantState mState;
    
    public ShadowDrawable(LauncherIcons this$0, Drawable param1Drawable) {
      super(param1Drawable);
      this.mState = new MyConstantState((Bitmap)this$0, param1Drawable.getConstantState());
    }
    
    ShadowDrawable(LauncherIcons this$0) {
      super(((MyConstantState)this$0).mChildState.newDrawable());
      this.mState = (MyConstantState)this$0;
    }
    
    public Drawable.ConstantState getConstantState() {
      return this.mState;
    }
    
    public void draw(Canvas param1Canvas) {
      Rect rect = getBounds();
      param1Canvas.drawBitmap(this.mState.mShadow, null, rect, this.mState.mPaint);
      param1Canvas.save();
      float f1 = rect.width();
      float f2 = rect.height();
      param1Canvas.translate(f1 * 0.9599999F * 0.020833334F, f2 * 0.9599999F * 0.010416667F);
      param1Canvas.scale(0.9599999F, 0.9599999F);
      super.draw(param1Canvas);
      param1Canvas.restore();
    }
    
    class MyConstantState extends Drawable.ConstantState {
      final Drawable.ConstantState mChildState;
      
      final Paint mPaint = new Paint(2);
      
      final Bitmap mShadow;
      
      MyConstantState(LauncherIcons.ShadowDrawable this$0, Drawable.ConstantState param2ConstantState) {
        this.mShadow = (Bitmap)this$0;
        this.mChildState = param2ConstantState;
      }
      
      public Drawable newDrawable() {
        return (Drawable)new LauncherIcons.ShadowDrawable(this);
      }
      
      public int getChangingConfigurations() {
        return this.mChildState.getChangingConfigurations();
      }
    }
  }
}
