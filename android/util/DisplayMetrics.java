package android.util;

import android.os.SystemProperties;

public class DisplayMetrics {
  public static final int DENSITY_140 = 140;
  
  public static final int DENSITY_180 = 180;
  
  public static final int DENSITY_200 = 200;
  
  public static final int DENSITY_220 = 220;
  
  public static final int DENSITY_260 = 260;
  
  public static final int DENSITY_280 = 280;
  
  public static final int DENSITY_300 = 300;
  
  public static final int DENSITY_340 = 340;
  
  public static final int DENSITY_360 = 360;
  
  public static final int DENSITY_400 = 400;
  
  public static final int DENSITY_420 = 420;
  
  public static final int DENSITY_440 = 440;
  
  public static final int DENSITY_450 = 450;
  
  public static final int DENSITY_560 = 560;
  
  public static final int DENSITY_600 = 600;
  
  public static final int DENSITY_DEFAULT = 160;
  
  public static final float DENSITY_DEFAULT_SCALE = 0.00625F;
  
  @Deprecated
  public static int DENSITY_DEVICE = getDeviceDensity();
  
  public static final int DENSITY_DEVICE_STABLE = getDeviceDensity();
  
  public static final int DENSITY_HIGH = 240;
  
  public static final int DENSITY_LOW = 120;
  
  public static final int DENSITY_MEDIUM = 160;
  
  public static final int DENSITY_TV = 213;
  
  public static final int DENSITY_XHIGH = 320;
  
  public static final int DENSITY_XXHIGH = 480;
  
  public static final int DENSITY_XXXHIGH = 640;
  
  public float density;
  
  public int densityDpi;
  
  public int heightPixels;
  
  public float noncompatDensity;
  
  public int noncompatDensityDpi;
  
  public int noncompatHeightPixels;
  
  public float noncompatScaledDensity;
  
  public int noncompatWidthPixels;
  
  public float noncompatXdpi;
  
  public float noncompatYdpi;
  
  public float scaledDensity;
  
  public int widthPixels;
  
  public float xdpi;
  
  public float ydpi;
  
  public void setTo(DisplayMetrics paramDisplayMetrics) {
    if (this == paramDisplayMetrics)
      return; 
    this.widthPixels = paramDisplayMetrics.widthPixels;
    this.heightPixels = paramDisplayMetrics.heightPixels;
    this.density = paramDisplayMetrics.density;
    this.densityDpi = paramDisplayMetrics.densityDpi;
    this.scaledDensity = paramDisplayMetrics.scaledDensity;
    this.xdpi = paramDisplayMetrics.xdpi;
    this.ydpi = paramDisplayMetrics.ydpi;
    this.noncompatWidthPixels = paramDisplayMetrics.noncompatWidthPixels;
    this.noncompatHeightPixels = paramDisplayMetrics.noncompatHeightPixels;
    this.noncompatDensity = paramDisplayMetrics.noncompatDensity;
    this.noncompatDensityDpi = paramDisplayMetrics.noncompatDensityDpi;
    this.noncompatScaledDensity = paramDisplayMetrics.noncompatScaledDensity;
    this.noncompatXdpi = paramDisplayMetrics.noncompatXdpi;
    this.noncompatYdpi = paramDisplayMetrics.noncompatYdpi;
  }
  
  public void setToDefaults() {
    this.widthPixels = 0;
    this.heightPixels = 0;
    int i = DENSITY_DEVICE;
    float f1 = i / 160.0F;
    this.densityDpi = i;
    this.scaledDensity = f1;
    float f2 = i;
    float f3 = i;
    this.noncompatWidthPixels = 0;
    this.noncompatHeightPixels = 0;
    this.noncompatDensity = f1;
    this.noncompatDensityDpi = i;
    this.noncompatScaledDensity = f1;
    this.noncompatXdpi = f2;
    this.noncompatYdpi = f3;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool;
    if (paramObject instanceof DisplayMetrics && equals((DisplayMetrics)paramObject)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean equals(DisplayMetrics paramDisplayMetrics) {
    boolean bool;
    if (equalsPhysical(paramDisplayMetrics) && this.scaledDensity == paramDisplayMetrics.scaledDensity && this.noncompatScaledDensity == paramDisplayMetrics.noncompatScaledDensity) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean equalsPhysical(DisplayMetrics paramDisplayMetrics) {
    boolean bool;
    if (paramDisplayMetrics != null && this.widthPixels == paramDisplayMetrics.widthPixels && this.heightPixels == paramDisplayMetrics.heightPixels && this.density == paramDisplayMetrics.density && this.densityDpi == paramDisplayMetrics.densityDpi && this.xdpi == paramDisplayMetrics.xdpi && this.ydpi == paramDisplayMetrics.ydpi && this.noncompatWidthPixels == paramDisplayMetrics.noncompatWidthPixels && this.noncompatHeightPixels == paramDisplayMetrics.noncompatHeightPixels && this.noncompatDensity == paramDisplayMetrics.noncompatDensity && this.noncompatDensityDpi == paramDisplayMetrics.noncompatDensityDpi && this.noncompatXdpi == paramDisplayMetrics.noncompatXdpi && this.noncompatYdpi == paramDisplayMetrics.noncompatYdpi) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int hashCode() {
    return this.widthPixels * this.heightPixels * this.densityDpi;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("DisplayMetrics{density=");
    stringBuilder.append(this.density);
    stringBuilder.append(", width=");
    stringBuilder.append(this.widthPixels);
    stringBuilder.append(", height=");
    stringBuilder.append(this.heightPixels);
    stringBuilder.append(", scaledDensity=");
    stringBuilder.append(this.scaledDensity);
    stringBuilder.append(", xdpi=");
    stringBuilder.append(this.xdpi);
    stringBuilder.append(", ydpi=");
    stringBuilder.append(this.ydpi);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  private static int getDeviceDensity() {
    int i = SystemProperties.getInt("ro.sf.lcd_density", 160);
    return SystemProperties.getInt("qemu.sf.lcd_density", i);
  }
}
