package android.util;

import libcore.util.FP16;

public final class Half extends Number implements Comparable<Half> {
  public static final short EPSILON = 5120;
  
  public static final short LOWEST_VALUE = -1025;
  
  public static final int MAX_EXPONENT = 15;
  
  public static final short MAX_VALUE = 31743;
  
  public static final int MIN_EXPONENT = -14;
  
  public static final short MIN_NORMAL = 1024;
  
  public static final short MIN_VALUE = 1;
  
  public static final short NEGATIVE_INFINITY = -1024;
  
  public static final short NEGATIVE_ZERO = -32768;
  
  public static final short NaN = 32256;
  
  public static final short POSITIVE_INFINITY = 31744;
  
  public static final short POSITIVE_ZERO = 0;
  
  public static final int SIZE = 16;
  
  private final short mValue;
  
  public Half(short paramShort) {
    this.mValue = paramShort;
  }
  
  public Half(float paramFloat) {
    this.mValue = toHalf(paramFloat);
  }
  
  public Half(double paramDouble) {
    this.mValue = toHalf((float)paramDouble);
  }
  
  public Half(String paramString) throws NumberFormatException {
    this.mValue = toHalf(Float.parseFloat(paramString));
  }
  
  public short halfValue() {
    return this.mValue;
  }
  
  public byte byteValue() {
    return (byte)(int)toFloat(this.mValue);
  }
  
  public short shortValue() {
    return (short)(int)toFloat(this.mValue);
  }
  
  public int intValue() {
    return (int)toFloat(this.mValue);
  }
  
  public long longValue() {
    return (long)toFloat(this.mValue);
  }
  
  public float floatValue() {
    return toFloat(this.mValue);
  }
  
  public double doubleValue() {
    return toFloat(this.mValue);
  }
  
  public boolean isNaN() {
    return isNaN(this.mValue);
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject instanceof Half) {
      short s = ((Half)paramObject).mValue;
      if (halfToIntBits(s) == halfToIntBits(this.mValue))
        return true; 
    } 
    return false;
  }
  
  public int hashCode() {
    return hashCode(this.mValue);
  }
  
  public String toString() {
    return toString(this.mValue);
  }
  
  public int compareTo(Half paramHalf) {
    return compare(this.mValue, paramHalf.mValue);
  }
  
  public static int hashCode(short paramShort) {
    return halfToIntBits(paramShort);
  }
  
  public static int compare(short paramShort1, short paramShort2) {
    return FP16.compare(paramShort1, paramShort2);
  }
  
  public static short halfToShortBits(short paramShort) {
    if ((paramShort & Short.MAX_VALUE) > 31744)
      paramShort = 32256; 
    return paramShort;
  }
  
  public static int halfToIntBits(short paramShort) {
    int i;
    if ((paramShort & Short.MAX_VALUE) > 31744) {
      paramShort = 32256;
    } else {
      i = 0xFFFF & paramShort;
    } 
    return i;
  }
  
  public static int halfToRawIntBits(short paramShort) {
    return 0xFFFF & paramShort;
  }
  
  public static short intBitsToHalf(int paramInt) {
    return (short)(0xFFFF & paramInt);
  }
  
  public static short copySign(short paramShort1, short paramShort2) {
    return (short)(0x8000 & paramShort2 | paramShort1 & Short.MAX_VALUE);
  }
  
  public static short abs(short paramShort) {
    return (short)(paramShort & Short.MAX_VALUE);
  }
  
  public static short round(short paramShort) {
    return FP16.rint(paramShort);
  }
  
  public static short ceil(short paramShort) {
    return FP16.ceil(paramShort);
  }
  
  public static short floor(short paramShort) {
    return FP16.floor(paramShort);
  }
  
  public static short trunc(short paramShort) {
    return FP16.trunc(paramShort);
  }
  
  public static short min(short paramShort1, short paramShort2) {
    return FP16.min(paramShort1, paramShort2);
  }
  
  public static short max(short paramShort1, short paramShort2) {
    return FP16.max(paramShort1, paramShort2);
  }
  
  public static boolean less(short paramShort1, short paramShort2) {
    return FP16.less(paramShort1, paramShort2);
  }
  
  public static boolean lessEquals(short paramShort1, short paramShort2) {
    return FP16.lessEquals(paramShort1, paramShort2);
  }
  
  public static boolean greater(short paramShort1, short paramShort2) {
    return FP16.greater(paramShort1, paramShort2);
  }
  
  public static boolean greaterEquals(short paramShort1, short paramShort2) {
    return FP16.greaterEquals(paramShort1, paramShort2);
  }
  
  public static boolean equals(short paramShort1, short paramShort2) {
    return FP16.equals(paramShort1, paramShort2);
  }
  
  public static int getSign(short paramShort) {
    if ((0x8000 & paramShort) == 0) {
      paramShort = 1;
    } else {
      paramShort = -1;
    } 
    return paramShort;
  }
  
  public static int getExponent(short paramShort) {
    return (paramShort >>> 10 & 0x1F) - 15;
  }
  
  public static int getSignificand(short paramShort) {
    return paramShort & 0x3FF;
  }
  
  public static boolean isInfinite(short paramShort) {
    return FP16.isInfinite(paramShort);
  }
  
  public static boolean isNaN(short paramShort) {
    return FP16.isNaN(paramShort);
  }
  
  public static boolean isNormalized(short paramShort) {
    return FP16.isNormalized(paramShort);
  }
  
  public static float toFloat(short paramShort) {
    return FP16.toFloat(paramShort);
  }
  
  public static short toHalf(float paramFloat) {
    return FP16.toHalf(paramFloat);
  }
  
  public static Half valueOf(short paramShort) {
    return new Half(paramShort);
  }
  
  public static Half valueOf(float paramFloat) {
    return new Half(paramFloat);
  }
  
  public static Half valueOf(String paramString) {
    return new Half(paramString);
  }
  
  public static short parseHalf(String paramString) throws NumberFormatException {
    return toHalf(Float.parseFloat(paramString));
  }
  
  public static String toString(short paramShort) {
    return Float.toString(toFloat(paramShort));
  }
  
  public static String toHexString(short paramShort) {
    return FP16.toHexString(paramShort);
  }
}
