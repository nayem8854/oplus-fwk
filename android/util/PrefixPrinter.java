package android.util;

public class PrefixPrinter implements Printer {
  private final String mPrefix;
  
  private final Printer mPrinter;
  
  public static Printer create(Printer paramPrinter, String paramString) {
    if (paramString == null || paramString.equals(""))
      return paramPrinter; 
    return new PrefixPrinter(paramPrinter, paramString);
  }
  
  private PrefixPrinter(Printer paramPrinter, String paramString) {
    this.mPrinter = paramPrinter;
    this.mPrefix = paramString;
  }
  
  public void println(String paramString) {
    Printer printer = this.mPrinter;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.mPrefix);
    stringBuilder.append(paramString);
    printer.println(stringBuilder.toString());
  }
}
