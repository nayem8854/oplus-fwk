package android.util;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Set;
import java.util.function.Predicate;
import libcore.util.EmptyArray;

public final class ArraySet<E> implements Collection<E>, Set<E> {
  private static final int BASE_SIZE = 4;
  
  private static final int CACHE_SIZE = 10;
  
  private static final boolean DEBUG = false;
  
  private static final String TAG = "ArraySet";
  
  static Object[] sBaseCache;
  
  private static final Object sBaseCacheLock = new Object();
  
  static int sBaseCacheSize;
  
  static Object[] sTwiceBaseCache;
  
  private static final Object sTwiceBaseCacheLock = new Object();
  
  static int sTwiceBaseCacheSize;
  
  Object[] mArray;
  
  private MapCollections<E, E> mCollections;
  
  int[] mHashes;
  
  private final boolean mIdentityHashCode;
  
  int mSize;
  
  private int binarySearch(int[] paramArrayOfint, int paramInt) {
    try {
      return ContainerHelpers.binarySearch(paramArrayOfint, this.mSize, paramInt);
    } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
      throw new ConcurrentModificationException();
    } 
  }
  
  private int indexOf(Object paramObject, int paramInt) {
    int i = this.mSize;
    if (i == 0)
      return -1; 
    int j = binarySearch(this.mHashes, paramInt);
    if (j < 0)
      return j; 
    if (paramObject.equals(this.mArray[j]))
      return j; 
    int k;
    for (k = j + 1; k < i && this.mHashes[k] == paramInt; k++) {
      if (paramObject.equals(this.mArray[k]))
        return k; 
    } 
    for (i = j - 1; i >= 0 && this.mHashes[i] == paramInt; i--) {
      if (paramObject.equals(this.mArray[i]))
        return i; 
    } 
    return k ^ 0xFFFFFFFF;
  }
  
  private int indexOfNull() {
    int i = this.mSize;
    if (i == 0)
      return -1; 
    int j = binarySearch(this.mHashes, 0);
    if (j < 0)
      return j; 
    if (this.mArray[j] == null)
      return j; 
    int k;
    for (k = j + 1; k < i && this.mHashes[k] == 0; k++) {
      if (this.mArray[k] == null)
        return k; 
    } 
    for (; --j >= 0 && this.mHashes[j] == 0; j--) {
      if (this.mArray[j] == null)
        return j; 
    } 
    return k ^ 0xFFFFFFFF;
  }
  
  private void allocArrays(int paramInt) {
    if (paramInt == 8) {
      synchronized (sTwiceBaseCacheLock) {
        if (sTwiceBaseCache != null) {
          Object[] arrayOfObject = sTwiceBaseCache;
          try {
            this.mArray = arrayOfObject;
            sTwiceBaseCache = (Object[])arrayOfObject[0];
            int[] arrayOfInt = (int[])arrayOfObject[1];
            if (arrayOfInt != null) {
              arrayOfObject[1] = null;
              arrayOfObject[0] = null;
              sTwiceBaseCacheSize--;
              return;
            } 
          } catch (ClassCastException classCastException) {}
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Found corrupt ArraySet cache: [0]=");
          stringBuilder.append(arrayOfObject[0]);
          stringBuilder.append(" [1]=");
          stringBuilder.append(arrayOfObject[1]);
          Slog.wtf("ArraySet", stringBuilder.toString());
          sTwiceBaseCache = null;
          sTwiceBaseCacheSize = 0;
        } 
      } 
    } else if (paramInt == 4) {
      synchronized (sBaseCacheLock) {
        if (sBaseCache != null) {
          Object[] arrayOfObject = sBaseCache;
          try {
            this.mArray = arrayOfObject;
            sBaseCache = (Object[])arrayOfObject[0];
            int[] arrayOfInt = (int[])arrayOfObject[1];
            if (arrayOfInt != null) {
              arrayOfObject[1] = null;
              arrayOfObject[0] = null;
              sBaseCacheSize--;
              return;
            } 
          } catch (ClassCastException classCastException) {}
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Found corrupt ArraySet cache: [0]=");
          stringBuilder.append(arrayOfObject[0]);
          stringBuilder.append(" [1]=");
          stringBuilder.append(arrayOfObject[1]);
          Slog.wtf("ArraySet", stringBuilder.toString());
          sBaseCache = null;
          sBaseCacheSize = 0;
        } 
      } 
    } 
    this.mHashes = new int[paramInt];
    this.mArray = new Object[paramInt];
  }
  
  private static void freeArrays(int[] paramArrayOfint, Object[] paramArrayOfObject, int paramInt) {
    if (paramArrayOfint.length == 8) {
      synchronized (sTwiceBaseCacheLock) {
        if (sTwiceBaseCacheSize < 10) {
          paramArrayOfObject[0] = sTwiceBaseCache;
          paramArrayOfObject[1] = paramArrayOfint;
          for (; --paramInt >= 2; paramInt--)
            paramArrayOfObject[paramInt] = null; 
          sTwiceBaseCache = paramArrayOfObject;
          sTwiceBaseCacheSize++;
        } 
      } 
    } else if (paramArrayOfint.length == 4) {
      synchronized (sBaseCacheLock) {
        if (sBaseCacheSize < 10) {
          paramArrayOfObject[0] = sBaseCache;
          paramArrayOfObject[1] = paramArrayOfint;
          for (; --paramInt >= 2; paramInt--)
            paramArrayOfObject[paramInt] = null; 
          sBaseCache = paramArrayOfObject;
          sBaseCacheSize++;
        } 
      } 
    } 
  }
  
  public ArraySet() {
    this(0, false);
  }
  
  public ArraySet(int paramInt) {
    this(paramInt, false);
  }
  
  public ArraySet(int paramInt, boolean paramBoolean) {
    this.mIdentityHashCode = paramBoolean;
    if (paramInt == 0) {
      this.mHashes = EmptyArray.INT;
      this.mArray = EmptyArray.OBJECT;
    } else {
      allocArrays(paramInt);
    } 
    this.mSize = 0;
  }
  
  public ArraySet(ArraySet<E> paramArraySet) {
    this();
    if (paramArraySet != null)
      addAll(paramArraySet); 
  }
  
  public ArraySet(Collection<? extends E> paramCollection) {
    this();
    if (paramCollection != null)
      addAll(paramCollection); 
  }
  
  public ArraySet(E[] paramArrayOfE) {
    this();
    if (paramArrayOfE != null) {
      int i;
      byte b;
      for (i = paramArrayOfE.length, b = 0; b < i; ) {
        E e = paramArrayOfE[b];
        add(e);
        b++;
      } 
    } 
  }
  
  public void clear() {
    if (this.mSize != 0) {
      int[] arrayOfInt = this.mHashes;
      Object[] arrayOfObject = this.mArray;
      int i = this.mSize;
      this.mHashes = EmptyArray.INT;
      this.mArray = EmptyArray.OBJECT;
      this.mSize = 0;
      freeArrays(arrayOfInt, arrayOfObject, i);
    } 
    if (this.mSize == 0)
      return; 
    throw new ConcurrentModificationException();
  }
  
  public void ensureCapacity(int paramInt) {
    int i = this.mSize;
    if (this.mHashes.length < paramInt) {
      int[] arrayOfInt = this.mHashes;
      Object[] arrayOfObject = this.mArray;
      allocArrays(paramInt);
      paramInt = this.mSize;
      if (paramInt > 0) {
        System.arraycopy(arrayOfInt, 0, this.mHashes, 0, paramInt);
        System.arraycopy(arrayOfObject, 0, this.mArray, 0, this.mSize);
      } 
      freeArrays(arrayOfInt, arrayOfObject, this.mSize);
    } 
    if (this.mSize == i)
      return; 
    throw new ConcurrentModificationException();
  }
  
  public boolean contains(Object paramObject) {
    boolean bool;
    if (indexOf(paramObject) >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int indexOf(Object paramObject) {
    int i;
    if (paramObject == null) {
      i = indexOfNull();
    } else {
      if (this.mIdentityHashCode) {
        i = System.identityHashCode(paramObject);
      } else {
        i = paramObject.hashCode();
      } 
      i = indexOf(paramObject, i);
    } 
    return i;
  }
  
  public E valueAt(int paramInt) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds)
      return valueAtUnchecked(paramInt); 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public E valueAtUnchecked(int paramInt) {
    return (E)this.mArray[paramInt];
  }
  
  public boolean isEmpty() {
    boolean bool;
    if (this.mSize <= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean add(E paramE) {
    int j, k, i = this.mSize;
    if (paramE == null) {
      j = 0;
      k = indexOfNull();
    } else {
      int n;
      if (this.mIdentityHashCode) {
        n = System.identityHashCode(paramE);
      } else {
        n = paramE.hashCode();
      } 
      k = indexOf(paramE, n);
      j = n;
    } 
    if (k >= 0)
      return false; 
    k ^= 0xFFFFFFFF;
    if (i >= this.mHashes.length) {
      int n = 4;
      if (i >= 8) {
        n = (i >> 1) + i;
      } else if (i >= 4) {
        n = 8;
      } 
      int[] arrayOfInt = this.mHashes;
      Object[] arrayOfObject = this.mArray;
      allocArrays(n);
      if (i == this.mSize) {
        int[] arrayOfInt1 = this.mHashes;
        if (arrayOfInt1.length > 0) {
          System.arraycopy(arrayOfInt, 0, arrayOfInt1, 0, arrayOfInt.length);
          System.arraycopy(arrayOfObject, 0, this.mArray, 0, arrayOfObject.length);
        } 
        freeArrays(arrayOfInt, arrayOfObject, i);
      } else {
        throw new ConcurrentModificationException();
      } 
    } 
    if (k < i) {
      int[] arrayOfInt = this.mHashes;
      System.arraycopy(arrayOfInt, k, arrayOfInt, k + 1, i - k);
      Object[] arrayOfObject = this.mArray;
      System.arraycopy(arrayOfObject, k, arrayOfObject, k + 1, i - k);
    } 
    int m = this.mSize;
    if (i == m) {
      int[] arrayOfInt = this.mHashes;
      if (k < arrayOfInt.length) {
        arrayOfInt[k] = j;
        this.mArray[k] = paramE;
        this.mSize = m + 1;
        return true;
      } 
    } 
    throw new ConcurrentModificationException();
  }
  
  public void append(E paramE) {
    int k, i = this.mSize;
    int j = this.mSize;
    if (paramE == null) {
      k = 0;
    } else if (this.mIdentityHashCode) {
      k = System.identityHashCode(paramE);
    } else {
      k = paramE.hashCode();
    } 
    int[] arrayOfInt = this.mHashes;
    if (j < arrayOfInt.length) {
      if (j > 0 && arrayOfInt[j - 1] > k) {
        add(paramE);
        return;
      } 
      if (i == this.mSize) {
        this.mSize = j + 1;
        this.mHashes[j] = k;
        this.mArray[j] = paramE;
        return;
      } 
      throw new ConcurrentModificationException();
    } 
    throw new IllegalStateException("Array is full");
  }
  
  public void addAll(ArraySet<? extends E> paramArraySet) {
    int i = paramArraySet.mSize;
    ensureCapacity(this.mSize + i);
    if (this.mSize == 0) {
      if (i > 0) {
        System.arraycopy(paramArraySet.mHashes, 0, this.mHashes, 0, i);
        System.arraycopy(paramArraySet.mArray, 0, this.mArray, 0, i);
        if (this.mSize == 0) {
          this.mSize = i;
        } else {
          throw new ConcurrentModificationException();
        } 
      } 
    } else {
      for (byte b = 0; b < i; b++)
        add(paramArraySet.valueAt(b)); 
    } 
  }
  
  public boolean remove(Object paramObject) {
    int i = indexOf(paramObject);
    if (i >= 0) {
      removeAt(i);
      return true;
    } 
    return false;
  }
  
  private boolean shouldShrink() {
    boolean bool;
    int[] arrayOfInt = this.mHashes;
    if (arrayOfInt.length > 8 && this.mSize < arrayOfInt.length / 3) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private int getNewShrunkenSize() {
    int i = this.mSize, j = 8;
    if (i > 8)
      j = (i >> 1) + i; 
    return j;
  }
  
  public E removeAt(int paramInt) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds) {
      int i = this.mSize;
      Object object = this.mArray[paramInt];
      if (i <= 1) {
        clear();
      } else {
        int j = i - 1;
        if (shouldShrink()) {
          int k = getNewShrunkenSize();
          int[] arrayOfInt = this.mHashes;
          Object[] arrayOfObject = this.mArray;
          allocArrays(k);
          if (paramInt > 0) {
            System.arraycopy(arrayOfInt, 0, this.mHashes, 0, paramInt);
            System.arraycopy(arrayOfObject, 0, this.mArray, 0, paramInt);
          } 
          if (paramInt < j) {
            System.arraycopy(arrayOfInt, paramInt + 1, this.mHashes, paramInt, j - paramInt);
            System.arraycopy(arrayOfObject, paramInt + 1, this.mArray, paramInt, j - paramInt);
          } 
        } else {
          if (paramInt < j) {
            int[] arrayOfInt = this.mHashes;
            System.arraycopy(arrayOfInt, paramInt + 1, arrayOfInt, paramInt, j - paramInt);
            Object[] arrayOfObject = this.mArray;
            System.arraycopy(arrayOfObject, paramInt + 1, arrayOfObject, paramInt, j - paramInt);
          } 
          this.mArray[j] = null;
        } 
        if (i == this.mSize) {
          this.mSize = j;
          return (E)object;
        } 
        throw new ConcurrentModificationException();
      } 
      return (E)object;
    } 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public boolean removeAll(ArraySet<? extends E> paramArraySet) {
    boolean bool;
    int i = paramArraySet.mSize;
    int j = this.mSize;
    for (byte b = 0; b < i; b++)
      remove(paramArraySet.valueAt(b)); 
    if (j != this.mSize) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean removeIf(Predicate<? super E> paramPredicate) {
    int k;
    if (this.mSize == 0)
      return false; 
    int i = 0;
    byte b = 0;
    int j = 0;
    while (true) {
      k = this.mSize;
      if (j < k) {
        if (paramPredicate.test((E)this.mArray[j])) {
          b++;
        } else {
          if (i != j) {
            Object[] arrayOfObject = this.mArray;
            arrayOfObject[i] = arrayOfObject[j];
            int[] arrayOfInt = this.mHashes;
            arrayOfInt[i] = arrayOfInt[j];
          } 
          i++;
        } 
        j++;
        continue;
      } 
      break;
    } 
    if (b == 0)
      return false; 
    if (b == k) {
      clear();
      return true;
    } 
    this.mSize = k - b;
    if (shouldShrink()) {
      j = getNewShrunkenSize();
      int[] arrayOfInt = this.mHashes;
      Object[] arrayOfObject = this.mArray;
      allocArrays(j);
      System.arraycopy(arrayOfInt, 0, this.mHashes, 0, this.mSize);
      System.arraycopy(arrayOfObject, 0, this.mArray, 0, this.mSize);
    } else {
      j = this.mSize;
      while (true) {
        Object[] arrayOfObject = this.mArray;
        if (j < arrayOfObject.length) {
          arrayOfObject[j] = null;
          j++;
          continue;
        } 
        break;
      } 
    } 
    return true;
  }
  
  public int size() {
    return this.mSize;
  }
  
  public Object[] toArray() {
    int i = this.mSize;
    Object[] arrayOfObject = new Object[i];
    System.arraycopy(this.mArray, 0, arrayOfObject, 0, i);
    return arrayOfObject;
  }
  
  public <T> T[] toArray(T[] paramArrayOfT) {
    T[] arrayOfT = paramArrayOfT;
    if (paramArrayOfT.length < this.mSize)
      arrayOfT = (T[])Array.newInstance(paramArrayOfT.getClass().getComponentType(), this.mSize); 
    System.arraycopy(this.mArray, 0, arrayOfT, 0, this.mSize);
    int i = arrayOfT.length, j = this.mSize;
    if (i > j)
      arrayOfT[j] = null; 
    return arrayOfT;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject instanceof Set) {
      Set set = (Set)paramObject;
      if (size() != set.size())
        return false; 
      byte b = 0;
      try {
        for (; b < this.mSize; b++) {
          paramObject = valueAt(b);
          boolean bool = set.contains(paramObject);
          if (!bool)
            return false; 
        } 
        return true;
      } catch (NullPointerException nullPointerException) {
        return false;
      } catch (ClassCastException classCastException) {
        return false;
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int[] arrayOfInt = this.mHashes;
    int i = 0;
    byte b;
    int j;
    for (b = 0, j = this.mSize; b < j; b++)
      i += arrayOfInt[b]; 
    return i;
  }
  
  public String toString() {
    if (isEmpty())
      return "{}"; 
    StringBuilder stringBuilder = new StringBuilder(this.mSize * 14);
    stringBuilder.append('{');
    for (byte b = 0; b < this.mSize; b++) {
      if (b > 0)
        stringBuilder.append(", "); 
      E e = valueAt(b);
      if (e != this) {
        stringBuilder.append(e);
      } else {
        stringBuilder.append("(this Set)");
      } 
    } 
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  private MapCollections<E, E> getCollection() {
    if (this.mCollections == null)
      this.mCollections = (MapCollections<E, E>)new Object(this); 
    return this.mCollections;
  }
  
  public Iterator<E> iterator() {
    return getCollection().getKeySet().iterator();
  }
  
  public boolean containsAll(Collection<?> paramCollection) {
    Iterator<?> iterator = paramCollection.iterator();
    while (iterator.hasNext()) {
      if (!contains(iterator.next()))
        return false; 
    } 
    return true;
  }
  
  public boolean addAll(Collection<? extends E> paramCollection) {
    // Byte code:
    //   0: aload_0
    //   1: aload_0
    //   2: getfield mSize : I
    //   5: aload_1
    //   6: invokeinterface size : ()I
    //   11: iadd
    //   12: invokevirtual ensureCapacity : (I)V
    //   15: iconst_0
    //   16: istore_2
    //   17: aload_1
    //   18: invokeinterface iterator : ()Ljava/util/Iterator;
    //   23: astore_1
    //   24: aload_1
    //   25: invokeinterface hasNext : ()Z
    //   30: ifeq -> 51
    //   33: aload_1
    //   34: invokeinterface next : ()Ljava/lang/Object;
    //   39: astore_3
    //   40: iload_2
    //   41: aload_0
    //   42: aload_3
    //   43: invokevirtual add : (Ljava/lang/Object;)Z
    //   46: ior
    //   47: istore_2
    //   48: goto -> 24
    //   51: iload_2
    //   52: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #943	-> 0
    //   #944	-> 15
    //   #945	-> 17
    //   #946	-> 40
    //   #947	-> 48
    //   #948	-> 51
  }
  
  public boolean removeAll(Collection<?> paramCollection) {
    boolean bool = false;
    for (Collection<?> paramCollection : paramCollection)
      bool |= remove(paramCollection); 
    return bool;
  }
  
  public boolean retainAll(Collection<?> paramCollection) {
    boolean bool = false;
    for (int i = this.mSize - 1; i >= 0; i--) {
      if (!paramCollection.contains(this.mArray[i])) {
        removeAt(i);
        bool = true;
      } 
    } 
    return bool;
  }
}
