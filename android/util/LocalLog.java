package android.util;

import android.os.SystemClock;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayDeque;
import java.util.Deque;

public final class LocalLog {
  private final Deque<String> mLog;
  
  private final int mMaxLines;
  
  private final boolean mUseLocalTimestamps;
  
  public LocalLog(int paramInt) {
    this(paramInt, true);
  }
  
  public LocalLog(int paramInt, boolean paramBoolean) {
    this.mMaxLines = Math.max(0, paramInt);
    this.mLog = new ArrayDeque<>(this.mMaxLines);
    this.mUseLocalTimestamps = paramBoolean;
  }
  
  public void log(String paramString) {
    if (this.mMaxLines <= 0)
      return; 
    if (this.mUseLocalTimestamps) {
      paramString = String.format("%s - %s", new Object[] { LocalDateTime.now(), paramString });
    } else {
      long l = SystemClock.elapsedRealtime();
      Instant instant = Instant.now();
      paramString = String.format("%s / %s - %s", new Object[] { Long.valueOf(l), instant, paramString });
    } 
    append(paramString);
  }
  
  private void append(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mLog : Ljava/util/Deque;
    //   6: invokeinterface size : ()I
    //   11: aload_0
    //   12: getfield mMaxLines : I
    //   15: if_icmplt -> 31
    //   18: aload_0
    //   19: getfield mLog : Ljava/util/Deque;
    //   22: invokeinterface remove : ()Ljava/lang/Object;
    //   27: pop
    //   28: goto -> 2
    //   31: aload_0
    //   32: getfield mLog : Ljava/util/Deque;
    //   35: aload_1
    //   36: invokeinterface add : (Ljava/lang/Object;)Z
    //   41: pop
    //   42: aload_0
    //   43: monitorexit
    //   44: return
    //   45: astore_1
    //   46: aload_0
    //   47: monitorexit
    //   48: aload_1
    //   49: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #72	-> 2
    //   #73	-> 18
    //   #75	-> 31
    //   #76	-> 42
    //   #71	-> 45
    // Exception table:
    //   from	to	target	type
    //   2	18	45	finally
    //   18	28	45	finally
    //   31	42	45	finally
  }
  
  public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_2
    //   4: invokevirtual dump : (Ljava/io/PrintWriter;)V
    //   7: aload_0
    //   8: monitorexit
    //   9: return
    //   10: astore_1
    //   11: aload_0
    //   12: monitorexit
    //   13: aload_1
    //   14: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #80	-> 2
    //   #81	-> 7
    //   #79	-> 10
    // Exception table:
    //   from	to	target	type
    //   2	7	10	finally
  }
  
  public void dump(PrintWriter paramPrintWriter) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: ldc ''
    //   5: aload_1
    //   6: invokevirtual dump : (Ljava/lang/String;Ljava/io/PrintWriter;)V
    //   9: aload_0
    //   10: monitorexit
    //   11: return
    //   12: astore_1
    //   13: aload_0
    //   14: monitorexit
    //   15: aload_1
    //   16: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #84	-> 2
    //   #85	-> 9
    //   #83	-> 12
    // Exception table:
    //   from	to	target	type
    //   2	9	12	finally
  }
  
  public void dump(String paramString, PrintWriter paramPrintWriter) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mLog : Ljava/util/Deque;
    //   6: invokeinterface iterator : ()Ljava/util/Iterator;
    //   11: astore_3
    //   12: aload_3
    //   13: invokeinterface hasNext : ()Z
    //   18: ifeq -> 48
    //   21: aload_2
    //   22: ldc '%s%s\\n'
    //   24: iconst_2
    //   25: anewarray java/lang/Object
    //   28: dup
    //   29: iconst_0
    //   30: aload_1
    //   31: aastore
    //   32: dup
    //   33: iconst_1
    //   34: aload_3
    //   35: invokeinterface next : ()Ljava/lang/Object;
    //   40: aastore
    //   41: invokevirtual printf : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
    //   44: pop
    //   45: goto -> 12
    //   48: aload_0
    //   49: monitorexit
    //   50: return
    //   51: astore_1
    //   52: aload_0
    //   53: monitorexit
    //   54: aload_1
    //   55: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #94	-> 2
    //   #95	-> 12
    //   #96	-> 21
    //   #98	-> 48
    //   #93	-> 51
    // Exception table:
    //   from	to	target	type
    //   2	12	51	finally
    //   12	21	51	finally
    //   21	45	51	finally
  }
  
  public void reverseDump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_2
    //   4: invokevirtual reverseDump : (Ljava/io/PrintWriter;)V
    //   7: aload_0
    //   8: monitorexit
    //   9: return
    //   10: astore_1
    //   11: aload_0
    //   12: monitorexit
    //   13: aload_1
    //   14: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #101	-> 2
    //   #102	-> 7
    //   #100	-> 10
    // Exception table:
    //   from	to	target	type
    //   2	7	10	finally
  }
  
  public void reverseDump(PrintWriter paramPrintWriter) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mLog : Ljava/util/Deque;
    //   6: invokeinterface descendingIterator : ()Ljava/util/Iterator;
    //   11: astore_2
    //   12: aload_2
    //   13: invokeinterface hasNext : ()Z
    //   18: ifeq -> 37
    //   21: aload_1
    //   22: aload_2
    //   23: invokeinterface next : ()Ljava/lang/Object;
    //   28: checkcast java/lang/String
    //   31: invokevirtual println : (Ljava/lang/String;)V
    //   34: goto -> 12
    //   37: aload_0
    //   38: monitorexit
    //   39: return
    //   40: astore_1
    //   41: aload_0
    //   42: monitorexit
    //   43: aload_1
    //   44: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #105	-> 2
    //   #106	-> 12
    //   #107	-> 21
    //   #109	-> 37
    //   #104	-> 40
    // Exception table:
    //   from	to	target	type
    //   2	12	40	finally
    //   12	21	40	finally
    //   21	34	40	finally
  }
  
  public static class ReadOnlyLocalLog {
    private final LocalLog mLog;
    
    ReadOnlyLocalLog(LocalLog param1LocalLog) {
      this.mLog = param1LocalLog;
    }
    
    public void dump(FileDescriptor param1FileDescriptor, PrintWriter param1PrintWriter, String[] param1ArrayOfString) {
      this.mLog.dump(param1PrintWriter);
    }
    
    public void dump(PrintWriter param1PrintWriter) {
      this.mLog.dump(param1PrintWriter);
    }
    
    public void reverseDump(FileDescriptor param1FileDescriptor, PrintWriter param1PrintWriter, String[] param1ArrayOfString) {
      this.mLog.reverseDump(param1PrintWriter);
    }
    
    public void reverseDump(PrintWriter param1PrintWriter) {
      this.mLog.reverseDump(param1PrintWriter);
    }
  }
  
  public ReadOnlyLocalLog readOnlyLocalLog() {
    return new ReadOnlyLocalLog(this);
  }
}
