package android.util;

import android.graphics.Path;
import dalvik.annotation.optimization.FastNative;

public class PathParser {
  static final String LOGTAG = PathParser.class.getSimpleName();
  
  public static Path createPathFromPathData(String paramString) {
    if (paramString != null) {
      Path path = new Path();
      nParseStringForPath(path.mNativePath, paramString, paramString.length());
      return path;
    } 
    throw new IllegalArgumentException("Path string can not be null.");
  }
  
  public static void createPathFromPathData(Path paramPath, PathData paramPathData) {
    nCreatePathFromPathData(paramPath.mNativePath, paramPathData.mNativePathData);
  }
  
  public static boolean canMorph(PathData paramPathData1, PathData paramPathData2) {
    return nCanMorph(paramPathData1.mNativePathData, paramPathData2.mNativePathData);
  }
  
  public static class PathData {
    long mNativePathData = 0L;
    
    public PathData() {
      this.mNativePathData = PathParser.nCreateEmptyPathData();
    }
    
    public PathData(PathData param1PathData) {
      this.mNativePathData = PathParser.nCreatePathData(param1PathData.mNativePathData);
    }
    
    public PathData(String param1String) {
      long l = PathParser.nCreatePathDataFromString(param1String, param1String.length());
      if (l != 0L)
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid pathData: ");
      stringBuilder.append(param1String);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public long getNativePtr() {
      return this.mNativePathData;
    }
    
    public void setPathData(PathData param1PathData) {
      PathParser.nSetPathData(this.mNativePathData, param1PathData.mNativePathData);
    }
    
    protected void finalize() throws Throwable {
      long l = this.mNativePathData;
      if (l != 0L) {
        PathParser.nFinalize(l);
        this.mNativePathData = 0L;
      } 
      super.finalize();
    }
  }
  
  public static boolean interpolatePathData(PathData paramPathData1, PathData paramPathData2, PathData paramPathData3, float paramFloat) {
    return nInterpolatePathData(paramPathData1.mNativePathData, paramPathData2.mNativePathData, paramPathData3.mNativePathData, paramFloat);
  }
  
  @FastNative
  private static native boolean nCanMorph(long paramLong1, long paramLong2);
  
  @FastNative
  private static native long nCreateEmptyPathData();
  
  @FastNative
  private static native long nCreatePathData(long paramLong);
  
  private static native long nCreatePathDataFromString(String paramString, int paramInt);
  
  @FastNative
  private static native void nCreatePathFromPathData(long paramLong1, long paramLong2);
  
  @FastNative
  private static native void nFinalize(long paramLong);
  
  @FastNative
  private static native boolean nInterpolatePathData(long paramLong1, long paramLong2, long paramLong3, float paramFloat);
  
  private static native void nParseStringForPath(long paramLong, String paramString, int paramInt);
  
  @FastNative
  private static native void nSetPathData(long paramLong1, long paramLong2);
}
