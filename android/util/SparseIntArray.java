package android.util;

import com.android.internal.util.ArrayUtils;
import com.android.internal.util.GrowingArrayUtils;
import java.util.Arrays;
import libcore.util.EmptyArray;

public class SparseIntArray implements Cloneable {
  private int[] mKeys;
  
  private int mSize;
  
  private int[] mValues;
  
  public SparseIntArray() {
    this(10);
  }
  
  public SparseIntArray(int paramInt) {
    if (paramInt == 0) {
      this.mKeys = EmptyArray.INT;
      this.mValues = EmptyArray.INT;
    } else {
      int[] arrayOfInt = ArrayUtils.newUnpaddedIntArray(paramInt);
      this.mValues = new int[arrayOfInt.length];
    } 
    this.mSize = 0;
  }
  
  public SparseIntArray clone() {
    SparseIntArray sparseIntArray = null;
    try {
      SparseIntArray sparseIntArray1 = (SparseIntArray)super.clone();
      sparseIntArray = sparseIntArray1;
      sparseIntArray1.mKeys = (int[])this.mKeys.clone();
      sparseIntArray = sparseIntArray1;
      sparseIntArray1.mValues = (int[])this.mValues.clone();
      sparseIntArray = sparseIntArray1;
    } catch (CloneNotSupportedException cloneNotSupportedException) {}
    return sparseIntArray;
  }
  
  public int get(int paramInt) {
    return get(paramInt, 0);
  }
  
  public int get(int paramInt1, int paramInt2) {
    paramInt1 = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt1);
    if (paramInt1 < 0)
      return paramInt2; 
    return this.mValues[paramInt1];
  }
  
  public void delete(int paramInt) {
    paramInt = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt);
    if (paramInt >= 0)
      removeAt(paramInt); 
  }
  
  public void removeAt(int paramInt) {
    int[] arrayOfInt = this.mKeys;
    System.arraycopy(arrayOfInt, paramInt + 1, arrayOfInt, paramInt, this.mSize - paramInt + 1);
    arrayOfInt = this.mValues;
    System.arraycopy(arrayOfInt, paramInt + 1, arrayOfInt, paramInt, this.mSize - paramInt + 1);
    this.mSize--;
  }
  
  public void put(int paramInt1, int paramInt2) {
    int i = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt1);
    if (i >= 0) {
      this.mValues[i] = paramInt2;
    } else {
      i ^= 0xFFFFFFFF;
      this.mKeys = GrowingArrayUtils.insert(this.mKeys, this.mSize, i, paramInt1);
      this.mValues = GrowingArrayUtils.insert(this.mValues, this.mSize, i, paramInt2);
      this.mSize++;
    } 
  }
  
  public int size() {
    return this.mSize;
  }
  
  public int keyAt(int paramInt) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds)
      return this.mKeys[paramInt]; 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public int valueAt(int paramInt) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds)
      return this.mValues[paramInt]; 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public void setValueAt(int paramInt1, int paramInt2) {
    if (paramInt1 < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds) {
      this.mValues[paramInt1] = paramInt2;
      return;
    } 
    throw new ArrayIndexOutOfBoundsException(paramInt1);
  }
  
  public int indexOfKey(int paramInt) {
    return ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt);
  }
  
  public int indexOfValue(int paramInt) {
    for (byte b = 0; b < this.mSize; b++) {
      if (this.mValues[b] == paramInt)
        return b; 
    } 
    return -1;
  }
  
  public void clear() {
    this.mSize = 0;
  }
  
  public void append(int paramInt1, int paramInt2) {
    int i = this.mSize;
    if (i != 0 && paramInt1 <= this.mKeys[i - 1]) {
      put(paramInt1, paramInt2);
      return;
    } 
    this.mKeys = GrowingArrayUtils.append(this.mKeys, this.mSize, paramInt1);
    this.mValues = GrowingArrayUtils.append(this.mValues, this.mSize, paramInt2);
    this.mSize++;
  }
  
  public int[] copyKeys() {
    if (size() == 0)
      return null; 
    return Arrays.copyOf(this.mKeys, size());
  }
  
  public String toString() {
    if (size() <= 0)
      return "{}"; 
    StringBuilder stringBuilder = new StringBuilder(this.mSize * 28);
    stringBuilder.append('{');
    for (byte b = 0; b < this.mSize; b++) {
      if (b > 0)
        stringBuilder.append(", "); 
      int i = keyAt(b);
      stringBuilder.append(i);
      stringBuilder.append('=');
      i = valueAt(b);
      stringBuilder.append(i);
    } 
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
}
