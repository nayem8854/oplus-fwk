package android.util;

import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DebugUtils {
  public static boolean isObjectSelected(Object paramObject) {
    boolean bool1 = false, bool2 = false;
    String str = System.getenv("ANDROID_OBJECT_FILTER");
    boolean bool3 = bool1;
    if (str != null) {
      bool3 = bool1;
      if (str.length() > 0) {
        String[] arrayOfString = str.split("@");
        bool3 = bool1;
        if (paramObject.getClass().getSimpleName().matches(arrayOfString[0])) {
          byte b = 1;
          bool1 = bool2;
          while (true) {
            bool3 = bool1;
            if (b < arrayOfString.length) {
              String[] arrayOfString1 = arrayOfString[b].split("=");
              Class<?> clazz2 = paramObject.getClass();
              Class<?> clazz1 = clazz2;
              try {
                Class<?> clazz;
                Method method;
                do {
                  StringBuilder stringBuilder = new StringBuilder();
                  this();
                  stringBuilder.append("get");
                  String str2 = arrayOfString1[0];
                  stringBuilder.append(str2.substring(0, 1).toUpperCase(Locale.ROOT));
                  str2 = arrayOfString1[0];
                  stringBuilder.append(str2.substring(1));
                  String str1 = stringBuilder.toString();
                  Class[] arrayOfClass = (Class[])null;
                  method = clazz1.getDeclaredMethod(str1, arrayOfClass);
                  clazz1 = clazz = clazz2.getSuperclass();
                } while (clazz != null && method == null);
                bool3 = bool1;
                if (method != null) {
                  Object[] arrayOfObject = (Object[])null;
                  Object object = method.invoke(paramObject, arrayOfObject);
                  if (object != null) {
                    object = object.toString();
                  } else {
                    object = "null";
                  } 
                  bool3 = object.matches(arrayOfString1[1]);
                  bool3 = bool1 | bool3;
                } 
                bool1 = bool3;
              } catch (NoSuchMethodException noSuchMethodException) {
                noSuchMethodException.printStackTrace();
                bool3 = bool1;
                bool1 = bool3;
              } catch (IllegalAccessException illegalAccessException) {
                illegalAccessException.printStackTrace();
                bool3 = bool1;
                bool1 = bool3;
              } catch (InvocationTargetException invocationTargetException) {
                invocationTargetException.printStackTrace();
              } 
              b++;
              continue;
            } 
            break;
          } 
        } 
      } 
    } 
    return bool3;
  }
  
  public static void buildShortClassTag(Object paramObject, StringBuilder paramStringBuilder) {
    // Byte code:
    //   0: aload_0
    //   1: ifnonnull -> 14
    //   4: aload_1
    //   5: ldc 'null'
    //   7: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10: pop
    //   11: goto -> 92
    //   14: aload_0
    //   15: invokevirtual getClass : ()Ljava/lang/Class;
    //   18: invokevirtual getSimpleName : ()Ljava/lang/String;
    //   21: astore_2
    //   22: aload_2
    //   23: ifnull -> 35
    //   26: aload_2
    //   27: astore_3
    //   28: aload_2
    //   29: invokevirtual isEmpty : ()Z
    //   32: ifeq -> 67
    //   35: aload_0
    //   36: invokevirtual getClass : ()Ljava/lang/Class;
    //   39: invokevirtual getName : ()Ljava/lang/String;
    //   42: astore_2
    //   43: aload_2
    //   44: bipush #46
    //   46: invokevirtual lastIndexOf : (I)I
    //   49: istore #4
    //   51: aload_2
    //   52: astore_3
    //   53: iload #4
    //   55: ifle -> 67
    //   58: aload_2
    //   59: iload #4
    //   61: iconst_1
    //   62: iadd
    //   63: invokevirtual substring : (I)Ljava/lang/String;
    //   66: astore_3
    //   67: aload_1
    //   68: aload_3
    //   69: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   72: pop
    //   73: aload_1
    //   74: bipush #123
    //   76: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   79: pop
    //   80: aload_1
    //   81: aload_0
    //   82: invokestatic identityHashCode : (Ljava/lang/Object;)I
    //   85: invokestatic toHexString : (I)Ljava/lang/String;
    //   88: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   91: pop
    //   92: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #120	-> 0
    //   #121	-> 4
    //   #123	-> 14
    //   #124	-> 22
    //   #125	-> 35
    //   #126	-> 43
    //   #127	-> 51
    //   #128	-> 58
    //   #131	-> 67
    //   #132	-> 73
    //   #133	-> 80
    //   #135	-> 92
  }
  
  public static void printSizeValue(PrintWriter paramPrintWriter, long paramLong) {
    float f1 = (float)paramLong;
    String str1 = "";
    float f2 = f1;
    if (f1 > 900.0F) {
      str1 = "KB";
      f2 = f1 / 1024.0F;
    } 
    float f3 = f2;
    if (f2 > 900.0F) {
      str1 = "MB";
      f3 = f2 / 1024.0F;
    } 
    f1 = f3;
    if (f3 > 900.0F) {
      str1 = "GB";
      f1 = f3 / 1024.0F;
    } 
    f2 = f1;
    if (f1 > 900.0F) {
      str1 = "TB";
      f2 = f1 / 1024.0F;
    } 
    f1 = f2;
    String str2 = str1;
    if (f2 > 900.0F) {
      str2 = "PB";
      f1 = f2 / 1024.0F;
    } 
    if (f1 < 1.0F) {
      str1 = String.format("%.2f", new Object[] { Float.valueOf(f1) });
    } else if (f1 < 10.0F) {
      str1 = String.format("%.1f", new Object[] { Float.valueOf(f1) });
    } else if (f1 < 100.0F) {
      str1 = String.format("%.0f", new Object[] { Float.valueOf(f1) });
    } else {
      str1 = String.format("%.0f", new Object[] { Float.valueOf(f1) });
    } 
    paramPrintWriter.print(str1);
    paramPrintWriter.print(str2);
  }
  
  public static String sizeValueToString(long paramLong, StringBuilder paramStringBuilder) {
    StringBuilder stringBuilder = paramStringBuilder;
    if (paramStringBuilder == null)
      stringBuilder = new StringBuilder(32); 
    float f1 = (float)paramLong;
    String str1 = "";
    float f2 = f1;
    if (f1 > 900.0F) {
      str1 = "KB";
      f2 = f1 / 1024.0F;
    } 
    f1 = f2;
    if (f2 > 900.0F) {
      str1 = "MB";
      f1 = f2 / 1024.0F;
    } 
    f2 = f1;
    if (f1 > 900.0F) {
      str1 = "GB";
      f2 = f1 / 1024.0F;
    } 
    f1 = f2;
    if (f2 > 900.0F) {
      str1 = "TB";
      f1 = f2 / 1024.0F;
    } 
    f2 = f1;
    String str2 = str1;
    if (f1 > 900.0F) {
      str2 = "PB";
      f2 = f1 / 1024.0F;
    } 
    if (f2 < 1.0F) {
      str1 = String.format("%.2f", new Object[] { Float.valueOf(f2) });
    } else if (f2 < 10.0F) {
      str1 = String.format("%.1f", new Object[] { Float.valueOf(f2) });
    } else if (f2 < 100.0F) {
      str1 = String.format("%.0f", new Object[] { Float.valueOf(f2) });
    } else {
      str1 = String.format("%.0f", new Object[] { Float.valueOf(f2) });
    } 
    stringBuilder.append(str1);
    stringBuilder.append(str2);
    return stringBuilder.toString();
  }
  
  public static String valueToString(Class<?> paramClass, String paramString, int paramInt) {
    for (Field field : paramClass.getDeclaredFields()) {
      int i = field.getModifiers();
      if (Modifier.isStatic(i) && Modifier.isFinal(i) && 
        field.getType().equals(int.class) && field.getName().startsWith(paramString))
        try {
          if (paramInt == field.getInt(null))
            return constNameWithoutPrefix(paramString, field); 
        } catch (IllegalAccessException illegalAccessException) {} 
    } 
    return Integer.toString(paramInt);
  }
  
  public static String flagsToString(Class<?> paramClass, String paramString, int paramInt) {
    boolean bool;
    StringBuilder stringBuilder = new StringBuilder();
    byte b = 0;
    if (paramInt == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Field[] arrayOfField;
    int i;
    for (arrayOfField = paramClass.getDeclaredFields(), i = arrayOfField.length; b < i; ) {
      Field field = arrayOfField[b];
      int j = field.getModifiers();
      int k = paramInt;
      if (Modifier.isStatic(j)) {
        k = paramInt;
        if (Modifier.isFinal(j)) {
          k = paramInt;
          if (field.getType().equals(int.class)) {
            k = paramInt;
            if (field.getName().startsWith(paramString)) {
              k = paramInt;
              try {
                j = field.getInt(null);
                if (j == 0 && bool) {
                  k = paramInt;
                  return constNameWithoutPrefix(paramString, field);
                } 
                k = paramInt;
                if (j != 0) {
                  k = paramInt;
                  if ((paramInt & j) == j) {
                    paramInt &= j ^ 0xFFFFFFFF;
                    k = paramInt;
                    stringBuilder.append(constNameWithoutPrefix(paramString, field));
                    k = paramInt;
                    stringBuilder.append('|');
                    k = paramInt;
                  } 
                } 
              } catch (IllegalAccessException illegalAccessException) {}
            } 
          } 
        } 
      } 
      b++;
      paramInt = k;
    } 
    if (paramInt != 0 || stringBuilder.length() == 0) {
      stringBuilder.append(Integer.toHexString(paramInt));
      return stringBuilder.toString();
    } 
    stringBuilder.deleteCharAt(stringBuilder.length() - 1);
    return stringBuilder.toString();
  }
  
  private static String constNameWithoutPrefix(String paramString, Field paramField) {
    return paramField.getName().substring(paramString.length());
  }
  
  public static List<String> callersWithin(Class<?> paramClass, int paramInt) {
    Stream<StackTraceElement> stream1 = Arrays.stream(Thread.currentThread().getStackTrace());
    long l = (paramInt + 3);
    stream1 = stream1.skip(l);
    _$$Lambda$DebugUtils$s1zXuvdixY14YVtRkbZKqVfnthU _$$Lambda$DebugUtils$s1zXuvdixY14YVtRkbZKqVfnthU = new _$$Lambda$DebugUtils$s1zXuvdixY14YVtRkbZKqVfnthU(paramClass);
    stream1 = stream1.filter(_$$Lambda$DebugUtils$s1zXuvdixY14YVtRkbZKqVfnthU);
    -$.Lambda.PNcWARaUCnqxo5KZVrKltn9O64M pNcWARaUCnqxo5KZVrKltn9O64M = _$$Lambda$PNcWARaUCnqxo5KZVrKltn9O64M.INSTANCE;
    Stream<?> stream = stream1.map((Function<? super StackTraceElement, ?>)pNcWARaUCnqxo5KZVrKltn9O64M);
    List<?> list = stream.collect((Collector)Collectors.toList());
    Collections.reverse(list);
    return (List)list;
  }
}
