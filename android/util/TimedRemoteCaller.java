package android.util;

import android.os.SystemClock;
import java.util.concurrent.TimeoutException;

public abstract class TimedRemoteCaller<T> {
  private final Object mLock = new Object();
  
  private final SparseIntArray mAwaitedCalls = new SparseIntArray(1);
  
  private final SparseArray<T> mReceivedCalls = new SparseArray<>(1);
  
  public static final long DEFAULT_CALL_TIMEOUT_MILLIS = 5000L;
  
  private final long mCallTimeoutMillis;
  
  private int mSequenceCounter;
  
  public TimedRemoteCaller(long paramLong) {
    this.mCallTimeoutMillis = paramLong;
  }
  
  protected final int onBeforeRemoteCall() {
    synchronized (this.mLock) {
      int i;
      do {
        i = this.mSequenceCounter;
        this.mSequenceCounter = i + 1;
      } while (this.mAwaitedCalls.get(i) != 0);
      this.mAwaitedCalls.put(i, 1);
      return i;
    } 
  }
  
  protected final void onRemoteMethodResult(T paramT, int paramInt) {
    synchronized (this.mLock) {
      boolean bool;
      if (this.mAwaitedCalls.get(paramInt) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      if (bool) {
        this.mAwaitedCalls.delete(paramInt);
        this.mReceivedCalls.put(paramInt, paramT);
        this.mLock.notifyAll();
      } 
      return;
    } 
  }
  
  protected final T getResultTimed(int paramInt) throws TimeoutException {
    long l = SystemClock.uptimeMillis();
    while (true) {
      try {
        synchronized (this.mLock) {
          if (this.mReceivedCalls.indexOfKey(paramInt) >= 0)
            return this.mReceivedCalls.removeReturnOld(paramInt); 
          long l1 = SystemClock.uptimeMillis();
          l1 = this.mCallTimeoutMillis - l1 - l;
          if (l1 > 0L) {
            this.mLock.wait(l1);
            continue;
          } 
          this.mAwaitedCalls.delete(paramInt);
          TimeoutException timeoutException = new TimeoutException();
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("No response for sequence: ");
          stringBuilder.append(paramInt);
          this(stringBuilder.toString());
          throw timeoutException;
        } 
      } catch (InterruptedException interruptedException) {}
    } 
  }
}
