package android.util.proto;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class ProtoStream {
  public static final long FIELD_COUNT_MASK = 16492674416640L;
  
  public static final long FIELD_COUNT_PACKED = 5497558138880L;
  
  public static final long FIELD_COUNT_REPEATED = 2199023255552L;
  
  public static final int FIELD_COUNT_SHIFT = 40;
  
  public static final long FIELD_COUNT_SINGLE = 1099511627776L;
  
  public static final long FIELD_COUNT_UNKNOWN = 0L;
  
  public static final int FIELD_ID_MASK = -8;
  
  public static final int FIELD_ID_SHIFT = 3;
  
  public static final long FIELD_TYPE_BOOL = 34359738368L;
  
  public static final long FIELD_TYPE_BYTES = 51539607552L;
  
  public static final long FIELD_TYPE_DOUBLE = 4294967296L;
  
  public static final long FIELD_TYPE_ENUM = 60129542144L;
  
  public static final long FIELD_TYPE_FIXED32 = 30064771072L;
  
  public static final long FIELD_TYPE_FIXED64 = 25769803776L;
  
  public static final long FIELD_TYPE_FLOAT = 8589934592L;
  
  public static final long FIELD_TYPE_INT32 = 21474836480L;
  
  public static final long FIELD_TYPE_INT64 = 12884901888L;
  
  public static final long FIELD_TYPE_MASK = 1095216660480L;
  
  public static final long FIELD_TYPE_MESSAGE = 47244640256L;
  
  private static final String[] FIELD_TYPE_NAMES = new String[] { 
      "Double", "Float", "Int64", "UInt64", "Int32", "Fixed64", "Fixed32", "Bool", "String", "Group", 
      "Message", "Bytes", "UInt32", "Enum", "SFixed32", "SFixed64", "SInt32", "SInt64" };
  
  public static final long FIELD_TYPE_SFIXED32 = 64424509440L;
  
  public static final long FIELD_TYPE_SFIXED64 = 68719476736L;
  
  public static final int FIELD_TYPE_SHIFT = 32;
  
  public static final long FIELD_TYPE_SINT32 = 73014444032L;
  
  public static final long FIELD_TYPE_SINT64 = 77309411328L;
  
  public static final long FIELD_TYPE_STRING = 38654705664L;
  
  public static final long FIELD_TYPE_UINT32 = 55834574848L;
  
  public static final long FIELD_TYPE_UINT64 = 17179869184L;
  
  public static final long FIELD_TYPE_UNKNOWN = 0L;
  
  public static final int WIRE_TYPE_END_GROUP = 4;
  
  public static final int WIRE_TYPE_FIXED32 = 5;
  
  public static final int WIRE_TYPE_FIXED64 = 1;
  
  public static final int WIRE_TYPE_LENGTH_DELIMITED = 2;
  
  public static final int WIRE_TYPE_MASK = 7;
  
  public static final int WIRE_TYPE_START_GROUP = 3;
  
  public static final int WIRE_TYPE_VARINT = 0;
  
  public static String getFieldTypeString(long paramLong) {
    int i = (int)((0xFF00000000L & paramLong) >>> 32L) - 1;
    if (i >= 0) {
      String[] arrayOfString = FIELD_TYPE_NAMES;
      if (i < arrayOfString.length)
        return arrayOfString[i]; 
    } 
    return null;
  }
  
  public static String getFieldCountString(long paramLong) {
    if (paramLong == 1099511627776L)
      return ""; 
    if (paramLong == 2199023255552L)
      return "Repeated"; 
    if (paramLong == 5497558138880L)
      return "Packed"; 
    return null;
  }
  
  public static String getWireTypeString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt != 4) {
              if (paramInt != 5)
                return null; 
              return "Fixed32";
            } 
            return "End Group";
          } 
          return "Start Group";
        } 
        return "Length Delimited";
      } 
      return "Fixed64";
    } 
    return "Varint";
  }
  
  public static String getFieldIdString(long paramLong) {
    long l = 0xF0000000000L & paramLong;
    String str1 = getFieldCountString(l);
    null = str1;
    if (str1 == null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("fieldCount=");
      stringBuilder1.append(l);
      null = stringBuilder1.toString();
    } 
    str1 = null;
    if (null.length() > 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(null);
      stringBuilder1.append(" ");
      str1 = stringBuilder1.toString();
    } 
    l = 0xFF00000000L & paramLong;
    String str2 = getFieldTypeString(l);
    null = str2;
    if (str2 == null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("fieldType=");
      stringBuilder1.append(l);
      null = stringBuilder1.toString();
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str1);
    stringBuilder.append(null);
    stringBuilder.append(" tag=");
    stringBuilder.append((int)paramLong);
    stringBuilder.append(" fieldId=0x");
    stringBuilder.append(Long.toHexString(paramLong));
    return stringBuilder.toString();
  }
  
  public static long makeFieldId(int paramInt, long paramLong) {
    return paramInt & 0xFFFFFFFFL | paramLong;
  }
  
  public static long makeToken(int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3, int paramInt4) {
    long l2, l1 = paramInt1;
    if (paramBoolean) {
      l2 = 1152921504606846976L;
    } else {
      l2 = 0L;
    } 
    long l3 = paramInt2, l4 = paramInt3, l5 = paramInt4;
    return (l1 & 0x7L) << 61L | l2 | (0x1FFL & l3) << 51L | (0x7FFFFL & l4) << 32L | 0xFFFFFFFFL & l5;
  }
  
  public static int getTagSizeFromToken(long paramLong) {
    return (int)(paramLong >> 61L & 0x7L);
  }
  
  public static boolean getRepeatedFromToken(long paramLong) {
    boolean bool;
    if ((paramLong >> 60L & 0x1L) != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static int getDepthFromToken(long paramLong) {
    return (int)(paramLong >> 51L & 0x1FFL);
  }
  
  public static int getObjectIdFromToken(long paramLong) {
    return (int)(paramLong >> 32L & 0x7FFFFL);
  }
  
  public static int getOffsetFromToken(long paramLong) {
    return (int)paramLong;
  }
  
  public static int convertObjectIdToOrdinal(int paramInt) {
    return 524287 - paramInt;
  }
  
  public static String token2String(long paramLong) {
    if (paramLong == 0L)
      return "Token(0)"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Token(val=0x");
    stringBuilder.append(Long.toHexString(paramLong));
    stringBuilder.append(" depth=");
    stringBuilder.append(getDepthFromToken(paramLong));
    stringBuilder.append(" object=");
    stringBuilder.append(convertObjectIdToOrdinal(getObjectIdFromToken(paramLong)));
    stringBuilder.append(" tagSize=");
    stringBuilder.append(getTagSizeFromToken(paramLong));
    stringBuilder.append(" offset=");
    stringBuilder.append(getOffsetFromToken(paramLong));
    stringBuilder.append(')');
    return stringBuilder.toString();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface FieldCount {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface FieldType {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface WireType {}
}
