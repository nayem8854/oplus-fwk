package android.util.proto;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public final class ProtoInputStream extends ProtoStream {
  private byte mState = 0;
  
  private ArrayList<Long> mExpectedObjectTokenStack = null;
  
  private int mDepth = -1;
  
  private int mDiscardedBytes = 0;
  
  private int mOffset = 0;
  
  private int mEnd = 0;
  
  private int mPackedEnd = 0;
  
  private static final int DEFAULT_BUFFER_SIZE = 8192;
  
  public static final int NO_MORE_FIELDS = -1;
  
  private static final byte STATE_FIELD_MISS = 4;
  
  private static final byte STATE_READING_PACKED = 2;
  
  private static final byte STATE_STARTED_FIELD_READ = 1;
  
  private byte[] mBuffer;
  
  private final int mBufferSize;
  
  private int mFieldNumber;
  
  private InputStream mStream;
  
  private int mWireType;
  
  public ProtoInputStream(InputStream paramInputStream, int paramInt) {
    this.mStream = paramInputStream;
    if (paramInt > 0) {
      this.mBufferSize = paramInt;
    } else {
      this.mBufferSize = 8192;
    } 
    this.mBuffer = new byte[this.mBufferSize];
  }
  
  public ProtoInputStream(InputStream paramInputStream) {
    this(paramInputStream, 8192);
  }
  
  public ProtoInputStream(byte[] paramArrayOfbyte) {
    this.mBufferSize = paramArrayOfbyte.length;
    this.mEnd = paramArrayOfbyte.length;
    this.mBuffer = paramArrayOfbyte;
    this.mStream = null;
  }
  
  public int getFieldNumber() {
    return this.mFieldNumber;
  }
  
  public int getWireType() {
    if ((this.mState & 0x2) == 2)
      return 2; 
    return this.mWireType;
  }
  
  public int getOffset() {
    return this.mOffset + this.mDiscardedBytes;
  }
  
  public int nextField() throws IOException {
    byte b = this.mState;
    if ((b & 0x4) == 4) {
      this.mState = (byte)(b & 0xFFFFFFFB);
      return this.mFieldNumber;
    } 
    if ((b & 0x1) == 1) {
      skip();
      this.mState = (byte)(this.mState & 0xFFFFFFFE);
    } 
    if ((this.mState & 0x2) == 2) {
      if (getOffset() < this.mPackedEnd) {
        this.mState = (byte)(this.mState | 0x1);
        return this.mFieldNumber;
      } 
      if (getOffset() == this.mPackedEnd) {
        this.mState = (byte)(this.mState & 0xFFFFFFFD);
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unexpectedly reached end of packed field at offset 0x");
        int i = this.mPackedEnd;
        stringBuilder.append(Integer.toHexString(i));
        stringBuilder.append(dumpDebugData());
        throw new ProtoParseException(stringBuilder.toString());
      } 
    } 
    if (this.mDepth >= 0) {
      int i = getOffset();
      ArrayList<Long> arrayList = this.mExpectedObjectTokenStack;
      int j = this.mDepth;
      long l = ((Long)arrayList.get(j)).longValue();
      if (i == getOffsetFromToken(l)) {
        this.mFieldNumber = -1;
        return this.mFieldNumber;
      } 
    } 
    readTag();
    return this.mFieldNumber;
  }
  
  public boolean nextField(long paramLong) throws IOException {
    if (nextField() == (int)paramLong)
      return true; 
    this.mState = (byte)(this.mState | 0x4);
    return false;
  }
  
  public double readDouble(long paramLong) throws IOException {
    assertFreshData();
    assertFieldNumber(paramLong);
    checkPacked(paramLong);
    if ((int)((0xFF00000000L & paramLong) >>> 32L) == 1) {
      assertWireType(1);
      double d = Double.longBitsToDouble(readFixed64());
      this.mState = (byte)(this.mState & 0xFFFFFFFE);
      return d;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Requested field id (");
    stringBuilder.append(getFieldIdString(paramLong));
    stringBuilder.append(") cannot be read as a double");
    stringBuilder.append(dumpDebugData());
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public float readFloat(long paramLong) throws IOException {
    assertFreshData();
    assertFieldNumber(paramLong);
    checkPacked(paramLong);
    if ((int)((0xFF00000000L & paramLong) >>> 32L) == 2) {
      assertWireType(5);
      float f = Float.intBitsToFloat(readFixed32());
      this.mState = (byte)(this.mState & 0xFFFFFFFE);
      return f;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Requested field id (");
    stringBuilder.append(getFieldIdString(paramLong));
    stringBuilder.append(") is not a float");
    stringBuilder.append(dumpDebugData());
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public int readInt(long paramLong) throws IOException {
    assertFreshData();
    assertFieldNumber(paramLong);
    checkPacked(paramLong);
    int i = (int)((0xFF00000000L & paramLong) >>> 32L);
    if (i != 5)
      if (i != 7) {
        if (i != 17) {
          StringBuilder stringBuilder;
          switch (i) {
            default:
              stringBuilder = new StringBuilder();
              stringBuilder.append("Requested field id (");
              stringBuilder.append(getFieldIdString(paramLong));
              stringBuilder.append(") is not an int");
              stringBuilder.append(dumpDebugData());
              throw new IllegalArgumentException(stringBuilder.toString());
            case 15:
              assertWireType(5);
              i = readFixed32();
              this.mState = (byte)(this.mState & 0xFFFFFFFE);
              return i;
            case 13:
            case 14:
              break;
          } 
        } else {
          assertWireType(0);
          i = decodeZigZag32((int)readVarint());
          this.mState = (byte)(this.mState & 0xFFFFFFFE);
          return i;
        } 
      } else {
      
      }  
    assertWireType(0);
    i = (int)readVarint();
    this.mState = (byte)(this.mState & 0xFFFFFFFE);
    return i;
  }
  
  public long readLong(long paramLong) throws IOException {
    assertFreshData();
    assertFieldNumber(paramLong);
    checkPacked(paramLong);
    int i = (int)((0xFF00000000L & paramLong) >>> 32L);
    if (i != 3 && i != 4) {
      if (i != 6 && i != 16) {
        if (i == 18) {
          assertWireType(0);
          paramLong = decodeZigZag64(readVarint());
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Requested field id (");
          stringBuilder.append(getFieldIdString(paramLong));
          stringBuilder.append(") is not an long");
          stringBuilder.append(dumpDebugData());
          throw new IllegalArgumentException(stringBuilder.toString());
        } 
      } else {
        assertWireType(1);
        paramLong = readFixed64();
      } 
    } else {
      assertWireType(0);
      paramLong = readVarint();
    } 
    this.mState = (byte)(this.mState & 0xFFFFFFFE);
    return paramLong;
  }
  
  public boolean readBoolean(long paramLong) throws IOException {
    assertFreshData();
    assertFieldNumber(paramLong);
    checkPacked(paramLong);
    if ((int)((0xFF00000000L & paramLong) >>> 32L) == 8) {
      boolean bool = false;
      assertWireType(0);
      if (readVarint() != 0L)
        bool = true; 
      this.mState = (byte)(this.mState & 0xFFFFFFFE);
      return bool;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Requested field id (");
    stringBuilder.append(getFieldIdString(paramLong));
    stringBuilder.append(") is not an boolean");
    stringBuilder.append(dumpDebugData());
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public String readString(long paramLong) throws IOException {
    assertFreshData();
    assertFieldNumber(paramLong);
    if ((int)((0xFF00000000L & paramLong) >>> 32L) == 9) {
      assertWireType(2);
      int i = (int)readVarint();
      String str = readRawString(i);
      this.mState = (byte)(this.mState & 0xFFFFFFFE);
      return str;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Requested field id(");
    stringBuilder.append(getFieldIdString(paramLong));
    stringBuilder.append(") is not an string");
    stringBuilder.append(dumpDebugData());
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public byte[] readBytes(long paramLong) throws IOException {
    assertFreshData();
    assertFieldNumber(paramLong);
    int i = (int)((0xFF00000000L & paramLong) >>> 32L);
    if (i == 11 || i == 12) {
      assertWireType(2);
      i = (int)readVarint();
      byte[] arrayOfByte = readRawBytes(i);
      this.mState = (byte)(this.mState & 0xFFFFFFFE);
      return arrayOfByte;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Requested field type (");
    stringBuilder.append(getFieldIdString(paramLong));
    stringBuilder.append(") cannot be read as raw bytes");
    stringBuilder.append(dumpDebugData());
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public long start(long paramLong) throws IOException {
    assertFreshData();
    assertFieldNumber(paramLong);
    assertWireType(2);
    int i = (int)readVarint();
    if (this.mExpectedObjectTokenStack == null)
      this.mExpectedObjectTokenStack = new ArrayList<>(); 
    int j = this.mDepth + 1;
    if (j == this.mExpectedObjectTokenStack.size()) {
      boolean bool;
      ArrayList<Long> arrayList = this.mExpectedObjectTokenStack;
      if ((paramLong & 0x20000000000L) == 2199023255552L) {
        bool = true;
      } else {
        bool = false;
      } 
      j = this.mDepth;
      int k = (int)paramLong;
      int m = getOffset();
      arrayList.add(Long.valueOf(makeToken(0, bool, j, k, m + i)));
    } else {
      boolean bool;
      ArrayList<Long> arrayList = this.mExpectedObjectTokenStack;
      j = this.mDepth;
      if ((paramLong & 0x20000000000L) == 2199023255552L) {
        bool = true;
      } else {
        bool = false;
      } 
      int k = this.mDepth, n = (int)paramLong;
      int m = getOffset();
      arrayList.set(j, Long.valueOf(makeToken(0, bool, k, n, m + i)));
    } 
    i = this.mDepth;
    if (i > 0) {
      ArrayList<Long> arrayList = this.mExpectedObjectTokenStack;
      i = getOffsetFromToken(((Long)arrayList.get(i)).longValue());
      arrayList = this.mExpectedObjectTokenStack;
      j = this.mDepth;
      if (i > getOffsetFromToken(((Long)arrayList.get(j - 1)).longValue())) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Embedded Object (");
        ArrayList<Long> arrayList1 = this.mExpectedObjectTokenStack;
        i = this.mDepth;
        stringBuilder.append(token2String(((Long)arrayList1.get(i)).longValue()));
        stringBuilder.append(") ends after of parent Objects's (");
        arrayList1 = this.mExpectedObjectTokenStack;
        i = this.mDepth;
        stringBuilder.append(token2String(((Long)arrayList1.get(i - 1)).longValue()));
        stringBuilder.append(") end");
        stringBuilder.append(dumpDebugData());
        throw new ProtoParseException(stringBuilder.toString());
      } 
    } 
    this.mState = (byte)(this.mState & 0xFFFFFFFE);
    return ((Long)this.mExpectedObjectTokenStack.get(this.mDepth)).longValue();
  }
  
  public void end(long paramLong) {
    if (((Long)this.mExpectedObjectTokenStack.get(this.mDepth)).longValue() == paramLong) {
      if (getOffsetFromToken(((Long)this.mExpectedObjectTokenStack.get(this.mDepth)).longValue()) > getOffset())
        incOffset(getOffsetFromToken(((Long)this.mExpectedObjectTokenStack.get(this.mDepth)).longValue()) - getOffset()); 
      this.mDepth--;
      this.mState = (byte)(this.mState & 0xFFFFFFFE);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("end token ");
    stringBuilder.append(paramLong);
    stringBuilder.append(" does not match current message token ");
    ArrayList<Long> arrayList = this.mExpectedObjectTokenStack;
    int i = this.mDepth;
    stringBuilder.append(arrayList.get(i));
    stringBuilder.append(dumpDebugData());
    throw new ProtoParseException(stringBuilder.toString());
  }
  
  private void readTag() throws IOException {
    fillBuffer();
    if (this.mOffset >= this.mEnd) {
      this.mFieldNumber = -1;
      return;
    } 
    int i = (int)readVarint();
    this.mFieldNumber = i >>> 3;
    this.mWireType = i & 0x7;
    this.mState = (byte)(this.mState | 0x1);
  }
  
  public int decodeZigZag32(int paramInt) {
    return paramInt >>> 1 ^ -(paramInt & 0x1);
  }
  
  public long decodeZigZag64(long paramLong) {
    return paramLong >>> 1L ^ -(0x1L & paramLong);
  }
  
  private long readVarint() throws IOException {
    long l = 0L;
    byte b = 0;
    while (true) {
      fillBuffer();
      int i = this.mEnd - this.mOffset;
      if (i >= 0) {
        for (byte b1 = 0; b1 < i; ) {
          byte b2 = this.mBuffer[this.mOffset + b1];
          l |= (b2 & 0x7FL) << b;
          if ((b2 & 0x80) == 0) {
            incOffset(b1 + 1);
            return l;
          } 
          b += true;
          if (b <= 63) {
            b1++;
            continue;
          } 
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Varint is too large at offset 0x");
          stringBuilder1.append(Integer.toHexString(getOffset() + b1));
          stringBuilder1.append(dumpDebugData());
          throw new ProtoParseException(stringBuilder1.toString());
        } 
        incOffset(i);
        continue;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Incomplete varint at offset 0x");
      stringBuilder.append(Integer.toHexString(getOffset()));
      stringBuilder.append(dumpDebugData());
      throw new ProtoParseException(stringBuilder.toString());
    } 
  }
  
  private int readFixed32() throws IOException {
    if (this.mOffset + 4 <= this.mEnd) {
      incOffset(4);
      byte[] arrayOfByte = this.mBuffer;
      int k = this.mOffset;
      byte b1 = arrayOfByte[k - 4], b2 = arrayOfByte[k - 3], b3 = arrayOfByte[k - 2];
      return (arrayOfByte[k - 1] & 0xFF) << 24 | b1 & 0xFF | (b2 & 0xFF) << 8 | (b3 & 0xFF) << 16;
    } 
    int j = 0;
    byte b = 0;
    int i = 4;
    while (i > 0) {
      fillBuffer();
      int m = this.mEnd, k = this.mOffset;
      if (m - k < i) {
        k = m - k;
      } else {
        k = i;
      } 
      if (k >= 0) {
        incOffset(k);
        i -= k;
        while (k > 0) {
          j |= (this.mBuffer[this.mOffset - k] & 0xFF) << b;
          k--;
          b += 8;
        } 
        continue;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Incomplete fixed32 at offset 0x");
      stringBuilder.append(Integer.toHexString(getOffset()));
      stringBuilder.append(dumpDebugData());
      throw new ProtoParseException(stringBuilder.toString());
    } 
    return j;
  }
  
  private long readFixed64() throws IOException {
    if (this.mOffset + 8 <= this.mEnd) {
      incOffset(8);
      byte[] arrayOfByte = this.mBuffer;
      int j = this.mOffset;
      long l1 = arrayOfByte[j - 8], l2 = arrayOfByte[j - 7], l3 = arrayOfByte[j - 6], l4 = arrayOfByte[j - 5], l5 = arrayOfByte[j - 4], l6 = arrayOfByte[j - 3], l7 = arrayOfByte[j - 2];
      return (arrayOfByte[j - 1] & 0xFFL) << 56L | l1 & 0xFFL | (l2 & 0xFFL) << 8L | (l3 & 0xFFL) << 16L | (l4 & 0xFFL) << 24L | (l5 & 0xFFL) << 32L | (l6 & 0xFFL) << 40L | (l7 & 0xFFL) << 48L;
    } 
    long l = 0L;
    boolean bool = false;
    int i = 8;
    while (i > 0) {
      fillBuffer();
      int j = this.mEnd, k = this.mOffset;
      if (j - k < i) {
        j -= k;
      } else {
        j = i;
      } 
      if (j >= 0) {
        incOffset(j);
        i -= j;
        while (j > 0) {
          l |= (this.mBuffer[this.mOffset - j] & 0xFFL) << bool;
          j--;
          bool += true;
        } 
        continue;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Incomplete fixed64 at offset 0x");
      stringBuilder.append(Integer.toHexString(getOffset()));
      stringBuilder.append(dumpDebugData());
      throw new ProtoParseException(stringBuilder.toString());
    } 
    return l;
  }
  
  private byte[] readRawBytes(int paramInt) throws IOException {
    StringBuilder stringBuilder;
    int j;
    byte[] arrayOfByte = new byte[paramInt];
    int i = 0;
    while (true) {
      j = this.mOffset;
      int k = this.mEnd;
      if (j + paramInt - i > k) {
        int m = k - j;
        k = i;
        if (m > 0) {
          System.arraycopy(this.mBuffer, j, arrayOfByte, i, m);
          incOffset(m);
          k = i + m;
        } 
        fillBuffer();
        if (this.mOffset < this.mEnd) {
          i = k;
          continue;
        } 
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unexpectedly reached end of the InputStream at offset 0x");
        paramInt = this.mEnd;
        stringBuilder.append(Integer.toHexString(paramInt));
        stringBuilder.append(dumpDebugData());
        throw new ProtoParseException(stringBuilder.toString());
      } 
      break;
    } 
    System.arraycopy(this.mBuffer, j, stringBuilder, i, paramInt - i);
    incOffset(paramInt - i);
    return (byte[])stringBuilder;
  }
  
  private String readRawString(int paramInt) throws IOException {
    fillBuffer();
    int i = this.mOffset, j = this.mEnd;
    if (i + paramInt <= j) {
      String str = new String(this.mBuffer, this.mOffset, paramInt, StandardCharsets.UTF_8);
      incOffset(paramInt);
      return str;
    } 
    if (paramInt <= this.mBufferSize) {
      j -= i;
      byte[] arrayOfByte = this.mBuffer;
      System.arraycopy(arrayOfByte, i, arrayOfByte, 0, j);
      this.mEnd = this.mStream.read(this.mBuffer, j, paramInt - j) + j;
      this.mDiscardedBytes += this.mOffset;
      this.mOffset = 0;
      String str = new String(this.mBuffer, this.mOffset, paramInt, StandardCharsets.UTF_8);
      incOffset(paramInt);
      return str;
    } 
    return new String(readRawBytes(paramInt), 0, paramInt, StandardCharsets.UTF_8);
  }
  
  private void fillBuffer() throws IOException {
    int i = this.mOffset, j = this.mEnd;
    if (i >= j) {
      InputStream inputStream = this.mStream;
      if (inputStream != null) {
        this.mOffset = i -= j;
        this.mDiscardedBytes += j;
        j = this.mBufferSize;
        if (i >= j) {
          j = (int)inputStream.skip((i / j * j));
          this.mDiscardedBytes += j;
          this.mOffset -= j;
        } 
        this.mEnd = this.mStream.read(this.mBuffer);
      } 
    } 
  }
  
  public void skip() throws IOException {
    if ((this.mState & 0x2) == 2) {
      incOffset(this.mPackedEnd - getOffset());
    } else {
      int i = this.mWireType;
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            if (i == 5) {
              incOffset(4);
            } else {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Unexpected wire type: ");
              stringBuilder.append(this.mWireType);
              stringBuilder.append(" at offset 0x");
              i = this.mOffset;
              stringBuilder.append(Integer.toHexString(i));
              stringBuilder.append(dumpDebugData());
              throw new ProtoParseException(stringBuilder.toString());
            } 
          } else {
            fillBuffer();
            i = (int)readVarint();
            incOffset(i);
          } 
        } else {
          incOffset(8);
        } 
      } else {
        do {
          fillBuffer();
          i = this.mBuffer[this.mOffset];
          incOffset(1);
        } while ((i & 0x80) != 0);
      } 
    } 
    this.mState = (byte)(this.mState & 0xFFFFFFFE);
  }
  
  private void incOffset(int paramInt) {
    this.mOffset += paramInt;
    if (this.mDepth >= 0) {
      paramInt = getOffset();
      ArrayList<Long> arrayList = this.mExpectedObjectTokenStack;
      int i = this.mDepth;
      long l = ((Long)arrayList.get(i)).longValue();
      if (paramInt > getOffsetFromToken(l)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unexpectedly reached end of embedded object.  ");
        ArrayList<Long> arrayList1 = this.mExpectedObjectTokenStack;
        paramInt = this.mDepth;
        stringBuilder.append(token2String(((Long)arrayList1.get(paramInt)).longValue()));
        stringBuilder.append(dumpDebugData());
        throw new ProtoParseException(stringBuilder.toString());
      } 
    } 
  }
  
  private void checkPacked(long paramLong) throws IOException {
    if (this.mWireType == 2) {
      StringBuilder stringBuilder;
      int i = (int)readVarint();
      this.mPackedEnd = getOffset() + i;
      this.mState = (byte)(0x2 | this.mState);
      switch ((int)((0xFF00000000L & paramLong) >>> 32L)) {
        default:
          stringBuilder = new StringBuilder();
          stringBuilder.append("Requested field id (");
          stringBuilder.append(getFieldIdString(paramLong));
          stringBuilder.append(") is not a packable field");
          stringBuilder.append(dumpDebugData());
          throw new IllegalArgumentException(stringBuilder.toString());
        case 3:
        case 4:
        case 5:
        case 8:
        case 13:
        case 14:
        case 17:
        case 18:
          this.mWireType = 0;
          return;
        case 2:
        case 7:
        case 15:
          if (i % 4 == 0) {
            this.mWireType = 5;
          } else {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Requested field id (");
            stringBuilder.append(getFieldIdString(paramLong));
            stringBuilder.append(") packed length ");
            stringBuilder.append(i);
            stringBuilder.append(" is not aligned for fixed32");
            stringBuilder.append(dumpDebugData());
            throw new IllegalArgumentException(stringBuilder.toString());
          } 
          return;
        case 1:
        case 6:
        case 16:
          break;
      } 
      if (i % 8 == 0) {
        this.mWireType = 1;
      } else {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Requested field id (");
        stringBuilder.append(getFieldIdString(paramLong));
        stringBuilder.append(") packed length ");
        stringBuilder.append(i);
        stringBuilder.append(" is not aligned for fixed64");
        stringBuilder.append(dumpDebugData());
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
    } 
  }
  
  private void assertFieldNumber(long paramLong) {
    if ((int)paramLong == this.mFieldNumber)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Requested field id (");
    stringBuilder.append(getFieldIdString(paramLong));
    stringBuilder.append(") does not match current field number (0x");
    int i = this.mFieldNumber;
    stringBuilder.append(Integer.toHexString(i));
    stringBuilder.append(") at offset 0x");
    stringBuilder.append(Integer.toHexString(getOffset()));
    stringBuilder.append(dumpDebugData());
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private void assertWireType(int paramInt) {
    if (paramInt == this.mWireType)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Current wire type ");
    int i = this.mWireType;
    stringBuilder.append(getWireTypeString(i));
    stringBuilder.append(" does not match expected wire type ");
    stringBuilder.append(getWireTypeString(paramInt));
    stringBuilder.append(" at offset 0x");
    stringBuilder.append(Integer.toHexString(getOffset()));
    stringBuilder.append(dumpDebugData());
    throw new WireTypeMismatchException(stringBuilder.toString());
  }
  
  private void assertFreshData() {
    if ((this.mState & 0x1) == 1)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Attempting to read already read field at offset 0x");
    int i = getOffset();
    stringBuilder.append(Integer.toHexString(i));
    stringBuilder.append(dumpDebugData());
    throw new ProtoParseException(stringBuilder.toString());
  }
  
  public String dumpDebugData() {
    StringBuilder stringBuilder1 = new StringBuilder();
    StringBuilder stringBuilder4 = new StringBuilder();
    stringBuilder4.append("\nmFieldNumber : 0x");
    stringBuilder4.append(Integer.toHexString(this.mFieldNumber));
    stringBuilder1.append(stringBuilder4.toString());
    stringBuilder4 = new StringBuilder();
    stringBuilder4.append("\nmWireType : 0x");
    stringBuilder4.append(Integer.toHexString(this.mWireType));
    stringBuilder1.append(stringBuilder4.toString());
    stringBuilder4 = new StringBuilder();
    stringBuilder4.append("\nmState : 0x");
    stringBuilder4.append(Integer.toHexString(this.mState));
    stringBuilder1.append(stringBuilder4.toString());
    stringBuilder4 = new StringBuilder();
    stringBuilder4.append("\nmDiscardedBytes : 0x");
    stringBuilder4.append(Integer.toHexString(this.mDiscardedBytes));
    stringBuilder1.append(stringBuilder4.toString());
    stringBuilder4 = new StringBuilder();
    stringBuilder4.append("\nmOffset : 0x");
    stringBuilder4.append(Integer.toHexString(this.mOffset));
    stringBuilder1.append(stringBuilder4.toString());
    stringBuilder1.append("\nmExpectedObjectTokenStack : ");
    ArrayList<Long> arrayList = this.mExpectedObjectTokenStack;
    if (arrayList == null) {
      stringBuilder1.append("null");
    } else {
      stringBuilder1.append(arrayList);
    } 
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append("\nmDepth : 0x");
    stringBuilder3.append(Integer.toHexString(this.mDepth));
    stringBuilder1.append(stringBuilder3.toString());
    stringBuilder1.append("\nmBuffer : ");
    byte[] arrayOfByte = this.mBuffer;
    if (arrayOfByte == null) {
      stringBuilder1.append("null");
    } else {
      stringBuilder1.append(arrayOfByte);
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nmBufferSize : 0x");
    stringBuilder2.append(Integer.toHexString(this.mBufferSize));
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nmEnd : 0x");
    stringBuilder2.append(Integer.toHexString(this.mEnd));
    stringBuilder1.append(stringBuilder2.toString());
    return stringBuilder1.toString();
  }
}
