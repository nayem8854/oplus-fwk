package android.util.proto;

import android.util.Log;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

public final class ProtoOutputStream extends ProtoStream {
  public static final String TAG = "ProtoOutputStream";
  
  private EncodedBuffer mBuffer;
  
  private boolean mCompacted;
  
  private int mCopyBegin;
  
  private int mDepth;
  
  private long mExpectedObjectToken;
  
  private int mNextObjectId = -1;
  
  private OutputStream mStream;
  
  public ProtoOutputStream() {
    this(0);
  }
  
  public ProtoOutputStream(int paramInt) {
    this.mBuffer = new EncodedBuffer(paramInt);
  }
  
  public ProtoOutputStream(OutputStream paramOutputStream) {
    this();
    this.mStream = paramOutputStream;
  }
  
  public ProtoOutputStream(FileDescriptor paramFileDescriptor) {
    this(new FileOutputStream(paramFileDescriptor));
  }
  
  public int getRawSize() {
    if (this.mCompacted)
      return (getBytes()).length; 
    return this.mBuffer.getSize();
  }
  
  public void write(long paramLong, double paramDouble) {
    StringBuilder stringBuilder;
    assertNotCompacted();
    int i = (int)paramLong;
    int j = (int)((0xFFF00000000L & paramLong) >> 32L);
    boolean bool1 = true, bool2 = true;
    switch (j) {
      default:
        switch (j) {
          default:
            switch (j) {
              default:
                switch (j) {
                  default:
                    switch (j) {
                      default:
                        switch (j) {
                          default:
                            stringBuilder = new StringBuilder();
                            stringBuilder.append("Attempt to call write(long, double) with ");
                            stringBuilder.append(getFieldIdString(paramLong));
                            throw new IllegalArgumentException(stringBuilder.toString());
                          case 1298:
                            writeRepeatedSInt64Impl(i, (long)paramDouble);
                            return;
                          case 1297:
                            writeRepeatedSInt32Impl(i, (int)paramDouble);
                            return;
                          case 1296:
                            writeRepeatedSFixed64Impl(i, (long)paramDouble);
                            return;
                          case 1295:
                            writeRepeatedSFixed32Impl(i, (int)paramDouble);
                            return;
                          case 1294:
                            writeRepeatedEnumImpl(i, (int)paramDouble);
                            return;
                          case 1293:
                            break;
                        } 
                      case 1288:
                        if (paramDouble == 0.0D)
                          bool2 = false; 
                        writeRepeatedBoolImpl(i, bool2);
                        return;
                      case 1287:
                        writeRepeatedFixed32Impl(i, (int)paramDouble);
                        return;
                      case 1286:
                        writeRepeatedFixed64Impl(i, (long)paramDouble);
                        return;
                      case 1285:
                        writeRepeatedInt32Impl(i, (int)paramDouble);
                        return;
                      case 1284:
                        writeRepeatedUInt64Impl(i, (long)paramDouble);
                        return;
                      case 1283:
                        writeRepeatedInt64Impl(i, (long)paramDouble);
                        return;
                      case 1282:
                      case 1281:
                        break;
                    } 
                    break;
                  case 530:
                  case 529:
                  case 528:
                  case 527:
                  case 526:
                  
                  case 525:
                    writeRepeatedUInt32Impl(i, (int)paramDouble);
                    return;
                } 
              case 520:
              case 519:
              case 518:
              case 517:
              case 516:
              case 515:
              
              case 514:
                writeRepeatedFloatImpl(i, (float)paramDouble);
                return;
              case 513:
                break;
            } 
            writeRepeatedDoubleImpl(i, paramDouble);
            return;
          case 274:
            writeSInt64Impl(i, (long)paramDouble);
            return;
          case 273:
            writeSInt32Impl(i, (int)paramDouble);
            return;
          case 272:
            writeSFixed64Impl(i, (long)paramDouble);
            return;
          case 271:
            writeSFixed32Impl(i, (int)paramDouble);
            return;
          case 270:
            writeEnumImpl(i, (int)paramDouble);
            return;
          case 269:
            break;
        } 
        writeUInt32Impl(i, (int)paramDouble);
        return;
      case 264:
        if (paramDouble != 0.0D) {
          bool2 = bool1;
        } else {
          bool2 = false;
        } 
        writeBoolImpl(i, bool2);
        return;
      case 263:
        writeFixed32Impl(i, (int)paramDouble);
        return;
      case 262:
        writeFixed64Impl(i, (long)paramDouble);
        return;
      case 261:
        writeInt32Impl(i, (int)paramDouble);
        return;
      case 260:
        writeUInt64Impl(i, (long)paramDouble);
        return;
      case 259:
        writeInt64Impl(i, (long)paramDouble);
        return;
      case 258:
        writeFloatImpl(i, (float)paramDouble);
        return;
      case 257:
        break;
    } 
    writeDoubleImpl(i, paramDouble);
  }
  
  public void write(long paramLong, float paramFloat) {
    StringBuilder stringBuilder;
    assertNotCompacted();
    int i = (int)paramLong;
    int j = (int)((0xFFF00000000L & paramLong) >> 32L);
    boolean bool1 = true, bool2 = true;
    switch (j) {
      default:
        switch (j) {
          default:
            switch (j) {
              default:
                switch (j) {
                  default:
                    switch (j) {
                      default:
                        switch (j) {
                          default:
                            stringBuilder = new StringBuilder();
                            stringBuilder.append("Attempt to call write(long, float) with ");
                            stringBuilder.append(getFieldIdString(paramLong));
                            throw new IllegalArgumentException(stringBuilder.toString());
                          case 1298:
                            writeRepeatedSInt64Impl(i, (long)paramFloat);
                            return;
                          case 1297:
                            writeRepeatedSInt32Impl(i, (int)paramFloat);
                            return;
                          case 1296:
                            writeRepeatedSFixed64Impl(i, (long)paramFloat);
                            return;
                          case 1295:
                            writeRepeatedSFixed32Impl(i, (int)paramFloat);
                            return;
                          case 1294:
                            writeRepeatedEnumImpl(i, (int)paramFloat);
                            return;
                          case 1293:
                            break;
                        } 
                      case 1288:
                        if (paramFloat != 0.0F) {
                          bool1 = bool2;
                        } else {
                          bool1 = false;
                        } 
                        writeRepeatedBoolImpl(i, bool1);
                        return;
                      case 1287:
                        writeRepeatedFixed32Impl(i, (int)paramFloat);
                        return;
                      case 1286:
                        writeRepeatedFixed64Impl(i, (long)paramFloat);
                        return;
                      case 1285:
                        writeRepeatedInt32Impl(i, (int)paramFloat);
                        return;
                      case 1284:
                        writeRepeatedUInt64Impl(i, (long)paramFloat);
                        return;
                      case 1283:
                        writeRepeatedInt64Impl(i, (long)paramFloat);
                        return;
                      case 1282:
                      case 1281:
                        break;
                    } 
                    break;
                  case 530:
                  case 529:
                  case 528:
                  case 527:
                  case 526:
                  
                  case 525:
                    writeRepeatedUInt32Impl(i, (int)paramFloat);
                    return;
                } 
              case 520:
              case 519:
              case 518:
              case 517:
              case 516:
              case 515:
              
              case 514:
                writeRepeatedFloatImpl(i, paramFloat);
                return;
              case 513:
                break;
            } 
            writeRepeatedDoubleImpl(i, paramFloat);
            return;
          case 274:
            writeSInt64Impl(i, (long)paramFloat);
            return;
          case 273:
            writeSInt32Impl(i, (int)paramFloat);
            return;
          case 272:
            writeSFixed64Impl(i, (long)paramFloat);
            return;
          case 271:
            writeSFixed32Impl(i, (int)paramFloat);
            return;
          case 270:
            writeEnumImpl(i, (int)paramFloat);
            return;
          case 269:
            break;
        } 
        writeUInt32Impl(i, (int)paramFloat);
        return;
      case 264:
        if (paramFloat == 0.0F)
          bool1 = false; 
        writeBoolImpl(i, bool1);
        return;
      case 263:
        writeFixed32Impl(i, (int)paramFloat);
        return;
      case 262:
        writeFixed64Impl(i, (long)paramFloat);
        return;
      case 261:
        writeInt32Impl(i, (int)paramFloat);
        return;
      case 260:
        writeUInt64Impl(i, (long)paramFloat);
        return;
      case 259:
        writeInt64Impl(i, (long)paramFloat);
        return;
      case 258:
        writeFloatImpl(i, paramFloat);
        return;
      case 257:
        break;
    } 
    writeDoubleImpl(i, paramFloat);
  }
  
  public void write(long paramLong, int paramInt) {
    StringBuilder stringBuilder;
    assertNotCompacted();
    int i = (int)paramLong;
    int j = (int)((0xFFF00000000L & paramLong) >> 32L);
    boolean bool1 = true, bool2 = true;
    switch (j) {
      default:
        switch (j) {
          default:
            switch (j) {
              default:
                switch (j) {
                  default:
                    switch (j) {
                      default:
                        switch (j) {
                          default:
                            stringBuilder = new StringBuilder();
                            stringBuilder.append("Attempt to call write(long, int) with ");
                            stringBuilder.append(getFieldIdString(paramLong));
                            throw new IllegalArgumentException(stringBuilder.toString());
                          case 1298:
                            writeRepeatedSInt64Impl(i, paramInt);
                            return;
                          case 1297:
                            writeRepeatedSInt32Impl(i, paramInt);
                            return;
                          case 1296:
                            writeRepeatedSFixed64Impl(i, paramInt);
                            return;
                          case 1295:
                            writeRepeatedSFixed32Impl(i, paramInt);
                            return;
                          case 1294:
                            writeRepeatedEnumImpl(i, paramInt);
                            return;
                          case 1293:
                            break;
                        } 
                      case 1288:
                        if (paramInt == 0)
                          bool2 = false; 
                        writeRepeatedBoolImpl(i, bool2);
                        return;
                      case 1287:
                        writeRepeatedFixed32Impl(i, paramInt);
                        return;
                      case 1286:
                        writeRepeatedFixed64Impl(i, paramInt);
                        return;
                      case 1285:
                        writeRepeatedInt32Impl(i, paramInt);
                        return;
                      case 1284:
                        writeRepeatedUInt64Impl(i, paramInt);
                        return;
                      case 1283:
                        writeRepeatedInt64Impl(i, paramInt);
                        return;
                      case 1282:
                      case 1281:
                        break;
                    } 
                    break;
                  case 530:
                  case 529:
                  case 528:
                  case 527:
                  case 526:
                  
                  case 525:
                    writeRepeatedUInt32Impl(i, paramInt);
                    return;
                } 
              case 520:
              case 519:
              case 518:
              case 517:
              case 516:
              case 515:
              
              case 514:
                writeRepeatedFloatImpl(i, paramInt);
                return;
              case 513:
                break;
            } 
            writeRepeatedDoubleImpl(i, paramInt);
            return;
          case 274:
            writeSInt64Impl(i, paramInt);
            return;
          case 273:
            writeSInt32Impl(i, paramInt);
            return;
          case 272:
            writeSFixed64Impl(i, paramInt);
            return;
          case 271:
            writeSFixed32Impl(i, paramInt);
            return;
          case 270:
            writeEnumImpl(i, paramInt);
            return;
          case 269:
            break;
        } 
        writeUInt32Impl(i, paramInt);
        return;
      case 264:
        if (paramInt != 0) {
          bool2 = bool1;
        } else {
          bool2 = false;
        } 
        writeBoolImpl(i, bool2);
        return;
      case 263:
        writeFixed32Impl(i, paramInt);
        return;
      case 262:
        writeFixed64Impl(i, paramInt);
        return;
      case 261:
        writeInt32Impl(i, paramInt);
        return;
      case 260:
        writeUInt64Impl(i, paramInt);
        return;
      case 259:
        writeInt64Impl(i, paramInt);
        return;
      case 258:
        writeFloatImpl(i, paramInt);
        return;
      case 257:
        break;
    } 
    writeDoubleImpl(i, paramInt);
  }
  
  public void write(long paramLong1, long paramLong2) {
    StringBuilder stringBuilder;
    assertNotCompacted();
    int i = (int)paramLong1;
    int j = (int)((0xFFF00000000L & paramLong1) >> 32L);
    boolean bool1 = true, bool2 = true;
    switch (j) {
      default:
        switch (j) {
          default:
            switch (j) {
              default:
                switch (j) {
                  default:
                    switch (j) {
                      default:
                        switch (j) {
                          default:
                            stringBuilder = new StringBuilder();
                            stringBuilder.append("Attempt to call write(long, long) with ");
                            stringBuilder.append(getFieldIdString(paramLong1));
                            throw new IllegalArgumentException(stringBuilder.toString());
                          case 1298:
                            writeRepeatedSInt64Impl(i, paramLong2);
                            return;
                          case 1297:
                            writeRepeatedSInt32Impl(i, (int)paramLong2);
                            return;
                          case 1296:
                            writeRepeatedSFixed64Impl(i, paramLong2);
                            return;
                          case 1295:
                            writeRepeatedSFixed32Impl(i, (int)paramLong2);
                            return;
                          case 1294:
                            writeRepeatedEnumImpl(i, (int)paramLong2);
                            return;
                          case 1293:
                            break;
                        } 
                      case 1288:
                        if (paramLong2 == 0L)
                          bool2 = false; 
                        writeRepeatedBoolImpl(i, bool2);
                        return;
                      case 1287:
                        writeRepeatedFixed32Impl(i, (int)paramLong2);
                        return;
                      case 1286:
                        writeRepeatedFixed64Impl(i, paramLong2);
                        return;
                      case 1285:
                        writeRepeatedInt32Impl(i, (int)paramLong2);
                        return;
                      case 1284:
                        writeRepeatedUInt64Impl(i, paramLong2);
                        return;
                      case 1283:
                        writeRepeatedInt64Impl(i, paramLong2);
                        return;
                      case 1282:
                      case 1281:
                        break;
                    } 
                    break;
                  case 530:
                  case 529:
                  case 528:
                  case 527:
                  case 526:
                  
                  case 525:
                    writeRepeatedUInt32Impl(i, (int)paramLong2);
                    return;
                } 
              case 520:
              case 519:
              case 518:
              case 517:
              case 516:
              case 515:
              
              case 514:
                writeRepeatedFloatImpl(i, (float)paramLong2);
                return;
              case 513:
                break;
            } 
            writeRepeatedDoubleImpl(i, paramLong2);
            return;
          case 274:
            writeSInt64Impl(i, paramLong2);
            return;
          case 273:
            writeSInt32Impl(i, (int)paramLong2);
            return;
          case 272:
            writeSFixed64Impl(i, paramLong2);
            return;
          case 271:
            writeSFixed32Impl(i, (int)paramLong2);
            return;
          case 270:
            writeEnumImpl(i, (int)paramLong2);
            return;
          case 269:
            break;
        } 
        writeUInt32Impl(i, (int)paramLong2);
        return;
      case 264:
        if (paramLong2 != 0L) {
          bool2 = bool1;
        } else {
          bool2 = false;
        } 
        writeBoolImpl(i, bool2);
        return;
      case 263:
        writeFixed32Impl(i, (int)paramLong2);
        return;
      case 262:
        writeFixed64Impl(i, paramLong2);
        return;
      case 261:
        writeInt32Impl(i, (int)paramLong2);
        return;
      case 260:
        writeUInt64Impl(i, paramLong2);
        return;
      case 259:
        writeInt64Impl(i, paramLong2);
        return;
      case 258:
        writeFloatImpl(i, (float)paramLong2);
        return;
      case 257:
        break;
    } 
    writeDoubleImpl(i, paramLong2);
  }
  
  public void write(long paramLong, boolean paramBoolean) {
    assertNotCompacted();
    int i = (int)paramLong;
    int j = (int)((0xFFF00000000L & paramLong) >> 32L);
    if (j != 264) {
      if (j == 520 || j == 1288) {
        writeRepeatedBoolImpl(i, paramBoolean);
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Attempt to call write(long, boolean) with ");
      stringBuilder.append(getFieldIdString(paramLong));
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    writeBoolImpl(i, paramBoolean);
  }
  
  public void write(long paramLong, String paramString) {
    StringBuilder stringBuilder;
    assertNotCompacted();
    int i = (int)paramLong;
    int j = (int)((0xFFF00000000L & paramLong) >> 32L);
    if (j != 265) {
      if (j == 521 || j == 1289) {
        writeRepeatedStringImpl(i, paramString);
        return;
      } 
      stringBuilder = new StringBuilder();
      stringBuilder.append("Attempt to call write(long, String) with ");
      stringBuilder.append(getFieldIdString(paramLong));
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    writeStringImpl(i, (String)stringBuilder);
  }
  
  public void write(long paramLong, byte[] paramArrayOfbyte) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial assertNotCompacted : ()V
    //   4: lload_1
    //   5: l2i
    //   6: istore #4
    //   8: ldc2_w 17587891077120
    //   11: lload_1
    //   12: land
    //   13: bipush #32
    //   15: lshr
    //   16: l2i
    //   17: istore #5
    //   19: iload #5
    //   21: sipush #267
    //   24: if_icmpeq -> 137
    //   27: iload #5
    //   29: sipush #268
    //   32: if_icmpeq -> 127
    //   35: iload #5
    //   37: sipush #523
    //   40: if_icmpeq -> 117
    //   43: iload #5
    //   45: sipush #524
    //   48: if_icmpeq -> 107
    //   51: iload #5
    //   53: sipush #1291
    //   56: if_icmpeq -> 117
    //   59: iload #5
    //   61: sipush #1292
    //   64: if_icmpne -> 70
    //   67: goto -> 107
    //   70: new java/lang/StringBuilder
    //   73: dup
    //   74: invokespecial <init> : ()V
    //   77: astore_3
    //   78: aload_3
    //   79: ldc_w 'Attempt to call write(long, byte[]) with '
    //   82: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   85: pop
    //   86: aload_3
    //   87: lload_1
    //   88: invokestatic getFieldIdString : (J)Ljava/lang/String;
    //   91: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   94: pop
    //   95: new java/lang/IllegalArgumentException
    //   98: dup
    //   99: aload_3
    //   100: invokevirtual toString : ()Ljava/lang/String;
    //   103: invokespecial <init> : (Ljava/lang/String;)V
    //   106: athrow
    //   107: aload_0
    //   108: iload #4
    //   110: aload_3
    //   111: invokespecial writeRepeatedBytesImpl : (I[B)V
    //   114: goto -> 144
    //   117: aload_0
    //   118: iload #4
    //   120: aload_3
    //   121: invokevirtual writeRepeatedObjectImpl : (I[B)V
    //   124: goto -> 144
    //   127: aload_0
    //   128: iload #4
    //   130: aload_3
    //   131: invokespecial writeBytesImpl : (I[B)V
    //   134: goto -> 144
    //   137: aload_0
    //   138: iload #4
    //   140: aload_3
    //   141: invokevirtual writeObjectImpl : (I[B)V
    //   144: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #824	-> 0
    //   #825	-> 4
    //   #827	-> 8
    //   #846	-> 70
    //   #847	-> 86
    //   #834	-> 107
    //   #835	-> 114
    //   #842	-> 117
    //   #843	-> 124
    //   #830	-> 127
    //   #831	-> 134
    //   #838	-> 137
    //   #839	-> 144
    //   #850	-> 144
  }
  
  public long start(long paramLong) {
    assertNotCompacted();
    int i = (int)paramLong;
    if ((0xFF00000000L & paramLong) == 47244640256L) {
      long l = 0xF0000000000L & paramLong;
      if (l == 1099511627776L)
        return startObjectImpl(i, false); 
      if (l == 2199023255552L || l == 5497558138880L)
        return startObjectImpl(i, true); 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Attempt to call start(long) with ");
    stringBuilder.append(getFieldIdString(paramLong));
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void end(long paramLong) {
    endObjectImpl(paramLong, getRepeatedFromToken(paramLong));
  }
  
  @Deprecated
  public void writeDouble(long paramLong, double paramDouble) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 1103806595072L);
    writeDoubleImpl(i, paramDouble);
  }
  
  private void writeDoubleImpl(int paramInt, double paramDouble) {
    if (paramDouble != 0.0D) {
      writeTag(paramInt, 1);
      this.mBuffer.writeRawFixed64(Double.doubleToLongBits(paramDouble));
    } 
  }
  
  @Deprecated
  public void writeRepeatedDouble(long paramLong, double paramDouble) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 2203318222848L);
    writeRepeatedDoubleImpl(i, paramDouble);
  }
  
  private void writeRepeatedDoubleImpl(int paramInt, double paramDouble) {
    writeTag(paramInt, 1);
    this.mBuffer.writeRawFixed64(Double.doubleToLongBits(paramDouble));
  }
  
  @Deprecated
  public void writePackedDouble(long paramLong, double[] paramArrayOfdouble) {
    byte b;
    assertNotCompacted();
    int i = checkFieldId(paramLong, 5501853106176L);
    if (paramArrayOfdouble != null) {
      b = paramArrayOfdouble.length;
    } else {
      b = 0;
    } 
    if (b) {
      writeKnownLengthHeader(i, b * 8);
      for (i = 0; i < b; i++)
        this.mBuffer.writeRawFixed64(Double.doubleToLongBits(paramArrayOfdouble[i])); 
    } 
  }
  
  @Deprecated
  public void writeFloat(long paramLong, float paramFloat) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 1108101562368L);
    writeFloatImpl(i, paramFloat);
  }
  
  private void writeFloatImpl(int paramInt, float paramFloat) {
    if (paramFloat != 0.0F) {
      writeTag(paramInt, 5);
      this.mBuffer.writeRawFixed32(Float.floatToIntBits(paramFloat));
    } 
  }
  
  @Deprecated
  public void writeRepeatedFloat(long paramLong, float paramFloat) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 2207613190144L);
    writeRepeatedFloatImpl(i, paramFloat);
  }
  
  private void writeRepeatedFloatImpl(int paramInt, float paramFloat) {
    writeTag(paramInt, 5);
    this.mBuffer.writeRawFixed32(Float.floatToIntBits(paramFloat));
  }
  
  @Deprecated
  public void writePackedFloat(long paramLong, float[] paramArrayOffloat) {
    byte b;
    assertNotCompacted();
    int i = checkFieldId(paramLong, 5506148073472L);
    if (paramArrayOffloat != null) {
      b = paramArrayOffloat.length;
    } else {
      b = 0;
    } 
    if (b) {
      writeKnownLengthHeader(i, b * 4);
      for (i = 0; i < b; i++)
        this.mBuffer.writeRawFixed32(Float.floatToIntBits(paramArrayOffloat[i])); 
    } 
  }
  
  private void writeUnsignedVarintFromSignedInt(int paramInt) {
    if (paramInt >= 0) {
      this.mBuffer.writeRawVarint32(paramInt);
    } else {
      this.mBuffer.writeRawVarint64(paramInt);
    } 
  }
  
  @Deprecated
  public void writeInt32(long paramLong, int paramInt) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 1120986464256L);
    writeInt32Impl(i, paramInt);
  }
  
  private void writeInt32Impl(int paramInt1, int paramInt2) {
    if (paramInt2 != 0) {
      writeTag(paramInt1, 0);
      writeUnsignedVarintFromSignedInt(paramInt2);
    } 
  }
  
  @Deprecated
  public void writeRepeatedInt32(long paramLong, int paramInt) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 2220498092032L);
    writeRepeatedInt32Impl(i, paramInt);
  }
  
  private void writeRepeatedInt32Impl(int paramInt1, int paramInt2) {
    writeTag(paramInt1, 0);
    writeUnsignedVarintFromSignedInt(paramInt2);
  }
  
  @Deprecated
  public void writePackedInt32(long paramLong, int[] paramArrayOfint) {
    byte b;
    assertNotCompacted();
    int i = checkFieldId(paramLong, 5519032975360L);
    if (paramArrayOfint != null) {
      b = paramArrayOfint.length;
    } else {
      b = 0;
    } 
    if (b) {
      int j = 0;
      byte b1;
      for (b1 = 0; b1 < b; b1++) {
        int k = paramArrayOfint[b1];
        if (k >= 0) {
          k = EncodedBuffer.getRawVarint32Size(k);
        } else {
          k = 10;
        } 
        j += k;
      } 
      writeKnownLengthHeader(i, j);
      for (b1 = 0; b1 < b; b1++)
        writeUnsignedVarintFromSignedInt(paramArrayOfint[b1]); 
    } 
  }
  
  @Deprecated
  public void writeInt64(long paramLong1, long paramLong2) {
    assertNotCompacted();
    int i = checkFieldId(paramLong1, 1112396529664L);
    writeInt64Impl(i, paramLong2);
  }
  
  private void writeInt64Impl(int paramInt, long paramLong) {
    if (paramLong != 0L) {
      writeTag(paramInt, 0);
      this.mBuffer.writeRawVarint64(paramLong);
    } 
  }
  
  @Deprecated
  public void writeRepeatedInt64(long paramLong1, long paramLong2) {
    assertNotCompacted();
    int i = checkFieldId(paramLong1, 2211908157440L);
    writeRepeatedInt64Impl(i, paramLong2);
  }
  
  private void writeRepeatedInt64Impl(int paramInt, long paramLong) {
    writeTag(paramInt, 0);
    this.mBuffer.writeRawVarint64(paramLong);
  }
  
  @Deprecated
  public void writePackedInt64(long paramLong, long[] paramArrayOflong) {
    byte b;
    assertNotCompacted();
    int i = checkFieldId(paramLong, 5510443040768L);
    if (paramArrayOflong != null) {
      b = paramArrayOflong.length;
    } else {
      b = 0;
    } 
    if (b) {
      int j = 0;
      byte b1;
      for (b1 = 0; b1 < b; b1++)
        j += EncodedBuffer.getRawVarint64Size(paramArrayOflong[b1]); 
      writeKnownLengthHeader(i, j);
      for (b1 = 0; b1 < b; b1++)
        this.mBuffer.writeRawVarint64(paramArrayOflong[b1]); 
    } 
  }
  
  @Deprecated
  public void writeUInt32(long paramLong, int paramInt) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 1155346202624L);
    writeUInt32Impl(i, paramInt);
  }
  
  private void writeUInt32Impl(int paramInt1, int paramInt2) {
    if (paramInt2 != 0) {
      writeTag(paramInt1, 0);
      this.mBuffer.writeRawVarint32(paramInt2);
    } 
  }
  
  @Deprecated
  public void writeRepeatedUInt32(long paramLong, int paramInt) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 2254857830400L);
    writeRepeatedUInt32Impl(i, paramInt);
  }
  
  private void writeRepeatedUInt32Impl(int paramInt1, int paramInt2) {
    writeTag(paramInt1, 0);
    this.mBuffer.writeRawVarint32(paramInt2);
  }
  
  @Deprecated
  public void writePackedUInt32(long paramLong, int[] paramArrayOfint) {
    byte b;
    assertNotCompacted();
    int i = checkFieldId(paramLong, 5553392713728L);
    if (paramArrayOfint != null) {
      b = paramArrayOfint.length;
    } else {
      b = 0;
    } 
    if (b) {
      int j = 0;
      byte b1;
      for (b1 = 0; b1 < b; b1++)
        j += EncodedBuffer.getRawVarint32Size(paramArrayOfint[b1]); 
      writeKnownLengthHeader(i, j);
      for (b1 = 0; b1 < b; b1++)
        this.mBuffer.writeRawVarint32(paramArrayOfint[b1]); 
    } 
  }
  
  @Deprecated
  public void writeUInt64(long paramLong1, long paramLong2) {
    assertNotCompacted();
    int i = checkFieldId(paramLong1, 1116691496960L);
    writeUInt64Impl(i, paramLong2);
  }
  
  private void writeUInt64Impl(int paramInt, long paramLong) {
    if (paramLong != 0L) {
      writeTag(paramInt, 0);
      this.mBuffer.writeRawVarint64(paramLong);
    } 
  }
  
  @Deprecated
  public void writeRepeatedUInt64(long paramLong1, long paramLong2) {
    assertNotCompacted();
    int i = checkFieldId(paramLong1, 2216203124736L);
    writeRepeatedUInt64Impl(i, paramLong2);
  }
  
  private void writeRepeatedUInt64Impl(int paramInt, long paramLong) {
    writeTag(paramInt, 0);
    this.mBuffer.writeRawVarint64(paramLong);
  }
  
  @Deprecated
  public void writePackedUInt64(long paramLong, long[] paramArrayOflong) {
    byte b;
    assertNotCompacted();
    int i = checkFieldId(paramLong, 5514738008064L);
    if (paramArrayOflong != null) {
      b = paramArrayOflong.length;
    } else {
      b = 0;
    } 
    if (b) {
      int j = 0;
      byte b1;
      for (b1 = 0; b1 < b; b1++)
        j += EncodedBuffer.getRawVarint64Size(paramArrayOflong[b1]); 
      writeKnownLengthHeader(i, j);
      for (b1 = 0; b1 < b; b1++)
        this.mBuffer.writeRawVarint64(paramArrayOflong[b1]); 
    } 
  }
  
  @Deprecated
  public void writeSInt32(long paramLong, int paramInt) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 1172526071808L);
    writeSInt32Impl(i, paramInt);
  }
  
  private void writeSInt32Impl(int paramInt1, int paramInt2) {
    if (paramInt2 != 0) {
      writeTag(paramInt1, 0);
      this.mBuffer.writeRawZigZag32(paramInt2);
    } 
  }
  
  @Deprecated
  public void writeRepeatedSInt32(long paramLong, int paramInt) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 2272037699584L);
    writeRepeatedSInt32Impl(i, paramInt);
  }
  
  private void writeRepeatedSInt32Impl(int paramInt1, int paramInt2) {
    writeTag(paramInt1, 0);
    this.mBuffer.writeRawZigZag32(paramInt2);
  }
  
  @Deprecated
  public void writePackedSInt32(long paramLong, int[] paramArrayOfint) {
    byte b;
    assertNotCompacted();
    int i = checkFieldId(paramLong, 5570572582912L);
    if (paramArrayOfint != null) {
      b = paramArrayOfint.length;
    } else {
      b = 0;
    } 
    if (b) {
      int j = 0;
      byte b1;
      for (b1 = 0; b1 < b; b1++)
        j += EncodedBuffer.getRawZigZag32Size(paramArrayOfint[b1]); 
      writeKnownLengthHeader(i, j);
      for (b1 = 0; b1 < b; b1++)
        this.mBuffer.writeRawZigZag32(paramArrayOfint[b1]); 
    } 
  }
  
  @Deprecated
  public void writeSInt64(long paramLong1, long paramLong2) {
    assertNotCompacted();
    int i = checkFieldId(paramLong1, 1176821039104L);
    writeSInt64Impl(i, paramLong2);
  }
  
  private void writeSInt64Impl(int paramInt, long paramLong) {
    if (paramLong != 0L) {
      writeTag(paramInt, 0);
      this.mBuffer.writeRawZigZag64(paramLong);
    } 
  }
  
  @Deprecated
  public void writeRepeatedSInt64(long paramLong1, long paramLong2) {
    assertNotCompacted();
    int i = checkFieldId(paramLong1, 2276332666880L);
    writeRepeatedSInt64Impl(i, paramLong2);
  }
  
  private void writeRepeatedSInt64Impl(int paramInt, long paramLong) {
    writeTag(paramInt, 0);
    this.mBuffer.writeRawZigZag64(paramLong);
  }
  
  @Deprecated
  public void writePackedSInt64(long paramLong, long[] paramArrayOflong) {
    byte b;
    assertNotCompacted();
    int i = checkFieldId(paramLong, 5574867550208L);
    if (paramArrayOflong != null) {
      b = paramArrayOflong.length;
    } else {
      b = 0;
    } 
    if (b) {
      int j = 0;
      byte b1;
      for (b1 = 0; b1 < b; b1++)
        j += EncodedBuffer.getRawZigZag64Size(paramArrayOflong[b1]); 
      writeKnownLengthHeader(i, j);
      for (b1 = 0; b1 < b; b1++)
        this.mBuffer.writeRawZigZag64(paramArrayOflong[b1]); 
    } 
  }
  
  @Deprecated
  public void writeFixed32(long paramLong, int paramInt) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 1129576398848L);
    writeFixed32Impl(i, paramInt);
  }
  
  private void writeFixed32Impl(int paramInt1, int paramInt2) {
    if (paramInt2 != 0) {
      writeTag(paramInt1, 5);
      this.mBuffer.writeRawFixed32(paramInt2);
    } 
  }
  
  @Deprecated
  public void writeRepeatedFixed32(long paramLong, int paramInt) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 2229088026624L);
    writeRepeatedFixed32Impl(i, paramInt);
  }
  
  private void writeRepeatedFixed32Impl(int paramInt1, int paramInt2) {
    writeTag(paramInt1, 5);
    this.mBuffer.writeRawFixed32(paramInt2);
  }
  
  @Deprecated
  public void writePackedFixed32(long paramLong, int[] paramArrayOfint) {
    byte b;
    assertNotCompacted();
    int i = checkFieldId(paramLong, 5527622909952L);
    if (paramArrayOfint != null) {
      b = paramArrayOfint.length;
    } else {
      b = 0;
    } 
    if (b) {
      writeKnownLengthHeader(i, b * 4);
      for (i = 0; i < b; i++)
        this.mBuffer.writeRawFixed32(paramArrayOfint[i]); 
    } 
  }
  
  @Deprecated
  public void writeFixed64(long paramLong1, long paramLong2) {
    assertNotCompacted();
    int i = checkFieldId(paramLong1, 1125281431552L);
    writeFixed64Impl(i, paramLong2);
  }
  
  private void writeFixed64Impl(int paramInt, long paramLong) {
    if (paramLong != 0L) {
      writeTag(paramInt, 1);
      this.mBuffer.writeRawFixed64(paramLong);
    } 
  }
  
  @Deprecated
  public void writeRepeatedFixed64(long paramLong1, long paramLong2) {
    assertNotCompacted();
    int i = checkFieldId(paramLong1, 2224793059328L);
    writeRepeatedFixed64Impl(i, paramLong2);
  }
  
  private void writeRepeatedFixed64Impl(int paramInt, long paramLong) {
    writeTag(paramInt, 1);
    this.mBuffer.writeRawFixed64(paramLong);
  }
  
  @Deprecated
  public void writePackedFixed64(long paramLong, long[] paramArrayOflong) {
    byte b;
    assertNotCompacted();
    int i = checkFieldId(paramLong, 5523327942656L);
    if (paramArrayOflong != null) {
      b = paramArrayOflong.length;
    } else {
      b = 0;
    } 
    if (b) {
      writeKnownLengthHeader(i, b * 8);
      for (i = 0; i < b; i++)
        this.mBuffer.writeRawFixed64(paramArrayOflong[i]); 
    } 
  }
  
  @Deprecated
  public void writeSFixed32(long paramLong, int paramInt) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 1163936137216L);
    writeSFixed32Impl(i, paramInt);
  }
  
  private void writeSFixed32Impl(int paramInt1, int paramInt2) {
    if (paramInt2 != 0) {
      writeTag(paramInt1, 5);
      this.mBuffer.writeRawFixed32(paramInt2);
    } 
  }
  
  @Deprecated
  public void writeRepeatedSFixed32(long paramLong, int paramInt) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 2263447764992L);
    writeRepeatedSFixed32Impl(i, paramInt);
  }
  
  private void writeRepeatedSFixed32Impl(int paramInt1, int paramInt2) {
    writeTag(paramInt1, 5);
    this.mBuffer.writeRawFixed32(paramInt2);
  }
  
  @Deprecated
  public void writePackedSFixed32(long paramLong, int[] paramArrayOfint) {
    byte b;
    assertNotCompacted();
    int i = checkFieldId(paramLong, 5561982648320L);
    if (paramArrayOfint != null) {
      b = paramArrayOfint.length;
    } else {
      b = 0;
    } 
    if (b) {
      writeKnownLengthHeader(i, b * 4);
      for (i = 0; i < b; i++)
        this.mBuffer.writeRawFixed32(paramArrayOfint[i]); 
    } 
  }
  
  @Deprecated
  public void writeSFixed64(long paramLong1, long paramLong2) {
    assertNotCompacted();
    int i = checkFieldId(paramLong1, 1168231104512L);
    writeSFixed64Impl(i, paramLong2);
  }
  
  private void writeSFixed64Impl(int paramInt, long paramLong) {
    if (paramLong != 0L) {
      writeTag(paramInt, 1);
      this.mBuffer.writeRawFixed64(paramLong);
    } 
  }
  
  @Deprecated
  public void writeRepeatedSFixed64(long paramLong1, long paramLong2) {
    assertNotCompacted();
    int i = checkFieldId(paramLong1, 2267742732288L);
    writeRepeatedSFixed64Impl(i, paramLong2);
  }
  
  private void writeRepeatedSFixed64Impl(int paramInt, long paramLong) {
    writeTag(paramInt, 1);
    this.mBuffer.writeRawFixed64(paramLong);
  }
  
  @Deprecated
  public void writePackedSFixed64(long paramLong, long[] paramArrayOflong) {
    byte b;
    assertNotCompacted();
    int i = checkFieldId(paramLong, 5566277615616L);
    if (paramArrayOflong != null) {
      b = paramArrayOflong.length;
    } else {
      b = 0;
    } 
    if (b) {
      writeKnownLengthHeader(i, b * 8);
      for (i = 0; i < b; i++)
        this.mBuffer.writeRawFixed64(paramArrayOflong[i]); 
    } 
  }
  
  @Deprecated
  public void writeBool(long paramLong, boolean paramBoolean) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 1133871366144L);
    writeBoolImpl(i, paramBoolean);
  }
  
  private void writeBoolImpl(int paramInt, boolean paramBoolean) {
    if (paramBoolean) {
      writeTag(paramInt, 0);
      this.mBuffer.writeRawByte((byte)1);
    } 
  }
  
  @Deprecated
  public void writeRepeatedBool(long paramLong, boolean paramBoolean) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 2233382993920L);
    writeRepeatedBoolImpl(i, paramBoolean);
  }
  
  private void writeRepeatedBoolImpl(int paramInt, boolean paramBoolean) {
    writeTag(paramInt, 0);
    this.mBuffer.writeRawByte((byte)paramBoolean);
  }
  
  @Deprecated
  public void writePackedBool(long paramLong, boolean[] paramArrayOfboolean) {
    byte b;
    assertNotCompacted();
    int i = checkFieldId(paramLong, 5531917877248L);
    if (paramArrayOfboolean != null) {
      b = paramArrayOfboolean.length;
    } else {
      b = 0;
    } 
    if (b) {
      writeKnownLengthHeader(i, b);
      for (i = 0; i < b; i++)
        this.mBuffer.writeRawByte((byte)paramArrayOfboolean[i]); 
    } 
  }
  
  @Deprecated
  public void writeString(long paramLong, String paramString) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 1138166333440L);
    writeStringImpl(i, paramString);
  }
  
  private void writeStringImpl(int paramInt, String paramString) {
    if (paramString != null && paramString.length() > 0)
      writeUtf8String(paramInt, paramString); 
  }
  
  @Deprecated
  public void writeRepeatedString(long paramLong, String paramString) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 2237677961216L);
    writeRepeatedStringImpl(i, paramString);
  }
  
  private void writeRepeatedStringImpl(int paramInt, String paramString) {
    if (paramString == null || paramString.length() == 0) {
      writeKnownLengthHeader(paramInt, 0);
      return;
    } 
    writeUtf8String(paramInt, paramString);
  }
  
  private void writeUtf8String(int paramInt, String paramString) {
    try {
      byte[] arrayOfByte = paramString.getBytes("UTF-8");
      writeKnownLengthHeader(paramInt, arrayOfByte.length);
      this.mBuffer.writeRawBuffer(arrayOfByte);
      return;
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      throw new RuntimeException("not possible");
    } 
  }
  
  @Deprecated
  public void writeBytes(long paramLong, byte[] paramArrayOfbyte) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 1151051235328L);
    writeBytesImpl(i, paramArrayOfbyte);
  }
  
  private void writeBytesImpl(int paramInt, byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null && paramArrayOfbyte.length > 0) {
      writeKnownLengthHeader(paramInt, paramArrayOfbyte.length);
      this.mBuffer.writeRawBuffer(paramArrayOfbyte);
    } 
  }
  
  @Deprecated
  public void writeRepeatedBytes(long paramLong, byte[] paramArrayOfbyte) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 2250562863104L);
    writeRepeatedBytesImpl(i, paramArrayOfbyte);
  }
  
  private void writeRepeatedBytesImpl(int paramInt, byte[] paramArrayOfbyte) {
    int i;
    if (paramArrayOfbyte == null) {
      i = 0;
    } else {
      i = paramArrayOfbyte.length;
    } 
    writeKnownLengthHeader(paramInt, i);
    this.mBuffer.writeRawBuffer(paramArrayOfbyte);
  }
  
  @Deprecated
  public void writeEnum(long paramLong, int paramInt) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 1159641169920L);
    writeEnumImpl(i, paramInt);
  }
  
  private void writeEnumImpl(int paramInt1, int paramInt2) {
    if (paramInt2 != 0) {
      writeTag(paramInt1, 0);
      writeUnsignedVarintFromSignedInt(paramInt2);
    } 
  }
  
  @Deprecated
  public void writeRepeatedEnum(long paramLong, int paramInt) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 2259152797696L);
    writeRepeatedEnumImpl(i, paramInt);
  }
  
  private void writeRepeatedEnumImpl(int paramInt1, int paramInt2) {
    writeTag(paramInt1, 0);
    writeUnsignedVarintFromSignedInt(paramInt2);
  }
  
  @Deprecated
  public void writePackedEnum(long paramLong, int[] paramArrayOfint) {
    byte b;
    assertNotCompacted();
    int i = checkFieldId(paramLong, 5557687681024L);
    if (paramArrayOfint != null) {
      b = paramArrayOfint.length;
    } else {
      b = 0;
    } 
    if (b) {
      int j = 0;
      byte b1;
      for (b1 = 0; b1 < b; b1++) {
        int k = paramArrayOfint[b1];
        if (k >= 0) {
          k = EncodedBuffer.getRawVarint32Size(k);
        } else {
          k = 10;
        } 
        j += k;
      } 
      writeKnownLengthHeader(i, j);
      for (b1 = 0; b1 < b; b1++)
        writeUnsignedVarintFromSignedInt(paramArrayOfint[b1]); 
    } 
  }
  
  @Deprecated
  public long startObject(long paramLong) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 1146756268032L);
    return startObjectImpl(i, false);
  }
  
  @Deprecated
  public void endObject(long paramLong) {
    assertNotCompacted();
    endObjectImpl(paramLong, false);
  }
  
  @Deprecated
  public long startRepeatedObject(long paramLong) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 2246267895808L);
    return startObjectImpl(i, true);
  }
  
  @Deprecated
  public void endRepeatedObject(long paramLong) {
    assertNotCompacted();
    endObjectImpl(paramLong, true);
  }
  
  private long startObjectImpl(int paramInt, boolean paramBoolean) {
    writeTag(paramInt, 2);
    int i = this.mBuffer.getWritePos();
    this.mDepth++;
    this.mNextObjectId--;
    this.mBuffer.writeRawFixed32((int)(this.mExpectedObjectToken >> 32L));
    this.mBuffer.writeRawFixed32((int)this.mExpectedObjectToken);
    long l = this.mExpectedObjectToken;
    this.mExpectedObjectToken = l = makeToken(getTagSize(paramInt), paramBoolean, this.mDepth, this.mNextObjectId, i);
    return l;
  }
  
  private void endObjectImpl(long paramLong, boolean paramBoolean) {
    int i = getDepthFromToken(paramLong);
    boolean bool = getRepeatedFromToken(paramLong);
    int j = getOffsetFromToken(paramLong);
    int k = this.mBuffer.getWritePos() - j - 8;
    if (paramBoolean != bool) {
      if (paramBoolean)
        throw new IllegalArgumentException("endRepeatedObject called where endObject should have been"); 
      throw new IllegalArgumentException("endObject called where endRepeatedObject should have been");
    } 
    if ((this.mDepth & 0x1FF) == i && this.mExpectedObjectToken == paramLong) {
      long l = this.mBuffer.getRawFixed32At(j);
      EncodedBuffer encodedBuffer = this.mBuffer;
      this.mExpectedObjectToken = l << 32L | 0xFFFFFFFFL & encodedBuffer.getRawFixed32At(j + 4);
      this.mDepth--;
      if (k > 0) {
        this.mBuffer.editRawFixed32(j, -k);
        this.mBuffer.editRawFixed32(j + 4, -1);
      } else if (paramBoolean) {
        this.mBuffer.editRawFixed32(j, 0);
        this.mBuffer.editRawFixed32(j + 4, 0);
      } else {
        this.mBuffer.rewindWriteTo(j - getTagSizeFromToken(paramLong));
      } 
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Mismatched startObject/endObject calls. Current depth ");
    stringBuilder.append(this.mDepth);
    stringBuilder.append(" token=");
    stringBuilder.append(token2String(paramLong));
    stringBuilder.append(" expectedToken=");
    paramLong = this.mExpectedObjectToken;
    stringBuilder.append(token2String(paramLong));
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  @Deprecated
  public void writeObject(long paramLong, byte[] paramArrayOfbyte) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 1146756268032L);
    writeObjectImpl(i, paramArrayOfbyte);
  }
  
  void writeObjectImpl(int paramInt, byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null && paramArrayOfbyte.length != 0) {
      writeKnownLengthHeader(paramInt, paramArrayOfbyte.length);
      this.mBuffer.writeRawBuffer(paramArrayOfbyte);
    } 
  }
  
  @Deprecated
  public void writeRepeatedObject(long paramLong, byte[] paramArrayOfbyte) {
    assertNotCompacted();
    int i = checkFieldId(paramLong, 2246267895808L);
    writeRepeatedObjectImpl(i, paramArrayOfbyte);
  }
  
  void writeRepeatedObjectImpl(int paramInt, byte[] paramArrayOfbyte) {
    int i;
    if (paramArrayOfbyte == null) {
      i = 0;
    } else {
      i = paramArrayOfbyte.length;
    } 
    writeKnownLengthHeader(paramInt, i);
    this.mBuffer.writeRawBuffer(paramArrayOfbyte);
  }
  
  public static long makeFieldId(int paramInt, long paramLong) {
    return paramInt & 0xFFFFFFFFL | paramLong;
  }
  
  public static int checkFieldId(long paramLong1, long paramLong2) {
    long l1 = paramLong1 & 0xF0000000000L;
    long l2 = paramLong1 & 0xFF00000000L;
    long l3 = paramLong2 & 0xF0000000000L;
    paramLong2 &= 0xFF00000000L;
    if ((int)paramLong1 != 0) {
      if (l2 != paramLong2 || (l1 != l3 && (l1 != 5497558138880L || l3 != 2199023255552L))) {
        String str1 = getFieldCountString(l1);
        String str2 = getFieldTypeString(l2);
        if (str2 != null && str1 != null) {
          StringBuilder stringBuilder2 = new StringBuilder();
          if (paramLong2 == 47244640256L) {
            stringBuilder2.append("start");
          } else {
            stringBuilder2.append("write");
          } 
          stringBuilder2.append(getFieldCountString(l3));
          stringBuilder2.append(getFieldTypeString(paramLong2));
          stringBuilder2.append(" called for field ");
          stringBuilder2.append((int)paramLong1);
          stringBuilder2.append(" which should be used with ");
          if (l2 == 47244640256L) {
            stringBuilder2.append("start");
          } else {
            stringBuilder2.append("write");
          } 
          stringBuilder2.append(str1);
          stringBuilder2.append(str2);
          if (l1 == 5497558138880L) {
            stringBuilder2.append(" or writeRepeated");
            stringBuilder2.append(str2);
          } 
          stringBuilder2.append('.');
          throw new IllegalArgumentException(stringBuilder2.toString());
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        if (paramLong2 == 47244640256L) {
          stringBuilder1.append("start");
        } else {
          stringBuilder1.append("write");
        } 
        stringBuilder1.append(getFieldCountString(l3));
        stringBuilder1.append(getFieldTypeString(paramLong2));
        stringBuilder1.append(" called with an invalid fieldId: 0x");
        stringBuilder1.append(Long.toHexString(paramLong1));
        stringBuilder1.append(". The proto field ID might be ");
        stringBuilder1.append((int)paramLong1);
        stringBuilder1.append('.');
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      return (int)paramLong1;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid proto field ");
    stringBuilder.append((int)paramLong1);
    stringBuilder.append(" fieldId=");
    stringBuilder.append(Long.toHexString(paramLong1));
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private static int getTagSize(int paramInt) {
    return EncodedBuffer.getRawVarint32Size(paramInt << 3);
  }
  
  public void writeTag(int paramInt1, int paramInt2) {
    this.mBuffer.writeRawVarint32(paramInt1 << 3 | paramInt2);
  }
  
  private void writeKnownLengthHeader(int paramInt1, int paramInt2) {
    writeTag(paramInt1, 2);
    this.mBuffer.writeRawFixed32(paramInt2);
    this.mBuffer.writeRawFixed32(paramInt2);
  }
  
  private void assertNotCompacted() {
    if (!this.mCompacted)
      return; 
    throw new IllegalArgumentException("write called after compact");
  }
  
  public byte[] getBytes() {
    compactIfNecessary();
    EncodedBuffer encodedBuffer = this.mBuffer;
    return encodedBuffer.getBytes(encodedBuffer.getReadableSize());
  }
  
  private void compactIfNecessary() {
    if (!this.mCompacted)
      if (this.mDepth == 0) {
        this.mBuffer.startEditing();
        int i = this.mBuffer.getReadableSize();
        editEncodedSize(i);
        this.mBuffer.rewindRead();
        compactSizes(i);
        int j = this.mCopyBegin;
        if (j < i)
          this.mBuffer.writeFromThisBuffer(j, i - j); 
        this.mBuffer.startEditing();
        this.mCompacted = true;
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Trying to compact with ");
        stringBuilder.append(this.mDepth);
        stringBuilder.append(" missing calls to endObject");
        throw new IllegalArgumentException(stringBuilder.toString());
      }  
  }
  
  private int editEncodedSize(int paramInt) {
    int i = this.mBuffer.getReadPos();
    int j = 0;
    label35: while (true) {
      int k = this.mBuffer.getReadPos();
      if (k < i + paramInt) {
        int m = readRawTag();
        int n = j + EncodedBuffer.getRawVarint32Size(m);
        j = m & 0x7;
        if (j != 0) {
          if (j != 1) {
            if (j != 2) {
              if (j != 3 && j != 4) {
                if (j == 5) {
                  j = n + 4;
                  this.mBuffer.skipRead(4);
                  continue;
                } 
                StringBuilder stringBuilder1 = new StringBuilder();
                stringBuilder1.append("editEncodedSize Bad tag tag=0x");
                stringBuilder1.append(Integer.toHexString(m));
                stringBuilder1.append(" wireType=");
                stringBuilder1.append(j);
                stringBuilder1.append(" -- ");
                EncodedBuffer encodedBuffer = this.mBuffer;
                stringBuilder1.append(encodedBuffer.getDebugString());
                throw new ProtoParseException(stringBuilder1.toString());
              } 
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("groups not supported at index ");
              stringBuilder.append(k);
              throw new RuntimeException(stringBuilder.toString());
            } 
            k = this.mBuffer.readRawFixed32();
            m = this.mBuffer.getReadPos();
            j = this.mBuffer.readRawFixed32();
            if (k >= 0) {
              if (j == k) {
                this.mBuffer.skipRead(k);
              } else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Pre-computed size where the precomputed size and the raw size in the buffer don't match! childRawSize=");
                stringBuilder.append(k);
                stringBuilder.append(" childEncodedSize=");
                stringBuilder.append(j);
                stringBuilder.append(" childEncodedSizePos=");
                stringBuilder.append(m);
                throw new RuntimeException(stringBuilder.toString());
              } 
            } else {
              j = editEncodedSize(-k);
              this.mBuffer.editRawFixed32(m, j);
            } 
            j = n + EncodedBuffer.getRawVarint32Size(j) + j;
            continue;
          } 
          j = n + 8;
          this.mBuffer.skipRead(8);
          continue;
        } 
        n++;
        while (true) {
          j = n;
          if ((this.mBuffer.readRawByte() & 0x80) != 0) {
            n++;
            continue;
          } 
          continue label35;
        } 
      } 
      break;
    } 
    return j;
  }
  
  private void compactSizes(int paramInt) {
    int i = this.mBuffer.getReadPos();
    while (true) {
      int j = this.mBuffer.getReadPos();
      if (j < i + paramInt) {
        int k = readRawTag();
        int m = k & 0x7;
        if (m != 0) {
          if (m != 1) {
            if (m != 2) {
              if (m != 3 && m != 4) {
                if (m == 5) {
                  this.mBuffer.skipRead(4);
                  continue;
                } 
                StringBuilder stringBuilder1 = new StringBuilder();
                stringBuilder1.append("compactSizes Bad tag tag=0x");
                stringBuilder1.append(Integer.toHexString(k));
                stringBuilder1.append(" wireType=");
                stringBuilder1.append(m);
                stringBuilder1.append(" -- ");
                EncodedBuffer encodedBuffer1 = this.mBuffer;
                stringBuilder1.append(encodedBuffer1.getDebugString());
                throw new ProtoParseException(stringBuilder1.toString());
              } 
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("groups not supported at index ");
              stringBuilder.append(j);
              throw new RuntimeException(stringBuilder.toString());
            } 
            EncodedBuffer encodedBuffer = this.mBuffer;
            encodedBuffer.writeFromThisBuffer(this.mCopyBegin, encodedBuffer.getReadPos() - this.mCopyBegin);
            j = this.mBuffer.readRawFixed32();
            m = this.mBuffer.readRawFixed32();
            this.mBuffer.writeRawVarint32(m);
            this.mCopyBegin = this.mBuffer.getReadPos();
            if (j >= 0) {
              this.mBuffer.skipRead(m);
              continue;
            } 
            compactSizes(-j);
            continue;
          } 
          this.mBuffer.skipRead(8);
          continue;
        } 
        while ((this.mBuffer.readRawByte() & 0x80) != 0);
        continue;
      } 
      break;
    } 
  }
  
  public void flush() {
    if (this.mStream == null)
      return; 
    if (this.mDepth != 0)
      return; 
    if (this.mCompacted)
      return; 
    compactIfNecessary();
    EncodedBuffer encodedBuffer = this.mBuffer;
    byte[] arrayOfByte = encodedBuffer.getBytes(encodedBuffer.getReadableSize());
    try {
      this.mStream.write(arrayOfByte);
      this.mStream.flush();
      return;
    } catch (IOException iOException) {
      throw new RuntimeException("Error flushing proto to stream", iOException);
    } 
  }
  
  private int readRawTag() {
    if (this.mBuffer.getReadPos() == this.mBuffer.getReadableSize())
      return 0; 
    return (int)this.mBuffer.readRawUnsigned();
  }
  
  public void dump(String paramString) {
    Log.d(paramString, this.mBuffer.getDebugString());
    this.mBuffer.dumpBuffers(paramString);
  }
}
