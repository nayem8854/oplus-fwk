package android.util.proto;

import android.util.Log;
import java.util.ArrayList;

public final class EncodedBuffer {
  private static final String TAG = "EncodedBuffer";
  
  private int mBufferCount;
  
  private final ArrayList<byte[]> mBuffers = (ArrayList)new ArrayList<>();
  
  private final int mChunkSize;
  
  private int mReadBufIndex;
  
  private byte[] mReadBuffer;
  
  private int mReadIndex;
  
  private int mReadLimit = -1;
  
  private int mReadableSize = -1;
  
  private int mWriteBufIndex;
  
  private byte[] mWriteBuffer;
  
  private int mWriteIndex;
  
  public EncodedBuffer() {
    this(0);
  }
  
  public EncodedBuffer(int paramInt) {
    int i = paramInt;
    if (paramInt <= 0)
      i = 8192; 
    this.mChunkSize = i;
    byte[] arrayOfByte = new byte[i];
    this.mBuffers.add(arrayOfByte);
    this.mBufferCount = 1;
  }
  
  public void startEditing() {
    int i = this.mWriteBufIndex, j = this.mChunkSize, k = this.mWriteIndex;
    this.mReadableSize = i * j + k;
    this.mReadLimit = k;
    byte[] arrayOfByte = this.mBuffers.get(0);
    this.mWriteIndex = 0;
    this.mWriteBufIndex = 0;
    this.mReadBuffer = arrayOfByte;
    this.mReadBufIndex = 0;
    this.mReadIndex = 0;
  }
  
  public void rewindRead() {
    this.mReadBuffer = this.mBuffers.get(0);
    this.mReadBufIndex = 0;
    this.mReadIndex = 0;
  }
  
  public int getReadableSize() {
    return this.mReadableSize;
  }
  
  public int getSize() {
    return (this.mBufferCount - 1) * this.mChunkSize + this.mWriteIndex;
  }
  
  public int getReadPos() {
    return this.mReadBufIndex * this.mChunkSize + this.mReadIndex;
  }
  
  public void skipRead(int paramInt) {
    if (paramInt >= 0) {
      if (paramInt == 0)
        return; 
      int i = this.mChunkSize, j = this.mReadIndex;
      if (paramInt <= i - j) {
        this.mReadIndex = j + paramInt;
      } else {
        paramInt -= i - j;
        this.mReadIndex = j = paramInt % i;
        if (j == 0) {
          this.mReadIndex = i;
          this.mReadBufIndex += paramInt / i;
        } else {
          this.mReadBufIndex += paramInt / i + 1;
        } 
        this.mReadBuffer = this.mBuffers.get(this.mReadBufIndex);
      } 
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("skipRead with negative amount=");
    stringBuilder.append(paramInt);
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public byte readRawByte() {
    int i = this.mReadBufIndex, j = this.mBufferCount;
    if (i <= j && (i != j - 1 || this.mReadIndex < this.mReadLimit)) {
      if (this.mReadIndex >= this.mChunkSize) {
        this.mReadBufIndex = i = this.mReadBufIndex + 1;
        this.mReadBuffer = this.mBuffers.get(i);
        this.mReadIndex = 0;
      } 
      byte[] arrayOfByte = this.mReadBuffer;
      i = this.mReadIndex;
      this.mReadIndex = i + 1;
      return arrayOfByte[i];
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Trying to read too much data mReadBufIndex=");
    stringBuilder.append(this.mReadBufIndex);
    stringBuilder.append(" mBufferCount=");
    stringBuilder.append(this.mBufferCount);
    stringBuilder.append(" mReadIndex=");
    stringBuilder.append(this.mReadIndex);
    stringBuilder.append(" mReadLimit=");
    stringBuilder.append(this.mReadLimit);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public long readRawUnsigned() {
    byte b = 0;
    long l = 0L;
    while (true) {
      byte b1 = readRawByte();
      l |= (b1 & Byte.MAX_VALUE) << b;
      if ((b1 & 0x80) == 0)
        return l; 
      b += true;
      if (b <= 64)
        continue; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Varint too long -- ");
      stringBuilder.append(getDebugString());
      throw new ProtoParseException(stringBuilder.toString());
    } 
  }
  
  public int readRawFixed32() {
    byte b1 = readRawByte();
    byte b2 = readRawByte();
    byte b3 = readRawByte();
    byte b4 = readRawByte();
    return b1 & 0xFF | (b2 & 0xFF) << 8 | (b3 & 0xFF) << 16 | (b4 & 0xFF) << 24;
  }
  
  private void nextWriteBuffer() {
    int i = this.mWriteBufIndex + 1;
    if (i >= this.mBufferCount) {
      byte[] arrayOfByte = new byte[this.mChunkSize];
      this.mBuffers.add(arrayOfByte);
      this.mBufferCount++;
    } else {
      this.mWriteBuffer = this.mBuffers.get(i);
    } 
    this.mWriteIndex = 0;
  }
  
  public void writeRawByte(byte paramByte) {
    if (this.mWriteIndex >= this.mChunkSize)
      nextWriteBuffer(); 
    byte[] arrayOfByte = this.mWriteBuffer;
    int i = this.mWriteIndex;
    this.mWriteIndex = i + 1;
    arrayOfByte[i] = paramByte;
  }
  
  public static int getRawVarint32Size(int paramInt) {
    if ((paramInt & 0xFFFFFF80) == 0)
      return 1; 
    if ((paramInt & 0xFFFFC000) == 0)
      return 2; 
    if ((0xFFE00000 & paramInt) == 0)
      return 3; 
    if ((0xF0000000 & paramInt) == 0)
      return 4; 
    return 5;
  }
  
  public void writeRawVarint32(int paramInt) {
    while (true) {
      if ((paramInt & 0xFFFFFF80) == 0) {
        writeRawByte((byte)paramInt);
        return;
      } 
      writeRawByte((byte)(paramInt & 0x7F | 0x80));
      paramInt >>>= 7;
    } 
  }
  
  public static int getRawZigZag32Size(int paramInt) {
    return getRawVarint32Size(zigZag32(paramInt));
  }
  
  public void writeRawZigZag32(int paramInt) {
    writeRawVarint32(zigZag32(paramInt));
  }
  
  public static int getRawVarint64Size(long paramLong) {
    if ((0xFFFFFFFFFFFFFF80L & paramLong) == 0L)
      return 1; 
    if ((0xFFFFFFFFFFFFC000L & paramLong) == 0L)
      return 2; 
    if ((0xFFFFFFFFFFE00000L & paramLong) == 0L)
      return 3; 
    if ((0xFFFFFFFFF0000000L & paramLong) == 0L)
      return 4; 
    if ((0xFFFFFFF800000000L & paramLong) == 0L)
      return 5; 
    if ((0xFFFFFC0000000000L & paramLong) == 0L)
      return 6; 
    if ((0xFFFE000000000000L & paramLong) == 0L)
      return 7; 
    if ((0xFF00000000000000L & paramLong) == 0L)
      return 8; 
    if ((Long.MIN_VALUE & paramLong) == 0L)
      return 9; 
    return 10;
  }
  
  public void writeRawVarint64(long paramLong) {
    while (true) {
      if ((0xFFFFFFFFFFFFFF80L & paramLong) == 0L) {
        writeRawByte((byte)(int)paramLong);
        return;
      } 
      writeRawByte((byte)(int)(0x7FL & paramLong | 0x80L));
      paramLong >>>= 7L;
    } 
  }
  
  public static int getRawZigZag64Size(long paramLong) {
    return getRawVarint64Size(zigZag64(paramLong));
  }
  
  public void writeRawZigZag64(long paramLong) {
    writeRawVarint64(zigZag64(paramLong));
  }
  
  public void writeRawFixed32(int paramInt) {
    writeRawByte((byte)paramInt);
    writeRawByte((byte)(paramInt >> 8));
    writeRawByte((byte)(paramInt >> 16));
    writeRawByte((byte)(paramInt >> 24));
  }
  
  public void writeRawFixed64(long paramLong) {
    writeRawByte((byte)(int)paramLong);
    writeRawByte((byte)(int)(paramLong >> 8L));
    writeRawByte((byte)(int)(paramLong >> 16L));
    writeRawByte((byte)(int)(paramLong >> 24L));
    writeRawByte((byte)(int)(paramLong >> 32L));
    writeRawByte((byte)(int)(paramLong >> 40L));
    writeRawByte((byte)(int)(paramLong >> 48L));
    writeRawByte((byte)(int)(paramLong >> 56L));
  }
  
  public void writeRawBuffer(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null && paramArrayOfbyte.length > 0)
      writeRawBuffer(paramArrayOfbyte, 0, paramArrayOfbyte.length); 
  }
  
  public void writeRawBuffer(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    int k;
    if (paramArrayOfbyte == null)
      return; 
    int i = this.mChunkSize, j = this.mWriteIndex;
    if (paramInt2 < i - j) {
      k = paramInt2;
    } else {
      k = i - j;
    } 
    j = paramInt1;
    i = paramInt2;
    if (k > 0) {
      System.arraycopy(paramArrayOfbyte, paramInt1, this.mWriteBuffer, this.mWriteIndex, k);
      this.mWriteIndex += k;
      i = paramInt2 - k;
      j = paramInt1 + k;
    } 
    while (i > 0) {
      nextWriteBuffer();
      paramInt1 = paramInt2 = this.mChunkSize;
      if (i < paramInt2)
        paramInt1 = i; 
      System.arraycopy(paramArrayOfbyte, j, this.mWriteBuffer, this.mWriteIndex, paramInt1);
      this.mWriteIndex += paramInt1;
      i -= paramInt1;
      j += paramInt1;
    } 
  }
  
  public void writeFromThisBuffer(int paramInt1, int paramInt2) {
    if (this.mReadLimit >= 0) {
      if (paramInt1 >= getWritePos()) {
        if (paramInt1 + paramInt2 <= this.mReadableSize) {
          if (paramInt2 == 0)
            return; 
          int i = this.mWriteBufIndex, j = this.mChunkSize, k = this.mWriteIndex;
          if (paramInt1 == i * j + k) {
            if (paramInt2 <= j - k) {
              this.mWriteIndex = k + paramInt2;
            } else {
              paramInt2 -= j - k;
              this.mWriteIndex = paramInt1 = paramInt2 % j;
              if (paramInt1 == 0) {
                this.mWriteIndex = j;
                this.mWriteBufIndex = i + paramInt2 / j;
              } else {
                this.mWriteBufIndex = i + paramInt2 / j + 1;
              } 
              this.mWriteBuffer = this.mBuffers.get(this.mWriteBufIndex);
            } 
          } else {
            k = paramInt1 / j;
            byte[] arrayOfByte = this.mBuffers.get(k);
            paramInt1 %= this.mChunkSize;
            while (paramInt2 > 0) {
              if (this.mWriteIndex >= this.mChunkSize)
                nextWriteBuffer(); 
              i = k;
              j = paramInt1;
              if (paramInt1 >= this.mChunkSize) {
                i = k + 1;
                arrayOfByte = this.mBuffers.get(i);
                j = 0;
              } 
              paramInt1 = this.mChunkSize;
              k = this.mWriteIndex;
              k = Math.min(paramInt2, Math.min(paramInt1 - k, paramInt1 - j));
              System.arraycopy(arrayOfByte, j, this.mWriteBuffer, this.mWriteIndex, k);
              this.mWriteIndex += k;
              paramInt1 = j + k;
              paramInt2 -= k;
              k = i;
            } 
          } 
          return;
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Trying to move more data than there is -- srcOffset=");
        stringBuilder1.append(paramInt1);
        stringBuilder1.append(" size=");
        stringBuilder1.append(paramInt2);
        stringBuilder1.append(" ");
        stringBuilder1.append(getDebugString());
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Can only move forward in the buffer -- srcOffset=");
      stringBuilder.append(paramInt1);
      stringBuilder.append(" size=");
      stringBuilder.append(paramInt2);
      stringBuilder.append(" ");
      stringBuilder.append(getDebugString());
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    throw new IllegalStateException("writeFromThisBuffer before startEditing");
  }
  
  public int getWritePos() {
    return this.mWriteBufIndex * this.mChunkSize + this.mWriteIndex;
  }
  
  public void rewindWriteTo(int paramInt) {
    if (paramInt <= getWritePos()) {
      int i = this.mChunkSize, j = paramInt / i;
      this.mWriteIndex = paramInt %= i;
      if (paramInt == 0 && j != 0) {
        this.mWriteIndex = i;
        this.mWriteBufIndex = j - 1;
      } 
      this.mWriteBuffer = this.mBuffers.get(this.mWriteBufIndex);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("rewindWriteTo only can go backwards");
    stringBuilder.append(paramInt);
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public int getRawFixed32At(int paramInt) {
    byte[] arrayOfByte3 = this.mBuffers.get(paramInt / this.mChunkSize);
    int i = this.mChunkSize;
    byte b = arrayOfByte3[paramInt % i];
    ArrayList<byte[]> arrayList3 = this.mBuffers;
    i = (paramInt + 1) / i;
    byte[] arrayOfByte2 = arrayList3.get(i);
    int j = this.mChunkSize;
    i = arrayOfByte2[(paramInt + 1) % j];
    ArrayList<byte[]> arrayList2 = this.mBuffers;
    j = (paramInt + 2) / j;
    byte[] arrayOfByte1 = arrayList2.get(j);
    int k = this.mChunkSize;
    j = arrayOfByte1[(paramInt + 2) % k];
    ArrayList<byte[]> arrayList1 = this.mBuffers;
    k = (paramInt + 3) / k;
    paramInt = ((byte[])arrayList1.get(k))[(paramInt + 3) % this.mChunkSize];
    return b & 0xFF | (i & 0xFF) << 8 | (j & 0xFF) << 16 | (paramInt & 0xFF) << 24;
  }
  
  public void editRawFixed32(int paramInt1, int paramInt2) {
    byte[] arrayOfByte = this.mBuffers.get(paramInt1 / this.mChunkSize);
    int i = this.mChunkSize;
    arrayOfByte[paramInt1 % i] = (byte)paramInt2;
    arrayOfByte = this.mBuffers.get((paramInt1 + 1) / i);
    i = this.mChunkSize;
    arrayOfByte[(paramInt1 + 1) % i] = (byte)(paramInt2 >> 8);
    arrayOfByte = this.mBuffers.get((paramInt1 + 2) / i);
    i = this.mChunkSize;
    arrayOfByte[(paramInt1 + 2) % i] = (byte)(paramInt2 >> 16);
    ((byte[])this.mBuffers.get((paramInt1 + 3) / i))[(paramInt1 + 3) % this.mChunkSize] = (byte)(paramInt2 >> 24);
  }
  
  private static int zigZag32(int paramInt) {
    return paramInt << 1 ^ paramInt >> 31;
  }
  
  private static long zigZag64(long paramLong) {
    return paramLong << 1L ^ paramLong >> 63L;
  }
  
  public byte[] getBytes(int paramInt) {
    byte[] arrayOfByte = new byte[paramInt];
    int i = paramInt / this.mChunkSize;
    int j = 0;
    byte b;
    for (b = 0; b < i; b++) {
      System.arraycopy(this.mBuffers.get(b), 0, arrayOfByte, j, this.mChunkSize);
      j += this.mChunkSize;
    } 
    paramInt -= this.mChunkSize * i;
    if (paramInt > 0)
      System.arraycopy(this.mBuffers.get(b), 0, arrayOfByte, j, paramInt); 
    return arrayOfByte;
  }
  
  public int getChunkCount() {
    return this.mBuffers.size();
  }
  
  public int getWriteIndex() {
    return this.mWriteIndex;
  }
  
  public int getWriteBufIndex() {
    return this.mWriteBufIndex;
  }
  
  public String getDebugString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("EncodedBuffer( mChunkSize=");
    stringBuilder.append(this.mChunkSize);
    stringBuilder.append(" mBuffers.size=");
    stringBuilder.append(this.mBuffers.size());
    stringBuilder.append(" mBufferCount=");
    stringBuilder.append(this.mBufferCount);
    stringBuilder.append(" mWriteIndex=");
    stringBuilder.append(this.mWriteIndex);
    stringBuilder.append(" mWriteBufIndex=");
    stringBuilder.append(this.mWriteBufIndex);
    stringBuilder.append(" mReadBufIndex=");
    stringBuilder.append(this.mReadBufIndex);
    stringBuilder.append(" mReadIndex=");
    stringBuilder.append(this.mReadIndex);
    stringBuilder.append(" mReadableSize=");
    stringBuilder.append(this.mReadableSize);
    stringBuilder.append(" mReadLimit=");
    stringBuilder.append(this.mReadLimit);
    stringBuilder.append(" )");
    return stringBuilder.toString();
  }
  
  public void dumpBuffers(String paramString) {
    int i = this.mBuffers.size();
    int j = 0;
    for (byte b = 0; b < i; b++) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("{");
      stringBuilder.append(b);
      stringBuilder.append("} ");
      j += dumpByteString(paramString, stringBuilder.toString(), j, this.mBuffers.get(b));
    } 
  }
  
  public static void dumpByteString(String paramString1, String paramString2, byte[] paramArrayOfbyte) {
    dumpByteString(paramString1, paramString2, 0, paramArrayOfbyte);
  }
  
  private static int dumpByteString(String paramString1, String paramString2, int paramInt, byte[] paramArrayOfbyte) {
    StringBuffer stringBuffer = new StringBuffer();
    int i = paramArrayOfbyte.length;
    for (byte b = 0; b < i; b++) {
      if (b % 16 == 0) {
        StringBuffer stringBuffer1 = stringBuffer;
        if (b != 0) {
          Log.d(paramString1, stringBuffer.toString());
          stringBuffer1 = new StringBuffer();
        } 
        stringBuffer1.append(paramString2);
        stringBuffer1.append('[');
        stringBuffer1.append(paramInt + b);
        stringBuffer1.append(']');
        stringBuffer1.append(' ');
        stringBuffer = stringBuffer1;
      } else {
        stringBuffer.append(' ');
      } 
      byte b1 = paramArrayOfbyte[b];
      byte b2 = (byte)(b1 >> 4 & 0xF);
      if (b2 < 10) {
        stringBuffer.append((char)(b2 + 48));
      } else {
        stringBuffer.append((char)(b2 + 87));
      } 
      b2 = (byte)(b1 & 0xF);
      if (b2 < 10) {
        stringBuffer.append((char)(b2 + 48));
      } else {
        stringBuffer.append((char)(b2 + 87));
      } 
    } 
    Log.d(paramString1, stringBuffer.toString());
    return i;
  }
}
