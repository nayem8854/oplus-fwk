package android.util.proto;

import java.io.IOException;

public class ProtoUtils {
  public static void toAggStatsProto(ProtoOutputStream paramProtoOutputStream, long paramLong1, long paramLong2, long paramLong3, long paramLong4, int paramInt1, int paramInt2) {
    paramLong1 = paramProtoOutputStream.start(paramLong1);
    paramProtoOutputStream.write(1112396529665L, paramLong2);
    paramProtoOutputStream.write(1112396529666L, paramLong3);
    paramProtoOutputStream.write(1112396529667L, paramLong4);
    paramProtoOutputStream.write(1120986464260L, paramInt1);
    paramProtoOutputStream.write(1120986464261L, paramInt2);
    paramProtoOutputStream.end(paramLong1);
  }
  
  public static void toAggStatsProto(ProtoOutputStream paramProtoOutputStream, long paramLong1, long paramLong2, long paramLong3, long paramLong4) {
    toAggStatsProto(paramProtoOutputStream, paramLong1, paramLong2, paramLong3, paramLong4, 0, 0);
  }
  
  public static void toDuration(ProtoOutputStream paramProtoOutputStream, long paramLong1, long paramLong2, long paramLong3) {
    paramLong1 = paramProtoOutputStream.start(paramLong1);
    paramProtoOutputStream.write(1112396529665L, paramLong2);
    paramProtoOutputStream.write(1112396529666L, paramLong3);
    paramProtoOutputStream.end(paramLong1);
  }
  
  public static void writeBitWiseFlagsToProtoEnum(ProtoOutputStream paramProtoOutputStream, long paramLong, int paramInt, int[] paramArrayOfint1, int[] paramArrayOfint2) {
    if (paramArrayOfint2.length == paramArrayOfint1.length) {
      int i = paramArrayOfint1.length;
      for (byte b = 0; b < i; b++) {
        if (paramArrayOfint1[b] == 0 && paramInt == 0) {
          paramProtoOutputStream.write(paramLong, paramArrayOfint2[b]);
          return;
        } 
        if ((paramArrayOfint1[b] & paramInt) != 0)
          paramProtoOutputStream.write(paramLong, paramArrayOfint2[b]); 
      } 
      return;
    } 
    throw new IllegalArgumentException("The length of origEnums must match protoEnums");
  }
  
  public static String currentFieldToString(ProtoInputStream paramProtoInputStream) throws IOException {
    StringBuilder stringBuilder1 = new StringBuilder();
    int i = paramProtoInputStream.getFieldNumber();
    int j = paramProtoInputStream.getWireType();
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("Offset : 0x");
    stringBuilder2.append(Integer.toHexString(paramProtoInputStream.getOffset()));
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("\nField Number : 0x");
    stringBuilder2.append(Integer.toHexString(paramProtoInputStream.getFieldNumber()));
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append("\nWire Type : ");
    if (j != 0) {
      if (j != 1) {
        if (j != 2) {
          if (j != 3) {
            if (j != 4) {
              if (j != 5) {
                stringBuilder2 = new StringBuilder();
                stringBuilder2.append("unknown(");
                stringBuilder2.append(paramProtoInputStream.getWireType());
                stringBuilder2.append(")");
                stringBuilder1.append(stringBuilder2.toString());
              } else {
                stringBuilder1.append("fixed32");
                long l = ProtoStream.makeFieldId(i, 1129576398848L);
                stringBuilder2 = new StringBuilder();
                stringBuilder2.append("\nField Value : 0x");
                stringBuilder2.append(Integer.toHexString(paramProtoInputStream.readInt(l)));
                stringBuilder1.append(stringBuilder2.toString());
              } 
            } else {
              stringBuilder1.append("end group");
            } 
          } else {
            stringBuilder1.append("start group");
          } 
        } else {
          stringBuilder1.append("length delimited");
          long l = ProtoStream.makeFieldId(i, 1151051235328L);
          stringBuilder2 = new StringBuilder();
          stringBuilder2.append("\nField Bytes : ");
          stringBuilder2.append(paramProtoInputStream.readBytes(l));
          stringBuilder1.append(stringBuilder2.toString());
        } 
      } else {
        stringBuilder1.append("fixed64");
        long l = ProtoStream.makeFieldId(i, 1125281431552L);
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append("\nField Value : 0x");
        stringBuilder2.append(Long.toHexString(paramProtoInputStream.readLong(l)));
        stringBuilder1.append(stringBuilder2.toString());
      } 
    } else {
      stringBuilder1.append("varint");
      long l = ProtoStream.makeFieldId(i, 1112396529664L);
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append("\nField Value : 0x");
      stringBuilder2.append(Long.toHexString(paramProtoInputStream.readLong(l)));
      stringBuilder1.append(stringBuilder2.toString());
    } 
    return stringBuilder1.toString();
  }
}
