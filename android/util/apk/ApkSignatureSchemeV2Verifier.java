package android.util.apk;

import android.util.ArrayMap;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.security.DigestException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Map;

public class ApkSignatureSchemeV2Verifier {
  private static final int APK_SIGNATURE_SCHEME_V2_BLOCK_ID = 1896449818;
  
  public static final int SF_ATTRIBUTE_ANDROID_APK_SIGNED_ID = 2;
  
  private static final int STRIPPING_PROTECTION_ATTR_ID = -1091571699;
  
  public static boolean hasSignature(String paramString) throws IOException {
    try {
      RandomAccessFile randomAccessFile = new RandomAccessFile();
      this(paramString, "r");
      try {
        findSignature(randomAccessFile);
        return true;
      } finally {
        try {
          randomAccessFile.close();
        } finally {
          randomAccessFile = null;
        } 
      } 
    } catch (SignatureNotFoundException signatureNotFoundException) {
      return false;
    } 
  }
  
  public static X509Certificate[][] verify(String paramString) throws SignatureNotFoundException, SecurityException, IOException {
    VerifiedSigner verifiedSigner = verify(paramString, true);
    return verifiedSigner.certs;
  }
  
  public static X509Certificate[][] unsafeGetCertsWithoutVerification(String paramString) throws SignatureNotFoundException, SecurityException, IOException {
    VerifiedSigner verifiedSigner = verify(paramString, false);
    return verifiedSigner.certs;
  }
  
  public static VerifiedSigner verify(String paramString, boolean paramBoolean) throws SignatureNotFoundException, SecurityException, IOException {
    RandomAccessFile randomAccessFile = new RandomAccessFile(paramString, "r");
    try {
      return verify(randomAccessFile, paramBoolean);
    } finally {
      try {
        randomAccessFile.close();
      } finally {
        randomAccessFile = null;
      } 
    } 
  }
  
  private static VerifiedSigner verify(RandomAccessFile paramRandomAccessFile, boolean paramBoolean) throws SignatureNotFoundException, SecurityException, IOException {
    SignatureInfo signatureInfo = findSignature(paramRandomAccessFile);
    return verify(paramRandomAccessFile, signatureInfo, paramBoolean);
  }
  
  private static SignatureInfo findSignature(RandomAccessFile paramRandomAccessFile) throws IOException, SignatureNotFoundException {
    return ApkSigningBlockUtils.findSignature(paramRandomAccessFile, 1896449818);
  }
  
  private static VerifiedSigner verify(RandomAccessFile paramRandomAccessFile, SignatureInfo paramSignatureInfo, boolean paramBoolean) throws SecurityException, IOException {
    byte b = 0;
    ArrayMap<Object, Object> arrayMap = new ArrayMap<>();
    ArrayList<X509Certificate[]> arrayList = new ArrayList();
    try {
      CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
      try {
        StringBuilder stringBuilder;
        ByteBuffer byteBuffer = ApkSigningBlockUtils.getLengthPrefixedSlice(paramSignatureInfo.signatureBlock);
        while (byteBuffer.hasRemaining()) {
          b++;
          try {
            ByteBuffer byteBuffer1 = ApkSigningBlockUtils.getLengthPrefixedSlice(byteBuffer);
            X509Certificate[] arrayOfX509Certificate = verifySigner(byteBuffer1, (Map)arrayMap, certificateFactory);
            arrayList.add(arrayOfX509Certificate);
          } catch (IOException|java.nio.BufferUnderflowException|SecurityException iOException) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Failed to parse/verify signer #");
            stringBuilder.append(b);
            stringBuilder.append(" block");
            throw new SecurityException(stringBuilder.toString(), iOException);
          } 
        } 
        if (b >= 1) {
          if (!arrayMap.isEmpty()) {
            byte[] arrayOfByte2;
            if (paramBoolean)
              ApkSigningBlockUtils.verifyIntegrity((Map)arrayMap, (RandomAccessFile)iOException, (SignatureInfo)stringBuilder); 
            byteBuffer = null;
            if (arrayMap.containsKey(Integer.valueOf(3))) {
              arrayOfByte2 = (byte[])arrayMap.get(Integer.valueOf(3));
              long l = iOException.length();
              arrayOfByte2 = ApkSigningBlockUtils.parseVerityDigestAndVerifySourceLength(arrayOfByte2, l, (SignatureInfo)stringBuilder);
            } 
            byte[] arrayOfByte1 = ApkSigningBlockUtils.pickBestDigestForV4((Map)arrayMap);
            return 
              new VerifiedSigner(arrayList.<X509Certificate[]>toArray(new X509Certificate[arrayList.size()][]), arrayOfByte2, arrayOfByte1);
          } 
          throw new SecurityException("No content digests found");
        } 
        throw new SecurityException("No signers found");
      } catch (IOException iOException) {
        throw new SecurityException("Failed to read list of signers", iOException);
      } 
    } catch (CertificateException certificateException) {
      throw new RuntimeException("Failed to obtain X.509 CertificateFactory", certificateException);
    } 
  }
  
  private static X509Certificate[] verifySigner(ByteBuffer paramByteBuffer, Map<Integer, byte[]> paramMap, CertificateFactory paramCertificateFactory) throws SecurityException, IOException {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic getLengthPrefixedSlice : (Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    //   4: astore_3
    //   5: aload_0
    //   6: invokestatic getLengthPrefixedSlice : (Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    //   9: astore #4
    //   11: aload_0
    //   12: invokestatic readLengthPrefixedByteArray : (Ljava/nio/ByteBuffer;)[B
    //   15: astore #5
    //   17: new java/util/ArrayList
    //   20: dup
    //   21: invokespecial <init> : ()V
    //   24: astore #6
    //   26: aconst_null
    //   27: astore_0
    //   28: iconst_m1
    //   29: istore #7
    //   31: iconst_0
    //   32: istore #8
    //   34: aload #4
    //   36: invokevirtual hasRemaining : ()Z
    //   39: ifeq -> 180
    //   42: iinc #8, 1
    //   45: aload #4
    //   47: invokestatic getLengthPrefixedSlice : (Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    //   50: astore #9
    //   52: aload #9
    //   54: invokevirtual remaining : ()I
    //   57: bipush #8
    //   59: if_icmplt -> 130
    //   62: aload #9
    //   64: invokevirtual getInt : ()I
    //   67: istore #10
    //   69: aload #6
    //   71: iload #10
    //   73: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   76: invokeinterface add : (Ljava/lang/Object;)Z
    //   81: pop
    //   82: iload #10
    //   84: invokestatic isSupportedSignatureAlgorithm : (I)Z
    //   87: ifne -> 93
    //   90: goto -> 34
    //   93: iload #7
    //   95: iconst_m1
    //   96: if_icmpeq -> 113
    //   99: iload #7
    //   101: istore #11
    //   103: iload #10
    //   105: iload #7
    //   107: invokestatic compareSignatureAlgorithm : (II)I
    //   110: ifle -> 123
    //   113: iload #10
    //   115: istore #11
    //   117: aload #9
    //   119: invokestatic readLengthPrefixedByteArray : (Ljava/nio/ByteBuffer;)[B
    //   122: astore_0
    //   123: iload #11
    //   125: istore #7
    //   127: goto -> 34
    //   130: new java/lang/SecurityException
    //   133: astore_0
    //   134: aload_0
    //   135: ldc_w 'Signature record too short'
    //   138: invokespecial <init> : (Ljava/lang/String;)V
    //   141: aload_0
    //   142: athrow
    //   143: astore_1
    //   144: new java/lang/StringBuilder
    //   147: dup
    //   148: invokespecial <init> : ()V
    //   151: astore_0
    //   152: aload_0
    //   153: ldc_w 'Failed to parse signature record #'
    //   156: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   159: pop
    //   160: aload_0
    //   161: iload #8
    //   163: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   166: pop
    //   167: new java/lang/SecurityException
    //   170: dup
    //   171: aload_0
    //   172: invokevirtual toString : ()Ljava/lang/String;
    //   175: aload_1
    //   176: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   179: athrow
    //   180: iload #7
    //   182: iconst_m1
    //   183: if_icmpne -> 213
    //   186: iload #8
    //   188: ifne -> 202
    //   191: new java/lang/SecurityException
    //   194: dup
    //   195: ldc_w 'No signatures found'
    //   198: invokespecial <init> : (Ljava/lang/String;)V
    //   201: athrow
    //   202: new java/lang/SecurityException
    //   205: dup
    //   206: ldc_w 'No supported signatures found'
    //   209: invokespecial <init> : (Ljava/lang/String;)V
    //   212: athrow
    //   213: iload #7
    //   215: invokestatic getSignatureAlgorithmJcaKeyAlgorithm : (I)Ljava/lang/String;
    //   218: astore #12
    //   220: iload #7
    //   222: invokestatic getSignatureAlgorithmJcaSignatureAlgorithm : (I)Landroid/util/Pair;
    //   225: astore #13
    //   227: aload #13
    //   229: getfield first : Ljava/lang/Object;
    //   232: checkcast java/lang/String
    //   235: astore #9
    //   237: aload #13
    //   239: getfield second : Ljava/lang/Object;
    //   242: checkcast java/security/spec/AlgorithmParameterSpec
    //   245: astore #13
    //   247: aload #12
    //   249: invokestatic getInstance : (Ljava/lang/String;)Ljava/security/KeyFactory;
    //   252: astore #12
    //   254: new java/security/spec/X509EncodedKeySpec
    //   257: astore #14
    //   259: aload #14
    //   261: aload #5
    //   263: invokespecial <init> : ([B)V
    //   266: aload #12
    //   268: aload #14
    //   270: invokevirtual generatePublic : (Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
    //   273: astore #14
    //   275: aload #9
    //   277: invokestatic getInstance : (Ljava/lang/String;)Ljava/security/Signature;
    //   280: astore #12
    //   282: aload #12
    //   284: aload #14
    //   286: invokevirtual initVerify : (Ljava/security/PublicKey;)V
    //   289: aload #13
    //   291: ifnull -> 308
    //   294: aload #12
    //   296: aload #13
    //   298: invokevirtual setParameter : (Ljava/security/spec/AlgorithmParameterSpec;)V
    //   301: goto -> 308
    //   304: astore_0
    //   305: goto -> 828
    //   308: aload #12
    //   310: aload_3
    //   311: invokevirtual update : (Ljava/nio/ByteBuffer;)V
    //   314: aload #12
    //   316: aload_0
    //   317: invokevirtual verify : ([B)Z
    //   320: istore #15
    //   322: iload #15
    //   324: ifeq -> 792
    //   327: aload_3
    //   328: invokevirtual clear : ()Ljava/nio/Buffer;
    //   331: pop
    //   332: aload_3
    //   333: invokestatic getLengthPrefixedSlice : (Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    //   336: astore #13
    //   338: new java/util/ArrayList
    //   341: dup
    //   342: invokespecial <init> : ()V
    //   345: astore #9
    //   347: iconst_0
    //   348: istore #11
    //   350: aconst_null
    //   351: astore_0
    //   352: aload #13
    //   354: invokevirtual hasRemaining : ()Z
    //   357: ifeq -> 474
    //   360: iinc #11, 1
    //   363: aload #13
    //   365: invokestatic getLengthPrefixedSlice : (Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    //   368: astore #12
    //   370: aload #12
    //   372: invokevirtual remaining : ()I
    //   375: istore #8
    //   377: iload #8
    //   379: bipush #8
    //   381: if_icmplt -> 424
    //   384: aload #12
    //   386: invokevirtual getInt : ()I
    //   389: istore #8
    //   391: aload #9
    //   393: iload #8
    //   395: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   398: invokeinterface add : (Ljava/lang/Object;)Z
    //   403: pop
    //   404: iload #8
    //   406: iload #7
    //   408: if_icmpne -> 417
    //   411: aload #12
    //   413: invokestatic readLengthPrefixedByteArray : (Ljava/nio/ByteBuffer;)[B
    //   416: astore_0
    //   417: goto -> 352
    //   420: astore_0
    //   421: goto -> 438
    //   424: new java/io/IOException
    //   427: astore_0
    //   428: aload_0
    //   429: ldc_w 'Record too short'
    //   432: invokespecial <init> : (Ljava/lang/String;)V
    //   435: aload_0
    //   436: athrow
    //   437: astore_0
    //   438: new java/lang/StringBuilder
    //   441: dup
    //   442: invokespecial <init> : ()V
    //   445: astore_1
    //   446: aload_1
    //   447: ldc_w 'Failed to parse digest record #'
    //   450: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   453: pop
    //   454: aload_1
    //   455: iload #11
    //   457: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   460: pop
    //   461: new java/io/IOException
    //   464: dup
    //   465: aload_1
    //   466: invokevirtual toString : ()Ljava/lang/String;
    //   469: aload_0
    //   470: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   473: athrow
    //   474: aload #6
    //   476: aload #9
    //   478: invokeinterface equals : (Ljava/lang/Object;)Z
    //   483: ifeq -> 781
    //   486: iload #7
    //   488: invokestatic getSignatureAlgorithmContentDigestAlgorithm : (I)I
    //   491: istore #7
    //   493: aload_1
    //   494: iload #7
    //   496: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   499: aload_0
    //   500: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   505: checkcast [B
    //   508: astore_1
    //   509: aload_1
    //   510: ifnull -> 562
    //   513: aload_1
    //   514: aload_0
    //   515: invokestatic isEqual : ([B[B)Z
    //   518: ifeq -> 524
    //   521: goto -> 562
    //   524: new java/lang/StringBuilder
    //   527: dup
    //   528: invokespecial <init> : ()V
    //   531: astore_0
    //   532: aload_0
    //   533: iload #7
    //   535: invokestatic getContentDigestAlgorithmJcaDigestAlgorithm : (I)Ljava/lang/String;
    //   538: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   541: pop
    //   542: aload_0
    //   543: ldc_w ' contents digest does not match the digest specified by a preceding signer'
    //   546: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   549: pop
    //   550: new java/lang/SecurityException
    //   553: dup
    //   554: aload_0
    //   555: invokevirtual toString : ()Ljava/lang/String;
    //   558: invokespecial <init> : (Ljava/lang/String;)V
    //   561: athrow
    //   562: aload_3
    //   563: invokestatic getLengthPrefixedSlice : (Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    //   566: astore_1
    //   567: new java/util/ArrayList
    //   570: dup
    //   571: invokespecial <init> : ()V
    //   574: astore #4
    //   576: iconst_0
    //   577: istore #11
    //   579: aload #6
    //   581: astore_0
    //   582: aload_1
    //   583: invokevirtual hasRemaining : ()Z
    //   586: ifeq -> 688
    //   589: iinc #11, 1
    //   592: aload_1
    //   593: invokestatic readLengthPrefixedByteArray : (Ljava/nio/ByteBuffer;)[B
    //   596: astore #6
    //   598: new java/io/ByteArrayInputStream
    //   601: astore #9
    //   603: aload #9
    //   605: aload #6
    //   607: invokespecial <init> : ([B)V
    //   610: aload_2
    //   611: aload #9
    //   613: invokevirtual generateCertificate : (Ljava/io/InputStream;)Ljava/security/cert/Certificate;
    //   616: checkcast java/security/cert/X509Certificate
    //   619: astore #9
    //   621: new android/util/apk/VerbatimX509Certificate
    //   624: dup
    //   625: aload #9
    //   627: aload #6
    //   629: invokespecial <init> : (Ljava/security/cert/X509Certificate;[B)V
    //   632: astore #6
    //   634: aload #4
    //   636: aload #6
    //   638: invokeinterface add : (Ljava/lang/Object;)Z
    //   643: pop
    //   644: goto -> 582
    //   647: astore_0
    //   648: goto -> 652
    //   651: astore_0
    //   652: new java/lang/StringBuilder
    //   655: dup
    //   656: invokespecial <init> : ()V
    //   659: astore_1
    //   660: aload_1
    //   661: ldc_w 'Failed to decode certificate #'
    //   664: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   667: pop
    //   668: aload_1
    //   669: iload #11
    //   671: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   674: pop
    //   675: new java/lang/SecurityException
    //   678: dup
    //   679: aload_1
    //   680: invokevirtual toString : ()Ljava/lang/String;
    //   683: aload_0
    //   684: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   687: athrow
    //   688: aload #4
    //   690: invokeinterface isEmpty : ()Z
    //   695: ifne -> 770
    //   698: aload #4
    //   700: iconst_0
    //   701: invokeinterface get : (I)Ljava/lang/Object;
    //   706: checkcast java/security/cert/X509Certificate
    //   709: astore_0
    //   710: aload_0
    //   711: invokevirtual getPublicKey : ()Ljava/security/PublicKey;
    //   714: invokeinterface getEncoded : ()[B
    //   719: astore_0
    //   720: aload #5
    //   722: aload_0
    //   723: invokestatic equals : ([B[B)Z
    //   726: ifeq -> 759
    //   729: aload_3
    //   730: invokestatic getLengthPrefixedSlice : (Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    //   733: astore_0
    //   734: aload_0
    //   735: invokestatic verifyAdditionalAttributes : (Ljava/nio/ByteBuffer;)V
    //   738: aload #4
    //   740: aload #4
    //   742: invokeinterface size : ()I
    //   747: anewarray java/security/cert/X509Certificate
    //   750: invokeinterface toArray : ([Ljava/lang/Object;)[Ljava/lang/Object;
    //   755: checkcast [Ljava/security/cert/X509Certificate;
    //   758: areturn
    //   759: new java/lang/SecurityException
    //   762: dup
    //   763: ldc_w 'Public key mismatch between certificate and signature record'
    //   766: invokespecial <init> : (Ljava/lang/String;)V
    //   769: athrow
    //   770: new java/lang/SecurityException
    //   773: dup
    //   774: ldc_w 'No certificates listed'
    //   777: invokespecial <init> : (Ljava/lang/String;)V
    //   780: athrow
    //   781: new java/lang/SecurityException
    //   784: dup
    //   785: ldc_w 'Signature algorithms don't match between digests and signatures records'
    //   788: invokespecial <init> : (Ljava/lang/String;)V
    //   791: athrow
    //   792: new java/lang/StringBuilder
    //   795: dup
    //   796: invokespecial <init> : ()V
    //   799: astore_0
    //   800: aload_0
    //   801: aload #9
    //   803: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   806: pop
    //   807: aload_0
    //   808: ldc_w ' signature did not verify'
    //   811: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   814: pop
    //   815: new java/lang/SecurityException
    //   818: dup
    //   819: aload_0
    //   820: invokevirtual toString : ()Ljava/lang/String;
    //   823: invokespecial <init> : (Ljava/lang/String;)V
    //   826: athrow
    //   827: astore_0
    //   828: new java/lang/StringBuilder
    //   831: dup
    //   832: invokespecial <init> : ()V
    //   835: astore_1
    //   836: aload_1
    //   837: ldc_w 'Failed to verify '
    //   840: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   843: pop
    //   844: aload_1
    //   845: aload #9
    //   847: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   850: pop
    //   851: aload_1
    //   852: ldc_w ' signature'
    //   855: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   858: pop
    //   859: new java/lang/SecurityException
    //   862: dup
    //   863: aload_1
    //   864: invokevirtual toString : ()Ljava/lang/String;
    //   867: aload_0
    //   868: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   871: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #227	-> 0
    //   #228	-> 5
    //   #229	-> 11
    //   #231	-> 17
    //   #232	-> 17
    //   #233	-> 17
    //   #234	-> 17
    //   #235	-> 34
    //   #236	-> 42
    //   #238	-> 45
    //   #239	-> 52
    //   #242	-> 62
    //   #243	-> 69
    //   #244	-> 82
    //   #245	-> 90
    //   #247	-> 93
    //   #248	-> 99
    //   #249	-> 113
    //   #250	-> 117
    //   #256	-> 123
    //   #240	-> 130
    //   #252	-> 143
    //   #253	-> 144
    //   #258	-> 180
    //   #259	-> 186
    //   #260	-> 191
    //   #262	-> 202
    //   #266	-> 213
    //   #267	-> 220
    //   #268	-> 220
    //   #269	-> 227
    //   #270	-> 237
    //   #273	-> 247
    //   #274	-> 247
    //   #275	-> 266
    //   #276	-> 275
    //   #277	-> 282
    //   #278	-> 289
    //   #279	-> 294
    //   #283	-> 304
    //   #281	-> 308
    //   #282	-> 314
    //   #287	-> 322
    //   #288	-> 322
    //   #294	-> 327
    //   #295	-> 327
    //   #296	-> 332
    //   #297	-> 338
    //   #298	-> 347
    //   #299	-> 352
    //   #300	-> 360
    //   #302	-> 363
    //   #303	-> 370
    //   #306	-> 384
    //   #307	-> 391
    //   #308	-> 404
    //   #309	-> 411
    //   #313	-> 417
    //   #311	-> 420
    //   #304	-> 424
    //   #311	-> 437
    //   #312	-> 438
    //   #316	-> 474
    //   #320	-> 486
    //   #321	-> 493
    //   #322	-> 509
    //   #323	-> 513
    //   #324	-> 524
    //   #325	-> 532
    //   #322	-> 562
    //   #329	-> 562
    //   #330	-> 567
    //   #331	-> 576
    //   #332	-> 582
    //   #333	-> 589
    //   #334	-> 592
    //   #337	-> 598
    //   #338	-> 610
    //   #341	-> 621
    //   #342	-> 621
    //   #344	-> 634
    //   #345	-> 644
    //   #339	-> 647
    //   #340	-> 652
    //   #347	-> 688
    //   #350	-> 698
    //   #351	-> 710
    //   #352	-> 720
    //   #357	-> 729
    //   #358	-> 734
    //   #360	-> 738
    //   #353	-> 759
    //   #348	-> 770
    //   #317	-> 781
    //   #289	-> 792
    //   #283	-> 827
    //   #285	-> 828
    // Exception table:
    //   from	to	target	type
    //   45	52	143	java/io/IOException
    //   45	52	143	java/nio/BufferUnderflowException
    //   52	62	143	java/io/IOException
    //   52	62	143	java/nio/BufferUnderflowException
    //   62	69	143	java/io/IOException
    //   62	69	143	java/nio/BufferUnderflowException
    //   69	82	143	java/io/IOException
    //   69	82	143	java/nio/BufferUnderflowException
    //   82	90	143	java/io/IOException
    //   82	90	143	java/nio/BufferUnderflowException
    //   103	113	143	java/io/IOException
    //   103	113	143	java/nio/BufferUnderflowException
    //   117	123	143	java/io/IOException
    //   117	123	143	java/nio/BufferUnderflowException
    //   130	143	143	java/io/IOException
    //   130	143	143	java/nio/BufferUnderflowException
    //   247	266	827	java/security/NoSuchAlgorithmException
    //   247	266	827	java/security/spec/InvalidKeySpecException
    //   247	266	827	java/security/InvalidKeyException
    //   247	266	827	java/security/InvalidAlgorithmParameterException
    //   247	266	827	java/security/SignatureException
    //   266	275	827	java/security/NoSuchAlgorithmException
    //   266	275	827	java/security/spec/InvalidKeySpecException
    //   266	275	827	java/security/InvalidKeyException
    //   266	275	827	java/security/InvalidAlgorithmParameterException
    //   266	275	827	java/security/SignatureException
    //   275	282	827	java/security/NoSuchAlgorithmException
    //   275	282	827	java/security/spec/InvalidKeySpecException
    //   275	282	827	java/security/InvalidKeyException
    //   275	282	827	java/security/InvalidAlgorithmParameterException
    //   275	282	827	java/security/SignatureException
    //   282	289	827	java/security/NoSuchAlgorithmException
    //   282	289	827	java/security/spec/InvalidKeySpecException
    //   282	289	827	java/security/InvalidKeyException
    //   282	289	827	java/security/InvalidAlgorithmParameterException
    //   282	289	827	java/security/SignatureException
    //   294	301	304	java/security/NoSuchAlgorithmException
    //   294	301	304	java/security/spec/InvalidKeySpecException
    //   294	301	304	java/security/InvalidKeyException
    //   294	301	304	java/security/InvalidAlgorithmParameterException
    //   294	301	304	java/security/SignatureException
    //   308	314	827	java/security/NoSuchAlgorithmException
    //   308	314	827	java/security/spec/InvalidKeySpecException
    //   308	314	827	java/security/InvalidKeyException
    //   308	314	827	java/security/InvalidAlgorithmParameterException
    //   308	314	827	java/security/SignatureException
    //   314	322	827	java/security/NoSuchAlgorithmException
    //   314	322	827	java/security/spec/InvalidKeySpecException
    //   314	322	827	java/security/InvalidKeyException
    //   314	322	827	java/security/InvalidAlgorithmParameterException
    //   314	322	827	java/security/SignatureException
    //   363	370	437	java/io/IOException
    //   363	370	437	java/nio/BufferUnderflowException
    //   370	377	437	java/io/IOException
    //   370	377	437	java/nio/BufferUnderflowException
    //   384	391	420	java/io/IOException
    //   384	391	420	java/nio/BufferUnderflowException
    //   391	404	420	java/io/IOException
    //   391	404	420	java/nio/BufferUnderflowException
    //   411	417	420	java/io/IOException
    //   411	417	420	java/nio/BufferUnderflowException
    //   424	437	420	java/io/IOException
    //   424	437	420	java/nio/BufferUnderflowException
    //   598	610	651	java/security/cert/CertificateException
    //   610	621	647	java/security/cert/CertificateException
  }
  
  private static void verifyAdditionalAttributes(ByteBuffer paramByteBuffer) throws SecurityException, IOException {
    while (paramByteBuffer.hasRemaining()) {
      ByteBuffer byteBuffer = ApkSigningBlockUtils.getLengthPrefixedSlice(paramByteBuffer);
      if (byteBuffer.remaining() >= 4) {
        int i = byteBuffer.getInt();
        if (i != -1091571699)
          continue; 
        if (byteBuffer.remaining() >= 4) {
          i = byteBuffer.getInt();
          if (i != 3)
            continue; 
          throw new SecurityException("V2 signature indicates APK is signed using APK Signature Scheme v3, but none was found. Signature stripped?");
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("V2 Signature Scheme Stripping Protection Attribute  value too small.  Expected 4 bytes, but found ");
        stringBuilder1.append(byteBuffer.remaining());
        throw new IOException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Remaining buffer too short to contain additional attribute ID. Remaining: ");
      stringBuilder.append(byteBuffer.remaining());
      throw new IOException(stringBuilder.toString());
    } 
  }
  
  static byte[] getVerityRootHash(String paramString) throws IOException, SignatureNotFoundException, SecurityException {
    RandomAccessFile randomAccessFile = new RandomAccessFile(paramString, "r");
    try {
      findSignature(randomAccessFile);
      VerifiedSigner verifiedSigner = verify(randomAccessFile, false);
      return verifiedSigner.verityRootHash;
    } finally {
      try {
        randomAccessFile.close();
      } finally {
        randomAccessFile = null;
      } 
    } 
  }
  
  static byte[] generateApkVerity(String paramString, ByteBufferFactory paramByteBufferFactory) throws IOException, SignatureNotFoundException, SecurityException, DigestException, NoSuchAlgorithmException {
    RandomAccessFile randomAccessFile = new RandomAccessFile(paramString, "r");
    try {
      SignatureInfo signatureInfo = findSignature(randomAccessFile);
      return VerityBuilder.generateApkVerity(paramString, paramByteBufferFactory, signatureInfo);
    } finally {
      try {
        randomAccessFile.close();
      } finally {
        paramByteBufferFactory = null;
      } 
    } 
  }
  
  static byte[] generateApkVerityRootHash(String paramString) throws IOException, SignatureNotFoundException, DigestException, NoSuchAlgorithmException {
    RandomAccessFile randomAccessFile = new RandomAccessFile(paramString, "r");
    try {
      SignatureInfo signatureInfo = findSignature(randomAccessFile);
      VerifiedSigner verifiedSigner = verify(randomAccessFile, false);
      byte[] arrayOfByte = verifiedSigner.verityRootHash;
      if (arrayOfByte == null)
        return null; 
      arrayOfByte = verifiedSigner.verityRootHash;
      ByteBuffer byteBuffer = ByteBuffer.wrap(arrayOfByte);
      return VerityBuilder.generateApkVerityRootHash(randomAccessFile, byteBuffer, signatureInfo);
    } finally {
      try {
        randomAccessFile.close();
      } finally {
        randomAccessFile = null;
      } 
    } 
  }
  
  public static class VerifiedSigner {
    public final X509Certificate[][] certs;
    
    public final byte[] digest;
    
    public final byte[] verityRootHash;
    
    public VerifiedSigner(X509Certificate[][] param1ArrayOfX509Certificate, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) {
      this.certs = param1ArrayOfX509Certificate;
      this.verityRootHash = param1ArrayOfbyte1;
      this.digest = param1ArrayOfbyte2;
    }
  }
}
