package android.util.apk;

import android.os.incremental.IncrementalManager;
import android.os.incremental.V4Signature;
import android.util.Pair;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

public class ApkSignatureSchemeV4Verifier {
  public static VerifiedSigner extractCertificates(String paramString) throws SignatureNotFoundException, SecurityException {
    File file = new File(paramString);
    String str = file.getAbsolutePath();
    byte[] arrayOfByte = IncrementalManager.unsafeGetFileSignature(str);
    if (arrayOfByte != null && arrayOfByte.length != 0)
      try {
        V4Signature.SigningInfo signingInfo;
        V4Signature v4Signature = V4Signature.readFrom(arrayOfByte);
        if (v4Signature.isVersionSupported()) {
          V4Signature.HashingInfo hashingInfo = V4Signature.HashingInfo.fromByteArray(v4Signature.hashingInfo);
          signingInfo = V4Signature.SigningInfo.fromByteArray(v4Signature.signingInfo);
          byte[] arrayOfByte1 = V4Signature.getSigningData(file.length(), hashingInfo, signingInfo);
          return verifySigner(signingInfo, arrayOfByte1);
        } 
        SecurityException securityException = new SecurityException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("v4 signature version ");
        stringBuilder.append(((V4Signature)signingInfo).version);
        stringBuilder.append(" is not supported");
        this(stringBuilder.toString());
        throw securityException;
      } catch (IOException iOException) {
        throw new SignatureNotFoundException("Failed to read V4 signature.", iOException);
      }  
    throw new SignatureNotFoundException("Failed to obtain signature bytes from IncFS.");
  }
  
  private static VerifiedSigner verifySigner(V4Signature.SigningInfo paramSigningInfo, byte[] paramArrayOfbyte) throws SecurityException {
    if (ApkSigningBlockUtils.isSupportedSignatureAlgorithm(paramSigningInfo.signatureAlgorithmId)) {
      byte[] arrayOfByte4;
      int i = paramSigningInfo.signatureAlgorithmId;
      byte[] arrayOfByte1 = paramSigningInfo.signature;
      byte[] arrayOfByte2 = paramSigningInfo.publicKey;
      byte[] arrayOfByte3 = paramSigningInfo.certificate;
      String str1 = ApkSigningBlockUtils.getSignatureAlgorithmJcaKeyAlgorithm(i);
      Pair<String, ? extends AlgorithmParameterSpec> pair = ApkSigningBlockUtils.getSignatureAlgorithmJcaSignatureAlgorithm(i);
      String str2 = (String)pair.first;
      AlgorithmParameterSpec algorithmParameterSpec = (AlgorithmParameterSpec)pair.second;
      try {
        KeyFactory keyFactory = KeyFactory.getInstance(str1);
        X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec();
        this(arrayOfByte2);
        PublicKey publicKey = keyFactory.generatePublic(x509EncodedKeySpec);
        Signature signature = Signature.getInstance(str2);
        signature.initVerify(publicKey);
        if (algorithmParameterSpec != null)
          signature.setParameter(algorithmParameterSpec); 
        try {
          signature.update(paramArrayOfbyte);
          boolean bool = signature.verify(arrayOfByte1);
          if (bool)
            try {
              CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
              try {
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream();
                this(arrayOfByte3);
                X509Certificate x509Certificate = (X509Certificate)certificateFactory.generateCertificate(byteArrayInputStream);
                x509Certificate = new VerbatimX509Certificate(x509Certificate, arrayOfByte3);
                arrayOfByte4 = x509Certificate.getPublicKey().getEncoded();
                if (Arrays.equals(arrayOfByte2, arrayOfByte4)) {
                  byte[] arrayOfByte = paramSigningInfo.apkDigest;
                  return new VerifiedSigner(new Certificate[] { x509Certificate }, arrayOfByte);
                } 
                throw new SecurityException("Public key mismatch between certificate and signature record");
              } catch (CertificateException certificateException) {
                throw new SecurityException("Failed to decode certificate", certificateException);
              } 
            } catch (CertificateException certificateException) {
              throw new RuntimeException("Failed to obtain X.509 CertificateFactory", certificateException);
            }  
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append((String)arrayOfByte4);
          stringBuilder1.append(" signature did not verify");
          throw new SecurityException(stringBuilder1.toString());
        } catch (NoSuchAlgorithmException|java.security.spec.InvalidKeySpecException|java.security.InvalidKeyException|java.security.InvalidAlgorithmParameterException|java.security.SignatureException null) {}
      } catch (NoSuchAlgorithmException|java.security.spec.InvalidKeySpecException|java.security.InvalidKeyException|java.security.InvalidAlgorithmParameterException|java.security.SignatureException noSuchAlgorithmException) {}
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to verify ");
      stringBuilder.append((String)arrayOfByte4);
      stringBuilder.append(" signature");
      throw new SecurityException(stringBuilder.toString(), noSuchAlgorithmException);
    } 
    throw new SecurityException("No supported signatures found");
  }
  
  public static class VerifiedSigner {
    public byte[] apkDigest;
    
    public final Certificate[] certs;
    
    public VerifiedSigner(Certificate[] param1ArrayOfCertificate, byte[] param1ArrayOfbyte) {
      this.certs = param1ArrayOfCertificate;
      this.apkDigest = param1ArrayOfbyte;
    }
  }
}
