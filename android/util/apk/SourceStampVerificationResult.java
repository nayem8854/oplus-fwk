package android.util.apk;

import java.security.cert.Certificate;

public final class SourceStampVerificationResult {
  private final Certificate mCertificate;
  
  private final boolean mPresent;
  
  private final boolean mVerified;
  
  private SourceStampVerificationResult(boolean paramBoolean1, boolean paramBoolean2, Certificate paramCertificate) {
    this.mPresent = paramBoolean1;
    this.mVerified = paramBoolean2;
    this.mCertificate = paramCertificate;
  }
  
  public boolean isPresent() {
    return this.mPresent;
  }
  
  public boolean isVerified() {
    return this.mVerified;
  }
  
  public Certificate getCertificate() {
    return this.mCertificate;
  }
  
  public static SourceStampVerificationResult notPresent() {
    return new SourceStampVerificationResult(false, false, null);
  }
  
  public static SourceStampVerificationResult verified(Certificate paramCertificate) {
    return new SourceStampVerificationResult(true, true, paramCertificate);
  }
  
  public static SourceStampVerificationResult notVerified() {
    return new SourceStampVerificationResult(true, false, null);
  }
}
