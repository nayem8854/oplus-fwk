package android.util.apk;

import java.io.IOException;
import java.security.DigestException;

interface DataSource {
  void feedIntoDataDigester(DataDigester paramDataDigester, long paramLong, int paramInt) throws IOException, DigestException;
  
  long size();
}
