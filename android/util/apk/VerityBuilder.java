package android.util.apk;

import java.io.FileDescriptor;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.DigestException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public abstract class VerityBuilder {
  private static final int CHUNK_SIZE_BYTES = 4096;
  
  private static final byte[] DEFAULT_SALT = new byte[8];
  
  private static final int DIGEST_SIZE_BYTES = 32;
  
  private static final int FSVERITY_HEADER_SIZE_BYTES = 64;
  
  private static final String JCA_DIGEST_ALGORITHM = "SHA-256";
  
  private static final int MMAP_REGION_SIZE_BYTES = 1048576;
  
  private static final int ZIP_EOCD_CENTRAL_DIR_OFFSET_FIELD_OFFSET = 16;
  
  private static final int ZIP_EOCD_CENTRAL_DIR_OFFSET_FIELD_SIZE = 4;
  
  public static class VerityResult {
    public final int merkleTreeSize;
    
    public final byte[] rootHash;
    
    public final ByteBuffer verityData;
    
    private VerityResult(ByteBuffer param1ByteBuffer, int param1Int, byte[] param1ArrayOfbyte) {
      this.verityData = param1ByteBuffer;
      this.merkleTreeSize = param1Int;
      this.rootHash = param1ArrayOfbyte;
    }
  }
  
  public static VerityResult generateApkVerityTree(RandomAccessFile paramRandomAccessFile, SignatureInfo paramSignatureInfo, ByteBufferFactory paramByteBufferFactory) throws IOException, SecurityException, NoSuchAlgorithmException, DigestException {
    return generateVerityTreeInternal(paramRandomAccessFile, paramByteBufferFactory, paramSignatureInfo);
  }
  
  private static VerityResult generateVerityTreeInternal(RandomAccessFile paramRandomAccessFile, ByteBufferFactory paramByteBufferFactory, SignatureInfo paramSignatureInfo) throws IOException, SecurityException, NoSuchAlgorithmException, DigestException {
    long l1 = paramSignatureInfo.centralDirOffset, l2 = paramSignatureInfo.apkSigningBlockOffset;
    long l3 = paramRandomAccessFile.length();
    int[] arrayOfInt = calculateVerityLevelOffset(l3 - l1 - l2);
    int i = arrayOfInt[arrayOfInt.length - 1];
    ByteBuffer byteBuffer1 = paramByteBufferFactory.create(i + 4096);
    byteBuffer1.order(ByteOrder.LITTLE_ENDIAN);
    ByteBuffer byteBuffer2 = slice(byteBuffer1, 0, i);
    byte[] arrayOfByte = generateVerityTreeInternal(paramRandomAccessFile, paramSignatureInfo, DEFAULT_SALT, arrayOfInt, byteBuffer2);
    return new VerityResult(byteBuffer1, i, arrayOfByte);
  }
  
  static void generateApkVerityFooter(RandomAccessFile paramRandomAccessFile, SignatureInfo paramSignatureInfo, ByteBuffer paramByteBuffer) throws IOException {
    paramByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
    generateApkVerityHeader(paramByteBuffer, paramRandomAccessFile.length(), DEFAULT_SALT);
    long l1 = paramSignatureInfo.centralDirOffset, l2 = paramSignatureInfo.apkSigningBlockOffset;
    generateApkVerityExtensions(paramByteBuffer, paramSignatureInfo.apkSigningBlockOffset, l1 - l2, paramSignatureInfo.eocdOffset);
  }
  
  static byte[] generateApkVerityRootHash(RandomAccessFile paramRandomAccessFile, ByteBuffer paramByteBuffer, SignatureInfo paramSignatureInfo) throws NoSuchAlgorithmException, DigestException, IOException {
    assertSigningBlockAlignedAndHasFullPages(paramSignatureInfo);
    ByteBuffer byteBuffer = ByteBuffer.allocate(4096).order(ByteOrder.LITTLE_ENDIAN);
    generateApkVerityFooter(paramRandomAccessFile, paramSignatureInfo, byteBuffer);
    byteBuffer.flip();
    MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
    messageDigest.update(byteBuffer);
    messageDigest.update(paramByteBuffer);
    return messageDigest.digest();
  }
  
  static byte[] generateApkVerity(String paramString, ByteBufferFactory paramByteBufferFactory, SignatureInfo paramSignatureInfo) throws IOException, SignatureNotFoundException, SecurityException, DigestException, NoSuchAlgorithmException {
    RandomAccessFile randomAccessFile = new RandomAccessFile(paramString, "r");
    try {
      VerityResult verityResult = generateVerityTreeInternal(randomAccessFile, paramByteBufferFactory, paramSignatureInfo);
      ByteBuffer byteBuffer1 = verityResult.verityData;
      int i = verityResult.merkleTreeSize;
      ByteBuffer byteBuffer2 = verityResult.verityData;
      int j = byteBuffer2.limit();
      byteBuffer2 = slice(byteBuffer1, i, j);
      generateApkVerityFooter(randomAccessFile, paramSignatureInfo, byteBuffer2);
      byteBuffer2.putInt(byteBuffer2.position() + 4);
      verityResult.verityData.limit(verityResult.merkleTreeSize + byteBuffer2.position());
      return verityResult.rootHash;
    } finally {
      try {
        randomAccessFile.close();
      } finally {
        randomAccessFile = null;
      } 
    } 
  }
  
  class BufferedDigester implements DataDigester {
    private static final int BUFFER_SIZE = 4096;
    
    private int mBytesDigestedSinceReset;
    
    private final byte[] mDigestBuffer = new byte[32];
    
    private final MessageDigest mMd;
    
    private final ByteBuffer mOutput;
    
    private final byte[] mSalt;
    
    private BufferedDigester(VerityBuilder this$0, ByteBuffer param1ByteBuffer) throws NoSuchAlgorithmException {
      this.mSalt = (byte[])this$0;
      this.mOutput = param1ByteBuffer.slice();
      MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
      byte[] arrayOfByte = this.mSalt;
      if (arrayOfByte != null)
        messageDigest.update(arrayOfByte); 
      this.mBytesDigestedSinceReset = 0;
    }
    
    public void consume(ByteBuffer param1ByteBuffer) throws DigestException {
      int i = param1ByteBuffer.position();
      int j = param1ByteBuffer.remaining();
      while (j > 0) {
        int k = Math.min(j, 4096 - this.mBytesDigestedSinceReset);
        param1ByteBuffer.limit(param1ByteBuffer.position() + k);
        this.mMd.update(param1ByteBuffer);
        i += k;
        j -= k;
        this.mBytesDigestedSinceReset = k = this.mBytesDigestedSinceReset + k;
        if (k == 4096) {
          MessageDigest messageDigest = this.mMd;
          byte[] arrayOfByte2 = this.mDigestBuffer;
          messageDigest.digest(arrayOfByte2, 0, arrayOfByte2.length);
          this.mOutput.put(this.mDigestBuffer);
          byte[] arrayOfByte1 = this.mSalt;
          if (arrayOfByte1 != null)
            this.mMd.update(arrayOfByte1); 
          this.mBytesDigestedSinceReset = 0;
        } 
      } 
    }
    
    public void assertEmptyBuffer() throws DigestException {
      if (this.mBytesDigestedSinceReset == 0)
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Buffer is not empty: ");
      stringBuilder.append(this.mBytesDigestedSinceReset);
      throw new IllegalStateException(stringBuilder.toString());
    }
    
    private void fillUpLastOutputChunk() {
      int i = this.mOutput.position() % 4096;
      if (i == 0)
        return; 
      this.mOutput.put(ByteBuffer.allocate(4096 - i));
    }
  }
  
  private static void consumeByChunk(DataDigester paramDataDigester, DataSource paramDataSource, int paramInt) throws IOException, DigestException {
    long l1 = paramDataSource.size();
    long l2 = 0L;
    while (l1 > 0L) {
      int i = (int)Math.min(l1, paramInt);
      paramDataSource.feedIntoDataDigester(paramDataDigester, l2, i);
      l2 += i;
      l1 -= i;
    } 
  }
  
  private static void generateFsVerityDigestAtLeafLevel(RandomAccessFile paramRandomAccessFile, ByteBuffer paramByteBuffer) throws IOException, NoSuchAlgorithmException, DigestException {
    BufferedDigester bufferedDigester = new BufferedDigester(paramByteBuffer);
    MemoryMappedFileDataSource memoryMappedFileDataSource = new MemoryMappedFileDataSource(paramRandomAccessFile.getFD(), 0L, paramRandomAccessFile.length());
    consumeByChunk(bufferedDigester, memoryMappedFileDataSource, 1048576);
    int i = (int)(paramRandomAccessFile.length() % 4096L);
    if (i != 0)
      bufferedDigester.consume(ByteBuffer.allocate(4096 - i)); 
    bufferedDigester.assertEmptyBuffer();
    bufferedDigester.fillUpLastOutputChunk();
  }
  
  private static void generateApkVerityDigestAtLeafLevel(RandomAccessFile paramRandomAccessFile, SignatureInfo paramSignatureInfo, byte[] paramArrayOfbyte, ByteBuffer paramByteBuffer) throws IOException, NoSuchAlgorithmException, DigestException {
    BufferedDigester bufferedDigester = new BufferedDigester(paramByteBuffer);
    MemoryMappedFileDataSource memoryMappedFileDataSource2 = new MemoryMappedFileDataSource(paramRandomAccessFile.getFD(), 0L, paramSignatureInfo.apkSigningBlockOffset);
    consumeByChunk(bufferedDigester, memoryMappedFileDataSource2, 1048576);
    long l = paramSignatureInfo.eocdOffset + 16L;
    memoryMappedFileDataSource2 = new MemoryMappedFileDataSource(paramRandomAccessFile.getFD(), paramSignatureInfo.centralDirOffset, l - paramSignatureInfo.centralDirOffset);
    consumeByChunk(bufferedDigester, memoryMappedFileDataSource2, 1048576);
    ByteBuffer byteBuffer = ByteBuffer.allocate(4);
    ByteOrder byteOrder = ByteOrder.LITTLE_ENDIAN;
    byteBuffer = byteBuffer.order(byteOrder);
    byteBuffer.putInt(Math.toIntExact(paramSignatureInfo.apkSigningBlockOffset));
    byteBuffer.flip();
    bufferedDigester.consume(byteBuffer);
    l = 4L + l;
    FileDescriptor fileDescriptor = paramRandomAccessFile.getFD();
    MemoryMappedFileDataSource memoryMappedFileDataSource1 = new MemoryMappedFileDataSource(fileDescriptor, l, paramRandomAccessFile.length() - l);
    consumeByChunk(bufferedDigester, memoryMappedFileDataSource1, 1048576);
    int i = (int)(paramRandomAccessFile.length() % 4096L);
    if (i != 0)
      bufferedDigester.consume(ByteBuffer.allocate(4096 - i)); 
    bufferedDigester.assertEmptyBuffer();
    bufferedDigester.fillUpLastOutputChunk();
  }
  
  private static byte[] generateVerityTreeInternal(RandomAccessFile paramRandomAccessFile, SignatureInfo paramSignatureInfo, byte[] paramArrayOfbyte, int[] paramArrayOfint, ByteBuffer paramByteBuffer) throws IOException, NoSuchAlgorithmException, DigestException {
    assertSigningBlockAlignedAndHasFullPages(paramSignatureInfo);
    generateApkVerityDigestAtLeafLevel(paramRandomAccessFile, paramSignatureInfo, paramArrayOfbyte, slice(paramByteBuffer, paramArrayOfint[paramArrayOfint.length - 2], paramArrayOfint[paramArrayOfint.length - 1]));
    for (int i = paramArrayOfint.length - 3; i >= 0; i--) {
      ByteBuffer byteBuffer2 = slice(paramByteBuffer, paramArrayOfint[i + 1], paramArrayOfint[i + 2]);
      ByteBuffer byteBuffer1 = slice(paramByteBuffer, paramArrayOfint[i], paramArrayOfint[i + 1]);
      ByteBufferDataSource byteBufferDataSource = new ByteBufferDataSource(byteBuffer2);
      BufferedDigester bufferedDigester1 = new BufferedDigester(byteBuffer1);
      consumeByChunk(bufferedDigester1, byteBufferDataSource, 4096);
      bufferedDigester1.assertEmptyBuffer();
      bufferedDigester1.fillUpLastOutputChunk();
    } 
    byte[] arrayOfByte = new byte[32];
    BufferedDigester bufferedDigester = new BufferedDigester(ByteBuffer.wrap(arrayOfByte));
    bufferedDigester.consume(slice(paramByteBuffer, 0, 4096));
    bufferedDigester.assertEmptyBuffer();
    return arrayOfByte;
  }
  
  private static ByteBuffer generateApkVerityHeader(ByteBuffer paramByteBuffer, long paramLong, byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte.length == 8) {
      paramByteBuffer.put("TrueBrew".getBytes());
      paramByteBuffer.put((byte)1);
      paramByteBuffer.put((byte)0);
      paramByteBuffer.put((byte)12);
      paramByteBuffer.put((byte)7);
      paramByteBuffer.putShort((short)1);
      paramByteBuffer.putShort((short)1);
      paramByteBuffer.putInt(0);
      paramByteBuffer.putInt(0);
      paramByteBuffer.putLong(paramLong);
      paramByteBuffer.put((byte)2);
      paramByteBuffer.put((byte)0);
      paramByteBuffer.put(paramArrayOfbyte);
      skip(paramByteBuffer, 22);
      return paramByteBuffer;
    } 
    throw new IllegalArgumentException("salt is not 8 bytes long");
  }
  
  private static ByteBuffer generateApkVerityExtensions(ByteBuffer paramByteBuffer, long paramLong1, long paramLong2, long paramLong3) {
    paramByteBuffer.putInt(24);
    paramByteBuffer.putShort((short)1);
    skip(paramByteBuffer, 2);
    paramByteBuffer.putLong(paramLong1);
    paramByteBuffer.putLong(paramLong2);
    paramByteBuffer.putInt(20);
    paramByteBuffer.putShort((short)2);
    skip(paramByteBuffer, 2);
    paramByteBuffer.putLong(16L + paramLong3);
    paramByteBuffer.putInt(Math.toIntExact(paramLong1));
    byte b = 4;
    if (4 == 8)
      b = 0; 
    skip(paramByteBuffer, b);
    return paramByteBuffer;
  }
  
  private static int[] calculateVerityLevelOffset(long paramLong) {
    ArrayList<Long> arrayList = new ArrayList();
    do {
      paramLong = divideRoundup(paramLong, 4096L) * 32L;
      long l = divideRoundup(paramLong, 4096L);
      arrayList.add(Long.valueOf(l * 4096L));
    } while (paramLong > 4096L);
    int[] arrayOfInt = new int[arrayList.size() + 1];
    arrayOfInt[0] = 0;
    for (byte b = 0; b < arrayList.size(); b++) {
      int i = arrayOfInt[b];
      arrayOfInt[b + 1] = i + Math.toIntExact(((Long)arrayList.get(arrayList.size() - b - 1)).longValue());
    } 
    return arrayOfInt;
  }
  
  private static void assertSigningBlockAlignedAndHasFullPages(SignatureInfo paramSignatureInfo) {
    if (paramSignatureInfo.apkSigningBlockOffset % 4096L == 0L) {
      if ((paramSignatureInfo.centralDirOffset - paramSignatureInfo.apkSigningBlockOffset) % 4096L == 0L)
        return; 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Size of APK Signing Block is not a multiple of 4096: ");
      stringBuilder1.append(paramSignatureInfo.centralDirOffset - paramSignatureInfo.apkSigningBlockOffset);
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("APK Signing Block does not start at the page boundary: ");
    stringBuilder.append(paramSignatureInfo.apkSigningBlockOffset);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private static ByteBuffer slice(ByteBuffer paramByteBuffer, int paramInt1, int paramInt2) {
    paramByteBuffer = paramByteBuffer.duplicate();
    paramByteBuffer.position(0);
    paramByteBuffer.limit(paramInt2);
    paramByteBuffer.position(paramInt1);
    return paramByteBuffer.slice();
  }
  
  private static void skip(ByteBuffer paramByteBuffer, int paramInt) {
    paramByteBuffer.position(paramByteBuffer.position() + paramInt);
  }
  
  private static long divideRoundup(long paramLong1, long paramLong2) {
    return (paramLong1 + paramLong2 - 1L) / paramLong2;
  }
}
