package android.util.apk;

import android.util.Pair;
import android.util.Slog;
import android.util.jar.StrictJarFile;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.zip.ZipEntry;
import libcore.io.Streams;

public abstract class SourceStampVerifier {
  private static final int APK_SIGNATURE_SCHEME_V2_BLOCK_ID = 1896449818;
  
  private static final int APK_SIGNATURE_SCHEME_V3_BLOCK_ID = -262969152;
  
  private static final int SOURCE_STAMP_BLOCK_ID = 1845461005;
  
  private static final String SOURCE_STAMP_CERTIFICATE_HASH_ZIP_ENTRY_NAME = "stamp-cert-sha256";
  
  private static final String TAG = "SourceStampVerifier";
  
  private static final int VERSION_APK_SIGNATURE_SCHEME_V2 = 2;
  
  private static final int VERSION_APK_SIGNATURE_SCHEME_V3 = 3;
  
  private static final int VERSION_JAR_SIGNATURE_SCHEME = 1;
  
  public static SourceStampVerificationResult verify(List<String> paramList) {
    Certificate certificate;
    List list = null;
    for (Iterator<String> iterator = paramList.iterator(); iterator.hasNext(); ) {
      String str = iterator.next();
      SourceStampVerificationResult sourceStampVerificationResult = verify(str);
      if (!sourceStampVerificationResult.isPresent() || 
        !sourceStampVerificationResult.isVerified())
        return sourceStampVerificationResult; 
      if (paramList != null && 
        !paramList.equals(sourceStampVerificationResult.getCertificate()))
        return SourceStampVerificationResult.notVerified(); 
      certificate = sourceStampVerificationResult.getCertificate();
    } 
    return SourceStampVerificationResult.verified(certificate);
  }
  
  public static SourceStampVerificationResult verify(String paramString) {
    StrictJarFile strictJarFile1 = null, strictJarFile2 = null, strictJarFile3 = null;
    StrictJarFile strictJarFile4 = strictJarFile1, strictJarFile5 = strictJarFile2;
    try {
      RandomAccessFile randomAccessFile = new RandomAccessFile();
      strictJarFile4 = strictJarFile1;
      strictJarFile5 = strictJarFile2;
      this(paramString, "r");
      strictJarFile2 = strictJarFile3;
      try {
        strictJarFile4 = new StrictJarFile();
        strictJarFile2 = strictJarFile3;
        this(paramString, false, false);
        StrictJarFile strictJarFile6 = strictJarFile4;
        strictJarFile2 = strictJarFile6;
        byte[] arrayOfByte1 = getSourceStampCertificateDigest(strictJarFile6);
        if (arrayOfByte1 == null) {
          strictJarFile2 = strictJarFile6;
          SourceStampVerificationResult sourceStampVerificationResult1 = SourceStampVerificationResult.notPresent();
          strictJarFile7 = strictJarFile6;
          return sourceStampVerificationResult1;
        } 
        strictJarFile2 = strictJarFile6;
        byte[] arrayOfByte2 = getManifestBytes(strictJarFile6);
        strictJarFile2 = strictJarFile6;
        SourceStampVerificationResult sourceStampVerificationResult = verify(randomAccessFile, (byte[])strictJarFile7, arrayOfByte2);
        StrictJarFile strictJarFile7 = strictJarFile6;
        return sourceStampVerificationResult;
      } finally {
        try {
          randomAccessFile.close();
        } finally {
          strictJarFile3 = null;
          strictJarFile4 = strictJarFile2;
          strictJarFile5 = strictJarFile2;
        } 
        strictJarFile4 = strictJarFile2;
        strictJarFile5 = strictJarFile2;
      } 
    } catch (IOException iOException) {
      strictJarFile4 = strictJarFile5;
      SourceStampVerificationResult sourceStampVerificationResult = SourceStampVerificationResult.notPresent();
      closeApkJar(strictJarFile5);
      return sourceStampVerificationResult;
    } finally {}
    closeApkJar(strictJarFile4);
    throw paramString;
  }
  
  private static SourceStampVerificationResult verify(RandomAccessFile paramRandomAccessFile, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) {
    try {
      SignatureInfo signatureInfo = ApkSigningBlockUtils.findSignature(paramRandomAccessFile, 1845461005);
      Map<Integer, Map<Integer, byte[]>> map = getSignatureSchemeApkContentDigests(paramRandomAccessFile, paramArrayOfbyte2);
      map = (Map)getSignatureSchemeDigests(map);
      return verify(signatureInfo, (Map)map, paramArrayOfbyte1);
    } catch (IOException|SignatureNotFoundException|RuntimeException iOException) {
      return SourceStampVerificationResult.notVerified();
    } 
  }
  
  private static SourceStampVerificationResult verify(SignatureInfo paramSignatureInfo, Map<Integer, byte[]> paramMap, byte[] paramArrayOfbyte) throws SecurityException, IOException {
    ByteBuffer byteBuffer1 = paramSignatureInfo.signatureBlock;
    ByteBuffer byteBuffer2 = ApkSigningBlockUtils.getLengthPrefixedSlice(byteBuffer1);
    X509Certificate x509Certificate = verifySourceStampCertificate(byteBuffer2, paramArrayOfbyte);
    ByteBuffer byteBuffer3 = ApkSigningBlockUtils.getLengthPrefixedSlice(byteBuffer2);
    HashMap<Object, Object> hashMap = new HashMap<>();
    while (byteBuffer3.hasRemaining()) {
      byteBuffer2 = ApkSigningBlockUtils.getLengthPrefixedSlice(byteBuffer3);
      int i = byteBuffer2.getInt();
      hashMap.put(Integer.valueOf(i), byteBuffer2);
    } 
    for (Map.Entry<Integer, byte> entry : paramMap.entrySet()) {
      byte[] arrayOfByte;
      if (hashMap.containsKey(entry.getKey())) {
        byteBuffer2 = (ByteBuffer)hashMap.get(entry.getKey());
        arrayOfByte = (byte[])entry.getValue();
        verifySourceStampSignature(byteBuffer2, x509Certificate, arrayOfByte);
        continue;
      } 
      x509Certificate = (X509Certificate)arrayOfByte.getKey();
      throw new SecurityException(String.format("No signatures found for signature scheme %d", new Object[] { x509Certificate }));
    } 
    return SourceStampVerificationResult.verified(x509Certificate);
  }
  
  private static X509Certificate verifySourceStampCertificate(ByteBuffer paramByteBuffer, byte[] paramArrayOfbyte) throws IOException {
    try {
      CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
      byte[] arrayOfByte = ApkSigningBlockUtils.readLengthPrefixedByteArray(paramByteBuffer);
      try {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream();
        this(arrayOfByte);
        X509Certificate x509Certificate = (X509Certificate)certificateFactory.generateCertificate(byteArrayInputStream);
        byte[] arrayOfByte1 = computeSha256Digest(arrayOfByte);
        if (Arrays.equals(paramArrayOfbyte, arrayOfByte1))
          return new VerbatimX509Certificate(x509Certificate, arrayOfByte); 
        throw new SecurityException("Certificate mismatch between APK and signature block");
      } catch (CertificateException certificateException) {
        throw new SecurityException("Failed to decode certificate", certificateException);
      } 
    } catch (CertificateException certificateException) {
      throw new RuntimeException("Failed to obtain X.509 CertificateFactory", certificateException);
    } 
  }
  
  private static void verifySourceStampSignature(ByteBuffer paramByteBuffer, X509Certificate paramX509Certificate, byte[] paramArrayOfbyte) throws IOException {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic getLengthPrefixedSlice : (Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    //   4: astore_3
    //   5: iconst_0
    //   6: istore #4
    //   8: iconst_m1
    //   9: istore #5
    //   11: aconst_null
    //   12: astore_0
    //   13: aload_3
    //   14: invokevirtual hasRemaining : ()Z
    //   17: ifeq -> 144
    //   20: iinc #4, 1
    //   23: aload_3
    //   24: invokestatic getLengthPrefixedSlice : (Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    //   27: astore #6
    //   29: aload #6
    //   31: invokevirtual remaining : ()I
    //   34: bipush #8
    //   36: if_icmplt -> 94
    //   39: aload #6
    //   41: invokevirtual getInt : ()I
    //   44: istore #7
    //   46: iload #7
    //   48: invokestatic isSupportedSignatureAlgorithm : (I)Z
    //   51: ifne -> 57
    //   54: goto -> 13
    //   57: iload #5
    //   59: iconst_m1
    //   60: if_icmpeq -> 77
    //   63: iload #5
    //   65: istore #8
    //   67: iload #7
    //   69: iload #5
    //   71: invokestatic compareSignatureAlgorithm : (II)I
    //   74: ifle -> 87
    //   77: iload #7
    //   79: istore #8
    //   81: aload #6
    //   83: invokestatic readLengthPrefixedByteArray : (Ljava/nio/ByteBuffer;)[B
    //   86: astore_0
    //   87: iload #8
    //   89: istore #5
    //   91: goto -> 13
    //   94: new java/lang/SecurityException
    //   97: astore_0
    //   98: aload_0
    //   99: ldc_w 'Signature record too short'
    //   102: invokespecial <init> : (Ljava/lang/String;)V
    //   105: aload_0
    //   106: athrow
    //   107: astore_0
    //   108: new java/lang/StringBuilder
    //   111: dup
    //   112: invokespecial <init> : ()V
    //   115: astore_1
    //   116: aload_1
    //   117: ldc_w 'Failed to parse signature record #'
    //   120: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   123: pop
    //   124: aload_1
    //   125: iload #4
    //   127: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   130: pop
    //   131: new java/lang/SecurityException
    //   134: dup
    //   135: aload_1
    //   136: invokevirtual toString : ()Ljava/lang/String;
    //   139: aload_0
    //   140: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   143: athrow
    //   144: iload #5
    //   146: iconst_m1
    //   147: if_icmpne -> 177
    //   150: iload #4
    //   152: ifne -> 166
    //   155: new java/lang/SecurityException
    //   158: dup
    //   159: ldc_w 'No signatures found'
    //   162: invokespecial <init> : (Ljava/lang/String;)V
    //   165: athrow
    //   166: new java/lang/SecurityException
    //   169: dup
    //   170: ldc_w 'No supported signatures found'
    //   173: invokespecial <init> : (Ljava/lang/String;)V
    //   176: athrow
    //   177: iload #5
    //   179: invokestatic getSignatureAlgorithmJcaSignatureAlgorithm : (I)Landroid/util/Pair;
    //   182: astore #6
    //   184: aload #6
    //   186: getfield first : Ljava/lang/Object;
    //   189: checkcast java/lang/String
    //   192: astore_3
    //   193: aload #6
    //   195: getfield second : Ljava/lang/Object;
    //   198: checkcast java/security/spec/AlgorithmParameterSpec
    //   201: astore #6
    //   203: aload_1
    //   204: invokevirtual getPublicKey : ()Ljava/security/PublicKey;
    //   207: astore #9
    //   209: aload_3
    //   210: invokestatic getInstance : (Ljava/lang/String;)Ljava/security/Signature;
    //   213: astore_1
    //   214: aload_1
    //   215: aload #9
    //   217: invokevirtual initVerify : (Ljava/security/PublicKey;)V
    //   220: aload #6
    //   222: ifnull -> 231
    //   225: aload_1
    //   226: aload #6
    //   228: invokevirtual setParameter : (Ljava/security/spec/AlgorithmParameterSpec;)V
    //   231: aload_1
    //   232: aload_2
    //   233: invokevirtual update : ([B)V
    //   236: aload_1
    //   237: aload_0
    //   238: invokevirtual verify : ([B)Z
    //   241: istore #10
    //   243: iload #10
    //   245: ifeq -> 249
    //   248: return
    //   249: new java/lang/StringBuilder
    //   252: dup
    //   253: invokespecial <init> : ()V
    //   256: astore_0
    //   257: aload_0
    //   258: aload_3
    //   259: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   262: pop
    //   263: aload_0
    //   264: ldc_w ' signature did not verify'
    //   267: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   270: pop
    //   271: new java/lang/SecurityException
    //   274: dup
    //   275: aload_0
    //   276: invokevirtual toString : ()Ljava/lang/String;
    //   279: invokespecial <init> : (Ljava/lang/String;)V
    //   282: athrow
    //   283: astore_1
    //   284: new java/lang/StringBuilder
    //   287: dup
    //   288: invokespecial <init> : ()V
    //   291: astore_0
    //   292: aload_0
    //   293: ldc_w 'Failed to verify '
    //   296: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   299: pop
    //   300: aload_0
    //   301: aload_3
    //   302: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   305: pop
    //   306: aload_0
    //   307: ldc_w ' signature'
    //   310: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   313: pop
    //   314: new java/lang/SecurityException
    //   317: dup
    //   318: aload_0
    //   319: invokevirtual toString : ()Ljava/lang/String;
    //   322: aload_1
    //   323: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   326: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #242	-> 0
    //   #243	-> 5
    //   #244	-> 8
    //   #245	-> 11
    //   #246	-> 13
    //   #247	-> 20
    //   #249	-> 23
    //   #250	-> 29
    //   #253	-> 39
    //   #254	-> 46
    //   #255	-> 54
    //   #257	-> 57
    //   #258	-> 63
    //   #259	-> 77
    //   #260	-> 81
    //   #265	-> 87
    //   #251	-> 94
    //   #262	-> 107
    //   #263	-> 108
    //   #267	-> 144
    //   #268	-> 150
    //   #269	-> 155
    //   #271	-> 166
    //   #276	-> 177
    //   #277	-> 177
    //   #278	-> 184
    //   #279	-> 193
    //   #280	-> 203
    //   #283	-> 209
    //   #284	-> 214
    //   #285	-> 220
    //   #286	-> 225
    //   #288	-> 231
    //   #289	-> 236
    //   #296	-> 243
    //   #297	-> 243
    //   #300	-> 248
    //   #298	-> 249
    //   #290	-> 283
    //   #294	-> 284
    // Exception table:
    //   from	to	target	type
    //   23	29	107	java/io/IOException
    //   23	29	107	java/nio/BufferUnderflowException
    //   29	39	107	java/io/IOException
    //   29	39	107	java/nio/BufferUnderflowException
    //   39	46	107	java/io/IOException
    //   39	46	107	java/nio/BufferUnderflowException
    //   46	54	107	java/io/IOException
    //   46	54	107	java/nio/BufferUnderflowException
    //   67	77	107	java/io/IOException
    //   67	77	107	java/nio/BufferUnderflowException
    //   81	87	107	java/io/IOException
    //   81	87	107	java/nio/BufferUnderflowException
    //   94	107	107	java/io/IOException
    //   94	107	107	java/nio/BufferUnderflowException
    //   209	214	283	java/security/InvalidKeyException
    //   209	214	283	java/security/InvalidAlgorithmParameterException
    //   209	214	283	java/security/SignatureException
    //   209	214	283	java/security/NoSuchAlgorithmException
    //   214	220	283	java/security/InvalidKeyException
    //   214	220	283	java/security/InvalidAlgorithmParameterException
    //   214	220	283	java/security/SignatureException
    //   214	220	283	java/security/NoSuchAlgorithmException
    //   225	231	283	java/security/InvalidKeyException
    //   225	231	283	java/security/InvalidAlgorithmParameterException
    //   225	231	283	java/security/SignatureException
    //   225	231	283	java/security/NoSuchAlgorithmException
    //   231	236	283	java/security/InvalidKeyException
    //   231	236	283	java/security/InvalidAlgorithmParameterException
    //   231	236	283	java/security/SignatureException
    //   231	236	283	java/security/NoSuchAlgorithmException
    //   236	243	283	java/security/InvalidKeyException
    //   236	243	283	java/security/InvalidAlgorithmParameterException
    //   236	243	283	java/security/SignatureException
    //   236	243	283	java/security/NoSuchAlgorithmException
  }
  
  private static Map<Integer, Map<Integer, byte[]>> getSignatureSchemeApkContentDigests(RandomAccessFile paramRandomAccessFile, byte[] paramArrayOfbyte) throws IOException {
    HashMap<Object, Object> hashMap = new HashMap<>();
    try {
      SignatureInfo signatureInfo = ApkSigningBlockUtils.findSignature(paramRandomAccessFile, -262969152);
      ByteBuffer byteBuffer = signatureInfo.signatureBlock;
      Map<Integer, byte[]> map = getApkContentDigestsFromSignatureBlock(byteBuffer);
      hashMap.put(Integer.valueOf(3), map);
    } catch (SignatureNotFoundException signatureNotFoundException) {}
    try {
      SignatureInfo signatureInfo = ApkSigningBlockUtils.findSignature(paramRandomAccessFile, 1896449818);
      ByteBuffer byteBuffer = signatureInfo.signatureBlock;
      Map<Integer, byte[]> map = getApkContentDigestsFromSignatureBlock(byteBuffer);
      hashMap.put(Integer.valueOf(2), map);
    } catch (SignatureNotFoundException signatureNotFoundException) {}
    if (paramArrayOfbyte != null) {
      HashMap<Object, Object> hashMap1 = new HashMap<>();
      paramArrayOfbyte = computeSha256Digest(paramArrayOfbyte);
      hashMap1.put(Integer.valueOf(4), paramArrayOfbyte);
      hashMap.put(Integer.valueOf(1), hashMap1);
    } 
    return (Map)hashMap;
  }
  
  private static Map<Integer, byte[]> getApkContentDigestsFromSignatureBlock(ByteBuffer paramByteBuffer) throws IOException {
    HashMap<Object, Object> hashMap = new HashMap<>();
    paramByteBuffer = ApkSigningBlockUtils.getLengthPrefixedSlice(paramByteBuffer);
    while (paramByteBuffer.hasRemaining()) {
      ByteBuffer byteBuffer = ApkSigningBlockUtils.getLengthPrefixedSlice(paramByteBuffer);
      byteBuffer = ApkSigningBlockUtils.getLengthPrefixedSlice(byteBuffer);
      byteBuffer = ApkSigningBlockUtils.getLengthPrefixedSlice(byteBuffer);
      while (byteBuffer.hasRemaining()) {
        ByteBuffer byteBuffer1 = ApkSigningBlockUtils.getLengthPrefixedSlice(byteBuffer);
        int i = byteBuffer1.getInt();
        byte[] arrayOfByte = ApkSigningBlockUtils.readLengthPrefixedByteArray(byteBuffer1);
        i = ApkSigningBlockUtils.getSignatureAlgorithmContentDigestAlgorithm(i);
        hashMap.put(Integer.valueOf(i), arrayOfByte);
      } 
    } 
    return (Map)hashMap;
  }
  
  private static Map<Integer, byte[]> getSignatureSchemeDigests(Map<Integer, Map<Integer, byte[]>> paramMap) {
    HashMap<Object, Object> hashMap = new HashMap<>();
    for (Map.Entry<Integer, Map<Integer, byte[]>> entry : paramMap.entrySet()) {
      List<Pair<Integer, byte[]>> list = getApkDigests((Map<Integer, byte[]>)entry.getValue());
      Integer integer = (Integer)entry.getKey();
      byte[] arrayOfByte = encodeApkContentDigests(list);
      hashMap.put(integer, arrayOfByte);
    } 
    return (Map)hashMap;
  }
  
  private static List<Pair<Integer, byte[]>> getApkDigests(Map<Integer, byte[]> paramMap) {
    ArrayList<Pair<Integer, byte[]>> arrayList = new ArrayList();
    for (Map.Entry<Integer, byte> entry : paramMap.entrySet())
      arrayList.add(Pair.create((Integer)entry.getKey(), (byte[])entry.getValue())); 
    arrayList.sort(Comparator.comparing((Function<?, ? extends Comparable>)_$$Lambda$SourceStampVerifier$iTSqvmQISl76OGD_hCNerhN3N2A.INSTANCE));
    return arrayList;
  }
  
  private static byte[] getSourceStampCertificateDigest(StrictJarFile paramStrictJarFile) throws IOException {
    ZipEntry zipEntry = paramStrictJarFile.findEntry("stamp-cert-sha256");
    if (zipEntry == null)
      return null; 
    return Streams.readFully(paramStrictJarFile.getInputStream(zipEntry));
  }
  
  private static byte[] getManifestBytes(StrictJarFile paramStrictJarFile) throws IOException {
    ZipEntry zipEntry = paramStrictJarFile.findEntry("META-INF/MANIFEST.MF");
    if (zipEntry == null)
      return null; 
    return Streams.readFully(paramStrictJarFile.getInputStream(zipEntry));
  }
  
  private static byte[] encodeApkContentDigests(List<Pair<Integer, byte[]>> paramList) {
    int i = 0;
    for (Pair<Integer, byte[]> pair : paramList)
      i += ((byte[])pair.second).length + 12; 
    ByteBuffer byteBuffer = ByteBuffer.allocate(i);
    byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
    for (Pair<Integer, byte[]> pair : paramList) {
      byte[] arrayOfByte = (byte[])pair.second;
      byteBuffer.putInt(arrayOfByte.length + 8);
      byteBuffer.putInt(((Integer)pair.first).intValue());
      byteBuffer.putInt(arrayOfByte.length);
      byteBuffer.put(arrayOfByte);
    } 
    return byteBuffer.array();
  }
  
  private static byte[] computeSha256Digest(byte[] paramArrayOfbyte) {
    try {
      MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
      messageDigest.update(paramArrayOfbyte);
      return messageDigest.digest();
    } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
      throw new RuntimeException("Failed to find SHA-256", noSuchAlgorithmException);
    } 
  }
  
  private static void closeApkJar(StrictJarFile paramStrictJarFile) {
    if (paramStrictJarFile == null)
      return; 
    try {
      paramStrictJarFile.close();
    } catch (IOException iOException) {
      Slog.e("SourceStampVerifier", "Could not close APK jar", iOException);
    } 
  }
}
