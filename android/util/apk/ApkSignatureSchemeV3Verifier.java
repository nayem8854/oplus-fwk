package android.util.apk;

import android.util.ArrayMap;
import android.util.Pair;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.security.DigestException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.AlgorithmParameterSpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class ApkSignatureSchemeV3Verifier {
  private static final int APK_SIGNATURE_SCHEME_V3_BLOCK_ID = -262969152;
  
  private static final int PROOF_OF_ROTATION_ATTR_ID = 1000370060;
  
  public static final int SF_ATTRIBUTE_ANDROID_APK_SIGNED_ID = 3;
  
  public static boolean hasSignature(String paramString) throws IOException {
    try {
      RandomAccessFile randomAccessFile = new RandomAccessFile();
      this(paramString, "r");
      try {
        findSignature(randomAccessFile);
        return true;
      } finally {
        try {
          randomAccessFile.close();
        } finally {
          randomAccessFile = null;
        } 
      } 
    } catch (SignatureNotFoundException signatureNotFoundException) {
      return false;
    } 
  }
  
  public static VerifiedSigner verify(String paramString) throws SignatureNotFoundException, SecurityException, IOException {
    return verify(paramString, true);
  }
  
  public static VerifiedSigner unsafeGetCertsWithoutVerification(String paramString) throws SignatureNotFoundException, SecurityException, IOException {
    return verify(paramString, false);
  }
  
  private static VerifiedSigner verify(String paramString, boolean paramBoolean) throws SignatureNotFoundException, SecurityException, IOException {
    RandomAccessFile randomAccessFile = new RandomAccessFile(paramString, "r");
    try {
      return verify(randomAccessFile, paramBoolean);
    } finally {
      try {
        randomAccessFile.close();
      } finally {
        randomAccessFile = null;
      } 
    } 
  }
  
  private static VerifiedSigner verify(RandomAccessFile paramRandomAccessFile, boolean paramBoolean) throws SignatureNotFoundException, SecurityException, IOException {
    SignatureInfo signatureInfo = findSignature(paramRandomAccessFile);
    return verify(paramRandomAccessFile, signatureInfo, paramBoolean);
  }
  
  private static SignatureInfo findSignature(RandomAccessFile paramRandomAccessFile) throws IOException, SignatureNotFoundException {
    return ApkSigningBlockUtils.findSignature(paramRandomAccessFile, -262969152);
  }
  
  private static VerifiedSigner verify(RandomAccessFile paramRandomAccessFile, SignatureInfo paramSignatureInfo, boolean paramBoolean) throws SecurityException, IOException {
    byte b = 0;
    ArrayMap<Object, Object> arrayMap = new ArrayMap<>();
    VerifiedSigner verifiedSigner = null;
    try {
      CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
      try {
        StringBuilder stringBuilder;
        ByteBuffer byteBuffer = ApkSigningBlockUtils.getLengthPrefixedSlice(paramSignatureInfo.signatureBlock);
        while (byteBuffer.hasRemaining()) {
          try {
            ByteBuffer byteBuffer1 = ApkSigningBlockUtils.getLengthPrefixedSlice(byteBuffer);
            VerifiedSigner verifiedSigner1 = verifySigner(byteBuffer1, (Map)arrayMap, certificateFactory);
            b++;
          } catch (PlatformNotSupportedException platformNotSupportedException) {
          
          } catch (IOException|java.nio.BufferUnderflowException|SecurityException iOException) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Failed to parse/verify signer #");
            stringBuilder.append(b);
            stringBuilder.append(" block");
            throw new SecurityException(stringBuilder.toString(), iOException);
          } 
        } 
        if (b >= 1 && verifiedSigner != null) {
          if (b == 1) {
            if (!arrayMap.isEmpty()) {
              if (paramBoolean)
                ApkSigningBlockUtils.verifyIntegrity((Map)arrayMap, (RandomAccessFile)iOException, (SignatureInfo)stringBuilder); 
              if (arrayMap.containsKey(Integer.valueOf(3))) {
                byte[] arrayOfByte = (byte[])arrayMap.get(Integer.valueOf(3));
                long l = iOException.length();
                verifiedSigner.verityRootHash = ApkSigningBlockUtils.parseVerityDigestAndVerifySourceLength(arrayOfByte, l, (SignatureInfo)stringBuilder);
              } 
              verifiedSigner.digest = ApkSigningBlockUtils.pickBestDigestForV4((Map)arrayMap);
              return verifiedSigner;
            } 
            throw new SecurityException("No content digests found");
          } 
          throw new SecurityException("APK Signature Scheme V3 only supports one signer: multiple signers found.");
        } 
        throw new SecurityException("No signers found");
      } catch (IOException iOException) {
        throw new SecurityException("Failed to read list of signers", iOException);
      } 
    } catch (CertificateException certificateException) {
      throw new RuntimeException("Failed to obtain X.509 CertificateFactory", certificateException);
    } 
  }
  
  private static VerifiedSigner verifySigner(ByteBuffer paramByteBuffer, Map<Integer, byte[]> paramMap, CertificateFactory paramCertificateFactory) throws SecurityException, IOException, PlatformNotSupportedException {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic getLengthPrefixedSlice : (Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    //   4: astore_3
    //   5: aload_0
    //   6: invokevirtual getInt : ()I
    //   9: istore #4
    //   11: aload_0
    //   12: invokevirtual getInt : ()I
    //   15: istore #5
    //   17: getstatic android/os/Build$VERSION.SDK_INT : I
    //   20: iload #4
    //   22: if_icmplt -> 931
    //   25: getstatic android/os/Build$VERSION.SDK_INT : I
    //   28: iload #5
    //   30: if_icmpgt -> 931
    //   33: aload_0
    //   34: invokestatic getLengthPrefixedSlice : (Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    //   37: astore #6
    //   39: aload_0
    //   40: invokestatic readLengthPrefixedByteArray : (Ljava/nio/ByteBuffer;)[B
    //   43: astore #7
    //   45: new java/util/ArrayList
    //   48: dup
    //   49: invokespecial <init> : ()V
    //   52: astore #8
    //   54: aconst_null
    //   55: astore_0
    //   56: iconst_m1
    //   57: istore #9
    //   59: iconst_0
    //   60: istore #10
    //   62: aload #6
    //   64: invokevirtual hasRemaining : ()Z
    //   67: ifeq -> 208
    //   70: iinc #10, 1
    //   73: aload #6
    //   75: invokestatic getLengthPrefixedSlice : (Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    //   78: astore #11
    //   80: aload #11
    //   82: invokevirtual remaining : ()I
    //   85: bipush #8
    //   87: if_icmplt -> 158
    //   90: aload #11
    //   92: invokevirtual getInt : ()I
    //   95: istore #12
    //   97: aload #8
    //   99: iload #12
    //   101: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   104: invokeinterface add : (Ljava/lang/Object;)Z
    //   109: pop
    //   110: iload #12
    //   112: invokestatic isSupportedSignatureAlgorithm : (I)Z
    //   115: ifne -> 121
    //   118: goto -> 62
    //   121: iload #9
    //   123: iconst_m1
    //   124: if_icmpeq -> 141
    //   127: iload #9
    //   129: istore #13
    //   131: iload #12
    //   133: iload #9
    //   135: invokestatic compareSignatureAlgorithm : (II)I
    //   138: ifle -> 151
    //   141: iload #12
    //   143: istore #13
    //   145: aload #11
    //   147: invokestatic readLengthPrefixedByteArray : (Ljava/nio/ByteBuffer;)[B
    //   150: astore_0
    //   151: iload #13
    //   153: istore #9
    //   155: goto -> 62
    //   158: new java/lang/SecurityException
    //   161: astore_0
    //   162: aload_0
    //   163: ldc_w 'Signature record too short'
    //   166: invokespecial <init> : (Ljava/lang/String;)V
    //   169: aload_0
    //   170: athrow
    //   171: astore_1
    //   172: new java/lang/StringBuilder
    //   175: dup
    //   176: invokespecial <init> : ()V
    //   179: astore_0
    //   180: aload_0
    //   181: ldc_w 'Failed to parse signature record #'
    //   184: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   187: pop
    //   188: aload_0
    //   189: iload #10
    //   191: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   194: pop
    //   195: new java/lang/SecurityException
    //   198: dup
    //   199: aload_0
    //   200: invokevirtual toString : ()Ljava/lang/String;
    //   203: aload_1
    //   204: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   207: athrow
    //   208: iload #9
    //   210: iconst_m1
    //   211: if_icmpne -> 241
    //   214: iload #10
    //   216: ifne -> 230
    //   219: new java/lang/SecurityException
    //   222: dup
    //   223: ldc_w 'No signatures found'
    //   226: invokespecial <init> : (Ljava/lang/String;)V
    //   229: athrow
    //   230: new java/lang/SecurityException
    //   233: dup
    //   234: ldc_w 'No supported signatures found'
    //   237: invokespecial <init> : (Ljava/lang/String;)V
    //   240: athrow
    //   241: iload #9
    //   243: invokestatic getSignatureAlgorithmJcaKeyAlgorithm : (I)Ljava/lang/String;
    //   246: astore #6
    //   248: iload #9
    //   250: invokestatic getSignatureAlgorithmJcaSignatureAlgorithm : (I)Landroid/util/Pair;
    //   253: astore #14
    //   255: aload #14
    //   257: getfield first : Ljava/lang/Object;
    //   260: checkcast java/lang/String
    //   263: astore #11
    //   265: aload #14
    //   267: getfield second : Ljava/lang/Object;
    //   270: checkcast java/security/spec/AlgorithmParameterSpec
    //   273: astore #14
    //   275: aload #6
    //   277: invokestatic getInstance : (Ljava/lang/String;)Ljava/security/KeyFactory;
    //   280: astore #15
    //   282: new java/security/spec/X509EncodedKeySpec
    //   285: astore #16
    //   287: aload #16
    //   289: aload #7
    //   291: invokespecial <init> : ([B)V
    //   294: aload #15
    //   296: aload #16
    //   298: invokevirtual generatePublic : (Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
    //   301: astore #16
    //   303: aload #11
    //   305: invokestatic getInstance : (Ljava/lang/String;)Ljava/security/Signature;
    //   308: astore #15
    //   310: aload #15
    //   312: aload #16
    //   314: invokevirtual initVerify : (Ljava/security/PublicKey;)V
    //   317: aload #14
    //   319: ifnull -> 336
    //   322: aload #15
    //   324: aload #14
    //   326: invokevirtual setParameter : (Ljava/security/spec/AlgorithmParameterSpec;)V
    //   329: goto -> 336
    //   332: astore_0
    //   333: goto -> 887
    //   336: aload #15
    //   338: aload_3
    //   339: invokevirtual update : (Ljava/nio/ByteBuffer;)V
    //   342: aload #15
    //   344: aload_0
    //   345: invokevirtual verify : ([B)Z
    //   348: istore #17
    //   350: iload #17
    //   352: ifeq -> 851
    //   355: aload_3
    //   356: invokevirtual clear : ()Ljava/nio/Buffer;
    //   359: pop
    //   360: aload_3
    //   361: invokestatic getLengthPrefixedSlice : (Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    //   364: astore #15
    //   366: new java/util/ArrayList
    //   369: dup
    //   370: invokespecial <init> : ()V
    //   373: astore #11
    //   375: iconst_0
    //   376: istore #13
    //   378: aconst_null
    //   379: astore #14
    //   381: aload #15
    //   383: invokevirtual hasRemaining : ()Z
    //   386: ifeq -> 512
    //   389: iinc #13, 1
    //   392: aload #15
    //   394: invokestatic getLengthPrefixedSlice : (Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    //   397: astore #16
    //   399: aload #16
    //   401: invokevirtual remaining : ()I
    //   404: istore #12
    //   406: iload #12
    //   408: bipush #8
    //   410: if_icmplt -> 454
    //   413: aload #16
    //   415: invokevirtual getInt : ()I
    //   418: istore #12
    //   420: aload #11
    //   422: iload #12
    //   424: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   427: invokeinterface add : (Ljava/lang/Object;)Z
    //   432: pop
    //   433: iload #12
    //   435: iload #9
    //   437: if_icmpne -> 447
    //   440: aload #16
    //   442: invokestatic readLengthPrefixedByteArray : (Ljava/nio/ByteBuffer;)[B
    //   445: astore #14
    //   447: goto -> 381
    //   450: astore_0
    //   451: goto -> 476
    //   454: new java/io/IOException
    //   457: astore_0
    //   458: aload_0
    //   459: ldc_w 'Record too short'
    //   462: invokespecial <init> : (Ljava/lang/String;)V
    //   465: aload_0
    //   466: athrow
    //   467: astore_0
    //   468: goto -> 476
    //   471: astore_0
    //   472: goto -> 476
    //   475: astore_0
    //   476: new java/lang/StringBuilder
    //   479: dup
    //   480: invokespecial <init> : ()V
    //   483: astore_1
    //   484: aload_1
    //   485: ldc_w 'Failed to parse digest record #'
    //   488: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   491: pop
    //   492: aload_1
    //   493: iload #13
    //   495: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   498: pop
    //   499: new java/io/IOException
    //   502: dup
    //   503: aload_1
    //   504: invokevirtual toString : ()Ljava/lang/String;
    //   507: aload_0
    //   508: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   511: athrow
    //   512: aload #8
    //   514: aload #11
    //   516: invokeinterface equals : (Ljava/lang/Object;)Z
    //   521: ifeq -> 840
    //   524: iload #9
    //   526: invokestatic getSignatureAlgorithmContentDigestAlgorithm : (I)I
    //   529: istore #10
    //   531: aload_1
    //   532: iload #10
    //   534: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   537: aload #14
    //   539: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   544: checkcast [B
    //   547: astore_0
    //   548: aload_0
    //   549: ifnull -> 602
    //   552: aload_0
    //   553: aload #14
    //   555: invokestatic isEqual : ([B[B)Z
    //   558: ifeq -> 564
    //   561: goto -> 602
    //   564: new java/lang/StringBuilder
    //   567: dup
    //   568: invokespecial <init> : ()V
    //   571: astore_0
    //   572: aload_0
    //   573: iload #10
    //   575: invokestatic getContentDigestAlgorithmJcaDigestAlgorithm : (I)Ljava/lang/String;
    //   578: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   581: pop
    //   582: aload_0
    //   583: ldc_w ' contents digest does not match the digest specified by a preceding signer'
    //   586: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   589: pop
    //   590: new java/lang/SecurityException
    //   593: dup
    //   594: aload_0
    //   595: invokevirtual toString : ()Ljava/lang/String;
    //   598: invokespecial <init> : (Ljava/lang/String;)V
    //   601: athrow
    //   602: aload_3
    //   603: invokestatic getLengthPrefixedSlice : (Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    //   606: astore_0
    //   607: new java/util/ArrayList
    //   610: dup
    //   611: invokespecial <init> : ()V
    //   614: astore_1
    //   615: iconst_0
    //   616: istore #13
    //   618: aload_0
    //   619: invokevirtual hasRemaining : ()Z
    //   622: ifeq -> 719
    //   625: iinc #13, 1
    //   628: aload_0
    //   629: invokestatic readLengthPrefixedByteArray : (Ljava/nio/ByteBuffer;)[B
    //   632: astore #6
    //   634: new java/io/ByteArrayInputStream
    //   637: astore #11
    //   639: aload #11
    //   641: aload #6
    //   643: invokespecial <init> : ([B)V
    //   646: aload_2
    //   647: aload #11
    //   649: invokevirtual generateCertificate : (Ljava/io/InputStream;)Ljava/security/cert/Certificate;
    //   652: checkcast java/security/cert/X509Certificate
    //   655: astore #11
    //   657: new android/util/apk/VerbatimX509Certificate
    //   660: dup
    //   661: aload #11
    //   663: aload #6
    //   665: invokespecial <init> : (Ljava/security/cert/X509Certificate;[B)V
    //   668: astore #6
    //   670: aload_1
    //   671: aload #6
    //   673: invokeinterface add : (Ljava/lang/Object;)Z
    //   678: pop
    //   679: goto -> 618
    //   682: astore_1
    //   683: new java/lang/StringBuilder
    //   686: dup
    //   687: invokespecial <init> : ()V
    //   690: astore_0
    //   691: aload_0
    //   692: ldc_w 'Failed to decode certificate #'
    //   695: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   698: pop
    //   699: aload_0
    //   700: iload #13
    //   702: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   705: pop
    //   706: new java/lang/SecurityException
    //   709: dup
    //   710: aload_0
    //   711: invokevirtual toString : ()Ljava/lang/String;
    //   714: aload_1
    //   715: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   718: athrow
    //   719: aload_1
    //   720: invokeinterface isEmpty : ()Z
    //   725: ifne -> 829
    //   728: aload_1
    //   729: iconst_0
    //   730: invokeinterface get : (I)Ljava/lang/Object;
    //   735: checkcast java/security/cert/X509Certificate
    //   738: astore_0
    //   739: aload_0
    //   740: invokevirtual getPublicKey : ()Ljava/security/PublicKey;
    //   743: invokeinterface getEncoded : ()[B
    //   748: astore_0
    //   749: aload #7
    //   751: aload_0
    //   752: invokestatic equals : ([B[B)Z
    //   755: ifeq -> 818
    //   758: aload_3
    //   759: invokevirtual getInt : ()I
    //   762: istore #9
    //   764: iload #9
    //   766: iload #4
    //   768: if_icmpne -> 807
    //   771: aload_3
    //   772: invokevirtual getInt : ()I
    //   775: istore #9
    //   777: iload #9
    //   779: iload #5
    //   781: if_icmpne -> 796
    //   784: aload_3
    //   785: invokestatic getLengthPrefixedSlice : (Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    //   788: astore_0
    //   789: aload_0
    //   790: aload_1
    //   791: aload_2
    //   792: invokestatic verifyAdditionalAttributes : (Ljava/nio/ByteBuffer;Ljava/util/List;Ljava/security/cert/CertificateFactory;)Landroid/util/apk/ApkSignatureSchemeV3Verifier$VerifiedSigner;
    //   795: areturn
    //   796: new java/lang/SecurityException
    //   799: dup
    //   800: ldc_w 'maxSdkVersion mismatch between signed and unsigned in v3 signer block.'
    //   803: invokespecial <init> : (Ljava/lang/String;)V
    //   806: athrow
    //   807: new java/lang/SecurityException
    //   810: dup
    //   811: ldc_w 'minSdkVersion mismatch between signed and unsigned in v3 signer block.'
    //   814: invokespecial <init> : (Ljava/lang/String;)V
    //   817: athrow
    //   818: new java/lang/SecurityException
    //   821: dup
    //   822: ldc_w 'Public key mismatch between certificate and signature record'
    //   825: invokespecial <init> : (Ljava/lang/String;)V
    //   828: athrow
    //   829: new java/lang/SecurityException
    //   832: dup
    //   833: ldc_w 'No certificates listed'
    //   836: invokespecial <init> : (Ljava/lang/String;)V
    //   839: athrow
    //   840: new java/lang/SecurityException
    //   843: dup
    //   844: ldc_w 'Signature algorithms don't match between digests and signatures records'
    //   847: invokespecial <init> : (Ljava/lang/String;)V
    //   850: athrow
    //   851: new java/lang/StringBuilder
    //   854: dup
    //   855: invokespecial <init> : ()V
    //   858: astore_0
    //   859: aload_0
    //   860: aload #11
    //   862: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   865: pop
    //   866: aload_0
    //   867: ldc_w ' signature did not verify'
    //   870: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   873: pop
    //   874: new java/lang/SecurityException
    //   877: dup
    //   878: aload_0
    //   879: invokevirtual toString : ()Ljava/lang/String;
    //   882: invokespecial <init> : (Ljava/lang/String;)V
    //   885: athrow
    //   886: astore_0
    //   887: new java/lang/StringBuilder
    //   890: dup
    //   891: invokespecial <init> : ()V
    //   894: astore_1
    //   895: aload_1
    //   896: ldc_w 'Failed to verify '
    //   899: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   902: pop
    //   903: aload_1
    //   904: aload #11
    //   906: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   909: pop
    //   910: aload_1
    //   911: ldc_w ' signature'
    //   914: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   917: pop
    //   918: new java/lang/SecurityException
    //   921: dup
    //   922: aload_1
    //   923: invokevirtual toString : ()Ljava/lang/String;
    //   926: aload_0
    //   927: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   930: athrow
    //   931: new java/lang/StringBuilder
    //   934: dup
    //   935: invokespecial <init> : ()V
    //   938: astore_0
    //   939: aload_0
    //   940: ldc_w 'Signer not supported by this platform version. This platform: '
    //   943: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   946: pop
    //   947: aload_0
    //   948: getstatic android/os/Build$VERSION.SDK_INT : I
    //   951: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   954: pop
    //   955: aload_0
    //   956: ldc_w ', signer minSdkVersion: '
    //   959: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   962: pop
    //   963: aload_0
    //   964: iload #4
    //   966: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   969: pop
    //   970: aload_0
    //   971: ldc_w ', maxSdkVersion: '
    //   974: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   977: pop
    //   978: aload_0
    //   979: iload #5
    //   981: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   984: pop
    //   985: new android/util/apk/ApkSignatureSchemeV3Verifier$PlatformNotSupportedException
    //   988: dup
    //   989: aload_0
    //   990: invokevirtual toString : ()Ljava/lang/String;
    //   993: invokespecial <init> : (Ljava/lang/String;)V
    //   996: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #225	-> 0
    //   #226	-> 5
    //   #227	-> 11
    //   #229	-> 17
    //   #238	-> 33
    //   #239	-> 39
    //   #241	-> 45
    //   #242	-> 45
    //   #243	-> 45
    //   #244	-> 45
    //   #245	-> 62
    //   #246	-> 70
    //   #248	-> 73
    //   #249	-> 80
    //   #252	-> 90
    //   #253	-> 97
    //   #254	-> 110
    //   #255	-> 118
    //   #257	-> 121
    //   #258	-> 127
    //   #259	-> 141
    //   #260	-> 145
    //   #266	-> 151
    //   #250	-> 158
    //   #262	-> 171
    //   #263	-> 172
    //   #268	-> 208
    //   #269	-> 214
    //   #270	-> 219
    //   #272	-> 230
    //   #276	-> 241
    //   #277	-> 248
    //   #278	-> 248
    //   #279	-> 255
    //   #280	-> 265
    //   #283	-> 275
    //   #284	-> 275
    //   #285	-> 294
    //   #286	-> 303
    //   #287	-> 310
    //   #288	-> 317
    //   #289	-> 322
    //   #293	-> 332
    //   #291	-> 336
    //   #292	-> 342
    //   #297	-> 350
    //   #298	-> 350
    //   #304	-> 355
    //   #305	-> 355
    //   #306	-> 360
    //   #307	-> 366
    //   #308	-> 375
    //   #309	-> 381
    //   #310	-> 389
    //   #312	-> 392
    //   #313	-> 399
    //   #316	-> 413
    //   #317	-> 420
    //   #318	-> 433
    //   #319	-> 440
    //   #323	-> 447
    //   #321	-> 450
    //   #314	-> 454
    //   #321	-> 467
    //   #322	-> 476
    //   #326	-> 512
    //   #330	-> 524
    //   #331	-> 531
    //   #332	-> 548
    //   #333	-> 552
    //   #334	-> 564
    //   #335	-> 572
    //   #332	-> 602
    //   #339	-> 602
    //   #340	-> 607
    //   #341	-> 615
    //   #342	-> 618
    //   #343	-> 625
    //   #344	-> 628
    //   #347	-> 634
    //   #348	-> 646
    //   #351	-> 657
    //   #352	-> 657
    //   #354	-> 670
    //   #355	-> 679
    //   #349	-> 682
    //   #350	-> 683
    //   #357	-> 719
    //   #360	-> 728
    //   #361	-> 739
    //   #362	-> 749
    //   #367	-> 758
    //   #368	-> 764
    //   #373	-> 771
    //   #374	-> 777
    //   #379	-> 784
    //   #380	-> 789
    //   #375	-> 796
    //   #369	-> 807
    //   #363	-> 818
    //   #358	-> 829
    //   #327	-> 840
    //   #299	-> 851
    //   #293	-> 886
    //   #295	-> 887
    //   #229	-> 931
    //   #231	-> 931
    // Exception table:
    //   from	to	target	type
    //   73	80	171	java/io/IOException
    //   73	80	171	java/nio/BufferUnderflowException
    //   80	90	171	java/io/IOException
    //   80	90	171	java/nio/BufferUnderflowException
    //   90	97	171	java/io/IOException
    //   90	97	171	java/nio/BufferUnderflowException
    //   97	110	171	java/io/IOException
    //   97	110	171	java/nio/BufferUnderflowException
    //   110	118	171	java/io/IOException
    //   110	118	171	java/nio/BufferUnderflowException
    //   131	141	171	java/io/IOException
    //   131	141	171	java/nio/BufferUnderflowException
    //   145	151	171	java/io/IOException
    //   145	151	171	java/nio/BufferUnderflowException
    //   158	171	171	java/io/IOException
    //   158	171	171	java/nio/BufferUnderflowException
    //   275	294	886	java/security/NoSuchAlgorithmException
    //   275	294	886	java/security/spec/InvalidKeySpecException
    //   275	294	886	java/security/InvalidKeyException
    //   275	294	886	java/security/InvalidAlgorithmParameterException
    //   275	294	886	java/security/SignatureException
    //   294	303	886	java/security/NoSuchAlgorithmException
    //   294	303	886	java/security/spec/InvalidKeySpecException
    //   294	303	886	java/security/InvalidKeyException
    //   294	303	886	java/security/InvalidAlgorithmParameterException
    //   294	303	886	java/security/SignatureException
    //   303	310	886	java/security/NoSuchAlgorithmException
    //   303	310	886	java/security/spec/InvalidKeySpecException
    //   303	310	886	java/security/InvalidKeyException
    //   303	310	886	java/security/InvalidAlgorithmParameterException
    //   303	310	886	java/security/SignatureException
    //   310	317	886	java/security/NoSuchAlgorithmException
    //   310	317	886	java/security/spec/InvalidKeySpecException
    //   310	317	886	java/security/InvalidKeyException
    //   310	317	886	java/security/InvalidAlgorithmParameterException
    //   310	317	886	java/security/SignatureException
    //   322	329	332	java/security/NoSuchAlgorithmException
    //   322	329	332	java/security/spec/InvalidKeySpecException
    //   322	329	332	java/security/InvalidKeyException
    //   322	329	332	java/security/InvalidAlgorithmParameterException
    //   322	329	332	java/security/SignatureException
    //   336	342	886	java/security/NoSuchAlgorithmException
    //   336	342	886	java/security/spec/InvalidKeySpecException
    //   336	342	886	java/security/InvalidKeyException
    //   336	342	886	java/security/InvalidAlgorithmParameterException
    //   336	342	886	java/security/SignatureException
    //   342	350	886	java/security/NoSuchAlgorithmException
    //   342	350	886	java/security/spec/InvalidKeySpecException
    //   342	350	886	java/security/InvalidKeyException
    //   342	350	886	java/security/InvalidAlgorithmParameterException
    //   342	350	886	java/security/SignatureException
    //   392	399	475	java/io/IOException
    //   392	399	475	java/nio/BufferUnderflowException
    //   399	406	471	java/io/IOException
    //   399	406	471	java/nio/BufferUnderflowException
    //   413	420	450	java/io/IOException
    //   413	420	450	java/nio/BufferUnderflowException
    //   420	433	467	java/io/IOException
    //   420	433	467	java/nio/BufferUnderflowException
    //   440	447	467	java/io/IOException
    //   440	447	467	java/nio/BufferUnderflowException
    //   454	467	467	java/io/IOException
    //   454	467	467	java/nio/BufferUnderflowException
    //   634	646	682	java/security/cert/CertificateException
    //   646	657	682	java/security/cert/CertificateException
  }
  
  private static VerifiedSigner verifyAdditionalAttributes(ByteBuffer paramByteBuffer, List<X509Certificate> paramList, CertificateFactory paramCertificateFactory) throws IOException {
    VerifiedProofOfRotation verifiedProofOfRotation;
    X509Certificate[] arrayOfX509Certificate = paramList.<X509Certificate>toArray(new X509Certificate[paramList.size()]);
    paramList = null;
    while (paramByteBuffer.hasRemaining()) {
      byte[] arrayOfByte;
      ByteBuffer byteBuffer = ApkSigningBlockUtils.getLengthPrefixedSlice(paramByteBuffer);
      if (byteBuffer.remaining() >= 4) {
        int i = byteBuffer.getInt();
        if (i != 1000370060)
          continue; 
        if (paramList == null) {
          verifiedProofOfRotation = verifyProofOfRotationStruct(byteBuffer, paramCertificateFactory);
          try {
            if (verifiedProofOfRotation.certs.size() > 0) {
              List<X509Certificate> list1 = verifiedProofOfRotation.certs, list2 = verifiedProofOfRotation.certs;
              arrayOfByte = ((X509Certificate)list1.get(list2.size() - 1)).getEncoded();
              X509Certificate x509Certificate = arrayOfX509Certificate[0];
              byte[] arrayOfByte1 = x509Certificate.getEncoded();
              if (Arrays.equals(arrayOfByte, arrayOfByte1))
                continue; 
              SecurityException securityException = new SecurityException();
              this("Terminal certificate in Proof-of-rotation record does not match APK signing certificate");
              throw securityException;
            } 
          } catch (CertificateEncodingException certificateEncodingException) {
            throw new SecurityException("Failed to encode certificate when comparing Proof-of-rotation record and signing certificate", certificateEncodingException);
          } 
          continue;
        } 
        throw new SecurityException("Encountered multiple Proof-of-rotation records when verifying APK Signature Scheme v3 signature");
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Remaining buffer too short to contain additional attribute ID. Remaining: ");
      stringBuilder.append(arrayOfByte.remaining());
      throw new IOException(stringBuilder.toString());
    } 
    return new VerifiedSigner(arrayOfX509Certificate, verifiedProofOfRotation);
  }
  
  private static VerifiedProofOfRotation verifyProofOfRotationStruct(ByteBuffer paramByteBuffer, CertificateFactory paramCertificateFactory) throws SecurityException, IOException {
    int i = 0, j = 0, k = 0;
    int m = -1;
    X509Certificate x509Certificate = null;
    ArrayList<VerbatimX509Certificate> arrayList = new ArrayList();
    ArrayList<Integer> arrayList1 = new ArrayList();
    int n = i, i1 = j;
    try {
      paramByteBuffer.getInt();
      n = i;
      i1 = j;
      HashSet<VerbatimX509Certificate> hashSet = new HashSet();
      n = i;
      i1 = j;
      this();
      while (true) {
        n = k;
        i1 = k;
        if (paramByteBuffer.hasRemaining()) {
          n = ++k;
          i1 = k;
          ByteBuffer byteBuffer1 = ApkSigningBlockUtils.getLengthPrefixedSlice(paramByteBuffer);
          n = k;
          i1 = k;
          ByteBuffer byteBuffer2 = ApkSigningBlockUtils.getLengthPrefixedSlice(byteBuffer1);
          n = k;
          i1 = k;
          j = byteBuffer1.getInt();
          n = k;
          i1 = k;
          i = byteBuffer1.getInt();
          n = k;
          i1 = k;
          byte[] arrayOfByte2 = ApkSigningBlockUtils.readLengthPrefixedByteArray(byteBuffer1);
          if (x509Certificate != null) {
            n = k;
            i1 = k;
            Pair<String, ? extends AlgorithmParameterSpec> pair = ApkSigningBlockUtils.getSignatureAlgorithmJcaSignatureAlgorithm(m);
            n = k;
            i1 = k;
            PublicKey publicKey = x509Certificate.getPublicKey();
            n = k;
            i1 = k;
            Signature signature = Signature.getInstance((String)pair.first);
            n = k;
            i1 = k;
            signature.initVerify(publicKey);
            n = k;
            i1 = k;
            if (pair.second != null) {
              n = k;
              i1 = k;
              signature.setParameter((AlgorithmParameterSpec)pair.second);
            } 
            n = k;
            i1 = k;
            signature.update(byteBuffer2);
            n = k;
            i1 = k;
            if (!signature.verify(arrayOfByte2)) {
              n = k;
              i1 = k;
              SecurityException securityException1 = new SecurityException();
              n = k;
              i1 = k;
              stringBuilder = new StringBuilder();
              n = k;
              i1 = k;
              this();
              n = k;
              i1 = k;
              stringBuilder.append("Unable to verify signature of certificate #");
              n = k;
              i1 = k;
              stringBuilder.append(k);
              n = k;
              i1 = k;
              stringBuilder.append(" using ");
              n = k;
              i1 = k;
              stringBuilder.append((String)pair.first);
              n = k;
              i1 = k;
              stringBuilder.append(" when verifying Proof-of-rotation record");
              n = k;
              i1 = k;
              this(stringBuilder.toString());
              n = k;
              i1 = k;
              throw securityException1;
            } 
          } 
          n = k;
          i1 = k;
          byteBuffer2.rewind();
          n = k;
          i1 = k;
          byte[] arrayOfByte1 = ApkSigningBlockUtils.readLengthPrefixedByteArray(byteBuffer2);
          n = k;
          i1 = k;
          int i2 = byteBuffer2.getInt();
          if (x509Certificate == null || m == i2) {
            n = k;
            i1 = k;
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream();
            n = k;
            i1 = k;
            this(arrayOfByte1);
            try {
              X509Certificate x509Certificate1 = (X509Certificate)stringBuilder.generateCertificate(byteArrayInputStream);
              VerbatimX509Certificate verbatimX509Certificate = new VerbatimX509Certificate();
              this(x509Certificate1, arrayOfByte1);
              m = i;
              if (!hashSet.contains(verbatimX509Certificate)) {
                hashSet.add(verbatimX509Certificate);
                arrayList.add(verbatimX509Certificate);
                arrayList1.add(Integer.valueOf(j));
                continue;
              } 
              SecurityException securityException1 = new SecurityException();
              StringBuilder stringBuilder1 = new StringBuilder();
              this();
              stringBuilder1.append("Encountered duplicate entries in Proof-of-rotation record at certificate #");
              stringBuilder1.append(k);
              stringBuilder1.append(".  All signing certificates should be unique");
              this(stringBuilder1.toString());
              throw securityException1;
            } catch (IOException|java.nio.BufferUnderflowException iOException) {
              break;
            } catch (NoSuchAlgorithmException|java.security.InvalidKeyException|java.security.InvalidAlgorithmParameterException|java.security.SignatureException noSuchAlgorithmException) {
              // Byte code: goto -> 834
            } catch (CertificateException null) {
              // Byte code: goto -> 785
            } 
          } 
          n = k;
          i1 = k;
          SecurityException securityException = new SecurityException();
          n = k;
          i1 = k;
          StringBuilder stringBuilder = new StringBuilder();
          n = k;
          i1 = k;
          this();
          n = k;
          i1 = k;
          stringBuilder.append("Signing algorithm ID mismatch for certificate #");
          n = k;
          i1 = k;
          stringBuilder.append(k);
          n = k;
          i1 = k;
          stringBuilder.append(" when verifying Proof-of-rotation record");
          n = k;
          i1 = k;
          this(stringBuilder.toString());
          n = k;
          i1 = k;
          throw securityException;
        } 
        return new VerifiedProofOfRotation((List)arrayList, arrayList1);
      } 
    } catch (IOException|java.nio.BufferUnderflowException iOException) {
    
    } catch (NoSuchAlgorithmException|java.security.InvalidKeyException|java.security.InvalidAlgorithmParameterException|java.security.SignatureException noSuchAlgorithmException) {
      k = i1;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to verify signature over signed data for certificate #");
      stringBuilder.append(k);
      stringBuilder.append(" when verifying Proof-of-rotation record");
      throw new SecurityException(stringBuilder.toString(), noSuchAlgorithmException);
    } catch (CertificateException certificateException) {
      k = n;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to decode certificate #");
      stringBuilder.append(k);
      stringBuilder.append(" when verifying Proof-of-rotation record");
      throw new SecurityException(stringBuilder.toString(), certificateException);
    } 
    throw new IOException("Failed to parse Proof-of-rotation record", certificateException);
  }
  
  static byte[] getVerityRootHash(String paramString) throws IOException, SignatureNotFoundException, SecurityException {
    RandomAccessFile randomAccessFile = new RandomAccessFile(paramString, "r");
    try {
      findSignature(randomAccessFile);
      VerifiedSigner verifiedSigner = verify(randomAccessFile, false);
      return verifiedSigner.verityRootHash;
    } finally {
      try {
        randomAccessFile.close();
      } finally {
        randomAccessFile = null;
      } 
    } 
  }
  
  static byte[] generateApkVerity(String paramString, ByteBufferFactory paramByteBufferFactory) throws IOException, SignatureNotFoundException, SecurityException, DigestException, NoSuchAlgorithmException {
    RandomAccessFile randomAccessFile = new RandomAccessFile(paramString, "r");
    try {
      SignatureInfo signatureInfo = findSignature(randomAccessFile);
      return VerityBuilder.generateApkVerity(paramString, paramByteBufferFactory, signatureInfo);
    } finally {
      try {
        randomAccessFile.close();
      } finally {
        paramByteBufferFactory = null;
      } 
    } 
  }
  
  static byte[] generateApkVerityRootHash(String paramString) throws NoSuchAlgorithmException, DigestException, IOException, SignatureNotFoundException {
    RandomAccessFile randomAccessFile = new RandomAccessFile(paramString, "r");
    try {
      SignatureInfo signatureInfo = findSignature(randomAccessFile);
      VerifiedSigner verifiedSigner = verify(randomAccessFile, false);
      byte[] arrayOfByte2 = verifiedSigner.verityRootHash;
      if (arrayOfByte2 == null)
        return null; 
      byte[] arrayOfByte1 = verifiedSigner.verityRootHash;
      ByteBuffer byteBuffer = ByteBuffer.wrap(arrayOfByte1);
      return VerityBuilder.generateApkVerityRootHash(randomAccessFile, byteBuffer, signatureInfo);
    } finally {
      try {
        randomAccessFile.close();
      } finally {
        randomAccessFile = null;
      } 
    } 
  }
  
  public static class VerifiedProofOfRotation {
    public final List<X509Certificate> certs;
    
    public final List<Integer> flagsList;
    
    public VerifiedProofOfRotation(List<X509Certificate> param1List, List<Integer> param1List1) {
      this.certs = param1List;
      this.flagsList = param1List1;
    }
  }
  
  public static class VerifiedSigner {
    public final X509Certificate[] certs;
    
    public byte[] digest;
    
    public final ApkSignatureSchemeV3Verifier.VerifiedProofOfRotation por;
    
    public byte[] verityRootHash;
    
    public VerifiedSigner(X509Certificate[] param1ArrayOfX509Certificate, ApkSignatureSchemeV3Verifier.VerifiedProofOfRotation param1VerifiedProofOfRotation) {
      this.certs = param1ArrayOfX509Certificate;
      this.por = param1VerifiedProofOfRotation;
    }
  }
  
  private static class PlatformNotSupportedException extends Exception {
    PlatformNotSupportedException(String param1String) {
      super(param1String);
    }
  }
}
