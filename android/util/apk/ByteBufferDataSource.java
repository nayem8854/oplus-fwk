package android.util.apk;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.DigestException;

class ByteBufferDataSource implements DataSource {
  private final ByteBuffer mBuf;
  
  ByteBufferDataSource(ByteBuffer paramByteBuffer) {
    this.mBuf = paramByteBuffer.slice();
  }
  
  public long size() {
    return this.mBuf.capacity();
  }
  
  public void feedIntoDataDigester(DataDigester paramDataDigester, long paramLong, int paramInt) throws IOException, DigestException {
    synchronized (this.mBuf) {
      this.mBuf.position(0);
      this.mBuf.limit((int)paramLong + paramInt);
      this.mBuf.position((int)paramLong);
      ByteBuffer byteBuffer = this.mBuf.slice();
      paramDataDigester.consume(byteBuffer);
      return;
    } 
  }
}
