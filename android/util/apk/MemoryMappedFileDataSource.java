package android.util.apk;

import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import java.io.FileDescriptor;
import java.io.IOException;
import java.security.DigestException;

class MemoryMappedFileDataSource implements DataSource {
  private static final long MEMORY_PAGE_SIZE_BYTES = Os.sysconf(OsConstants._SC_PAGESIZE);
  
  private final FileDescriptor mFd;
  
  private final long mFilePosition;
  
  private final long mSize;
  
  MemoryMappedFileDataSource(FileDescriptor paramFileDescriptor, long paramLong1, long paramLong2) {
    this.mFd = paramFileDescriptor;
    this.mFilePosition = paramLong1;
    this.mSize = paramLong2;
  }
  
  public long size() {
    return this.mSize;
  }
  
  public void feedIntoDataDigester(DataDigester paramDataDigester, long paramLong, int paramInt) throws IOException, DigestException {
    long l1 = this.mFilePosition + paramLong;
    paramLong = MEMORY_PAGE_SIZE_BYTES;
    paramLong = l1 / paramLong * paramLong;
    int i = (int)(l1 - paramLong);
    long l2 = (paramInt + i);
    l1 = 0L;
    try {
      int j = OsConstants.PROT_READ, k = OsConstants.MAP_SHARED, m = OsConstants.MAP_POPULATE;
      FileDescriptor fileDescriptor = this.mFd;
    } catch (ErrnoException errnoException) {
    
    } finally {
      paramDataDigester = null;
    } 
    if (paramLong != 0L)
      try {
        Os.munmap(paramLong, l2);
      } catch (ErrnoException errnoException) {} 
    throw paramDataDigester;
  }
}
