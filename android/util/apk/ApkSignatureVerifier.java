package android.util.apk;

import android.content.pm.PackageParser;
import android.content.pm.Signature;
import android.os.Trace;
import android.util.ArrayMap;
import android.util.BoostFramework;
import android.util.Slog;
import android.util.jar.StrictJarFile;
import com.android.internal.util.ArrayUtils;
import java.io.IOException;
import java.io.InputStream;
import java.security.DigestException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.zip.ZipEntry;
import libcore.io.IoUtils;

public class ApkSignatureVerifier {
  private static final int NUMBER_OF_CORES;
  
  private static final String TAG = "ApkSignatureVerifier";
  
  private static final AtomicReference<byte[]> sBuffer = (AtomicReference)new AtomicReference<>();
  
  private static boolean sIsPerfLockAcquired;
  
  public static boolean sOnScanDataApp;
  
  private static BoostFramework sPerfBoost;
  
  static {
    int i = Runtime.getRuntime().availableProcessors(), j = 4;
    if (i < 4)
      j = Runtime.getRuntime().availableProcessors(); 
    NUMBER_OF_CORES = j;
    sPerfBoost = null;
    sIsPerfLockAcquired = false;
    sOnScanDataApp = false;
  }
  
  public static PackageParser.SigningDetails verify(String paramString, int paramInt) throws PackageParser.PackageParserException {
    return verifySignatures(paramString, paramInt, true);
  }
  
  public static PackageParser.SigningDetails unsafeGetCertsWithoutVerification(String paramString, int paramInt) throws PackageParser.PackageParserException {
    return verifySignatures(paramString, paramInt, false);
  }
  
  private static PackageParser.SigningDetails verifySignatures(String paramString, int paramInt, boolean paramBoolean) throws PackageParser.PackageParserException {
    if (paramInt <= 4)
      try {
        return verifyV4Signature(paramString, paramInt, paramBoolean);
      } catch (SignatureNotFoundException signatureNotFoundException) {
        if (paramInt < 4) {
          if (paramInt <= 3)
            return verifyV3AndBelowSignatures(paramString, paramInt, paramBoolean); 
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("No signature found in package of version ");
          stringBuilder2.append(paramInt);
          stringBuilder2.append(" or newer for package ");
          stringBuilder2.append(paramString);
          throw new PackageParser.PackageParserException(-103, stringBuilder2.toString());
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("No APK Signature Scheme v4 signature in package ");
        stringBuilder1.append(paramString);
        throw new PackageParser.PackageParserException(-103, stringBuilder1.toString(), signatureNotFoundException);
      }  
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("No signature found in package of version ");
    stringBuilder.append(paramInt);
    stringBuilder.append(" or newer for package ");
    stringBuilder.append(paramString);
    throw new PackageParser.PackageParserException(-103, stringBuilder.toString());
  }
  
  private static PackageParser.SigningDetails verifyV3AndBelowSignatures(String paramString, int paramInt, boolean paramBoolean) throws PackageParser.PackageParserException {
    try {
      return verifyV3Signature(paramString, paramBoolean);
    } catch (SignatureNotFoundException signatureNotFoundException) {
      StringBuilder stringBuilder2;
      if (paramInt < 3) {
        if (paramInt <= 2)
          try {
            return verifyV2Signature(paramString, paramBoolean);
          } catch (SignatureNotFoundException signatureNotFoundException1) {
            StringBuilder stringBuilder3;
            if (paramInt < 2) {
              if (paramInt <= 1)
                return verifyV1Signature(paramString, paramBoolean); 
              stringBuilder3 = new StringBuilder();
              stringBuilder3.append("No signature found in package of version ");
              stringBuilder3.append(paramInt);
              stringBuilder3.append(" or newer for package ");
              stringBuilder3.append(paramString);
              throw new PackageParser.PackageParserException(-103, stringBuilder3.toString());
            } 
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append("No APK Signature Scheme v2 signature in package ");
            stringBuilder2.append(paramString);
            throw new PackageParser.PackageParserException(-103, stringBuilder2.toString(), stringBuilder3);
          }  
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("No signature found in package of version ");
        stringBuilder.append(paramInt);
        stringBuilder.append(" or newer for package ");
        stringBuilder.append(paramString);
        throw new PackageParser.PackageParserException(-103, stringBuilder.toString());
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("No APK Signature Scheme v3 signature in package ");
      stringBuilder1.append(paramString);
      throw new PackageParser.PackageParserException(-103, stringBuilder1.toString(), stringBuilder2);
    } 
  }
  
  private static PackageParser.SigningDetails verifyV4Signature(String paramString, int paramInt, boolean paramBoolean) throws SignatureNotFoundException, PackageParser.PackageParserException {
    // Byte code:
    //   0: iload_2
    //   1: ifeq -> 11
    //   4: ldc_w 'verifyV4'
    //   7: astore_3
    //   8: goto -> 15
    //   11: ldc_w 'certsOnlyV4'
    //   14: astore_3
    //   15: ldc2_w 262144
    //   18: aload_3
    //   19: invokestatic traceBegin : (JLjava/lang/String;)V
    //   22: aload_0
    //   23: invokestatic extractCertificates : (Ljava/lang/String;)Landroid/util/apk/ApkSignatureSchemeV4Verifier$VerifiedSigner;
    //   26: astore #4
    //   28: aload #4
    //   30: getfield certs : [Ljava/security/cert/Certificate;
    //   33: astore_3
    //   34: iconst_1
    //   35: anewarray [Ljava/security/cert/Certificate;
    //   38: dup
    //   39: iconst_0
    //   40: aload_3
    //   41: aastore
    //   42: invokestatic convertToSignatures : ([[Ljava/security/cert/Certificate;)[Landroid/content/pm/Signature;
    //   45: astore #5
    //   47: iload_2
    //   48: ifeq -> 284
    //   51: aload_0
    //   52: invokestatic unsafeGetCertsWithoutVerification : (Ljava/lang/String;)Landroid/util/apk/ApkSignatureSchemeV3Verifier$VerifiedSigner;
    //   55: astore #6
    //   57: aload #6
    //   59: getfield digest : [B
    //   62: astore_3
    //   63: iconst_1
    //   64: anewarray [Ljava/security/cert/Certificate;
    //   67: astore #7
    //   69: aload #7
    //   71: iconst_0
    //   72: aload #6
    //   74: getfield certs : [Ljava/security/cert/X509Certificate;
    //   77: aastore
    //   78: goto -> 102
    //   81: astore_3
    //   82: aload_0
    //   83: iconst_0
    //   84: invokestatic verify : (Ljava/lang/String;Z)Landroid/util/apk/ApkSignatureSchemeV2Verifier$VerifiedSigner;
    //   87: astore #7
    //   89: aload #7
    //   91: getfield digest : [B
    //   94: astore_3
    //   95: aload #7
    //   97: getfield certs : [[Ljava/security/cert/X509Certificate;
    //   100: astore #7
    //   102: aload #7
    //   104: invokestatic convertToSignatures : ([[Ljava/security/cert/Certificate;)[Landroid/content/pm/Signature;
    //   107: astore #7
    //   109: aload #7
    //   111: arraylength
    //   112: aload #5
    //   114: arraylength
    //   115: if_icmpne -> 198
    //   118: iconst_0
    //   119: istore_1
    //   120: aload #5
    //   122: arraylength
    //   123: istore #8
    //   125: iload_1
    //   126: iload #8
    //   128: if_icmpge -> 164
    //   131: aload #7
    //   133: iload_1
    //   134: aaload
    //   135: aload #5
    //   137: iload_1
    //   138: aaload
    //   139: invokevirtual equals : (Ljava/lang/Object;)Z
    //   142: ifeq -> 151
    //   145: iinc #1, 1
    //   148: goto -> 125
    //   151: new java/lang/SecurityException
    //   154: astore_3
    //   155: aload_3
    //   156: ldc_w 'V4 signature certificate does not match V2/V3'
    //   159: invokespecial <init> : (Ljava/lang/String;)V
    //   162: aload_3
    //   163: athrow
    //   164: aload #4
    //   166: getfield apkDigest : [B
    //   169: aload_3
    //   170: aload #4
    //   172: getfield apkDigest : [B
    //   175: arraylength
    //   176: invokestatic equals : ([B[BI)Z
    //   179: ifeq -> 185
    //   182: goto -> 284
    //   185: new java/lang/SecurityException
    //   188: astore_3
    //   189: aload_3
    //   190: ldc_w 'APK digest in V4 signature does not match V2/V3'
    //   193: invokespecial <init> : (Ljava/lang/String;)V
    //   196: aload_3
    //   197: athrow
    //   198: new java/lang/SecurityException
    //   201: astore #4
    //   203: new java/lang/StringBuilder
    //   206: astore_3
    //   207: aload_3
    //   208: invokespecial <init> : ()V
    //   211: aload_3
    //   212: ldc_w 'Invalid number of certificates: '
    //   215: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   218: pop
    //   219: aload_3
    //   220: aload #7
    //   222: arraylength
    //   223: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   226: pop
    //   227: aload #4
    //   229: aload_3
    //   230: invokevirtual toString : ()Ljava/lang/String;
    //   233: invokespecial <init> : (Ljava/lang/String;)V
    //   236: aload #4
    //   238: athrow
    //   239: astore #4
    //   241: new java/lang/SecurityException
    //   244: astore_3
    //   245: new java/lang/StringBuilder
    //   248: astore #7
    //   250: aload #7
    //   252: invokespecial <init> : ()V
    //   255: aload #7
    //   257: ldc_w 'V4 verification failed to collect V2/V3 certificates from : '
    //   260: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   263: pop
    //   264: aload #7
    //   266: aload_0
    //   267: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   270: pop
    //   271: aload_3
    //   272: aload #7
    //   274: invokevirtual toString : ()Ljava/lang/String;
    //   277: aload #4
    //   279: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   282: aload_3
    //   283: athrow
    //   284: new android/content/pm/PackageParser$SigningDetails
    //   287: dup
    //   288: aload #5
    //   290: iconst_4
    //   291: invokespecial <init> : ([Landroid/content/pm/Signature;I)V
    //   294: astore_3
    //   295: ldc2_w 262144
    //   298: invokestatic traceEnd : (J)V
    //   301: aload_3
    //   302: areturn
    //   303: astore_0
    //   304: goto -> 366
    //   307: astore #7
    //   309: new android/content/pm/PackageParser$PackageParserException
    //   312: astore_3
    //   313: new java/lang/StringBuilder
    //   316: astore #4
    //   318: aload #4
    //   320: invokespecial <init> : ()V
    //   323: aload #4
    //   325: ldc_w 'Failed to collect certificates from '
    //   328: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   331: pop
    //   332: aload #4
    //   334: aload_0
    //   335: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   338: pop
    //   339: aload #4
    //   341: ldc_w ' using APK Signature Scheme v4'
    //   344: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   347: pop
    //   348: aload_3
    //   349: bipush #-103
    //   351: aload #4
    //   353: invokevirtual toString : ()Ljava/lang/String;
    //   356: aload #7
    //   358: invokespecial <init> : (ILjava/lang/String;Ljava/lang/Throwable;)V
    //   361: aload_3
    //   362: athrow
    //   363: astore_0
    //   364: aload_0
    //   365: athrow
    //   366: ldc2_w 262144
    //   369: invokestatic traceEnd : (J)V
    //   372: aload_0
    //   373: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #197	-> 0
    //   #199	-> 22
    //   #200	-> 22
    //   #201	-> 28
    //   #202	-> 34
    //   #204	-> 47
    //   #205	-> 51
    //   #206	-> 51
    //   #211	-> 51
    //   #212	-> 51
    //   #213	-> 57
    //   #214	-> 63
    //   #226	-> 78
    //   #215	-> 81
    //   #217	-> 82
    //   #218	-> 82
    //   #219	-> 89
    //   #220	-> 95
    //   #225	-> 102
    //   #228	-> 102
    //   #229	-> 109
    //   #234	-> 118
    //   #235	-> 131
    //   #234	-> 145
    //   #236	-> 151
    //   #241	-> 164
    //   #243	-> 185
    //   #230	-> 198
    //   #221	-> 239
    //   #222	-> 241
    //   #247	-> 284
    //   #257	-> 295
    //   #247	-> 301
    //   #257	-> 303
    //   #251	-> 307
    //   #253	-> 309
    //   #249	-> 363
    //   #250	-> 364
    //   #257	-> 366
    //   #258	-> 372
    // Exception table:
    //   from	to	target	type
    //   22	28	363	android/util/apk/SignatureNotFoundException
    //   22	28	307	java/lang/Exception
    //   22	28	303	finally
    //   28	34	363	android/util/apk/SignatureNotFoundException
    //   28	34	307	java/lang/Exception
    //   28	34	303	finally
    //   34	47	363	android/util/apk/SignatureNotFoundException
    //   34	47	307	java/lang/Exception
    //   34	47	303	finally
    //   51	57	81	android/util/apk/SignatureNotFoundException
    //   51	57	307	java/lang/Exception
    //   51	57	303	finally
    //   57	63	81	android/util/apk/SignatureNotFoundException
    //   57	63	307	java/lang/Exception
    //   57	63	303	finally
    //   63	78	81	android/util/apk/SignatureNotFoundException
    //   63	78	307	java/lang/Exception
    //   63	78	303	finally
    //   82	89	239	android/util/apk/SignatureNotFoundException
    //   82	89	307	java/lang/Exception
    //   82	89	303	finally
    //   89	95	239	android/util/apk/SignatureNotFoundException
    //   89	95	307	java/lang/Exception
    //   89	95	303	finally
    //   95	102	239	android/util/apk/SignatureNotFoundException
    //   95	102	307	java/lang/Exception
    //   95	102	303	finally
    //   102	109	363	android/util/apk/SignatureNotFoundException
    //   102	109	307	java/lang/Exception
    //   102	109	303	finally
    //   109	118	363	android/util/apk/SignatureNotFoundException
    //   109	118	307	java/lang/Exception
    //   109	118	303	finally
    //   120	125	363	android/util/apk/SignatureNotFoundException
    //   120	125	307	java/lang/Exception
    //   120	125	303	finally
    //   131	145	363	android/util/apk/SignatureNotFoundException
    //   131	145	307	java/lang/Exception
    //   131	145	303	finally
    //   151	164	363	android/util/apk/SignatureNotFoundException
    //   151	164	307	java/lang/Exception
    //   151	164	303	finally
    //   164	182	363	android/util/apk/SignatureNotFoundException
    //   164	182	307	java/lang/Exception
    //   164	182	303	finally
    //   185	198	363	android/util/apk/SignatureNotFoundException
    //   185	198	307	java/lang/Exception
    //   185	198	303	finally
    //   198	239	363	android/util/apk/SignatureNotFoundException
    //   198	239	307	java/lang/Exception
    //   198	239	303	finally
    //   241	284	363	android/util/apk/SignatureNotFoundException
    //   241	284	307	java/lang/Exception
    //   241	284	303	finally
    //   284	295	363	android/util/apk/SignatureNotFoundException
    //   284	295	307	java/lang/Exception
    //   284	295	303	finally
    //   309	363	303	finally
    //   364	366	303	finally
  }
  
  private static PackageParser.SigningDetails verifyV3Signature(String paramString, boolean paramBoolean) throws SignatureNotFoundException, PackageParser.PackageParserException {
    String str;
    if (paramBoolean) {
      str = "verifyV3";
    } else {
      str = "certsOnlyV3";
    } 
    Trace.traceBegin(262144L, str);
    if (paramBoolean) {
      try {
        ApkSignatureSchemeV3Verifier.VerifiedSigner verifiedSigner = ApkSignatureSchemeV3Verifier.verify(paramString);
        X509Certificate[] arrayOfX509Certificate = verifiedSigner.certs;
      } catch (SignatureNotFoundException signatureNotFoundException) {
        throw signatureNotFoundException;
      } catch (Exception exception) {
        PackageParser.PackageParserException packageParserException = new PackageParser.PackageParserException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Failed to collect certificates from ");
        stringBuilder.append((String)signatureNotFoundException);
        stringBuilder.append(" using APK Signature Scheme v3");
        this(-103, stringBuilder.toString(), exception);
        throw packageParserException;
      } finally {}
    } else {
      ApkSignatureSchemeV3Verifier.VerifiedSigner verifiedSigner = ApkSignatureSchemeV3Verifier.unsafeGetCertsWithoutVerification((String)signatureNotFoundException);
      X509Certificate[] arrayOfX509Certificate = verifiedSigner.certs;
    } 
    Trace.traceEnd(262144L);
    throw signatureNotFoundException;
  }
  
  private static PackageParser.SigningDetails verifyV2Signature(String paramString, boolean paramBoolean) throws SignatureNotFoundException, PackageParser.PackageParserException {
    String str;
    if (paramBoolean) {
      str = "verifyV2";
    } else {
      str = "certsOnlyV2";
    } 
    Trace.traceBegin(262144L, str);
    if (paramBoolean) {
      try {
        X509Certificate[][] arrayOfX509Certificate = ApkSignatureSchemeV2Verifier.verify(paramString);
        Signature[] arrayOfSignature = convertToSignatures((Certificate[][])arrayOfX509Certificate);
      } catch (SignatureNotFoundException signatureNotFoundException) {
        throw signatureNotFoundException;
      } catch (Exception exception) {
        PackageParser.PackageParserException packageParserException = new PackageParser.PackageParserException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Failed to collect certificates from ");
        stringBuilder.append((String)signatureNotFoundException);
        stringBuilder.append(" using APK Signature Scheme v2");
        this(-103, stringBuilder.toString(), exception);
        throw packageParserException;
      } finally {}
    } else {
      X509Certificate[][] arrayOfX509Certificate = ApkSignatureSchemeV2Verifier.unsafeGetCertsWithoutVerification((String)signatureNotFoundException);
      Signature[] arrayOfSignature = convertToSignatures((Certificate[][])arrayOfX509Certificate);
    } 
    Trace.traceEnd(262144L);
    throw signatureNotFoundException;
  }
  
  private static PackageParser.SigningDetails verifyV1Signature(String paramString, boolean paramBoolean) throws PackageParser.PackageParserException {
    BoostFramework boostFramework;
    byte b1;
    Exception exception;
    if (paramBoolean) {
      b1 = NUMBER_OF_CORES;
    } else {
      b1 = 1;
    } 
    StrictJarFile[] arrayOfStrictJarFile = new StrictJarFile[b1];
    ArrayMap<Object, Object> arrayMap = new ArrayMap<>();
    try {
      Trace.traceBegin(262144L, "strictJarFileCtor");
      if (sPerfBoost == null) {
        BoostFramework boostFramework1 = new BoostFramework();
        this();
        sPerfBoost = boostFramework1;
      } 
      if (sPerfBoost != null && !sIsPerfLockAcquired && paramBoolean) {
        sPerfBoost.perfHint(4232, null, 2147483647, -1);
        Slog.d("ApkSignatureVerifier", "Perflock acquired for PackageInstall ");
        sIsPerfLockAcquired = true;
      } 
      int i;
      for (i = 0; i < b1; i++)
        arrayOfStrictJarFile[i] = new StrictJarFile(paramString, true, paramBoolean); 
      ArrayList<ZipEntry> arrayList = new ArrayList();
      this();
      ZipEntry zipEntry = arrayOfStrictJarFile[0].findEntry("AndroidManifest.xml");
      if (zipEntry != null) {
        Certificate[][] arrayOfCertificate = loadCertificates(arrayOfStrictJarFile[0], zipEntry);
        if (!ArrayUtils.isEmpty((Object[])arrayOfCertificate)) {
          PackageParser.PackageParserException packageParserException2;
          Signature[] arrayOfSignature = convertToSignatures(arrayOfCertificate);
          if (paramBoolean) {
            Iterator<ZipEntry> iterator = arrayOfStrictJarFile[0].iterator();
            while (iterator.hasNext()) {
              ZipEntry zipEntry1 = iterator.next();
              if (zipEntry1.isDirectory())
                continue; 
              String str = zipEntry1.getName();
              if (str.startsWith("META-INF/") || 
                str.equals("AndroidManifest.xml"))
                continue; 
              arrayList.add(zipEntry1);
            } 
            VerificationData verificationData = new VerificationData();
            this();
            Object object = new Object();
            this();
            verificationData.objWaitAll = object;
            object = new ThreadPoolExecutor();
            i = NUMBER_OF_CORES;
            int j = NUMBER_OF_CORES;
            TimeUnit timeUnit = TimeUnit.SECONDS;
            LinkedBlockingQueue<Runnable> linkedBlockingQueue = new LinkedBlockingQueue();
            this();
            super(i, j, 1L, timeUnit, linkedBlockingQueue);
            for (ZipEntry zipEntry1 : arrayList) {
              Runnable runnable = new Runnable() {
                  final String val$apkPath;
                  
                  final ZipEntry val$entry;
                  
                  final StrictJarFile[] val$jarFile;
                  
                  final Signature[] val$lastSigs;
                  
                  final ArrayMap val$strictJarFiles;
                  
                  final ApkSignatureVerifier.VerificationData val$vData;
                  
                  public void run() {
                    try {
                      ArrayMap arrayMap;
                      ZipEntry zipEntry;
                      if (vData.exceptionFlag != 0) {
                        StringBuilder stringBuilder = new StringBuilder();
                        this();
                        stringBuilder.append("VerifyV1 exit with exception ");
                        stringBuilder.append(vData.exceptionFlag);
                        Slog.w("ApkSignatureVerifier", stringBuilder.toString());
                        return;
                      } 
                      String str = Long.toString(Thread.currentThread().getId());
                      synchronized (strictJarFiles) {
                        StrictJarFile strictJarFile2 = (StrictJarFile)strictJarFiles.get(str);
                        StrictJarFile strictJarFile1 = strictJarFile2;
                        if (strictJarFile2 == null) {
                          if (vData.index >= ApkSignatureVerifier.NUMBER_OF_CORES)
                            vData.index = 0; 
                          StrictJarFile[] arrayOfStrictJarFile = jarFile;
                          ApkSignatureVerifier.VerificationData verificationData = vData;
                          int i = verificationData.index;
                          verificationData.index = i + 1;
                          strictJarFile1 = arrayOfStrictJarFile[i];
                          strictJarFiles.put(str, strictJarFile1);
                        } 
                        Certificate[][] arrayOfCertificate = ApkSignatureVerifier.loadCertificates(strictJarFile1, entry);
                        if (!ArrayUtils.isEmpty((Object[])arrayOfCertificate)) {
                          Signature[] arrayOfSignature = ApkSignatureVerifier.convertToSignatures(arrayOfCertificate);
                          if (!Signature.areExactMatch(lastSigs, arrayOfSignature)) {
                            PackageParser.PackageParserException packageParserException = new PackageParser.PackageParserException();
                            StringBuilder stringBuilder = new StringBuilder();
                            this();
                            stringBuilder.append("Package ");
                            stringBuilder.append(apkPath);
                            stringBuilder.append(" has mismatched certificates at entry ");
                            ZipEntry zipEntry1 = entry;
                            stringBuilder.append(zipEntry1.getName());
                            this(-104, stringBuilder.toString());
                            throw packageParserException;
                          } 
                        } else {
                          PackageParser.PackageParserException packageParserException = new PackageParser.PackageParserException();
                          StringBuilder stringBuilder = new StringBuilder();
                          this();
                          stringBuilder.append("Package ");
                          stringBuilder.append(apkPath);
                          stringBuilder.append(" has no certificates at entry ");
                          zipEntry = entry;
                          stringBuilder.append(zipEntry.getName());
                          this(-103, stringBuilder.toString());
                          throw packageParserException;
                        } 
                      } 
                    } catch (GeneralSecurityException null) {
                      synchronized (vData.objWaitAll) {
                        vData.exceptionFlag = -105;
                        vData.exception = null;
                        return;
                      } 
                    } catch (android.content.pm.PackageParser.PackageParserException null) {
                      synchronized (vData.objWaitAll) {
                        vData.exceptionFlag = -102;
                        vData.exception = (Exception)null;
                      } 
                    } 
                  }
                };
              super(verificationData, arrayMap, arrayOfStrictJarFile, zipEntry1, paramString, arrayOfSignature);
              synchronized (verificationData.objWaitAll) {
                if (verificationData.exceptionFlag == 0)
                  object.execute(runnable); 
              } 
            } 
            verificationData.wait = true;
            object.shutdown();
            while (true) {
              paramBoolean = verificationData.wait;
              if (paramBoolean) {
                try {
                  if (verificationData.exceptionFlag != 0 && !verificationData.shutDown) {
                    StringBuilder stringBuilder2 = new StringBuilder();
                    this();
                    stringBuilder2.append("verifyV1 Exception ");
                    stringBuilder2.append(verificationData.exceptionFlag);
                    Slog.w("ApkSignatureVerifier", stringBuilder2.toString());
                    object.shutdownNow();
                    verificationData.shutDown = true;
                  } 
                  if (!object.awaitTermination(50L, TimeUnit.MILLISECONDS)) {
                    paramBoolean = true;
                  } else {
                    paramBoolean = false;
                  } 
                  verificationData.wait = paramBoolean;
                } catch (InterruptedException interruptedException) {
                  Slog.w("ApkSignatureVerifier", "VerifyV1 interrupted while awaiting all threads done...");
                } 
                continue;
              } 
              break;
            } 
            if (verificationData.exceptionFlag != 0) {
              packageParserException2 = new PackageParser.PackageParserException();
              i = verificationData.exceptionFlag;
              StringBuilder stringBuilder2 = new StringBuilder();
              this();
              stringBuilder2.append("Failed to collect certificates from ");
              stringBuilder2.append(paramString);
              this(i, stringBuilder2.toString(), verificationData.exception);
              throw packageParserException2;
            } 
          } 
          PackageParser.SigningDetails signingDetails = new PackageParser.SigningDetails((Signature[])packageParserException2, 1);
          if (sIsPerfLockAcquired) {
            boostFramework = sPerfBoost;
            if (boostFramework != null) {
              boostFramework.perfLockRelease();
              sIsPerfLockAcquired = false;
              Slog.d("ApkSignatureVerifier", "Perflock released for PackageInstall ");
            } 
          } 
          arrayMap.clear();
          Trace.traceEnd(262144L);
          for (i = 0; i < b1; i++)
            closeQuietly(arrayOfStrictJarFile[i]); 
          return signingDetails;
        } 
        PackageParser.PackageParserException packageParserException1 = new PackageParser.PackageParserException();
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("Package ");
        stringBuilder1.append((String)boostFramework);
        stringBuilder1.append(" has no certificates at entry ");
        stringBuilder1.append("AndroidManifest.xml");
        this(-103, stringBuilder1.toString());
        throw packageParserException1;
      } 
      PackageParser.PackageParserException packageParserException = new PackageParser.PackageParserException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Package ");
      stringBuilder.append((String)boostFramework);
      stringBuilder.append(" has no manifest");
      this(-101, stringBuilder.toString());
      throw packageParserException;
    } catch (GeneralSecurityException generalSecurityException) {
      PackageParser.PackageParserException packageParserException = new PackageParser.PackageParserException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Failed to collect certificates from ");
      stringBuilder.append((String)boostFramework);
      this(-105, stringBuilder.toString(), generalSecurityException);
      throw packageParserException;
    } catch (IOException|RuntimeException iOException) {
      PackageParser.PackageParserException packageParserException = new PackageParser.PackageParserException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Failed to collect certificates from ");
      stringBuilder.append((String)boostFramework);
      this(-103, stringBuilder.toString(), iOException);
      throw packageParserException;
    } finally {}
    class VerificationData {
      public Exception exception;
      
      public int exceptionFlag;
      
      public int index;
      
      public Object objWaitAll;
      
      public boolean shutDown;
      
      public boolean wait;
    };
    if (sIsPerfLockAcquired) {
      boostFramework = sPerfBoost;
      if (boostFramework != null) {
        boostFramework.perfLockRelease();
        sIsPerfLockAcquired = false;
        Slog.d("ApkSignatureVerifier", "Perflock released for PackageInstall ");
      } 
    } 
    arrayMap.clear();
    Trace.traceEnd(262144L);
    for (byte b2 = 0; b2 < b1; b2++)
      closeQuietly(arrayOfStrictJarFile[b2]); 
    throw exception;
  }
  
  private static Certificate[][] loadCertificates(StrictJarFile paramStrictJarFile, ZipEntry paramZipEntry) throws PackageParser.PackageParserException {
    InputStream inputStream1 = null, inputStream2 = null;
    try {
      InputStream inputStream = paramStrictJarFile.getInputStream(paramZipEntry);
      inputStream2 = inputStream;
      inputStream1 = inputStream;
      readFullyIgnoringContents(inputStream);
      inputStream2 = inputStream;
      inputStream1 = inputStream;
      Certificate[][] arrayOfCertificate = paramStrictJarFile.getCertificateChains(paramZipEntry);
      IoUtils.closeQuietly(inputStream);
      return arrayOfCertificate;
    } catch (IOException|RuntimeException iOException) {
      inputStream2 = inputStream1;
      PackageParser.PackageParserException packageParserException = new PackageParser.PackageParserException();
      inputStream2 = inputStream1;
      StringBuilder stringBuilder = new StringBuilder();
      inputStream2 = inputStream1;
      this();
      inputStream2 = inputStream1;
      stringBuilder.append("Failed reading ");
      inputStream2 = inputStream1;
      stringBuilder.append(paramZipEntry.getName());
      inputStream2 = inputStream1;
      stringBuilder.append(" in ");
      inputStream2 = inputStream1;
      stringBuilder.append(paramStrictJarFile);
      inputStream2 = inputStream1;
      this(-102, stringBuilder.toString(), iOException);
      inputStream2 = inputStream1;
      throw packageParserException;
    } finally {}
    IoUtils.closeQuietly(inputStream2);
    throw paramStrictJarFile;
  }
  
  private static void readFullyIgnoringContents(InputStream paramInputStream) throws IOException {
    byte[] arrayOfByte1 = sBuffer.getAndSet(null);
    byte[] arrayOfByte2 = arrayOfByte1;
    if (arrayOfByte1 == null)
      arrayOfByte2 = new byte[4096]; 
    int i = 0;
    while (true) {
      int j = paramInputStream.read(arrayOfByte2, 0, arrayOfByte2.length);
      if (j != -1) {
        i += j;
        continue;
      } 
      break;
    } 
    sBuffer.set(arrayOfByte2);
  }
  
  private static Signature[] convertToSignatures(Certificate[][] paramArrayOfCertificate) throws CertificateEncodingException {
    Signature[] arrayOfSignature = new Signature[paramArrayOfCertificate.length];
    for (byte b = 0; b < paramArrayOfCertificate.length; b++)
      arrayOfSignature[b] = new Signature(paramArrayOfCertificate[b]); 
    return arrayOfSignature;
  }
  
  private static void closeQuietly(StrictJarFile paramStrictJarFile) {
    if (paramStrictJarFile != null)
      try {
        paramStrictJarFile.close();
      } catch (Exception exception) {} 
  }
  
  public static int getMinimumSignatureSchemeVersionForTargetSdk(int paramInt) {
    if (paramInt >= 30) {
      if (sOnScanDataApp)
        return 1; 
      return 2;
    } 
    return 1;
  }
  
  public static class Result {
    public final Certificate[][] certs;
    
    public final int signatureSchemeVersion;
    
    public final Signature[] sigs;
    
    public Result(Certificate[][] param1ArrayOfCertificate, Signature[] param1ArrayOfSignature, int param1Int) {
      this.certs = param1ArrayOfCertificate;
      this.sigs = param1ArrayOfSignature;
      this.signatureSchemeVersion = param1Int;
    }
  }
  
  public static byte[] getVerityRootHash(String paramString) throws IOException, SecurityException {
    try {
      return ApkSignatureSchemeV3Verifier.getVerityRootHash(paramString);
    } catch (SignatureNotFoundException signatureNotFoundException) {
      try {
        return ApkSignatureSchemeV2Verifier.getVerityRootHash(paramString);
      } catch (SignatureNotFoundException signatureNotFoundException1) {
        return null;
      } 
    } 
  }
  
  public static byte[] generateApkVerity(String paramString, ByteBufferFactory paramByteBufferFactory) throws IOException, SignatureNotFoundException, SecurityException, DigestException, NoSuchAlgorithmException {
    try {
      return ApkSignatureSchemeV3Verifier.generateApkVerity(paramString, paramByteBufferFactory);
    } catch (SignatureNotFoundException signatureNotFoundException) {
      return ApkSignatureSchemeV2Verifier.generateApkVerity(paramString, paramByteBufferFactory);
    } 
  }
  
  public static byte[] generateApkVerityRootHash(String paramString) throws NoSuchAlgorithmException, DigestException, IOException {
    try {
      return ApkSignatureSchemeV3Verifier.generateApkVerityRootHash(paramString);
    } catch (SignatureNotFoundException signatureNotFoundException) {
      try {
        return ApkSignatureSchemeV2Verifier.generateApkVerityRootHash(paramString);
      } catch (SignatureNotFoundException signatureNotFoundException1) {
        return null;
      } 
    } 
  }
}
