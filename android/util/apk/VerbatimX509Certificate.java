package android.util.apk;

import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Arrays;

class VerbatimX509Certificate extends WrappedX509Certificate {
  private final byte[] mEncodedVerbatim;
  
  private int mHash = -1;
  
  VerbatimX509Certificate(X509Certificate paramX509Certificate, byte[] paramArrayOfbyte) {
    super(paramX509Certificate);
    this.mEncodedVerbatim = paramArrayOfbyte;
  }
  
  public byte[] getEncoded() throws CertificateEncodingException {
    return this.mEncodedVerbatim;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof VerbatimX509Certificate))
      return false; 
    try {
      byte[] arrayOfByte = getEncoded();
      paramObject = ((VerbatimX509Certificate)paramObject).getEncoded();
      return Arrays.equals(arrayOfByte, (byte[])paramObject);
    } catch (CertificateEncodingException certificateEncodingException) {
      return false;
    } 
  }
  
  public int hashCode() {
    if (this.mHash == -1)
      try {
        this.mHash = Arrays.hashCode(getEncoded());
      } catch (CertificateEncodingException certificateEncodingException) {
        this.mHash = 0;
      }  
    return this.mHash;
  }
}
