package android.util.apk;

import android.util.Pair;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

abstract class ZipUtils {
  private static final int UINT16_MAX_VALUE = 65535;
  
  private static final int ZIP64_EOCD_LOCATOR_SIG_REVERSE_BYTE_ORDER = 1347094023;
  
  private static final int ZIP64_EOCD_LOCATOR_SIZE = 20;
  
  private static final int ZIP_EOCD_CENTRAL_DIR_OFFSET_FIELD_OFFSET = 16;
  
  private static final int ZIP_EOCD_CENTRAL_DIR_SIZE_FIELD_OFFSET = 12;
  
  private static final int ZIP_EOCD_COMMENT_LENGTH_FIELD_OFFSET = 20;
  
  private static final int ZIP_EOCD_REC_MIN_SIZE = 22;
  
  private static final int ZIP_EOCD_REC_SIG = 101010256;
  
  static Pair<ByteBuffer, Long> findZipEndOfCentralDirectoryRecord(RandomAccessFile paramRandomAccessFile) throws IOException {
    long l = paramRandomAccessFile.length();
    if (l < 22L)
      return null; 
    Pair<ByteBuffer, Long> pair = findZipEndOfCentralDirectoryRecord(paramRandomAccessFile, 0);
    if (pair != null)
      return pair; 
    return findZipEndOfCentralDirectoryRecord(paramRandomAccessFile, 65535);
  }
  
  private static Pair<ByteBuffer, Long> findZipEndOfCentralDirectoryRecord(RandomAccessFile paramRandomAccessFile, int paramInt) throws IOException {
    if (paramInt >= 0 && paramInt <= 65535) {
      long l = paramRandomAccessFile.length();
      if (l < 22L)
        return null; 
      paramInt = (int)Math.min(paramInt, l - 22L);
      ByteBuffer byteBuffer2 = ByteBuffer.allocate(paramInt + 22);
      byteBuffer2.order(ByteOrder.LITTLE_ENDIAN);
      l -= byteBuffer2.capacity();
      paramRandomAccessFile.seek(l);
      paramRandomAccessFile.readFully(byteBuffer2.array(), byteBuffer2.arrayOffset(), byteBuffer2.capacity());
      paramInt = findZipEndOfCentralDirectoryRecord(byteBuffer2);
      if (paramInt == -1)
        return null; 
      byteBuffer2.position(paramInt);
      ByteBuffer byteBuffer1 = byteBuffer2.slice();
      byteBuffer1.order(ByteOrder.LITTLE_ENDIAN);
      return Pair.create(byteBuffer1, Long.valueOf(paramInt + l));
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("maxCommentSize: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private static int findZipEndOfCentralDirectoryRecord(ByteBuffer paramByteBuffer) {
    assertByteOrderLittleEndian(paramByteBuffer);
    int i = paramByteBuffer.capacity();
    if (i < 22)
      return -1; 
    int j = Math.min(i - 22, 65535);
    for (byte b = 0; b <= j; 
      b++) {
      int k = i - 22 - b;
      if (paramByteBuffer.getInt(k) == 101010256) {
        int m = getUnsignedInt16(paramByteBuffer, k + 20);
        if (m == b)
          return k; 
      } 
    } 
    return -1;
  }
  
  public static final boolean isZip64EndOfCentralDirectoryLocatorPresent(RandomAccessFile paramRandomAccessFile, long paramLong) throws IOException {
    paramLong -= 20L;
    boolean bool = false;
    if (paramLong < 0L)
      return false; 
    paramRandomAccessFile.seek(paramLong);
    if (paramRandomAccessFile.readInt() == 1347094023)
      bool = true; 
    return bool;
  }
  
  public static long getZipEocdCentralDirectoryOffset(ByteBuffer paramByteBuffer) {
    assertByteOrderLittleEndian(paramByteBuffer);
    int i = paramByteBuffer.position();
    return getUnsignedInt32(paramByteBuffer, i + 16);
  }
  
  public static void setZipEocdCentralDirectoryOffset(ByteBuffer paramByteBuffer, long paramLong) {
    assertByteOrderLittleEndian(paramByteBuffer);
    int i = paramByteBuffer.position();
    setUnsignedInt32(paramByteBuffer, i + 16, paramLong);
  }
  
  public static long getZipEocdCentralDirectorySizeBytes(ByteBuffer paramByteBuffer) {
    assertByteOrderLittleEndian(paramByteBuffer);
    int i = paramByteBuffer.position();
    return getUnsignedInt32(paramByteBuffer, i + 12);
  }
  
  private static void assertByteOrderLittleEndian(ByteBuffer paramByteBuffer) {
    if (paramByteBuffer.order() == ByteOrder.LITTLE_ENDIAN)
      return; 
    throw new IllegalArgumentException("ByteBuffer byte order must be little endian");
  }
  
  private static int getUnsignedInt16(ByteBuffer paramByteBuffer, int paramInt) {
    return paramByteBuffer.getShort(paramInt) & 0xFFFF;
  }
  
  private static long getUnsignedInt32(ByteBuffer paramByteBuffer, int paramInt) {
    return paramByteBuffer.getInt(paramInt) & 0xFFFFFFFFL;
  }
  
  private static void setUnsignedInt32(ByteBuffer paramByteBuffer, int paramInt, long paramLong) {
    if (paramLong >= 0L && paramLong <= 4294967295L) {
      paramByteBuffer.putInt(paramByteBuffer.position() + paramInt, (int)paramLong);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("uint32 value of out range: ");
    stringBuilder.append(paramLong);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
}
