package android.util.apk;

import android.util.ArrayMap;
import android.util.Pair;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.DigestException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.MGF1ParameterSpec;
import java.security.spec.PSSParameterSpec;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

final class ApkSigningBlockUtils {
  private static final long APK_SIG_BLOCK_MAGIC_HI = 3617552046287187010L;
  
  private static final long APK_SIG_BLOCK_MAGIC_LO = 2334950737559900225L;
  
  private static final int APK_SIG_BLOCK_MIN_SIZE = 32;
  
  private static final int CHUNK_SIZE_BYTES = 1048576;
  
  static final int CONTENT_DIGEST_CHUNKED_SHA256 = 1;
  
  static final int CONTENT_DIGEST_CHUNKED_SHA512 = 2;
  
  static final int CONTENT_DIGEST_SHA256 = 4;
  
  static final int CONTENT_DIGEST_VERITY_CHUNKED_SHA256 = 3;
  
  static final int SIGNATURE_DSA_WITH_SHA256 = 769;
  
  static final int SIGNATURE_ECDSA_WITH_SHA256 = 513;
  
  static final int SIGNATURE_ECDSA_WITH_SHA512 = 514;
  
  static final int SIGNATURE_RSA_PKCS1_V1_5_WITH_SHA256 = 259;
  
  static final int SIGNATURE_RSA_PKCS1_V1_5_WITH_SHA512 = 260;
  
  static final int SIGNATURE_RSA_PSS_WITH_SHA256 = 257;
  
  static final int SIGNATURE_RSA_PSS_WITH_SHA512 = 258;
  
  static final int SIGNATURE_VERITY_DSA_WITH_SHA256 = 1061;
  
  static final int SIGNATURE_VERITY_ECDSA_WITH_SHA256 = 1059;
  
  static final int SIGNATURE_VERITY_RSA_PKCS1_V1_5_WITH_SHA256 = 1057;
  
  static SignatureInfo findSignature(RandomAccessFile paramRandomAccessFile, int paramInt) throws IOException, SignatureNotFoundException {
    Pair<ByteBuffer, Long> pair = getEocd(paramRandomAccessFile);
    ByteBuffer byteBuffer = (ByteBuffer)pair.first;
    long l = ((Long)pair.second).longValue();
    if (!ZipUtils.isZip64EndOfCentralDirectoryLocatorPresent(paramRandomAccessFile, l)) {
      long l1 = getCentralDirOffset(byteBuffer, l);
      Pair<ByteBuffer, Long> pair1 = findApkSigningBlock(paramRandomAccessFile, l1);
      ByteBuffer byteBuffer2 = (ByteBuffer)pair1.first;
      long l2 = ((Long)pair1.second).longValue();
      ByteBuffer byteBuffer1 = findApkSignatureSchemeBlock(byteBuffer2, paramInt);
      return new SignatureInfo(byteBuffer1, l2, l1, l, byteBuffer);
    } 
    throw new SignatureNotFoundException("ZIP64 APK not supported");
  }
  
  static void verifyIntegrity(Map<Integer, byte[]> paramMap, RandomAccessFile paramRandomAccessFile, SignatureInfo paramSignatureInfo) throws SecurityException {
    if (!paramMap.isEmpty()) {
      boolean bool = true;
      ArrayMap<Object, Object> arrayMap = new ArrayMap<>();
      if (paramMap.containsKey(Integer.valueOf(1))) {
        byte[] arrayOfByte = paramMap.get(Integer.valueOf(1));
        arrayMap.put(Integer.valueOf(1), arrayOfByte);
      } 
      if (paramMap.containsKey(Integer.valueOf(2))) {
        byte[] arrayOfByte = paramMap.get(Integer.valueOf(2));
        arrayMap.put(Integer.valueOf(2), arrayOfByte);
      } 
      if (!arrayMap.isEmpty())
        try {
          verifyIntegrityFor1MbChunkBasedAlgorithm((Map)arrayMap, paramRandomAccessFile.getFD(), paramSignatureInfo);
          bool = false;
        } catch (IOException iOException) {
          throw new SecurityException("Cannot get FD", iOException);
        }  
      if (iOException.containsKey(Integer.valueOf(3))) {
        byte[] arrayOfByte = (byte[])iOException.get(Integer.valueOf(3));
        verifyIntegrityForVerityBasedAlgorithm(arrayOfByte, paramRandomAccessFile, paramSignatureInfo);
        bool = false;
      } 
      if (!bool)
        return; 
      throw new SecurityException("No known digest exists for integrity check");
    } 
    throw new SecurityException("No digests provided");
  }
  
  static boolean isSupportedSignatureAlgorithm(int paramInt) {
    if (paramInt != 513 && paramInt != 514 && paramInt != 769 && paramInt != 1057 && paramInt != 1059 && paramInt != 1061)
      switch (paramInt) {
        default:
          return false;
        case 257:
        case 258:
        case 259:
        case 260:
          break;
      }  
    return true;
  }
  
  private static void verifyIntegrityFor1MbChunkBasedAlgorithm(Map<Integer, byte[]> paramMap, FileDescriptor paramFileDescriptor, SignatureInfo paramSignatureInfo) throws SecurityException {
    MemoryMappedFileDataSource memoryMappedFileDataSource1 = new MemoryMappedFileDataSource(paramFileDescriptor, 0L, paramSignatureInfo.apkSigningBlockOffset);
    MemoryMappedFileDataSource memoryMappedFileDataSource2 = new MemoryMappedFileDataSource(paramFileDescriptor, paramSignatureInfo.centralDirOffset, paramSignatureInfo.eocdOffset - paramSignatureInfo.centralDirOffset);
    ByteBuffer byteBuffer = paramSignatureInfo.eocd.duplicate();
    byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
    ZipUtils.setZipEocdCentralDirectoryOffset(byteBuffer, paramSignatureInfo.apkSigningBlockOffset);
    ByteBufferDataSource byteBufferDataSource = new ByteBufferDataSource(byteBuffer);
    int[] arrayOfInt = new int[paramMap.size()];
    byte b;
    for (Iterator<Integer> iterator = paramMap.keySet().iterator(); iterator.hasNext(); ) {
      int i = ((Integer)iterator.next()).intValue();
      arrayOfInt[b] = i;
      b++;
    } 
    try {
      byte[][] arrayOfByte = computeContentDigestsPer1MbChunk(arrayOfInt, new DataSource[] { memoryMappedFileDataSource1, memoryMappedFileDataSource2, byteBufferDataSource });
      for (b = 0; b < arrayOfInt.length; ) {
        int i = arrayOfInt[b];
        byte[] arrayOfByte2 = paramMap.get(Integer.valueOf(i));
        byte[] arrayOfByte1 = arrayOfByte[b];
        if (MessageDigest.isEqual(arrayOfByte2, arrayOfByte1)) {
          b++;
          continue;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getContentDigestAlgorithmJcaDigestAlgorithm(i));
        stringBuilder.append(" digest of contents did not verify");
        throw new SecurityException(stringBuilder.toString());
      } 
      return;
    } catch (DigestException digestException) {
      throw new SecurityException("Failed to compute digest(s) of contents", digestException);
    } 
  }
  
  private static byte[][] computeContentDigestsPer1MbChunk(int[] paramArrayOfint, DataSource[] paramArrayOfDataSource) throws DigestException {
    long l = 0L;
    int i, j;
    for (i = paramArrayOfDataSource.length, j = 0; j < i; ) {
      DataSource dataSource = paramArrayOfDataSource[j];
      l += getChunkCount(dataSource.size());
      j++;
    } 
    if (l < 2097151L) {
      StringBuilder stringBuilder1, stringBuilder2;
      int k = (int)l;
      byte[][] arrayOfByte2 = new byte[paramArrayOfint.length][];
      for (j = 0; j < paramArrayOfint.length; j++) {
        i = paramArrayOfint[j];
        i = getContentDigestAlgorithmOutputSizeBytes(i);
        byte[] arrayOfByte3 = new byte[k * i + 5];
        arrayOfByte3[0] = 90;
        setUnsignedInt32LittleEndian(k, arrayOfByte3, 1);
        arrayOfByte2[j] = arrayOfByte3;
      } 
      byte[] arrayOfByte = new byte[5];
      arrayOfByte[0] = -91;
      i = 0;
      MessageDigest[] arrayOfMessageDigest = new MessageDigest[paramArrayOfint.length];
      for (j = 0; j < paramArrayOfint.length; ) {
        int n = paramArrayOfint[j];
        String str = getContentDigestAlgorithmJcaDigestAlgorithm(n);
        try {
          arrayOfMessageDigest[j] = MessageDigest.getInstance(str);
          j++;
        } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
          stringBuilder1 = new StringBuilder();
          stringBuilder1.append(str);
          stringBuilder1.append(" digest not supported");
          throw new RuntimeException(stringBuilder1.toString(), noSuchAlgorithmException);
        } 
      } 
      MultipleDigestDataDigester multipleDigestDataDigester = new MultipleDigestDataDigester(arrayOfMessageDigest);
      byte b1;
      int m;
      byte b2;
      for (m = noSuchAlgorithmException.length, b1 = 0, b2 = 0, j = i, i = b2; i < m; ) {
        NoSuchAlgorithmException noSuchAlgorithmException1 = noSuchAlgorithmException[i];
        long l1 = noSuchAlgorithmException1.size(), l2 = 0L;
        while (l1 > 0L) {
          int n = (int)Math.min(l1, 1048576L);
          setUnsignedInt32LittleEndian(n, arrayOfByte, 1);
          for (b2 = 0; b2 < arrayOfMessageDigest.length; b2++)
            arrayOfMessageDigest[b2].update(arrayOfByte); 
          try {
            noSuchAlgorithmException1.feedIntoDataDigester(multipleDigestDataDigester, l2, n);
            for (b2 = 0; b2 < stringBuilder1.length; ) {
              StringBuilder stringBuilder3 = stringBuilder1[b2];
              byte[] arrayOfByte3 = arrayOfByte2[b2];
              int i2 = getContentDigestAlgorithmOutputSizeBytes(stringBuilder3);
              MessageDigest messageDigest = arrayOfMessageDigest[b2];
              int i1 = messageDigest.digest(arrayOfByte3, j * i2 + 5, i2);
              if (i1 == i2) {
                b2++;
                continue;
              } 
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append("Unexpected output size of ");
              stringBuilder1.append(messageDigest.getAlgorithm());
              stringBuilder1.append(" digest: ");
              stringBuilder1.append(i1);
              throw new RuntimeException(stringBuilder1.toString());
            } 
            long l3 = n;
            l1 -= n;
            j++;
            l2 += l3;
          } catch (IOException iOException) {
            StringBuilder stringBuilder3 = new StringBuilder();
            stringBuilder3.append("Failed to digest chunk #");
            stringBuilder3.append(j);
            stringBuilder3.append(" of section #");
            stringBuilder3.append(b1);
            throw new DigestException(stringBuilder3.toString(), iOException);
          } 
        } 
        b1++;
        i++;
      } 
      byte[][] arrayOfByte1 = new byte[iOException.length][];
      for (j = 0; j < iOException.length; ) {
        byte[] arrayOfByte3;
        IOException iOException1 = iOException[j];
        byte[] arrayOfByte4 = arrayOfByte2[j];
        String str = getContentDigestAlgorithmJcaDigestAlgorithm(iOException1);
        try {
          MessageDigest messageDigest = MessageDigest.getInstance(str);
          arrayOfByte3 = messageDigest.digest(arrayOfByte4);
          arrayOfByte1[j] = arrayOfByte3;
          j++;
        } catch (NoSuchAlgorithmException noSuchAlgorithmException1) {
          stringBuilder2 = new StringBuilder();
          stringBuilder2.append((String)arrayOfByte3);
          stringBuilder2.append(" digest not supported");
          throw new RuntimeException(stringBuilder2.toString(), noSuchAlgorithmException1);
        } 
      } 
      return (byte[][])stringBuilder2;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Too many chunks: ");
    stringBuilder.append(l);
    throw new DigestException(stringBuilder.toString());
  }
  
  static byte[] parseVerityDigestAndVerifySourceLength(byte[] paramArrayOfbyte, long paramLong, SignatureInfo paramSignatureInfo) throws SecurityException {
    if (paramArrayOfbyte.length == 32 + 8) {
      ByteBuffer byteBuffer = ByteBuffer.wrap(paramArrayOfbyte).order(ByteOrder.LITTLE_ENDIAN);
      byteBuffer.position(32);
      long l1 = byteBuffer.getLong();
      long l2 = paramSignatureInfo.centralDirOffset, l3 = paramSignatureInfo.apkSigningBlockOffset;
      if (l1 == paramLong - l2 - l3)
        return Arrays.copyOfRange(paramArrayOfbyte, 0, 32); 
      throw new SecurityException("APK content size did not verify");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Verity digest size is wrong: ");
    stringBuilder.append(paramArrayOfbyte.length);
    throw new SecurityException(stringBuilder.toString());
  }
  
  private static void verifyIntegrityForVerityBasedAlgorithm(byte[] paramArrayOfbyte, RandomAccessFile paramRandomAccessFile, SignatureInfo paramSignatureInfo) throws SecurityException {
    try {
      long l = paramRandomAccessFile.length();
      paramArrayOfbyte = parseVerityDigestAndVerifySourceLength(paramArrayOfbyte, l, paramSignatureInfo);
      Object object = new Object();
      super();
      VerityBuilder.VerityResult verityResult = VerityBuilder.generateApkVerityTree(paramRandomAccessFile, paramSignatureInfo, (ByteBufferFactory)object);
      if (Arrays.equals(paramArrayOfbyte, verityResult.rootHash))
        return; 
      SecurityException securityException = new SecurityException();
      this("APK verity digest of contents did not verify");
      throw securityException;
    } catch (DigestException|IOException|NoSuchAlgorithmException digestException) {
      throw new SecurityException("Error during verification", digestException);
    } 
  }
  
  static Pair<ByteBuffer, Long> getEocd(RandomAccessFile paramRandomAccessFile) throws IOException, SignatureNotFoundException {
    Pair<ByteBuffer, Long> pair = ZipUtils.findZipEndOfCentralDirectoryRecord(paramRandomAccessFile);
    if (pair != null)
      return pair; 
    throw new SignatureNotFoundException("Not an APK file: ZIP End of Central Directory record not found");
  }
  
  static long getCentralDirOffset(ByteBuffer paramByteBuffer, long paramLong) throws SignatureNotFoundException {
    long l = ZipUtils.getZipEocdCentralDirectoryOffset(paramByteBuffer);
    if (l <= paramLong) {
      long l1 = ZipUtils.getZipEocdCentralDirectorySizeBytes(paramByteBuffer);
      if (l + l1 == paramLong)
        return l; 
      throw new SignatureNotFoundException("ZIP Central Directory is not immediately followed by End of Central Directory");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ZIP Central Directory offset out of range: ");
    stringBuilder.append(l);
    stringBuilder.append(". ZIP End of Central Directory offset: ");
    stringBuilder.append(paramLong);
    throw new SignatureNotFoundException(stringBuilder.toString());
  }
  
  private static long getChunkCount(long paramLong) {
    return (paramLong + 1048576L - 1L) / 1048576L;
  }
  
  private static final int[] V4_CONTENT_DIGEST_ALGORITHMS = new int[] { 2, 3, 1 };
  
  static int compareSignatureAlgorithm(int paramInt1, int paramInt2) {
    paramInt1 = getSignatureAlgorithmContentDigestAlgorithm(paramInt1);
    paramInt2 = getSignatureAlgorithmContentDigestAlgorithm(paramInt2);
    return compareContentDigestAlgorithm(paramInt1, paramInt2);
  }
  
  private static int compareContentDigestAlgorithm(int paramInt1, int paramInt2) {
    if (paramInt1 != 1) {
      if (paramInt1 != 2) {
        if (paramInt1 == 3) {
          if (paramInt2 != 1) {
            if (paramInt2 != 2) {
              if (paramInt2 == 3)
                return 0; 
              StringBuilder stringBuilder1 = new StringBuilder();
              stringBuilder1.append("Unknown digestAlgorithm2: ");
              stringBuilder1.append(paramInt2);
              throw new IllegalArgumentException(stringBuilder1.toString());
            } 
            return -1;
          } 
          return 1;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown digestAlgorithm1: ");
        stringBuilder.append(paramInt1);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      if (paramInt2 != 1)
        if (paramInt2 != 2) {
          if (paramInt2 != 3) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unknown digestAlgorithm2: ");
            stringBuilder.append(paramInt2);
            throw new IllegalArgumentException(stringBuilder.toString());
          } 
        } else {
          return 0;
        }  
      return 1;
    } 
    if (paramInt2 != 1) {
      if (paramInt2 == 2 || paramInt2 == 3)
        return -1; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown digestAlgorithm2: ");
      stringBuilder.append(paramInt2);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    return 0;
  }
  
  static int getSignatureAlgorithmContentDigestAlgorithm(int paramInt) {
    if (paramInt != 513)
      if (paramInt != 514) {
        if (paramInt != 769)
          if (paramInt != 1057 && paramInt != 1059 && paramInt != 1061) {
            StringBuilder stringBuilder;
            long l;
            switch (paramInt) {
              default:
                stringBuilder = new StringBuilder();
                stringBuilder.append("Unknown signature algorithm: 0x");
                l = (paramInt & 0xFFFFFFFF);
                stringBuilder.append(Long.toHexString(l));
                throw new IllegalArgumentException(stringBuilder.toString());
              case 258:
              case 260:
                return 2;
              case 257:
              case 259:
                break;
            } 
          } else {
            return 3;
          }  
        return 1;
      }  
    return 1;
  }
  
  static String getContentDigestAlgorithmJcaDigestAlgorithm(int paramInt) {
    if (paramInt != 1)
      if (paramInt != 2) {
        if (paramInt != 3) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown content digest algorthm: ");
          stringBuilder.append(paramInt);
          throw new IllegalArgumentException(stringBuilder.toString());
        } 
      } else {
        return "SHA-512";
      }  
    return "SHA-256";
  }
  
  private static int getContentDigestAlgorithmOutputSizeBytes(int paramInt) {
    if (paramInt != 1)
      if (paramInt != 2) {
        if (paramInt != 3) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown content digest algorthm: ");
          stringBuilder.append(paramInt);
          throw new IllegalArgumentException(stringBuilder.toString());
        } 
      } else {
        return 64;
      }  
    return 32;
  }
  
  static String getSignatureAlgorithmJcaKeyAlgorithm(int paramInt) {
    if (paramInt != 513 && paramInt != 514)
      if (paramInt != 769) {
        if (paramInt != 1057) {
          if (paramInt != 1059) {
            if (paramInt != 1061) {
              StringBuilder stringBuilder;
              long l;
              switch (paramInt) {
                default:
                  stringBuilder = new StringBuilder();
                  stringBuilder.append("Unknown signature algorithm: 0x");
                  l = (paramInt & 0xFFFFFFFF);
                  stringBuilder.append(Long.toHexString(l));
                  throw new IllegalArgumentException(stringBuilder.toString());
                case 257:
                case 258:
                case 259:
                case 260:
                  break;
              } 
              return "RSA";
            } 
            return "DSA";
          } 
        } else {
          return "RSA";
        } 
      } else {
        return "DSA";
      }  
    return "EC";
  }
  
  static Pair<String, ? extends AlgorithmParameterSpec> getSignatureAlgorithmJcaSignatureAlgorithm(int paramInt) {
    if (paramInt != 513)
      if (paramInt != 514) {
        if (paramInt != 769) {
          if (paramInt != 1057) {
            if (paramInt != 1059) {
              if (paramInt != 1061) {
                StringBuilder stringBuilder;
                long l;
                switch (paramInt) {
                  default:
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("Unknown signature algorithm: 0x");
                    l = (paramInt & 0xFFFFFFFF);
                    stringBuilder.append(Long.toHexString(l));
                    throw new IllegalArgumentException(stringBuilder.toString());
                  case 260:
                    return Pair.create("SHA512withRSA", null);
                  case 258:
                    return Pair.create("SHA512withRSA/PSS", new PSSParameterSpec("SHA-512", "MGF1", MGF1ParameterSpec.SHA512, 64, 1));
                  case 257:
                    return Pair.create("SHA256withRSA/PSS", new PSSParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA256, 32, 1));
                  case 259:
                    break;
                } 
                return Pair.create("SHA256withRSA", null);
              } 
              return Pair.create("SHA256withDSA", null);
            } 
          } else {
            return Pair.create("SHA256withRSA", null);
          } 
        } else {
          return Pair.create("SHA256withDSA", null);
        } 
      } else {
        return Pair.create("SHA512withECDSA", null);
      }  
    return Pair.create("SHA256withECDSA", null);
  }
  
  static byte[] pickBestDigestForV4(Map<Integer, byte[]> paramMap) {
    for (int i : V4_CONTENT_DIGEST_ALGORITHMS) {
      if (paramMap.containsKey(Integer.valueOf(i)))
        return paramMap.get(Integer.valueOf(i)); 
    } 
    return null;
  }
  
  static ByteBuffer sliceFromTo(ByteBuffer paramByteBuffer, int paramInt1, int paramInt2) {
    if (paramInt1 >= 0) {
      if (paramInt2 >= paramInt1) {
        int i = paramByteBuffer.capacity();
        if (paramInt2 <= paramByteBuffer.capacity()) {
          i = paramByteBuffer.limit();
          int j = paramByteBuffer.position();
          try {
            paramByteBuffer.position(0);
            paramByteBuffer.limit(paramInt2);
            paramByteBuffer.position(paramInt1);
            ByteBuffer byteBuffer = paramByteBuffer.slice();
            byteBuffer.order(paramByteBuffer.order());
            return byteBuffer;
          } finally {
            paramByteBuffer.position(0);
            paramByteBuffer.limit(i);
            paramByteBuffer.position(j);
          } 
        } 
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("end > capacity: ");
        stringBuilder2.append(paramInt2);
        stringBuilder2.append(" > ");
        stringBuilder2.append(i);
        throw new IllegalArgumentException(stringBuilder2.toString());
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("end < start: ");
      stringBuilder1.append(paramInt2);
      stringBuilder1.append(" < ");
      stringBuilder1.append(paramInt1);
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("start: ");
    stringBuilder.append(paramInt1);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  static ByteBuffer getByteBuffer(ByteBuffer paramByteBuffer, int paramInt) throws BufferUnderflowException {
    if (paramInt >= 0) {
      int i = paramByteBuffer.limit();
      int j = paramByteBuffer.position();
      paramInt = j + paramInt;
      if (paramInt >= j && paramInt <= i) {
        paramByteBuffer.limit(paramInt);
        try {
          ByteBuffer byteBuffer = paramByteBuffer.slice();
          byteBuffer.order(paramByteBuffer.order());
          paramByteBuffer.position(paramInt);
          return byteBuffer;
        } finally {
          paramByteBuffer.limit(i);
        } 
      } 
      throw new BufferUnderflowException();
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("size: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  static ByteBuffer getLengthPrefixedSlice(ByteBuffer paramByteBuffer) throws IOException {
    if (paramByteBuffer.remaining() >= 4) {
      int i = paramByteBuffer.getInt();
      if (i >= 0) {
        if (i <= paramByteBuffer.remaining())
          return getByteBuffer(paramByteBuffer, i); 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Length-prefixed field longer than remaining buffer. Field length: ");
        stringBuilder1.append(i);
        stringBuilder1.append(", remaining: ");
        stringBuilder1.append(paramByteBuffer.remaining());
        throw new IOException(stringBuilder1.toString());
      } 
      throw new IllegalArgumentException("Negative length");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Remaining buffer too short to contain length of length-prefixed field. Remaining: ");
    stringBuilder.append(paramByteBuffer.remaining());
    throw new IOException(stringBuilder.toString());
  }
  
  static byte[] readLengthPrefixedByteArray(ByteBuffer paramByteBuffer) throws IOException {
    int i = paramByteBuffer.getInt();
    if (i >= 0) {
      if (i <= paramByteBuffer.remaining()) {
        byte[] arrayOfByte = new byte[i];
        paramByteBuffer.get(arrayOfByte);
        return arrayOfByte;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Underflow while reading length-prefixed value. Length: ");
      stringBuilder.append(i);
      stringBuilder.append(", available: ");
      stringBuilder.append(paramByteBuffer.remaining());
      throw new IOException(stringBuilder.toString());
    } 
    throw new IOException("Negative length");
  }
  
  static void setUnsignedInt32LittleEndian(int paramInt1, byte[] paramArrayOfbyte, int paramInt2) {
    paramArrayOfbyte[paramInt2] = (byte)(paramInt1 & 0xFF);
    paramArrayOfbyte[paramInt2 + 1] = (byte)(paramInt1 >>> 8 & 0xFF);
    paramArrayOfbyte[paramInt2 + 2] = (byte)(paramInt1 >>> 16 & 0xFF);
    paramArrayOfbyte[paramInt2 + 3] = (byte)(paramInt1 >>> 24 & 0xFF);
  }
  
  static Pair<ByteBuffer, Long> findApkSigningBlock(RandomAccessFile paramRandomAccessFile, long paramLong) throws IOException, SignatureNotFoundException {
    if (paramLong >= 32L) {
      ByteBuffer byteBuffer = ByteBuffer.allocate(24);
      byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
      paramRandomAccessFile.seek(paramLong - byteBuffer.capacity());
      paramRandomAccessFile.readFully(byteBuffer.array(), byteBuffer.arrayOffset(), byteBuffer.capacity());
      if (byteBuffer.getLong(8) == 2334950737559900225L && 
        byteBuffer.getLong(16) == 3617552046287187010L) {
        long l = byteBuffer.getLong(0);
        if (l >= byteBuffer.capacity() && l <= 2147483639L) {
          int i = (int)(8L + l);
          long l1 = paramLong - i;
          if (l1 >= 0L) {
            byteBuffer = ByteBuffer.allocate(i);
            byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
            paramRandomAccessFile.seek(l1);
            paramRandomAccessFile.readFully(byteBuffer.array(), byteBuffer.arrayOffset(), byteBuffer.capacity());
            paramLong = byteBuffer.getLong(0);
            if (paramLong == l)
              return Pair.create(byteBuffer, Long.valueOf(l1)); 
            StringBuilder stringBuilder3 = new StringBuilder();
            stringBuilder3.append("APK Signing Block sizes in header and footer do not match: ");
            stringBuilder3.append(paramLong);
            stringBuilder3.append(" vs ");
            stringBuilder3.append(l);
            throw new SignatureNotFoundException(stringBuilder3.toString());
          } 
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("APK Signing Block offset out of range: ");
          stringBuilder2.append(l1);
          throw new SignatureNotFoundException(stringBuilder2.toString());
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("APK Signing Block size out of range: ");
        stringBuilder1.append(l);
        throw new SignatureNotFoundException(stringBuilder1.toString());
      } 
      throw new SignatureNotFoundException("No APK Signing Block before ZIP Central Directory");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("APK too small for APK Signing Block. ZIP Central Directory offset: ");
    stringBuilder.append(paramLong);
    throw new SignatureNotFoundException(stringBuilder.toString());
  }
  
  static ByteBuffer findApkSignatureSchemeBlock(ByteBuffer paramByteBuffer, int paramInt) throws SignatureNotFoundException {
    checkByteOrderLittleEndian(paramByteBuffer);
    ByteBuffer byteBuffer = sliceFromTo(paramByteBuffer, 8, paramByteBuffer.capacity() - 24);
    byte b = 0;
    while (byteBuffer.hasRemaining()) {
      b++;
      if (byteBuffer.remaining() >= 8) {
        long l = byteBuffer.getLong();
        if (l >= 4L && l <= 2147483647L) {
          int i = (int)l;
          int j = byteBuffer.position();
          if (i <= byteBuffer.remaining()) {
            int k = byteBuffer.getInt();
            if (k == paramInt)
              return getByteBuffer(byteBuffer, i - 4); 
            byteBuffer.position(j + i);
            continue;
          } 
          StringBuilder stringBuilder3 = new StringBuilder();
          stringBuilder3.append("APK Signing Block entry #");
          stringBuilder3.append(b);
          stringBuilder3.append(" size out of range: ");
          stringBuilder3.append(i);
          stringBuilder3.append(", available: ");
          stringBuilder3.append(byteBuffer.remaining());
          throw new SignatureNotFoundException(stringBuilder3.toString());
        } 
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("APK Signing Block entry #");
        stringBuilder2.append(b);
        stringBuilder2.append(" size out of range: ");
        stringBuilder2.append(l);
        throw new SignatureNotFoundException(stringBuilder2.toString());
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Insufficient data to read size of APK Signing Block entry #");
      stringBuilder1.append(b);
      throw new SignatureNotFoundException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("No block with ID ");
    stringBuilder.append(paramInt);
    stringBuilder.append(" in APK Signing Block.");
    throw new SignatureNotFoundException(stringBuilder.toString());
  }
  
  private static void checkByteOrderLittleEndian(ByteBuffer paramByteBuffer) {
    if (paramByteBuffer.order() == ByteOrder.LITTLE_ENDIAN)
      return; 
    throw new IllegalArgumentException("ByteBuffer byte order must be little endian");
  }
  
  class MultipleDigestDataDigester implements DataDigester {
    private final MessageDigest[] mMds;
    
    MultipleDigestDataDigester(ApkSigningBlockUtils this$0) {
      this.mMds = (MessageDigest[])this$0;
    }
    
    public void consume(ByteBuffer param1ByteBuffer) {
      ByteBuffer byteBuffer = param1ByteBuffer.slice();
      for (MessageDigest messageDigest : this.mMds) {
        byteBuffer.position(0);
        messageDigest.update(byteBuffer);
      } 
    }
  }
}
