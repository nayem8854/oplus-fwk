package android.util;

import com.android.internal.util.XmlUtils;
import org.xmlpull.v1.XmlPullParser;

class XmlPullAttributes implements AttributeSet {
  XmlPullParser mParser;
  
  public XmlPullAttributes(XmlPullParser paramXmlPullParser) {
    this.mParser = paramXmlPullParser;
  }
  
  public int getAttributeCount() {
    return this.mParser.getAttributeCount();
  }
  
  public String getAttributeNamespace(int paramInt) {
    return this.mParser.getAttributeNamespace(paramInt);
  }
  
  public String getAttributeName(int paramInt) {
    return this.mParser.getAttributeName(paramInt);
  }
  
  public String getAttributeValue(int paramInt) {
    return this.mParser.getAttributeValue(paramInt);
  }
  
  public String getAttributeValue(String paramString1, String paramString2) {
    return this.mParser.getAttributeValue(paramString1, paramString2);
  }
  
  public String getPositionDescription() {
    return this.mParser.getPositionDescription();
  }
  
  public int getAttributeNameResource(int paramInt) {
    return 0;
  }
  
  public int getAttributeListValue(String paramString1, String paramString2, String[] paramArrayOfString, int paramInt) {
    paramString1 = getAttributeValue(paramString1, paramString2);
    return XmlUtils.convertValueToList(paramString1, paramArrayOfString, paramInt);
  }
  
  public boolean getAttributeBooleanValue(String paramString1, String paramString2, boolean paramBoolean) {
    paramString1 = getAttributeValue(paramString1, paramString2);
    return XmlUtils.convertValueToBoolean(paramString1, paramBoolean);
  }
  
  public int getAttributeResourceValue(String paramString1, String paramString2, int paramInt) {
    paramString1 = getAttributeValue(paramString1, paramString2);
    return XmlUtils.convertValueToInt(paramString1, paramInt);
  }
  
  public int getAttributeIntValue(String paramString1, String paramString2, int paramInt) {
    paramString1 = getAttributeValue(paramString1, paramString2);
    return XmlUtils.convertValueToInt(paramString1, paramInt);
  }
  
  public int getAttributeUnsignedIntValue(String paramString1, String paramString2, int paramInt) {
    paramString1 = getAttributeValue(paramString1, paramString2);
    return XmlUtils.convertValueToUnsignedInt(paramString1, paramInt);
  }
  
  public float getAttributeFloatValue(String paramString1, String paramString2, float paramFloat) {
    paramString1 = getAttributeValue(paramString1, paramString2);
    if (paramString1 != null)
      return Float.parseFloat(paramString1); 
    return paramFloat;
  }
  
  public int getAttributeListValue(int paramInt1, String[] paramArrayOfString, int paramInt2) {
    String str = getAttributeValue(paramInt1);
    return XmlUtils.convertValueToList(str, paramArrayOfString, paramInt2);
  }
  
  public boolean getAttributeBooleanValue(int paramInt, boolean paramBoolean) {
    String str = getAttributeValue(paramInt);
    return XmlUtils.convertValueToBoolean(str, paramBoolean);
  }
  
  public int getAttributeResourceValue(int paramInt1, int paramInt2) {
    String str = getAttributeValue(paramInt1);
    return XmlUtils.convertValueToInt(str, paramInt2);
  }
  
  public int getAttributeIntValue(int paramInt1, int paramInt2) {
    String str = getAttributeValue(paramInt1);
    return XmlUtils.convertValueToInt(str, paramInt2);
  }
  
  public int getAttributeUnsignedIntValue(int paramInt1, int paramInt2) {
    String str = getAttributeValue(paramInt1);
    return XmlUtils.convertValueToUnsignedInt(str, paramInt2);
  }
  
  public float getAttributeFloatValue(int paramInt, float paramFloat) {
    String str = getAttributeValue(paramInt);
    if (str != null)
      return Float.parseFloat(str); 
    return paramFloat;
  }
  
  public String getIdAttribute() {
    return getAttributeValue(null, "id");
  }
  
  public String getClassAttribute() {
    return getAttributeValue(null, "class");
  }
  
  public int getIdAttributeResourceValue(int paramInt) {
    return getAttributeResourceValue(null, "id", paramInt);
  }
  
  public int getStyleAttribute() {
    return getAttributeResourceValue(null, "style", 0);
  }
}
