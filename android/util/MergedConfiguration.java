package android.util;

import android.content.res.Configuration;
import android.os.Parcel;
import android.os.Parcelable;
import java.io.PrintWriter;

public class MergedConfiguration extends OplusBaseMergedConfiguration implements Parcelable {
  private Configuration mGlobalConfig = new Configuration();
  
  private Configuration mOverrideConfig = new Configuration();
  
  private Configuration mMergedConfig = new Configuration();
  
  public MergedConfiguration(Configuration paramConfiguration1, Configuration paramConfiguration2) {
    setConfiguration(paramConfiguration1, paramConfiguration2);
  }
  
  public MergedConfiguration(Configuration paramConfiguration) {
    setGlobalConfiguration(paramConfiguration);
  }
  
  public MergedConfiguration(MergedConfiguration paramMergedConfiguration) {
    Configuration configuration2 = paramMergedConfiguration.getGlobalConfiguration();
    Configuration configuration1 = paramMergedConfiguration.getOverrideConfiguration();
    setConfiguration(configuration2, configuration1);
  }
  
  private MergedConfiguration(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.mGlobalConfig, paramInt);
    paramParcel.writeParcelable((Parcelable)this.mOverrideConfig, paramInt);
    paramParcel.writeParcelable((Parcelable)this.mMergedConfig, paramInt);
    writeParcel(paramParcel, paramInt);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mGlobalConfig = (Configuration)paramParcel.readParcelable(Configuration.class.getClassLoader());
    this.mOverrideConfig = (Configuration)paramParcel.readParcelable(Configuration.class.getClassLoader());
    this.mMergedConfig = (Configuration)paramParcel.readParcelable(Configuration.class.getClassLoader());
    readParcel(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<MergedConfiguration> CREATOR = new Parcelable.Creator<MergedConfiguration>() {
      public MergedConfiguration createFromParcel(Parcel param1Parcel) {
        return new MergedConfiguration(param1Parcel);
      }
      
      public MergedConfiguration[] newArray(int param1Int) {
        return new MergedConfiguration[param1Int];
      }
    };
  
  private static final String TAG = "MergedConfiguration";
  
  public void setConfiguration(Configuration paramConfiguration1, Configuration paramConfiguration2) {
    this.mGlobalConfig.setTo(paramConfiguration1);
    if (paramConfiguration2 == null)
      Slog.e("MergedConfiguration", "overrideConfig is null"); 
    if (paramConfiguration2 != null)
      this.mOverrideConfig.setTo(paramConfiguration2); 
    updateMergedConfig();
  }
  
  public void setGlobalConfiguration(Configuration paramConfiguration) {
    this.mGlobalConfig.setTo(paramConfiguration);
    updateMergedConfig();
  }
  
  public void setOverrideConfiguration(Configuration paramConfiguration) {
    this.mOverrideConfig.setTo(paramConfiguration);
    updateMergedConfig();
  }
  
  public void setTo(MergedConfiguration paramMergedConfiguration) {
    setConfiguration(paramMergedConfiguration.mGlobalConfig, paramMergedConfiguration.mOverrideConfig);
  }
  
  public void unset() {
    this.mGlobalConfig.unset();
    this.mOverrideConfig.unset();
    updateMergedConfig();
  }
  
  public Configuration getGlobalConfiguration() {
    return this.mGlobalConfig;
  }
  
  public Configuration getOverrideConfiguration() {
    return this.mOverrideConfig;
  }
  
  public Configuration getMergedConfiguration() {
    return this.mMergedConfig;
  }
  
  private void updateMergedConfig() {
    this.mMergedConfig.setTo(this.mGlobalConfig);
    this.mMergedConfig.updateFrom(this.mOverrideConfig);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{mGlobalConfig=");
    stringBuilder.append(this.mGlobalConfig);
    stringBuilder.append(" mOverrideConfig=");
    stringBuilder.append(this.mOverrideConfig);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int hashCode() {
    return this.mMergedConfig.hashCode();
  }
  
  public boolean equals(Object paramObject) {
    if (!(paramObject instanceof MergedConfiguration))
      return false; 
    if (paramObject == this)
      return true; 
    return this.mMergedConfig.equals(((MergedConfiguration)paramObject).mMergedConfig);
  }
  
  public void dump(PrintWriter paramPrintWriter, String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append("mGlobalConfig=");
    stringBuilder.append(this.mGlobalConfig);
    paramPrintWriter.println(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append("mOverrideConfig=");
    stringBuilder.append(this.mOverrideConfig);
    paramPrintWriter.println(stringBuilder.toString());
  }
  
  public MergedConfiguration() {}
}
