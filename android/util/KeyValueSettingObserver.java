package android.util;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;

public abstract class KeyValueSettingObserver {
  private static final String TAG = "KeyValueSettingObserver";
  
  private final ContentObserver mObserver;
  
  private final KeyValueListParser mParser = new KeyValueListParser(',');
  
  private final ContentResolver mResolver;
  
  private final Uri mSettingUri;
  
  public KeyValueSettingObserver(Handler paramHandler, ContentResolver paramContentResolver, Uri paramUri) {
    this.mObserver = new SettingObserver(paramHandler);
    this.mResolver = paramContentResolver;
    this.mSettingUri = paramUri;
  }
  
  public void start() {
    this.mResolver.registerContentObserver(this.mSettingUri, false, this.mObserver);
    setParserValue();
    update(this.mParser);
  }
  
  public void stop() {
    this.mResolver.unregisterContentObserver(this.mObserver);
  }
  
  private void setParserValue() {
    String str = getSettingValue(this.mResolver);
    try {
      this.mParser.setString(str);
    } catch (IllegalArgumentException illegalArgumentException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Malformed setting: ");
      stringBuilder.append(str);
      Slog.e("KeyValueSettingObserver", stringBuilder.toString());
    } 
  }
  
  public abstract String getSettingValue(ContentResolver paramContentResolver);
  
  public abstract void update(KeyValueListParser paramKeyValueListParser);
  
  class SettingObserver extends ContentObserver {
    final KeyValueSettingObserver this$0;
    
    private SettingObserver(Handler param1Handler) {
      super(param1Handler);
    }
    
    public void onChange(boolean param1Boolean) {
      KeyValueSettingObserver.this.setParserValue();
      KeyValueSettingObserver keyValueSettingObserver = KeyValueSettingObserver.this;
      keyValueSettingObserver.update(keyValueSettingObserver.mParser);
    }
  }
}
