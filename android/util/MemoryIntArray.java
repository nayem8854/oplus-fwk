package android.util;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import dalvik.system.CloseGuard;
import java.io.Closeable;
import java.io.IOException;
import java.util.UUID;
import libcore.io.IoUtils;

public final class MemoryIntArray implements Parcelable, Closeable {
  private final CloseGuard mCloseGuard = CloseGuard.get();
  
  private int mFd = -1;
  
  public MemoryIntArray(int paramInt) throws IOException {
    if (paramInt <= 1024) {
      this.mIsOwner = true;
      String str = UUID.randomUUID().toString();
      this.mFd = paramInt = nativeCreate(str, paramInt);
      this.mMemoryAddr = nativeOpen(paramInt, this.mIsOwner);
      this.mCloseGuard.open("close");
      return;
    } 
    throw new IllegalArgumentException("Max size is 1024");
  }
  
  private MemoryIntArray(Parcel paramParcel) throws IOException {
    this.mIsOwner = false;
    ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)paramParcel.readParcelable(null);
    if (parcelFileDescriptor != null) {
      int i = parcelFileDescriptor.detachFd();
      this.mMemoryAddr = nativeOpen(i, this.mIsOwner);
      this.mCloseGuard.open("close");
      return;
    } 
    throw new IOException("No backing file descriptor");
  }
  
  public boolean isWritable() {
    enforceNotClosed();
    return this.mIsOwner;
  }
  
  public int get(int paramInt) throws IOException {
    enforceNotClosed();
    enforceValidIndex(paramInt);
    return nativeGet(this.mFd, this.mMemoryAddr, paramInt);
  }
  
  public void set(int paramInt1, int paramInt2) throws IOException {
    enforceNotClosed();
    enforceWritable();
    enforceValidIndex(paramInt1);
    nativeSet(this.mFd, this.mMemoryAddr, paramInt1, paramInt2);
  }
  
  public int size() throws IOException {
    enforceNotClosed();
    return nativeSize(this.mFd);
  }
  
  public void close() throws IOException {
    if (!isClosed()) {
      nativeClose(this.mFd, this.mMemoryAddr, this.mIsOwner);
      this.mFd = -1;
      this.mCloseGuard.close();
    } 
  }
  
  public boolean isClosed() {
    boolean bool;
    if (this.mFd == -1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mCloseGuard != null)
        this.mCloseGuard.warnIfOpen(); 
      IoUtils.closeQuietly(this);
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public int describeContents() {
    return 1;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    try {
      ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.fromFd(this.mFd);
      try {
        paramParcel.writeParcelable((Parcelable)parcelFileDescriptor, paramInt);
        return;
      } finally {
        if (parcelFileDescriptor != null)
          try {
            parcelFileDescriptor.close();
          } finally {
            parcelFileDescriptor = null;
          }  
      } 
    } catch (IOException iOException) {
      throw new RuntimeException(iOException);
    } 
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    if (paramObject == null)
      return false; 
    if (this == paramObject)
      return true; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mFd == ((MemoryIntArray)paramObject).mFd)
      bool = true; 
    return bool;
  }
  
  public int hashCode() {
    return this.mFd;
  }
  
  private void enforceNotClosed() {
    if (!isClosed())
      return; 
    throw new IllegalStateException("cannot interact with a closed instance");
  }
  
  private void enforceValidIndex(int paramInt) throws IOException {
    int i = size();
    if (paramInt >= 0 && paramInt <= i - 1)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramInt);
    stringBuilder.append(" not between 0 and ");
    stringBuilder.append(i - 1);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  private void enforceWritable() {
    if (isWritable())
      return; 
    throw new UnsupportedOperationException("array is not writable");
  }
  
  public static int getMaxSize() {
    return 1024;
  }
  
  public static final Parcelable.Creator<MemoryIntArray> CREATOR = new Parcelable.Creator<MemoryIntArray>() {
      public MemoryIntArray createFromParcel(Parcel param1Parcel) {
        try {
          return new MemoryIntArray(param1Parcel);
        } catch (IOException iOException) {
          throw new IllegalArgumentException("Error unparceling MemoryIntArray");
        } 
      }
      
      public MemoryIntArray[] newArray(int param1Int) {
        return new MemoryIntArray[param1Int];
      }
    };
  
  private static final int MAX_SIZE = 1024;
  
  private static final String TAG = "MemoryIntArray";
  
  private final boolean mIsOwner;
  
  private final long mMemoryAddr;
  
  private native void nativeClose(int paramInt, long paramLong, boolean paramBoolean);
  
  private native int nativeCreate(String paramString, int paramInt);
  
  private native int nativeGet(int paramInt1, long paramLong, int paramInt2);
  
  private native long nativeOpen(int paramInt, boolean paramBoolean);
  
  private native void nativeSet(int paramInt1, long paramLong, int paramInt2, int paramInt3);
  
  private native int nativeSize(int paramInt);
}
