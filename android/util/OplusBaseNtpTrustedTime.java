package android.util;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Network;
import android.net.OplusHttpClient;
import android.net.SntpClient;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.provider.Settings;
import java.net.InetAddress;
import java.util.Objects;

public abstract class OplusBaseNtpTrustedTime {
  private static final boolean IS_SUPPORT_OPLUS_NTP_TRUSTED_TIME = false;
  
  private static final boolean LOGD = true;
  
  private static final String TAG = "OplusBaseNtpTrustedTime";
  
  protected String[] mOplusNTPserverArray = new String[] { "", "", "" };
  
  private final Context sContext;
  
  public OplusBaseNtpTrustedTime(Context paramContext) {
    Objects.requireNonNull(paramContext);
    this.sContext = paramContext;
  }
  
  protected boolean isAutomaticTimeRequested() {
    Context context = this.sContext;
    ContentResolver contentResolver = context.getContentResolver();
    boolean bool = false;
    if (Settings.Global.getInt(contentResolver, "auto_time", 0) != 0)
      bool = true; 
    return bool;
  }
  
  protected boolean foceRefreshForCnRegion(int paramInt) {
    String str = SystemProperties.get("persist.sys.oppo.region", "CN");
    if ("CN".equals(str) || "OC".equals(str)) {
      Log.e("OplusBaseNtpTrustedTime", "foceRefreshForCnRegion, get feature: FEATURE_AUTOTIMEUPDATE_FORCE");
      OplusHttpClient oplusHttpClient = new OplusHttpClient();
      if (oplusHttpClient.requestTime(this.sContext, 0, paramInt)) {
        Log.d("OplusBaseNtpTrustedTime", "Use oppo http server algin time success!");
        long l1 = oplusHttpClient.getHttpTime();
        long l2 = oplusHttpClient.getHttpTimeReference();
        long l3 = oplusHttpClient.getRoundTripTime() / 2L;
        updateCacheStatus(l1, l2, l3);
        return true;
      } 
      InetAddress.clearDnsCache();
      if (oplusHttpClient.requestTime(this.sContext, 1, paramInt)) {
        Log.d("OplusBaseNtpTrustedTime", "Use oppo http server1 algin time success!");
        long l3 = oplusHttpClient.getHttpTime();
        long l1 = oplusHttpClient.getHttpTimeReference();
        long l2 = oplusHttpClient.getRoundTripTime() / 2L;
        updateCacheStatus(l3, l1, l2);
        return true;
      } 
    } 
    return false;
  }
  
  protected boolean refreshTimeWithOplusNTP(Network paramNetwork, String paramString, int paramInt) {
    SntpClient sntpClient = new SntpClient();
    String[] arrayOfString = this.mOplusNTPserverArray;
    arrayOfString[0] = paramString;
    int i = arrayOfString.length;
    paramString = updateBackupStatus();
    if (paramString != null) {
      this.mOplusNTPserverArray[2] = paramString;
    } else {
      i = this.mOplusNTPserverArray.length - 1;
    } 
    for (byte b = 0; b < i; b++) {
      boolean bool = "1".equals(SystemProperties.get("sys.ntp.exception", "0"));
      if (bool) {
        SystemClock.sleep(paramInt);
      } else {
        if (sntpClient.requestTime(this.mOplusNTPserverArray[b], paramInt, paramNetwork)) {
          long l1 = sntpClient.getNtpTime();
          long l2 = sntpClient.getNtpTimeReference();
          long l3 = sntpClient.getRoundTripTime() / 2L;
          updateCacheStatus(l1, l2, l3);
          return true;
        } 
        if (b == 2)
          onCountInBackupmode(); 
      } 
    } 
    return false;
  }
  
  protected boolean isSupportOplusNtpTrustedTime() {
    return false;
  }
  
  protected abstract void onCountInBackupmode();
  
  protected abstract String updateBackupStatus();
  
  protected abstract void updateCacheStatus(long paramLong1, long paramLong2, long paramLong3);
}
