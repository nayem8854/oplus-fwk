package android.util;

@Deprecated
public final class MutableBoolean {
  public boolean value;
  
  public MutableBoolean(boolean paramBoolean) {
    this.value = paramBoolean;
  }
}
