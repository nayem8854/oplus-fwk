package android.util;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ProtocolException;
import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Iterator;
import java.util.Objects;

public class RecurrenceRule implements Parcelable {
  public static final Parcelable.Creator<RecurrenceRule> CREATOR;
  
  private static final boolean LOGD = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static final String TAG = "RecurrenceRule";
  
  private static final int VERSION_INIT = 0;
  
  public static Clock sClock = Clock.systemDefaultZone();
  
  public final ZonedDateTime end;
  
  public final Period period;
  
  public final ZonedDateTime start;
  
  public RecurrenceRule(ZonedDateTime paramZonedDateTime1, ZonedDateTime paramZonedDateTime2, Period paramPeriod) {
    this.start = paramZonedDateTime1;
    this.end = paramZonedDateTime2;
    this.period = paramPeriod;
  }
  
  @Deprecated
  public static RecurrenceRule buildNever() {
    return new RecurrenceRule(null, null, null);
  }
  
  @Deprecated
  public static RecurrenceRule buildRecurringMonthly(int paramInt, ZoneId paramZoneId) {
    ZonedDateTime zonedDateTime2 = ZonedDateTime.now(sClock).withZoneSameInstant(paramZoneId);
    LocalDate localDate = zonedDateTime2.toLocalDate().minusYears(1L).withMonth(1).withDayOfMonth(paramInt);
    LocalTime localTime = LocalTime.MIDNIGHT;
    ZonedDateTime zonedDateTime1 = ZonedDateTime.of(localDate, localTime, paramZoneId);
    return new RecurrenceRule(zonedDateTime1, null, Period.ofMonths(1));
  }
  
  private RecurrenceRule(Parcel paramParcel) {
    this.start = convertZonedDateTime(paramParcel.readString());
    this.end = convertZonedDateTime(paramParcel.readString());
    this.period = convertPeriod(paramParcel.readString());
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(convertZonedDateTime(this.start));
    paramParcel.writeString(convertZonedDateTime(this.end));
    paramParcel.writeString(convertPeriod(this.period));
  }
  
  public RecurrenceRule(DataInputStream paramDataInputStream) throws IOException {
    int i = paramDataInputStream.readInt();
    if (i == 0) {
      this.start = convertZonedDateTime(BackupUtils.readString(paramDataInputStream));
      this.end = convertZonedDateTime(BackupUtils.readString(paramDataInputStream));
      this.period = convertPeriod(BackupUtils.readString(paramDataInputStream));
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unknown version ");
    stringBuilder.append(i);
    throw new ProtocolException(stringBuilder.toString());
  }
  
  public void writeToStream(DataOutputStream paramDataOutputStream) throws IOException {
    paramDataOutputStream.writeInt(0);
    BackupUtils.writeString(paramDataOutputStream, convertZonedDateTime(this.start));
    BackupUtils.writeString(paramDataOutputStream, convertZonedDateTime(this.end));
    BackupUtils.writeString(paramDataOutputStream, convertPeriod(this.period));
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("RecurrenceRule{");
    stringBuilder.append("start=");
    stringBuilder.append(this.start);
    stringBuilder.append(" end=");
    stringBuilder.append(this.end);
    stringBuilder.append(" period=");
    stringBuilder.append(this.period);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.start, this.end, this.period });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof RecurrenceRule;
    boolean bool1 = false;
    if (bool) {
      paramObject = paramObject;
      if (Objects.equals(this.start, ((RecurrenceRule)paramObject).start)) {
        ZonedDateTime zonedDateTime1 = this.end, zonedDateTime2 = ((RecurrenceRule)paramObject).end;
        if (Objects.equals(zonedDateTime1, zonedDateTime2)) {
          Period period = this.period;
          paramObject = ((RecurrenceRule)paramObject).period;
          if (Objects.equals(period, paramObject))
            bool1 = true; 
        } 
      } 
      return bool1;
    } 
    return false;
  }
  
  static {
    CREATOR = new Parcelable.Creator<RecurrenceRule>() {
        public RecurrenceRule createFromParcel(Parcel param1Parcel) {
          return new RecurrenceRule(param1Parcel);
        }
        
        public RecurrenceRule[] newArray(int param1Int) {
          return new RecurrenceRule[param1Int];
        }
      };
  }
  
  public boolean isRecurring() {
    boolean bool;
    if (this.period != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @Deprecated
  public boolean isMonthly() {
    ZonedDateTime zonedDateTime = this.start;
    null = true;
    if (zonedDateTime != null) {
      Period period = this.period;
      if (period != null)
        if (period.getYears() == 0) {
          period = this.period;
          if (period.getMonths() == 1) {
            period = this.period;
            if (period.getDays() == 0)
              return null; 
          } 
        }  
    } 
    return false;
  }
  
  public Iterator<Range<ZonedDateTime>> cycleIterator() {
    if (this.period != null)
      return new RecurringIterator(); 
    return new NonrecurringIterator();
  }
  
  class NonrecurringIterator implements Iterator<Range<ZonedDateTime>> {
    boolean hasNext;
    
    final RecurrenceRule this$0;
    
    public NonrecurringIterator() {
      boolean bool;
      if (RecurrenceRule.this.start != null && RecurrenceRule.this.end != null) {
        bool = true;
      } else {
        bool = false;
      } 
      this.hasNext = bool;
    }
    
    public boolean hasNext() {
      return this.hasNext;
    }
    
    public Range<ZonedDateTime> next() {
      this.hasNext = false;
      return new Range<>(RecurrenceRule.this.start, RecurrenceRule.this.end);
    }
  }
  
  class RecurringIterator implements Iterator<Range<ZonedDateTime>> {
    ZonedDateTime cycleEnd;
    
    ZonedDateTime cycleStart;
    
    int i;
    
    final RecurrenceRule this$0;
    
    public RecurringIterator() {
      ZonedDateTime zonedDateTime;
      if (RecurrenceRule.this.end != null) {
        zonedDateTime = RecurrenceRule.this.end;
      } else {
        zonedDateTime = ZonedDateTime.now(RecurrenceRule.sClock).withZoneSameInstant(((RecurrenceRule)zonedDateTime).start.getZone());
      } 
      if (RecurrenceRule.LOGD) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Resolving using anchor ");
        stringBuilder.append(zonedDateTime);
        Log.d("RecurrenceRule", stringBuilder.toString());
      } 
      updateCycle();
      while (zonedDateTime.toEpochSecond() > this.cycleEnd.toEpochSecond()) {
        this.i++;
        updateCycle();
      } 
      while (zonedDateTime.toEpochSecond() <= this.cycleStart.toEpochSecond()) {
        this.i--;
        updateCycle();
      } 
    }
    
    private void updateCycle() {
      this.cycleStart = roundBoundaryTime(RecurrenceRule.this.start.plus(RecurrenceRule.this.period.multipliedBy(this.i)));
      this.cycleEnd = roundBoundaryTime(RecurrenceRule.this.start.plus(RecurrenceRule.this.period.multipliedBy(this.i + 1)));
    }
    
    private ZonedDateTime roundBoundaryTime(ZonedDateTime param1ZonedDateTime) {
      if (RecurrenceRule.this.isMonthly() && param1ZonedDateTime.getDayOfMonth() < RecurrenceRule.this.start.getDayOfMonth())
        return ZonedDateTime.of(param1ZonedDateTime.toLocalDate(), LocalTime.MAX, RecurrenceRule.this.start.getZone()); 
      return param1ZonedDateTime;
    }
    
    public boolean hasNext() {
      boolean bool;
      if (this.cycleStart.toEpochSecond() >= RecurrenceRule.this.start.toEpochSecond()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public Range<ZonedDateTime> next() {
      if (RecurrenceRule.LOGD) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Cycle ");
        stringBuilder.append(this.i);
        stringBuilder.append(" from ");
        stringBuilder.append(this.cycleStart);
        stringBuilder.append(" to ");
        stringBuilder.append(this.cycleEnd);
        Log.d("RecurrenceRule", stringBuilder.toString());
      } 
      Range<ZonedDateTime> range = new Range<>(this.cycleStart, this.cycleEnd);
      this.i--;
      updateCycle();
      return range;
    }
  }
  
  public static String convertZonedDateTime(ZonedDateTime paramZonedDateTime) {
    if (paramZonedDateTime != null) {
      String str = paramZonedDateTime.toString();
    } else {
      paramZonedDateTime = null;
    } 
    return (String)paramZonedDateTime;
  }
  
  public static ZonedDateTime convertZonedDateTime(String paramString) {
    if (paramString != null) {
      ZonedDateTime zonedDateTime = ZonedDateTime.parse(paramString);
    } else {
      paramString = null;
    } 
    return (ZonedDateTime)paramString;
  }
  
  public static String convertPeriod(Period paramPeriod) {
    if (paramPeriod != null) {
      String str = paramPeriod.toString();
    } else {
      paramPeriod = null;
    } 
    return (String)paramPeriod;
  }
  
  public static Period convertPeriod(String paramString) {
    if (paramString != null) {
      Period period = Period.parse(paramString);
    } else {
      paramString = null;
    } 
    return (Period)paramString;
  }
}
