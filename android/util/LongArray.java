package android.util;

import com.android.internal.util.ArrayUtils;
import com.android.internal.util.Preconditions;
import java.util.Arrays;
import libcore.util.EmptyArray;

public class LongArray implements Cloneable {
  private static final int MIN_CAPACITY_INCREMENT = 12;
  
  private int mSize;
  
  private long[] mValues;
  
  private LongArray(long[] paramArrayOflong, int paramInt) {
    this.mValues = paramArrayOflong;
    this.mSize = Preconditions.checkArgumentInRange(paramInt, 0, paramArrayOflong.length, "size");
  }
  
  public LongArray() {
    this(10);
  }
  
  public LongArray(int paramInt) {
    if (paramInt == 0) {
      this.mValues = EmptyArray.LONG;
    } else {
      this.mValues = ArrayUtils.newUnpaddedLongArray(paramInt);
    } 
    this.mSize = 0;
  }
  
  public static LongArray wrap(long[] paramArrayOflong) {
    return new LongArray(paramArrayOflong, paramArrayOflong.length);
  }
  
  public static LongArray fromArray(long[] paramArrayOflong, int paramInt) {
    return wrap(Arrays.copyOf(paramArrayOflong, paramInt));
  }
  
  public void resize(int paramInt) {
    Preconditions.checkArgumentNonnegative(paramInt);
    long[] arrayOfLong = this.mValues;
    if (paramInt <= arrayOfLong.length) {
      Arrays.fill(arrayOfLong, paramInt, arrayOfLong.length, 0L);
    } else {
      ensureCapacity(paramInt - this.mSize);
    } 
    this.mSize = paramInt;
  }
  
  public void add(long paramLong) {
    add(this.mSize, paramLong);
  }
  
  public void add(int paramInt, long paramLong) {
    ensureCapacity(1);
    int i = this.mSize, j = i - paramInt;
    this.mSize = ++i;
    ArrayUtils.checkBounds(i, paramInt);
    if (j != 0) {
      long[] arrayOfLong = this.mValues;
      System.arraycopy(arrayOfLong, paramInt, arrayOfLong, paramInt + 1, j);
    } 
    this.mValues[paramInt] = paramLong;
  }
  
  public void addAll(LongArray paramLongArray) {
    int i = paramLongArray.mSize;
    ensureCapacity(i);
    System.arraycopy(paramLongArray.mValues, 0, this.mValues, this.mSize, i);
    this.mSize += i;
  }
  
  private void ensureCapacity(int paramInt) {
    int i = this.mSize;
    int j = i + paramInt;
    if (j >= this.mValues.length) {
      if (i < 6) {
        paramInt = 12;
      } else {
        paramInt = i >> 1;
      } 
      paramInt += i;
      if (paramInt <= j)
        paramInt = j; 
      long[] arrayOfLong = ArrayUtils.newUnpaddedLongArray(paramInt);
      System.arraycopy(this.mValues, 0, arrayOfLong, 0, i);
      this.mValues = arrayOfLong;
    } 
  }
  
  public void clear() {
    this.mSize = 0;
  }
  
  public LongArray clone() {
    LongArray longArray = null;
    try {
      LongArray longArray1 = (LongArray)super.clone();
      longArray = longArray1;
      longArray1.mValues = (long[])this.mValues.clone();
      longArray = longArray1;
    } catch (CloneNotSupportedException cloneNotSupportedException) {}
    return longArray;
  }
  
  public long get(int paramInt) {
    ArrayUtils.checkBounds(this.mSize, paramInt);
    return this.mValues[paramInt];
  }
  
  public void set(int paramInt, long paramLong) {
    ArrayUtils.checkBounds(this.mSize, paramInt);
    this.mValues[paramInt] = paramLong;
  }
  
  public int indexOf(long paramLong) {
    int i = this.mSize;
    for (byte b = 0; b < i; b++) {
      if (this.mValues[b] == paramLong)
        return b; 
    } 
    return -1;
  }
  
  public void remove(int paramInt) {
    ArrayUtils.checkBounds(this.mSize, paramInt);
    long[] arrayOfLong = this.mValues;
    System.arraycopy(arrayOfLong, paramInt + 1, arrayOfLong, paramInt, this.mSize - paramInt - 1);
    this.mSize--;
  }
  
  public int size() {
    return this.mSize;
  }
  
  public long[] toArray() {
    return Arrays.copyOf(this.mValues, this.mSize);
  }
  
  public static boolean elementsEqual(LongArray paramLongArray1, LongArray paramLongArray2) {
    boolean bool = true;
    if (paramLongArray1 == null || paramLongArray2 == null) {
      if (paramLongArray1 != paramLongArray2)
        bool = false; 
      return bool;
    } 
    if (paramLongArray1.mSize != paramLongArray2.mSize)
      return false; 
    for (byte b = 0; b < paramLongArray1.mSize; b++) {
      if (paramLongArray1.get(b) != paramLongArray2.get(b))
        return false; 
    } 
    return true;
  }
}
