package android.util;

import java.util.function.Consumer;

public class SparseArrayMap<T> {
  private final SparseArray<ArrayMap<String, T>> mData = new SparseArray<>();
  
  public void add(int paramInt, String paramString, T paramT) {
    ArrayMap<Object, Object> arrayMap1 = (ArrayMap)this.mData.get(paramInt);
    ArrayMap<Object, Object> arrayMap2 = arrayMap1;
    if (arrayMap1 == null) {
      arrayMap2 = new ArrayMap<>();
      this.mData.put(paramInt, arrayMap2);
    } 
    arrayMap2.put(paramString, paramT);
  }
  
  public void clear() {
    for (byte b = 0; b < this.mData.size(); b++)
      ((ArrayMap)this.mData.valueAt(b)).clear(); 
  }
  
  public boolean contains(int paramInt, String paramString) {
    boolean bool;
    if (this.mData.contains(paramInt) && ((ArrayMap)this.mData.get(paramInt)).containsKey(paramString)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void delete(int paramInt) {
    this.mData.delete(paramInt);
  }
  
  public T delete(int paramInt, String paramString) {
    ArrayMap arrayMap = this.mData.get(paramInt);
    if (arrayMap != null)
      return (T)arrayMap.remove(paramString); 
    return null;
  }
  
  public T get(int paramInt, String paramString) {
    ArrayMap arrayMap = this.mData.get(paramInt);
    if (arrayMap != null)
      return (T)arrayMap.get(paramString); 
    return null;
  }
  
  public T getOrDefault(int paramInt, String paramString, T paramT) {
    if (this.mData.contains(paramInt)) {
      ArrayMap arrayMap = this.mData.get(paramInt);
      if (arrayMap != null && arrayMap.containsKey(paramString))
        return (T)arrayMap.get(paramString); 
    } 
    return paramT;
  }
  
  public int indexOfKey(int paramInt) {
    return this.mData.indexOfKey(paramInt);
  }
  
  public int indexOfKey(int paramInt, String paramString) {
    ArrayMap arrayMap = this.mData.get(paramInt);
    if (arrayMap != null)
      return arrayMap.indexOfKey(paramString); 
    return -1;
  }
  
  public int keyAt(int paramInt) {
    return this.mData.keyAt(paramInt);
  }
  
  public String keyAt(int paramInt1, int paramInt2) {
    return (String)((ArrayMap)this.mData.valueAt(paramInt1)).keyAt(paramInt2);
  }
  
  public int numMaps() {
    return this.mData.size();
  }
  
  public int numElementsForKey(int paramInt) {
    ArrayMap arrayMap = this.mData.get(paramInt);
    if (arrayMap == null) {
      paramInt = 0;
    } else {
      paramInt = arrayMap.size();
    } 
    return paramInt;
  }
  
  public T valueAt(int paramInt1, int paramInt2) {
    return (T)((ArrayMap)this.mData.valueAt(paramInt1)).valueAt(paramInt2);
  }
  
  public void forEach(Consumer<T> paramConsumer) {
    for (int i = numMaps() - 1; i >= 0; i--) {
      ArrayMap arrayMap = this.mData.valueAt(i);
      for (int j = arrayMap.size() - 1; j >= 0; j--)
        paramConsumer.accept((T)arrayMap.valueAt(j)); 
    } 
  }
}
