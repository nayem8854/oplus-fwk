package android.util;

public abstract class Spline {
  public static Spline createSpline(float[] paramArrayOffloat1, float[] paramArrayOffloat2) {
    if (isStrictlyIncreasing(paramArrayOffloat1)) {
      if (isMonotonic(paramArrayOffloat2))
        return createMonotoneCubicSpline(paramArrayOffloat1, paramArrayOffloat2); 
      return createLinearSpline(paramArrayOffloat1, paramArrayOffloat2);
    } 
    throw new IllegalArgumentException("The control points must all have strictly increasing X values.");
  }
  
  public static Spline createMonotoneCubicSpline(float[] paramArrayOffloat1, float[] paramArrayOffloat2) {
    return new MonotoneCubicSpline(paramArrayOffloat1, paramArrayOffloat2);
  }
  
  public static Spline createLinearSpline(float[] paramArrayOffloat1, float[] paramArrayOffloat2) {
    return new LinearSpline(paramArrayOffloat1, paramArrayOffloat2);
  }
  
  private static boolean isStrictlyIncreasing(float[] paramArrayOffloat) {
    if (paramArrayOffloat != null && paramArrayOffloat.length >= 2) {
      float f = paramArrayOffloat[0];
      for (byte b = 1; b < paramArrayOffloat.length; b++) {
        float f1 = paramArrayOffloat[b];
        if (f1 <= f)
          return false; 
        f = f1;
      } 
      return true;
    } 
    throw new IllegalArgumentException("There must be at least two control points.");
  }
  
  private static boolean isMonotonic(float[] paramArrayOffloat) {
    if (paramArrayOffloat != null && paramArrayOffloat.length >= 2) {
      float f = paramArrayOffloat[0];
      for (byte b = 1; b < paramArrayOffloat.length; b++) {
        float f1 = paramArrayOffloat[b];
        if (f1 < f)
          return false; 
        f = f1;
      } 
      return true;
    } 
    throw new IllegalArgumentException("There must be at least two control points.");
  }
  
  public abstract float interpolate(float paramFloat);
  
  class MonotoneCubicSpline extends Spline {
    private float[] mM;
    
    private float[] mX;
    
    private float[] mY;
    
    public MonotoneCubicSpline(Spline this$0, float[] param1ArrayOffloat1) {
      if (this$0 != null && param1ArrayOffloat1 != null && this$0.length == param1ArrayOffloat1.length && this$0.length >= 2) {
        int i = this$0.length;
        float[] arrayOfFloat1 = new float[i - 1];
        float[] arrayOfFloat2 = new float[i];
        byte b;
        for (b = 0; b < i - 1; ) {
          float f = this$0[b + 1] - this$0[b];
          if (f > 0.0F) {
            arrayOfFloat1[b] = (param1ArrayOffloat1[b + 1] - param1ArrayOffloat1[b]) / f;
            b++;
          } 
          throw new IllegalArgumentException("The control points must all have strictly increasing X values.");
        } 
        arrayOfFloat2[0] = arrayOfFloat1[0];
        for (b = 1; b < i - 1; b++)
          arrayOfFloat2[b] = (arrayOfFloat1[b - 1] + arrayOfFloat1[b]) * 0.5F; 
        arrayOfFloat2[i - 1] = arrayOfFloat1[i - 2];
        for (b = 0; b < i - 1; b++) {
          if (arrayOfFloat1[b] == 0.0F) {
            arrayOfFloat2[b] = 0.0F;
            arrayOfFloat2[b + 1] = 0.0F;
          } else {
            float f1 = arrayOfFloat2[b] / arrayOfFloat1[b];
            float f2 = arrayOfFloat2[b + 1] / arrayOfFloat1[b];
            if (f1 >= 0.0F && f2 >= 0.0F) {
              f1 = (float)Math.hypot(f1, f2);
              if (f1 > 3.0F) {
                f1 = 3.0F / f1;
                arrayOfFloat2[b] = arrayOfFloat2[b] * f1;
                int j = b + 1;
                arrayOfFloat2[j] = arrayOfFloat2[j] * f1;
              } 
            } else {
              throw new IllegalArgumentException("The control points must have monotonic Y values.");
            } 
          } 
        } 
        this.mX = (float[])this$0;
        this.mY = param1ArrayOffloat1;
        this.mM = arrayOfFloat2;
        return;
      } 
      throw new IllegalArgumentException("There must be at least two control points and the arrays must be of equal length.");
    }
    
    public float interpolate(float param1Float) {
      int i = this.mX.length;
      if (Float.isNaN(param1Float))
        return param1Float; 
      float[] arrayOfFloat1 = this.mX;
      if (param1Float <= arrayOfFloat1[0])
        return this.mY[0]; 
      if (param1Float >= arrayOfFloat1[i - 1])
        return this.mY[i - 1]; 
      i = 0;
      while (true) {
        arrayOfFloat1 = this.mX;
        if (param1Float >= arrayOfFloat1[i + 1]) {
          int j = i + 1;
          i = j;
          if (param1Float == arrayOfFloat1[j])
            return this.mY[j]; 
          continue;
        } 
        break;
      } 
      float f1 = arrayOfFloat1[i + 1] - arrayOfFloat1[i];
      float f2 = (param1Float - arrayOfFloat1[i]) / f1;
      float[] arrayOfFloat2 = this.mY;
      param1Float = arrayOfFloat2[i];
      arrayOfFloat1 = this.mM;
      return (param1Float * (f2 * 2.0F + 1.0F) + arrayOfFloat1[i] * f1 * f2) * (1.0F - f2) * (1.0F - f2) + (arrayOfFloat2[i + 1] * (3.0F - 2.0F * f2) + arrayOfFloat1[i + 1] * f1 * (f2 - 1.0F)) * f2 * f2;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      int i = this.mX.length;
      stringBuilder.append("MonotoneCubicSpline{[");
      for (byte b = 0; b < i; b++) {
        if (b != 0)
          stringBuilder.append(", "); 
        stringBuilder.append("(");
        stringBuilder.append(this.mX[b]);
        stringBuilder.append(", ");
        stringBuilder.append(this.mY[b]);
        stringBuilder.append(": ");
        stringBuilder.append(this.mM[b]);
        stringBuilder.append(")");
      } 
      stringBuilder.append("]}");
      return stringBuilder.toString();
    }
  }
  
  class LinearSpline extends Spline {
    private final float[] mM;
    
    private final float[] mX;
    
    private final float[] mY;
    
    public LinearSpline(Spline this$0, float[] param1ArrayOffloat1) {
      if (this$0 != null && param1ArrayOffloat1 != null && this$0.length == param1ArrayOffloat1.length && this$0.length >= 2) {
        int i = this$0.length;
        this.mM = new float[i - 1];
        for (byte b = 0; b < i - 1; b++)
          this.mM[b] = (param1ArrayOffloat1[b + 1] - param1ArrayOffloat1[b]) / (this$0[b + 1] - this$0[b]); 
        this.mX = (float[])this$0;
        this.mY = param1ArrayOffloat1;
        return;
      } 
      throw new IllegalArgumentException("There must be at least two control points and the arrays must be of equal length.");
    }
    
    public float interpolate(float param1Float) {
      int i = this.mX.length;
      if (Float.isNaN(param1Float))
        return param1Float; 
      float[] arrayOfFloat = this.mX;
      if (param1Float <= arrayOfFloat[0])
        return this.mY[0]; 
      if (param1Float >= arrayOfFloat[i - 1])
        return this.mY[i - 1]; 
      i = 0;
      while (true) {
        arrayOfFloat = this.mX;
        if (param1Float >= arrayOfFloat[i + 1]) {
          int j = i + 1;
          i = j;
          if (param1Float == arrayOfFloat[j])
            return this.mY[j]; 
          continue;
        } 
        break;
      } 
      return this.mY[i] + this.mM[i] * (param1Float - arrayOfFloat[i]);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      int i = this.mX.length;
      stringBuilder.append("LinearSpline{[");
      for (byte b = 0; b < i; b++) {
        if (b != 0)
          stringBuilder.append(", "); 
        stringBuilder.append("(");
        stringBuilder.append(this.mX[b]);
        stringBuilder.append(", ");
        stringBuilder.append(this.mY[b]);
        if (b < i - 1) {
          stringBuilder.append(": ");
          stringBuilder.append(this.mM[b]);
        } 
        stringBuilder.append(")");
      } 
      stringBuilder.append("]}");
      return stringBuilder.toString();
    }
  }
}
