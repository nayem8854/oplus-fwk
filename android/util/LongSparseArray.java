package android.util;

import android.os.Parcel;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.GrowingArrayUtils;
import com.android.internal.util.Parcelling;
import com.android.internal.util.Preconditions;
import com.android.internal.util.function.LongObjPredicate;
import java.util.Arrays;
import java.util.Objects;
import libcore.util.EmptyArray;

public class LongSparseArray<E> implements Cloneable {
  private static final Object DELETED = new Object();
  
  private boolean mGarbage = false;
  
  private long[] mKeys;
  
  private int mSize;
  
  private Object[] mValues;
  
  public LongSparseArray() {
    this(10);
  }
  
  public LongSparseArray(int paramInt) {
    if (paramInt == 0) {
      this.mKeys = EmptyArray.LONG;
      this.mValues = EmptyArray.OBJECT;
    } else {
      this.mKeys = ArrayUtils.newUnpaddedLongArray(paramInt);
      this.mValues = ArrayUtils.newUnpaddedObjectArray(paramInt);
    } 
    this.mSize = 0;
  }
  
  public LongSparseArray<E> clone() {
    LongSparseArray<E> longSparseArray = null;
    try {
      LongSparseArray<E> longSparseArray1 = (LongSparseArray)super.clone();
      longSparseArray = longSparseArray1;
      longSparseArray1.mKeys = (long[])this.mKeys.clone();
      longSparseArray = longSparseArray1;
      longSparseArray1.mValues = (Object[])this.mValues.clone();
      longSparseArray = longSparseArray1;
    } catch (CloneNotSupportedException cloneNotSupportedException) {}
    return longSparseArray;
  }
  
  public E get(long paramLong) {
    return get(paramLong, null);
  }
  
  public E get(long paramLong, E paramE) {
    int i = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramLong);
    if (i >= 0) {
      Object[] arrayOfObject = this.mValues;
      if (arrayOfObject[i] != DELETED)
        return (E)arrayOfObject[i]; 
    } 
    return paramE;
  }
  
  public void delete(long paramLong) {
    int i = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramLong);
    if (i >= 0) {
      Object arrayOfObject[] = this.mValues, object1 = arrayOfObject[i], object2 = DELETED;
      if (object1 != object2) {
        arrayOfObject[i] = object2;
        this.mGarbage = true;
      } 
    } 
  }
  
  public void remove(long paramLong) {
    delete(paramLong);
  }
  
  public void removeIf(LongObjPredicate<? super E> paramLongObjPredicate) {
    Objects.requireNonNull(paramLongObjPredicate);
    for (byte b = 0; b < this.mSize; b++) {
      Object[] arrayOfObject = this.mValues;
      if (arrayOfObject[b] != DELETED && paramLongObjPredicate.test(this.mKeys[b], arrayOfObject[b])) {
        this.mValues[b] = DELETED;
        this.mGarbage = true;
      } 
    } 
  }
  
  public void removeAt(int paramInt) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds) {
      Object arrayOfObject[] = this.mValues, object1 = arrayOfObject[paramInt], object2 = DELETED;
      if (object1 != object2) {
        arrayOfObject[paramInt] = object2;
        this.mGarbage = true;
      } 
      return;
    } 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  private void gc() {
    int i = this.mSize;
    int j = 0;
    long[] arrayOfLong = this.mKeys;
    Object[] arrayOfObject = this.mValues;
    for (int k = 0; k < i; k++, j = m) {
      Object object = arrayOfObject[k];
      int m = j;
      if (object != DELETED) {
        if (k != j) {
          arrayOfLong[j] = arrayOfLong[k];
          arrayOfObject[j] = object;
          arrayOfObject[k] = null;
        } 
        m = j + 1;
      } 
    } 
    this.mGarbage = false;
    this.mSize = j;
  }
  
  public void put(long paramLong, E paramE) {
    int i = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramLong);
    if (i >= 0) {
      this.mValues[i] = paramE;
    } else {
      int j = i ^ 0xFFFFFFFF;
      if (j < this.mSize) {
        Object[] arrayOfObject = this.mValues;
        if (arrayOfObject[j] == DELETED) {
          this.mKeys[j] = paramLong;
          arrayOfObject[j] = paramE;
          return;
        } 
      } 
      i = j;
      if (this.mGarbage) {
        i = j;
        if (this.mSize >= this.mKeys.length) {
          gc();
          i = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramLong) ^ 0xFFFFFFFF;
        } 
      } 
      this.mKeys = GrowingArrayUtils.insert(this.mKeys, this.mSize, i, paramLong);
      this.mValues = GrowingArrayUtils.insert(this.mValues, this.mSize, i, paramE);
      this.mSize++;
    } 
  }
  
  public int size() {
    if (this.mGarbage)
      gc(); 
    return this.mSize;
  }
  
  public long keyAt(int paramInt) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds) {
      if (this.mGarbage)
        gc(); 
      return this.mKeys[paramInt];
    } 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public E valueAt(int paramInt) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds) {
      if (this.mGarbage)
        gc(); 
      return (E)this.mValues[paramInt];
    } 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public void setValueAt(int paramInt, E paramE) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds) {
      if (this.mGarbage)
        gc(); 
      this.mValues[paramInt] = paramE;
      return;
    } 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public int indexOfKey(long paramLong) {
    if (this.mGarbage)
      gc(); 
    return ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramLong);
  }
  
  public int indexOfValue(E paramE) {
    if (this.mGarbage)
      gc(); 
    for (byte b = 0; b < this.mSize; b++) {
      if (this.mValues[b] == paramE)
        return b; 
    } 
    return -1;
  }
  
  public int indexOfValueByValue(E paramE) {
    if (this.mGarbage)
      gc(); 
    for (byte b = 0; b < this.mSize; b++) {
      if (paramE == null) {
        if (this.mValues[b] == null)
          return b; 
      } else if (paramE.equals(this.mValues[b])) {
        return b;
      } 
    } 
    return -1;
  }
  
  public void clear() {
    int i = this.mSize;
    Object[] arrayOfObject = this.mValues;
    for (byte b = 0; b < i; b++)
      arrayOfObject[b] = null; 
    this.mSize = 0;
    this.mGarbage = false;
  }
  
  public void append(long paramLong, E paramE) {
    int i = this.mSize;
    if (i != 0 && paramLong <= this.mKeys[i - 1]) {
      put(paramLong, paramE);
      return;
    } 
    if (this.mGarbage && this.mSize >= this.mKeys.length)
      gc(); 
    this.mKeys = GrowingArrayUtils.append(this.mKeys, this.mSize, paramLong);
    this.mValues = GrowingArrayUtils.append(this.mValues, this.mSize, paramE);
    this.mSize++;
  }
  
  public String toString() {
    if (size() <= 0)
      return "{}"; 
    StringBuilder stringBuilder = new StringBuilder(this.mSize * 28);
    stringBuilder.append('{');
    for (byte b = 0; b < this.mSize; b++) {
      if (b > 0)
        stringBuilder.append(", "); 
      long l = keyAt(b);
      stringBuilder.append(l);
      stringBuilder.append('=');
      E e = valueAt(b);
      if (e != this) {
        stringBuilder.append(e);
      } else {
        stringBuilder.append("(this Map)");
      } 
    } 
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  class StringParcelling implements Parcelling<LongSparseArray<String>> {
    public void parcel(LongSparseArray<String> param1LongSparseArray, Parcel param1Parcel, int param1Int) {
      if (param1LongSparseArray == null) {
        param1Parcel.writeInt(-1);
        return;
      } 
      param1Int = param1LongSparseArray.mSize;
      param1Parcel.writeInt(param1Int);
      param1Parcel.writeLongArray(param1LongSparseArray.mKeys);
      param1Parcel.writeStringArray(Arrays.<String, Object>copyOfRange(param1LongSparseArray.mValues, 0, param1Int, String[].class));
    }
    
    public LongSparseArray<String> unparcel(Parcel param1Parcel) {
      boolean bool;
      int i = param1Parcel.readInt();
      if (i == -1)
        return null; 
      LongSparseArray<String> longSparseArray = new LongSparseArray(0);
      LongSparseArray.access$002(longSparseArray, i);
      LongSparseArray.access$102(longSparseArray, param1Parcel.createLongArray());
      LongSparseArray.access$202(longSparseArray, (Object[])param1Parcel.createStringArray());
      if (longSparseArray.mKeys.length >= i) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkArgument(bool);
      if (longSparseArray.mValues.length >= i) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkArgument(bool);
      if (i > 0) {
        long l = longSparseArray.mKeys[0];
        for (byte b = 1; b < i; b++) {
          if (l < longSparseArray.mKeys[b]) {
            bool = true;
          } else {
            bool = false;
          } 
          Preconditions.checkArgument(bool);
        } 
      } 
      return longSparseArray;
    }
  }
}
