package android.util;

import android.graphics.Insets;

public class RotationUtils {
  public static Insets rotateInsets(Insets paramInsets, int paramInt) {
    Insets insets;
    if (paramInsets == null || paramInsets == Insets.NONE)
      return paramInsets; 
    if (paramInt != 0)
      if (paramInt != 1) {
        StringBuilder stringBuilder;
        if (paramInt != 2) {
          if (paramInt == 3) {
            paramInsets = Insets.of(paramInsets.bottom, paramInsets.left, paramInsets.top, paramInsets.right);
          } else {
            stringBuilder = new StringBuilder();
            stringBuilder.append("unknown rotation: ");
            stringBuilder.append(paramInt);
            throw new IllegalArgumentException(stringBuilder.toString());
          } 
        } else {
          insets = Insets.of(((Insets)stringBuilder).right, ((Insets)stringBuilder).bottom, ((Insets)stringBuilder).left, ((Insets)stringBuilder).top);
        } 
      } else {
        insets = Insets.of(insets.top, insets.right, insets.bottom, insets.left);
      }  
    return insets;
  }
}
