package android.util;

import java.io.Closeable;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public final class JsonWriter implements Closeable {
  private String indent;
  
  private boolean lenient;
  
  private final Writer out;
  
  private String separator;
  
  private final List<JsonScope> stack;
  
  public JsonWriter(Writer paramWriter) {
    ArrayList<JsonScope> arrayList = new ArrayList();
    arrayList.add(JsonScope.EMPTY_DOCUMENT);
    this.separator = ":";
    if (paramWriter != null) {
      this.out = paramWriter;
      return;
    } 
    throw new NullPointerException("out == null");
  }
  
  public void setIndent(String paramString) {
    if (paramString.isEmpty()) {
      this.indent = null;
      this.separator = ":";
    } else {
      this.indent = paramString;
      this.separator = ": ";
    } 
  }
  
  public void setLenient(boolean paramBoolean) {
    this.lenient = paramBoolean;
  }
  
  public boolean isLenient() {
    return this.lenient;
  }
  
  public JsonWriter beginArray() throws IOException {
    return open(JsonScope.EMPTY_ARRAY, "[");
  }
  
  public JsonWriter endArray() throws IOException {
    return close(JsonScope.EMPTY_ARRAY, JsonScope.NONEMPTY_ARRAY, "]");
  }
  
  public JsonWriter beginObject() throws IOException {
    return open(JsonScope.EMPTY_OBJECT, "{");
  }
  
  public JsonWriter endObject() throws IOException {
    return close(JsonScope.EMPTY_OBJECT, JsonScope.NONEMPTY_OBJECT, "}");
  }
  
  private JsonWriter open(JsonScope paramJsonScope, String paramString) throws IOException {
    beforeValue(true);
    this.stack.add(paramJsonScope);
    this.out.write(paramString);
    return this;
  }
  
  private JsonWriter close(JsonScope paramJsonScope1, JsonScope paramJsonScope2, String paramString) throws IOException {
    JsonScope jsonScope = peek();
    if (jsonScope == paramJsonScope2 || jsonScope == paramJsonScope1) {
      List<JsonScope> list = this.stack;
      list.remove(list.size() - 1);
      if (jsonScope == paramJsonScope2)
        newline(); 
      this.out.write(paramString);
      return this;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Nesting problem: ");
    stringBuilder.append(this.stack);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  private JsonScope peek() {
    List<JsonScope> list = this.stack;
    return list.get(list.size() - 1);
  }
  
  private void replaceTop(JsonScope paramJsonScope) {
    List<JsonScope> list = this.stack;
    list.set(list.size() - 1, paramJsonScope);
  }
  
  public JsonWriter name(String paramString) throws IOException {
    if (paramString != null) {
      beforeName();
      string(paramString);
      return this;
    } 
    throw new NullPointerException("name == null");
  }
  
  public JsonWriter value(String paramString) throws IOException {
    if (paramString == null)
      return nullValue(); 
    beforeValue(false);
    string(paramString);
    return this;
  }
  
  public JsonWriter nullValue() throws IOException {
    beforeValue(false);
    this.out.write("null");
    return this;
  }
  
  public JsonWriter value(boolean paramBoolean) throws IOException {
    String str;
    beforeValue(false);
    Writer writer = this.out;
    if (paramBoolean) {
      str = "true";
    } else {
      str = "false";
    } 
    writer.write(str);
    return this;
  }
  
  public JsonWriter value(double paramDouble) throws IOException {
    if (this.lenient || (!Double.isNaN(paramDouble) && !Double.isInfinite(paramDouble))) {
      beforeValue(false);
      this.out.append(Double.toString(paramDouble));
      return this;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Numeric values must be finite, but was ");
    stringBuilder.append(paramDouble);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public JsonWriter value(long paramLong) throws IOException {
    beforeValue(false);
    this.out.write(Long.toString(paramLong));
    return this;
  }
  
  public JsonWriter value(Number paramNumber) throws IOException {
    if (paramNumber == null)
      return nullValue(); 
    String str = paramNumber.toString();
    if (this.lenient || (
      !str.equals("-Infinity") && !str.equals("Infinity") && !str.equals("NaN"))) {
      beforeValue(false);
      this.out.append(str);
      return this;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Numeric values must be finite, but was ");
    stringBuilder.append(paramNumber);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void flush() throws IOException {
    this.out.flush();
  }
  
  public void close() throws IOException {
    this.out.close();
    if (peek() == JsonScope.NONEMPTY_DOCUMENT)
      return; 
    throw new IOException("Incomplete document");
  }
  
  private void string(String paramString) throws IOException {
    this.out.write("\"");
    byte b;
    int i;
    for (b = 0, i = paramString.length(); b < i; b++) {
      char c = paramString.charAt(b);
      if (c != '\f') {
        if (c != '\r') {
          if (c != '"' && c != '\\') {
            if (c != ' ' && c != ' ') {
              switch (c) {
                default:
                  if (c <= '\037') {
                    this.out.write(String.format("\\u%04x", new Object[] { Integer.valueOf(c) }));
                    break;
                  } 
                  this.out.write(c);
                  break;
                case '\n':
                  this.out.write("\\n");
                  break;
                case '\t':
                  this.out.write("\\t");
                  break;
                case '\b':
                  this.out.write("\\b");
                  break;
              } 
            } else {
              this.out.write(String.format("\\u%04x", new Object[] { Integer.valueOf(c) }));
            } 
          } else {
            this.out.write(92);
            this.out.write(c);
          } 
        } else {
          this.out.write("\\r");
        } 
      } else {
        this.out.write("\\f");
      } 
    } 
    this.out.write("\"");
  }
  
  private void newline() throws IOException {
    if (this.indent == null)
      return; 
    this.out.write("\n");
    for (byte b = 1; b < this.stack.size(); b++)
      this.out.write(this.indent); 
  }
  
  private void beforeName() throws IOException {
    JsonScope jsonScope = peek();
    if (jsonScope == JsonScope.NONEMPTY_OBJECT) {
      this.out.write(44);
    } else if (jsonScope != JsonScope.EMPTY_OBJECT) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Nesting problem: ");
      stringBuilder.append(this.stack);
      throw new IllegalStateException(stringBuilder.toString());
    } 
    newline();
    replaceTop(JsonScope.DANGLING_NAME);
  }
  
  private void beforeValue(boolean paramBoolean) throws IOException {
    int i = null.$SwitchMap$android$util$JsonScope[peek().ordinal()];
    if (i != 1) {
      if (i != 2) {
        if (i != 3) {
          if (i != 4) {
            if (i != 5) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Nesting problem: ");
              stringBuilder.append(this.stack);
              throw new IllegalStateException(stringBuilder.toString());
            } 
            throw new IllegalStateException("JSON must have only one top-level value.");
          } 
          this.out.append(this.separator);
          replaceTop(JsonScope.NONEMPTY_OBJECT);
        } else {
          this.out.append(',');
          newline();
        } 
      } else {
        replaceTop(JsonScope.NONEMPTY_ARRAY);
        newline();
      } 
    } else {
      if (this.lenient || paramBoolean) {
        replaceTop(JsonScope.NONEMPTY_DOCUMENT);
        return;
      } 
      throw new IllegalStateException("JSON must start with an array or an object.");
    } 
  }
}
