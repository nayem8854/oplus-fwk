package android.util;

import android.os.Build;
import android.os.SystemClock;
import android.os.Trace;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TimingsTraceLog {
  private static final boolean DEBUG_BOOT_TIME = Build.IS_USER ^ true;
  
  private static final int MAX_NESTED_CALLS = 10;
  
  private int mCurrentLevel = -1;
  
  private final int mMaxNestedCalls;
  
  private final String[] mStartNames;
  
  private final long[] mStartTimes;
  
  private final String mTag;
  
  private final long mThreadId;
  
  private final long mTraceTag;
  
  public TimingsTraceLog(String paramString, long paramLong) {
    this(paramString, paramLong, b);
  }
  
  public TimingsTraceLog(String paramString, long paramLong, int paramInt) {
    this.mTag = paramString;
    this.mTraceTag = paramLong;
    this.mThreadId = Thread.currentThread().getId();
    this.mMaxNestedCalls = paramInt;
    if (paramInt > 0) {
      this.mStartNames = new String[paramInt];
      this.mStartTimes = new long[paramInt];
    } else {
      this.mStartNames = null;
      this.mStartTimes = null;
    } 
  }
  
  public void traceBegin(String paramString) {
    assertSameThread();
    Trace.traceBegin(this.mTraceTag, paramString);
    if (!DEBUG_BOOT_TIME)
      return; 
    int i = this.mCurrentLevel;
    if (i + 1 >= this.mMaxNestedCalls) {
      String str = this.mTag;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("not tracing duration of '");
      stringBuilder.append(paramString);
      stringBuilder.append("' because already reached ");
      stringBuilder.append(this.mMaxNestedCalls);
      stringBuilder.append(" levels");
      Slog.w(str, stringBuilder.toString());
      return;
    } 
    this.mCurrentLevel = ++i;
    this.mStartNames[i] = paramString;
    this.mStartTimes[i] = SystemClock.elapsedRealtime();
  }
  
  public void traceEnd() {
    assertSameThread();
    Trace.traceEnd(this.mTraceTag);
    if (!DEBUG_BOOT_TIME)
      return; 
    int i = this.mCurrentLevel;
    if (i < 0) {
      Slog.w(this.mTag, "traceEnd called more times than traceBegin");
      return;
    } 
    String str = this.mStartNames[i];
    long l1 = SystemClock.elapsedRealtime(), arrayOfLong[] = this.mStartTimes;
    i = this.mCurrentLevel;
    long l2 = arrayOfLong[i];
    this.mCurrentLevel = i - 1;
    logDuration(str, l1 - l2);
  }
  
  private void assertSameThread() {
    Thread thread = Thread.currentThread();
    if (thread.getId() == this.mThreadId)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Instance of TimingsTraceLog can only be called from the thread it was created on (tid: ");
    stringBuilder.append(this.mThreadId);
    stringBuilder.append("), but was from ");
    stringBuilder.append(thread.getName());
    stringBuilder.append(" (tid: ");
    stringBuilder.append(thread.getId());
    stringBuilder.append(")");
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public void logDuration(String paramString, long paramLong) {
    String str = this.mTag;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(" took to complete: ");
    stringBuilder.append(paramLong);
    stringBuilder.append("ms");
    Slog.d(str, stringBuilder.toString());
  }
  
  public final List<String> getUnfinishedTracesForDebug() {
    if (this.mStartTimes == null || this.mCurrentLevel < 0)
      return Collections.emptyList(); 
    ArrayList<String> arrayList = new ArrayList(this.mCurrentLevel + 1);
    for (byte b = 0; b <= this.mCurrentLevel; b++)
      arrayList.add(this.mStartNames[b]); 
    return arrayList;
  }
}
