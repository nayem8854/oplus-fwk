package android.util;

public class DayOfMonthCursor extends MonthDisplayHelper {
  private int mColumn;
  
  private int mRow;
  
  public DayOfMonthCursor(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super(paramInt1, paramInt2, paramInt4);
    this.mRow = getRowOf(paramInt3);
    this.mColumn = getColumnOf(paramInt3);
  }
  
  public int getSelectedRow() {
    return this.mRow;
  }
  
  public int getSelectedColumn() {
    return this.mColumn;
  }
  
  public void setSelectedRowColumn(int paramInt1, int paramInt2) {
    this.mRow = paramInt1;
    this.mColumn = paramInt2;
  }
  
  public int getSelectedDayOfMonth() {
    return getDayAt(this.mRow, this.mColumn);
  }
  
  public int getSelectedMonthOffset() {
    if (isWithinCurrentMonth(this.mRow, this.mColumn))
      return 0; 
    if (this.mRow == 0)
      return -1; 
    return 1;
  }
  
  public void setSelectedDayOfMonth(int paramInt) {
    this.mRow = getRowOf(paramInt);
    this.mColumn = getColumnOf(paramInt);
  }
  
  public boolean isSelected(int paramInt1, int paramInt2) {
    boolean bool;
    if (this.mRow == paramInt1 && this.mColumn == paramInt2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean up() {
    if (isWithinCurrentMonth(this.mRow - 1, this.mColumn)) {
      this.mRow--;
      return false;
    } 
    previousMonth();
    this.mRow = 5;
    while (!isWithinCurrentMonth(this.mRow, this.mColumn))
      this.mRow--; 
    return true;
  }
  
  public boolean down() {
    if (isWithinCurrentMonth(this.mRow + 1, this.mColumn)) {
      this.mRow++;
      return false;
    } 
    nextMonth();
    this.mRow = 0;
    while (!isWithinCurrentMonth(this.mRow, this.mColumn))
      this.mRow++; 
    return true;
  }
  
  public boolean left() {
    int i = this.mColumn;
    if (i == 0) {
      this.mRow--;
      this.mColumn = 6;
    } else {
      this.mColumn = i - 1;
    } 
    if (isWithinCurrentMonth(this.mRow, this.mColumn))
      return false; 
    previousMonth();
    i = getNumberOfDaysInMonth();
    this.mRow = getRowOf(i);
    this.mColumn = getColumnOf(i);
    return true;
  }
  
  public boolean right() {
    int i = this.mColumn;
    if (i == 6) {
      this.mRow++;
      this.mColumn = 0;
    } else {
      this.mColumn = i + 1;
    } 
    if (isWithinCurrentMonth(this.mRow, this.mColumn))
      return false; 
    nextMonth();
    this.mRow = 0;
    this.mColumn = 0;
    while (!isWithinCurrentMonth(this.mRow, this.mColumn))
      this.mColumn++; 
    return true;
  }
}
