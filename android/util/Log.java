package android.util;

import android.annotation.SystemApi;
import com.android.internal.util.FastPrintWriter;
import com.android.internal.util.LineBreakBufferedWriter;
import dalvik.annotation.optimization.FastNative;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class Log {
  public static final int ASSERT = 7;
  
  public static final int DEBUG = 3;
  
  public static final int ERROR = 6;
  
  public static final int INFO = 4;
  
  public static final int LOG_ID_CRASH = 4;
  
  public static final int LOG_ID_EVENTS = 2;
  
  public static final int LOG_ID_MAIN = 0;
  
  public static final int LOG_ID_PERFORMANCE = 6;
  
  public static final int LOG_ID_RADIO = 1;
  
  public static final int LOG_ID_SYSTEM = 3;
  
  public static final int VERBOSE = 2;
  
  public static final int WARN = 5;
  
  public static interface TerribleFailureHandler {
    void onTerribleFailure(String param1String, Log.TerribleFailure param1TerribleFailure, boolean param1Boolean);
  }
  
  public static class TerribleFailure extends Exception {
    TerribleFailure(String param1String, Throwable param1Throwable) {
      super(param1String, param1Throwable);
    }
  }
  
  private static TerribleFailureHandler sWtfHandler = (TerribleFailureHandler)new Object();
  
  public static int v(String paramString1, String paramString2) {
    return println_native(0, 2, paramString1, paramString2);
  }
  
  public static int v(String paramString1, String paramString2, Throwable paramThrowable) {
    return printlns(0, 2, paramString1, paramString2, paramThrowable);
  }
  
  public static int d(String paramString1, String paramString2) {
    return println_native(0, 3, paramString1, paramString2);
  }
  
  public static int d(String paramString1, String paramString2, Throwable paramThrowable) {
    return printlns(0, 3, paramString1, paramString2, paramThrowable);
  }
  
  public static int i(String paramString1, String paramString2) {
    return println_native(0, 4, paramString1, paramString2);
  }
  
  public static int i(String paramString1, String paramString2, Throwable paramThrowable) {
    return printlns(0, 4, paramString1, paramString2, paramThrowable);
  }
  
  public static int p(String paramString1, String paramString2) {
    return println_native(6, 4, paramString1, paramString2);
  }
  
  public static int w(String paramString1, String paramString2) {
    return println_native(0, 5, paramString1, paramString2);
  }
  
  public static int w(String paramString1, String paramString2, Throwable paramThrowable) {
    return printlns(0, 5, paramString1, paramString2, paramThrowable);
  }
  
  public static int w(String paramString, Throwable paramThrowable) {
    return printlns(0, 5, paramString, "", paramThrowable);
  }
  
  public static int e(String paramString1, String paramString2) {
    return println_native(0, 6, paramString1, paramString2);
  }
  
  public static int e(String paramString1, String paramString2, Throwable paramThrowable) {
    return printlns(0, 6, paramString1, paramString2, paramThrowable);
  }
  
  public static int wtf(String paramString1, String paramString2) {
    return wtf(0, paramString1, paramString2, null, false, false);
  }
  
  public static int wtfStack(String paramString1, String paramString2) {
    return wtf(0, paramString1, paramString2, null, true, false);
  }
  
  public static int wtf(String paramString, Throwable paramThrowable) {
    return wtf(0, paramString, paramThrowable.getMessage(), paramThrowable, false, false);
  }
  
  public static int wtf(String paramString1, String paramString2, Throwable paramThrowable) {
    return wtf(0, paramString1, paramString2, paramThrowable, false, false);
  }
  
  static int wtf(int paramInt, String paramString1, String paramString2, Throwable paramThrowable, boolean paramBoolean1, boolean paramBoolean2) {
    TerribleFailure terribleFailure = new TerribleFailure(paramString2, paramThrowable);
    if (paramBoolean1)
      paramThrowable = terribleFailure; 
    paramInt = printlns(paramInt, 6, paramString1, paramString2, paramThrowable);
    sWtfHandler.onTerribleFailure(paramString1, terribleFailure, paramBoolean2);
    return paramInt;
  }
  
  static void wtfQuiet(int paramInt, String paramString1, String paramString2, boolean paramBoolean) {
    TerribleFailure terribleFailure = new TerribleFailure(paramString2, null);
    sWtfHandler.onTerribleFailure(paramString1, terribleFailure, paramBoolean);
  }
  
  public static TerribleFailureHandler setWtfHandler(TerribleFailureHandler paramTerribleFailureHandler) {
    if (paramTerribleFailureHandler != null) {
      TerribleFailureHandler terribleFailureHandler = sWtfHandler;
      sWtfHandler = paramTerribleFailureHandler;
      return terribleFailureHandler;
    } 
    throw new NullPointerException("handler == null");
  }
  
  public static String getStackTraceString(Throwable paramThrowable) {
    if (paramThrowable == null)
      return ""; 
    Throwable throwable = paramThrowable;
    while (throwable != null) {
      if (throwable instanceof java.net.UnknownHostException)
        return ""; 
      throwable = throwable.getCause();
    } 
    StringWriter stringWriter = new StringWriter();
    FastPrintWriter fastPrintWriter = new FastPrintWriter(stringWriter, false, 256);
    paramThrowable.printStackTrace((PrintWriter)fastPrintWriter);
    fastPrintWriter.flush();
    return stringWriter.toString();
  }
  
  public static int println(int paramInt, String paramString1, String paramString2) {
    return println_native(0, paramInt, paramString1, paramString2);
  }
  
  @SystemApi(client = SystemApi.Client.MODULE_LIBRARIES)
  public static int logToRadioBuffer(int paramInt, String paramString1, String paramString2) {
    return println_native(1, paramInt, paramString1, paramString2);
  }
  
  public static int printlns(int paramInt1, int paramInt2, String paramString1, String paramString2, Throwable paramThrowable) {
    ImmediateLogWriter immediateLogWriter = new ImmediateLogWriter(paramInt1, paramInt2, paramString1);
    paramInt2 = PreloadHolder.LOGGER_ENTRY_MAX_PAYLOAD;
    if (paramString1 != null) {
      paramInt1 = paramString1.length();
    } else {
      paramInt1 = 0;
    } 
    paramInt1 = Math.max(paramInt2 - 2 - paramInt1 - 32, 100);
    LineBreakBufferedWriter lineBreakBufferedWriter = new LineBreakBufferedWriter(immediateLogWriter, paramInt1);
    lineBreakBufferedWriter.println(paramString2);
    if (paramThrowable != null) {
      Throwable throwable = paramThrowable;
      while (throwable != null && 
        !(throwable instanceof java.net.UnknownHostException)) {
        if (throwable instanceof android.os.DeadSystemException) {
          lineBreakBufferedWriter.println("DeadSystemException: The system died; earlier logs will point to the root cause");
          break;
        } 
        throwable = throwable.getCause();
      } 
      if (throwable == null)
        paramThrowable.printStackTrace((PrintWriter)lineBreakBufferedWriter); 
    } 
    lineBreakBufferedWriter.flush();
    return immediateLogWriter.getWritten();
  }
  
  @FastNative
  public static native boolean isLoggable(String paramString, int paramInt);
  
  private static native int logger_entry_max_payload_native();
  
  public static native int println_native(int paramInt1, int paramInt2, String paramString1, String paramString2);
  
  static class PreloadHolder {
    public static final int LOGGER_ENTRY_MAX_PAYLOAD = Log.logger_entry_max_payload_native();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Level {}
  
  private static class ImmediateLogWriter extends Writer {
    private int bufID;
    
    private int priority;
    
    private String tag;
    
    private int written = 0;
    
    public ImmediateLogWriter(int param1Int1, int param1Int2, String param1String) {
      this.bufID = param1Int1;
      this.priority = param1Int2;
      this.tag = param1String;
    }
    
    public int getWritten() {
      return this.written;
    }
    
    public void write(char[] param1ArrayOfchar, int param1Int1, int param1Int2) {
      this.written += Log.println_native(this.bufID, this.priority, this.tag, new String(param1ArrayOfchar, param1Int1, param1Int2));
    }
    
    public void flush() {}
    
    public void close() {}
  }
}
