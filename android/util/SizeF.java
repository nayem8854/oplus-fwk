package android.util;

import com.android.internal.util.Preconditions;

public final class SizeF {
  private final float mHeight;
  
  private final float mWidth;
  
  public SizeF(float paramFloat1, float paramFloat2) {
    this.mWidth = Preconditions.checkArgumentFinite(paramFloat1, "width");
    this.mHeight = Preconditions.checkArgumentFinite(paramFloat2, "height");
  }
  
  public float getWidth() {
    return this.mWidth;
  }
  
  public float getHeight() {
    return this.mHeight;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    if (paramObject == null)
      return false; 
    if (this == paramObject)
      return true; 
    if (paramObject instanceof SizeF) {
      paramObject = paramObject;
      boolean bool1 = bool;
      if (this.mWidth == ((SizeF)paramObject).mWidth) {
        bool1 = bool;
        if (this.mHeight == ((SizeF)paramObject).mHeight)
          bool1 = true; 
      } 
      return bool1;
    } 
    return false;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.mWidth);
    stringBuilder.append("x");
    stringBuilder.append(this.mHeight);
    return stringBuilder.toString();
  }
  
  private static NumberFormatException invalidSizeF(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid SizeF: \"");
    stringBuilder.append(paramString);
    stringBuilder.append("\"");
    throw new NumberFormatException(stringBuilder.toString());
  }
  
  public static SizeF parseSizeF(String paramString) throws NumberFormatException {
    Preconditions.checkNotNull(paramString, "string must not be null");
    int i = paramString.indexOf('*');
    int j = i;
    if (i < 0)
      j = paramString.indexOf('x'); 
    if (j >= 0)
      try {
        float f = Float.parseFloat(paramString.substring(0, j));
        return 
          new SizeF(f, Float.parseFloat(paramString.substring(j + 1)));
      } catch (NumberFormatException numberFormatException) {
        throw invalidSizeF(paramString);
      } catch (IllegalArgumentException illegalArgumentException) {
        throw invalidSizeF(paramString);
      }  
    throw invalidSizeF(paramString);
  }
  
  public int hashCode() {
    return Float.floatToIntBits(this.mWidth) ^ Float.floatToIntBits(this.mHeight);
  }
}
