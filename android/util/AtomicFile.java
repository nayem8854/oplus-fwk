package android.util;

import android.os.FileUtils;
import android.os.SystemClock;
import com.android.internal.logging.EventLogTags;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.function.Consumer;

public class AtomicFile {
  private static final String LOG_TAG = "AtomicFile";
  
  private final File mBaseName;
  
  private final String mCommitTag;
  
  private final File mLegacyBackupName;
  
  private final File mNewName;
  
  private long mStartTime;
  
  public AtomicFile(File paramFile) {
    this(paramFile, null);
  }
  
  public AtomicFile(File paramFile, String paramString) {
    this.mBaseName = paramFile;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramFile.getPath());
    stringBuilder.append(".new");
    this.mNewName = new File(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append(paramFile.getPath());
    stringBuilder.append(".bak");
    this.mLegacyBackupName = new File(stringBuilder.toString());
    this.mCommitTag = paramString;
  }
  
  public File getBaseFile() {
    return this.mBaseName;
  }
  
  public void delete() {
    this.mBaseName.delete();
    this.mNewName.delete();
    this.mLegacyBackupName.delete();
  }
  
  public FileOutputStream startWrite() throws IOException {
    long l;
    if (this.mCommitTag != null) {
      l = SystemClock.uptimeMillis();
    } else {
      l = 0L;
    } 
    return startWrite(l);
  }
  
  public FileOutputStream startWrite(long paramLong) throws IOException {
    this.mStartTime = paramLong;
    if (this.mLegacyBackupName.exists())
      rename(this.mLegacyBackupName, this.mBaseName); 
    try {
      return new FileOutputStream(this.mNewName);
    } catch (FileNotFoundException fileNotFoundException) {
      File file = this.mNewName.getParentFile();
      if (file.mkdirs()) {
        FileUtils.setPermissions(file.getPath(), 505, -1, -1);
        try {
          return new FileOutputStream(this.mNewName);
        } catch (FileNotFoundException fileNotFoundException1) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Failed to create new file ");
          stringBuilder1.append(this.mNewName);
          throw new IOException(stringBuilder1.toString(), fileNotFoundException1);
        } 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to create directory for ");
      stringBuilder.append(this.mNewName);
      throw new IOException(stringBuilder.toString());
    } 
  }
  
  public void finishWrite(FileOutputStream paramFileOutputStream) {
    if (paramFileOutputStream == null)
      return; 
    if (!FileUtils.sync(paramFileOutputStream))
      Log.e("AtomicFile", "Failed to sync file output stream"); 
    try {
      paramFileOutputStream.close();
    } catch (IOException iOException) {
      Log.e("AtomicFile", "Failed to close file output stream", iOException);
    } 
    rename(this.mNewName, this.mBaseName);
    String str = this.mCommitTag;
    if (str != null) {
      long l1 = SystemClock.uptimeMillis(), l2 = this.mStartTime;
      EventLogTags.writeCommitSysConfigFile(str, l1 - l2);
    } 
  }
  
  public void failWrite(FileOutputStream paramFileOutputStream) {
    if (paramFileOutputStream == null)
      return; 
    if (!FileUtils.sync(paramFileOutputStream))
      Log.e("AtomicFile", "Failed to sync file output stream"); 
    try {
      paramFileOutputStream.close();
    } catch (IOException iOException) {
      Log.e("AtomicFile", "Failed to close file output stream", iOException);
    } 
    if (!this.mNewName.delete()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to delete new file ");
      stringBuilder.append(this.mNewName);
      Log.e("AtomicFile", stringBuilder.toString());
    } 
  }
  
  @Deprecated
  public void truncate() throws IOException {
    try {
      FileOutputStream fileOutputStream = new FileOutputStream();
      this(this.mBaseName);
      FileUtils.sync(fileOutputStream);
      fileOutputStream.close();
      return;
    } catch (FileNotFoundException fileNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Couldn't append ");
      stringBuilder.append(this.mBaseName);
      throw new IOException(stringBuilder.toString());
    } catch (IOException iOException) {
      return;
    } 
  }
  
  @Deprecated
  public FileOutputStream openAppend() throws IOException {
    try {
      return new FileOutputStream(this.mBaseName, true);
    } catch (FileNotFoundException fileNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Couldn't append ");
      stringBuilder.append(this.mBaseName);
      throw new IOException(stringBuilder.toString());
    } 
  }
  
  public FileInputStream openRead() throws FileNotFoundException {
    if (this.mLegacyBackupName.exists())
      rename(this.mLegacyBackupName, this.mBaseName); 
    if (this.mNewName.exists() && this.mBaseName.exists() && 
      !this.mNewName.delete()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to delete outdated new file ");
      stringBuilder.append(this.mNewName);
      Log.e("AtomicFile", stringBuilder.toString());
    } 
    return new FileInputStream(this.mBaseName);
  }
  
  public boolean exists() {
    return (this.mBaseName.exists() || this.mLegacyBackupName.exists());
  }
  
  public long getLastModifiedTime() {
    if (this.mLegacyBackupName.exists())
      return this.mLegacyBackupName.lastModified(); 
    return this.mBaseName.lastModified();
  }
  
  public byte[] readFully() throws IOException {
    FileInputStream fileInputStream = openRead();
    int i = 0;
    try {
      int j = fileInputStream.available();
      byte[] arrayOfByte = new byte[j];
      while (true) {
        j = fileInputStream.read(arrayOfByte, i, arrayOfByte.length - i);
        if (j <= 0)
          return arrayOfByte; 
        i += j;
        j = fileInputStream.available();
        byte[] arrayOfByte1 = arrayOfByte;
        if (j > arrayOfByte.length - i) {
          arrayOfByte1 = new byte[i + j];
          System.arraycopy(arrayOfByte, 0, arrayOfByte1, 0, i);
        } 
        arrayOfByte = arrayOfByte1;
      } 
    } finally {
      fileInputStream.close();
    } 
  }
  
  public void write(Consumer<FileOutputStream> paramConsumer) {
    FileOutputStream fileOutputStream = null;
    try {
      FileOutputStream fileOutputStream1 = startWrite();
      fileOutputStream = fileOutputStream1;
      paramConsumer.accept(fileOutputStream1);
      fileOutputStream = fileOutputStream1;
      finishWrite(fileOutputStream1);
      return;
    } finally {
      paramConsumer = null;
    } 
  }
  
  private static void rename(File paramFile1, File paramFile2) {
    if (paramFile2.isDirectory() && 
      !paramFile2.delete()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to delete file which is a directory ");
      stringBuilder.append(paramFile2);
      Log.e("AtomicFile", stringBuilder.toString());
    } 
    if (!paramFile1.renameTo(paramFile2)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to rename ");
      stringBuilder.append(paramFile1);
      stringBuilder.append(" to ");
      stringBuilder.append(paramFile2);
      Log.e("AtomicFile", stringBuilder.toString());
    } 
  }
}
