package android.util;

import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import libcore.internal.StringPool;

public final class JsonReader implements Closeable {
  private final StringPool stringPool = new StringPool();
  
  private boolean lenient = false;
  
  private final char[] buffer = new char[1024];
  
  private int pos = 0;
  
  private int limit = 0;
  
  private int bufferStartLine = 1;
  
  private int bufferStartColumn = 1;
  
  private final List<JsonScope> stack = new ArrayList<>();
  
  private static final String FALSE = "false";
  
  private static final String TRUE = "true";
  
  private final Reader in;
  
  private String name;
  
  private boolean skipping;
  
  private JsonToken token;
  
  private String value;
  
  private int valueLength;
  
  private int valuePos;
  
  public JsonReader(Reader paramReader) {
    push(JsonScope.EMPTY_DOCUMENT);
    this.skipping = false;
    if (paramReader != null) {
      this.in = paramReader;
      return;
    } 
    throw new NullPointerException("in == null");
  }
  
  public void setLenient(boolean paramBoolean) {
    this.lenient = paramBoolean;
  }
  
  public boolean isLenient() {
    return this.lenient;
  }
  
  public void beginArray() throws IOException {
    expect(JsonToken.BEGIN_ARRAY);
  }
  
  public void endArray() throws IOException {
    expect(JsonToken.END_ARRAY);
  }
  
  public void beginObject() throws IOException {
    expect(JsonToken.BEGIN_OBJECT);
  }
  
  public void endObject() throws IOException {
    expect(JsonToken.END_OBJECT);
  }
  
  private void expect(JsonToken paramJsonToken) throws IOException {
    peek();
    if (this.token == paramJsonToken) {
      advance();
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Expected ");
    stringBuilder.append(paramJsonToken);
    stringBuilder.append(" but was ");
    stringBuilder.append(peek());
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public boolean hasNext() throws IOException {
    boolean bool;
    peek();
    if (this.token != JsonToken.END_OBJECT && this.token != JsonToken.END_ARRAY) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public JsonToken peek() throws IOException {
    JsonToken jsonToken = this.token;
    if (jsonToken != null)
      return jsonToken; 
    switch (peekStack()) {
      default:
        throw new AssertionError();
      case CLOSED:
        throw new IllegalStateException("JsonReader is closed");
      case NONEMPTY_DOCUMENT:
        try {
          jsonToken = nextValue();
          if (this.lenient)
            return jsonToken; 
          throw syntaxError("Expected EOF");
        } catch (EOFException eOFException) {
          JsonToken jsonToken1 = JsonToken.END_DOCUMENT;
          return jsonToken1;
        } 
      case NONEMPTY_OBJECT:
        return nextInObject(false);
      case DANGLING_NAME:
        return objectValue();
      case EMPTY_OBJECT:
        return nextInObject(true);
      case NONEMPTY_ARRAY:
        return nextInArray(false);
      case EMPTY_ARRAY:
        return nextInArray(true);
      case EMPTY_DOCUMENT:
        break;
    } 
    replaceTop(JsonScope.NONEMPTY_DOCUMENT);
    jsonToken = nextValue();
    if (this.lenient || this.token == JsonToken.BEGIN_ARRAY || this.token == JsonToken.BEGIN_OBJECT)
      return jsonToken; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Expected JSON document to start with '[' or '{' but was ");
    stringBuilder.append(this.token);
    throw new IOException(stringBuilder.toString());
  }
  
  private JsonToken advance() throws IOException {
    peek();
    JsonToken jsonToken = this.token;
    this.token = null;
    this.value = null;
    this.name = null;
    return jsonToken;
  }
  
  public String nextName() throws IOException {
    peek();
    if (this.token == JsonToken.NAME) {
      String str = this.name;
      advance();
      return str;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Expected a name but was ");
    stringBuilder.append(peek());
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public String nextString() throws IOException {
    peek();
    if (this.token == JsonToken.STRING || this.token == JsonToken.NUMBER) {
      String str = this.value;
      advance();
      return str;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Expected a string but was ");
    stringBuilder.append(peek());
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public boolean nextBoolean() throws IOException {
    peek();
    if (this.token == JsonToken.BOOLEAN) {
      boolean bool;
      if (this.value == "true") {
        bool = true;
      } else {
        bool = false;
      } 
      advance();
      return bool;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Expected a boolean but was ");
    stringBuilder.append(this.token);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public void nextNull() throws IOException {
    peek();
    if (this.token == JsonToken.NULL) {
      advance();
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Expected null but was ");
    stringBuilder.append(this.token);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public double nextDouble() throws IOException {
    peek();
    if (this.token == JsonToken.STRING || this.token == JsonToken.NUMBER) {
      double d = Double.parseDouble(this.value);
      advance();
      return d;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Expected a double but was ");
    stringBuilder.append(this.token);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public long nextLong() throws IOException {
    peek();
    if (this.token == JsonToken.STRING || this.token == JsonToken.NUMBER) {
      long l;
      try {
        l = Long.parseLong(this.value);
      } catch (NumberFormatException numberFormatException) {
        double d = Double.parseDouble(this.value);
        l = (long)d;
        if (l != d)
          throw new NumberFormatException(this.value); 
      } 
      advance();
      return l;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Expected a long but was ");
    stringBuilder.append(this.token);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public int nextInt() throws IOException {
    peek();
    if (this.token == JsonToken.STRING || this.token == JsonToken.NUMBER) {
      int i;
      try {
        i = Integer.parseInt(this.value);
      } catch (NumberFormatException numberFormatException) {
        double d = Double.parseDouble(this.value);
        i = (int)d;
        if (i != d)
          throw new NumberFormatException(this.value); 
      } 
      advance();
      return i;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Expected an int but was ");
    stringBuilder.append(this.token);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public void close() throws IOException {
    this.value = null;
    this.token = null;
    this.stack.clear();
    this.stack.add(JsonScope.CLOSED);
    this.in.close();
  }
  
  public void skipValue() throws IOException {
    // Byte code:
    //   0: aload_0
    //   1: iconst_1
    //   2: putfield skipping : Z
    //   5: aload_0
    //   6: invokevirtual hasNext : ()Z
    //   9: ifeq -> 92
    //   12: aload_0
    //   13: invokevirtual peek : ()Landroid/util/JsonToken;
    //   16: getstatic android/util/JsonToken.END_DOCUMENT : Landroid/util/JsonToken;
    //   19: if_acmpeq -> 92
    //   22: iconst_0
    //   23: istore_1
    //   24: aload_0
    //   25: invokespecial advance : ()Landroid/util/JsonToken;
    //   28: astore_2
    //   29: aload_2
    //   30: getstatic android/util/JsonToken.BEGIN_ARRAY : Landroid/util/JsonToken;
    //   33: if_acmpeq -> 73
    //   36: aload_2
    //   37: getstatic android/util/JsonToken.BEGIN_OBJECT : Landroid/util/JsonToken;
    //   40: if_acmpne -> 46
    //   43: goto -> 73
    //   46: aload_2
    //   47: getstatic android/util/JsonToken.END_ARRAY : Landroid/util/JsonToken;
    //   50: if_acmpeq -> 65
    //   53: getstatic android/util/JsonToken.END_OBJECT : Landroid/util/JsonToken;
    //   56: astore_3
    //   57: iload_1
    //   58: istore #4
    //   60: aload_2
    //   61: aload_3
    //   62: if_acmpne -> 78
    //   65: iload_1
    //   66: iconst_1
    //   67: isub
    //   68: istore #4
    //   70: goto -> 78
    //   73: iload_1
    //   74: iconst_1
    //   75: iadd
    //   76: istore #4
    //   78: iload #4
    //   80: istore_1
    //   81: iload #4
    //   83: ifne -> 24
    //   86: aload_0
    //   87: iconst_0
    //   88: putfield skipping : Z
    //   91: return
    //   92: new java/lang/IllegalStateException
    //   95: astore_2
    //   96: aload_2
    //   97: ldc_w 'No element left to skip'
    //   100: invokespecial <init> : (Ljava/lang/String;)V
    //   103: aload_2
    //   104: athrow
    //   105: astore_2
    //   106: aload_0
    //   107: iconst_0
    //   108: putfield skipping : Z
    //   111: aload_2
    //   112: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #549	-> 0
    //   #551	-> 5
    //   #554	-> 22
    //   #556	-> 24
    //   #557	-> 29
    //   #559	-> 46
    //   #560	-> 65
    //   #558	-> 73
    //   #562	-> 78
    //   #564	-> 86
    //   #565	-> 91
    //   #566	-> 91
    //   #552	-> 92
    //   #564	-> 105
    //   #565	-> 111
    // Exception table:
    //   from	to	target	type
    //   5	22	105	finally
    //   24	29	105	finally
    //   29	43	105	finally
    //   46	57	105	finally
    //   92	105	105	finally
  }
  
  private JsonScope peekStack() {
    List<JsonScope> list = this.stack;
    return list.get(list.size() - 1);
  }
  
  private JsonScope pop() {
    List<JsonScope> list = this.stack;
    return list.remove(list.size() - 1);
  }
  
  private void push(JsonScope paramJsonScope) {
    this.stack.add(paramJsonScope);
  }
  
  private void replaceTop(JsonScope paramJsonScope) {
    List<JsonScope> list = this.stack;
    list.set(list.size() - 1, paramJsonScope);
  }
  
  private JsonToken nextInArray(boolean paramBoolean) throws IOException {
    if (paramBoolean) {
      replaceTop(JsonScope.NONEMPTY_ARRAY);
    } else {
      int j = nextNonWhitespace();
      if (j != 44) {
        if (j != 59) {
          if (j == 93) {
            pop();
            JsonToken jsonToken1 = JsonToken.END_ARRAY;
            return jsonToken1;
          } 
          throw syntaxError("Unterminated array");
        } 
        checkLenient();
      } 
    } 
    int i = nextNonWhitespace();
    if (i != 44 && i != 59) {
      if (i != 93) {
        this.pos--;
        return nextValue();
      } 
      if (paramBoolean) {
        pop();
        JsonToken jsonToken1 = JsonToken.END_ARRAY;
        return jsonToken1;
      } 
    } 
    checkLenient();
    this.pos--;
    this.value = "null";
    JsonToken jsonToken = JsonToken.NULL;
    return jsonToken;
  }
  
  private JsonToken nextInObject(boolean paramBoolean) throws IOException {
    if (paramBoolean) {
      if (nextNonWhitespace() != 125) {
        this.pos--;
      } else {
        pop();
        JsonToken jsonToken = JsonToken.END_OBJECT;
        return jsonToken;
      } 
    } else {
      int j = nextNonWhitespace();
      if (j != 44 && j != 59) {
        if (j == 125) {
          pop();
          JsonToken jsonToken = JsonToken.END_OBJECT;
          return jsonToken;
        } 
        throw syntaxError("Unterminated object");
      } 
    } 
    int i = nextNonWhitespace();
    if (i != 34) {
      if (i != 39) {
        checkLenient();
        this.pos--;
        String str = nextLiteral(false);
        if (str.isEmpty())
          throw syntaxError("Expected name"); 
      } else {
        checkLenient();
        this.name = nextString((char)i);
      } 
      replaceTop(JsonScope.DANGLING_NAME);
      JsonToken jsonToken = JsonToken.NAME;
      return jsonToken;
    } 
    this.name = nextString((char)i);
  }
  
  private JsonToken objectValue() throws IOException {
    int i = nextNonWhitespace();
    if (i != 58)
      if (i == 61) {
        checkLenient();
        if (this.pos < this.limit || fillBuffer(1)) {
          char[] arrayOfChar = this.buffer;
          i = this.pos;
          if (arrayOfChar[i] == '>')
            this.pos = i + 1; 
        } 
      } else {
        throw syntaxError("Expected ':'");
      }  
    replaceTop(JsonScope.NONEMPTY_OBJECT);
    return nextValue();
  }
  
  private JsonToken nextValue() throws IOException {
    int i = nextNonWhitespace();
    if (i != 34) {
      if (i != 39) {
        if (i != 91) {
          if (i != 123) {
            this.pos--;
            return readLiteral();
          } 
          push(JsonScope.EMPTY_OBJECT);
          JsonToken jsonToken2 = JsonToken.BEGIN_OBJECT;
          return jsonToken2;
        } 
        push(JsonScope.EMPTY_ARRAY);
        JsonToken jsonToken1 = JsonToken.BEGIN_ARRAY;
        return jsonToken1;
      } 
      checkLenient();
    } 
    this.value = nextString((char)i);
    JsonToken jsonToken = JsonToken.STRING;
    return jsonToken;
  }
  
  private boolean fillBuffer(int paramInt) throws IOException {
    int j, i = 0;
    while (true) {
      j = this.pos;
      if (i < j) {
        if (this.buffer[i] == '\n') {
          this.bufferStartLine++;
          this.bufferStartColumn = 1;
        } else {
          this.bufferStartColumn++;
        } 
        i++;
        continue;
      } 
      break;
    } 
    i = this.limit;
    if (i != j) {
      this.limit = i -= j;
      char[] arrayOfChar = this.buffer;
      System.arraycopy(arrayOfChar, j, arrayOfChar, 0, i);
    } else {
      this.limit = 0;
    } 
    this.pos = 0;
    while (true) {
      Reader reader = this.in;
      char[] arrayOfChar = this.buffer;
      i = this.limit;
      i = reader.read(arrayOfChar, i, arrayOfChar.length - i);
      if (i != -1) {
        this.limit = i = this.limit + i;
        if (this.bufferStartLine == 1) {
          j = this.bufferStartColumn;
          if (j == 1 && i > 0 && this.buffer[0] == '﻿') {
            this.pos++;
            this.bufferStartColumn = j - 1;
          } 
        } 
        if (this.limit >= paramInt)
          return true; 
        continue;
      } 
      break;
    } 
    return false;
  }
  
  private int getLineNumber() {
    int i = this.bufferStartLine;
    for (byte b = 0; b < this.pos; b++, i = j) {
      int j = i;
      if (this.buffer[b] == '\n')
        j = i + 1; 
    } 
    return i;
  }
  
  private int getColumnNumber() {
    int i = this.bufferStartColumn;
    for (byte b = 0; b < this.pos; b++) {
      if (this.buffer[b] == '\n') {
        i = 1;
      } else {
        i++;
      } 
    } 
    return i;
  }
  
  private int nextNonWhitespace() throws IOException {
    while (true) {
      if (this.pos < this.limit || fillBuffer(1)) {
        char[] arrayOfChar = this.buffer;
        int i = this.pos, j = i + 1;
        i = arrayOfChar[i];
        if (i != 9 && i != 10 && i != 13 && i != 32) {
          if (i != 35) {
            if (i != 47)
              return i; 
            if (j == this.limit && !fillBuffer(1))
              return i; 
            checkLenient();
            arrayOfChar = this.buffer;
            int k = this.pos;
            j = arrayOfChar[k];
            if (j != 42) {
              if (j != 47)
                return i; 
              this.pos = k + 1;
              skipToEndOfLine();
              continue;
            } 
            this.pos = k + 1;
            if (skipTo("*/")) {
              this.pos += 2;
              continue;
            } 
            throw syntaxError("Unterminated comment");
          } 
          checkLenient();
          skipToEndOfLine();
        } 
        continue;
      } 
      throw new EOFException("End of input");
    } 
  }
  
  private void checkLenient() throws IOException {
    if (this.lenient)
      return; 
    throw syntaxError("Use JsonReader.setLenient(true) to accept malformed JSON");
  }
  
  private void skipToEndOfLine() throws IOException {
    while (this.pos < this.limit || fillBuffer(1)) {
      char[] arrayOfChar = this.buffer;
      int i = this.pos;
      this.pos = i + 1;
      i = arrayOfChar[i];
      if (i == 13 || i == 10)
        break; 
    } 
  }
  
  private boolean skipTo(String paramString) throws IOException {
    label15: while (true) {
      if (this.pos + paramString.length() <= this.limit || fillBuffer(paramString.length())) {
        for (byte b = 0; b < paramString.length(); b++) {
          if (this.buffer[this.pos + b] != paramString.charAt(b)) {
            this.pos++;
            continue label15;
          } 
        } 
        break;
      } 
      return false;
    } 
    return true;
  }
  
  private String nextString(char paramChar) throws IOException {
    StringBuilder stringBuilder = null;
    while (true) {
      int i = this.pos;
      while (true) {
        int j = this.pos;
        if (j < this.limit) {
          char[] arrayOfChar = this.buffer;
          int k = j + 1;
          j = arrayOfChar[j];
          if (j == paramChar) {
            if (this.skipping)
              return "skipped!"; 
            if (stringBuilder == null)
              return this.stringPool.get(arrayOfChar, i, k - i - 1); 
            stringBuilder.append(arrayOfChar, i, k - i - 1);
            return stringBuilder.toString();
          } 
          StringBuilder stringBuilder2 = stringBuilder;
          k = i;
          if (j == 92) {
            stringBuilder2 = stringBuilder;
            if (stringBuilder == null)
              stringBuilder2 = new StringBuilder(); 
            stringBuilder2.append(this.buffer, i, this.pos - i - 1);
            stringBuilder2.append(readEscapeCharacter());
            k = this.pos;
          } 
          stringBuilder = stringBuilder2;
          i = k;
          continue;
        } 
        break;
      } 
      StringBuilder stringBuilder1 = stringBuilder;
      if (stringBuilder == null)
        stringBuilder1 = new StringBuilder(); 
      stringBuilder1.append(this.buffer, i, this.pos - i);
      if (fillBuffer(1)) {
        stringBuilder = stringBuilder1;
        continue;
      } 
      break;
    } 
    throw syntaxError("Unterminated string");
  }
  
  private String nextLiteral(boolean paramBoolean) throws IOException {
    StringBuilder stringBuilder = null;
    this.valuePos = -1;
    this.valueLength = 0;
    int i = 0;
    while (true) {
      StringBuilder stringBuilder1;
      String str;
      int j = this.pos;
      if (j + i < this.limit) {
        j = this.buffer[j + i];
        if (j != 9 && j != 10 && j != 12 && j != 13 && j != 32)
          if (j != 35) {
            if (j != 44)
              if (j != 47 && j != 61) {
                if (j != 123 && j != 125 && j != 58)
                  if (j != 59) {
                    switch (j) {
                      default:
                        i++;
                        continue;
                      case 92:
                        checkLenient();
                        break;
                      case 91:
                      case 93:
                        break;
                    } 
                  } else {
                  
                  }  
              } else {
              
              }  
          } else {
          
          }  
        stringBuilder1 = stringBuilder;
      } else if (i < this.buffer.length) {
        if (fillBuffer(i + 1))
          continue; 
        this.buffer[this.limit] = Character.MIN_VALUE;
        stringBuilder1 = stringBuilder;
      } else {
        stringBuilder1 = stringBuilder;
        if (stringBuilder == null)
          stringBuilder1 = new StringBuilder(); 
        stringBuilder1.append(this.buffer, this.pos, i);
        this.valueLength += i;
        this.pos += i;
        j = 0;
        i = 0;
        if (!fillBuffer(1)) {
          i = j;
        } else {
          stringBuilder = stringBuilder1;
          continue;
        } 
      } 
      if (paramBoolean && stringBuilder1 == null) {
        this.valuePos = this.pos;
        stringBuilder1 = null;
      } else if (this.skipping) {
        str = "skipped!";
      } else if (str == null) {
        str = this.stringPool.get(this.buffer, this.pos, i);
      } else {
        str.append(this.buffer, this.pos, i);
        str = str.toString();
      } 
      this.valueLength += i;
      this.pos += i;
      return str;
    } 
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(getClass().getSimpleName());
    stringBuilder.append(" near ");
    stringBuilder.append(getSnippet());
    return stringBuilder.toString();
  }
  
  private char readEscapeCharacter() throws IOException {
    if (this.pos != this.limit || fillBuffer(1)) {
      char[] arrayOfChar = this.buffer;
      int i = this.pos, j = i + 1;
      char c = arrayOfChar[i];
      if (c != 'b') {
        if (c != 'f') {
          if (c != 'n') {
            if (c != 'r') {
              if (c != 't') {
                if (c != 'u')
                  return c; 
                if (j + 4 <= this.limit || fillBuffer(4)) {
                  String str = this.stringPool.get(this.buffer, this.pos, 4);
                  this.pos += 4;
                  return (char)Integer.parseInt(str, 16);
                } 
                throw syntaxError("Unterminated escape sequence");
              } 
              return '\t';
            } 
            return '\r';
          } 
          return '\n';
        } 
        return '\f';
      } 
      return '\b';
    } 
    throw syntaxError("Unterminated escape sequence");
  }
  
  private JsonToken readLiteral() throws IOException {
    this.value = nextLiteral(true);
    if (this.valueLength != 0) {
      JsonToken jsonToken = decodeLiteral();
      if (jsonToken == JsonToken.STRING)
        checkLenient(); 
      return this.token;
    } 
    throw syntaxError("Expected literal value");
  }
  
  private JsonToken decodeLiteral() throws IOException {
    int i = this.valuePos;
    if (i == -1)
      return JsonToken.STRING; 
    if (this.valueLength == 4) {
      char[] arrayOfChar = this.buffer;
      if ('n' == arrayOfChar[i] || 'N' == arrayOfChar[i]) {
        arrayOfChar = this.buffer;
        i = this.valuePos;
        if ('u' == arrayOfChar[i + 1] || 'U' == arrayOfChar[i + 1]) {
          arrayOfChar = this.buffer;
          i = this.valuePos;
          if ('l' == arrayOfChar[i + 2] || 'L' == arrayOfChar[i + 2]) {
            arrayOfChar = this.buffer;
            i = this.valuePos;
            if ('l' == arrayOfChar[i + 3] || 'L' == arrayOfChar[i + 3]) {
              this.value = "null";
              return JsonToken.NULL;
            } 
          } 
        } 
      } 
    } 
    if (this.valueLength == 4) {
      char[] arrayOfChar = this.buffer;
      i = this.valuePos;
      if ('t' == arrayOfChar[i] || 'T' == arrayOfChar[i]) {
        arrayOfChar = this.buffer;
        i = this.valuePos;
        if ('r' == arrayOfChar[i + 1] || 'R' == arrayOfChar[i + 1]) {
          arrayOfChar = this.buffer;
          i = this.valuePos;
          if ('u' == arrayOfChar[i + 2] || 'U' == arrayOfChar[i + 2]) {
            arrayOfChar = this.buffer;
            i = this.valuePos;
            if ('e' == arrayOfChar[i + 3] || 'E' == arrayOfChar[i + 3]) {
              this.value = "true";
              return JsonToken.BOOLEAN;
            } 
          } 
        } 
      } 
    } 
    if (this.valueLength == 5) {
      char[] arrayOfChar = this.buffer;
      i = this.valuePos;
      if ('f' == arrayOfChar[i] || 'F' == arrayOfChar[i]) {
        arrayOfChar = this.buffer;
        i = this.valuePos;
        if ('a' == arrayOfChar[i + 1] || 'A' == arrayOfChar[i + 1]) {
          arrayOfChar = this.buffer;
          i = this.valuePos;
          if ('l' == arrayOfChar[i + 2] || 'L' == arrayOfChar[i + 2]) {
            arrayOfChar = this.buffer;
            i = this.valuePos;
            if ('s' == arrayOfChar[i + 3] || 'S' == arrayOfChar[i + 3]) {
              arrayOfChar = this.buffer;
              i = this.valuePos;
              if ('e' == arrayOfChar[i + 4] || 'E' == arrayOfChar[i + 4]) {
                this.value = "false";
                return JsonToken.BOOLEAN;
              } 
            } 
          } 
        } 
      } 
    } 
    this.value = this.stringPool.get(this.buffer, this.valuePos, this.valueLength);
    return decodeNumber(this.buffer, this.valuePos, this.valueLength);
  }
  
  private JsonToken decodeNumber(char[] paramArrayOfchar, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: iload_2
    //   1: istore #4
    //   3: aload_1
    //   4: iload #4
    //   6: caload
    //   7: istore #5
    //   9: iload #4
    //   11: istore #6
    //   13: iload #5
    //   15: istore #7
    //   17: iload #5
    //   19: bipush #45
    //   21: if_icmpne -> 36
    //   24: iload #4
    //   26: iconst_1
    //   27: iadd
    //   28: istore #6
    //   30: aload_1
    //   31: iload #6
    //   33: caload
    //   34: istore #7
    //   36: iload #7
    //   38: bipush #48
    //   40: if_icmpne -> 58
    //   43: iload #6
    //   45: iconst_1
    //   46: iadd
    //   47: istore #4
    //   49: aload_1
    //   50: iload #4
    //   52: caload
    //   53: istore #6
    //   55: goto -> 126
    //   58: iload #7
    //   60: bipush #49
    //   62: if_icmplt -> 339
    //   65: iload #7
    //   67: bipush #57
    //   69: if_icmpgt -> 339
    //   72: iload #6
    //   74: iconst_1
    //   75: iadd
    //   76: istore #7
    //   78: aload_1
    //   79: iload #7
    //   81: caload
    //   82: istore #5
    //   84: iload #7
    //   86: istore #4
    //   88: iload #5
    //   90: istore #6
    //   92: iload #5
    //   94: bipush #48
    //   96: if_icmplt -> 126
    //   99: iload #7
    //   101: istore #4
    //   103: iload #5
    //   105: istore #6
    //   107: iload #5
    //   109: bipush #57
    //   111: if_icmpgt -> 126
    //   114: iinc #7, 1
    //   117: aload_1
    //   118: iload #7
    //   120: caload
    //   121: istore #5
    //   123: goto -> 84
    //   126: iload #4
    //   128: istore #7
    //   130: iload #6
    //   132: istore #5
    //   134: iload #6
    //   136: bipush #46
    //   138: if_icmpne -> 195
    //   141: iload #4
    //   143: iconst_1
    //   144: iadd
    //   145: istore #6
    //   147: aload_1
    //   148: iload #6
    //   150: caload
    //   151: istore #4
    //   153: iload #6
    //   155: istore #7
    //   157: iload #4
    //   159: istore #5
    //   161: iload #4
    //   163: bipush #48
    //   165: if_icmplt -> 195
    //   168: iload #6
    //   170: istore #7
    //   172: iload #4
    //   174: istore #5
    //   176: iload #4
    //   178: bipush #57
    //   180: if_icmpgt -> 195
    //   183: iinc #6, 1
    //   186: aload_1
    //   187: iload #6
    //   189: caload
    //   190: istore #4
    //   192: goto -> 153
    //   195: iload #5
    //   197: bipush #101
    //   199: if_icmpeq -> 213
    //   202: iload #7
    //   204: istore #4
    //   206: iload #5
    //   208: bipush #69
    //   210: if_icmpne -> 319
    //   213: iload #7
    //   215: iconst_1
    //   216: iadd
    //   217: istore #6
    //   219: aload_1
    //   220: iload #6
    //   222: caload
    //   223: istore #5
    //   225: iload #5
    //   227: bipush #43
    //   229: if_icmpeq -> 247
    //   232: iload #6
    //   234: istore #4
    //   236: iload #5
    //   238: istore #7
    //   240: iload #5
    //   242: bipush #45
    //   244: if_icmpne -> 259
    //   247: iload #6
    //   249: iconst_1
    //   250: iadd
    //   251: istore #4
    //   253: aload_1
    //   254: iload #4
    //   256: caload
    //   257: istore #7
    //   259: iload #7
    //   261: bipush #48
    //   263: if_icmplt -> 335
    //   266: iload #7
    //   268: bipush #57
    //   270: if_icmpgt -> 335
    //   273: iload #4
    //   275: iconst_1
    //   276: iadd
    //   277: istore #7
    //   279: aload_1
    //   280: iload #7
    //   282: caload
    //   283: istore #6
    //   285: iload #7
    //   287: istore #4
    //   289: iload #6
    //   291: bipush #48
    //   293: if_icmplt -> 319
    //   296: iload #7
    //   298: istore #4
    //   300: iload #6
    //   302: bipush #57
    //   304: if_icmpgt -> 319
    //   307: iinc #7, 1
    //   310: aload_1
    //   311: iload #7
    //   313: caload
    //   314: istore #6
    //   316: goto -> 285
    //   319: iload #4
    //   321: iload_2
    //   322: iload_3
    //   323: iadd
    //   324: if_icmpne -> 331
    //   327: getstatic android/util/JsonToken.NUMBER : Landroid/util/JsonToken;
    //   330: areturn
    //   331: getstatic android/util/JsonToken.STRING : Landroid/util/JsonToken;
    //   334: areturn
    //   335: getstatic android/util/JsonToken.STRING : Landroid/util/JsonToken;
    //   338: areturn
    //   339: getstatic android/util/JsonToken.STRING : Landroid/util/JsonToken;
    //   342: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1109	-> 0
    //   #1110	-> 3
    //   #1112	-> 9
    //   #1113	-> 24
    //   #1116	-> 36
    //   #1117	-> 43
    //   #1118	-> 58
    //   #1119	-> 72
    //   #1120	-> 84
    //   #1121	-> 114
    //   #1127	-> 126
    //   #1128	-> 141
    //   #1129	-> 153
    //   #1130	-> 183
    //   #1134	-> 195
    //   #1135	-> 213
    //   #1136	-> 225
    //   #1137	-> 247
    //   #1139	-> 259
    //   #1140	-> 273
    //   #1141	-> 285
    //   #1142	-> 307
    //   #1149	-> 319
    //   #1150	-> 327
    //   #1152	-> 331
    //   #1145	-> 335
    //   #1124	-> 339
  }
  
  private IOException syntaxError(String paramString) throws IOException {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(" at line ");
    stringBuilder.append(getLineNumber());
    stringBuilder.append(" column ");
    stringBuilder.append(getColumnNumber());
    throw new MalformedJsonException(stringBuilder.toString());
  }
  
  private CharSequence getSnippet() {
    StringBuilder stringBuilder = new StringBuilder();
    int i = Math.min(this.pos, 20);
    stringBuilder.append(this.buffer, this.pos - i, i);
    i = Math.min(this.limit - this.pos, 20);
    stringBuilder.append(this.buffer, this.pos, i);
    return stringBuilder;
  }
}
