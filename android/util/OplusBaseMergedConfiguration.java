package android.util;

import android.os.Parcel;
import android.os.Parcelable;
import com.oplus.darkmode.OplusDarkModeData;

public abstract class OplusBaseMergedConfiguration {
  public float mDialogBgMaxL = -1.0F;
  
  public float mBackgroundMaxL = -1.0F;
  
  public float mForegroundMinL = -1.0F;
  
  public OplusDarkModeData mOplusDarkModeData;
  
  public int mOplusForceDarkValue;
  
  public void writeParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.mOplusDarkModeData, 0);
  }
  
  public void readParcel(Parcel paramParcel) {
    this.mOplusDarkModeData = (OplusDarkModeData)paramParcel.readParcelable(OplusBaseMergedConfiguration.class.getClassLoader());
  }
}
