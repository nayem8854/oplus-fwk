package android.util;

import java.util.LinkedHashMap;
import java.util.Map;

public class LruCache<K, V> {
  private int createCount;
  
  private int evictionCount;
  
  private int hitCount;
  
  private final LinkedHashMap<K, V> map;
  
  private int maxSize;
  
  private int missCount;
  
  private int putCount;
  
  private int size;
  
  public LruCache(int paramInt) {
    if (paramInt > 0) {
      this.maxSize = paramInt;
      this.map = new LinkedHashMap<>(0, 0.75F, true);
      return;
    } 
    throw new IllegalArgumentException("maxSize <= 0");
  }
  
  public void resize(int paramInt) {
    // Byte code:
    //   0: iload_1
    //   1: ifle -> 24
    //   4: aload_0
    //   5: monitorenter
    //   6: aload_0
    //   7: iload_1
    //   8: putfield maxSize : I
    //   11: aload_0
    //   12: monitorexit
    //   13: aload_0
    //   14: iload_1
    //   15: invokevirtual trimToSize : (I)V
    //   18: return
    //   19: astore_2
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_2
    //   23: athrow
    //   24: new java/lang/IllegalArgumentException
    //   27: dup
    //   28: ldc 'maxSize <= 0'
    //   30: invokespecial <init> : (Ljava/lang/String;)V
    //   33: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #97	-> 0
    //   #101	-> 4
    //   #102	-> 6
    //   #103	-> 11
    //   #104	-> 13
    //   #105	-> 18
    //   #103	-> 19
    //   #98	-> 24
    // Exception table:
    //   from	to	target	type
    //   6	11	19	finally
    //   11	13	19	finally
    //   20	22	19	finally
  }
  
  public final V get(K paramK) {
    // Byte code:
    //   0: aload_1
    //   1: ifnull -> 147
    //   4: aload_0
    //   5: monitorenter
    //   6: aload_0
    //   7: getfield map : Ljava/util/LinkedHashMap;
    //   10: aload_1
    //   11: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   14: astore_2
    //   15: aload_2
    //   16: ifnull -> 33
    //   19: aload_0
    //   20: aload_0
    //   21: getfield hitCount : I
    //   24: iconst_1
    //   25: iadd
    //   26: putfield hitCount : I
    //   29: aload_0
    //   30: monitorexit
    //   31: aload_2
    //   32: areturn
    //   33: aload_0
    //   34: aload_0
    //   35: getfield missCount : I
    //   38: iconst_1
    //   39: iadd
    //   40: putfield missCount : I
    //   43: aload_0
    //   44: monitorexit
    //   45: aload_0
    //   46: aload_1
    //   47: invokevirtual create : (Ljava/lang/Object;)Ljava/lang/Object;
    //   50: astore_2
    //   51: aload_2
    //   52: ifnonnull -> 57
    //   55: aconst_null
    //   56: areturn
    //   57: aload_0
    //   58: monitorenter
    //   59: aload_0
    //   60: aload_0
    //   61: getfield createCount : I
    //   64: iconst_1
    //   65: iadd
    //   66: putfield createCount : I
    //   69: aload_0
    //   70: getfield map : Ljava/util/LinkedHashMap;
    //   73: aload_1
    //   74: aload_2
    //   75: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   78: astore_3
    //   79: aload_3
    //   80: ifnull -> 96
    //   83: aload_0
    //   84: getfield map : Ljava/util/LinkedHashMap;
    //   87: aload_1
    //   88: aload_3
    //   89: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   92: pop
    //   93: goto -> 111
    //   96: aload_0
    //   97: aload_0
    //   98: getfield size : I
    //   101: aload_0
    //   102: aload_1
    //   103: aload_2
    //   104: invokespecial safeSizeOf : (Ljava/lang/Object;Ljava/lang/Object;)I
    //   107: iadd
    //   108: putfield size : I
    //   111: aload_0
    //   112: monitorexit
    //   113: aload_3
    //   114: ifnull -> 127
    //   117: aload_0
    //   118: iconst_0
    //   119: aload_1
    //   120: aload_2
    //   121: aload_3
    //   122: invokevirtual entryRemoved : (ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    //   125: aload_3
    //   126: areturn
    //   127: aload_0
    //   128: aload_0
    //   129: getfield maxSize : I
    //   132: invokevirtual trimToSize : (I)V
    //   135: aload_2
    //   136: areturn
    //   137: astore_1
    //   138: aload_0
    //   139: monitorexit
    //   140: aload_1
    //   141: athrow
    //   142: astore_1
    //   143: aload_0
    //   144: monitorexit
    //   145: aload_1
    //   146: athrow
    //   147: new java/lang/NullPointerException
    //   150: dup
    //   151: ldc 'key == null'
    //   153: invokespecial <init> : (Ljava/lang/String;)V
    //   156: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #114	-> 0
    //   #119	-> 4
    //   #120	-> 6
    //   #121	-> 15
    //   #122	-> 19
    //   #123	-> 29
    //   #125	-> 33
    //   #126	-> 43
    //   #135	-> 45
    //   #136	-> 51
    //   #137	-> 55
    //   #140	-> 57
    //   #141	-> 59
    //   #142	-> 69
    //   #144	-> 79
    //   #146	-> 83
    //   #148	-> 96
    //   #150	-> 111
    //   #152	-> 113
    //   #153	-> 117
    //   #154	-> 125
    //   #156	-> 127
    //   #157	-> 135
    //   #150	-> 137
    //   #126	-> 142
    //   #115	-> 147
    // Exception table:
    //   from	to	target	type
    //   6	15	142	finally
    //   19	29	142	finally
    //   29	31	142	finally
    //   33	43	142	finally
    //   43	45	142	finally
    //   59	69	137	finally
    //   69	79	137	finally
    //   83	93	137	finally
    //   96	111	137	finally
    //   111	113	137	finally
    //   138	140	137	finally
    //   143	145	142	finally
  }
  
  public final V put(K paramK, V paramV) {
    // Byte code:
    //   0: aload_1
    //   1: ifnull -> 93
    //   4: aload_2
    //   5: ifnull -> 93
    //   8: aload_0
    //   9: monitorenter
    //   10: aload_0
    //   11: aload_0
    //   12: getfield putCount : I
    //   15: iconst_1
    //   16: iadd
    //   17: putfield putCount : I
    //   20: aload_0
    //   21: aload_0
    //   22: getfield size : I
    //   25: aload_0
    //   26: aload_1
    //   27: aload_2
    //   28: invokespecial safeSizeOf : (Ljava/lang/Object;Ljava/lang/Object;)I
    //   31: iadd
    //   32: putfield size : I
    //   35: aload_0
    //   36: getfield map : Ljava/util/LinkedHashMap;
    //   39: aload_1
    //   40: aload_2
    //   41: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   44: astore_3
    //   45: aload_3
    //   46: ifnull -> 64
    //   49: aload_0
    //   50: aload_0
    //   51: getfield size : I
    //   54: aload_0
    //   55: aload_1
    //   56: aload_3
    //   57: invokespecial safeSizeOf : (Ljava/lang/Object;Ljava/lang/Object;)I
    //   60: isub
    //   61: putfield size : I
    //   64: aload_0
    //   65: monitorexit
    //   66: aload_3
    //   67: ifnull -> 78
    //   70: aload_0
    //   71: iconst_0
    //   72: aload_1
    //   73: aload_3
    //   74: aload_2
    //   75: invokevirtual entryRemoved : (ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    //   78: aload_0
    //   79: aload_0
    //   80: getfield maxSize : I
    //   83: invokevirtual trimToSize : (I)V
    //   86: aload_3
    //   87: areturn
    //   88: astore_1
    //   89: aload_0
    //   90: monitorexit
    //   91: aload_1
    //   92: athrow
    //   93: new java/lang/NullPointerException
    //   96: dup
    //   97: ldc 'key == null || value == null'
    //   99: invokespecial <init> : (Ljava/lang/String;)V
    //   102: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #168	-> 0
    //   #173	-> 8
    //   #174	-> 10
    //   #175	-> 20
    //   #176	-> 35
    //   #177	-> 45
    //   #178	-> 49
    //   #180	-> 64
    //   #182	-> 66
    //   #183	-> 70
    //   #186	-> 78
    //   #187	-> 86
    //   #180	-> 88
    //   #169	-> 93
    // Exception table:
    //   from	to	target	type
    //   10	20	88	finally
    //   20	35	88	finally
    //   35	45	88	finally
    //   49	64	88	finally
    //   64	66	88	finally
    //   89	91	88	finally
  }
  
  public void trimToSize(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield size : I
    //   6: iflt -> 115
    //   9: aload_0
    //   10: getfield map : Ljava/util/LinkedHashMap;
    //   13: invokevirtual isEmpty : ()Z
    //   16: ifeq -> 26
    //   19: aload_0
    //   20: getfield size : I
    //   23: ifne -> 115
    //   26: aload_0
    //   27: getfield size : I
    //   30: iload_1
    //   31: if_icmpgt -> 39
    //   34: aload_0
    //   35: monitorexit
    //   36: goto -> 53
    //   39: aload_0
    //   40: getfield map : Ljava/util/LinkedHashMap;
    //   43: invokevirtual eldest : ()Ljava/util/Map$Entry;
    //   46: astore_2
    //   47: aload_2
    //   48: ifnonnull -> 54
    //   51: aload_0
    //   52: monitorexit
    //   53: return
    //   54: aload_2
    //   55: invokeinterface getKey : ()Ljava/lang/Object;
    //   60: astore_3
    //   61: aload_2
    //   62: invokeinterface getValue : ()Ljava/lang/Object;
    //   67: astore_2
    //   68: aload_0
    //   69: getfield map : Ljava/util/LinkedHashMap;
    //   72: aload_3
    //   73: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   76: pop
    //   77: aload_0
    //   78: aload_0
    //   79: getfield size : I
    //   82: aload_0
    //   83: aload_3
    //   84: aload_2
    //   85: invokespecial safeSizeOf : (Ljava/lang/Object;Ljava/lang/Object;)I
    //   88: isub
    //   89: putfield size : I
    //   92: aload_0
    //   93: aload_0
    //   94: getfield evictionCount : I
    //   97: iconst_1
    //   98: iadd
    //   99: putfield evictionCount : I
    //   102: aload_0
    //   103: monitorexit
    //   104: aload_0
    //   105: iconst_1
    //   106: aload_3
    //   107: aload_2
    //   108: aconst_null
    //   109: invokevirtual entryRemoved : (ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    //   112: goto -> 0
    //   115: new java/lang/IllegalStateException
    //   118: astore_2
    //   119: new java/lang/StringBuilder
    //   122: astore_3
    //   123: aload_3
    //   124: invokespecial <init> : ()V
    //   127: aload_3
    //   128: aload_0
    //   129: invokevirtual getClass : ()Ljava/lang/Class;
    //   132: invokevirtual getName : ()Ljava/lang/String;
    //   135: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   138: pop
    //   139: aload_3
    //   140: ldc '.sizeOf() is reporting inconsistent results!'
    //   142: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   145: pop
    //   146: aload_2
    //   147: aload_3
    //   148: invokevirtual toString : ()Ljava/lang/String;
    //   151: invokespecial <init> : (Ljava/lang/String;)V
    //   154: aload_2
    //   155: athrow
    //   156: astore_3
    //   157: aload_0
    //   158: monitorexit
    //   159: aload_3
    //   160: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #201	-> 0
    //   #202	-> 2
    //   #207	-> 26
    //   #208	-> 34
    //   #211	-> 39
    //   #212	-> 47
    //   #213	-> 51
    //   #225	-> 53
    //   #216	-> 54
    //   #217	-> 61
    //   #218	-> 68
    //   #219	-> 77
    //   #220	-> 92
    //   #221	-> 102
    //   #223	-> 104
    //   #224	-> 112
    //   #203	-> 115
    //   #221	-> 156
    // Exception table:
    //   from	to	target	type
    //   2	26	156	finally
    //   26	34	156	finally
    //   34	36	156	finally
    //   39	47	156	finally
    //   51	53	156	finally
    //   54	61	156	finally
    //   61	68	156	finally
    //   68	77	156	finally
    //   77	92	156	finally
    //   92	102	156	finally
    //   102	104	156	finally
    //   115	156	156	finally
    //   157	159	156	finally
  }
  
  public final V remove(K paramK) {
    // Byte code:
    //   0: aload_1
    //   1: ifnull -> 55
    //   4: aload_0
    //   5: monitorenter
    //   6: aload_0
    //   7: getfield map : Ljava/util/LinkedHashMap;
    //   10: aload_1
    //   11: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   14: astore_2
    //   15: aload_2
    //   16: ifnull -> 34
    //   19: aload_0
    //   20: aload_0
    //   21: getfield size : I
    //   24: aload_0
    //   25: aload_1
    //   26: aload_2
    //   27: invokespecial safeSizeOf : (Ljava/lang/Object;Ljava/lang/Object;)I
    //   30: isub
    //   31: putfield size : I
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_2
    //   37: ifnull -> 48
    //   40: aload_0
    //   41: iconst_0
    //   42: aload_1
    //   43: aload_2
    //   44: aconst_null
    //   45: invokevirtual entryRemoved : (ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    //   48: aload_2
    //   49: areturn
    //   50: astore_1
    //   51: aload_0
    //   52: monitorexit
    //   53: aload_1
    //   54: athrow
    //   55: new java/lang/NullPointerException
    //   58: dup
    //   59: ldc 'key == null'
    //   61: invokespecial <init> : (Ljava/lang/String;)V
    //   64: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #233	-> 0
    //   #238	-> 4
    //   #239	-> 6
    //   #240	-> 15
    //   #241	-> 19
    //   #243	-> 34
    //   #245	-> 36
    //   #246	-> 40
    //   #249	-> 48
    //   #243	-> 50
    //   #234	-> 55
    // Exception table:
    //   from	to	target	type
    //   6	15	50	finally
    //   19	34	50	finally
    //   34	36	50	finally
    //   51	53	50	finally
  }
  
  protected void entryRemoved(boolean paramBoolean, K paramK, V paramV1, V paramV2) {}
  
  protected V create(K paramK) {
    return null;
  }
  
  private int safeSizeOf(K paramK, V paramV) {
    int i = sizeOf(paramK, paramV);
    if (i >= 0)
      return i; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Negative size: ");
    stringBuilder.append(paramK);
    stringBuilder.append("=");
    stringBuilder.append(paramV);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  protected int sizeOf(K paramK, V paramV) {
    return 1;
  }
  
  public final void evictAll() {
    trimToSize(-1);
  }
  
  public final int size() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield size : I
    //   6: istore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: iload_1
    //   10: ireturn
    //   11: astore_2
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_2
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #320	-> 2
    //   #320	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	7	11	finally
  }
  
  public final int maxSize() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield maxSize : I
    //   6: istore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: iload_1
    //   10: ireturn
    //   11: astore_2
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_2
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #329	-> 2
    //   #329	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	7	11	finally
  }
  
  public final int hitCount() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield hitCount : I
    //   6: istore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: iload_1
    //   10: ireturn
    //   11: astore_2
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_2
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #337	-> 2
    //   #337	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	7	11	finally
  }
  
  public final int missCount() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield missCount : I
    //   6: istore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: iload_1
    //   10: ireturn
    //   11: astore_2
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_2
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #345	-> 2
    //   #345	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	7	11	finally
  }
  
  public final int createCount() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield createCount : I
    //   6: istore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: iload_1
    //   10: ireturn
    //   11: astore_2
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_2
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #352	-> 2
    //   #352	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	7	11	finally
  }
  
  public final int putCount() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield putCount : I
    //   6: istore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: iload_1
    //   10: ireturn
    //   11: astore_2
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_2
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #359	-> 2
    //   #359	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	7	11	finally
  }
  
  public final int evictionCount() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield evictionCount : I
    //   6: istore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: iload_1
    //   10: ireturn
    //   11: astore_2
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_2
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #366	-> 2
    //   #366	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	7	11	finally
  }
  
  public final Map<K, V> snapshot() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new java/util/LinkedHashMap
    //   5: dup
    //   6: aload_0
    //   7: getfield map : Ljava/util/LinkedHashMap;
    //   10: invokespecial <init> : (Ljava/util/Map;)V
    //   13: astore_1
    //   14: aload_0
    //   15: monitorexit
    //   16: aload_1
    //   17: areturn
    //   18: astore_1
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_1
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #374	-> 2
    //   #374	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	14	18	finally
  }
  
  public final String toString() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield hitCount : I
    //   6: aload_0
    //   7: getfield missCount : I
    //   10: iadd
    //   11: istore_1
    //   12: iload_1
    //   13: ifeq -> 29
    //   16: aload_0
    //   17: getfield hitCount : I
    //   20: bipush #100
    //   22: imul
    //   23: iload_1
    //   24: idiv
    //   25: istore_1
    //   26: goto -> 31
    //   29: iconst_0
    //   30: istore_1
    //   31: aload_0
    //   32: getfield maxSize : I
    //   35: istore_2
    //   36: aload_0
    //   37: getfield hitCount : I
    //   40: istore_3
    //   41: aload_0
    //   42: getfield missCount : I
    //   45: istore #4
    //   47: ldc 'LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]'
    //   49: iconst_4
    //   50: anewarray java/lang/Object
    //   53: dup
    //   54: iconst_0
    //   55: iload_2
    //   56: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   59: aastore
    //   60: dup
    //   61: iconst_1
    //   62: iload_3
    //   63: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   66: aastore
    //   67: dup
    //   68: iconst_2
    //   69: iload #4
    //   71: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   74: aastore
    //   75: dup
    //   76: iconst_3
    //   77: iload_1
    //   78: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   81: aastore
    //   82: invokestatic format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   85: astore #5
    //   87: aload_0
    //   88: monitorexit
    //   89: aload #5
    //   91: areturn
    //   92: astore #5
    //   94: aload_0
    //   95: monitorexit
    //   96: aload #5
    //   98: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #378	-> 2
    //   #379	-> 12
    //   #380	-> 31
    //   #381	-> 36
    //   #380	-> 47
    //   #377	-> 92
    // Exception table:
    //   from	to	target	type
    //   2	12	92	finally
    //   16	26	92	finally
    //   31	36	92	finally
    //   36	47	92	finally
    //   47	87	92	finally
  }
}
