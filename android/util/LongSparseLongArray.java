package android.util;

import android.os.Parcel;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.GrowingArrayUtils;
import com.android.internal.util.Preconditions;
import libcore.util.EmptyArray;

public class LongSparseLongArray implements Cloneable {
  private long[] mKeys;
  
  private int mSize;
  
  private long[] mValues;
  
  public LongSparseLongArray() {
    this(10);
  }
  
  public LongSparseLongArray(int paramInt) {
    if (paramInt == 0) {
      this.mKeys = EmptyArray.LONG;
      this.mValues = EmptyArray.LONG;
    } else {
      long[] arrayOfLong = ArrayUtils.newUnpaddedLongArray(paramInt);
      this.mValues = new long[arrayOfLong.length];
    } 
    this.mSize = 0;
  }
  
  public LongSparseLongArray clone() {
    LongSparseLongArray longSparseLongArray = null;
    try {
      LongSparseLongArray longSparseLongArray1 = (LongSparseLongArray)super.clone();
      longSparseLongArray = longSparseLongArray1;
      longSparseLongArray1.mKeys = (long[])this.mKeys.clone();
      longSparseLongArray = longSparseLongArray1;
      longSparseLongArray1.mValues = (long[])this.mValues.clone();
      longSparseLongArray = longSparseLongArray1;
    } catch (CloneNotSupportedException cloneNotSupportedException) {}
    return longSparseLongArray;
  }
  
  public long get(long paramLong) {
    return get(paramLong, 0L);
  }
  
  public long get(long paramLong1, long paramLong2) {
    int i = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramLong1);
    if (i < 0)
      return paramLong2; 
    return this.mValues[i];
  }
  
  public void delete(long paramLong) {
    int i = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramLong);
    if (i >= 0)
      removeAt(i); 
  }
  
  public void removeAt(int paramInt) {
    long[] arrayOfLong = this.mKeys;
    System.arraycopy(arrayOfLong, paramInt + 1, arrayOfLong, paramInt, this.mSize - paramInt + 1);
    arrayOfLong = this.mValues;
    System.arraycopy(arrayOfLong, paramInt + 1, arrayOfLong, paramInt, this.mSize - paramInt + 1);
    this.mSize--;
  }
  
  public void put(long paramLong1, long paramLong2) {
    int i = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramLong1);
    if (i >= 0) {
      this.mValues[i] = paramLong2;
    } else {
      i ^= 0xFFFFFFFF;
      this.mKeys = GrowingArrayUtils.insert(this.mKeys, this.mSize, i, paramLong1);
      this.mValues = GrowingArrayUtils.insert(this.mValues, this.mSize, i, paramLong2);
      this.mSize++;
    } 
  }
  
  public int size() {
    return this.mSize;
  }
  
  public long keyAt(int paramInt) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds)
      return this.mKeys[paramInt]; 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public long valueAt(int paramInt) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds)
      return this.mValues[paramInt]; 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public int indexOfKey(long paramLong) {
    return ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramLong);
  }
  
  public int indexOfValue(long paramLong) {
    for (byte b = 0; b < this.mSize; b++) {
      if (this.mValues[b] == paramLong)
        return b; 
    } 
    return -1;
  }
  
  public void clear() {
    this.mSize = 0;
  }
  
  public void append(long paramLong1, long paramLong2) {
    int i = this.mSize;
    if (i != 0 && paramLong1 <= this.mKeys[i - 1]) {
      put(paramLong1, paramLong2);
      return;
    } 
    this.mKeys = GrowingArrayUtils.append(this.mKeys, this.mSize, paramLong1);
    this.mValues = GrowingArrayUtils.append(this.mValues, this.mSize, paramLong2);
    this.mSize++;
  }
  
  public String toString() {
    if (size() <= 0)
      return "{}"; 
    StringBuilder stringBuilder = new StringBuilder(this.mSize * 28);
    stringBuilder.append('{');
    for (byte b = 0; b < this.mSize; b++) {
      if (b > 0)
        stringBuilder.append(", "); 
      long l = keyAt(b);
      stringBuilder.append(l);
      stringBuilder.append('=');
      l = valueAt(b);
      stringBuilder.append(l);
    } 
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  class Parcelling implements com.android.internal.util.Parcelling<LongSparseLongArray> {
    public void parcel(LongSparseLongArray param1LongSparseLongArray, Parcel param1Parcel, int param1Int) {
      if (param1LongSparseLongArray == null) {
        param1Parcel.writeInt(-1);
        return;
      } 
      param1Parcel.writeInt(param1LongSparseLongArray.mSize);
      param1Parcel.writeLongArray(param1LongSparseLongArray.mKeys);
      param1Parcel.writeLongArray(param1LongSparseLongArray.mValues);
    }
    
    public LongSparseLongArray unparcel(Parcel param1Parcel) {
      boolean bool;
      int i = param1Parcel.readInt();
      if (i == -1)
        return null; 
      LongSparseLongArray longSparseLongArray = new LongSparseLongArray(0);
      LongSparseLongArray.access$002(longSparseLongArray, i);
      LongSparseLongArray.access$102(longSparseLongArray, param1Parcel.createLongArray());
      LongSparseLongArray.access$202(longSparseLongArray, param1Parcel.createLongArray());
      if (longSparseLongArray.mKeys.length >= i) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkArgument(bool);
      if (longSparseLongArray.mValues.length >= i) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkArgument(bool);
      if (i > 0) {
        long l = longSparseLongArray.mKeys[0];
        for (byte b = 1; b < i; b++) {
          if (l < longSparseLongArray.mKeys[b]) {
            bool = true;
          } else {
            bool = false;
          } 
          Preconditions.checkArgument(bool);
        } 
      } 
      return longSparseLongArray;
    }
  }
}
