package android.util;

import java.io.BufferedReader;
import java.io.IOException;

@Deprecated
public class EventLogTags {
  public static class Description {
    public final String mName;
    
    public final int mTag;
    
    Description(int param1Int, String param1String) {
      this.mTag = param1Int;
      this.mName = param1String;
    }
  }
  
  public EventLogTags() throws IOException {}
  
  public EventLogTags(BufferedReader paramBufferedReader) throws IOException {}
  
  public Description get(String paramString) {
    return null;
  }
  
  public Description get(int paramInt) {
    return null;
  }
}
