package android.util;

import android.app.ActivityManagerNative;
import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.FileObserver;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.SystemProperties;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;

public final class OplusSafeDbReader {
  private static boolean sDebug = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static volatile OplusSafeDbReader sInstance = null;
  
  private ArrayList<String> mUserAllowPackageName = new ArrayList<>();
  
  private ArrayList<String> mUserClosePackageName = new ArrayList<>();
  
  private ArrayList<String> mUserAllowToastPackageName = new ArrayList<>();
  
  private ArrayList<String> mUserCloseToastPackageName = new ArrayList<>();
  
  private Uri mUri = Uri.parse("content://com.coloros.provider.PermissionProvider/pp_float_window");
  
  private Object mLock = new Object();
  
  private boolean mHasInit = false;
  
  private static final String ALLOWED = "allowed";
  
  private static final String PKG_NAME = "pkg_name";
  
  private static final int SLEEP_TIME = 100;
  
  private static final String TAG = "OplusSafeDbReader";
  
  private static final String TOAST_FILTER_FILE_PATH = "//data//oppo//coloros//toast//toast.xml";
  
  private static final int USR_OPEN_BIT = 4;
  
  private static final int WHITE_LIST_BIT = 1;
  
  private Context mContext;
  
  private DataFileListener mDataFileListener;
  
  private Handler mHandler;
  
  private HandlerThread mHandlerThread;
  
  private SafePPWindowObserver mSafePPWindowObserver;
  
  private Thread mThread;
  
  public static OplusSafeDbReader getInstance(Context paramContext) {
    // Byte code:
    //   0: getstatic android/util/OplusSafeDbReader.sInstance : Landroid/util/OplusSafeDbReader;
    //   3: ifnonnull -> 40
    //   6: ldc android/util/OplusSafeDbReader
    //   8: monitorenter
    //   9: getstatic android/util/OplusSafeDbReader.sInstance : Landroid/util/OplusSafeDbReader;
    //   12: ifnonnull -> 28
    //   15: new android/util/OplusSafeDbReader
    //   18: astore_1
    //   19: aload_1
    //   20: aload_0
    //   21: invokespecial <init> : (Landroid/content/Context;)V
    //   24: aload_1
    //   25: putstatic android/util/OplusSafeDbReader.sInstance : Landroid/util/OplusSafeDbReader;
    //   28: ldc android/util/OplusSafeDbReader
    //   30: monitorexit
    //   31: goto -> 40
    //   34: astore_0
    //   35: ldc android/util/OplusSafeDbReader
    //   37: monitorexit
    //   38: aload_0
    //   39: athrow
    //   40: getstatic android/util/OplusSafeDbReader.sInstance : Landroid/util/OplusSafeDbReader;
    //   43: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #67	-> 0
    //   #68	-> 6
    //   #69	-> 9
    //   #70	-> 15
    //   #72	-> 28
    //   #74	-> 40
    // Exception table:
    //   from	to	target	type
    //   9	15	34	finally
    //   15	28	34	finally
    //   28	31	34	finally
    //   35	38	34	finally
  }
  
  private OplusSafeDbReader(Context paramContext) {
    this.mContext = paramContext;
  }
  
  private void init() {
    HandlerThread handlerThread = new HandlerThread("OplusSafeDbReader");
    handlerThread.start();
    Handler handler = new Handler(this.mHandlerThread.getLooper());
    SafePPWindowObserver safePPWindowObserver = new SafePPWindowObserver(handler);
    safePPWindowObserver.observe();
    DataFileListener dataFileListener = new DataFileListener("//data//oppo//coloros//toast//toast.xml");
    dataFileListener.startWatching();
    this.mThread = new Thread(new GetDataFromProviderRunnable(), "get_data");
    if (sInstance != null)
      sInstance.getToastAppMapPri(); 
  }
  
  private boolean getData() {
    Cursor cursor1 = null, cursor2 = null;
    boolean bool1 = true, bool2 = true;
    try {
      Cursor cursor = this.mContext.getContentResolver().query(this.mUri, new String[] { "pkg_name", "allowed" }, null, null, null);
      cursor2 = cursor;
      cursor1 = cursor;
      int i = cursor.getColumnIndex("allowed");
      cursor2 = cursor;
      cursor1 = cursor;
      int j = cursor.getColumnIndex("pkg_name");
      cursor2 = cursor;
      cursor1 = cursor;
      null = new ArrayList();
      cursor2 = cursor;
      cursor1 = cursor;
      this();
      cursor2 = cursor;
      cursor1 = cursor;
      ArrayList<String> arrayList = new ArrayList();
      cursor2 = cursor;
      cursor1 = cursor;
      this();
      while (cursor != null) {
        cursor2 = cursor;
        cursor1 = cursor;
        if (cursor.moveToNext()) {
          cursor2 = cursor;
          cursor1 = cursor;
          int k = cursor.getInt(i);
          if ((k & 0x4) != 0 || (k & 0x1) != 0) {
            cursor2 = cursor;
            cursor1 = cursor;
            null.add(cursor.getString(j));
            cursor2 = cursor;
            cursor1 = cursor;
            if (sDebug) {
              cursor2 = cursor;
              cursor1 = cursor;
              StringBuilder stringBuilder = new StringBuilder();
              cursor2 = cursor;
              cursor1 = cursor;
              this();
              cursor2 = cursor;
              cursor1 = cursor;
              stringBuilder.append("allow: ");
              cursor2 = cursor;
              cursor1 = cursor;
              stringBuilder.append(cursor.getString(j));
              cursor2 = cursor;
              cursor1 = cursor;
              Log.d("OplusSafeDbReader", stringBuilder.toString());
            } 
            continue;
          } 
          cursor2 = cursor;
          cursor1 = cursor;
          arrayList.add(cursor.getString(j));
          cursor2 = cursor;
          cursor1 = cursor;
          if (sDebug) {
            cursor2 = cursor;
            cursor1 = cursor;
            StringBuilder stringBuilder = new StringBuilder();
            cursor2 = cursor;
            cursor1 = cursor;
            this();
            cursor2 = cursor;
            cursor1 = cursor;
            stringBuilder.append("not allow: ");
            cursor2 = cursor;
            cursor1 = cursor;
            stringBuilder.append(cursor.getString(j));
            cursor2 = cursor;
            cursor1 = cursor;
            Log.d("OplusSafeDbReader", stringBuilder.toString());
          } 
        } 
      } 
      cursor2 = cursor;
      cursor1 = cursor;
      Object object = this.mLock;
      cursor2 = cursor;
      synchronized (cursor) {
        this.mUserAllowPackageName.clear();
        this.mUserClosePackageName.clear();
        this.mUserAllowPackageName = null;
        this.mUserClosePackageName = arrayList;
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        cursor2 = cursor;
        cursor1 = cursor;
        this.mHasInit = true;
        if (cursor != null) {
          cursor1 = cursor;
          bool1 = bool2;
        } else {
          return bool1;
        } 
      } 
    } catch (Exception exception) {
      cursor2 = cursor1;
      StringBuilder stringBuilder = new StringBuilder();
      cursor2 = cursor1;
      this();
      cursor2 = cursor1;
      stringBuilder.append("We can not get floatwindow app user config data from provider,because of ");
      cursor2 = cursor1;
      stringBuilder.append(exception);
      cursor2 = cursor1;
      Log.w("OplusSafeDbReader", stringBuilder.toString());
      bool1 = false;
      bool2 = false;
      if (cursor1 != null) {
        bool1 = bool2;
      } else {
        return bool1;
      } 
    } finally {}
    cursor1.close();
    return bool1;
  }
  
  public boolean isUserOpen(String paramString) {
    boolean bool = false;
    synchronized (this.mLock) {
      boolean bool1 = this.mHasInit;
      boolean bool2 = true;
      if (!bool1) {
        Log.i("OplusSafeDbReader", "init data error, don't intercept");
        return true;
      } 
      bool1 = bool;
      if (this.mUserAllowPackageName != null) {
        bool1 = bool;
        if (this.mUserAllowToastPackageName != null) {
          bool1 = bool2;
          if (!this.mUserAllowPackageName.contains(paramString))
            if (this.mUserAllowToastPackageName.contains(paramString)) {
              bool1 = bool2;
            } else {
              bool1 = false;
            }  
        } 
      } 
      if (sDebug) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("isUserOpen return: ");
        stringBuilder.append(bool1);
        Log.i("OplusSafeDbReader", stringBuilder.toString());
      } 
      return bool1;
    } 
  }
  
  public boolean isUserClose(String paramString) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_2
    //   2: aload_0
    //   3: getfield mLock : Ljava/lang/Object;
    //   6: astore_3
    //   7: aload_3
    //   8: monitorenter
    //   9: iload_2
    //   10: istore #4
    //   12: aload_0
    //   13: getfield mUserClosePackageName : Ljava/util/ArrayList;
    //   16: ifnull -> 63
    //   19: iload_2
    //   20: istore #4
    //   22: aload_0
    //   23: getfield mUserClosePackageName : Ljava/util/ArrayList;
    //   26: ifnull -> 63
    //   29: aload_0
    //   30: getfield mUserClosePackageName : Ljava/util/ArrayList;
    //   33: aload_1
    //   34: invokevirtual contains : (Ljava/lang/Object;)Z
    //   37: ifne -> 60
    //   40: aload_0
    //   41: getfield mUserCloseToastPackageName : Ljava/util/ArrayList;
    //   44: aload_1
    //   45: invokevirtual contains : (Ljava/lang/Object;)Z
    //   48: ifeq -> 54
    //   51: goto -> 60
    //   54: iconst_0
    //   55: istore #4
    //   57: goto -> 63
    //   60: iconst_1
    //   61: istore #4
    //   63: aload_3
    //   64: monitorexit
    //   65: iload #4
    //   67: ireturn
    //   68: astore_1
    //   69: aload_3
    //   70: monitorexit
    //   71: aload_1
    //   72: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #157	-> 0
    //   #158	-> 2
    //   #159	-> 9
    //   #160	-> 29
    //   #162	-> 63
    //   #163	-> 65
    //   #162	-> 68
    // Exception table:
    //   from	to	target	type
    //   12	19	68	finally
    //   22	29	68	finally
    //   29	51	68	finally
    //   63	65	68	finally
    //   69	71	68	finally
  }
  
  class SafePPWindowObserver extends ContentObserver {
    final OplusSafeDbReader this$0;
    
    SafePPWindowObserver(Handler param1Handler) {
      super(param1Handler);
    }
    
    public void onChange(boolean param1Boolean) {
      Log.d("OplusSafeDbReader", "change  begin:");
      OplusSafeDbReader.this.getData();
      Log.d("OplusSafeDbReader", "change end:");
    }
    
    void observe() {
      ContentResolver contentResolver = OplusSafeDbReader.this.mContext.getContentResolver();
      contentResolver.registerContentObserver(OplusSafeDbReader.this.mUri, true, this);
    }
  }
  
  class DataFileListener extends FileObserver {
    String observerPath = null;
    
    final OplusSafeDbReader this$0;
    
    public DataFileListener(String param1String) {
      super(param1String, 264);
      this.observerPath = param1String;
    }
    
    public void onEvent(int param1Int, String param1String) {
      if (param1Int == 8)
        if (this.observerPath != null) {
          File file = new File(this.observerPath);
          if (!file.exists()) {
            if (!file.getParentFile().exists())
              file.getParentFile().mkdirs(); 
            try {
              file.createNewFile();
            } catch (IOException iOException) {
              iOException.printStackTrace();
            } 
          } 
          if (this.observerPath.equals("//data//oppo//coloros//toast//toast.xml"))
            OplusSafeDbReader.this.getToastAppMapPri(); 
        }  
    }
  }
  
  private class GetDataFromProviderRunnable implements Runnable {
    final OplusSafeDbReader this$0;
    
    public void run() {
      if (OplusSafeDbReader.sDebug)
        Log.d("OplusSafeDbReader", "start run "); 
      while (true) {
        if (ActivityManagerNative.isSystemReady()) {
          OplusSafeDbReader.this.getData();
          if (OplusSafeDbReader.sDebug)
            Log.d("OplusSafeDbReader", "isSystemReady is true  !!!!! "); 
          return;
        } 
        try {
          Thread.sleep(100L);
        } catch (InterruptedException interruptedException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("sleep 100 ms is Interrupted because of ");
          stringBuilder.append(interruptedException);
          Log.w("OplusSafeDbReader", stringBuilder.toString());
        } 
      } 
    }
  }
  
  private void getToastAppMapPri() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLock : Ljava/lang/Object;
    //   4: astore_1
    //   5: aload_1
    //   6: monitorenter
    //   7: aconst_null
    //   8: astore_2
    //   9: aconst_null
    //   10: astore_3
    //   11: aconst_null
    //   12: astore #4
    //   14: aload_2
    //   15: astore #5
    //   17: aload_3
    //   18: astore #6
    //   20: new java/io/File
    //   23: astore #7
    //   25: aload_2
    //   26: astore #5
    //   28: aload_3
    //   29: astore #6
    //   31: aload #7
    //   33: ldc '//data//oppo//coloros//toast//toast.xml'
    //   35: invokespecial <init> : (Ljava/lang/String;)V
    //   38: aload_2
    //   39: astore #5
    //   41: aload_3
    //   42: astore #6
    //   44: aload #7
    //   46: invokevirtual exists : ()Z
    //   49: ifeq -> 120
    //   52: aload_2
    //   53: astore #5
    //   55: aload_3
    //   56: astore #6
    //   58: new java/io/FileInputStream
    //   61: astore #4
    //   63: aload_2
    //   64: astore #5
    //   66: aload_3
    //   67: astore #6
    //   69: aload #4
    //   71: aload #7
    //   73: invokespecial <init> : (Ljava/io/File;)V
    //   76: aload #4
    //   78: astore #5
    //   80: aload #4
    //   82: astore #6
    //   84: aload_0
    //   85: getfield mUserCloseToastPackageName : Ljava/util/ArrayList;
    //   88: invokevirtual clear : ()V
    //   91: aload #4
    //   93: astore #5
    //   95: aload #4
    //   97: astore #6
    //   99: aload_0
    //   100: getfield mUserAllowToastPackageName : Ljava/util/ArrayList;
    //   103: invokevirtual clear : ()V
    //   106: aload #4
    //   108: astore #5
    //   110: aload #4
    //   112: astore #6
    //   114: aload_0
    //   115: aload #4
    //   117: invokespecial readToastFromXML : (Ljava/io/FileInputStream;)V
    //   120: aload #4
    //   122: ifnull -> 189
    //   125: aload #4
    //   127: invokevirtual close : ()V
    //   130: goto -> 189
    //   133: astore #4
    //   135: aload #4
    //   137: invokevirtual printStackTrace : ()V
    //   140: goto -> 130
    //   143: astore #4
    //   145: goto -> 192
    //   148: astore #4
    //   150: aload #6
    //   152: astore #5
    //   154: ldc 'OplusSafeDbReader'
    //   156: ldc 'getToastAppMap() error !'
    //   158: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   161: pop
    //   162: aload #6
    //   164: astore #5
    //   166: aload #4
    //   168: invokevirtual printStackTrace : ()V
    //   171: aload #6
    //   173: ifnull -> 189
    //   176: aload #6
    //   178: invokevirtual close : ()V
    //   181: goto -> 130
    //   184: astore #4
    //   186: goto -> 135
    //   189: aload_1
    //   190: monitorexit
    //   191: return
    //   192: aload #5
    //   194: ifnull -> 212
    //   197: aload #5
    //   199: invokevirtual close : ()V
    //   202: goto -> 212
    //   205: astore #5
    //   207: aload #5
    //   209: invokevirtual printStackTrace : ()V
    //   212: aload #4
    //   214: athrow
    //   215: astore #4
    //   217: aload_1
    //   218: monitorexit
    //   219: aload #4
    //   221: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #243	-> 0
    //   #244	-> 7
    //   #246	-> 14
    //   #247	-> 38
    //   #248	-> 52
    //   #249	-> 76
    //   #250	-> 91
    //   #251	-> 106
    //   #257	-> 120
    //   #259	-> 125
    //   #262	-> 130
    //   #260	-> 133
    //   #261	-> 135
    //   #257	-> 143
    //   #253	-> 148
    //   #254	-> 150
    //   #255	-> 162
    //   #257	-> 171
    //   #259	-> 176
    //   #260	-> 184
    //   #265	-> 189
    //   #266	-> 191
    //   #257	-> 192
    //   #259	-> 197
    //   #262	-> 202
    //   #260	-> 205
    //   #261	-> 207
    //   #264	-> 212
    //   #265	-> 215
    // Exception table:
    //   from	to	target	type
    //   20	25	148	java/lang/Exception
    //   20	25	143	finally
    //   31	38	148	java/lang/Exception
    //   31	38	143	finally
    //   44	52	148	java/lang/Exception
    //   44	52	143	finally
    //   58	63	148	java/lang/Exception
    //   58	63	143	finally
    //   69	76	148	java/lang/Exception
    //   69	76	143	finally
    //   84	91	148	java/lang/Exception
    //   84	91	143	finally
    //   99	106	148	java/lang/Exception
    //   99	106	143	finally
    //   114	120	148	java/lang/Exception
    //   114	120	143	finally
    //   125	130	133	java/io/IOException
    //   125	130	215	finally
    //   135	140	215	finally
    //   154	162	143	finally
    //   166	171	143	finally
    //   176	181	184	java/io/IOException
    //   176	181	215	finally
    //   189	191	215	finally
    //   197	202	205	java/io/IOException
    //   197	202	215	finally
    //   207	212	215	finally
    //   212	215	215	finally
    //   217	219	215	finally
  }
  
  public void startThread() {
    init();
    this.mThread.start();
  }
  
  private void readToastFromXML(FileInputStream paramFileInputStream) {
    try {
      int i;
      XmlPullParser xmlPullParser = Xml.newPullParser();
      xmlPullParser.setInput(paramFileInputStream, null);
      do {
        String str3;
        i = xmlPullParser.next();
        if (i != 2)
          continue; 
        String str1 = xmlPullParser.getName();
        boolean bool = "toast".equals(str1);
        if (!bool)
          continue; 
        str1 = null;
        String str2 = null;
        try {
          str3 = xmlPullParser.getAttributeValue(null, "packagename");
          str1 = str3;
          String str = xmlPullParser.nextText();
          str1 = str3;
          str3 = str2;
        } catch (IOException iOException) {
          iOException.printStackTrace();
          str3 = str2;
        } 
        if (str1 == null || str3 == null)
          continue; 
        if ("0".equals(str3)) {
          this.mUserCloseToastPackageName.add(str1);
        } else if ("1".equals(str3)) {
          this.mUserAllowToastPackageName.add(str1);
        } 
      } while (i != 1);
    } catch (Exception exception) {
      Log.e("OplusSafeDbReader", "readToastFromXML() error");
      exception.printStackTrace();
    } finally {}
  }
}
