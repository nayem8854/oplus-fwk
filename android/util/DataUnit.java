package android.util;

public enum DataUnit {
  GIBIBYTES, GIGABYTES, KIBIBYTES, KILOBYTES, MEBIBYTES, MEGABYTES;
  
  private static final DataUnit[] $VALUES;
  
  static {
    GIGABYTES = (DataUnit)new Object("GIGABYTES", 2);
    KIBIBYTES = (DataUnit)new Object("KIBIBYTES", 3);
    MEBIBYTES = (DataUnit)new Object("MEBIBYTES", 4);
    Object object = new Object("GIBIBYTES", 5);
    $VALUES = new DataUnit[] { KILOBYTES, MEGABYTES, GIGABYTES, KIBIBYTES, MEBIBYTES, (DataUnit)object };
  }
  
  public long toBytes(long paramLong) {
    throw new AbstractMethodError();
  }
}
