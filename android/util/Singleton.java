package android.util;

public abstract class Singleton<T> {
  private T mInstance;
  
  protected abstract T create();
  
  public final T get() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mInstance : Ljava/lang/Object;
    //   6: ifnonnull -> 17
    //   9: aload_0
    //   10: aload_0
    //   11: invokevirtual create : ()Ljava/lang/Object;
    //   14: putfield mInstance : Ljava/lang/Object;
    //   17: aload_0
    //   18: getfield mInstance : Ljava/lang/Object;
    //   21: astore_1
    //   22: aload_0
    //   23: monitorexit
    //   24: aload_1
    //   25: areturn
    //   26: astore_1
    //   27: aload_0
    //   28: monitorexit
    //   29: aload_1
    //   30: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #41	-> 0
    //   #42	-> 2
    //   #43	-> 9
    //   #45	-> 17
    //   #46	-> 26
    // Exception table:
    //   from	to	target	type
    //   2	9	26	finally
    //   9	17	26	finally
    //   17	24	26	finally
    //   27	29	26	finally
  }
}
