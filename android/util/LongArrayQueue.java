package android.util;

import com.android.internal.util.ArrayUtils;
import com.android.internal.util.GrowingArrayUtils;
import java.util.NoSuchElementException;
import libcore.util.EmptyArray;

public class LongArrayQueue {
  private int mHead;
  
  private int mSize;
  
  private int mTail;
  
  private long[] mValues;
  
  public LongArrayQueue(int paramInt) {
    if (paramInt == 0) {
      this.mValues = EmptyArray.LONG;
    } else {
      this.mValues = ArrayUtils.newUnpaddedLongArray(paramInt);
    } 
    this.mSize = 0;
    this.mTail = 0;
    this.mHead = 0;
  }
  
  public LongArrayQueue() {
    this(16);
  }
  
  private void grow() {
    int i = this.mSize;
    if (i >= this.mValues.length) {
      i = GrowingArrayUtils.growSize(i);
      long[] arrayOfLong1 = ArrayUtils.newUnpaddedLongArray(i);
      long[] arrayOfLong2 = this.mValues;
      int j = arrayOfLong2.length;
      i = this.mHead;
      j -= i;
      System.arraycopy(arrayOfLong2, i, arrayOfLong1, 0, j);
      System.arraycopy(this.mValues, 0, arrayOfLong1, j, this.mHead);
      this.mValues = arrayOfLong1;
      this.mHead = 0;
      this.mTail = this.mSize;
      return;
    } 
    throw new IllegalStateException("Queue not full yet!");
  }
  
  public int size() {
    return this.mSize;
  }
  
  public void clear() {
    this.mSize = 0;
    this.mTail = 0;
    this.mHead = 0;
  }
  
  public void addLast(long paramLong) {
    if (this.mSize == this.mValues.length)
      grow(); 
    long[] arrayOfLong = this.mValues;
    int i = this.mTail;
    arrayOfLong[i] = paramLong;
    this.mTail = (i + 1) % arrayOfLong.length;
    this.mSize++;
  }
  
  public long removeFirst() {
    int i = this.mSize;
    if (i != 0) {
      long[] arrayOfLong = this.mValues;
      int j = this.mHead;
      long l = arrayOfLong[j];
      this.mHead = (j + 1) % arrayOfLong.length;
      this.mSize = i - 1;
      return l;
    } 
    throw new NoSuchElementException("Queue is empty!");
  }
  
  public long get(int paramInt) {
    if (paramInt >= 0 && paramInt < this.mSize) {
      int i = this.mHead;
      long[] arrayOfLong = this.mValues;
      int j = arrayOfLong.length;
      return arrayOfLong[(i + paramInt) % j];
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Index ");
    stringBuilder.append(paramInt);
    stringBuilder.append(" not valid for a queue of size ");
    stringBuilder.append(this.mSize);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public long peekFirst() {
    if (this.mSize != 0)
      return this.mValues[this.mHead]; 
    throw new NoSuchElementException("Queue is empty!");
  }
  
  public long peekLast() {
    if (this.mSize != 0) {
      int i = this.mTail, j = i;
      if (i == 0)
        j = this.mValues.length; 
      return this.mValues[j - 1];
    } 
    throw new NoSuchElementException("Queue is empty!");
  }
  
  public String toString() {
    int i = this.mSize;
    if (i <= 0)
      return "{}"; 
    StringBuilder stringBuilder = new StringBuilder(i * 64);
    stringBuilder.append('{');
    stringBuilder.append(get(0));
    for (i = 1; i < this.mSize; i++) {
      stringBuilder.append(", ");
      stringBuilder.append(get(i));
    } 
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
}
