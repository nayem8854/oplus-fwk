package android.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import libcore.util.XmlObjectFactory;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class Xml {
  public static String FEATURE_RELAXED = "http://xmlpull.org/v1/doc/features.html#relaxed";
  
  public static void parse(String paramString, ContentHandler paramContentHandler) throws SAXException {
    try {
      XMLReader xMLReader = XmlObjectFactory.newXMLReader();
      xMLReader.setContentHandler(paramContentHandler);
      InputSource inputSource = new InputSource();
      StringReader stringReader = new StringReader();
      this(paramString);
      this(stringReader);
      xMLReader.parse(inputSource);
      return;
    } catch (IOException iOException) {
      throw new AssertionError(iOException);
    } 
  }
  
  public static void parse(Reader paramReader, ContentHandler paramContentHandler) throws IOException, SAXException {
    XMLReader xMLReader = XmlObjectFactory.newXMLReader();
    xMLReader.setContentHandler(paramContentHandler);
    xMLReader.parse(new InputSource(paramReader));
  }
  
  public static void parse(InputStream paramInputStream, Encoding paramEncoding, ContentHandler paramContentHandler) throws IOException, SAXException {
    XMLReader xMLReader = XmlObjectFactory.newXMLReader();
    xMLReader.setContentHandler(paramContentHandler);
    InputSource inputSource = new InputSource(paramInputStream);
    inputSource.setEncoding(paramEncoding.expatName);
    xMLReader.parse(inputSource);
  }
  
  public static XmlPullParser newPullParser() {
    try {
      XmlPullParser xmlPullParser = XmlObjectFactory.newXmlPullParser();
      xmlPullParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-docdecl", true);
      xmlPullParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", true);
      return xmlPullParser;
    } catch (XmlPullParserException xmlPullParserException) {
      throw new AssertionError();
    } 
  }
  
  public static XmlSerializer newSerializer() {
    return XmlObjectFactory.newXmlSerializer();
  }
  
  public enum Encoding {
    ISO_8859_1,
    US_ASCII("US-ASCII"),
    UTF_16("US-ASCII"),
    UTF_8("UTF-8");
    
    private static final Encoding[] $VALUES;
    
    final String expatName;
    
    static {
      Encoding encoding = new Encoding("ISO_8859_1", 3, "ISO-8859-1");
      $VALUES = new Encoding[] { US_ASCII, UTF_8, UTF_16, encoding };
    }
    
    Encoding(String param1String1) {
      this.expatName = param1String1;
    }
  }
  
  public static Encoding findEncodingByName(String paramString) throws UnsupportedEncodingException {
    if (paramString == null)
      return Encoding.UTF_8; 
    for (Encoding encoding : Encoding.values()) {
      if (encoding.expatName.equalsIgnoreCase(paramString))
        return encoding; 
    } 
    throw new UnsupportedEncodingException(paramString);
  }
  
  public static AttributeSet asAttributeSet(XmlPullParser paramXmlPullParser) {
    AttributeSet attributeSet;
    if (paramXmlPullParser instanceof AttributeSet) {
      attributeSet = (AttributeSet)paramXmlPullParser;
    } else {
      attributeSet = new XmlPullAttributes((XmlPullParser)attributeSet);
    } 
    return attributeSet;
  }
}
