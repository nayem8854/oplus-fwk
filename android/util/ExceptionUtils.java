package android.util;

import android.os.ParcelableException;
import com.android.internal.util.Preconditions;
import java.io.IOException;

public class ExceptionUtils {
  public static RuntimeException wrap(IOException paramIOException) {
    throw new ParcelableException(paramIOException);
  }
  
  public static void maybeUnwrapIOException(RuntimeException paramRuntimeException) throws IOException {
    if (paramRuntimeException instanceof ParcelableException)
      ((ParcelableException)paramRuntimeException).maybeRethrow(IOException.class); 
  }
  
  public static String getCompleteMessage(String paramString, Throwable paramThrowable) {
    StringBuilder stringBuilder = new StringBuilder();
    if (paramString != null) {
      stringBuilder.append(paramString);
      stringBuilder.append(": ");
    } 
    stringBuilder.append(paramThrowable.getMessage());
    Throwable throwable = paramThrowable;
    while (true) {
      throwable = paramThrowable = throwable.getCause();
      if (paramThrowable != null) {
        stringBuilder.append(": ");
        stringBuilder.append(throwable.getMessage());
        continue;
      } 
      break;
    } 
    return stringBuilder.toString();
  }
  
  public static String getCompleteMessage(Throwable paramThrowable) {
    return getCompleteMessage(null, paramThrowable);
  }
  
  public static <E extends Throwable> void propagateIfInstanceOf(Throwable paramThrowable, Class<E> paramClass) throws E {
    if (paramThrowable == null || !paramClass.isInstance(paramThrowable))
      return; 
    throw (E)paramClass.cast(paramThrowable);
  }
  
  public static <E extends Exception> RuntimeException propagate(Throwable paramThrowable, Class<E> paramClass) throws E {
    propagateIfInstanceOf(paramThrowable, (Class)paramClass);
    return propagate(paramThrowable);
  }
  
  public static RuntimeException propagate(Throwable paramThrowable) {
    Preconditions.checkNotNull(paramThrowable);
    propagateIfInstanceOf(paramThrowable, Error.class);
    propagateIfInstanceOf(paramThrowable, RuntimeException.class);
    throw new RuntimeException(paramThrowable);
  }
  
  public static Throwable getRootCause(Throwable paramThrowable) {
    for (; paramThrowable.getCause() != null; paramThrowable = paramThrowable.getCause());
    return paramThrowable;
  }
  
  public static Throwable appendCause(Throwable paramThrowable1, Throwable paramThrowable2) {
    if (paramThrowable2 != null)
      getRootCause(paramThrowable1).initCause(paramThrowable2); 
    return paramThrowable1;
  }
}
