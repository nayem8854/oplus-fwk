package android.util;

import java.io.Writer;

public class LogWriter extends Writer {
  private final int mBuffer;
  
  private StringBuilder mBuilder = new StringBuilder(128);
  
  private final int mPriority;
  
  private final String mTag;
  
  public LogWriter(int paramInt, String paramString) {
    this.mPriority = paramInt;
    this.mTag = paramString;
    this.mBuffer = 0;
  }
  
  public LogWriter(int paramInt1, String paramString, int paramInt2) {
    this.mPriority = paramInt1;
    this.mTag = paramString;
    this.mBuffer = paramInt2;
  }
  
  public void close() {
    flushBuilder();
  }
  
  public void flush() {
    flushBuilder();
  }
  
  public void write(char[] paramArrayOfchar, int paramInt1, int paramInt2) {
    for (byte b = 0; b < paramInt2; b++) {
      char c = paramArrayOfchar[paramInt1 + b];
      if (c == '\n') {
        flushBuilder();
      } else {
        this.mBuilder.append(c);
      } 
    } 
  }
  
  private void flushBuilder() {
    if (this.mBuilder.length() > 0) {
      Log.println_native(this.mBuffer, this.mPriority, this.mTag, this.mBuilder.toString());
      StringBuilder stringBuilder = this.mBuilder;
      stringBuilder.delete(0, stringBuilder.length());
    } 
  }
}
