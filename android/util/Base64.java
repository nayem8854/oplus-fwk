package android.util;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import res.Hex;

public class Base64 {
  static final boolean $assertionsDisabled = false;
  
  public static final int CRLF = 4;
  
  public static final int DEFAULT = 0;
  
  public static final int NO_CLOSE = 16;
  
  public static final int NO_PADDING = 1;
  
  public static final int NO_WRAP = 2;
  
  public static final int URL_SAFE = 8;
  
  static abstract class Coder {
    public int op;
    
    public byte[] output;
    
    public abstract int maxOutputSize(int param1Int);
    
    public abstract boolean process(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2, boolean param1Boolean);
  }
  
  public static byte[] decode(String paramString, int paramInt) {
    return decode(paramString.getBytes(), paramInt);
  }
  
  public static byte[] decode(byte[] paramArrayOfbyte, int paramInt) {
    return decode(paramArrayOfbyte, 0, paramArrayOfbyte.length, paramInt);
  }
  
  public static byte[] decode(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3) {
    Decoder decoder = new Decoder(paramInt3, new byte[paramInt2 * 3 / 4]);
    if (decoder.process(paramArrayOfbyte, paramInt1, paramInt2, true)) {
      if (decoder.op == decoder.output.length)
        return decoder.output; 
      paramArrayOfbyte = new byte[decoder.op];
      System.arraycopy(decoder.output, 0, paramArrayOfbyte, 0, decoder.op);
      return paramArrayOfbyte;
    } 
    throw new IllegalArgumentException("bad base-64");
  }
  
  class Decoder extends Coder {
    private static long[] $d2j$hex$32675909$decode_J(String src) {
      byte[] d = Hex.decode_B(src);
      ByteBuffer b = ByteBuffer.wrap(d);
      b.order(ByteOrder.LITTLE_ENDIAN);
      LongBuffer s = b.asLongBuffer();
      long[] data = new long[d.length / 8];
      s.get(data);
      return data;
    }
    
    private static int[] $d2j$hex$32675909$decode_I(String src) {
      byte[] d = Hex.decode_B(src);
      ByteBuffer b = ByteBuffer.wrap(d);
      b.order(ByteOrder.LITTLE_ENDIAN);
      IntBuffer s = b.asIntBuffer();
      int[] data = new int[d.length / 4];
      s.get(data);
      return data;
    }
    
    private static short[] $d2j$hex$32675909$decode_S(String src) {
      byte[] d = Hex.decode_B(src);
      ByteBuffer b = ByteBuffer.wrap(d);
      b.order(ByteOrder.LITTLE_ENDIAN);
      ShortBuffer s = b.asShortBuffer();
      short[] data = new short[d.length / 2];
      s.get(data);
      return data;
    }
    
    private static byte[] $d2j$hex$32675909$decode_B(String src) {
      char[] d = src.toCharArray();
      byte[] ret = new byte[src.length() / 2];
      for (int i = 0; i < ret.length; i++) {
        char h = d[2 * i];
        char l = d[2 * i + 1];
        int hh = 0;
        if (h >= '0' && h <= '9') {
          hh = h - 48;
        } else if (h >= 'a' && h <= 'f') {
          hh = h - 97 + 10;
        } else if (h >= 'A' && h <= 'F') {
          hh = h - 65 + 10;
        } else {
          throw new RuntimeException();
        } 
        int ll = 0;
        if (l >= '0' && l <= '9') {
          ll = h - 48;
        } else if (l >= 'a' && l <= 'f') {
          ll = h - 97 + 10;
        } else if (l >= 'A' && l <= 'F') {
          ll = h - 65 + 10;
        } else {
          throw new RuntimeException();
        } 
        d[i] = (char)(hh << 4 | ll);
      } 
      return ret;
    }
    
    private static final int[] DECODE = $d2j$hex$32675909$decode_I("ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff3e000000ffffffffffffffffffffffff3f0000003400000035000000360000003700000038000000390000003a0000003b0000003c0000003d000000fffffffffffffffffffffffffeffffffffffffffffffffffffffffff000000000100000002000000030000000400000005000000060000000700000008000000090000000a0000000b0000000c0000000d0000000e0000000f00000010000000110000001200000013000000140000001500000016000000170000001800000019000000ffffffffffffffffffffffffffffffffffffffffffffffff1a0000001b0000001c0000001d0000001e0000001f000000200000002100000022000000230000002400000025000000260000002700000028000000290000002a0000002b0000002c0000002d0000002e0000002f00000030000000310000003200000033000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff");
    
    private static final int[] DECODE_WEBSAFE = $d2j$hex$32675909$decode_I("ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff3e000000ffffffffffffffff3400000035000000360000003700000038000000390000003a0000003b0000003c0000003d000000fffffffffffffffffffffffffeffffffffffffffffffffffffffffff000000000100000002000000030000000400000005000000060000000700000008000000090000000a0000000b0000000c0000000d0000000e0000000f00000010000000110000001200000013000000140000001500000016000000170000001800000019000000ffffffffffffffffffffffffffffffff3f000000ffffffff1a0000001b0000001c0000001d0000001e0000001f000000200000002100000022000000230000002400000025000000260000002700000028000000290000002a0000002b0000002c0000002d0000002e0000002f00000030000000310000003200000033000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff");
    
    private static final int EQUALS = -2;
    
    private static final int SKIP = -1;
    
    private final int[] alphabet;
    
    private int state;
    
    private int value;
    
    public Decoder(Base64 this$0, byte[] param1ArrayOfbyte) {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial <init> : ()V
      //   4: aload_0
      //   5: aload_2
      //   6: putfield output : [B
      //   9: iload_1
      //   10: bipush #8
      //   12: iand
      //   13: ifne -> 23
      //   16: getstatic android/util/Base64$Decoder.DECODE : [I
      //   19: astore_2
      //   20: goto -> 27
      //   23: getstatic android/util/Base64$Decoder.DECODE_WEBSAFE : [I
      //   26: astore_2
      //   27: aload_0
      //   28: aload_2
      //   29: putfield alphabet : [I
      //   32: aload_0
      //   33: iconst_0
      //   34: putfield state : I
      //   37: aload_0
      //   38: iconst_0
      //   39: putfield value : I
      //   42: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #243	-> 0
      //   #244	-> 4
      //   #246	-> 9
      //   #247	-> 32
      //   #248	-> 37
      //   #249	-> 42
    }
    
    public int maxOutputSize(int param1Int) {
      return param1Int * 3 / 4 + 10;
    }
    
    public boolean process(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2, boolean param1Boolean) {
      int m, n;
      if (this.state == 6)
        return false; 
      int i = param1Int1;
      int j = param1Int2 + param1Int1;
      int k = this.state;
      param1Int2 = this.value;
      param1Int1 = 0;
      byte[] arrayOfByte = this.output;
      int[] arrayOfInt = this.alphabet;
      while (true) {
        m = param1Int2;
        n = param1Int1;
        if (i < j) {
          int i1 = i;
          m = param1Int2;
          n = param1Int1;
          if (k == 0) {
            while (i + 4 <= j) {
              param1Int2 = n = m = arrayOfInt[param1ArrayOfbyte[i] & 0xFF] << 18 | arrayOfInt[param1ArrayOfbyte[i + 1] & 0xFF] << 12 | arrayOfInt[param1ArrayOfbyte[i + 2] & 0xFF] << 6 | arrayOfInt[param1ArrayOfbyte[i + 3] & 0xFF];
              if (m >= 0) {
                arrayOfByte[param1Int1 + 2] = (byte)n;
                arrayOfByte[param1Int1 + 1] = (byte)(n >> 8);
                arrayOfByte[param1Int1] = (byte)(n >> 16);
                param1Int1 += 3;
                i += 4;
                param1Int2 = n;
              } 
            } 
            i1 = i;
            m = param1Int2;
            n = param1Int1;
            if (i >= j) {
              m = param1Int2;
              n = param1Int1;
              break;
            } 
          } 
          i = arrayOfInt[param1ArrayOfbyte[i1] & 0xFF];
          if (k != 0) {
            if (k != 1) {
              if (k != 2) {
                if (k != 3) {
                  if (k != 4) {
                    if (k != 5) {
                      param1Int1 = k;
                      param1Int2 = m;
                    } else {
                      param1Int1 = k;
                      param1Int2 = m;
                      if (i != -1) {
                        this.state = 6;
                        return false;
                      } 
                    } 
                  } else if (i == -2) {
                    param1Int1 = k + 1;
                    param1Int2 = m;
                  } else {
                    param1Int1 = k;
                    param1Int2 = m;
                    if (i != -1) {
                      this.state = 6;
                      return false;
                    } 
                  } 
                } else if (i >= 0) {
                  param1Int2 = m << 6 | i;
                  arrayOfByte[n + 2] = (byte)param1Int2;
                  arrayOfByte[n + 1] = (byte)(param1Int2 >> 8);
                  arrayOfByte[n] = (byte)(param1Int2 >> 16);
                  n += 3;
                  param1Int1 = 0;
                } else if (i == -2) {
                  arrayOfByte[n + 1] = (byte)(m >> 2);
                  arrayOfByte[n] = (byte)(m >> 10);
                  n += 2;
                  param1Int1 = 5;
                  param1Int2 = m;
                } else {
                  param1Int1 = k;
                  param1Int2 = m;
                  if (i != -1) {
                    this.state = 6;
                    return false;
                  } 
                } 
              } else if (i >= 0) {
                param1Int2 = m << 6 | i;
                param1Int1 = k + 1;
              } else if (i == -2) {
                arrayOfByte[n] = (byte)(m >> 4);
                param1Int1 = 4;
                n++;
                param1Int2 = m;
              } else {
                param1Int1 = k;
                param1Int2 = m;
                if (i != -1) {
                  this.state = 6;
                  return false;
                } 
              } 
            } else if (i >= 0) {
              param1Int2 = m << 6 | i;
              param1Int1 = k + 1;
            } else {
              param1Int1 = k;
              param1Int2 = m;
              if (i != -1) {
                this.state = 6;
                return false;
              } 
            } 
          } else if (i >= 0) {
            param1Int2 = i;
            param1Int1 = k + 1;
          } else {
            param1Int1 = k;
            param1Int2 = m;
            if (i != -1) {
              this.state = 6;
              return false;
            } 
          } 
          i = i1 + 1;
          k = param1Int1;
          param1Int1 = n;
          continue;
        } 
        break;
      } 
      if (!param1Boolean) {
        this.state = k;
        this.value = m;
        this.op = n;
        return true;
      } 
      if (k != 1) {
        if (k != 2) {
          if (k != 3) {
            if (k == 4) {
              this.state = 6;
              return false;
            } 
          } else {
            param1Int1 = n + 1;
            arrayOfByte[n] = (byte)(m >> 10);
            n = param1Int1 + 1;
            arrayOfByte[param1Int1] = (byte)(m >> 2);
          } 
        } else {
          arrayOfByte[n] = (byte)(m >> 4);
          n++;
        } 
        this.state = k;
        this.op = n;
        return true;
      } 
      this.state = 6;
      return false;
    }
  }
  
  public static String encodeToString(byte[] paramArrayOfbyte, int paramInt) {
    try {
      return new String(encode(paramArrayOfbyte, paramInt), "US-ASCII");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      throw new AssertionError(unsupportedEncodingException);
    } 
  }
  
  public static String encodeToString(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3) {
    try {
      return new String(encode(paramArrayOfbyte, paramInt1, paramInt2, paramInt3), "US-ASCII");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      throw new AssertionError(unsupportedEncodingException);
    } 
  }
  
  public static byte[] encode(byte[] paramArrayOfbyte, int paramInt) {
    return encode(paramArrayOfbyte, 0, paramArrayOfbyte.length, paramInt);
  }
  
  public static byte[] encode(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3) {
    Encoder encoder = new Encoder(paramInt3, null);
    int i = paramInt2 / 3 * 4;
    boolean bool = encoder.do_padding;
    byte b = 2;
    if (bool) {
      paramInt3 = i;
      if (paramInt2 % 3 > 0)
        paramInt3 = i + 4; 
    } else {
      paramInt3 = paramInt2 % 3;
      if (paramInt3 != 1) {
        if (paramInt3 != 2) {
          paramInt3 = i;
        } else {
          paramInt3 = i + 3;
        } 
      } else {
        paramInt3 = i + 2;
      } 
    } 
    i = paramInt3;
    if (encoder.do_newline) {
      i = paramInt3;
      if (paramInt2 > 0) {
        int j = (paramInt2 - 1) / 57;
        if (encoder.do_cr) {
          i = b;
        } else {
          i = 1;
        } 
        i = paramInt3 + (j + 1) * i;
      } 
    } 
    encoder.output = new byte[i];
    encoder.process(paramArrayOfbyte, paramInt1, paramInt2, true);
    return encoder.output;
  }
  
  class Encoder extends Coder {
    static final boolean $assertionsDisabled = false;
    
    private static final byte[] ENCODE = new byte[] { 
        65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 
        75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 
        85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 
        101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 
        111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 
        121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 
        56, 57, 43, 47 };
    
    private static final byte[] ENCODE_WEBSAFE = new byte[] { 
        65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 
        75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 
        85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 
        101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 
        111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 
        121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 
        56, 57, 45, 95 };
    
    public static final int LINE_GROUPS = 19;
    
    private final byte[] alphabet;
    
    private int count;
    
    public final boolean do_cr;
    
    public final boolean do_newline;
    
    public final boolean do_padding;
    
    private final byte[] tail;
    
    int tailLen;
    
    public Encoder(Base64 this$0, byte[] param1ArrayOfbyte) {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial <init> : ()V
      //   4: aload_0
      //   5: aload_2
      //   6: putfield output : [B
      //   9: iconst_1
      //   10: istore_3
      //   11: iload_1
      //   12: iconst_1
      //   13: iand
      //   14: ifne -> 23
      //   17: iconst_1
      //   18: istore #4
      //   20: goto -> 26
      //   23: iconst_0
      //   24: istore #4
      //   26: aload_0
      //   27: iload #4
      //   29: putfield do_padding : Z
      //   32: iload_1
      //   33: iconst_2
      //   34: iand
      //   35: ifne -> 44
      //   38: iconst_1
      //   39: istore #4
      //   41: goto -> 47
      //   44: iconst_0
      //   45: istore #4
      //   47: aload_0
      //   48: iload #4
      //   50: putfield do_newline : Z
      //   53: iload_1
      //   54: iconst_4
      //   55: iand
      //   56: ifeq -> 65
      //   59: iload_3
      //   60: istore #4
      //   62: goto -> 68
      //   65: iconst_0
      //   66: istore #4
      //   68: aload_0
      //   69: iload #4
      //   71: putfield do_cr : Z
      //   74: iload_1
      //   75: bipush #8
      //   77: iand
      //   78: ifne -> 88
      //   81: getstatic android/util/Base64$Encoder.ENCODE : [B
      //   84: astore_2
      //   85: goto -> 92
      //   88: getstatic android/util/Base64$Encoder.ENCODE_WEBSAFE : [B
      //   91: astore_2
      //   92: aload_0
      //   93: aload_2
      //   94: putfield alphabet : [B
      //   97: aload_0
      //   98: iconst_2
      //   99: newarray byte
      //   101: putfield tail : [B
      //   104: aload_0
      //   105: iconst_0
      //   106: putfield tailLen : I
      //   109: aload_0
      //   110: getfield do_newline : Z
      //   113: ifeq -> 122
      //   116: bipush #19
      //   118: istore_1
      //   119: goto -> 124
      //   122: iconst_m1
      //   123: istore_1
      //   124: aload_0
      //   125: iload_1
      //   126: putfield count : I
      //   129: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #583	-> 0
      //   #584	-> 4
      //   #586	-> 9
      //   #587	-> 32
      //   #588	-> 53
      //   #589	-> 74
      //   #591	-> 97
      //   #592	-> 104
      //   #594	-> 109
      //   #595	-> 129
    }
    
    public int maxOutputSize(int param1Int) {
      return param1Int * 8 / 5 + 10;
    }
    
    public boolean process(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2, boolean param1Boolean) {
      int i1;
      byte[] arrayOfByte1 = this.alphabet;
      byte[] arrayOfByte2 = this.output;
      int i = 0;
      int j = this.count;
      int k = param1Int1;
      int m = param1Int2 + param1Int1;
      int n = -1;
      param1Int1 = this.tailLen;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          i1 = k;
        } else {
          i1 = k;
          if (k + 1 <= m) {
            byte[] arrayOfByte = this.tail;
            param1Int1 = arrayOfByte[0];
            n = (arrayOfByte[1] & 0xFF) << 8 | (param1Int1 & 0xFF) << 16 | param1ArrayOfbyte[k] & 0xFF;
            this.tailLen = 0;
            i1 = k + 1;
          } 
        } 
      } else {
        i1 = k;
        if (k + 2 <= m) {
          param1Int2 = this.tail[0];
          param1Int1 = k + 1;
          n = (param1ArrayOfbyte[k] & 0xFF) << 8 | (param1Int2 & 0xFF) << 16 | param1ArrayOfbyte[param1Int1] & 0xFF;
          this.tailLen = 0;
          i1 = param1Int1 + 1;
        } 
      } 
      param1Int1 = i;
      param1Int2 = j;
      k = i1;
      if (n != -1) {
        param1Int1 = 0 + 1;
        arrayOfByte2[0] = arrayOfByte1[n >> 18 & 0x3F];
        param1Int2 = param1Int1 + 1;
        arrayOfByte2[param1Int1] = arrayOfByte1[n >> 12 & 0x3F];
        param1Int1 = param1Int2 + 1;
        arrayOfByte2[param1Int2] = arrayOfByte1[n >> 6 & 0x3F];
        i = param1Int1 + 1;
        arrayOfByte2[param1Int1] = arrayOfByte1[n & 0x3F];
        n = j - 1;
        param1Int1 = i;
        param1Int2 = n;
        k = i1;
        if (n == 0) {
          param1Int1 = i;
          if (this.do_cr) {
            arrayOfByte2[i] = 13;
            param1Int1 = i + 1;
          } 
          arrayOfByte2[param1Int1] = 10;
          param1Int2 = 19;
          param1Int1++;
          k = i1;
        } 
      } 
      while (k + 3 <= m) {
        i1 = (param1ArrayOfbyte[k] & 0xFF) << 16 | (param1ArrayOfbyte[k + 1] & 0xFF) << 8 | param1ArrayOfbyte[k + 2] & 0xFF;
        arrayOfByte2[param1Int1] = arrayOfByte1[i1 >> 18 & 0x3F];
        arrayOfByte2[param1Int1 + 1] = arrayOfByte1[i1 >> 12 & 0x3F];
        arrayOfByte2[param1Int1 + 2] = arrayOfByte1[i1 >> 6 & 0x3F];
        arrayOfByte2[param1Int1 + 3] = arrayOfByte1[i1 & 0x3F];
        n = k + 3;
        i1 = param1Int1 + 4;
        i = param1Int2 - 1;
        param1Int1 = i1;
        param1Int2 = i;
        k = n;
        if (i == 0) {
          param1Int1 = i1;
          if (this.do_cr) {
            arrayOfByte2[i1] = 13;
            param1Int1 = i1 + 1;
          } 
          arrayOfByte2[param1Int1] = 10;
          param1Int2 = 19;
          param1Int1++;
          k = n;
        } 
      } 
      if (param1Boolean) {
        n = this.tailLen;
        if (k - n == m - 1) {
          i1 = 0;
          if (n > 0) {
            k = this.tail[0];
            i1 = 0 + 1;
          } else {
            k = param1ArrayOfbyte[k];
          } 
          k = (k & 0xFF) << 4;
          this.tailLen -= i1;
          n = param1Int1 + 1;
          arrayOfByte2[param1Int1] = arrayOfByte1[k >> 6 & 0x3F];
          i1 = n + 1;
          arrayOfByte2[n] = arrayOfByte1[k & 0x3F];
          param1Int1 = i1;
          if (this.do_padding) {
            k = i1 + 1;
            arrayOfByte2[i1] = 61;
            param1Int1 = k + 1;
            arrayOfByte2[k] = 61;
          } 
          i1 = param1Int1;
          if (this.do_newline) {
            i1 = param1Int1;
            if (this.do_cr) {
              arrayOfByte2[param1Int1] = 13;
              i1 = param1Int1 + 1;
            } 
            arrayOfByte2[i1] = 10;
            i1++;
          } 
        } else if (k - n == m - 2) {
          i1 = 0;
          if (n > 1) {
            i = this.tail[0];
            i1 = 0 + 1;
            n = k;
            k = i;
          } else {
            n = param1ArrayOfbyte[k];
            i = k + 1;
            k = n;
            n = i;
          } 
          if (this.tailLen > 0) {
            param1ArrayOfbyte = this.tail;
            i = i1 + 1;
            n = param1ArrayOfbyte[i1];
            i1 = i;
          } else {
            n = param1ArrayOfbyte[n];
          } 
          k = (k & 0xFF) << 10 | (n & 0xFF) << 2;
          this.tailLen -= i1;
          i1 = param1Int1 + 1;
          arrayOfByte2[param1Int1] = arrayOfByte1[k >> 12 & 0x3F];
          param1Int1 = i1 + 1;
          arrayOfByte2[i1] = arrayOfByte1[k >> 6 & 0x3F];
          i1 = param1Int1 + 1;
          arrayOfByte2[param1Int1] = arrayOfByte1[k & 0x3F];
          param1Int1 = i1;
          if (this.do_padding) {
            arrayOfByte2[i1] = 61;
            param1Int1 = i1 + 1;
          } 
          i1 = param1Int1;
          if (this.do_newline) {
            i1 = param1Int1;
            if (this.do_cr) {
              arrayOfByte2[param1Int1] = 13;
              i1 = param1Int1 + 1;
            } 
            arrayOfByte2[i1] = 10;
            i1++;
          } 
        } else {
          i1 = param1Int1;
          if (this.do_newline) {
            i1 = param1Int1;
            if (param1Int1 > 0) {
              i1 = param1Int1;
              if (param1Int2 != 19) {
                i1 = param1Int1;
                if (this.do_cr) {
                  arrayOfByte2[param1Int1] = 13;
                  i1 = param1Int1 + 1;
                } 
                arrayOfByte2[i1] = 10;
                i1++;
              } 
            } 
          } 
        } 
      } else if (k == m - 1) {
        arrayOfByte2 = this.tail;
        i1 = this.tailLen;
        this.tailLen = i1 + 1;
        arrayOfByte2[i1] = param1ArrayOfbyte[k];
        i1 = param1Int1;
      } else {
        i1 = param1Int1;
        if (k == m - 2) {
          arrayOfByte2 = this.tail;
          n = this.tailLen;
          this.tailLen = i1 = n + 1;
          arrayOfByte2[n] = param1ArrayOfbyte[k];
          this.tailLen = i1 + 1;
          arrayOfByte2[i1] = param1ArrayOfbyte[k + 1];
          i1 = param1Int1;
        } 
      } 
      this.op = i1;
      this.count = param1Int2;
      return true;
    }
  }
}
