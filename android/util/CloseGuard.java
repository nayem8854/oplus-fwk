package android.util;

public final class CloseGuard {
  private final dalvik.system.CloseGuard mImpl = dalvik.system.CloseGuard.get();
  
  public void open(String paramString) {
    this.mImpl.open(paramString);
  }
  
  public void close() {
    this.mImpl.close();
  }
  
  public void warnIfOpen() {
    this.mImpl.warnIfOpen();
  }
}
