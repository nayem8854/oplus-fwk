package android.util;

import android.content.pm.Signature;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import libcore.util.HexEncoding;

public final class PackageUtils {
  public static String[] computeSignaturesSha256Digests(Signature[] paramArrayOfSignature) {
    int i = paramArrayOfSignature.length;
    String[] arrayOfString = new String[i];
    for (byte b = 0; b < i; b++)
      arrayOfString[b] = computeSha256Digest(paramArrayOfSignature[b].toByteArray()); 
    return arrayOfString;
  }
  
  public static String computeSignaturesSha256Digest(Signature[] paramArrayOfSignature) {
    if (paramArrayOfSignature.length == 1)
      return computeSha256Digest(paramArrayOfSignature[0].toByteArray()); 
    String[] arrayOfString = computeSignaturesSha256Digests(paramArrayOfSignature);
    return computeSignaturesSha256Digest(arrayOfString);
  }
  
  public static String computeSignaturesSha256Digest(String[] paramArrayOfString) {
    int i = paramArrayOfString.length;
    byte b = 0;
    if (i == 1)
      return paramArrayOfString[0]; 
    Arrays.sort((Object[])paramArrayOfString);
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    for (i = paramArrayOfString.length; b < i; ) {
      String str = paramArrayOfString[b];
      try {
        byteArrayOutputStream.write(str.getBytes());
      } catch (IOException iOException) {}
      b++;
    } 
    return computeSha256Digest(byteArrayOutputStream.toByteArray());
  }
  
  public static byte[] computeSha256DigestBytes(byte[] paramArrayOfbyte) {
    try {
      MessageDigest messageDigest = MessageDigest.getInstance("SHA256");
      messageDigest.update(paramArrayOfbyte);
      return messageDigest.digest();
    } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
      return null;
    } 
  }
  
  public static String computeSha256Digest(byte[] paramArrayOfbyte) {
    paramArrayOfbyte = computeSha256DigestBytes(paramArrayOfbyte);
    if (paramArrayOfbyte == null)
      return null; 
    return HexEncoding.encodeToString(paramArrayOfbyte, true);
  }
}
