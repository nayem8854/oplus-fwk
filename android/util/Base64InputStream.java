package android.util;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Base64InputStream extends FilterInputStream {
  private static final int BUFFER_SIZE = 2048;
  
  private static byte[] EMPTY = new byte[0];
  
  private final Base64.Coder coder;
  
  private boolean eof;
  
  private byte[] inputBuffer;
  
  private int outputEnd;
  
  private int outputStart;
  
  public Base64InputStream(InputStream paramInputStream, int paramInt) {
    this(paramInputStream, paramInt, false);
  }
  
  public Base64InputStream(InputStream paramInputStream, int paramInt, boolean paramBoolean) {
    super(paramInputStream);
    this.eof = false;
    this.inputBuffer = new byte[2048];
    if (paramBoolean) {
      this.coder = new Base64.Encoder(paramInt, null);
    } else {
      this.coder = new Base64.Decoder(paramInt, null);
    } 
    Base64.Coder coder = this.coder;
    coder.output = new byte[coder.maxOutputSize(2048)];
    this.outputStart = 0;
    this.outputEnd = 0;
  }
  
  public boolean markSupported() {
    return false;
  }
  
  public void mark(int paramInt) {
    throw new UnsupportedOperationException();
  }
  
  public void reset() {
    throw new UnsupportedOperationException();
  }
  
  public void close() throws IOException {
    this.in.close();
    this.inputBuffer = null;
  }
  
  public int available() {
    return this.outputEnd - this.outputStart;
  }
  
  public long skip(long paramLong) throws IOException {
    if (this.outputStart >= this.outputEnd)
      refill(); 
    int i = this.outputStart, j = this.outputEnd;
    if (i >= j)
      return 0L; 
    paramLong = Math.min(paramLong, (j - i));
    this.outputStart = (int)(this.outputStart + paramLong);
    return paramLong;
  }
  
  public int read() throws IOException {
    if (this.outputStart >= this.outputEnd)
      refill(); 
    if (this.outputStart >= this.outputEnd)
      return -1; 
    byte[] arrayOfByte = this.coder.output;
    int i = this.outputStart;
    this.outputStart = i + 1;
    return arrayOfByte[i] & 0xFF;
  }
  
  public int read(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws IOException {
    if (this.outputStart >= this.outputEnd)
      refill(); 
    int i = this.outputStart, j = this.outputEnd;
    if (i >= j)
      return -1; 
    paramInt2 = Math.min(paramInt2, j - i);
    System.arraycopy(this.coder.output, this.outputStart, paramArrayOfbyte, paramInt1, paramInt2);
    this.outputStart += paramInt2;
    return paramInt2;
  }
  
  private void refill() throws IOException {
    boolean bool;
    if (this.eof)
      return; 
    int i = this.in.read(this.inputBuffer);
    if (i == -1) {
      this.eof = true;
      bool = this.coder.process(EMPTY, 0, 0, true);
    } else {
      bool = this.coder.process(this.inputBuffer, 0, i, false);
    } 
    if (bool) {
      this.outputEnd = this.coder.op;
      this.outputStart = 0;
      return;
    } 
    throw new Base64DataException("bad base-64");
  }
}
