package android.util;

import com.android.internal.R;

public class StateSet {
  public static final int[] NOTHING;
  
  public static final int VIEW_STATE_ACCELERATED = 64;
  
  public static final int VIEW_STATE_ACTIVATED = 32;
  
  public static final int VIEW_STATE_DRAG_CAN_ACCEPT = 256;
  
  public static final int VIEW_STATE_DRAG_HOVERED = 512;
  
  public static final int VIEW_STATE_ENABLED = 8;
  
  public static final int VIEW_STATE_FOCUSED = 4;
  
  public static final int VIEW_STATE_HOVERED = 128;
  
  static final int[] VIEW_STATE_IDS;
  
  public static final int VIEW_STATE_PRESSED = 16;
  
  public static final int VIEW_STATE_SELECTED = 2;
  
  private static final int[][] VIEW_STATE_SETS;
  
  public static final int VIEW_STATE_WINDOW_FOCUSED = 1;
  
  public static final int[] WILD_CARD;
  
  static {
    int[] arrayOfInt = new int[20];
    arrayOfInt[0] = 16842909;
    arrayOfInt[1] = 1;
    arrayOfInt[2] = 16842913;
    arrayOfInt[3] = 2;
    arrayOfInt[4] = 16842908;
    arrayOfInt[5] = 4;
    arrayOfInt[6] = 16842910;
    arrayOfInt[7] = 8;
    arrayOfInt[8] = 16842919;
    arrayOfInt[9] = 16;
    arrayOfInt[10] = 16843518;
    arrayOfInt[11] = 32;
    arrayOfInt[12] = 16843547;
    arrayOfInt[13] = 64;
    arrayOfInt[14] = 16843623;
    arrayOfInt[15] = 128;
    arrayOfInt[16] = 16843624;
    arrayOfInt[17] = 256;
    arrayOfInt[18] = 16843625;
    arrayOfInt[19] = 512;
    VIEW_STATE_IDS = arrayOfInt;
    if (arrayOfInt.length / 2 == R.styleable.ViewDrawableStates.length) {
      arrayOfInt = new int[VIEW_STATE_IDS.length];
      int i;
      for (i = 0; i < R.styleable.ViewDrawableStates.length; ) {
        int j = R.styleable.ViewDrawableStates[i];
        byte b = 0;
        for (;; i++) {
          int[] arrayOfInt1 = VIEW_STATE_IDS;
          if (b < arrayOfInt1.length) {
            if (arrayOfInt1[b] == j) {
              arrayOfInt[i * 2] = j;
              arrayOfInt[i * 2 + 1] = arrayOfInt1[b + 1];
            } 
            b += 2;
            continue;
          } 
        } 
      } 
      i = VIEW_STATE_IDS.length / 2;
      VIEW_STATE_SETS = new int[1 << i][];
      for (i = 0; i < VIEW_STATE_SETS.length; i++) {
        int k = Integer.bitCount(i);
        int[] arrayOfInt1 = new int[k];
        int j = 0;
        for (k = 0; k < arrayOfInt.length; k += 2, j = m) {
          int m = j;
          if ((arrayOfInt[k + 1] & i) != 0) {
            arrayOfInt1[j] = arrayOfInt[k];
            m = j + 1;
          } 
        } 
        VIEW_STATE_SETS[i] = arrayOfInt1;
      } 
      WILD_CARD = new int[0];
      NOTHING = new int[] { 0 };
      return;
    } 
    throw new IllegalStateException("VIEW_STATE_IDs array length does not match ViewDrawableStates style array");
  }
  
  public static int[] get(int paramInt) {
    int[][] arrayOfInt = VIEW_STATE_SETS;
    if (paramInt < arrayOfInt.length)
      return arrayOfInt[paramInt]; 
    throw new IllegalArgumentException("Invalid state set mask");
  }
  
  public static boolean isWildCard(int[] paramArrayOfint) {
    int i = paramArrayOfint.length;
    boolean bool = false;
    if (i == 0 || paramArrayOfint[0] == 0)
      bool = true; 
    return bool;
  }
  
  public static boolean stateSetMatches(int[] paramArrayOfint1, int[] paramArrayOfint2) {
    boolean bool = false;
    if (paramArrayOfint2 == null) {
      if (paramArrayOfint1 == null || isWildCard(paramArrayOfint1))
        bool = true; 
      return bool;
    } 
    int i = paramArrayOfint1.length;
    int j = paramArrayOfint2.length;
    for (byte b = 0; b < i; b++) {
      boolean bool1;
      int m, k = paramArrayOfint1[b];
      if (k == 0)
        return true; 
      if (k > 0) {
        bool1 = true;
      } else {
        bool1 = false;
        k = -k;
      } 
      byte b1 = 0;
      byte b2 = 0;
      while (true) {
        m = b1;
        if (b2 < j) {
          m = paramArrayOfint2[b2];
          if (m == 0) {
            m = b1;
            if (bool1)
              return false; 
            break;
          } 
          if (m == k) {
            if (bool1) {
              m = 1;
              break;
            } 
            return false;
          } 
          b2++;
          continue;
        } 
        break;
      } 
      if (bool1 && m == 0)
        return false; 
    } 
    return true;
  }
  
  public static boolean stateSetMatches(int[] paramArrayOfint, int paramInt) {
    int i = paramArrayOfint.length;
    for (byte b = 0; b < i; b++) {
      int j = paramArrayOfint[b];
      if (j == 0)
        return true; 
      if (j > 0) {
        if (paramInt != j)
          return false; 
      } else if (paramInt == -j) {
        return false;
      } 
    } 
    return true;
  }
  
  public static boolean containsAttribute(int[][] paramArrayOfint, int paramInt) {
    if (paramArrayOfint != null) {
      int i;
      byte b;
      for (i = paramArrayOfint.length, b = 0; b < i; ) {
        int[] arrayOfInt = paramArrayOfint[b];
        if (arrayOfInt == null)
          break; 
        int j;
        byte b1;
        for (j = arrayOfInt.length, b1 = 0; b1 < j; ) {
          int k = arrayOfInt[b1];
          if (k == paramInt || -k == paramInt)
            return true; 
          b1++;
        } 
        b++;
      } 
    } 
    return false;
  }
  
  public static int[] trimStateSet(int[] paramArrayOfint, int paramInt) {
    if (paramArrayOfint.length == paramInt)
      return paramArrayOfint; 
    int[] arrayOfInt = new int[paramInt];
    System.arraycopy(paramArrayOfint, 0, arrayOfInt, 0, paramInt);
    return arrayOfInt;
  }
  
  public static String dump(int[] paramArrayOfint) {
    StringBuilder stringBuilder = new StringBuilder();
    int i = paramArrayOfint.length;
    for (byte b = 0; b < i; b++) {
      switch (paramArrayOfint[b]) {
        case 16843518:
          stringBuilder.append("A ");
          break;
        case 16842919:
          stringBuilder.append("P ");
          break;
        case 16842913:
          stringBuilder.append("S ");
          break;
        case 16842912:
          stringBuilder.append("C ");
          break;
        case 16842910:
          stringBuilder.append("E ");
          break;
        case 16842909:
          stringBuilder.append("W ");
          break;
        case 16842908:
          stringBuilder.append("F ");
          break;
      } 
    } 
    return stringBuilder.toString();
  }
}
