package android.util;

import com.android.internal.util.ArrayUtils;
import com.android.internal.util.GrowingArrayUtils;
import libcore.util.EmptyArray;

public class SparseBooleanArray implements Cloneable {
  private int[] mKeys;
  
  private int mSize;
  
  private boolean[] mValues;
  
  public SparseBooleanArray() {
    this(10);
  }
  
  public SparseBooleanArray(int paramInt) {
    if (paramInt == 0) {
      this.mKeys = EmptyArray.INT;
      this.mValues = EmptyArray.BOOLEAN;
    } else {
      int[] arrayOfInt = ArrayUtils.newUnpaddedIntArray(paramInt);
      this.mValues = new boolean[arrayOfInt.length];
    } 
    this.mSize = 0;
  }
  
  public SparseBooleanArray clone() {
    SparseBooleanArray sparseBooleanArray = null;
    try {
      SparseBooleanArray sparseBooleanArray1 = (SparseBooleanArray)super.clone();
      sparseBooleanArray = sparseBooleanArray1;
      sparseBooleanArray1.mKeys = (int[])this.mKeys.clone();
      sparseBooleanArray = sparseBooleanArray1;
      sparseBooleanArray1.mValues = (boolean[])this.mValues.clone();
      sparseBooleanArray = sparseBooleanArray1;
    } catch (CloneNotSupportedException cloneNotSupportedException) {}
    return sparseBooleanArray;
  }
  
  public boolean get(int paramInt) {
    return get(paramInt, false);
  }
  
  public boolean get(int paramInt, boolean paramBoolean) {
    paramInt = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt);
    if (paramInt < 0)
      return paramBoolean; 
    return this.mValues[paramInt];
  }
  
  public void delete(int paramInt) {
    paramInt = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt);
    if (paramInt >= 0) {
      int[] arrayOfInt = this.mKeys;
      System.arraycopy(arrayOfInt, paramInt + 1, arrayOfInt, paramInt, this.mSize - paramInt + 1);
      boolean[] arrayOfBoolean = this.mValues;
      System.arraycopy(arrayOfBoolean, paramInt + 1, arrayOfBoolean, paramInt, this.mSize - paramInt + 1);
      this.mSize--;
    } 
  }
  
  public void removeAt(int paramInt) {
    int[] arrayOfInt = this.mKeys;
    System.arraycopy(arrayOfInt, paramInt + 1, arrayOfInt, paramInt, this.mSize - paramInt + 1);
    boolean[] arrayOfBoolean = this.mValues;
    System.arraycopy(arrayOfBoolean, paramInt + 1, arrayOfBoolean, paramInt, this.mSize - paramInt + 1);
    this.mSize--;
  }
  
  public void put(int paramInt, boolean paramBoolean) {
    int i = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt);
    if (i >= 0) {
      this.mValues[i] = paramBoolean;
    } else {
      i ^= 0xFFFFFFFF;
      this.mKeys = GrowingArrayUtils.insert(this.mKeys, this.mSize, i, paramInt);
      this.mValues = GrowingArrayUtils.insert(this.mValues, this.mSize, i, paramBoolean);
      this.mSize++;
    } 
  }
  
  public int size() {
    return this.mSize;
  }
  
  public int keyAt(int paramInt) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds)
      return this.mKeys[paramInt]; 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public boolean valueAt(int paramInt) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds)
      return this.mValues[paramInt]; 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public void setValueAt(int paramInt, boolean paramBoolean) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds) {
      this.mValues[paramInt] = paramBoolean;
      return;
    } 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public void setKeyAt(int paramInt1, int paramInt2) {
    if (paramInt1 < this.mSize) {
      this.mKeys[paramInt1] = paramInt2;
      return;
    } 
    throw new ArrayIndexOutOfBoundsException(paramInt1);
  }
  
  public int indexOfKey(int paramInt) {
    return ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt);
  }
  
  public int indexOfValue(boolean paramBoolean) {
    for (byte b = 0; b < this.mSize; b++) {
      if (this.mValues[b] == paramBoolean)
        return b; 
    } 
    return -1;
  }
  
  public void clear() {
    this.mSize = 0;
  }
  
  public void append(int paramInt, boolean paramBoolean) {
    int i = this.mSize;
    if (i != 0 && paramInt <= this.mKeys[i - 1]) {
      put(paramInt, paramBoolean);
      return;
    } 
    this.mKeys = GrowingArrayUtils.append(this.mKeys, this.mSize, paramInt);
    this.mValues = GrowingArrayUtils.append(this.mValues, this.mSize, paramBoolean);
    this.mSize++;
  }
  
  public int hashCode() {
    int i = this.mSize;
    for (byte b = 0; b < this.mSize; b++)
      i = i * 31 + this.mKeys[b] | this.mValues[b]; 
    return i;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof SparseBooleanArray))
      return false; 
    paramObject = paramObject;
    if (this.mSize != ((SparseBooleanArray)paramObject).mSize)
      return false; 
    for (byte b = 0; b < this.mSize; b++) {
      if (this.mKeys[b] != ((SparseBooleanArray)paramObject).mKeys[b])
        return false; 
      if (this.mValues[b] != ((SparseBooleanArray)paramObject).mValues[b])
        return false; 
    } 
    return true;
  }
  
  public String toString() {
    if (size() <= 0)
      return "{}"; 
    StringBuilder stringBuilder = new StringBuilder(this.mSize * 28);
    stringBuilder.append('{');
    for (byte b = 0; b < this.mSize; b++) {
      if (b > 0)
        stringBuilder.append(", "); 
      int i = keyAt(b);
      stringBuilder.append(i);
      stringBuilder.append('=');
      boolean bool = valueAt(b);
      stringBuilder.append(bool);
    } 
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
}
