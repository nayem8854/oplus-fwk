package android.util;

import com.android.internal.util.ArrayUtils;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Map;
import java.util.Set;
import libcore.util.EmptyArray;

public final class ArrayMap<K, V> implements Map<K, V> {
  private static final int BASE_SIZE = 4;
  
  private static final int CACHE_SIZE = 10;
  
  private static final boolean CONCURRENT_MODIFICATION_EXCEPTIONS = true;
  
  private static final boolean DEBUG = false;
  
  public static final ArrayMap EMPTY;
  
  static final int[] EMPTY_IMMUTABLE_INTS = new int[0];
  
  private static final String TAG = "ArrayMap";
  
  static Object[] mBaseCache;
  
  static int mBaseCacheSize;
  
  static Object[] mTwiceBaseCache;
  
  static int mTwiceBaseCacheSize;
  
  private static final Object sBaseCacheLock;
  
  private static final Object sTwiceBaseCacheLock;
  
  Object[] mArray;
  
  private MapCollections<K, V> mCollections;
  
  int[] mHashes;
  
  private final boolean mIdentityHashCode;
  
  int mSize;
  
  static {
    EMPTY = new ArrayMap(-1);
    sBaseCacheLock = new Object();
    sTwiceBaseCacheLock = new Object();
  }
  
  private static int binarySearchHashes(int[] paramArrayOfint, int paramInt1, int paramInt2) {
    try {
      return ContainerHelpers.binarySearch(paramArrayOfint, paramInt1, paramInt2);
    } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
      throw new ConcurrentModificationException();
    } 
  }
  
  int indexOf(Object paramObject, int paramInt) {
    int i = this.mSize;
    if (i == 0)
      return -1; 
    int j = binarySearchHashes(this.mHashes, i, paramInt);
    if (j < 0)
      return j; 
    if (paramObject.equals(this.mArray[j << 1]))
      return j; 
    int k;
    for (k = j + 1; k < i && this.mHashes[k] == paramInt; k++) {
      if (paramObject.equals(this.mArray[k << 1]))
        return k; 
    } 
    for (i = j - 1; i >= 0 && this.mHashes[i] == paramInt; i--) {
      if (paramObject.equals(this.mArray[i << 1]))
        return i; 
    } 
    return k ^ 0xFFFFFFFF;
  }
  
  int indexOfNull() {
    int i = this.mSize;
    if (i == 0)
      return -1; 
    int j = binarySearchHashes(this.mHashes, i, 0);
    if (j < 0)
      return j; 
    if (this.mArray[j << 1] == null)
      return j; 
    int k;
    for (k = j + 1; k < i && this.mHashes[k] == 0; k++) {
      if (this.mArray[k << 1] == null)
        return k; 
    } 
    for (; --j >= 0 && this.mHashes[j] == 0; j--) {
      if (this.mArray[j << 1] == null)
        return j; 
    } 
    return k ^ 0xFFFFFFFF;
  }
  
  private void allocArrays(int paramInt) {
    if (this.mHashes != EMPTY_IMMUTABLE_INTS) {
      if (paramInt == 8) {
        synchronized (sTwiceBaseCacheLock) {
          if (mTwiceBaseCache != null) {
            Object[] arrayOfObject = mTwiceBaseCache;
            this.mArray = arrayOfObject;
            try {
              mTwiceBaseCache = (Object[])arrayOfObject[0];
              int[] arrayOfInt = (int[])arrayOfObject[1];
              if (arrayOfInt != null) {
                arrayOfObject[1] = null;
                arrayOfObject[0] = null;
                mTwiceBaseCacheSize--;
                return;
              } 
            } catch (ClassCastException classCastException) {}
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Found corrupt ArrayMap cache: [0]=");
            stringBuilder.append(arrayOfObject[0]);
            stringBuilder.append(" [1]=");
            stringBuilder.append(arrayOfObject[1]);
            Slog.wtf("ArrayMap", stringBuilder.toString());
            mTwiceBaseCache = null;
            mTwiceBaseCacheSize = 0;
          } 
        } 
      } else if (paramInt == 4) {
        synchronized (sBaseCacheLock) {
          if (mBaseCache != null) {
            Object[] arrayOfObject = mBaseCache;
            this.mArray = arrayOfObject;
            try {
              mBaseCache = (Object[])arrayOfObject[0];
              int[] arrayOfInt = (int[])arrayOfObject[1];
              if (arrayOfInt != null) {
                arrayOfObject[1] = null;
                arrayOfObject[0] = null;
                mBaseCacheSize--;
                return;
              } 
            } catch (ClassCastException classCastException) {}
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Found corrupt ArrayMap cache: [0]=");
            stringBuilder.append(arrayOfObject[0]);
            stringBuilder.append(" [1]=");
            stringBuilder.append(arrayOfObject[1]);
            Slog.wtf("ArrayMap", stringBuilder.toString());
            mBaseCache = null;
            mBaseCacheSize = 0;
          } 
        } 
      } 
      this.mHashes = new int[paramInt];
      this.mArray = new Object[paramInt << 1];
      return;
    } 
    throw new UnsupportedOperationException("ArrayMap is immutable");
  }
  
  private static void freeArrays(int[] paramArrayOfint, Object[] paramArrayOfObject, int paramInt) {
    if (paramArrayOfint.length == 8) {
      synchronized (sTwiceBaseCacheLock) {
        if (mTwiceBaseCacheSize < 10) {
          paramArrayOfObject[0] = mTwiceBaseCache;
          paramArrayOfObject[1] = paramArrayOfint;
          for (paramInt = (paramInt << 1) - 1; paramInt >= 2; paramInt--)
            paramArrayOfObject[paramInt] = null; 
          mTwiceBaseCache = paramArrayOfObject;
          mTwiceBaseCacheSize++;
        } 
      } 
    } else if (paramArrayOfint.length == 4) {
      synchronized (sBaseCacheLock) {
        if (mBaseCacheSize < 10) {
          paramArrayOfObject[0] = mBaseCache;
          paramArrayOfObject[1] = paramArrayOfint;
          for (paramInt = (paramInt << 1) - 1; paramInt >= 2; paramInt--)
            paramArrayOfObject[paramInt] = null; 
          mBaseCache = paramArrayOfObject;
          mBaseCacheSize++;
        } 
      } 
    } 
  }
  
  public ArrayMap() {
    this(0, false);
  }
  
  public ArrayMap(int paramInt) {
    this(paramInt, false);
  }
  
  public ArrayMap(int paramInt, boolean paramBoolean) {
    this.mIdentityHashCode = paramBoolean;
    if (paramInt < 0) {
      this.mHashes = EMPTY_IMMUTABLE_INTS;
      this.mArray = EmptyArray.OBJECT;
    } else if (paramInt == 0) {
      this.mHashes = EmptyArray.INT;
      this.mArray = EmptyArray.OBJECT;
    } else {
      allocArrays(paramInt);
    } 
    this.mSize = 0;
  }
  
  public ArrayMap(ArrayMap<K, V> paramArrayMap) {
    this();
    if (paramArrayMap != null)
      putAll(paramArrayMap); 
  }
  
  public void clear() {
    if (this.mSize > 0) {
      int[] arrayOfInt = this.mHashes;
      Object[] arrayOfObject = this.mArray;
      int i = this.mSize;
      this.mHashes = EmptyArray.INT;
      this.mArray = EmptyArray.OBJECT;
      this.mSize = 0;
      freeArrays(arrayOfInt, arrayOfObject, i);
    } 
    if (this.mSize <= 0)
      return; 
    throw new ConcurrentModificationException();
  }
  
  public void erase() {
    int i = this.mSize;
    if (i > 0) {
      Object[] arrayOfObject = this.mArray;
      for (byte b = 0; b < i << 1; b++)
        arrayOfObject[b] = null; 
      this.mSize = 0;
    } 
  }
  
  public void ensureCapacity(int paramInt) {
    int i = this.mSize;
    if (this.mHashes.length < paramInt) {
      int[] arrayOfInt = this.mHashes;
      Object[] arrayOfObject = this.mArray;
      allocArrays(paramInt);
      if (this.mSize > 0) {
        System.arraycopy(arrayOfInt, 0, this.mHashes, 0, i);
        System.arraycopy(arrayOfObject, 0, this.mArray, 0, i << 1);
      } 
      freeArrays(arrayOfInt, arrayOfObject, i);
    } 
    if (this.mSize == i)
      return; 
    throw new ConcurrentModificationException();
  }
  
  public boolean containsKey(Object paramObject) {
    boolean bool;
    if (indexOfKey(paramObject) >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int indexOfKey(Object paramObject) {
    int i;
    if (paramObject == null) {
      i = indexOfNull();
    } else {
      if (this.mIdentityHashCode) {
        i = System.identityHashCode(paramObject);
      } else {
        i = paramObject.hashCode();
      } 
      i = indexOf(paramObject, i);
    } 
    return i;
  }
  
  public int indexOfValue(Object paramObject) {
    int i = this.mSize * 2;
    Object[] arrayOfObject = this.mArray;
    if (paramObject == null) {
      for (byte b = 1; b < i; b += 2) {
        if (arrayOfObject[b] == null)
          return b >> 1; 
      } 
    } else {
      for (byte b = 1; b < i; b += 2) {
        if (paramObject.equals(arrayOfObject[b]))
          return b >> 1; 
      } 
    } 
    return -1;
  }
  
  public boolean containsValue(Object paramObject) {
    boolean bool;
    if (indexOfValue(paramObject) >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public V get(Object paramObject) {
    int i = indexOfKey(paramObject);
    if (i >= 0) {
      paramObject = this.mArray[(i << 1) + 1];
    } else {
      paramObject = null;
    } 
    return (V)paramObject;
  }
  
  public K keyAt(int paramInt) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds)
      return (K)this.mArray[paramInt << 1]; 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public V valueAt(int paramInt) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds)
      return (V)this.mArray[(paramInt << 1) + 1]; 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public V setValueAt(int paramInt, V paramV) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds) {
      paramInt = (paramInt << 1) + 1;
      Object arrayOfObject[] = this.mArray, object = arrayOfObject[paramInt];
      arrayOfObject[paramInt] = paramV;
      return (V)object;
    } 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public boolean isEmpty() {
    boolean bool;
    if (this.mSize <= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public V put(K paramK, V paramV) {
    int j, k, i = this.mSize;
    if (paramK == null) {
      j = 0;
      k = indexOfNull();
    } else {
      int n;
      if (this.mIdentityHashCode) {
        n = System.identityHashCode(paramK);
      } else {
        n = paramK.hashCode();
      } 
      k = indexOf(paramK, n);
      j = n;
    } 
    if (k >= 0) {
      int n = (k << 1) + 1;
      Object[] arrayOfObject = this.mArray;
      paramK = (K)arrayOfObject[n];
      arrayOfObject[n] = paramV;
      return (V)paramK;
    } 
    k ^= 0xFFFFFFFF;
    if (i >= this.mHashes.length) {
      int n = 4;
      if (i >= 8) {
        n = (i >> 1) + i;
      } else if (i >= 4) {
        n = 8;
      } 
      int[] arrayOfInt = this.mHashes;
      Object[] arrayOfObject = this.mArray;
      allocArrays(n);
      if (i == this.mSize) {
        int[] arrayOfInt1 = this.mHashes;
        if (arrayOfInt1.length > 0) {
          System.arraycopy(arrayOfInt, 0, arrayOfInt1, 0, arrayOfInt.length);
          System.arraycopy(arrayOfObject, 0, this.mArray, 0, arrayOfObject.length);
        } 
        freeArrays(arrayOfInt, arrayOfObject, i);
      } else {
        throw new ConcurrentModificationException();
      } 
    } 
    if (k < i) {
      int[] arrayOfInt = this.mHashes;
      System.arraycopy(arrayOfInt, k, arrayOfInt, k + 1, i - k);
      Object[] arrayOfObject = this.mArray;
      System.arraycopy(arrayOfObject, k << 1, arrayOfObject, k + 1 << 1, this.mSize - k << 1);
    } 
    int m = this.mSize;
    if (i == m) {
      int[] arrayOfInt = this.mHashes;
      if (k < arrayOfInt.length) {
        arrayOfInt[k] = j;
        Object[] arrayOfObject = this.mArray;
        arrayOfObject[k << 1] = paramK;
        arrayOfObject[(k << 1) + 1] = paramV;
        this.mSize = m + 1;
        return null;
      } 
    } 
    throw new ConcurrentModificationException();
  }
  
  public void append(K paramK, V paramV) {
    int j, i = this.mSize;
    if (paramK == null) {
      j = 0;
    } else if (this.mIdentityHashCode) {
      j = System.identityHashCode(paramK);
    } else {
      j = paramK.hashCode();
    } 
    int[] arrayOfInt = this.mHashes;
    if (i < arrayOfInt.length) {
      if (i > 0 && arrayOfInt[i - 1] > j) {
        RuntimeException runtimeException = new RuntimeException("here");
        runtimeException.fillInStackTrace();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("New hash ");
        stringBuilder.append(j);
        stringBuilder.append(" is before end of array hash ");
        stringBuilder.append(this.mHashes[i - 1]);
        stringBuilder.append(" at index ");
        stringBuilder.append(i);
        stringBuilder.append(" key ");
        stringBuilder.append(paramK);
        Log.w("ArrayMap", stringBuilder.toString(), runtimeException);
        put(paramK, paramV);
        return;
      } 
      this.mSize = i + 1;
      this.mHashes[i] = j;
      j = i << 1;
      Object[] arrayOfObject = this.mArray;
      arrayOfObject[j] = paramK;
      arrayOfObject[j + 1] = paramV;
      return;
    } 
    throw new IllegalStateException("Array is full");
  }
  
  public void validate() {
    int i = this.mSize;
    if (i <= 1)
      return; 
    int j = this.mHashes[0];
    byte b1 = 0;
    for (byte b2 = 1; b2 < i; b2++, j = k, b1 = b) {
      byte b;
      int k = this.mHashes[b2];
      if (k != j) {
        b = b2;
      } else {
        Object object = this.mArray[b2 << 1];
        int m = b2 - 1;
        while (true) {
          k = j;
          b = b1;
          if (m >= b1) {
            Object object1 = this.mArray[m << 1];
            if (object != object1) {
              if (object == null || object1 == null || !object.equals(object1)) {
                m--;
                continue;
              } 
              object1 = new StringBuilder();
              object1.append("Duplicate key in ArrayMap: ");
              object1.append(object);
              throw new IllegalArgumentException(object1.toString());
            } 
            object1 = new StringBuilder();
            object1.append("Duplicate key in ArrayMap: ");
            object1.append(object);
            throw new IllegalArgumentException(object1.toString());
          } 
          break;
        } 
      } 
    } 
  }
  
  public void putAll(ArrayMap<? extends K, ? extends V> paramArrayMap) {
    int i = paramArrayMap.mSize;
    ensureCapacity(this.mSize + i);
    if (this.mSize == 0) {
      if (i > 0) {
        System.arraycopy(paramArrayMap.mHashes, 0, this.mHashes, 0, i);
        System.arraycopy(paramArrayMap.mArray, 0, this.mArray, 0, i << 1);
        this.mSize = i;
      } 
    } else {
      for (byte b = 0; b < i; b++)
        put(paramArrayMap.keyAt(b), paramArrayMap.valueAt(b)); 
    } 
  }
  
  public V remove(Object paramObject) {
    int i = indexOfKey(paramObject);
    if (i >= 0)
      return removeAt(i); 
    return null;
  }
  
  public V removeAt(int paramInt) {
    if (paramInt < this.mSize || !UtilConfig.sThrowExceptionForUpperArrayOutOfBounds) {
      Object object = this.mArray[(paramInt << 1) + 1];
      int i = this.mSize;
      if (i <= 1) {
        int[] arrayOfInt = this.mHashes;
        Object[] arrayOfObject = this.mArray;
        this.mHashes = EmptyArray.INT;
        this.mArray = EmptyArray.OBJECT;
        freeArrays(arrayOfInt, arrayOfObject, i);
        paramInt = 0;
      } else {
        int j = i - 1;
        int arrayOfInt[] = this.mHashes, k = arrayOfInt.length, m = 8;
        if (k > 8 && this.mSize < arrayOfInt.length / 3) {
          if (i > 8)
            m = i + (i >> 1); 
          int[] arrayOfInt1 = this.mHashes;
          Object[] arrayOfObject = this.mArray;
          allocArrays(m);
          if (i == this.mSize) {
            if (paramInt > 0) {
              System.arraycopy(arrayOfInt1, 0, this.mHashes, 0, paramInt);
              System.arraycopy(arrayOfObject, 0, this.mArray, 0, paramInt << 1);
            } 
            if (paramInt < j) {
              System.arraycopy(arrayOfInt1, paramInt + 1, this.mHashes, paramInt, j - paramInt);
              System.arraycopy(arrayOfObject, paramInt + 1 << 1, this.mArray, paramInt << 1, j - paramInt << 1);
            } 
          } else {
            throw new ConcurrentModificationException();
          } 
        } else {
          if (paramInt < j) {
            arrayOfInt = this.mHashes;
            System.arraycopy(arrayOfInt, paramInt + 1, arrayOfInt, paramInt, j - paramInt);
            Object[] arrayOfObject1 = this.mArray;
            System.arraycopy(arrayOfObject1, paramInt + 1 << 1, arrayOfObject1, paramInt << 1, j - paramInt << 1);
          } 
          Object[] arrayOfObject = this.mArray;
          arrayOfObject[j << 1] = null;
          arrayOfObject[(j << 1) + 1] = null;
        } 
        paramInt = j;
      } 
      if (i == this.mSize) {
        this.mSize = paramInt;
        return (V)object;
      } 
      throw new ConcurrentModificationException();
    } 
    throw new ArrayIndexOutOfBoundsException(paramInt);
  }
  
  public int size() {
    return this.mSize;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject instanceof Map) {
      paramObject = paramObject;
      if (size() != paramObject.size())
        return false; 
      byte b = 0;
      try {
        for (; b < this.mSize; b++) {
          K k = keyAt(b);
          V v = valueAt(b);
          Object object = paramObject.get(k);
          if (v == null) {
            if (object != null || !paramObject.containsKey(k))
              return false; 
          } else {
            boolean bool = v.equals(object);
            if (!bool)
              return false; 
          } 
        } 
        return true;
      } catch (NullPointerException nullPointerException) {
        return false;
      } catch (ClassCastException classCastException) {
        return false;
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int[] arrayOfInt = this.mHashes;
    Object[] arrayOfObject = this.mArray;
    int i = 0;
    byte b;
    boolean bool;
    int j;
    for (b = 0, bool = true, j = this.mSize; b < j; b++, bool += true) {
      int m;
      Object object = arrayOfObject[bool];
      int k = arrayOfInt[b];
      if (object == null) {
        m = 0;
      } else {
        m = object.hashCode();
      } 
      i += k ^ m;
    } 
    return i;
  }
  
  public String toString() {
    if (isEmpty())
      return "{}"; 
    StringBuilder stringBuilder = new StringBuilder(this.mSize * 28);
    stringBuilder.append('{');
    for (byte b = 0; b < this.mSize; b++) {
      if (b > 0)
        stringBuilder.append(", "); 
      V v = (V)keyAt(b);
      if (v != this) {
        stringBuilder.append(v);
      } else {
        stringBuilder.append("(this Map)");
      } 
      stringBuilder.append('=');
      v = valueAt(b);
      if (v != this) {
        stringBuilder.append(ArrayUtils.deepToString(v));
      } else {
        stringBuilder.append("(this Map)");
      } 
    } 
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  private MapCollections<K, V> getCollection() {
    if (this.mCollections == null)
      this.mCollections = (MapCollections<K, V>)new Object(this); 
    return this.mCollections;
  }
  
  public boolean containsAll(Collection<?> paramCollection) {
    return MapCollections.containsAllHelper(this, paramCollection);
  }
  
  public void putAll(Map<? extends K, ? extends V> paramMap) {
    ensureCapacity(this.mSize + paramMap.size());
    for (Map.Entry<? extends K, ? extends V> entry : paramMap.entrySet())
      put((K)entry.getKey(), (V)entry.getValue()); 
  }
  
  public boolean removeAll(Collection<?> paramCollection) {
    return MapCollections.removeAllHelper(this, paramCollection);
  }
  
  public boolean retainAll(Collection<?> paramCollection) {
    return MapCollections.retainAllHelper(this, paramCollection);
  }
  
  public Set<Map.Entry<K, V>> entrySet() {
    return getCollection().getEntrySet();
  }
  
  public Set<K> keySet() {
    return getCollection().getKeySet();
  }
  
  public Collection<V> values() {
    return getCollection().getValues();
  }
}
