package android.util;

public class SparseSetArray<T> {
  private final SparseArray<ArraySet<T>> mData = new SparseArray<>();
  
  public boolean add(int paramInt, T paramT) {
    ArraySet<T> arraySet1 = this.mData.get(paramInt);
    ArraySet<T> arraySet2 = arraySet1;
    if (arraySet1 == null) {
      arraySet2 = new ArraySet();
      this.mData.put(paramInt, arraySet2);
    } 
    if (arraySet2.contains(paramT))
      return false; 
    arraySet2.add(paramT);
    return true;
  }
  
  public void clear() {
    this.mData.clear();
  }
  
  public boolean contains(int paramInt, T paramT) {
    ArraySet arraySet = this.mData.get(paramInt);
    if (arraySet == null)
      return false; 
    return arraySet.contains(paramT);
  }
  
  public ArraySet<T> get(int paramInt) {
    return this.mData.get(paramInt);
  }
  
  public boolean remove(int paramInt, T paramT) {
    ArraySet arraySet = this.mData.get(paramInt);
    if (arraySet == null)
      return false; 
    boolean bool = arraySet.remove(paramT);
    if (arraySet.size() == 0)
      this.mData.remove(paramInt); 
    return bool;
  }
  
  public void remove(int paramInt) {
    this.mData.remove(paramInt);
  }
  
  public int size() {
    return this.mData.size();
  }
  
  public int keyAt(int paramInt) {
    return this.mData.keyAt(paramInt);
  }
  
  public int sizeAt(int paramInt) {
    ArraySet arraySet = this.mData.valueAt(paramInt);
    if (arraySet == null)
      return 0; 
    return arraySet.size();
  }
  
  public T valueAt(int paramInt1, int paramInt2) {
    return ((ArraySet<T>)this.mData.valueAt(paramInt1)).valueAt(paramInt2);
  }
}
