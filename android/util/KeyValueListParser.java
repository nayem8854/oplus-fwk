package android.util;

import android.text.TextUtils;
import android.util.proto.ProtoOutputStream;
import java.io.PrintWriter;
import java.time.Duration;
import java.util.Iterator;

public class KeyValueListParser {
  private final TextUtils.StringSplitter mSplitter;
  
  private final ArrayMap<String, String> mValues = new ArrayMap<>();
  
  public KeyValueListParser(char paramChar) {
    this.mSplitter = new TextUtils.SimpleStringSplitter(paramChar);
  }
  
  public void setString(String paramString) throws IllegalArgumentException {
    this.mValues.clear();
    if (paramString != null) {
      this.mSplitter.setString(paramString);
      for (Iterator<String> iterator = this.mSplitter.iterator(); iterator.hasNext(); ) {
        String str = iterator.next();
        int i = str.indexOf('=');
        if (i >= 0) {
          this.mValues.put(str.substring(0, i).trim(), str.substring(i + 1).trim());
          continue;
        } 
        this.mValues.clear();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("'");
        stringBuilder.append(str);
        stringBuilder.append("' in '");
        stringBuilder.append(paramString);
        stringBuilder.append("' is not a valid key-value pair");
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
    } 
  }
  
  public int getInt(String paramString, int paramInt) {
    paramString = this.mValues.get(paramString);
    if (paramString != null)
      try {
        return Integer.parseInt(paramString);
      } catch (NumberFormatException numberFormatException) {} 
    return paramInt;
  }
  
  public long getLong(String paramString, long paramLong) {
    paramString = this.mValues.get(paramString);
    if (paramString != null)
      try {
        return Long.parseLong(paramString);
      } catch (NumberFormatException numberFormatException) {} 
    return paramLong;
  }
  
  public float getFloat(String paramString, float paramFloat) {
    paramString = this.mValues.get(paramString);
    if (paramString != null)
      try {
        return Float.parseFloat(paramString);
      } catch (NumberFormatException numberFormatException) {} 
    return paramFloat;
  }
  
  public String getString(String paramString1, String paramString2) {
    paramString1 = this.mValues.get(paramString1);
    if (paramString1 != null)
      return paramString1; 
    return paramString2;
  }
  
  public boolean getBoolean(String paramString, boolean paramBoolean) {
    paramString = this.mValues.get(paramString);
    if (paramString != null)
      try {
        return Boolean.parseBoolean(paramString);
      } catch (NumberFormatException numberFormatException) {} 
    return paramBoolean;
  }
  
  public int[] getIntArray(String paramString, int[] paramArrayOfint) {
    paramString = this.mValues.get(paramString);
    if (paramString != null)
      try {
        String[] arrayOfString = paramString.split(":");
        if (arrayOfString.length > 0) {
          int[] arrayOfInt = new int[arrayOfString.length];
          for (byte b = 0; b < arrayOfString.length; b++)
            arrayOfInt[b] = Integer.parseInt(arrayOfString[b]); 
          return arrayOfInt;
        } 
      } catch (NumberFormatException numberFormatException) {} 
    return paramArrayOfint;
  }
  
  public int size() {
    return this.mValues.size();
  }
  
  public String keyAt(int paramInt) {
    return this.mValues.keyAt(paramInt);
  }
  
  public long getDurationMillis(String paramString, long paramLong) {
    paramString = this.mValues.get(paramString);
    if (paramString != null)
      try {
        if (paramString.startsWith("P") || paramString.startsWith("p"))
          return Duration.parse(paramString).toMillis(); 
        return Long.parseLong(paramString);
      } catch (NumberFormatException|java.time.format.DateTimeParseException numberFormatException) {} 
    return paramLong;
  }
  
  public static class IntValue {
    private final int mDefaultValue;
    
    private final String mKey;
    
    private int mValue;
    
    public IntValue(String param1String, int param1Int) {
      this.mKey = param1String;
      this.mDefaultValue = param1Int;
      this.mValue = param1Int;
    }
    
    public void parse(KeyValueListParser param1KeyValueListParser) {
      this.mValue = param1KeyValueListParser.getInt(this.mKey, this.mDefaultValue);
    }
    
    public String getKey() {
      return this.mKey;
    }
    
    public int getDefaultValue() {
      return this.mDefaultValue;
    }
    
    public int getValue() {
      return this.mValue;
    }
    
    public void setValue(int param1Int) {
      this.mValue = param1Int;
    }
    
    public void dump(PrintWriter param1PrintWriter, String param1String) {
      param1PrintWriter.print(param1String);
      param1PrintWriter.print(this.mKey);
      param1PrintWriter.print("=");
      param1PrintWriter.print(this.mValue);
      param1PrintWriter.println();
    }
    
    public void dumpProto(ProtoOutputStream param1ProtoOutputStream, long param1Long) {
      param1ProtoOutputStream.write(param1Long, this.mValue);
    }
  }
  
  public static class LongValue {
    private final long mDefaultValue;
    
    private final String mKey;
    
    private long mValue;
    
    public LongValue(String param1String, long param1Long) {
      this.mKey = param1String;
      this.mDefaultValue = param1Long;
      this.mValue = param1Long;
    }
    
    public void parse(KeyValueListParser param1KeyValueListParser) {
      this.mValue = param1KeyValueListParser.getLong(this.mKey, this.mDefaultValue);
    }
    
    public String getKey() {
      return this.mKey;
    }
    
    public long getDefaultValue() {
      return this.mDefaultValue;
    }
    
    public long getValue() {
      return this.mValue;
    }
    
    public void setValue(long param1Long) {
      this.mValue = param1Long;
    }
    
    public void dump(PrintWriter param1PrintWriter, String param1String) {
      param1PrintWriter.print(param1String);
      param1PrintWriter.print(this.mKey);
      param1PrintWriter.print("=");
      param1PrintWriter.print(this.mValue);
      param1PrintWriter.println();
    }
    
    public void dumpProto(ProtoOutputStream param1ProtoOutputStream, long param1Long) {
      param1ProtoOutputStream.write(param1Long, this.mValue);
    }
  }
  
  public static class StringValue {
    private final String mDefaultValue;
    
    private final String mKey;
    
    private String mValue;
    
    public StringValue(String param1String1, String param1String2) {
      this.mKey = param1String1;
      this.mDefaultValue = param1String2;
      this.mValue = param1String2;
    }
    
    public void parse(KeyValueListParser param1KeyValueListParser) {
      this.mValue = param1KeyValueListParser.getString(this.mKey, this.mDefaultValue);
    }
    
    public String getKey() {
      return this.mKey;
    }
    
    public String getDefaultValue() {
      return this.mDefaultValue;
    }
    
    public String getValue() {
      return this.mValue;
    }
    
    public void setValue(String param1String) {
      this.mValue = param1String;
    }
    
    public void dump(PrintWriter param1PrintWriter, String param1String) {
      param1PrintWriter.print(param1String);
      param1PrintWriter.print(this.mKey);
      param1PrintWriter.print("=");
      param1PrintWriter.print(this.mValue);
      param1PrintWriter.println();
    }
    
    public void dumpProto(ProtoOutputStream param1ProtoOutputStream, long param1Long) {
      param1ProtoOutputStream.write(param1Long, this.mValue);
    }
  }
  
  public static class FloatValue {
    private final float mDefaultValue;
    
    private final String mKey;
    
    private float mValue;
    
    public FloatValue(String param1String, float param1Float) {
      this.mKey = param1String;
      this.mDefaultValue = param1Float;
      this.mValue = param1Float;
    }
    
    public void parse(KeyValueListParser param1KeyValueListParser) {
      this.mValue = param1KeyValueListParser.getFloat(this.mKey, this.mDefaultValue);
    }
    
    public String getKey() {
      return this.mKey;
    }
    
    public float getDefaultValue() {
      return this.mDefaultValue;
    }
    
    public float getValue() {
      return this.mValue;
    }
    
    public void setValue(float param1Float) {
      this.mValue = param1Float;
    }
    
    public void dump(PrintWriter param1PrintWriter, String param1String) {
      param1PrintWriter.print(param1String);
      param1PrintWriter.print(this.mKey);
      param1PrintWriter.print("=");
      param1PrintWriter.print(this.mValue);
      param1PrintWriter.println();
    }
    
    public void dumpProto(ProtoOutputStream param1ProtoOutputStream, long param1Long) {
      param1ProtoOutputStream.write(param1Long, this.mValue);
    }
  }
}
