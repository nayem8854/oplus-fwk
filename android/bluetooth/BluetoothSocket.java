package android.bluetooth;

import android.net.LocalSocket;
import android.os.ParcelFileDescriptor;
import android.os.ParcelUuid;
import android.os.RemoteException;
import android.util.Log;
import java.io.Closeable;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Locale;
import java.util.UUID;

public final class BluetoothSocket implements Closeable {
  static final int BTSOCK_FLAG_NO_SDP = 4;
  
  private static final boolean DBG = Log.isLoggable("BluetoothSocket", 3);
  
  static final int EADDRINUSE = 98;
  
  static final int EBADFD = 77;
  
  static final int MAX_L2CAP_PACKAGE_SIZE = 65535;
  
  public static final int MAX_RFCOMM_CHANNEL = 30;
  
  private static final int PROXY_CONNECTION_TIMEOUT = 5000;
  
  static final int SEC_FLAG_AUTH = 2;
  
  static final int SEC_FLAG_AUTH_16_DIGIT = 16;
  
  static final int SEC_FLAG_AUTH_MITM = 8;
  
  static final int SEC_FLAG_ENCRYPT = 1;
  
  private static final int SOCK_SIGNAL_SIZE = 20;
  
  private static final String TAG = "BluetoothSocket";
  
  public static final int TYPE_L2CAP = 3;
  
  public static final int TYPE_L2CAP_BREDR = 3;
  
  public static final int TYPE_L2CAP_LE = 4;
  
  public static final int TYPE_RFCOMM = 1;
  
  public static final int TYPE_SCO = 2;
  
  private static final boolean VDBG = Log.isLoggable("BluetoothSocket", 2);
  
  private String mAddress;
  
  private final boolean mAuth;
  
  private boolean mAuthMitm;
  
  private BluetoothDevice mDevice;
  
  private final boolean mEncrypt;
  
  private boolean mExcludeSdp;
  
  private int mFd;
  
  private final BluetoothInputStream mInputStream;
  
  private ByteBuffer mL2capBuffer;
  
  private int mMaxRxPacketSize;
  
  private int mMaxTxPacketSize;
  
  private boolean mMin16DigitPin;
  
  private final BluetoothOutputStream mOutputStream;
  
  private ParcelFileDescriptor mPfd;
  
  private int mPort;
  
  private String mServiceName;
  
  private LocalSocket mSocket;
  
  private InputStream mSocketIS;
  
  private OutputStream mSocketOS;
  
  private volatile SocketState mSocketState;
  
  private final int mType;
  
  private final ParcelUuid mUuid;
  
  private enum SocketState {
    CLOSED, CONNECTED, INIT, LISTENING;
    
    private static final SocketState[] $VALUES;
    
    static {
      SocketState socketState = new SocketState("CLOSED", 3);
      $VALUES = new SocketState[] { INIT, CONNECTED, LISTENING, socketState };
    }
  }
  
  BluetoothSocket(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, BluetoothDevice paramBluetoothDevice, int paramInt3, ParcelUuid paramParcelUuid) throws IOException {
    this(paramInt1, paramInt2, paramBoolean1, paramBoolean2, paramBluetoothDevice, paramInt3, paramParcelUuid, false, false);
  }
  
  BluetoothSocket(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, BluetoothDevice paramBluetoothDevice, int paramInt3, ParcelUuid paramParcelUuid, boolean paramBoolean3, boolean paramBoolean4) throws IOException {
    StringBuilder stringBuilder;
    this.mExcludeSdp = false;
    this.mAuthMitm = false;
    this.mMin16DigitPin = false;
    this.mL2capBuffer = null;
    this.mMaxTxPacketSize = 0;
    this.mMaxRxPacketSize = 0;
    if (VDBG) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Creating new BluetoothSocket of type: ");
      stringBuilder1.append(paramInt1);
      Log.d("BluetoothSocket", stringBuilder1.toString());
    } 
    if (paramInt1 == 1 && paramParcelUuid == null && paramInt2 == -1 && paramInt3 != -2)
      if (paramInt3 < 1 || paramInt3 > 30) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid RFCOMM channel: ");
        stringBuilder.append(paramInt3);
        throw new IOException(stringBuilder.toString());
      }  
    if (paramParcelUuid != null) {
      this.mUuid = paramParcelUuid;
    } else {
      this.mUuid = new ParcelUuid(new UUID(0L, 0L));
    } 
    this.mType = paramInt1;
    this.mAuth = paramBoolean1;
    this.mAuthMitm = paramBoolean3;
    this.mMin16DigitPin = paramBoolean4;
    this.mEncrypt = paramBoolean2;
    this.mDevice = (BluetoothDevice)stringBuilder;
    this.mPort = paramInt3;
    this.mFd = paramInt2;
    this.mSocketState = SocketState.INIT;
    if (stringBuilder == null) {
      this.mAddress = BluetoothAdapter.getDefaultAdapter().getAddress();
    } else {
      this.mAddress = stringBuilder.getAddress();
    } 
    this.mInputStream = new BluetoothInputStream(this);
    this.mOutputStream = new BluetoothOutputStream(this);
  }
  
  private BluetoothSocket(BluetoothSocket paramBluetoothSocket) {
    this.mExcludeSdp = false;
    this.mAuthMitm = false;
    this.mMin16DigitPin = false;
    this.mL2capBuffer = null;
    this.mMaxTxPacketSize = 0;
    this.mMaxRxPacketSize = 0;
    if (VDBG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Creating new Private BluetoothSocket of type: ");
      stringBuilder.append(paramBluetoothSocket.mType);
      Log.d("BluetoothSocket", stringBuilder.toString());
    } 
    this.mUuid = paramBluetoothSocket.mUuid;
    this.mType = paramBluetoothSocket.mType;
    this.mAuth = paramBluetoothSocket.mAuth;
    this.mEncrypt = paramBluetoothSocket.mEncrypt;
    this.mPort = paramBluetoothSocket.mPort;
    this.mInputStream = new BluetoothInputStream(this);
    this.mOutputStream = new BluetoothOutputStream(this);
    this.mMaxRxPacketSize = paramBluetoothSocket.mMaxRxPacketSize;
    this.mMaxTxPacketSize = paramBluetoothSocket.mMaxTxPacketSize;
    this.mServiceName = paramBluetoothSocket.mServiceName;
    this.mExcludeSdp = paramBluetoothSocket.mExcludeSdp;
    this.mAuthMitm = paramBluetoothSocket.mAuthMitm;
    this.mMin16DigitPin = paramBluetoothSocket.mMin16DigitPin;
  }
  
  private BluetoothSocket acceptSocket(String paramString) throws IOException {
    LocalSocket localSocket;
    BluetoothSocket bluetoothSocket = new BluetoothSocket(this);
    bluetoothSocket.mSocketState = SocketState.CONNECTED;
    FileDescriptor[] arrayOfFileDescriptor = this.mSocket.getAncillaryFileDescriptors();
    if (DBG) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("socket fd passed by stack fds: ");
      stringBuilder1.append(Arrays.toString((Object[])arrayOfFileDescriptor));
      Log.d("BluetoothSocket", stringBuilder1.toString());
    } 
    if (arrayOfFileDescriptor != null && arrayOfFileDescriptor.length == 1) {
      bluetoothSocket.mPfd = new ParcelFileDescriptor(arrayOfFileDescriptor[0]);
      bluetoothSocket.mSocket = localSocket = LocalSocket.createConnectedLocalSocket(arrayOfFileDescriptor[0]);
      bluetoothSocket.mSocketIS = localSocket.getInputStream();
      bluetoothSocket.mSocketOS = bluetoothSocket.mSocket.getOutputStream();
      bluetoothSocket.mAddress = paramString;
      bluetoothSocket.mDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(paramString);
      bluetoothSocket.mPort = this.mPort;
      return bluetoothSocket;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("socket fd passed from stack failed, fds: ");
    stringBuilder.append(Arrays.toString((Object[])localSocket));
    Log.e("BluetoothSocket", stringBuilder.toString());
    bluetoothSocket.close();
    throw new IOException("bt socket acept failed");
  }
  
  private BluetoothSocket(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, String paramString, int paramInt3) throws IOException {
    this(paramInt1, paramInt2, paramBoolean1, paramBoolean2, new BluetoothDevice(paramString), paramInt3, null, false, false);
  }
  
  protected void finalize() throws Throwable {
    try {
      close();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  private int getSecurityFlags() {
    int i = 0;
    if (this.mAuth)
      i = 0x0 | 0x2; 
    int j = i;
    if (this.mEncrypt)
      j = i | 0x1; 
    i = j;
    if (this.mExcludeSdp)
      i = j | 0x4; 
    j = i;
    if (this.mAuthMitm)
      j = i | 0x8; 
    i = j;
    if (this.mMin16DigitPin)
      i = j | 0x10; 
    return i;
  }
  
  public BluetoothDevice getRemoteDevice() {
    return this.mDevice;
  }
  
  public InputStream getInputStream() throws IOException {
    return this.mInputStream;
  }
  
  public OutputStream getOutputStream() throws IOException {
    return this.mOutputStream;
  }
  
  public boolean isConnected() {
    boolean bool;
    if (this.mSocketState == SocketState.CONNECTED) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  void setServiceName(String paramString) {
    this.mServiceName = paramString;
  }
  
  public void connect() throws IOException {
    // Byte code:
    //   0: aload_0
    //   1: getfield mDevice : Landroid/bluetooth/BluetoothDevice;
    //   4: ifnull -> 392
    //   7: aload_0
    //   8: getfield mSocketState : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   11: getstatic android/bluetooth/BluetoothSocket$SocketState.CLOSED : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   14: if_acmpeq -> 325
    //   17: invokestatic getDefaultAdapter : ()Landroid/bluetooth/BluetoothAdapter;
    //   20: aconst_null
    //   21: invokevirtual getBluetoothService : (Landroid/bluetooth/IBluetoothManagerCallback;)Landroid/bluetooth/IBluetooth;
    //   24: astore_1
    //   25: aload_1
    //   26: ifnull -> 312
    //   29: aload_1
    //   30: invokeinterface getSocketManager : ()Landroid/bluetooth/IBluetoothSocketManager;
    //   35: astore_2
    //   36: aload_0
    //   37: getfield mDevice : Landroid/bluetooth/BluetoothDevice;
    //   40: astore_3
    //   41: aload_0
    //   42: getfield mType : I
    //   45: istore #4
    //   47: aload_0
    //   48: getfield mUuid : Landroid/os/ParcelUuid;
    //   51: astore_1
    //   52: aload_0
    //   53: getfield mPort : I
    //   56: istore #5
    //   58: aload_0
    //   59: invokespecial getSecurityFlags : ()I
    //   62: istore #6
    //   64: aload_0
    //   65: aload_2
    //   66: aload_3
    //   67: iload #4
    //   69: aload_1
    //   70: iload #5
    //   72: iload #6
    //   74: invokeinterface connectSocket : (Landroid/bluetooth/BluetoothDevice;ILandroid/os/ParcelUuid;II)Landroid/os/ParcelFileDescriptor;
    //   79: putfield mPfd : Landroid/os/ParcelFileDescriptor;
    //   82: aload_0
    //   83: monitorenter
    //   84: getstatic android/bluetooth/BluetoothSocket.DBG : Z
    //   87: ifeq -> 142
    //   90: new java/lang/StringBuilder
    //   93: astore_1
    //   94: aload_1
    //   95: invokespecial <init> : ()V
    //   98: aload_1
    //   99: ldc_w 'connect(), SocketState: '
    //   102: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   105: pop
    //   106: aload_1
    //   107: aload_0
    //   108: getfield mSocketState : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   111: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   114: pop
    //   115: aload_1
    //   116: ldc_w ', mPfd: '
    //   119: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   122: pop
    //   123: aload_1
    //   124: aload_0
    //   125: getfield mPfd : Landroid/os/ParcelFileDescriptor;
    //   128: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   131: pop
    //   132: ldc 'BluetoothSocket'
    //   134: aload_1
    //   135: invokevirtual toString : ()Ljava/lang/String;
    //   138: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   141: pop
    //   142: aload_0
    //   143: getfield mSocketState : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   146: getstatic android/bluetooth/BluetoothSocket$SocketState.CLOSED : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   149: if_acmpeq -> 294
    //   152: aload_0
    //   153: getfield mPfd : Landroid/os/ParcelFileDescriptor;
    //   156: ifnull -> 281
    //   159: aload_0
    //   160: getfield mPfd : Landroid/os/ParcelFileDescriptor;
    //   163: invokevirtual getFileDescriptor : ()Ljava/io/FileDescriptor;
    //   166: astore_1
    //   167: aload_1
    //   168: invokestatic createConnectedLocalSocket : (Ljava/io/FileDescriptor;)Landroid/net/LocalSocket;
    //   171: astore_1
    //   172: aload_0
    //   173: aload_1
    //   174: putfield mSocket : Landroid/net/LocalSocket;
    //   177: aload_0
    //   178: aload_1
    //   179: invokevirtual getInputStream : ()Ljava/io/InputStream;
    //   182: putfield mSocketIS : Ljava/io/InputStream;
    //   185: aload_0
    //   186: aload_0
    //   187: getfield mSocket : Landroid/net/LocalSocket;
    //   190: invokevirtual getOutputStream : ()Ljava/io/OutputStream;
    //   193: putfield mSocketOS : Ljava/io/OutputStream;
    //   196: aload_0
    //   197: monitorexit
    //   198: aload_0
    //   199: aload_0
    //   200: getfield mSocketIS : Ljava/io/InputStream;
    //   203: invokespecial readInt : (Ljava/io/InputStream;)I
    //   206: istore #4
    //   208: iload #4
    //   210: ifle -> 268
    //   213: aload_0
    //   214: iload #4
    //   216: putfield mPort : I
    //   219: aload_0
    //   220: aload_0
    //   221: getfield mSocketIS : Ljava/io/InputStream;
    //   224: invokespecial waitSocketSignal : (Ljava/io/InputStream;)Ljava/lang/String;
    //   227: pop
    //   228: aload_0
    //   229: monitorenter
    //   230: aload_0
    //   231: getfield mSocketState : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   234: getstatic android/bluetooth/BluetoothSocket$SocketState.CLOSED : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   237: if_acmpeq -> 250
    //   240: aload_0
    //   241: getstatic android/bluetooth/BluetoothSocket$SocketState.CONNECTED : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   244: putfield mSocketState : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   247: aload_0
    //   248: monitorexit
    //   249: return
    //   250: new java/io/IOException
    //   253: astore_1
    //   254: aload_1
    //   255: ldc_w 'bt socket closed'
    //   258: invokespecial <init> : (Ljava/lang/String;)V
    //   261: aload_1
    //   262: athrow
    //   263: astore_1
    //   264: aload_0
    //   265: monitorexit
    //   266: aload_1
    //   267: athrow
    //   268: new java/io/IOException
    //   271: astore_1
    //   272: aload_1
    //   273: ldc_w 'bt socket connect failed'
    //   276: invokespecial <init> : (Ljava/lang/String;)V
    //   279: aload_1
    //   280: athrow
    //   281: new java/io/IOException
    //   284: astore_1
    //   285: aload_1
    //   286: ldc_w 'bt socket connect failed'
    //   289: invokespecial <init> : (Ljava/lang/String;)V
    //   292: aload_1
    //   293: athrow
    //   294: new java/io/IOException
    //   297: astore_1
    //   298: aload_1
    //   299: ldc_w 'socket closed'
    //   302: invokespecial <init> : (Ljava/lang/String;)V
    //   305: aload_1
    //   306: athrow
    //   307: astore_1
    //   308: aload_0
    //   309: monitorexit
    //   310: aload_1
    //   311: athrow
    //   312: new java/io/IOException
    //   315: astore_1
    //   316: aload_1
    //   317: ldc_w 'Bluetooth is off'
    //   320: invokespecial <init> : (Ljava/lang/String;)V
    //   323: aload_1
    //   324: athrow
    //   325: new java/io/IOException
    //   328: astore_1
    //   329: aload_1
    //   330: ldc_w 'socket closed'
    //   333: invokespecial <init> : (Ljava/lang/String;)V
    //   336: aload_1
    //   337: athrow
    //   338: astore_3
    //   339: ldc 'BluetoothSocket'
    //   341: new java/lang/Throwable
    //   344: dup
    //   345: invokespecial <init> : ()V
    //   348: invokestatic getStackTraceString : (Ljava/lang/Throwable;)Ljava/lang/String;
    //   351: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   354: pop
    //   355: new java/lang/StringBuilder
    //   358: dup
    //   359: invokespecial <init> : ()V
    //   362: astore_1
    //   363: aload_1
    //   364: ldc_w 'unable to send RPC: '
    //   367: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   370: pop
    //   371: aload_1
    //   372: aload_3
    //   373: invokevirtual getMessage : ()Ljava/lang/String;
    //   376: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   379: pop
    //   380: new java/io/IOException
    //   383: dup
    //   384: aload_1
    //   385: invokevirtual toString : ()Ljava/lang/String;
    //   388: invokespecial <init> : (Ljava/lang/String;)V
    //   391: athrow
    //   392: new java/io/IOException
    //   395: dup
    //   396: ldc_w 'Connect is called on null device'
    //   399: invokespecial <init> : (Ljava/lang/String;)V
    //   402: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #386	-> 0
    //   #389	-> 7
    //   #391	-> 17
    //   #392	-> 25
    //   #393	-> 29
    //   #394	-> 58
    //   #393	-> 64
    //   #395	-> 82
    //   #396	-> 84
    //   #397	-> 142
    //   #398	-> 152
    //   #399	-> 159
    //   #400	-> 167
    //   #401	-> 177
    //   #402	-> 185
    //   #403	-> 196
    //   #404	-> 198
    //   #405	-> 208
    //   #408	-> 213
    //   #409	-> 219
    //   #410	-> 228
    //   #411	-> 230
    //   #414	-> 240
    //   #415	-> 247
    //   #419	-> 249
    //   #420	-> 249
    //   #412	-> 250
    //   #415	-> 263
    //   #406	-> 268
    //   #398	-> 281
    //   #397	-> 294
    //   #403	-> 307
    //   #392	-> 312
    //   #389	-> 325
    //   #416	-> 338
    //   #417	-> 339
    //   #418	-> 355
    //   #386	-> 392
    // Exception table:
    //   from	to	target	type
    //   7	17	338	android/os/RemoteException
    //   17	25	338	android/os/RemoteException
    //   29	58	338	android/os/RemoteException
    //   58	64	338	android/os/RemoteException
    //   64	82	338	android/os/RemoteException
    //   82	84	338	android/os/RemoteException
    //   84	142	307	finally
    //   142	152	307	finally
    //   152	159	307	finally
    //   159	167	307	finally
    //   167	177	307	finally
    //   177	185	307	finally
    //   185	196	307	finally
    //   196	198	307	finally
    //   198	208	338	android/os/RemoteException
    //   213	219	338	android/os/RemoteException
    //   219	228	338	android/os/RemoteException
    //   228	230	338	android/os/RemoteException
    //   230	240	263	finally
    //   240	247	263	finally
    //   247	249	263	finally
    //   250	263	263	finally
    //   264	266	263	finally
    //   266	268	338	android/os/RemoteException
    //   268	281	338	android/os/RemoteException
    //   281	294	307	finally
    //   294	307	307	finally
    //   308	310	307	finally
    //   310	312	338	android/os/RemoteException
    //   312	325	338	android/os/RemoteException
    //   325	338	338	android/os/RemoteException
  }
  
  int bindListen() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mSocketState : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   4: getstatic android/bluetooth/BluetoothSocket$SocketState.CLOSED : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   7: if_acmpne -> 13
    //   10: bipush #77
    //   12: ireturn
    //   13: invokestatic getDefaultAdapter : ()Landroid/bluetooth/BluetoothAdapter;
    //   16: aconst_null
    //   17: invokevirtual getBluetoothService : (Landroid/bluetooth/IBluetoothManagerCallback;)Landroid/bluetooth/IBluetooth;
    //   20: astore_1
    //   21: aload_1
    //   22: ifnonnull -> 36
    //   25: ldc 'BluetoothSocket'
    //   27: ldc_w 'bindListen fail, reason: bluetooth is off'
    //   30: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   33: pop
    //   34: iconst_m1
    //   35: ireturn
    //   36: getstatic android/bluetooth/BluetoothSocket.DBG : Z
    //   39: ifeq -> 94
    //   42: new java/lang/StringBuilder
    //   45: astore_2
    //   46: aload_2
    //   47: invokespecial <init> : ()V
    //   50: aload_2
    //   51: ldc_w 'bindListen(): mPort='
    //   54: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   57: pop
    //   58: aload_2
    //   59: aload_0
    //   60: getfield mPort : I
    //   63: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   66: pop
    //   67: aload_2
    //   68: ldc_w ', mType='
    //   71: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   74: pop
    //   75: aload_2
    //   76: aload_0
    //   77: getfield mType : I
    //   80: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   83: pop
    //   84: ldc 'BluetoothSocket'
    //   86: aload_2
    //   87: invokevirtual toString : ()Ljava/lang/String;
    //   90: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   93: pop
    //   94: aload_1
    //   95: invokeinterface getSocketManager : ()Landroid/bluetooth/IBluetoothSocketManager;
    //   100: astore_3
    //   101: aload_0
    //   102: getfield mType : I
    //   105: istore #4
    //   107: aload_0
    //   108: getfield mServiceName : Ljava/lang/String;
    //   111: astore_2
    //   112: aload_0
    //   113: getfield mUuid : Landroid/os/ParcelUuid;
    //   116: astore_1
    //   117: aload_0
    //   118: getfield mPort : I
    //   121: istore #5
    //   123: aload_0
    //   124: invokespecial getSecurityFlags : ()I
    //   127: istore #6
    //   129: aload_0
    //   130: aload_3
    //   131: iload #4
    //   133: aload_2
    //   134: aload_1
    //   135: iload #5
    //   137: iload #6
    //   139: invokeinterface createSocketChannel : (ILjava/lang/String;Landroid/os/ParcelUuid;II)Landroid/os/ParcelFileDescriptor;
    //   144: putfield mPfd : Landroid/os/ParcelFileDescriptor;
    //   147: aload_0
    //   148: monitorenter
    //   149: getstatic android/bluetooth/BluetoothSocket.DBG : Z
    //   152: ifeq -> 207
    //   155: new java/lang/StringBuilder
    //   158: astore_1
    //   159: aload_1
    //   160: invokespecial <init> : ()V
    //   163: aload_1
    //   164: ldc_w 'bindListen(), SocketState: '
    //   167: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   170: pop
    //   171: aload_1
    //   172: aload_0
    //   173: getfield mSocketState : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   176: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   179: pop
    //   180: aload_1
    //   181: ldc_w ', mPfd: '
    //   184: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   187: pop
    //   188: aload_1
    //   189: aload_0
    //   190: getfield mPfd : Landroid/os/ParcelFileDescriptor;
    //   193: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   196: pop
    //   197: ldc 'BluetoothSocket'
    //   199: aload_1
    //   200: invokevirtual toString : ()Ljava/lang/String;
    //   203: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   206: pop
    //   207: aload_0
    //   208: getfield mSocketState : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   211: getstatic android/bluetooth/BluetoothSocket$SocketState.INIT : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   214: if_acmpeq -> 222
    //   217: aload_0
    //   218: monitorexit
    //   219: bipush #77
    //   221: ireturn
    //   222: aload_0
    //   223: getfield mPfd : Landroid/os/ParcelFileDescriptor;
    //   226: ifnonnull -> 233
    //   229: aload_0
    //   230: monitorexit
    //   231: iconst_m1
    //   232: ireturn
    //   233: aload_0
    //   234: getfield mPfd : Landroid/os/ParcelFileDescriptor;
    //   237: invokevirtual getFileDescriptor : ()Ljava/io/FileDescriptor;
    //   240: astore_1
    //   241: aload_1
    //   242: ifnonnull -> 258
    //   245: ldc 'BluetoothSocket'
    //   247: ldc_w 'bindListen(), null file descriptor'
    //   250: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   253: pop
    //   254: aload_0
    //   255: monitorexit
    //   256: iconst_m1
    //   257: ireturn
    //   258: getstatic android/bluetooth/BluetoothSocket.DBG : Z
    //   261: ifeq -> 273
    //   264: ldc 'BluetoothSocket'
    //   266: ldc_w 'bindListen(), Create LocalSocket'
    //   269: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   272: pop
    //   273: aload_0
    //   274: aload_1
    //   275: invokestatic createConnectedLocalSocket : (Ljava/io/FileDescriptor;)Landroid/net/LocalSocket;
    //   278: putfield mSocket : Landroid/net/LocalSocket;
    //   281: getstatic android/bluetooth/BluetoothSocket.DBG : Z
    //   284: ifeq -> 296
    //   287: ldc 'BluetoothSocket'
    //   289: ldc_w 'bindListen(), new LocalSocket.getInputStream()'
    //   292: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   295: pop
    //   296: aload_0
    //   297: aload_0
    //   298: getfield mSocket : Landroid/net/LocalSocket;
    //   301: invokevirtual getInputStream : ()Ljava/io/InputStream;
    //   304: putfield mSocketIS : Ljava/io/InputStream;
    //   307: aload_0
    //   308: aload_0
    //   309: getfield mSocket : Landroid/net/LocalSocket;
    //   312: invokevirtual getOutputStream : ()Ljava/io/OutputStream;
    //   315: putfield mSocketOS : Ljava/io/OutputStream;
    //   318: aload_0
    //   319: monitorexit
    //   320: getstatic android/bluetooth/BluetoothSocket.DBG : Z
    //   323: ifeq -> 361
    //   326: new java/lang/StringBuilder
    //   329: astore_1
    //   330: aload_1
    //   331: invokespecial <init> : ()V
    //   334: aload_1
    //   335: ldc_w 'bindListen(), readInt mSocketIS: '
    //   338: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   341: pop
    //   342: aload_1
    //   343: aload_0
    //   344: getfield mSocketIS : Ljava/io/InputStream;
    //   347: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   350: pop
    //   351: ldc 'BluetoothSocket'
    //   353: aload_1
    //   354: invokevirtual toString : ()Ljava/lang/String;
    //   357: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   360: pop
    //   361: aload_0
    //   362: aload_0
    //   363: getfield mSocketIS : Ljava/io/InputStream;
    //   366: invokespecial readInt : (Ljava/io/InputStream;)I
    //   369: istore #5
    //   371: aload_0
    //   372: monitorenter
    //   373: aload_0
    //   374: getfield mSocketState : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   377: getstatic android/bluetooth/BluetoothSocket$SocketState.INIT : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   380: if_acmpne -> 390
    //   383: aload_0
    //   384: getstatic android/bluetooth/BluetoothSocket$SocketState.LISTENING : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   387: putfield mSocketState : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   390: aload_0
    //   391: monitorexit
    //   392: getstatic android/bluetooth/BluetoothSocket.DBG : Z
    //   395: ifeq -> 448
    //   398: new java/lang/StringBuilder
    //   401: astore_1
    //   402: aload_1
    //   403: invokespecial <init> : ()V
    //   406: aload_1
    //   407: ldc_w 'bindListen(): channel='
    //   410: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   413: pop
    //   414: aload_1
    //   415: iload #5
    //   417: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   420: pop
    //   421: aload_1
    //   422: ldc_w ', mPort='
    //   425: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   428: pop
    //   429: aload_1
    //   430: aload_0
    //   431: getfield mPort : I
    //   434: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   437: pop
    //   438: ldc 'BluetoothSocket'
    //   440: aload_1
    //   441: invokevirtual toString : ()Ljava/lang/String;
    //   444: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   447: pop
    //   448: aload_0
    //   449: getfield mPort : I
    //   452: iconst_m1
    //   453: if_icmpgt -> 462
    //   456: aload_0
    //   457: iload #5
    //   459: putfield mPort : I
    //   462: iconst_0
    //   463: ireturn
    //   464: astore_1
    //   465: aload_0
    //   466: monitorexit
    //   467: aload_1
    //   468: athrow
    //   469: astore_1
    //   470: aload_0
    //   471: monitorexit
    //   472: aload_1
    //   473: athrow
    //   474: astore_1
    //   475: aload_0
    //   476: getfield mPfd : Landroid/os/ParcelFileDescriptor;
    //   479: astore_2
    //   480: aload_2
    //   481: ifnull -> 529
    //   484: aload_2
    //   485: invokevirtual close : ()V
    //   488: goto -> 524
    //   491: astore_2
    //   492: new java/lang/StringBuilder
    //   495: dup
    //   496: invokespecial <init> : ()V
    //   499: astore_3
    //   500: aload_3
    //   501: ldc_w 'bindListen, close mPfd: '
    //   504: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   507: pop
    //   508: aload_3
    //   509: aload_2
    //   510: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   513: pop
    //   514: ldc 'BluetoothSocket'
    //   516: aload_3
    //   517: invokevirtual toString : ()Ljava/lang/String;
    //   520: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   523: pop
    //   524: aload_0
    //   525: aconst_null
    //   526: putfield mPfd : Landroid/os/ParcelFileDescriptor;
    //   529: new java/lang/StringBuilder
    //   532: dup
    //   533: invokespecial <init> : ()V
    //   536: astore_2
    //   537: aload_2
    //   538: ldc_w 'bindListen, fail to get port number, exception: '
    //   541: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   544: pop
    //   545: aload_2
    //   546: aload_1
    //   547: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   550: pop
    //   551: ldc 'BluetoothSocket'
    //   553: aload_2
    //   554: invokevirtual toString : ()Ljava/lang/String;
    //   557: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   560: pop
    //   561: iconst_m1
    //   562: ireturn
    //   563: astore_1
    //   564: ldc 'BluetoothSocket'
    //   566: new java/lang/Throwable
    //   569: dup
    //   570: invokespecial <init> : ()V
    //   573: invokestatic getStackTraceString : (Ljava/lang/Throwable;)Ljava/lang/String;
    //   576: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   579: pop
    //   580: iconst_m1
    //   581: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #428	-> 0
    //   #429	-> 13
    //   #430	-> 21
    //   #431	-> 25
    //   #432	-> 34
    //   #435	-> 36
    //   #436	-> 94
    //   #437	-> 123
    //   #436	-> 129
    //   #441	-> 147
    //   #445	-> 147
    //   #446	-> 149
    //   #447	-> 155
    //   #449	-> 207
    //   #450	-> 222
    //   #451	-> 233
    //   #452	-> 241
    //   #453	-> 245
    //   #454	-> 254
    //   #457	-> 258
    //   #458	-> 273
    //   #459	-> 281
    //   #460	-> 296
    //   #461	-> 307
    //   #462	-> 318
    //   #463	-> 320
    //   #464	-> 361
    //   #465	-> 371
    //   #466	-> 373
    //   #467	-> 383
    //   #469	-> 390
    //   #470	-> 392
    //   #471	-> 448
    //   #472	-> 456
    //   #474	-> 462
    //   #486	-> 462
    //   #487	-> 462
    //   #469	-> 464
    //   #462	-> 469
    //   #475	-> 474
    //   #476	-> 475
    //   #478	-> 484
    //   #481	-> 488
    //   #479	-> 491
    //   #480	-> 492
    //   #482	-> 524
    //   #484	-> 529
    //   #485	-> 561
    //   #438	-> 563
    //   #439	-> 564
    //   #440	-> 580
    // Exception table:
    //   from	to	target	type
    //   36	94	563	android/os/RemoteException
    //   94	123	563	android/os/RemoteException
    //   123	129	563	android/os/RemoteException
    //   129	147	563	android/os/RemoteException
    //   147	149	474	java/io/IOException
    //   149	155	469	finally
    //   155	207	469	finally
    //   207	219	469	finally
    //   222	231	469	finally
    //   233	241	469	finally
    //   245	254	469	finally
    //   254	256	469	finally
    //   258	273	469	finally
    //   273	281	469	finally
    //   281	296	469	finally
    //   296	307	469	finally
    //   307	318	469	finally
    //   318	320	469	finally
    //   320	361	474	java/io/IOException
    //   361	371	474	java/io/IOException
    //   371	373	474	java/io/IOException
    //   373	383	464	finally
    //   383	390	464	finally
    //   390	392	464	finally
    //   392	448	474	java/io/IOException
    //   448	456	474	java/io/IOException
    //   456	462	474	java/io/IOException
    //   465	467	464	finally
    //   467	469	474	java/io/IOException
    //   470	472	469	finally
    //   472	474	474	java/io/IOException
    //   484	488	491	java/io/IOException
  }
  
  BluetoothSocket accept(int paramInt) throws IOException {
    // Byte code:
    //   0: aload_0
    //   1: getfield mSocketState : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   4: getstatic android/bluetooth/BluetoothSocket$SocketState.LISTENING : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   7: if_acmpne -> 115
    //   10: iload_1
    //   11: ifle -> 54
    //   14: new java/lang/StringBuilder
    //   17: dup
    //   18: invokespecial <init> : ()V
    //   21: astore_2
    //   22: aload_2
    //   23: ldc_w 'accept() set timeout (ms):'
    //   26: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   29: pop
    //   30: aload_2
    //   31: iload_1
    //   32: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   35: pop
    //   36: ldc 'BluetoothSocket'
    //   38: aload_2
    //   39: invokevirtual toString : ()Ljava/lang/String;
    //   42: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   45: pop
    //   46: aload_0
    //   47: getfield mSocket : Landroid/net/LocalSocket;
    //   50: iload_1
    //   51: invokevirtual setSoTimeout : (I)V
    //   54: aload_0
    //   55: aload_0
    //   56: getfield mSocketIS : Ljava/io/InputStream;
    //   59: invokespecial waitSocketSignal : (Ljava/io/InputStream;)Ljava/lang/String;
    //   62: astore_2
    //   63: iload_1
    //   64: ifle -> 75
    //   67: aload_0
    //   68: getfield mSocket : Landroid/net/LocalSocket;
    //   71: iconst_0
    //   72: invokevirtual setSoTimeout : (I)V
    //   75: aload_0
    //   76: monitorenter
    //   77: aload_0
    //   78: getfield mSocketState : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   81: getstatic android/bluetooth/BluetoothSocket$SocketState.LISTENING : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   84: if_acmpne -> 97
    //   87: aload_0
    //   88: aload_2
    //   89: invokespecial acceptSocket : (Ljava/lang/String;)Landroid/bluetooth/BluetoothSocket;
    //   92: astore_2
    //   93: aload_0
    //   94: monitorexit
    //   95: aload_2
    //   96: areturn
    //   97: new java/io/IOException
    //   100: astore_2
    //   101: aload_2
    //   102: ldc_w 'bt socket is not in listen state'
    //   105: invokespecial <init> : (Ljava/lang/String;)V
    //   108: aload_2
    //   109: athrow
    //   110: astore_2
    //   111: aload_0
    //   112: monitorexit
    //   113: aload_2
    //   114: athrow
    //   115: new java/io/IOException
    //   118: dup
    //   119: ldc_w 'bt socket is not in listen state'
    //   122: invokespecial <init> : (Ljava/lang/String;)V
    //   125: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #492	-> 0
    //   #495	-> 10
    //   #496	-> 14
    //   #497	-> 46
    //   #499	-> 54
    //   #500	-> 63
    //   #501	-> 67
    //   #503	-> 75
    //   #504	-> 77
    //   #507	-> 87
    //   #509	-> 93
    //   #510	-> 95
    //   #505	-> 97
    //   #509	-> 110
    //   #493	-> 115
    // Exception table:
    //   from	to	target	type
    //   77	87	110	finally
    //   87	93	110	finally
    //   93	95	110	finally
    //   97	110	110	finally
    //   111	113	110	finally
  }
  
  int available() throws IOException {
    if (VDBG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("available: ");
      stringBuilder.append(this.mSocketIS);
      Log.d("BluetoothSocket", stringBuilder.toString());
    } 
    return this.mSocketIS.available();
  }
  
  int read(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws IOException {
    if (VDBG) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("read in:  ");
      stringBuilder1.append(this.mSocketIS);
      stringBuilder1.append(" len: ");
      stringBuilder1.append(paramInt2);
      Log.d("BluetoothSocket", stringBuilder1.toString());
    } 
    int i = this.mType;
    if (i == 3 || i == 4) {
      i = paramInt2;
      if (VDBG) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("l2cap: read(): offset: ");
        stringBuilder1.append(paramInt1);
        stringBuilder1.append(" length:");
        stringBuilder1.append(paramInt2);
        stringBuilder1.append("mL2capBuffer= ");
        stringBuilder1.append(this.mL2capBuffer);
        Log.v("BluetoothSocket", stringBuilder1.toString());
      } 
      if (this.mL2capBuffer == null)
        createL2capRxBuffer(); 
      if (this.mL2capBuffer.remaining() == 0) {
        if (VDBG)
          Log.v("BluetoothSocket", "l2cap buffer empty, refilling..."); 
        if (fillL2capRxBuffer() == -1)
          return -1; 
      } 
      paramInt2 = i;
      if (i > this.mL2capBuffer.remaining())
        paramInt2 = this.mL2capBuffer.remaining(); 
      if (VDBG) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("get(): offset: ");
        stringBuilder1.append(paramInt1);
        stringBuilder1.append(" bytesToRead: ");
        stringBuilder1.append(paramInt2);
        Log.v("BluetoothSocket", stringBuilder1.toString());
      } 
      this.mL2capBuffer.get(paramArrayOfbyte, paramInt1, paramInt2);
    } else {
      if (VDBG) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("default: read(): offset: ");
        stringBuilder1.append(paramInt1);
        stringBuilder1.append(" length:");
        stringBuilder1.append(paramInt2);
        Log.v("BluetoothSocket", stringBuilder1.toString());
      } 
      paramInt2 = this.mSocketIS.read(paramArrayOfbyte, paramInt1, paramInt2);
    } 
    if (paramInt2 >= 0) {
      if (VDBG) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("read out:  ");
        stringBuilder1.append(this.mSocketIS);
        stringBuilder1.append(" ret: ");
        stringBuilder1.append(paramInt2);
        Log.d("BluetoothSocket", stringBuilder1.toString());
      } 
      return paramInt2;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("bt socket closed, read return: ");
    stringBuilder.append(paramInt2);
    throw new IOException(stringBuilder.toString());
  }
  
  int write(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws IOException {
    if (VDBG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("write: ");
      stringBuilder.append(this.mSocketOS);
      stringBuilder.append(" length: ");
      stringBuilder.append(paramInt2);
      Log.d("BluetoothSocket", stringBuilder.toString());
    } 
    int i = this.mType;
    if (i == 3 || i == 4) {
      if (paramInt2 <= this.mMaxTxPacketSize) {
        this.mSocketOS.write(paramArrayOfbyte, paramInt1, paramInt2);
      } else {
        if (DBG) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("WARNING: Write buffer larger than L2CAP packet size!\nPacket will be divided into SDU packets of size ");
          stringBuilder.append(this.mMaxTxPacketSize);
          Log.w("BluetoothSocket", stringBuilder.toString());
        } 
        i = paramInt1;
        paramInt1 = paramInt2;
        while (paramInt1 > 0) {
          int j = this.mMaxTxPacketSize;
          if (paramInt1 <= j)
            j = paramInt1; 
          this.mSocketOS.write(paramArrayOfbyte, i, j);
          i += j;
          paramInt1 -= j;
        } 
      } 
    } else {
      this.mSocketOS.write(paramArrayOfbyte, paramInt1, paramInt2);
    } 
    if (VDBG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("write out: ");
      stringBuilder.append(this.mSocketOS);
      stringBuilder.append(" length: ");
      stringBuilder.append(paramInt2);
      Log.d("BluetoothSocket", stringBuilder.toString());
    } 
    return paramInt2;
  }
  
  public void close() throws IOException {
    // Byte code:
    //   0: new java/lang/StringBuilder
    //   3: dup
    //   4: invokespecial <init> : ()V
    //   7: astore_1
    //   8: aload_1
    //   9: ldc_w 'close() this: '
    //   12: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   15: pop
    //   16: aload_1
    //   17: aload_0
    //   18: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   21: pop
    //   22: aload_1
    //   23: ldc_w ', channel: '
    //   26: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   29: pop
    //   30: aload_1
    //   31: aload_0
    //   32: getfield mPort : I
    //   35: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   38: pop
    //   39: aload_1
    //   40: ldc_w ', mSocketIS: '
    //   43: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   46: pop
    //   47: aload_1
    //   48: aload_0
    //   49: getfield mSocketIS : Ljava/io/InputStream;
    //   52: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   55: pop
    //   56: aload_1
    //   57: ldc_w ', mSocketOS: '
    //   60: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   63: pop
    //   64: aload_1
    //   65: aload_0
    //   66: getfield mSocketOS : Ljava/io/OutputStream;
    //   69: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   72: pop
    //   73: aload_1
    //   74: ldc_w 'mSocket: '
    //   77: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   80: pop
    //   81: aload_1
    //   82: aload_0
    //   83: getfield mSocket : Landroid/net/LocalSocket;
    //   86: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   89: pop
    //   90: aload_1
    //   91: ldc_w ', mSocketState: '
    //   94: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   97: pop
    //   98: aload_1
    //   99: aload_0
    //   100: getfield mSocketState : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   103: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   106: pop
    //   107: ldc 'BluetoothSocket'
    //   109: aload_1
    //   110: invokevirtual toString : ()Ljava/lang/String;
    //   113: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   116: pop
    //   117: aload_0
    //   118: getfield mSocketState : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   121: getstatic android/bluetooth/BluetoothSocket$SocketState.CLOSED : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   124: if_acmpne -> 128
    //   127: return
    //   128: aload_0
    //   129: monitorenter
    //   130: aload_0
    //   131: getfield mSocketState : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   134: getstatic android/bluetooth/BluetoothSocket$SocketState.CLOSED : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   137: if_acmpne -> 143
    //   140: aload_0
    //   141: monitorexit
    //   142: return
    //   143: aload_0
    //   144: getstatic android/bluetooth/BluetoothSocket$SocketState.CLOSED : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   147: putfield mSocketState : Landroid/bluetooth/BluetoothSocket$SocketState;
    //   150: aload_0
    //   151: getfield mSocket : Landroid/net/LocalSocket;
    //   154: ifnull -> 224
    //   157: getstatic android/bluetooth/BluetoothSocket.DBG : Z
    //   160: ifeq -> 198
    //   163: new java/lang/StringBuilder
    //   166: astore_1
    //   167: aload_1
    //   168: invokespecial <init> : ()V
    //   171: aload_1
    //   172: ldc_w 'Closing mSocket: '
    //   175: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   178: pop
    //   179: aload_1
    //   180: aload_0
    //   181: getfield mSocket : Landroid/net/LocalSocket;
    //   184: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   187: pop
    //   188: ldc 'BluetoothSocket'
    //   190: aload_1
    //   191: invokevirtual toString : ()Ljava/lang/String;
    //   194: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   197: pop
    //   198: aload_0
    //   199: getfield mSocket : Landroid/net/LocalSocket;
    //   202: invokevirtual shutdownInput : ()V
    //   205: aload_0
    //   206: getfield mSocket : Landroid/net/LocalSocket;
    //   209: invokevirtual shutdownOutput : ()V
    //   212: aload_0
    //   213: getfield mSocket : Landroid/net/LocalSocket;
    //   216: invokevirtual close : ()V
    //   219: aload_0
    //   220: aconst_null
    //   221: putfield mSocket : Landroid/net/LocalSocket;
    //   224: aload_0
    //   225: getfield mPfd : Landroid/os/ParcelFileDescriptor;
    //   228: ifnull -> 243
    //   231: aload_0
    //   232: getfield mPfd : Landroid/os/ParcelFileDescriptor;
    //   235: invokevirtual close : ()V
    //   238: aload_0
    //   239: aconst_null
    //   240: putfield mPfd : Landroid/os/ParcelFileDescriptor;
    //   243: aload_0
    //   244: monitorexit
    //   245: return
    //   246: astore_1
    //   247: aload_0
    //   248: monitorexit
    //   249: aload_1
    //   250: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #594	-> 0
    //   #597	-> 117
    //   #598	-> 127
    //   #600	-> 128
    //   #601	-> 130
    //   #602	-> 140
    //   #604	-> 143
    //   #605	-> 150
    //   #606	-> 157
    //   #607	-> 198
    //   #608	-> 205
    //   #609	-> 212
    //   #610	-> 219
    //   #612	-> 224
    //   #613	-> 231
    //   #614	-> 238
    //   #616	-> 243
    //   #618	-> 245
    //   #616	-> 246
    // Exception table:
    //   from	to	target	type
    //   130	140	246	finally
    //   140	142	246	finally
    //   143	150	246	finally
    //   150	157	246	finally
    //   157	198	246	finally
    //   198	205	246	finally
    //   205	212	246	finally
    //   212	219	246	finally
    //   219	224	246	finally
    //   224	231	246	finally
    //   231	238	246	finally
    //   238	243	246	finally
    //   243	245	246	finally
    //   247	249	246	finally
  }
  
  void removeChannel() {}
  
  int getPort() {
    return this.mPort;
  }
  
  public int getMaxTransmitPacketSize() {
    return this.mMaxTxPacketSize;
  }
  
  public int getMaxReceivePacketSize() {
    return this.mMaxRxPacketSize;
  }
  
  public int getConnectionType() {
    int i = this.mType;
    if (i == 4)
      return 3; 
    return i;
  }
  
  public void setExcludeSdp(boolean paramBoolean) {
    this.mExcludeSdp = paramBoolean;
  }
  
  public void requestMaximumTxDataLength() throws IOException {
    if (this.mDevice != null)
      try {
        if (this.mSocketState != SocketState.CLOSED) {
          IBluetooth iBluetooth = BluetoothAdapter.getDefaultAdapter().getBluetoothService(null);
          if (iBluetooth != null) {
            if (DBG)
              Log.d("BluetoothSocket", "requestMaximumTxDataLength"); 
            iBluetooth.getSocketManager().requestMaximumTxDataLength(this.mDevice);
            return;
          } 
          IOException iOException1 = new IOException();
          this("Bluetooth is off");
          throw iOException1;
        } 
        IOException iOException = new IOException();
        this("socket closed");
        throw iOException;
      } catch (RemoteException remoteException) {
        Log.e("BluetoothSocket", Log.getStackTraceString(new Throwable()));
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("unable to send RPC: ");
        stringBuilder.append(remoteException.getMessage());
        throw new IOException(stringBuilder.toString());
      }  
    throw new IOException("requestMaximumTxDataLength is called on null device");
  }
  
  private String convertAddr(byte[] paramArrayOfbyte) {
    Locale locale = Locale.US;
    byte b1 = paramArrayOfbyte[0];
    byte b2 = paramArrayOfbyte[1], b3 = paramArrayOfbyte[2], b4 = paramArrayOfbyte[3], b5 = paramArrayOfbyte[4], b6 = paramArrayOfbyte[5];
    return String.format(locale, "%02X:%02X:%02X:%02X:%02X:%02X", new Object[] { Byte.valueOf(b1), Byte.valueOf(b2), Byte.valueOf(b3), Byte.valueOf(b4), Byte.valueOf(b5), Byte.valueOf(b6) });
  }
  
  private String waitSocketSignal(InputStream paramInputStream) throws IOException {
    byte[] arrayOfByte = new byte[20];
    int i = readAll(paramInputStream, arrayOfByte);
    if (VDBG) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("waitSocketSignal read 20 bytes signal ret: ");
      stringBuilder1.append(i);
      Log.d("BluetoothSocket", stringBuilder1.toString());
    } 
    ByteBuffer byteBuffer = ByteBuffer.wrap(arrayOfByte);
    byteBuffer.order(ByteOrder.nativeOrder());
    short s = byteBuffer.getShort();
    if (s == 20) {
      byte[] arrayOfByte1 = new byte[6];
      byteBuffer.get(arrayOfByte1);
      i = byteBuffer.getInt();
      int j = byteBuffer.getInt();
      this.mMaxTxPacketSize = byteBuffer.getShort() & 0xFFFF;
      this.mMaxRxPacketSize = byteBuffer.getShort() & 0xFFFF;
      String str = convertAddr(arrayOfByte1);
      if (VDBG) {
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("waitSocketSignal: sig size: ");
        stringBuilder2.append(s);
        stringBuilder2.append(", remote addr: ");
        stringBuilder2.append(str);
        stringBuilder2.append(", channel: ");
        stringBuilder2.append(i);
        stringBuilder2.append(", status: ");
        stringBuilder2.append(j);
        stringBuilder2.append(" MaxRxPktSize: ");
        stringBuilder2.append(this.mMaxRxPacketSize);
        stringBuilder2.append(" MaxTxPktSize: ");
        stringBuilder2.append(this.mMaxTxPacketSize);
        Log.d("BluetoothSocket", stringBuilder2.toString());
      } 
      if (j == 0)
        return str; 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Connection failure, status: ");
      stringBuilder1.append(j);
      throw new IOException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Connection failure, wrong signal size: ");
    stringBuilder.append(s);
    throw new IOException(stringBuilder.toString());
  }
  
  private void createL2capRxBuffer() {
    int i = this.mType;
    if (i == 3 || i == 4) {
      if (VDBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("  Creating mL2capBuffer: mMaxPacketSize: ");
        stringBuilder.append(this.mMaxRxPacketSize);
        Log.v("BluetoothSocket", stringBuilder.toString());
      } 
      this.mL2capBuffer = ByteBuffer.wrap(new byte[this.mMaxRxPacketSize]);
      if (VDBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("mL2capBuffer.remaining()");
        stringBuilder.append(this.mL2capBuffer.remaining());
        Log.v("BluetoothSocket", stringBuilder.toString());
      } 
      this.mL2capBuffer.limit(0);
      if (VDBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("mL2capBuffer.remaining() after limit(0):");
        stringBuilder.append(this.mL2capBuffer.remaining());
        Log.v("BluetoothSocket", stringBuilder.toString());
      } 
    } 
  }
  
  private int readAll(InputStream paramInputStream, byte[] paramArrayOfbyte) throws IOException {
    int i = paramArrayOfbyte.length;
    while (i > 0) {
      int j = paramInputStream.read(paramArrayOfbyte, paramArrayOfbyte.length - i, i);
      if (j > 0) {
        i -= j;
        if (i != 0) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("readAll() looping, read partial size: ");
          stringBuilder1.append(paramArrayOfbyte.length - i);
          stringBuilder1.append(", expect size: ");
          stringBuilder1.append(paramArrayOfbyte.length);
          Log.w("BluetoothSocket", stringBuilder1.toString());
        } 
        continue;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("read failed, socket might closed or timeout, read ret: ");
      stringBuilder.append(j);
      throw new IOException(stringBuilder.toString());
    } 
    return paramArrayOfbyte.length;
  }
  
  private int readInt(InputStream paramInputStream) throws IOException {
    byte[] arrayOfByte = new byte[4];
    int i = readAll(paramInputStream, arrayOfByte);
    if (VDBG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("inputStream.read ret: ");
      stringBuilder.append(i);
      Log.d("BluetoothSocket", stringBuilder.toString());
    } 
    ByteBuffer byteBuffer = ByteBuffer.wrap(arrayOfByte);
    byteBuffer.order(ByteOrder.nativeOrder());
    return byteBuffer.getInt();
  }
  
  private int fillL2capRxBuffer() throws IOException {
    this.mL2capBuffer.rewind();
    int i = this.mSocketIS.read(this.mL2capBuffer.array());
    if (i == -1) {
      this.mL2capBuffer.limit(0);
      return -1;
    } 
    this.mL2capBuffer.limit(i);
    return i;
  }
  
  public int setSocketOpt(int paramInt1, byte[] paramArrayOfbyte, int paramInt2) throws IOException {
    if (this.mSocketState != SocketState.CLOSED) {
      IBluetooth iBluetooth = BluetoothAdapter.getDefaultAdapter().getBluetoothService(null);
      if (iBluetooth == null) {
        Log.e("BluetoothSocket", "setSocketOpt fail, reason: bluetooth is off");
        return -1;
      } 
      try {
        if (VDBG) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("setSocketOpt(), mType: ");
          stringBuilder.append(this.mType);
          stringBuilder.append(" mPort: ");
          stringBuilder.append(this.mPort);
          Log.d("BluetoothSocket", stringBuilder.toString());
        } 
        paramInt1 = iBluetooth.setSocketOpt(this.mType, this.mPort, paramInt1, paramArrayOfbyte, paramInt2);
        return paramInt1;
      } catch (RemoteException remoteException) {
        Log.e("BluetoothSocket", Log.getStackTraceString(new Throwable()));
        return -1;
      } 
    } 
    throw new IOException("socket closed");
  }
  
  public int getSocketOpt(int paramInt, byte[] paramArrayOfbyte) throws IOException {
    if (this.mSocketState != SocketState.CLOSED) {
      IBluetooth iBluetooth = BluetoothAdapter.getDefaultAdapter().getBluetoothService(null);
      if (iBluetooth == null) {
        Log.e("BluetoothSocket", "getSocketOpt fail, reason: bluetooth is off");
        return -1;
      } 
      try {
        if (VDBG) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("getSocketOpt(), mType: ");
          stringBuilder.append(this.mType);
          stringBuilder.append(" mPort: ");
          stringBuilder.append(this.mPort);
          Log.d("BluetoothSocket", stringBuilder.toString());
        } 
        paramInt = iBluetooth.getSocketOpt(this.mType, this.mPort, paramInt, paramArrayOfbyte);
        return paramInt;
      } catch (RemoteException remoteException) {
        Log.e("BluetoothSocket", Log.getStackTraceString(new Throwable()));
        return -1;
      } 
    } 
    throw new IOException("socket closed");
  }
}
