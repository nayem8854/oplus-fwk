package android.bluetooth;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.EventLog;

public final class BluetoothHidDeviceAppSdpSettings implements Parcelable {
  public BluetoothHidDeviceAppSdpSettings(String paramString1, String paramString2, String paramString3, byte paramByte, byte[] paramArrayOfbyte) {
    this.mName = paramString1;
    this.mDescription = paramString2;
    this.mProvider = paramString3;
    this.mSubclass = paramByte;
    if (paramArrayOfbyte != null && paramArrayOfbyte.length <= 2048) {
      this.mDescriptors = (byte[])paramArrayOfbyte.clone();
      return;
    } 
    EventLog.writeEvent(1397638484, new Object[] { "119819889", Integer.valueOf(-1), "" });
    throw new IllegalArgumentException("descriptors must be not null and shorter than 2048");
  }
  
  public String getName() {
    return this.mName;
  }
  
  public String getDescription() {
    return this.mDescription;
  }
  
  public String getProvider() {
    return this.mProvider;
  }
  
  public byte getSubclass() {
    return this.mSubclass;
  }
  
  public byte[] getDescriptors() {
    return this.mDescriptors;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<BluetoothHidDeviceAppSdpSettings> CREATOR = new Parcelable.Creator<BluetoothHidDeviceAppSdpSettings>() {
      public BluetoothHidDeviceAppSdpSettings createFromParcel(Parcel param1Parcel) {
        String str1 = param1Parcel.readString();
        String str2 = param1Parcel.readString();
        String str3 = param1Parcel.readString();
        byte b = param1Parcel.readByte();
        return new BluetoothHidDeviceAppSdpSettings(str1, str2, str3, b, param1Parcel.createByteArray());
      }
      
      public BluetoothHidDeviceAppSdpSettings[] newArray(int param1Int) {
        return new BluetoothHidDeviceAppSdpSettings[param1Int];
      }
    };
  
  private static final int MAX_DESCRIPTOR_SIZE = 2048;
  
  private final String mDescription;
  
  private final byte[] mDescriptors;
  
  private final String mName;
  
  private final String mProvider;
  
  private final byte mSubclass;
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mName);
    paramParcel.writeString(this.mDescription);
    paramParcel.writeString(this.mProvider);
    paramParcel.writeByte(this.mSubclass);
    paramParcel.writeByteArray(this.mDescriptors);
  }
}
