package android.bluetooth;

import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;

public class OplusBluetoothGatt {
  public static final String DESCRIPTOR = "android.bluetooth.BluetoothGatt";
  
  public static final int OPLUS_CALL_TRANSACTION_INDEX = 10000;
  
  public static final int OPLUS_GATT_FAST_MODE = 1;
  
  public static final int OPLUS_GATT_NORMAL_MODE = 2;
  
  private static final String TAG = "OplusBluetoothGatt";
  
  public static final int TRANSACTION_CLIENT_SET_CONNECT_MODE = 10001;
  
  static boolean oplusClientSetConnectMode(IBluetoothGatt paramIBluetoothGatt, String paramString, int paramInt) throws RemoteException {
    boolean bool = false;
    if (paramIBluetoothGatt == null) {
      Log.w("OplusBluetoothGatt", "oplusClientSetConnectMode bluetoothGatt is null!");
      return false;
    } 
    if (paramString == null) {
      Log.w("OplusBluetoothGatt", "oplusClientSetConnectMode address is null!");
      return false;
    } 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.bluetooth.BluetoothGatt");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      paramIBluetoothGatt.asBinder().transact(10001, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      if (paramInt != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  static boolean oplusClientSetFastConnectMode(IBluetoothGatt paramIBluetoothGatt, String paramString) throws RemoteException {
    return oplusClientSetConnectMode(paramIBluetoothGatt, paramString, 1);
  }
}
