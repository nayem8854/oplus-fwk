package android.bluetooth;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IBluetoothHidHost extends IInterface {
  boolean connect(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  boolean disconnect(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  List<BluetoothDevice> getConnectedDevices() throws RemoteException;
  
  int getConnectionPolicy(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  int getConnectionState(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] paramArrayOfint) throws RemoteException;
  
  boolean getIdleTime(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  boolean getProtocolMode(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  boolean getReport(BluetoothDevice paramBluetoothDevice, byte paramByte1, byte paramByte2, int paramInt) throws RemoteException;
  
  boolean sendData(BluetoothDevice paramBluetoothDevice, String paramString) throws RemoteException;
  
  boolean setConnectionPolicy(BluetoothDevice paramBluetoothDevice, int paramInt) throws RemoteException;
  
  boolean setIdleTime(BluetoothDevice paramBluetoothDevice, byte paramByte) throws RemoteException;
  
  boolean setProtocolMode(BluetoothDevice paramBluetoothDevice, int paramInt) throws RemoteException;
  
  boolean setReport(BluetoothDevice paramBluetoothDevice, byte paramByte, String paramString) throws RemoteException;
  
  boolean virtualUnplug(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  class Default implements IBluetoothHidHost {
    public boolean connect(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return false;
    }
    
    public boolean disconnect(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return false;
    }
    
    public List<BluetoothDevice> getConnectedDevices() throws RemoteException {
      return null;
    }
    
    public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public int getConnectionState(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return 0;
    }
    
    public boolean setConnectionPolicy(BluetoothDevice param1BluetoothDevice, int param1Int) throws RemoteException {
      return false;
    }
    
    public int getConnectionPolicy(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return 0;
    }
    
    public boolean getProtocolMode(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return false;
    }
    
    public boolean virtualUnplug(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return false;
    }
    
    public boolean setProtocolMode(BluetoothDevice param1BluetoothDevice, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean getReport(BluetoothDevice param1BluetoothDevice, byte param1Byte1, byte param1Byte2, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean setReport(BluetoothDevice param1BluetoothDevice, byte param1Byte, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean sendData(BluetoothDevice param1BluetoothDevice, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean getIdleTime(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return false;
    }
    
    public boolean setIdleTime(BluetoothDevice param1BluetoothDevice, byte param1Byte) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IBluetoothHidHost {
    private static final String DESCRIPTOR = "android.bluetooth.IBluetoothHidHost";
    
    static final int TRANSACTION_connect = 1;
    
    static final int TRANSACTION_disconnect = 2;
    
    static final int TRANSACTION_getConnectedDevices = 3;
    
    static final int TRANSACTION_getConnectionPolicy = 7;
    
    static final int TRANSACTION_getConnectionState = 5;
    
    static final int TRANSACTION_getDevicesMatchingConnectionStates = 4;
    
    static final int TRANSACTION_getIdleTime = 14;
    
    static final int TRANSACTION_getProtocolMode = 8;
    
    static final int TRANSACTION_getReport = 11;
    
    static final int TRANSACTION_sendData = 13;
    
    static final int TRANSACTION_setConnectionPolicy = 6;
    
    static final int TRANSACTION_setIdleTime = 15;
    
    static final int TRANSACTION_setProtocolMode = 10;
    
    static final int TRANSACTION_setReport = 12;
    
    static final int TRANSACTION_virtualUnplug = 9;
    
    public Stub() {
      attachInterface(this, "android.bluetooth.IBluetoothHidHost");
    }
    
    public static IBluetoothHidHost asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.bluetooth.IBluetoothHidHost");
      if (iInterface != null && iInterface instanceof IBluetoothHidHost)
        return (IBluetoothHidHost)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 15:
          return "setIdleTime";
        case 14:
          return "getIdleTime";
        case 13:
          return "sendData";
        case 12:
          return "setReport";
        case 11:
          return "getReport";
        case 10:
          return "setProtocolMode";
        case 9:
          return "virtualUnplug";
        case 8:
          return "getProtocolMode";
        case 7:
          return "getConnectionPolicy";
        case 6:
          return "setConnectionPolicy";
        case 5:
          return "getConnectionState";
        case 4:
          return "getDevicesMatchingConnectionStates";
        case 3:
          return "getConnectedDevices";
        case 2:
          return "disconnect";
        case 1:
          break;
      } 
      return "connect";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool5;
        int m;
        boolean bool4;
        int k;
        boolean bool3;
        int j;
        boolean bool2;
        int i;
        String str;
        int[] arrayOfInt;
        List<BluetoothDevice> list;
        BluetoothDevice bluetoothDevice;
        byte b1, b2;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 15:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothHidHost");
            if (param1Parcel1.readInt() != 0) {
              bluetoothDevice = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(param1Parcel1);
            } else {
              bluetoothDevice = null;
            } 
            b1 = param1Parcel1.readByte();
            bool5 = setIdleTime(bluetoothDevice, b1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 14:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothHidHost");
            if (param1Parcel1.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            bool5 = getIdleTime((BluetoothDevice)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 13:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothHidHost");
            if (param1Parcel1.readInt() != 0) {
              bluetoothDevice = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(param1Parcel1);
            } else {
              bluetoothDevice = null;
            } 
            str = param1Parcel1.readString();
            bool5 = sendData(bluetoothDevice, str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 12:
            str.enforceInterface("android.bluetooth.IBluetoothHidHost");
            if (str.readInt() != 0) {
              bluetoothDevice = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str);
            } else {
              bluetoothDevice = null;
            } 
            b1 = str.readByte();
            str = str.readString();
            bool5 = setReport(bluetoothDevice, b1, str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 11:
            str.enforceInterface("android.bluetooth.IBluetoothHidHost");
            if (str.readInt() != 0) {
              bluetoothDevice = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str);
            } else {
              bluetoothDevice = null;
            } 
            b2 = str.readByte();
            b1 = str.readByte();
            m = str.readInt();
            bool4 = getReport(bluetoothDevice, b2, b1, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 10:
            str.enforceInterface("android.bluetooth.IBluetoothHidHost");
            if (str.readInt() != 0) {
              bluetoothDevice = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str);
            } else {
              bluetoothDevice = null;
            } 
            k = str.readInt();
            bool3 = setProtocolMode(bluetoothDevice, k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 9:
            str.enforceInterface("android.bluetooth.IBluetoothHidHost");
            if (str.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            bool3 = virtualUnplug((BluetoothDevice)str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 8:
            str.enforceInterface("android.bluetooth.IBluetoothHidHost");
            if (str.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            bool3 = getProtocolMode((BluetoothDevice)str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 7:
            str.enforceInterface("android.bluetooth.IBluetoothHidHost");
            if (str.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            j = getConnectionPolicy((BluetoothDevice)str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 6:
            str.enforceInterface("android.bluetooth.IBluetoothHidHost");
            if (str.readInt() != 0) {
              bluetoothDevice = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str);
            } else {
              bluetoothDevice = null;
            } 
            j = str.readInt();
            bool2 = setConnectionPolicy(bluetoothDevice, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 5:
            str.enforceInterface("android.bluetooth.IBluetoothHidHost");
            if (str.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            i = getConnectionState((BluetoothDevice)str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 4:
            str.enforceInterface("android.bluetooth.IBluetoothHidHost");
            arrayOfInt = str.createIntArray();
            list = getDevicesMatchingConnectionStates(arrayOfInt);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 3:
            list.enforceInterface("android.bluetooth.IBluetoothHidHost");
            list = getConnectedDevices();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 2:
            list.enforceInterface("android.bluetooth.IBluetoothHidHost");
            if (list.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)list);
            } else {
              list = null;
            } 
            bool1 = disconnect((BluetoothDevice)list);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        list.enforceInterface("android.bluetooth.IBluetoothHidHost");
        if (list.readInt() != 0) {
          BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)list);
        } else {
          list = null;
        } 
        boolean bool1 = connect((BluetoothDevice)list);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("android.bluetooth.IBluetoothHidHost");
      return true;
    }
    
    private static class Proxy implements IBluetoothHidHost {
      public static IBluetoothHidHost sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.bluetooth.IBluetoothHidHost";
      }
      
      public boolean connect(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHidHost");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHidHost.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHidHost.Stub.getDefaultImpl().connect(param2BluetoothDevice);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean disconnect(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHidHost");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHidHost.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHidHost.Stub.getDefaultImpl().disconnect(param2BluetoothDevice);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<BluetoothDevice> getConnectedDevices() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHidHost");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IBluetoothHidHost.Stub.getDefaultImpl() != null)
            return IBluetoothHidHost.Stub.getDefaultImpl().getConnectedDevices(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(BluetoothDevice.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHidHost");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IBluetoothHidHost.Stub.getDefaultImpl() != null)
            return IBluetoothHidHost.Stub.getDefaultImpl().getDevicesMatchingConnectionStates(param2ArrayOfint); 
          parcel2.readException();
          return parcel2.createTypedArrayList(BluetoothDevice.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getConnectionState(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHidHost");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IBluetoothHidHost.Stub.getDefaultImpl() != null)
            return IBluetoothHidHost.Stub.getDefaultImpl().getConnectionState(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setConnectionPolicy(BluetoothDevice param2BluetoothDevice, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHidHost");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHidHost.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHidHost.Stub.getDefaultImpl().setConnectionPolicy(param2BluetoothDevice, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getConnectionPolicy(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHidHost");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IBluetoothHidHost.Stub.getDefaultImpl() != null)
            return IBluetoothHidHost.Stub.getDefaultImpl().getConnectionPolicy(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getProtocolMode(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHidHost");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHidHost.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHidHost.Stub.getDefaultImpl().getProtocolMode(param2BluetoothDevice);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean virtualUnplug(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHidHost");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHidHost.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHidHost.Stub.getDefaultImpl().virtualUnplug(param2BluetoothDevice);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setProtocolMode(BluetoothDevice param2BluetoothDevice, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHidHost");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHidHost.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHidHost.Stub.getDefaultImpl().setProtocolMode(param2BluetoothDevice, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getReport(BluetoothDevice param2BluetoothDevice, byte param2Byte1, byte param2Byte2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHidHost");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeByte(param2Byte1);
          parcel1.writeByte(param2Byte2);
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHidHost.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHidHost.Stub.getDefaultImpl().getReport(param2BluetoothDevice, param2Byte1, param2Byte2, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setReport(BluetoothDevice param2BluetoothDevice, byte param2Byte, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHidHost");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeByte(param2Byte);
          parcel1.writeString(param2String);
          boolean bool2 = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHidHost.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHidHost.Stub.getDefaultImpl().setReport(param2BluetoothDevice, param2Byte, param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean sendData(BluetoothDevice param2BluetoothDevice, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHidHost");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool2 = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHidHost.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHidHost.Stub.getDefaultImpl().sendData(param2BluetoothDevice, param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getIdleTime(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHidHost");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHidHost.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHidHost.Stub.getDefaultImpl().getIdleTime(param2BluetoothDevice);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setIdleTime(BluetoothDevice param2BluetoothDevice, byte param2Byte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHidHost");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeByte(param2Byte);
          boolean bool2 = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHidHost.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHidHost.Stub.getDefaultImpl().setIdleTime(param2BluetoothDevice, param2Byte);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IBluetoothHidHost param1IBluetoothHidHost) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IBluetoothHidHost != null) {
          Proxy.sDefaultImpl = param1IBluetoothHidHost;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IBluetoothHidHost getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
