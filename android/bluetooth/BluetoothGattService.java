package android.bluetooth;

import android.os.Parcel;
import android.os.ParcelUuid;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BluetoothGattService implements Parcelable {
  protected UUID mUuid;
  
  protected int mServiceType;
  
  protected int mInstanceId;
  
  protected List<BluetoothGattService> mIncludedServices;
  
  protected int mHandles = 0;
  
  protected BluetoothDevice mDevice;
  
  protected List<BluetoothGattCharacteristic> mCharacteristics;
  
  private boolean mAdvertisePreferred;
  
  public static final int SERVICE_TYPE_SECONDARY = 1;
  
  public static final int SERVICE_TYPE_PRIMARY = 0;
  
  public BluetoothGattService(UUID paramUUID, int paramInt) {
    this.mDevice = null;
    this.mUuid = paramUUID;
    this.mInstanceId = 0;
    this.mServiceType = paramInt;
    this.mCharacteristics = new ArrayList<>();
    this.mIncludedServices = new ArrayList<>();
  }
  
  BluetoothGattService(BluetoothDevice paramBluetoothDevice, UUID paramUUID, int paramInt1, int paramInt2) {
    this.mDevice = paramBluetoothDevice;
    this.mUuid = paramUUID;
    this.mInstanceId = paramInt1;
    this.mServiceType = paramInt2;
    this.mCharacteristics = new ArrayList<>();
    this.mIncludedServices = new ArrayList<>();
  }
  
  public BluetoothGattService(UUID paramUUID, int paramInt1, int paramInt2) {
    this.mDevice = null;
    this.mUuid = paramUUID;
    this.mInstanceId = paramInt1;
    this.mServiceType = paramInt2;
    this.mCharacteristics = new ArrayList<>();
    this.mIncludedServices = new ArrayList<>();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)new ParcelUuid(this.mUuid), 0);
    paramParcel.writeInt(this.mInstanceId);
    paramParcel.writeInt(this.mServiceType);
    paramParcel.writeTypedList(this.mCharacteristics);
    List<BluetoothGattService> list = this.mIncludedServices;
    list = new ArrayList<>(list.size());
    for (BluetoothGattService bluetoothGattService : this.mIncludedServices) {
      UUID uUID = bluetoothGattService.getUuid();
      BluetoothGattIncludedService bluetoothGattIncludedService = new BluetoothGattIncludedService(uUID, bluetoothGattService.getInstanceId(), bluetoothGattService.getType());
      list.add(bluetoothGattIncludedService);
    } 
    paramParcel.writeTypedList(list);
  }
  
  public static final Parcelable.Creator<BluetoothGattService> CREATOR = new Parcelable.Creator<BluetoothGattService>() {
      public BluetoothGattService createFromParcel(Parcel param1Parcel) {
        return new BluetoothGattService(param1Parcel);
      }
      
      public BluetoothGattService[] newArray(int param1Int) {
        return new BluetoothGattService[param1Int];
      }
    };
  
  private BluetoothGattService(Parcel paramParcel) {
    this.mUuid = ((ParcelUuid)paramParcel.readParcelable(null)).getUuid();
    this.mInstanceId = paramParcel.readInt();
    this.mServiceType = paramParcel.readInt();
    this.mCharacteristics = new ArrayList<>();
    Parcelable.Creator<BluetoothGattCharacteristic> creator = BluetoothGattCharacteristic.CREATOR;
    ArrayList arrayList2 = paramParcel.createTypedArrayList(creator);
    if (arrayList2 != null)
      for (BluetoothGattCharacteristic bluetoothGattCharacteristic : arrayList2) {
        bluetoothGattCharacteristic.setService(this);
        this.mCharacteristics.add(bluetoothGattCharacteristic);
      }  
    this.mIncludedServices = new ArrayList<>();
    Parcelable.Creator<BluetoothGattIncludedService> creator1 = BluetoothGattIncludedService.CREATOR;
    ArrayList arrayList1 = paramParcel.createTypedArrayList(creator1);
    if (arrayList2 != null)
      for (BluetoothGattIncludedService bluetoothGattIncludedService : arrayList1) {
        List<BluetoothGattService> list = this.mIncludedServices;
        UUID uUID = bluetoothGattIncludedService.getUuid();
        BluetoothGattService bluetoothGattService = new BluetoothGattService(null, uUID, bluetoothGattIncludedService.getInstanceId(), bluetoothGattIncludedService.getType());
        list.add(bluetoothGattService);
      }  
  }
  
  BluetoothDevice getDevice() {
    return this.mDevice;
  }
  
  void setDevice(BluetoothDevice paramBluetoothDevice) {
    this.mDevice = paramBluetoothDevice;
  }
  
  public boolean addService(BluetoothGattService paramBluetoothGattService) {
    this.mIncludedServices.add(paramBluetoothGattService);
    return true;
  }
  
  public boolean addCharacteristic(BluetoothGattCharacteristic paramBluetoothGattCharacteristic) {
    this.mCharacteristics.add(paramBluetoothGattCharacteristic);
    paramBluetoothGattCharacteristic.setService(this);
    return true;
  }
  
  BluetoothGattCharacteristic getCharacteristic(UUID paramUUID, int paramInt) {
    for (BluetoothGattCharacteristic bluetoothGattCharacteristic : this.mCharacteristics) {
      if (paramUUID.equals(bluetoothGattCharacteristic.getUuid()) && 
        bluetoothGattCharacteristic.getInstanceId() == paramInt)
        return bluetoothGattCharacteristic; 
    } 
    return null;
  }
  
  public void setInstanceId(int paramInt) {
    this.mInstanceId = paramInt;
  }
  
  int getHandles() {
    return this.mHandles;
  }
  
  public void setHandles(int paramInt) {
    this.mHandles = paramInt;
  }
  
  public void addIncludedService(BluetoothGattService paramBluetoothGattService) {
    this.mIncludedServices.add(paramBluetoothGattService);
  }
  
  public UUID getUuid() {
    return this.mUuid;
  }
  
  public int getInstanceId() {
    return this.mInstanceId;
  }
  
  public int getType() {
    return this.mServiceType;
  }
  
  public List<BluetoothGattService> getIncludedServices() {
    return this.mIncludedServices;
  }
  
  public List<BluetoothGattCharacteristic> getCharacteristics() {
    return this.mCharacteristics;
  }
  
  public BluetoothGattCharacteristic getCharacteristic(UUID paramUUID) {
    for (BluetoothGattCharacteristic bluetoothGattCharacteristic : this.mCharacteristics) {
      if (paramUUID.equals(bluetoothGattCharacteristic.getUuid()))
        return bluetoothGattCharacteristic; 
    } 
    return null;
  }
  
  public boolean isAdvertisePreferred() {
    return this.mAdvertisePreferred;
  }
  
  public void setAdvertisePreferred(boolean paramBoolean) {
    this.mAdvertisePreferred = paramBoolean;
  }
}
