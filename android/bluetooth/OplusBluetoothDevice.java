package android.bluetooth;

import android.content.Context;
import android.os.Handler;
import android.os.RemoteException;
import android.util.Log;

public final class OplusBluetoothDevice {
  public static final String TAG = "OplusBluetoothDevice";
  
  private BluetoothDevice mBluetoothDevice = null;
  
  public OplusBluetoothDevice(BluetoothDevice paramBluetoothDevice) {
    this.mBluetoothDevice = paramBluetoothDevice;
  }
  
  public BluetoothGatt connectGatt(Context paramContext, boolean paramBoolean1, BluetoothGattCallback paramBluetoothGattCallback, boolean paramBoolean2) {
    return connectGatt(paramContext, paramBoolean1, paramBluetoothGattCallback, 0, paramBoolean2);
  }
  
  public BluetoothGatt connectGatt(Context paramContext, boolean paramBoolean1, BluetoothGattCallback paramBluetoothGattCallback, int paramInt, boolean paramBoolean2) {
    return connectGatt(paramContext, paramBoolean1, paramBluetoothGattCallback, paramInt, 1, paramBoolean2);
  }
  
  public BluetoothGatt connectGatt(Context paramContext, boolean paramBoolean1, BluetoothGattCallback paramBluetoothGattCallback, int paramInt1, int paramInt2, boolean paramBoolean2) {
    return connectGatt(paramContext, paramBoolean1, paramBluetoothGattCallback, paramInt1, paramInt2, null, paramBoolean2);
  }
  
  public BluetoothGatt connectGatt(Context paramContext, boolean paramBoolean1, BluetoothGattCallback paramBluetoothGattCallback, int paramInt1, int paramInt2, Handler paramHandler, boolean paramBoolean2) {
    return connectGatt(paramContext, paramBoolean1, paramBluetoothGattCallback, paramInt1, false, paramInt2, paramHandler, paramBoolean2);
  }
  
  public BluetoothGatt connectGatt(Context paramContext, boolean paramBoolean1, BluetoothGattCallback paramBluetoothGattCallback, int paramInt1, boolean paramBoolean2, int paramInt2, Handler paramHandler, boolean paramBoolean3) {
    if (paramBluetoothGattCallback != null) {
      BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
      IBluetoothManager iBluetoothManager = bluetoothAdapter.getBluetoothManager();
      try {
        IBluetoothGatt iBluetoothGatt = iBluetoothManager.getBluetoothGatt();
        if (iBluetoothGatt == null)
          return null; 
        if (this.mBluetoothDevice == null)
          return null; 
        BluetoothGatt bluetoothGatt = new BluetoothGatt();
        this(iBluetoothGatt, this.mBluetoothDevice, paramInt1, paramBoolean2, paramInt2);
        if (!paramBoolean1 && paramBoolean3) {
          paramBoolean2 = OplusBluetoothGatt.oplusClientSetFastConnectMode(iBluetoothGatt, this.mBluetoothDevice.getAddress());
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("setConnectMode ");
          stringBuilder.append(paramBoolean2);
          Log.d("OplusBluetoothDevice", stringBuilder.toString());
        } 
        try {
          bluetoothGatt.connect(Boolean.valueOf(paramBoolean1), paramBluetoothGattCallback, paramHandler);
          return bluetoothGatt;
        } catch (RemoteException null) {}
      } catch (RemoteException remoteException) {}
      Log.e("OplusBluetoothDevice", "", (Throwable)remoteException);
      return null;
    } 
    throw new NullPointerException("callback is null");
  }
}
