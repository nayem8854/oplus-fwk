package android.bluetooth;

import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;
import java.util.List;

public final class OplusBluetoothAdapter {
  public static final String DESCRIPTOR = "android.bluetooth.IBluetooth";
  
  public static final int OPLUS_CALL_TRANSACTION_INDEX = 10000;
  
  public static final int OPLUS_FIRST_CALL_TRANSACTION = 10001;
  
  public static final String TAG = "OplusBluetoothAdapter";
  
  public static final int TRANSACTION_DISABLE_AUTO_CONNECT_POLICY = 10007;
  
  public static final int TRANSACTION_ENABLE_AUTO_CONNECT_POLICY = 10006;
  
  public static final int TRANSACTION_GET_BLUETOOTH_CONNECTED_APP = 10005;
  
  public static final int TRANSACTION_GET_BLUETOOTH_CONNECTION_COUNT = 10004;
  
  public static final int TRANSACTION_SCO_AVAILABLE_OFFCALL = 10003;
  
  public static final int TRANSACTION_SETBLBLACKORWHITELIST = 10002;
  
  private static OplusBluetoothAdapter sAdapter;
  
  private static volatile IBluetooth sService;
  
  public static OplusBluetoothAdapter getOplusBluetoothAdapter() {
    // Byte code:
    //   0: ldc android/bluetooth/OplusBluetoothAdapter
    //   2: monitorenter
    //   3: getstatic android/bluetooth/OplusBluetoothAdapter.sAdapter : Landroid/bluetooth/OplusBluetoothAdapter;
    //   6: ifnonnull -> 21
    //   9: new android/bluetooth/OplusBluetoothAdapter
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic android/bluetooth/OplusBluetoothAdapter.sAdapter : Landroid/bluetooth/OplusBluetoothAdapter;
    //   21: getstatic android/bluetooth/OplusBluetoothAdapter.sAdapter : Landroid/bluetooth/OplusBluetoothAdapter;
    //   24: astore_0
    //   25: ldc android/bluetooth/OplusBluetoothAdapter
    //   27: monitorexit
    //   28: aload_0
    //   29: areturn
    //   30: astore_0
    //   31: ldc android/bluetooth/OplusBluetoothAdapter
    //   33: monitorexit
    //   34: aload_0
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #61	-> 3
    //   #62	-> 9
    //   #64	-> 21
    //   #60	-> 30
    // Exception table:
    //   from	to	target	type
    //   3	9	30	finally
    //   9	21	30	finally
    //   21	25	30	finally
  }
  
  OplusBluetoothAdapter() {
    getService();
  }
  
  private static IBluetooth getService() {
    // Byte code:
    //   0: invokestatic getDefaultAdapter : ()Landroid/bluetooth/BluetoothAdapter;
    //   3: astore_0
    //   4: aload_0
    //   5: getstatic android/bluetooth/OplusBluetoothAdapter.sStateChangeCallback : Landroid/bluetooth/IBluetoothManagerCallback;
    //   8: invokevirtual getBluetoothService : (Landroid/bluetooth/IBluetoothManagerCallback;)Landroid/bluetooth/IBluetooth;
    //   11: astore_0
    //   12: ldc android/bluetooth/OplusBluetoothAdapter
    //   14: monitorenter
    //   15: getstatic android/bluetooth/OplusBluetoothAdapter.sService : Landroid/bluetooth/IBluetooth;
    //   18: ifnonnull -> 25
    //   21: aload_0
    //   22: putstatic android/bluetooth/OplusBluetoothAdapter.sService : Landroid/bluetooth/IBluetooth;
    //   25: ldc android/bluetooth/OplusBluetoothAdapter
    //   27: monitorexit
    //   28: getstatic android/bluetooth/OplusBluetoothAdapter.sService : Landroid/bluetooth/IBluetooth;
    //   31: areturn
    //   32: astore_0
    //   33: ldc android/bluetooth/OplusBluetoothAdapter
    //   35: monitorexit
    //   36: aload_0
    //   37: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #72	-> 0
    //   #73	-> 4
    //   #75	-> 12
    //   #76	-> 15
    //   #77	-> 21
    //   #79	-> 25
    //   #80	-> 28
    //   #79	-> 32
    // Exception table:
    //   from	to	target	type
    //   15	21	32	finally
    //   21	25	32	finally
    //   25	28	32	finally
    //   33	36	32	finally
  }
  
  private static IBluetoothManagerCallback sStateChangeCallback = (IBluetoothManagerCallback)new Object();
  
  public void setBLBlackOrWhiteList(List<String> paramList, int paramInt, boolean paramBoolean) throws RemoteException {
    IBluetooth iBluetooth = sService;
    if (iBluetooth == null) {
      Log.w("OplusBluetoothAdapter", "oppoBluetoothAdapter setBLBlackOrWhiteList null!");
      return;
    } 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
      parcel1.writeStringList(paramList);
      parcel1.writeInt(paramInt);
      if (paramBoolean) {
        paramInt = 1;
      } else {
        paramInt = 0;
      } 
      parcel1.writeInt(paramInt);
      iBluetooth.asBinder().transact(10002, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean isBluetoothScoAvailableOffCall() throws RemoteException {
    null = sService;
    boolean bool = true;
    if (null == null) {
      Log.w("OplusBluetoothAdapter", "oppoBluetoothAdapter isBluetoothScoAvailableOffCall null!");
      return true;
    } 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
      null.asBinder().transact(10003, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i == 0)
        bool = false; 
      return bool;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int getBluetoothConnectionCount() throws RemoteException {
    null = sService;
    if (null == null) {
      Log.w("OplusBluetoothAdapter", "oppoBluetoothAdapter getBluetoothConnectionCount null!");
      return 0;
    } 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
      null.asBinder().transact(10004, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readInt();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int[] getBluetoothConnectedAppPID() throws RemoteException {
    null = sService;
    if (null == null) {
      Log.w("OplusBluetoothAdapter", "oppoBluetoothAdapter getBluetoothConnectedAppPID null!");
      return null;
    } 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
      null.asBinder().transact(10005, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      int[] arrayOfInt = new int[i];
      parcel2.readIntArray(arrayOfInt);
      return arrayOfInt;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void oplusEnableAutoConnectPolicy(BluetoothDevice paramBluetoothDevice) throws RemoteException {
    IBluetooth iBluetooth = sService;
    if (iBluetooth == null || paramBluetoothDevice == null)
      return; 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
      parcel1.writeString(paramBluetoothDevice.getAddress());
      iBluetooth.asBinder().transact(10006, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void oplusDisableAutoConnectPolicy(BluetoothDevice paramBluetoothDevice) throws RemoteException {
    IBluetooth iBluetooth = sService;
    if (iBluetooth == null || paramBluetoothDevice == null)
      return; 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
      parcel1.writeString(paramBluetoothDevice.getAddress());
      iBluetooth.asBinder().transact(10007, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
}
