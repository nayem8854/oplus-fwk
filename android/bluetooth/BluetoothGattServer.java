package android.bluetooth;

import android.os.ParcelUuid;
import android.os.RemoteException;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class BluetoothGattServer implements BluetoothProfile {
  private int mTransport;
  
  private List<BluetoothGattService> mServices;
  
  private IBluetoothGatt mService;
  
  private Object mServerIfLock = new Object();
  
  private int mServerIf;
  
  private BluetoothGattService mPendingService;
  
  private BluetoothGattServerCallback mCallback;
  
  private final IBluetoothGattServerCallback mBluetoothGattServerCallback = (IBluetoothGattServerCallback)new Object(this);
  
  private BluetoothAdapter mAdapter;
  
  private static final boolean VDBG = false;
  
  private static final String TAG = "BluetoothGattServer";
  
  private static final boolean DBG = true;
  
  private static final int CALLBACK_REG_TIMEOUT = 10000;
  
  BluetoothGattServer(IBluetoothGatt paramIBluetoothGatt, int paramInt) {
    this.mService = paramIBluetoothGatt;
    this.mAdapter = BluetoothAdapter.getDefaultAdapter();
    this.mCallback = null;
    this.mServerIf = 0;
    this.mTransport = paramInt;
    this.mServices = new ArrayList<>();
  }
  
  BluetoothGattCharacteristic getCharacteristicByHandle(int paramInt) {
    for (BluetoothGattService bluetoothGattService : this.mServices) {
      for (BluetoothGattCharacteristic bluetoothGattCharacteristic : bluetoothGattService.getCharacteristics()) {
        if (bluetoothGattCharacteristic.getInstanceId() == paramInt)
          return bluetoothGattCharacteristic; 
      } 
    } 
    return null;
  }
  
  BluetoothGattDescriptor getDescriptorByHandle(int paramInt) {
    for (BluetoothGattService bluetoothGattService : this.mServices) {
      for (BluetoothGattCharacteristic bluetoothGattCharacteristic : bluetoothGattService.getCharacteristics()) {
        for (BluetoothGattDescriptor bluetoothGattDescriptor : bluetoothGattCharacteristic.getDescriptors()) {
          if (bluetoothGattDescriptor.getInstanceId() == paramInt)
            return bluetoothGattDescriptor; 
        } 
      } 
    } 
    return null;
  }
  
  public void close() {
    Log.d("BluetoothGattServer", "close()");
    unregisterCallback();
  }
  
  boolean registerCallback(BluetoothGattServerCallback paramBluetoothGattServerCallback) {
    Log.d("BluetoothGattServer", "registerCallback()");
    if (this.mService == null) {
      Log.e("BluetoothGattServer", "GATT service not available");
      return false;
    } 
    UUID uUID = UUID.randomUUID();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("registerCallback() - UUID=");
    stringBuilder.append(uUID);
    Log.d("BluetoothGattServer", stringBuilder.toString());
    synchronized (this.mServerIfLock) {
      if (this.mCallback != null) {
        Log.e("BluetoothGattServer", "App can register callback only once");
        return false;
      } 
      this.mCallback = paramBluetoothGattServerCallback;
      try {
        IBluetoothGatt iBluetoothGatt = this.mService;
        ParcelUuid parcelUuid = new ParcelUuid();
        this(uUID);
        iBluetoothGatt.registerServer(parcelUuid, this.mBluetoothGattServerCallback);
        try {
          this.mServerIfLock.wait(10000L);
        } catch (InterruptedException interruptedException) {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("");
          stringBuilder1.append(interruptedException);
          Log.e("BluetoothGattServer", stringBuilder1.toString());
          this.mCallback = null;
        } 
        if (this.mServerIf == 0) {
          this.mCallback = null;
          return false;
        } 
        return true;
      } catch (RemoteException remoteException) {
        Log.e("BluetoothGattServer", "", (Throwable)remoteException);
        this.mCallback = null;
        return false;
      } 
    } 
  }
  
  private void unregisterCallback() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("unregisterCallback() - mServerIf=");
    stringBuilder.append(this.mServerIf);
    Log.d("BluetoothGattServer", stringBuilder.toString());
    IBluetoothGatt iBluetoothGatt = this.mService;
    if (iBluetoothGatt != null) {
      int i = this.mServerIf;
      if (i != 0) {
        try {
          this.mCallback = null;
          iBluetoothGatt.unregisterServer(i);
          this.mServerIf = 0;
        } catch (RemoteException remoteException) {
          Log.e("BluetoothGattServer", "", (Throwable)remoteException);
        } 
        return;
      } 
    } 
  }
  
  BluetoothGattService getService(UUID paramUUID, int paramInt1, int paramInt2) {
    for (BluetoothGattService bluetoothGattService : this.mServices) {
      if (bluetoothGattService.getType() == paramInt2 && 
        bluetoothGattService.getInstanceId() == paramInt1 && 
        bluetoothGattService.getUuid().equals(paramUUID))
        return bluetoothGattService; 
    } 
    return null;
  }
  
  public boolean connect(BluetoothDevice paramBluetoothDevice, boolean paramBoolean) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("connect() - device: ");
    stringBuilder.append(paramBluetoothDevice.getAddress());
    stringBuilder.append(", auto: ");
    stringBuilder.append(paramBoolean);
    String str = stringBuilder.toString();
    Log.d("BluetoothGattServer", str);
    IBluetoothGatt iBluetoothGatt = this.mService;
    if (iBluetoothGatt != null) {
      int i = this.mServerIf;
      if (i != 0)
        try {
          String str1 = paramBluetoothDevice.getAddress();
          if (!paramBoolean) {
            paramBoolean = true;
          } else {
            paramBoolean = false;
          } 
          iBluetoothGatt.serverConnect(i, str1, paramBoolean, this.mTransport);
          return true;
        } catch (RemoteException remoteException) {
          Log.e("BluetoothGattServer", "", (Throwable)remoteException);
          return false;
        }  
    } 
    return false;
  }
  
  public void cancelConnection(BluetoothDevice paramBluetoothDevice) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("cancelConnection() - device: ");
    stringBuilder.append(paramBluetoothDevice.getAddress());
    Log.d("BluetoothGattServer", stringBuilder.toString());
    IBluetoothGatt iBluetoothGatt = this.mService;
    if (iBluetoothGatt != null) {
      int i = this.mServerIf;
      if (i != 0) {
        try {
          iBluetoothGatt.serverDisconnect(i, paramBluetoothDevice.getAddress());
        } catch (RemoteException remoteException) {
          Log.e("BluetoothGattServer", "", (Throwable)remoteException);
        } 
        return;
      } 
    } 
  }
  
  public void setPreferredPhy(BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2, int paramInt3) {
    try {
      this.mService.serverSetPreferredPhy(this.mServerIf, paramBluetoothDevice.getAddress(), paramInt1, paramInt2, paramInt3);
    } catch (RemoteException remoteException) {
      Log.e("BluetoothGattServer", "", (Throwable)remoteException);
    } 
  }
  
  public void readPhy(BluetoothDevice paramBluetoothDevice) {
    try {
      this.mService.serverReadPhy(this.mServerIf, paramBluetoothDevice.getAddress());
    } catch (RemoteException remoteException) {
      Log.e("BluetoothGattServer", "", (Throwable)remoteException);
    } 
  }
  
  public boolean sendResponse(BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2, int paramInt3, byte[] paramArrayOfbyte) {
    IBluetoothGatt iBluetoothGatt = this.mService;
    if (iBluetoothGatt != null) {
      int i = this.mServerIf;
      if (i != 0)
        try {
          iBluetoothGatt.sendResponse(i, paramBluetoothDevice.getAddress(), paramInt1, paramInt2, paramInt3, paramArrayOfbyte);
          return true;
        } catch (RemoteException remoteException) {
          Log.e("BluetoothGattServer", "", (Throwable)remoteException);
          return false;
        }  
    } 
    return false;
  }
  
  public boolean notifyCharacteristicChanged(BluetoothDevice paramBluetoothDevice, BluetoothGattCharacteristic paramBluetoothGattCharacteristic, boolean paramBoolean) {
    if (this.mService == null || this.mServerIf == 0)
      return false; 
    BluetoothGattService bluetoothGattService = paramBluetoothGattCharacteristic.getService();
    if (bluetoothGattService == null)
      return false; 
    if (paramBluetoothGattCharacteristic.getValue() != null)
      try {
        IBluetoothGatt iBluetoothGatt = this.mService;
        int i = this.mServerIf;
        String str = paramBluetoothDevice.getAddress();
        int j = paramBluetoothGattCharacteristic.getInstanceId();
        byte[] arrayOfByte = paramBluetoothGattCharacteristic.getValue();
        iBluetoothGatt.sendNotification(i, str, j, paramBoolean, arrayOfByte);
        return true;
      } catch (RemoteException remoteException) {
        Log.e("BluetoothGattServer", "", (Throwable)remoteException);
        return false;
      }  
    throw new IllegalArgumentException("Chracteristic value is empty. Use BluetoothGattCharacteristic#setvalue to update");
  }
  
  public boolean addService(BluetoothGattService paramBluetoothGattService) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("addService() - service: ");
    stringBuilder.append(paramBluetoothGattService.getUuid());
    Log.d("BluetoothGattServer", stringBuilder.toString());
    IBluetoothGatt iBluetoothGatt = this.mService;
    if (iBluetoothGatt != null) {
      int i = this.mServerIf;
      if (i != 0) {
        this.mPendingService = paramBluetoothGattService;
        try {
          iBluetoothGatt.addService(i, paramBluetoothGattService);
          return true;
        } catch (RemoteException remoteException) {
          Log.e("BluetoothGattServer", "", (Throwable)remoteException);
          return false;
        } 
      } 
    } 
    return false;
  }
  
  public boolean removeService(BluetoothGattService paramBluetoothGattService) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("removeService() - service: ");
    stringBuilder.append(paramBluetoothGattService.getUuid());
    Log.d("BluetoothGattServer", stringBuilder.toString());
    if (this.mService == null || this.mServerIf == 0)
      return false; 
    UUID uUID = paramBluetoothGattService.getUuid();
    int i = paramBluetoothGattService.getInstanceId(), j = paramBluetoothGattService.getType();
    BluetoothGattService bluetoothGattService = getService(uUID, i, j);
    if (bluetoothGattService == null)
      return false; 
    try {
      this.mService.removeService(this.mServerIf, paramBluetoothGattService.getInstanceId());
      this.mServices.remove(bluetoothGattService);
      return true;
    } catch (RemoteException remoteException) {
      Log.e("BluetoothGattServer", "", (Throwable)remoteException);
      return false;
    } 
  }
  
  public void clearServices() {
    Log.d("BluetoothGattServer", "clearServices()");
    IBluetoothGatt iBluetoothGatt = this.mService;
    if (iBluetoothGatt != null) {
      int i = this.mServerIf;
      if (i != 0) {
        try {
          iBluetoothGatt.clearServices(i);
          this.mServices.clear();
        } catch (RemoteException remoteException) {
          Log.e("BluetoothGattServer", "", (Throwable)remoteException);
        } 
        return;
      } 
    } 
  }
  
  public List<BluetoothGattService> getServices() {
    return this.mServices;
  }
  
  public BluetoothGattService getService(UUID paramUUID) {
    for (BluetoothGattService bluetoothGattService : this.mServices) {
      if (bluetoothGattService.getUuid().equals(paramUUID))
        return bluetoothGattService; 
    } 
    return null;
  }
  
  public int getConnectionState(BluetoothDevice paramBluetoothDevice) {
    throw new UnsupportedOperationException("Use BluetoothManager#getConnectionState instead.");
  }
  
  public List<BluetoothDevice> getConnectedDevices() {
    throw new UnsupportedOperationException("Use BluetoothManager#getConnectedDevices instead.");
  }
  
  public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] paramArrayOfint) {
    throw new UnsupportedOperationException("Use BluetoothManager#getDevicesMatchingConnectionStates instead.");
  }
}
