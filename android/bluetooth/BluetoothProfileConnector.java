package android.bluetooth;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.UserHandle;
import android.util.Log;

public abstract class BluetoothProfileConnector<T> {
  private final IBluetoothStateChangeCallback mBluetoothStateChangeCallback = (IBluetoothStateChangeCallback)new Object(this);
  
  private final ServiceConnection mConnection = (ServiceConnection)new Object(this);
  
  private Context mContext;
  
  private final int mProfileId;
  
  private final String mProfileName;
  
  private final BluetoothProfile mProfileProxy;
  
  private volatile T mService;
  
  private BluetoothProfile.ServiceListener mServiceListener;
  
  private final String mServiceName;
  
  BluetoothProfileConnector(BluetoothProfile paramBluetoothProfile, int paramInt, String paramString1, String paramString2) {
    this.mProfileId = paramInt;
    this.mProfileProxy = paramBluetoothProfile;
    this.mProfileName = paramString1;
    this.mServiceName = paramString2;
  }
  
  private boolean doBind() {
    synchronized (this.mConnection) {
      if (this.mService == null) {
        logDebug("Binding service...");
        try {
          Intent intent = new Intent();
          this(this.mServiceName);
          Context context = this.mContext;
          PackageManager packageManager = context.getPackageManager();
          ComponentName componentName = intent.resolveSystemService(packageManager, 0);
          intent.setComponent(componentName);
          if (componentName == null || !this.mContext.bindServiceAsUser(intent, this.mConnection, 0, UserHandle.CURRENT_OR_SELF)) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Could not bind to Bluetooth Service with ");
            stringBuilder.append(intent);
            logError(stringBuilder.toString());
            return false;
          } 
        } catch (SecurityException securityException) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Failed to bind service. ");
          stringBuilder.append(securityException);
          logError(stringBuilder.toString());
          return false;
        } 
      } 
      return true;
    } 
  }
  
  private void doUnbind() {
    synchronized (this.mConnection) {
      if (this.mService != null) {
        logDebug("Unbinding service...");
        try {
          this.mContext.unbindService(this.mConnection);
          this.mService = null;
        } catch (IllegalArgumentException illegalArgumentException) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Unable to unbind service: ");
          stringBuilder.append(illegalArgumentException);
          logError(stringBuilder.toString());
          this.mService = null;
        } finally {
          Exception exception;
        } 
      } 
      return;
    } 
  }
  
  void connect(Context paramContext, BluetoothProfile.ServiceListener paramServiceListener) {
    this.mContext = paramContext;
    this.mServiceListener = paramServiceListener;
    IBluetoothManager iBluetoothManager = BluetoothAdapter.getDefaultAdapter().getBluetoothManager();
    if (iBluetoothManager != null)
      try {
        iBluetoothManager.registerStateChangeCallback(this.mBluetoothStateChangeCallback);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to register state change callback. ");
        stringBuilder.append(remoteException);
        logError(stringBuilder.toString());
      }  
    doBind();
  }
  
  void disconnect() {
    this.mServiceListener = null;
    IBluetoothManager iBluetoothManager = BluetoothAdapter.getDefaultAdapter().getBluetoothManager();
    if (iBluetoothManager != null)
      try {
        iBluetoothManager.unregisterStateChangeCallback(this.mBluetoothStateChangeCallback);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to unregister state change callback");
        stringBuilder.append(remoteException);
        logError(stringBuilder.toString());
      }  
    doUnbind();
  }
  
  T getService() {
    return this.mService;
  }
  
  private void logDebug(String paramString) {
    Log.d(this.mProfileName, paramString);
  }
  
  private void logError(String paramString) {
    Log.e(this.mProfileName, paramString);
  }
  
  public abstract T getServiceInterface(IBinder paramIBinder);
}
