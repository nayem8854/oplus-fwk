package android.bluetooth;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.Objects;

public final class BluetoothCodecStatus implements Parcelable {
  public BluetoothCodecStatus(BluetoothCodecConfig paramBluetoothCodecConfig, BluetoothCodecConfig[] paramArrayOfBluetoothCodecConfig1, BluetoothCodecConfig[] paramArrayOfBluetoothCodecConfig2) {
    this.mCodecConfig = paramBluetoothCodecConfig;
    this.mCodecsLocalCapabilities = paramArrayOfBluetoothCodecConfig1;
    this.mCodecsSelectableCapabilities = paramArrayOfBluetoothCodecConfig2;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof BluetoothCodecStatus;
    boolean bool1 = false;
    if (bool) {
      paramObject = paramObject;
      if (Objects.equals(((BluetoothCodecStatus)paramObject).mCodecConfig, this.mCodecConfig)) {
        BluetoothCodecConfig[] arrayOfBluetoothCodecConfig1 = ((BluetoothCodecStatus)paramObject).mCodecsLocalCapabilities, arrayOfBluetoothCodecConfig2 = this.mCodecsLocalCapabilities;
        if (sameCapabilities(arrayOfBluetoothCodecConfig1, arrayOfBluetoothCodecConfig2)) {
          arrayOfBluetoothCodecConfig2 = ((BluetoothCodecStatus)paramObject).mCodecsSelectableCapabilities;
          paramObject = this.mCodecsSelectableCapabilities;
          if (sameCapabilities(arrayOfBluetoothCodecConfig2, (BluetoothCodecConfig[])paramObject))
            bool1 = true; 
        } 
      } 
      return bool1;
    } 
    return false;
  }
  
  public static boolean sameCapabilities(BluetoothCodecConfig[] paramArrayOfBluetoothCodecConfig1, BluetoothCodecConfig[] paramArrayOfBluetoothCodecConfig2) {
    boolean bool = false;
    if (paramArrayOfBluetoothCodecConfig1 == null) {
      if (paramArrayOfBluetoothCodecConfig2 == null)
        bool = true; 
      return bool;
    } 
    if (paramArrayOfBluetoothCodecConfig2 == null)
      return false; 
    if (paramArrayOfBluetoothCodecConfig1.length != paramArrayOfBluetoothCodecConfig2.length)
      return false; 
    return Arrays.<BluetoothCodecConfig>asList(paramArrayOfBluetoothCodecConfig1).containsAll(Arrays.asList((Object[])paramArrayOfBluetoothCodecConfig2));
  }
  
  public boolean isCodecConfigSelectable(BluetoothCodecConfig paramBluetoothCodecConfig) {
    if (paramBluetoothCodecConfig == null || !paramBluetoothCodecConfig.hasSingleSampleRate() || 
      !paramBluetoothCodecConfig.hasSingleBitsPerSample() || !paramBluetoothCodecConfig.hasSingleChannelMode())
      return false; 
    BluetoothCodecConfig[] arrayOfBluetoothCodecConfig;
    int i;
    byte b;
    for (arrayOfBluetoothCodecConfig = this.mCodecsSelectableCapabilities, i = arrayOfBluetoothCodecConfig.length, b = 0; b < i; ) {
      BluetoothCodecConfig bluetoothCodecConfig = arrayOfBluetoothCodecConfig[b];
      if (paramBluetoothCodecConfig.getCodecType() != bluetoothCodecConfig.getCodecType()) {
        b++;
        continue;
      } 
      return true;
    } 
    return false;
  }
  
  public int hashCode() {
    BluetoothCodecConfig bluetoothCodecConfig = this.mCodecConfig, arrayOfBluetoothCodecConfig[] = this.mCodecsLocalCapabilities;
    return Objects.hash(new Object[] { bluetoothCodecConfig, arrayOfBluetoothCodecConfig, arrayOfBluetoothCodecConfig });
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{mCodecConfig:");
    stringBuilder.append(this.mCodecConfig);
    stringBuilder.append(",mCodecsLocalCapabilities:");
    BluetoothCodecConfig[] arrayOfBluetoothCodecConfig = this.mCodecsLocalCapabilities;
    stringBuilder.append(Arrays.toString((Object[])arrayOfBluetoothCodecConfig));
    stringBuilder.append(",mCodecsSelectableCapabilities:");
    arrayOfBluetoothCodecConfig = this.mCodecsSelectableCapabilities;
    stringBuilder.append(Arrays.toString((Object[])arrayOfBluetoothCodecConfig));
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<BluetoothCodecStatus> CREATOR = new Parcelable.Creator<BluetoothCodecStatus>() {
      public BluetoothCodecStatus createFromParcel(Parcel param1Parcel) {
        BluetoothCodecConfig bluetoothCodecConfig = (BluetoothCodecConfig)param1Parcel.readTypedObject(BluetoothCodecConfig.CREATOR);
        BluetoothCodecConfig[] arrayOfBluetoothCodecConfig2 = (BluetoothCodecConfig[])param1Parcel.createTypedArray(BluetoothCodecConfig.CREATOR);
        BluetoothCodecConfig[] arrayOfBluetoothCodecConfig1 = (BluetoothCodecConfig[])param1Parcel.createTypedArray(BluetoothCodecConfig.CREATOR);
        return new BluetoothCodecStatus(bluetoothCodecConfig, arrayOfBluetoothCodecConfig2, arrayOfBluetoothCodecConfig1);
      }
      
      public BluetoothCodecStatus[] newArray(int param1Int) {
        return new BluetoothCodecStatus[param1Int];
      }
    };
  
  public static final String EXTRA_CODEC_STATUS = "android.bluetooth.extra.CODEC_STATUS";
  
  private final BluetoothCodecConfig mCodecConfig;
  
  private final BluetoothCodecConfig[] mCodecsLocalCapabilities;
  
  private final BluetoothCodecConfig[] mCodecsSelectableCapabilities;
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeTypedObject(this.mCodecConfig, 0);
    paramParcel.writeTypedArray((Parcelable[])this.mCodecsLocalCapabilities, 0);
    paramParcel.writeTypedArray((Parcelable[])this.mCodecsSelectableCapabilities, 0);
  }
  
  public BluetoothCodecConfig getCodecConfig() {
    return this.mCodecConfig;
  }
  
  public BluetoothCodecConfig[] getCodecsLocalCapabilities() {
    return this.mCodecsLocalCapabilities;
  }
  
  public BluetoothCodecConfig[] getCodecsSelectableCapabilities() {
    return this.mCodecsSelectableCapabilities;
  }
}
