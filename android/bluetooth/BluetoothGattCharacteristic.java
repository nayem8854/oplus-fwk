package android.bluetooth;

import android.os.Parcel;
import android.os.ParcelUuid;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BluetoothGattCharacteristic implements Parcelable {
  protected int mWriteType;
  
  protected byte[] mValue;
  
  protected UUID mUuid;
  
  protected BluetoothGattService mService;
  
  protected int mProperties;
  
  protected int mPermissions;
  
  protected int mKeySize = 16;
  
  protected int mInstance;
  
  protected List<BluetoothGattDescriptor> mDescriptors;
  
  public static final int WRITE_TYPE_SIGNED = 4;
  
  public static final int WRITE_TYPE_NO_RESPONSE = 1;
  
  public static final int WRITE_TYPE_DEFAULT = 2;
  
  public static final int PROPERTY_WRITE_NO_RESPONSE = 4;
  
  public static final int PROPERTY_WRITE = 8;
  
  public static final int PROPERTY_SIGNED_WRITE = 64;
  
  public static final int PROPERTY_READ = 2;
  
  public static final int PROPERTY_NOTIFY = 16;
  
  public static final int PROPERTY_INDICATE = 32;
  
  public static final int PROPERTY_EXTENDED_PROPS = 128;
  
  public static final int PROPERTY_BROADCAST = 1;
  
  public static final int PERMISSION_WRITE_SIGNED_MITM = 256;
  
  public static final int PERMISSION_WRITE_SIGNED = 128;
  
  public static final int PERMISSION_WRITE_ENCRYPTED_MITM = 64;
  
  public static final int PERMISSION_WRITE_ENCRYPTED = 32;
  
  public static final int PERMISSION_WRITE = 16;
  
  public static final int PERMISSION_READ_ENCRYPTED_MITM = 4;
  
  public static final int PERMISSION_READ_ENCRYPTED = 2;
  
  public static final int PERMISSION_READ = 1;
  
  public static final int FORMAT_UINT8 = 17;
  
  public static final int FORMAT_UINT32 = 20;
  
  public static final int FORMAT_UINT16 = 18;
  
  public static final int FORMAT_SINT8 = 33;
  
  public static final int FORMAT_SINT32 = 36;
  
  public static final int FORMAT_SINT16 = 34;
  
  public static final int FORMAT_SFLOAT = 50;
  
  public static final int FORMAT_FLOAT = 52;
  
  public BluetoothGattCharacteristic(UUID paramUUID, int paramInt1, int paramInt2) {
    initCharacteristic(null, paramUUID, 0, paramInt1, paramInt2);
  }
  
  BluetoothGattCharacteristic(BluetoothGattService paramBluetoothGattService, UUID paramUUID, int paramInt1, int paramInt2, int paramInt3) {
    initCharacteristic(paramBluetoothGattService, paramUUID, paramInt1, paramInt2, paramInt3);
  }
  
  public BluetoothGattCharacteristic(UUID paramUUID, int paramInt1, int paramInt2, int paramInt3) {
    initCharacteristic(null, paramUUID, paramInt1, paramInt2, paramInt3);
  }
  
  private void initCharacteristic(BluetoothGattService paramBluetoothGattService, UUID paramUUID, int paramInt1, int paramInt2, int paramInt3) {
    this.mUuid = paramUUID;
    this.mInstance = paramInt1;
    this.mProperties = paramInt2;
    this.mPermissions = paramInt3;
    this.mService = paramBluetoothGattService;
    this.mValue = null;
    this.mDescriptors = new ArrayList<>();
    if ((this.mProperties & 0x4) != 0) {
      this.mWriteType = 1;
    } else {
      this.mWriteType = 2;
    } 
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)new ParcelUuid(this.mUuid), 0);
    paramParcel.writeInt(this.mInstance);
    paramParcel.writeInt(this.mProperties);
    paramParcel.writeInt(this.mPermissions);
    paramParcel.writeInt(this.mKeySize);
    paramParcel.writeInt(this.mWriteType);
    paramParcel.writeTypedList(this.mDescriptors);
  }
  
  public static final Parcelable.Creator<BluetoothGattCharacteristic> CREATOR = new Parcelable.Creator<BluetoothGattCharacteristic>() {
      public BluetoothGattCharacteristic createFromParcel(Parcel param1Parcel) {
        return new BluetoothGattCharacteristic(param1Parcel);
      }
      
      public BluetoothGattCharacteristic[] newArray(int param1Int) {
        return new BluetoothGattCharacteristic[param1Int];
      }
    };
  
  private BluetoothGattCharacteristic(Parcel paramParcel) {
    this.mUuid = ((ParcelUuid)paramParcel.readParcelable(null)).getUuid();
    this.mInstance = paramParcel.readInt();
    this.mProperties = paramParcel.readInt();
    this.mPermissions = paramParcel.readInt();
    this.mKeySize = paramParcel.readInt();
    this.mWriteType = paramParcel.readInt();
    this.mDescriptors = new ArrayList<>();
    Parcelable.Creator<BluetoothGattDescriptor> creator = BluetoothGattDescriptor.CREATOR;
    ArrayList arrayList = paramParcel.createTypedArrayList(creator);
    if (arrayList != null)
      for (BluetoothGattDescriptor bluetoothGattDescriptor : arrayList) {
        bluetoothGattDescriptor.setCharacteristic(this);
        this.mDescriptors.add(bluetoothGattDescriptor);
      }  
  }
  
  public int getKeySize() {
    return this.mKeySize;
  }
  
  public boolean addDescriptor(BluetoothGattDescriptor paramBluetoothGattDescriptor) {
    this.mDescriptors.add(paramBluetoothGattDescriptor);
    paramBluetoothGattDescriptor.setCharacteristic(this);
    return true;
  }
  
  BluetoothGattDescriptor getDescriptor(UUID paramUUID, int paramInt) {
    for (BluetoothGattDescriptor bluetoothGattDescriptor : this.mDescriptors) {
      if (bluetoothGattDescriptor.getUuid().equals(paramUUID) && 
        bluetoothGattDescriptor.getInstanceId() == paramInt)
        return bluetoothGattDescriptor; 
    } 
    return null;
  }
  
  public BluetoothGattService getService() {
    return this.mService;
  }
  
  void setService(BluetoothGattService paramBluetoothGattService) {
    this.mService = paramBluetoothGattService;
  }
  
  public UUID getUuid() {
    return this.mUuid;
  }
  
  public int getInstanceId() {
    return this.mInstance;
  }
  
  public void setInstanceId(int paramInt) {
    this.mInstance = paramInt;
  }
  
  public int getProperties() {
    return this.mProperties;
  }
  
  public int getPermissions() {
    return this.mPermissions;
  }
  
  public int getWriteType() {
    return this.mWriteType;
  }
  
  public void setWriteType(int paramInt) {
    this.mWriteType = paramInt;
  }
  
  public void setKeySize(int paramInt) {
    this.mKeySize = paramInt;
  }
  
  public List<BluetoothGattDescriptor> getDescriptors() {
    return this.mDescriptors;
  }
  
  public BluetoothGattDescriptor getDescriptor(UUID paramUUID) {
    for (BluetoothGattDescriptor bluetoothGattDescriptor : this.mDescriptors) {
      if (bluetoothGattDescriptor.getUuid().equals(paramUUID))
        return bluetoothGattDescriptor; 
    } 
    return null;
  }
  
  public byte[] getValue() {
    return this.mValue;
  }
  
  public Integer getIntValue(int paramInt1, int paramInt2) {
    int i = getTypeLen(paramInt1);
    byte[] arrayOfByte = this.mValue;
    if (i + paramInt2 > arrayOfByte.length)
      return null; 
    if (paramInt1 != 17) {
      if (paramInt1 != 18) {
        if (paramInt1 != 20) {
          if (paramInt1 != 36) {
            if (paramInt1 != 33) {
              if (paramInt1 != 34)
                return null; 
              return Integer.valueOf(unsignedToSigned(unsignedBytesToInt(arrayOfByte[paramInt2], arrayOfByte[paramInt2 + 1]), 16));
            } 
            return Integer.valueOf(unsignedToSigned(unsignedByteToInt(arrayOfByte[paramInt2]), 8));
          } 
          return Integer.valueOf(unsignedToSigned(unsignedBytesToInt(arrayOfByte[paramInt2], arrayOfByte[paramInt2 + 1], arrayOfByte[paramInt2 + 2], arrayOfByte[paramInt2 + 3]), 32));
        } 
        return Integer.valueOf(unsignedBytesToInt(arrayOfByte[paramInt2], arrayOfByte[paramInt2 + 1], arrayOfByte[paramInt2 + 2], arrayOfByte[paramInt2 + 3]));
      } 
      return Integer.valueOf(unsignedBytesToInt(arrayOfByte[paramInt2], arrayOfByte[paramInt2 + 1]));
    } 
    return Integer.valueOf(unsignedByteToInt(arrayOfByte[paramInt2]));
  }
  
  public Float getFloatValue(int paramInt1, int paramInt2) {
    int i = getTypeLen(paramInt1);
    byte[] arrayOfByte = this.mValue;
    if (i + paramInt2 > arrayOfByte.length)
      return null; 
    if (paramInt1 != 50) {
      if (paramInt1 != 52)
        return null; 
      return Float.valueOf(bytesToFloat(arrayOfByte[paramInt2], arrayOfByte[paramInt2 + 1], arrayOfByte[paramInt2 + 2], arrayOfByte[paramInt2 + 3]));
    } 
    return Float.valueOf(bytesToFloat(arrayOfByte[paramInt2], arrayOfByte[paramInt2 + 1]));
  }
  
  public String getStringValue(int paramInt) {
    byte[] arrayOfByte = this.mValue;
    if (arrayOfByte == null || paramInt > arrayOfByte.length)
      return null; 
    arrayOfByte = new byte[arrayOfByte.length - paramInt];
    byte b = 0;
    while (true) {
      byte[] arrayOfByte1 = this.mValue;
      if (b != arrayOfByte1.length - paramInt) {
        arrayOfByte[b] = arrayOfByte1[paramInt + b];
        b++;
        continue;
      } 
      return new String(arrayOfByte);
    } 
  }
  
  public boolean setValue(byte[] paramArrayOfbyte) {
    this.mValue = paramArrayOfbyte;
    return true;
  }
  
  public boolean setValue(int paramInt1, int paramInt2, int paramInt3) {
    // Byte code:
    //   0: aload_0
    //   1: iload_2
    //   2: invokespecial getTypeLen : (I)I
    //   5: iload_3
    //   6: iadd
    //   7: istore #4
    //   9: aload_0
    //   10: getfield mValue : [B
    //   13: ifnonnull -> 24
    //   16: aload_0
    //   17: iload #4
    //   19: newarray byte
    //   21: putfield mValue : [B
    //   24: iload #4
    //   26: aload_0
    //   27: getfield mValue : [B
    //   30: arraylength
    //   31: if_icmple -> 36
    //   34: iconst_0
    //   35: ireturn
    //   36: iload_1
    //   37: istore #4
    //   39: iload_2
    //   40: bipush #17
    //   42: if_icmpeq -> 224
    //   45: iload_1
    //   46: istore #4
    //   48: iload_2
    //   49: bipush #18
    //   51: if_icmpeq -> 188
    //   54: iload_1
    //   55: istore #4
    //   57: iload_2
    //   58: bipush #20
    //   60: if_icmpeq -> 116
    //   63: iload_2
    //   64: bipush #36
    //   66: if_icmpeq -> 107
    //   69: iload_2
    //   70: bipush #33
    //   72: if_icmpeq -> 95
    //   75: iload_2
    //   76: bipush #34
    //   78: if_icmpeq -> 83
    //   81: iconst_0
    //   82: ireturn
    //   83: aload_0
    //   84: iload_1
    //   85: bipush #16
    //   87: invokespecial intToSignedBits : (II)I
    //   90: istore #4
    //   92: goto -> 188
    //   95: aload_0
    //   96: iload_1
    //   97: bipush #8
    //   99: invokespecial intToSignedBits : (II)I
    //   102: istore #4
    //   104: goto -> 224
    //   107: aload_0
    //   108: iload_1
    //   109: bipush #32
    //   111: invokespecial intToSignedBits : (II)I
    //   114: istore #4
    //   116: aload_0
    //   117: getfield mValue : [B
    //   120: astore #5
    //   122: iload_3
    //   123: iconst_1
    //   124: iadd
    //   125: istore_1
    //   126: aload #5
    //   128: iload_3
    //   129: iload #4
    //   131: sipush #255
    //   134: iand
    //   135: i2b
    //   136: bastore
    //   137: iload_1
    //   138: iconst_1
    //   139: iadd
    //   140: istore_2
    //   141: aload #5
    //   143: iload_1
    //   144: iload #4
    //   146: bipush #8
    //   148: ishr
    //   149: sipush #255
    //   152: iand
    //   153: i2b
    //   154: bastore
    //   155: aload #5
    //   157: iload_2
    //   158: iload #4
    //   160: bipush #16
    //   162: ishr
    //   163: sipush #255
    //   166: iand
    //   167: i2b
    //   168: bastore
    //   169: aload #5
    //   171: iload_2
    //   172: iconst_1
    //   173: iadd
    //   174: iload #4
    //   176: bipush #24
    //   178: ishr
    //   179: sipush #255
    //   182: iand
    //   183: i2b
    //   184: bastore
    //   185: goto -> 237
    //   188: aload_0
    //   189: getfield mValue : [B
    //   192: astore #5
    //   194: aload #5
    //   196: iload_3
    //   197: iload #4
    //   199: sipush #255
    //   202: iand
    //   203: i2b
    //   204: bastore
    //   205: aload #5
    //   207: iload_3
    //   208: iconst_1
    //   209: iadd
    //   210: iload #4
    //   212: bipush #8
    //   214: ishr
    //   215: sipush #255
    //   218: iand
    //   219: i2b
    //   220: bastore
    //   221: goto -> 237
    //   224: aload_0
    //   225: getfield mValue : [B
    //   228: iload_3
    //   229: iload #4
    //   231: sipush #255
    //   234: iand
    //   235: i2b
    //   236: bastore
    //   237: iconst_1
    //   238: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #620	-> 0
    //   #621	-> 9
    //   #622	-> 24
    //   #624	-> 36
    //   #651	-> 81
    //   #633	-> 83
    //   #626	-> 95
    //   #641	-> 107
    //   #644	-> 116
    //   #645	-> 137
    //   #646	-> 155
    //   #647	-> 169
    //   #648	-> 185
    //   #636	-> 188
    //   #637	-> 205
    //   #638	-> 221
    //   #629	-> 224
    //   #630	-> 237
    //   #653	-> 237
  }
  
  public boolean setValue(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    int i = getTypeLen(paramInt3) + paramInt4;
    if (this.mValue == null)
      this.mValue = new byte[i]; 
    if (i > this.mValue.length)
      return false; 
    if (paramInt3 != 50) {
      if (paramInt3 != 52)
        return false; 
      paramInt1 = intToSignedBits(paramInt1, 24);
      paramInt2 = intToSignedBits(paramInt2, 8);
      byte[] arrayOfByte = this.mValue;
      paramInt3 = paramInt4 + 1;
      arrayOfByte[paramInt4] = (byte)(paramInt1 & 0xFF);
      paramInt4 = paramInt3 + 1;
      arrayOfByte[paramInt3] = (byte)(paramInt1 >> 8 & 0xFF);
      paramInt3 = paramInt4 + 1;
      arrayOfByte[paramInt4] = (byte)(paramInt1 >> 16 & 0xFF);
      arrayOfByte[paramInt3] = (byte)(arrayOfByte[paramInt3] + (byte)(paramInt2 & 0xFF));
    } else {
      paramInt1 = intToSignedBits(paramInt1, 12);
      paramInt2 = intToSignedBits(paramInt2, 4);
      byte[] arrayOfByte = this.mValue;
      paramInt3 = paramInt4 + 1;
      arrayOfByte[paramInt4] = (byte)(paramInt1 & 0xFF);
      arrayOfByte[paramInt3] = (byte)(paramInt1 >> 8 & 0xF);
      arrayOfByte[paramInt3] = (byte)(arrayOfByte[paramInt3] + (byte)((paramInt2 & 0xF) << 4));
    } 
    return true;
  }
  
  public boolean setValue(String paramString) {
    this.mValue = paramString.getBytes();
    return true;
  }
  
  private int getTypeLen(int paramInt) {
    return paramInt & 0xF;
  }
  
  private int unsignedByteToInt(byte paramByte) {
    return paramByte & 0xFF;
  }
  
  private int unsignedBytesToInt(byte paramByte1, byte paramByte2) {
    return unsignedByteToInt(paramByte1) + (unsignedByteToInt(paramByte2) << 8);
  }
  
  private int unsignedBytesToInt(byte paramByte1, byte paramByte2, byte paramByte3, byte paramByte4) {
    int i = unsignedByteToInt(paramByte1), j = unsignedByteToInt(paramByte2);
    int k = unsignedByteToInt(paramByte3), m = unsignedByteToInt(paramByte4);
    return i + (j << 8) + (k << 16) + (m << 24);
  }
  
  private float bytesToFloat(byte paramByte1, byte paramByte2) {
    int i = unsignedByteToInt(paramByte1);
    int j = unsignedByteToInt(paramByte2);
    i = unsignedToSigned(i + ((j & 0xF) << 8), 12);
    j = unsignedToSigned(unsignedByteToInt(paramByte2) >> 4, 4);
    return (float)(i * Math.pow(10.0D, j));
  }
  
  private float bytesToFloat(byte paramByte1, byte paramByte2, byte paramByte3, byte paramByte4) {
    int i = unsignedByteToInt(paramByte1);
    int j = unsignedByteToInt(paramByte2);
    int k = unsignedByteToInt(paramByte3);
    i = unsignedToSigned(i + (j << 8) + (k << 16), 24);
    return (float)(i * Math.pow(10.0D, paramByte4));
  }
  
  private int unsignedToSigned(int paramInt1, int paramInt2) {
    int i = paramInt1;
    if ((1 << paramInt2 - 1 & paramInt1) != 0)
      i = ((1 << paramInt2 - 1) - (paramInt1 & (1 << paramInt2 - 1) - 1)) * -1; 
    return i;
  }
  
  private int intToSignedBits(int paramInt1, int paramInt2) {
    int i = paramInt1;
    if (paramInt1 < 0)
      i = (1 << paramInt2 - 1) + (paramInt1 & (1 << paramInt2 - 1) - 1); 
    return i;
  }
}
