package android.bluetooth;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IBluetoothAvrcpController extends IInterface {
  List<BluetoothDevice> getConnectedDevices() throws RemoteException;
  
  int getConnectionState(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] paramArrayOfint) throws RemoteException;
  
  BluetoothAvrcpPlayerSettings getPlayerSettings(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  void sendGroupNavigationCmd(BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2) throws RemoteException;
  
  boolean setPlayerApplicationSetting(BluetoothAvrcpPlayerSettings paramBluetoothAvrcpPlayerSettings) throws RemoteException;
  
  class Default implements IBluetoothAvrcpController {
    public List<BluetoothDevice> getConnectedDevices() throws RemoteException {
      return null;
    }
    
    public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public int getConnectionState(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return 0;
    }
    
    public BluetoothAvrcpPlayerSettings getPlayerSettings(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return null;
    }
    
    public boolean setPlayerApplicationSetting(BluetoothAvrcpPlayerSettings param1BluetoothAvrcpPlayerSettings) throws RemoteException {
      return false;
    }
    
    public void sendGroupNavigationCmd(BluetoothDevice param1BluetoothDevice, int param1Int1, int param1Int2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IBluetoothAvrcpController {
    private static final String DESCRIPTOR = "android.bluetooth.IBluetoothAvrcpController";
    
    static final int TRANSACTION_getConnectedDevices = 1;
    
    static final int TRANSACTION_getConnectionState = 3;
    
    static final int TRANSACTION_getDevicesMatchingConnectionStates = 2;
    
    static final int TRANSACTION_getPlayerSettings = 4;
    
    static final int TRANSACTION_sendGroupNavigationCmd = 6;
    
    static final int TRANSACTION_setPlayerApplicationSetting = 5;
    
    public Stub() {
      attachInterface(this, "android.bluetooth.IBluetoothAvrcpController");
    }
    
    public static IBluetoothAvrcpController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.bluetooth.IBluetoothAvrcpController");
      if (iInterface != null && iInterface instanceof IBluetoothAvrcpController)
        return (IBluetoothAvrcpController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "sendGroupNavigationCmd";
        case 5:
          return "setPlayerApplicationSetting";
        case 4:
          return "getPlayerSettings";
        case 3:
          return "getConnectionState";
        case 2:
          return "getDevicesMatchingConnectionStates";
        case 1:
          break;
      } 
      return "getConnectedDevices";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        int i;
        BluetoothAvrcpPlayerSettings bluetoothAvrcpPlayerSettings;
        int[] arrayOfInt;
        BluetoothDevice bluetoothDevice;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothAvrcpController");
            if (param1Parcel1.readInt() != 0) {
              bluetoothDevice = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(param1Parcel1);
            } else {
              bluetoothDevice = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            sendGroupNavigationCmd(bluetoothDevice, param1Int1, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothAvrcpController");
            if (param1Parcel1.readInt() != 0) {
              BluetoothAvrcpPlayerSettings bluetoothAvrcpPlayerSettings1 = (BluetoothAvrcpPlayerSettings)BluetoothAvrcpPlayerSettings.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            bool = setPlayerApplicationSetting((BluetoothAvrcpPlayerSettings)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothAvrcpController");
            if (param1Parcel1.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            bluetoothAvrcpPlayerSettings = getPlayerSettings((BluetoothDevice)param1Parcel1);
            param1Parcel2.writeNoException();
            if (bluetoothAvrcpPlayerSettings != null) {
              param1Parcel2.writeInt(1);
              bluetoothAvrcpPlayerSettings.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            bluetoothAvrcpPlayerSettings.enforceInterface("android.bluetooth.IBluetoothAvrcpController");
            if (bluetoothAvrcpPlayerSettings.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)bluetoothAvrcpPlayerSettings);
            } else {
              bluetoothAvrcpPlayerSettings = null;
            } 
            i = getConnectionState((BluetoothDevice)bluetoothAvrcpPlayerSettings);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 2:
            bluetoothAvrcpPlayerSettings.enforceInterface("android.bluetooth.IBluetoothAvrcpController");
            arrayOfInt = bluetoothAvrcpPlayerSettings.createIntArray();
            list = getDevicesMatchingConnectionStates(arrayOfInt);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 1:
            break;
        } 
        list.enforceInterface("android.bluetooth.IBluetoothAvrcpController");
        List<BluetoothDevice> list = getConnectedDevices();
        param1Parcel2.writeNoException();
        param1Parcel2.writeTypedList(list);
        return true;
      } 
      param1Parcel2.writeString("android.bluetooth.IBluetoothAvrcpController");
      return true;
    }
    
    private static class Proxy implements IBluetoothAvrcpController {
      public static IBluetoothAvrcpController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.bluetooth.IBluetoothAvrcpController";
      }
      
      public List<BluetoothDevice> getConnectedDevices() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothAvrcpController");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IBluetoothAvrcpController.Stub.getDefaultImpl() != null)
            return IBluetoothAvrcpController.Stub.getDefaultImpl().getConnectedDevices(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(BluetoothDevice.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothAvrcpController");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IBluetoothAvrcpController.Stub.getDefaultImpl() != null)
            return IBluetoothAvrcpController.Stub.getDefaultImpl().getDevicesMatchingConnectionStates(param2ArrayOfint); 
          parcel2.readException();
          return parcel2.createTypedArrayList(BluetoothDevice.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getConnectionState(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothAvrcpController");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IBluetoothAvrcpController.Stub.getDefaultImpl() != null)
            return IBluetoothAvrcpController.Stub.getDefaultImpl().getConnectionState(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public BluetoothAvrcpPlayerSettings getPlayerSettings(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothAvrcpController");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IBluetoothAvrcpController.Stub.getDefaultImpl() != null)
            return IBluetoothAvrcpController.Stub.getDefaultImpl().getPlayerSettings(param2BluetoothDevice); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            BluetoothAvrcpPlayerSettings bluetoothAvrcpPlayerSettings = (BluetoothAvrcpPlayerSettings)BluetoothAvrcpPlayerSettings.CREATOR.createFromParcel(parcel2);
          } else {
            param2BluetoothDevice = null;
          } 
          return (BluetoothAvrcpPlayerSettings)param2BluetoothDevice;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setPlayerApplicationSetting(BluetoothAvrcpPlayerSettings param2BluetoothAvrcpPlayerSettings) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothAvrcpController");
          boolean bool1 = true;
          if (param2BluetoothAvrcpPlayerSettings != null) {
            parcel1.writeInt(1);
            param2BluetoothAvrcpPlayerSettings.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothAvrcpController.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothAvrcpController.Stub.getDefaultImpl().setPlayerApplicationSetting(param2BluetoothAvrcpPlayerSettings);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendGroupNavigationCmd(BluetoothDevice param2BluetoothDevice, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothAvrcpController");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IBluetoothAvrcpController.Stub.getDefaultImpl() != null) {
            IBluetoothAvrcpController.Stub.getDefaultImpl().sendGroupNavigationCmd(param2BluetoothDevice, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IBluetoothAvrcpController param1IBluetoothAvrcpController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IBluetoothAvrcpController != null) {
          Proxy.sDefaultImpl = param1IBluetoothAvrcpController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IBluetoothAvrcpController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
