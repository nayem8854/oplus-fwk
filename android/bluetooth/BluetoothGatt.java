package android.bluetooth;

import android.os.Handler;
import android.os.ParcelUuid;
import android.os.RemoteException;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class BluetoothGatt implements BluetoothProfile {
  private int mTransport;
  
  private final Object mStateLock = new Object();
  
  private List<BluetoothGattService> mServices;
  
  private IBluetoothGatt mService;
  
  private int mPhy;
  
  private boolean mOpportunistic;
  
  private Handler mHandler;
  
  private final Object mDeviceBusyLock = new Object();
  
  private Boolean mDeviceBusy = Boolean.valueOf(false);
  
  private BluetoothDevice mDevice;
  
  private int mConnState;
  
  private int mClientIf;
  
  private volatile BluetoothGattCallback mCallback;
  
  private final IBluetoothGattCallback mBluetoothGattCallback = (IBluetoothGattCallback)new Object(this);
  
  private boolean mAutoConnect;
  
  private int mAuthRetryState;
  
  private static final boolean VDBG = false;
  
  private static final String TAG = "BluetoothGatt";
  
  public static final int GATT_WRITE_NOT_PERMITTED = 3;
  
  public static final int GATT_SUCCESS = 0;
  
  public static final int GATT_REQUEST_NOT_SUPPORTED = 6;
  
  public static final int GATT_READ_NOT_PERMITTED = 2;
  
  public static final int GATT_INVALID_OFFSET = 7;
  
  public static final int GATT_INVALID_ATTRIBUTE_LENGTH = 13;
  
  public static final int GATT_INSUFFICIENT_ENCRYPTION = 15;
  
  public static final int GATT_INSUFFICIENT_AUTHENTICATION = 5;
  
  public static final int GATT_FAILURE = 257;
  
  public static final int GATT_CONNECTION_CONGESTED = 143;
  
  private static final boolean DBG = true;
  
  private static final int CONN_STATE_IDLE = 0;
  
  private static final int CONN_STATE_DISCONNECTING = 3;
  
  private static final int CONN_STATE_CONNECTING = 1;
  
  private static final int CONN_STATE_CONNECTED = 2;
  
  private static final int CONN_STATE_CLOSED = 4;
  
  public static final int CONNECTION_PRIORITY_LOW_POWER = 2;
  
  public static final int CONNECTION_PRIORITY_HIGH = 1;
  
  public static final int CONNECTION_PRIORITY_BALANCED = 0;
  
  private static final int AUTH_RETRY_STATE_NO_MITM = 1;
  
  private static final int AUTH_RETRY_STATE_MITM = 2;
  
  private static final int AUTH_RETRY_STATE_IDLE = 0;
  
  static final int AUTHENTICATION_NO_MITM = 1;
  
  static final int AUTHENTICATION_NONE = 0;
  
  static final int AUTHENTICATION_MITM = 2;
  
  BluetoothGatt(IBluetoothGatt paramIBluetoothGatt, BluetoothDevice paramBluetoothDevice, int paramInt1, boolean paramBoolean, int paramInt2) {
    this.mService = paramIBluetoothGatt;
    this.mDevice = paramBluetoothDevice;
    this.mTransport = paramInt1;
    this.mPhy = paramInt2;
    this.mOpportunistic = paramBoolean;
    this.mServices = new ArrayList<>();
    this.mConnState = 0;
    this.mAuthRetryState = 0;
  }
  
  public void close() {
    Log.d("BluetoothGatt", "close()");
    unregisterApp();
    this.mConnState = 4;
    this.mAuthRetryState = 0;
  }
  
  BluetoothGattService getService(BluetoothDevice paramBluetoothDevice, UUID paramUUID, int paramInt) {
    for (BluetoothGattService bluetoothGattService : this.mServices) {
      if (bluetoothGattService.getDevice().equals(paramBluetoothDevice) && 
        bluetoothGattService.getInstanceId() == paramInt && 
        bluetoothGattService.getUuid().equals(paramUUID))
        return bluetoothGattService; 
    } 
    return null;
  }
  
  BluetoothGattCharacteristic getCharacteristicById(BluetoothDevice paramBluetoothDevice, int paramInt) {
    for (BluetoothGattService bluetoothGattService : this.mServices) {
      for (BluetoothGattCharacteristic bluetoothGattCharacteristic : bluetoothGattService.getCharacteristics()) {
        if (bluetoothGattCharacteristic.getInstanceId() == paramInt)
          return bluetoothGattCharacteristic; 
      } 
    } 
    return null;
  }
  
  BluetoothGattDescriptor getDescriptorById(BluetoothDevice paramBluetoothDevice, int paramInt) {
    for (BluetoothGattService bluetoothGattService : this.mServices) {
      for (BluetoothGattCharacteristic bluetoothGattCharacteristic : bluetoothGattService.getCharacteristics()) {
        for (BluetoothGattDescriptor bluetoothGattDescriptor : bluetoothGattCharacteristic.getDescriptors()) {
          if (bluetoothGattDescriptor.getInstanceId() == paramInt)
            return bluetoothGattDescriptor; 
        } 
      } 
    } 
    return null;
  }
  
  private void runOrQueueCallback(Runnable paramRunnable) {
    Handler handler = this.mHandler;
    if (handler == null) {
      try {
        paramRunnable.run();
      } catch (Exception exception) {
        Log.w("BluetoothGatt", "Unhandled exception in callback", exception);
      } 
    } else {
      handler.post((Runnable)exception);
    } 
  }
  
  private boolean registerApp(BluetoothGattCallback paramBluetoothGattCallback, Handler paramHandler) {
    Log.d("BluetoothGatt", "registerApp()");
    if (this.mService == null)
      return false; 
    this.mCallback = paramBluetoothGattCallback;
    this.mHandler = paramHandler;
    UUID uUID = UUID.randomUUID();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("registerApp() - UUID=");
    stringBuilder.append(uUID);
    Log.d("BluetoothGatt", stringBuilder.toString());
    try {
      IBluetoothGatt iBluetoothGatt = this.mService;
      ParcelUuid parcelUuid = new ParcelUuid();
      this(uUID);
      iBluetoothGatt.registerClient(parcelUuid, this.mBluetoothGattCallback);
      return true;
    } catch (RemoteException remoteException) {
      Log.e("BluetoothGatt", "", (Throwable)remoteException);
      return false;
    } 
  }
  
  private void unregisterApp() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("unregisterApp() - mClientIf=");
    stringBuilder.append(this.mClientIf);
    Log.d("BluetoothGatt", stringBuilder.toString());
    if (this.mService == null || this.mClientIf == 0)
      return; 
    try {
      this.mCallback = null;
      this.mService.unregisterClient(this.mClientIf);
      this.mClientIf = 0;
    } catch (RemoteException remoteException) {
      Log.e("BluetoothGatt", "", (Throwable)remoteException);
    } 
  }
  
  boolean connect(Boolean paramBoolean, BluetoothGattCallback paramBluetoothGattCallback, Handler paramHandler) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("connect() - device: ");
    BluetoothDevice bluetoothDevice = this.mDevice;
    stringBuilder.append(bluetoothDevice.getAddress());
    stringBuilder.append(", auto: ");
    stringBuilder.append(paramBoolean);
    String str = stringBuilder.toString();
    Log.d("BluetoothGatt", str);
    synchronized (this.mStateLock) {
      if (this.mConnState == 0) {
        this.mConnState = 1;
        this.mAutoConnect = paramBoolean.booleanValue();
        if (!registerApp(paramBluetoothGattCallback, paramHandler))
          synchronized (this.mStateLock) {
            this.mConnState = 0;
            Log.e("BluetoothGatt", "Failed to register callback");
            return false;
          }  
        return true;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Not idle");
      throw illegalStateException;
    } 
  }
  
  public void disconnect() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("cancelOpen() - device: ");
    stringBuilder.append(this.mDevice.getAddress());
    Log.d("BluetoothGatt", stringBuilder.toString());
    IBluetoothGatt iBluetoothGatt = this.mService;
    if (iBluetoothGatt != null) {
      int i = this.mClientIf;
      if (i != 0) {
        try {
          iBluetoothGatt.clientDisconnect(i, this.mDevice.getAddress());
        } catch (RemoteException remoteException) {
          Log.e("BluetoothGatt", "", (Throwable)remoteException);
        } 
        return;
      } 
    } 
  }
  
  public boolean connect() {
    try {
      this.mService.clientConnect(this.mClientIf, this.mDevice.getAddress(), false, this.mTransport, this.mOpportunistic, this.mPhy);
      return true;
    } catch (RemoteException remoteException) {
      Log.e("BluetoothGatt", "", (Throwable)remoteException);
      return false;
    } 
  }
  
  public void setPreferredPhy(int paramInt1, int paramInt2, int paramInt3) {
    try {
      this.mService.clientSetPreferredPhy(this.mClientIf, this.mDevice.getAddress(), paramInt1, paramInt2, paramInt3);
    } catch (RemoteException remoteException) {
      Log.e("BluetoothGatt", "", (Throwable)remoteException);
    } 
  }
  
  public void readPhy() {
    try {
      this.mService.clientReadPhy(this.mClientIf, this.mDevice.getAddress());
    } catch (RemoteException remoteException) {
      Log.e("BluetoothGatt", "", (Throwable)remoteException);
    } 
  }
  
  public BluetoothDevice getDevice() {
    return this.mDevice;
  }
  
  public boolean discoverServices() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("discoverServices() - device: ");
    stringBuilder.append(this.mDevice.getAddress());
    Log.d("BluetoothGatt", stringBuilder.toString());
    if (this.mService == null || this.mClientIf == 0)
      return false; 
    this.mServices.clear();
    try {
      this.mService.discoverServices(this.mClientIf, this.mDevice.getAddress());
      return true;
    } catch (RemoteException remoteException) {
      Log.e("BluetoothGatt", "", (Throwable)remoteException);
      return false;
    } 
  }
  
  public boolean discoverServiceByUuid(UUID paramUUID) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("discoverServiceByUuid() - device: ");
    stringBuilder.append(this.mDevice.getAddress());
    Log.d("BluetoothGatt", stringBuilder.toString());
    if (this.mService == null || this.mClientIf == 0)
      return false; 
    this.mServices.clear();
    try {
      IBluetoothGatt iBluetoothGatt = this.mService;
      int i = this.mClientIf;
      String str = this.mDevice.getAddress();
      ParcelUuid parcelUuid = new ParcelUuid();
      this(paramUUID);
      iBluetoothGatt.discoverServiceByUuid(i, str, parcelUuid);
      return true;
    } catch (RemoteException remoteException) {
      Log.e("BluetoothGatt", "", (Throwable)remoteException);
      return false;
    } 
  }
  
  public List<BluetoothGattService> getServices() {
    ArrayList<BluetoothGattService> arrayList = new ArrayList();
    for (BluetoothGattService bluetoothGattService : this.mServices) {
      if (bluetoothGattService.getDevice().equals(this.mDevice))
        arrayList.add(bluetoothGattService); 
    } 
    return arrayList;
  }
  
  public BluetoothGattService getService(UUID paramUUID) {
    for (BluetoothGattService bluetoothGattService : this.mServices) {
      if (bluetoothGattService.getDevice().equals(this.mDevice) && bluetoothGattService.getUuid().equals(paramUUID))
        return bluetoothGattService; 
    } 
    return null;
  }
  
  public boolean readCharacteristic(BluetoothGattCharacteristic paramBluetoothGattCharacteristic) {
    if ((paramBluetoothGattCharacteristic.getProperties() & 0x2) == 0)
      return false; 
    if (this.mService == null || this.mClientIf == 0)
      return false; 
    BluetoothGattService bluetoothGattService = paramBluetoothGattCharacteristic.getService();
    if (bluetoothGattService == null)
      return false; 
    BluetoothDevice bluetoothDevice = bluetoothGattService.getDevice();
    if (bluetoothDevice == null)
      return false; 
    synchronized (this.mDeviceBusyLock) {
      if (this.mDeviceBusy.booleanValue())
        return false; 
      this.mDeviceBusy = Boolean.valueOf(true);
      try {
        null = this.mService;
        int i = this.mClientIf;
        String str = bluetoothDevice.getAddress();
        int j = paramBluetoothGattCharacteristic.getInstanceId();
        null.readCharacteristic(i, str, j, 0);
        return true;
      } catch (RemoteException remoteException) {
        Log.e("BluetoothGatt", "", (Throwable)remoteException);
        this.mDeviceBusy = Boolean.valueOf(false);
        return false;
      } 
    } 
  }
  
  public boolean readUsingCharacteristicUuid(UUID paramUUID, int paramInt1, int paramInt2) {
    if (this.mService == null || this.mClientIf == 0)
      return false; 
    synchronized (this.mDeviceBusyLock) {
      if (this.mDeviceBusy.booleanValue())
        return false; 
      this.mDeviceBusy = Boolean.valueOf(true);
      try {
        IBluetoothGatt iBluetoothGatt = this.mService;
        int i = this.mClientIf;
        String str = this.mDevice.getAddress();
        null = new ParcelUuid();
        super(paramUUID);
        iBluetoothGatt.readUsingCharacteristicUuid(i, str, (ParcelUuid)null, paramInt1, paramInt2, 0);
        return true;
      } catch (RemoteException remoteException) {
        Log.e("BluetoothGatt", "", (Throwable)remoteException);
        this.mDeviceBusy = Boolean.valueOf(false);
        return false;
      } 
    } 
  }
  
  public boolean writeCharacteristic(BluetoothGattCharacteristic paramBluetoothGattCharacteristic) {
    if ((paramBluetoothGattCharacteristic.getProperties() & 0x8) == 0 && (
      paramBluetoothGattCharacteristic.getProperties() & 0x4) == 0)
      return false; 
    if (this.mService == null || this.mClientIf == 0 || paramBluetoothGattCharacteristic.getValue() == null)
      return false; 
    BluetoothGattService bluetoothGattService = paramBluetoothGattCharacteristic.getService();
    if (bluetoothGattService == null)
      return false; 
    BluetoothDevice bluetoothDevice = bluetoothGattService.getDevice();
    if (bluetoothDevice == null)
      return false; 
    synchronized (this.mDeviceBusyLock) {
      if (this.mDeviceBusy.booleanValue())
        return false; 
      this.mDeviceBusy = Boolean.valueOf(true);
      try {
        null = this.mService;
        int i = this.mClientIf;
        String str = bluetoothDevice.getAddress();
        int j = paramBluetoothGattCharacteristic.getInstanceId(), k = paramBluetoothGattCharacteristic.getWriteType();
        byte[] arrayOfByte = paramBluetoothGattCharacteristic.getValue();
        null.writeCharacteristic(i, str, j, k, 0, arrayOfByte);
        return true;
      } catch (RemoteException remoteException) {
        Log.e("BluetoothGatt", "", (Throwable)remoteException);
        this.mDeviceBusy = Boolean.valueOf(false);
        return false;
      } 
    } 
  }
  
  public boolean readDescriptor(BluetoothGattDescriptor paramBluetoothGattDescriptor) {
    if (this.mService == null || this.mClientIf == 0)
      return false; 
    BluetoothGattCharacteristic bluetoothGattCharacteristic = paramBluetoothGattDescriptor.getCharacteristic();
    if (bluetoothGattCharacteristic == null)
      return false; 
    BluetoothGattService bluetoothGattService = bluetoothGattCharacteristic.getService();
    if (bluetoothGattService == null)
      return false; 
    BluetoothDevice bluetoothDevice = bluetoothGattService.getDevice();
    if (bluetoothDevice == null)
      return false; 
    synchronized (this.mDeviceBusyLock) {
      if (this.mDeviceBusy.booleanValue())
        return false; 
      this.mDeviceBusy = Boolean.valueOf(true);
      try {
        null = this.mService;
        int i = this.mClientIf;
        String str = bluetoothDevice.getAddress();
        int j = paramBluetoothGattDescriptor.getInstanceId();
        null.readDescriptor(i, str, j, 0);
        return true;
      } catch (RemoteException remoteException) {
        Log.e("BluetoothGatt", "", (Throwable)remoteException);
        this.mDeviceBusy = Boolean.valueOf(false);
        return false;
      } 
    } 
  }
  
  public boolean writeDescriptor(BluetoothGattDescriptor paramBluetoothGattDescriptor) {
    if (this.mService == null || this.mClientIf == 0 || paramBluetoothGattDescriptor.getValue() == null)
      return false; 
    BluetoothGattCharacteristic bluetoothGattCharacteristic = paramBluetoothGattDescriptor.getCharacteristic();
    if (bluetoothGattCharacteristic == null)
      return false; 
    BluetoothGattService bluetoothGattService = bluetoothGattCharacteristic.getService();
    if (bluetoothGattService == null)
      return false; 
    BluetoothDevice bluetoothDevice = bluetoothGattService.getDevice();
    if (bluetoothDevice == null)
      return false; 
    synchronized (this.mDeviceBusyLock) {
      if (this.mDeviceBusy.booleanValue())
        return false; 
      this.mDeviceBusy = Boolean.valueOf(true);
      try {
        null = this.mService;
        int i = this.mClientIf;
        String str = bluetoothDevice.getAddress();
        int j = paramBluetoothGattDescriptor.getInstanceId();
        byte[] arrayOfByte = paramBluetoothGattDescriptor.getValue();
        null.writeDescriptor(i, str, j, 0, arrayOfByte);
        return true;
      } catch (RemoteException remoteException) {
        Log.e("BluetoothGatt", "", (Throwable)remoteException);
        this.mDeviceBusy = Boolean.valueOf(false);
        return false;
      } 
    } 
  }
  
  public boolean beginReliableWrite() {
    IBluetoothGatt iBluetoothGatt = this.mService;
    if (iBluetoothGatt != null) {
      int i = this.mClientIf;
      if (i != 0)
        try {
          iBluetoothGatt.beginReliableWrite(i, this.mDevice.getAddress());
          return true;
        } catch (RemoteException remoteException) {
          Log.e("BluetoothGatt", "", (Throwable)remoteException);
          return false;
        }  
    } 
    return false;
  }
  
  public boolean executeReliableWrite() {
    if (this.mService == null || this.mClientIf == 0)
      return false; 
    synchronized (this.mDeviceBusyLock) {
      if (this.mDeviceBusy.booleanValue())
        return false; 
      this.mDeviceBusy = Boolean.valueOf(true);
      try {
        this.mService.endReliableWrite(this.mClientIf, this.mDevice.getAddress(), true);
        return true;
      } catch (RemoteException remoteException) {
        Log.e("BluetoothGatt", "", (Throwable)remoteException);
        this.mDeviceBusy = Boolean.valueOf(false);
        return false;
      } 
    } 
  }
  
  public void abortReliableWrite() {
    IBluetoothGatt iBluetoothGatt = this.mService;
    if (iBluetoothGatt != null) {
      int i = this.mClientIf;
      if (i != 0) {
        try {
          iBluetoothGatt.endReliableWrite(i, this.mDevice.getAddress(), false);
        } catch (RemoteException remoteException) {
          Log.e("BluetoothGatt", "", (Throwable)remoteException);
        } 
        return;
      } 
    } 
  }
  
  @Deprecated
  public void abortReliableWrite(BluetoothDevice paramBluetoothDevice) {
    abortReliableWrite();
  }
  
  public boolean setCharacteristicNotification(BluetoothGattCharacteristic paramBluetoothGattCharacteristic, boolean paramBoolean) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("setCharacteristicNotification() - uuid: ");
    stringBuilder.append(paramBluetoothGattCharacteristic.getUuid());
    stringBuilder.append(" enable: ");
    stringBuilder.append(paramBoolean);
    Log.d("BluetoothGatt", stringBuilder.toString());
    if (this.mService == null || this.mClientIf == 0)
      return false; 
    BluetoothGattService bluetoothGattService = paramBluetoothGattCharacteristic.getService();
    if (bluetoothGattService == null)
      return false; 
    BluetoothDevice bluetoothDevice = bluetoothGattService.getDevice();
    if (bluetoothDevice == null)
      return false; 
    try {
      IBluetoothGatt iBluetoothGatt = this.mService;
      int i = this.mClientIf;
      String str = bluetoothDevice.getAddress();
      int j = paramBluetoothGattCharacteristic.getInstanceId();
      iBluetoothGatt.registerForNotification(i, str, j, paramBoolean);
      return true;
    } catch (RemoteException remoteException) {
      Log.e("BluetoothGatt", "", (Throwable)remoteException);
      return false;
    } 
  }
  
  public boolean refresh() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("refresh() - device: ");
    stringBuilder.append(this.mDevice.getAddress());
    Log.d("BluetoothGatt", stringBuilder.toString());
    IBluetoothGatt iBluetoothGatt = this.mService;
    if (iBluetoothGatt != null) {
      int i = this.mClientIf;
      if (i != 0)
        try {
          iBluetoothGatt.refreshDevice(i, this.mDevice.getAddress());
          return true;
        } catch (RemoteException remoteException) {
          Log.e("BluetoothGatt", "", (Throwable)remoteException);
          return false;
        }  
    } 
    return false;
  }
  
  public boolean readRemoteRssi() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("readRssi() - device: ");
    stringBuilder.append(this.mDevice.getAddress());
    Log.d("BluetoothGatt", stringBuilder.toString());
    IBluetoothGatt iBluetoothGatt = this.mService;
    if (iBluetoothGatt != null) {
      int i = this.mClientIf;
      if (i != 0)
        try {
          iBluetoothGatt.readRemoteRssi(i, this.mDevice.getAddress());
          return true;
        } catch (RemoteException remoteException) {
          Log.e("BluetoothGatt", "", (Throwable)remoteException);
          return false;
        }  
    } 
    return false;
  }
  
  public boolean requestMtu(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("configureMTU() - device: ");
    stringBuilder.append(this.mDevice.getAddress());
    stringBuilder.append(" mtu: ");
    stringBuilder.append(paramInt);
    Log.d("BluetoothGatt", stringBuilder.toString());
    IBluetoothGatt iBluetoothGatt = this.mService;
    if (iBluetoothGatt != null) {
      int i = this.mClientIf;
      if (i != 0)
        try {
          iBluetoothGatt.configureMTU(i, this.mDevice.getAddress(), paramInt);
          return true;
        } catch (RemoteException remoteException) {
          Log.e("BluetoothGatt", "", (Throwable)remoteException);
          return false;
        }  
    } 
    return false;
  }
  
  public boolean requestConnectionPriority(int paramInt) {
    if (paramInt >= 0 && paramInt <= 2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("requestConnectionPriority() - params: ");
      stringBuilder.append(paramInt);
      Log.d("BluetoothGatt", stringBuilder.toString());
      IBluetoothGatt iBluetoothGatt = this.mService;
      if (iBluetoothGatt != null) {
        int i = this.mClientIf;
        if (i != 0)
          try {
            iBluetoothGatt.connectionParameterUpdate(i, this.mDevice.getAddress(), paramInt);
            return true;
          } catch (RemoteException remoteException) {
            Log.e("BluetoothGatt", "", (Throwable)remoteException);
            return false;
          }  
      } 
      return false;
    } 
    throw new IllegalArgumentException("connectionPriority not within valid range");
  }
  
  public boolean requestLeConnectionUpdate(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("requestLeConnectionUpdate() - min=(");
    stringBuilder.append(paramInt1);
    stringBuilder.append(")");
    stringBuilder.append(paramInt1 * 1.25D);
    stringBuilder.append("msec, max=(");
    stringBuilder.append(paramInt2);
    stringBuilder.append(")");
    stringBuilder.append(paramInt2 * 1.25D);
    stringBuilder.append("msec, latency=");
    stringBuilder.append(paramInt3);
    stringBuilder.append(", timeout=");
    stringBuilder.append(paramInt4);
    stringBuilder.append("msec, min_ce=");
    stringBuilder.append(paramInt5);
    stringBuilder.append(", max_ce=");
    stringBuilder.append(paramInt6);
    Log.d("BluetoothGatt", stringBuilder.toString());
    IBluetoothGatt iBluetoothGatt = this.mService;
    if (iBluetoothGatt != null) {
      int i = this.mClientIf;
      if (i != 0) {
        try {
          String str = this.mDevice.getAddress();
          try {
            iBluetoothGatt.leConnectionUpdate(i, str, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
            return true;
          } catch (RemoteException null) {}
        } catch (RemoteException remoteException) {}
        Log.e("BluetoothGatt", "", (Throwable)remoteException);
        return false;
      } 
    } 
    return false;
  }
  
  public int getConnectionState(BluetoothDevice paramBluetoothDevice) {
    throw new UnsupportedOperationException("Use BluetoothManager#getConnectionState instead.");
  }
  
  public List<BluetoothDevice> getConnectedDevices() {
    throw new UnsupportedOperationException("Use BluetoothManager#getConnectedDevices instead.");
  }
  
  public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] paramArrayOfint) {
    throw new UnsupportedOperationException("Use BluetoothManager#getDevicesMatchingConnectionStates instead.");
  }
}
