package android.bluetooth.feature.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONObject;

public final class OplusBtDcsUtils {
  private static final String APP_ID = "appId";
  
  private static final String APP_NAME = "appName";
  
  private static final String APP_PACKAGE = "appPackage";
  
  private static final String APP_VERSION = "appVersion";
  
  private static final int COMMON = 1006;
  
  private static final int COMMON_LIST = 1010;
  
  private static final String DATA_TYPE = "dataType";
  
  private static final boolean DBG;
  
  public static final int ENABLE_DISABLE_REASON_AIRPLANE_MODE = 2;
  
  public static final int ENABLE_DISABLE_REASON_APPLICATION_REQUEST = 1;
  
  public static final int ENABLE_DISABLE_REASON_CRASH = 7;
  
  public static final int ENABLE_DISABLE_REASON_DISALLOWED = 3;
  
  public static final int ENABLE_DISABLE_REASON_FACTORY_RESET = 10;
  
  public static final int ENABLE_DISABLE_REASON_RESTARTED = 4;
  
  public static final int ENABLE_DISABLE_REASON_RESTORE_USER_SETTING = 9;
  
  public static final int ENABLE_DISABLE_REASON_START_ERROR = 5;
  
  public static final int ENABLE_DISABLE_REASON_SYSTEM_BOOT = 6;
  
  public static final int ENABLE_DISABLE_REASON_UNSPECIFIED = 0;
  
  public static final int ENABLE_DISABLE_REASON_USER_SWITCH = 8;
  
  private static final String EVENT_ID = "eventID";
  
  public static final int GENERAL_APP_CALL_REASON_DEFAULT = 999;
  
  private static final String LOG_MAP = "logMap";
  
  private static final String LOG_TAG = "logTag";
  
  private static final String MAP_LIST = "mapList";
  
  private static final String PKG_NAME_DCS = "com.nearme.statistics.rom";
  
  private static final String SERVICE_NAME_DCS = "com.nearme.statistics.rom.service.ReceiverService";
  
  private static final String SSOID = "ssoid";
  
  private static final String SYSTEM = "system";
  
  private static final String TAG = "OplusBtDcsUtils_fwk";
  
  private static final String UPLOAD_NOW = "uploadNow";
  
  static {
    boolean bool = false;
    if (!SystemProperties.getBoolean("ro.build.release_type", false) || 
      SystemProperties.getBoolean("persist.sys.assert.panic", false))
      bool = true; 
    DBG = bool;
  }
  
  private static int sAppId = 20131;
  
  private static ExecutorService sSingleThreadExecutor = Executors.newSingleThreadExecutor();
  
  public static void onCommon(final Context context, final String logTag, final String eventId, final Map<String, String> cloneMap, final boolean uploadNow) {
    if (context == null) {
      Log.w("OplusBtDcsUtils_fwk", "onCommon: context is null!");
      return;
    } 
    if (DBG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onCommon begin: logTag=");
      stringBuilder.append(logTag);
      stringBuilder.append(", eventId=");
      stringBuilder.append(eventId);
      stringBuilder.append(", logMap=");
      stringBuilder.append(cloneMap);
      stringBuilder.append(", uploadNow=");
      stringBuilder.append(uploadNow);
      Log.d("OplusBtDcsUtils_fwk", stringBuilder.toString());
    } 
    if (TextUtils.isEmpty(logTag))
      return; 
    if (cloneMap != null) {
      cloneMap = new HashMap<>(cloneMap);
    } else {
      cloneMap = new HashMap<>();
    } 
    Runnable runnable = new Runnable() {
        final Map val$cloneMap;
        
        final Context val$context;
        
        final String val$eventId;
        
        final String val$logTag;
        
        final boolean val$uploadNow;
        
        public void run() {
          Intent intent = new Intent();
          intent.setComponent(new ComponentName("com.nearme.statistics.rom", "com.nearme.statistics.rom.service.ReceiverService"));
          intent.putExtra("appPackage", "system");
          intent.putExtra("appName", "system");
          intent.putExtra("appVersion", "system");
          intent.putExtra("ssoid", "system");
          intent.putExtra("appId", OplusBtDcsUtils.sAppId);
          intent.putExtra("eventID", eventId);
          intent.putExtra("uploadNow", uploadNow);
          intent.putExtra("logTag", logTag);
          intent.putExtra("logMap", OplusBtDcsUtils.getCommonObject(cloneMap).toString());
          intent.putExtra("dataType", 1006);
          OplusBtDcsUtils.startDcsService(context, intent);
          cloneMap.clear();
        }
      };
    sSingleThreadExecutor.execute(runnable);
  }
  
  private static JSONObject getCommonObject(Map<String, String> paramMap) {
    JSONObject jSONObject = new JSONObject();
    if (paramMap != null && !paramMap.isEmpty())
      try {
        for (String str : paramMap.keySet())
          jSONObject.put(str, paramMap.get(str)); 
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Exception: ");
        stringBuilder.append(exception);
        Log.w("OplusBtDcsUtils_fwk", stringBuilder.toString());
      }  
    return jSONObject;
  }
  
  private static void startDcsService(Context paramContext, Intent paramIntent) {
    if (paramContext == null || paramIntent == null) {
      Log.w("OplusBtDcsUtils_fwk", "startDcsService failed, Params is null.");
      return;
    } 
    try {
      paramContext.startService(paramIntent);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("startDcsService Exception: ");
      stringBuilder.append(exception);
      Log.w("OplusBtDcsUtils_fwk", stringBuilder.toString());
    } 
  }
  
  public static void dumpLogMapInfo(String paramString, HashMap<String, String> paramHashMap) {
    if (paramHashMap == null)
      return; 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append('[');
    stringBuilder2.append(paramString);
    StringBuilder stringBuilder1 = stringBuilder2.append(']');
    for (Map.Entry<String, String> entry : paramHashMap.entrySet()) {
      stringBuilder1.append(" ");
      stringBuilder1.append((String)entry.getKey());
      stringBuilder1.append(":");
      stringBuilder1.append((String)entry.getValue());
    } 
    Log.i("OplusBtDcsUtils_fwk", stringBuilder1.toString());
  }
  
  public static String timeStampFormat(long paramLong) {
    return DateFormat.format("yyyy-MM-dd HH:mm:ss", paramLong).toString();
  }
  
  public static String durationFormat(long paramLong) {
    Locale locale = Locale.US;
    int i = (int)(paramLong / 3600000L);
    int j = (int)(paramLong / 60000L % 60L);
    int k = (int)(paramLong / 1000L % 60L), m = (int)(paramLong % 1000L);
    return String.format(locale, "%02d:%02d:%02d.%03d", new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m) });
  }
  
  public static String getInfoMapValue(HashMap<String, String> paramHashMap, String paramString) {
    if (paramHashMap == null)
      return null; 
    if (paramHashMap.isEmpty())
      return null; 
    for (Map.Entry<String, String> entry : paramHashMap.entrySet()) {
      if (entry.getKey().equals(paramString))
        return (String)entry.getValue(); 
    } 
    return null;
  }
  
  public static String getEnableDisableReasonString(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("unkown reason(");
        stringBuilder.append(paramInt);
        stringBuilder.append(")");
        return stringBuilder.toString();
      case 10:
        return "factory_reset";
      case 9:
        return "restore_user_setting";
      case 8:
        return "user_switch";
      case 7:
        return "crash";
      case 6:
        return "system_boot";
      case 5:
        return "start_error";
      case 4:
        return "restarted";
      case 3:
        return "disallowed";
      case 2:
        return "airplane_mode";
      case 1:
        return "application_request";
      case 0:
        break;
    } 
    return "unspecified";
  }
}
