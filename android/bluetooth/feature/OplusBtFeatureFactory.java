package android.bluetooth.feature;

import android.bluetooth.feature.dcs.OplusBtAppCallStats;
import android.bluetooth.feature.dcs.OplusBtSwitchEventStats;
import android.content.Context;
import android.os.SystemProperties;
import android.util.Log;
import com.oplus.content.OplusFeatureConfigManager;

public class OplusBtFeatureFactory {
  private static final boolean DBG;
  
  private static final String TAG = "OplusBtFeatureFactory_fwk";
  
  static {
    boolean bool = false;
    if (!SystemProperties.getBoolean("ro.build.release_type", false) || 
      SystemProperties.getBoolean("persist.sys.assert.panic", false))
      bool = true; 
    DBG = bool;
  }
  
  private static OplusBtAppCallStats sBtAppCallRecorder = null;
  
  private static OplusBtSwitchEventStats sBtSwitchRecorder = null;
  
  public static boolean isTargetOperatorInBlackList() {
    int i = OplusFeatureConfigManager.getInstacne().hasFeature("oplus.software.bt.iot_enablelogging") ^ true;
    if (DBG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isTargetOperatorInBlackList() ");
      stringBuilder.append(i);
      Log.i("OplusBtFeatureFactory_fwk", stringBuilder.toString());
    } 
    return i;
  }
  
  public static void init(Context paramContext) {
    if (DBG)
      Log.i("OplusBtFeatureFactory_fwk", "init()"); 
    boolean bool = SystemProperties.getBoolean("persist.bluetooth.bt_monitor", true);
    if (!isTargetOperatorInBlackList() && bool) {
      sBtSwitchRecorder = OplusBtSwitchEventStats.makeSingleInstance(paramContext);
      sBtAppCallRecorder = OplusBtAppCallStats.makeSingleInstance(paramContext);
    } 
  }
  
  public static void cleanUp() {
    if (DBG)
      Log.i("OplusBtFeatureFactory_fwk", "cleanUp()"); 
    OplusBtAppCallStats oplusBtAppCallStats = sBtAppCallRecorder;
    if (oplusBtAppCallStats != null) {
      oplusBtAppCallStats.cleanUp();
      sBtAppCallRecorder = null;
    } 
    OplusBtSwitchEventStats oplusBtSwitchEventStats = sBtSwitchRecorder;
    if (oplusBtSwitchEventStats != null) {
      oplusBtSwitchEventStats.cleanUp();
      sBtSwitchRecorder = null;
    } 
  }
}
