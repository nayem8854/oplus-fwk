package android.bluetooth.feature.dcs;

import android.bluetooth.feature.utils.OplusBtDcsUtils;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemProperties;
import android.util.Log;
import java.util.HashMap;
import java.util.Map;

public final class OplusBtAppCallStats<T> {
  static {
    boolean bool = false;
    if (!SystemProperties.getBoolean("ro.build.release_type", false) || 
      SystemProperties.getBoolean("persist.sys.assert.panic", false))
      bool = true; 
    DBG = bool;
  }
  
  private static OplusBtAppCallStats sBtAppCallRecorder = null;
  
  private AppCallRecorderInfo mEnableDisable = null;
  
  private AppCallRecorderInfo mBleEnableDisable = null;
  
  private OplusBtSwitchEventStats mOplusBtSwitchEventStats = null;
  
  public static final int ADAPTER_STATE_CHANGE = 8;
  
  private static final String APP_CALL_TYPLE = "appCallType";
  
  private static final String CALL_APP = "callApp";
  
  private static final String CALL_FUNCTION_DEFAULT = "defalut()";
  
  private static final String CALL_PARAMETER = "CallParameter";
  
  private static final String CALL_REASON_DEFAULT = "unknow_reason";
  
  private static final String CALL_TYPE_SWITCH = "switch";
  
  private static final boolean DBG;
  
  private static final String EVENT_ID_BT_APP_CALL = "bt_app_call_event";
  
  public static final int FLAG_DISABLE_BLE_RESET = 7;
  
  public static final int FLAG_DISABLE_BLE_SET = 6;
  
  public static final int FLAG_ENABLE_BLE_SET = 4;
  
  public static final int FLAG_ENALBE_BLE_RESET = 5;
  
  private static final String ISSUE_TYPE = "issueType";
  
  private static final String ISSUE_TYPE_GENERAL_APP_CALL = "general_app_call";
  
  private static final String LOG_TAG_BT_DATA_COLLECTOR = "bt_data_collector";
  
  public static final int RECORD_DISABLE = 2;
  
  public static final int RECORD_ENABLE = 1;
  
  public static final int RECORD_ENABLE_QUIET = 3;
  
  private static final String START_CALL_APP = "startCallApp";
  
  private static final String START_CALL_FUNCTION = "startCallFunction";
  
  private static final String START_CALL_REASON = "startCallReason";
  
  private static final String START_CALL_TIME = "startCallTime";
  
  private static final String STOP_CALL_APP = "stopCallApp";
  
  private static final String STOP_CALL_FUNCTION = "stopCallFunction";
  
  private static final String STOP_CALL_REASON = "stopCallReason";
  
  private static final String STOP_CALL_TIME = "stopCallTime";
  
  private static final String TAG = "OplusBtAppCallStats_fwk";
  
  private static final String WORK_DURATION = "workDuration";
  
  private boolean mBleAppCallFlag;
  
  private Context mContext;
  
  private boolean mDisableCompletely;
  
  private boolean mEnableCompletely;
  
  private BtAppCallStatsMessageHandler mHandler;
  
  private boolean mQuietEnableFlag;
  
  public OplusBtAppCallStats(Context paramContext) {
    this.mContext = paramContext;
    HandlerThread handlerThread = new HandlerThread("OplusBtAppCallStatsThread");
    handlerThread.start();
    Looper looper = handlerThread.getLooper();
    this.mHandler = new BtAppCallStatsMessageHandler(looper);
    this.mBleAppCallFlag = false;
    this.mQuietEnableFlag = false;
    this.mEnableCompletely = true;
    this.mDisableCompletely = true;
    this.mOplusBtSwitchEventStats = OplusBtSwitchEventStats.getInstance();
    sBtAppCallRecorder = this;
  }
  
  public static OplusBtAppCallStats makeSingleInstance(Context paramContext) {
    if (sBtAppCallRecorder == null) {
      if (DBG)
        Log.e("OplusBtAppCallStats_fwk", "create OplusBtAppCallStats obj"); 
      sBtAppCallRecorder = new OplusBtAppCallStats(paramContext);
    } 
    return sBtAppCallRecorder;
  }
  
  public static OplusBtAppCallStats getInstance() {
    return sBtAppCallRecorder;
  }
  
  public void cleanUp() {
    // Byte code:
    //   0: getstatic android/bluetooth/feature/dcs/OplusBtAppCallStats.DBG : Z
    //   3: ifeq -> 15
    //   6: ldc 'OplusBtAppCallStats_fwk'
    //   8: ldc_w 'cleanUp()'
    //   11: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   14: pop
    //   15: aload_0
    //   16: monitorenter
    //   17: aload_0
    //   18: getfield mHandler : Landroid/bluetooth/feature/dcs/OplusBtAppCallStats$BtAppCallStatsMessageHandler;
    //   21: aconst_null
    //   22: invokevirtual removeCallbacksAndMessages : (Ljava/lang/Object;)V
    //   25: aload_0
    //   26: getfield mHandler : Landroid/bluetooth/feature/dcs/OplusBtAppCallStats$BtAppCallStatsMessageHandler;
    //   29: invokevirtual getLooper : ()Landroid/os/Looper;
    //   32: astore_1
    //   33: aload_1
    //   34: ifnull -> 41
    //   37: aload_1
    //   38: invokevirtual quit : ()V
    //   41: aload_0
    //   42: monitorexit
    //   43: aconst_null
    //   44: putstatic android/bluetooth/feature/dcs/OplusBtAppCallStats.sBtAppCallRecorder : Landroid/bluetooth/feature/dcs/OplusBtAppCallStats;
    //   47: return
    //   48: astore_1
    //   49: aload_0
    //   50: monitorexit
    //   51: aload_1
    //   52: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #106	-> 0
    //   #107	-> 15
    //   #108	-> 17
    //   #109	-> 25
    //   #110	-> 33
    //   #111	-> 37
    //   #113	-> 41
    //   #114	-> 43
    //   #115	-> 47
    //   #113	-> 48
    // Exception table:
    //   from	to	target	type
    //   17	25	48	finally
    //   25	33	48	finally
    //   37	41	48	finally
    //   41	43	48	finally
    //   49	51	48	finally
  }
  
  class BtAppCallStatsMessageHandler extends Handler {
    final OplusBtAppCallStats this$0;
    
    private BtAppCallStatsMessageHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      OplusBtAppCallStats oplusBtAppCallStats;
      int i, j;
      if (OplusBtAppCallStats.DBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("handle message, msg = ");
        stringBuilder.append(OplusBtAppCallStats.this.dumpMessage(param1Message));
        Log.d("OplusBtAppCallStats_fwk", stringBuilder.toString());
      } 
      HashMap<String, String> hashMap = (HashMap)param1Message.obj;
      String str = OplusBtDcsUtils.getInfoMapValue(hashMap, "callApp");
      switch (param1Message.what) {
        default:
          Log.e("OplusBtAppCallStats_fwk", "unknow handle message");
          return;
        case 8:
          i = param1Message.arg1;
          j = param1Message.arg2;
          if (OplusBtAppCallStats.this.mOplusBtSwitchEventStats != null)
            OplusBtAppCallStats.this.mOplusBtSwitchEventStats.recordAdapterStateChange(i, j); 
          return;
        case 7:
          OplusBtAppCallStats.access$802(OplusBtAppCallStats.this, true);
          return;
        case 6:
          OplusBtAppCallStats.access$802(OplusBtAppCallStats.this, false);
          return;
        case 5:
          OplusBtAppCallStats.access$402(OplusBtAppCallStats.this, true);
          return;
        case 4:
          OplusBtAppCallStats.access$402(OplusBtAppCallStats.this, false);
          return;
        case 2:
          if (OplusBtAppCallStats.this.mOplusBtSwitchEventStats != null)
            OplusBtAppCallStats.this.mOplusBtSwitchEventStats.recordSwitchCall(str, param1Message.what, param1Message.arg1, new Object[] { Boolean.valueOf(OplusBtAppCallStats.access$800(this.this$0)) }); 
          if (!OplusBtAppCallStats.this.mDisableCompletely) {
            oplusBtAppCallStats = OplusBtAppCallStats.this;
            oplusBtAppCallStats.normalStopCall(oplusBtAppCallStats.mBleEnableDisable, param1Message);
          } else {
            oplusBtAppCallStats = OplusBtAppCallStats.this;
            oplusBtAppCallStats.normalStopCall(oplusBtAppCallStats.mEnableDisable, param1Message);
            oplusBtAppCallStats = OplusBtAppCallStats.this;
            oplusBtAppCallStats.normalStopCall(oplusBtAppCallStats.mBleEnableDisable, param1Message);
          } 
          return;
        case 1:
        case 3:
          break;
      } 
      if (OplusBtAppCallStats.this.mOplusBtSwitchEventStats != null)
        OplusBtAppCallStats.this.mOplusBtSwitchEventStats.recordSwitchCall((String)oplusBtAppCallStats, param1Message.what, param1Message.arg1, new Object[] { Boolean.valueOf(OplusBtAppCallStats.access$400(this.this$0)) }); 
      if (!OplusBtAppCallStats.this.mEnableCompletely) {
        if (OplusBtAppCallStats.this.mBleEnableDisable == null) {
          oplusBtAppCallStats = OplusBtAppCallStats.this;
          OplusBtAppCallStats.access$502(oplusBtAppCallStats, oplusBtAppCallStats.normalStartCall(oplusBtAppCallStats.mBleEnableDisable, param1Message));
        } else {
          oplusBtAppCallStats = OplusBtAppCallStats.this;
          oplusBtAppCallStats.normalStartCall(oplusBtAppCallStats.mBleEnableDisable, param1Message);
        } 
      } else if (OplusBtAppCallStats.this.mEnableDisable == null) {
        oplusBtAppCallStats = OplusBtAppCallStats.this;
        OplusBtAppCallStats.access$702(oplusBtAppCallStats, oplusBtAppCallStats.normalStartCall(oplusBtAppCallStats.mEnableDisable, param1Message));
      } else {
        oplusBtAppCallStats = OplusBtAppCallStats.this;
        oplusBtAppCallStats.normalStartCall(oplusBtAppCallStats.mEnableDisable, param1Message);
      } 
    }
  }
  
  public void setBleAppCount(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iload_1
    //   3: ifne -> 14
    //   6: aload_0
    //   7: iconst_0
    //   8: putfield mBleAppCallFlag : Z
    //   11: goto -> 19
    //   14: aload_0
    //   15: iconst_1
    //   16: putfield mBleAppCallFlag : Z
    //   19: aload_0
    //   20: monitorexit
    //   21: return
    //   22: astore_2
    //   23: aload_0
    //   24: monitorexit
    //   25: aload_2
    //   26: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #187	-> 2
    //   #188	-> 6
    //   #190	-> 14
    //   #192	-> 19
    //   #186	-> 22
    // Exception table:
    //   from	to	target	type
    //   6	11	22	finally
    //   14	19	22	finally
  }
  
  public void setQuietEnableFlag(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iload_1
    //   4: putfield mQuietEnableFlag : Z
    //   7: aload_0
    //   8: monitorexit
    //   9: return
    //   10: astore_2
    //   11: aload_0
    //   12: monitorexit
    //   13: aload_2
    //   14: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #198	-> 2
    //   #199	-> 7
    //   #197	-> 10
    // Exception table:
    //   from	to	target	type
    //   2	7	10	finally
  }
  
  public void recordSwitchFlag(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mHandler : Landroid/bluetooth/feature/dcs/OplusBtAppCallStats$BtAppCallStatsMessageHandler;
    //   6: invokevirtual obtainMessage : ()Landroid/os/Message;
    //   9: astore_2
    //   10: aload_2
    //   11: iload_1
    //   12: putfield what : I
    //   15: aload_0
    //   16: getfield mHandler : Landroid/bluetooth/feature/dcs/OplusBtAppCallStats$BtAppCallStatsMessageHandler;
    //   19: aload_2
    //   20: invokevirtual sendMessage : (Landroid/os/Message;)Z
    //   23: pop
    //   24: aload_0
    //   25: monitorexit
    //   26: return
    //   27: astore_2
    //   28: aload_0
    //   29: monitorexit
    //   30: aload_2
    //   31: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #205	-> 2
    //   #206	-> 10
    //   #207	-> 15
    //   #208	-> 24
    //   #204	-> 27
    // Exception table:
    //   from	to	target	type
    //   2	10	27	finally
    //   10	15	27	finally
    //   15	24	27	finally
  }
  
  public void recordAdapterStateChange(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mHandler : Landroid/bluetooth/feature/dcs/OplusBtAppCallStats$BtAppCallStatsMessageHandler;
    //   6: invokevirtual obtainMessage : ()Landroid/os/Message;
    //   9: astore_3
    //   10: aload_3
    //   11: bipush #8
    //   13: putfield what : I
    //   16: aload_3
    //   17: iload_1
    //   18: putfield arg1 : I
    //   21: aload_3
    //   22: iload_2
    //   23: putfield arg2 : I
    //   26: aload_0
    //   27: getfield mHandler : Landroid/bluetooth/feature/dcs/OplusBtAppCallStats$BtAppCallStatsMessageHandler;
    //   30: aload_3
    //   31: invokevirtual sendMessage : (Landroid/os/Message;)Z
    //   34: pop
    //   35: aload_0
    //   36: monitorexit
    //   37: return
    //   38: astore_3
    //   39: aload_0
    //   40: monitorexit
    //   41: aload_3
    //   42: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #215	-> 2
    //   #216	-> 10
    //   #217	-> 16
    //   #218	-> 21
    //   #219	-> 26
    //   #220	-> 35
    //   #214	-> 38
    // Exception table:
    //   from	to	target	type
    //   2	10	38	finally
    //   10	16	38	finally
    //   16	21	38	finally
    //   21	26	38	finally
    //   26	35	38	finally
  }
  
  public void recordAppCall(String paramString, int paramInt1, int paramInt2, T... paramVarArgs) {
    if (DBG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("recordAppCall():  packageName = ");
      stringBuilder.append(paramString);
      stringBuilder.append(" call ");
      stringBuilder.append(recordEventToString(paramInt1));
      String str = stringBuilder.toString();
      Log.d("OplusBtAppCallStats_fwk", str);
    } 
    HashMap<Object, Object> hashMap = new HashMap<>();
    hashMap.put("callApp", paramString);
    for (byte b = 0; b < paramVarArgs.length; b++) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("CallParameter");
      stringBuilder.append(String.valueOf(b));
      String str = stringBuilder.toString();
      hashMap.put(str, String.valueOf(paramVarArgs[b]));
    } 
    Message message = this.mHandler.obtainMessage();
    message.what = paramInt1;
    message.arg1 = paramInt2;
    message.obj = hashMap;
    this.mHandler.sendMessage(message);
  }
  
  private void processUploadEvent(AppCallRecorderInfo paramAppCallRecorderInfo) {
    if (DBG)
      Log.d("OplusBtAppCallStats_fwk", "processUploadEvent(): enter."); 
    if (paramAppCallRecorderInfo == null)
      return; 
    HashMap<Object, Object> hashMap = new HashMap<>();
    hashMap.put("issueType", "general_app_call");
    hashMap.put("appCallType", paramAppCallRecorderInfo.appCallType);
    hashMap.put("startCallTime", OplusBtDcsUtils.timeStampFormat(paramAppCallRecorderInfo.startCallTime.longValue()));
    hashMap.put("stopCallTime", OplusBtDcsUtils.timeStampFormat(paramAppCallRecorderInfo.stopCallTime.longValue()));
    hashMap.put("workDuration", OplusBtDcsUtils.durationFormat(paramAppCallRecorderInfo.workDuration.longValue()));
    hashMap.put("startCallApp", paramAppCallRecorderInfo.startCallApp);
    hashMap.put("stopCallApp", paramAppCallRecorderInfo.stopCallApp);
    hashMap.put("startCallReason", paramAppCallRecorderInfo.startCallReason);
    hashMap.put("stopCallReason", paramAppCallRecorderInfo.stopCallReason);
    hashMap.put("startCallFunction", paramAppCallRecorderInfo.startCallFunction);
    hashMap.put("stopCallFunction", paramAppCallRecorderInfo.stopCallFunction);
    if (!hashMap.isEmpty())
      OplusBtDcsUtils.onCommon(this.mContext, "bt_data_collector", "bt_app_call_event", (Map)hashMap, false); 
    if (DBG)
      OplusBtDcsUtils.dumpLogMapInfo("Statistics", (HashMap)hashMap); 
    paramAppCallRecorderInfo.resetInfo();
  }
  
  private AppCallRecorderInfo normalStartCall(AppCallRecorderInfo paramAppCallRecorderInfo, Message paramMessage) {
    String str1, str2;
    AppCallRecorderInfo appCallRecorderInfo;
    if (DBG)
      Log.d("OplusBtAppCallStats_fwk", "normalStartCall(): enter."); 
    HashMap<String, String> hashMap = (HashMap)paramMessage.obj;
    String str3 = OplusBtDcsUtils.getInfoMapValue(hashMap, "callApp");
    String str4 = recordEventToCallType(paramMessage.what);
    if (str4.equals("switch")) {
      String str = OplusBtDcsUtils.getEnableDisableReasonString(paramMessage.arg1);
      str2 = recordEventToString(paramMessage.what);
      str1 = str;
    } else {
      str1 = "unknow_reason";
      str2 = "defalut()";
    } 
    if (paramAppCallRecorderInfo == null) {
      appCallRecorderInfo = new AppCallRecorderInfo(Long.valueOf(System.currentTimeMillis()), str3, str1, str4, str2);
    } else if (paramAppCallRecorderInfo.isEmpty) {
      paramAppCallRecorderInfo.startCallTime = Long.valueOf(System.currentTimeMillis());
      paramAppCallRecorderInfo.startCallApp = str3;
      paramAppCallRecorderInfo.startCallReason = str1;
      paramAppCallRecorderInfo.startCallFunction = str2;
      paramAppCallRecorderInfo.appCallType = str4;
      paramAppCallRecorderInfo.isEmpty = false;
      appCallRecorderInfo = paramAppCallRecorderInfo;
    } else {
      appCallRecorderInfo = paramAppCallRecorderInfo;
      if (!paramAppCallRecorderInfo.isEmpty) {
        paramAppCallRecorderInfo.resetInfo();
        paramAppCallRecorderInfo.startCallTime = Long.valueOf(System.currentTimeMillis());
        paramAppCallRecorderInfo.startCallApp = str3;
        paramAppCallRecorderInfo.startCallReason = str1;
        paramAppCallRecorderInfo.startCallFunction = str2;
        paramAppCallRecorderInfo.appCallType = str4;
        paramAppCallRecorderInfo.isEmpty = false;
        appCallRecorderInfo = paramAppCallRecorderInfo;
      } 
    } 
    return appCallRecorderInfo;
  }
  
  private void normalStopCall(AppCallRecorderInfo paramAppCallRecorderInfo, Message paramMessage) {
    if (DBG)
      Log.d("OplusBtAppCallStats_fwk", "normalStopCall(): enter."); 
    if (paramAppCallRecorderInfo == null)
      return; 
    HashMap<String, String> hashMap = (HashMap)paramMessage.obj;
    String str2 = OplusBtDcsUtils.getInfoMapValue(hashMap, "callApp");
    String str3 = recordEventToCallType(paramMessage.what);
    String str1 = "unknow_reason";
    String str4 = "defalut()";
    if (str3.equals("switch")) {
      str1 = OplusBtDcsUtils.getEnableDisableReasonString(paramMessage.arg1);
      str4 = recordEventToString(paramMessage.what);
    } 
    if (!paramAppCallRecorderInfo.isEmpty) {
      paramAppCallRecorderInfo.stopCallTime = Long.valueOf(System.currentTimeMillis());
      paramAppCallRecorderInfo.workDuration = Long.valueOf(paramAppCallRecorderInfo.stopCallTime.longValue() - paramAppCallRecorderInfo.startCallTime.longValue());
      paramAppCallRecorderInfo.stopCallApp = str2;
      paramAppCallRecorderInfo.stopCallReason = str1;
      paramAppCallRecorderInfo.stopCallFunction = str4;
      processUploadEvent(paramAppCallRecorderInfo);
    } 
  }
  
  private String dumpMessage(Message paramMessage) {
    StringBuilder stringBuilder = new StringBuilder();
    HashMap<String, String> hashMap = (HashMap)paramMessage.obj;
    String str = OplusBtDcsUtils.getInfoMapValue(hashMap, "callApp");
    stringBuilder.append("[");
    stringBuilder.append(recordEventToString(paramMessage.what));
    stringBuilder.append(": called by ");
    stringBuilder.append(str);
    stringBuilder.append(" for ");
    stringBuilder.append(OplusBtDcsUtils.getEnableDisableReasonString(paramMessage.arg1));
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  private String recordEventToString(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2)
        return Integer.toString(paramInt); 
      if (!this.mDisableCompletely)
        return "disableBLE()"; 
      return "disable()";
    } 
    if (!this.mEnableCompletely)
      return "enableBLE()"; 
    if (this.mQuietEnableFlag)
      return "enableNoAutoConnect()"; 
    return "enable()";
  }
  
  private static String recordEventToCallType(int paramInt) {
    if (paramInt != 1 && paramInt != 2 && paramInt != 3)
      return Integer.toString(paramInt); 
    return "switch";
  }
  
  public class AppCallRecorderInfo {
    public String appCallType;
    
    public boolean isEmpty = true;
    
    public String startCallApp;
    
    public String startCallFunction;
    
    public String startCallReason;
    
    public Long startCallTime;
    
    public String stopCallApp;
    
    public String stopCallFunction;
    
    public String stopCallReason;
    
    public Long stopCallTime;
    
    final OplusBtAppCallStats this$0;
    
    public Long workDuration;
    
    public AppCallRecorderInfo(Long param1Long, String param1String1, String param1String2, String param1String3, String param1String4) {
      Long long_ = Long.valueOf(0L);
      this.stopCallTime = long_;
      this.workDuration = long_;
      this.startCallApp = null;
      this.startCallFunction = null;
      this.startCallReason = null;
      this.stopCallApp = null;
      this.stopCallFunction = null;
      this.stopCallReason = null;
      this.appCallType = null;
      this.isEmpty = false;
      this.startCallTime = param1Long;
      this.startCallApp = param1String1;
      this.startCallReason = param1String2;
      this.startCallFunction = param1String4;
      this.appCallType = param1String3;
    }
    
    public void resetInfo() {
      this.isEmpty = true;
      Long long_ = Long.valueOf(0L);
      this.stopCallTime = long_;
      this.workDuration = long_;
      this.startCallApp = null;
      this.stopCallApp = null;
      this.startCallReason = null;
      this.stopCallReason = null;
      this.startCallFunction = null;
      this.stopCallFunction = null;
      this.appCallType = null;
    }
  }
}
