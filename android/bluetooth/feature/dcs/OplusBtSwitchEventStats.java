package android.bluetooth.feature.dcs;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.feature.utils.OplusBtDcsUtils;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemProperties;
import android.util.Log;
import java.util.HashMap;
import java.util.Map;

public final class OplusBtSwitchEventStats<T> {
  static {
    boolean bool = false;
    if (!SystemProperties.getBoolean("ro.build.release_type", false) || 
      SystemProperties.getBoolean("persist.sys.assert.panic", false))
      bool = true; 
    DBG = bool;
  }
  
  private static OplusBtSwitchEventStats sBtSwitchRecorder = null;
  
  private SwitchRecorderInfo mBleOnSwitchInfo = null;
  
  private SwitchRecorderInfo mBtOnSwitchInfo = null;
  
  private SwitchRecorderInfo mBleOffSwitchInfo = null;
  
  private SwitchRecorderInfo mBtOffSwitchInfo = null;
  
  private boolean isRecorderDetailed = SystemProperties.getBoolean("persist.vendor.bluetooth.record.detailed", true);
  
  private int mPreState = 10;
  
  private static final String BLUETOOTH_RECORDER = "bluetooth_layer";
  
  private static final String CALL_APP = "callApp";
  
  private static final String CALL_FUNCTION_CRASH = "crash()";
  
  private static final String CALL_FUNCTION_DISABLE = "disable()";
  
  private static final String CALL_FUNCTION_DISABLE_BLE = "disableBLE()";
  
  private static final String CALL_FUNCTION_ENABLE = "enable()";
  
  private static final String CALL_FUNCTION_ENABLE_BLE = "enableBLE()";
  
  private static final String CALL_FUNCTION_ENABLE_QUIET = "enableNoAutoConnect()";
  
  private static final String CALL_PARAMETER = "CallParameter";
  
  private static final boolean DBG;
  
  private static final String DEFAULT_CALL_FUNCTION = "default()";
  
  private static final String DEFAULT_CALL_REASON = "unknow_reason";
  
  private static final String DEFAULT_REASON = "normal";
  
  private static final String DEFAULT_RESULT = "success";
  
  private static final String DEFAULT_STATE = "OFF";
  
  private static final String EVENT_ID_BT_SWITCH_RECORDER = "bt_switch_event";
  
  private static final String FAIL = "fail";
  
  private static final String FRAMEWORK_RECORDER = "framework_layer";
  
  private static final String ISSUE_TYPE = "issueType";
  
  private static final String ISSUE_TYPE_SWITCH_EVENT = "switch_event";
  
  private static final String LOG_TAG_BT_DATA_COLLECTOR = "bt_data_collector";
  
  private static final String PROPERTY_DETAILED_RECORDER = "persist.vendor.bluetooth.record.detailed";
  
  public static final int RECORD_ADAPTER_STATE_CHANGE = 4;
  
  public static final int RECORD_BLE_START_TIMEOUT = 8;
  
  public static final int RECORD_BLE_STOP_TIMEOUT = 7;
  
  public static final int RECORD_BLUETOOTH_CRASH = 17;
  
  public static final int RECORD_BREDR_CLEANUP_TIMEOUT = 10;
  
  public static final int RECORD_BREDR_START_TIMEOUT = 5;
  
  public static final int RECORD_BREDR_STOP_TIMEOUT = 6;
  
  public static final int RECORD_BT_BIND_FAILURE = 16;
  
  public static final int RECORD_BT_BIND_TIMEOUT = 13;
  
  public static final int RECORD_BT_FORCEKILL_TIMEOUT = 11;
  
  public static final int RECORD_BT_LE_SERVICE_UP_TIMEOUT = 15;
  
  public static final int RECORD_BT_UNBIND_TIMEOUT = 14;
  
  public static final int RECORD_DISABLE = 2;
  
  public static final int RECORD_ENABLE = 1;
  
  public static final int RECORD_ENABLE_QUIET = 3;
  
  public static final int RECORD_STACK_DISABLE_ERROR = 12;
  
  public static final int RECORD_STACK_DISABLE_TIMEOUT = 9;
  
  private static final String STATE_0_KEY = "state0";
  
  private static final String TAG = "OplusBtSwitchEventStats_fwk";
  
  private boolean mBleAppCallFlag;
  
  private Context mContext;
  
  private int mCurrentAdapterState;
  
  private boolean mDisableCompletely;
  
  private boolean mEnableCompletely;
  
  private BtSwitchRecordMessageHandler mHandler;
  
  private boolean mQuietEnableFlag;
  
  public OplusBtSwitchEventStats(Context paramContext) {
    this.mContext = paramContext;
    HandlerThread handlerThread = new HandlerThread("OplusBtSwitchEventStatsThread");
    handlerThread.start();
    Looper looper = handlerThread.getLooper();
    this.mHandler = new BtSwitchRecordMessageHandler(looper);
    this.mCurrentAdapterState = 10;
    this.mQuietEnableFlag = false;
    this.mEnableCompletely = true;
    this.mDisableCompletely = true;
    this.mBleOnSwitchInfo = new SwitchRecorderInfo("enableBLE()");
    this.mBtOnSwitchInfo = new SwitchRecorderInfo("enable()");
    this.mBleOffSwitchInfo = new SwitchRecorderInfo("disableBLE()");
    this.mBtOffSwitchInfo = new SwitchRecorderInfo("disable()");
    this.mBleAppCallFlag = false;
    sBtSwitchRecorder = this;
  }
  
  public static OplusBtSwitchEventStats makeSingleInstance(Context paramContext) {
    if (sBtSwitchRecorder == null) {
      if (DBG)
        Log.e("OplusBtSwitchEventStats_fwk", "create OplusBtSwitchEventStats obj"); 
      sBtSwitchRecorder = new OplusBtSwitchEventStats(paramContext);
    } 
    return sBtSwitchRecorder;
  }
  
  public static OplusBtSwitchEventStats getInstance() {
    return sBtSwitchRecorder;
  }
  
  public void cleanUp() {
    // Byte code:
    //   0: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   3: ifeq -> 15
    //   6: ldc 'OplusBtSwitchEventStats_fwk'
    //   8: ldc_w 'cleanUp()'
    //   11: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   14: pop
    //   15: aload_0
    //   16: monitorenter
    //   17: aload_0
    //   18: getfield mHandler : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$BtSwitchRecordMessageHandler;
    //   21: aconst_null
    //   22: invokevirtual removeCallbacksAndMessages : (Ljava/lang/Object;)V
    //   25: aload_0
    //   26: getfield mHandler : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$BtSwitchRecordMessageHandler;
    //   29: invokevirtual getLooper : ()Landroid/os/Looper;
    //   32: astore_1
    //   33: aload_1
    //   34: ifnull -> 41
    //   37: aload_1
    //   38: invokevirtual quit : ()V
    //   41: aload_0
    //   42: monitorexit
    //   43: aconst_null
    //   44: putstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.sBtSwitchRecorder : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats;
    //   47: return
    //   48: astore_1
    //   49: aload_0
    //   50: monitorexit
    //   51: aload_1
    //   52: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #129	-> 0
    //   #130	-> 15
    //   #131	-> 17
    //   #132	-> 25
    //   #133	-> 33
    //   #134	-> 37
    //   #136	-> 41
    //   #137	-> 43
    //   #138	-> 47
    //   #136	-> 48
    // Exception table:
    //   from	to	target	type
    //   17	25	48	finally
    //   25	33	48	finally
    //   37	41	48	finally
    //   41	43	48	finally
    //   49	51	48	finally
  }
  
  public void setBleAppCount(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iload_1
    //   3: ifne -> 14
    //   6: aload_0
    //   7: iconst_0
    //   8: putfield mBleAppCallFlag : Z
    //   11: goto -> 19
    //   14: aload_0
    //   15: iconst_1
    //   16: putfield mBleAppCallFlag : Z
    //   19: aload_0
    //   20: monitorexit
    //   21: return
    //   22: astore_2
    //   23: aload_0
    //   24: monitorexit
    //   25: aload_2
    //   26: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #144	-> 2
    //   #145	-> 6
    //   #147	-> 14
    //   #149	-> 19
    //   #143	-> 22
    // Exception table:
    //   from	to	target	type
    //   6	11	22	finally
    //   14	19	22	finally
  }
  
  public void setQuietEnableFlag(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iload_1
    //   4: putfield mQuietEnableFlag : Z
    //   7: aload_0
    //   8: monitorexit
    //   9: return
    //   10: astore_2
    //   11: aload_0
    //   12: monitorexit
    //   13: aload_2
    //   14: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #153	-> 2
    //   #154	-> 7
    //   #152	-> 10
    // Exception table:
    //   from	to	target	type
    //   2	7	10	finally
  }
  
  public void recordSwitchCall(String paramString, int paramInt1, int paramInt2, T... paramVarArgs) {
    if (DBG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("recordSwitchCall():  packageName = ");
      stringBuilder.append(paramString);
      stringBuilder.append(", recordEvent = ");
      stringBuilder.append(recordEventToString(paramInt1));
      String str = stringBuilder.toString();
      Log.d("OplusBtSwitchEventStats_fwk", str);
    } 
    HashMap<Object, Object> hashMap = new HashMap<>();
    hashMap.put("callApp", paramString);
    for (byte b = 0; b < paramVarArgs.length; b++) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("CallParameter");
      stringBuilder.append(String.valueOf(b));
      String str = stringBuilder.toString();
      hashMap.put(str, String.valueOf(paramVarArgs[b]));
    } 
    Message message = this.mHandler.obtainMessage();
    message.what = paramInt1;
    message.obj = hashMap;
    message.arg1 = paramInt2;
    this.mHandler.sendMessage(message);
  }
  
  public void recordAdapterStateChange(int paramInt1, int paramInt2) {
    if (DBG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("recordAdapterStateChange():  preState = ");
      stringBuilder.append(BluetoothAdapter.nameForState(paramInt1));
      stringBuilder.append(", newState =");
      stringBuilder.append(BluetoothAdapter.nameForState(paramInt2));
      String str = stringBuilder.toString();
      Log.d("OplusBtSwitchEventStats_fwk", str);
    } 
    Message message = this.mHandler.obtainMessage();
    message.what = 4;
    message.arg1 = paramInt1;
    message.arg2 = paramInt2;
    this.mHandler.sendMessage(message);
  }
  
  public void recordErrorEvent(int paramInt) {
    if (DBG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("recordErrorEvent():  event = ");
      stringBuilder.append(recordEventToString(paramInt));
      Log.d("OplusBtSwitchEventStats_fwk", stringBuilder.toString());
    } 
    Message message = this.mHandler.obtainMessage();
    message.what = paramInt;
    this.mHandler.sendMessage(message);
  }
  
  class BtSwitchRecordMessageHandler extends Handler {
    final OplusBtSwitchEventStats this$0;
    
    private BtSwitchRecordMessageHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      if (OplusBtSwitchEventStats.DBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("handle message, msg.what = ");
        stringBuilder.append(OplusBtSwitchEventStats.this.recordEventToString(param1Message.what));
        Log.d("OplusBtSwitchEventStats_fwk", stringBuilder.toString());
      } 
      switch (param1Message.what) {
        default:
          Log.e("OplusBtSwitchEventStats_fwk", "unknow handle message, ignore");
          return;
        case 17:
          OplusBtSwitchEventStats.this.processCrashEvent(param1Message);
          return;
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        case 16:
          OplusBtSwitchEventStats.this.processErrorEvent(param1Message);
          return;
        case 4:
          OplusBtSwitchEventStats.this.updateAdapterState(param1Message);
          return;
        case 2:
          if (param1Message.obj == null) {
            if (OplusBtSwitchEventStats.DBG)
              Log.e("OplusBtSwitchEventStats_fwk", "invalid bt switch params"); 
          } else {
            HashMap hashMap = (HashMap)param1Message.obj;
            if (hashMap.size() >= 2) {
              String str = (String)hashMap.get("CallParameter0");
              OplusBtSwitchEventStats.this.processEnableDisableEvent(param1Message, false, str);
            } 
          } 
          return;
        case 1:
        case 3:
          break;
      } 
      if (param1Message.obj == null) {
        if (OplusBtSwitchEventStats.DBG)
          Log.e("OplusBtSwitchEventStats_fwk", "invalid bt switch params"); 
      } else {
        HashMap hashMap = (HashMap)param1Message.obj;
        if (hashMap.size() >= 2) {
          String str = (String)hashMap.get("CallParameter0");
          OplusBtSwitchEventStats.this.processEnableDisableEvent(param1Message, true, str);
        } 
      } 
    }
  }
  
  private void processEnableDisableEvent(Message paramMessage, boolean paramBoolean, String paramString) {
    if (DBG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("processEnableDisableEvent(): event: ");
      stringBuilder.append(paramBoolean);
      stringBuilder.append(", flag: ");
      stringBuilder.append(paramString);
      Log.d("OplusBtSwitchEventStats_fwk", stringBuilder.toString());
    } 
    if (paramString == null) {
      Log.e("OplusBtSwitchEventStats_fwk", "processEnableDisableEvent, invalid params");
      return;
    } 
    Long long_ = Long.valueOf(System.currentTimeMillis());
    HashMap<String, String> hashMap = (HashMap)paramMessage.obj;
    String str2 = OplusBtDcsUtils.getInfoMapValue(hashMap, "callApp");
    String str1 = OplusBtDcsUtils.getEnableDisableReasonString(paramMessage.arg1);
    if (paramBoolean) {
      if (paramString.equals("true")) {
        this.mEnableCompletely = true;
        if (this.mBtOnSwitchInfo.isEmpty()) {
          this.mBtOnSwitchInfo.isEmpty = false;
          this.mBtOnSwitchInfo.callApp = str2;
          this.mBtOnSwitchInfo.callReason = str1;
          this.mBtOnSwitchInfo.callTimeStamp = long_;
          this.mBtOnSwitchInfo.preStatesTimeStamp = long_;
        } else if (DBG) {
          Log.e("OplusBtSwitchEventStats_fwk", "bt on is in progress, ignore new request");
        } 
      } else if (paramString.equals("false")) {
        this.mEnableCompletely = false;
        if (this.mBleOnSwitchInfo.isEmpty()) {
          this.mBleOnSwitchInfo.isEmpty = false;
          this.mBleOnSwitchInfo.callApp = str2;
          this.mBleOnSwitchInfo.callReason = str1;
          this.mBleOnSwitchInfo.callTimeStamp = long_;
          this.mBleOnSwitchInfo.preStatesTimeStamp = long_;
        } else if (DBG) {
          Log.e("OplusBtSwitchEventStats_fwk", "ble on is in progress, ignore new request");
        } 
      } 
    } else if (paramString.equals("true")) {
      this.mDisableCompletely = true;
      if (this.mBleOffSwitchInfo.isEmpty() && this.mBtOffSwitchInfo.isEmpty()) {
        this.mBtOffSwitchInfo.isEmpty = false;
        this.mBtOffSwitchInfo.callApp = str2;
        this.mBtOffSwitchInfo.callReason = str1;
        this.mBtOffSwitchInfo.callTimeStamp = long_;
        this.mBtOffSwitchInfo.preStatesTimeStamp = long_;
      } else if (DBG) {
        Log.e("OplusBtSwitchEventStats_fwk", "bt off is in progress, ignore new request");
      } 
    } else if (paramString.equals("false")) {
      this.mDisableCompletely = false;
      if (this.mBleOffSwitchInfo.isEmpty()) {
        this.mBleOffSwitchInfo.isEmpty = false;
        this.mBleOffSwitchInfo.callApp = str2;
        this.mBleOffSwitchInfo.callReason = str1;
        this.mBleOffSwitchInfo.callTimeStamp = long_;
        this.mBleOffSwitchInfo.preStatesTimeStamp = long_;
      } else if (DBG) {
        Log.e("OplusBtSwitchEventStats_fwk", "bt off is in progress, ignore new request");
      } 
    } 
  }
  
  private void updateAdapterState(Message paramMessage) {
    // Byte code:
    //   0: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   3: ifeq -> 15
    //   6: ldc 'OplusBtSwitchEventStats_fwk'
    //   8: ldc_w 'updateAdapterState(): enter.'
    //   11: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   14: pop
    //   15: aconst_null
    //   16: astore_2
    //   17: aload_1
    //   18: getfield arg1 : I
    //   21: istore_3
    //   22: aload_1
    //   23: getfield arg2 : I
    //   26: istore #4
    //   28: iload_3
    //   29: iload #4
    //   31: if_icmpne -> 35
    //   34: return
    //   35: aload_0
    //   36: iload #4
    //   38: putfield mCurrentAdapterState : I
    //   41: new java/lang/StringBuilder
    //   44: dup
    //   45: invokespecial <init> : ()V
    //   48: astore_1
    //   49: aload_1
    //   50: iload_3
    //   51: invokestatic nameForState : (I)Ljava/lang/String;
    //   54: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   57: pop
    //   58: aload_1
    //   59: ldc_w '->'
    //   62: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   65: pop
    //   66: aload_1
    //   67: iload #4
    //   69: invokestatic nameForState : (I)Ljava/lang/String;
    //   72: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   75: pop
    //   76: aload_1
    //   77: invokevirtual toString : ()Ljava/lang/String;
    //   80: astore_1
    //   81: invokestatic currentTimeMillis : ()J
    //   84: invokestatic valueOf : (J)Ljava/lang/Long;
    //   87: astore #5
    //   89: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   92: ifeq -> 131
    //   95: new java/lang/StringBuilder
    //   98: dup
    //   99: invokespecial <init> : ()V
    //   102: astore #6
    //   104: aload #6
    //   106: ldc_w 'updateAdapterState() '
    //   109: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   112: pop
    //   113: aload #6
    //   115: aload_1
    //   116: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   119: pop
    //   120: ldc 'OplusBtSwitchEventStats_fwk'
    //   122: aload #6
    //   124: invokevirtual toString : ()Ljava/lang/String;
    //   127: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   130: pop
    //   131: iload #4
    //   133: tableswitch default -> 172, 10 -> 1195, 11 -> 1074, 12 -> 943, 13 -> 822, 14 -> 605, 15 -> 177
    //   172: aload_2
    //   173: astore_1
    //   174: goto -> 1429
    //   177: iload_3
    //   178: bipush #14
    //   180: if_icmpne -> 378
    //   183: aload_0
    //   184: getfield mEnableCompletely : Z
    //   187: ifne -> 289
    //   190: aload_0
    //   191: getfield mBleOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   194: invokevirtual isEmpty : ()Z
    //   197: ifne -> 267
    //   200: aload_0
    //   201: getfield mBleOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   204: ldc 'success'
    //   206: putfield result : Ljava/lang/String;
    //   209: aload_0
    //   210: getfield mBleOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   213: getfield optionalStateInfo : Ljava/util/HashMap;
    //   216: astore_2
    //   217: aload #5
    //   219: invokevirtual longValue : ()J
    //   222: lstore #7
    //   224: aload_0
    //   225: getfield mBleOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   228: getfield preStatesTimeStamp : Ljava/lang/Long;
    //   231: invokevirtual longValue : ()J
    //   234: lstore #9
    //   236: aload_2
    //   237: aload_1
    //   238: lload #7
    //   240: lload #9
    //   242: lsub
    //   243: invokestatic valueOf : (J)Ljava/lang/String;
    //   246: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   249: pop
    //   250: aload_0
    //   251: getfield mBleOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   254: aload #5
    //   256: putfield preStatesTimeStamp : Ljava/lang/Long;
    //   259: aload_0
    //   260: getfield mBleOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   263: astore_1
    //   264: goto -> 1429
    //   267: aload_2
    //   268: astore_1
    //   269: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   272: ifeq -> 1429
    //   275: ldc 'OplusBtSwitchEventStats_fwk'
    //   277: ldc_w 'ble on recorder is empty for state_ble_on'
    //   280: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   283: pop
    //   284: aload_2
    //   285: astore_1
    //   286: goto -> 1429
    //   289: aload_0
    //   290: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   293: invokevirtual isEmpty : ()Z
    //   296: ifne -> 356
    //   299: aload_0
    //   300: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   303: getfield optionalStateInfo : Ljava/util/HashMap;
    //   306: astore #6
    //   308: aload #5
    //   310: invokevirtual longValue : ()J
    //   313: lstore #9
    //   315: aload_0
    //   316: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   319: getfield preStatesTimeStamp : Ljava/lang/Long;
    //   322: invokevirtual longValue : ()J
    //   325: lstore #7
    //   327: aload #6
    //   329: aload_1
    //   330: lload #9
    //   332: lload #7
    //   334: lsub
    //   335: invokestatic valueOf : (J)Ljava/lang/String;
    //   338: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   341: pop
    //   342: aload_0
    //   343: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   346: aload #5
    //   348: putfield preStatesTimeStamp : Ljava/lang/Long;
    //   351: aload_2
    //   352: astore_1
    //   353: goto -> 1429
    //   356: aload_2
    //   357: astore_1
    //   358: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   361: ifeq -> 1429
    //   364: ldc 'OplusBtSwitchEventStats_fwk'
    //   366: ldc_w 'bt on recorder is empty for state_ble_on'
    //   369: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   372: pop
    //   373: aload_2
    //   374: astore_1
    //   375: goto -> 1429
    //   378: iload_3
    //   379: bipush #13
    //   381: if_icmpne -> 579
    //   384: aload_0
    //   385: getfield mDisableCompletely : Z
    //   388: ifne -> 490
    //   391: aload_0
    //   392: getfield mBleOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   395: invokevirtual isEmpty : ()Z
    //   398: ifne -> 468
    //   401: aload_0
    //   402: getfield mBleOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   405: ldc 'success'
    //   407: putfield result : Ljava/lang/String;
    //   410: aload_0
    //   411: getfield mBleOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   414: getfield optionalStateInfo : Ljava/util/HashMap;
    //   417: astore_2
    //   418: aload #5
    //   420: invokevirtual longValue : ()J
    //   423: lstore #9
    //   425: aload_0
    //   426: getfield mBleOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   429: getfield preStatesTimeStamp : Ljava/lang/Long;
    //   432: invokevirtual longValue : ()J
    //   435: lstore #7
    //   437: aload_2
    //   438: aload_1
    //   439: lload #9
    //   441: lload #7
    //   443: lsub
    //   444: invokestatic valueOf : (J)Ljava/lang/String;
    //   447: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   450: pop
    //   451: aload_0
    //   452: getfield mBleOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   455: aload #5
    //   457: putfield preStatesTimeStamp : Ljava/lang/Long;
    //   460: aload_0
    //   461: getfield mBleOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   464: astore_1
    //   465: goto -> 1429
    //   468: aload_2
    //   469: astore_1
    //   470: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   473: ifeq -> 1429
    //   476: ldc 'OplusBtSwitchEventStats_fwk'
    //   478: ldc_w 'ble off recorder is empty for state_ble_on'
    //   481: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   484: pop
    //   485: aload_2
    //   486: astore_1
    //   487: goto -> 1429
    //   490: aload_0
    //   491: getfield mBtOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   494: invokevirtual isEmpty : ()Z
    //   497: ifne -> 557
    //   500: aload_0
    //   501: getfield mBtOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   504: getfield optionalStateInfo : Ljava/util/HashMap;
    //   507: astore #6
    //   509: aload #5
    //   511: invokevirtual longValue : ()J
    //   514: lstore #7
    //   516: aload_0
    //   517: getfield mBtOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   520: getfield preStatesTimeStamp : Ljava/lang/Long;
    //   523: invokevirtual longValue : ()J
    //   526: lstore #9
    //   528: aload #6
    //   530: aload_1
    //   531: lload #7
    //   533: lload #9
    //   535: lsub
    //   536: invokestatic valueOf : (J)Ljava/lang/String;
    //   539: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   542: pop
    //   543: aload_0
    //   544: getfield mBtOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   547: aload #5
    //   549: putfield preStatesTimeStamp : Ljava/lang/Long;
    //   552: aload_2
    //   553: astore_1
    //   554: goto -> 1429
    //   557: aload_2
    //   558: astore_1
    //   559: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   562: ifeq -> 1429
    //   565: ldc 'OplusBtSwitchEventStats_fwk'
    //   567: ldc_w 'bt off recorder is empty for state_ble_on'
    //   570: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   573: pop
    //   574: aload_2
    //   575: astore_1
    //   576: goto -> 1429
    //   579: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   582: ifeq -> 594
    //   585: ldc 'OplusBtSwitchEventStats_fwk'
    //   587: ldc_w 'invalid ble turn on state.'
    //   590: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   593: pop
    //   594: aload_0
    //   595: aload_1
    //   596: aload #5
    //   598: invokespecial recordUnexpectedStateChangeEvent : (Ljava/lang/String;Ljava/lang/Long;)Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   601: astore_1
    //   602: goto -> 1429
    //   605: iload_3
    //   606: bipush #10
    //   608: if_icmpne -> 796
    //   611: aload_0
    //   612: getfield mEnableCompletely : Z
    //   615: ifne -> 707
    //   618: aload_0
    //   619: getfield mBleOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   622: invokevirtual isEmpty : ()Z
    //   625: ifne -> 685
    //   628: aload_0
    //   629: getfield mBleOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   632: getfield optionalStateInfo : Ljava/util/HashMap;
    //   635: astore #6
    //   637: aload #5
    //   639: invokevirtual longValue : ()J
    //   642: lstore #7
    //   644: aload_0
    //   645: getfield mBleOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   648: getfield preStatesTimeStamp : Ljava/lang/Long;
    //   651: invokevirtual longValue : ()J
    //   654: lstore #9
    //   656: aload #6
    //   658: aload_1
    //   659: lload #7
    //   661: lload #9
    //   663: lsub
    //   664: invokestatic valueOf : (J)Ljava/lang/String;
    //   667: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   670: pop
    //   671: aload_0
    //   672: getfield mBleOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   675: aload #5
    //   677: putfield preStatesTimeStamp : Ljava/lang/Long;
    //   680: aload_2
    //   681: astore_1
    //   682: goto -> 1429
    //   685: aload_2
    //   686: astore_1
    //   687: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   690: ifeq -> 1429
    //   693: ldc 'OplusBtSwitchEventStats_fwk'
    //   695: ldc_w 'ble on recorder is empty for state_ble_turning_on'
    //   698: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   701: pop
    //   702: aload_2
    //   703: astore_1
    //   704: goto -> 1429
    //   707: aload_0
    //   708: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   711: invokevirtual isEmpty : ()Z
    //   714: ifne -> 774
    //   717: aload_0
    //   718: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   721: getfield optionalStateInfo : Ljava/util/HashMap;
    //   724: astore #6
    //   726: aload #5
    //   728: invokevirtual longValue : ()J
    //   731: lstore #7
    //   733: aload_0
    //   734: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   737: getfield preStatesTimeStamp : Ljava/lang/Long;
    //   740: invokevirtual longValue : ()J
    //   743: lstore #9
    //   745: aload #6
    //   747: aload_1
    //   748: lload #7
    //   750: lload #9
    //   752: lsub
    //   753: invokestatic valueOf : (J)Ljava/lang/String;
    //   756: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   759: pop
    //   760: aload_0
    //   761: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   764: aload #5
    //   766: putfield preStatesTimeStamp : Ljava/lang/Long;
    //   769: aload_2
    //   770: astore_1
    //   771: goto -> 1429
    //   774: aload_2
    //   775: astore_1
    //   776: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   779: ifeq -> 1429
    //   782: ldc 'OplusBtSwitchEventStats_fwk'
    //   784: ldc_w 'bt on recorder is empty for state_ble_turning_on'
    //   787: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   790: pop
    //   791: aload_2
    //   792: astore_1
    //   793: goto -> 1429
    //   796: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   799: ifeq -> 811
    //   802: ldc 'OplusBtSwitchEventStats_fwk'
    //   804: ldc_w 'invalid ble turning on state.'
    //   807: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   810: pop
    //   811: aload_0
    //   812: aload_1
    //   813: aload #5
    //   815: invokespecial recordUnexpectedStateChangeEvent : (Ljava/lang/String;Ljava/lang/Long;)Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   818: astore_1
    //   819: goto -> 1429
    //   822: iload_3
    //   823: bipush #12
    //   825: if_icmpne -> 917
    //   828: aload_0
    //   829: getfield mBtOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   832: invokevirtual isEmpty : ()Z
    //   835: ifne -> 895
    //   838: aload_0
    //   839: getfield mBtOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   842: getfield optionalStateInfo : Ljava/util/HashMap;
    //   845: astore #6
    //   847: aload #5
    //   849: invokevirtual longValue : ()J
    //   852: lstore #7
    //   854: aload_0
    //   855: getfield mBtOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   858: getfield preStatesTimeStamp : Ljava/lang/Long;
    //   861: invokevirtual longValue : ()J
    //   864: lstore #9
    //   866: aload #6
    //   868: aload_1
    //   869: lload #7
    //   871: lload #9
    //   873: lsub
    //   874: invokestatic valueOf : (J)Ljava/lang/String;
    //   877: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   880: pop
    //   881: aload_0
    //   882: getfield mBtOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   885: aload #5
    //   887: putfield preStatesTimeStamp : Ljava/lang/Long;
    //   890: aload_2
    //   891: astore_1
    //   892: goto -> 1429
    //   895: aload_2
    //   896: astore_1
    //   897: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   900: ifeq -> 1429
    //   903: ldc 'OplusBtSwitchEventStats_fwk'
    //   905: ldc_w 'bt off recorder is empty for state_turning_off'
    //   908: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   911: pop
    //   912: aload_2
    //   913: astore_1
    //   914: goto -> 1429
    //   917: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   920: ifeq -> 932
    //   923: ldc 'OplusBtSwitchEventStats_fwk'
    //   925: ldc_w 'invalid bt turning off state.'
    //   928: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   931: pop
    //   932: aload_0
    //   933: aload_1
    //   934: aload #5
    //   936: invokespecial recordUnexpectedStateChangeEvent : (Ljava/lang/String;Ljava/lang/Long;)Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   939: astore_1
    //   940: goto -> 1429
    //   943: iload_3
    //   944: bipush #11
    //   946: if_icmpne -> 1048
    //   949: aload_0
    //   950: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   953: invokevirtual isEmpty : ()Z
    //   956: ifne -> 1026
    //   959: aload_0
    //   960: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   963: ldc 'success'
    //   965: putfield result : Ljava/lang/String;
    //   968: aload_0
    //   969: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   972: getfield optionalStateInfo : Ljava/util/HashMap;
    //   975: astore_2
    //   976: aload #5
    //   978: invokevirtual longValue : ()J
    //   981: lstore #7
    //   983: aload_0
    //   984: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   987: getfield preStatesTimeStamp : Ljava/lang/Long;
    //   990: invokevirtual longValue : ()J
    //   993: lstore #9
    //   995: aload_2
    //   996: aload_1
    //   997: lload #7
    //   999: lload #9
    //   1001: lsub
    //   1002: invokestatic valueOf : (J)Ljava/lang/String;
    //   1005: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1008: pop
    //   1009: aload_0
    //   1010: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1013: aload #5
    //   1015: putfield preStatesTimeStamp : Ljava/lang/Long;
    //   1018: aload_0
    //   1019: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1022: astore_1
    //   1023: goto -> 1429
    //   1026: aload_2
    //   1027: astore_1
    //   1028: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   1031: ifeq -> 1429
    //   1034: ldc 'OplusBtSwitchEventStats_fwk'
    //   1036: ldc_w 'bt on recorder is empty for state_on'
    //   1039: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1042: pop
    //   1043: aload_2
    //   1044: astore_1
    //   1045: goto -> 1429
    //   1048: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   1051: ifeq -> 1063
    //   1054: ldc 'OplusBtSwitchEventStats_fwk'
    //   1056: ldc_w 'invalid bt turn on state.'
    //   1059: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1062: pop
    //   1063: aload_0
    //   1064: aload_1
    //   1065: aload #5
    //   1067: invokespecial recordUnexpectedStateChangeEvent : (Ljava/lang/String;Ljava/lang/Long;)Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1070: astore_1
    //   1071: goto -> 1429
    //   1074: iload_3
    //   1075: bipush #15
    //   1077: if_icmpne -> 1169
    //   1080: aload_0
    //   1081: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1084: invokevirtual isEmpty : ()Z
    //   1087: ifne -> 1147
    //   1090: aload_0
    //   1091: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1094: getfield optionalStateInfo : Ljava/util/HashMap;
    //   1097: astore #6
    //   1099: aload #5
    //   1101: invokevirtual longValue : ()J
    //   1104: lstore #9
    //   1106: aload_0
    //   1107: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1110: getfield preStatesTimeStamp : Ljava/lang/Long;
    //   1113: invokevirtual longValue : ()J
    //   1116: lstore #7
    //   1118: aload #6
    //   1120: aload_1
    //   1121: lload #9
    //   1123: lload #7
    //   1125: lsub
    //   1126: invokestatic valueOf : (J)Ljava/lang/String;
    //   1129: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1132: pop
    //   1133: aload_0
    //   1134: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1137: aload #5
    //   1139: putfield preStatesTimeStamp : Ljava/lang/Long;
    //   1142: aload_2
    //   1143: astore_1
    //   1144: goto -> 1429
    //   1147: aload_2
    //   1148: astore_1
    //   1149: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   1152: ifeq -> 1429
    //   1155: ldc 'OplusBtSwitchEventStats_fwk'
    //   1157: ldc_w 'bt on recorder is empty for state_truning_on'
    //   1160: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1163: pop
    //   1164: aload_2
    //   1165: astore_1
    //   1166: goto -> 1429
    //   1169: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   1172: ifeq -> 1184
    //   1175: ldc 'OplusBtSwitchEventStats_fwk'
    //   1177: ldc_w 'invalid bt turning on state.'
    //   1180: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1183: pop
    //   1184: aload_0
    //   1185: aload_1
    //   1186: aload #5
    //   1188: invokespecial recordUnexpectedStateChangeEvent : (Ljava/lang/String;Ljava/lang/Long;)Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1191: astore_1
    //   1192: goto -> 1429
    //   1195: iload_3
    //   1196: bipush #16
    //   1198: if_icmpne -> 1406
    //   1201: aload_0
    //   1202: getfield mDisableCompletely : Z
    //   1205: ifne -> 1307
    //   1208: aload_0
    //   1209: getfield mBleOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1212: invokevirtual isEmpty : ()Z
    //   1215: ifne -> 1285
    //   1218: aload_0
    //   1219: getfield mBleOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1222: ldc 'success'
    //   1224: putfield result : Ljava/lang/String;
    //   1227: aload_0
    //   1228: getfield mBleOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1231: getfield optionalStateInfo : Ljava/util/HashMap;
    //   1234: astore_2
    //   1235: aload #5
    //   1237: invokevirtual longValue : ()J
    //   1240: lstore #9
    //   1242: aload_0
    //   1243: getfield mBleOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1246: getfield preStatesTimeStamp : Ljava/lang/Long;
    //   1249: invokevirtual longValue : ()J
    //   1252: lstore #7
    //   1254: aload_2
    //   1255: aload_1
    //   1256: lload #9
    //   1258: lload #7
    //   1260: lsub
    //   1261: invokestatic valueOf : (J)Ljava/lang/String;
    //   1264: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1267: pop
    //   1268: aload_0
    //   1269: getfield mBleOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1272: aload #5
    //   1274: putfield preStatesTimeStamp : Ljava/lang/Long;
    //   1277: aload_0
    //   1278: getfield mBleOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1281: astore_1
    //   1282: goto -> 1429
    //   1285: aload_2
    //   1286: astore_1
    //   1287: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   1290: ifeq -> 1429
    //   1293: ldc 'OplusBtSwitchEventStats_fwk'
    //   1295: ldc_w 'ble off recorder is empty for state_off'
    //   1298: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1301: pop
    //   1302: aload_2
    //   1303: astore_1
    //   1304: goto -> 1429
    //   1307: aload_0
    //   1308: getfield mBtOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1311: invokevirtual isEmpty : ()Z
    //   1314: ifne -> 1384
    //   1317: aload_0
    //   1318: getfield mBtOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1321: ldc 'success'
    //   1323: putfield result : Ljava/lang/String;
    //   1326: aload_0
    //   1327: getfield mBtOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1330: getfield optionalStateInfo : Ljava/util/HashMap;
    //   1333: astore_2
    //   1334: aload #5
    //   1336: invokevirtual longValue : ()J
    //   1339: lstore #9
    //   1341: aload_0
    //   1342: getfield mBtOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1345: getfield preStatesTimeStamp : Ljava/lang/Long;
    //   1348: invokevirtual longValue : ()J
    //   1351: lstore #7
    //   1353: aload_2
    //   1354: aload_1
    //   1355: lload #9
    //   1357: lload #7
    //   1359: lsub
    //   1360: invokestatic valueOf : (J)Ljava/lang/String;
    //   1363: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1366: pop
    //   1367: aload_0
    //   1368: getfield mBleOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1371: aload #5
    //   1373: putfield preStatesTimeStamp : Ljava/lang/Long;
    //   1376: aload_0
    //   1377: getfield mBtOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1380: astore_1
    //   1381: goto -> 1429
    //   1384: aload_2
    //   1385: astore_1
    //   1386: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   1389: ifeq -> 1429
    //   1392: ldc 'OplusBtSwitchEventStats_fwk'
    //   1394: ldc_w 'bt off recorder is empty for state_off'
    //   1397: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1400: pop
    //   1401: aload_2
    //   1402: astore_1
    //   1403: goto -> 1429
    //   1406: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   1409: ifeq -> 1421
    //   1412: ldc 'OplusBtSwitchEventStats_fwk'
    //   1414: ldc_w 'invalid turn off state.'
    //   1417: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1420: pop
    //   1421: aload_0
    //   1422: aload_1
    //   1423: aload #5
    //   1425: invokespecial recordUnexpectedStateChangeEvent : (Ljava/lang/String;Ljava/lang/Long;)Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   1428: astore_1
    //   1429: aload_0
    //   1430: monitorenter
    //   1431: aload_1
    //   1432: ifnull -> 1476
    //   1435: aload_1
    //   1436: aload_0
    //   1437: getfield mBleAppCallFlag : Z
    //   1440: putfield bleFlag : Z
    //   1443: aload_1
    //   1444: aload_0
    //   1445: getfield mQuietEnableFlag : Z
    //   1448: putfield isQuietEnable : Z
    //   1451: aload_1
    //   1452: aload #5
    //   1454: invokevirtual longValue : ()J
    //   1457: aload_1
    //   1458: getfield callTimeStamp : Ljava/lang/Long;
    //   1461: invokevirtual longValue : ()J
    //   1464: lsub
    //   1465: invokestatic valueOf : (J)Ljava/lang/Long;
    //   1468: putfield duration : Ljava/lang/Long;
    //   1471: aload_0
    //   1472: aload_1
    //   1473: invokespecial processUploadEvent : (Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;)V
    //   1476: aload_0
    //   1477: monitorexit
    //   1478: return
    //   1479: astore_1
    //   1480: aload_0
    //   1481: monitorexit
    //   1482: aload_1
    //   1483: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #355	-> 0
    //   #356	-> 15
    //   #358	-> 17
    //   #359	-> 22
    //   #360	-> 28
    //   #362	-> 35
    //   #363	-> 41
    //   #364	-> 66
    //   #365	-> 81
    //   #367	-> 89
    //   #368	-> 95
    //   #371	-> 131
    //   #441	-> 177
    //   #442	-> 183
    //   #444	-> 190
    //   #445	-> 200
    //   #446	-> 209
    //   #447	-> 217
    //   #446	-> 236
    //   #448	-> 250
    //   #449	-> 259
    //   #451	-> 267
    //   #452	-> 275
    //   #457	-> 289
    //   #458	-> 299
    //   #459	-> 308
    //   #458	-> 327
    //   #460	-> 342
    //   #462	-> 356
    //   #463	-> 364
    //   #467	-> 378
    //   #468	-> 384
    //   #470	-> 391
    //   #471	-> 401
    //   #472	-> 410
    //   #473	-> 418
    //   #472	-> 437
    //   #474	-> 451
    //   #475	-> 460
    //   #477	-> 468
    //   #478	-> 476
    //   #483	-> 490
    //   #484	-> 500
    //   #485	-> 509
    //   #484	-> 528
    //   #486	-> 543
    //   #488	-> 557
    //   #489	-> 565
    //   #494	-> 579
    //   #495	-> 585
    //   #497	-> 594
    //   #499	-> 602
    //   #409	-> 605
    //   #410	-> 611
    //   #412	-> 618
    //   #413	-> 628
    //   #414	-> 637
    //   #413	-> 656
    //   #415	-> 671
    //   #417	-> 685
    //   #418	-> 693
    //   #423	-> 707
    //   #424	-> 717
    //   #425	-> 726
    //   #424	-> 745
    //   #426	-> 760
    //   #428	-> 774
    //   #429	-> 782
    //   #434	-> 796
    //   #435	-> 802
    //   #437	-> 811
    //   #439	-> 819
    //   #541	-> 822
    //   #543	-> 828
    //   #544	-> 838
    //   #545	-> 847
    //   #544	-> 866
    //   #546	-> 881
    //   #548	-> 895
    //   #549	-> 903
    //   #553	-> 917
    //   #554	-> 923
    //   #556	-> 932
    //   #520	-> 943
    //   #522	-> 949
    //   #523	-> 959
    //   #524	-> 968
    //   #525	-> 976
    //   #524	-> 995
    //   #526	-> 1009
    //   #527	-> 1018
    //   #529	-> 1026
    //   #530	-> 1034
    //   #534	-> 1048
    //   #535	-> 1054
    //   #537	-> 1063
    //   #539	-> 1071
    //   #501	-> 1074
    //   #503	-> 1080
    //   #504	-> 1090
    //   #505	-> 1099
    //   #504	-> 1118
    //   #506	-> 1133
    //   #508	-> 1147
    //   #509	-> 1155
    //   #513	-> 1169
    //   #514	-> 1175
    //   #516	-> 1184
    //   #518	-> 1192
    //   #373	-> 1195
    //   #374	-> 1201
    //   #376	-> 1208
    //   #377	-> 1218
    //   #378	-> 1227
    //   #379	-> 1235
    //   #378	-> 1254
    //   #380	-> 1268
    //   #381	-> 1277
    //   #383	-> 1285
    //   #384	-> 1293
    //   #389	-> 1307
    //   #390	-> 1317
    //   #391	-> 1326
    //   #392	-> 1334
    //   #391	-> 1353
    //   #393	-> 1367
    //   #394	-> 1376
    //   #396	-> 1384
    //   #397	-> 1392
    //   #402	-> 1406
    //   #403	-> 1412
    //   #405	-> 1421
    //   #407	-> 1429
    //   #560	-> 1429
    //   #561	-> 1431
    //   #562	-> 1435
    //   #563	-> 1443
    //   #564	-> 1451
    //   #565	-> 1471
    //   #567	-> 1476
    //   #568	-> 1478
    //   #567	-> 1479
    // Exception table:
    //   from	to	target	type
    //   1435	1443	1479	finally
    //   1443	1451	1479	finally
    //   1451	1471	1479	finally
    //   1471	1476	1479	finally
    //   1476	1478	1479	finally
    //   1480	1482	1479	finally
  }
  
  private void processCrashEvent(Message paramMessage) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mEnableCompletely : Z
    //   6: ifne -> 27
    //   9: aload_0
    //   10: getfield mCurrentAdapterState : I
    //   13: bipush #12
    //   15: if_icmpeq -> 43
    //   18: aload_0
    //   19: getfield mCurrentAdapterState : I
    //   22: bipush #15
    //   24: if_icmpeq -> 43
    //   27: aload_0
    //   28: getfield mEnableCompletely : Z
    //   31: ifeq -> 83
    //   34: aload_0
    //   35: getfield mCurrentAdapterState : I
    //   38: bipush #12
    //   40: if_icmpne -> 83
    //   43: new android/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo
    //   46: astore_1
    //   47: aload_1
    //   48: aload_0
    //   49: ldc 'crash()'
    //   51: invokespecial <init> : (Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats;Ljava/lang/String;)V
    //   54: aload_1
    //   55: invokestatic currentTimeMillis : ()J
    //   58: invokestatic valueOf : (J)Ljava/lang/Long;
    //   61: putfield callTimeStamp : Ljava/lang/Long;
    //   64: aload_1
    //   65: aload_0
    //   66: getfield mBleAppCallFlag : Z
    //   69: putfield bleFlag : Z
    //   72: aload_1
    //   73: ldc 'fail'
    //   75: putfield result : Ljava/lang/String;
    //   78: aload_0
    //   79: aload_1
    //   80: invokespecial processUploadEvent : (Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;)V
    //   83: aload_0
    //   84: monitorexit
    //   85: return
    //   86: astore_1
    //   87: aload_0
    //   88: monitorexit
    //   89: aload_1
    //   90: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #574	-> 2
    //   #576	-> 43
    //   #577	-> 54
    //   #578	-> 64
    //   #579	-> 72
    //   #580	-> 78
    //   #582	-> 83
    //   #573	-> 86
    // Exception table:
    //   from	to	target	type
    //   2	27	86	finally
    //   27	43	86	finally
    //   43	54	86	finally
    //   54	64	86	finally
    //   64	72	86	finally
    //   72	78	86	finally
    //   78	83	86	finally
  }
  
  private void processErrorEvent(Message paramMessage) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   5: ifeq -> 17
    //   8: ldc 'OplusBtSwitchEventStats_fwk'
    //   10: ldc_w 'processErrorEvent(): enter.'
    //   13: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   16: pop
    //   17: aconst_null
    //   18: astore_2
    //   19: aload_1
    //   20: getfield what : I
    //   23: tableswitch default -> 84, 5 -> 464, 6 -> 306, 7 -> 306, 8 -> 89, 9 -> 306, 10 -> 306, 11 -> 84, 12 -> 306, 13 -> 89, 14 -> 89, 15 -> 84, 16 -> 89
    //   84: aload_2
    //   85: astore_3
    //   86: goto -> 566
    //   89: aload_0
    //   90: getfield mEnableCompletely : Z
    //   93: ifne -> 201
    //   96: aload_0
    //   97: getfield mBleOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   100: invokevirtual isEmpty : ()Z
    //   103: ifne -> 149
    //   106: aload_0
    //   107: getfield mBleOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   110: aload_0
    //   111: getfield mBleAppCallFlag : Z
    //   114: putfield bleFlag : Z
    //   117: aload_0
    //   118: getfield mBleOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   121: aload_0
    //   122: aload_1
    //   123: getfield what : I
    //   126: invokespecial recordEventToString : (I)Ljava/lang/String;
    //   129: putfield reason : Ljava/lang/String;
    //   132: aload_0
    //   133: getfield mBleOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   136: ldc 'fail'
    //   138: putfield result : Ljava/lang/String;
    //   141: aload_0
    //   142: getfield mBleOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   145: astore_3
    //   146: goto -> 566
    //   149: aload_2
    //   150: astore_3
    //   151: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   154: ifeq -> 566
    //   157: new java/lang/StringBuilder
    //   160: astore_3
    //   161: aload_3
    //   162: invokespecial <init> : ()V
    //   165: aload_3
    //   166: ldc_w 'ble on recorder is empty for '
    //   169: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   172: pop
    //   173: aload_3
    //   174: aload_0
    //   175: aload_1
    //   176: getfield what : I
    //   179: invokespecial recordEventToString : (I)Ljava/lang/String;
    //   182: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   185: pop
    //   186: ldc 'OplusBtSwitchEventStats_fwk'
    //   188: aload_3
    //   189: invokevirtual toString : ()Ljava/lang/String;
    //   192: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   195: pop
    //   196: aload_2
    //   197: astore_3
    //   198: goto -> 566
    //   201: aload_0
    //   202: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   205: invokevirtual isEmpty : ()Z
    //   208: ifne -> 254
    //   211: aload_0
    //   212: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   215: aload_0
    //   216: getfield mBleAppCallFlag : Z
    //   219: putfield bleFlag : Z
    //   222: aload_0
    //   223: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   226: aload_0
    //   227: aload_1
    //   228: getfield what : I
    //   231: invokespecial recordEventToString : (I)Ljava/lang/String;
    //   234: putfield reason : Ljava/lang/String;
    //   237: aload_0
    //   238: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   241: ldc 'fail'
    //   243: putfield result : Ljava/lang/String;
    //   246: aload_0
    //   247: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   250: astore_3
    //   251: goto -> 566
    //   254: aload_2
    //   255: astore_3
    //   256: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   259: ifeq -> 566
    //   262: new java/lang/StringBuilder
    //   265: astore_3
    //   266: aload_3
    //   267: invokespecial <init> : ()V
    //   270: aload_3
    //   271: ldc_w 'bt on recorder is empty for '
    //   274: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   277: pop
    //   278: aload_3
    //   279: aload_0
    //   280: aload_1
    //   281: getfield what : I
    //   284: invokespecial recordEventToString : (I)Ljava/lang/String;
    //   287: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   290: pop
    //   291: ldc 'OplusBtSwitchEventStats_fwk'
    //   293: aload_3
    //   294: invokevirtual toString : ()Ljava/lang/String;
    //   297: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   300: pop
    //   301: aload_2
    //   302: astore_3
    //   303: goto -> 566
    //   306: aload_0
    //   307: getfield mBtOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   310: invokevirtual isEmpty : ()Z
    //   313: ifne -> 359
    //   316: aload_0
    //   317: getfield mBtOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   320: aload_0
    //   321: getfield mBleAppCallFlag : Z
    //   324: putfield bleFlag : Z
    //   327: aload_0
    //   328: getfield mBtOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   331: aload_0
    //   332: aload_1
    //   333: getfield what : I
    //   336: invokespecial recordEventToString : (I)Ljava/lang/String;
    //   339: putfield reason : Ljava/lang/String;
    //   342: aload_0
    //   343: getfield mBtOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   346: ldc 'fail'
    //   348: putfield result : Ljava/lang/String;
    //   351: aload_0
    //   352: getfield mBtOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   355: astore_3
    //   356: goto -> 566
    //   359: aload_0
    //   360: getfield mBleOffSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   363: invokevirtual isEmpty : ()Z
    //   366: ifne -> 412
    //   369: aload_0
    //   370: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   373: aload_0
    //   374: getfield mBleAppCallFlag : Z
    //   377: putfield bleFlag : Z
    //   380: aload_0
    //   381: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   384: aload_0
    //   385: aload_1
    //   386: getfield what : I
    //   389: invokespecial recordEventToString : (I)Ljava/lang/String;
    //   392: putfield reason : Ljava/lang/String;
    //   395: aload_0
    //   396: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   399: ldc 'fail'
    //   401: putfield result : Ljava/lang/String;
    //   404: aload_0
    //   405: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   408: astore_3
    //   409: goto -> 566
    //   412: aload_2
    //   413: astore_3
    //   414: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   417: ifeq -> 566
    //   420: new java/lang/StringBuilder
    //   423: astore_3
    //   424: aload_3
    //   425: invokespecial <init> : ()V
    //   428: aload_3
    //   429: ldc_w 'bt off recorder is empty for '
    //   432: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   435: pop
    //   436: aload_3
    //   437: aload_0
    //   438: aload_1
    //   439: getfield what : I
    //   442: invokespecial recordEventToString : (I)Ljava/lang/String;
    //   445: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   448: pop
    //   449: ldc 'OplusBtSwitchEventStats_fwk'
    //   451: aload_3
    //   452: invokevirtual toString : ()Ljava/lang/String;
    //   455: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   458: pop
    //   459: aload_2
    //   460: astore_3
    //   461: goto -> 566
    //   464: aload_0
    //   465: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   468: invokevirtual isEmpty : ()Z
    //   471: ifne -> 517
    //   474: aload_0
    //   475: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   478: aload_0
    //   479: getfield mBleAppCallFlag : Z
    //   482: putfield bleFlag : Z
    //   485: aload_0
    //   486: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   489: aload_0
    //   490: aload_1
    //   491: getfield what : I
    //   494: invokespecial recordEventToString : (I)Ljava/lang/String;
    //   497: putfield reason : Ljava/lang/String;
    //   500: aload_0
    //   501: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   504: ldc 'fail'
    //   506: putfield result : Ljava/lang/String;
    //   509: aload_0
    //   510: getfield mBtOnSwitchInfo : Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;
    //   513: astore_3
    //   514: goto -> 566
    //   517: aload_2
    //   518: astore_3
    //   519: getstatic android/bluetooth/feature/dcs/OplusBtSwitchEventStats.DBG : Z
    //   522: ifeq -> 566
    //   525: new java/lang/StringBuilder
    //   528: astore_3
    //   529: aload_3
    //   530: invokespecial <init> : ()V
    //   533: aload_3
    //   534: ldc_w 'bt on recorder is empty for '
    //   537: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   540: pop
    //   541: aload_3
    //   542: aload_0
    //   543: aload_1
    //   544: getfield what : I
    //   547: invokespecial recordEventToString : (I)Ljava/lang/String;
    //   550: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   553: pop
    //   554: ldc 'OplusBtSwitchEventStats_fwk'
    //   556: aload_3
    //   557: invokevirtual toString : ()Ljava/lang/String;
    //   560: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   563: pop
    //   564: aload_2
    //   565: astore_3
    //   566: aload_3
    //   567: ifnull -> 575
    //   570: aload_0
    //   571: aload_3
    //   572: invokespecial processUploadEvent : (Landroid/bluetooth/feature/dcs/OplusBtSwitchEventStats$SwitchRecorderInfo;)V
    //   575: aload_0
    //   576: monitorexit
    //   577: return
    //   578: astore_1
    //   579: aload_0
    //   580: monitorexit
    //   581: aload_1
    //   582: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #588	-> 2
    //   #589	-> 8
    //   #591	-> 17
    //   #592	-> 19
    //   #597	-> 89
    //   #598	-> 96
    //   #599	-> 106
    //   #600	-> 117
    //   #601	-> 132
    //   #602	-> 141
    //   #604	-> 149
    //   #605	-> 157
    //   #609	-> 201
    //   #610	-> 211
    //   #611	-> 222
    //   #612	-> 237
    //   #613	-> 246
    //   #615	-> 254
    //   #616	-> 262
    //   #638	-> 306
    //   #639	-> 316
    //   #640	-> 327
    //   #641	-> 342
    //   #642	-> 351
    //   #643	-> 359
    //   #644	-> 369
    //   #645	-> 380
    //   #646	-> 395
    //   #647	-> 404
    //   #649	-> 412
    //   #650	-> 420
    //   #622	-> 464
    //   #623	-> 474
    //   #624	-> 485
    //   #625	-> 500
    //   #626	-> 509
    //   #628	-> 517
    //   #629	-> 525
    //   #657	-> 566
    //   #658	-> 570
    //   #660	-> 575
    //   #587	-> 578
    // Exception table:
    //   from	to	target	type
    //   2	8	578	finally
    //   8	17	578	finally
    //   19	84	578	finally
    //   89	96	578	finally
    //   96	106	578	finally
    //   106	117	578	finally
    //   117	132	578	finally
    //   132	141	578	finally
    //   141	146	578	finally
    //   151	157	578	finally
    //   157	196	578	finally
    //   201	211	578	finally
    //   211	222	578	finally
    //   222	237	578	finally
    //   237	246	578	finally
    //   246	251	578	finally
    //   256	262	578	finally
    //   262	301	578	finally
    //   306	316	578	finally
    //   316	327	578	finally
    //   327	342	578	finally
    //   342	351	578	finally
    //   351	356	578	finally
    //   359	369	578	finally
    //   369	380	578	finally
    //   380	395	578	finally
    //   395	404	578	finally
    //   404	409	578	finally
    //   414	420	578	finally
    //   420	459	578	finally
    //   464	474	578	finally
    //   474	485	578	finally
    //   485	500	578	finally
    //   500	509	578	finally
    //   509	514	578	finally
    //   519	525	578	finally
    //   525	564	578	finally
    //   570	575	578	finally
  }
  
  private void processUploadEvent(SwitchRecorderInfo paramSwitchRecorderInfo) {
    String str;
    if (DBG)
      Log.d("OplusBtSwitchEventStats_fwk", "processUploadEvent(): enter."); 
    if (paramSwitchRecorderInfo == null)
      return; 
    HashMap<Object, Object> hashMap = new HashMap<>();
    if (this.isRecorderDetailed)
      for (Map.Entry<String, String> entry : paramSwitchRecorderInfo.optionalStateInfo.entrySet())
        hashMap.put(entry.getKey(), entry.getValue());  
    hashMap.put("issueType", "switch_event");
    hashMap.put("recordLayer", paramSwitchRecorderInfo.recordLayer);
    hashMap.put("switchEvent", paramSwitchRecorderInfo.switchEvent);
    hashMap.put("result", paramSwitchRecorderInfo.result);
    hashMap.put("reason", paramSwitchRecorderInfo.reason);
    hashMap.put("callReason", paramSwitchRecorderInfo.callReason);
    hashMap.put("callTimeStamp", OplusBtDcsUtils.timeStampFormat(paramSwitchRecorderInfo.callTimeStamp.longValue()));
    hashMap.put("duration", String.valueOf(paramSwitchRecorderInfo.duration));
    if (paramSwitchRecorderInfo.isQuietEnable) {
      str = "1";
    } else {
      str = "0";
    } 
    hashMap.put("quietEnable", str);
    hashMap.put("bleFlag", String.valueOf(paramSwitchRecorderInfo.bleFlag));
    hashMap.put("callApp", paramSwitchRecorderInfo.callApp);
    if (!hashMap.isEmpty())
      OplusBtDcsUtils.onCommon(this.mContext, "bt_data_collector", "bt_switch_event", (Map)hashMap, false); 
    paramSwitchRecorderInfo.resetInfo();
  }
  
  private SwitchRecorderInfo recordUnexpectedStateChangeEvent(String paramString, Long paramLong) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("recordUnexpectedStateChangeEvent() state event: ");
    stringBuilder.append(paramString);
    Log.d("OplusBtSwitchEventStats_fwk", stringBuilder.toString());
    SwitchRecorderInfo switchRecorderInfo = new SwitchRecorderInfo("default()");
    switchRecorderInfo.isEmpty = false;
    switchRecorderInfo.callTimeStamp = paramLong;
    switchRecorderInfo.optionalStateInfo.put(paramString, "0");
    return switchRecorderInfo;
  }
  
  private String recordEventToString(int paramInt) {
    switch (paramInt) {
      default:
        return Integer.toString(paramInt);
      case 17:
        return "bt_crash";
      case 16:
        return "bt_bind_failure";
      case 15:
        return "bt_LeServiceUp_timeout";
      case 14:
        return "bt_unbind_timeout";
      case 13:
        return "bt_bind_timeout";
      case 12:
        return "stack_disable_error";
      case 11:
        return "bt_forcekill_timeout";
      case 10:
        return "bredr_cleanup_timeout";
      case 9:
        return "stack_disable_timeout";
      case 8:
        return "ble_start_timeout";
      case 7:
        return "ble_stop_timeout";
      case 6:
        return "bredr_stop_timeout";
      case 5:
        return "bredr_start_timeout";
      case 4:
        return "adapter_state_change";
      case 3:
        return "enableNoAutoConnect()";
      case 2:
        return "disable()";
      case 1:
        break;
    } 
    return "enable()";
  }
  
  public class SwitchRecorderInfo {
    public String recordLayer = "framework_layer";
    
    public String switchEvent = "default()";
    
    public String result = "success";
    
    public String reason = "normal";
    
    public String callApp = null;
    
    public String callReason = "unknow_reason";
    
    public boolean bleFlag;
    
    public Long callTimeStamp;
    
    public Long duration;
    
    public boolean isEmpty;
    
    public boolean isQuietEnable;
    
    public HashMap<String, String> optionalStateInfo;
    
    public Long preStatesTimeStamp;
    
    final OplusBtSwitchEventStats this$0;
    
    public SwitchRecorderInfo(Long param1Long, String param1String1, String param1String2, String param1String3) {
      Long long_ = Long.valueOf(0L);
      this.preStatesTimeStamp = long_;
      this.duration = long_;
      this.isQuietEnable = false;
      this.bleFlag = false;
      this.isEmpty = true;
      this.optionalStateInfo = null;
      this.callTimeStamp = param1Long;
      this.callApp = param1String1;
      this.callReason = param1String3;
      this.switchEvent = param1String2;
      this.optionalStateInfo = new HashMap<>();
      this.isEmpty = true;
    }
    
    public SwitchRecorderInfo(String param1String) {
      Long long_ = Long.valueOf(0L);
      this.preStatesTimeStamp = long_;
      this.duration = long_;
      this.isQuietEnable = false;
      this.bleFlag = false;
      this.isEmpty = true;
      this.optionalStateInfo = null;
      this.switchEvent = param1String;
      this.isEmpty = true;
      this.optionalStateInfo = new HashMap<>();
    }
    
    public boolean isEmpty() {
      return this.isEmpty;
    }
    
    public void resetInfo() {
      this.result = "success";
      this.reason = "normal";
      this.callReason = "unknow_reason";
      this.callApp = null;
      Long long_ = Long.valueOf(0L);
      this.preStatesTimeStamp = long_;
      this.duration = long_;
      this.bleFlag = false;
      this.isQuietEnable = false;
      this.optionalStateInfo.clear();
      this.isEmpty = true;
    }
  }
}
