package android.bluetooth;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Process;
import android.os.RemoteException;
import android.os.UserHandle;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public final class BluetoothDun implements BluetoothProfile {
  public static final String ACTION_CONNECTION_STATE_CHANGED = "codeaurora.bluetooth.dun.profile.action.CONNECTION_STATE_CHANGED";
  
  private static final boolean DBG = false;
  
  private static final String TAG = "BluetoothDun";
  
  private static final boolean VDBG = false;
  
  private BluetoothAdapter mAdapter;
  
  private ServiceConnection mConnection;
  
  private Context mContext;
  
  private IBluetoothDun mDunService;
  
  private BluetoothProfile.ServiceListener mServiceListener;
  
  private IBluetoothStateChangeCallback mStateChangeCallback;
  
  BluetoothDun(Context paramContext, BluetoothProfile.ServiceListener paramServiceListener) {
    this.mStateChangeCallback = (IBluetoothStateChangeCallback)new Object(this);
    this.mConnection = new ServiceConnection() {
        final BluetoothDun this$0;
        
        public void onServiceConnected(ComponentName param1ComponentName, IBinder param1IBinder) {
          BluetoothDun.access$002(BluetoothDun.this, IBluetoothDun.Stub.asInterface(param1IBinder));
          if (BluetoothDun.this.mServiceListener != null)
            BluetoothDun.this.mServiceListener.onServiceConnected(22, BluetoothDun.this); 
        }
        
        public void onServiceDisconnected(ComponentName param1ComponentName) {
          BluetoothDun.access$002(BluetoothDun.this, null);
          if (BluetoothDun.this.mServiceListener != null)
            BluetoothDun.this.mServiceListener.onServiceDisconnected(22); 
        }
      };
    this.mContext = paramContext;
    this.mServiceListener = paramServiceListener;
    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    try {
      bluetoothAdapter.getBluetoothManager().registerStateChangeCallback(this.mStateChangeCallback);
    } catch (RemoteException remoteException) {
      Log.w("BluetoothDun", "Unable to register BluetoothStateChangeCallback", (Throwable)remoteException);
    } 
    Log.d("BluetoothDun", "BluetoothDun() call bindService");
    doBind();
  }
  
  boolean doBind() {
    Intent intent = new Intent(IBluetoothDun.class.getName());
    ComponentName componentName = intent.resolveSystemService(this.mContext.getPackageManager(), 0);
    intent.setComponent(componentName);
    if (componentName != null) {
      Context context = this.mContext;
      ServiceConnection serviceConnection = this.mConnection;
      UserHandle userHandle = Process.myUserHandle();
      if (context.bindServiceAsUser(intent, serviceConnection, 0, userHandle))
        return true; 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Could not bind to Bluetooth Dun Service with ");
    stringBuilder.append(intent);
    Log.e("BluetoothDun", stringBuilder.toString());
    return false;
  }
  
  void close() {
    this.mServiceListener = null;
    IBluetoothManager iBluetoothManager = this.mAdapter.getBluetoothManager();
    if (iBluetoothManager != null)
      try {
        iBluetoothManager.unregisterStateChangeCallback(this.mStateChangeCallback);
      } catch (RemoteException remoteException) {
        Log.w("BluetoothDun", "Unable to unregister BluetoothStateChangeCallback", (Throwable)remoteException);
      }  
    synchronized (this.mConnection) {
      IBluetoothDun iBluetoothDun = this.mDunService;
      if (iBluetoothDun != null)
        try {
          this.mDunService = null;
          this.mContext.unbindService(this.mConnection);
        } catch (Exception exception) {
          Log.e("BluetoothDun", "", exception);
        }  
      return;
    } 
  }
  
  protected void finalize() {
    close();
  }
  
  public boolean disconnect(BluetoothDevice paramBluetoothDevice) {
    if (this.mDunService != null && isEnabled() && isValidDevice(paramBluetoothDevice))
      try {
        return this.mDunService.disconnect(paramBluetoothDevice);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Stack:");
        stringBuilder.append(Log.getStackTraceString(new Throwable()));
        Log.e("BluetoothDun", stringBuilder.toString());
        return false;
      }  
    if (this.mDunService == null)
      Log.w("BluetoothDun", "Proxy not attached to service"); 
    return false;
  }
  
  public List<BluetoothDevice> getConnectedDevices() {
    if (this.mDunService != null && isEnabled())
      try {
        return this.mDunService.getConnectedDevices();
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Stack:");
        stringBuilder.append(Log.getStackTraceString(new Throwable()));
        Log.e("BluetoothDun", stringBuilder.toString());
        return new ArrayList<>();
      }  
    if (this.mDunService == null)
      Log.w("BluetoothDun", "Proxy not attached to service"); 
    return new ArrayList<>();
  }
  
  public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] paramArrayOfint) {
    if (this.mDunService != null && isEnabled())
      try {
        return this.mDunService.getDevicesMatchingConnectionStates(paramArrayOfint);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Stack:");
        stringBuilder.append(Log.getStackTraceString(new Throwable()));
        Log.e("BluetoothDun", stringBuilder.toString());
        return new ArrayList<>();
      }  
    if (this.mDunService == null)
      Log.w("BluetoothDun", "Proxy not attached to service"); 
    return new ArrayList<>();
  }
  
  public int getConnectionState(BluetoothDevice paramBluetoothDevice) {
    if (this.mDunService != null && isEnabled() && isValidDevice(paramBluetoothDevice))
      try {
        return this.mDunService.getConnectionState(paramBluetoothDevice);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Stack:");
        stringBuilder.append(Log.getStackTraceString(new Throwable()));
        Log.e("BluetoothDun", stringBuilder.toString());
        return 0;
      }  
    if (this.mDunService == null)
      Log.w("BluetoothDun", "Proxy not attached to service"); 
    return 0;
  }
  
  private boolean isEnabled() {
    if (this.mAdapter.getState() == 12)
      return true; 
    return false;
  }
  
  private boolean isValidDevice(BluetoothDevice paramBluetoothDevice) {
    if (paramBluetoothDevice == null)
      return false; 
    if (BluetoothAdapter.checkBluetoothAddress(paramBluetoothDevice.getAddress()))
      return true; 
    return false;
  }
  
  private static void log(String paramString) {
    Log.d("BluetoothDun", paramString);
  }
}
