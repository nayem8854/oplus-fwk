package android.bluetooth;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class BluetoothQualityReport implements Parcelable {
  class PacketType extends Enum<PacketType> {
    private static final PacketType[] $VALUES;
    
    private PacketType(BluetoothQualityReport this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static PacketType valueOf(String param1String) {
      return Enum.<PacketType>valueOf(PacketType.class, param1String);
    }
    
    public static PacketType[] values() {
      return (PacketType[])$VALUES.clone();
    }
    
    public static final PacketType INVALID = new PacketType("INVALID", 0);
    
    public static final PacketType TYPE_2DH1;
    
    public static final PacketType TYPE_2DH3;
    
    public static final PacketType TYPE_2DH5;
    
    public static final PacketType TYPE_2EV3;
    
    public static final PacketType TYPE_2EV5;
    
    public static final PacketType TYPE_3DH1;
    
    public static final PacketType TYPE_3DH3;
    
    public static final PacketType TYPE_3DH5;
    
    public static final PacketType TYPE_3EV3;
    
    public static final PacketType TYPE_3EV5;
    
    public static final PacketType TYPE_AUX1;
    
    public static final PacketType TYPE_DH1;
    
    public static final PacketType TYPE_DH3;
    
    public static final PacketType TYPE_DH5;
    
    public static final PacketType TYPE_DM1;
    
    public static final PacketType TYPE_DM3;
    
    public static final PacketType TYPE_DM5;
    
    public static final PacketType TYPE_DV;
    
    public static final PacketType TYPE_EV3;
    
    public static final PacketType TYPE_EV4;
    
    public static final PacketType TYPE_EV5;
    
    public static final PacketType TYPE_FHS = new PacketType("TYPE_FHS", 4);
    
    public static final PacketType TYPE_HV1 = new PacketType("TYPE_HV1", 5);
    
    public static final PacketType TYPE_HV2 = new PacketType("TYPE_HV2", 6);
    
    public static final PacketType TYPE_HV3 = new PacketType("TYPE_HV3", 7);
    
    public static final PacketType TYPE_ID = new PacketType("TYPE_ID", 1);
    
    public static final PacketType TYPE_NULL = new PacketType("TYPE_NULL", 2);
    
    public static final PacketType TYPE_POLL = new PacketType("TYPE_POLL", 3);
    
    private static PacketType[] sAllValues;
    
    static {
      TYPE_DV = new PacketType("TYPE_DV", 8);
      TYPE_EV3 = new PacketType("TYPE_EV3", 9);
      TYPE_EV4 = new PacketType("TYPE_EV4", 10);
      TYPE_EV5 = new PacketType("TYPE_EV5", 11);
      TYPE_2EV3 = new PacketType("TYPE_2EV3", 12);
      TYPE_2EV5 = new PacketType("TYPE_2EV5", 13);
      TYPE_3EV3 = new PacketType("TYPE_3EV3", 14);
      TYPE_3EV5 = new PacketType("TYPE_3EV5", 15);
      TYPE_DM1 = new PacketType("TYPE_DM1", 16);
      TYPE_DH1 = new PacketType("TYPE_DH1", 17);
      TYPE_DM3 = new PacketType("TYPE_DM3", 18);
      TYPE_DH3 = new PacketType("TYPE_DH3", 19);
      TYPE_DM5 = new PacketType("TYPE_DM5", 20);
      TYPE_DH5 = new PacketType("TYPE_DH5", 21);
      TYPE_AUX1 = new PacketType("TYPE_AUX1", 22);
      TYPE_2DH1 = new PacketType("TYPE_2DH1", 23);
      TYPE_2DH3 = new PacketType("TYPE_2DH3", 24);
      TYPE_2DH5 = new PacketType("TYPE_2DH5", 25);
      TYPE_3DH1 = new PacketType("TYPE_3DH1", 26);
      TYPE_3DH3 = new PacketType("TYPE_3DH3", 27);
      PacketType packetType = new PacketType("TYPE_3DH5", 28);
      $VALUES = new PacketType[] { 
          INVALID, TYPE_ID, TYPE_NULL, TYPE_POLL, TYPE_FHS, TYPE_HV1, TYPE_HV2, TYPE_HV3, TYPE_DV, TYPE_EV3, 
          TYPE_EV4, TYPE_EV5, TYPE_2EV3, TYPE_2EV5, TYPE_3EV3, TYPE_3EV5, TYPE_DM1, TYPE_DH1, TYPE_DM3, TYPE_DH3, 
          TYPE_DM5, TYPE_DH5, TYPE_AUX1, TYPE_2DH1, TYPE_2DH3, TYPE_2DH5, TYPE_3DH1, TYPE_3DH3, packetType };
      sAllValues = values();
    }
    
    static PacketType fromOrdinal(int param1Int) {
      PacketType[] arrayOfPacketType = sAllValues;
      if (param1Int < arrayOfPacketType.length)
        return arrayOfPacketType[param1Int]; 
      return INVALID;
    }
  }
  
  class ConnState extends Enum<ConnState> {
    private static final ConnState[] $VALUES;
    
    public static final ConnState CONN_ACTIVE = new ConnState("CONN_ACTIVE", 1, 129);
    
    public static final ConnState CONN_ANT_ACTIVE;
    
    public static final ConnState CONN_DISCONNECT_PENDING;
    
    public static final ConnState CONN_HOLD = new ConnState("CONN_HOLD", 2, 2);
    
    public static ConnState valueOf(String param1String) {
      return Enum.<ConnState>valueOf(ConnState.class, param1String);
    }
    
    public static ConnState[] values() {
      return (ConnState[])$VALUES.clone();
    }
    
    public static final ConnState CONN_IDLE = new ConnState("CONN_IDLE", 0, 0);
    
    public static final ConnState CONN_LE_ACTIVE;
    
    public static final ConnState CONN_LOCAL_LOOPBACK;
    
    public static final ConnState CONN_PAGE_SCAN;
    
    public static final ConnState CONN_PAGING;
    
    public static final ConnState CONN_PARK;
    
    public static final ConnState CONN_PARK_PEND;
    
    public static final ConnState CONN_RECONNECTING;
    
    public static final ConnState CONN_SEMI_CONN;
    
    public static final ConnState CONN_SNIFF_ACTIVE;
    
    public static final ConnState CONN_SNIFF_IDLE = new ConnState("CONN_SNIFF_IDLE", 3, 3);
    
    public static final ConnState CONN_SNIFF_MASTER_TRANSITION;
    
    public static final ConnState CONN_TRIGGER_SCAN;
    
    public static final ConnState CONN_UNPARK_ACTIVE;
    
    public static final ConnState CONN_UNPARK_PEND;
    
    private static ConnState[] sAllStates;
    
    private int mValue;
    
    static {
      CONN_SNIFF_ACTIVE = new ConnState("CONN_SNIFF_ACTIVE", 4, 132);
      CONN_SNIFF_MASTER_TRANSITION = new ConnState("CONN_SNIFF_MASTER_TRANSITION", 5, 133);
      CONN_PARK = new ConnState("CONN_PARK", 6, 6);
      CONN_PARK_PEND = new ConnState("CONN_PARK_PEND", 7, 71);
      CONN_UNPARK_PEND = new ConnState("CONN_UNPARK_PEND", 8, 8);
      CONN_UNPARK_ACTIVE = new ConnState("CONN_UNPARK_ACTIVE", 9, 137);
      CONN_DISCONNECT_PENDING = new ConnState("CONN_DISCONNECT_PENDING", 10, 74);
      CONN_PAGING = new ConnState("CONN_PAGING", 11, 11);
      CONN_PAGE_SCAN = new ConnState("CONN_PAGE_SCAN", 12, 12);
      CONN_LOCAL_LOOPBACK = new ConnState("CONN_LOCAL_LOOPBACK", 13, 13);
      CONN_LE_ACTIVE = new ConnState("CONN_LE_ACTIVE", 14, 14);
      CONN_ANT_ACTIVE = new ConnState("CONN_ANT_ACTIVE", 15, 15);
      CONN_TRIGGER_SCAN = new ConnState("CONN_TRIGGER_SCAN", 16, 16);
      CONN_RECONNECTING = new ConnState("CONN_RECONNECTING", 17, 17);
      ConnState connState = new ConnState("CONN_SEMI_CONN", 18, 18);
      $VALUES = new ConnState[] { 
          CONN_IDLE, CONN_ACTIVE, CONN_HOLD, CONN_SNIFF_IDLE, CONN_SNIFF_ACTIVE, CONN_SNIFF_MASTER_TRANSITION, CONN_PARK, CONN_PARK_PEND, CONN_UNPARK_PEND, CONN_UNPARK_ACTIVE, 
          CONN_DISCONNECT_PENDING, CONN_PAGING, CONN_PAGE_SCAN, CONN_LOCAL_LOOPBACK, CONN_LE_ACTIVE, CONN_ANT_ACTIVE, CONN_TRIGGER_SCAN, CONN_RECONNECTING, connState };
      sAllStates = values();
    }
    
    private ConnState(BluetoothQualityReport this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.mValue = param1Int2;
    }
    
    public static String getName(int param1Int) {
      for (ConnState connState : sAllStates) {
        if (connState.mValue == param1Int)
          return connState.toString(); 
      } 
      return "INVALID";
    }
  }
  
  class LinkQuality extends Enum<LinkQuality> {
    private static final LinkQuality[] $VALUES;
    
    public static final LinkQuality HIGH = new LinkQuality("HIGH", 1);
    
    public static final LinkQuality INVALID;
    
    public static final LinkQuality LOW;
    
    public static final LinkQuality MEDIUM;
    
    public static final LinkQuality STANDARD = new LinkQuality("STANDARD", 2);
    
    private LinkQuality(BluetoothQualityReport this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static LinkQuality valueOf(String param1String) {
      return Enum.<LinkQuality>valueOf(LinkQuality.class, param1String);
    }
    
    public static LinkQuality[] values() {
      return (LinkQuality[])$VALUES.clone();
    }
    
    public static final LinkQuality ULTRA_HIGH = new LinkQuality("ULTRA_HIGH", 0);
    
    private static LinkQuality[] sAllValues;
    
    static {
      MEDIUM = new LinkQuality("MEDIUM", 3);
      LOW = new LinkQuality("LOW", 4);
      LinkQuality linkQuality = new LinkQuality("INVALID", 5);
      $VALUES = new LinkQuality[] { ULTRA_HIGH, HIGH, STANDARD, MEDIUM, LOW, linkQuality };
      sAllValues = values();
    }
    
    static LinkQuality fromOrdinal(int param1Int) {
      LinkQuality[] arrayOfLinkQuality = sAllValues;
      if (param1Int < arrayOfLinkQuality.length - 1)
        return arrayOfLinkQuality[param1Int]; 
      return INVALID;
    }
  }
  
  class AirMode extends Enum<AirMode> {
    private static final AirMode[] $VALUES;
    
    public static final AirMode CVSD;
    
    public static final AirMode INVALID;
    
    public static final AirMode aLaw = new AirMode("aLaw", 1);
    
    private static AirMode[] sAllValues;
    
    public static final AirMode transparent_msbc;
    
    private AirMode(BluetoothQualityReport this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static AirMode valueOf(String param1String) {
      return Enum.<AirMode>valueOf(AirMode.class, param1String);
    }
    
    public static AirMode[] values() {
      return (AirMode[])$VALUES.clone();
    }
    
    public static final AirMode uLaw = new AirMode("uLaw", 0);
    
    static {
      CVSD = new AirMode("CVSD", 2);
      transparent_msbc = new AirMode("transparent_msbc", 3);
      AirMode airMode = new AirMode("INVALID", 4);
      $VALUES = new AirMode[] { uLaw, aLaw, CVSD, transparent_msbc, airMode };
      sAllValues = values();
    }
    
    static AirMode fromOrdinal(int param1Int) {
      AirMode[] arrayOfAirMode = sAllValues;
      if (param1Int < arrayOfAirMode.length - 1)
        return arrayOfAirMode[param1Int]; 
      return INVALID;
    }
  }
  
  public BluetoothQualityReport(String paramString1, int paramInt1, int paramInt2, int paramInt3, String paramString2, int paramInt4, byte[] paramArrayOfbyte) {
    if (!BluetoothAdapter.checkBluetoothAddress(paramString1)) {
      Log.d("BluetoothQualityReport", "remote addr is invalid");
      this.mAddr = "00:00:00:00:00:00";
    } else {
      this.mAddr = paramString1;
    } 
    this.mLmpVer = paramInt1;
    this.mLmpSubVer = paramInt2;
    this.mManufacturerId = paramInt3;
    if (paramString2 == null) {
      Log.d("BluetoothQualityReport", "remote name is null");
      this.mName = "";
    } else {
      this.mName = paramString2;
    } 
    this.mBluetoothClass = paramInt4;
    this.mBqrCommon = new BqrCommon(paramArrayOfbyte, 0);
    this.mBqrVsCommon = new BqrVsCommon(paramArrayOfbyte, 48);
    paramInt1 = this.mBqrCommon.getQualityReportId();
    if (paramInt1 == 1)
      return; 
    paramInt2 = this.mBqrVsCommon.getLength() + 48;
    if (paramInt1 == 2) {
      this.mBqrVsLsto = new BqrVsLsto(paramArrayOfbyte, paramInt2);
    } else if (paramInt1 == 3) {
      this.mBqrVsA2dpChoppy = new BqrVsA2dpChoppy(paramArrayOfbyte, paramInt2);
    } else if (paramInt1 == 4) {
      this.mBqrVsScoChoppy = new BqrVsScoChoppy(paramArrayOfbyte, paramInt2);
    } else {
      if (paramInt1 == 32) {
        this.mBqrVsConnectFail = new BqrVsConnectFail(paramArrayOfbyte, paramInt2);
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("BluetoothQualityReport: unkown quality report id:");
      stringBuilder.append(paramInt1);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
  }
  
  private BluetoothQualityReport(Parcel paramParcel) {
    this.mBqrCommon = new BqrCommon(paramParcel);
    this.mAddr = paramParcel.readString();
    this.mLmpVer = paramParcel.readInt();
    this.mLmpSubVer = paramParcel.readInt();
    this.mManufacturerId = paramParcel.readInt();
    this.mName = paramParcel.readString();
    this.mBluetoothClass = paramParcel.readInt();
    this.mBqrVsCommon = new BqrVsCommon(paramParcel);
    int i = this.mBqrCommon.getQualityReportId();
    if (i == 2) {
      this.mBqrVsLsto = new BqrVsLsto(paramParcel);
    } else if (i == 3) {
      this.mBqrVsA2dpChoppy = new BqrVsA2dpChoppy(paramParcel);
    } else if (i == 4) {
      this.mBqrVsScoChoppy = new BqrVsScoChoppy(paramParcel);
    } else if (i == 32) {
      this.mBqrVsConnectFail = new BqrVsConnectFail(paramParcel);
    } 
  }
  
  public int getQualityReportId() {
    return this.mBqrCommon.getQualityReportId();
  }
  
  public String getQualityReportIdStr() {
    int i = this.mBqrCommon.getQualityReportId();
    if (i != 1) {
      if (i != 2) {
        if (i != 3) {
          if (i != 4) {
            if (i != 32)
              return "INVALID"; 
            return "Connect fail";
          } 
          return "SCO choppy";
        } 
        return "A2DP choppy";
      } 
      return "Approaching LSTO";
    } 
    return "Quality monitor";
  }
  
  public String getAddress() {
    return this.mAddr;
  }
  
  public int getLmpVersion() {
    return this.mLmpVer;
  }
  
  public int getLmpSubVersion() {
    return this.mLmpSubVer;
  }
  
  public int getManufacturerId() {
    return this.mManufacturerId;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public int getBluetoothClass() {
    return this.mBluetoothClass;
  }
  
  public BqrCommon getBqrCommon() {
    return this.mBqrCommon;
  }
  
  public BqrVsCommon getBqrVsCommon() {
    return this.mBqrVsCommon;
  }
  
  public BqrVsLsto getBqrVsLsto() {
    return this.mBqrVsLsto;
  }
  
  public BqrVsA2dpChoppy getBqrVsA2dpChoppy() {
    return this.mBqrVsA2dpChoppy;
  }
  
  public BqrVsScoChoppy getBqrVsScoChoppy() {
    return this.mBqrVsScoChoppy;
  }
  
  public BqrVsConnectFail getBqrVsConnectFail() {
    return this.mBqrVsConnectFail;
  }
  
  public static final Parcelable.Creator<BluetoothQualityReport> CREATOR = (Parcelable.Creator<BluetoothQualityReport>)new Object();
  
  public static final int QUALITY_REPORT_ID_A2DP_CHOPPY = 3;
  
  public static final int QUALITY_REPORT_ID_APPROACH_LSTO = 2;
  
  public static final int QUALITY_REPORT_ID_CONN_FAIL = 32;
  
  public static final int QUALITY_REPORT_ID_MONITOR = 1;
  
  public static final int QUALITY_REPORT_ID_SCO_CHOPPY = 4;
  
  private static final String TAG = "BluetoothQualityReport";
  
  private String mAddr;
  
  private int mBluetoothClass;
  
  private BqrCommon mBqrCommon;
  
  private BqrVsA2dpChoppy mBqrVsA2dpChoppy;
  
  private BqrVsCommon mBqrVsCommon;
  
  private BqrVsConnectFail mBqrVsConnectFail;
  
  private BqrVsLsto mBqrVsLsto;
  
  private BqrVsScoChoppy mBqrVsScoChoppy;
  
  private int mLmpSubVer;
  
  private int mLmpVer;
  
  private int mManufacturerId;
  
  private String mName;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.mBqrCommon.writeToParcel(paramParcel, paramInt);
    paramParcel.writeString(this.mAddr);
    paramParcel.writeInt(this.mLmpVer);
    paramParcel.writeInt(this.mLmpSubVer);
    paramParcel.writeInt(this.mManufacturerId);
    paramParcel.writeString(this.mName);
    paramParcel.writeInt(this.mBluetoothClass);
    this.mBqrVsCommon.writeToParcel(paramParcel, paramInt);
    int i = this.mBqrCommon.getQualityReportId();
    if (i == 2) {
      this.mBqrVsLsto.writeToParcel(paramParcel, paramInt);
    } else if (i == 3) {
      this.mBqrVsA2dpChoppy.writeToParcel(paramParcel, paramInt);
    } else if (i == 4) {
      this.mBqrVsScoChoppy.writeToParcel(paramParcel, paramInt);
    } else if (i == 32) {
      this.mBqrVsConnectFail.writeToParcel(paramParcel, paramInt);
    } 
  }
  
  public String toString() {
    String str1;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("BQR: {\n  mAddr: ");
    stringBuilder.append(this.mAddr);
    stringBuilder.append(", mLmpVer: ");
    int i = this.mLmpVer;
    stringBuilder.append(String.format("0x%02X", new Object[] { Integer.valueOf(i) }));
    stringBuilder.append(", mLmpSubVer: ");
    i = this.mLmpSubVer;
    stringBuilder.append(String.format("0x%04X", new Object[] { Integer.valueOf(i) }));
    stringBuilder.append(", mManufacturerId: ");
    i = this.mManufacturerId;
    stringBuilder.append(String.format("0x%04X", new Object[] { Integer.valueOf(i) }));
    stringBuilder.append(", mName: ");
    stringBuilder.append(this.mName);
    stringBuilder.append(", mBluetoothClass: ");
    i = this.mBluetoothClass;
    stringBuilder.append(String.format("0x%X", new Object[] { Integer.valueOf(i) }));
    stringBuilder.append(",\n");
    stringBuilder.append(this.mBqrCommon);
    stringBuilder.append("\n");
    stringBuilder.append(this.mBqrVsCommon);
    stringBuilder.append("\n");
    String str2 = stringBuilder.toString();
    i = this.mBqrCommon.getQualityReportId();
    if (i == 2) {
      stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append(this.mBqrVsLsto);
      stringBuilder.append("\n}");
      str1 = stringBuilder.toString();
    } else if (i == 3) {
      stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append(this.mBqrVsA2dpChoppy);
      stringBuilder.append("\n}");
      str1 = stringBuilder.toString();
    } else if (i == 4) {
      stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append(this.mBqrVsScoChoppy);
      stringBuilder.append("\n}");
      str1 = stringBuilder.toString();
    } else if (i == 32) {
      stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append(this.mBqrVsConnectFail);
      stringBuilder.append("\n}");
      str1 = stringBuilder.toString();
    } else {
      str1 = str2;
      if (i == 1) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(str2);
        stringBuilder1.append("}");
        str1 = stringBuilder1.toString();
      } 
    } 
    return str1;
  }
  
  class BqrCommon implements Parcelable {
    static final int BQR_COMMON_LEN = 48;
    
    private static final String TAG = "BluetoothQualityReport.BqrCommon";
    
    private int mAfhSelectUnidealChannelCount;
    
    private int mConnectionHandle;
    
    private int mConnectionRole;
    
    private long mFlowOffCount;
    
    private long mLastFlowOnTimestamp;
    
    private long mLastTxAckTimestamp;
    
    private int mLsto;
    
    private long mNakCount;
    
    private long mNoRxCount;
    
    private long mOverflowCount;
    
    private int mPacketType;
    
    private long mPiconetClock;
    
    private int mQualityReportId;
    
    private long mRetransmissionCount;
    
    private int mRssi;
    
    private int mSnr;
    
    private int mTxPowerLevel;
    
    private long mUnderflowCount;
    
    private int mUnusedAfhChannelCount;
    
    final BluetoothQualityReport this$0;
    
    private BqrCommon(byte[] param1ArrayOfbyte, int param1Int) {
      if (param1ArrayOfbyte != null && param1ArrayOfbyte.length >= param1Int + 48) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(param1ArrayOfbyte, param1Int, param1ArrayOfbyte.length - param1Int);
        byteBuffer = byteBuffer.asReadOnlyBuffer();
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.mQualityReportId = byteBuffer.get() & 0xFF;
        this.mPacketType = byteBuffer.get() & 0xFF;
        this.mConnectionHandle = byteBuffer.getShort() & 0xFFFF;
        this.mConnectionRole = byteBuffer.get() & 0xFF;
        this.mTxPowerLevel = byteBuffer.get() & 0xFF;
        this.mRssi = byteBuffer.get();
        this.mSnr = byteBuffer.get();
        this.mUnusedAfhChannelCount = byteBuffer.get() & 0xFF;
        this.mAfhSelectUnidealChannelCount = byteBuffer.get() & 0xFF;
        this.mLsto = byteBuffer.getShort() & 0xFFFF;
        this.mPiconetClock = byteBuffer.getInt() & 0xFFFFFFFFL;
        this.mRetransmissionCount = byteBuffer.getInt() & 0xFFFFFFFFL;
        this.mNoRxCount = byteBuffer.getInt() & 0xFFFFFFFFL;
        this.mNakCount = byteBuffer.getInt() & 0xFFFFFFFFL;
        this.mLastTxAckTimestamp = byteBuffer.getInt() & 0xFFFFFFFFL;
        this.mFlowOffCount = byteBuffer.getInt() & 0xFFFFFFFFL;
        this.mLastFlowOnTimestamp = byteBuffer.getInt() & 0xFFFFFFFFL;
        this.mOverflowCount = byteBuffer.getInt() & 0xFFFFFFFFL;
        this.mUnderflowCount = byteBuffer.getInt() & 0xFFFFFFFFL;
        return;
      } 
      throw new IllegalArgumentException("BluetoothQualityReport.BqrCommon: BQR raw data length is abnormal.");
    }
    
    private BqrCommon(Parcel param1Parcel) {
      this.mQualityReportId = param1Parcel.readInt();
      this.mPacketType = param1Parcel.readInt();
      this.mConnectionHandle = param1Parcel.readInt();
      this.mConnectionRole = param1Parcel.readInt();
      this.mTxPowerLevel = param1Parcel.readInt();
      this.mRssi = param1Parcel.readInt();
      this.mSnr = param1Parcel.readInt();
      this.mUnusedAfhChannelCount = param1Parcel.readInt();
      this.mAfhSelectUnidealChannelCount = param1Parcel.readInt();
      this.mLsto = param1Parcel.readInt();
      this.mPiconetClock = param1Parcel.readLong();
      this.mRetransmissionCount = param1Parcel.readLong();
      this.mNoRxCount = param1Parcel.readLong();
      this.mNakCount = param1Parcel.readLong();
      this.mLastTxAckTimestamp = param1Parcel.readLong();
      this.mFlowOffCount = param1Parcel.readLong();
      this.mLastFlowOnTimestamp = param1Parcel.readLong();
      this.mOverflowCount = param1Parcel.readLong();
      this.mUnderflowCount = param1Parcel.readLong();
    }
    
    int getQualityReportId() {
      return this.mQualityReportId;
    }
    
    public int getPacketType() {
      return this.mPacketType;
    }
    
    public String getPacketTypeStr() {
      BluetoothQualityReport.PacketType packetType = BluetoothQualityReport.PacketType.fromOrdinal(this.mPacketType);
      return packetType.toString();
    }
    
    public int getConnectionHandle() {
      return this.mConnectionHandle;
    }
    
    public String getConnectionRole() {
      int i = this.mConnectionRole;
      if (i == 0)
        return "Master"; 
      if (i == 1)
        return "Slave"; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("INVALID:");
      stringBuilder.append(this.mConnectionRole);
      return stringBuilder.toString();
    }
    
    public int getTxPowerLevel() {
      return this.mTxPowerLevel;
    }
    
    public int getRssi() {
      return this.mRssi;
    }
    
    public int getSnr() {
      return this.mSnr;
    }
    
    public int getUnusedAfhChannelCount() {
      return this.mUnusedAfhChannelCount;
    }
    
    public int getAfhSelectUnidealChannelCount() {
      return this.mAfhSelectUnidealChannelCount;
    }
    
    public int getLsto() {
      return this.mLsto;
    }
    
    public long getPiconetClock() {
      return this.mPiconetClock;
    }
    
    public long getRetransmissionCount() {
      return this.mRetransmissionCount;
    }
    
    public long getNoRxCount() {
      return this.mNoRxCount;
    }
    
    public long getNakCount() {
      return this.mNakCount;
    }
    
    public long getLastTxAckTimestamp() {
      return this.mLastTxAckTimestamp;
    }
    
    public long getFlowOffCount() {
      return this.mFlowOffCount;
    }
    
    public long getLastFlowOnTimestamp() {
      return this.mLastFlowOnTimestamp;
    }
    
    public long getOverflowCount() {
      return this.mOverflowCount;
    }
    
    public long getUnderflowCount() {
      return this.mUnderflowCount;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mQualityReportId);
      param1Parcel.writeInt(this.mPacketType);
      param1Parcel.writeInt(this.mConnectionHandle);
      param1Parcel.writeInt(this.mConnectionRole);
      param1Parcel.writeInt(this.mTxPowerLevel);
      param1Parcel.writeInt(this.mRssi);
      param1Parcel.writeInt(this.mSnr);
      param1Parcel.writeInt(this.mUnusedAfhChannelCount);
      param1Parcel.writeInt(this.mAfhSelectUnidealChannelCount);
      param1Parcel.writeInt(this.mLsto);
      param1Parcel.writeLong(this.mPiconetClock);
      param1Parcel.writeLong(this.mRetransmissionCount);
      param1Parcel.writeLong(this.mNoRxCount);
      param1Parcel.writeLong(this.mNakCount);
      param1Parcel.writeLong(this.mLastTxAckTimestamp);
      param1Parcel.writeLong(this.mFlowOffCount);
      param1Parcel.writeLong(this.mLastFlowOnTimestamp);
      param1Parcel.writeLong(this.mOverflowCount);
      param1Parcel.writeLong(this.mUnderflowCount);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("  BqrCommon: {\n    mQualityReportId: ");
      BluetoothQualityReport bluetoothQualityReport = BluetoothQualityReport.this;
      stringBuilder.append(bluetoothQualityReport.getQualityReportIdStr());
      stringBuilder.append("(");
      int i = this.mQualityReportId;
      stringBuilder.append(String.format("0x%02X", new Object[] { Integer.valueOf(i) }));
      stringBuilder.append("), mPacketType: ");
      stringBuilder.append(getPacketTypeStr());
      stringBuilder.append("(");
      i = this.mPacketType;
      stringBuilder.append(String.format("0x%02X", new Object[] { Integer.valueOf(i) }));
      stringBuilder.append("), mConnectionHandle: ");
      i = this.mConnectionHandle;
      stringBuilder.append(String.format("0x%04X", new Object[] { Integer.valueOf(i) }));
      stringBuilder.append(", mConnectionRole: ");
      stringBuilder.append(getConnectionRole());
      stringBuilder.append("(");
      stringBuilder.append(this.mConnectionRole);
      stringBuilder.append("), mTxPowerLevel: ");
      stringBuilder.append(this.mTxPowerLevel);
      stringBuilder.append(", mRssi: ");
      stringBuilder.append(this.mRssi);
      stringBuilder.append(", mSnr: ");
      stringBuilder.append(this.mSnr);
      stringBuilder.append(", mUnusedAfhChannelCount: ");
      stringBuilder.append(this.mUnusedAfhChannelCount);
      stringBuilder.append(",\n    mAfhSelectUnidealChannelCount: ");
      stringBuilder.append(this.mAfhSelectUnidealChannelCount);
      stringBuilder.append(", mLsto: ");
      stringBuilder.append(this.mLsto);
      stringBuilder.append(", mPiconetClock: ");
      long l = this.mPiconetClock;
      stringBuilder.append(String.format("0x%08X", new Object[] { Long.valueOf(l) }));
      stringBuilder.append(", mRetransmissionCount: ");
      stringBuilder.append(this.mRetransmissionCount);
      stringBuilder.append(", mNoRxCount: ");
      stringBuilder.append(this.mNoRxCount);
      stringBuilder.append(", mNakCount: ");
      stringBuilder.append(this.mNakCount);
      stringBuilder.append(", mLastTxAckTimestamp: ");
      l = this.mLastTxAckTimestamp;
      stringBuilder.append(String.format("0x%08X", new Object[] { Long.valueOf(l) }));
      stringBuilder.append(", mFlowOffCount: ");
      stringBuilder.append(this.mFlowOffCount);
      stringBuilder.append(",\n    mLastFlowOnTimestamp: ");
      l = this.mLastFlowOnTimestamp;
      stringBuilder.append(String.format("0x%08X", new Object[] { Long.valueOf(l) }));
      stringBuilder.append(", mOverflowCount: ");
      stringBuilder.append(this.mOverflowCount);
      stringBuilder.append(", mUnderflowCount: ");
      stringBuilder.append(this.mUnderflowCount);
      stringBuilder.append("\n  }");
      return stringBuilder.toString();
    }
  }
  
  class BqrVsCommon implements Parcelable {
    private static final int BQR_VS_COMMON_LEN = 7;
    
    private static final String TAG = "BluetoothQualityReport.BqrVsCommon";
    
    private String mAddr;
    
    private int mCalFailedItemCount;
    
    final BluetoothQualityReport this$0;
    
    private BqrVsCommon(byte[] param1ArrayOfbyte, int param1Int) {
      if (param1ArrayOfbyte != null && param1ArrayOfbyte.length >= param1Int + 7) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(param1ArrayOfbyte, param1Int, param1ArrayOfbyte.length - param1Int);
        byteBuffer = byteBuffer.asReadOnlyBuffer();
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        byte b1 = byteBuffer.get(param1Int + 5);
        byte b2 = byteBuffer.get(param1Int + 4), b3 = byteBuffer.get(param1Int + 3), b4 = byteBuffer.get(param1Int + 2);
        byte b5 = byteBuffer.get(param1Int + 1), b6 = byteBuffer.get(param1Int + 0);
        this.mAddr = String.format("%02X:%02X:%02X:%02X:%02X:%02X", new Object[] { Byte.valueOf(b1), Byte.valueOf(b2), Byte.valueOf(b3), Byte.valueOf(b4), Byte.valueOf(b5), Byte.valueOf(b6) });
        byteBuffer.position(param1Int + 6);
        this.mCalFailedItemCount = byteBuffer.get() & 0xFF;
        return;
      } 
      throw new IllegalArgumentException("BluetoothQualityReport.BqrVsCommon: BQR raw data length is abnormal.");
    }
    
    private BqrVsCommon(Parcel param1Parcel) {
      this.mAddr = param1Parcel.readString();
      this.mCalFailedItemCount = param1Parcel.readInt();
    }
    
    public int getCalFailedItemCount() {
      return this.mCalFailedItemCount;
    }
    
    int getLength() {
      return 7;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeString(this.mAddr);
      param1Parcel.writeInt(this.mCalFailedItemCount);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("  BqrVsCommon: {\n    mAddr: ");
      stringBuilder.append(this.mAddr);
      stringBuilder.append(", mCalFailedItemCount: ");
      stringBuilder.append(this.mCalFailedItemCount);
      stringBuilder.append("\n  }");
      return stringBuilder.toString();
    }
  }
  
  public class BqrVsLsto implements Parcelable {
    private static final String TAG = "BluetoothQualityReport.BqrVsLsto";
    
    private long mBasebandStats;
    
    private int mConnState;
    
    private int mCxmDenials;
    
    private long mLastTxAckTimestamp;
    
    private long mNativeClock;
    
    private int mRfLoss;
    
    private long mSlotsUsed;
    
    private int mTxSkipped;
    
    final BluetoothQualityReport this$0;
    
    private BqrVsLsto(byte[] param1ArrayOfbyte, int param1Int) {
      if (param1ArrayOfbyte != null && param1ArrayOfbyte.length > param1Int) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(param1ArrayOfbyte, param1Int, param1ArrayOfbyte.length - param1Int);
        byteBuffer = byteBuffer.asReadOnlyBuffer();
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.mConnState = byteBuffer.get() & 0xFF;
        this.mBasebandStats = byteBuffer.getInt() & 0xFFFFFFFFL;
        this.mSlotsUsed = byteBuffer.getInt() & 0xFFFFFFFFL;
        this.mCxmDenials = byteBuffer.getShort() & 0xFFFF;
        this.mTxSkipped = byteBuffer.getShort() & 0xFFFF;
        this.mRfLoss = byteBuffer.getShort() & 0xFFFF;
        this.mNativeClock = byteBuffer.getInt() & 0xFFFFFFFFL;
        this.mLastTxAckTimestamp = byteBuffer.getInt() & 0xFFFFFFFFL;
        return;
      } 
      throw new IllegalArgumentException("BluetoothQualityReport.BqrVsLsto: BQR raw data length is abnormal.");
    }
    
    private BqrVsLsto(Parcel param1Parcel) {
      this.mConnState = param1Parcel.readInt();
      this.mBasebandStats = param1Parcel.readLong();
      this.mSlotsUsed = param1Parcel.readLong();
      this.mCxmDenials = param1Parcel.readInt();
      this.mTxSkipped = param1Parcel.readInt();
      this.mRfLoss = param1Parcel.readInt();
      this.mNativeClock = param1Parcel.readLong();
      this.mLastTxAckTimestamp = param1Parcel.readLong();
    }
    
    public int getConnState() {
      return this.mConnState;
    }
    
    public String getConnStateStr() {
      return BluetoothQualityReport.ConnState.getName(this.mConnState);
    }
    
    public long getBasebandStats() {
      return this.mBasebandStats;
    }
    
    public long getSlotsUsed() {
      return this.mSlotsUsed;
    }
    
    public int getCxmDenials() {
      return this.mCxmDenials;
    }
    
    public int getTxSkipped() {
      return this.mTxSkipped;
    }
    
    public int getRfLoss() {
      return this.mRfLoss;
    }
    
    public long getNativeClock() {
      return this.mNativeClock;
    }
    
    public long getLastTxAckTimestamp() {
      return this.mLastTxAckTimestamp;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mConnState);
      param1Parcel.writeLong(this.mBasebandStats);
      param1Parcel.writeLong(this.mSlotsUsed);
      param1Parcel.writeInt(this.mCxmDenials);
      param1Parcel.writeInt(this.mTxSkipped);
      param1Parcel.writeInt(this.mRfLoss);
      param1Parcel.writeLong(this.mNativeClock);
      param1Parcel.writeLong(this.mLastTxAckTimestamp);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("  BqrVsLsto: {\n    mConnState: ");
      stringBuilder.append(getConnStateStr());
      stringBuilder.append("(");
      int i = this.mConnState;
      stringBuilder.append(String.format("0x%02X", new Object[] { Integer.valueOf(i) }));
      stringBuilder.append("), mBasebandStats: ");
      long l = this.mBasebandStats;
      stringBuilder.append(String.format("0x%08X", new Object[] { Long.valueOf(l) }));
      stringBuilder.append(", mSlotsUsed: ");
      stringBuilder.append(this.mSlotsUsed);
      stringBuilder.append(", mCxmDenials: ");
      stringBuilder.append(this.mCxmDenials);
      stringBuilder.append(", mTxSkipped: ");
      stringBuilder.append(this.mTxSkipped);
      stringBuilder.append(", mRfLoss: ");
      stringBuilder.append(this.mRfLoss);
      stringBuilder.append(", mNativeClock: ");
      l = this.mNativeClock;
      stringBuilder.append(String.format("0x%08X", new Object[] { Long.valueOf(l) }));
      stringBuilder.append(", mLastTxAckTimestamp: ");
      l = this.mLastTxAckTimestamp;
      stringBuilder.append(String.format("0x%08X", new Object[] { Long.valueOf(l) }));
      stringBuilder.append("\n  }");
      return stringBuilder.toString();
    }
  }
  
  class BqrVsA2dpChoppy implements Parcelable {
    private static final String TAG = "BluetoothQualityReport.BqrVsA2dpChoppy";
    
    private int mAclTxQueueLength;
    
    private long mArrivalTime;
    
    private int mGlitchCount;
    
    private int mLinkQuality;
    
    private int mRxCxmDenials;
    
    private long mScheduleTime;
    
    private int mTxCxmDenials;
    
    final BluetoothQualityReport this$0;
    
    private BqrVsA2dpChoppy(byte[] param1ArrayOfbyte, int param1Int) {
      if (param1ArrayOfbyte != null && param1ArrayOfbyte.length > param1Int) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(param1ArrayOfbyte, param1Int, param1ArrayOfbyte.length - param1Int);
        byteBuffer = byteBuffer.asReadOnlyBuffer();
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.mArrivalTime = byteBuffer.getInt() & 0xFFFFFFFFL;
        this.mScheduleTime = byteBuffer.getInt() & 0xFFFFFFFFL;
        this.mGlitchCount = byteBuffer.getShort() & 0xFFFF;
        this.mTxCxmDenials = byteBuffer.getShort() & 0xFFFF;
        this.mRxCxmDenials = byteBuffer.getShort() & 0xFFFF;
        this.mAclTxQueueLength = byteBuffer.get() & 0xFF;
        this.mLinkQuality = byteBuffer.get() & 0xFF;
        return;
      } 
      throw new IllegalArgumentException("BluetoothQualityReport.BqrVsA2dpChoppy: BQR raw data length is abnormal.");
    }
    
    private BqrVsA2dpChoppy(Parcel param1Parcel) {
      this.mArrivalTime = param1Parcel.readLong();
      this.mScheduleTime = param1Parcel.readLong();
      this.mGlitchCount = param1Parcel.readInt();
      this.mTxCxmDenials = param1Parcel.readInt();
      this.mRxCxmDenials = param1Parcel.readInt();
      this.mAclTxQueueLength = param1Parcel.readInt();
      this.mLinkQuality = param1Parcel.readInt();
    }
    
    public long getArrivalTime() {
      return this.mArrivalTime;
    }
    
    public long getScheduleTime() {
      return this.mScheduleTime;
    }
    
    public int getGlitchCount() {
      return this.mGlitchCount;
    }
    
    public int getTxCxmDenials() {
      return this.mTxCxmDenials;
    }
    
    public int getRxCxmDenials() {
      return this.mRxCxmDenials;
    }
    
    public int getAclTxQueueLength() {
      return this.mAclTxQueueLength;
    }
    
    public int getLinkQuality() {
      return this.mLinkQuality;
    }
    
    public String getLinkQualityStr() {
      BluetoothQualityReport.LinkQuality linkQuality = BluetoothQualityReport.LinkQuality.fromOrdinal(this.mLinkQuality);
      return linkQuality.toString();
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeLong(this.mArrivalTime);
      param1Parcel.writeLong(this.mScheduleTime);
      param1Parcel.writeInt(this.mGlitchCount);
      param1Parcel.writeInt(this.mTxCxmDenials);
      param1Parcel.writeInt(this.mRxCxmDenials);
      param1Parcel.writeInt(this.mAclTxQueueLength);
      param1Parcel.writeInt(this.mLinkQuality);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("  BqrVsA2dpChoppy: {\n    mArrivalTime: ");
      long l = this.mArrivalTime;
      stringBuilder.append(String.format("0x%08X", new Object[] { Long.valueOf(l) }));
      stringBuilder.append(", mScheduleTime: ");
      l = this.mScheduleTime;
      stringBuilder.append(String.format("0x%08X", new Object[] { Long.valueOf(l) }));
      stringBuilder.append(", mGlitchCount: ");
      stringBuilder.append(this.mGlitchCount);
      stringBuilder.append(", mTxCxmDenials: ");
      stringBuilder.append(this.mTxCxmDenials);
      stringBuilder.append(", mRxCxmDenials: ");
      stringBuilder.append(this.mRxCxmDenials);
      stringBuilder.append(", mAclTxQueueLength: ");
      stringBuilder.append(this.mAclTxQueueLength);
      stringBuilder.append(", mLinkQuality: ");
      stringBuilder.append(getLinkQualityStr());
      stringBuilder.append("(");
      int i = this.mLinkQuality;
      stringBuilder.append(String.format("0x%02X", new Object[] { Integer.valueOf(i) }));
      stringBuilder.append(")\n  }");
      return stringBuilder.toString();
    }
  }
  
  public class BqrVsScoChoppy implements Parcelable {
    private static final String TAG = "BluetoothQualityReport.BqrVsScoChoppy";
    
    private int mAirFormat;
    
    private int mGlitchCount;
    
    private int mGoodRxFrameCount;
    
    private int mInstanceCount;
    
    private int mIntervalEsco;
    
    private int mLateDispatch;
    
    private int mLpaIntrMiss;
    
    private int mMicIntrMiss;
    
    private int mMissedInstanceCount;
    
    private int mPlcDiscardCount;
    
    private int mPlcFillCount;
    
    private int mRxCxmDenials;
    
    private int mRxRetransmitSlotCount;
    
    private int mSprIntrMiss;
    
    private int mTxAbortCount;
    
    private int mTxCxmDenials;
    
    private int mTxRetransmitSlotCount;
    
    private int mWindowEsco;
    
    final BluetoothQualityReport this$0;
    
    private BqrVsScoChoppy(byte[] param1ArrayOfbyte, int param1Int) {
      if (param1ArrayOfbyte != null && param1ArrayOfbyte.length > param1Int) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(param1ArrayOfbyte, param1Int, param1ArrayOfbyte.length - param1Int);
        byteBuffer = byteBuffer.asReadOnlyBuffer();
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.mGlitchCount = byteBuffer.getShort() & 0xFFFF;
        this.mIntervalEsco = byteBuffer.get() & 0xFF;
        this.mWindowEsco = byteBuffer.get() & 0xFF;
        this.mAirFormat = byteBuffer.get() & 0xFF;
        this.mInstanceCount = byteBuffer.getShort() & 0xFFFF;
        this.mTxCxmDenials = byteBuffer.getShort() & 0xFFFF;
        this.mRxCxmDenials = byteBuffer.getShort() & 0xFFFF;
        this.mTxAbortCount = byteBuffer.getShort() & 0xFFFF;
        this.mLateDispatch = byteBuffer.getShort() & 0xFFFF;
        this.mMicIntrMiss = byteBuffer.getShort() & 0xFFFF;
        this.mLpaIntrMiss = byteBuffer.getShort() & 0xFFFF;
        this.mSprIntrMiss = byteBuffer.getShort() & 0xFFFF;
        this.mPlcFillCount = byteBuffer.getShort() & 0xFFFF;
        this.mPlcDiscardCount = byteBuffer.getShort() & 0xFFFF;
        try {
          this.mMissedInstanceCount = byteBuffer.getShort() & 0xFFFF;
          this.mTxRetransmitSlotCount = byteBuffer.getShort() & 0xFFFF;
          this.mRxRetransmitSlotCount = byteBuffer.getShort() & 0xFFFF;
          this.mGoodRxFrameCount = byteBuffer.getShort() & 0xFFFF;
        } catch (BufferUnderflowException bufferUnderflowException) {
          Log.v("BluetoothQualityReport.BqrVsScoChoppy", "some fields are not contained");
        } 
        return;
      } 
      throw new IllegalArgumentException("BluetoothQualityReport.BqrVsScoChoppy: BQR raw data length is abnormal.");
    }
    
    private BqrVsScoChoppy(Parcel param1Parcel) {
      this.mGlitchCount = param1Parcel.readInt();
      this.mIntervalEsco = param1Parcel.readInt();
      this.mWindowEsco = param1Parcel.readInt();
      this.mAirFormat = param1Parcel.readInt();
      this.mInstanceCount = param1Parcel.readInt();
      this.mTxCxmDenials = param1Parcel.readInt();
      this.mRxCxmDenials = param1Parcel.readInt();
      this.mTxAbortCount = param1Parcel.readInt();
      this.mLateDispatch = param1Parcel.readInt();
      this.mMicIntrMiss = param1Parcel.readInt();
      this.mLpaIntrMiss = param1Parcel.readInt();
      this.mSprIntrMiss = param1Parcel.readInt();
      this.mPlcFillCount = param1Parcel.readInt();
      this.mPlcDiscardCount = param1Parcel.readInt();
      this.mMissedInstanceCount = param1Parcel.readInt();
      this.mTxRetransmitSlotCount = param1Parcel.readInt();
      this.mRxRetransmitSlotCount = param1Parcel.readInt();
      this.mGoodRxFrameCount = param1Parcel.readInt();
    }
    
    public int getGlitchCount() {
      return this.mGlitchCount;
    }
    
    public int getIntervalEsco() {
      return this.mIntervalEsco;
    }
    
    public int getWindowEsco() {
      return this.mWindowEsco;
    }
    
    public int getAirFormat() {
      return this.mAirFormat;
    }
    
    public String getAirFormatStr() {
      BluetoothQualityReport.AirMode airMode = BluetoothQualityReport.AirMode.fromOrdinal(this.mAirFormat);
      return airMode.toString();
    }
    
    public int getInstanceCount() {
      return this.mInstanceCount;
    }
    
    public int getTxCxmDenials() {
      return this.mTxCxmDenials;
    }
    
    public int getRxCxmDenials() {
      return this.mRxCxmDenials;
    }
    
    public int getTxAbortCount() {
      return this.mTxAbortCount;
    }
    
    public int getLateDispatch() {
      return this.mLateDispatch;
    }
    
    public int getMicIntrMiss() {
      return this.mMicIntrMiss;
    }
    
    public int getLpaIntrMiss() {
      return this.mLpaIntrMiss;
    }
    
    public int getSprIntrMiss() {
      return this.mSprIntrMiss;
    }
    
    public int getPlcFillCount() {
      return this.mPlcFillCount;
    }
    
    public int getPlcDiscardCount() {
      return this.mPlcDiscardCount;
    }
    
    public int getMissedInstanceCount() {
      return this.mMissedInstanceCount;
    }
    
    public int getTxRetransmitSlotCount() {
      return this.mTxRetransmitSlotCount;
    }
    
    public int getRxRetransmitSlotCount() {
      return this.mRxRetransmitSlotCount;
    }
    
    public int getGoodRxFrameCount() {
      return this.mGoodRxFrameCount;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mGlitchCount);
      param1Parcel.writeInt(this.mIntervalEsco);
      param1Parcel.writeInt(this.mWindowEsco);
      param1Parcel.writeInt(this.mAirFormat);
      param1Parcel.writeInt(this.mInstanceCount);
      param1Parcel.writeInt(this.mTxCxmDenials);
      param1Parcel.writeInt(this.mRxCxmDenials);
      param1Parcel.writeInt(this.mTxAbortCount);
      param1Parcel.writeInt(this.mLateDispatch);
      param1Parcel.writeInt(this.mMicIntrMiss);
      param1Parcel.writeInt(this.mLpaIntrMiss);
      param1Parcel.writeInt(this.mSprIntrMiss);
      param1Parcel.writeInt(this.mPlcFillCount);
      param1Parcel.writeInt(this.mPlcDiscardCount);
      param1Parcel.writeInt(this.mMissedInstanceCount);
      param1Parcel.writeInt(this.mTxRetransmitSlotCount);
      param1Parcel.writeInt(this.mRxRetransmitSlotCount);
      param1Parcel.writeInt(this.mGoodRxFrameCount);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("  BqrVsScoChoppy: {\n    mGlitchCount: ");
      stringBuilder.append(this.mGlitchCount);
      stringBuilder.append(", mIntervalEsco: ");
      stringBuilder.append(this.mIntervalEsco);
      stringBuilder.append(", mWindowEsco: ");
      stringBuilder.append(this.mWindowEsco);
      stringBuilder.append(", mAirFormat: ");
      stringBuilder.append(getAirFormatStr());
      stringBuilder.append("(");
      int i = this.mAirFormat;
      stringBuilder.append(String.format("0x%02X", new Object[] { Integer.valueOf(i) }));
      stringBuilder.append("), mInstanceCount: ");
      stringBuilder.append(this.mInstanceCount);
      stringBuilder.append(", mTxCxmDenials: ");
      stringBuilder.append(this.mTxCxmDenials);
      stringBuilder.append(", mRxCxmDenials: ");
      stringBuilder.append(this.mRxCxmDenials);
      stringBuilder.append(", mTxAbortCount: ");
      stringBuilder.append(this.mTxAbortCount);
      stringBuilder.append(",\n    mLateDispatch: ");
      stringBuilder.append(this.mLateDispatch);
      stringBuilder.append(", mMicIntrMiss: ");
      stringBuilder.append(this.mMicIntrMiss);
      stringBuilder.append(", mLpaIntrMiss: ");
      stringBuilder.append(this.mLpaIntrMiss);
      stringBuilder.append(", mSprIntrMiss: ");
      stringBuilder.append(this.mSprIntrMiss);
      stringBuilder.append(", mPlcFillCount: ");
      stringBuilder.append(this.mPlcFillCount);
      stringBuilder.append(", mPlcDiscardCount: ");
      stringBuilder.append(this.mPlcDiscardCount);
      stringBuilder.append(", mMissedInstanceCount: ");
      stringBuilder.append(this.mMissedInstanceCount);
      stringBuilder.append(", mTxRetransmitSlotCount: ");
      stringBuilder.append(this.mTxRetransmitSlotCount);
      stringBuilder.append(",\n    mRxRetransmitSlotCount: ");
      stringBuilder.append(this.mRxRetransmitSlotCount);
      stringBuilder.append(", mGoodRxFrameCount: ");
      stringBuilder.append(this.mGoodRxFrameCount);
      stringBuilder.append("\n  }");
      return stringBuilder.toString();
    }
  }
  
  public class BqrVsConnectFail implements Parcelable {
    private static final String TAG = "BluetoothQualityReport.BqrVsConnectFail";
    
    private int mFailReason;
    
    final BluetoothQualityReport this$0;
    
    private BqrVsConnectFail(byte[] param1ArrayOfbyte, int param1Int) {
      if (param1ArrayOfbyte != null && param1ArrayOfbyte.length > param1Int) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(param1ArrayOfbyte, param1Int, param1ArrayOfbyte.length - param1Int);
        byteBuffer = byteBuffer.asReadOnlyBuffer();
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.mFailReason = byteBuffer.get() & 0xFF;
        return;
      } 
      throw new IllegalArgumentException("BluetoothQualityReport.BqrVsConnectFail: BQR raw data length is abnormal.");
    }
    
    private BqrVsConnectFail(Parcel param1Parcel) {
      this.mFailReason = param1Parcel.readInt();
    }
    
    public int getFailReason() {
      return this.mFailReason;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mFailReason);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("  BqrVsConnectFail: {\n    mFailReason: ");
      int i = this.mFailReason;
      stringBuilder.append(String.format("0x%02X", new Object[] { Integer.valueOf(i) }));
      stringBuilder.append("\n  }");
      return stringBuilder.toString();
    }
  }
}
