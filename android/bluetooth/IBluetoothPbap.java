package android.bluetooth;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IBluetoothPbap extends IInterface {
  void disconnect(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  List<BluetoothDevice> getConnectedDevices() throws RemoteException;
  
  int getConnectionState(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] paramArrayOfint) throws RemoteException;
  
  boolean setConnectionPolicy(BluetoothDevice paramBluetoothDevice, int paramInt) throws RemoteException;
  
  class Default implements IBluetoothPbap {
    public List<BluetoothDevice> getConnectedDevices() throws RemoteException {
      return null;
    }
    
    public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public int getConnectionState(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return 0;
    }
    
    public void disconnect(BluetoothDevice param1BluetoothDevice) throws RemoteException {}
    
    public boolean setConnectionPolicy(BluetoothDevice param1BluetoothDevice, int param1Int) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IBluetoothPbap {
    private static final String DESCRIPTOR = "android.bluetooth.IBluetoothPbap";
    
    static final int TRANSACTION_disconnect = 4;
    
    static final int TRANSACTION_getConnectedDevices = 1;
    
    static final int TRANSACTION_getConnectionState = 3;
    
    static final int TRANSACTION_getDevicesMatchingConnectionStates = 2;
    
    static final int TRANSACTION_setConnectionPolicy = 5;
    
    public Stub() {
      attachInterface(this, "android.bluetooth.IBluetoothPbap");
    }
    
    public static IBluetoothPbap asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.bluetooth.IBluetoothPbap");
      if (iInterface != null && iInterface instanceof IBluetoothPbap)
        return (IBluetoothPbap)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "setConnectionPolicy";
            } 
            return "disconnect";
          } 
          return "getConnectionState";
        } 
        return "getDevicesMatchingConnectionStates";
      } 
      return "getConnectedDevices";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              BluetoothDevice bluetoothDevice;
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.bluetooth.IBluetoothPbap");
                return true;
              } 
              param1Parcel1.enforceInterface("android.bluetooth.IBluetoothPbap");
              if (param1Parcel1.readInt() != 0) {
                bluetoothDevice = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(param1Parcel1);
              } else {
                bluetoothDevice = null;
              } 
              param1Int1 = param1Parcel1.readInt();
              boolean bool = setConnectionPolicy(bluetoothDevice, param1Int1);
              param1Parcel2.writeNoException();
              param1Parcel2.writeInt(bool);
              return true;
            } 
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothPbap");
            if (param1Parcel1.readInt() != 0) {
              BluetoothDevice bluetoothDevice = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            disconnect((BluetoothDevice)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          } 
          param1Parcel1.enforceInterface("android.bluetooth.IBluetoothPbap");
          if (param1Parcel1.readInt() != 0) {
            BluetoothDevice bluetoothDevice = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          param1Int1 = getConnectionState((BluetoothDevice)param1Parcel1);
          param1Parcel2.writeNoException();
          param1Parcel2.writeInt(param1Int1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.bluetooth.IBluetoothPbap");
        int[] arrayOfInt = param1Parcel1.createIntArray();
        list = getDevicesMatchingConnectionStates(arrayOfInt);
        param1Parcel2.writeNoException();
        param1Parcel2.writeTypedList(list);
        return true;
      } 
      list.enforceInterface("android.bluetooth.IBluetoothPbap");
      List<BluetoothDevice> list = getConnectedDevices();
      param1Parcel2.writeNoException();
      param1Parcel2.writeTypedList(list);
      return true;
    }
    
    private static class Proxy implements IBluetoothPbap {
      public static IBluetoothPbap sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.bluetooth.IBluetoothPbap";
      }
      
      public List<BluetoothDevice> getConnectedDevices() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothPbap");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IBluetoothPbap.Stub.getDefaultImpl() != null)
            return IBluetoothPbap.Stub.getDefaultImpl().getConnectedDevices(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(BluetoothDevice.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothPbap");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IBluetoothPbap.Stub.getDefaultImpl() != null)
            return IBluetoothPbap.Stub.getDefaultImpl().getDevicesMatchingConnectionStates(param2ArrayOfint); 
          parcel2.readException();
          return parcel2.createTypedArrayList(BluetoothDevice.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getConnectionState(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothPbap");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IBluetoothPbap.Stub.getDefaultImpl() != null)
            return IBluetoothPbap.Stub.getDefaultImpl().getConnectionState(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disconnect(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothPbap");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IBluetoothPbap.Stub.getDefaultImpl() != null) {
            IBluetoothPbap.Stub.getDefaultImpl().disconnect(param2BluetoothDevice);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setConnectionPolicy(BluetoothDevice param2BluetoothDevice, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothPbap");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothPbap.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothPbap.Stub.getDefaultImpl().setConnectionPolicy(param2BluetoothDevice, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IBluetoothPbap param1IBluetoothPbap) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IBluetoothPbap != null) {
          Proxy.sDefaultImpl = param1IBluetoothPbap;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IBluetoothPbap getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
