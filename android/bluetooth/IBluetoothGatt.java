package android.bluetooth;

import android.app.PendingIntent;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertisingSetParameters;
import android.bluetooth.le.IAdvertisingSetCallback;
import android.bluetooth.le.IPeriodicAdvertisingCallback;
import android.bluetooth.le.IScannerCallback;
import android.bluetooth.le.PeriodicAdvertisingParameters;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelUuid;
import android.os.RemoteException;
import android.os.WorkSource;
import java.util.ArrayList;
import java.util.List;

public interface IBluetoothGatt extends IInterface {
  void addService(int paramInt, BluetoothGattService paramBluetoothGattService) throws RemoteException;
  
  void beginReliableWrite(int paramInt, String paramString) throws RemoteException;
  
  void clearServices(int paramInt) throws RemoteException;
  
  void clientConnect(int paramInt1, String paramString, boolean paramBoolean1, int paramInt2, boolean paramBoolean2, int paramInt3) throws RemoteException;
  
  void clientDisconnect(int paramInt, String paramString) throws RemoteException;
  
  void clientReadPhy(int paramInt, String paramString) throws RemoteException;
  
  void clientSetPreferredPhy(int paramInt1, String paramString, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  void configureMTU(int paramInt1, String paramString, int paramInt2) throws RemoteException;
  
  void connectionParameterUpdate(int paramInt1, String paramString, int paramInt2) throws RemoteException;
  
  void disconnectAll() throws RemoteException;
  
  void discoverServiceByUuid(int paramInt, String paramString, ParcelUuid paramParcelUuid) throws RemoteException;
  
  void discoverServices(int paramInt, String paramString) throws RemoteException;
  
  void enableAdvertisingSet(int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3) throws RemoteException;
  
  void endReliableWrite(int paramInt, String paramString, boolean paramBoolean) throws RemoteException;
  
  void flushPendingBatchResults(int paramInt) throws RemoteException;
  
  List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] paramArrayOfint) throws RemoteException;
  
  void getOwnAddress(int paramInt) throws RemoteException;
  
  void leConnectionUpdate(int paramInt1, String paramString, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7) throws RemoteException;
  
  int numHwTrackFiltersAvailable() throws RemoteException;
  
  void readCharacteristic(int paramInt1, String paramString, int paramInt2, int paramInt3) throws RemoteException;
  
  void readDescriptor(int paramInt1, String paramString, int paramInt2, int paramInt3) throws RemoteException;
  
  void readRemoteRssi(int paramInt, String paramString) throws RemoteException;
  
  void readUsingCharacteristicUuid(int paramInt1, String paramString, ParcelUuid paramParcelUuid, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  void refreshDevice(int paramInt, String paramString) throws RemoteException;
  
  void registerClient(ParcelUuid paramParcelUuid, IBluetoothGattCallback paramIBluetoothGattCallback) throws RemoteException;
  
  void registerForNotification(int paramInt1, String paramString, int paramInt2, boolean paramBoolean) throws RemoteException;
  
  void registerScanner(IScannerCallback paramIScannerCallback, WorkSource paramWorkSource) throws RemoteException;
  
  void registerServer(ParcelUuid paramParcelUuid, IBluetoothGattServerCallback paramIBluetoothGattServerCallback) throws RemoteException;
  
  void registerSync(ScanResult paramScanResult, int paramInt1, int paramInt2, IPeriodicAdvertisingCallback paramIPeriodicAdvertisingCallback) throws RemoteException;
  
  void removeService(int paramInt1, int paramInt2) throws RemoteException;
  
  void sendNotification(int paramInt1, String paramString, int paramInt2, boolean paramBoolean, byte[] paramArrayOfbyte) throws RemoteException;
  
  void sendResponse(int paramInt1, String paramString, int paramInt2, int paramInt3, int paramInt4, byte[] paramArrayOfbyte) throws RemoteException;
  
  void serverConnect(int paramInt1, String paramString, boolean paramBoolean, int paramInt2) throws RemoteException;
  
  void serverDisconnect(int paramInt, String paramString) throws RemoteException;
  
  void serverReadPhy(int paramInt, String paramString) throws RemoteException;
  
  void serverSetPreferredPhy(int paramInt1, String paramString, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  void setAdvertisingData(int paramInt, AdvertiseData paramAdvertiseData) throws RemoteException;
  
  void setAdvertisingParameters(int paramInt, AdvertisingSetParameters paramAdvertisingSetParameters) throws RemoteException;
  
  void setPeriodicAdvertisingData(int paramInt, AdvertiseData paramAdvertiseData) throws RemoteException;
  
  void setPeriodicAdvertisingEnable(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setPeriodicAdvertisingParameters(int paramInt, PeriodicAdvertisingParameters paramPeriodicAdvertisingParameters) throws RemoteException;
  
  void setScanResponseData(int paramInt, AdvertiseData paramAdvertiseData) throws RemoteException;
  
  void startAdvertisingSet(AdvertisingSetParameters paramAdvertisingSetParameters, AdvertiseData paramAdvertiseData1, AdvertiseData paramAdvertiseData2, PeriodicAdvertisingParameters paramPeriodicAdvertisingParameters, AdvertiseData paramAdvertiseData3, int paramInt1, int paramInt2, IAdvertisingSetCallback paramIAdvertisingSetCallback) throws RemoteException;
  
  void startScan(int paramInt, ScanSettings paramScanSettings, List<ScanFilter> paramList, List paramList1, String paramString1, String paramString2) throws RemoteException;
  
  void startScanForIntent(PendingIntent paramPendingIntent, ScanSettings paramScanSettings, List<ScanFilter> paramList, String paramString1, String paramString2) throws RemoteException;
  
  void stopAdvertisingSet(IAdvertisingSetCallback paramIAdvertisingSetCallback) throws RemoteException;
  
  void stopScan(int paramInt) throws RemoteException;
  
  void stopScanForIntent(PendingIntent paramPendingIntent, String paramString) throws RemoteException;
  
  void unregAll() throws RemoteException;
  
  void unregisterClient(int paramInt) throws RemoteException;
  
  void unregisterScanner(int paramInt) throws RemoteException;
  
  void unregisterServer(int paramInt) throws RemoteException;
  
  void unregisterSync(IPeriodicAdvertisingCallback paramIPeriodicAdvertisingCallback) throws RemoteException;
  
  void writeCharacteristic(int paramInt1, String paramString, int paramInt2, int paramInt3, int paramInt4, byte[] paramArrayOfbyte) throws RemoteException;
  
  void writeDescriptor(int paramInt1, String paramString, int paramInt2, int paramInt3, byte[] paramArrayOfbyte) throws RemoteException;
  
  class Default implements IBluetoothGatt {
    public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public void registerScanner(IScannerCallback param1IScannerCallback, WorkSource param1WorkSource) throws RemoteException {}
    
    public void unregisterScanner(int param1Int) throws RemoteException {}
    
    public void startScan(int param1Int, ScanSettings param1ScanSettings, List<ScanFilter> param1List, List param1List1, String param1String1, String param1String2) throws RemoteException {}
    
    public void startScanForIntent(PendingIntent param1PendingIntent, ScanSettings param1ScanSettings, List<ScanFilter> param1List, String param1String1, String param1String2) throws RemoteException {}
    
    public void stopScanForIntent(PendingIntent param1PendingIntent, String param1String) throws RemoteException {}
    
    public void stopScan(int param1Int) throws RemoteException {}
    
    public void flushPendingBatchResults(int param1Int) throws RemoteException {}
    
    public void startAdvertisingSet(AdvertisingSetParameters param1AdvertisingSetParameters, AdvertiseData param1AdvertiseData1, AdvertiseData param1AdvertiseData2, PeriodicAdvertisingParameters param1PeriodicAdvertisingParameters, AdvertiseData param1AdvertiseData3, int param1Int1, int param1Int2, IAdvertisingSetCallback param1IAdvertisingSetCallback) throws RemoteException {}
    
    public void stopAdvertisingSet(IAdvertisingSetCallback param1IAdvertisingSetCallback) throws RemoteException {}
    
    public void getOwnAddress(int param1Int) throws RemoteException {}
    
    public void enableAdvertisingSet(int param1Int1, boolean param1Boolean, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void setAdvertisingData(int param1Int, AdvertiseData param1AdvertiseData) throws RemoteException {}
    
    public void setScanResponseData(int param1Int, AdvertiseData param1AdvertiseData) throws RemoteException {}
    
    public void setAdvertisingParameters(int param1Int, AdvertisingSetParameters param1AdvertisingSetParameters) throws RemoteException {}
    
    public void setPeriodicAdvertisingParameters(int param1Int, PeriodicAdvertisingParameters param1PeriodicAdvertisingParameters) throws RemoteException {}
    
    public void setPeriodicAdvertisingData(int param1Int, AdvertiseData param1AdvertiseData) throws RemoteException {}
    
    public void setPeriodicAdvertisingEnable(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void registerSync(ScanResult param1ScanResult, int param1Int1, int param1Int2, IPeriodicAdvertisingCallback param1IPeriodicAdvertisingCallback) throws RemoteException {}
    
    public void unregisterSync(IPeriodicAdvertisingCallback param1IPeriodicAdvertisingCallback) throws RemoteException {}
    
    public void registerClient(ParcelUuid param1ParcelUuid, IBluetoothGattCallback param1IBluetoothGattCallback) throws RemoteException {}
    
    public void unregisterClient(int param1Int) throws RemoteException {}
    
    public void clientConnect(int param1Int1, String param1String, boolean param1Boolean1, int param1Int2, boolean param1Boolean2, int param1Int3) throws RemoteException {}
    
    public void clientDisconnect(int param1Int, String param1String) throws RemoteException {}
    
    public void clientSetPreferredPhy(int param1Int1, String param1String, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {}
    
    public void clientReadPhy(int param1Int, String param1String) throws RemoteException {}
    
    public void refreshDevice(int param1Int, String param1String) throws RemoteException {}
    
    public void discoverServices(int param1Int, String param1String) throws RemoteException {}
    
    public void discoverServiceByUuid(int param1Int, String param1String, ParcelUuid param1ParcelUuid) throws RemoteException {}
    
    public void readCharacteristic(int param1Int1, String param1String, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void readUsingCharacteristicUuid(int param1Int1, String param1String, ParcelUuid param1ParcelUuid, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {}
    
    public void writeCharacteristic(int param1Int1, String param1String, int param1Int2, int param1Int3, int param1Int4, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void readDescriptor(int param1Int1, String param1String, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void writeDescriptor(int param1Int1, String param1String, int param1Int2, int param1Int3, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void registerForNotification(int param1Int1, String param1String, int param1Int2, boolean param1Boolean) throws RemoteException {}
    
    public void beginReliableWrite(int param1Int, String param1String) throws RemoteException {}
    
    public void endReliableWrite(int param1Int, String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void readRemoteRssi(int param1Int, String param1String) throws RemoteException {}
    
    public void configureMTU(int param1Int1, String param1String, int param1Int2) throws RemoteException {}
    
    public void connectionParameterUpdate(int param1Int1, String param1String, int param1Int2) throws RemoteException {}
    
    public void leConnectionUpdate(int param1Int1, String param1String, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6, int param1Int7) throws RemoteException {}
    
    public void registerServer(ParcelUuid param1ParcelUuid, IBluetoothGattServerCallback param1IBluetoothGattServerCallback) throws RemoteException {}
    
    public void unregisterServer(int param1Int) throws RemoteException {}
    
    public void serverConnect(int param1Int1, String param1String, boolean param1Boolean, int param1Int2) throws RemoteException {}
    
    public void serverDisconnect(int param1Int, String param1String) throws RemoteException {}
    
    public void serverSetPreferredPhy(int param1Int1, String param1String, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {}
    
    public void serverReadPhy(int param1Int, String param1String) throws RemoteException {}
    
    public void addService(int param1Int, BluetoothGattService param1BluetoothGattService) throws RemoteException {}
    
    public void removeService(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void clearServices(int param1Int) throws RemoteException {}
    
    public void sendResponse(int param1Int1, String param1String, int param1Int2, int param1Int3, int param1Int4, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void sendNotification(int param1Int1, String param1String, int param1Int2, boolean param1Boolean, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void disconnectAll() throws RemoteException {}
    
    public void unregAll() throws RemoteException {}
    
    public int numHwTrackFiltersAvailable() throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IBluetoothGatt {
    private static final String DESCRIPTOR = "android.bluetooth.IBluetoothGatt";
    
    static final int TRANSACTION_addService = 48;
    
    static final int TRANSACTION_beginReliableWrite = 36;
    
    static final int TRANSACTION_clearServices = 50;
    
    static final int TRANSACTION_clientConnect = 23;
    
    static final int TRANSACTION_clientDisconnect = 24;
    
    static final int TRANSACTION_clientReadPhy = 26;
    
    static final int TRANSACTION_clientSetPreferredPhy = 25;
    
    static final int TRANSACTION_configureMTU = 39;
    
    static final int TRANSACTION_connectionParameterUpdate = 40;
    
    static final int TRANSACTION_disconnectAll = 53;
    
    static final int TRANSACTION_discoverServiceByUuid = 29;
    
    static final int TRANSACTION_discoverServices = 28;
    
    static final int TRANSACTION_enableAdvertisingSet = 12;
    
    static final int TRANSACTION_endReliableWrite = 37;
    
    static final int TRANSACTION_flushPendingBatchResults = 8;
    
    static final int TRANSACTION_getDevicesMatchingConnectionStates = 1;
    
    static final int TRANSACTION_getOwnAddress = 11;
    
    static final int TRANSACTION_leConnectionUpdate = 41;
    
    static final int TRANSACTION_numHwTrackFiltersAvailable = 55;
    
    static final int TRANSACTION_readCharacteristic = 30;
    
    static final int TRANSACTION_readDescriptor = 33;
    
    static final int TRANSACTION_readRemoteRssi = 38;
    
    static final int TRANSACTION_readUsingCharacteristicUuid = 31;
    
    static final int TRANSACTION_refreshDevice = 27;
    
    static final int TRANSACTION_registerClient = 21;
    
    static final int TRANSACTION_registerForNotification = 35;
    
    static final int TRANSACTION_registerScanner = 2;
    
    static final int TRANSACTION_registerServer = 42;
    
    static final int TRANSACTION_registerSync = 19;
    
    static final int TRANSACTION_removeService = 49;
    
    static final int TRANSACTION_sendNotification = 52;
    
    static final int TRANSACTION_sendResponse = 51;
    
    static final int TRANSACTION_serverConnect = 44;
    
    static final int TRANSACTION_serverDisconnect = 45;
    
    static final int TRANSACTION_serverReadPhy = 47;
    
    static final int TRANSACTION_serverSetPreferredPhy = 46;
    
    static final int TRANSACTION_setAdvertisingData = 13;
    
    static final int TRANSACTION_setAdvertisingParameters = 15;
    
    static final int TRANSACTION_setPeriodicAdvertisingData = 17;
    
    static final int TRANSACTION_setPeriodicAdvertisingEnable = 18;
    
    static final int TRANSACTION_setPeriodicAdvertisingParameters = 16;
    
    static final int TRANSACTION_setScanResponseData = 14;
    
    static final int TRANSACTION_startAdvertisingSet = 9;
    
    static final int TRANSACTION_startScan = 4;
    
    static final int TRANSACTION_startScanForIntent = 5;
    
    static final int TRANSACTION_stopAdvertisingSet = 10;
    
    static final int TRANSACTION_stopScan = 7;
    
    static final int TRANSACTION_stopScanForIntent = 6;
    
    static final int TRANSACTION_unregAll = 54;
    
    static final int TRANSACTION_unregisterClient = 22;
    
    static final int TRANSACTION_unregisterScanner = 3;
    
    static final int TRANSACTION_unregisterServer = 43;
    
    static final int TRANSACTION_unregisterSync = 20;
    
    static final int TRANSACTION_writeCharacteristic = 32;
    
    static final int TRANSACTION_writeDescriptor = 34;
    
    public Stub() {
      attachInterface(this, "android.bluetooth.IBluetoothGatt");
    }
    
    public static IBluetoothGatt asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.bluetooth.IBluetoothGatt");
      if (iInterface != null && iInterface instanceof IBluetoothGatt)
        return (IBluetoothGatt)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 55:
          return "numHwTrackFiltersAvailable";
        case 54:
          return "unregAll";
        case 53:
          return "disconnectAll";
        case 52:
          return "sendNotification";
        case 51:
          return "sendResponse";
        case 50:
          return "clearServices";
        case 49:
          return "removeService";
        case 48:
          return "addService";
        case 47:
          return "serverReadPhy";
        case 46:
          return "serverSetPreferredPhy";
        case 45:
          return "serverDisconnect";
        case 44:
          return "serverConnect";
        case 43:
          return "unregisterServer";
        case 42:
          return "registerServer";
        case 41:
          return "leConnectionUpdate";
        case 40:
          return "connectionParameterUpdate";
        case 39:
          return "configureMTU";
        case 38:
          return "readRemoteRssi";
        case 37:
          return "endReliableWrite";
        case 36:
          return "beginReliableWrite";
        case 35:
          return "registerForNotification";
        case 34:
          return "writeDescriptor";
        case 33:
          return "readDescriptor";
        case 32:
          return "writeCharacteristic";
        case 31:
          return "readUsingCharacteristicUuid";
        case 30:
          return "readCharacteristic";
        case 29:
          return "discoverServiceByUuid";
        case 28:
          return "discoverServices";
        case 27:
          return "refreshDevice";
        case 26:
          return "clientReadPhy";
        case 25:
          return "clientSetPreferredPhy";
        case 24:
          return "clientDisconnect";
        case 23:
          return "clientConnect";
        case 22:
          return "unregisterClient";
        case 21:
          return "registerClient";
        case 20:
          return "unregisterSync";
        case 19:
          return "registerSync";
        case 18:
          return "setPeriodicAdvertisingEnable";
        case 17:
          return "setPeriodicAdvertisingData";
        case 16:
          return "setPeriodicAdvertisingParameters";
        case 15:
          return "setAdvertisingParameters";
        case 14:
          return "setScanResponseData";
        case 13:
          return "setAdvertisingData";
        case 12:
          return "enableAdvertisingSet";
        case 11:
          return "getOwnAddress";
        case 10:
          return "stopAdvertisingSet";
        case 9:
          return "startAdvertisingSet";
        case 8:
          return "flushPendingBatchResults";
        case 7:
          return "stopScan";
        case 6:
          return "stopScanForIntent";
        case 5:
          return "startScanForIntent";
        case 4:
          return "startScan";
        case 3:
          return "unregisterScanner";
        case 2:
          return "registerScanner";
        case 1:
          break;
      } 
      return "getDevicesMatchingConnectionStates";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        byte[] arrayOfByte2;
        String str4;
        IBluetoothGattServerCallback iBluetoothGattServerCallback;
        String str3;
        byte[] arrayOfByte1;
        String str2;
        IBluetoothGattCallback iBluetoothGattCallback;
        IPeriodicAdvertisingCallback iPeriodicAdvertisingCallback;
        IAdvertisingSetCallback iAdvertisingSetCallback;
        String str1, str5;
        IScannerCallback iScannerCallback;
        int i, j, k, m, n;
        String str6;
        ArrayList<ScanFilter> arrayList1;
        AdvertiseData advertiseData1;
        String str7;
        ClassLoader classLoader;
        ArrayList arrayList;
        PeriodicAdvertisingParameters periodicAdvertisingParameters;
        ArrayList<ScanFilter> arrayList2;
        String str8;
        AdvertiseData advertiseData2;
        boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false, bool5 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 55:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = numHwTrackFiltersAvailable();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 54:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothGatt");
            unregAll();
            param1Parcel2.writeNoException();
            return true;
          case 53:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothGatt");
            disconnectAll();
            param1Parcel2.writeNoException();
            return true;
          case 52:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = param1Parcel1.readInt();
            str5 = param1Parcel1.readString();
            param1Int2 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            arrayOfByte2 = param1Parcel1.createByteArray();
            sendNotification(param1Int1, str5, param1Int2, bool4, arrayOfByte2);
            param1Parcel2.writeNoException();
            return true;
          case 51:
            arrayOfByte2.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = arrayOfByte2.readInt();
            str5 = arrayOfByte2.readString();
            param1Int2 = arrayOfByte2.readInt();
            i = arrayOfByte2.readInt();
            j = arrayOfByte2.readInt();
            arrayOfByte2 = arrayOfByte2.createByteArray();
            sendResponse(param1Int1, str5, param1Int2, i, j, arrayOfByte2);
            param1Parcel2.writeNoException();
            return true;
          case 50:
            arrayOfByte2.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = arrayOfByte2.readInt();
            clearServices(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 49:
            arrayOfByte2.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int2 = arrayOfByte2.readInt();
            param1Int1 = arrayOfByte2.readInt();
            removeService(param1Int2, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 48:
            arrayOfByte2.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = arrayOfByte2.readInt();
            if (arrayOfByte2.readInt() != 0) {
              BluetoothGattService bluetoothGattService = (BluetoothGattService)BluetoothGattService.CREATOR.createFromParcel((Parcel)arrayOfByte2);
            } else {
              arrayOfByte2 = null;
            } 
            addService(param1Int1, (BluetoothGattService)arrayOfByte2);
            param1Parcel2.writeNoException();
            return true;
          case 47:
            arrayOfByte2.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = arrayOfByte2.readInt();
            str4 = arrayOfByte2.readString();
            serverReadPhy(param1Int1, str4);
            param1Parcel2.writeNoException();
            return true;
          case 46:
            str4.enforceInterface("android.bluetooth.IBluetoothGatt");
            j = str4.readInt();
            str5 = str4.readString();
            param1Int1 = str4.readInt();
            i = str4.readInt();
            param1Int2 = str4.readInt();
            serverSetPreferredPhy(j, str5, param1Int1, i, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 45:
            str4.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = str4.readInt();
            str4 = str4.readString();
            serverDisconnect(param1Int1, str4);
            param1Parcel2.writeNoException();
            return true;
          case 44:
            str4.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int2 = str4.readInt();
            str5 = str4.readString();
            bool4 = bool5;
            if (str4.readInt() != 0)
              bool4 = true; 
            param1Int1 = str4.readInt();
            serverConnect(param1Int2, str5, bool4, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 43:
            str4.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = str4.readInt();
            unregisterServer(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 42:
            str4.enforceInterface("android.bluetooth.IBluetoothGatt");
            if (str4.readInt() != 0) {
              ParcelUuid parcelUuid = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel((Parcel)str4);
            } else {
              str5 = null;
            } 
            iBluetoothGattServerCallback = IBluetoothGattServerCallback.Stub.asInterface(str4.readStrongBinder());
            registerServer((ParcelUuid)str5, iBluetoothGattServerCallback);
            param1Parcel2.writeNoException();
            return true;
          case 41:
            iBluetoothGattServerCallback.enforceInterface("android.bluetooth.IBluetoothGatt");
            k = iBluetoothGattServerCallback.readInt();
            str5 = iBluetoothGattServerCallback.readString();
            j = iBluetoothGattServerCallback.readInt();
            i = iBluetoothGattServerCallback.readInt();
            m = iBluetoothGattServerCallback.readInt();
            n = iBluetoothGattServerCallback.readInt();
            param1Int1 = iBluetoothGattServerCallback.readInt();
            param1Int2 = iBluetoothGattServerCallback.readInt();
            leConnectionUpdate(k, str5, j, i, m, n, param1Int1, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 40:
            iBluetoothGattServerCallback.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int2 = iBluetoothGattServerCallback.readInt();
            str5 = iBluetoothGattServerCallback.readString();
            param1Int1 = iBluetoothGattServerCallback.readInt();
            connectionParameterUpdate(param1Int2, str5, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 39:
            iBluetoothGattServerCallback.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = iBluetoothGattServerCallback.readInt();
            str5 = iBluetoothGattServerCallback.readString();
            param1Int2 = iBluetoothGattServerCallback.readInt();
            configureMTU(param1Int1, str5, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 38:
            iBluetoothGattServerCallback.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = iBluetoothGattServerCallback.readInt();
            str3 = iBluetoothGattServerCallback.readString();
            readRemoteRssi(param1Int1, str3);
            param1Parcel2.writeNoException();
            return true;
          case 37:
            str3.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = str3.readInt();
            str5 = str3.readString();
            bool4 = bool1;
            if (str3.readInt() != 0)
              bool4 = true; 
            endReliableWrite(param1Int1, str5, bool4);
            param1Parcel2.writeNoException();
            return true;
          case 36:
            str3.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = str3.readInt();
            str3 = str3.readString();
            beginReliableWrite(param1Int1, str3);
            param1Parcel2.writeNoException();
            return true;
          case 35:
            str3.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int2 = str3.readInt();
            str5 = str3.readString();
            param1Int1 = str3.readInt();
            bool4 = bool2;
            if (str3.readInt() != 0)
              bool4 = true; 
            registerForNotification(param1Int2, str5, param1Int1, bool4);
            param1Parcel2.writeNoException();
            return true;
          case 34:
            str3.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int2 = str3.readInt();
            str5 = str3.readString();
            j = str3.readInt();
            param1Int1 = str3.readInt();
            arrayOfByte1 = str3.createByteArray();
            writeDescriptor(param1Int2, str5, j, param1Int1, arrayOfByte1);
            param1Parcel2.writeNoException();
            return true;
          case 33:
            arrayOfByte1.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int2 = arrayOfByte1.readInt();
            str5 = arrayOfByte1.readString();
            param1Int1 = arrayOfByte1.readInt();
            j = arrayOfByte1.readInt();
            readDescriptor(param1Int2, str5, param1Int1, j);
            param1Parcel2.writeNoException();
            return true;
          case 32:
            arrayOfByte1.enforceInterface("android.bluetooth.IBluetoothGatt");
            i = arrayOfByte1.readInt();
            str5 = arrayOfByte1.readString();
            param1Int2 = arrayOfByte1.readInt();
            j = arrayOfByte1.readInt();
            param1Int1 = arrayOfByte1.readInt();
            arrayOfByte1 = arrayOfByte1.createByteArray();
            writeCharacteristic(i, str5, param1Int2, j, param1Int1, arrayOfByte1);
            param1Parcel2.writeNoException();
            return true;
          case 31:
            arrayOfByte1.enforceInterface("android.bluetooth.IBluetoothGatt");
            j = arrayOfByte1.readInt();
            str6 = arrayOfByte1.readString();
            if (arrayOfByte1.readInt() != 0) {
              ParcelUuid parcelUuid = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel((Parcel)arrayOfByte1);
            } else {
              str5 = null;
            } 
            param1Int2 = arrayOfByte1.readInt();
            i = arrayOfByte1.readInt();
            param1Int1 = arrayOfByte1.readInt();
            readUsingCharacteristicUuid(j, str6, (ParcelUuid)str5, param1Int2, i, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 30:
            arrayOfByte1.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int2 = arrayOfByte1.readInt();
            str5 = arrayOfByte1.readString();
            param1Int1 = arrayOfByte1.readInt();
            j = arrayOfByte1.readInt();
            readCharacteristic(param1Int2, str5, param1Int1, j);
            param1Parcel2.writeNoException();
            return true;
          case 29:
            arrayOfByte1.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = arrayOfByte1.readInt();
            str5 = arrayOfByte1.readString();
            if (arrayOfByte1.readInt() != 0) {
              ParcelUuid parcelUuid = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel((Parcel)arrayOfByte1);
            } else {
              arrayOfByte1 = null;
            } 
            discoverServiceByUuid(param1Int1, str5, (ParcelUuid)arrayOfByte1);
            param1Parcel2.writeNoException();
            return true;
          case 28:
            arrayOfByte1.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = arrayOfByte1.readInt();
            str2 = arrayOfByte1.readString();
            discoverServices(param1Int1, str2);
            param1Parcel2.writeNoException();
            return true;
          case 27:
            str2.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = str2.readInt();
            str2 = str2.readString();
            refreshDevice(param1Int1, str2);
            param1Parcel2.writeNoException();
            return true;
          case 26:
            str2.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = str2.readInt();
            str2 = str2.readString();
            clientReadPhy(param1Int1, str2);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            str2.enforceInterface("android.bluetooth.IBluetoothGatt");
            i = str2.readInt();
            str5 = str2.readString();
            param1Int2 = str2.readInt();
            param1Int1 = str2.readInt();
            j = str2.readInt();
            clientSetPreferredPhy(i, str5, param1Int2, param1Int1, j);
            param1Parcel2.writeNoException();
            return true;
          case 24:
            str2.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = str2.readInt();
            str2 = str2.readString();
            clientDisconnect(param1Int1, str2);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            str2.enforceInterface("android.bluetooth.IBluetoothGatt");
            j = str2.readInt();
            str5 = str2.readString();
            if (str2.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            param1Int2 = str2.readInt();
            if (str2.readInt() != 0) {
              bool3 = true;
            } else {
              bool3 = false;
            } 
            param1Int1 = str2.readInt();
            clientConnect(j, str5, bool4, param1Int2, bool3, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            str2.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = str2.readInt();
            unregisterClient(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            str2.enforceInterface("android.bluetooth.IBluetoothGatt");
            if (str2.readInt() != 0) {
              ParcelUuid parcelUuid = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str5 = null;
            } 
            iBluetoothGattCallback = IBluetoothGattCallback.Stub.asInterface(str2.readStrongBinder());
            registerClient((ParcelUuid)str5, iBluetoothGattCallback);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            iBluetoothGattCallback.enforceInterface("android.bluetooth.IBluetoothGatt");
            iPeriodicAdvertisingCallback = IPeriodicAdvertisingCallback.Stub.asInterface(iBluetoothGattCallback.readStrongBinder());
            unregisterSync(iPeriodicAdvertisingCallback);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            iPeriodicAdvertisingCallback.enforceInterface("android.bluetooth.IBluetoothGatt");
            if (iPeriodicAdvertisingCallback.readInt() != 0) {
              ScanResult scanResult = (ScanResult)ScanResult.CREATOR.createFromParcel((Parcel)iPeriodicAdvertisingCallback);
            } else {
              str5 = null;
            } 
            param1Int2 = iPeriodicAdvertisingCallback.readInt();
            param1Int1 = iPeriodicAdvertisingCallback.readInt();
            iPeriodicAdvertisingCallback = IPeriodicAdvertisingCallback.Stub.asInterface(iPeriodicAdvertisingCallback.readStrongBinder());
            registerSync((ScanResult)str5, param1Int2, param1Int1, iPeriodicAdvertisingCallback);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            iPeriodicAdvertisingCallback.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = iPeriodicAdvertisingCallback.readInt();
            bool4 = bool3;
            if (iPeriodicAdvertisingCallback.readInt() != 0)
              bool4 = true; 
            setPeriodicAdvertisingEnable(param1Int1, bool4);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            iPeriodicAdvertisingCallback.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = iPeriodicAdvertisingCallback.readInt();
            if (iPeriodicAdvertisingCallback.readInt() != 0) {
              AdvertiseData advertiseData = (AdvertiseData)AdvertiseData.CREATOR.createFromParcel((Parcel)iPeriodicAdvertisingCallback);
            } else {
              iPeriodicAdvertisingCallback = null;
            } 
            setPeriodicAdvertisingData(param1Int1, (AdvertiseData)iPeriodicAdvertisingCallback);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            iPeriodicAdvertisingCallback.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = iPeriodicAdvertisingCallback.readInt();
            if (iPeriodicAdvertisingCallback.readInt() != 0) {
              PeriodicAdvertisingParameters periodicAdvertisingParameters1 = (PeriodicAdvertisingParameters)PeriodicAdvertisingParameters.CREATOR.createFromParcel((Parcel)iPeriodicAdvertisingCallback);
            } else {
              iPeriodicAdvertisingCallback = null;
            } 
            setPeriodicAdvertisingParameters(param1Int1, (PeriodicAdvertisingParameters)iPeriodicAdvertisingCallback);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            iPeriodicAdvertisingCallback.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = iPeriodicAdvertisingCallback.readInt();
            if (iPeriodicAdvertisingCallback.readInt() != 0) {
              AdvertisingSetParameters advertisingSetParameters = (AdvertisingSetParameters)AdvertisingSetParameters.CREATOR.createFromParcel((Parcel)iPeriodicAdvertisingCallback);
            } else {
              iPeriodicAdvertisingCallback = null;
            } 
            setAdvertisingParameters(param1Int1, (AdvertisingSetParameters)iPeriodicAdvertisingCallback);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            iPeriodicAdvertisingCallback.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = iPeriodicAdvertisingCallback.readInt();
            if (iPeriodicAdvertisingCallback.readInt() != 0) {
              AdvertiseData advertiseData = (AdvertiseData)AdvertiseData.CREATOR.createFromParcel((Parcel)iPeriodicAdvertisingCallback);
            } else {
              iPeriodicAdvertisingCallback = null;
            } 
            setScanResponseData(param1Int1, (AdvertiseData)iPeriodicAdvertisingCallback);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            iPeriodicAdvertisingCallback.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = iPeriodicAdvertisingCallback.readInt();
            if (iPeriodicAdvertisingCallback.readInt() != 0) {
              AdvertiseData advertiseData = (AdvertiseData)AdvertiseData.CREATOR.createFromParcel((Parcel)iPeriodicAdvertisingCallback);
            } else {
              iPeriodicAdvertisingCallback = null;
            } 
            setAdvertisingData(param1Int1, (AdvertiseData)iPeriodicAdvertisingCallback);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            iPeriodicAdvertisingCallback.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = iPeriodicAdvertisingCallback.readInt();
            if (iPeriodicAdvertisingCallback.readInt() != 0)
              bool4 = true; 
            param1Int2 = iPeriodicAdvertisingCallback.readInt();
            j = iPeriodicAdvertisingCallback.readInt();
            enableAdvertisingSet(param1Int1, bool4, param1Int2, j);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            iPeriodicAdvertisingCallback.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = iPeriodicAdvertisingCallback.readInt();
            getOwnAddress(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            iPeriodicAdvertisingCallback.enforceInterface("android.bluetooth.IBluetoothGatt");
            iAdvertisingSetCallback = IAdvertisingSetCallback.Stub.asInterface(iPeriodicAdvertisingCallback.readStrongBinder());
            stopAdvertisingSet(iAdvertisingSetCallback);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            iAdvertisingSetCallback.enforceInterface("android.bluetooth.IBluetoothGatt");
            if (iAdvertisingSetCallback.readInt() != 0) {
              AdvertisingSetParameters advertisingSetParameters = (AdvertisingSetParameters)AdvertisingSetParameters.CREATOR.createFromParcel((Parcel)iAdvertisingSetCallback);
            } else {
              str5 = null;
            } 
            if (iAdvertisingSetCallback.readInt() != 0) {
              AdvertiseData advertiseData = (AdvertiseData)AdvertiseData.CREATOR.createFromParcel((Parcel)iAdvertisingSetCallback);
            } else {
              str6 = null;
            } 
            if (iAdvertisingSetCallback.readInt() != 0) {
              advertiseData1 = (AdvertiseData)AdvertiseData.CREATOR.createFromParcel((Parcel)iAdvertisingSetCallback);
            } else {
              advertiseData1 = null;
            } 
            if (iAdvertisingSetCallback.readInt() != 0) {
              periodicAdvertisingParameters = (PeriodicAdvertisingParameters)PeriodicAdvertisingParameters.CREATOR.createFromParcel((Parcel)iAdvertisingSetCallback);
            } else {
              periodicAdvertisingParameters = null;
            } 
            if (iAdvertisingSetCallback.readInt() != 0) {
              advertiseData2 = (AdvertiseData)AdvertiseData.CREATOR.createFromParcel((Parcel)iAdvertisingSetCallback);
            } else {
              advertiseData2 = null;
            } 
            param1Int1 = iAdvertisingSetCallback.readInt();
            param1Int2 = iAdvertisingSetCallback.readInt();
            iAdvertisingSetCallback = IAdvertisingSetCallback.Stub.asInterface(iAdvertisingSetCallback.readStrongBinder());
            startAdvertisingSet((AdvertisingSetParameters)str5, (AdvertiseData)str6, advertiseData1, periodicAdvertisingParameters, advertiseData2, param1Int1, param1Int2, iAdvertisingSetCallback);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            iAdvertisingSetCallback.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = iAdvertisingSetCallback.readInt();
            flushPendingBatchResults(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            iAdvertisingSetCallback.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = iAdvertisingSetCallback.readInt();
            stopScan(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            iAdvertisingSetCallback.enforceInterface("android.bluetooth.IBluetoothGatt");
            if (iAdvertisingSetCallback.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)iAdvertisingSetCallback);
            } else {
              str5 = null;
            } 
            str1 = iAdvertisingSetCallback.readString();
            stopScanForIntent((PendingIntent)str5, str1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            str1.enforceInterface("android.bluetooth.IBluetoothGatt");
            if (str1.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str5 = null;
            } 
            if (str1.readInt() != 0) {
              ScanSettings scanSettings = (ScanSettings)ScanSettings.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str6 = null;
            } 
            arrayList2 = str1.createTypedArrayList(ScanFilter.CREATOR);
            str7 = str1.readString();
            str1 = str1.readString();
            startScanForIntent((PendingIntent)str5, (ScanSettings)str6, arrayList2, str7, str1);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str1.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = str1.readInt();
            if (str1.readInt() != 0) {
              ScanSettings scanSettings = (ScanSettings)ScanSettings.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str5 = null;
            } 
            arrayList1 = str1.createTypedArrayList(ScanFilter.CREATOR);
            classLoader = getClass().getClassLoader();
            arrayList = str1.readArrayList(classLoader);
            str8 = str1.readString();
            str1 = str1.readString();
            startScan(param1Int1, (ScanSettings)str5, arrayList1, arrayList, str8, str1);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            str1.enforceInterface("android.bluetooth.IBluetoothGatt");
            param1Int1 = str1.readInt();
            unregisterScanner(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str1.enforceInterface("android.bluetooth.IBluetoothGatt");
            iScannerCallback = IScannerCallback.Stub.asInterface(str1.readStrongBinder());
            if (str1.readInt() != 0) {
              WorkSource workSource = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            registerScanner(iScannerCallback, (WorkSource)str1);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.bluetooth.IBluetoothGatt");
        int[] arrayOfInt = str1.createIntArray();
        List<BluetoothDevice> list = getDevicesMatchingConnectionStates(arrayOfInt);
        param1Parcel2.writeNoException();
        param1Parcel2.writeTypedList(list);
        return true;
      } 
      param1Parcel2.writeString("android.bluetooth.IBluetoothGatt");
      return true;
    }
    
    private static class Proxy implements IBluetoothGatt {
      public static IBluetoothGatt sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.bluetooth.IBluetoothGatt";
      }
      
      public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null)
            return IBluetoothGatt.Stub.getDefaultImpl().getDevicesMatchingConnectionStates(param2ArrayOfint); 
          parcel2.readException();
          return parcel2.createTypedArrayList(BluetoothDevice.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerScanner(IScannerCallback param2IScannerCallback, WorkSource param2WorkSource) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          if (param2IScannerCallback != null) {
            iBinder = param2IScannerCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().registerScanner(param2IScannerCallback, param2WorkSource);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterScanner(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().unregisterScanner(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startScan(int param2Int, ScanSettings param2ScanSettings, List<ScanFilter> param2List, List param2List1, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          try {
            parcel1.writeInt(param2Int);
            if (param2ScanSettings != null) {
              parcel1.writeInt(1);
              param2ScanSettings.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            try {
              parcel1.writeTypedList(param2List);
              try {
                parcel1.writeList(param2List1);
                try {
                  parcel1.writeString(param2String1);
                  try {
                    parcel1.writeString(param2String2);
                    boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
                    if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
                      IBluetoothGatt.Stub.getDefaultImpl().startScan(param2Int, param2ScanSettings, param2List, param2List1, param2String1, param2String2);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2ScanSettings;
      }
      
      public void startScanForIntent(PendingIntent param2PendingIntent, ScanSettings param2ScanSettings, List<ScanFilter> param2List, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2ScanSettings != null) {
            parcel1.writeInt(1);
            param2ScanSettings.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeTypedList(param2List);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().startScanForIntent(param2PendingIntent, param2ScanSettings, param2List, param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopScanForIntent(PendingIntent param2PendingIntent, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().stopScanForIntent(param2PendingIntent, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopScan(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().stopScan(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void flushPendingBatchResults(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().flushPendingBatchResults(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startAdvertisingSet(AdvertisingSetParameters param2AdvertisingSetParameters, AdvertiseData param2AdvertiseData1, AdvertiseData param2AdvertiseData2, PeriodicAdvertisingParameters param2PeriodicAdvertisingParameters, AdvertiseData param2AdvertiseData3, int param2Int1, int param2Int2, IAdvertisingSetCallback param2IAdvertisingSetCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          if (param2AdvertisingSetParameters != null) {
            try {
              parcel1.writeInt(1);
              param2AdvertisingSetParameters.writeToParcel(parcel1, 0);
            } finally {}
          } else {
            parcel1.writeInt(0);
          } 
          if (param2AdvertiseData1 != null) {
            parcel1.writeInt(1);
            param2AdvertiseData1.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2AdvertiseData2 != null) {
            parcel1.writeInt(1);
            param2AdvertiseData2.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2PeriodicAdvertisingParameters != null) {
            parcel1.writeInt(1);
            param2PeriodicAdvertisingParameters.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2AdvertiseData3 != null) {
            parcel1.writeInt(1);
            param2AdvertiseData3.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2IAdvertisingSetCallback != null) {
            iBinder = param2IAdvertisingSetCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt iBluetoothGatt = IBluetoothGatt.Stub.getDefaultImpl();
            try {
              iBluetoothGatt.startAdvertisingSet(param2AdvertisingSetParameters, param2AdvertiseData1, param2AdvertiseData2, param2PeriodicAdvertisingParameters, param2AdvertiseData3, param2Int1, param2Int2, param2IAdvertisingSetCallback);
              parcel2.recycle();
              parcel1.recycle();
              return;
            } finally {}
          } else {
            parcel2.readException();
            parcel2.recycle();
            parcel1.recycle();
            return;
          } 
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2AdvertisingSetParameters;
      }
      
      public void stopAdvertisingSet(IAdvertisingSetCallback param2IAdvertisingSetCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          if (param2IAdvertisingSetCallback != null) {
            iBinder = param2IAdvertisingSetCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().stopAdvertisingSet(param2IAdvertisingSetCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getOwnAddress(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().getOwnAddress(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enableAdvertisingSet(int param2Int1, boolean param2Boolean, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int1);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool1 = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool1 && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().enableAdvertisingSet(param2Int1, param2Boolean, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAdvertisingData(int param2Int, AdvertiseData param2AdvertiseData) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          if (param2AdvertiseData != null) {
            parcel1.writeInt(1);
            param2AdvertiseData.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().setAdvertisingData(param2Int, param2AdvertiseData);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setScanResponseData(int param2Int, AdvertiseData param2AdvertiseData) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          if (param2AdvertiseData != null) {
            parcel1.writeInt(1);
            param2AdvertiseData.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().setScanResponseData(param2Int, param2AdvertiseData);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAdvertisingParameters(int param2Int, AdvertisingSetParameters param2AdvertisingSetParameters) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          if (param2AdvertisingSetParameters != null) {
            parcel1.writeInt(1);
            param2AdvertisingSetParameters.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().setAdvertisingParameters(param2Int, param2AdvertisingSetParameters);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPeriodicAdvertisingParameters(int param2Int, PeriodicAdvertisingParameters param2PeriodicAdvertisingParameters) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          if (param2PeriodicAdvertisingParameters != null) {
            parcel1.writeInt(1);
            param2PeriodicAdvertisingParameters.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().setPeriodicAdvertisingParameters(param2Int, param2PeriodicAdvertisingParameters);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPeriodicAdvertisingData(int param2Int, AdvertiseData param2AdvertiseData) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          if (param2AdvertiseData != null) {
            parcel1.writeInt(1);
            param2AdvertiseData.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().setPeriodicAdvertisingData(param2Int, param2AdvertiseData);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPeriodicAdvertisingEnable(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool1 && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().setPeriodicAdvertisingEnable(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerSync(ScanResult param2ScanResult, int param2Int1, int param2Int2, IPeriodicAdvertisingCallback param2IPeriodicAdvertisingCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          if (param2ScanResult != null) {
            parcel1.writeInt(1);
            param2ScanResult.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2IPeriodicAdvertisingCallback != null) {
            iBinder = param2IPeriodicAdvertisingCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().registerSync(param2ScanResult, param2Int1, param2Int2, param2IPeriodicAdvertisingCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterSync(IPeriodicAdvertisingCallback param2IPeriodicAdvertisingCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          if (param2IPeriodicAdvertisingCallback != null) {
            iBinder = param2IPeriodicAdvertisingCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().unregisterSync(param2IPeriodicAdvertisingCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerClient(ParcelUuid param2ParcelUuid, IBluetoothGattCallback param2IBluetoothGattCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          if (param2ParcelUuid != null) {
            parcel1.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IBluetoothGattCallback != null) {
            iBinder = param2IBluetoothGattCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().registerClient(param2ParcelUuid, param2IBluetoothGattCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterClient(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().unregisterClient(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clientConnect(int param2Int1, String param2String, boolean param2Boolean1, int param2Int2, boolean param2Boolean2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          try {
            parcel1.writeInt(param2Int1);
            try {
              boolean bool2;
              parcel1.writeString(param2String);
              boolean bool1 = true;
              if (param2Boolean1) {
                bool2 = true;
              } else {
                bool2 = false;
              } 
              parcel1.writeInt(bool2);
              try {
                parcel1.writeInt(param2Int2);
                if (param2Boolean2) {
                  bool2 = bool1;
                } else {
                  bool2 = false;
                } 
                parcel1.writeInt(bool2);
                try {
                  parcel1.writeInt(param2Int3);
                  try {
                    boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
                    if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
                      IBluetoothGatt.Stub.getDefaultImpl().clientConnect(param2Int1, param2String, param2Boolean1, param2Int2, param2Boolean2, param2Int3);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public void clientDisconnect(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().clientDisconnect(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clientSetPreferredPhy(int param2Int1, String param2String, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeInt(param2Int4);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().clientSetPreferredPhy(param2Int1, param2String, param2Int2, param2Int3, param2Int4);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clientReadPhy(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().clientReadPhy(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void refreshDevice(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().refreshDevice(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void discoverServices(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().discoverServices(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void discoverServiceByUuid(int param2Int, String param2String, ParcelUuid param2ParcelUuid) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          if (param2ParcelUuid != null) {
            parcel1.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().discoverServiceByUuid(param2Int, param2String, param2ParcelUuid);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void readCharacteristic(int param2Int1, String param2String, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().readCharacteristic(param2Int1, param2String, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void readUsingCharacteristicUuid(int param2Int1, String param2String, ParcelUuid param2ParcelUuid, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeString(param2String);
              if (param2ParcelUuid != null) {
                parcel1.writeInt(1);
                param2ParcelUuid.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              try {
                parcel1.writeInt(param2Int2);
                try {
                  parcel1.writeInt(param2Int3);
                  try {
                    parcel1.writeInt(param2Int4);
                    boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
                    if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
                      IBluetoothGatt.Stub.getDefaultImpl().readUsingCharacteristicUuid(param2Int1, param2String, param2ParcelUuid, param2Int2, param2Int3, param2Int4);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public void writeCharacteristic(int param2Int1, String param2String, int param2Int2, int param2Int3, int param2Int4, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeString(param2String);
              try {
                parcel1.writeInt(param2Int2);
                try {
                  parcel1.writeInt(param2Int3);
                  try {
                    parcel1.writeInt(param2Int4);
                    try {
                      parcel1.writeByteArray(param2ArrayOfbyte);
                      boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
                      if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
                        IBluetoothGatt.Stub.getDefaultImpl().writeCharacteristic(param2Int1, param2String, param2Int2, param2Int3, param2Int4, param2ArrayOfbyte);
                        parcel2.recycle();
                        parcel1.recycle();
                        return;
                      } 
                      parcel2.readException();
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public void readDescriptor(int param2Int1, String param2String, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().readDescriptor(param2Int1, param2String, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void writeDescriptor(int param2Int1, String param2String, int param2Int2, int param2Int3, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().writeDescriptor(param2Int1, param2String, param2Int2, param2Int3, param2ArrayOfbyte);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerForNotification(int param2Int1, String param2String, int param2Int2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool1 && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().registerForNotification(param2Int1, param2String, param2Int2, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void beginReliableWrite(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().beginReliableWrite(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void endReliableWrite(int param2Int, String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool1 && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().endReliableWrite(param2Int, param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void readRemoteRssi(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().readRemoteRssi(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void configureMTU(int param2Int1, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().configureMTU(param2Int1, param2String, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void connectionParameterUpdate(int param2Int1, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().connectionParameterUpdate(param2Int1, param2String, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void leConnectionUpdate(int param2Int1, String param2String, int param2Int2, int param2Int3, int param2Int4, int param2Int5, int param2Int6, int param2Int7) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeString(param2String);
              try {
                parcel1.writeInt(param2Int2);
                try {
                  parcel1.writeInt(param2Int3);
                  parcel1.writeInt(param2Int4);
                  parcel1.writeInt(param2Int5);
                  parcel1.writeInt(param2Int6);
                  parcel1.writeInt(param2Int7);
                  boolean bool = this.mRemote.transact(41, parcel1, parcel2, 0);
                  if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
                    IBluetoothGatt.Stub.getDefaultImpl().leConnectionUpdate(param2Int1, param2String, param2Int2, param2Int3, param2Int4, param2Int5, param2Int6, param2Int7);
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } 
                  parcel2.readException();
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public void registerServer(ParcelUuid param2ParcelUuid, IBluetoothGattServerCallback param2IBluetoothGattServerCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          if (param2ParcelUuid != null) {
            parcel1.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IBluetoothGattServerCallback != null) {
            iBinder = param2IBluetoothGattServerCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().registerServer(param2ParcelUuid, param2IBluetoothGattServerCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterServer(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().unregisterServer(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void serverConnect(int param2Int1, String param2String, boolean param2Boolean, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int2);
          boolean bool1 = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool1 && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().serverConnect(param2Int1, param2String, param2Boolean, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void serverDisconnect(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().serverDisconnect(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void serverSetPreferredPhy(int param2Int1, String param2String, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeInt(param2Int4);
          boolean bool = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().serverSetPreferredPhy(param2Int1, param2String, param2Int2, param2Int3, param2Int4);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void serverReadPhy(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().serverReadPhy(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addService(int param2Int, BluetoothGattService param2BluetoothGattService) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          if (param2BluetoothGattService != null) {
            parcel1.writeInt(1);
            param2BluetoothGattService.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(48, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().addService(param2Int, param2BluetoothGattService);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeService(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(49, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().removeService(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearServices(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(50, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().clearServices(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendResponse(int param2Int1, String param2String, int param2Int2, int param2Int3, int param2Int4, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeString(param2String);
              try {
                parcel1.writeInt(param2Int2);
                try {
                  parcel1.writeInt(param2Int3);
                  try {
                    parcel1.writeInt(param2Int4);
                    try {
                      parcel1.writeByteArray(param2ArrayOfbyte);
                      boolean bool = this.mRemote.transact(51, parcel1, parcel2, 0);
                      if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
                        IBluetoothGatt.Stub.getDefaultImpl().sendResponse(param2Int1, param2String, param2Int2, param2Int3, param2Int4, param2ArrayOfbyte);
                        parcel2.recycle();
                        parcel1.recycle();
                        return;
                      } 
                      parcel2.readException();
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public void sendNotification(int param2Int1, String param2String, int param2Int2, boolean param2Boolean, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool1 = this.mRemote.transact(52, parcel1, parcel2, 0);
          if (!bool1 && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().sendNotification(param2Int1, param2String, param2Int2, param2Boolean, param2ArrayOfbyte);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disconnectAll() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          boolean bool = this.mRemote.transact(53, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().disconnectAll();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregAll() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          boolean bool = this.mRemote.transact(54, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null) {
            IBluetoothGatt.Stub.getDefaultImpl().unregAll();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int numHwTrackFiltersAvailable() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothGatt");
          boolean bool = this.mRemote.transact(55, parcel1, parcel2, 0);
          if (!bool && IBluetoothGatt.Stub.getDefaultImpl() != null)
            return IBluetoothGatt.Stub.getDefaultImpl().numHwTrackFiltersAvailable(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IBluetoothGatt param1IBluetoothGatt) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IBluetoothGatt != null) {
          Proxy.sDefaultImpl = param1IBluetoothGatt;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IBluetoothGatt getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
