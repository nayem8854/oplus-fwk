package android.bluetooth;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IBluetoothHeadsetPhone extends IInterface {
  boolean answerCall() throws RemoteException;
  
  void cdmaSetSecondCallState(boolean paramBoolean) throws RemoteException;
  
  void cdmaSwapSecondCallState() throws RemoteException;
  
  String getNetworkOperator() throws RemoteException;
  
  String getSubscriberNumber() throws RemoteException;
  
  boolean hangupCall() throws RemoteException;
  
  boolean isCsCallInProgress() throws RemoteException;
  
  boolean isHighDefCallInProgress() throws RemoteException;
  
  boolean listCurrentCalls() throws RemoteException;
  
  boolean processChld(int paramInt) throws RemoteException;
  
  boolean queryPhoneState() throws RemoteException;
  
  boolean sendDtmf(int paramInt) throws RemoteException;
  
  void updateBtHandsfreeAfterRadioTechnologyChange() throws RemoteException;
  
  class Default implements IBluetoothHeadsetPhone {
    public boolean answerCall() throws RemoteException {
      return false;
    }
    
    public boolean hangupCall() throws RemoteException {
      return false;
    }
    
    public boolean sendDtmf(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean processChld(int param1Int) throws RemoteException {
      return false;
    }
    
    public String getNetworkOperator() throws RemoteException {
      return null;
    }
    
    public String getSubscriberNumber() throws RemoteException {
      return null;
    }
    
    public boolean listCurrentCalls() throws RemoteException {
      return false;
    }
    
    public boolean queryPhoneState() throws RemoteException {
      return false;
    }
    
    public boolean isHighDefCallInProgress() throws RemoteException {
      return false;
    }
    
    public boolean isCsCallInProgress() throws RemoteException {
      return false;
    }
    
    public void updateBtHandsfreeAfterRadioTechnologyChange() throws RemoteException {}
    
    public void cdmaSwapSecondCallState() throws RemoteException {}
    
    public void cdmaSetSecondCallState(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IBluetoothHeadsetPhone {
    private static final String DESCRIPTOR = "android.bluetooth.IBluetoothHeadsetPhone";
    
    static final int TRANSACTION_answerCall = 1;
    
    static final int TRANSACTION_cdmaSetSecondCallState = 13;
    
    static final int TRANSACTION_cdmaSwapSecondCallState = 12;
    
    static final int TRANSACTION_getNetworkOperator = 5;
    
    static final int TRANSACTION_getSubscriberNumber = 6;
    
    static final int TRANSACTION_hangupCall = 2;
    
    static final int TRANSACTION_isCsCallInProgress = 10;
    
    static final int TRANSACTION_isHighDefCallInProgress = 9;
    
    static final int TRANSACTION_listCurrentCalls = 7;
    
    static final int TRANSACTION_processChld = 4;
    
    static final int TRANSACTION_queryPhoneState = 8;
    
    static final int TRANSACTION_sendDtmf = 3;
    
    static final int TRANSACTION_updateBtHandsfreeAfterRadioTechnologyChange = 11;
    
    public Stub() {
      attachInterface(this, "android.bluetooth.IBluetoothHeadsetPhone");
    }
    
    public static IBluetoothHeadsetPhone asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.bluetooth.IBluetoothHeadsetPhone");
      if (iInterface != null && iInterface instanceof IBluetoothHeadsetPhone)
        return (IBluetoothHeadsetPhone)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 13:
          return "cdmaSetSecondCallState";
        case 12:
          return "cdmaSwapSecondCallState";
        case 11:
          return "updateBtHandsfreeAfterRadioTechnologyChange";
        case 10:
          return "isCsCallInProgress";
        case 9:
          return "isHighDefCallInProgress";
        case 8:
          return "queryPhoneState";
        case 7:
          return "listCurrentCalls";
        case 6:
          return "getSubscriberNumber";
        case 5:
          return "getNetworkOperator";
        case 4:
          return "processChld";
        case 3:
          return "sendDtmf";
        case 2:
          return "hangupCall";
        case 1:
          break;
      } 
      return "answerCall";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool3;
        int j;
        boolean bool2;
        int i;
        String str;
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 13:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothHeadsetPhone");
            if (param1Parcel1.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            cdmaSetSecondCallState(bool);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothHeadsetPhone");
            cdmaSwapSecondCallState();
            param1Parcel2.writeNoException();
            return true;
          case 11:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothHeadsetPhone");
            updateBtHandsfreeAfterRadioTechnologyChange();
            param1Parcel2.writeNoException();
            return true;
          case 10:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothHeadsetPhone");
            bool3 = isCsCallInProgress();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothHeadsetPhone");
            bool3 = isHighDefCallInProgress();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothHeadsetPhone");
            bool3 = queryPhoneState();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothHeadsetPhone");
            bool3 = listCurrentCalls();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothHeadsetPhone");
            str = getSubscriberNumber();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str);
            return true;
          case 5:
            str.enforceInterface("android.bluetooth.IBluetoothHeadsetPhone");
            str = getNetworkOperator();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str);
            return true;
          case 4:
            str.enforceInterface("android.bluetooth.IBluetoothHeadsetPhone");
            j = str.readInt();
            bool2 = processChld(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 3:
            str.enforceInterface("android.bluetooth.IBluetoothHeadsetPhone");
            i = str.readInt();
            bool1 = sendDtmf(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 2:
            str.enforceInterface("android.bluetooth.IBluetoothHeadsetPhone");
            bool1 = hangupCall();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        str.enforceInterface("android.bluetooth.IBluetoothHeadsetPhone");
        boolean bool1 = answerCall();
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("android.bluetooth.IBluetoothHeadsetPhone");
      return true;
    }
    
    private static class Proxy implements IBluetoothHeadsetPhone {
      public static IBluetoothHeadsetPhone sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.bluetooth.IBluetoothHeadsetPhone";
      }
      
      public boolean answerCall() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadsetPhone");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHeadsetPhone.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHeadsetPhone.Stub.getDefaultImpl().answerCall();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hangupCall() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadsetPhone");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHeadsetPhone.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHeadsetPhone.Stub.getDefaultImpl().hangupCall();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean sendDtmf(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadsetPhone");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHeadsetPhone.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHeadsetPhone.Stub.getDefaultImpl().sendDtmf(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean processChld(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadsetPhone");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHeadsetPhone.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHeadsetPhone.Stub.getDefaultImpl().processChld(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getNetworkOperator() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadsetPhone");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IBluetoothHeadsetPhone.Stub.getDefaultImpl() != null)
            return IBluetoothHeadsetPhone.Stub.getDefaultImpl().getNetworkOperator(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSubscriberNumber() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadsetPhone");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IBluetoothHeadsetPhone.Stub.getDefaultImpl() != null)
            return IBluetoothHeadsetPhone.Stub.getDefaultImpl().getSubscriberNumber(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean listCurrentCalls() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadsetPhone");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHeadsetPhone.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHeadsetPhone.Stub.getDefaultImpl().listCurrentCalls();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean queryPhoneState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadsetPhone");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(8, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHeadsetPhone.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHeadsetPhone.Stub.getDefaultImpl().queryPhoneState();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isHighDefCallInProgress() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadsetPhone");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHeadsetPhone.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHeadsetPhone.Stub.getDefaultImpl().isHighDefCallInProgress();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isCsCallInProgress() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadsetPhone");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHeadsetPhone.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHeadsetPhone.Stub.getDefaultImpl().isCsCallInProgress();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateBtHandsfreeAfterRadioTechnologyChange() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadsetPhone");
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IBluetoothHeadsetPhone.Stub.getDefaultImpl() != null) {
            IBluetoothHeadsetPhone.Stub.getDefaultImpl().updateBtHandsfreeAfterRadioTechnologyChange();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cdmaSwapSecondCallState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadsetPhone");
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IBluetoothHeadsetPhone.Stub.getDefaultImpl() != null) {
            IBluetoothHeadsetPhone.Stub.getDefaultImpl().cdmaSwapSecondCallState();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cdmaSetSecondCallState(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadsetPhone");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool1 && IBluetoothHeadsetPhone.Stub.getDefaultImpl() != null) {
            IBluetoothHeadsetPhone.Stub.getDefaultImpl().cdmaSetSecondCallState(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IBluetoothHeadsetPhone param1IBluetoothHeadsetPhone) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IBluetoothHeadsetPhone != null) {
          Proxy.sDefaultImpl = param1IBluetoothHeadsetPhone;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IBluetoothHeadsetPhone getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
