package android.bluetooth;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IBluetoothDun extends IInterface {
  boolean disconnect(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  List<BluetoothDevice> getConnectedDevices() throws RemoteException;
  
  int getConnectionState(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] paramArrayOfint) throws RemoteException;
  
  class Default implements IBluetoothDun {
    public boolean disconnect(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return false;
    }
    
    public int getConnectionState(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return 0;
    }
    
    public List<BluetoothDevice> getConnectedDevices() throws RemoteException {
      return null;
    }
    
    public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IBluetoothDun {
    private static final String DESCRIPTOR = "android.bluetooth.IBluetoothDun";
    
    static final int TRANSACTION_disconnect = 1;
    
    static final int TRANSACTION_getConnectedDevices = 3;
    
    static final int TRANSACTION_getConnectionState = 2;
    
    static final int TRANSACTION_getDevicesMatchingConnectionStates = 4;
    
    public Stub() {
      attachInterface(this, "android.bluetooth.IBluetoothDun");
    }
    
    public static IBluetoothDun asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.bluetooth.IBluetoothDun");
      if (iInterface != null && iInterface instanceof IBluetoothDun)
        return (IBluetoothDun)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "getDevicesMatchingConnectionStates";
          } 
          return "getConnectedDevices";
        } 
        return "getConnectionState";
      } 
      return "disconnect";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      List<BluetoothDevice> list;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.bluetooth.IBluetoothDun");
              return true;
            } 
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothDun");
            int[] arrayOfInt = param1Parcel1.createIntArray();
            list = getDevicesMatchingConnectionStates(arrayOfInt);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          } 
          list.enforceInterface("android.bluetooth.IBluetoothDun");
          list = getConnectedDevices();
          param1Parcel2.writeNoException();
          param1Parcel2.writeTypedList(list);
          return true;
        } 
        list.enforceInterface("android.bluetooth.IBluetoothDun");
        if (list.readInt() != 0) {
          BluetoothDevice bluetoothDevice = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)list);
        } else {
          list = null;
        } 
        param1Int1 = getConnectionState((BluetoothDevice)list);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(param1Int1);
        return true;
      } 
      list.enforceInterface("android.bluetooth.IBluetoothDun");
      if (list.readInt() != 0) {
        BluetoothDevice bluetoothDevice = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)list);
      } else {
        list = null;
      } 
      boolean bool = disconnect((BluetoothDevice)list);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements IBluetoothDun {
      public static IBluetoothDun sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.bluetooth.IBluetoothDun";
      }
      
      public boolean disconnect(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothDun");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothDun.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothDun.Stub.getDefaultImpl().disconnect(param2BluetoothDevice);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getConnectionState(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothDun");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IBluetoothDun.Stub.getDefaultImpl() != null)
            return IBluetoothDun.Stub.getDefaultImpl().getConnectionState(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<BluetoothDevice> getConnectedDevices() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothDun");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IBluetoothDun.Stub.getDefaultImpl() != null)
            return IBluetoothDun.Stub.getDefaultImpl().getConnectedDevices(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(BluetoothDevice.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothDun");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IBluetoothDun.Stub.getDefaultImpl() != null)
            return IBluetoothDun.Stub.getDefaultImpl().getDevicesMatchingConnectionStates(param2ArrayOfint); 
          parcel2.readException();
          return parcel2.createTypedArrayList(BluetoothDevice.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IBluetoothDun param1IBluetoothDun) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IBluetoothDun != null) {
          Proxy.sDefaultImpl = param1IBluetoothDun;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IBluetoothDun getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
