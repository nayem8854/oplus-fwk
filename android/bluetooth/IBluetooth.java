package android.bluetooth;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelUuid;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.ResultReceiver;
import java.util.List;

public interface IBluetooth extends IInterface {
  boolean cancelBondProcess(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  boolean cancelDiscovery() throws RemoteException;
  
  boolean connectAllEnabledProfiles(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  boolean createBond(BluetoothDevice paramBluetoothDevice, int paramInt, OobData paramOobData) throws RemoteException;
  
  boolean disable() throws RemoteException;
  
  boolean disconnectAllEnabledProfiles(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  boolean enable(boolean paramBoolean) throws RemoteException;
  
  boolean factoryReset() throws RemoteException;
  
  boolean fetchRemoteUuids(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  int getAdapterConnectionState() throws RemoteException;
  
  String getAddress() throws RemoteException;
  
  int getBatteryLevel(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  BluetoothClass getBluetoothClass() throws RemoteException;
  
  int getBondState(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  BluetoothDevice[] getBondedDevices() throws RemoteException;
  
  int getConnectionState(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  int getDiscoverableTimeout() throws RemoteException;
  
  long getDiscoveryEndMillis() throws RemoteException;
  
  int getIoCapability() throws RemoteException;
  
  int getLeIoCapability() throws RemoteException;
  
  int getLeMaximumAdvertisingDataLength() throws RemoteException;
  
  int getMaxConnectedAudioDevices() throws RemoteException;
  
  int getMessageAccessPermission(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  byte[] getMetadata(BluetoothDevice paramBluetoothDevice, int paramInt) throws RemoteException;
  
  List<BluetoothDevice> getMostRecentlyConnectedDevices() throws RemoteException;
  
  String getName() throws RemoteException;
  
  int getPhonebookAccessPermission(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  int getProfileConnectionState(int paramInt) throws RemoteException;
  
  String getRemoteAlias(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  int getRemoteClass(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  String getRemoteName(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  int getRemoteType(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  ParcelUuid[] getRemoteUuids(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  int getScanMode() throws RemoteException;
  
  boolean getSilenceMode(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  int getSimAccessPermission(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  IBluetoothSocketManager getSocketManager() throws RemoteException;
  
  int getSocketOpt(int paramInt1, int paramInt2, int paramInt3, byte[] paramArrayOfbyte) throws RemoteException;
  
  int getState() throws RemoteException;
  
  long getSupportedProfiles() throws RemoteException;
  
  String getTwsPlusPeerAddress(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  ParcelUuid[] getUuids() throws RemoteException;
  
  boolean isActivityAndEnergyReportingSupported() throws RemoteException;
  
  boolean isBondingInitiatedLocally(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  boolean isDiscovering() throws RemoteException;
  
  boolean isLe2MPhySupported() throws RemoteException;
  
  boolean isLeCodedPhySupported() throws RemoteException;
  
  boolean isLeExtendedAdvertisingSupported() throws RemoteException;
  
  boolean isLePeriodicAdvertisingSupported() throws RemoteException;
  
  boolean isMultiAdvertisementSupported() throws RemoteException;
  
  boolean isOffloadedFilteringSupported() throws RemoteException;
  
  boolean isOffloadedScanBatchingSupported() throws RemoteException;
  
  boolean isTwsPlusDevice(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  void onBrEdrDown() throws RemoteException;
  
  void onLeServiceUp() throws RemoteException;
  
  void registerCallback(IBluetoothCallback paramIBluetoothCallback) throws RemoteException;
  
  boolean registerMetadataListener(IBluetoothMetadataListener paramIBluetoothMetadataListener, BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  boolean removeActiveDevice(int paramInt) throws RemoteException;
  
  boolean removeBond(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  BluetoothActivityEnergyInfo reportActivityInfo() throws RemoteException;
  
  void requestActivityInfo(ResultReceiver paramResultReceiver) throws RemoteException;
  
  boolean sdpSearch(BluetoothDevice paramBluetoothDevice, ParcelUuid paramParcelUuid) throws RemoteException;
  
  boolean setActiveDevice(BluetoothDevice paramBluetoothDevice, int paramInt) throws RemoteException;
  
  boolean setBluetoothClass(BluetoothClass paramBluetoothClass) throws RemoteException;
  
  void setBondingInitiatedLocally(BluetoothDevice paramBluetoothDevice, boolean paramBoolean) throws RemoteException;
  
  boolean setDiscoverableTimeout(int paramInt) throws RemoteException;
  
  boolean setIoCapability(int paramInt) throws RemoteException;
  
  boolean setLeIoCapability(int paramInt) throws RemoteException;
  
  boolean setMessageAccessPermission(BluetoothDevice paramBluetoothDevice, int paramInt) throws RemoteException;
  
  boolean setMetadata(BluetoothDevice paramBluetoothDevice, int paramInt, byte[] paramArrayOfbyte) throws RemoteException;
  
  boolean setName(String paramString) throws RemoteException;
  
  boolean setPairingConfirmation(BluetoothDevice paramBluetoothDevice, boolean paramBoolean) throws RemoteException;
  
  boolean setPasskey(BluetoothDevice paramBluetoothDevice, boolean paramBoolean, int paramInt, byte[] paramArrayOfbyte) throws RemoteException;
  
  boolean setPhonebookAccessPermission(BluetoothDevice paramBluetoothDevice, int paramInt) throws RemoteException;
  
  boolean setPin(BluetoothDevice paramBluetoothDevice, boolean paramBoolean, int paramInt, byte[] paramArrayOfbyte) throws RemoteException;
  
  boolean setRemoteAlias(BluetoothDevice paramBluetoothDevice, String paramString) throws RemoteException;
  
  boolean setScanMode(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean setSilenceMode(BluetoothDevice paramBluetoothDevice, boolean paramBoolean) throws RemoteException;
  
  boolean setSimAccessPermission(BluetoothDevice paramBluetoothDevice, int paramInt) throws RemoteException;
  
  int setSocketOpt(int paramInt1, int paramInt2, int paramInt3, byte[] paramArrayOfbyte, int paramInt4) throws RemoteException;
  
  boolean startDiscovery(String paramString1, String paramString2) throws RemoteException;
  
  void unregisterCallback(IBluetoothCallback paramIBluetoothCallback) throws RemoteException;
  
  boolean unregisterMetadataListener(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  void updateQuietModeStatus(boolean paramBoolean) throws RemoteException;
  
  class Default implements IBluetooth {
    public int getState() throws RemoteException {
      return 0;
    }
    
    public boolean enable(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean disable() throws RemoteException {
      return false;
    }
    
    public String getAddress() throws RemoteException {
      return null;
    }
    
    public ParcelUuid[] getUuids() throws RemoteException {
      return null;
    }
    
    public boolean setName(String param1String) throws RemoteException {
      return false;
    }
    
    public String getName() throws RemoteException {
      return null;
    }
    
    public BluetoothClass getBluetoothClass() throws RemoteException {
      return null;
    }
    
    public boolean setBluetoothClass(BluetoothClass param1BluetoothClass) throws RemoteException {
      return false;
    }
    
    public int getIoCapability() throws RemoteException {
      return 0;
    }
    
    public boolean setIoCapability(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getLeIoCapability() throws RemoteException {
      return 0;
    }
    
    public boolean setLeIoCapability(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getScanMode() throws RemoteException {
      return 0;
    }
    
    public boolean setScanMode(int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public int getDiscoverableTimeout() throws RemoteException {
      return 0;
    }
    
    public boolean setDiscoverableTimeout(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean startDiscovery(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public boolean cancelDiscovery() throws RemoteException {
      return false;
    }
    
    public boolean isDiscovering() throws RemoteException {
      return false;
    }
    
    public long getDiscoveryEndMillis() throws RemoteException {
      return 0L;
    }
    
    public int getAdapterConnectionState() throws RemoteException {
      return 0;
    }
    
    public int getProfileConnectionState(int param1Int) throws RemoteException {
      return 0;
    }
    
    public BluetoothDevice[] getBondedDevices() throws RemoteException {
      return null;
    }
    
    public boolean createBond(BluetoothDevice param1BluetoothDevice, int param1Int, OobData param1OobData) throws RemoteException {
      return false;
    }
    
    public boolean cancelBondProcess(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return false;
    }
    
    public boolean removeBond(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return false;
    }
    
    public int getBondState(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return 0;
    }
    
    public boolean isBondingInitiatedLocally(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return false;
    }
    
    public void setBondingInitiatedLocally(BluetoothDevice param1BluetoothDevice, boolean param1Boolean) throws RemoteException {}
    
    public long getSupportedProfiles() throws RemoteException {
      return 0L;
    }
    
    public int getConnectionState(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return 0;
    }
    
    public String getRemoteName(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return null;
    }
    
    public int getRemoteType(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return 0;
    }
    
    public String getRemoteAlias(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return null;
    }
    
    public boolean setRemoteAlias(BluetoothDevice param1BluetoothDevice, String param1String) throws RemoteException {
      return false;
    }
    
    public int getRemoteClass(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return 0;
    }
    
    public ParcelUuid[] getRemoteUuids(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return null;
    }
    
    public boolean fetchRemoteUuids(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return false;
    }
    
    public boolean sdpSearch(BluetoothDevice param1BluetoothDevice, ParcelUuid param1ParcelUuid) throws RemoteException {
      return false;
    }
    
    public int getBatteryLevel(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return 0;
    }
    
    public int getMaxConnectedAudioDevices() throws RemoteException {
      return 0;
    }
    
    public boolean isTwsPlusDevice(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return false;
    }
    
    public String getTwsPlusPeerAddress(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return null;
    }
    
    public boolean setPin(BluetoothDevice param1BluetoothDevice, boolean param1Boolean, int param1Int, byte[] param1ArrayOfbyte) throws RemoteException {
      return false;
    }
    
    public boolean setPasskey(BluetoothDevice param1BluetoothDevice, boolean param1Boolean, int param1Int, byte[] param1ArrayOfbyte) throws RemoteException {
      return false;
    }
    
    public boolean setPairingConfirmation(BluetoothDevice param1BluetoothDevice, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public int getPhonebookAccessPermission(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return 0;
    }
    
    public boolean setSilenceMode(BluetoothDevice param1BluetoothDevice, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean getSilenceMode(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return false;
    }
    
    public boolean setPhonebookAccessPermission(BluetoothDevice param1BluetoothDevice, int param1Int) throws RemoteException {
      return false;
    }
    
    public int getMessageAccessPermission(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return 0;
    }
    
    public boolean setMessageAccessPermission(BluetoothDevice param1BluetoothDevice, int param1Int) throws RemoteException {
      return false;
    }
    
    public int getSimAccessPermission(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return 0;
    }
    
    public boolean setSimAccessPermission(BluetoothDevice param1BluetoothDevice, int param1Int) throws RemoteException {
      return false;
    }
    
    public void registerCallback(IBluetoothCallback param1IBluetoothCallback) throws RemoteException {}
    
    public void unregisterCallback(IBluetoothCallback param1IBluetoothCallback) throws RemoteException {}
    
    public IBluetoothSocketManager getSocketManager() throws RemoteException {
      return null;
    }
    
    public boolean factoryReset() throws RemoteException {
      return false;
    }
    
    public boolean isMultiAdvertisementSupported() throws RemoteException {
      return false;
    }
    
    public boolean isOffloadedFilteringSupported() throws RemoteException {
      return false;
    }
    
    public boolean isOffloadedScanBatchingSupported() throws RemoteException {
      return false;
    }
    
    public boolean isActivityAndEnergyReportingSupported() throws RemoteException {
      return false;
    }
    
    public boolean isLe2MPhySupported() throws RemoteException {
      return false;
    }
    
    public boolean isLeCodedPhySupported() throws RemoteException {
      return false;
    }
    
    public boolean isLeExtendedAdvertisingSupported() throws RemoteException {
      return false;
    }
    
    public boolean isLePeriodicAdvertisingSupported() throws RemoteException {
      return false;
    }
    
    public int getLeMaximumAdvertisingDataLength() throws RemoteException {
      return 0;
    }
    
    public BluetoothActivityEnergyInfo reportActivityInfo() throws RemoteException {
      return null;
    }
    
    public boolean registerMetadataListener(IBluetoothMetadataListener param1IBluetoothMetadataListener, BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return false;
    }
    
    public boolean unregisterMetadataListener(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return false;
    }
    
    public boolean setMetadata(BluetoothDevice param1BluetoothDevice, int param1Int, byte[] param1ArrayOfbyte) throws RemoteException {
      return false;
    }
    
    public byte[] getMetadata(BluetoothDevice param1BluetoothDevice, int param1Int) throws RemoteException {
      return null;
    }
    
    public void requestActivityInfo(ResultReceiver param1ResultReceiver) throws RemoteException {}
    
    public void onLeServiceUp() throws RemoteException {}
    
    public void updateQuietModeStatus(boolean param1Boolean) throws RemoteException {}
    
    public void onBrEdrDown() throws RemoteException {}
    
    public int setSocketOpt(int param1Int1, int param1Int2, int param1Int3, byte[] param1ArrayOfbyte, int param1Int4) throws RemoteException {
      return 0;
    }
    
    public int getSocketOpt(int param1Int1, int param1Int2, int param1Int3, byte[] param1ArrayOfbyte) throws RemoteException {
      return 0;
    }
    
    public boolean connectAllEnabledProfiles(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return false;
    }
    
    public boolean disconnectAllEnabledProfiles(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return false;
    }
    
    public boolean setActiveDevice(BluetoothDevice param1BluetoothDevice, int param1Int) throws RemoteException {
      return false;
    }
    
    public List<BluetoothDevice> getMostRecentlyConnectedDevices() throws RemoteException {
      return null;
    }
    
    public boolean removeActiveDevice(int param1Int) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IBluetooth {
    private static final String DESCRIPTOR = "android.bluetooth.IBluetooth";
    
    static final int TRANSACTION_cancelBondProcess = 26;
    
    static final int TRANSACTION_cancelDiscovery = 19;
    
    static final int TRANSACTION_connectAllEnabledProfiles = 80;
    
    static final int TRANSACTION_createBond = 25;
    
    static final int TRANSACTION_disable = 3;
    
    static final int TRANSACTION_disconnectAllEnabledProfiles = 81;
    
    static final int TRANSACTION_enable = 2;
    
    static final int TRANSACTION_factoryReset = 59;
    
    static final int TRANSACTION_fetchRemoteUuids = 39;
    
    static final int TRANSACTION_getAdapterConnectionState = 22;
    
    static final int TRANSACTION_getAddress = 4;
    
    static final int TRANSACTION_getBatteryLevel = 41;
    
    static final int TRANSACTION_getBluetoothClass = 8;
    
    static final int TRANSACTION_getBondState = 28;
    
    static final int TRANSACTION_getBondedDevices = 24;
    
    static final int TRANSACTION_getConnectionState = 32;
    
    static final int TRANSACTION_getDiscoverableTimeout = 16;
    
    static final int TRANSACTION_getDiscoveryEndMillis = 21;
    
    static final int TRANSACTION_getIoCapability = 10;
    
    static final int TRANSACTION_getLeIoCapability = 12;
    
    static final int TRANSACTION_getLeMaximumAdvertisingDataLength = 68;
    
    static final int TRANSACTION_getMaxConnectedAudioDevices = 42;
    
    static final int TRANSACTION_getMessageAccessPermission = 52;
    
    static final int TRANSACTION_getMetadata = 73;
    
    static final int TRANSACTION_getMostRecentlyConnectedDevices = 83;
    
    static final int TRANSACTION_getName = 7;
    
    static final int TRANSACTION_getPhonebookAccessPermission = 48;
    
    static final int TRANSACTION_getProfileConnectionState = 23;
    
    static final int TRANSACTION_getRemoteAlias = 35;
    
    static final int TRANSACTION_getRemoteClass = 37;
    
    static final int TRANSACTION_getRemoteName = 33;
    
    static final int TRANSACTION_getRemoteType = 34;
    
    static final int TRANSACTION_getRemoteUuids = 38;
    
    static final int TRANSACTION_getScanMode = 14;
    
    static final int TRANSACTION_getSilenceMode = 50;
    
    static final int TRANSACTION_getSimAccessPermission = 54;
    
    static final int TRANSACTION_getSocketManager = 58;
    
    static final int TRANSACTION_getSocketOpt = 79;
    
    static final int TRANSACTION_getState = 1;
    
    static final int TRANSACTION_getSupportedProfiles = 31;
    
    static final int TRANSACTION_getTwsPlusPeerAddress = 44;
    
    static final int TRANSACTION_getUuids = 5;
    
    static final int TRANSACTION_isActivityAndEnergyReportingSupported = 63;
    
    static final int TRANSACTION_isBondingInitiatedLocally = 29;
    
    static final int TRANSACTION_isDiscovering = 20;
    
    static final int TRANSACTION_isLe2MPhySupported = 64;
    
    static final int TRANSACTION_isLeCodedPhySupported = 65;
    
    static final int TRANSACTION_isLeExtendedAdvertisingSupported = 66;
    
    static final int TRANSACTION_isLePeriodicAdvertisingSupported = 67;
    
    static final int TRANSACTION_isMultiAdvertisementSupported = 60;
    
    static final int TRANSACTION_isOffloadedFilteringSupported = 61;
    
    static final int TRANSACTION_isOffloadedScanBatchingSupported = 62;
    
    static final int TRANSACTION_isTwsPlusDevice = 43;
    
    static final int TRANSACTION_onBrEdrDown = 77;
    
    static final int TRANSACTION_onLeServiceUp = 75;
    
    static final int TRANSACTION_registerCallback = 56;
    
    static final int TRANSACTION_registerMetadataListener = 70;
    
    static final int TRANSACTION_removeActiveDevice = 84;
    
    static final int TRANSACTION_removeBond = 27;
    
    static final int TRANSACTION_reportActivityInfo = 69;
    
    static final int TRANSACTION_requestActivityInfo = 74;
    
    static final int TRANSACTION_sdpSearch = 40;
    
    static final int TRANSACTION_setActiveDevice = 82;
    
    static final int TRANSACTION_setBluetoothClass = 9;
    
    static final int TRANSACTION_setBondingInitiatedLocally = 30;
    
    static final int TRANSACTION_setDiscoverableTimeout = 17;
    
    static final int TRANSACTION_setIoCapability = 11;
    
    static final int TRANSACTION_setLeIoCapability = 13;
    
    static final int TRANSACTION_setMessageAccessPermission = 53;
    
    static final int TRANSACTION_setMetadata = 72;
    
    static final int TRANSACTION_setName = 6;
    
    static final int TRANSACTION_setPairingConfirmation = 47;
    
    static final int TRANSACTION_setPasskey = 46;
    
    static final int TRANSACTION_setPhonebookAccessPermission = 51;
    
    static final int TRANSACTION_setPin = 45;
    
    static final int TRANSACTION_setRemoteAlias = 36;
    
    static final int TRANSACTION_setScanMode = 15;
    
    static final int TRANSACTION_setSilenceMode = 49;
    
    static final int TRANSACTION_setSimAccessPermission = 55;
    
    static final int TRANSACTION_setSocketOpt = 78;
    
    static final int TRANSACTION_startDiscovery = 18;
    
    static final int TRANSACTION_unregisterCallback = 57;
    
    static final int TRANSACTION_unregisterMetadataListener = 71;
    
    static final int TRANSACTION_updateQuietModeStatus = 76;
    
    public Stub() {
      attachInterface(this, "android.bluetooth.IBluetooth");
    }
    
    public static IBluetooth asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.bluetooth.IBluetooth");
      if (iInterface != null && iInterface instanceof IBluetooth)
        return (IBluetooth)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 84:
          return "removeActiveDevice";
        case 83:
          return "getMostRecentlyConnectedDevices";
        case 82:
          return "setActiveDevice";
        case 81:
          return "disconnectAllEnabledProfiles";
        case 80:
          return "connectAllEnabledProfiles";
        case 79:
          return "getSocketOpt";
        case 78:
          return "setSocketOpt";
        case 77:
          return "onBrEdrDown";
        case 76:
          return "updateQuietModeStatus";
        case 75:
          return "onLeServiceUp";
        case 74:
          return "requestActivityInfo";
        case 73:
          return "getMetadata";
        case 72:
          return "setMetadata";
        case 71:
          return "unregisterMetadataListener";
        case 70:
          return "registerMetadataListener";
        case 69:
          return "reportActivityInfo";
        case 68:
          return "getLeMaximumAdvertisingDataLength";
        case 67:
          return "isLePeriodicAdvertisingSupported";
        case 66:
          return "isLeExtendedAdvertisingSupported";
        case 65:
          return "isLeCodedPhySupported";
        case 64:
          return "isLe2MPhySupported";
        case 63:
          return "isActivityAndEnergyReportingSupported";
        case 62:
          return "isOffloadedScanBatchingSupported";
        case 61:
          return "isOffloadedFilteringSupported";
        case 60:
          return "isMultiAdvertisementSupported";
        case 59:
          return "factoryReset";
        case 58:
          return "getSocketManager";
        case 57:
          return "unregisterCallback";
        case 56:
          return "registerCallback";
        case 55:
          return "setSimAccessPermission";
        case 54:
          return "getSimAccessPermission";
        case 53:
          return "setMessageAccessPermission";
        case 52:
          return "getMessageAccessPermission";
        case 51:
          return "setPhonebookAccessPermission";
        case 50:
          return "getSilenceMode";
        case 49:
          return "setSilenceMode";
        case 48:
          return "getPhonebookAccessPermission";
        case 47:
          return "setPairingConfirmation";
        case 46:
          return "setPasskey";
        case 45:
          return "setPin";
        case 44:
          return "getTwsPlusPeerAddress";
        case 43:
          return "isTwsPlusDevice";
        case 42:
          return "getMaxConnectedAudioDevices";
        case 41:
          return "getBatteryLevel";
        case 40:
          return "sdpSearch";
        case 39:
          return "fetchRemoteUuids";
        case 38:
          return "getRemoteUuids";
        case 37:
          return "getRemoteClass";
        case 36:
          return "setRemoteAlias";
        case 35:
          return "getRemoteAlias";
        case 34:
          return "getRemoteType";
        case 33:
          return "getRemoteName";
        case 32:
          return "getConnectionState";
        case 31:
          return "getSupportedProfiles";
        case 30:
          return "setBondingInitiatedLocally";
        case 29:
          return "isBondingInitiatedLocally";
        case 28:
          return "getBondState";
        case 27:
          return "removeBond";
        case 26:
          return "cancelBondProcess";
        case 25:
          return "createBond";
        case 24:
          return "getBondedDevices";
        case 23:
          return "getProfileConnectionState";
        case 22:
          return "getAdapterConnectionState";
        case 21:
          return "getDiscoveryEndMillis";
        case 20:
          return "isDiscovering";
        case 19:
          return "cancelDiscovery";
        case 18:
          return "startDiscovery";
        case 17:
          return "setDiscoverableTimeout";
        case 16:
          return "getDiscoverableTimeout";
        case 15:
          return "setScanMode";
        case 14:
          return "getScanMode";
        case 13:
          return "setLeIoCapability";
        case 12:
          return "getLeIoCapability";
        case 11:
          return "setIoCapability";
        case 10:
          return "getIoCapability";
        case 9:
          return "setBluetoothClass";
        case 8:
          return "getBluetoothClass";
        case 7:
          return "getName";
        case 6:
          return "setName";
        case 5:
          return "getUuids";
        case 4:
          return "getAddress";
        case 3:
          return "disable";
        case 2:
          return "enable";
        case 1:
          break;
      } 
      return "getState";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool21;
        int i16;
        boolean bool20;
        int i15;
        boolean bool19;
        int i14;
        boolean bool18;
        int i13;
        boolean bool17;
        int i12;
        boolean bool16;
        int i11;
        boolean bool15;
        int i10;
        boolean bool14;
        int i9;
        boolean bool13;
        int i8;
        boolean bool12;
        int i7;
        boolean bool11;
        int i6;
        boolean bool10;
        int i5;
        boolean bool9;
        int i4;
        boolean bool8;
        int i3;
        boolean bool7;
        int i2;
        boolean bool6;
        int i1;
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        List<BluetoothDevice> list;
        byte[] arrayOfByte2;
        BluetoothActivityEnergyInfo bluetoothActivityEnergyInfo;
        IBluetoothSocketManager iBluetoothSocketManager;
        IBluetoothCallback iBluetoothCallback;
        byte[] arrayOfByte1;
        String str5;
        ParcelUuid[] arrayOfParcelUuid2;
        String str4;
        BluetoothDevice[] arrayOfBluetoothDevice;
        String str3;
        BluetoothClass bluetoothClass;
        String str2;
        ParcelUuid[] arrayOfParcelUuid1;
        String str1;
        BluetoothDevice bluetoothDevice;
        byte[] arrayOfByte3;
        IBluetoothMetadataListener iBluetoothMetadataListener;
        String str6;
        int i17, i18;
        long l;
        boolean bool22 = false, bool23 = false, bool24 = false, bool25 = false, bool26 = false, bool27 = false, bool28 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 84:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetooth");
            param1Int1 = param1Parcel1.readInt();
            bool21 = removeActiveDevice(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool21);
            return true;
          case 83:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetooth");
            list = getMostRecentlyConnectedDevices();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 82:
            list.enforceInterface("android.bluetooth.IBluetooth");
            if (list.readInt() != 0) {
              bluetoothDevice = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)list);
            } else {
              bluetoothDevice = null;
            } 
            i16 = list.readInt();
            bool20 = setActiveDevice(bluetoothDevice, i16);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool20);
            return true;
          case 81:
            list.enforceInterface("android.bluetooth.IBluetooth");
            if (list.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)list);
            } else {
              list = null;
            } 
            bool20 = disconnectAllEnabledProfiles((BluetoothDevice)list);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool20);
            return true;
          case 80:
            list.enforceInterface("android.bluetooth.IBluetooth");
            if (list.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)list);
            } else {
              list = null;
            } 
            bool20 = connectAllEnabledProfiles((BluetoothDevice)list);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool20);
            return true;
          case 79:
            list.enforceInterface("android.bluetooth.IBluetooth");
            i15 = list.readInt();
            param1Int2 = list.readInt();
            i17 = list.readInt();
            i18 = list.readInt();
            if (i18 < 0) {
              list = null;
            } else {
              arrayOfByte2 = new byte[i18];
            } 
            i15 = getSocketOpt(i15, param1Int2, i17, arrayOfByte2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i15);
            param1Parcel2.writeByteArray(arrayOfByte2);
            return true;
          case 78:
            arrayOfByte2.enforceInterface("android.bluetooth.IBluetooth");
            i15 = arrayOfByte2.readInt();
            i18 = arrayOfByte2.readInt();
            param1Int2 = arrayOfByte2.readInt();
            arrayOfByte3 = arrayOfByte2.createByteArray();
            i17 = arrayOfByte2.readInt();
            i15 = setSocketOpt(i15, i18, param1Int2, arrayOfByte3, i17);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i15);
            return true;
          case 77:
            arrayOfByte2.enforceInterface("android.bluetooth.IBluetooth");
            onBrEdrDown();
            param1Parcel2.writeNoException();
            return true;
          case 76:
            arrayOfByte2.enforceInterface("android.bluetooth.IBluetooth");
            bool25 = bool28;
            if (arrayOfByte2.readInt() != 0)
              bool25 = true; 
            updateQuietModeStatus(bool25);
            param1Parcel2.writeNoException();
            return true;
          case 75:
            arrayOfByte2.enforceInterface("android.bluetooth.IBluetooth");
            onLeServiceUp();
            param1Parcel2.writeNoException();
            return true;
          case 74:
            arrayOfByte2.enforceInterface("android.bluetooth.IBluetooth");
            if (arrayOfByte2.readInt() != 0) {
              ResultReceiver resultReceiver = (ResultReceiver)ResultReceiver.CREATOR.createFromParcel((Parcel)arrayOfByte2);
            } else {
              arrayOfByte2 = null;
            } 
            requestActivityInfo((ResultReceiver)arrayOfByte2);
            return true;
          case 73:
            arrayOfByte2.enforceInterface("android.bluetooth.IBluetooth");
            if (arrayOfByte2.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)arrayOfByte2);
            } else {
              arrayOfByte3 = null;
            } 
            i15 = arrayOfByte2.readInt();
            arrayOfByte2 = getMetadata((BluetoothDevice)arrayOfByte3, i15);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte2);
            return true;
          case 72:
            arrayOfByte2.enforceInterface("android.bluetooth.IBluetooth");
            if (arrayOfByte2.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)arrayOfByte2);
            } else {
              arrayOfByte3 = null;
            } 
            i15 = arrayOfByte2.readInt();
            arrayOfByte2 = arrayOfByte2.createByteArray();
            bool19 = setMetadata((BluetoothDevice)arrayOfByte3, i15, arrayOfByte2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool19);
            return true;
          case 71:
            arrayOfByte2.enforceInterface("android.bluetooth.IBluetooth");
            if (arrayOfByte2.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)arrayOfByte2);
            } else {
              arrayOfByte2 = null;
            } 
            bool19 = unregisterMetadataListener((BluetoothDevice)arrayOfByte2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool19);
            return true;
          case 70:
            arrayOfByte2.enforceInterface("android.bluetooth.IBluetooth");
            iBluetoothMetadataListener = IBluetoothMetadataListener.Stub.asInterface(arrayOfByte2.readStrongBinder());
            if (arrayOfByte2.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)arrayOfByte2);
            } else {
              arrayOfByte2 = null;
            } 
            bool19 = registerMetadataListener(iBluetoothMetadataListener, (BluetoothDevice)arrayOfByte2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool19);
            return true;
          case 69:
            arrayOfByte2.enforceInterface("android.bluetooth.IBluetooth");
            bluetoothActivityEnergyInfo = reportActivityInfo();
            param1Parcel2.writeNoException();
            if (bluetoothActivityEnergyInfo != null) {
              param1Parcel2.writeInt(1);
              bluetoothActivityEnergyInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 68:
            bluetoothActivityEnergyInfo.enforceInterface("android.bluetooth.IBluetooth");
            i14 = getLeMaximumAdvertisingDataLength();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i14);
            return true;
          case 67:
            bluetoothActivityEnergyInfo.enforceInterface("android.bluetooth.IBluetooth");
            bool18 = isLePeriodicAdvertisingSupported();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool18);
            return true;
          case 66:
            bluetoothActivityEnergyInfo.enforceInterface("android.bluetooth.IBluetooth");
            bool18 = isLeExtendedAdvertisingSupported();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool18);
            return true;
          case 65:
            bluetoothActivityEnergyInfo.enforceInterface("android.bluetooth.IBluetooth");
            bool18 = isLeCodedPhySupported();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool18);
            return true;
          case 64:
            bluetoothActivityEnergyInfo.enforceInterface("android.bluetooth.IBluetooth");
            bool18 = isLe2MPhySupported();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool18);
            return true;
          case 63:
            bluetoothActivityEnergyInfo.enforceInterface("android.bluetooth.IBluetooth");
            bool18 = isActivityAndEnergyReportingSupported();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool18);
            return true;
          case 62:
            bluetoothActivityEnergyInfo.enforceInterface("android.bluetooth.IBluetooth");
            bool18 = isOffloadedScanBatchingSupported();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool18);
            return true;
          case 61:
            bluetoothActivityEnergyInfo.enforceInterface("android.bluetooth.IBluetooth");
            bool18 = isOffloadedFilteringSupported();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool18);
            return true;
          case 60:
            bluetoothActivityEnergyInfo.enforceInterface("android.bluetooth.IBluetooth");
            bool18 = isMultiAdvertisementSupported();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool18);
            return true;
          case 59:
            bluetoothActivityEnergyInfo.enforceInterface("android.bluetooth.IBluetooth");
            bool18 = factoryReset();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool18);
            return true;
          case 58:
            bluetoothActivityEnergyInfo.enforceInterface("android.bluetooth.IBluetooth");
            iBluetoothSocketManager = getSocketManager();
            param1Parcel2.writeNoException();
            if (iBluetoothSocketManager != null) {
              IBinder iBinder = iBluetoothSocketManager.asBinder();
            } else {
              iBluetoothSocketManager = null;
            } 
            param1Parcel2.writeStrongBinder((IBinder)iBluetoothSocketManager);
            return true;
          case 57:
            iBluetoothSocketManager.enforceInterface("android.bluetooth.IBluetooth");
            iBluetoothCallback = IBluetoothCallback.Stub.asInterface(iBluetoothSocketManager.readStrongBinder());
            unregisterCallback(iBluetoothCallback);
            param1Parcel2.writeNoException();
            return true;
          case 56:
            iBluetoothCallback.enforceInterface("android.bluetooth.IBluetooth");
            iBluetoothCallback = IBluetoothCallback.Stub.asInterface(iBluetoothCallback.readStrongBinder());
            registerCallback(iBluetoothCallback);
            param1Parcel2.writeNoException();
            return true;
          case 55:
            iBluetoothCallback.enforceInterface("android.bluetooth.IBluetooth");
            if (iBluetoothCallback.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)iBluetoothCallback);
            } else {
              iBluetoothMetadataListener = null;
            } 
            i13 = iBluetoothCallback.readInt();
            bool17 = setSimAccessPermission((BluetoothDevice)iBluetoothMetadataListener, i13);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool17);
            return true;
          case 54:
            iBluetoothCallback.enforceInterface("android.bluetooth.IBluetooth");
            if (iBluetoothCallback.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)iBluetoothCallback);
            } else {
              iBluetoothCallback = null;
            } 
            i12 = getSimAccessPermission((BluetoothDevice)iBluetoothCallback);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i12);
            return true;
          case 53:
            iBluetoothCallback.enforceInterface("android.bluetooth.IBluetooth");
            if (iBluetoothCallback.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)iBluetoothCallback);
            } else {
              iBluetoothMetadataListener = null;
            } 
            i12 = iBluetoothCallback.readInt();
            bool16 = setMessageAccessPermission((BluetoothDevice)iBluetoothMetadataListener, i12);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 52:
            iBluetoothCallback.enforceInterface("android.bluetooth.IBluetooth");
            if (iBluetoothCallback.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)iBluetoothCallback);
            } else {
              iBluetoothCallback = null;
            } 
            i11 = getMessageAccessPermission((BluetoothDevice)iBluetoothCallback);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i11);
            return true;
          case 51:
            iBluetoothCallback.enforceInterface("android.bluetooth.IBluetooth");
            if (iBluetoothCallback.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)iBluetoothCallback);
            } else {
              iBluetoothMetadataListener = null;
            } 
            i11 = iBluetoothCallback.readInt();
            bool15 = setPhonebookAccessPermission((BluetoothDevice)iBluetoothMetadataListener, i11);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 50:
            iBluetoothCallback.enforceInterface("android.bluetooth.IBluetooth");
            if (iBluetoothCallback.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)iBluetoothCallback);
            } else {
              iBluetoothCallback = null;
            } 
            bool15 = getSilenceMode((BluetoothDevice)iBluetoothCallback);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 49:
            iBluetoothCallback.enforceInterface("android.bluetooth.IBluetooth");
            if (iBluetoothCallback.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)iBluetoothCallback);
            } else {
              iBluetoothMetadataListener = null;
            } 
            bool25 = bool22;
            if (iBluetoothCallback.readInt() != 0)
              bool25 = true; 
            bool15 = setSilenceMode((BluetoothDevice)iBluetoothMetadataListener, bool25);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 48:
            iBluetoothCallback.enforceInterface("android.bluetooth.IBluetooth");
            if (iBluetoothCallback.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)iBluetoothCallback);
            } else {
              iBluetoothCallback = null;
            } 
            i10 = getPhonebookAccessPermission((BluetoothDevice)iBluetoothCallback);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i10);
            return true;
          case 47:
            iBluetoothCallback.enforceInterface("android.bluetooth.IBluetooth");
            if (iBluetoothCallback.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)iBluetoothCallback);
            } else {
              iBluetoothMetadataListener = null;
            } 
            bool25 = bool23;
            if (iBluetoothCallback.readInt() != 0)
              bool25 = true; 
            bool14 = setPairingConfirmation((BluetoothDevice)iBluetoothMetadataListener, bool25);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool14);
            return true;
          case 46:
            iBluetoothCallback.enforceInterface("android.bluetooth.IBluetooth");
            if (iBluetoothCallback.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)iBluetoothCallback);
            } else {
              iBluetoothMetadataListener = null;
            } 
            bool25 = bool24;
            if (iBluetoothCallback.readInt() != 0)
              bool25 = true; 
            i9 = iBluetoothCallback.readInt();
            arrayOfByte1 = iBluetoothCallback.createByteArray();
            bool13 = setPasskey((BluetoothDevice)iBluetoothMetadataListener, bool25, i9, arrayOfByte1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool13);
            return true;
          case 45:
            arrayOfByte1.enforceInterface("android.bluetooth.IBluetooth");
            if (arrayOfByte1.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)arrayOfByte1);
            } else {
              iBluetoothMetadataListener = null;
            } 
            if (arrayOfByte1.readInt() != 0)
              bool25 = true; 
            i8 = arrayOfByte1.readInt();
            arrayOfByte1 = arrayOfByte1.createByteArray();
            bool12 = setPin((BluetoothDevice)iBluetoothMetadataListener, bool25, i8, arrayOfByte1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool12);
            return true;
          case 44:
            arrayOfByte1.enforceInterface("android.bluetooth.IBluetooth");
            if (arrayOfByte1.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)arrayOfByte1);
            } else {
              arrayOfByte1 = null;
            } 
            str5 = getTwsPlusPeerAddress((BluetoothDevice)arrayOfByte1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str5);
            return true;
          case 43:
            str5.enforceInterface("android.bluetooth.IBluetooth");
            if (str5.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str5);
            } else {
              str5 = null;
            } 
            bool12 = isTwsPlusDevice((BluetoothDevice)str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool12);
            return true;
          case 42:
            str5.enforceInterface("android.bluetooth.IBluetooth");
            i7 = getMaxConnectedAudioDevices();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i7);
            return true;
          case 41:
            str5.enforceInterface("android.bluetooth.IBluetooth");
            if (str5.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str5);
            } else {
              str5 = null;
            } 
            i7 = getBatteryLevel((BluetoothDevice)str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i7);
            return true;
          case 40:
            str5.enforceInterface("android.bluetooth.IBluetooth");
            if (str5.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str5);
            } else {
              iBluetoothMetadataListener = null;
            } 
            if (str5.readInt() != 0) {
              ParcelUuid parcelUuid = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel((Parcel)str5);
            } else {
              str5 = null;
            } 
            bool11 = sdpSearch((BluetoothDevice)iBluetoothMetadataListener, (ParcelUuid)str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool11);
            return true;
          case 39:
            str5.enforceInterface("android.bluetooth.IBluetooth");
            if (str5.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str5);
            } else {
              str5 = null;
            } 
            bool11 = fetchRemoteUuids((BluetoothDevice)str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool11);
            return true;
          case 38:
            str5.enforceInterface("android.bluetooth.IBluetooth");
            if (str5.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str5);
            } else {
              str5 = null;
            } 
            arrayOfParcelUuid2 = getRemoteUuids((BluetoothDevice)str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray((Parcelable[])arrayOfParcelUuid2, 1);
            return true;
          case 37:
            arrayOfParcelUuid2.enforceInterface("android.bluetooth.IBluetooth");
            if (arrayOfParcelUuid2.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)arrayOfParcelUuid2);
            } else {
              arrayOfParcelUuid2 = null;
            } 
            i6 = getRemoteClass((BluetoothDevice)arrayOfParcelUuid2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i6);
            return true;
          case 36:
            arrayOfParcelUuid2.enforceInterface("android.bluetooth.IBluetooth");
            if (arrayOfParcelUuid2.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)arrayOfParcelUuid2);
            } else {
              iBluetoothMetadataListener = null;
            } 
            str4 = arrayOfParcelUuid2.readString();
            bool10 = setRemoteAlias((BluetoothDevice)iBluetoothMetadataListener, str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool10);
            return true;
          case 35:
            str4.enforceInterface("android.bluetooth.IBluetooth");
            if (str4.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str4);
            } else {
              str4 = null;
            } 
            str4 = getRemoteAlias((BluetoothDevice)str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str4);
            return true;
          case 34:
            str4.enforceInterface("android.bluetooth.IBluetooth");
            if (str4.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str4);
            } else {
              str4 = null;
            } 
            i5 = getRemoteType((BluetoothDevice)str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i5);
            return true;
          case 33:
            str4.enforceInterface("android.bluetooth.IBluetooth");
            if (str4.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str4);
            } else {
              str4 = null;
            } 
            str4 = getRemoteName((BluetoothDevice)str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str4);
            return true;
          case 32:
            str4.enforceInterface("android.bluetooth.IBluetooth");
            if (str4.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str4);
            } else {
              str4 = null;
            } 
            i5 = getConnectionState((BluetoothDevice)str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i5);
            return true;
          case 31:
            str4.enforceInterface("android.bluetooth.IBluetooth");
            l = getSupportedProfiles();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 30:
            str4.enforceInterface("android.bluetooth.IBluetooth");
            if (str4.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str4);
            } else {
              iBluetoothMetadataListener = null;
            } 
            bool25 = bool26;
            if (str4.readInt() != 0)
              bool25 = true; 
            setBondingInitiatedLocally((BluetoothDevice)iBluetoothMetadataListener, bool25);
            param1Parcel2.writeNoException();
            return true;
          case 29:
            str4.enforceInterface("android.bluetooth.IBluetooth");
            if (str4.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str4);
            } else {
              str4 = null;
            } 
            bool9 = isBondingInitiatedLocally((BluetoothDevice)str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 28:
            str4.enforceInterface("android.bluetooth.IBluetooth");
            if (str4.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str4);
            } else {
              str4 = null;
            } 
            i4 = getBondState((BluetoothDevice)str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i4);
            return true;
          case 27:
            str4.enforceInterface("android.bluetooth.IBluetooth");
            if (str4.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str4);
            } else {
              str4 = null;
            } 
            bool8 = removeBond((BluetoothDevice)str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 26:
            str4.enforceInterface("android.bluetooth.IBluetooth");
            if (str4.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str4);
            } else {
              str4 = null;
            } 
            bool8 = cancelBondProcess((BluetoothDevice)str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 25:
            str4.enforceInterface("android.bluetooth.IBluetooth");
            if (str4.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)str4);
            } else {
              iBluetoothMetadataListener = null;
            } 
            i3 = str4.readInt();
            if (str4.readInt() != 0) {
              OobData oobData = (OobData)OobData.CREATOR.createFromParcel((Parcel)str4);
            } else {
              str4 = null;
            } 
            bool7 = createBond((BluetoothDevice)iBluetoothMetadataListener, i3, (OobData)str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 24:
            str4.enforceInterface("android.bluetooth.IBluetooth");
            arrayOfBluetoothDevice = getBondedDevices();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray((Parcelable[])arrayOfBluetoothDevice, 1);
            return true;
          case 23:
            arrayOfBluetoothDevice.enforceInterface("android.bluetooth.IBluetooth");
            i2 = arrayOfBluetoothDevice.readInt();
            i2 = getProfileConnectionState(i2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i2);
            return true;
          case 22:
            arrayOfBluetoothDevice.enforceInterface("android.bluetooth.IBluetooth");
            i2 = getAdapterConnectionState();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i2);
            return true;
          case 21:
            arrayOfBluetoothDevice.enforceInterface("android.bluetooth.IBluetooth");
            l = getDiscoveryEndMillis();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 20:
            arrayOfBluetoothDevice.enforceInterface("android.bluetooth.IBluetooth");
            bool6 = isDiscovering();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 19:
            arrayOfBluetoothDevice.enforceInterface("android.bluetooth.IBluetooth");
            bool6 = cancelDiscovery();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 18:
            arrayOfBluetoothDevice.enforceInterface("android.bluetooth.IBluetooth");
            str6 = arrayOfBluetoothDevice.readString();
            str3 = arrayOfBluetoothDevice.readString();
            bool6 = startDiscovery(str6, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 17:
            str3.enforceInterface("android.bluetooth.IBluetooth");
            i1 = str3.readInt();
            bool5 = setDiscoverableTimeout(i1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 16:
            str3.enforceInterface("android.bluetooth.IBluetooth");
            n = getDiscoverableTimeout();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(n);
            return true;
          case 15:
            str3.enforceInterface("android.bluetooth.IBluetooth");
            param1Int2 = str3.readInt();
            n = str3.readInt();
            bool4 = setScanMode(param1Int2, n);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 14:
            str3.enforceInterface("android.bluetooth.IBluetooth");
            m = getScanMode();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 13:
            str3.enforceInterface("android.bluetooth.IBluetooth");
            m = str3.readInt();
            bool3 = setLeIoCapability(m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 12:
            str3.enforceInterface("android.bluetooth.IBluetooth");
            k = getLeIoCapability();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 11:
            str3.enforceInterface("android.bluetooth.IBluetooth");
            k = str3.readInt();
            bool2 = setIoCapability(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 10:
            str3.enforceInterface("android.bluetooth.IBluetooth");
            j = getIoCapability();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 9:
            str3.enforceInterface("android.bluetooth.IBluetooth");
            if (str3.readInt() != 0) {
              BluetoothClass bluetoothClass1 = (BluetoothClass)BluetoothClass.CREATOR.createFromParcel((Parcel)str3);
            } else {
              str3 = null;
            } 
            bool1 = setBluetoothClass((BluetoothClass)str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 8:
            str3.enforceInterface("android.bluetooth.IBluetooth");
            bluetoothClass = getBluetoothClass();
            param1Parcel2.writeNoException();
            if (bluetoothClass != null) {
              param1Parcel2.writeInt(1);
              bluetoothClass.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 7:
            bluetoothClass.enforceInterface("android.bluetooth.IBluetooth");
            str2 = getName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 6:
            str2.enforceInterface("android.bluetooth.IBluetooth");
            str2 = str2.readString();
            bool1 = setName(str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 5:
            str2.enforceInterface("android.bluetooth.IBluetooth");
            arrayOfParcelUuid1 = getUuids();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray((Parcelable[])arrayOfParcelUuid1, 1);
            return true;
          case 4:
            arrayOfParcelUuid1.enforceInterface("android.bluetooth.IBluetooth");
            str1 = getAddress();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 3:
            str1.enforceInterface("android.bluetooth.IBluetooth");
            bool1 = disable();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 2:
            str1.enforceInterface("android.bluetooth.IBluetooth");
            bool25 = bool27;
            if (str1.readInt() != 0)
              bool25 = true; 
            bool1 = enable(bool25);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.bluetooth.IBluetooth");
        int i = getState();
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(i);
        return true;
      } 
      param1Parcel2.writeString("android.bluetooth.IBluetooth");
      return true;
    }
    
    private static class Proxy implements IBluetooth {
      public static IBluetooth sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.bluetooth.IBluetooth";
      }
      
      public int getState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getState(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean enable(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool1 && IBluetooth.Stub.getDefaultImpl() != null) {
            param2Boolean = IBluetooth.Stub.getDefaultImpl().enable(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean disable() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().disable();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getAddress() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getAddress(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParcelUuid[] getUuids() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getUuids(); 
          parcel2.readException();
          return (ParcelUuid[])parcel2.createTypedArray(ParcelUuid.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setName(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().setName(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public BluetoothClass getBluetoothClass() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          BluetoothClass bluetoothClass;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null) {
            bluetoothClass = IBluetooth.Stub.getDefaultImpl().getBluetoothClass();
            return bluetoothClass;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            bluetoothClass = (BluetoothClass)BluetoothClass.CREATOR.createFromParcel(parcel2);
          } else {
            bluetoothClass = null;
          } 
          return bluetoothClass;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setBluetoothClass(BluetoothClass param2BluetoothClass) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool1 = true;
          if (param2BluetoothClass != null) {
            parcel1.writeInt(1);
            param2BluetoothClass.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().setBluetoothClass(param2BluetoothClass);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getIoCapability() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getIoCapability(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setIoCapability(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(11, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().setIoCapability(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getLeIoCapability() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getLeIoCapability(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setLeIoCapability(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(13, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().setLeIoCapability(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getScanMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getScanMode(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setScanMode(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(15, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().setScanMode(param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDiscoverableTimeout() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getDiscoverableTimeout(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setDiscoverableTimeout(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(17, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().setDiscoverableTimeout(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean startDiscovery(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(18, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().startDiscovery(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean cancelDiscovery() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(19, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().cancelDiscovery();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDiscovering() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(20, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().isDiscovering();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getDiscoveryEndMillis() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getDiscoveryEndMillis(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getAdapterConnectionState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getAdapterConnectionState(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getProfileConnectionState(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null) {
            param2Int = IBluetooth.Stub.getDefaultImpl().getProfileConnectionState(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public BluetoothDevice[] getBondedDevices() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getBondedDevices(); 
          parcel2.readException();
          return (BluetoothDevice[])parcel2.createTypedArray(BluetoothDevice.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean createBond(BluetoothDevice param2BluetoothDevice, int param2Int, OobData param2OobData) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          if (param2OobData != null) {
            parcel1.writeInt(1);
            param2OobData.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().createBond(param2BluetoothDevice, param2Int, param2OobData);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean cancelBondProcess(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().cancelBondProcess(param2BluetoothDevice);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeBond(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().removeBond(param2BluetoothDevice);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getBondState(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getBondState(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isBondingInitiatedLocally(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().isBondingInitiatedLocally(param2BluetoothDevice);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setBondingInitiatedLocally(BluetoothDevice param2BluetoothDevice, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool1 && IBluetooth.Stub.getDefaultImpl() != null) {
            IBluetooth.Stub.getDefaultImpl().setBondingInitiatedLocally(param2BluetoothDevice, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getSupportedProfiles() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getSupportedProfiles(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getConnectionState(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getConnectionState(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getRemoteName(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getRemoteName(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getRemoteType(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getRemoteType(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getRemoteAlias(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getRemoteAlias(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setRemoteAlias(BluetoothDevice param2BluetoothDevice, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool2 = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().setRemoteAlias(param2BluetoothDevice, param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getRemoteClass(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getRemoteClass(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParcelUuid[] getRemoteUuids(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getRemoteUuids(param2BluetoothDevice); 
          parcel2.readException();
          return (ParcelUuid[])parcel2.createTypedArray(ParcelUuid.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean fetchRemoteUuids(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().fetchRemoteUuids(param2BluetoothDevice);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean sdpSearch(BluetoothDevice param2BluetoothDevice, ParcelUuid param2ParcelUuid) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2ParcelUuid != null) {
            parcel1.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().sdpSearch(param2BluetoothDevice, param2ParcelUuid);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getBatteryLevel(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getBatteryLevel(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getMaxConnectedAudioDevices() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getMaxConnectedAudioDevices(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isTwsPlusDevice(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().isTwsPlusDevice(param2BluetoothDevice);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getTwsPlusPeerAddress(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getTwsPlusPeerAddress(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setPin(BluetoothDevice param2BluetoothDevice, boolean param2Boolean, int param2Int, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null) {
            param2Boolean = IBluetooth.Stub.getDefaultImpl().setPin(param2BluetoothDevice, param2Boolean, param2Int, param2ArrayOfbyte);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setPasskey(BluetoothDevice param2BluetoothDevice, boolean param2Boolean, int param2Int, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null) {
            param2Boolean = IBluetooth.Stub.getDefaultImpl().setPasskey(param2BluetoothDevice, param2Boolean, param2Int, param2ArrayOfbyte);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setPairingConfirmation(BluetoothDevice param2BluetoothDevice, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool1 && IBluetooth.Stub.getDefaultImpl() != null) {
            param2Boolean = IBluetooth.Stub.getDefaultImpl().setPairingConfirmation(param2BluetoothDevice, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPhonebookAccessPermission(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(48, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getPhonebookAccessPermission(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setSilenceMode(BluetoothDevice param2BluetoothDevice, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(49, parcel1, parcel2, 0);
          if (!bool1 && IBluetooth.Stub.getDefaultImpl() != null) {
            param2Boolean = IBluetooth.Stub.getDefaultImpl().setSilenceMode(param2BluetoothDevice, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getSilenceMode(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(50, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().getSilenceMode(param2BluetoothDevice);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setPhonebookAccessPermission(BluetoothDevice param2BluetoothDevice, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(51, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().setPhonebookAccessPermission(param2BluetoothDevice, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getMessageAccessPermission(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(52, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getMessageAccessPermission(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setMessageAccessPermission(BluetoothDevice param2BluetoothDevice, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(53, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().setMessageAccessPermission(param2BluetoothDevice, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSimAccessPermission(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(54, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getSimAccessPermission(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setSimAccessPermission(BluetoothDevice param2BluetoothDevice, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(55, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().setSimAccessPermission(param2BluetoothDevice, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerCallback(IBluetoothCallback param2IBluetoothCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          if (param2IBluetoothCallback != null) {
            iBinder = param2IBluetoothCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(56, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null) {
            IBluetooth.Stub.getDefaultImpl().registerCallback(param2IBluetoothCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterCallback(IBluetoothCallback param2IBluetoothCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          if (param2IBluetoothCallback != null) {
            iBinder = param2IBluetoothCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(57, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null) {
            IBluetooth.Stub.getDefaultImpl().unregisterCallback(param2IBluetoothCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IBluetoothSocketManager getSocketManager() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = this.mRemote.transact(58, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getSocketManager(); 
          parcel2.readException();
          return IBluetoothSocketManager.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean factoryReset() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(59, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().factoryReset();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isMultiAdvertisementSupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(60, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().isMultiAdvertisementSupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isOffloadedFilteringSupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(61, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().isOffloadedFilteringSupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isOffloadedScanBatchingSupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(62, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().isOffloadedScanBatchingSupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isActivityAndEnergyReportingSupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(63, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().isActivityAndEnergyReportingSupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isLe2MPhySupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(64, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().isLe2MPhySupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isLeCodedPhySupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(65, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().isLeCodedPhySupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isLeExtendedAdvertisingSupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(66, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().isLeExtendedAdvertisingSupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isLePeriodicAdvertisingSupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(67, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().isLePeriodicAdvertisingSupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getLeMaximumAdvertisingDataLength() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = this.mRemote.transact(68, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getLeMaximumAdvertisingDataLength(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public BluetoothActivityEnergyInfo reportActivityInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          BluetoothActivityEnergyInfo bluetoothActivityEnergyInfo;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = this.mRemote.transact(69, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null) {
            bluetoothActivityEnergyInfo = IBluetooth.Stub.getDefaultImpl().reportActivityInfo();
            return bluetoothActivityEnergyInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            bluetoothActivityEnergyInfo = (BluetoothActivityEnergyInfo)BluetoothActivityEnergyInfo.CREATOR.createFromParcel(parcel2);
          } else {
            bluetoothActivityEnergyInfo = null;
          } 
          return bluetoothActivityEnergyInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean registerMetadataListener(IBluetoothMetadataListener param2IBluetoothMetadataListener, BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          if (param2IBluetoothMetadataListener != null) {
            iBinder = param2IBluetoothMetadataListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(70, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().registerMetadataListener(param2IBluetoothMetadataListener, param2BluetoothDevice);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean unregisterMetadataListener(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(71, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().unregisterMetadataListener(param2BluetoothDevice);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setMetadata(BluetoothDevice param2BluetoothDevice, int param2Int, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool2 = this.mRemote.transact(72, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().setMetadata(param2BluetoothDevice, param2Int, param2ArrayOfbyte);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getMetadata(BluetoothDevice param2BluetoothDevice, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(73, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getMetadata(param2BluetoothDevice, param2Int); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestActivityInfo(ResultReceiver param2ResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.bluetooth.IBluetooth");
          if (param2ResultReceiver != null) {
            parcel.writeInt(1);
            param2ResultReceiver.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(74, parcel, null, 1);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null) {
            IBluetooth.Stub.getDefaultImpl().requestActivityInfo(param2ResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onLeServiceUp() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = this.mRemote.transact(75, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null) {
            IBluetooth.Stub.getDefaultImpl().onLeServiceUp();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateQuietModeStatus(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(76, parcel1, parcel2, 0);
          if (!bool1 && IBluetooth.Stub.getDefaultImpl() != null) {
            IBluetooth.Stub.getDefaultImpl().updateQuietModeStatus(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onBrEdrDown() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = this.mRemote.transact(77, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null) {
            IBluetooth.Stub.getDefaultImpl().onBrEdrDown();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setSocketOpt(int param2Int1, int param2Int2, int param2Int3, byte[] param2ArrayOfbyte, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeInt(param2Int4);
          boolean bool = this.mRemote.transact(78, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null) {
            param2Int1 = IBluetooth.Stub.getDefaultImpl().setSocketOpt(param2Int1, param2Int2, param2Int3, param2ArrayOfbyte, param2Int4);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSocketOpt(int param2Int1, int param2Int2, int param2Int3, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          if (param2ArrayOfbyte == null) {
            parcel1.writeInt(-1);
          } else {
            parcel1.writeInt(param2ArrayOfbyte.length);
          } 
          boolean bool = this.mRemote.transact(79, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null) {
            param2Int1 = IBluetooth.Stub.getDefaultImpl().getSocketOpt(param2Int1, param2Int2, param2Int3, param2ArrayOfbyte);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          parcel2.readByteArray(param2ArrayOfbyte);
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean connectAllEnabledProfiles(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(80, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().connectAllEnabledProfiles(param2BluetoothDevice);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean disconnectAllEnabledProfiles(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(81, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().disconnectAllEnabledProfiles(param2BluetoothDevice);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setActiveDevice(BluetoothDevice param2BluetoothDevice, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(82, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().setActiveDevice(param2BluetoothDevice, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<BluetoothDevice> getMostRecentlyConnectedDevices() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          boolean bool = this.mRemote.transact(83, parcel1, parcel2, 0);
          if (!bool && IBluetooth.Stub.getDefaultImpl() != null)
            return IBluetooth.Stub.getDefaultImpl().getMostRecentlyConnectedDevices(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(BluetoothDevice.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeActiveDevice(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(84, parcel1, parcel2, 0);
          if (!bool2 && IBluetooth.Stub.getDefaultImpl() != null) {
            bool1 = IBluetooth.Stub.getDefaultImpl().removeActiveDevice(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IBluetooth param1IBluetooth) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IBluetooth != null) {
          Proxy.sDefaultImpl = param1IBluetooth;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IBluetooth getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
