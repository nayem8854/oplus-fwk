package android.bluetooth.le;

import android.os.Parcel;
import android.os.ParcelUuid;
import android.os.Parcelable;
import android.util.ArrayMap;
import android.util.SparseArray;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public final class AdvertiseData implements Parcelable {
  private AdvertiseData(List<ParcelUuid> paramList, SparseArray<byte[]> paramSparseArray, Map<ParcelUuid, byte[]> paramMap, boolean paramBoolean1, boolean paramBoolean2, byte[] paramArrayOfbyte) {
    this.mServiceUuids = paramList;
    this.mManufacturerSpecificData = paramSparseArray;
    this.mServiceData = paramMap;
    this.mIncludeTxPowerLevel = paramBoolean1;
    this.mIncludeDeviceName = paramBoolean2;
    this.mTransportDiscoveryData = paramArrayOfbyte;
  }
  
  public List<ParcelUuid> getServiceUuids() {
    return this.mServiceUuids;
  }
  
  public SparseArray<byte[]> getManufacturerSpecificData() {
    return this.mManufacturerSpecificData;
  }
  
  public Map<ParcelUuid, byte[]> getServiceData() {
    return this.mServiceData;
  }
  
  public boolean getIncludeTxPowerLevel() {
    return this.mIncludeTxPowerLevel;
  }
  
  public boolean getIncludeDeviceName() {
    return this.mIncludeDeviceName;
  }
  
  public byte[] getTransportDiscoveryData() {
    return this.mTransportDiscoveryData;
  }
  
  public int hashCode() {
    List<ParcelUuid> list = this.mServiceUuids;
    SparseArray<byte[]> sparseArray = this.mManufacturerSpecificData;
    Map<ParcelUuid, byte[]> map = this.mServiceData;
    boolean bool1 = this.mIncludeDeviceName;
    boolean bool2 = this.mIncludeTxPowerLevel;
    byte[] arrayOfByte = this.mTransportDiscoveryData;
    return Objects.hash(new Object[] { list, sparseArray, map, Boolean.valueOf(bool1), Boolean.valueOf(bool2), arrayOfByte });
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (Objects.equals(this.mServiceUuids, ((AdvertiseData)paramObject).mServiceUuids)) {
      SparseArray<byte[]> sparseArray1 = this.mManufacturerSpecificData, sparseArray2 = ((AdvertiseData)paramObject).mManufacturerSpecificData;
      if (BluetoothLeUtils.equals(sparseArray1, sparseArray2)) {
        Map<ParcelUuid, byte[]> map2 = this.mServiceData, map1 = ((AdvertiseData)paramObject).mServiceData;
        if (BluetoothLeUtils.equals(map2, map1) && this.mIncludeDeviceName == ((AdvertiseData)paramObject).mIncludeDeviceName && this.mIncludeTxPowerLevel == ((AdvertiseData)paramObject).mIncludeTxPowerLevel) {
          byte[] arrayOfByte = this.mTransportDiscoveryData;
          paramObject = ((AdvertiseData)paramObject).mTransportDiscoveryData;
          if (BluetoothLeUtils.equals(arrayOfByte, (byte[])paramObject))
            return null; 
        } 
      } 
    } 
    return false;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("AdvertiseData [mServiceUuids=");
    stringBuilder.append(this.mServiceUuids);
    stringBuilder.append(", mManufacturerSpecificData=");
    SparseArray<byte[]> sparseArray = this.mManufacturerSpecificData;
    stringBuilder.append(BluetoothLeUtils.toString(sparseArray));
    stringBuilder.append(", mServiceData=");
    Map<ParcelUuid, byte[]> map = this.mServiceData;
    stringBuilder.append(BluetoothLeUtils.toString(map));
    stringBuilder.append(", mIncludeTxPowerLevel=");
    stringBuilder.append(this.mIncludeTxPowerLevel);
    stringBuilder.append(", mIncludeDeviceName=");
    stringBuilder.append(this.mIncludeDeviceName);
    stringBuilder.append(", mTransportDiscoveryData=");
    byte[] arrayOfByte = this.mTransportDiscoveryData;
    stringBuilder.append(BluetoothLeUtils.toString(arrayOfByte));
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    List<ParcelUuid> list = this.mServiceUuids;
    paramParcel.writeTypedArray((Parcelable[])list.<ParcelUuid>toArray(new ParcelUuid[list.size()]), paramInt);
    paramParcel.writeInt(this.mManufacturerSpecificData.size());
    for (byte b = 0; b < this.mManufacturerSpecificData.size(); b++) {
      paramParcel.writeInt(this.mManufacturerSpecificData.keyAt(b));
      paramParcel.writeByteArray((byte[])this.mManufacturerSpecificData.valueAt(b));
    } 
    paramParcel.writeInt(this.mServiceData.size());
    for (ParcelUuid parcelUuid : this.mServiceData.keySet()) {
      paramParcel.writeTypedObject((Parcelable)parcelUuid, paramInt);
      paramParcel.writeByteArray(this.mServiceData.get(parcelUuid));
    } 
    paramParcel.writeByte((byte)getIncludeTxPowerLevel());
    paramParcel.writeByte((byte)getIncludeDeviceName());
    byte[] arrayOfByte = this.mTransportDiscoveryData;
    if (arrayOfByte != null) {
      paramInt = arrayOfByte.length;
    } else {
      paramInt = 0;
    } 
    paramParcel.writeInt(paramInt);
    arrayOfByte = this.mTransportDiscoveryData;
    if (arrayOfByte != null)
      paramParcel.writeByteArray(arrayOfByte); 
  }
  
  public static final Parcelable.Creator<AdvertiseData> CREATOR = new Parcelable.Creator<AdvertiseData>() {
      public AdvertiseData[] newArray(int param1Int) {
        return new AdvertiseData[param1Int];
      }
      
      public AdvertiseData createFromParcel(Parcel param1Parcel) {
        AdvertiseData.Builder builder = new AdvertiseData.Builder();
        ArrayList arrayList = param1Parcel.createTypedArrayList(ParcelUuid.CREATOR);
        for (ParcelUuid parcelUuid : arrayList)
          builder.addServiceUuid(parcelUuid); 
        int i = param1Parcel.readInt();
        int j;
        for (j = 0; j < i; j++) {
          int k = param1Parcel.readInt();
          byte[] arrayOfByte = param1Parcel.createByteArray();
          builder.addManufacturerData(k, arrayOfByte);
        } 
        i = param1Parcel.readInt();
        for (j = 0; j < i; j++) {
          ParcelUuid parcelUuid = (ParcelUuid)param1Parcel.readTypedObject(ParcelUuid.CREATOR);
          byte[] arrayOfByte = param1Parcel.createByteArray();
          builder.addServiceData(parcelUuid, arrayOfByte);
        } 
        j = param1Parcel.readByte();
        boolean bool1 = false;
        if (j == 1) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        builder.setIncludeTxPowerLevel(bool2);
        boolean bool2 = bool1;
        if (param1Parcel.readByte() == 1)
          bool2 = true; 
        builder.setIncludeDeviceName(bool2);
        j = param1Parcel.readInt();
        if (j > 0) {
          byte[] arrayOfByte = param1Parcel.createByteArray();
          builder.addTransportDiscoveryData(arrayOfByte);
        } 
        return builder.build();
      }
    };
  
  private final boolean mIncludeDeviceName;
  
  private final boolean mIncludeTxPowerLevel;
  
  private final SparseArray<byte[]> mManufacturerSpecificData;
  
  private final Map<ParcelUuid, byte[]> mServiceData;
  
  private final List<ParcelUuid> mServiceUuids;
  
  private final byte[] mTransportDiscoveryData;
  
  class Builder {
    private boolean mIncludeDeviceName;
    
    private boolean mIncludeTxPowerLevel;
    
    private SparseArray<byte[]> mManufacturerSpecificData;
    
    private Map<ParcelUuid, byte[]> mServiceData;
    
    private List<ParcelUuid> mServiceUuids;
    
    private byte[] mTransportDiscoveryData;
    
    public Builder() {
      this.mServiceUuids = new ArrayList<>();
      this.mManufacturerSpecificData = new SparseArray();
      this.mServiceData = (Map<ParcelUuid, byte[]>)new ArrayMap();
    }
    
    public Builder addServiceUuid(ParcelUuid param1ParcelUuid) {
      if (param1ParcelUuid != null) {
        this.mServiceUuids.add(param1ParcelUuid);
        return this;
      } 
      throw new IllegalArgumentException("serivceUuids are null");
    }
    
    public Builder addServiceData(ParcelUuid param1ParcelUuid, byte[] param1ArrayOfbyte) {
      if (param1ParcelUuid != null && param1ArrayOfbyte != null) {
        this.mServiceData.put(param1ParcelUuid, param1ArrayOfbyte);
        return this;
      } 
      throw new IllegalArgumentException("serviceDataUuid or serviceDataUuid is null");
    }
    
    public Builder addManufacturerData(int param1Int, byte[] param1ArrayOfbyte) {
      if (param1Int >= 0) {
        if (param1ArrayOfbyte != null) {
          this.mManufacturerSpecificData.put(param1Int, param1ArrayOfbyte);
          return this;
        } 
        throw new IllegalArgumentException("manufacturerSpecificData is null");
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("invalid manufacturerId - ");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public Builder setIncludeTxPowerLevel(boolean param1Boolean) {
      this.mIncludeTxPowerLevel = param1Boolean;
      return this;
    }
    
    public Builder setIncludeDeviceName(boolean param1Boolean) {
      this.mIncludeDeviceName = param1Boolean;
      return this;
    }
    
    public Builder addTransportDiscoveryData(byte[] param1ArrayOfbyte) {
      if (param1ArrayOfbyte != null && param1ArrayOfbyte.length != 0) {
        this.mTransportDiscoveryData = param1ArrayOfbyte;
        return this;
      } 
      throw new IllegalArgumentException("transportDiscoveryData is null");
    }
    
    public AdvertiseData build() {
      return new AdvertiseData(this.mServiceUuids, this.mManufacturerSpecificData, this.mServiceData, this.mIncludeTxPowerLevel, this.mIncludeDeviceName, this.mTransportDiscoveryData);
    }
  }
}
