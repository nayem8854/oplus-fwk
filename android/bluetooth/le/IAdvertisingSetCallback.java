package android.bluetooth.le;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAdvertisingSetCallback extends IInterface {
  void onAdvertisingDataSet(int paramInt1, int paramInt2) throws RemoteException;
  
  void onAdvertisingEnabled(int paramInt1, boolean paramBoolean, int paramInt2) throws RemoteException;
  
  void onAdvertisingParametersUpdated(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void onAdvertisingSetStarted(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void onAdvertisingSetStopped(int paramInt) throws RemoteException;
  
  void onOwnAddressRead(int paramInt1, int paramInt2, String paramString) throws RemoteException;
  
  void onPeriodicAdvertisingDataSet(int paramInt1, int paramInt2) throws RemoteException;
  
  void onPeriodicAdvertisingEnabled(int paramInt1, boolean paramBoolean, int paramInt2) throws RemoteException;
  
  void onPeriodicAdvertisingParametersUpdated(int paramInt1, int paramInt2) throws RemoteException;
  
  void onScanResponseDataSet(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements IAdvertisingSetCallback {
    public void onAdvertisingSetStarted(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void onOwnAddressRead(int param1Int1, int param1Int2, String param1String) throws RemoteException {}
    
    public void onAdvertisingSetStopped(int param1Int) throws RemoteException {}
    
    public void onAdvertisingEnabled(int param1Int1, boolean param1Boolean, int param1Int2) throws RemoteException {}
    
    public void onAdvertisingDataSet(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onScanResponseDataSet(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onAdvertisingParametersUpdated(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void onPeriodicAdvertisingParametersUpdated(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onPeriodicAdvertisingDataSet(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onPeriodicAdvertisingEnabled(int param1Int1, boolean param1Boolean, int param1Int2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAdvertisingSetCallback {
    private static final String DESCRIPTOR = "android.bluetooth.le.IAdvertisingSetCallback";
    
    static final int TRANSACTION_onAdvertisingDataSet = 5;
    
    static final int TRANSACTION_onAdvertisingEnabled = 4;
    
    static final int TRANSACTION_onAdvertisingParametersUpdated = 7;
    
    static final int TRANSACTION_onAdvertisingSetStarted = 1;
    
    static final int TRANSACTION_onAdvertisingSetStopped = 3;
    
    static final int TRANSACTION_onOwnAddressRead = 2;
    
    static final int TRANSACTION_onPeriodicAdvertisingDataSet = 9;
    
    static final int TRANSACTION_onPeriodicAdvertisingEnabled = 10;
    
    static final int TRANSACTION_onPeriodicAdvertisingParametersUpdated = 8;
    
    static final int TRANSACTION_onScanResponseDataSet = 6;
    
    public Stub() {
      attachInterface(this, "android.bluetooth.le.IAdvertisingSetCallback");
    }
    
    public static IAdvertisingSetCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.bluetooth.le.IAdvertisingSetCallback");
      if (iInterface != null && iInterface instanceof IAdvertisingSetCallback)
        return (IAdvertisingSetCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 10:
          return "onPeriodicAdvertisingEnabled";
        case 9:
          return "onPeriodicAdvertisingDataSet";
        case 8:
          return "onPeriodicAdvertisingParametersUpdated";
        case 7:
          return "onAdvertisingParametersUpdated";
        case 6:
          return "onScanResponseDataSet";
        case 5:
          return "onAdvertisingDataSet";
        case 4:
          return "onAdvertisingEnabled";
        case 3:
          return "onAdvertisingSetStopped";
        case 2:
          return "onOwnAddressRead";
        case 1:
          break;
      } 
      return "onAdvertisingSetStarted";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String str;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 10:
            param1Parcel1.enforceInterface("android.bluetooth.le.IAdvertisingSetCallback");
            param1Int1 = param1Parcel1.readInt();
            bool1 = bool2;
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            param1Int2 = param1Parcel1.readInt();
            onPeriodicAdvertisingEnabled(param1Int1, bool1, param1Int2);
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.bluetooth.le.IAdvertisingSetCallback");
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            onPeriodicAdvertisingDataSet(param1Int2, param1Int1);
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.bluetooth.le.IAdvertisingSetCallback");
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            onPeriodicAdvertisingParametersUpdated(param1Int2, param1Int1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.bluetooth.le.IAdvertisingSetCallback");
            i = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            onAdvertisingParametersUpdated(i, param1Int1, param1Int2);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.bluetooth.le.IAdvertisingSetCallback");
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            onScanResponseDataSet(param1Int1, param1Int2);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.bluetooth.le.IAdvertisingSetCallback");
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            onAdvertisingDataSet(param1Int1, param1Int2);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.bluetooth.le.IAdvertisingSetCallback");
            param1Int2 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            param1Int1 = param1Parcel1.readInt();
            onAdvertisingEnabled(param1Int2, bool1, param1Int1);
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.bluetooth.le.IAdvertisingSetCallback");
            param1Int1 = param1Parcel1.readInt();
            onAdvertisingSetStopped(param1Int1);
            return true;
          case 2:
            param1Parcel1.enforceInterface("android.bluetooth.le.IAdvertisingSetCallback");
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            str = param1Parcel1.readString();
            onOwnAddressRead(param1Int2, param1Int1, str);
            return true;
          case 1:
            break;
        } 
        str.enforceInterface("android.bluetooth.le.IAdvertisingSetCallback");
        param1Int1 = str.readInt();
        param1Int2 = str.readInt();
        int i = str.readInt();
        onAdvertisingSetStarted(param1Int1, param1Int2, i);
        return true;
      } 
      param1Parcel2.writeString("android.bluetooth.le.IAdvertisingSetCallback");
      return true;
    }
    
    private static class Proxy implements IAdvertisingSetCallback {
      public static IAdvertisingSetCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.bluetooth.le.IAdvertisingSetCallback";
      }
      
      public void onAdvertisingSetStarted(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.bluetooth.le.IAdvertisingSetCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IAdvertisingSetCallback.Stub.getDefaultImpl() != null) {
            IAdvertisingSetCallback.Stub.getDefaultImpl().onAdvertisingSetStarted(param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onOwnAddressRead(int param2Int1, int param2Int2, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.bluetooth.le.IAdvertisingSetCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IAdvertisingSetCallback.Stub.getDefaultImpl() != null) {
            IAdvertisingSetCallback.Stub.getDefaultImpl().onOwnAddressRead(param2Int1, param2Int2, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAdvertisingSetStopped(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.bluetooth.le.IAdvertisingSetCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IAdvertisingSetCallback.Stub.getDefaultImpl() != null) {
            IAdvertisingSetCallback.Stub.getDefaultImpl().onAdvertisingSetStopped(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAdvertisingEnabled(int param2Int1, boolean param2Boolean, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.bluetooth.le.IAdvertisingSetCallback");
          parcel.writeInt(param2Int1);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeInt(param2Int2);
          boolean bool1 = this.mRemote.transact(4, parcel, null, 1);
          if (!bool1 && IAdvertisingSetCallback.Stub.getDefaultImpl() != null) {
            IAdvertisingSetCallback.Stub.getDefaultImpl().onAdvertisingEnabled(param2Int1, param2Boolean, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAdvertisingDataSet(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.bluetooth.le.IAdvertisingSetCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IAdvertisingSetCallback.Stub.getDefaultImpl() != null) {
            IAdvertisingSetCallback.Stub.getDefaultImpl().onAdvertisingDataSet(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onScanResponseDataSet(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.bluetooth.le.IAdvertisingSetCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IAdvertisingSetCallback.Stub.getDefaultImpl() != null) {
            IAdvertisingSetCallback.Stub.getDefaultImpl().onScanResponseDataSet(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAdvertisingParametersUpdated(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.bluetooth.le.IAdvertisingSetCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IAdvertisingSetCallback.Stub.getDefaultImpl() != null) {
            IAdvertisingSetCallback.Stub.getDefaultImpl().onAdvertisingParametersUpdated(param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPeriodicAdvertisingParametersUpdated(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.bluetooth.le.IAdvertisingSetCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IAdvertisingSetCallback.Stub.getDefaultImpl() != null) {
            IAdvertisingSetCallback.Stub.getDefaultImpl().onPeriodicAdvertisingParametersUpdated(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPeriodicAdvertisingDataSet(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.bluetooth.le.IAdvertisingSetCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IAdvertisingSetCallback.Stub.getDefaultImpl() != null) {
            IAdvertisingSetCallback.Stub.getDefaultImpl().onPeriodicAdvertisingDataSet(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPeriodicAdvertisingEnabled(int param2Int1, boolean param2Boolean, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.bluetooth.le.IAdvertisingSetCallback");
          parcel.writeInt(param2Int1);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeInt(param2Int2);
          boolean bool1 = this.mRemote.transact(10, parcel, null, 1);
          if (!bool1 && IAdvertisingSetCallback.Stub.getDefaultImpl() != null) {
            IAdvertisingSetCallback.Stub.getDefaultImpl().onPeriodicAdvertisingEnabled(param2Int1, param2Boolean, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAdvertisingSetCallback param1IAdvertisingSetCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAdvertisingSetCallback != null) {
          Proxy.sDefaultImpl = param1IAdvertisingSetCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAdvertisingSetCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
