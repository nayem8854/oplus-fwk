package android.bluetooth.le;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.ParcelUuid;
import android.os.Parcelable;
import com.android.internal.util.BitUtils;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public final class ScanFilter implements Parcelable {
  public static final Parcelable.Creator<ScanFilter> CREATOR;
  
  public static final ScanFilter EMPTY = (new Builder()).build();
  
  public static final int WIFI_ALLIANCE_ORG_ID = 2;
  
  private final String mDeviceAddress;
  
  private final String mDeviceName;
  
  private final byte[] mManufacturerData;
  
  private final byte[] mManufacturerDataMask;
  
  private final int mManufacturerId;
  
  private final int mOrgId;
  
  private final byte[] mServiceData;
  
  private final byte[] mServiceDataMask;
  
  private final ParcelUuid mServiceDataUuid;
  
  private final ParcelUuid mServiceSolicitationUuid;
  
  private final ParcelUuid mServiceSolicitationUuidMask;
  
  private final ParcelUuid mServiceUuid;
  
  private final ParcelUuid mServiceUuidMask;
  
  private final int mTDSFlags;
  
  private final int mTDSFlagsMask;
  
  private final byte[] mWifiNANHash;
  
  private ScanFilter(String paramString1, String paramString2, ParcelUuid paramParcelUuid1, ParcelUuid paramParcelUuid2, ParcelUuid paramParcelUuid3, ParcelUuid paramParcelUuid4, ParcelUuid paramParcelUuid5, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, int paramInt1, byte[] paramArrayOfbyte3, byte[] paramArrayOfbyte4, int paramInt2, int paramInt3, int paramInt4, byte[] paramArrayOfbyte5) {
    this.mDeviceName = paramString1;
    this.mServiceUuid = paramParcelUuid1;
    this.mServiceUuidMask = paramParcelUuid2;
    this.mServiceSolicitationUuid = paramParcelUuid3;
    this.mServiceSolicitationUuidMask = paramParcelUuid4;
    this.mDeviceAddress = paramString2;
    this.mServiceDataUuid = paramParcelUuid5;
    this.mServiceData = paramArrayOfbyte1;
    this.mServiceDataMask = paramArrayOfbyte2;
    this.mManufacturerId = paramInt1;
    this.mManufacturerData = paramArrayOfbyte3;
    this.mManufacturerDataMask = paramArrayOfbyte4;
    this.mOrgId = paramInt2;
    this.mTDSFlags = paramInt3;
    this.mTDSFlagsMask = paramInt4;
    this.mWifiNANHash = paramArrayOfbyte5;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    boolean bool2;
    String str = this.mDeviceName;
    boolean bool1 = false;
    if (str == null) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    paramParcel.writeInt(bool2);
    str = this.mDeviceName;
    if (str != null)
      paramParcel.writeString(str); 
    if (this.mDeviceAddress == null) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    paramParcel.writeInt(bool2);
    str = this.mDeviceAddress;
    if (str != null)
      paramParcel.writeString(str); 
    if (this.mServiceUuid == null) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    paramParcel.writeInt(bool2);
    ParcelUuid parcelUuid = this.mServiceUuid;
    if (parcelUuid != null) {
      paramParcel.writeParcelable((Parcelable)parcelUuid, paramInt);
      if (this.mServiceUuidMask == null) {
        bool2 = false;
      } else {
        bool2 = true;
      } 
      paramParcel.writeInt(bool2);
      parcelUuid = this.mServiceUuidMask;
      if (parcelUuid != null)
        paramParcel.writeParcelable((Parcelable)parcelUuid, paramInt); 
    } 
    if (this.mServiceSolicitationUuid == null) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    paramParcel.writeInt(bool2);
    parcelUuid = this.mServiceSolicitationUuid;
    if (parcelUuid != null) {
      paramParcel.writeParcelable((Parcelable)parcelUuid, paramInt);
      if (this.mServiceSolicitationUuidMask == null) {
        bool2 = false;
      } else {
        bool2 = true;
      } 
      paramParcel.writeInt(bool2);
      parcelUuid = this.mServiceSolicitationUuidMask;
      if (parcelUuid != null)
        paramParcel.writeParcelable((Parcelable)parcelUuid, paramInt); 
    } 
    if (this.mServiceDataUuid == null) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    paramParcel.writeInt(bool2);
    parcelUuid = this.mServiceDataUuid;
    if (parcelUuid != null) {
      paramParcel.writeParcelable((Parcelable)parcelUuid, paramInt);
      if (this.mServiceData == null) {
        paramInt = 0;
      } else {
        paramInt = 1;
      } 
      paramParcel.writeInt(paramInt);
      byte[] arrayOfByte1 = this.mServiceData;
      if (arrayOfByte1 != null) {
        paramParcel.writeInt(arrayOfByte1.length);
        paramParcel.writeByteArray(this.mServiceData);
        if (this.mServiceDataMask == null) {
          paramInt = 0;
        } else {
          paramInt = 1;
        } 
        paramParcel.writeInt(paramInt);
        arrayOfByte1 = this.mServiceDataMask;
        if (arrayOfByte1 != null) {
          paramParcel.writeInt(arrayOfByte1.length);
          paramParcel.writeByteArray(this.mServiceDataMask);
        } 
      } 
    } 
    paramParcel.writeInt(this.mManufacturerId);
    if (this.mManufacturerData == null) {
      paramInt = 0;
    } else {
      paramInt = 1;
    } 
    paramParcel.writeInt(paramInt);
    byte[] arrayOfByte = this.mManufacturerData;
    if (arrayOfByte != null) {
      paramParcel.writeInt(arrayOfByte.length);
      paramParcel.writeByteArray(this.mManufacturerData);
      if (this.mManufacturerDataMask == null) {
        paramInt = 0;
      } else {
        paramInt = 1;
      } 
      paramParcel.writeInt(paramInt);
      arrayOfByte = this.mManufacturerDataMask;
      if (arrayOfByte != null) {
        paramParcel.writeInt(arrayOfByte.length);
        paramParcel.writeByteArray(this.mManufacturerDataMask);
      } 
    } 
    paramParcel.writeInt(this.mOrgId);
    if (this.mOrgId < 0) {
      paramInt = 0;
    } else {
      paramInt = 1;
    } 
    paramParcel.writeInt(paramInt);
    if (this.mOrgId >= 0) {
      paramParcel.writeInt(this.mTDSFlags);
      paramParcel.writeInt(this.mTDSFlagsMask);
      if (this.mWifiNANHash == null) {
        paramInt = bool1;
      } else {
        paramInt = 1;
      } 
      paramParcel.writeInt(paramInt);
      arrayOfByte = this.mWifiNANHash;
      if (arrayOfByte != null) {
        paramParcel.writeInt(arrayOfByte.length);
        paramParcel.writeByteArray(this.mWifiNANHash);
      } 
    } 
  }
  
  static {
    CREATOR = new Parcelable.Creator<ScanFilter>() {
        public ScanFilter[] newArray(int param1Int) {
          return new ScanFilter[param1Int];
        }
        
        public ScanFilter createFromParcel(Parcel param1Parcel) {
          ScanFilter.Builder builder = new ScanFilter.Builder();
          if (param1Parcel.readInt() == 1)
            builder.setDeviceName(param1Parcel.readString()); 
          if (param1Parcel.readInt() == 1)
            builder.setDeviceAddress(param1Parcel.readString()); 
          if (param1Parcel.readInt() == 1) {
            ParcelUuid parcelUuid = (ParcelUuid)param1Parcel.readParcelable(ParcelUuid.class.getClassLoader());
            builder.setServiceUuid(parcelUuid);
            if (param1Parcel.readInt() == 1) {
              ClassLoader classLoader = ParcelUuid.class.getClassLoader();
              ParcelUuid parcelUuid1 = (ParcelUuid)param1Parcel.readParcelable(classLoader);
              builder.setServiceUuid(parcelUuid, parcelUuid1);
            } 
          } 
          if (param1Parcel.readInt() == 1) {
            ClassLoader classLoader = ParcelUuid.class.getClassLoader();
            ParcelUuid parcelUuid = (ParcelUuid)param1Parcel.readParcelable(classLoader);
            builder.setServiceSolicitationUuid(parcelUuid);
            if (param1Parcel.readInt() == 1) {
              ClassLoader classLoader1 = ParcelUuid.class.getClassLoader();
              ParcelUuid parcelUuid1 = (ParcelUuid)param1Parcel.readParcelable(classLoader1);
              builder.setServiceSolicitationUuid(parcelUuid, parcelUuid1);
            } 
          } 
          if (param1Parcel.readInt() == 1) {
            ParcelUuid parcelUuid = (ParcelUuid)param1Parcel.readParcelable(ParcelUuid.class.getClassLoader());
            if (param1Parcel.readInt() == 1) {
              int k = param1Parcel.readInt();
              byte[] arrayOfByte = new byte[k];
              param1Parcel.readByteArray(arrayOfByte);
              if (param1Parcel.readInt() == 0) {
                builder.setServiceData(parcelUuid, arrayOfByte);
              } else {
                k = param1Parcel.readInt();
                byte[] arrayOfByte1 = new byte[k];
                param1Parcel.readByteArray(arrayOfByte1);
                builder.setServiceData(parcelUuid, arrayOfByte, arrayOfByte1);
              } 
            } 
          } 
          int i = param1Parcel.readInt();
          if (param1Parcel.readInt() == 1) {
            int k = param1Parcel.readInt();
            byte[] arrayOfByte = new byte[k];
            param1Parcel.readByteArray(arrayOfByte);
            if (param1Parcel.readInt() == 0) {
              builder.setManufacturerData(i, arrayOfByte);
            } else {
              k = param1Parcel.readInt();
              byte[] arrayOfByte1 = new byte[k];
              param1Parcel.readByteArray(arrayOfByte1);
              builder.setManufacturerData(i, arrayOfByte, arrayOfByte1);
            } 
          } 
          int j = param1Parcel.readInt();
          if (param1Parcel.readInt() == 1) {
            int k = param1Parcel.readInt();
            int m = param1Parcel.readInt();
            if (param1Parcel.readInt() == 1) {
              i = param1Parcel.readInt();
              byte[] arrayOfByte = new byte[i];
              param1Parcel.readByteArray(arrayOfByte);
              builder.setTransportDiscoveryData(j, k, m, arrayOfByte);
            } else {
              builder.setTransportDiscoveryData(j, k, m, null);
            } 
          } 
          return builder.build();
        }
      };
  }
  
  public String getDeviceName() {
    return this.mDeviceName;
  }
  
  public ParcelUuid getServiceUuid() {
    return this.mServiceUuid;
  }
  
  public ParcelUuid getServiceUuidMask() {
    return this.mServiceUuidMask;
  }
  
  public ParcelUuid getServiceSolicitationUuid() {
    return this.mServiceSolicitationUuid;
  }
  
  public ParcelUuid getServiceSolicitationUuidMask() {
    return this.mServiceSolicitationUuidMask;
  }
  
  public String getDeviceAddress() {
    return this.mDeviceAddress;
  }
  
  public byte[] getServiceData() {
    return this.mServiceData;
  }
  
  public byte[] getServiceDataMask() {
    return this.mServiceDataMask;
  }
  
  public ParcelUuid getServiceDataUuid() {
    return this.mServiceDataUuid;
  }
  
  public int getManufacturerId() {
    return this.mManufacturerId;
  }
  
  public byte[] getManufacturerData() {
    return this.mManufacturerData;
  }
  
  public byte[] getManufacturerDataMask() {
    return this.mManufacturerDataMask;
  }
  
  public int getOrgId() {
    return this.mOrgId;
  }
  
  public int getTDSFlags() {
    return this.mTDSFlags;
  }
  
  public int getTDSFlagsMask() {
    return this.mTDSFlagsMask;
  }
  
  public byte[] getWifiNANHash() {
    return this.mWifiNANHash;
  }
  
  public boolean matches(ScanResult paramScanResult) {
    if (paramScanResult == null)
      return false; 
    BluetoothDevice bluetoothDevice = paramScanResult.getDevice();
    String str = this.mDeviceAddress;
    if (str != null && (bluetoothDevice == null || 
      !str.equals(bluetoothDevice.getAddress())))
      return false; 
    ScanRecord scanRecord = paramScanResult.getScanRecord();
    if (scanRecord == null && (this.mDeviceName != null || this.mServiceUuid != null || this.mManufacturerData != null || this.mServiceData != null || this.mServiceSolicitationUuid != null))
      return false; 
    str = this.mDeviceName;
    if (str != null && !str.equals(scanRecord.getDeviceName()))
      return false; 
    ParcelUuid parcelUuid1 = this.mServiceUuid;
    if (parcelUuid1 != null) {
      ParcelUuid parcelUuid = this.mServiceUuidMask;
      List<ParcelUuid> list = scanRecord.getServiceUuids();
      if (!matchesServiceUuids(parcelUuid1, parcelUuid, list))
        return false; 
    } 
    ParcelUuid parcelUuid2 = this.mServiceSolicitationUuid;
    if (parcelUuid2 != null) {
      ParcelUuid parcelUuid = this.mServiceSolicitationUuidMask;
      List<ParcelUuid> list = scanRecord.getServiceSolicitationUuids();
      if (!matchesServiceSolicitationUuids(parcelUuid2, parcelUuid, list))
        return false; 
    } 
    parcelUuid2 = this.mServiceDataUuid;
    if (parcelUuid2 != null) {
      byte[] arrayOfByte1 = this.mServiceData, arrayOfByte2 = this.mServiceDataMask;
      byte[] arrayOfByte3 = scanRecord.getServiceData(parcelUuid2);
      if (!matchesPartialData(arrayOfByte1, arrayOfByte2, arrayOfByte3))
        return false; 
    } 
    int i = this.mManufacturerId;
    if (i >= 0) {
      byte[] arrayOfByte1 = this.mManufacturerData, arrayOfByte3 = this.mManufacturerDataMask;
      byte[] arrayOfByte2 = scanRecord.getManufacturerSpecificData(i);
      if (!matchesPartialData(arrayOfByte1, arrayOfByte3, arrayOfByte2))
        return false; 
    } 
    if (this.mOrgId >= 0) {
      byte[] arrayOfByte = scanRecord.getTDSData();
      if (arrayOfByte != null && arrayOfByte.length > 0)
        if (this.mOrgId == arrayOfByte[0]) {
          i = this.mTDSFlags;
          int j = this.mTDSFlagsMask;
          if ((i & j) != (j & arrayOfByte[1]))
            return false; 
        } else {
          return false;
        }  
    } 
    return true;
  }
  
  public static boolean matchesServiceUuids(ParcelUuid paramParcelUuid1, ParcelUuid paramParcelUuid2, List<ParcelUuid> paramList) {
    if (paramParcelUuid1 == null)
      return true; 
    if (paramList == null)
      return false; 
    for (ParcelUuid parcelUuid : paramList) {
      UUID uUID;
      if (paramParcelUuid2 == null) {
        paramList = null;
      } else {
        uUID = paramParcelUuid2.getUuid();
      } 
      if (matchesServiceUuid(paramParcelUuid1.getUuid(), uUID, parcelUuid.getUuid()))
        return true; 
    } 
    return false;
  }
  
  private static boolean matchesServiceUuid(UUID paramUUID1, UUID paramUUID2, UUID paramUUID3) {
    return BitUtils.maskedEquals(paramUUID3, paramUUID1, paramUUID2);
  }
  
  private static boolean matchesServiceSolicitationUuids(ParcelUuid paramParcelUuid1, ParcelUuid paramParcelUuid2, List<ParcelUuid> paramList) {
    if (paramParcelUuid1 == null)
      return true; 
    if (paramList == null)
      return false; 
    for (ParcelUuid parcelUuid : paramList) {
      UUID uUID1;
      if (paramParcelUuid2 == null) {
        paramList = null;
      } else {
        uUID1 = paramParcelUuid2.getUuid();
      } 
      UUID uUID3 = paramParcelUuid1.getUuid();
      UUID uUID2 = parcelUuid.getUuid();
      if (matchesServiceUuid(uUID3, uUID1, uUID2))
        return true; 
    } 
    return false;
  }
  
  private static boolean matchesServiceSolicitationUuid(UUID paramUUID1, UUID paramUUID2, UUID paramUUID3) {
    return BitUtils.maskedEquals(paramUUID3, paramUUID1, paramUUID2);
  }
  
  private boolean matchesPartialData(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3) {
    if (paramArrayOfbyte3 == null || paramArrayOfbyte3.length < paramArrayOfbyte1.length)
      return false; 
    if (paramArrayOfbyte2 == null) {
      for (byte b1 = 0; b1 < paramArrayOfbyte1.length; b1++) {
        if (paramArrayOfbyte3[b1] != paramArrayOfbyte1[b1])
          return false; 
      } 
      return true;
    } 
    for (byte b = 0; b < paramArrayOfbyte1.length; b++) {
      if ((paramArrayOfbyte2[b] & paramArrayOfbyte3[b]) != (paramArrayOfbyte2[b] & paramArrayOfbyte1[b]))
        return false; 
    } 
    return true;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("BluetoothLeScanFilter [mDeviceName=");
    stringBuilder.append(this.mDeviceName);
    stringBuilder.append(", mDeviceAddress=");
    stringBuilder.append(this.mDeviceAddress);
    stringBuilder.append(", mUuid=");
    stringBuilder.append(this.mServiceUuid);
    stringBuilder.append(", mUuidMask=");
    stringBuilder.append(this.mServiceUuidMask);
    stringBuilder.append(", mServiceSolicitationUuid=");
    stringBuilder.append(this.mServiceSolicitationUuid);
    stringBuilder.append(", mServiceSolicitationUuidMask=");
    stringBuilder.append(this.mServiceSolicitationUuidMask);
    stringBuilder.append(", mServiceDataUuid=");
    ParcelUuid parcelUuid = this.mServiceDataUuid;
    stringBuilder.append(Objects.toString(parcelUuid));
    stringBuilder.append(", mServiceData=");
    byte[] arrayOfByte = this.mServiceData;
    stringBuilder.append(Arrays.toString(arrayOfByte));
    stringBuilder.append(", mServiceDataMask=");
    arrayOfByte = this.mServiceDataMask;
    stringBuilder.append(Arrays.toString(arrayOfByte));
    stringBuilder.append(", mManufacturerId=");
    stringBuilder.append(this.mManufacturerId);
    stringBuilder.append(", mManufacturerData=");
    arrayOfByte = this.mManufacturerData;
    stringBuilder.append(Arrays.toString(arrayOfByte));
    stringBuilder.append(", mManufacturerDataMask=");
    arrayOfByte = this.mManufacturerDataMask;
    stringBuilder.append(Arrays.toString(arrayOfByte));
    stringBuilder.append(", mOrganizationId=");
    stringBuilder.append(this.mOrgId);
    stringBuilder.append(", mTDSFlags=");
    stringBuilder.append(this.mTDSFlags);
    stringBuilder.append(", mTDSFlagsMask=");
    stringBuilder.append(this.mTDSFlagsMask);
    stringBuilder.append(", mWifiNANHash=");
    arrayOfByte = this.mWifiNANHash;
    stringBuilder.append(Arrays.toString(arrayOfByte));
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int hashCode() {
    String str1 = this.mDeviceName, str2 = this.mDeviceAddress;
    int i = this.mManufacturerId;
    byte[] arrayOfByte1 = this.mManufacturerData;
    int j = Arrays.hashCode(arrayOfByte1);
    arrayOfByte1 = this.mManufacturerDataMask;
    int k = Arrays.hashCode(arrayOfByte1);
    ParcelUuid parcelUuid1 = this.mServiceDataUuid;
    byte[] arrayOfByte2 = this.mServiceData;
    int m = Arrays.hashCode(arrayOfByte2);
    arrayOfByte2 = this.mServiceDataMask;
    int n = Arrays.hashCode(arrayOfByte2);
    ParcelUuid parcelUuid3 = this.mServiceUuid, parcelUuid4 = this.mServiceUuidMask, parcelUuid2 = this.mServiceSolicitationUuid, parcelUuid5 = this.mServiceSolicitationUuidMask;
    int i1 = this.mOrgId;
    int i2 = this.mTDSFlags, i3 = this.mTDSFlagsMask, i4 = Arrays.hashCode(this.mWifiNANHash);
    return Objects.hash(new Object[] { 
          str1, str2, Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), parcelUuid1, Integer.valueOf(m), Integer.valueOf(n), parcelUuid3, parcelUuid4, 
          parcelUuid2, parcelUuid5, Integer.valueOf(i1), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4) });
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (Objects.equals(this.mDeviceName, ((ScanFilter)paramObject).mDeviceName)) {
      String str1 = this.mDeviceAddress, str2 = ((ScanFilter)paramObject).mDeviceAddress;
      if (Objects.equals(str1, str2) && this.mManufacturerId == ((ScanFilter)paramObject).mManufacturerId) {
        byte[] arrayOfByte1 = this.mManufacturerData, arrayOfByte2 = ((ScanFilter)paramObject).mManufacturerData;
        if (Objects.deepEquals(arrayOfByte1, arrayOfByte2)) {
          arrayOfByte2 = this.mManufacturerDataMask;
          arrayOfByte1 = ((ScanFilter)paramObject).mManufacturerDataMask;
          if (Objects.deepEquals(arrayOfByte2, arrayOfByte1)) {
            ParcelUuid parcelUuid2 = this.mServiceDataUuid, parcelUuid1 = ((ScanFilter)paramObject).mServiceDataUuid;
            if (Objects.equals(parcelUuid2, parcelUuid1)) {
              byte[] arrayOfByte4 = this.mServiceData, arrayOfByte3 = ((ScanFilter)paramObject).mServiceData;
              if (Objects.deepEquals(arrayOfByte4, arrayOfByte3)) {
                arrayOfByte3 = this.mServiceDataMask;
                arrayOfByte4 = ((ScanFilter)paramObject).mServiceDataMask;
                if (Objects.deepEquals(arrayOfByte3, arrayOfByte4)) {
                  ParcelUuid parcelUuid4 = this.mServiceUuid, parcelUuid3 = ((ScanFilter)paramObject).mServiceUuid;
                  if (Objects.equals(parcelUuid4, parcelUuid3)) {
                    parcelUuid4 = this.mServiceUuidMask;
                    parcelUuid3 = ((ScanFilter)paramObject).mServiceUuidMask;
                    if (Objects.equals(parcelUuid4, parcelUuid3)) {
                      parcelUuid4 = this.mServiceSolicitationUuid;
                      parcelUuid3 = ((ScanFilter)paramObject).mServiceSolicitationUuid;
                      if (Objects.equals(parcelUuid4, parcelUuid3)) {
                        parcelUuid3 = this.mServiceSolicitationUuidMask;
                        parcelUuid4 = ((ScanFilter)paramObject).mServiceSolicitationUuidMask;
                        if (Objects.equals(parcelUuid3, parcelUuid4) && this.mOrgId == ((ScanFilter)paramObject).mOrgId && this.mTDSFlags == ((ScanFilter)paramObject).mTDSFlags && this.mTDSFlagsMask == ((ScanFilter)paramObject).mTDSFlagsMask) {
                          byte[] arrayOfByte = this.mWifiNANHash;
                          paramObject = ((ScanFilter)paramObject).mWifiNANHash;
                          if (Objects.deepEquals(arrayOfByte, paramObject))
                            return null; 
                        } 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public boolean isAllFieldsEmpty() {
    return EMPTY.equals(this);
  }
  
  class Builder {
    private String mDeviceAddress;
    
    private String mDeviceName;
    
    private byte[] mManufacturerData;
    
    private byte[] mManufacturerDataMask;
    
    private int mManufacturerId;
    
    private int mOrgId;
    
    private byte[] mServiceData;
    
    private byte[] mServiceDataMask;
    
    private ParcelUuid mServiceDataUuid;
    
    private ParcelUuid mServiceSolicitationUuid;
    
    private ParcelUuid mServiceSolicitationUuidMask;
    
    private ParcelUuid mServiceUuid;
    
    private int mTDSFlags;
    
    private int mTDSFlagsMask;
    
    private ParcelUuid mUuidMask;
    
    private byte[] mWifiNANHash;
    
    public Builder() {
      this.mManufacturerId = -1;
      this.mOrgId = -1;
      this.mTDSFlags = -1;
      this.mTDSFlagsMask = -1;
    }
    
    public Builder setDeviceName(String param1String) {
      this.mDeviceName = param1String;
      return this;
    }
    
    public Builder setDeviceAddress(String param1String) {
      if (param1String == null || BluetoothAdapter.checkBluetoothAddress(param1String)) {
        this.mDeviceAddress = param1String;
        return this;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("invalid device address ");
      stringBuilder.append(param1String);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public Builder setServiceUuid(ParcelUuid param1ParcelUuid) {
      this.mServiceUuid = param1ParcelUuid;
      this.mUuidMask = null;
      return this;
    }
    
    public Builder setServiceUuid(ParcelUuid param1ParcelUuid1, ParcelUuid param1ParcelUuid2) {
      if (this.mUuidMask == null || this.mServiceUuid != null) {
        this.mServiceUuid = param1ParcelUuid1;
        this.mUuidMask = param1ParcelUuid2;
        return this;
      } 
      throw new IllegalArgumentException("uuid is null while uuidMask is not null!");
    }
    
    public Builder setServiceSolicitationUuid(ParcelUuid param1ParcelUuid) {
      this.mServiceSolicitationUuid = param1ParcelUuid;
      if (param1ParcelUuid == null)
        this.mServiceSolicitationUuidMask = null; 
      return this;
    }
    
    public Builder setServiceSolicitationUuid(ParcelUuid param1ParcelUuid1, ParcelUuid param1ParcelUuid2) {
      if (param1ParcelUuid2 == null || param1ParcelUuid1 != null) {
        this.mServiceSolicitationUuid = param1ParcelUuid1;
        this.mServiceSolicitationUuidMask = param1ParcelUuid2;
        return this;
      } 
      throw new IllegalArgumentException("SolicitationUuid is null while SolicitationUuidMask is not null!");
    }
    
    public Builder setServiceData(ParcelUuid param1ParcelUuid, byte[] param1ArrayOfbyte) {
      if (param1ParcelUuid != null) {
        this.mServiceDataUuid = param1ParcelUuid;
        this.mServiceData = param1ArrayOfbyte;
        this.mServiceDataMask = null;
        return this;
      } 
      throw new IllegalArgumentException("serviceDataUuid is null");
    }
    
    public Builder setServiceData(ParcelUuid param1ParcelUuid, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) {
      if (param1ParcelUuid != null) {
        byte[] arrayOfByte = this.mServiceDataMask;
        if (arrayOfByte != null) {
          byte[] arrayOfByte1 = this.mServiceData;
          if (arrayOfByte1 != null) {
            if (arrayOfByte1.length != arrayOfByte.length)
              throw new IllegalArgumentException("size mismatch for service data and service data mask"); 
          } else {
            throw new IllegalArgumentException("serviceData is null while serviceDataMask is not null");
          } 
        } 
        this.mServiceDataUuid = param1ParcelUuid;
        this.mServiceData = param1ArrayOfbyte1;
        this.mServiceDataMask = param1ArrayOfbyte2;
        return this;
      } 
      throw new IllegalArgumentException("serviceDataUuid is null");
    }
    
    public Builder setManufacturerData(int param1Int, byte[] param1ArrayOfbyte) {
      if (param1ArrayOfbyte == null || param1Int >= 0) {
        this.mManufacturerId = param1Int;
        this.mManufacturerData = param1ArrayOfbyte;
        this.mManufacturerDataMask = null;
        return this;
      } 
      throw new IllegalArgumentException("invalid manufacture id");
    }
    
    public Builder setManufacturerData(int param1Int, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) {
      if (param1ArrayOfbyte1 == null || param1Int >= 0) {
        byte[] arrayOfByte = this.mManufacturerDataMask;
        if (arrayOfByte != null) {
          byte[] arrayOfByte1 = this.mManufacturerData;
          if (arrayOfByte1 != null) {
            if (arrayOfByte1.length != arrayOfByte.length)
              throw new IllegalArgumentException("size mismatch for manufacturerData and manufacturerDataMask"); 
          } else {
            throw new IllegalArgumentException("manufacturerData is null while manufacturerDataMask is not null");
          } 
        } 
        this.mManufacturerId = param1Int;
        this.mManufacturerData = param1ArrayOfbyte1;
        this.mManufacturerDataMask = param1ArrayOfbyte2;
        return this;
      } 
      throw new IllegalArgumentException("invalid manufacture id");
    }
    
    public Builder setTransportDiscoveryData(int param1Int1, int param1Int2, int param1Int3, byte[] param1ArrayOfbyte) {
      if (param1Int1 >= 0) {
        if (param1Int1 == 2 || param1ArrayOfbyte == null) {
          this.mOrgId = param1Int1;
          this.mTDSFlags = param1Int2;
          this.mTDSFlagsMask = param1Int3;
          this.mWifiNANHash = param1ArrayOfbyte;
          return this;
        } 
        throw new IllegalArgumentException("Wifi NAN Hash is not null for non-Wifi Org Id");
      } 
      throw new IllegalArgumentException("invalid organization id");
    }
    
    public ScanFilter build() {
      return new ScanFilter(this.mDeviceName, this.mDeviceAddress, this.mServiceUuid, this.mUuidMask, this.mServiceSolicitationUuid, this.mServiceSolicitationUuidMask, this.mServiceDataUuid, this.mServiceData, this.mServiceDataMask, this.mManufacturerId, this.mManufacturerData, this.mManufacturerDataMask, this.mOrgId, this.mTDSFlags, this.mTDSFlagsMask, this.mWifiNANHash);
    }
  }
}
