package android.bluetooth.le;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public final class PeriodicAdvertisingReport implements Parcelable {
  public PeriodicAdvertisingReport(int paramInt1, int paramInt2, int paramInt3, int paramInt4, ScanRecord paramScanRecord) {
    this.mSyncHandle = paramInt1;
    this.mTxPower = paramInt2;
    this.mRssi = paramInt3;
    this.mDataStatus = paramInt4;
    this.mData = paramScanRecord;
  }
  
  private PeriodicAdvertisingReport(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mSyncHandle);
    paramParcel.writeInt(this.mTxPower);
    paramParcel.writeInt(this.mRssi);
    paramParcel.writeInt(this.mDataStatus);
    if (this.mData != null) {
      paramParcel.writeInt(1);
      paramParcel.writeByteArray(this.mData.getBytes());
    } else {
      paramParcel.writeInt(0);
    } 
  }
  
  private void readFromParcel(Parcel paramParcel) {
    this.mSyncHandle = paramParcel.readInt();
    this.mTxPower = paramParcel.readInt();
    this.mRssi = paramParcel.readInt();
    this.mDataStatus = paramParcel.readInt();
    if (paramParcel.readInt() == 1)
      this.mData = ScanRecord.parseFromBytes(paramParcel.createByteArray()); 
  }
  
  public int describeContents() {
    return 0;
  }
  
  public int getSyncHandle() {
    return this.mSyncHandle;
  }
  
  public int getTxPower() {
    return this.mTxPower;
  }
  
  public int getRssi() {
    return this.mRssi;
  }
  
  public int getDataStatus() {
    return this.mDataStatus;
  }
  
  public ScanRecord getData() {
    return this.mData;
  }
  
  public long getTimestampNanos() {
    return this.mTimestampNanos;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mSyncHandle), Integer.valueOf(this.mTxPower), Integer.valueOf(this.mRssi), Integer.valueOf(this.mDataStatus), this.mData, Long.valueOf(this.mTimestampNanos) });
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    PeriodicAdvertisingReport periodicAdvertisingReport = (PeriodicAdvertisingReport)paramObject;
    if (this.mSyncHandle == periodicAdvertisingReport.mSyncHandle && this.mTxPower == periodicAdvertisingReport.mTxPower && this.mRssi == periodicAdvertisingReport.mRssi && this.mDataStatus == periodicAdvertisingReport.mDataStatus) {
      ScanRecord scanRecord = this.mData;
      paramObject = periodicAdvertisingReport.mData;
      if (Objects.equals(scanRecord, paramObject) && this.mTimestampNanos == periodicAdvertisingReport.mTimestampNanos)
        return null; 
    } 
    return false;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("PeriodicAdvertisingReport{syncHandle=");
    stringBuilder.append(this.mSyncHandle);
    stringBuilder.append(", txPower=");
    stringBuilder.append(this.mTxPower);
    stringBuilder.append(", rssi=");
    stringBuilder.append(this.mRssi);
    stringBuilder.append(", dataStatus=");
    stringBuilder.append(this.mDataStatus);
    stringBuilder.append(", data=");
    ScanRecord scanRecord = this.mData;
    stringBuilder.append(Objects.toString(scanRecord));
    stringBuilder.append(", timestampNanos=");
    stringBuilder.append(this.mTimestampNanos);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<PeriodicAdvertisingReport> CREATOR = new Parcelable.Creator<PeriodicAdvertisingReport>() {
      public PeriodicAdvertisingReport createFromParcel(Parcel param1Parcel) {
        return new PeriodicAdvertisingReport(param1Parcel);
      }
      
      public PeriodicAdvertisingReport[] newArray(int param1Int) {
        return new PeriodicAdvertisingReport[param1Int];
      }
    };
  
  public static final int DATA_COMPLETE = 0;
  
  public static final int DATA_INCOMPLETE_TRUNCATED = 2;
  
  private ScanRecord mData;
  
  private int mDataStatus;
  
  private int mRssi;
  
  private int mSyncHandle;
  
  private long mTimestampNanos;
  
  private int mTxPower;
}
