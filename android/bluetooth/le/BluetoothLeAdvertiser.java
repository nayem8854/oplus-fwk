package android.bluetooth.le;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothUuid;
import android.bluetooth.IBluetoothGatt;
import android.bluetooth.IBluetoothManager;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelUuid;
import android.os.RemoteException;
import android.util.Log;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class BluetoothLeAdvertiser {
  private static final int FLAGS_FIELD_BYTES = 3;
  
  private static final int MANUFACTURER_SPECIFIC_DATA_LENGTH = 2;
  
  private static final int MAX_ADVERTISING_DATA_BYTES = 1650;
  
  private static final int MAX_LEGACY_ADVERTISING_DATA_BYTES = 31;
  
  private static final int OVERHEAD_BYTES_PER_FIELD = 2;
  
  private static final String TAG = "BluetoothLeAdvertiser";
  
  private final Map<Integer, AdvertisingSet> mAdvertisingSets;
  
  private BluetoothAdapter mBluetoothAdapter;
  
  private final IBluetoothManager mBluetoothManager;
  
  private final Map<AdvertisingSetCallback, IAdvertisingSetCallback> mCallbackWrappers;
  
  private final Handler mHandler;
  
  private final Map<AdvertiseCallback, AdvertisingSetCallback> mLegacyAdvertisers = new HashMap<>();
  
  public BluetoothLeAdvertiser(IBluetoothManager paramIBluetoothManager) {
    HashMap<Object, Object> hashMap = new HashMap<>();
    this.mCallbackWrappers = Collections.synchronizedMap(hashMap);
    hashMap = new HashMap<>();
    this.mAdvertisingSets = Collections.synchronizedMap(hashMap);
    this.mBluetoothManager = paramIBluetoothManager;
    this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    this.mHandler = new Handler(Looper.getMainLooper());
  }
  
  public void startAdvertising(AdvertiseSettings paramAdvertiseSettings, AdvertiseData paramAdvertiseData, AdvertiseCallback paramAdvertiseCallback) {
    startAdvertising(paramAdvertiseSettings, paramAdvertiseData, null, paramAdvertiseCallback);
  }
  
  public void startAdvertising(AdvertiseSettings paramAdvertiseSettings, AdvertiseData paramAdvertiseData1, AdvertiseData paramAdvertiseData2, AdvertiseCallback paramAdvertiseCallback) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLegacyAdvertisers : Ljava/util/Map;
    //   4: astore #5
    //   6: aload #5
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mBluetoothAdapter : Landroid/bluetooth/BluetoothAdapter;
    //   13: invokestatic checkAdapterStateOn : (Landroid/bluetooth/BluetoothAdapter;)V
    //   16: aload #4
    //   18: ifnull -> 335
    //   21: aload_1
    //   22: invokevirtual isConnectable : ()Z
    //   25: istore #6
    //   27: aload_0
    //   28: aload_2
    //   29: iload #6
    //   31: invokespecial totalBytes : (Landroid/bluetooth/le/AdvertiseData;Z)I
    //   34: istore #7
    //   36: iconst_1
    //   37: istore #8
    //   39: iload #7
    //   41: bipush #31
    //   43: if_icmpgt -> 320
    //   46: aload_0
    //   47: aload_3
    //   48: iconst_0
    //   49: invokespecial totalBytes : (Landroid/bluetooth/le/AdvertiseData;Z)I
    //   52: bipush #31
    //   54: if_icmple -> 60
    //   57: goto -> 320
    //   60: aload_0
    //   61: getfield mLegacyAdvertisers : Ljava/util/Map;
    //   64: aload #4
    //   66: invokeinterface containsKey : (Ljava/lang/Object;)Z
    //   71: ifeq -> 85
    //   74: aload_0
    //   75: aload #4
    //   77: iconst_3
    //   78: invokespecial postStartFailure : (Landroid/bluetooth/le/AdvertiseCallback;I)V
    //   81: aload #5
    //   83: monitorexit
    //   84: return
    //   85: new android/bluetooth/le/AdvertisingSetParameters$Builder
    //   88: astore #9
    //   90: aload #9
    //   92: invokespecial <init> : ()V
    //   95: aload #9
    //   97: iconst_1
    //   98: invokevirtual setLegacyMode : (Z)Landroid/bluetooth/le/AdvertisingSetParameters$Builder;
    //   101: pop
    //   102: aload #9
    //   104: iload #6
    //   106: invokevirtual setConnectable : (Z)Landroid/bluetooth/le/AdvertisingSetParameters$Builder;
    //   109: pop
    //   110: aload #9
    //   112: iconst_1
    //   113: invokevirtual setScannable : (Z)Landroid/bluetooth/le/AdvertisingSetParameters$Builder;
    //   116: pop
    //   117: aload_1
    //   118: invokevirtual getMode : ()I
    //   121: ifne -> 136
    //   124: aload #9
    //   126: sipush #1600
    //   129: invokevirtual setInterval : (I)Landroid/bluetooth/le/AdvertisingSetParameters$Builder;
    //   132: pop
    //   133: goto -> 173
    //   136: aload_1
    //   137: invokevirtual getMode : ()I
    //   140: iconst_1
    //   141: if_icmpne -> 156
    //   144: aload #9
    //   146: sipush #400
    //   149: invokevirtual setInterval : (I)Landroid/bluetooth/le/AdvertisingSetParameters$Builder;
    //   152: pop
    //   153: goto -> 173
    //   156: aload_1
    //   157: invokevirtual getMode : ()I
    //   160: iconst_2
    //   161: if_icmpne -> 173
    //   164: aload #9
    //   166: sipush #160
    //   169: invokevirtual setInterval : (I)Landroid/bluetooth/le/AdvertisingSetParameters$Builder;
    //   172: pop
    //   173: aload_1
    //   174: invokevirtual getTxPowerLevel : ()I
    //   177: ifne -> 191
    //   180: aload #9
    //   182: bipush #-21
    //   184: invokevirtual setTxPowerLevel : (I)Landroid/bluetooth/le/AdvertisingSetParameters$Builder;
    //   187: pop
    //   188: goto -> 244
    //   191: aload_1
    //   192: invokevirtual getTxPowerLevel : ()I
    //   195: iconst_1
    //   196: if_icmpne -> 210
    //   199: aload #9
    //   201: bipush #-15
    //   203: invokevirtual setTxPowerLevel : (I)Landroid/bluetooth/le/AdvertisingSetParameters$Builder;
    //   206: pop
    //   207: goto -> 244
    //   210: aload_1
    //   211: invokevirtual getTxPowerLevel : ()I
    //   214: iconst_2
    //   215: if_icmpne -> 229
    //   218: aload #9
    //   220: bipush #-7
    //   222: invokevirtual setTxPowerLevel : (I)Landroid/bluetooth/le/AdvertisingSetParameters$Builder;
    //   225: pop
    //   226: goto -> 244
    //   229: aload_1
    //   230: invokevirtual getTxPowerLevel : ()I
    //   233: iconst_3
    //   234: if_icmpne -> 244
    //   237: aload #9
    //   239: iconst_1
    //   240: invokevirtual setTxPowerLevel : (I)Landroid/bluetooth/le/AdvertisingSetParameters$Builder;
    //   243: pop
    //   244: aload_1
    //   245: invokevirtual getTimeout : ()I
    //   248: istore #7
    //   250: iload #7
    //   252: ifle -> 275
    //   255: iload #7
    //   257: bipush #10
    //   259: if_icmpge -> 265
    //   262: goto -> 272
    //   265: iload #7
    //   267: bipush #10
    //   269: idiv
    //   270: istore #8
    //   272: goto -> 278
    //   275: iconst_0
    //   276: istore #8
    //   278: aload_0
    //   279: aload #4
    //   281: aload_1
    //   282: invokevirtual wrapOldCallback : (Landroid/bluetooth/le/AdvertiseCallback;Landroid/bluetooth/le/AdvertiseSettings;)Landroid/bluetooth/le/AdvertisingSetCallback;
    //   285: astore_1
    //   286: aload_0
    //   287: getfield mLegacyAdvertisers : Ljava/util/Map;
    //   290: aload #4
    //   292: aload_1
    //   293: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   298: pop
    //   299: aload_0
    //   300: aload #9
    //   302: invokevirtual build : ()Landroid/bluetooth/le/AdvertisingSetParameters;
    //   305: aload_2
    //   306: aload_3
    //   307: aconst_null
    //   308: aconst_null
    //   309: iload #8
    //   311: iconst_0
    //   312: aload_1
    //   313: invokevirtual startAdvertisingSet : (Landroid/bluetooth/le/AdvertisingSetParameters;Landroid/bluetooth/le/AdvertiseData;Landroid/bluetooth/le/AdvertiseData;Landroid/bluetooth/le/PeriodicAdvertisingParameters;Landroid/bluetooth/le/AdvertiseData;IILandroid/bluetooth/le/AdvertisingSetCallback;)V
    //   316: aload #5
    //   318: monitorexit
    //   319: return
    //   320: aload_0
    //   321: aload #4
    //   323: iconst_1
    //   324: invokespecial postStartFailure : (Landroid/bluetooth/le/AdvertiseCallback;I)V
    //   327: aload #5
    //   329: monitorexit
    //   330: return
    //   331: astore_1
    //   332: goto -> 349
    //   335: new java/lang/IllegalArgumentException
    //   338: astore_1
    //   339: aload_1
    //   340: ldc_w 'callback cannot be null'
    //   343: invokespecial <init> : (Ljava/lang/String;)V
    //   346: aload_1
    //   347: athrow
    //   348: astore_1
    //   349: aload #5
    //   351: monitorexit
    //   352: aload_1
    //   353: athrow
    //   354: astore_1
    //   355: goto -> 349
    // Line number table:
    //   Java source line number -> byte code offset
    //   #112	-> 0
    //   #113	-> 9
    //   #114	-> 16
    //   #117	-> 21
    //   #118	-> 27
    //   #119	-> 46
    //   #123	-> 60
    //   #124	-> 74
    //   #125	-> 81
    //   #128	-> 85
    //   #129	-> 95
    //   #130	-> 102
    //   #131	-> 110
    //   #132	-> 117
    //   #133	-> 124
    //   #134	-> 136
    //   #135	-> 144
    //   #136	-> 156
    //   #137	-> 164
    //   #140	-> 173
    //   #141	-> 180
    //   #142	-> 191
    //   #143	-> 199
    //   #144	-> 210
    //   #145	-> 218
    //   #146	-> 229
    //   #147	-> 237
    //   #150	-> 244
    //   #151	-> 244
    //   #152	-> 250
    //   #153	-> 255
    //   #152	-> 275
    //   #156	-> 278
    //   #157	-> 286
    //   #158	-> 299
    //   #160	-> 316
    //   #161	-> 319
    //   #118	-> 320
    //   #120	-> 320
    //   #121	-> 327
    //   #160	-> 331
    //   #115	-> 335
    //   #160	-> 348
    // Exception table:
    //   from	to	target	type
    //   9	16	348	finally
    //   21	27	348	finally
    //   27	36	331	finally
    //   46	57	354	finally
    //   60	74	354	finally
    //   74	81	354	finally
    //   81	84	354	finally
    //   85	95	354	finally
    //   95	102	354	finally
    //   102	110	354	finally
    //   110	117	354	finally
    //   117	124	354	finally
    //   124	133	354	finally
    //   136	144	354	finally
    //   144	153	354	finally
    //   156	164	354	finally
    //   164	173	354	finally
    //   173	180	354	finally
    //   180	188	354	finally
    //   191	199	354	finally
    //   199	207	354	finally
    //   210	218	354	finally
    //   218	226	354	finally
    //   229	237	354	finally
    //   237	244	354	finally
    //   244	250	354	finally
    //   265	272	354	finally
    //   278	286	354	finally
    //   286	299	354	finally
    //   299	316	354	finally
    //   316	319	354	finally
    //   320	327	354	finally
    //   327	330	354	finally
    //   335	348	354	finally
    //   349	352	354	finally
  }
  
  AdvertisingSetCallback wrapOldCallback(AdvertiseCallback paramAdvertiseCallback, AdvertiseSettings paramAdvertiseSettings) {
    return (AdvertisingSetCallback)new Object(this, paramAdvertiseCallback, paramAdvertiseSettings);
  }
  
  public void stopAdvertising(AdvertiseCallback paramAdvertiseCallback) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLegacyAdvertisers : Ljava/util/Map;
    //   4: astore_2
    //   5: aload_2
    //   6: monitorenter
    //   7: aload_1
    //   8: ifnull -> 55
    //   11: aload_0
    //   12: getfield mLegacyAdvertisers : Ljava/util/Map;
    //   15: aload_1
    //   16: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   21: checkcast android/bluetooth/le/AdvertisingSetCallback
    //   24: astore_3
    //   25: aload_3
    //   26: ifnonnull -> 32
    //   29: aload_2
    //   30: monitorexit
    //   31: return
    //   32: aload_0
    //   33: aload_3
    //   34: invokevirtual stopAdvertisingSet : (Landroid/bluetooth/le/AdvertisingSetCallback;)V
    //   37: aload_0
    //   38: getfield mLegacyAdvertisers : Ljava/util/Map;
    //   41: aload_1
    //   42: invokeinterface remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   47: pop
    //   48: aload_2
    //   49: monitorexit
    //   50: return
    //   51: astore_1
    //   52: goto -> 68
    //   55: new java/lang/IllegalArgumentException
    //   58: astore_1
    //   59: aload_1
    //   60: ldc_w 'callback cannot be null'
    //   63: invokespecial <init> : (Ljava/lang/String;)V
    //   66: aload_1
    //   67: athrow
    //   68: aload_2
    //   69: monitorexit
    //   70: aload_1
    //   71: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #201	-> 0
    //   #202	-> 7
    //   #205	-> 11
    //   #206	-> 25
    //   #208	-> 32
    //   #210	-> 37
    //   #211	-> 48
    //   #212	-> 50
    //   #211	-> 51
    //   #203	-> 55
    //   #211	-> 68
    // Exception table:
    //   from	to	target	type
    //   11	25	51	finally
    //   29	31	51	finally
    //   32	37	51	finally
    //   37	48	51	finally
    //   48	50	51	finally
    //   55	68	51	finally
    //   68	70	51	finally
  }
  
  public void startAdvertisingSet(AdvertisingSetParameters paramAdvertisingSetParameters, AdvertiseData paramAdvertiseData1, AdvertiseData paramAdvertiseData2, PeriodicAdvertisingParameters paramPeriodicAdvertisingParameters, AdvertiseData paramAdvertiseData3, AdvertisingSetCallback paramAdvertisingSetCallback) {
    Handler handler = new Handler(Looper.getMainLooper());
    startAdvertisingSet(paramAdvertisingSetParameters, paramAdvertiseData1, paramAdvertiseData2, paramPeriodicAdvertisingParameters, paramAdvertiseData3, 0, 0, paramAdvertisingSetCallback, handler);
  }
  
  public void startAdvertisingSet(AdvertisingSetParameters paramAdvertisingSetParameters, AdvertiseData paramAdvertiseData1, AdvertiseData paramAdvertiseData2, PeriodicAdvertisingParameters paramPeriodicAdvertisingParameters, AdvertiseData paramAdvertiseData3, AdvertisingSetCallback paramAdvertisingSetCallback, Handler paramHandler) {
    startAdvertisingSet(paramAdvertisingSetParameters, paramAdvertiseData1, paramAdvertiseData2, paramPeriodicAdvertisingParameters, paramAdvertiseData3, 0, 0, paramAdvertisingSetCallback, paramHandler);
  }
  
  public void startAdvertisingSet(AdvertisingSetParameters paramAdvertisingSetParameters, AdvertiseData paramAdvertiseData1, AdvertiseData paramAdvertiseData2, PeriodicAdvertisingParameters paramPeriodicAdvertisingParameters, AdvertiseData paramAdvertiseData3, int paramInt1, int paramInt2, AdvertisingSetCallback paramAdvertisingSetCallback) {
    Handler handler = new Handler(Looper.getMainLooper());
    startAdvertisingSet(paramAdvertisingSetParameters, paramAdvertiseData1, paramAdvertiseData2, paramPeriodicAdvertisingParameters, paramAdvertiseData3, paramInt1, paramInt2, paramAdvertisingSetCallback, handler);
  }
  
  public void startAdvertisingSet(AdvertisingSetParameters paramAdvertisingSetParameters, AdvertiseData paramAdvertiseData1, AdvertiseData paramAdvertiseData2, PeriodicAdvertisingParameters paramPeriodicAdvertisingParameters, AdvertiseData paramAdvertiseData3, int paramInt1, int paramInt2, AdvertisingSetCallback paramAdvertisingSetCallback, Handler paramHandler) {
    BluetoothLeUtils.checkAdapterStateOn(this.mBluetoothAdapter);
    if (paramAdvertisingSetCallback != null) {
      boolean bool = paramAdvertisingSetParameters.isConnectable();
      if (paramAdvertisingSetParameters.isLegacy()) {
        if (totalBytes(paramAdvertiseData1, bool) <= 31) {
          if (totalBytes(paramAdvertiseData2, false) > 31)
            throw new IllegalArgumentException("Legacy scan response data too big"); 
        } else {
          throw new IllegalArgumentException("Legacy advertising data too big");
        } 
      } else {
        boolean bool1 = this.mBluetoothAdapter.isLeCodedPhySupported();
        boolean bool2 = this.mBluetoothAdapter.isLe2MPhySupported();
        int i = paramAdvertisingSetParameters.getPrimaryPhy();
        int j = paramAdvertisingSetParameters.getSecondaryPhy();
        if (i != 3 || bool1) {
          if ((j != 3 || bool1) && (j != 2 || bool2)) {
            j = this.mBluetoothAdapter.getLeMaximumAdvertisingDataLength();
            if (totalBytes(paramAdvertiseData1, bool) <= j) {
              if (totalBytes(paramAdvertiseData2, false) <= j) {
                if (totalBytes(paramAdvertiseData3, false) <= j) {
                  bool = this.mBluetoothAdapter.isLePeriodicAdvertisingSupported();
                  if (paramPeriodicAdvertisingParameters != null && !bool)
                    throw new IllegalArgumentException("Controller does not support LE Periodic Advertising"); 
                } else {
                  throw new IllegalArgumentException("Periodic advertising data too big");
                } 
              } else {
                throw new IllegalArgumentException("Scan response data too big");
              } 
            } else {
              throw new IllegalArgumentException("Advertising data too big");
            } 
          } else {
            throw new IllegalArgumentException("Unsupported secondary PHY selected");
          } 
        } else {
          throw new IllegalArgumentException("Unsupported primary PHY selected");
        } 
      } 
      if (paramInt2 >= 0 && paramInt2 <= 255) {
        if (paramInt2 != 0) {
          BluetoothAdapter bluetoothAdapter = this.mBluetoothAdapter;
          if (!bluetoothAdapter.isLePeriodicAdvertisingSupported())
            throw new IllegalArgumentException("Can't use maxExtendedAdvertisingEvents with controller that don't support LE Extended Advertising"); 
        } 
        if (paramInt1 >= 0 && paramInt1 <= 65535)
          try {
            IBluetoothGatt iBluetoothGatt = this.mBluetoothManager.getBluetoothGatt();
            if (iBluetoothGatt == null) {
              Log.e("BluetoothLeAdvertiser", "Bluetooth GATT is null");
              postStartSetFailure(paramHandler, paramAdvertisingSetCallback, 4);
              return;
            } 
            IAdvertisingSetCallback iAdvertisingSetCallback = wrap(paramAdvertisingSetCallback, paramHandler);
            if (this.mCallbackWrappers.putIfAbsent(paramAdvertisingSetCallback, iAdvertisingSetCallback) == null)
              try {
                iBluetoothGatt.startAdvertisingSet(paramAdvertisingSetParameters, paramAdvertiseData1, paramAdvertiseData2, paramPeriodicAdvertisingParameters, paramAdvertiseData3, paramInt1, paramInt2, iAdvertisingSetCallback);
                return;
              } catch (RemoteException remoteException) {
                Log.e("BluetoothLeAdvertiser", "Failed to start advertising set - ", (Throwable)remoteException);
                postStartSetFailure(paramHandler, paramAdvertisingSetCallback, 4);
                return;
              }  
            throw new IllegalArgumentException("callback instance already associated with advertising");
          } catch (RemoteException remoteException) {
            Log.e("BluetoothLeAdvertiser", "Failed to get Bluetooth GATT - ", (Throwable)remoteException);
            postStartSetFailure(paramHandler, paramAdvertisingSetCallback, 4);
            return;
          }  
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("duration out of range: ");
        stringBuilder1.append(paramInt1);
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("maxExtendedAdvertisingEvents out of range: ");
      stringBuilder.append(paramInt2);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    throw new IllegalArgumentException("callback cannot be null");
  }
  
  public void stopAdvertisingSet(AdvertisingSetCallback paramAdvertisingSetCallback) {
    if (paramAdvertisingSetCallback != null) {
      IAdvertisingSetCallback iAdvertisingSetCallback = this.mCallbackWrappers.remove(paramAdvertisingSetCallback);
      if (iAdvertisingSetCallback == null)
        return; 
      try {
        IBluetoothGatt iBluetoothGatt = this.mBluetoothManager.getBluetoothGatt();
        if (iBluetoothGatt != null)
          iBluetoothGatt.stopAdvertisingSet(iAdvertisingSetCallback); 
      } catch (RemoteException remoteException) {
        Log.e("BluetoothLeAdvertiser", "Failed to stop advertising - ", (Throwable)remoteException);
      } 
      return;
    } 
    throw new IllegalArgumentException("callback cannot be null");
  }
  
  public void cleanup() {
    this.mLegacyAdvertisers.clear();
    this.mCallbackWrappers.clear();
    this.mAdvertisingSets.clear();
  }
  
  private int totalBytes(AdvertiseData paramAdvertiseData, boolean paramBoolean) {
    int i = 0;
    if (paramAdvertiseData == null)
      return 0; 
    if (paramBoolean)
      i = 3; 
    int j = i;
    if (paramAdvertiseData.getServiceUuids() != null) {
      byte b1 = 0;
      byte b2 = 0;
      byte b3 = 0;
      for (ParcelUuid parcelUuid : paramAdvertiseData.getServiceUuids()) {
        if (BluetoothUuid.is16BitUuid(parcelUuid)) {
          b1++;
          continue;
        } 
        if (BluetoothUuid.is32BitUuid(parcelUuid)) {
          b2++;
          continue;
        } 
        b3++;
      } 
      j = i;
      if (b1 != 0)
        j = i + b1 * 2 + 2; 
      i = j;
      if (b2 != 0)
        i = j + b2 * 4 + 2; 
      j = i;
      if (b3 != 0)
        j = i + b3 * 16 + 2; 
    } 
    for (ParcelUuid parcelUuid : paramAdvertiseData.getServiceData().keySet()) {
      i = (BluetoothUuid.uuidToBytes(parcelUuid)).length;
      j += i + 2 + byteLength(paramAdvertiseData.getServiceData().get(parcelUuid));
    } 
    for (i = 0; i < paramAdvertiseData.getManufacturerSpecificData().size(); i++)
      j += byteLength((byte[])paramAdvertiseData.getManufacturerSpecificData().valueAt(i)) + 4; 
    i = j;
    if (paramAdvertiseData.getIncludeTxPowerLevel())
      i = j + 3; 
    j = i;
    if (paramAdvertiseData.getIncludeDeviceName()) {
      j = i;
      if (this.mBluetoothAdapter.getName() != null)
        j = i + this.mBluetoothAdapter.getName().length() + 2; 
    } 
    return j;
  }
  
  private int byteLength(byte[] paramArrayOfbyte) {
    int i;
    if (paramArrayOfbyte == null) {
      i = 0;
    } else {
      i = paramArrayOfbyte.length;
    } 
    return i;
  }
  
  IAdvertisingSetCallback wrap(AdvertisingSetCallback paramAdvertisingSetCallback, Handler paramHandler) {
    return (IAdvertisingSetCallback)new Object(this, paramHandler, paramAdvertisingSetCallback);
  }
  
  private void postStartSetFailure(Handler paramHandler, final AdvertisingSetCallback callback, final int error) {
    paramHandler.post(new Runnable() {
          final BluetoothLeAdvertiser this$0;
          
          final AdvertisingSetCallback val$callback;
          
          final int val$error;
          
          public void run() {
            callback.onAdvertisingSetStarted(null, 0, error);
          }
        });
  }
  
  private void postStartFailure(final AdvertiseCallback callback, final int error) {
    this.mHandler.post(new Runnable() {
          final BluetoothLeAdvertiser this$0;
          
          final AdvertiseCallback val$callback;
          
          final int val$error;
          
          public void run() {
            callback.onStartFailure(error);
          }
        });
  }
  
  private void postStartSuccess(final AdvertiseCallback callback, final AdvertiseSettings settings) {
    this.mHandler.post(new Runnable() {
          final BluetoothLeAdvertiser this$0;
          
          final AdvertiseCallback val$callback;
          
          final AdvertiseSettings val$settings;
          
          public void run() {
            callback.onStartSuccess(settings);
          }
        });
  }
}
