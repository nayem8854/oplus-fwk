package android.bluetooth.le;

import android.os.Parcel;
import android.os.Parcelable;

public final class PeriodicAdvertisingParameters implements Parcelable {
  private PeriodicAdvertisingParameters(boolean paramBoolean, int paramInt) {
    this.mIncludeTxPower = paramBoolean;
    this.mInterval = paramInt;
  }
  
  private PeriodicAdvertisingParameters(Parcel paramParcel) {
    boolean bool;
    if (paramParcel.readInt() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mIncludeTxPower = bool;
    this.mInterval = paramParcel.readInt();
  }
  
  public boolean getIncludeTxPower() {
    return this.mIncludeTxPower;
  }
  
  public int getInterval() {
    return this.mInterval;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mIncludeTxPower);
    paramParcel.writeInt(this.mInterval);
  }
  
  public static final Parcelable.Creator<PeriodicAdvertisingParameters> CREATOR = new Parcelable.Creator<PeriodicAdvertisingParameters>() {
      public PeriodicAdvertisingParameters[] newArray(int param1Int) {
        return new PeriodicAdvertisingParameters[param1Int];
      }
      
      public PeriodicAdvertisingParameters createFromParcel(Parcel param1Parcel) {
        return new PeriodicAdvertisingParameters(param1Parcel);
      }
    };
  
  private static final int INTERVAL_MAX = 65519;
  
  private static final int INTERVAL_MIN = 80;
  
  private final boolean mIncludeTxPower;
  
  private final int mInterval;
  
  class Builder {
    private boolean mIncludeTxPower;
    
    private int mInterval;
    
    public Builder() {
      this.mIncludeTxPower = false;
      this.mInterval = 65519;
    }
    
    public Builder setIncludeTxPower(boolean param1Boolean) {
      this.mIncludeTxPower = param1Boolean;
      return this;
    }
    
    public Builder setInterval(int param1Int) {
      if (param1Int >= 80 && param1Int <= 65519) {
        this.mInterval = param1Int;
        return this;
      } 
      throw new IllegalArgumentException("Invalid interval (must be 80-65519)");
    }
    
    public PeriodicAdvertisingParameters build() {
      return new PeriodicAdvertisingParameters(this.mIncludeTxPower, this.mInterval);
    }
  }
}
