package android.bluetooth.le;

import android.bluetooth.BluetoothUuid;
import android.os.ParcelUuid;
import android.util.SparseArray;
import java.util.List;
import java.util.Map;

public final class ScanRecord {
  private static final int DATA_TYPE_FLAGS = 1;
  
  private static final int DATA_TYPE_LOCAL_NAME_COMPLETE = 9;
  
  private static final int DATA_TYPE_LOCAL_NAME_SHORT = 8;
  
  private static final int DATA_TYPE_MANUFACTURER_SPECIFIC_DATA = 255;
  
  private static final int DATA_TYPE_SERVICE_DATA_128_BIT = 33;
  
  private static final int DATA_TYPE_SERVICE_DATA_16_BIT = 22;
  
  private static final int DATA_TYPE_SERVICE_DATA_32_BIT = 32;
  
  private static final int DATA_TYPE_SERVICE_SOLICITATION_UUIDS_128_BIT = 21;
  
  private static final int DATA_TYPE_SERVICE_SOLICITATION_UUIDS_16_BIT = 20;
  
  private static final int DATA_TYPE_SERVICE_SOLICITATION_UUIDS_32_BIT = 31;
  
  private static final int DATA_TYPE_SERVICE_UUIDS_128_BIT_COMPLETE = 7;
  
  private static final int DATA_TYPE_SERVICE_UUIDS_128_BIT_PARTIAL = 6;
  
  private static final int DATA_TYPE_SERVICE_UUIDS_16_BIT_COMPLETE = 3;
  
  private static final int DATA_TYPE_SERVICE_UUIDS_16_BIT_PARTIAL = 2;
  
  private static final int DATA_TYPE_SERVICE_UUIDS_32_BIT_COMPLETE = 5;
  
  private static final int DATA_TYPE_SERVICE_UUIDS_32_BIT_PARTIAL = 4;
  
  private static final int DATA_TYPE_TRANSPORT_DISCOVERY_DATA = 38;
  
  private static final int DATA_TYPE_TX_POWER_LEVEL = 10;
  
  private static final String TAG = "ScanRecord";
  
  private final int mAdvertiseFlags;
  
  private final byte[] mBytes;
  
  private final String mDeviceName;
  
  private final SparseArray<byte[]> mManufacturerSpecificData;
  
  private final Map<ParcelUuid, byte[]> mServiceData;
  
  private final List<ParcelUuid> mServiceSolicitationUuids;
  
  private final List<ParcelUuid> mServiceUuids;
  
  private final byte[] mTDSData;
  
  private final int mTxPowerLevel;
  
  public int getAdvertiseFlags() {
    return this.mAdvertiseFlags;
  }
  
  public List<ParcelUuid> getServiceUuids() {
    return this.mServiceUuids;
  }
  
  public List<ParcelUuid> getServiceSolicitationUuids() {
    return this.mServiceSolicitationUuids;
  }
  
  public SparseArray<byte[]> getManufacturerSpecificData() {
    return this.mManufacturerSpecificData;
  }
  
  public byte[] getManufacturerSpecificData(int paramInt) {
    SparseArray<byte[]> sparseArray = this.mManufacturerSpecificData;
    if (sparseArray == null)
      return null; 
    return (byte[])sparseArray.get(paramInt);
  }
  
  public Map<ParcelUuid, byte[]> getServiceData() {
    return this.mServiceData;
  }
  
  public byte[] getServiceData(ParcelUuid paramParcelUuid) {
    if (paramParcelUuid != null) {
      Map<ParcelUuid, byte[]> map = this.mServiceData;
      if (map != null)
        return map.get(paramParcelUuid); 
    } 
    return null;
  }
  
  public int getTxPowerLevel() {
    return this.mTxPowerLevel;
  }
  
  public String getDeviceName() {
    return this.mDeviceName;
  }
  
  public byte[] getTDSData() {
    return this.mTDSData;
  }
  
  public byte[] getBytes() {
    return this.mBytes;
  }
  
  private ScanRecord(List<ParcelUuid> paramList1, List<ParcelUuid> paramList2, SparseArray<byte[]> paramSparseArray, Map<ParcelUuid, byte[]> paramMap, int paramInt1, int paramInt2, String paramString, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) {
    this.mServiceSolicitationUuids = paramList2;
    this.mServiceUuids = paramList1;
    this.mManufacturerSpecificData = paramSparseArray;
    this.mServiceData = paramMap;
    this.mDeviceName = paramString;
    this.mAdvertiseFlags = paramInt1;
    this.mTxPowerLevel = paramInt2;
    this.mTDSData = paramArrayOfbyte1;
    this.mBytes = paramArrayOfbyte2;
  }
  
  public static ScanRecord parseFromBytes(byte[] paramArrayOfbyte) {
    // Byte code:
    //   0: aload_0
    //   1: ifnonnull -> 6
    //   4: aconst_null
    //   5: areturn
    //   6: new java/util/ArrayList
    //   9: dup
    //   10: invokespecial <init> : ()V
    //   13: astore_1
    //   14: new java/util/ArrayList
    //   17: dup
    //   18: invokespecial <init> : ()V
    //   21: astore_2
    //   22: new android/util/SparseArray
    //   25: dup
    //   26: invokespecial <init> : ()V
    //   29: astore_3
    //   30: new android/util/ArrayMap
    //   33: dup
    //   34: invokespecial <init> : ()V
    //   37: astore #4
    //   39: iconst_m1
    //   40: istore #5
    //   42: aconst_null
    //   43: astore #6
    //   45: ldc -2147483648
    //   47: istore #7
    //   49: aconst_null
    //   50: astore #8
    //   52: iconst_0
    //   53: istore #9
    //   55: aload_0
    //   56: arraylength
    //   57: istore #10
    //   59: iload #9
    //   61: iload #10
    //   63: if_icmpge -> 532
    //   66: iload #9
    //   68: iconst_1
    //   69: iadd
    //   70: istore #11
    //   72: aload_0
    //   73: iload #9
    //   75: baload
    //   76: istore #9
    //   78: iload #9
    //   80: sipush #255
    //   83: iand
    //   84: istore #9
    //   86: iload #9
    //   88: ifne -> 94
    //   91: goto -> 532
    //   94: iload #9
    //   96: iconst_1
    //   97: isub
    //   98: istore #10
    //   100: iload #11
    //   102: iconst_1
    //   103: iadd
    //   104: istore #12
    //   106: aload_0
    //   107: iload #11
    //   109: baload
    //   110: sipush #255
    //   113: iand
    //   114: istore #11
    //   116: iload #11
    //   118: bipush #38
    //   120: if_icmpeq -> 507
    //   123: iload #11
    //   125: sipush #255
    //   128: if_icmpeq -> 454
    //   131: iload #11
    //   133: tableswitch default -> 188, 1 -> 437, 2 -> 423, 3 -> 423, 4 -> 409, 5 -> 409, 6 -> 394, 7 -> 394, 8 -> 374, 9 -> 374, 10 -> 365
    //   188: iload #11
    //   190: tableswitch default -> 216, 20 -> 351, 21 -> 336, 22 -> 261
    //   216: iload #11
    //   218: tableswitch default -> 244, 31 -> 247, 32 -> 261, 33 -> 261
    //   244: goto -> 517
    //   247: aload_0
    //   248: iload #12
    //   250: iload #10
    //   252: iconst_4
    //   253: aload_2
    //   254: invokestatic parseServiceSolicitationUuid : ([BIIILjava/util/List;)I
    //   257: pop
    //   258: goto -> 517
    //   261: iconst_2
    //   262: istore #9
    //   264: iload #11
    //   266: bipush #32
    //   268: if_icmpne -> 277
    //   271: iconst_4
    //   272: istore #9
    //   274: goto -> 288
    //   277: iload #11
    //   279: bipush #33
    //   281: if_icmpne -> 288
    //   284: bipush #16
    //   286: istore #9
    //   288: aload_0
    //   289: iload #12
    //   291: iload #9
    //   293: invokestatic extractBytes : ([BII)[B
    //   296: astore #13
    //   298: aload #13
    //   300: invokestatic parseUuidFrom : ([B)Landroid/os/ParcelUuid;
    //   303: astore #13
    //   305: aload_0
    //   306: iload #12
    //   308: iload #9
    //   310: iadd
    //   311: iload #10
    //   313: iload #9
    //   315: isub
    //   316: invokestatic extractBytes : ([BII)[B
    //   319: astore #14
    //   321: aload #4
    //   323: aload #13
    //   325: aload #14
    //   327: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   332: pop
    //   333: goto -> 517
    //   336: aload_0
    //   337: iload #12
    //   339: iload #10
    //   341: bipush #16
    //   343: aload_2
    //   344: invokestatic parseServiceSolicitationUuid : ([BIIILjava/util/List;)I
    //   347: pop
    //   348: goto -> 517
    //   351: aload_0
    //   352: iload #12
    //   354: iload #10
    //   356: iconst_2
    //   357: aload_2
    //   358: invokestatic parseServiceSolicitationUuid : ([BIIILjava/util/List;)I
    //   361: pop
    //   362: goto -> 517
    //   365: aload_0
    //   366: iload #12
    //   368: baload
    //   369: istore #7
    //   371: goto -> 517
    //   374: new java/lang/String
    //   377: dup
    //   378: aload_0
    //   379: iload #12
    //   381: iload #10
    //   383: invokestatic extractBytes : ([BII)[B
    //   386: invokespecial <init> : ([B)V
    //   389: astore #6
    //   391: goto -> 517
    //   394: aload_0
    //   395: iload #12
    //   397: iload #10
    //   399: bipush #16
    //   401: aload_1
    //   402: invokestatic parseServiceUuid : ([BIIILjava/util/List;)I
    //   405: pop
    //   406: goto -> 517
    //   409: aload_0
    //   410: iload #12
    //   412: iload #10
    //   414: iconst_4
    //   415: aload_1
    //   416: invokestatic parseServiceUuid : ([BIIILjava/util/List;)I
    //   419: pop
    //   420: goto -> 517
    //   423: aload_0
    //   424: iload #12
    //   426: iload #10
    //   428: iconst_2
    //   429: aload_1
    //   430: invokestatic parseServiceUuid : ([BIIILjava/util/List;)I
    //   433: pop
    //   434: goto -> 517
    //   437: aload_0
    //   438: iload #12
    //   440: baload
    //   441: istore #9
    //   443: sipush #255
    //   446: iload #9
    //   448: iand
    //   449: istore #5
    //   451: goto -> 517
    //   454: aload_0
    //   455: iload #12
    //   457: iconst_1
    //   458: iadd
    //   459: baload
    //   460: istore #9
    //   462: aload_0
    //   463: iload #12
    //   465: baload
    //   466: istore #11
    //   468: aload_0
    //   469: iload #12
    //   471: iconst_2
    //   472: iadd
    //   473: iload #10
    //   475: iconst_2
    //   476: isub
    //   477: invokestatic extractBytes : ([BII)[B
    //   480: astore #13
    //   482: aload_3
    //   483: iload #9
    //   485: sipush #255
    //   488: iand
    //   489: bipush #8
    //   491: ishl
    //   492: sipush #255
    //   495: iload #11
    //   497: iand
    //   498: iadd
    //   499: aload #13
    //   501: invokevirtual put : (ILjava/lang/Object;)V
    //   504: goto -> 517
    //   507: aload_0
    //   508: iload #12
    //   510: iload #10
    //   512: invokestatic extractBytes : ([BII)[B
    //   515: astore #8
    //   517: iload #12
    //   519: iload #10
    //   521: iadd
    //   522: istore #9
    //   524: goto -> 55
    //   527: astore #8
    //   529: goto -> 588
    //   532: aload_1
    //   533: invokeinterface isEmpty : ()Z
    //   538: istore #15
    //   540: iload #15
    //   542: ifeq -> 550
    //   545: aconst_null
    //   546: astore_1
    //   547: goto -> 550
    //   550: new android/bluetooth/le/ScanRecord
    //   553: dup
    //   554: aload_1
    //   555: aload_2
    //   556: aload_3
    //   557: aload #4
    //   559: iload #5
    //   561: iload #7
    //   563: aload #6
    //   565: aload #8
    //   567: aload_0
    //   568: invokespecial <init> : (Ljava/util/List;Ljava/util/List;Landroid/util/SparseArray;Ljava/util/Map;IILjava/lang/String;[B[B)V
    //   571: astore #8
    //   573: aload #8
    //   575: areturn
    //   576: astore #8
    //   578: goto -> 588
    //   581: astore #8
    //   583: goto -> 588
    //   586: astore #8
    //   588: new java/lang/StringBuilder
    //   591: dup
    //   592: invokespecial <init> : ()V
    //   595: astore #8
    //   597: aload #8
    //   599: ldc 'unable to parse scan record: '
    //   601: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   604: pop
    //   605: aload #8
    //   607: aload_0
    //   608: invokestatic toString : ([B)Ljava/lang/String;
    //   611: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   614: pop
    //   615: ldc 'ScanRecord'
    //   617: aload #8
    //   619: invokevirtual toString : ()Ljava/lang/String;
    //   622: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   625: pop
    //   626: new android/bluetooth/le/ScanRecord
    //   629: dup
    //   630: aconst_null
    //   631: aconst_null
    //   632: aconst_null
    //   633: aconst_null
    //   634: iconst_m1
    //   635: ldc -2147483648
    //   637: aconst_null
    //   638: aconst_null
    //   639: aload_0
    //   640: invokespecial <init> : (Ljava/util/List;Ljava/util/List;Landroid/util/SparseArray;Ljava/util/Map;IILjava/lang/String;[B[B)V
    //   643: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #213	-> 0
    //   #214	-> 4
    //   #217	-> 6
    //   #218	-> 6
    //   #219	-> 6
    //   #220	-> 14
    //   #221	-> 22
    //   #222	-> 22
    //   #224	-> 22
    //   #225	-> 30
    //   #227	-> 39
    //   #230	-> 55
    //   #232	-> 66
    //   #233	-> 86
    //   #234	-> 91
    //   #237	-> 94
    //   #239	-> 100
    //   #240	-> 116
    //   #264	-> 247
    //   #266	-> 258
    //   #282	-> 261
    //   #283	-> 264
    //   #284	-> 271
    //   #285	-> 277
    //   #286	-> 284
    //   #289	-> 288
    //   #291	-> 298
    //   #293	-> 305
    //   #295	-> 321
    //   #296	-> 333
    //   #268	-> 336
    //   #270	-> 348
    //   #260	-> 351
    //   #262	-> 362
    //   #277	-> 365
    //   #278	-> 371
    //   #273	-> 374
    //   #274	-> 374
    //   #275	-> 391
    //   #256	-> 394
    //   #258	-> 406
    //   #251	-> 409
    //   #253	-> 420
    //   #246	-> 423
    //   #248	-> 434
    //   #242	-> 437
    //   #243	-> 443
    //   #300	-> 454
    //   #302	-> 468
    //   #304	-> 482
    //   #305	-> 504
    //   #307	-> 507
    //   #308	-> 517
    //   #313	-> 517
    //   #314	-> 524
    //   #321	-> 527
    //   #230	-> 532
    //   #316	-> 532
    //   #317	-> 545
    //   #316	-> 550
    //   #319	-> 550
    //   #321	-> 576
    //   #322	-> 588
    //   #325	-> 626
    // Exception table:
    //   from	to	target	type
    //   55	59	586	java/lang/Exception
    //   247	258	527	java/lang/Exception
    //   288	298	527	java/lang/Exception
    //   298	305	527	java/lang/Exception
    //   305	321	527	java/lang/Exception
    //   321	333	527	java/lang/Exception
    //   336	348	527	java/lang/Exception
    //   351	362	527	java/lang/Exception
    //   374	391	527	java/lang/Exception
    //   394	406	527	java/lang/Exception
    //   409	420	527	java/lang/Exception
    //   423	434	527	java/lang/Exception
    //   468	482	527	java/lang/Exception
    //   482	504	527	java/lang/Exception
    //   507	517	527	java/lang/Exception
    //   532	540	581	java/lang/Exception
    //   550	573	576	java/lang/Exception
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ScanRecord [mAdvertiseFlags=");
    stringBuilder.append(this.mAdvertiseFlags);
    stringBuilder.append(", mServiceUuids=");
    stringBuilder.append(this.mServiceUuids);
    stringBuilder.append(", mServiceSolicitationUuids=");
    stringBuilder.append(this.mServiceSolicitationUuids);
    stringBuilder.append(", mManufacturerSpecificData=");
    SparseArray<byte[]> sparseArray = this.mManufacturerSpecificData;
    stringBuilder.append(BluetoothLeUtils.toString(sparseArray));
    stringBuilder.append(", mServiceData=");
    Map<ParcelUuid, byte[]> map = this.mServiceData;
    stringBuilder.append(BluetoothLeUtils.toString(map));
    stringBuilder.append(", mTxPowerLevel=");
    stringBuilder.append(this.mTxPowerLevel);
    stringBuilder.append(", mDeviceName=");
    stringBuilder.append(this.mDeviceName);
    stringBuilder.append(", mTDSData=");
    byte[] arrayOfByte = this.mTDSData;
    stringBuilder.append(BluetoothLeUtils.toString(arrayOfByte));
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  private static int parseServiceUuid(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3, List<ParcelUuid> paramList) {
    while (paramInt2 > 0) {
      byte[] arrayOfByte = extractBytes(paramArrayOfbyte, paramInt1, paramInt3);
      paramList.add(BluetoothUuid.parseUuidFrom(arrayOfByte));
      paramInt2 -= paramInt3;
      paramInt1 += paramInt3;
    } 
    return paramInt1;
  }
  
  private static int parseServiceSolicitationUuid(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3, List<ParcelUuid> paramList) {
    while (paramInt2 > 0) {
      byte[] arrayOfByte = extractBytes(paramArrayOfbyte, paramInt1, paramInt3);
      paramList.add(BluetoothUuid.parseUuidFrom(arrayOfByte));
      paramInt2 -= paramInt3;
      paramInt1 += paramInt3;
    } 
    return paramInt1;
  }
  
  private static byte[] extractBytes(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    byte[] arrayOfByte = new byte[paramInt2];
    System.arraycopy(paramArrayOfbyte, paramInt1, arrayOfByte, 0, paramInt2);
    return arrayOfByte;
  }
}
