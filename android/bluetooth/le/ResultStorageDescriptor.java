package android.bluetooth.le;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class ResultStorageDescriptor implements Parcelable {
  public int getType() {
    return this.mType;
  }
  
  public int getOffset() {
    return this.mOffset;
  }
  
  public int getLength() {
    return this.mLength;
  }
  
  public ResultStorageDescriptor(int paramInt1, int paramInt2, int paramInt3) {
    this.mType = paramInt1;
    this.mOffset = paramInt2;
    this.mLength = paramInt3;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mType);
    paramParcel.writeInt(this.mOffset);
    paramParcel.writeInt(this.mLength);
  }
  
  private ResultStorageDescriptor(Parcel paramParcel) {
    ReadFromParcel(paramParcel);
  }
  
  private void ReadFromParcel(Parcel paramParcel) {
    this.mType = paramParcel.readInt();
    this.mOffset = paramParcel.readInt();
    this.mLength = paramParcel.readInt();
  }
  
  public static final Parcelable.Creator<ResultStorageDescriptor> CREATOR = new Parcelable.Creator<ResultStorageDescriptor>() {
      public ResultStorageDescriptor createFromParcel(Parcel param1Parcel) {
        return new ResultStorageDescriptor(param1Parcel);
      }
      
      public ResultStorageDescriptor[] newArray(int param1Int) {
        return new ResultStorageDescriptor[param1Int];
      }
    };
  
  private int mLength;
  
  private int mOffset;
  
  private int mType;
}
