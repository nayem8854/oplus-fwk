package android.bluetooth;

import android.annotation.SystemApi;
import android.os.ParcelUuid;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.HashSet;
import java.util.UUID;

@SystemApi
public final class BluetoothUuid {
  @SystemApi
  public static final ParcelUuid A2DP_SINK = ParcelUuid.fromString("0000110B-0000-1000-8000-00805F9B34FB");
  
  @SystemApi
  public static final ParcelUuid A2DP_SOURCE = ParcelUuid.fromString("0000110A-0000-1000-8000-00805F9B34FB");
  
  @SystemApi
  public static final ParcelUuid ADV_AUDIO_DIST = ParcelUuid.fromString("0000110D-0000-1000-8000-00805F9B34FB");
  
  @SystemApi
  public static final ParcelUuid AVRCP_CONTROLLER;
  
  @SystemApi
  public static final ParcelUuid AVRCP_TARGET;
  
  @SystemApi
  public static final ParcelUuid BASE_UUID;
  
  @SystemApi
  public static final ParcelUuid BNEP;
  
  @SystemApi
  public static final ParcelUuid HEARING_AID;
  
  @SystemApi
  public static final ParcelUuid HFP;
  
  @SystemApi
  public static final ParcelUuid HFP_AG;
  
  @SystemApi
  public static final ParcelUuid HID;
  
  @SystemApi
  public static final ParcelUuid HOGP;
  
  @SystemApi
  public static final ParcelUuid HSP = ParcelUuid.fromString("00001108-0000-1000-8000-00805F9B34FB");
  
  @SystemApi
  public static final ParcelUuid HSP_AG = ParcelUuid.fromString("00001112-0000-1000-8000-00805F9B34FB");
  
  @SystemApi
  public static final ParcelUuid MAP;
  
  @SystemApi
  public static final ParcelUuid MAS;
  
  @SystemApi
  public static final ParcelUuid MNS;
  
  @SystemApi
  public static final ParcelUuid NAP;
  
  @SystemApi
  public static final ParcelUuid OBEX_OBJECT_PUSH;
  
  @SystemApi
  public static final ParcelUuid PANU;
  
  @SystemApi
  public static final ParcelUuid PBAP_PCE;
  
  @SystemApi
  public static final ParcelUuid PBAP_PSE;
  
  @SystemApi
  public static final ParcelUuid SAP;
  
  @SystemApi
  public static final int UUID_BYTES_128_BIT = 16;
  
  @SystemApi
  public static final int UUID_BYTES_16_BIT = 2;
  
  @SystemApi
  public static final int UUID_BYTES_32_BIT = 4;
  
  static {
    HFP = ParcelUuid.fromString("0000111E-0000-1000-8000-00805F9B34FB");
    HFP_AG = ParcelUuid.fromString("0000111F-0000-1000-8000-00805F9B34FB");
    AVRCP_CONTROLLER = ParcelUuid.fromString("0000110E-0000-1000-8000-00805F9B34FB");
    AVRCP_TARGET = ParcelUuid.fromString("0000110C-0000-1000-8000-00805F9B34FB");
    OBEX_OBJECT_PUSH = ParcelUuid.fromString("00001105-0000-1000-8000-00805f9b34fb");
    HID = ParcelUuid.fromString("00001124-0000-1000-8000-00805f9b34fb");
    HOGP = ParcelUuid.fromString("00001812-0000-1000-8000-00805f9b34fb");
    PANU = ParcelUuid.fromString("00001115-0000-1000-8000-00805F9B34FB");
    NAP = ParcelUuid.fromString("00001116-0000-1000-8000-00805F9B34FB");
    BNEP = ParcelUuid.fromString("0000000f-0000-1000-8000-00805F9B34FB");
    PBAP_PCE = ParcelUuid.fromString("0000112e-0000-1000-8000-00805F9B34FB");
    PBAP_PSE = ParcelUuid.fromString("0000112f-0000-1000-8000-00805F9B34FB");
    MAP = ParcelUuid.fromString("00001134-0000-1000-8000-00805F9B34FB");
    MNS = ParcelUuid.fromString("00001133-0000-1000-8000-00805F9B34FB");
    MAS = ParcelUuid.fromString("00001132-0000-1000-8000-00805F9B34FB");
    SAP = ParcelUuid.fromString("0000112D-0000-1000-8000-00805F9B34FB");
    HEARING_AID = ParcelUuid.fromString("0000FDF0-0000-1000-8000-00805f9b34fb");
    BASE_UUID = ParcelUuid.fromString("00000000-0000-1000-8000-00805F9B34FB");
  }
  
  @SystemApi
  public static boolean containsAnyUuid(ParcelUuid[] paramArrayOfParcelUuid1, ParcelUuid[] paramArrayOfParcelUuid2) {
    boolean bool1 = true, bool2 = true;
    if (paramArrayOfParcelUuid1 == null && paramArrayOfParcelUuid2 == null)
      return true; 
    if (paramArrayOfParcelUuid1 == null) {
      if (paramArrayOfParcelUuid2.length != 0)
        bool2 = false; 
      return bool2;
    } 
    if (paramArrayOfParcelUuid2 == null) {
      if (paramArrayOfParcelUuid1.length == 0) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      return bool2;
    } 
    HashSet hashSet = new HashSet(Arrays.asList((Object[])paramArrayOfParcelUuid1));
    int i;
    byte b;
    for (i = paramArrayOfParcelUuid2.length, b = 0; b < i; ) {
      ParcelUuid parcelUuid = paramArrayOfParcelUuid2[b];
      if (hashSet.contains(parcelUuid))
        return true; 
      b++;
    } 
    return false;
  }
  
  private static int getServiceIdentifierFromParcelUuid(ParcelUuid paramParcelUuid) {
    UUID uUID = paramParcelUuid.getUuid();
    long l = uUID.getMostSignificantBits();
    return (int)((l & 0xFFFFFFFF00000000L) >>> 32L);
  }
  
  @SystemApi
  public static ParcelUuid parseUuidFrom(byte[] paramArrayOfbyte) {
    // Byte code:
    //   0: aload_0
    //   1: ifnull -> 240
    //   4: aload_0
    //   5: arraylength
    //   6: istore_1
    //   7: iload_1
    //   8: iconst_2
    //   9: if_icmpeq -> 59
    //   12: iload_1
    //   13: iconst_4
    //   14: if_icmpeq -> 59
    //   17: iload_1
    //   18: bipush #16
    //   20: if_icmpne -> 26
    //   23: goto -> 59
    //   26: new java/lang/StringBuilder
    //   29: dup
    //   30: invokespecial <init> : ()V
    //   33: astore_0
    //   34: aload_0
    //   35: ldc 'uuidBytes length invalid - '
    //   37: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   40: pop
    //   41: aload_0
    //   42: iload_1
    //   43: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   46: pop
    //   47: new java/lang/IllegalArgumentException
    //   50: dup
    //   51: aload_0
    //   52: invokevirtual toString : ()Ljava/lang/String;
    //   55: invokespecial <init> : (Ljava/lang/String;)V
    //   58: athrow
    //   59: iload_1
    //   60: bipush #16
    //   62: if_icmpne -> 108
    //   65: aload_0
    //   66: invokestatic wrap : ([B)Ljava/nio/ByteBuffer;
    //   69: getstatic java/nio/ByteOrder.LITTLE_ENDIAN : Ljava/nio/ByteOrder;
    //   72: invokevirtual order : (Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
    //   75: astore_0
    //   76: aload_0
    //   77: bipush #8
    //   79: invokevirtual getLong : (I)J
    //   82: lstore_2
    //   83: aload_0
    //   84: iconst_0
    //   85: invokevirtual getLong : (I)J
    //   88: lstore #4
    //   90: new android/os/ParcelUuid
    //   93: dup
    //   94: new java/util/UUID
    //   97: dup
    //   98: lload_2
    //   99: lload #4
    //   101: invokespecial <init> : (JJ)V
    //   104: invokespecial <init> : (Ljava/util/UUID;)V
    //   107: areturn
    //   108: iload_1
    //   109: iconst_2
    //   110: if_icmpne -> 139
    //   113: aload_0
    //   114: iconst_0
    //   115: baload
    //   116: sipush #255
    //   119: iand
    //   120: i2l
    //   121: lstore_2
    //   122: lload_2
    //   123: aload_0
    //   124: iconst_1
    //   125: baload
    //   126: sipush #255
    //   129: iand
    //   130: bipush #8
    //   132: ishl
    //   133: i2l
    //   134: ladd
    //   135: lstore_2
    //   136: goto -> 194
    //   139: aload_0
    //   140: iconst_0
    //   141: baload
    //   142: sipush #255
    //   145: iand
    //   146: i2l
    //   147: lstore_2
    //   148: aload_0
    //   149: iconst_1
    //   150: baload
    //   151: sipush #255
    //   154: iand
    //   155: bipush #8
    //   157: ishl
    //   158: i2l
    //   159: lstore #4
    //   161: aload_0
    //   162: iconst_2
    //   163: baload
    //   164: sipush #255
    //   167: iand
    //   168: bipush #16
    //   170: ishl
    //   171: i2l
    //   172: lstore #6
    //   174: aload_0
    //   175: iconst_3
    //   176: baload
    //   177: sipush #255
    //   180: iand
    //   181: bipush #24
    //   183: ishl
    //   184: i2l
    //   185: lload_2
    //   186: lload #4
    //   188: ladd
    //   189: lload #6
    //   191: ladd
    //   192: ladd
    //   193: lstore_2
    //   194: getstatic android/bluetooth/BluetoothUuid.BASE_UUID : Landroid/os/ParcelUuid;
    //   197: invokevirtual getUuid : ()Ljava/util/UUID;
    //   200: invokevirtual getMostSignificantBits : ()J
    //   203: lstore #4
    //   205: getstatic android/bluetooth/BluetoothUuid.BASE_UUID : Landroid/os/ParcelUuid;
    //   208: invokevirtual getUuid : ()Ljava/util/UUID;
    //   211: invokevirtual getLeastSignificantBits : ()J
    //   214: lstore #6
    //   216: new android/os/ParcelUuid
    //   219: dup
    //   220: new java/util/UUID
    //   223: dup
    //   224: lload #4
    //   226: lload_2
    //   227: bipush #32
    //   229: lshl
    //   230: ladd
    //   231: lload #6
    //   233: invokespecial <init> : (JJ)V
    //   236: invokespecial <init> : (Ljava/util/UUID;)V
    //   239: areturn
    //   240: new java/lang/IllegalArgumentException
    //   243: dup
    //   244: ldc 'uuidBytes cannot be null'
    //   246: invokespecial <init> : (Ljava/lang/String;)V
    //   249: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #241	-> 0
    //   #244	-> 4
    //   #245	-> 7
    //   #247	-> 26
    //   #251	-> 59
    //   #252	-> 65
    //   #253	-> 76
    //   #254	-> 83
    //   #255	-> 90
    //   #261	-> 108
    //   #262	-> 113
    //   #263	-> 122
    //   #265	-> 139
    //   #266	-> 148
    //   #267	-> 161
    //   #268	-> 174
    //   #270	-> 194
    //   #271	-> 205
    //   #272	-> 216
    //   #242	-> 240
  }
  
  public static byte[] uuidToBytes(ParcelUuid paramParcelUuid) {
    if (paramParcelUuid != null) {
      if (is16BitUuid(paramParcelUuid)) {
        int i = getServiceIdentifierFromParcelUuid(paramParcelUuid);
        byte b1 = (byte)(i & 0xFF);
        byte b2 = (byte)((0xFF00 & i) >> 8);
        return new byte[] { b1, b2 };
      } 
      if (is32BitUuid(paramParcelUuid)) {
        int i = getServiceIdentifierFromParcelUuid(paramParcelUuid);
        byte b3 = (byte)(i & 0xFF);
        byte b4 = (byte)((0xFF00 & i) >> 8);
        byte b2 = (byte)((0xFF0000 & i) >> 16);
        byte b1 = (byte)((0xFF000000 & i) >> 24);
        return new byte[] { b3, b4, b2, b1 };
      } 
      long l1 = paramParcelUuid.getUuid().getMostSignificantBits();
      long l2 = paramParcelUuid.getUuid().getLeastSignificantBits();
      byte[] arrayOfByte = new byte[16];
      ByteBuffer byteBuffer = ByteBuffer.wrap(arrayOfByte).order(ByteOrder.LITTLE_ENDIAN);
      byteBuffer.putLong(8, l1);
      byteBuffer.putLong(0, l2);
      return arrayOfByte;
    } 
    throw new IllegalArgumentException("uuid cannot be null");
  }
  
  public static boolean is16BitUuid(ParcelUuid paramParcelUuid) {
    UUID uUID = paramParcelUuid.getUuid();
    long l1 = uUID.getLeastSignificantBits(), l2 = BASE_UUID.getUuid().getLeastSignificantBits();
    boolean bool = false;
    if (l1 != l2)
      return false; 
    if ((uUID.getMostSignificantBits() & 0xFFFF0000FFFFFFFFL) == 4096L)
      bool = true; 
    return bool;
  }
  
  public static boolean is32BitUuid(ParcelUuid paramParcelUuid) {
    UUID uUID = paramParcelUuid.getUuid();
    long l1 = uUID.getLeastSignificantBits(), l2 = BASE_UUID.getUuid().getLeastSignificantBits();
    boolean bool = false;
    if (l1 != l2)
      return false; 
    if (is16BitUuid(paramParcelUuid))
      return false; 
    if ((uUID.getMostSignificantBits() & 0xFFFFFFFFL) == 4096L)
      bool = true; 
    return bool;
  }
}
