package android.bluetooth;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IBluetoothHearingAid extends IInterface {
  public static final int HI_SYNC_ID_INVALID = 0;
  
  public static final int MODE_BINAURAL = 1;
  
  public static final int MODE_MONAURAL = 0;
  
  public static final int SIDE_LEFT = 0;
  
  public static final int SIDE_RIGHT = 1;
  
  boolean connect(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  boolean disconnect(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  List<BluetoothDevice> getActiveDevices() throws RemoteException;
  
  List<BluetoothDevice> getConnectedDevices() throws RemoteException;
  
  int getConnectionPolicy(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  int getConnectionState(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  int getDeviceMode(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  int getDeviceSide(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] paramArrayOfint) throws RemoteException;
  
  long getHiSyncId(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  boolean setActiveDevice(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  boolean setConnectionPolicy(BluetoothDevice paramBluetoothDevice, int paramInt) throws RemoteException;
  
  void setVolume(int paramInt) throws RemoteException;
  
  class Default implements IBluetoothHearingAid {
    public boolean connect(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return false;
    }
    
    public boolean disconnect(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return false;
    }
    
    public List<BluetoothDevice> getConnectedDevices() throws RemoteException {
      return null;
    }
    
    public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public int getConnectionState(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return 0;
    }
    
    public boolean setActiveDevice(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return false;
    }
    
    public List<BluetoothDevice> getActiveDevices() throws RemoteException {
      return null;
    }
    
    public boolean setConnectionPolicy(BluetoothDevice param1BluetoothDevice, int param1Int) throws RemoteException {
      return false;
    }
    
    public int getConnectionPolicy(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return 0;
    }
    
    public void setVolume(int param1Int) throws RemoteException {}
    
    public long getHiSyncId(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return 0L;
    }
    
    public int getDeviceSide(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return 0;
    }
    
    public int getDeviceMode(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IBluetoothHearingAid {
    private static final String DESCRIPTOR = "android.bluetooth.IBluetoothHearingAid";
    
    static final int TRANSACTION_connect = 1;
    
    static final int TRANSACTION_disconnect = 2;
    
    static final int TRANSACTION_getActiveDevices = 7;
    
    static final int TRANSACTION_getConnectedDevices = 3;
    
    static final int TRANSACTION_getConnectionPolicy = 9;
    
    static final int TRANSACTION_getConnectionState = 5;
    
    static final int TRANSACTION_getDeviceMode = 13;
    
    static final int TRANSACTION_getDeviceSide = 12;
    
    static final int TRANSACTION_getDevicesMatchingConnectionStates = 4;
    
    static final int TRANSACTION_getHiSyncId = 11;
    
    static final int TRANSACTION_setActiveDevice = 6;
    
    static final int TRANSACTION_setConnectionPolicy = 8;
    
    static final int TRANSACTION_setVolume = 10;
    
    public Stub() {
      attachInterface(this, "android.bluetooth.IBluetoothHearingAid");
    }
    
    public static IBluetoothHearingAid asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.bluetooth.IBluetoothHearingAid");
      if (iInterface != null && iInterface instanceof IBluetoothHearingAid)
        return (IBluetoothHearingAid)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 13:
          return "getDeviceMode";
        case 12:
          return "getDeviceSide";
        case 11:
          return "getHiSyncId";
        case 10:
          return "setVolume";
        case 9:
          return "getConnectionPolicy";
        case 8:
          return "setConnectionPolicy";
        case 7:
          return "getActiveDevices";
        case 6:
          return "setActiveDevice";
        case 5:
          return "getConnectionState";
        case 4:
          return "getDevicesMatchingConnectionStates";
        case 3:
          return "getConnectedDevices";
        case 2:
          return "disconnect";
        case 1:
          break;
      } 
      return "connect";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int i;
        List<BluetoothDevice> list2;
        int[] arrayOfInt;
        List<BluetoothDevice> list1;
        long l;
        BluetoothDevice bluetoothDevice;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 13:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothHearingAid");
            if (param1Parcel1.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            param1Int1 = getDeviceMode((BluetoothDevice)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 12:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothHearingAid");
            if (param1Parcel1.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            param1Int1 = getDeviceSide((BluetoothDevice)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 11:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothHearingAid");
            if (param1Parcel1.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            l = getHiSyncId((BluetoothDevice)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 10:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothHearingAid");
            param1Int1 = param1Parcel1.readInt();
            setVolume(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothHearingAid");
            if (param1Parcel1.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            param1Int1 = getConnectionPolicy((BluetoothDevice)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothHearingAid");
            if (param1Parcel1.readInt() != 0) {
              bluetoothDevice = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(param1Parcel1);
            } else {
              bluetoothDevice = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            bool2 = setConnectionPolicy(bluetoothDevice, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.bluetooth.IBluetoothHearingAid");
            list2 = getActiveDevices();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list2);
            return true;
          case 6:
            list2.enforceInterface("android.bluetooth.IBluetoothHearingAid");
            if (list2.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)list2);
            } else {
              list2 = null;
            } 
            bool2 = setActiveDevice((BluetoothDevice)list2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 5:
            list2.enforceInterface("android.bluetooth.IBluetoothHearingAid");
            if (list2.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)list2);
            } else {
              list2 = null;
            } 
            i = getConnectionState((BluetoothDevice)list2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 4:
            list2.enforceInterface("android.bluetooth.IBluetoothHearingAid");
            arrayOfInt = list2.createIntArray();
            list1 = getDevicesMatchingConnectionStates(arrayOfInt);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list1);
            return true;
          case 3:
            list1.enforceInterface("android.bluetooth.IBluetoothHearingAid");
            list1 = getConnectedDevices();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list1);
            return true;
          case 2:
            list1.enforceInterface("android.bluetooth.IBluetoothHearingAid");
            if (list1.readInt() != 0) {
              BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)list1);
            } else {
              list1 = null;
            } 
            bool1 = disconnect((BluetoothDevice)list1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        list1.enforceInterface("android.bluetooth.IBluetoothHearingAid");
        if (list1.readInt() != 0) {
          BluetoothDevice bluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel((Parcel)list1);
        } else {
          list1 = null;
        } 
        boolean bool1 = connect((BluetoothDevice)list1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("android.bluetooth.IBluetoothHearingAid");
      return true;
    }
    
    private static class Proxy implements IBluetoothHearingAid {
      public static IBluetoothHearingAid sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.bluetooth.IBluetoothHearingAid";
      }
      
      public boolean connect(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHearingAid");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHearingAid.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHearingAid.Stub.getDefaultImpl().connect(param2BluetoothDevice);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean disconnect(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHearingAid");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHearingAid.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHearingAid.Stub.getDefaultImpl().disconnect(param2BluetoothDevice);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<BluetoothDevice> getConnectedDevices() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHearingAid");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IBluetoothHearingAid.Stub.getDefaultImpl() != null)
            return IBluetoothHearingAid.Stub.getDefaultImpl().getConnectedDevices(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(BluetoothDevice.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHearingAid");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IBluetoothHearingAid.Stub.getDefaultImpl() != null)
            return IBluetoothHearingAid.Stub.getDefaultImpl().getDevicesMatchingConnectionStates(param2ArrayOfint); 
          parcel2.readException();
          return parcel2.createTypedArrayList(BluetoothDevice.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getConnectionState(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHearingAid");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IBluetoothHearingAid.Stub.getDefaultImpl() != null)
            return IBluetoothHearingAid.Stub.getDefaultImpl().getConnectionState(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setActiveDevice(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHearingAid");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHearingAid.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHearingAid.Stub.getDefaultImpl().setActiveDevice(param2BluetoothDevice);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<BluetoothDevice> getActiveDevices() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHearingAid");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IBluetoothHearingAid.Stub.getDefaultImpl() != null)
            return IBluetoothHearingAid.Stub.getDefaultImpl().getActiveDevices(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(BluetoothDevice.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setConnectionPolicy(BluetoothDevice param2BluetoothDevice, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHearingAid");
          boolean bool1 = true;
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool2 && IBluetoothHearingAid.Stub.getDefaultImpl() != null) {
            bool1 = IBluetoothHearingAid.Stub.getDefaultImpl().setConnectionPolicy(param2BluetoothDevice, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getConnectionPolicy(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHearingAid");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IBluetoothHearingAid.Stub.getDefaultImpl() != null)
            return IBluetoothHearingAid.Stub.getDefaultImpl().getConnectionPolicy(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVolume(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHearingAid");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IBluetoothHearingAid.Stub.getDefaultImpl() != null) {
            IBluetoothHearingAid.Stub.getDefaultImpl().setVolume(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getHiSyncId(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHearingAid");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IBluetoothHearingAid.Stub.getDefaultImpl() != null)
            return IBluetoothHearingAid.Stub.getDefaultImpl().getHiSyncId(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDeviceSide(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHearingAid");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IBluetoothHearingAid.Stub.getDefaultImpl() != null)
            return IBluetoothHearingAid.Stub.getDefaultImpl().getDeviceSide(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDeviceMode(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.bluetooth.IBluetoothHearingAid");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IBluetoothHearingAid.Stub.getDefaultImpl() != null)
            return IBluetoothHearingAid.Stub.getDefaultImpl().getDeviceMode(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IBluetoothHearingAid param1IBluetoothHearingAid) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IBluetoothHearingAid != null) {
          Proxy.sDefaultImpl = param1IBluetoothHearingAid;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IBluetoothHearingAid getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
