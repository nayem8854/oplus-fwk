package android.database;

import android.content.ContentResolver;
import android.net.Uri;
import android.os.Bundle;
import java.io.Closeable;
import java.util.Arrays;
import java.util.List;

public interface Cursor extends Closeable {
  public static final int FIELD_TYPE_BLOB = 4;
  
  public static final int FIELD_TYPE_FLOAT = 2;
  
  public static final int FIELD_TYPE_INTEGER = 1;
  
  public static final int FIELD_TYPE_NULL = 0;
  
  public static final int FIELD_TYPE_STRING = 3;
  
  void unregisterDataSetObserver(DataSetObserver paramDataSetObserver);
  
  void unregisterContentObserver(ContentObserver paramContentObserver);
  
  default void setNotificationUris(ContentResolver paramContentResolver, List<Uri> paramList) {
    setNotificationUri(paramContentResolver, paramList.get(0));
  }
  
  void setNotificationUri(ContentResolver paramContentResolver, Uri paramUri);
  
  void setExtras(Bundle paramBundle);
  
  Bundle respond(Bundle paramBundle);
  
  @Deprecated
  boolean requery();
  
  void registerDataSetObserver(DataSetObserver paramDataSetObserver);
  
  void registerContentObserver(ContentObserver paramContentObserver);
  
  boolean moveToPrevious();
  
  boolean moveToPosition(int paramInt);
  
  boolean moveToNext();
  
  boolean moveToLast();
  
  boolean moveToFirst();
  
  boolean move(int paramInt);
  
  boolean isNull(int paramInt);
  
  boolean isLast();
  
  boolean isFirst();
  
  boolean isClosed();
  
  boolean isBeforeFirst();
  
  boolean isAfterLast();
  
  boolean getWantsAllOnMoveCalls();
  
  int getType(int paramInt);
  
  String getString(int paramInt);
  
  short getShort(int paramInt);
  
  int getPosition();
  
  default List<Uri> getNotificationUris() {
    List<Uri> list;
    Uri uri = getNotificationUri();
    if (uri == null) {
      uri = null;
    } else {
      list = Arrays.asList(new Uri[] { uri });
    } 
    return list;
  }
  
  Uri getNotificationUri();
  
  long getLong(int paramInt);
  
  int getInt(int paramInt);
  
  float getFloat(int paramInt);
  
  Bundle getExtras();
  
  double getDouble(int paramInt);
  
  int getCount();
  
  String[] getColumnNames();
  
  String getColumnName(int paramInt);
  
  int getColumnIndexOrThrow(String paramString) throws IllegalArgumentException;
  
  int getColumnIndex(String paramString);
  
  int getColumnCount();
  
  byte[] getBlob(int paramInt);
  
  @Deprecated
  void deactivate();
  
  void copyStringToBuffer(int paramInt, CharArrayBuffer paramCharArrayBuffer);
  
  void close();
}
