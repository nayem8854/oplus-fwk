package android.database;

import java.util.ArrayList;

public class MatrixCursor extends AbstractCursor {
  private final int columnCount;
  
  private final String[] columnNames;
  
  private Object[] data;
  
  private int rowCount = 0;
  
  public MatrixCursor(String[] paramArrayOfString, int paramInt) {
    this.columnNames = paramArrayOfString;
    this.columnCount = paramArrayOfString.length;
    int i = paramInt;
    if (paramInt < 1)
      i = 1; 
    this.data = new Object[this.columnCount * i];
  }
  
  public MatrixCursor(String[] paramArrayOfString) {
    this(paramArrayOfString, 16);
  }
  
  private Object get(int paramInt) {
    if (paramInt >= 0 && paramInt < this.columnCount) {
      if (this.mPos >= 0) {
        if (this.mPos < this.rowCount)
          return this.data[this.mPos * this.columnCount + paramInt]; 
        throw new CursorIndexOutOfBoundsException("After last row.");
      } 
      throw new CursorIndexOutOfBoundsException("Before first row.");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Requested column: ");
    stringBuilder.append(paramInt);
    stringBuilder.append(", # of columns: ");
    stringBuilder.append(this.columnCount);
    throw new CursorIndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public RowBuilder newRow() {
    int i = this.rowCount, j = i + 1;
    int k = this.columnCount;
    ensureCapacity(j * k);
    return new RowBuilder(i);
  }
  
  public void addRow(Object[] paramArrayOfObject) {
    int i = paramArrayOfObject.length, j = this.columnCount;
    if (i == j) {
      i = this.rowCount;
      this.rowCount = i + 1;
      i *= j;
      ensureCapacity(j + i);
      System.arraycopy(paramArrayOfObject, 0, this.data, i, this.columnCount);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("columnNames.length = ");
    stringBuilder.append(this.columnCount);
    stringBuilder.append(", columnValues.length = ");
    stringBuilder.append(paramArrayOfObject.length);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void addRow(Iterable<?> paramIterable) {
    int i = this.rowCount, j = this.columnCount;
    i *= j;
    j += i;
    ensureCapacity(j);
    if (paramIterable instanceof ArrayList) {
      addRow((ArrayList)paramIterable, i);
      return;
    } 
    Object[] arrayOfObject = this.data;
    for (Object object : paramIterable) {
      if (i != j) {
        arrayOfObject[i] = object;
        i++;
        continue;
      } 
      throw new IllegalArgumentException("columnValues.size() > columnNames.length");
    } 
    if (i == j) {
      this.rowCount++;
      return;
    } 
    throw new IllegalArgumentException("columnValues.size() < columnNames.length");
  }
  
  private void addRow(ArrayList<?> paramArrayList, int paramInt) {
    int i = paramArrayList.size();
    if (i == this.columnCount) {
      this.rowCount++;
      Object[] arrayOfObject = this.data;
      for (byte b = 0; b < i; b++)
        arrayOfObject[paramInt + b] = paramArrayList.get(b); 
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("columnNames.length = ");
    stringBuilder.append(this.columnCount);
    stringBuilder.append(", columnValues.size() = ");
    stringBuilder.append(i);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private void ensureCapacity(int paramInt) {
    Object[] arrayOfObject = this.data;
    if (paramInt > arrayOfObject.length) {
      Object[] arrayOfObject1 = this.data;
      int i = arrayOfObject.length * 2;
      int j = i;
      if (i < paramInt)
        j = paramInt; 
      this.data = arrayOfObject = new Object[j];
      System.arraycopy(arrayOfObject1, 0, arrayOfObject, 0, arrayOfObject1.length);
    } 
  }
  
  class RowBuilder {
    private final int endIndex;
    
    private int index;
    
    private final int row;
    
    final MatrixCursor this$0;
    
    RowBuilder(int param1Int) {
      this.row = param1Int;
      this.index = param1Int = MatrixCursor.this.columnCount * param1Int;
      this.endIndex = param1Int + MatrixCursor.this.columnCount;
    }
    
    public RowBuilder add(Object param1Object) {
      if (this.index != this.endIndex) {
        Object[] arrayOfObject = MatrixCursor.this.data;
        int i = this.index;
        this.index = i + 1;
        arrayOfObject[i] = param1Object;
        return this;
      } 
      throw new CursorIndexOutOfBoundsException("No more columns left.");
    }
    
    public RowBuilder add(String param1String, Object param1Object) {
      for (byte b = 0; b < MatrixCursor.this.columnNames.length; b++) {
        if (param1String.equals(MatrixCursor.this.columnNames[b]))
          MatrixCursor.this.data[this.row * MatrixCursor.this.columnCount + b] = param1Object; 
      } 
      return this;
    }
    
    public final RowBuilder add(int param1Int, Object param1Object) {
      MatrixCursor.this.data[this.row * MatrixCursor.this.columnCount + param1Int] = param1Object;
      return this;
    }
  }
  
  public int getCount() {
    return this.rowCount;
  }
  
  public String[] getColumnNames() {
    return this.columnNames;
  }
  
  public String getString(int paramInt) {
    Object object = get(paramInt);
    if (object == null)
      return null; 
    return object.toString();
  }
  
  public short getShort(int paramInt) {
    Object object = get(paramInt);
    if (object == null)
      return 0; 
    if (object instanceof Number)
      return ((Number)object).shortValue(); 
    return Short.parseShort(object.toString());
  }
  
  public int getInt(int paramInt) {
    Object object = get(paramInt);
    if (object == null)
      return 0; 
    if (object instanceof Number)
      return ((Number)object).intValue(); 
    return Integer.parseInt(object.toString());
  }
  
  public long getLong(int paramInt) {
    Object object = get(paramInt);
    if (object == null)
      return 0L; 
    if (object instanceof Number)
      return ((Number)object).longValue(); 
    return Long.parseLong(object.toString());
  }
  
  public float getFloat(int paramInt) {
    Object object = get(paramInt);
    if (object == null)
      return 0.0F; 
    if (object instanceof Number)
      return ((Number)object).floatValue(); 
    return Float.parseFloat(object.toString());
  }
  
  public double getDouble(int paramInt) {
    Object object = get(paramInt);
    if (object == null)
      return 0.0D; 
    if (object instanceof Number)
      return ((Number)object).doubleValue(); 
    return Double.parseDouble(object.toString());
  }
  
  public byte[] getBlob(int paramInt) {
    Object object = get(paramInt);
    return (byte[])object;
  }
  
  public int getType(int paramInt) {
    return DatabaseUtils.getTypeOfObject(get(paramInt));
  }
  
  public boolean isNull(int paramInt) {
    boolean bool;
    if (get(paramInt) == null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
