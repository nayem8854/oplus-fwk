package android.database;

import android.content.ContentResolver;
import android.net.Uri;
import android.os.Bundle;
import android.os.UserHandle;
import java.util.List;

public class CursorWrapper implements Cursor {
  protected final Cursor mCursor;
  
  public CursorWrapper(Cursor paramCursor) {
    this.mCursor = paramCursor;
  }
  
  public Cursor getWrappedCursor() {
    return this.mCursor;
  }
  
  public void close() {
    this.mCursor.close();
  }
  
  public boolean isClosed() {
    return this.mCursor.isClosed();
  }
  
  public int getCount() {
    return this.mCursor.getCount();
  }
  
  @Deprecated
  public void deactivate() {
    this.mCursor.deactivate();
  }
  
  public boolean moveToFirst() {
    return this.mCursor.moveToFirst();
  }
  
  public int getColumnCount() {
    return this.mCursor.getColumnCount();
  }
  
  public int getColumnIndex(String paramString) {
    return this.mCursor.getColumnIndex(paramString);
  }
  
  public int getColumnIndexOrThrow(String paramString) throws IllegalArgumentException {
    return this.mCursor.getColumnIndexOrThrow(paramString);
  }
  
  public String getColumnName(int paramInt) {
    return this.mCursor.getColumnName(paramInt);
  }
  
  public String[] getColumnNames() {
    return this.mCursor.getColumnNames();
  }
  
  public double getDouble(int paramInt) {
    return this.mCursor.getDouble(paramInt);
  }
  
  public void setExtras(Bundle paramBundle) {
    this.mCursor.setExtras(paramBundle);
  }
  
  public Bundle getExtras() {
    return this.mCursor.getExtras();
  }
  
  public float getFloat(int paramInt) {
    return this.mCursor.getFloat(paramInt);
  }
  
  public int getInt(int paramInt) {
    return this.mCursor.getInt(paramInt);
  }
  
  public long getLong(int paramInt) {
    return this.mCursor.getLong(paramInt);
  }
  
  public short getShort(int paramInt) {
    return this.mCursor.getShort(paramInt);
  }
  
  public String getString(int paramInt) {
    if (UserHandle.myUserId() != 999 || 
      !"_data".equals(getColumnName(paramInt)))
      return this.mCursor.getString(paramInt); 
    String str = this.mCursor.getString(paramInt);
    if (str == null)
      return str; 
    if (str.startsWith("/storage/emulated/0"))
      return str.replace("/storage/emulated/0", "/storage/ace-0"); 
    if (str.startsWith("/storage/ace-999"))
      return str.replace("/storage/ace-999", "/storage/emulated/999"); 
    return str;
  }
  
  public void copyStringToBuffer(int paramInt, CharArrayBuffer paramCharArrayBuffer) {
    this.mCursor.copyStringToBuffer(paramInt, paramCharArrayBuffer);
  }
  
  public byte[] getBlob(int paramInt) {
    return this.mCursor.getBlob(paramInt);
  }
  
  public boolean getWantsAllOnMoveCalls() {
    return this.mCursor.getWantsAllOnMoveCalls();
  }
  
  public boolean isAfterLast() {
    return this.mCursor.isAfterLast();
  }
  
  public boolean isBeforeFirst() {
    return this.mCursor.isBeforeFirst();
  }
  
  public boolean isFirst() {
    return this.mCursor.isFirst();
  }
  
  public boolean isLast() {
    return this.mCursor.isLast();
  }
  
  public int getType(int paramInt) {
    return this.mCursor.getType(paramInt);
  }
  
  public boolean isNull(int paramInt) {
    return this.mCursor.isNull(paramInt);
  }
  
  public boolean moveToLast() {
    return this.mCursor.moveToLast();
  }
  
  public boolean move(int paramInt) {
    return this.mCursor.move(paramInt);
  }
  
  public boolean moveToPosition(int paramInt) {
    return this.mCursor.moveToPosition(paramInt);
  }
  
  public boolean moveToNext() {
    return this.mCursor.moveToNext();
  }
  
  public int getPosition() {
    return this.mCursor.getPosition();
  }
  
  public boolean moveToPrevious() {
    return this.mCursor.moveToPrevious();
  }
  
  public void registerContentObserver(ContentObserver paramContentObserver) {
    this.mCursor.registerContentObserver(paramContentObserver);
  }
  
  public void registerDataSetObserver(DataSetObserver paramDataSetObserver) {
    this.mCursor.registerDataSetObserver(paramDataSetObserver);
  }
  
  @Deprecated
  public boolean requery() {
    return this.mCursor.requery();
  }
  
  public Bundle respond(Bundle paramBundle) {
    return this.mCursor.respond(paramBundle);
  }
  
  public void setNotificationUri(ContentResolver paramContentResolver, Uri paramUri) {
    this.mCursor.setNotificationUri(paramContentResolver, paramUri);
  }
  
  public void setNotificationUris(ContentResolver paramContentResolver, List<Uri> paramList) {
    this.mCursor.setNotificationUris(paramContentResolver, paramList);
  }
  
  public Uri getNotificationUri() {
    return this.mCursor.getNotificationUri();
  }
  
  public List<Uri> getNotificationUris() {
    return this.mCursor.getNotificationUris();
  }
  
  public void unregisterContentObserver(ContentObserver paramContentObserver) {
    this.mCursor.unregisterContentObserver(paramContentObserver);
  }
  
  public void unregisterDataSetObserver(DataSetObserver paramDataSetObserver) {
    this.mCursor.unregisterDataSetObserver(paramDataSetObserver);
  }
}
