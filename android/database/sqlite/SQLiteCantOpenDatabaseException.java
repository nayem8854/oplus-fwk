package android.database.sqlite;

public class SQLiteCantOpenDatabaseException extends SQLiteException {
  public SQLiteCantOpenDatabaseException() {}
  
  public SQLiteCantOpenDatabaseException(String paramString) {
    super(paramString);
  }
  
  public SQLiteCantOpenDatabaseException(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
  }
}
