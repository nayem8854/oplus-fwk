package android.database.sqlite;

import android.database.Cursor;

public interface SQLiteCursorDriver {
  void cursorClosed();
  
  void cursorDeactivated();
  
  void cursorRequeried(Cursor paramCursor);
  
  Cursor query(SQLiteDatabase.CursorFactory paramCursorFactory, String[] paramArrayOfString);
  
  void setBindArguments(String[] paramArrayOfString);
}
