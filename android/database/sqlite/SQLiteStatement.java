package android.database.sqlite;

import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;

public final class SQLiteStatement extends SQLiteProgram {
  SQLiteStatement(SQLiteDatabase paramSQLiteDatabase, String paramString, Object[] paramArrayOfObject) {
    super(paramSQLiteDatabase, paramString, paramArrayOfObject, (CancellationSignal)null);
  }
  
  public void execute() {
    Exception exception;
    acquireReference();
    try {
      getSession().execute(getSql(), getBindArgs(), getConnectionFlags(), null);
      releaseReference();
      return;
    } catch (SQLiteDatabaseCorruptException null) {
      onCorruption();
      throw exception;
    } finally {}
    releaseReference();
    throw exception;
  }
  
  public int executeUpdateDelete() {
    Exception exception;
    acquireReference();
    try {
      SQLiteSession sQLiteSession = getSession();
      String str = getSql();
      Object[] arrayOfObject = getBindArgs();
      int i = getConnectionFlags();
      i = sQLiteSession.executeForChangedRowCount(str, arrayOfObject, i, null);
      releaseReference();
      return i;
    } catch (SQLiteDatabaseCorruptException null) {
      onCorruption();
      throw exception;
    } finally {}
    releaseReference();
    throw exception;
  }
  
  public long executeInsert() {
    Exception exception;
    acquireReference();
    try {
      SQLiteSession sQLiteSession = getSession();
      String str = getSql();
      Object[] arrayOfObject = getBindArgs();
      int i = getConnectionFlags();
      long l = sQLiteSession.executeForLastInsertedRowId(str, arrayOfObject, i, null);
      releaseReference();
      return l;
    } catch (SQLiteDatabaseCorruptException null) {
      onCorruption();
      throw exception;
    } finally {}
    releaseReference();
    throw exception;
  }
  
  public long simpleQueryForLong() {
    Exception exception;
    acquireReference();
    try {
      SQLiteSession sQLiteSession = getSession();
      String str = getSql();
      Object[] arrayOfObject = getBindArgs();
      int i = getConnectionFlags();
      long l = sQLiteSession.executeForLong(str, arrayOfObject, i, null);
      releaseReference();
      return l;
    } catch (SQLiteDatabaseCorruptException null) {
      onCorruption();
      throw exception;
    } finally {}
    releaseReference();
    throw exception;
  }
  
  public String simpleQueryForString() {
    Exception exception;
    acquireReference();
    try {
      SQLiteSession sQLiteSession = getSession();
      String str2 = getSql();
      Object[] arrayOfObject = getBindArgs();
      int i = getConnectionFlags();
      String str1 = sQLiteSession.executeForString(str2, arrayOfObject, i, null);
      releaseReference();
      return str1;
    } catch (SQLiteDatabaseCorruptException null) {
      onCorruption();
      throw exception;
    } finally {}
    releaseReference();
    throw exception;
  }
  
  public ParcelFileDescriptor simpleQueryForBlobFileDescriptor() {
    Exception exception;
    acquireReference();
    try {
      SQLiteSession sQLiteSession = getSession();
      String str = getSql();
      Object[] arrayOfObject = getBindArgs();
      int i = getConnectionFlags();
      ParcelFileDescriptor parcelFileDescriptor = sQLiteSession.executeForBlobFileDescriptor(str, arrayOfObject, i, null);
      releaseReference();
      return parcelFileDescriptor;
    } catch (SQLiteDatabaseCorruptException null) {
      onCorruption();
      throw exception;
    } finally {}
    releaseReference();
    throw exception;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SQLiteProgram: ");
    stringBuilder.append(getSql());
    return stringBuilder.toString();
  }
}
