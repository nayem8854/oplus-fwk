package android.database.sqlite;

import android.os.CancellationSignal;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.OperationCanceledException;
import android.os.SystemClock;
import android.util.ArraySet;
import android.util.Log;
import android.util.Printer;
import dalvik.system.CloseGuard;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.LockSupport;

public final class SQLiteConnectionPool implements Closeable {
  private final CloseGuard mCloseGuard = CloseGuard.get();
  
  private final Object mLock = new Object();
  
  private final AtomicBoolean mConnectionLeaked = new AtomicBoolean();
  
  private final ArrayList<SQLiteConnection> mAvailableNonPrimaryConnections = new ArrayList<>();
  
  private final AtomicLong mTotalExecutionTimeCounter = new AtomicLong(0L);
  
  enum AcquiredConnectionStatus {
    DISCARD, NORMAL, RECONFIGURE;
    
    private static final AcquiredConnectionStatus[] $VALUES;
    
    static {
      AcquiredConnectionStatus acquiredConnectionStatus = new AcquiredConnectionStatus("DISCARD", 2);
      $VALUES = new AcquiredConnectionStatus[] { NORMAL, RECONFIGURE, acquiredConnectionStatus };
    }
  }
  
  private final WeakHashMap<SQLiteConnection, AcquiredConnectionStatus> mAcquiredConnections = new WeakHashMap<>();
  
  static final boolean $assertionsDisabled = false;
  
  public static final int CONNECTION_FLAG_INTERACTIVE = 4;
  
  public static final int CONNECTION_FLAG_PRIMARY_CONNECTION_AFFINITY = 2;
  
  public static final int CONNECTION_FLAG_READ_ONLY = 1;
  
  private static final long CONNECTION_POOL_BUSY_MILLIS = 30000L;
  
  private static final String TAG = "SQLiteConnectionPool";
  
  private SQLiteConnection mAvailablePrimaryConnection;
  
  private final SQLiteDatabaseConfiguration mConfiguration;
  
  private ConnectionWaiter mConnectionWaiterPool;
  
  private ConnectionWaiter mConnectionWaiterQueue;
  
  private IdleConnectionHandler mIdleConnectionHandler;
  
  private boolean mIsOpen;
  
  private int mMaxConnectionPoolSize;
  
  private int mNextConnectionId;
  
  private SQLiteConnectionPool(SQLiteDatabaseConfiguration paramSQLiteDatabaseConfiguration) {
    this.mConfiguration = new SQLiteDatabaseConfiguration(paramSQLiteDatabaseConfiguration);
    setMaxConnectionPoolSizeLocked();
    if (this.mConfiguration.idleConnectionTimeoutMs != Long.MAX_VALUE)
      setupIdleConnectionHandler(Looper.getMainLooper(), this.mConfiguration.idleConnectionTimeoutMs); 
  }
  
  protected void finalize() throws Throwable {
    try {
      dispose(true);
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public static SQLiteConnectionPool open(SQLiteDatabaseConfiguration paramSQLiteDatabaseConfiguration) {
    if (paramSQLiteDatabaseConfiguration != null) {
      SQLiteConnectionPool sQLiteConnectionPool = new SQLiteConnectionPool(paramSQLiteDatabaseConfiguration);
      sQLiteConnectionPool.open();
      return sQLiteConnectionPool;
    } 
    throw new IllegalArgumentException("configuration must not be null.");
  }
  
  private void open() {
    this.mAvailablePrimaryConnection = openConnectionLocked(this.mConfiguration, true);
    synchronized (this.mLock) {
      if (this.mIdleConnectionHandler != null)
        this.mIdleConnectionHandler.connectionReleased(this.mAvailablePrimaryConnection); 
      this.mIsOpen = true;
      this.mCloseGuard.open("close");
      return;
    } 
  }
  
  public void close() {
    dispose(false);
  }
  
  private void dispose(boolean paramBoolean) {
    CloseGuard closeGuard = this.mCloseGuard;
    if (closeGuard != null) {
      if (paramBoolean)
        closeGuard.warnIfOpen(); 
      this.mCloseGuard.close();
    } 
    if (!paramBoolean)
      synchronized (this.mLock) {
        throwIfClosedLocked();
        this.mIsOpen = false;
        closeAvailableConnectionsAndLogExceptionsLocked();
        int i = this.mAcquiredConnections.size();
        if (i != 0) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("The connection pool for ");
          stringBuilder.append(this.mConfiguration.label);
          stringBuilder.append(" has been closed but there are still ");
          stringBuilder.append(i);
          stringBuilder.append(" connections in use.  They will be closed as they are released back to the pool.");
          Log.i("SQLiteConnectionPool", stringBuilder.toString());
        } 
        wakeConnectionWaitersLocked();
      }  
  }
  
  public void reconfigure(SQLiteDatabaseConfiguration paramSQLiteDatabaseConfiguration) {
    if (paramSQLiteDatabaseConfiguration != null)
      synchronized (this.mLock) {
        IllegalStateException illegalStateException;
        throwIfClosedLocked();
        int i = paramSQLiteDatabaseConfiguration.openFlags, j = this.mConfiguration.openFlags;
        boolean bool = false;
        if (((i ^ j) & 0x20000000) != 0) {
          j = 1;
        } else {
          j = 0;
        } 
        if (j != 0)
          if (this.mAcquiredConnections.isEmpty()) {
            closeAvailableNonPrimaryConnectionsAndLogExceptionsLocked();
          } else {
            illegalStateException = new IllegalStateException();
            this("Write Ahead Logging (WAL) mode cannot be enabled or disabled while there are transactions in progress.  Finish all transactions and release all active database connections first.");
            throw illegalStateException;
          }  
        if (((SQLiteDatabaseConfiguration)illegalStateException).foreignKeyConstraintsEnabled != this.mConfiguration.foreignKeyConstraintsEnabled) {
          i = 1;
        } else {
          i = 0;
        } 
        if (i != 0)
          if (!this.mAcquiredConnections.isEmpty()) {
            illegalStateException = new IllegalStateException();
            this("Foreign Key Constraints cannot be enabled or disabled while there are transactions in progress.  Finish all transactions and release all active database connections first.");
            throw illegalStateException;
          }  
        i = bool;
        if ((this.mConfiguration.openFlags ^ ((SQLiteDatabaseConfiguration)illegalStateException).openFlags) == Integer.MIN_VALUE)
          i = 1; 
        if (i == 0 && this.mConfiguration.openFlags != ((SQLiteDatabaseConfiguration)illegalStateException).openFlags) {
          if (j != 0)
            closeAvailableConnectionsAndLogExceptionsLocked(); 
          SQLiteConnection sQLiteConnection = openConnectionLocked((SQLiteDatabaseConfiguration)illegalStateException, true);
          closeAvailableConnectionsAndLogExceptionsLocked();
          discardAcquiredConnectionsLocked();
          this.mAvailablePrimaryConnection = sQLiteConnection;
          this.mConfiguration.updateParametersFrom((SQLiteDatabaseConfiguration)illegalStateException);
          setMaxConnectionPoolSizeLocked();
        } else {
          this.mConfiguration.updateParametersFrom((SQLiteDatabaseConfiguration)illegalStateException);
          setMaxConnectionPoolSizeLocked();
          closeExcessConnectionsAndLogExceptionsLocked();
          reconfigureAllConnectionsLocked();
        } 
        wakeConnectionWaitersLocked();
        return;
      }  
    throw new IllegalArgumentException("configuration must not be null.");
  }
  
  public SQLiteConnection acquireConnection(String paramString, int paramInt, CancellationSignal paramCancellationSignal) {
    long l = SystemClock.uptimeMillis();
    SQLiteConnection sQLiteConnection = waitForConnection(paramString, paramInt, paramCancellationSignal);
    synchronized (this.mLock) {
      boolean bool;
      if (this.mIdleConnectionHandler != null)
        this.mIdleConnectionHandler.connectionAcquired(sQLiteConnection); 
      long l1 = SystemClock.uptimeMillis();
      if ((paramInt & 0x2) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      OplusBaseSQLiteDebug.onConnectionObtained(this.mConfiguration.label, paramString, l1 - l, bool);
      return sQLiteConnection;
    } 
  }
  
  public void releaseConnection(SQLiteConnection paramSQLiteConnection) {
    synchronized (this.mLock) {
      if (this.mIdleConnectionHandler != null)
        this.mIdleConnectionHandler.connectionReleased(paramSQLiteConnection); 
      AcquiredConnectionStatus acquiredConnectionStatus = this.mAcquiredConnections.remove(paramSQLiteConnection);
      if (acquiredConnectionStatus != null) {
        if (!this.mIsOpen) {
          closeConnectionAndLogExceptionsLocked(paramSQLiteConnection);
        } else if (paramSQLiteConnection.isPrimaryConnection()) {
          if (recycleConnectionLocked(paramSQLiteConnection, acquiredConnectionStatus))
            this.mAvailablePrimaryConnection = paramSQLiteConnection; 
          wakeConnectionWaitersLocked();
        } else if (this.mAvailableNonPrimaryConnections.size() >= this.mMaxConnectionPoolSize - 1) {
          closeConnectionAndLogExceptionsLocked(paramSQLiteConnection);
        } else {
          if (recycleConnectionLocked(paramSQLiteConnection, acquiredConnectionStatus))
            this.mAvailableNonPrimaryConnections.add(paramSQLiteConnection); 
          wakeConnectionWaitersLocked();
        } 
        return;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Cannot perform this operation because the specified connection was not acquired from this pool or has already been released.");
      throw illegalStateException;
    } 
  }
  
  private boolean recycleConnectionLocked(SQLiteConnection paramSQLiteConnection, AcquiredConnectionStatus paramAcquiredConnectionStatus) {
    AcquiredConnectionStatus acquiredConnectionStatus = paramAcquiredConnectionStatus;
    if (paramAcquiredConnectionStatus == AcquiredConnectionStatus.RECONFIGURE)
      try {
        paramSQLiteConnection.reconfigure(this.mConfiguration);
        acquiredConnectionStatus = paramAcquiredConnectionStatus;
      } catch (RuntimeException runtimeException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to reconfigure released connection, closing it: ");
        stringBuilder.append(paramSQLiteConnection);
        Log.e("SQLiteConnectionPool", stringBuilder.toString(), runtimeException);
        acquiredConnectionStatus = AcquiredConnectionStatus.DISCARD;
      }  
    if (acquiredConnectionStatus == AcquiredConnectionStatus.DISCARD) {
      closeConnectionAndLogExceptionsLocked(paramSQLiteConnection);
      return false;
    } 
    return true;
  }
  
  public boolean shouldYieldConnection(SQLiteConnection paramSQLiteConnection, int paramInt) {
    synchronized (this.mLock) {
      if (this.mAcquiredConnections.containsKey(paramSQLiteConnection)) {
        if (!this.mIsOpen)
          return false; 
        boolean bool = paramSQLiteConnection.isPrimaryConnection();
        bool = isSessionBlockingImportantConnectionWaitersLocked(bool, paramInt);
        return bool;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Cannot perform this operation because the specified connection was not acquired from this pool or has already been released.");
      throw illegalStateException;
    } 
  }
  
  public void collectDbStats(ArrayList<SQLiteDebug.DbStats> paramArrayList) {
    synchronized (this.mLock) {
      if (this.mAvailablePrimaryConnection != null)
        this.mAvailablePrimaryConnection.collectDbStats(paramArrayList); 
      for (SQLiteConnection sQLiteConnection : this.mAvailableNonPrimaryConnections)
        sQLiteConnection.collectDbStats(paramArrayList); 
      for (SQLiteConnection sQLiteConnection : this.mAcquiredConnections.keySet())
        sQLiteConnection.collectDbStatsUnsafe(paramArrayList); 
      return;
    } 
  }
  
  private SQLiteConnection openConnectionLocked(SQLiteDatabaseConfiguration paramSQLiteDatabaseConfiguration, boolean paramBoolean) {
    int i = this.mNextConnectionId;
    this.mNextConnectionId = i + 1;
    return SQLiteConnection.open(this, paramSQLiteDatabaseConfiguration, i, paramBoolean);
  }
  
  void onConnectionLeaked() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("A SQLiteConnection object for database '");
    stringBuilder.append(this.mConfiguration.label);
    stringBuilder.append("' was leaked!  Please fix your application to end transactions in progress properly and to close the database when it is no longer needed.");
    Log.w("SQLiteConnectionPool", stringBuilder.toString());
    this.mConnectionLeaked.set(true);
  }
  
  void onStatementExecuted(long paramLong) {
    this.mTotalExecutionTimeCounter.addAndGet(paramLong);
  }
  
  private void closeAvailableConnectionsAndLogExceptionsLocked() {
    closeAvailableNonPrimaryConnectionsAndLogExceptionsLocked();
    SQLiteConnection sQLiteConnection = this.mAvailablePrimaryConnection;
    if (sQLiteConnection != null) {
      closeConnectionAndLogExceptionsLocked(sQLiteConnection);
      this.mAvailablePrimaryConnection = null;
    } 
  }
  
  private boolean closeAvailableConnectionLocked(int paramInt) {
    int i = this.mAvailableNonPrimaryConnections.size();
    for (; --i >= 0; i--) {
      SQLiteConnection sQLiteConnection1 = this.mAvailableNonPrimaryConnections.get(i);
      if (sQLiteConnection1.getConnectionId() == paramInt) {
        closeConnectionAndLogExceptionsLocked(sQLiteConnection1);
        this.mAvailableNonPrimaryConnections.remove(i);
        return true;
      } 
    } 
    SQLiteConnection sQLiteConnection = this.mAvailablePrimaryConnection;
    if (sQLiteConnection != null && 
      sQLiteConnection.getConnectionId() == paramInt) {
      closeConnectionAndLogExceptionsLocked(this.mAvailablePrimaryConnection);
      this.mAvailablePrimaryConnection = null;
      return true;
    } 
    return false;
  }
  
  private void closeAvailableNonPrimaryConnectionsAndLogExceptionsLocked() {
    int i = this.mAvailableNonPrimaryConnections.size();
    for (byte b = 0; b < i; b++)
      closeConnectionAndLogExceptionsLocked(this.mAvailableNonPrimaryConnections.get(b)); 
    this.mAvailableNonPrimaryConnections.clear();
  }
  
  void closeAvailableNonPrimaryConnectionsAndLogExceptions() {
    synchronized (this.mLock) {
      closeAvailableNonPrimaryConnectionsAndLogExceptionsLocked();
      return;
    } 
  }
  
  private void closeExcessConnectionsAndLogExceptionsLocked() {
    int i = this.mAvailableNonPrimaryConnections.size();
    while (true) {
      int j = i - 1;
      if (i > this.mMaxConnectionPoolSize - 1) {
        ArrayList<SQLiteConnection> arrayList = this.mAvailableNonPrimaryConnections;
        SQLiteConnection sQLiteConnection = arrayList.remove(j);
        closeConnectionAndLogExceptionsLocked(sQLiteConnection);
        i = j;
        continue;
      } 
      break;
    } 
  }
  
  private void closeConnectionAndLogExceptionsLocked(SQLiteConnection paramSQLiteConnection) {
    try {
      paramSQLiteConnection.close();
      if (this.mIdleConnectionHandler != null)
        this.mIdleConnectionHandler.connectionClosed(paramSQLiteConnection); 
    } catch (RuntimeException runtimeException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to close connection, its fate is now in the hands of the merciful GC: ");
      stringBuilder.append(paramSQLiteConnection);
      Log.e("SQLiteConnectionPool", stringBuilder.toString(), runtimeException);
    } 
  }
  
  private void discardAcquiredConnectionsLocked() {
    markAcquiredConnectionsLocked(AcquiredConnectionStatus.DISCARD);
  }
  
  private void reconfigureAllConnectionsLocked() {
    SQLiteConnection sQLiteConnection = this.mAvailablePrimaryConnection;
    if (sQLiteConnection != null)
      try {
        sQLiteConnection.reconfigure(this.mConfiguration);
      } catch (RuntimeException runtimeException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to reconfigure available primary connection, closing it: ");
        stringBuilder.append(this.mAvailablePrimaryConnection);
        Log.e("SQLiteConnectionPool", stringBuilder.toString(), runtimeException);
        closeConnectionAndLogExceptionsLocked(this.mAvailablePrimaryConnection);
        this.mAvailablePrimaryConnection = null;
      }  
    int i = this.mAvailableNonPrimaryConnections.size();
    for (byte b = 0; b < i; b++) {
      SQLiteConnection sQLiteConnection1 = this.mAvailableNonPrimaryConnections.get(b);
      try {
        sQLiteConnection1.reconfigure(this.mConfiguration);
      } catch (RuntimeException runtimeException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to reconfigure available non-primary connection, closing it: ");
        stringBuilder.append(sQLiteConnection1);
        Log.e("SQLiteConnectionPool", stringBuilder.toString(), runtimeException);
        closeConnectionAndLogExceptionsLocked(sQLiteConnection1);
        this.mAvailableNonPrimaryConnections.remove(b);
        i--;
        b--;
      } 
    } 
    markAcquiredConnectionsLocked(AcquiredConnectionStatus.RECONFIGURE);
  }
  
  private void markAcquiredConnectionsLocked(AcquiredConnectionStatus paramAcquiredConnectionStatus) {
    if (!this.mAcquiredConnections.isEmpty()) {
      WeakHashMap<SQLiteConnection, AcquiredConnectionStatus> weakHashMap = this.mAcquiredConnections;
      ArrayList<SQLiteConnection> arrayList = new ArrayList(weakHashMap.size());
      for (Map.Entry<SQLiteConnection, AcquiredConnectionStatus> entry : this.mAcquiredConnections.entrySet()) {
        AcquiredConnectionStatus acquiredConnectionStatus = (AcquiredConnectionStatus)entry.getValue();
        if (paramAcquiredConnectionStatus != acquiredConnectionStatus && acquiredConnectionStatus != AcquiredConnectionStatus.DISCARD)
          arrayList.add((SQLiteConnection)entry.getKey()); 
      } 
      int i = arrayList.size();
      for (byte b = 0; b < i; b++)
        this.mAcquiredConnections.put(arrayList.get(b), paramAcquiredConnectionStatus); 
    } 
  }
  
  private SQLiteConnection waitForConnection(String paramString, int paramInt, CancellationSignal paramCancellationSignal) {
    boolean bool;
    if ((paramInt & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Object object = this.mLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      ConnectionWaiter connectionWaiter2;
      throwIfClosedLocked();
      if (paramCancellationSignal != null)
        try {
          paramCancellationSignal.throwIfCanceled();
        } finally {} 
      SQLiteConnection sQLiteConnection1 = null;
      if (!bool)
        sQLiteConnection1 = tryAcquireNonPrimaryConnectionLocked(paramString, paramInt); 
      SQLiteConnection sQLiteConnection2 = sQLiteConnection1;
      if (sQLiteConnection1 == null)
        sQLiteConnection2 = tryAcquirePrimaryConnectionLocked(paramInt); 
      if (sQLiteConnection2 != null)
        return sQLiteConnection2; 
      int i = getPriority(paramInt);
      long l = SystemClock.uptimeMillis();
      ConnectionWaiter connectionWaiter3 = obtainConnectionWaiterLocked(Thread.currentThread(), l, i, bool, paramString, paramInt);
      sQLiteConnection1 = null;
      ConnectionWaiter connectionWaiter1 = this.mConnectionWaiterQueue;
      while (connectionWaiter1 != null) {
        if (i > connectionWaiter1.mPriority) {
          connectionWaiter3.mNext = connectionWaiter1;
          break;
        } 
        connectionWaiter2 = connectionWaiter1;
        connectionWaiter1 = connectionWaiter1.mNext;
      } 
      if (connectionWaiter2 != null) {
        connectionWaiter2.mNext = connectionWaiter3;
      } else {
        this.mConnectionWaiterQueue = connectionWaiter3;
      } 
      i = connectionWaiter3.mNonce;
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      if (paramCancellationSignal != null)
        paramCancellationSignal.setOnCancelListener((CancellationSignal.OnCancelListener)new Object(this, connectionWaiter3, i)); 
      l = 30000L;
      boolean bool1 = bool;
      try {
        long l1 = connectionWaiter3.mStartTime + 30000L;
        while (true) {
          bool1 = bool;
          bool1 = this.mConnectionLeaked.compareAndSet(true, false);
          if (bool1)
            try {
              synchronized (this.mLock) {
                wakeConnectionWaitersLocked();
              } 
            } finally {} 
          try {
            LockSupport.parkNanos(this, l * 1000000L);
            Thread.interrupted();
            Object object1 = this.mLock;
            /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
            try {
              throwIfClosedLocked();
              SQLiteConnection sQLiteConnection = connectionWaiter3.mAssignedConnection;
              object = connectionWaiter3.mException;
              if (sQLiteConnection != null || object != null) {
                recycleConnectionWaiterLocked(connectionWaiter3);
                if (sQLiteConnection != null) {
                  /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
                  return sQLiteConnection;
                } 
                throw object;
              } 
              long l2 = SystemClock.uptimeMillis();
              if (l2 < l1) {
                l = l2 - l1;
              } else {
                try {
                  logConnectionPoolBusyLocked(l2 - connectionWaiter3.mStartTime, paramInt);
                  l = 30000L;
                  l1 = l2 + 30000L;
                  try {
                    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
                    continue;
                  } finally {}
                } finally {}
                /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
              } 
              try {
                /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
                continue;
              } finally {}
            } finally {}
            /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          } finally {}
          if (paramCancellationSignal != null)
            paramCancellationSignal.setOnCancelListener(null); 
          throw connectionWaiter1;
        } 
      } finally {}
      if (paramCancellationSignal != null)
        paramCancellationSignal.setOnCancelListener(null); 
      throw connectionWaiter1;
    } finally {
      paramString = null;
    } 
  }
  
  private void cancelConnectionWaiterLocked(ConnectionWaiter paramConnectionWaiter) {
    if (paramConnectionWaiter.mAssignedConnection != null || paramConnectionWaiter.mException != null)
      return; 
    ConnectionWaiter connectionWaiter1 = null;
    ConnectionWaiter connectionWaiter2 = this.mConnectionWaiterQueue;
    while (connectionWaiter2 != paramConnectionWaiter) {
      connectionWaiter1 = connectionWaiter2;
      connectionWaiter2 = connectionWaiter2.mNext;
    } 
    if (connectionWaiter1 != null) {
      connectionWaiter1.mNext = paramConnectionWaiter.mNext;
    } else {
      this.mConnectionWaiterQueue = paramConnectionWaiter.mNext;
    } 
    paramConnectionWaiter.mException = (RuntimeException)new OperationCanceledException();
    LockSupport.unpark(paramConnectionWaiter.mThread);
    wakeConnectionWaitersLocked();
  }
  
  private void logConnectionPoolBusyLocked(long paramLong, int paramInt) {
    Thread thread = Thread.currentThread();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("The connection pool for database '");
    stringBuilder.append(this.mConfiguration.label);
    stringBuilder.append("' has been unable to grant a connection to thread ");
    stringBuilder.append(thread.getId());
    stringBuilder.append(" (");
    stringBuilder.append(thread.getName());
    stringBuilder.append(") ");
    stringBuilder.append("with flags 0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    stringBuilder.append(" for ");
    stringBuilder.append((float)paramLong * 0.001F);
    stringBuilder.append(" seconds.\n");
    ArrayList<String> arrayList = new ArrayList();
    byte b = 0;
    paramInt = 0;
    if (!this.mAcquiredConnections.isEmpty()) {
      for (SQLiteConnection sQLiteConnection : this.mAcquiredConnections.keySet()) {
        String str = sQLiteConnection.describeCurrentOperationUnsafe();
        if (str != null) {
          arrayList.add(str);
          b++;
          continue;
        } 
        paramInt++;
      } 
    } else {
      b = 0;
      paramInt = 0;
    } 
    int i = this.mAvailableNonPrimaryConnections.size();
    if (this.mAvailablePrimaryConnection != null)
      i++; 
    stringBuilder.append("Connections: ");
    stringBuilder.append(b);
    stringBuilder.append(" active, ");
    stringBuilder.append(paramInt);
    stringBuilder.append(" idle, ");
    stringBuilder.append(i);
    stringBuilder.append(" available.\n");
    if (!arrayList.isEmpty()) {
      stringBuilder.append("\nRequests in progress:\n");
      for (String str : arrayList) {
        stringBuilder.append("  ");
        stringBuilder.append(str);
        stringBuilder.append("\n");
      } 
    } 
    String str2 = this.mConfiguration.label;
    long l = thread.getId();
    String str1 = thread.getName();
    OplusBaseSQLiteDebug.onConnectionPoolBusy(str2, l, str1, b, paramInt, i, arrayList, paramLong);
    Log.w("SQLiteConnectionPool", stringBuilder.toString());
  }
  
  private void wakeConnectionWaitersLocked() {
    ConnectionWaiter connectionWaiter1 = null;
    ConnectionWaiter connectionWaiter2 = this.mConnectionWaiterQueue;
    boolean bool1 = false;
    boolean bool2 = false;
    while (connectionWaiter2 != null) {
      boolean bool4, bool3 = false;
      if (!this.mIsOpen) {
        boolean bool = true;
        bool4 = bool2;
        bool2 = bool;
      } else {
        SQLiteConnection sQLiteConnection1 = null;
        bool4 = bool2;
        SQLiteConnection sQLiteConnection2 = sQLiteConnection1;
        boolean bool5 = bool1, bool6 = bool2;
        try {
          if (!connectionWaiter2.mWantPrimaryConnection) {
            bool4 = bool2;
            sQLiteConnection2 = sQLiteConnection1;
            if (!bool2) {
              bool5 = bool1;
              bool6 = bool2;
              sQLiteConnection1 = tryAcquireNonPrimaryConnectionLocked(connectionWaiter2.mSql, connectionWaiter2.mConnectionFlags);
              bool4 = bool2;
              sQLiteConnection2 = sQLiteConnection1;
              if (sQLiteConnection1 == null) {
                bool4 = true;
                sQLiteConnection2 = sQLiteConnection1;
              } 
            } 
          } 
          bool2 = bool1;
          sQLiteConnection1 = sQLiteConnection2;
          if (sQLiteConnection2 == null) {
            bool2 = bool1;
            sQLiteConnection1 = sQLiteConnection2;
            if (!bool1) {
              bool5 = bool1;
              bool6 = bool4;
              sQLiteConnection2 = tryAcquirePrimaryConnectionLocked(connectionWaiter2.mConnectionFlags);
              bool2 = bool1;
              sQLiteConnection1 = sQLiteConnection2;
              if (sQLiteConnection2 == null) {
                bool2 = true;
                sQLiteConnection1 = sQLiteConnection2;
              } 
            } 
          } 
          if (sQLiteConnection1 != null) {
            bool5 = bool2;
            bool6 = bool4;
            connectionWaiter2.mAssignedConnection = sQLiteConnection1;
            bool5 = true;
          } else {
            bool5 = bool3;
            if (bool4) {
              bool5 = bool3;
              if (bool2)
                break; 
            } 
          } 
          bool1 = bool2;
          bool2 = bool5;
        } catch (RuntimeException runtimeException) {
          connectionWaiter2.mException = runtimeException;
          bool2 = true;
          bool4 = bool6;
          bool1 = bool5;
        } 
      } 
      ConnectionWaiter connectionWaiter = connectionWaiter2.mNext;
      if (bool2) {
        if (connectionWaiter1 != null) {
          connectionWaiter1.mNext = connectionWaiter;
        } else {
          this.mConnectionWaiterQueue = connectionWaiter;
        } 
        connectionWaiter2.mNext = null;
        LockSupport.unpark(connectionWaiter2.mThread);
      } else {
        connectionWaiter1 = connectionWaiter2;
      } 
      connectionWaiter2 = connectionWaiter;
      bool2 = bool4;
    } 
  }
  
  private SQLiteConnection tryAcquirePrimaryConnectionLocked(int paramInt) {
    SQLiteConnection sQLiteConnection = this.mAvailablePrimaryConnection;
    if (sQLiteConnection != null) {
      this.mAvailablePrimaryConnection = null;
      finishAcquireConnectionLocked(sQLiteConnection, paramInt);
      return sQLiteConnection;
    } 
    for (SQLiteConnection sQLiteConnection1 : this.mAcquiredConnections.keySet()) {
      if (sQLiteConnection1.isPrimaryConnection())
        return null; 
    } 
    sQLiteConnection = openConnectionLocked(this.mConfiguration, true);
    finishAcquireConnectionLocked(sQLiteConnection, paramInt);
    return sQLiteConnection;
  }
  
  private SQLiteConnection tryAcquireNonPrimaryConnectionLocked(String paramString, int paramInt) {
    int i = this.mAvailableNonPrimaryConnections.size();
    if (i > 1 && paramString != null)
      for (byte b = 0; b < i; b++) {
        SQLiteConnection sQLiteConnection1 = this.mAvailableNonPrimaryConnections.get(b);
        if (sQLiteConnection1.isPreparedStatementInCache(paramString)) {
          this.mAvailableNonPrimaryConnections.remove(b);
          finishAcquireConnectionLocked(sQLiteConnection1, paramInt);
          return sQLiteConnection1;
        } 
      }  
    if (i > 0) {
      SQLiteConnection sQLiteConnection1 = this.mAvailableNonPrimaryConnections.remove(i - 1);
      finishAcquireConnectionLocked(sQLiteConnection1, paramInt);
      return sQLiteConnection1;
    } 
    i = this.mAcquiredConnections.size();
    int j = i;
    if (this.mAvailablePrimaryConnection != null)
      j = i + 1; 
    if (j >= this.mMaxConnectionPoolSize)
      return null; 
    SQLiteConnection sQLiteConnection = openConnectionLocked(this.mConfiguration, false);
    finishAcquireConnectionLocked(sQLiteConnection, paramInt);
    return sQLiteConnection;
  }
  
  private void finishAcquireConnectionLocked(SQLiteConnection paramSQLiteConnection, int paramInt) {
    boolean bool;
    if ((paramInt & 0x1) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    try {
      paramSQLiteConnection.setOnlyAllowReadOnlyOperations(bool);
      this.mAcquiredConnections.put(paramSQLiteConnection, AcquiredConnectionStatus.NORMAL);
      return;
    } catch (RuntimeException runtimeException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to prepare acquired connection for session, closing it: ");
      stringBuilder.append(paramSQLiteConnection);
      stringBuilder.append(", connectionFlags=");
      stringBuilder.append(paramInt);
      Log.e("SQLiteConnectionPool", stringBuilder.toString());
      closeConnectionAndLogExceptionsLocked(paramSQLiteConnection);
      throw runtimeException;
    } 
  }
  
  private boolean isSessionBlockingImportantConnectionWaitersLocked(boolean paramBoolean, int paramInt) {
    ConnectionWaiter connectionWaiter = this.mConnectionWaiterQueue;
    if (connectionWaiter != null) {
      paramInt = getPriority(paramInt);
      while (paramInt <= connectionWaiter.mPriority) {
        if (paramBoolean || !connectionWaiter.mWantPrimaryConnection)
          return true; 
        ConnectionWaiter connectionWaiter1 = connectionWaiter.mNext;
        connectionWaiter = connectionWaiter1;
        if (connectionWaiter1 == null)
          break; 
      } 
    } 
    return false;
  }
  
  private static int getPriority(int paramInt) {
    if ((paramInt & 0x4) != 0) {
      paramInt = 1;
    } else {
      paramInt = 0;
    } 
    return paramInt;
  }
  
  private void setMaxConnectionPoolSizeLocked() {
    if (!this.mConfiguration.isInMemoryDb() && (this.mConfiguration.openFlags & 0x20000000) != 0) {
      this.mMaxConnectionPoolSize = SQLiteGlobal.getWALConnectionPoolSize();
    } else {
      this.mMaxConnectionPoolSize = 1;
    } 
  }
  
  public void setupIdleConnectionHandler(Looper paramLooper, long paramLong) {
    synchronized (this.mLock) {
      IdleConnectionHandler idleConnectionHandler = new IdleConnectionHandler();
      this(this, paramLooper, paramLong);
      this.mIdleConnectionHandler = idleConnectionHandler;
      return;
    } 
  }
  
  void disableIdleConnectionHandler() {
    synchronized (this.mLock) {
      this.mIdleConnectionHandler = null;
      return;
    } 
  }
  
  private void throwIfClosedLocked() {
    if (this.mIsOpen)
      return; 
    throw new IllegalStateException("Cannot perform this operation because the connection pool has been closed.");
  }
  
  private ConnectionWaiter obtainConnectionWaiterLocked(Thread paramThread, long paramLong, int paramInt1, boolean paramBoolean, String paramString, int paramInt2) {
    ConnectionWaiter connectionWaiter = this.mConnectionWaiterPool;
    if (connectionWaiter != null) {
      this.mConnectionWaiterPool = connectionWaiter.mNext;
      connectionWaiter.mNext = null;
    } else {
      connectionWaiter = new ConnectionWaiter();
    } 
    connectionWaiter.mThread = paramThread;
    connectionWaiter.mStartTime = paramLong;
    connectionWaiter.mPriority = paramInt1;
    connectionWaiter.mWantPrimaryConnection = paramBoolean;
    connectionWaiter.mSql = paramString;
    connectionWaiter.mConnectionFlags = paramInt2;
    return connectionWaiter;
  }
  
  private void recycleConnectionWaiterLocked(ConnectionWaiter paramConnectionWaiter) {
    paramConnectionWaiter.mNext = this.mConnectionWaiterPool;
    paramConnectionWaiter.mThread = null;
    paramConnectionWaiter.mSql = null;
    paramConnectionWaiter.mAssignedConnection = null;
    paramConnectionWaiter.mException = null;
    paramConnectionWaiter.mNonce++;
    this.mConnectionWaiterPool = paramConnectionWaiter;
  }
  
  public void dump(Printer paramPrinter, boolean paramBoolean, ArraySet<String> paramArraySet) {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w '    '
    //   4: invokestatic create : (Landroid/util/Printer;Ljava/lang/String;)Landroid/util/Printer;
    //   7: astore #4
    //   9: aload_0
    //   10: getfield mLock : Ljava/lang/Object;
    //   13: astore #5
    //   15: aload #5
    //   17: monitorenter
    //   18: aload_3
    //   19: ifnull -> 49
    //   22: new java/io/File
    //   25: astore #6
    //   27: aload #6
    //   29: aload_0
    //   30: getfield mConfiguration : Landroid/database/sqlite/SQLiteDatabaseConfiguration;
    //   33: getfield path : Ljava/lang/String;
    //   36: invokespecial <init> : (Ljava/lang/String;)V
    //   39: aload_3
    //   40: aload #6
    //   42: invokevirtual getParent : ()Ljava/lang/String;
    //   45: invokevirtual add : (Ljava/lang/Object;)Z
    //   48: pop
    //   49: aload_0
    //   50: getfield mConfiguration : Landroid/database/sqlite/SQLiteDatabaseConfiguration;
    //   53: invokevirtual isLegacyCompatibilityWalEnabled : ()Z
    //   56: istore #7
    //   58: new java/lang/StringBuilder
    //   61: astore_3
    //   62: aload_3
    //   63: invokespecial <init> : ()V
    //   66: aload_3
    //   67: ldc_w 'Connection pool for '
    //   70: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   73: pop
    //   74: aload_3
    //   75: aload_0
    //   76: getfield mConfiguration : Landroid/database/sqlite/SQLiteDatabaseConfiguration;
    //   79: getfield path : Ljava/lang/String;
    //   82: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   85: pop
    //   86: aload_3
    //   87: ldc_w ':'
    //   90: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   93: pop
    //   94: aload_1
    //   95: aload_3
    //   96: invokevirtual toString : ()Ljava/lang/String;
    //   99: invokeinterface println : (Ljava/lang/String;)V
    //   104: new java/lang/StringBuilder
    //   107: astore_3
    //   108: aload_3
    //   109: invokespecial <init> : ()V
    //   112: aload_3
    //   113: ldc_w '  Open: '
    //   116: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   119: pop
    //   120: aload_3
    //   121: aload_0
    //   122: getfield mIsOpen : Z
    //   125: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   128: pop
    //   129: aload_1
    //   130: aload_3
    //   131: invokevirtual toString : ()Ljava/lang/String;
    //   134: invokeinterface println : (Ljava/lang/String;)V
    //   139: new java/lang/StringBuilder
    //   142: astore_3
    //   143: aload_3
    //   144: invokespecial <init> : ()V
    //   147: aload_3
    //   148: ldc_w '  Max connections: '
    //   151: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   154: pop
    //   155: aload_3
    //   156: aload_0
    //   157: getfield mMaxConnectionPoolSize : I
    //   160: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   163: pop
    //   164: aload_1
    //   165: aload_3
    //   166: invokevirtual toString : ()Ljava/lang/String;
    //   169: invokeinterface println : (Ljava/lang/String;)V
    //   174: new java/lang/StringBuilder
    //   177: astore_3
    //   178: aload_3
    //   179: invokespecial <init> : ()V
    //   182: aload_3
    //   183: ldc_w '  Total execution time: '
    //   186: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   189: pop
    //   190: aload_3
    //   191: aload_0
    //   192: getfield mTotalExecutionTimeCounter : Ljava/util/concurrent/atomic/AtomicLong;
    //   195: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   198: pop
    //   199: aload_1
    //   200: aload_3
    //   201: invokevirtual toString : ()Ljava/lang/String;
    //   204: invokeinterface println : (Ljava/lang/String;)V
    //   209: new java/lang/StringBuilder
    //   212: astore_3
    //   213: aload_3
    //   214: invokespecial <init> : ()V
    //   217: aload_3
    //   218: ldc_w '  Configuration: openFlags='
    //   221: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   224: pop
    //   225: aload_3
    //   226: aload_0
    //   227: getfield mConfiguration : Landroid/database/sqlite/SQLiteDatabaseConfiguration;
    //   230: getfield openFlags : I
    //   233: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   236: pop
    //   237: aload_3
    //   238: ldc_w ', isLegacyCompatibilityWalEnabled='
    //   241: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   244: pop
    //   245: aload_3
    //   246: iload #7
    //   248: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   251: pop
    //   252: aload_3
    //   253: ldc_w ', journalMode='
    //   256: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   259: pop
    //   260: aload_0
    //   261: getfield mConfiguration : Landroid/database/sqlite/SQLiteDatabaseConfiguration;
    //   264: getfield journalMode : Ljava/lang/String;
    //   267: astore #6
    //   269: aload_3
    //   270: aload #6
    //   272: invokestatic emptyIfNull : (Ljava/lang/String;)Ljava/lang/String;
    //   275: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   278: pop
    //   279: aload_3
    //   280: ldc_w ', syncMode='
    //   283: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   286: pop
    //   287: aload_0
    //   288: getfield mConfiguration : Landroid/database/sqlite/SQLiteDatabaseConfiguration;
    //   291: getfield syncMode : Ljava/lang/String;
    //   294: astore #6
    //   296: aload_3
    //   297: aload #6
    //   299: invokestatic emptyIfNull : (Ljava/lang/String;)Ljava/lang/String;
    //   302: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   305: pop
    //   306: aload_3
    //   307: invokevirtual toString : ()Ljava/lang/String;
    //   310: astore_3
    //   311: aload_1
    //   312: aload_3
    //   313: invokeinterface println : (Ljava/lang/String;)V
    //   318: iload #7
    //   320: ifeq -> 359
    //   323: new java/lang/StringBuilder
    //   326: astore_3
    //   327: aload_3
    //   328: invokespecial <init> : ()V
    //   331: aload_3
    //   332: ldc_w '  Compatibility WAL enabled: wal_syncmode='
    //   335: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   338: pop
    //   339: aload_3
    //   340: invokestatic getWALSyncMode : ()Ljava/lang/String;
    //   343: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   346: pop
    //   347: aload_3
    //   348: invokevirtual toString : ()Ljava/lang/String;
    //   351: astore_3
    //   352: aload_1
    //   353: aload_3
    //   354: invokeinterface println : (Ljava/lang/String;)V
    //   359: aload_0
    //   360: getfield mConfiguration : Landroid/database/sqlite/SQLiteDatabaseConfiguration;
    //   363: invokevirtual isLookasideConfigSet : ()Z
    //   366: ifeq -> 427
    //   369: new java/lang/StringBuilder
    //   372: astore_3
    //   373: aload_3
    //   374: invokespecial <init> : ()V
    //   377: aload_3
    //   378: ldc_w '  Lookaside config: sz='
    //   381: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   384: pop
    //   385: aload_3
    //   386: aload_0
    //   387: getfield mConfiguration : Landroid/database/sqlite/SQLiteDatabaseConfiguration;
    //   390: getfield lookasideSlotSize : I
    //   393: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   396: pop
    //   397: aload_3
    //   398: ldc_w ' cnt='
    //   401: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   404: pop
    //   405: aload_3
    //   406: aload_0
    //   407: getfield mConfiguration : Landroid/database/sqlite/SQLiteDatabaseConfiguration;
    //   410: getfield lookasideSlotCount : I
    //   413: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   416: pop
    //   417: aload_1
    //   418: aload_3
    //   419: invokevirtual toString : ()Ljava/lang/String;
    //   422: invokeinterface println : (Ljava/lang/String;)V
    //   427: aload_0
    //   428: getfield mConfiguration : Landroid/database/sqlite/SQLiteDatabaseConfiguration;
    //   431: getfield idleConnectionTimeoutMs : J
    //   434: ldc2_w 9223372036854775807
    //   437: lcmp
    //   438: ifeq -> 479
    //   441: new java/lang/StringBuilder
    //   444: astore_3
    //   445: aload_3
    //   446: invokespecial <init> : ()V
    //   449: aload_3
    //   450: ldc_w '  Idle connection timeout: '
    //   453: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   456: pop
    //   457: aload_3
    //   458: aload_0
    //   459: getfield mConfiguration : Landroid/database/sqlite/SQLiteDatabaseConfiguration;
    //   462: getfield idleConnectionTimeoutMs : J
    //   465: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   468: pop
    //   469: aload_1
    //   470: aload_3
    //   471: invokevirtual toString : ()Ljava/lang/String;
    //   474: invokeinterface println : (Ljava/lang/String;)V
    //   479: aload_1
    //   480: ldc_w '  Available primary connection:'
    //   483: invokeinterface println : (Ljava/lang/String;)V
    //   488: aload_0
    //   489: getfield mAvailablePrimaryConnection : Landroid/database/sqlite/SQLiteConnection;
    //   492: ifnull -> 508
    //   495: aload_0
    //   496: getfield mAvailablePrimaryConnection : Landroid/database/sqlite/SQLiteConnection;
    //   499: aload #4
    //   501: iload_2
    //   502: invokevirtual dump : (Landroid/util/Printer;Z)V
    //   505: goto -> 518
    //   508: aload #4
    //   510: ldc_w '<none>'
    //   513: invokeinterface println : (Ljava/lang/String;)V
    //   518: aload_1
    //   519: ldc_w '  Available non-primary connections:'
    //   522: invokeinterface println : (Ljava/lang/String;)V
    //   527: aload_0
    //   528: getfield mAvailableNonPrimaryConnections : Ljava/util/ArrayList;
    //   531: invokevirtual isEmpty : ()Z
    //   534: ifne -> 583
    //   537: aload_0
    //   538: getfield mAvailableNonPrimaryConnections : Ljava/util/ArrayList;
    //   541: invokevirtual size : ()I
    //   544: istore #8
    //   546: iconst_0
    //   547: istore #9
    //   549: iload #9
    //   551: iload #8
    //   553: if_icmpge -> 580
    //   556: aload_0
    //   557: getfield mAvailableNonPrimaryConnections : Ljava/util/ArrayList;
    //   560: iload #9
    //   562: invokevirtual get : (I)Ljava/lang/Object;
    //   565: checkcast android/database/sqlite/SQLiteConnection
    //   568: aload #4
    //   570: iload_2
    //   571: invokevirtual dump : (Landroid/util/Printer;Z)V
    //   574: iinc #9, 1
    //   577: goto -> 549
    //   580: goto -> 593
    //   583: aload #4
    //   585: ldc_w '<none>'
    //   588: invokeinterface println : (Ljava/lang/String;)V
    //   593: aload_1
    //   594: ldc_w '  Acquired connections:'
    //   597: invokeinterface println : (Ljava/lang/String;)V
    //   602: aload_0
    //   603: getfield mAcquiredConnections : Ljava/util/WeakHashMap;
    //   606: invokevirtual isEmpty : ()Z
    //   609: ifne -> 715
    //   612: aload_0
    //   613: getfield mAcquiredConnections : Ljava/util/WeakHashMap;
    //   616: invokevirtual entrySet : ()Ljava/util/Set;
    //   619: invokeinterface iterator : ()Ljava/util/Iterator;
    //   624: astore_3
    //   625: aload_3
    //   626: invokeinterface hasNext : ()Z
    //   631: ifeq -> 712
    //   634: aload_3
    //   635: invokeinterface next : ()Ljava/lang/Object;
    //   640: checkcast java/util/Map$Entry
    //   643: astore #6
    //   645: aload #6
    //   647: invokeinterface getKey : ()Ljava/lang/Object;
    //   652: checkcast android/database/sqlite/SQLiteConnection
    //   655: astore #10
    //   657: aload #10
    //   659: aload #4
    //   661: iload_2
    //   662: invokevirtual dumpUnsafe : (Landroid/util/Printer;Z)V
    //   665: new java/lang/StringBuilder
    //   668: astore #10
    //   670: aload #10
    //   672: invokespecial <init> : ()V
    //   675: aload #10
    //   677: ldc_w '  Status: '
    //   680: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   683: pop
    //   684: aload #10
    //   686: aload #6
    //   688: invokeinterface getValue : ()Ljava/lang/Object;
    //   693: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   696: pop
    //   697: aload #4
    //   699: aload #10
    //   701: invokevirtual toString : ()Ljava/lang/String;
    //   704: invokeinterface println : (Ljava/lang/String;)V
    //   709: goto -> 625
    //   712: goto -> 725
    //   715: aload #4
    //   717: ldc_w '<none>'
    //   720: invokeinterface println : (Ljava/lang/String;)V
    //   725: aload_1
    //   726: ldc_w '  Connection waiters:'
    //   729: invokeinterface println : (Ljava/lang/String;)V
    //   734: aload_0
    //   735: getfield mConnectionWaiterQueue : Landroid/database/sqlite/SQLiteConnectionPool$ConnectionWaiter;
    //   738: ifnull -> 882
    //   741: iconst_0
    //   742: istore #9
    //   744: invokestatic uptimeMillis : ()J
    //   747: lstore #11
    //   749: aload_0
    //   750: getfield mConnectionWaiterQueue : Landroid/database/sqlite/SQLiteConnectionPool$ConnectionWaiter;
    //   753: astore_1
    //   754: aload_1
    //   755: ifnull -> 879
    //   758: new java/lang/StringBuilder
    //   761: astore_3
    //   762: aload_3
    //   763: invokespecial <init> : ()V
    //   766: aload_3
    //   767: iload #9
    //   769: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   772: pop
    //   773: aload_3
    //   774: ldc_w ': waited for '
    //   777: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   780: pop
    //   781: aload_3
    //   782: lload #11
    //   784: aload_1
    //   785: getfield mStartTime : J
    //   788: lsub
    //   789: l2f
    //   790: ldc_w 0.001
    //   793: fmul
    //   794: invokevirtual append : (F)Ljava/lang/StringBuilder;
    //   797: pop
    //   798: aload_3
    //   799: ldc_w ' ms - thread='
    //   802: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   805: pop
    //   806: aload_3
    //   807: aload_1
    //   808: getfield mThread : Ljava/lang/Thread;
    //   811: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   814: pop
    //   815: aload_3
    //   816: ldc_w ', priority='
    //   819: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   822: pop
    //   823: aload_3
    //   824: aload_1
    //   825: getfield mPriority : I
    //   828: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   831: pop
    //   832: aload_3
    //   833: ldc_w ', sql=''
    //   836: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   839: pop
    //   840: aload_3
    //   841: aload_1
    //   842: getfield mSql : Ljava/lang/String;
    //   845: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   848: pop
    //   849: aload_3
    //   850: ldc_w '''
    //   853: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   856: pop
    //   857: aload #4
    //   859: aload_3
    //   860: invokevirtual toString : ()Ljava/lang/String;
    //   863: invokeinterface println : (Ljava/lang/String;)V
    //   868: aload_1
    //   869: getfield mNext : Landroid/database/sqlite/SQLiteConnectionPool$ConnectionWaiter;
    //   872: astore_1
    //   873: iinc #9, 1
    //   876: goto -> 754
    //   879: goto -> 892
    //   882: aload #4
    //   884: ldc_w '<none>'
    //   887: invokeinterface println : (Ljava/lang/String;)V
    //   892: aload #5
    //   894: monitorexit
    //   895: return
    //   896: astore_1
    //   897: aload #5
    //   899: monitorexit
    //   900: aload_1
    //   901: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1133	-> 0
    //   #1134	-> 9
    //   #1135	-> 18
    //   #1136	-> 22
    //   #1138	-> 49
    //   #1139	-> 58
    //   #1140	-> 104
    //   #1141	-> 139
    //   #1142	-> 174
    //   #1143	-> 209
    //   #1145	-> 269
    //   #1146	-> 296
    //   #1143	-> 311
    //   #1148	-> 318
    //   #1149	-> 323
    //   #1150	-> 339
    //   #1149	-> 352
    //   #1152	-> 359
    //   #1153	-> 369
    //   #1156	-> 427
    //   #1157	-> 441
    //   #1160	-> 479
    //   #1161	-> 488
    //   #1162	-> 495
    //   #1164	-> 508
    //   #1167	-> 518
    //   #1168	-> 527
    //   #1169	-> 537
    //   #1170	-> 546
    //   #1171	-> 556
    //   #1170	-> 574
    //   #1173	-> 580
    //   #1174	-> 583
    //   #1177	-> 593
    //   #1178	-> 602
    //   #1180	-> 612
    //   #1181	-> 645
    //   #1182	-> 657
    //   #1183	-> 665
    //   #1184	-> 709
    //   #1186	-> 715
    //   #1189	-> 725
    //   #1190	-> 734
    //   #1191	-> 741
    //   #1192	-> 744
    //   #1193	-> 749
    //   #1195	-> 758
    //   #1194	-> 868
    //   #1201	-> 879
    //   #1202	-> 882
    //   #1204	-> 892
    //   #1205	-> 895
    //   #1204	-> 896
    // Exception table:
    //   from	to	target	type
    //   22	49	896	finally
    //   49	58	896	finally
    //   58	104	896	finally
    //   104	139	896	finally
    //   139	174	896	finally
    //   174	209	896	finally
    //   209	269	896	finally
    //   269	296	896	finally
    //   296	311	896	finally
    //   311	318	896	finally
    //   323	339	896	finally
    //   339	352	896	finally
    //   352	359	896	finally
    //   359	369	896	finally
    //   369	427	896	finally
    //   427	441	896	finally
    //   441	479	896	finally
    //   479	488	896	finally
    //   488	495	896	finally
    //   495	505	896	finally
    //   508	518	896	finally
    //   518	527	896	finally
    //   527	537	896	finally
    //   537	546	896	finally
    //   556	574	896	finally
    //   583	593	896	finally
    //   593	602	896	finally
    //   602	612	896	finally
    //   612	625	896	finally
    //   625	645	896	finally
    //   645	657	896	finally
    //   657	665	896	finally
    //   665	709	896	finally
    //   715	725	896	finally
    //   725	734	896	finally
    //   734	741	896	finally
    //   744	749	896	finally
    //   749	754	896	finally
    //   758	868	896	finally
    //   868	873	896	finally
    //   882	892	896	finally
    //   892	895	896	finally
    //   897	900	896	finally
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SQLiteConnectionPool: ");
    stringBuilder.append(this.mConfiguration.path);
    return stringBuilder.toString();
  }
  
  public String getPath() {
    return this.mConfiguration.path;
  }
  
  public String getLabel() {
    return this.mConfiguration.label;
  }
  
  private static final class ConnectionWaiter {
    public SQLiteConnection mAssignedConnection;
    
    public int mConnectionFlags;
    
    public RuntimeException mException;
    
    public ConnectionWaiter mNext;
    
    public int mNonce;
    
    public int mPriority;
    
    public String mSql;
    
    public long mStartTime;
    
    public Thread mThread;
    
    public boolean mWantPrimaryConnection;
    
    private ConnectionWaiter() {}
  }
  
  class IdleConnectionHandler extends Handler {
    private final long mTimeout;
    
    final SQLiteConnectionPool this$0;
    
    IdleConnectionHandler(Looper param1Looper, long param1Long) {
      super(param1Looper);
      this.mTimeout = param1Long;
    }
    
    public void handleMessage(Message param1Message) {
      synchronized (SQLiteConnectionPool.this.mLock) {
        if (this != SQLiteConnectionPool.this.mIdleConnectionHandler)
          return; 
        if (SQLiteConnectionPool.this.closeAvailableConnectionLocked(param1Message.what) && 
          Log.isLoggable("SQLiteConnectionPool", 3)) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Closed idle connection ");
          stringBuilder.append(SQLiteConnectionPool.this.mConfiguration.label);
          stringBuilder.append(" ");
          stringBuilder.append(param1Message.what);
          stringBuilder.append(" after ");
          stringBuilder.append(this.mTimeout);
          Log.d("SQLiteConnectionPool", stringBuilder.toString());
        } 
        return;
      } 
    }
    
    void connectionReleased(SQLiteConnection param1SQLiteConnection) {
      sendEmptyMessageDelayed(param1SQLiteConnection.getConnectionId(), this.mTimeout);
    }
    
    void connectionAcquired(SQLiteConnection param1SQLiteConnection) {
      removeMessages(param1SQLiteConnection.getConnectionId());
    }
    
    void connectionClosed(SQLiteConnection param1SQLiteConnection) {
      removeMessages(param1SQLiteConnection.getConnectionId());
    }
  }
}
