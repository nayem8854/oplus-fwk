package android.database.sqlite;

import android.content.Context;
import android.database.DatabaseUtils;
import android.os.SystemProperties;
import android.util.ArrayMap;
import android.util.Log;
import java.util.ArrayList;
import java.util.Map;
import oplus.util.OplusStatistics;

public final class OplusBaseSQLiteDebug {
  private static final String EVENT_INFO = "sqliteinfo";
  
  private static final String LOGTAG = "2016101";
  
  private static final int SQL_CTRL_MODE = 4;
  
  private static final int SQL_LOG_MODE = 1;
  
  private static final String TAG = "OplusBaseSQLiteDebug";
  
  private static final String mHwShutdownSqlPropStr = "sys.oppo.sqlctrl_hwsd";
  
  private static final String mLowBatterySqlPropStr = "sys.oppo.sqlctrl_lb";
  
  private static final String mSqlstrlPropStr = "sys.oppo.sqlctrl";
  
  private static int sCaclCountExecuted = 0;
  
  private static int sCaclCountObtained = 0;
  
  private static long sCalcStartTime = 0L;
  
  private static Context sContext;
  
  private static final int sCountExecuted = 1;
  
  private static final int sCountObtained = 2;
  
  private static final int sDefStatementType = 0;
  
  private static long sDetalCalcTime = 0L;
  
  private static final int sExecTime = 100;
  
  private static boolean sLogenable;
  
  private static boolean sLowBattery = true;
  
  private static int sMaxCaclCount;
  
  private static boolean sSqlctrl = false;
  
  private static boolean sUrgentDisable;
  
  static {
    sLogenable = false;
    sUrgentDisable = false;
    sCalcStartTime = 0L;
    sDetalCalcTime = 28800000L;
    sCaclCountExecuted = 0;
    sCaclCountObtained = 0;
    sMaxCaclCount = 3000;
  }
  
  public static void setContext(Context paramContext) {
    sContext = paramContext;
  }
  
  public static void onCommon(Context paramContext, String paramString1, String paramString2, Map<String, String> paramMap, int paramInt) {
    if (sCalcStartTime == 0L) {
      sCalcStartTime = System.currentTimeMillis();
    } else if (System.currentTimeMillis() - sCalcStartTime >= sDetalCalcTime) {
      sCaclCountExecuted = 0;
      sCaclCountObtained = 0;
      sCalcStartTime = System.currentTimeMillis();
    } 
    if (paramInt == 1 && sCaclCountExecuted > sMaxCaclCount)
      return; 
    if (paramInt == 2 && sCaclCountObtained > sMaxCaclCount)
      return; 
    OplusStatistics.onCommon(paramContext, paramString1, paramString2, paramMap, false);
    if (paramInt == 1) {
      sCaclCountExecuted++;
    } else if (paramInt == 2) {
      sCaclCountObtained++;
    } 
  }
  
  public static void onAnalyzeSqlctrlEndop() {
    boolean bool = SystemProperties.getBoolean("sys.oppo.sqlctrl_hwsd", false);
    if (!bool) {
      if (sSqlctrl == true && !sLowBattery) {
        nativeSetOSQL(1);
      } else {
        nativeSetOSQL(0);
      } 
    } else {
      nativeSetOSQL(0);
    } 
  }
  
  public static void onAnalyzeSqlctrl() {
    int i = SystemProperties.getInt("sys.oppo.sqlctrl", 0);
    if ((i & 0x4) != 0) {
      sSqlctrl = true;
    } else {
      sSqlctrl = false;
    } 
    sLowBattery = SystemProperties.getBoolean("sys.oppo.sqlctrl_lb", false);
    onAnalyzeSqlctrlEndop();
  }
  
  public static void onSQLExecuted(String paramString1, String paramString2, int paramInt, long paramLong) {
    if (sLogenable && 
      paramLong >= 100L) {
      ArrayMap arrayMap = new ArrayMap();
      arrayMap.put("Status", "Execute");
      arrayMap.put("label", paramString1);
      if (paramString2 != null) {
        arrayMap.put("SQL", String.valueOf(DatabaseUtils.getSqlStatementType(paramString2)));
      } else {
        arrayMap.put("SQL", String.valueOf(0));
      } 
      arrayMap.put("execTime", String.valueOf(paramLong));
      Context context = sContext;
      if (context != null) {
        onCommon(context, "2016101", "sqliteinfo", (Map<String, String>)arrayMap, 1);
      } else {
        Log.i("OplusBaseSQLiteDebug", "onSQLExecuted, but context is null!");
      } 
    } 
  }
  
  public static void onConnectionObtained(String paramString1, String paramString2, long paramLong, boolean paramBoolean) {
    if (sLogenable && 
      paramLong >= 100L) {
      ArrayMap arrayMap = new ArrayMap();
      arrayMap.put("Status", "Obtained");
      arrayMap.put("label", paramString1);
      if (paramString2 != null) {
        arrayMap.put("SQL", String.valueOf(DatabaseUtils.getSqlStatementType(paramString2)));
      } else {
        arrayMap.put("SQL", String.valueOf(0));
      } 
      arrayMap.put("execTime", String.valueOf(paramLong));
      Context context = sContext;
      if (context != null) {
        onCommon(context, "2016101", "sqliteinfo", (Map<String, String>)arrayMap, 2);
      } else {
        Log.i("OplusBaseSQLiteDebug", "onConnectionObtained, but context is null!");
      } 
    } 
  }
  
  public static void onConnectionPoolBusy(String paramString1, long paramLong1, String paramString2, int paramInt1, int paramInt2, int paramInt3, ArrayList<String> paramArrayList, long paramLong2) {
    if (sLogenable) {
      ArrayMap arrayMap = new ArrayMap();
      arrayMap.put("Status", "ConnectionPoolBusy");
      arrayMap.put("label", paramString1);
      arrayMap.put("Tid", String.valueOf(paramLong1));
      arrayMap.put("Tname", paramString2);
      arrayMap.put("activeC", String.valueOf(paramInt1));
      arrayMap.put("idleC", String.valueOf(paramInt2));
      arrayMap.put("availableC", String.valueOf(paramInt3));
      arrayMap.put("execTime", String.valueOf(paramLong2));
      if (!paramArrayList.isEmpty()) {
        Log.i("OplusBaseSQLiteDebug", "ConnectionPoolBusy, requests in progress:");
        for (String str : paramArrayList) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("");
          stringBuilder.append(str);
          Log.i("OplusBaseSQLiteDebug", stringBuilder.toString());
          arrayMap.put("request", str);
        } 
        Log.i("OplusBaseSQLiteDebug", "ConnectionPoolBusy, requests end");
      } 
      Context context = sContext;
      if (context != null) {
        onCommon(context, "2016101", "sqliteinfo", (Map<String, String>)arrayMap, 0);
      } else {
        Log.i("OplusBaseSQLiteDebug", "onConnectionPoolBusy, but context is null!");
      } 
    } 
  }
  
  public static void onDatabaseCorrupted(SQLiteDatabase paramSQLiteDatabase) {
    if (sLogenable) {
      ArrayMap arrayMap = new ArrayMap();
      arrayMap.put("Status", "Corrupted");
      arrayMap.put("label", paramSQLiteDatabase.getLabel());
      Context context = sContext;
      if (context != null) {
        onCommon(context, "2016101", "sqliteinfo", (Map<String, String>)arrayMap, 0);
      } else {
        Log.i("OplusBaseSQLiteDebug", "onDatabaseCorrupted, but context is null!");
      } 
    } 
  }
  
  private static native void nativeSetOSQL(int paramInt);
}
