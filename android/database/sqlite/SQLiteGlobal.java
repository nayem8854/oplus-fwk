package android.database.sqlite;

import android.content.res.Resources;
import android.os.StatFs;
import android.os.SystemProperties;

public final class SQLiteGlobal {
  public static final String SYNC_MODE_FULL = "FULL";
  
  private static final String TAG = "SQLiteGlobal";
  
  static final String WIPE_CHECK_FILE_SUFFIX = "-wipecheck";
  
  private static int sDefaultPageSize;
  
  public static volatile String sDefaultSyncMode;
  
  private static final Object sLock = new Object();
  
  public static int releaseMemory() {
    return nativeReleaseMemory();
  }
  
  public static int getDefaultPageSize() {
    synchronized (sLock) {
      if (sDefaultPageSize == 0) {
        StatFs statFs = new StatFs();
        this("/data");
        sDefaultPageSize = statFs.getBlockSize();
      } 
      return SystemProperties.getInt("debug.sqlite.pagesize", sDefaultPageSize);
    } 
  }
  
  public static String getDefaultJournalMode() {
    String str = Resources.getSystem().getString(17040044);
    return SystemProperties.get("debug.sqlite.journalmode", str);
  }
  
  public static int getJournalSizeLimit() {
    int i = Resources.getSystem().getInteger(17694929);
    return SystemProperties.getInt("debug.sqlite.journalsizelimit", i);
  }
  
  public static String getDefaultSyncMode() {
    String str = sDefaultSyncMode;
    if (str != null)
      return str; 
    str = Resources.getSystem().getString(17040045);
    return SystemProperties.get("debug.sqlite.syncmode", str);
  }
  
  public static String getWALSyncMode() {
    String str = sDefaultSyncMode;
    if (str != null)
      return str; 
    str = Resources.getSystem().getString(17040046);
    return SystemProperties.get("debug.sqlite.wal.syncmode", str);
  }
  
  public static int getWALAutoCheckpoint() {
    int i = Resources.getSystem().getInteger(17694930);
    i = SystemProperties.getInt("debug.sqlite.wal.autocheckpoint", i);
    return Math.max(1, i);
  }
  
  public static int getWALConnectionPoolSize() {
    int i = Resources.getSystem().getInteger(17694927);
    i = SystemProperties.getInt("debug.sqlite.wal.poolsize", i);
    return Math.max(2, i);
  }
  
  public static int getIdleConnectionTimeout() {
    int i = Resources.getSystem().getInteger(17694928);
    return SystemProperties.getInt("debug.sqlite.idle_connection_timeout", i);
  }
  
  public static long getWALTruncateSize() {
    long l = SQLiteCompatibilityWalFlags.getTruncateSize();
    if (l >= 0L)
      return l; 
    int i = Resources.getSystem().getInteger(17694931);
    return SystemProperties.getInt("debug.sqlite.wal.truncatesize", i);
  }
  
  public static boolean checkDbWipe() {
    return false;
  }
  
  private static native int nativeReleaseMemory();
}
