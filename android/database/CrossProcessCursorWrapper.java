package android.database;

public class CrossProcessCursorWrapper extends CursorWrapper implements CrossProcessCursor {
  public CrossProcessCursorWrapper(Cursor paramCursor) {
    super(paramCursor);
  }
  
  public void fillWindow(int paramInt, CursorWindow paramCursorWindow) {
    if (this.mCursor instanceof CrossProcessCursor) {
      CrossProcessCursor crossProcessCursor = (CrossProcessCursor)this.mCursor;
      crossProcessCursor.fillWindow(paramInt, paramCursorWindow);
      return;
    } 
    DatabaseUtils.cursorFillWindow(this.mCursor, paramInt, paramCursorWindow);
  }
  
  public CursorWindow getWindow() {
    if (this.mCursor instanceof CrossProcessCursor) {
      CrossProcessCursor crossProcessCursor = (CrossProcessCursor)this.mCursor;
      return crossProcessCursor.getWindow();
    } 
    return null;
  }
  
  public boolean onMove(int paramInt1, int paramInt2) {
    if (this.mCursor instanceof CrossProcessCursor) {
      CrossProcessCursor crossProcessCursor = (CrossProcessCursor)this.mCursor;
      return crossProcessCursor.onMove(paramInt1, paramInt2);
    } 
    return true;
  }
}
