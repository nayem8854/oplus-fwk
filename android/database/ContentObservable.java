package android.database;

import android.net.Uri;

public class ContentObservable extends Observable<ContentObserver> {
  public void registerObserver(ContentObserver paramContentObserver) {
    super.registerObserver(paramContentObserver);
  }
  
  @Deprecated
  public void dispatchChange(boolean paramBoolean) {
    dispatchChange(paramBoolean, null);
  }
  
  public void dispatchChange(boolean paramBoolean, Uri paramUri) {
    synchronized (this.mObservers) {
      for (ContentObserver contentObserver : this.mObservers) {
        if (!paramBoolean || contentObserver.deliverSelfNotifications())
          contentObserver.dispatchChange(paramBoolean, paramUri); 
      } 
      return;
    } 
  }
  
  @Deprecated
  public void notifyChange(boolean paramBoolean) {
    synchronized (this.mObservers) {
      for (ContentObserver contentObserver : this.mObservers)
        contentObserver.onChange(paramBoolean, null); 
      return;
    } 
  }
}
