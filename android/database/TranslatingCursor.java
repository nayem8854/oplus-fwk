package android.database;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.CancellationSignal;
import android.util.ArraySet;
import com.android.internal.util.ArrayUtils;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;

public class TranslatingCursor extends CrossProcessCursorWrapper {
  private final int mAuxiliaryColumnIndex;
  
  private final Config mConfig;
  
  private final boolean mDropLast;
  
  private final ArraySet<Integer> mTranslateColumnIndices;
  
  private final Translator mTranslator;
  
  class Config {
    public final String auxiliaryColumn;
    
    public final Uri baseUri;
    
    public final String[] translateColumns;
    
    public Config(TranslatingCursor this$0, String param1String, String... param1VarArgs) {
      this.baseUri = (Uri)this$0;
      this.auxiliaryColumn = param1String;
      this.translateColumns = param1VarArgs;
    }
  }
  
  public TranslatingCursor(Cursor paramCursor, Config paramConfig, Translator paramTranslator, boolean paramBoolean) {
    super(paramCursor);
    Objects.requireNonNull(paramConfig);
    this.mConfig = paramConfig;
    Objects.requireNonNull(paramTranslator);
    this.mTranslator = paramTranslator;
    this.mDropLast = paramBoolean;
    this.mAuxiliaryColumnIndex = paramCursor.getColumnIndexOrThrow(paramConfig.auxiliaryColumn);
    this.mTranslateColumnIndices = new ArraySet();
    for (byte b = 0; b < paramCursor.getColumnCount(); b++) {
      String str = paramCursor.getColumnName(b);
      if (ArrayUtils.contains((Object[])paramConfig.translateColumns, str))
        this.mTranslateColumnIndices.add(Integer.valueOf(b)); 
    } 
  }
  
  public int getColumnCount() {
    if (this.mDropLast)
      return super.getColumnCount() - 1; 
    return super.getColumnCount();
  }
  
  public String[] getColumnNames() {
    if (this.mDropLast)
      return Arrays.<String>copyOfRange(super.getColumnNames(), 0, super.getColumnCount() - 1); 
    return super.getColumnNames();
  }
  
  public static Cursor query(Config paramConfig, Translator paramTranslator, SQLiteQueryBuilder paramSQLiteQueryBuilder, SQLiteDatabase paramSQLiteDatabase, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, String paramString3, String paramString4, String paramString5, CancellationSignal paramCancellationSignal) {
    // Byte code:
    //   0: aload #4
    //   2: astore #12
    //   4: aload #4
    //   6: invokestatic isEmpty : ([Ljava/lang/Object;)Z
    //   9: istore #13
    //   11: iconst_0
    //   12: istore #14
    //   14: iload #13
    //   16: ifne -> 44
    //   19: aload_0
    //   20: getfield auxiliaryColumn : Ljava/lang/String;
    //   23: astore #15
    //   25: aload #12
    //   27: aload #15
    //   29: invokestatic contains : ([Ljava/lang/Object;Ljava/lang/Object;)Z
    //   32: ifeq -> 38
    //   35: goto -> 44
    //   38: iconst_0
    //   39: istore #16
    //   41: goto -> 47
    //   44: iconst_1
    //   45: istore #16
    //   47: aload #4
    //   49: invokestatic isEmpty : ([Ljava/lang/Object;)Z
    //   52: ifne -> 80
    //   55: aload_0
    //   56: getfield translateColumns : [Ljava/lang/String;
    //   59: astore #15
    //   61: aload #12
    //   63: aload #15
    //   65: invokestatic containsAny : ([Ljava/lang/Object;[Ljava/lang/Object;)Z
    //   68: ifeq -> 74
    //   71: goto -> 80
    //   74: iconst_0
    //   75: istore #17
    //   77: goto -> 83
    //   80: iconst_1
    //   81: istore #17
    //   83: iload #17
    //   85: ifne -> 110
    //   88: aload_2
    //   89: aload_3
    //   90: aload #4
    //   92: aload #5
    //   94: aload #6
    //   96: aload #7
    //   98: aload #8
    //   100: aload #9
    //   102: aload #10
    //   104: aload #11
    //   106: invokevirtual query : (Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    //   109: areturn
    //   110: aload #12
    //   112: astore #4
    //   114: iload #16
    //   116: ifne -> 135
    //   119: ldc java/lang/String
    //   121: aload #12
    //   123: aload_0
    //   124: getfield auxiliaryColumn : Ljava/lang/String;
    //   127: invokestatic appendElement : (Ljava/lang/Class;[Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;
    //   130: checkcast [Ljava/lang/String;
    //   133: astore #4
    //   135: aload_2
    //   136: aload_3
    //   137: aload #4
    //   139: aload #5
    //   141: aload #6
    //   143: aload #7
    //   145: aload #8
    //   147: aload #9
    //   149: invokevirtual query : (Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   152: astore_2
    //   153: iload #16
    //   155: ifne -> 161
    //   158: iconst_1
    //   159: istore #14
    //   161: new android/database/TranslatingCursor
    //   164: dup
    //   165: aload_2
    //   166: aload_0
    //   167: aload_1
    //   168: iload #14
    //   170: invokespecial <init> : (Landroid/database/Cursor;Landroid/database/TranslatingCursor$Config;Landroid/database/TranslatingCursor$Translator;Z)V
    //   173: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #105	-> 0
    //   #106	-> 25
    //   #107	-> 47
    //   #108	-> 61
    //   #112	-> 83
    //   #113	-> 88
    //   #118	-> 110
    //   #119	-> 119
    //   #123	-> 135
    //   #125	-> 153
  }
  
  public void fillWindow(int paramInt, CursorWindow paramCursorWindow) {
    DatabaseUtils.cursorFillWindow(this, paramInt, paramCursorWindow);
  }
  
  public CursorWindow getWindow() {
    return null;
  }
  
  public Cursor getWrappedCursor() {
    throw new UnsupportedOperationException("Returning underlying cursor risks leaking data");
  }
  
  public double getDouble(int paramInt) {
    if (!ArrayUtils.contains((Collection)this.mTranslateColumnIndices, Integer.valueOf(paramInt)))
      return super.getDouble(paramInt); 
    throw new IllegalArgumentException();
  }
  
  public float getFloat(int paramInt) {
    if (!ArrayUtils.contains((Collection)this.mTranslateColumnIndices, Integer.valueOf(paramInt)))
      return super.getFloat(paramInt); 
    throw new IllegalArgumentException();
  }
  
  public int getInt(int paramInt) {
    if (!ArrayUtils.contains((Collection)this.mTranslateColumnIndices, Integer.valueOf(paramInt)))
      return super.getInt(paramInt); 
    throw new IllegalArgumentException();
  }
  
  public long getLong(int paramInt) {
    if (!ArrayUtils.contains((Collection)this.mTranslateColumnIndices, Integer.valueOf(paramInt)))
      return super.getLong(paramInt); 
    throw new IllegalArgumentException();
  }
  
  public short getShort(int paramInt) {
    if (!ArrayUtils.contains((Collection)this.mTranslateColumnIndices, Integer.valueOf(paramInt)))
      return super.getShort(paramInt); 
    throw new IllegalArgumentException();
  }
  
  public String getString(int paramInt) {
    if (ArrayUtils.contains((Collection)this.mTranslateColumnIndices, Integer.valueOf(paramInt))) {
      Translator translator = this.mTranslator;
      String str1 = super.getString(paramInt);
      int i = this.mAuxiliaryColumnIndex;
      String str2 = getColumnName(paramInt);
      return translator.translate(str1, i, str2, this);
    } 
    return super.getString(paramInt);
  }
  
  public void copyStringToBuffer(int paramInt, CharArrayBuffer paramCharArrayBuffer) {
    if (!ArrayUtils.contains((Collection)this.mTranslateColumnIndices, Integer.valueOf(paramInt))) {
      super.copyStringToBuffer(paramInt, paramCharArrayBuffer);
      return;
    } 
    throw new IllegalArgumentException();
  }
  
  public byte[] getBlob(int paramInt) {
    if (!ArrayUtils.contains((Collection)this.mTranslateColumnIndices, Integer.valueOf(paramInt)))
      return super.getBlob(paramInt); 
    throw new IllegalArgumentException();
  }
  
  public int getType(int paramInt) {
    if (ArrayUtils.contains((Collection)this.mTranslateColumnIndices, Integer.valueOf(paramInt)))
      return 3; 
    return super.getType(paramInt);
  }
  
  public boolean isNull(int paramInt) {
    if (ArrayUtils.contains((Collection)this.mTranslateColumnIndices, Integer.valueOf(paramInt))) {
      boolean bool;
      if (getString(paramInt) == null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
    return super.isNull(paramInt);
  }
  
  class Translator {
    public abstract String translate(String param1String1, int param1Int, String param1String2, Cursor param1Cursor);
  }
}
