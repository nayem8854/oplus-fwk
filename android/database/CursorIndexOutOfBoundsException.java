package android.database;

public class CursorIndexOutOfBoundsException extends IndexOutOfBoundsException {
  public CursorIndexOutOfBoundsException(int paramInt1, int paramInt2) {
    super(stringBuilder.toString());
  }
  
  public CursorIndexOutOfBoundsException(String paramString) {
    super(paramString);
  }
}
