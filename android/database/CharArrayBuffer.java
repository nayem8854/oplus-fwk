package android.database;

public final class CharArrayBuffer {
  public char[] data;
  
  public int sizeCopied;
  
  public CharArrayBuffer(int paramInt) {
    this.data = new char[paramInt];
  }
  
  public CharArrayBuffer(char[] paramArrayOfchar) {
    this.data = paramArrayOfchar;
  }
}
