package android.database;

import android.util.SparseArray;
import java.util.Map;

public class RedactingCursor extends CrossProcessCursorWrapper {
  private final SparseArray<Object> mRedactions;
  
  private RedactingCursor(Cursor paramCursor, SparseArray<Object> paramSparseArray) {
    super(paramCursor);
    this.mRedactions = paramSparseArray;
  }
  
  public static Cursor create(Cursor paramCursor, Map<String, Object> paramMap) {
    SparseArray<Object> sparseArray = new SparseArray();
    String[] arrayOfString = paramCursor.getColumnNames();
    for (byte b = 0; b < arrayOfString.length; b++) {
      if (paramMap.containsKey(arrayOfString[b]))
        sparseArray.put(b, paramMap.get(arrayOfString[b])); 
    } 
    if (sparseArray.size() == 0)
      return paramCursor; 
    return new RedactingCursor(paramCursor, sparseArray);
  }
  
  public void fillWindow(int paramInt, CursorWindow paramCursorWindow) {
    DatabaseUtils.cursorFillWindow(this, paramInt, paramCursorWindow);
  }
  
  public CursorWindow getWindow() {
    return null;
  }
  
  public Cursor getWrappedCursor() {
    throw new UnsupportedOperationException("Returning underlying cursor risks leaking redacted data");
  }
  
  public double getDouble(int paramInt) {
    int i = this.mRedactions.indexOfKey(paramInt);
    if (i >= 0)
      return ((Double)this.mRedactions.valueAt(i)).doubleValue(); 
    return super.getDouble(paramInt);
  }
  
  public float getFloat(int paramInt) {
    int i = this.mRedactions.indexOfKey(paramInt);
    if (i >= 0)
      return ((Float)this.mRedactions.valueAt(i)).floatValue(); 
    return super.getFloat(paramInt);
  }
  
  public int getInt(int paramInt) {
    int i = this.mRedactions.indexOfKey(paramInt);
    if (i >= 0)
      return ((Integer)this.mRedactions.valueAt(i)).intValue(); 
    return super.getInt(paramInt);
  }
  
  public long getLong(int paramInt) {
    int i = this.mRedactions.indexOfKey(paramInt);
    if (i >= 0)
      return ((Long)this.mRedactions.valueAt(i)).longValue(); 
    return super.getLong(paramInt);
  }
  
  public short getShort(int paramInt) {
    int i = this.mRedactions.indexOfKey(paramInt);
    if (i >= 0)
      return ((Short)this.mRedactions.valueAt(i)).shortValue(); 
    return super.getShort(paramInt);
  }
  
  public String getString(int paramInt) {
    int i = this.mRedactions.indexOfKey(paramInt);
    if (i >= 0)
      return (String)this.mRedactions.valueAt(i); 
    return super.getString(paramInt);
  }
  
  public void copyStringToBuffer(int paramInt, CharArrayBuffer paramCharArrayBuffer) {
    int i = this.mRedactions.indexOfKey(paramInt);
    if (i >= 0) {
      paramCharArrayBuffer.data = ((String)this.mRedactions.valueAt(i)).toCharArray();
      paramCharArrayBuffer.sizeCopied = paramCharArrayBuffer.data.length;
    } else {
      super.copyStringToBuffer(paramInt, paramCharArrayBuffer);
    } 
  }
  
  public byte[] getBlob(int paramInt) {
    int i = this.mRedactions.indexOfKey(paramInt);
    if (i >= 0)
      return (byte[])this.mRedactions.valueAt(i); 
    return super.getBlob(paramInt);
  }
  
  public int getType(int paramInt) {
    int i = this.mRedactions.indexOfKey(paramInt);
    if (i >= 0)
      return DatabaseUtils.getTypeOfObject(this.mRedactions.valueAt(i)); 
    return super.getType(paramInt);
  }
  
  public boolean isNull(int paramInt) {
    int i = this.mRedactions.indexOfKey(paramInt);
    if (i >= 0) {
      boolean bool;
      if (this.mRedactions.valueAt(i) == null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
    return super.isNull(paramInt);
  }
}
