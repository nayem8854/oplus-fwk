package android.database;

import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

public final class BulkCursorToCursorAdaptor extends AbstractWindowedCursor {
  private static final String TAG = "BulkCursor";
  
  private IBulkCursor mBulkCursor;
  
  private String[] mColumns;
  
  private int mCount;
  
  private AbstractCursor.SelfContentObserver mObserverBridge = new AbstractCursor.SelfContentObserver(this);
  
  private boolean mWantsAllOnMoveCalls;
  
  public void initialize(BulkCursorDescriptor paramBulkCursorDescriptor) {
    this.mBulkCursor = paramBulkCursorDescriptor.cursor;
    this.mColumns = paramBulkCursorDescriptor.columnNames;
    this.mWantsAllOnMoveCalls = paramBulkCursorDescriptor.wantsAllOnMoveCalls;
    this.mCount = paramBulkCursorDescriptor.count;
    if (paramBulkCursorDescriptor.window != null)
      setWindow(paramBulkCursorDescriptor.window); 
  }
  
  public IContentObserver getObserver() {
    return this.mObserverBridge.getContentObserver();
  }
  
  private void throwIfCursorIsClosed() {
    if (this.mBulkCursor != null)
      return; 
    throw new StaleDataException("Attempted to access a cursor after it has been closed.");
  }
  
  public int getCount() {
    throwIfCursorIsClosed();
    return this.mCount;
  }
  
  public boolean onMove(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial throwIfCursorIsClosed : ()V
    //   4: aload_0
    //   5: getfield mWindow : Landroid/database/CursorWindow;
    //   8: ifnull -> 68
    //   11: aload_0
    //   12: getfield mWindow : Landroid/database/CursorWindow;
    //   15: astore_3
    //   16: iload_2
    //   17: aload_3
    //   18: invokevirtual getStartPosition : ()I
    //   21: if_icmplt -> 68
    //   24: aload_0
    //   25: getfield mWindow : Landroid/database/CursorWindow;
    //   28: astore_3
    //   29: iload_2
    //   30: aload_3
    //   31: invokevirtual getStartPosition : ()I
    //   34: aload_0
    //   35: getfield mWindow : Landroid/database/CursorWindow;
    //   38: invokevirtual getNumRows : ()I
    //   41: iadd
    //   42: if_icmplt -> 48
    //   45: goto -> 68
    //   48: aload_0
    //   49: getfield mWantsAllOnMoveCalls : Z
    //   52: ifeq -> 82
    //   55: aload_0
    //   56: getfield mBulkCursor : Landroid/database/IBulkCursor;
    //   59: iload_2
    //   60: invokeinterface onMove : (I)V
    //   65: goto -> 82
    //   68: aload_0
    //   69: aload_0
    //   70: getfield mBulkCursor : Landroid/database/IBulkCursor;
    //   73: iload_2
    //   74: invokeinterface getWindow : (I)Landroid/database/CursorWindow;
    //   79: invokevirtual setWindow : (Landroid/database/CursorWindow;)V
    //   82: aload_0
    //   83: getfield mWindow : Landroid/database/CursorWindow;
    //   86: ifnonnull -> 91
    //   89: iconst_0
    //   90: ireturn
    //   91: iconst_1
    //   92: ireturn
    //   93: astore_3
    //   94: ldc 'BulkCursor'
    //   96: ldc 'Unable to get window because the remote process is dead'
    //   98: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   101: pop
    //   102: iconst_0
    //   103: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #75	-> 0
    //   #79	-> 4
    //   #80	-> 16
    //   #81	-> 29
    //   #83	-> 48
    //   #84	-> 55
    //   #82	-> 68
    //   #90	-> 82
    //   #93	-> 82
    //   #94	-> 89
    //   #97	-> 91
    //   #86	-> 93
    //   #88	-> 94
    //   #89	-> 102
    // Exception table:
    //   from	to	target	type
    //   4	16	93	android/os/RemoteException
    //   16	29	93	android/os/RemoteException
    //   29	45	93	android/os/RemoteException
    //   48	55	93	android/os/RemoteException
    //   55	65	93	android/os/RemoteException
    //   68	82	93	android/os/RemoteException
  }
  
  public void deactivate() {
    super.deactivate();
    IBulkCursor iBulkCursor = this.mBulkCursor;
    if (iBulkCursor != null)
      try {
        iBulkCursor.deactivate();
      } catch (RemoteException remoteException) {
        Log.w("BulkCursor", "Remote process exception when deactivating");
      }  
  }
  
  public void close() {
    super.close();
    IBulkCursor iBulkCursor = this.mBulkCursor;
    if (iBulkCursor != null)
      try {
        iBulkCursor.close();
        this.mBulkCursor = null;
      } catch (RemoteException remoteException) {
        Log.w("BulkCursor", "Remote process exception when closing");
        this.mBulkCursor = null;
      } finally {} 
  }
  
  public boolean requery() {
    throwIfCursorIsClosed();
    try {
      int i = this.mBulkCursor.requery(getObserver());
      if (i != -1) {
        this.mPos = -1;
        closeWindow();
        super.requery();
        return true;
      } 
      deactivate();
      return false;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to requery because the remote process exception ");
      stringBuilder.append(exception.getMessage());
      Log.e("BulkCursor", stringBuilder.toString());
      deactivate();
      return false;
    } 
  }
  
  public String[] getColumnNames() {
    throwIfCursorIsClosed();
    return this.mColumns;
  }
  
  public Bundle getExtras() {
    throwIfCursorIsClosed();
    try {
      return this.mBulkCursor.getExtras();
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException);
    } 
  }
  
  public Bundle respond(Bundle paramBundle) {
    throwIfCursorIsClosed();
    try {
      return this.mBulkCursor.respond(paramBundle);
    } catch (RemoteException remoteException) {
      Log.w("BulkCursor", "respond() threw RemoteException, returning an empty bundle.", (Throwable)remoteException);
      return Bundle.EMPTY;
    } 
  }
}
