package android.database;

public interface CrossProcessCursor extends Cursor {
  void fillWindow(int paramInt, CursorWindow paramCursorWindow);
  
  CursorWindow getWindow();
  
  boolean onMove(int paramInt1, int paramInt2);
}
