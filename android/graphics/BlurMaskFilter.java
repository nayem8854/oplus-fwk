package android.graphics;

public class BlurMaskFilter extends MaskFilter {
  class Blur extends Enum<Blur> {
    private static final Blur[] $VALUES;
    
    public static final Blur INNER;
    
    public static Blur valueOf(String param1String) {
      return Enum.<Blur>valueOf(Blur.class, param1String);
    }
    
    public static Blur[] values() {
      return (Blur[])$VALUES.clone();
    }
    
    public static final Blur NORMAL = new Blur("NORMAL", 0, 0);
    
    public static final Blur OUTER = new Blur("OUTER", 2, 2);
    
    public static final Blur SOLID = new Blur("SOLID", 1, 1);
    
    final int native_int;
    
    static {
      Blur blur = new Blur("INNER", 3, 3);
      $VALUES = new Blur[] { NORMAL, SOLID, OUTER, blur };
    }
    
    private Blur(BlurMaskFilter this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.native_int = param1Int2;
    }
  }
  
  public BlurMaskFilter(float paramFloat, Blur paramBlur) {
    this.native_instance = nativeConstructor(paramFloat, paramBlur.native_int);
  }
  
  private static native long nativeConstructor(float paramFloat, int paramInt);
}
