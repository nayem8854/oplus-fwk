package android.graphics;

public class SweepGradient extends Shader {
  private int mColor0;
  
  private int mColor1;
  
  private long[] mColorLongs;
  
  private int[] mColors;
  
  private float mCx;
  
  private float mCy;
  
  private float[] mPositions;
  
  public SweepGradient(float paramFloat1, float paramFloat2, int[] paramArrayOfint, float[] paramArrayOffloat) {
    this(paramFloat1, paramFloat2, convertColors(paramArrayOfint), paramArrayOffloat, ColorSpace.get(ColorSpace.Named.SRGB));
  }
  
  public SweepGradient(float paramFloat1, float paramFloat2, long[] paramArrayOflong, float[] paramArrayOffloat) {
    this(paramFloat1, paramFloat2, (long[])paramArrayOflong.clone(), paramArrayOffloat, detectColorSpace(paramArrayOflong));
  }
  
  private SweepGradient(float paramFloat1, float paramFloat2, long[] paramArrayOflong, float[] paramArrayOffloat, ColorSpace paramColorSpace) {
    super(paramColorSpace);
    if (paramArrayOffloat == null || paramArrayOflong.length == paramArrayOffloat.length) {
      this.mCx = paramFloat1;
      this.mCy = paramFloat2;
      this.mColorLongs = paramArrayOflong;
      if (paramArrayOffloat != null) {
        float[] arrayOfFloat = (float[])paramArrayOffloat.clone();
      } else {
        paramArrayOflong = null;
      } 
      this.mPositions = (float[])paramArrayOflong;
      return;
    } 
    throw new IllegalArgumentException("color and position arrays must be of equal length");
  }
  
  public SweepGradient(float paramFloat1, float paramFloat2, int paramInt1, int paramInt2) {
    this(paramFloat1, paramFloat2, Color.pack(paramInt1), Color.pack(paramInt2));
  }
  
  public SweepGradient(float paramFloat1, float paramFloat2, long paramLong1, long paramLong2) {
    this(paramFloat1, paramFloat2, new long[] { paramLong1, paramLong2 }, (float[])null);
  }
  
  long createNativeInstance(long paramLong) {
    float f1 = this.mCx, f2 = this.mCy;
    long[] arrayOfLong = this.mColorLongs;
    float[] arrayOfFloat = this.mPositions;
    long l = colorSpace().getNativeInstance();
    return nativeCreate(paramLong, f1, f2, arrayOfLong, arrayOfFloat, l);
  }
  
  public void setColors(long[] paramArrayOflong) {
    this.mColorLongs = paramArrayOflong;
    discardNativeInstance();
  }
  
  public long[] getColorLongs() {
    return this.mColorLongs;
  }
  
  private static native long nativeCreate(long paramLong1, float paramFloat1, float paramFloat2, long[] paramArrayOflong, float[] paramArrayOffloat, long paramLong2);
}
