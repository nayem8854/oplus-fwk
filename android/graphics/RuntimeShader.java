package android.graphics;

import libcore.util.NativeAllocationRegistry;

public class RuntimeShader extends Shader {
  private boolean mIsOpaque;
  
  private long mNativeInstanceRuntimeShaderFactory;
  
  private byte[] mUniforms;
  
  class NoImagePreloadHolder {
    public static final NativeAllocationRegistry sRegistry;
    
    static {
      ClassLoader classLoader = RuntimeShader.class.getClassLoader();
      long l = RuntimeShader.nativeGetFinalizer();
      sRegistry = NativeAllocationRegistry.createMalloced(classLoader, l);
    }
  }
  
  public RuntimeShader(String paramString, byte[] paramArrayOfbyte, boolean paramBoolean) {
    this(paramString, paramArrayOfbyte, paramBoolean, ColorSpace.get(ColorSpace.Named.SRGB));
  }
  
  private RuntimeShader(String paramString, byte[] paramArrayOfbyte, boolean paramBoolean, ColorSpace paramColorSpace) {
    super(paramColorSpace);
    this.mUniforms = paramArrayOfbyte;
    this.mIsOpaque = paramBoolean;
    this.mNativeInstanceRuntimeShaderFactory = nativeCreateShaderFactory(paramString);
    NoImagePreloadHolder.sRegistry.registerNativeAllocation(this, this.mNativeInstanceRuntimeShaderFactory);
  }
  
  public void updateUniforms(byte[] paramArrayOfbyte) {
    this.mUniforms = paramArrayOfbyte;
    discardNativeInstance();
  }
  
  long createNativeInstance(long paramLong) {
    long l1 = this.mNativeInstanceRuntimeShaderFactory;
    byte[] arrayOfByte = this.mUniforms;
    long l2 = colorSpace().getNativeInstance();
    boolean bool = this.mIsOpaque;
    return nativeCreate(l1, paramLong, arrayOfByte, l2, bool);
  }
  
  private static native long nativeCreate(long paramLong1, long paramLong2, byte[] paramArrayOfbyte, long paramLong3, boolean paramBoolean);
  
  private static native long nativeCreateShaderFactory(String paramString);
  
  private static native long nativeGetFinalizer();
}
