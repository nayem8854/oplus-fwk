package android.graphics;

import libcore.util.NativeAllocationRegistry;

public class ColorFilter extends OplusBaseOplusFilter {
  private Runnable mCleaner;
  
  private long mNativeInstance;
  
  private static native long nativeGetFinalizer();
  
  class NoImagePreloadHolder {
    public static final NativeAllocationRegistry sRegistry;
    
    static {
      ClassLoader classLoader = ColorFilter.class.getClassLoader();
      long l = ColorFilter.nativeGetFinalizer();
      sRegistry = NativeAllocationRegistry.createMalloced(classLoader, l);
    }
  }
  
  long createNativeInstance() {
    return 0L;
  }
  
  void discardNativeInstance() {
    if (this.mNativeInstance != 0L) {
      this.mCleaner.run();
      this.mCleaner = null;
      this.mNativeInstance = 0L;
    } 
  }
  
  public long getNativeInstance() {
    if (this.mNativeInstance == 0L) {
      long l = createNativeInstance();
      if (l != 0L)
        this.mCleaner = NoImagePreloadHolder.sRegistry.registerNativeAllocation(this, this.mNativeInstance); 
    } 
    return this.mNativeInstance;
  }
}
