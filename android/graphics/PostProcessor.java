package android.graphics;

public interface PostProcessor {
  int onPostProcess(Canvas paramCanvas);
}
