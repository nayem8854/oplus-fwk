package android.graphics;

public abstract class OplusBaseHardwareRenderer {
  private float mDialogBgMaxL = -1.0F;
  
  private float mBackgroundMaxL = -1.0F;
  
  private float mForegroundMinL = -1.0F;
  
  public long getNativeProxy() {
    return -1L;
  }
  
  public float getDarkModeDialogMaxBgMaxL() {
    return this.mDialogBgMaxL;
  }
  
  public boolean setForceDarkArgs(float paramFloat1, float paramFloat2, float paramFloat3) {
    return false;
  }
  
  public boolean setUsageForceDarkArgs(float paramFloat1, float paramFloat2, float paramFloat3) {
    long l = getNativeProxy();
    if (l != -1L && (
      this.mDialogBgMaxL != paramFloat1 || this.mBackgroundMaxL != paramFloat2 || this.mForegroundMinL != paramFloat3)) {
      this.mDialogBgMaxL = paramFloat1;
      this.mBackgroundMaxL = paramFloat2;
      this.mForegroundMinL = paramFloat3;
      return true;
    } 
    return false;
  }
}
