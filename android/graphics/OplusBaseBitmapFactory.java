package android.graphics;

import com.oplus.util.OplusTypeCastingHelper;

public class OplusBaseBitmapFactory {
  protected static void setAssetSourceAndHasCalculatedColor(Bitmap paramBitmap, boolean paramBoolean1, boolean paramBoolean2) {
    if (paramBitmap != null) {
      OplusBaseBitmap oplusBaseBitmap = (OplusBaseBitmap)OplusTypeCastingHelper.typeCasting(OplusBaseBitmap.class, paramBitmap);
      if (oplusBaseBitmap != null) {
        oplusBaseBitmap.setIsAssetSource(paramBoolean1);
        oplusBaseBitmap.setHasCalculatedColor(paramBoolean2);
      } 
    } 
  }
  
  protected static void setIsAssetSource(Bitmap paramBitmap, boolean paramBoolean) {
    if (paramBitmap != null) {
      OplusBaseBitmap oplusBaseBitmap = (OplusBaseBitmap)OplusTypeCastingHelper.typeCasting(OplusBaseBitmap.class, paramBitmap);
      if (oplusBaseBitmap != null)
        oplusBaseBitmap.setIsAssetSource(paramBoolean); 
    } 
  }
  
  protected static void setHasCalculatedColor(Bitmap paramBitmap, boolean paramBoolean) {
    if (paramBitmap != null) {
      OplusBaseBitmap oplusBaseBitmap = (OplusBaseBitmap)OplusTypeCastingHelper.typeCasting(OplusBaseBitmap.class, paramBitmap);
      if (oplusBaseBitmap != null)
        oplusBaseBitmap.setHasCalculatedColor(paramBoolean); 
    } 
  }
}
