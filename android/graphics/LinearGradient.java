package android.graphics;

public class LinearGradient extends Shader {
  private int mColor0;
  
  private int mColor1;
  
  private long[] mColorLongs;
  
  private int[] mColors;
  
  private float[] mPositions;
  
  private Shader.TileMode mTileMode;
  
  private float mX0;
  
  private float mX1;
  
  private float mY0;
  
  private float mY1;
  
  public LinearGradient(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int[] paramArrayOfint, float[] paramArrayOffloat, Shader.TileMode paramTileMode) {
    this(paramFloat1, paramFloat2, paramFloat3, paramFloat4, arrayOfLong, paramArrayOffloat, paramTileMode, colorSpace);
  }
  
  public LinearGradient(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, long[] paramArrayOflong, float[] paramArrayOffloat, Shader.TileMode paramTileMode) {
    this(paramFloat1, paramFloat2, paramFloat3, paramFloat4, (long[])paramArrayOflong.clone(), paramArrayOffloat, paramTileMode, detectColorSpace(paramArrayOflong));
  }
  
  private LinearGradient(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, long[] paramArrayOflong, float[] paramArrayOffloat, Shader.TileMode paramTileMode, ColorSpace paramColorSpace) {
    super(paramColorSpace);
    if (paramArrayOffloat == null || paramArrayOflong.length == paramArrayOffloat.length) {
      this.mX0 = paramFloat1;
      this.mY0 = paramFloat2;
      this.mX1 = paramFloat3;
      this.mY1 = paramFloat4;
      this.mColorLongs = paramArrayOflong;
      if (paramArrayOffloat != null) {
        float[] arrayOfFloat = (float[])paramArrayOffloat.clone();
      } else {
        paramArrayOflong = null;
      } 
      this.mPositions = (float[])paramArrayOflong;
      this.mTileMode = paramTileMode;
      return;
    } 
    throw new IllegalArgumentException("color and position arrays must be of equal length");
  }
  
  public LinearGradient(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt1, int paramInt2, Shader.TileMode paramTileMode) {
    this(paramFloat1, paramFloat2, paramFloat3, paramFloat4, Color.pack(paramInt1), Color.pack(paramInt2), paramTileMode);
  }
  
  public LinearGradient(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, long paramLong1, long paramLong2, Shader.TileMode paramTileMode) {
    this(paramFloat1, paramFloat2, paramFloat3, paramFloat4, new long[] { paramLong1, paramLong2 }, (float[])null, paramTileMode);
  }
  
  long createNativeInstance(long paramLong) {
    float f1 = this.mX0, f2 = this.mY0, f3 = this.mX1, f4 = this.mY1;
    long[] arrayOfLong = this.mColorLongs;
    float[] arrayOfFloat = this.mPositions;
    int i = this.mTileMode.nativeInt;
    long l = colorSpace().getNativeInstance();
    return nativeCreate(paramLong, f1, f2, f3, f4, arrayOfLong, arrayOfFloat, i, l);
  }
  
  public void setColors(long[] paramArrayOflong) {
    this.mColorLongs = paramArrayOflong;
    discardNativeInstance();
  }
  
  public long[] getColorLongs() {
    return this.mColorLongs;
  }
  
  private native long nativeCreate(long paramLong1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, long[] paramArrayOflong, float[] paramArrayOffloat, int paramInt, long paramLong2);
}
