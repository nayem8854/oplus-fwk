package android.graphics.drawable;

public interface Animatable2 extends Animatable {
  void clearAnimationCallbacks();
  
  void registerAnimationCallback(AnimationCallback paramAnimationCallback);
  
  boolean unregisterAnimationCallback(AnimationCallback paramAnimationCallback);
  
  class AnimationCallback {
    public void onAnimationStart(Drawable param1Drawable) {}
    
    public void onAnimationEnd(Drawable param1Drawable) {}
  }
}
