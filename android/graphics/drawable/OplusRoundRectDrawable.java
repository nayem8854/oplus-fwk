package android.graphics.drawable;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;

public class OplusRoundRectDrawable extends Drawable {
  Bitmap mBitmap;
  
  int mBottom;
  
  Drawable mDrawable;
  
  int mLeft;
  
  Paint mPaint;
  
  float mRadius = 0.0F;
  
  RectF mRectF;
  
  int mRight;
  
  int mTop;
  
  public OplusRoundRectDrawable(Drawable paramDrawable, float paramFloat) {
    this(paramDrawable, paramFloat, 0, 0, 1080, 2340);
  }
  
  public OplusRoundRectDrawable(Drawable paramDrawable, float paramFloat, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mDrawable = paramDrawable;
    this.mRadius = paramFloat;
    this.mLeft = paramInt1;
    this.mTop = paramInt2;
    this.mRight = paramInt3;
    this.mBottom = paramInt4;
    if (paramDrawable != null) {
      this.mBitmap = drawableToBitmap(paramDrawable);
      Paint paint = new Paint();
      paint.setAntiAlias(true);
      this.mPaint.setDither(true);
      this.mPaint.setShader(new BitmapShader(this.mBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
    } 
  }
  
  public void setBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.setBounds(paramInt1, paramInt2, paramInt3, paramInt4);
    RectF rectF = this.mRectF;
    if (rectF == null) {
      this.mRectF = new RectF(paramInt1, paramInt2, paramInt3, paramInt4);
    } else {
      rectF.set(paramInt1, paramInt2, paramInt3, paramInt4);
    } 
  }
  
  public void setColorRoundBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat) {
    RectF rectF = this.mRectF;
    if (rectF == null) {
      this.mRectF = new RectF(paramInt1, paramInt2, paramInt3, paramInt4);
    } else {
      rectF.set(paramInt1, paramInt2, paramInt3, paramInt4);
    } 
    this.mRadius = paramFloat;
    invalidateSelf();
  }
  
  public int getIntrinsicHeight() {
    return this.mBitmap.getHeight();
  }
  
  public int getIntrinsicWidth() {
    return this.mBitmap.getWidth();
  }
  
  public void draw(Canvas paramCanvas) {
    RectF rectF = this.mRectF;
    float f = this.mRadius;
    paramCanvas.drawRoundRect(rectF, f, f, this.mPaint);
  }
  
  public void setAlpha(int paramInt) {
    this.mPaint.setAlpha(paramInt);
  }
  
  public void setColorFilter(ColorFilter paramColorFilter) {
    this.mPaint.setColorFilter(paramColorFilter);
  }
  
  public int getOpacity() {
    return -3;
  }
  
  Bitmap drawableToBitmap(Drawable paramDrawable) {
    Bitmap.Config config;
    int i = this.mRight, j = this.mLeft;
    int k = this.mBottom, m = this.mTop;
    if (paramDrawable.getOpacity() != -1) {
      config = Bitmap.Config.ARGB_8888;
    } else {
      config = Bitmap.Config.RGB_565;
    } 
    Bitmap bitmap = Bitmap.createBitmap(i - j, k - m, config);
    Canvas canvas = new Canvas(bitmap);
    paramDrawable.setBounds(this.mLeft, this.mTop, this.mRight, this.mBottom);
    paramDrawable.draw(canvas);
    return bitmap;
  }
}
