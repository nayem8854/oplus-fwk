package android.graphics.drawable;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.ImageDecoder;
import android.graphics.Insets;
import android.graphics.NinePatch;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Region;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import com.android.internal.R;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class NinePatchDrawable extends Drawable {
  private Insets mOpticalInsets = Insets.NONE;
  
  private int mTargetDensity = 160;
  
  private int mBitmapWidth = -1;
  
  private int mBitmapHeight = -1;
  
  private static final boolean DEFAULT_DITHER = false;
  
  private BlendModeColorFilter mBlendModeFilter;
  
  private boolean mMutated;
  
  private NinePatchState mNinePatchState;
  
  private Rect mOutlineInsets;
  
  private float mOutlineRadius;
  
  private Rect mPadding;
  
  private Paint mPaint;
  
  private Rect mTempRect;
  
  NinePatchDrawable() {
    this.mNinePatchState = new NinePatchState();
  }
  
  @Deprecated
  public NinePatchDrawable(Bitmap paramBitmap, byte[] paramArrayOfbyte, Rect paramRect, String paramString) {
    this(new NinePatchState(new NinePatch(paramBitmap, paramArrayOfbyte, paramString), paramRect), (Resources)null);
  }
  
  public NinePatchDrawable(Resources paramResources, Bitmap paramBitmap, byte[] paramArrayOfbyte, Rect paramRect, String paramString) {
    this(new NinePatchState(new NinePatch(paramBitmap, paramArrayOfbyte, paramString), paramRect), paramResources);
  }
  
  public NinePatchDrawable(Resources paramResources, Bitmap paramBitmap, byte[] paramArrayOfbyte, Rect paramRect1, Rect paramRect2, String paramString) {
    this(new NinePatchState(new NinePatch(paramBitmap, paramArrayOfbyte, paramString), paramRect1, paramRect2), paramResources);
  }
  
  @Deprecated
  public NinePatchDrawable(NinePatch paramNinePatch) {
    this(new NinePatchState(paramNinePatch, new Rect()), (Resources)null);
  }
  
  public NinePatchDrawable(Resources paramResources, NinePatch paramNinePatch) {
    this(new NinePatchState(paramNinePatch, new Rect()), paramResources);
  }
  
  public void setTargetDensity(Canvas paramCanvas) {
    setTargetDensity(paramCanvas.getDensity());
  }
  
  public void setTargetDensity(DisplayMetrics paramDisplayMetrics) {
    setTargetDensity(paramDisplayMetrics.densityDpi);
  }
  
  public void setTargetDensity(int paramInt) {
    int i = paramInt;
    if (paramInt == 0)
      i = 160; 
    if (this.mTargetDensity != i) {
      this.mTargetDensity = i;
      computeBitmapSize();
      invalidateSelf();
    } 
  }
  
  public void draw(Canvas paramCanvas) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mNinePatchState : Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;
    //   4: astore_2
    //   5: aload_0
    //   6: invokevirtual getBounds : ()Landroid/graphics/Rect;
    //   9: astore_3
    //   10: iconst_m1
    //   11: istore #4
    //   13: aload_0
    //   14: getfield mBlendModeFilter : Landroid/graphics/BlendModeColorFilter;
    //   17: ifnull -> 48
    //   20: aload_0
    //   21: invokevirtual getPaint : ()Landroid/graphics/Paint;
    //   24: invokevirtual getColorFilter : ()Landroid/graphics/ColorFilter;
    //   27: ifnonnull -> 48
    //   30: aload_0
    //   31: getfield mPaint : Landroid/graphics/Paint;
    //   34: aload_0
    //   35: getfield mBlendModeFilter : Landroid/graphics/BlendModeColorFilter;
    //   38: invokevirtual setColorFilter : (Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
    //   41: pop
    //   42: iconst_1
    //   43: istore #5
    //   45: goto -> 51
    //   48: iconst_0
    //   49: istore #5
    //   51: aload_2
    //   52: getfield mBaseAlpha : F
    //   55: fconst_1
    //   56: fcmpl
    //   57: ifeq -> 92
    //   60: aload_0
    //   61: invokevirtual getPaint : ()Landroid/graphics/Paint;
    //   64: invokevirtual getAlpha : ()I
    //   67: istore #6
    //   69: aload_0
    //   70: getfield mPaint : Landroid/graphics/Paint;
    //   73: iload #6
    //   75: i2f
    //   76: aload_2
    //   77: getfield mBaseAlpha : F
    //   80: fmul
    //   81: ldc_w 0.5
    //   84: fadd
    //   85: f2i
    //   86: invokevirtual setAlpha : (I)V
    //   89: goto -> 95
    //   92: iconst_m1
    //   93: istore #6
    //   95: aload_1
    //   96: invokevirtual getDensity : ()I
    //   99: ifne -> 122
    //   102: aload_2
    //   103: getfield mNinePatch : Landroid/graphics/NinePatch;
    //   106: astore #7
    //   108: aload #7
    //   110: invokevirtual getDensity : ()I
    //   113: ifeq -> 122
    //   116: iconst_1
    //   117: istore #8
    //   119: goto -> 125
    //   122: iconst_0
    //   123: istore #8
    //   125: aload_3
    //   126: astore #7
    //   128: iload #8
    //   130: ifeq -> 275
    //   133: iconst_m1
    //   134: iflt -> 143
    //   137: iconst_m1
    //   138: istore #4
    //   140: goto -> 149
    //   143: aload_1
    //   144: invokevirtual save : ()I
    //   147: istore #4
    //   149: aload_0
    //   150: getfield mTargetDensity : I
    //   153: i2f
    //   154: aload_2
    //   155: getfield mNinePatch : Landroid/graphics/NinePatch;
    //   158: invokevirtual getDensity : ()I
    //   161: i2f
    //   162: fdiv
    //   163: fstore #9
    //   165: aload_3
    //   166: getfield left : I
    //   169: i2f
    //   170: fstore #10
    //   172: aload_3
    //   173: getfield top : I
    //   176: i2f
    //   177: fstore #11
    //   179: aload_1
    //   180: fload #9
    //   182: fload #9
    //   184: fload #10
    //   186: fload #11
    //   188: invokevirtual scale : (FFFF)V
    //   191: aload_0
    //   192: getfield mTempRect : Landroid/graphics/Rect;
    //   195: ifnonnull -> 209
    //   198: aload_0
    //   199: new android/graphics/Rect
    //   202: dup
    //   203: invokespecial <init> : ()V
    //   206: putfield mTempRect : Landroid/graphics/Rect;
    //   209: aload_0
    //   210: getfield mTempRect : Landroid/graphics/Rect;
    //   213: astore #7
    //   215: aload #7
    //   217: aload_3
    //   218: getfield left : I
    //   221: putfield left : I
    //   224: aload #7
    //   226: aload_3
    //   227: getfield top : I
    //   230: putfield top : I
    //   233: aload #7
    //   235: aload_3
    //   236: getfield left : I
    //   239: aload_3
    //   240: invokevirtual width : ()I
    //   243: i2f
    //   244: fload #9
    //   246: fdiv
    //   247: invokestatic round : (F)I
    //   250: iadd
    //   251: putfield right : I
    //   254: aload #7
    //   256: aload_3
    //   257: getfield top : I
    //   260: aload_3
    //   261: invokevirtual height : ()I
    //   264: i2f
    //   265: fload #9
    //   267: fdiv
    //   268: invokestatic round : (F)I
    //   271: iadd
    //   272: putfield bottom : I
    //   275: aload_0
    //   276: invokespecial needsMirroring : ()Z
    //   279: istore #12
    //   281: iload #4
    //   283: istore #8
    //   285: iload #12
    //   287: ifeq -> 352
    //   290: iload #4
    //   292: iflt -> 298
    //   295: goto -> 304
    //   298: aload_1
    //   299: invokevirtual save : ()I
    //   302: istore #4
    //   304: aload #7
    //   306: getfield left : I
    //   309: aload #7
    //   311: getfield right : I
    //   314: iadd
    //   315: i2f
    //   316: fconst_2
    //   317: fdiv
    //   318: fstore #9
    //   320: aload #7
    //   322: getfield top : I
    //   325: aload #7
    //   327: getfield bottom : I
    //   330: iadd
    //   331: i2f
    //   332: fconst_2
    //   333: fdiv
    //   334: fstore #10
    //   336: aload_1
    //   337: ldc_w -1.0
    //   340: fconst_1
    //   341: fload #9
    //   343: fload #10
    //   345: invokevirtual scale : (FFFF)V
    //   348: iload #4
    //   350: istore #8
    //   352: aload_2
    //   353: getfield mNinePatch : Landroid/graphics/NinePatch;
    //   356: aload_1
    //   357: aload #7
    //   359: aload_0
    //   360: getfield mPaint : Landroid/graphics/Paint;
    //   363: invokevirtual draw : (Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Paint;)V
    //   366: iload #8
    //   368: iflt -> 377
    //   371: aload_1
    //   372: iload #8
    //   374: invokevirtual restoreToCount : (I)V
    //   377: iload #5
    //   379: ifeq -> 391
    //   382: aload_0
    //   383: getfield mPaint : Landroid/graphics/Paint;
    //   386: aconst_null
    //   387: invokevirtual setColorFilter : (Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
    //   390: pop
    //   391: iload #6
    //   393: iflt -> 405
    //   396: aload_0
    //   397: getfield mPaint : Landroid/graphics/Paint;
    //   400: iload #6
    //   402: invokevirtual setAlpha : (I)V
    //   405: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #194	-> 0
    //   #196	-> 5
    //   #197	-> 10
    //   #200	-> 13
    //   #201	-> 30
    //   #202	-> 42
    //   #204	-> 48
    //   #208	-> 51
    //   #209	-> 60
    //   #210	-> 69
    //   #212	-> 92
    //   #215	-> 95
    //   #216	-> 108
    //   #217	-> 125
    //   #218	-> 133
    //   #221	-> 149
    //   #222	-> 165
    //   #223	-> 172
    //   #224	-> 179
    //   #226	-> 191
    //   #227	-> 198
    //   #231	-> 209
    //   #232	-> 215
    //   #233	-> 224
    //   #234	-> 233
    //   #235	-> 254
    //   #236	-> 275
    //   #239	-> 275
    //   #240	-> 281
    //   #241	-> 290
    //   #244	-> 304
    //   #245	-> 320
    //   #246	-> 336
    //   #249	-> 352
    //   #251	-> 366
    //   #252	-> 371
    //   #255	-> 377
    //   #256	-> 382
    //   #259	-> 391
    //   #260	-> 396
    //   #262	-> 405
  }
  
  public int getChangingConfigurations() {
    return super.getChangingConfigurations() | this.mNinePatchState.getChangingConfigurations();
  }
  
  public boolean getPadding(Rect paramRect) {
    Rect rect = this.mPadding;
    if (rect != null) {
      boolean bool;
      paramRect.set(rect);
      if ((paramRect.left | paramRect.top | paramRect.right | paramRect.bottom) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
    return super.getPadding(paramRect);
  }
  
  public void getOutline(Outline paramOutline) {
    Rect rect = getBounds();
    if (rect.isEmpty())
      return; 
    NinePatchState ninePatchState = this.mNinePatchState;
    if (ninePatchState != null && this.mOutlineInsets != null) {
      NinePatch ninePatch = ninePatchState.mNinePatch;
      NinePatch.InsetStruct insetStruct = ninePatch.getBitmap().getNinePatchInsets();
      if (insetStruct != null) {
        paramOutline.setRoundRect(rect.left + this.mOutlineInsets.left, rect.top + this.mOutlineInsets.top, rect.right - this.mOutlineInsets.right, rect.bottom - this.mOutlineInsets.bottom, this.mOutlineRadius);
        paramOutline.setAlpha(insetStruct.outlineAlpha * getAlpha() / 255.0F);
        return;
      } 
    } 
    super.getOutline(paramOutline);
  }
  
  public Insets getOpticalInsets() {
    Insets insets = this.mOpticalInsets;
    if (needsMirroring())
      return Insets.of(insets.right, insets.top, insets.left, insets.bottom); 
    return insets;
  }
  
  public void setAlpha(int paramInt) {
    if (this.mPaint == null && paramInt == 255)
      return; 
    getPaint().setAlpha(paramInt);
    invalidateSelf();
  }
  
  public int getAlpha() {
    if (this.mPaint == null)
      return 255; 
    return getPaint().getAlpha();
  }
  
  public void setColorFilter(ColorFilter paramColorFilter) {
    if (this.mPaint == null && paramColorFilter == null)
      return; 
    getPaint().setColorFilter(paramColorFilter);
    invalidateSelf();
  }
  
  public void setTintList(ColorStateList paramColorStateList) {
    this.mNinePatchState.mTint = paramColorStateList;
    this.mBlendModeFilter = updateBlendModeFilter(this.mBlendModeFilter, paramColorStateList, this.mNinePatchState.mBlendMode);
    invalidateSelf();
  }
  
  public void setTintBlendMode(BlendMode paramBlendMode) {
    this.mNinePatchState.mBlendMode = paramBlendMode;
    this.mBlendModeFilter = updateBlendModeFilter(this.mBlendModeFilter, this.mNinePatchState.mTint, paramBlendMode);
    invalidateSelf();
  }
  
  public void setDither(boolean paramBoolean) {
    if (this.mPaint == null && !paramBoolean)
      return; 
    getPaint().setDither(paramBoolean);
    invalidateSelf();
  }
  
  public void setAutoMirrored(boolean paramBoolean) {
    this.mNinePatchState.mAutoMirrored = paramBoolean;
  }
  
  private boolean needsMirroring() {
    boolean bool = isAutoMirrored();
    boolean bool1 = true;
    if (!bool || getLayoutDirection() != 1)
      bool1 = false; 
    return bool1;
  }
  
  public boolean isAutoMirrored() {
    return this.mNinePatchState.mAutoMirrored;
  }
  
  public void setFilterBitmap(boolean paramBoolean) {
    getPaint().setFilterBitmap(paramBoolean);
    invalidateSelf();
  }
  
  public boolean isFilterBitmap() {
    boolean bool;
    if (this.mPaint != null && getPaint().isFilterBitmap()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme) throws XmlPullParserException, IOException {
    super.inflate(paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
    TypedArray typedArray = obtainAttributes(paramResources, paramTheme, paramAttributeSet, R.styleable.NinePatchDrawable);
    updateStateFromTypedArray(typedArray);
    typedArray.recycle();
    updateLocalState(paramResources);
  }
  
  private void updateStateFromTypedArray(TypedArray paramTypedArray) throws XmlPullParserException {
    Resources resources = paramTypedArray.getResources();
    NinePatchState ninePatchState = this.mNinePatchState;
    ninePatchState.mChangingConfigurations |= paramTypedArray.getChangingConfigurations();
    ninePatchState.mThemeAttrs = paramTypedArray.extractThemeAttrs();
    ninePatchState.mDither = paramTypedArray.getBoolean(1, ninePatchState.mDither);
    int i = paramTypedArray.getResourceId(0, 0);
    if (i != 0) {
      Rect rect1 = new Rect();
      Rect rect2 = new Rect();
      Bitmap bitmap1 = null;
      Bitmap bitmap2 = bitmap1;
      try {
        TypedValue typedValue = new TypedValue();
        bitmap2 = bitmap1;
        this();
        bitmap2 = bitmap1;
        InputStream inputStream = resources.openRawResource(i, typedValue);
        i = 0;
        bitmap2 = bitmap1;
        if (typedValue.density == 0) {
          i = 160;
        } else {
          bitmap2 = bitmap1;
          if (typedValue.density != 65535) {
            bitmap2 = bitmap1;
            i = typedValue.density;
          } 
        } 
        bitmap2 = bitmap1;
        ImageDecoder.Source source = ImageDecoder.createSource(resources, inputStream, i);
        bitmap2 = bitmap1;
        _$$Lambda$NinePatchDrawable$yQvfm7FAkslD5wdGFysjgwt8cLE _$$Lambda$NinePatchDrawable$yQvfm7FAkslD5wdGFysjgwt8cLE = new _$$Lambda$NinePatchDrawable$yQvfm7FAkslD5wdGFysjgwt8cLE();
        bitmap2 = bitmap1;
        this(rect1);
        bitmap2 = bitmap1;
        bitmap1 = ImageDecoder.decodeBitmap(source, _$$Lambda$NinePatchDrawable$yQvfm7FAkslD5wdGFysjgwt8cLE);
        bitmap2 = bitmap1;
        inputStream.close();
        bitmap2 = bitmap1;
      } catch (IOException iOException) {}
      if (bitmap2 != null) {
        if (bitmap2.getNinePatchChunk() != null) {
          bitmap2.getOpticalInsets(rect2);
          ninePatchState.mNinePatch = new NinePatch(bitmap2, bitmap2.getNinePatchChunk());
          ninePatchState.mPadding = rect1;
          ninePatchState.mOpticalInsets = Insets.of(rect2);
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(paramTypedArray.getPositionDescription());
          stringBuilder.append(": <nine-patch> requires a valid 9-patch source image");
          throw new XmlPullParserException(stringBuilder.toString());
        } 
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramTypedArray.getPositionDescription());
        stringBuilder.append(": <nine-patch> requires a valid src attribute");
        throw new XmlPullParserException(stringBuilder.toString());
      } 
    } 
    ninePatchState.mAutoMirrored = paramTypedArray.getBoolean(4, ninePatchState.mAutoMirrored);
    ninePatchState.mBaseAlpha = paramTypedArray.getFloat(3, ninePatchState.mBaseAlpha);
    i = paramTypedArray.getInt(5, -1);
    if (i != -1)
      ninePatchState.mBlendMode = Drawable.parseBlendMode(i, BlendMode.SRC_IN); 
    ColorStateList colorStateList = paramTypedArray.getColorStateList(2);
    if (colorStateList != null)
      ninePatchState.mTint = colorStateList; 
  }
  
  public void applyTheme(Resources.Theme paramTheme) {
    super.applyTheme(paramTheme);
    NinePatchState ninePatchState = this.mNinePatchState;
    if (ninePatchState == null)
      return; 
    if (ninePatchState.mThemeAttrs != null) {
      TypedArray typedArray = paramTheme.resolveAttributes(ninePatchState.mThemeAttrs, R.styleable.NinePatchDrawable);
      try {
        updateStateFromTypedArray(typedArray);
        typedArray.recycle();
      } catch (XmlPullParserException xmlPullParserException) {
        rethrowAsRuntimeException((Exception)xmlPullParserException);
        typedArray.recycle();
      } finally {}
    } 
    if (ninePatchState.mTint != null && ninePatchState.mTint.canApplyTheme())
      ninePatchState.mTint = ninePatchState.mTint.obtainForTheme(paramTheme); 
    updateLocalState(paramTheme.getResources());
  }
  
  public boolean canApplyTheme() {
    boolean bool;
    NinePatchState ninePatchState = this.mNinePatchState;
    if (ninePatchState != null && ninePatchState.canApplyTheme()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public Paint getPaint() {
    if (this.mPaint == null) {
      Paint paint = new Paint();
      paint.setDither(false);
    } 
    return this.mPaint;
  }
  
  public int getIntrinsicWidth() {
    return this.mBitmapWidth;
  }
  
  public int getIntrinsicHeight() {
    return this.mBitmapHeight;
  }
  
  public int getOpacity() {
    NinePatch ninePatch = this.mNinePatchState.mNinePatch;
    if (!ninePatch.hasAlpha()) {
      Paint paint = this.mPaint;
      return (paint != null && paint.getAlpha() < 255) ? 
        -3 : -1;
    } 
    return -3;
  }
  
  public Region getTransparentRegion() {
    return this.mNinePatchState.mNinePatch.getTransparentRegion(getBounds());
  }
  
  public Drawable.ConstantState getConstantState() {
    this.mNinePatchState.mChangingConfigurations = getChangingConfigurations();
    return this.mNinePatchState;
  }
  
  public Drawable mutate() {
    if (!this.mMutated && super.mutate() == this) {
      this.mNinePatchState = new NinePatchState();
      this.mMutated = true;
    } 
    return this;
  }
  
  public void clearMutated() {
    super.clearMutated();
    this.mMutated = false;
  }
  
  protected boolean onStateChange(int[] paramArrayOfint) {
    NinePatchState ninePatchState = this.mNinePatchState;
    if (ninePatchState.mTint != null && ninePatchState.mBlendMode != null) {
      this.mBlendModeFilter = updateBlendModeFilter(this.mBlendModeFilter, ninePatchState.mTint, ninePatchState.mBlendMode);
      return true;
    } 
    return false;
  }
  
  public boolean isStateful() {
    NinePatchState ninePatchState = this.mNinePatchState;
    return (super.isStateful() || (ninePatchState.mTint != null && ninePatchState.mTint.isStateful()));
  }
  
  public boolean hasFocusStateSpecified() {
    boolean bool;
    if (this.mNinePatchState.mTint != null && this.mNinePatchState.mTint.hasFocusStateSpecified()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  class NinePatchState extends Drawable.ConstantState {
    NinePatch mNinePatch = null;
    
    ColorStateList mTint = null;
    
    BlendMode mBlendMode = Drawable.DEFAULT_BLEND_MODE;
    
    Rect mPadding = null;
    
    Insets mOpticalInsets = Insets.NONE;
    
    float mBaseAlpha = 1.0F;
    
    boolean mDither = false;
    
    boolean mAutoMirrored = false;
    
    int mChangingConfigurations;
    
    int[] mThemeAttrs;
    
    NinePatchState(Rect param1Rect) {
      this((NinePatch)this$0, param1Rect, null, false, false);
    }
    
    NinePatchState(Rect param1Rect1, Rect param1Rect2) {
      this((NinePatch)this$0, param1Rect1, param1Rect2, false, false);
    }
    
    NinePatchState(Rect param1Rect1, Rect param1Rect2, boolean param1Boolean1, boolean param1Boolean2) {
      this.mNinePatch = (NinePatch)this$0;
      this.mPadding = param1Rect1;
      this.mOpticalInsets = Insets.of(param1Rect2);
      this.mDither = param1Boolean1;
      this.mAutoMirrored = param1Boolean2;
    }
    
    NinePatchState() {
      this.mChangingConfigurations = ((NinePatchState)this$0).mChangingConfigurations;
      this.mNinePatch = ((NinePatchState)this$0).mNinePatch;
      this.mTint = ((NinePatchState)this$0).mTint;
      this.mBlendMode = ((NinePatchState)this$0).mBlendMode;
      this.mPadding = ((NinePatchState)this$0).mPadding;
      this.mOpticalInsets = ((NinePatchState)this$0).mOpticalInsets;
      this.mBaseAlpha = ((NinePatchState)this$0).mBaseAlpha;
      this.mDither = ((NinePatchState)this$0).mDither;
      this.mAutoMirrored = ((NinePatchState)this$0).mAutoMirrored;
      this.mThemeAttrs = ((NinePatchState)this$0).mThemeAttrs;
    }
    
    public boolean canApplyTheme() {
      if (this.mThemeAttrs == null) {
        ColorStateList colorStateList = this.mTint;
        return ((colorStateList != null && 
          colorStateList.canApplyTheme()) || 
          super.canApplyTheme());
      } 
      return true;
    }
    
    public Drawable newDrawable() {
      return new NinePatchDrawable(this, null);
    }
    
    public Drawable newDrawable(Resources param1Resources) {
      return new NinePatchDrawable(this, param1Resources);
    }
    
    public int getChangingConfigurations() {
      byte b;
      int i = this.mChangingConfigurations;
      ColorStateList colorStateList = this.mTint;
      if (colorStateList != null) {
        b = colorStateList.getChangingConfigurations();
      } else {
        b = 0;
      } 
      return i | b;
    }
    
    NinePatchState() {}
  }
  
  private void computeBitmapSize() {
    int j;
    NinePatch ninePatch = this.mNinePatchState.mNinePatch;
    if (ninePatch == null)
      return; 
    int i = this.mTargetDensity;
    if (ninePatch.getDensity() == 0) {
      j = i;
    } else {
      j = ninePatch.getDensity();
    } 
    Insets insets = this.mNinePatchState.mOpticalInsets;
    if (insets != Insets.NONE) {
      int m = Drawable.scaleFromDensity(insets.left, j, i, true);
      int n = Drawable.scaleFromDensity(insets.top, j, i, true);
      int i1 = Drawable.scaleFromDensity(insets.right, j, i, true);
      int i2 = Drawable.scaleFromDensity(insets.bottom, j, i, true);
      this.mOpticalInsets = Insets.of(m, n, i1, i2);
    } else {
      this.mOpticalInsets = Insets.NONE;
    } 
    Rect rect = this.mNinePatchState.mPadding;
    if (rect != null) {
      if (this.mPadding == null)
        this.mPadding = new Rect(); 
      this.mPadding.left = Drawable.scaleFromDensity(rect.left, j, i, true);
      this.mPadding.top = Drawable.scaleFromDensity(rect.top, j, i, true);
      this.mPadding.right = Drawable.scaleFromDensity(rect.right, j, i, true);
      this.mPadding.bottom = Drawable.scaleFromDensity(rect.bottom, j, i, true);
    } else {
      this.mPadding = null;
    } 
    int k = ninePatch.getHeight();
    this.mBitmapHeight = Drawable.scaleFromDensity(k, j, i, true);
    k = ninePatch.getWidth();
    this.mBitmapWidth = Drawable.scaleFromDensity(k, j, i, true);
    NinePatch.InsetStruct insetStruct = ninePatch.getBitmap().getNinePatchInsets();
    if (insetStruct != null) {
      rect = insetStruct.outlineRect;
      this.mOutlineInsets = NinePatch.InsetStruct.scaleInsets(rect.left, rect.top, rect.right, rect.bottom, i / j);
      this.mOutlineRadius = Drawable.scaleFromDensity(insetStruct.outlineRadius, j, i);
    } else {
      this.mOutlineInsets = null;
    } 
  }
  
  private NinePatchDrawable(NinePatchState paramNinePatchState, Resources paramResources) {
    this.mNinePatchState = paramNinePatchState;
    updateLocalState(paramResources);
  }
  
  private void updateLocalState(Resources paramResources) {
    NinePatchState ninePatchState = this.mNinePatchState;
    if (ninePatchState.mDither)
      setDither(ninePatchState.mDither); 
    if (paramResources == null && ninePatchState.mNinePatch != null) {
      this.mTargetDensity = ninePatchState.mNinePatch.getDensity();
    } else {
      this.mTargetDensity = Drawable.resolveDensity(paramResources, this.mTargetDensity);
    } 
    this.mBlendModeFilter = updateBlendModeFilter(this.mBlendModeFilter, ninePatchState.mTint, ninePatchState.mBlendMode);
    computeBitmapSize();
  }
}
