package android.graphics.drawable;

import android.app.uxicons.CustomAdaptiveIconConfig;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Shader;

public class OplusAdaptiveIconDrawable extends AdaptiveIconDrawable {
  private Matrix mCustomMatrix = new Matrix();
  
  private Rect mBackgroundSizeBounds = new Rect();
  
  private Rect mBackgroundPositionBounds = new Rect();
  
  private Rect mForegroundSizeBounds = new Rect();
  
  private Rect mForegroundPositionBounds = new Rect();
  
  private Paint mOplusPaint = new Paint(7);
  
  private static final float DEFAULT_VIEW_PORT_SCALE = 0.6666667F;
  
  private static final float EXTRA_INSET_PERCENTAGE = 0.25F;
  
  private CustomAdaptiveIconConfig mConfig;
  
  private Canvas mOplusCanvas;
  
  private OplusLayerState mOplusLayerState;
  
  private Bitmap mOplusLayersBitmap;
  
  private Path mOplusMask;
  
  private Path mOplusMaskScaleOnly;
  
  OplusAdaptiveIconDrawable(AdaptiveIconDrawable.LayerState paramLayerState, Resources paramResources, CustomAdaptiveIconConfig paramCustomAdaptiveIconConfig) {
    super(paramLayerState, paramResources);
    this.mConfig = paramCustomAdaptiveIconConfig;
    init();
  }
  
  public OplusAdaptiveIconDrawable(Drawable paramDrawable1, Drawable paramDrawable2, CustomAdaptiveIconConfig paramCustomAdaptiveIconConfig) {
    super(paramDrawable1, paramDrawable2);
    this.mConfig = paramCustomAdaptiveIconConfig;
    init();
  }
  
  public float getForegroundScalePercent() {
    float f1 = this.mConfig.getForegroundScalePercent() * this.mConfig.getScalePercent() * 1.0F;
    float f2 = f1;
    if (!this.mConfig.getIsPlatformDrawable()) {
      f2 = f1;
      if (this.mConfig.getIsAdaptiveIconDrawable())
        f2 = 1.0F * f1 / 0.6666667F; 
    } 
    return f2;
  }
  
  private void init() {
    CustomAdaptiveIconConfig customAdaptiveIconConfig = this.mConfig;
    if (customAdaptiveIconConfig != null && 
      customAdaptiveIconConfig.getCustomMask() != null) {
      this.mOplusCanvas = new Canvas();
      this.mOplusMask = new Path(this.mConfig.getCustomMask());
      this.mOplusMaskScaleOnly = new Path(this.mOplusMask);
      this.mOplusLayerState = new OplusLayerState(this.mLayerState, this.mConfig);
    } 
  }
  
  private boolean drawIcon(Canvas paramCanvas) {
    if (this.mOplusLayersBitmap == null || this.mOplusCanvas == null)
      return false; 
    if (this.mConfig.getCustomMask() != null) {
      paramCanvas.save();
      Bitmap bitmap = this.mOplusLayersBitmap;
      this.mOplusCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
      this.mOplusCanvas.setBitmap(bitmap);
      if (getBackground() != null) {
        this.mOplusCanvas.save();
        this.mOplusCanvas.translate(this.mBackgroundPositionBounds.left, this.mBackgroundPositionBounds.top);
        getBackground().draw(this.mOplusCanvas);
        this.mOplusCanvas.translate(-this.mBackgroundPositionBounds.left, -this.mBackgroundPositionBounds.top);
        this.mOplusCanvas.restore();
      } 
      if (getForeground() != null) {
        this.mOplusCanvas.save();
        this.mOplusCanvas.translate(this.mForegroundPositionBounds.left, this.mForegroundPositionBounds.top);
        getForeground().draw(this.mOplusCanvas);
        this.mOplusCanvas.translate(-this.mForegroundPositionBounds.left, -this.mForegroundPositionBounds.top);
        this.mOplusCanvas.restore();
      } 
      this.mOplusPaint.setShader(new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
      if (this.mOplusMaskScaleOnly != null) {
        Rect rect = getBounds();
        paramCanvas.translate(rect.left, rect.top);
        paramCanvas.drawPath(this.mOplusMaskScaleOnly, this.mOplusPaint);
        paramCanvas.translate(-rect.left, -rect.top);
      } 
      paramCanvas.restore();
      return true;
    } 
    return false;
  }
  
  protected boolean onDrawableBoundsChange(Rect paramRect) {
    if (this.mConfig.getCustomMask() == null)
      return false; 
    try {
      int i = updateBounds(paramRect);
      updateDrawableBounds();
      updateMaskBounds(paramRect, i);
      updateParams(paramRect);
      return true;
    } catch (Exception exception) {
      return false;
    } 
  }
  
  private int updateBounds(Rect paramRect) {
    int i = (int)Math.ceil((paramRect.width() * this.mConfig.getScalePercent()));
    int j = paramRect.width();
    j = (int)((j - i) / 2.0F);
    this.mBackgroundPositionBounds.set(j, j, i + j, i + j);
    this.mBackgroundSizeBounds.set(0, 0, i, i);
    i = (int)Math.ceil((paramRect.width() * this.mConfig.getScalePercent() * this.mConfig.getForegroundScalePercent()));
    int k = paramRect.width();
    k = (int)((k - i) / 2.0F);
    this.mForegroundSizeBounds.set(0, 0, i, i);
    this.mForegroundPositionBounds.set(k, k, i + k, i + k);
    if (!this.mConfig.getIsPlatformDrawable() && this.mConfig.getIsAdaptiveIconDrawable()) {
      updateThirdPartAdaptiveIconDrawableBound(this.mBackgroundSizeBounds);
      updateThirdPartAdaptiveIconDrawableBound(this.mForegroundSizeBounds);
    } 
    return j;
  }
  
  private void updateThirdPartAdaptiveIconDrawableBound(Rect paramRect) {
    int i = paramRect.width() / 2;
    int j = paramRect.height() / 2;
    int k = (int)(paramRect.width() / 1.3333334F);
    int m = (int)(paramRect.height() / 1.3333334F);
    paramRect.set(i - k, j - m, i + k, j + m);
  }
  
  private void updateDrawableBounds() {
    AdaptiveIconDrawable.ChildDrawable childDrawable = this.mLayerState.mChildren[0];
    if (childDrawable != null && childDrawable.mDrawable != null)
      childDrawable.mDrawable.setBounds(this.mBackgroundSizeBounds); 
    childDrawable = this.mLayerState.mChildren[1];
    if (childDrawable != null && childDrawable.mDrawable != null)
      childDrawable.mDrawable.setBounds(this.mForegroundSizeBounds); 
  }
  
  private void updateMaskBounds(Rect paramRect, int paramInt) {
    this.mCustomMatrix.reset();
    Matrix matrix = this.mCustomMatrix;
    float f1 = paramRect.width() * this.mConfig.getScalePercent() * 1.0F / 150.0F;
    float f2 = paramRect.height() * this.mConfig.getScalePercent() * 1.0F / 150.0F;
    matrix.setScale(f1, f2);
    this.mOplusMask.transform(this.mCustomMatrix, this.mOplusMaskScaleOnly);
    this.mOplusMaskScaleOnly.offset(paramInt, paramInt);
  }
  
  private void updateParams(Rect paramRect) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mOplusLayersBitmap : Landroid/graphics/Bitmap;
    //   4: astore_2
    //   5: aload_2
    //   6: ifnull -> 39
    //   9: aload_2
    //   10: invokevirtual getWidth : ()I
    //   13: aload_1
    //   14: invokevirtual width : ()I
    //   17: if_icmpne -> 39
    //   20: aload_0
    //   21: getfield mOplusLayersBitmap : Landroid/graphics/Bitmap;
    //   24: astore_2
    //   25: aload_2
    //   26: invokevirtual getHeight : ()I
    //   29: aload_0
    //   30: getfield mOplusLayersBitmap : Landroid/graphics/Bitmap;
    //   33: invokevirtual getHeight : ()I
    //   36: if_icmpeq -> 65
    //   39: aload_1
    //   40: invokevirtual width : ()I
    //   43: istore_3
    //   44: aload_1
    //   45: invokevirtual height : ()I
    //   48: istore #4
    //   50: getstatic android/graphics/Bitmap$Config.ARGB_8888 : Landroid/graphics/Bitmap$Config;
    //   53: astore_1
    //   54: aload_0
    //   55: iload_3
    //   56: iload #4
    //   58: aload_1
    //   59: invokestatic createBitmap : (IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    //   62: putfield mOplusLayersBitmap : Landroid/graphics/Bitmap;
    //   65: aload_0
    //   66: getfield mOplusPaint : Landroid/graphics/Paint;
    //   69: iconst_1
    //   70: invokevirtual setAntiAlias : (Z)V
    //   73: aload_0
    //   74: getfield mOplusPaint : Landroid/graphics/Paint;
    //   77: aconst_null
    //   78: invokevirtual setShader : (Landroid/graphics/Shader;)Landroid/graphics/Shader;
    //   81: pop
    //   82: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #207	-> 0
    //   #208	-> 25
    //   #209	-> 39
    //   #210	-> 44
    //   #209	-> 54
    //   #212	-> 65
    //   #213	-> 73
    //   #214	-> 82
  }
  
  class OplusLayerState extends Drawable.ConstantState {
    private int mChangingConfigurations;
    
    private CustomAdaptiveIconConfig mConfig;
    
    private AdaptiveIconDrawable.LayerState mParentLayerState;
    
    OplusLayerState(OplusAdaptiveIconDrawable this$0, CustomAdaptiveIconConfig param1CustomAdaptiveIconConfig) {
      this.mParentLayerState = (AdaptiveIconDrawable.LayerState)this$0;
      this.mConfig = param1CustomAdaptiveIconConfig;
      this.mChangingConfigurations = this$0.getChangingConfigurations();
    }
    
    public Drawable newDrawable() {
      return new OplusAdaptiveIconDrawable(this.mParentLayerState, null, this.mConfig);
    }
    
    public Drawable newDrawable(Resources param1Resources) {
      return new OplusAdaptiveIconDrawable(this.mParentLayerState, param1Resources, this.mConfig);
    }
    
    public int getChangingConfigurations() {
      return this.mChangingConfigurations;
    }
  }
  
  public Drawable.ConstantState getConstantState() {
    CustomAdaptiveIconConfig customAdaptiveIconConfig = this.mConfig;
    if (customAdaptiveIconConfig != null && customAdaptiveIconConfig.getCustomMask() != null)
      return this.mOplusLayerState; 
    return super.getConstantState();
  }
  
  protected boolean hookDraw(Canvas paramCanvas) {
    return drawIcon(paramCanvas);
  }
  
  protected boolean hookOnBoundsChange(Rect paramRect) {
    return onDrawableBoundsChange(paramRect);
  }
  
  protected Path hookGetIconMask() {
    return this.mOplusMask;
  }
  
  protected boolean hookGetIntrinsicHeight() {
    boolean bool;
    if (this.mConfig != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected boolean hookGetIntrinsicWidth() {
    boolean bool;
    if (this.mConfig != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public float getForegroundScalePercent(Drawable paramDrawable) {
    if (paramDrawable instanceof OplusAdaptiveIconDrawable)
      return ((OplusAdaptiveIconDrawable)paramDrawable).getForegroundScalePercent(); 
    return 0.0F;
  }
}
