package android.graphics.drawable.shapes;

import android.graphics.Canvas;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.RectF;

public class OvalShape extends RectShape {
  public void draw(Canvas paramCanvas, Paint paramPaint) {
    paramCanvas.drawOval(rect(), paramPaint);
  }
  
  public void getOutline(Outline paramOutline) {
    RectF rectF = rect();
    int i = (int)Math.ceil(rectF.left), j = (int)Math.ceil(rectF.top);
    double d = rectF.right;
    int k = (int)Math.floor(d), m = (int)Math.floor(rectF.bottom);
    paramOutline.setOval(i, j, k, m);
  }
  
  public OvalShape clone() throws CloneNotSupportedException {
    return (OvalShape)super.clone();
  }
}
