package android.graphics.drawable.shapes;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import java.util.Objects;

public class PathShape extends Shape {
  private Path mPath;
  
  private float mScaleX;
  
  private float mScaleY;
  
  private final float mStdHeight;
  
  private final float mStdWidth;
  
  public PathShape(Path paramPath, float paramFloat1, float paramFloat2) {
    this.mPath = paramPath;
    this.mStdWidth = paramFloat1;
    this.mStdHeight = paramFloat2;
  }
  
  public void draw(Canvas paramCanvas, Paint paramPaint) {
    paramCanvas.save();
    paramCanvas.scale(this.mScaleX, this.mScaleY);
    paramCanvas.drawPath(this.mPath, paramPaint);
    paramCanvas.restore();
  }
  
  protected void onResize(float paramFloat1, float paramFloat2) {
    this.mScaleX = paramFloat1 / this.mStdWidth;
    this.mScaleY = paramFloat2 / this.mStdHeight;
  }
  
  public PathShape clone() throws CloneNotSupportedException {
    PathShape pathShape = (PathShape)super.clone();
    pathShape.mPath = new Path(this.mPath);
    return pathShape;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    if (!super.equals(paramObject))
      return false; 
    PathShape pathShape = (PathShape)paramObject;
    if (Float.compare(pathShape.mStdWidth, this.mStdWidth) == 0) {
      float f1 = pathShape.mStdHeight, f2 = this.mStdHeight;
      if (Float.compare(f1, f2) == 0) {
        f1 = pathShape.mScaleX;
        f2 = this.mScaleX;
        if (Float.compare(f1, f2) == 0) {
          f2 = pathShape.mScaleY;
          f1 = this.mScaleY;
          if (Float.compare(f2, f1) == 0) {
            paramObject = this.mPath;
            Path path = pathShape.mPath;
            if (Objects.equals(paramObject, path))
              return null; 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(super.hashCode()), Float.valueOf(this.mStdWidth), Float.valueOf(this.mStdHeight), this.mPath, Float.valueOf(this.mScaleX), Float.valueOf(this.mScaleY) });
  }
}
