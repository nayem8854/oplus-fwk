package android.graphics.drawable.shapes;

import android.graphics.Canvas;
import android.graphics.Outline;
import android.graphics.Paint;
import java.util.Objects;

public class ArcShape extends RectShape {
  private final float mStartAngle;
  
  private final float mSweepAngle;
  
  public ArcShape(float paramFloat1, float paramFloat2) {
    this.mStartAngle = paramFloat1;
    this.mSweepAngle = paramFloat2;
  }
  
  public final float getStartAngle() {
    return this.mStartAngle;
  }
  
  public final float getSweepAngle() {
    return this.mSweepAngle;
  }
  
  public void draw(Canvas paramCanvas, Paint paramPaint) {
    paramCanvas.drawArc(rect(), this.mStartAngle, this.mSweepAngle, true, paramPaint);
  }
  
  public void getOutline(Outline paramOutline) {}
  
  public ArcShape clone() throws CloneNotSupportedException {
    return (ArcShape)super.clone();
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    if (!super.equals(paramObject))
      return false; 
    paramObject = paramObject;
    if (Float.compare(((ArcShape)paramObject).mStartAngle, this.mStartAngle) == 0) {
      float f1 = ((ArcShape)paramObject).mSweepAngle, f2 = this.mSweepAngle;
      if (Float.compare(f1, f2) == 0)
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(super.hashCode()), Float.valueOf(this.mStartAngle), Float.valueOf(this.mSweepAngle) });
  }
}
