package android.graphics.drawable.shapes;

import android.graphics.Canvas;
import android.graphics.Outline;
import android.graphics.Paint;
import java.util.Objects;

public abstract class Shape implements Cloneable {
  private float mHeight;
  
  private float mWidth;
  
  public final float getWidth() {
    return this.mWidth;
  }
  
  public final float getHeight() {
    return this.mHeight;
  }
  
  public final void resize(float paramFloat1, float paramFloat2) {
    float f = paramFloat1;
    if (paramFloat1 < 0.0F)
      f = 0.0F; 
    paramFloat1 = paramFloat2;
    if (paramFloat2 < 0.0F)
      paramFloat1 = 0.0F; 
    if (this.mWidth != f || this.mHeight != paramFloat1) {
      this.mWidth = f;
      this.mHeight = paramFloat1;
      onResize(f, paramFloat1);
    } 
  }
  
  public boolean hasAlpha() {
    return true;
  }
  
  protected void onResize(float paramFloat1, float paramFloat2) {}
  
  public void getOutline(Outline paramOutline) {}
  
  public Shape clone() throws CloneNotSupportedException {
    return (Shape)super.clone();
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (Float.compare(((Shape)paramObject).mWidth, this.mWidth) == 0) {
      float f1 = ((Shape)paramObject).mHeight, f2 = this.mHeight;
      if (Float.compare(f1, f2) == 0)
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Float.valueOf(this.mWidth), Float.valueOf(this.mHeight) });
  }
  
  public abstract void draw(Canvas paramCanvas, Paint paramPaint);
}
