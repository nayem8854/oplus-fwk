package android.graphics.drawable;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.util.AttributeSet;
import com.android.internal.R;
import java.io.IOException;
import java.util.Arrays;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class RippleDrawable extends LayerDrawable {
  private final Rect mTempRect = new Rect();
  
  private final Rect mHotspotBounds = new Rect();
  
  private final Rect mDrawingBounds = new Rect();
  
  private final Rect mDirtyBounds = new Rect();
  
  private int mExitingRipplesCount = 0;
  
  private static final int MASK_CONTENT = 1;
  
  private static final int MASK_EXPLICIT = 2;
  
  private static final int MASK_NONE = 0;
  
  private static final int MASK_UNKNOWN = -1;
  
  private static final int MAX_RIPPLES = 10;
  
  public static final int RADIUS_AUTO = -1;
  
  private RippleBackground mBackground;
  
  private int mDensity;
  
  private RippleForeground[] mExitingRipples;
  
  private boolean mForceSoftware;
  
  private boolean mHasPending;
  
  private boolean mHasValidMask;
  
  private Drawable mMask;
  
  private Bitmap mMaskBuffer;
  
  private Canvas mMaskCanvas;
  
  private PorterDuffColorFilter mMaskColorFilter;
  
  private Matrix mMaskMatrix;
  
  private BitmapShader mMaskShader;
  
  private boolean mOverrideBounds;
  
  private float mPendingX;
  
  private float mPendingY;
  
  private RippleForeground mRipple;
  
  private boolean mRippleActive;
  
  private Paint mRipplePaint;
  
  private RippleState mState;
  
  RippleDrawable() {
    this(new RippleState(null, null, null), (Resources)null);
  }
  
  public RippleDrawable(ColorStateList paramColorStateList, Drawable paramDrawable1, Drawable paramDrawable2) {
    this(new RippleState(null, null, null), (Resources)null);
    if (paramColorStateList != null) {
      if (paramDrawable1 != null)
        addLayer(paramDrawable1, (int[])null, 0, 0, 0, 0, 0); 
      if (paramDrawable2 != null)
        addLayer(paramDrawable2, (int[])null, 16908334, 0, 0, 0, 0); 
      setColor(paramColorStateList);
      ensurePadding();
      refreshPadding();
      updateLocalState();
      return;
    } 
    throw new IllegalArgumentException("RippleDrawable requires a non-null color");
  }
  
  public void jumpToCurrentState() {
    super.jumpToCurrentState();
    RippleForeground rippleForeground = this.mRipple;
    if (rippleForeground != null)
      rippleForeground.end(); 
    RippleBackground rippleBackground = this.mBackground;
    if (rippleBackground != null)
      rippleBackground.jumpToFinal(); 
    cancelExitingRipples();
  }
  
  private void cancelExitingRipples() {
    int i = this.mExitingRipplesCount;
    RippleForeground[] arrayOfRippleForeground = this.mExitingRipples;
    for (byte b = 0; b < i; b++)
      arrayOfRippleForeground[b].end(); 
    if (arrayOfRippleForeground != null)
      Arrays.fill((Object[])arrayOfRippleForeground, 0, i, (Object)null); 
    this.mExitingRipplesCount = 0;
    invalidateSelf(false);
  }
  
  public int getOpacity() {
    return -3;
  }
  
  protected boolean onStateChange(int[] paramArrayOfint) {
    boolean bool = super.onStateChange(paramArrayOfint);
    boolean bool1 = false;
    boolean bool2 = false;
    boolean bool3 = false;
    boolean bool4 = false;
    int i;
    boolean bool5;
    byte b;
    for (i = paramArrayOfint.length, bool5 = false, b = 0; b < i; ) {
      boolean bool7, bool8, bool9;
      int j = paramArrayOfint[b];
      if (j == 16842910) {
        bool7 = true;
        bool8 = bool2;
        bool9 = bool3;
      } else if (j == 16842908) {
        bool9 = true;
        bool7 = bool1;
        bool8 = bool2;
      } else if (j == 16842919) {
        bool8 = true;
        bool7 = bool1;
        bool9 = bool3;
      } else {
        bool7 = bool1;
        bool8 = bool2;
        bool9 = bool3;
        if (j == 16843623) {
          bool4 = true;
          bool9 = bool3;
          bool8 = bool2;
          bool7 = bool1;
        } 
      } 
      b++;
      bool1 = bool7;
      bool2 = bool8;
      bool3 = bool9;
    } 
    boolean bool6 = bool5;
    if (bool1) {
      bool6 = bool5;
      if (bool2)
        bool6 = true; 
    } 
    setRippleActive(bool6);
    setBackgroundActive(bool4, bool3, bool2);
    return bool;
  }
  
  private void setRippleActive(boolean paramBoolean) {
    if (this.mRippleActive != paramBoolean) {
      this.mRippleActive = paramBoolean;
      if (paramBoolean) {
        tryRippleEnter();
      } else {
        tryRippleExit();
      } 
    } 
  }
  
  private void setBackgroundActive(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    if (this.mBackground == null && (paramBoolean1 || paramBoolean2)) {
      RippleBackground rippleBackground1 = new RippleBackground(this, this.mHotspotBounds, isBounded());
      rippleBackground1.setup(this.mState.mMaxRadius, this.mDensity);
    } 
    RippleBackground rippleBackground = this.mBackground;
    if (rippleBackground != null)
      rippleBackground.setState(paramBoolean2, paramBoolean1, paramBoolean3); 
  }
  
  protected void onBoundsChange(Rect paramRect) {
    super.onBoundsChange(paramRect);
    if (!this.mOverrideBounds) {
      this.mHotspotBounds.set(paramRect);
      onHotspotBoundsChanged();
    } 
    int i = this.mExitingRipplesCount;
    RippleForeground[] arrayOfRippleForeground = this.mExitingRipples;
    for (byte b = 0; b < i; b++)
      arrayOfRippleForeground[b].onBoundsChange(); 
    RippleBackground rippleBackground = this.mBackground;
    if (rippleBackground != null)
      rippleBackground.onBoundsChange(); 
    RippleForeground rippleForeground = this.mRipple;
    if (rippleForeground != null)
      rippleForeground.onBoundsChange(); 
    invalidateSelf();
  }
  
  public boolean setVisible(boolean paramBoolean1, boolean paramBoolean2) {
    paramBoolean2 = super.setVisible(paramBoolean1, paramBoolean2);
    if (!paramBoolean1) {
      clearHotspots();
    } else if (paramBoolean2) {
      if (this.mRippleActive)
        tryRippleEnter(); 
      jumpToCurrentState();
    } 
    return paramBoolean2;
  }
  
  public boolean isProjected() {
    if (isBounded())
      return false; 
    int i = this.mState.mMaxRadius;
    Rect rect1 = getBounds();
    Rect rect2 = this.mHotspotBounds;
    if (i != -1 && 
      i <= rect2.width() / 2 && 
      i <= rect2.height() / 2 && (
      rect1.equals(rect2) || 
      rect1.contains(rect2)))
      return false; 
    return true;
  }
  
  private boolean isBounded() {
    boolean bool;
    if (getNumberOfLayers() > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isStateful() {
    return true;
  }
  
  public boolean hasFocusStateSpecified() {
    return true;
  }
  
  public void setColor(ColorStateList paramColorStateList) {
    this.mState.mColor = paramColorStateList;
    invalidateSelf(false);
  }
  
  public void setRadius(int paramInt) {
    this.mState.mMaxRadius = paramInt;
    invalidateSelf(false);
  }
  
  public int getRadius() {
    return this.mState.mMaxRadius;
  }
  
  public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme) throws XmlPullParserException, IOException {
    TypedArray typedArray = obtainAttributes(paramResources, paramTheme, paramAttributeSet, R.styleable.RippleDrawable);
    setPaddingMode(1);
    super.inflate(paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
    updateStateFromTypedArray(typedArray);
    verifyRequiredAttributes(typedArray);
    typedArray.recycle();
    updateLocalState();
  }
  
  public boolean setDrawableByLayerId(int paramInt, Drawable paramDrawable) {
    if (super.setDrawableByLayerId(paramInt, paramDrawable)) {
      if (paramInt == 16908334) {
        this.mMask = paramDrawable;
        this.mHasValidMask = false;
      } 
      return true;
    } 
    return false;
  }
  
  public void setPaddingMode(int paramInt) {
    super.setPaddingMode(paramInt);
  }
  
  private void updateStateFromTypedArray(TypedArray paramTypedArray) throws XmlPullParserException {
    RippleState rippleState2 = this.mState;
    rippleState2.mChangingConfigurations |= paramTypedArray.getChangingConfigurations();
    rippleState2.mTouchThemeAttrs = paramTypedArray.extractThemeAttrs();
    ColorStateList colorStateList = paramTypedArray.getColorStateList(0);
    if (colorStateList != null)
      this.mState.mColor = colorStateList; 
    RippleState rippleState1 = this.mState;
    rippleState1.mMaxRadius = paramTypedArray.getDimensionPixelSize(1, rippleState1.mMaxRadius);
  }
  
  private void verifyRequiredAttributes(TypedArray paramTypedArray) throws XmlPullParserException {
    if (this.mState.mColor != null || (this.mState.mTouchThemeAttrs != null && this.mState.mTouchThemeAttrs[0] != 0))
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramTypedArray.getPositionDescription());
    stringBuilder.append(": <ripple> requires a valid color attribute");
    throw new XmlPullParserException(stringBuilder.toString());
  }
  
  public void applyTheme(Resources.Theme paramTheme) {
    super.applyTheme(paramTheme);
    RippleState rippleState = this.mState;
    if (rippleState == null)
      return; 
    if (rippleState.mTouchThemeAttrs != null) {
      TypedArray typedArray = paramTheme.resolveAttributes(rippleState.mTouchThemeAttrs, R.styleable.RippleDrawable);
      try {
        updateStateFromTypedArray(typedArray);
        verifyRequiredAttributes(typedArray);
        typedArray.recycle();
      } catch (XmlPullParserException xmlPullParserException) {
        rethrowAsRuntimeException((Exception)xmlPullParserException);
        typedArray.recycle();
      } finally {}
    } 
    if (rippleState.mColor != null && rippleState.mColor.canApplyTheme())
      rippleState.mColor = rippleState.mColor.obtainForTheme(paramTheme); 
    updateLocalState();
  }
  
  public boolean canApplyTheme() {
    boolean bool;
    RippleState rippleState = this.mState;
    if ((rippleState != null && rippleState.canApplyTheme()) || super.canApplyTheme()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setHotspot(float paramFloat1, float paramFloat2) {
    if (this.mRipple == null || this.mBackground == null) {
      this.mPendingX = paramFloat1;
      this.mPendingY = paramFloat2;
      this.mHasPending = true;
    } 
    RippleForeground rippleForeground = this.mRipple;
    if (rippleForeground != null)
      rippleForeground.move(paramFloat1, paramFloat2); 
  }
  
  private void tryRippleEnter() {
    if (this.mExitingRipplesCount >= 10)
      return; 
    if (this.mRipple == null) {
      float f1, f2;
      if (this.mHasPending) {
        this.mHasPending = false;
        f1 = this.mPendingX;
        f2 = this.mPendingY;
      } else {
        f1 = this.mHotspotBounds.exactCenterX();
        f2 = this.mHotspotBounds.exactCenterY();
      } 
      this.mRipple = new RippleForeground(this, this.mHotspotBounds, f1, f2, this.mForceSoftware);
    } 
    this.mRipple.setup(this.mState.mMaxRadius, this.mDensity);
    this.mRipple.enter();
  }
  
  private void tryRippleExit() {
    if (this.mRipple != null) {
      if (this.mExitingRipples == null)
        this.mExitingRipples = new RippleForeground[10]; 
      RippleForeground[] arrayOfRippleForeground = this.mExitingRipples;
      int i = this.mExitingRipplesCount;
      this.mExitingRipplesCount = i + 1;
      RippleForeground rippleForeground = this.mRipple;
      arrayOfRippleForeground[i] = rippleForeground;
      rippleForeground.exit();
      this.mRipple = null;
    } 
  }
  
  private void clearHotspots() {
    RippleForeground rippleForeground = this.mRipple;
    if (rippleForeground != null) {
      rippleForeground.end();
      this.mRipple = null;
      this.mRippleActive = false;
    } 
    RippleBackground rippleBackground = this.mBackground;
    if (rippleBackground != null)
      rippleBackground.setState(false, false, false); 
    cancelExitingRipples();
  }
  
  public void setHotspotBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mOverrideBounds = true;
    this.mHotspotBounds.set(paramInt1, paramInt2, paramInt3, paramInt4);
    onHotspotBoundsChanged();
  }
  
  public void getHotspotBounds(Rect paramRect) {
    paramRect.set(this.mHotspotBounds);
  }
  
  private void onHotspotBoundsChanged() {
    int i = this.mExitingRipplesCount;
    RippleForeground[] arrayOfRippleForeground = this.mExitingRipples;
    for (byte b = 0; b < i; b++)
      arrayOfRippleForeground[b].onHotspotBoundsChanged(); 
    RippleForeground rippleForeground = this.mRipple;
    if (rippleForeground != null)
      rippleForeground.onHotspotBoundsChanged(); 
    RippleBackground rippleBackground = this.mBackground;
    if (rippleBackground != null)
      rippleBackground.onHotspotBoundsChanged(); 
  }
  
  public void getOutline(Outline paramOutline) {
    LayerDrawable.LayerState layerState = this.mLayerState;
    LayerDrawable.ChildDrawable[] arrayOfChildDrawable = layerState.mChildren;
    int i = layerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      if ((arrayOfChildDrawable[b]).mId != 16908334) {
        (arrayOfChildDrawable[b]).mDrawable.getOutline(paramOutline);
        if (!paramOutline.isEmpty())
          return; 
      } 
    } 
  }
  
  public void draw(Canvas paramCanvas) {
    pruneRipples();
    Rect rect = getDirtyBounds();
    int i = paramCanvas.save(2);
    if (isBounded())
      paramCanvas.clipRect(rect); 
    drawContent(paramCanvas);
    drawBackgroundAndRipples(paramCanvas);
    paramCanvas.restoreToCount(i);
  }
  
  public void invalidateSelf() {
    invalidateSelf(true);
  }
  
  void invalidateSelf(boolean paramBoolean) {
    super.invalidateSelf();
    if (paramBoolean)
      this.mHasValidMask = false; 
  }
  
  private void pruneRipples() {
    int i = 0;
    RippleForeground[] arrayOfRippleForeground = this.mExitingRipples;
    int j = this.mExitingRipplesCount;
    int k;
    for (k = 0; k < j; k++, i = m) {
      int m = i;
      if (!arrayOfRippleForeground[k].hasFinishedExit()) {
        arrayOfRippleForeground[i] = arrayOfRippleForeground[k];
        m = i + 1;
      } 
    } 
    for (k = i; k < j; k++)
      arrayOfRippleForeground[k] = null; 
    this.mExitingRipplesCount = i;
  }
  
  private void updateMaskShaderIfNeeded() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mHasValidMask : Z
    //   4: ifeq -> 8
    //   7: return
    //   8: aload_0
    //   9: invokespecial getMaskType : ()I
    //   12: istore_1
    //   13: iload_1
    //   14: iconst_m1
    //   15: if_icmpne -> 19
    //   18: return
    //   19: aload_0
    //   20: iconst_1
    //   21: putfield mHasValidMask : Z
    //   24: aload_0
    //   25: invokevirtual getBounds : ()Landroid/graphics/Rect;
    //   28: astore_2
    //   29: iload_1
    //   30: ifeq -> 288
    //   33: aload_2
    //   34: invokevirtual isEmpty : ()Z
    //   37: ifeq -> 43
    //   40: goto -> 288
    //   43: aload_0
    //   44: getfield mMaskBuffer : Landroid/graphics/Bitmap;
    //   47: astore_3
    //   48: aload_3
    //   49: ifnull -> 93
    //   52: aload_3
    //   53: invokevirtual getWidth : ()I
    //   56: aload_2
    //   57: invokevirtual width : ()I
    //   60: if_icmpne -> 93
    //   63: aload_0
    //   64: getfield mMaskBuffer : Landroid/graphics/Bitmap;
    //   67: astore_3
    //   68: aload_3
    //   69: invokevirtual getHeight : ()I
    //   72: aload_2
    //   73: invokevirtual height : ()I
    //   76: if_icmpeq -> 82
    //   79: goto -> 93
    //   82: aload_0
    //   83: getfield mMaskBuffer : Landroid/graphics/Bitmap;
    //   86: iconst_0
    //   87: invokevirtual eraseColor : (I)V
    //   90: goto -> 169
    //   93: aload_0
    //   94: getfield mMaskBuffer : Landroid/graphics/Bitmap;
    //   97: astore_3
    //   98: aload_3
    //   99: ifnull -> 106
    //   102: aload_3
    //   103: invokevirtual recycle : ()V
    //   106: aload_2
    //   107: invokevirtual width : ()I
    //   110: istore #4
    //   112: aload_2
    //   113: invokevirtual height : ()I
    //   116: istore #5
    //   118: getstatic android/graphics/Bitmap$Config.ALPHA_8 : Landroid/graphics/Bitmap$Config;
    //   121: astore_3
    //   122: iload #4
    //   124: iload #5
    //   126: aload_3
    //   127: invokestatic createBitmap : (IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    //   130: astore_3
    //   131: aload_0
    //   132: aload_3
    //   133: putfield mMaskBuffer : Landroid/graphics/Bitmap;
    //   136: aload_0
    //   137: new android/graphics/BitmapShader
    //   140: dup
    //   141: aload_3
    //   142: getstatic android/graphics/Shader$TileMode.CLAMP : Landroid/graphics/Shader$TileMode;
    //   145: getstatic android/graphics/Shader$TileMode.CLAMP : Landroid/graphics/Shader$TileMode;
    //   148: invokespecial <init> : (Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V
    //   151: putfield mMaskShader : Landroid/graphics/BitmapShader;
    //   154: aload_0
    //   155: new android/graphics/Canvas
    //   158: dup
    //   159: aload_0
    //   160: getfield mMaskBuffer : Landroid/graphics/Bitmap;
    //   163: invokespecial <init> : (Landroid/graphics/Bitmap;)V
    //   166: putfield mMaskCanvas : Landroid/graphics/Canvas;
    //   169: aload_0
    //   170: getfield mMaskMatrix : Landroid/graphics/Matrix;
    //   173: astore_3
    //   174: aload_3
    //   175: ifnonnull -> 192
    //   178: aload_0
    //   179: new android/graphics/Matrix
    //   182: dup
    //   183: invokespecial <init> : ()V
    //   186: putfield mMaskMatrix : Landroid/graphics/Matrix;
    //   189: goto -> 196
    //   192: aload_3
    //   193: invokevirtual reset : ()V
    //   196: aload_0
    //   197: getfield mMaskColorFilter : Landroid/graphics/PorterDuffColorFilter;
    //   200: ifnonnull -> 218
    //   203: aload_0
    //   204: new android/graphics/PorterDuffColorFilter
    //   207: dup
    //   208: iconst_0
    //   209: getstatic android/graphics/PorterDuff$Mode.SRC_IN : Landroid/graphics/PorterDuff$Mode;
    //   212: invokespecial <init> : (ILandroid/graphics/PorterDuff$Mode;)V
    //   215: putfield mMaskColorFilter : Landroid/graphics/PorterDuffColorFilter;
    //   218: aload_2
    //   219: getfield left : I
    //   222: istore #5
    //   224: aload_2
    //   225: getfield top : I
    //   228: istore #4
    //   230: aload_0
    //   231: getfield mMaskCanvas : Landroid/graphics/Canvas;
    //   234: iload #5
    //   236: ineg
    //   237: i2f
    //   238: iload #4
    //   240: ineg
    //   241: i2f
    //   242: invokevirtual translate : (FF)V
    //   245: iload_1
    //   246: iconst_2
    //   247: if_icmpne -> 261
    //   250: aload_0
    //   251: aload_0
    //   252: getfield mMaskCanvas : Landroid/graphics/Canvas;
    //   255: invokespecial drawMask : (Landroid/graphics/Canvas;)V
    //   258: goto -> 274
    //   261: iload_1
    //   262: iconst_1
    //   263: if_icmpne -> 274
    //   266: aload_0
    //   267: aload_0
    //   268: getfield mMaskCanvas : Landroid/graphics/Canvas;
    //   271: invokespecial drawContent : (Landroid/graphics/Canvas;)V
    //   274: aload_0
    //   275: getfield mMaskCanvas : Landroid/graphics/Canvas;
    //   278: iload #5
    //   280: i2f
    //   281: iload #4
    //   283: i2f
    //   284: invokevirtual translate : (FF)V
    //   287: return
    //   288: aload_0
    //   289: getfield mMaskBuffer : Landroid/graphics/Bitmap;
    //   292: astore_2
    //   293: aload_2
    //   294: ifnull -> 316
    //   297: aload_2
    //   298: invokevirtual recycle : ()V
    //   301: aload_0
    //   302: aconst_null
    //   303: putfield mMaskBuffer : Landroid/graphics/Bitmap;
    //   306: aload_0
    //   307: aconst_null
    //   308: putfield mMaskShader : Landroid/graphics/BitmapShader;
    //   311: aload_0
    //   312: aconst_null
    //   313: putfield mMaskCanvas : Landroid/graphics/Canvas;
    //   316: aload_0
    //   317: aconst_null
    //   318: putfield mMaskMatrix : Landroid/graphics/Matrix;
    //   321: aload_0
    //   322: aconst_null
    //   323: putfield mMaskColorFilter : Landroid/graphics/PorterDuffColorFilter;
    //   326: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #723	-> 0
    //   #724	-> 7
    //   #727	-> 8
    //   #728	-> 13
    //   #729	-> 18
    //   #732	-> 19
    //   #734	-> 24
    //   #735	-> 29
    //   #748	-> 43
    //   #749	-> 52
    //   #750	-> 68
    //   #761	-> 82
    //   #751	-> 93
    //   #752	-> 102
    //   #755	-> 106
    //   #756	-> 106
    //   #755	-> 122
    //   #757	-> 136
    //   #759	-> 154
    //   #764	-> 169
    //   #765	-> 178
    //   #767	-> 192
    //   #770	-> 196
    //   #771	-> 203
    //   #775	-> 218
    //   #776	-> 224
    //   #777	-> 230
    //   #778	-> 245
    //   #779	-> 250
    //   #780	-> 261
    //   #781	-> 266
    //   #783	-> 274
    //   #784	-> 287
    //   #736	-> 288
    //   #737	-> 297
    //   #738	-> 301
    //   #739	-> 306
    //   #740	-> 311
    //   #742	-> 316
    //   #743	-> 321
    //   #744	-> 326
  }
  
  private int getMaskType() {
    if (this.mRipple == null && this.mExitingRipplesCount <= 0) {
      RippleBackground rippleBackground = this.mBackground;
      if (rippleBackground == null || 
        !rippleBackground.isVisible())
        return -1; 
    } 
    Drawable drawable = this.mMask;
    if (drawable != null) {
      if (drawable.getOpacity() == -1)
        return 0; 
      return 2;
    } 
    LayerDrawable.ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      if ((arrayOfChildDrawable[b]).mDrawable.getOpacity() != -1)
        return 1; 
    } 
    return 0;
  }
  
  private void drawContent(Canvas paramCanvas) {
    LayerDrawable.ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      if ((arrayOfChildDrawable[b]).mId != 16908334)
        (arrayOfChildDrawable[b]).mDrawable.draw(paramCanvas); 
    } 
  }
  
  private void drawBackgroundAndRipples(Canvas paramCanvas) {
    RippleForeground rippleForeground = this.mRipple;
    RippleBackground rippleBackground = this.mBackground;
    int i = this.mExitingRipplesCount;
    if (rippleForeground == null && i <= 0 && (rippleBackground == null || !rippleBackground.isVisible()))
      return; 
    float f1 = this.mHotspotBounds.exactCenterX();
    float f2 = this.mHotspotBounds.exactCenterY();
    paramCanvas.translate(f1, f2);
    Paint paint = getRipplePaint();
    if (rippleBackground != null && rippleBackground.isVisible())
      rippleBackground.draw(paramCanvas, paint); 
    if (i > 0) {
      RippleForeground[] arrayOfRippleForeground = this.mExitingRipples;
      for (byte b = 0; b < i; b++)
        arrayOfRippleForeground[b].draw(paramCanvas, paint); 
    } 
    if (rippleForeground != null)
      rippleForeground.draw(paramCanvas, paint); 
    paramCanvas.translate(-f1, -f2);
  }
  
  private void drawMask(Canvas paramCanvas) {
    this.mMask.draw(paramCanvas);
  }
  
  Paint getRipplePaint() {
    if (this.mRipplePaint == null) {
      Paint paint1 = new Paint();
      paint1.setAntiAlias(true);
      this.mRipplePaint.setStyle(Paint.Style.FILL);
    } 
    float f1 = this.mHotspotBounds.exactCenterX();
    float f2 = this.mHotspotBounds.exactCenterY();
    updateMaskShaderIfNeeded();
    if (this.mMaskShader != null) {
      Rect rect = getBounds();
      this.mMaskMatrix.setTranslate(rect.left - f1, rect.top - f2);
      this.mMaskShader.setLocalMatrix(this.mMaskMatrix);
    } 
    int i = this.mState.mColor.getColorForState(getState(), -16777216);
    int j = i;
    if (Color.alpha(i) > 128)
      j = 0xFFFFFF & i | Integer.MIN_VALUE; 
    Paint paint = this.mRipplePaint;
    PorterDuffColorFilter porterDuffColorFilter = this.mMaskColorFilter;
    if (porterDuffColorFilter != null) {
      i = j | 0xFF000000;
      if (porterDuffColorFilter.getColor() != i)
        this.mMaskColorFilter = new PorterDuffColorFilter(i, this.mMaskColorFilter.getMode()); 
      paint.setColor(0xFF000000 & j);
      paint.setColorFilter(this.mMaskColorFilter);
      paint.setShader(this.mMaskShader);
    } else {
      paint.setColor(j);
      paint.setColorFilter(null);
      paint.setShader(null);
    } 
    return paint;
  }
  
  public Rect getDirtyBounds() {
    if (!isBounded()) {
      Rect rect1 = this.mDrawingBounds;
      Rect rect2 = this.mDirtyBounds;
      rect2.set(rect1);
      rect1.setEmpty();
      int i = (int)this.mHotspotBounds.exactCenterX();
      int j = (int)this.mHotspotBounds.exactCenterY();
      Rect rect3 = this.mTempRect;
      RippleForeground[] arrayOfRippleForeground = this.mExitingRipples;
      int k = this.mExitingRipplesCount;
      for (byte b = 0; b < k; b++) {
        arrayOfRippleForeground[b].getBounds(rect3);
        rect3.offset(i, j);
        rect1.union(rect3);
      } 
      RippleBackground rippleBackground = this.mBackground;
      if (rippleBackground != null) {
        rippleBackground.getBounds(rect3);
        rect3.offset(i, j);
        rect1.union(rect3);
      } 
      rect2.union(rect1);
      rect2.union(super.getDirtyBounds());
      return rect2;
    } 
    return getBounds();
  }
  
  public void setForceSoftware(boolean paramBoolean) {
    this.mForceSoftware = paramBoolean;
  }
  
  public Drawable.ConstantState getConstantState() {
    return this.mState;
  }
  
  public Drawable mutate() {
    super.mutate();
    this.mState = (RippleState)this.mLayerState;
    this.mMask = findDrawableByLayerId(16908334);
    return this;
  }
  
  RippleState createConstantState(LayerDrawable.LayerState paramLayerState, Resources paramResources) {
    return new RippleState(paramLayerState, this, paramResources);
  }
  
  class RippleState extends LayerDrawable.LayerState {
    ColorStateList mColor = ColorStateList.valueOf(-65281);
    
    int mMaxRadius = -1;
    
    int[] mTouchThemeAttrs;
    
    public RippleState(RippleDrawable this$0, RippleDrawable param1RippleDrawable, Resources param1Resources) {
      super((LayerDrawable.LayerState)this$0, param1RippleDrawable, param1Resources);
      if (this$0 != null && this$0 instanceof RippleState) {
        RippleState rippleState = (RippleState)this$0;
        this.mTouchThemeAttrs = rippleState.mTouchThemeAttrs;
        this.mColor = rippleState.mColor;
        this.mMaxRadius = rippleState.mMaxRadius;
        if (rippleState.mDensity != this.mDensity)
          applyDensityScaling(((LayerDrawable.LayerState)this$0).mDensity, this.mDensity); 
      } 
    }
    
    protected void onDensityChanged(int param1Int1, int param1Int2) {
      super.onDensityChanged(param1Int1, param1Int2);
      applyDensityScaling(param1Int1, param1Int2);
    }
    
    private void applyDensityScaling(int param1Int1, int param1Int2) {
      int i = this.mMaxRadius;
      if (i != -1)
        this.mMaxRadius = Drawable.scaleFromDensity(i, param1Int1, param1Int2, true); 
    }
    
    public boolean canApplyTheme() {
      if (this.mTouchThemeAttrs == null) {
        ColorStateList colorStateList = this.mColor;
        return ((colorStateList != null && 
          colorStateList.canApplyTheme()) || 
          super.canApplyTheme());
      } 
      return true;
    }
    
    public Drawable newDrawable() {
      return new RippleDrawable(this, null);
    }
    
    public Drawable newDrawable(Resources param1Resources) {
      return new RippleDrawable(this, param1Resources);
    }
    
    public int getChangingConfigurations() {
      byte b;
      int i = super.getChangingConfigurations();
      ColorStateList colorStateList = this.mColor;
      if (colorStateList != null) {
        b = colorStateList.getChangingConfigurations();
      } else {
        b = 0;
      } 
      return i | b;
    }
  }
  
  private RippleDrawable(RippleState paramRippleState, Resources paramResources) {
    this.mState = paramRippleState = new RippleState(paramRippleState, this, paramResources);
    this.mLayerState = paramRippleState;
    this.mDensity = Drawable.resolveDensity(paramResources, this.mState.mDensity);
    if (this.mState.mNumChildren > 0) {
      ensurePadding();
      refreshPadding();
    } 
    updateLocalState();
  }
  
  private void updateLocalState() {
    this.mMask = findDrawableByLayerId(16908334);
  }
}
