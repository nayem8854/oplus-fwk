package android.graphics.drawable;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.Gravity;
import com.android.internal.R;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class ClipDrawable extends DrawableWrapper {
  public static final int HORIZONTAL = 1;
  
  private static final int MAX_LEVEL = 10000;
  
  public static final int VERTICAL = 2;
  
  private ClipState mState;
  
  private final Rect mTmpRect = new Rect();
  
  ClipDrawable() {
    this(new ClipState(null, null), (Resources)null);
  }
  
  public ClipDrawable(Drawable paramDrawable, int paramInt1, int paramInt2) {
    this(new ClipState(null, null), (Resources)null);
    this.mState.mGravity = paramInt1;
    this.mState.mOrientation = paramInt2;
    setDrawable(paramDrawable);
  }
  
  public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme) throws XmlPullParserException, IOException {
    TypedArray typedArray = obtainAttributes(paramResources, paramTheme, paramAttributeSet, R.styleable.ClipDrawable);
    super.inflate(paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
    updateStateFromTypedArray(typedArray);
    verifyRequiredAttributes(typedArray);
    typedArray.recycle();
  }
  
  public void applyTheme(Resources.Theme paramTheme) {
    super.applyTheme(paramTheme);
    ClipState clipState = this.mState;
    if (clipState == null)
      return; 
    if (clipState.mThemeAttrs != null) {
      TypedArray typedArray = paramTheme.resolveAttributes(clipState.mThemeAttrs, R.styleable.ClipDrawable);
      try {
        updateStateFromTypedArray(typedArray);
        verifyRequiredAttributes(typedArray);
        typedArray.recycle();
      } catch (XmlPullParserException xmlPullParserException) {
        rethrowAsRuntimeException((Exception)xmlPullParserException);
        typedArray.recycle();
      } finally {}
    } 
  }
  
  private void verifyRequiredAttributes(TypedArray paramTypedArray) throws XmlPullParserException {
    if (getDrawable() == null) {
      if (this.mState.mThemeAttrs != null) {
        ClipState clipState = this.mState;
        if (clipState.mThemeAttrs[1] != 0)
          return; 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramTypedArray.getPositionDescription());
      stringBuilder.append(": <clip> tag requires a 'drawable' attribute or child tag defining a drawable");
      throw new XmlPullParserException(stringBuilder.toString());
    } 
  }
  
  private void updateStateFromTypedArray(TypedArray paramTypedArray) {
    ClipState clipState = this.mState;
    if (clipState == null)
      return; 
    clipState.mChangingConfigurations |= paramTypedArray.getChangingConfigurations();
    ClipState.access$002(clipState, paramTypedArray.extractThemeAttrs());
    clipState.mOrientation = paramTypedArray.getInt(2, clipState.mOrientation);
    clipState.mGravity = paramTypedArray.getInt(0, clipState.mGravity);
  }
  
  protected boolean onLevelChange(int paramInt) {
    super.onLevelChange(paramInt);
    invalidateSelf();
    return true;
  }
  
  public int getOpacity() {
    Drawable drawable = getDrawable();
    int i = drawable.getOpacity();
    if (i == -2 || drawable.getLevel() == 0)
      return -2; 
    i = getLevel();
    if (i >= 10000)
      return drawable.getOpacity(); 
    return -3;
  }
  
  public void draw(Canvas paramCanvas) {
    Drawable drawable = getDrawable();
    if (drawable.getLevel() == 0)
      return; 
    Rect rect1 = this.mTmpRect;
    Rect rect2 = getBounds();
    int i = getLevel();
    int j = rect2.width();
    if ((this.mState.mOrientation & 0x1) != 0)
      j -= (j + 0) * (10000 - i) / 10000; 
    int k = rect2.height();
    if ((this.mState.mOrientation & 0x2) != 0)
      k -= (k + 0) * (10000 - i) / 10000; 
    i = getLayoutDirection();
    Gravity.apply(this.mState.mGravity, j, k, rect2, rect1, i);
    if (j > 0 && k > 0) {
      paramCanvas.save();
      paramCanvas.clipRect(rect1);
      drawable.draw(paramCanvas);
      paramCanvas.restore();
    } 
  }
  
  DrawableWrapper.DrawableWrapperState mutateConstantState() {
    ClipState clipState = new ClipState(this.mState, null);
    return clipState;
  }
  
  class ClipState extends DrawableWrapper.DrawableWrapperState {
    private int[] mThemeAttrs;
    
    int mOrientation = 1;
    
    int mGravity = 3;
    
    ClipState(ClipDrawable this$0, Resources param1Resources) {
      super((DrawableWrapper.DrawableWrapperState)this$0, param1Resources);
      if (this$0 != null) {
        this.mOrientation = ((ClipState)this$0).mOrientation;
        this.mGravity = ((ClipState)this$0).mGravity;
      } 
    }
    
    public Drawable newDrawable(Resources param1Resources) {
      return new ClipDrawable(this, param1Resources);
    }
  }
  
  private ClipDrawable(ClipState paramClipState, Resources paramResources) {
    super(paramClipState, paramResources);
    this.mState = paramClipState;
  }
}
