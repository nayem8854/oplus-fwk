package android.graphics.drawable;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.TypedValue;
import com.android.internal.R;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class AnimatedRotateDrawable extends DrawableWrapper implements Animatable {
  private float mCurrentDegrees;
  
  private float mIncrement;
  
  private final Runnable mNextFrame;
  
  private boolean mRunning;
  
  private AnimatedRotateState mState;
  
  public AnimatedRotateDrawable() {
    this(new AnimatedRotateState(null, null), (Resources)null);
  }
  
  public void draw(Canvas paramCanvas) {
    float f1, f2;
    Drawable drawable = getDrawable();
    Rect rect = drawable.getBounds();
    int i = rect.right, j = rect.left;
    int k = rect.bottom, m = rect.top;
    AnimatedRotateState animatedRotateState = this.mState;
    if (animatedRotateState.mPivotXRel) {
      f1 = (i - j) * animatedRotateState.mPivotX;
    } else {
      f1 = animatedRotateState.mPivotX;
    } 
    if (animatedRotateState.mPivotYRel) {
      f2 = (k - m) * animatedRotateState.mPivotY;
    } else {
      f2 = animatedRotateState.mPivotY;
    } 
    m = paramCanvas.save();
    paramCanvas.rotate(this.mCurrentDegrees, rect.left + f1, rect.top + f2);
    drawable.draw(paramCanvas);
    paramCanvas.restoreToCount(m);
  }
  
  public void start() {
    if (!this.mRunning) {
      this.mRunning = true;
      nextFrame();
    } 
  }
  
  public void stop() {
    this.mRunning = false;
    unscheduleSelf(this.mNextFrame);
  }
  
  public boolean isRunning() {
    return this.mRunning;
  }
  
  private void nextFrame() {
    unscheduleSelf(this.mNextFrame);
    scheduleSelf(this.mNextFrame, SystemClock.uptimeMillis() + this.mState.mFrameDuration);
  }
  
  public boolean setVisible(boolean paramBoolean1, boolean paramBoolean2) {
    boolean bool = super.setVisible(paramBoolean1, paramBoolean2);
    if (paramBoolean1) {
      if (bool || paramBoolean2) {
        this.mCurrentDegrees = 0.0F;
        nextFrame();
      } 
    } else {
      unscheduleSelf(this.mNextFrame);
    } 
    return bool;
  }
  
  public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme) throws XmlPullParserException, IOException {
    TypedArray typedArray = obtainAttributes(paramResources, paramTheme, paramAttributeSet, R.styleable.AnimatedRotateDrawable);
    super.inflate(paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
    updateStateFromTypedArray(typedArray);
    verifyRequiredAttributes(typedArray);
    typedArray.recycle();
    updateLocalState();
  }
  
  public void applyTheme(Resources.Theme paramTheme) {
    super.applyTheme(paramTheme);
    AnimatedRotateState animatedRotateState = this.mState;
    if (animatedRotateState == null)
      return; 
    if (animatedRotateState.mThemeAttrs != null) {
      int[] arrayOfInt2 = animatedRotateState.mThemeAttrs, arrayOfInt1 = R.styleable.AnimatedRotateDrawable;
      TypedArray typedArray = paramTheme.resolveAttributes(arrayOfInt2, arrayOfInt1);
      try {
        updateStateFromTypedArray(typedArray);
        verifyRequiredAttributes(typedArray);
        typedArray.recycle();
      } catch (XmlPullParserException xmlPullParserException) {
        rethrowAsRuntimeException((Exception)xmlPullParserException);
        typedArray.recycle();
      } finally {}
    } 
    updateLocalState();
  }
  
  private void verifyRequiredAttributes(TypedArray paramTypedArray) throws XmlPullParserException {
    if (getDrawable() == null) {
      if (this.mState.mThemeAttrs != null) {
        AnimatedRotateState animatedRotateState = this.mState;
        if (animatedRotateState.mThemeAttrs[1] != 0)
          return; 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramTypedArray.getPositionDescription());
      stringBuilder.append(": <animated-rotate> tag requires a 'drawable' attribute or child tag defining a drawable");
      throw new XmlPullParserException(stringBuilder.toString());
    } 
  }
  
  private void updateStateFromTypedArray(TypedArray paramTypedArray) {
    AnimatedRotateState animatedRotateState = this.mState;
    if (animatedRotateState == null)
      return; 
    animatedRotateState.mChangingConfigurations |= paramTypedArray.getChangingConfigurations();
    AnimatedRotateState.access$002(animatedRotateState, paramTypedArray.extractThemeAttrs());
    boolean bool = paramTypedArray.hasValue(2);
    boolean bool1 = true;
    if (bool) {
      float f;
      TypedValue typedValue = paramTypedArray.peekValue(2);
      if (typedValue.type == 6) {
        bool = true;
      } else {
        bool = false;
      } 
      animatedRotateState.mPivotXRel = bool;
      if (animatedRotateState.mPivotXRel) {
        f = typedValue.getFraction(1.0F, 1.0F);
      } else {
        f = typedValue.getFloat();
      } 
      animatedRotateState.mPivotX = f;
    } 
    if (paramTypedArray.hasValue(3)) {
      float f;
      TypedValue typedValue = paramTypedArray.peekValue(3);
      if (typedValue.type == 6) {
        bool = bool1;
      } else {
        bool = false;
      } 
      animatedRotateState.mPivotYRel = bool;
      if (animatedRotateState.mPivotYRel) {
        f = typedValue.getFraction(1.0F, 1.0F);
      } else {
        f = typedValue.getFloat();
      } 
      animatedRotateState.mPivotY = f;
    } 
    setFramesCount(paramTypedArray.getInt(5, animatedRotateState.mFramesCount));
    setFramesDuration(paramTypedArray.getInt(4, animatedRotateState.mFrameDuration));
  }
  
  public void setFramesCount(int paramInt) {
    this.mState.mFramesCount = paramInt;
    this.mIncrement = 360.0F / this.mState.mFramesCount;
  }
  
  public void setFramesDuration(int paramInt) {
    this.mState.mFrameDuration = paramInt;
  }
  
  DrawableWrapper.DrawableWrapperState mutateConstantState() {
    AnimatedRotateState animatedRotateState = new AnimatedRotateState(this.mState, null);
    return animatedRotateState;
  }
  
  class AnimatedRotateState extends DrawableWrapper.DrawableWrapperState {
    boolean mPivotXRel = false;
    
    float mPivotX = 0.0F;
    
    boolean mPivotYRel = false;
    
    float mPivotY = 0.0F;
    
    int mFrameDuration = 150;
    
    int mFramesCount = 12;
    
    private int[] mThemeAttrs;
    
    public AnimatedRotateState(AnimatedRotateDrawable this$0, Resources param1Resources) {
      super((DrawableWrapper.DrawableWrapperState)this$0, param1Resources);
      if (this$0 != null) {
        this.mPivotXRel = ((AnimatedRotateState)this$0).mPivotXRel;
        this.mPivotX = ((AnimatedRotateState)this$0).mPivotX;
        this.mPivotYRel = ((AnimatedRotateState)this$0).mPivotYRel;
        this.mPivotY = ((AnimatedRotateState)this$0).mPivotY;
        this.mFramesCount = ((AnimatedRotateState)this$0).mFramesCount;
        this.mFrameDuration = ((AnimatedRotateState)this$0).mFrameDuration;
      } 
    }
    
    public Drawable newDrawable(Resources param1Resources) {
      return new AnimatedRotateDrawable(this, param1Resources);
    }
  }
  
  private AnimatedRotateDrawable(AnimatedRotateState paramAnimatedRotateState, Resources paramResources) {
    super(paramAnimatedRotateState, paramResources);
    this.mNextFrame = (Runnable)new Object(this);
    this.mState = paramAnimatedRotateState;
    updateLocalState();
  }
  
  private void updateLocalState() {
    AnimatedRotateState animatedRotateState = this.mState;
    this.mIncrement = 360.0F / animatedRotateState.mFramesCount;
    Drawable drawable = getDrawable();
    if (drawable != null) {
      drawable.setFilterBitmap(true);
      if (drawable instanceof BitmapDrawable)
        ((BitmapDrawable)drawable).setAntiAlias(true); 
    } 
  }
}
