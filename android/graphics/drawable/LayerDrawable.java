package android.graphics.drawable;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.BlendMode;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import com.android.internal.R;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class LayerDrawable extends Drawable implements Drawable.Callback {
  private final Rect mTmpRect = new Rect();
  
  private final Rect mTmpOutRect = new Rect();
  
  private final Rect mTmpContainer = new Rect();
  
  private boolean mSuspendChildInvalidation;
  
  private int[] mPaddingT;
  
  private int[] mPaddingR;
  
  private int[] mPaddingL;
  
  private int[] mPaddingB;
  
  private boolean mMutated;
  
  LayerState mLayerState;
  
  private Rect mHotspotBounds;
  
  private boolean mChildRequestedInvalidation;
  
  public static final int PADDING_MODE_STACK = 1;
  
  public static final int PADDING_MODE_NEST = 0;
  
  private static final String LOG_TAG = "LayerDrawable";
  
  public static final int INSET_UNDEFINED = -2147483648;
  
  public LayerDrawable(Drawable[] paramArrayOfDrawable) {
    this(paramArrayOfDrawable, (LayerState)null);
  }
  
  LayerDrawable(Drawable[] paramArrayOfDrawable, LayerState paramLayerState) {
    this(paramLayerState, (Resources)null);
    if (paramArrayOfDrawable != null) {
      int i = paramArrayOfDrawable.length;
      ChildDrawable[] arrayOfChildDrawable = new ChildDrawable[i];
      for (byte b = 0; b < i; b++) {
        arrayOfChildDrawable[b] = new ChildDrawable(this.mLayerState.mDensity);
        Drawable drawable = paramArrayOfDrawable[b];
        (arrayOfChildDrawable[b]).mDrawable = drawable;
        if (drawable != null) {
          drawable.setCallback(this);
          LayerState layerState = this.mLayerState;
          layerState.mChildrenChangingConfigurations |= drawable.getChangingConfigurations();
        } 
      } 
      this.mLayerState.mNumChildren = i;
      this.mLayerState.mChildren = arrayOfChildDrawable;
      ensurePadding();
      refreshPadding();
      return;
    } 
    throw new IllegalArgumentException("layers must be non-null");
  }
  
  LayerDrawable() {
    this((LayerState)null, (Resources)null);
  }
  
  LayerDrawable(LayerState paramLayerState, Resources paramResources) {
    this.mLayerState = paramLayerState = createConstantState(paramLayerState, paramResources);
    if (paramLayerState.mNumChildren > 0) {
      ensurePadding();
      refreshPadding();
    } 
  }
  
  LayerState createConstantState(LayerState paramLayerState, Resources paramResources) {
    return new LayerState(paramLayerState, this, paramResources);
  }
  
  public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme) throws XmlPullParserException, IOException {
    super.inflate(paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
    LayerState layerState = this.mLayerState;
    int i = Drawable.resolveDensity(paramResources, 0);
    layerState.setDensity(i);
    TypedArray typedArray = obtainAttributes(paramResources, paramTheme, paramAttributeSet, R.styleable.LayerDrawable);
    updateStateFromTypedArray(typedArray);
    typedArray.recycle();
    ChildDrawable[] arrayOfChildDrawable = layerState.mChildren;
    int j = layerState.mNumChildren;
    for (byte b = 0; b < j; b++) {
      ChildDrawable childDrawable = arrayOfChildDrawable[b];
      childDrawable.setDensity(i);
    } 
    inflateLayers(paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
    ensurePadding();
    refreshPadding();
  }
  
  public void applyTheme(Resources.Theme paramTheme) {
    super.applyTheme(paramTheme);
    LayerState layerState = this.mLayerState;
    int i = Drawable.resolveDensity(paramTheme.getResources(), 0);
    layerState.setDensity(i);
    if (layerState.mThemeAttrs != null) {
      int[] arrayOfInt1 = layerState.mThemeAttrs, arrayOfInt2 = R.styleable.LayerDrawable;
      TypedArray typedArray = paramTheme.resolveAttributes(arrayOfInt1, arrayOfInt2);
      updateStateFromTypedArray(typedArray);
      typedArray.recycle();
    } 
    ChildDrawable[] arrayOfChildDrawable = layerState.mChildren;
    int j = layerState.mNumChildren;
    for (byte b = 0; b < j; b++) {
      ChildDrawable childDrawable = arrayOfChildDrawable[b];
      childDrawable.setDensity(i);
      if (childDrawable.mThemeAttrs != null) {
        TypedArray typedArray = paramTheme.resolveAttributes(childDrawable.mThemeAttrs, R.styleable.LayerDrawableItem);
        updateLayerFromTypedArray(childDrawable, typedArray);
        typedArray.recycle();
      } 
      Drawable drawable = childDrawable.mDrawable;
      if (drawable != null && drawable.canApplyTheme()) {
        drawable.applyTheme(paramTheme);
        layerState.mChildrenChangingConfigurations |= drawable.getChangingConfigurations();
      } 
    } 
  }
  
  private void inflateLayers(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme) throws XmlPullParserException, IOException {
    LayerState layerState = this.mLayerState;
    int i = paramXmlPullParser.getDepth() + 1;
    while (true) {
      int j = paramXmlPullParser.next();
      if (j != 1) {
        int k = paramXmlPullParser.getDepth();
        if (k >= i || j != 3) {
          if (j != 2)
            continue; 
          if (k > i || !paramXmlPullParser.getName().equals("item"))
            continue; 
          ChildDrawable childDrawable = new ChildDrawable(layerState.mDensity);
          TypedArray typedArray = obtainAttributes(paramResources, paramTheme, paramAttributeSet, R.styleable.LayerDrawableItem);
          updateLayerFromTypedArray(childDrawable, typedArray);
          typedArray.recycle();
          if (childDrawable.mDrawable == null && (childDrawable.mThemeAttrs == null || childDrawable.mThemeAttrs[4] == 0)) {
            while (true) {
              j = paramXmlPullParser.next();
              if (j == 4)
                continue; 
              break;
            } 
            if (j == 2) {
              childDrawable.mDrawable = Drawable.createFromXmlInner(paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
              childDrawable.mDrawable.setCallback(this);
              j = layerState.mChildrenChangingConfigurations;
              Drawable drawable = childDrawable.mDrawable;
              layerState.mChildrenChangingConfigurations = j | drawable.getChangingConfigurations();
            } else {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append(paramXmlPullParser.getPositionDescription());
              stringBuilder.append(": <item> tag requires a 'drawable' attribute or child tag defining a drawable");
              throw new XmlPullParserException(stringBuilder.toString());
            } 
          } 
          addLayer(childDrawable);
          continue;
        } 
      } 
      break;
    } 
  }
  
  private void updateStateFromTypedArray(TypedArray paramTypedArray) {
    LayerState layerState = this.mLayerState;
    layerState.mChangingConfigurations |= paramTypedArray.getChangingConfigurations();
    LayerState.access$002(layerState, paramTypedArray.extractThemeAttrs());
    int i = paramTypedArray.getIndexCount();
    for (byte b = 0; b < i; b++) {
      int j = paramTypedArray.getIndex(b);
      switch (j) {
        case 8:
          LayerState.access$202(layerState, paramTypedArray.getInteger(j, layerState.mPaddingMode));
          break;
        case 7:
          LayerState.access$102(layerState, paramTypedArray.getBoolean(j, layerState.mAutoMirrored));
          break;
        case 6:
          layerState.mPaddingEnd = paramTypedArray.getDimensionPixelOffset(j, layerState.mPaddingEnd);
          break;
        case 5:
          layerState.mPaddingStart = paramTypedArray.getDimensionPixelOffset(j, layerState.mPaddingStart);
          break;
        case 4:
          layerState.mOpacityOverride = paramTypedArray.getInt(j, layerState.mOpacityOverride);
          break;
        case 3:
          layerState.mPaddingBottom = paramTypedArray.getDimensionPixelOffset(j, layerState.mPaddingBottom);
          break;
        case 2:
          layerState.mPaddingRight = paramTypedArray.getDimensionPixelOffset(j, layerState.mPaddingRight);
          break;
        case 1:
          layerState.mPaddingTop = paramTypedArray.getDimensionPixelOffset(j, layerState.mPaddingTop);
          break;
        case 0:
          layerState.mPaddingLeft = paramTypedArray.getDimensionPixelOffset(j, layerState.mPaddingLeft);
          break;
      } 
    } 
  }
  
  private void updateLayerFromTypedArray(ChildDrawable paramChildDrawable, TypedArray paramTypedArray) {
    LayerState layerState = this.mLayerState;
    layerState.mChildrenChangingConfigurations |= paramTypedArray.getChangingConfigurations();
    paramChildDrawable.mThemeAttrs = paramTypedArray.extractThemeAttrs();
    int i = paramTypedArray.getIndexCount();
    int j;
    for (j = 0; j < i; j++) {
      int k = paramTypedArray.getIndex(j);
      switch (k) {
        case 10:
          paramChildDrawable.mInsetE = paramTypedArray.getDimensionPixelOffset(k, paramChildDrawable.mInsetE);
          break;
        case 9:
          paramChildDrawable.mInsetS = paramTypedArray.getDimensionPixelOffset(k, paramChildDrawable.mInsetS);
          break;
        case 8:
          paramChildDrawable.mInsetB = paramTypedArray.getDimensionPixelOffset(k, paramChildDrawable.mInsetB);
          break;
        case 7:
          paramChildDrawable.mInsetR = paramTypedArray.getDimensionPixelOffset(k, paramChildDrawable.mInsetR);
          break;
        case 6:
          paramChildDrawable.mInsetT = paramTypedArray.getDimensionPixelOffset(k, paramChildDrawable.mInsetT);
          break;
        case 5:
          paramChildDrawable.mInsetL = paramTypedArray.getDimensionPixelOffset(k, paramChildDrawable.mInsetL);
          break;
        case 3:
          paramChildDrawable.mWidth = paramTypedArray.getDimensionPixelSize(k, paramChildDrawable.mWidth);
          break;
        case 2:
          paramChildDrawable.mHeight = paramTypedArray.getDimensionPixelSize(k, paramChildDrawable.mHeight);
          break;
        case 1:
          paramChildDrawable.mId = paramTypedArray.getResourceId(k, paramChildDrawable.mId);
          break;
        case 0:
          paramChildDrawable.mGravity = paramTypedArray.getInteger(k, paramChildDrawable.mGravity);
          break;
      } 
    } 
    Drawable drawable = paramTypedArray.getDrawable(4);
    if (drawable != null) {
      if (paramChildDrawable.mDrawable != null)
        paramChildDrawable.mDrawable.setCallback(null); 
      paramChildDrawable.mDrawable = drawable;
      paramChildDrawable.mDrawable.setCallback(this);
      j = layerState.mChildrenChangingConfigurations;
      Drawable drawable1 = paramChildDrawable.mDrawable;
      layerState.mChildrenChangingConfigurations = j | drawable1.getChangingConfigurations();
    } 
  }
  
  public boolean canApplyTheme() {
    return (this.mLayerState.canApplyTheme() || super.canApplyTheme());
  }
  
  public boolean isProjected() {
    if (super.isProjected())
      return true; 
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null && drawable.isProjected())
        return true; 
    } 
    return false;
  }
  
  int addLayer(ChildDrawable paramChildDrawable) {
    byte b;
    LayerState layerState = this.mLayerState;
    if (layerState.mChildren != null) {
      b = layerState.mChildren.length;
    } else {
      b = 0;
    } 
    int i = layerState.mNumChildren;
    if (i >= b) {
      ChildDrawable[] arrayOfChildDrawable = new ChildDrawable[b + 10];
      if (i > 0)
        System.arraycopy(layerState.mChildren, 0, arrayOfChildDrawable, 0, i); 
      layerState.mChildren = arrayOfChildDrawable;
    } 
    layerState.mChildren[i] = paramChildDrawable;
    layerState.mNumChildren++;
    layerState.invalidateCache();
    return i;
  }
  
  ChildDrawable addLayer(Drawable paramDrawable, int[] paramArrayOfint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    ChildDrawable childDrawable = createLayer(paramDrawable);
    childDrawable.mId = paramInt1;
    childDrawable.mThemeAttrs = paramArrayOfint;
    childDrawable.mDrawable.setAutoMirrored(isAutoMirrored());
    childDrawable.mInsetL = paramInt2;
    childDrawable.mInsetT = paramInt3;
    childDrawable.mInsetR = paramInt4;
    childDrawable.mInsetB = paramInt5;
    addLayer(childDrawable);
    LayerState layerState = this.mLayerState;
    layerState.mChildrenChangingConfigurations |= paramDrawable.getChangingConfigurations();
    paramDrawable.setCallback(this);
    return childDrawable;
  }
  
  private ChildDrawable createLayer(Drawable paramDrawable) {
    ChildDrawable childDrawable = new ChildDrawable(this.mLayerState.mDensity);
    childDrawable.mDrawable = paramDrawable;
    return childDrawable;
  }
  
  public int addLayer(Drawable paramDrawable) {
    ChildDrawable childDrawable = createLayer(paramDrawable);
    int i = addLayer(childDrawable);
    ensurePadding();
    refreshChildPadding(i, childDrawable);
    return i;
  }
  
  public Drawable findDrawableByLayerId(int paramInt) {
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    for (int i = this.mLayerState.mNumChildren - 1; i >= 0; i--) {
      if ((arrayOfChildDrawable[i]).mId == paramInt)
        return (arrayOfChildDrawable[i]).mDrawable; 
    } 
    return null;
  }
  
  public void setId(int paramInt1, int paramInt2) {
    (this.mLayerState.mChildren[paramInt1]).mId = paramInt2;
  }
  
  public int getId(int paramInt) {
    if (paramInt < this.mLayerState.mNumChildren)
      return (this.mLayerState.mChildren[paramInt]).mId; 
    throw new IndexOutOfBoundsException();
  }
  
  public int getNumberOfLayers() {
    return this.mLayerState.mNumChildren;
  }
  
  public boolean setDrawableByLayerId(int paramInt, Drawable paramDrawable) {
    paramInt = findIndexByLayerId(paramInt);
    if (paramInt < 0)
      return false; 
    setDrawable(paramInt, paramDrawable);
    return true;
  }
  
  public int findIndexByLayerId(int paramInt) {
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      ChildDrawable childDrawable = arrayOfChildDrawable[b];
      if (childDrawable.mId == paramInt)
        return b; 
    } 
    return -1;
  }
  
  public void setDrawable(int paramInt, Drawable paramDrawable) {
    if (paramInt < this.mLayerState.mNumChildren) {
      ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
      ChildDrawable childDrawable = arrayOfChildDrawable[paramInt];
      if (childDrawable.mDrawable != null) {
        if (paramDrawable != null) {
          Rect rect = childDrawable.mDrawable.getBounds();
          paramDrawable.setBounds(rect);
        } 
        childDrawable.mDrawable.setCallback(null);
      } 
      if (paramDrawable != null)
        paramDrawable.setCallback(this); 
      childDrawable.mDrawable = paramDrawable;
      this.mLayerState.invalidateCache();
      refreshChildPadding(paramInt, childDrawable);
      return;
    } 
    throw new IndexOutOfBoundsException();
  }
  
  public Drawable getDrawable(int paramInt) {
    if (paramInt < this.mLayerState.mNumChildren)
      return (this.mLayerState.mChildren[paramInt]).mDrawable; 
    throw new IndexOutOfBoundsException();
  }
  
  public void setLayerSize(int paramInt1, int paramInt2, int paramInt3) {
    ChildDrawable childDrawable = this.mLayerState.mChildren[paramInt1];
    childDrawable.mWidth = paramInt2;
    childDrawable.mHeight = paramInt3;
  }
  
  public void setLayerWidth(int paramInt1, int paramInt2) {
    ChildDrawable childDrawable = this.mLayerState.mChildren[paramInt1];
    childDrawable.mWidth = paramInt2;
  }
  
  public int getLayerWidth(int paramInt) {
    ChildDrawable childDrawable = this.mLayerState.mChildren[paramInt];
    return childDrawable.mWidth;
  }
  
  public void setLayerHeight(int paramInt1, int paramInt2) {
    ChildDrawable childDrawable = this.mLayerState.mChildren[paramInt1];
    childDrawable.mHeight = paramInt2;
  }
  
  public int getLayerHeight(int paramInt) {
    ChildDrawable childDrawable = this.mLayerState.mChildren[paramInt];
    return childDrawable.mHeight;
  }
  
  public void setLayerGravity(int paramInt1, int paramInt2) {
    ChildDrawable childDrawable = this.mLayerState.mChildren[paramInt1];
    childDrawable.mGravity = paramInt2;
  }
  
  public int getLayerGravity(int paramInt) {
    ChildDrawable childDrawable = this.mLayerState.mChildren[paramInt];
    return childDrawable.mGravity;
  }
  
  public void setLayerInset(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    setLayerInsetInternal(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, -2147483648, -2147483648);
  }
  
  public void setLayerInsetRelative(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    setLayerInsetInternal(paramInt1, 0, paramInt3, 0, paramInt5, paramInt2, paramInt4);
  }
  
  public void setLayerInsetLeft(int paramInt1, int paramInt2) {
    ChildDrawable childDrawable = this.mLayerState.mChildren[paramInt1];
    childDrawable.mInsetL = paramInt2;
  }
  
  public int getLayerInsetLeft(int paramInt) {
    ChildDrawable childDrawable = this.mLayerState.mChildren[paramInt];
    return childDrawable.mInsetL;
  }
  
  public void setLayerInsetRight(int paramInt1, int paramInt2) {
    ChildDrawable childDrawable = this.mLayerState.mChildren[paramInt1];
    childDrawable.mInsetR = paramInt2;
  }
  
  public int getLayerInsetRight(int paramInt) {
    ChildDrawable childDrawable = this.mLayerState.mChildren[paramInt];
    return childDrawable.mInsetR;
  }
  
  public void setLayerInsetTop(int paramInt1, int paramInt2) {
    ChildDrawable childDrawable = this.mLayerState.mChildren[paramInt1];
    childDrawable.mInsetT = paramInt2;
  }
  
  public int getLayerInsetTop(int paramInt) {
    ChildDrawable childDrawable = this.mLayerState.mChildren[paramInt];
    return childDrawable.mInsetT;
  }
  
  public void setLayerInsetBottom(int paramInt1, int paramInt2) {
    ChildDrawable childDrawable = this.mLayerState.mChildren[paramInt1];
    childDrawable.mInsetB = paramInt2;
  }
  
  public int getLayerInsetBottom(int paramInt) {
    ChildDrawable childDrawable = this.mLayerState.mChildren[paramInt];
    return childDrawable.mInsetB;
  }
  
  public void setLayerInsetStart(int paramInt1, int paramInt2) {
    ChildDrawable childDrawable = this.mLayerState.mChildren[paramInt1];
    childDrawable.mInsetS = paramInt2;
  }
  
  public int getLayerInsetStart(int paramInt) {
    ChildDrawable childDrawable = this.mLayerState.mChildren[paramInt];
    return childDrawable.mInsetS;
  }
  
  public void setLayerInsetEnd(int paramInt1, int paramInt2) {
    ChildDrawable childDrawable = this.mLayerState.mChildren[paramInt1];
    childDrawable.mInsetE = paramInt2;
  }
  
  public int getLayerInsetEnd(int paramInt) {
    ChildDrawable childDrawable = this.mLayerState.mChildren[paramInt];
    return childDrawable.mInsetE;
  }
  
  private void setLayerInsetInternal(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7) {
    ChildDrawable childDrawable = this.mLayerState.mChildren[paramInt1];
    childDrawable.mInsetL = paramInt2;
    childDrawable.mInsetT = paramInt3;
    childDrawable.mInsetR = paramInt4;
    childDrawable.mInsetB = paramInt5;
    childDrawable.mInsetS = paramInt6;
    childDrawable.mInsetE = paramInt7;
  }
  
  public void setPaddingMode(int paramInt) {
    if (this.mLayerState.mPaddingMode != paramInt)
      LayerState.access$202(this.mLayerState, paramInt); 
  }
  
  public int getPaddingMode() {
    return this.mLayerState.mPaddingMode;
  }
  
  private void suspendChildInvalidation() {
    this.mSuspendChildInvalidation = true;
  }
  
  private void resumeChildInvalidation() {
    this.mSuspendChildInvalidation = false;
    if (this.mChildRequestedInvalidation) {
      this.mChildRequestedInvalidation = false;
      invalidateSelf();
    } 
  }
  
  public void invalidateDrawable(Drawable paramDrawable) {
    if (this.mSuspendChildInvalidation) {
      this.mChildRequestedInvalidation = true;
    } else {
      this.mLayerState.invalidateCache();
      invalidateSelf();
    } 
  }
  
  public void scheduleDrawable(Drawable paramDrawable, Runnable paramRunnable, long paramLong) {
    scheduleSelf(paramRunnable, paramLong);
  }
  
  public void unscheduleDrawable(Drawable paramDrawable, Runnable paramRunnable) {
    unscheduleSelf(paramRunnable);
  }
  
  public void draw(Canvas paramCanvas) {
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.draw(paramCanvas); 
    } 
  }
  
  public int getChangingConfigurations() {
    return super.getChangingConfigurations() | this.mLayerState.getChangingConfigurations();
  }
  
  public boolean getPadding(Rect paramRect) {
    int m;
    LayerState layerState = this.mLayerState;
    if (layerState.mPaddingMode == 0) {
      computeNestedPadding(paramRect);
    } else {
      computeStackedPadding(paramRect);
    } 
    int i = layerState.mPaddingTop;
    int j = layerState.mPaddingBottom;
    int k = getLayoutDirection();
    boolean bool = false;
    if (k == 1) {
      m = 1;
    } else {
      m = 0;
    } 
    if (m) {
      k = layerState.mPaddingEnd;
    } else {
      k = layerState.mPaddingStart;
    } 
    if (m) {
      m = layerState.mPaddingStart;
    } else {
      m = layerState.mPaddingEnd;
    } 
    if (k < 0)
      k = layerState.mPaddingLeft; 
    if (m < 0)
      m = layerState.mPaddingRight; 
    if (k >= 0)
      paramRect.left = k; 
    if (i >= 0)
      paramRect.top = i; 
    if (m >= 0)
      paramRect.right = m; 
    if (j >= 0)
      paramRect.bottom = j; 
    if (paramRect.left != 0 || paramRect.top != 0 || paramRect.right != 0 || paramRect.bottom != 0)
      bool = true; 
    return bool;
  }
  
  public void setPadding(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    LayerState layerState = this.mLayerState;
    layerState.mPaddingLeft = paramInt1;
    layerState.mPaddingTop = paramInt2;
    layerState.mPaddingRight = paramInt3;
    layerState.mPaddingBottom = paramInt4;
    layerState.mPaddingStart = -1;
    layerState.mPaddingEnd = -1;
  }
  
  public void setPaddingRelative(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    LayerState layerState = this.mLayerState;
    layerState.mPaddingStart = paramInt1;
    layerState.mPaddingTop = paramInt2;
    layerState.mPaddingEnd = paramInt3;
    layerState.mPaddingBottom = paramInt4;
    layerState.mPaddingLeft = -1;
    layerState.mPaddingRight = -1;
  }
  
  public int getLeftPadding() {
    return this.mLayerState.mPaddingLeft;
  }
  
  public int getRightPadding() {
    return this.mLayerState.mPaddingRight;
  }
  
  public int getStartPadding() {
    return this.mLayerState.mPaddingStart;
  }
  
  public int getEndPadding() {
    return this.mLayerState.mPaddingEnd;
  }
  
  public int getTopPadding() {
    return this.mLayerState.mPaddingTop;
  }
  
  public int getBottomPadding() {
    return this.mLayerState.mPaddingBottom;
  }
  
  private void computeNestedPadding(Rect paramRect) {
    paramRect.left = 0;
    paramRect.top = 0;
    paramRect.right = 0;
    paramRect.bottom = 0;
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      refreshChildPadding(b, arrayOfChildDrawable[b]);
      paramRect.left += this.mPaddingL[b];
      paramRect.top += this.mPaddingT[b];
      paramRect.right += this.mPaddingR[b];
      paramRect.bottom += this.mPaddingB[b];
    } 
  }
  
  private void computeStackedPadding(Rect paramRect) {
    paramRect.left = 0;
    paramRect.top = 0;
    paramRect.right = 0;
    paramRect.bottom = 0;
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      refreshChildPadding(b, arrayOfChildDrawable[b]);
      paramRect.left = Math.max(paramRect.left, this.mPaddingL[b]);
      paramRect.top = Math.max(paramRect.top, this.mPaddingT[b]);
      paramRect.right = Math.max(paramRect.right, this.mPaddingR[b]);
      paramRect.bottom = Math.max(paramRect.bottom, this.mPaddingB[b]);
    } 
  }
  
  public void getOutline(Outline paramOutline) {
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null) {
        drawable.getOutline(paramOutline);
        if (!paramOutline.isEmpty())
          return; 
      } 
    } 
  }
  
  public void setHotspot(float paramFloat1, float paramFloat2) {
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.setHotspot(paramFloat1, paramFloat2); 
    } 
  }
  
  public void setHotspotBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.setHotspotBounds(paramInt1, paramInt2, paramInt3, paramInt4); 
    } 
    Rect rect = this.mHotspotBounds;
    if (rect == null) {
      this.mHotspotBounds = new Rect(paramInt1, paramInt2, paramInt3, paramInt4);
    } else {
      rect.set(paramInt1, paramInt2, paramInt3, paramInt4);
    } 
  }
  
  public void getHotspotBounds(Rect paramRect) {
    Rect rect = this.mHotspotBounds;
    if (rect != null) {
      paramRect.set(rect);
    } else {
      super.getHotspotBounds(paramRect);
    } 
  }
  
  public boolean setVisible(boolean paramBoolean1, boolean paramBoolean2) {
    boolean bool = super.setVisible(paramBoolean1, paramBoolean2);
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.setVisible(paramBoolean1, paramBoolean2); 
    } 
    return bool;
  }
  
  public void setDither(boolean paramBoolean) {
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.setDither(paramBoolean); 
    } 
  }
  
  public void setAlpha(int paramInt) {
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.setAlpha(paramInt); 
    } 
  }
  
  public int getAlpha() {
    Drawable drawable = getFirstNonNullDrawable();
    if (drawable != null)
      return drawable.getAlpha(); 
    return super.getAlpha();
  }
  
  public void setColorFilter(ColorFilter paramColorFilter) {
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.setColorFilter(paramColorFilter); 
    } 
  }
  
  public void setTintList(ColorStateList paramColorStateList) {
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.setTintList(paramColorStateList); 
    } 
  }
  
  public void setTintBlendMode(BlendMode paramBlendMode) {
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.setTintBlendMode(paramBlendMode); 
    } 
  }
  
  private Drawable getFirstNonNullDrawable() {
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        return drawable; 
    } 
    return null;
  }
  
  public void setOpacity(int paramInt) {
    this.mLayerState.mOpacityOverride = paramInt;
  }
  
  public int getOpacity() {
    if (this.mLayerState.mOpacityOverride != 0)
      return this.mLayerState.mOpacityOverride; 
    return this.mLayerState.getOpacity();
  }
  
  public void setAutoMirrored(boolean paramBoolean) {
    LayerState.access$102(this.mLayerState, paramBoolean);
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.setAutoMirrored(paramBoolean); 
    } 
  }
  
  public boolean isAutoMirrored() {
    return this.mLayerState.mAutoMirrored;
  }
  
  public void jumpToCurrentState() {
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.jumpToCurrentState(); 
    } 
  }
  
  public boolean isStateful() {
    return this.mLayerState.isStateful();
  }
  
  public boolean hasFocusStateSpecified() {
    return this.mLayerState.hasFocusStateSpecified();
  }
  
  protected boolean onStateChange(int[] paramArrayOfint) {
    boolean bool = false;
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++, bool = bool1) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      boolean bool1 = bool;
      if (drawable != null) {
        bool1 = bool;
        if (drawable.isStateful()) {
          bool1 = bool;
          if (drawable.setState(paramArrayOfint)) {
            refreshChildPadding(b, arrayOfChildDrawable[b]);
            bool1 = true;
          } 
        } 
      } 
    } 
    if (bool)
      updateLayerBounds(getBounds()); 
    return bool;
  }
  
  protected boolean onLevelChange(int paramInt) {
    boolean bool = false;
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++, bool = bool1) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      boolean bool1 = bool;
      if (drawable != null) {
        bool1 = bool;
        if (drawable.setLevel(paramInt)) {
          refreshChildPadding(b, arrayOfChildDrawable[b]);
          bool1 = true;
        } 
      } 
    } 
    if (bool)
      updateLayerBounds(getBounds()); 
    return bool;
  }
  
  protected void onBoundsChange(Rect paramRect) {
    updateLayerBounds(paramRect);
  }
  
  private void updateLayerBounds(Rect paramRect) {
    try {
      suspendChildInvalidation();
      updateLayerBoundsInternal(paramRect);
      return;
    } finally {
      resumeChildInvalidation();
    } 
  }
  
  private void updateLayerBoundsInternal(Rect paramRect) {
    int i = 0;
    int j = 0;
    int k = 0;
    int m = 0;
    Rect rect = this.mTmpOutRect;
    int n = getLayoutDirection();
    int i1 = 0;
    if (n == 1) {
      i2 = 1;
    } else {
      i2 = 0;
    } 
    int i3 = i2;
    int i2 = i1;
    if (this.mLayerState.mPaddingMode == 0)
      i2 = 1; 
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i4;
    byte b;
    int i5;
    for (i4 = this.mLayerState.mNumChildren, b = 0, i5 = i2; b < i4; b++, i = i6, j = i7, k = i1, m = i2) {
      int i6, i7;
      ChildDrawable childDrawable = arrayOfChildDrawable[b];
      Drawable drawable = childDrawable.mDrawable;
      if (drawable == null) {
        i6 = i;
        i7 = j;
        i1 = k;
        i2 = m;
      } else {
        i7 = childDrawable.mInsetT;
        i6 = childDrawable.mInsetB;
        if (i3) {
          i1 = childDrawable.mInsetE;
        } else {
          i1 = childDrawable.mInsetS;
        } 
        if (i3 != 0) {
          i2 = childDrawable.mInsetS;
        } else {
          i2 = childDrawable.mInsetE;
        } 
        if (i1 == Integer.MIN_VALUE)
          i1 = childDrawable.mInsetL; 
        if (i2 == Integer.MIN_VALUE)
          i2 = childDrawable.mInsetR; 
        Rect rect1 = this.mTmpContainer;
        rect1.set(paramRect.left + i1 + i, paramRect.top + i7 + j, paramRect.right - i2 - k, paramRect.bottom - i6 - m);
        i2 = drawable.getIntrinsicWidth();
        i7 = drawable.getIntrinsicHeight();
        i6 = childDrawable.mWidth;
        i1 = childDrawable.mHeight;
        int i8 = resolveGravity(childDrawable.mGravity, i6, i1, i2, i7);
        if (i6 >= 0)
          i2 = i6; 
        if (i1 < 0)
          i1 = i7; 
        Gravity.apply(i8, i2, i1, rect1, rect, n);
        drawable.setBounds(rect);
        i6 = i;
        i7 = j;
        i1 = k;
        i2 = m;
        if (i5 != 0) {
          i6 = i + this.mPaddingL[b];
          i1 = k + this.mPaddingR[b];
          i7 = j + this.mPaddingT[b];
          i2 = m + this.mPaddingB[b];
        } 
      } 
    } 
  }
  
  private static int resolveGravity(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    int i = paramInt1;
    if (!Gravity.isHorizontal(paramInt1))
      if (paramInt2 < 0) {
        i = paramInt1 | 0x7;
      } else {
        i = paramInt1 | 0x800003;
      }  
    paramInt1 = i;
    if (!Gravity.isVertical(i))
      if (paramInt3 < 0) {
        paramInt1 = i | 0x70;
      } else {
        paramInt1 = i | 0x30;
      }  
    i = paramInt1;
    if (paramInt2 < 0) {
      i = paramInt1;
      if (paramInt4 < 0)
        i = paramInt1 | 0x7; 
    } 
    paramInt1 = i;
    if (paramInt3 < 0) {
      paramInt1 = i;
      if (paramInt5 < 0)
        paramInt1 = i | 0x70; 
    } 
    return paramInt1;
  }
  
  public int getIntrinsicWidth() {
    boolean bool2;
    int i = -1;
    int j = 0;
    int k = 0;
    int m = this.mLayerState.mPaddingMode;
    boolean bool1 = false;
    if (m == 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (getLayoutDirection() == 1)
      bool1 = true; 
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int n = this.mLayerState.mNumChildren;
    for (byte b = 0; b < n; b++, i = m) {
      ChildDrawable childDrawable = arrayOfChildDrawable[b];
      if (childDrawable.mDrawable == null) {
        m = i;
      } else {
        int i1, i2;
        if (bool1) {
          i1 = childDrawable.mInsetE;
        } else {
          i1 = childDrawable.mInsetS;
        } 
        if (bool1) {
          m = childDrawable.mInsetS;
        } else {
          m = childDrawable.mInsetE;
        } 
        if (i1 == Integer.MIN_VALUE)
          i1 = childDrawable.mInsetL; 
        if (m == Integer.MIN_VALUE)
          m = childDrawable.mInsetR; 
        if (childDrawable.mWidth < 0) {
          i2 = childDrawable.mDrawable.getIntrinsicWidth();
        } else {
          i2 = childDrawable.mWidth;
        } 
        if (i2 < 0) {
          i1 = -1;
        } else {
          i1 = i2 + i1 + m + j + k;
        } 
        m = i;
        if (i1 > i)
          m = i1; 
        if (bool2) {
          j += this.mPaddingL[b];
          k += this.mPaddingR[b];
        } 
      } 
    } 
    return i;
  }
  
  public int getIntrinsicHeight() {
    boolean bool;
    int i = -1;
    int j = 0;
    int k = 0;
    if (this.mLayerState.mPaddingMode == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int m = this.mLayerState.mNumChildren;
    for (byte b = 0; b < m; b++, j = n, k = i1) {
      int n, i1;
      ChildDrawable childDrawable = arrayOfChildDrawable[b];
      if (childDrawable.mDrawable == null) {
        n = j;
        i1 = k;
      } else {
        if (childDrawable.mHeight < 0) {
          i2 = childDrawable.mDrawable.getIntrinsicHeight();
        } else {
          i2 = childDrawable.mHeight;
        } 
        if (i2 < 0) {
          i1 = -1;
        } else {
          i1 = childDrawable.mInsetT + i2 + childDrawable.mInsetB + j + k;
        } 
        int i2 = i;
        if (i1 > i)
          i2 = i1; 
        i = i2;
        n = j;
        i1 = k;
        if (bool) {
          n = j + this.mPaddingT[b];
          i1 = k + this.mPaddingB[b];
          i = i2;
        } 
      } 
    } 
    return i;
  }
  
  private boolean refreshChildPadding(int paramInt, ChildDrawable paramChildDrawable) {
    if (paramChildDrawable.mDrawable != null) {
      Rect rect = this.mTmpRect;
      paramChildDrawable.mDrawable.getPadding(rect);
      if (rect.left != this.mPaddingL[paramInt] || rect.top != this.mPaddingT[paramInt] || rect.right != this.mPaddingR[paramInt] || rect.bottom != this.mPaddingB[paramInt]) {
        this.mPaddingL[paramInt] = rect.left;
        this.mPaddingT[paramInt] = rect.top;
        this.mPaddingR[paramInt] = rect.right;
        this.mPaddingB[paramInt] = rect.bottom;
        return true;
      } 
    } 
    return false;
  }
  
  void ensurePadding() {
    int i = this.mLayerState.mNumChildren;
    int[] arrayOfInt = this.mPaddingL;
    if (arrayOfInt != null && arrayOfInt.length >= i)
      return; 
    this.mPaddingL = new int[i];
    this.mPaddingT = new int[i];
    this.mPaddingR = new int[i];
    this.mPaddingB = new int[i];
  }
  
  void refreshPadding() {
    int i = this.mLayerState.mNumChildren;
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    for (byte b = 0; b < i; b++)
      refreshChildPadding(b, arrayOfChildDrawable[b]); 
  }
  
  public Drawable.ConstantState getConstantState() {
    if (this.mLayerState.canConstantState()) {
      this.mLayerState.mChangingConfigurations = getChangingConfigurations();
      return this.mLayerState;
    } 
    return null;
  }
  
  public Drawable mutate() {
    if (!this.mMutated && super.mutate() == this) {
      LayerState layerState = createConstantState(this.mLayerState, (Resources)null);
      ChildDrawable[] arrayOfChildDrawable = layerState.mChildren;
      int i = this.mLayerState.mNumChildren;
      for (byte b = 0; b < i; b++) {
        Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
        if (drawable != null)
          drawable.mutate(); 
      } 
      this.mMutated = true;
    } 
    return this;
  }
  
  public void clearMutated() {
    super.clearMutated();
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.clearMutated(); 
    } 
    this.mMutated = false;
  }
  
  public boolean onLayoutDirectionChanged(int paramInt) {
    boolean bool = false;
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    int i = this.mLayerState.mNumChildren;
    for (byte b = 0; b < i; b++, bool = bool1) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      boolean bool1 = bool;
      if (drawable != null)
        bool1 = bool | drawable.setLayoutDirection(paramInt); 
    } 
    updateLayerBounds(getBounds());
    return bool;
  }
  
  class ChildDrawable {
    public int mDensity;
    
    public Drawable mDrawable;
    
    public int mGravity;
    
    public int mHeight;
    
    public int mId;
    
    public int mInsetB;
    
    public int mInsetE;
    
    public int mInsetL;
    
    public int mInsetR;
    
    public int mInsetS;
    
    public int mInsetT;
    
    public int[] mThemeAttrs;
    
    public int mWidth;
    
    ChildDrawable(LayerDrawable this$0) {
      this.mDensity = 160;
      this.mInsetS = Integer.MIN_VALUE;
      this.mInsetE = Integer.MIN_VALUE;
      this.mWidth = -1;
      this.mHeight = -1;
      this.mGravity = 0;
      this.mId = -1;
      this.mDensity = this$0;
    }
    
    ChildDrawable(LayerDrawable this$0, LayerDrawable param1LayerDrawable, Resources param1Resources) {
      Drawable drawable2;
      this.mDensity = 160;
      this.mInsetS = Integer.MIN_VALUE;
      this.mInsetE = Integer.MIN_VALUE;
      this.mWidth = -1;
      this.mHeight = -1;
      this.mGravity = 0;
      this.mId = -1;
      Drawable drawable1 = ((ChildDrawable)this$0).mDrawable;
      if (drawable1 != null) {
        Drawable.ConstantState constantState = drawable1.getConstantState();
        if (constantState == null) {
          Drawable drawable = drawable1;
          drawable2 = drawable;
          if (drawable1.getCallback() != null) {
            Log.w("LayerDrawable", "Invalid drawable added to LayerDrawable! Drawable already belongs to another owner but does not expose a constant state.", new RuntimeException());
            drawable2 = drawable;
          } 
        } else if (param1Resources != null) {
          drawable2 = drawable2.newDrawable(param1Resources);
        } else {
          drawable2 = drawable2.newDrawable();
        } 
        drawable2.setLayoutDirection(drawable1.getLayoutDirection());
        drawable2.setBounds(drawable1.getBounds());
        drawable2.setLevel(drawable1.getLevel());
        drawable2.setCallback(param1LayerDrawable);
      } else {
        drawable2 = null;
      } 
      this.mDrawable = drawable2;
      this.mThemeAttrs = ((ChildDrawable)this$0).mThemeAttrs;
      this.mInsetL = ((ChildDrawable)this$0).mInsetL;
      this.mInsetT = ((ChildDrawable)this$0).mInsetT;
      this.mInsetR = ((ChildDrawable)this$0).mInsetR;
      this.mInsetB = ((ChildDrawable)this$0).mInsetB;
      this.mInsetS = ((ChildDrawable)this$0).mInsetS;
      this.mInsetE = ((ChildDrawable)this$0).mInsetE;
      this.mWidth = ((ChildDrawable)this$0).mWidth;
      this.mHeight = ((ChildDrawable)this$0).mHeight;
      this.mGravity = ((ChildDrawable)this$0).mGravity;
      this.mId = ((ChildDrawable)this$0).mId;
      int i = Drawable.resolveDensity(param1Resources, ((ChildDrawable)this$0).mDensity);
      int j = ((ChildDrawable)this$0).mDensity;
      if (j != i)
        applyDensityScaling(j, i); 
    }
    
    public boolean canApplyTheme() {
      if (this.mThemeAttrs == null) {
        Drawable drawable = this.mDrawable;
        return (drawable != null && 
          drawable.canApplyTheme());
      } 
      return true;
    }
    
    public final void setDensity(int param1Int) {
      if (this.mDensity != param1Int) {
        int i = this.mDensity;
        this.mDensity = param1Int;
        applyDensityScaling(i, param1Int);
      } 
    }
    
    private void applyDensityScaling(int param1Int1, int param1Int2) {
      this.mInsetL = Drawable.scaleFromDensity(this.mInsetL, param1Int1, param1Int2, false);
      this.mInsetT = Drawable.scaleFromDensity(this.mInsetT, param1Int1, param1Int2, false);
      this.mInsetR = Drawable.scaleFromDensity(this.mInsetR, param1Int1, param1Int2, false);
      this.mInsetB = Drawable.scaleFromDensity(this.mInsetB, param1Int1, param1Int2, false);
      int i = this.mInsetS;
      if (i != Integer.MIN_VALUE)
        this.mInsetS = Drawable.scaleFromDensity(i, param1Int1, param1Int2, false); 
      i = this.mInsetE;
      if (i != Integer.MIN_VALUE)
        this.mInsetE = Drawable.scaleFromDensity(i, param1Int1, param1Int2, false); 
      i = this.mWidth;
      if (i > 0)
        this.mWidth = Drawable.scaleFromDensity(i, param1Int1, param1Int2, true); 
      i = this.mHeight;
      if (i > 0)
        this.mHeight = Drawable.scaleFromDensity(i, param1Int1, param1Int2, true); 
    }
  }
  
  class LayerState extends Drawable.ConstantState {
    private boolean mAutoMirrored;
    
    int mChangingConfigurations;
    
    private boolean mCheckedOpacity;
    
    private boolean mCheckedStateful;
    
    LayerDrawable.ChildDrawable[] mChildren;
    
    int mChildrenChangingConfigurations;
    
    int mDensity;
    
    private boolean mIsStateful;
    
    int mNumChildren;
    
    private int mOpacity;
    
    int mOpacityOverride;
    
    int mPaddingBottom;
    
    int mPaddingEnd;
    
    int mPaddingLeft;
    
    private int mPaddingMode;
    
    int mPaddingRight;
    
    int mPaddingStart;
    
    int mPaddingTop;
    
    private int[] mThemeAttrs;
    
    LayerState(LayerDrawable this$0, LayerDrawable param1LayerDrawable, Resources param1Resources) {
      int i;
      this.mPaddingTop = -1;
      this.mPaddingBottom = -1;
      this.mPaddingLeft = -1;
      this.mPaddingRight = -1;
      this.mPaddingStart = -1;
      this.mPaddingEnd = -1;
      this.mOpacityOverride = 0;
      this.mAutoMirrored = false;
      this.mPaddingMode = 0;
      if (this$0 != null) {
        i = ((LayerState)this$0).mDensity;
      } else {
        i = 0;
      } 
      this.mDensity = Drawable.resolveDensity(param1Resources, i);
      if (this$0 != null) {
        LayerDrawable.ChildDrawable[] arrayOfChildDrawable = ((LayerState)this$0).mChildren;
        int j = ((LayerState)this$0).mNumChildren;
        this.mNumChildren = j;
        this.mChildren = new LayerDrawable.ChildDrawable[j];
        this.mChangingConfigurations = ((LayerState)this$0).mChangingConfigurations;
        this.mChildrenChangingConfigurations = ((LayerState)this$0).mChildrenChangingConfigurations;
        for (i = 0; i < j; i++) {
          LayerDrawable.ChildDrawable childDrawable = arrayOfChildDrawable[i];
          this.mChildren[i] = new LayerDrawable.ChildDrawable(childDrawable, param1LayerDrawable, param1Resources);
        } 
        this.mCheckedOpacity = ((LayerState)this$0).mCheckedOpacity;
        this.mOpacity = ((LayerState)this$0).mOpacity;
        this.mCheckedStateful = ((LayerState)this$0).mCheckedStateful;
        this.mIsStateful = ((LayerState)this$0).mIsStateful;
        this.mAutoMirrored = ((LayerState)this$0).mAutoMirrored;
        this.mPaddingMode = ((LayerState)this$0).mPaddingMode;
        this.mThemeAttrs = ((LayerState)this$0).mThemeAttrs;
        this.mPaddingTop = ((LayerState)this$0).mPaddingTop;
        this.mPaddingBottom = ((LayerState)this$0).mPaddingBottom;
        this.mPaddingLeft = ((LayerState)this$0).mPaddingLeft;
        this.mPaddingRight = ((LayerState)this$0).mPaddingRight;
        this.mPaddingStart = ((LayerState)this$0).mPaddingStart;
        this.mPaddingEnd = ((LayerState)this$0).mPaddingEnd;
        this.mOpacityOverride = ((LayerState)this$0).mOpacityOverride;
        j = ((LayerState)this$0).mDensity;
        i = this.mDensity;
        if (j != i)
          applyDensityScaling(j, i); 
      } else {
        this.mNumChildren = 0;
        this.mChildren = null;
      } 
    }
    
    public final void setDensity(int param1Int) {
      if (this.mDensity != param1Int) {
        int i = this.mDensity;
        this.mDensity = param1Int;
        onDensityChanged(i, param1Int);
      } 
    }
    
    protected void onDensityChanged(int param1Int1, int param1Int2) {
      applyDensityScaling(param1Int1, param1Int2);
    }
    
    private void applyDensityScaling(int param1Int1, int param1Int2) {
      int i = this.mPaddingLeft;
      if (i > 0)
        this.mPaddingLeft = Drawable.scaleFromDensity(i, param1Int1, param1Int2, false); 
      i = this.mPaddingTop;
      if (i > 0)
        this.mPaddingTop = Drawable.scaleFromDensity(i, param1Int1, param1Int2, false); 
      i = this.mPaddingRight;
      if (i > 0)
        this.mPaddingRight = Drawable.scaleFromDensity(i, param1Int1, param1Int2, false); 
      i = this.mPaddingBottom;
      if (i > 0)
        this.mPaddingBottom = Drawable.scaleFromDensity(i, param1Int1, param1Int2, false); 
      i = this.mPaddingStart;
      if (i > 0)
        this.mPaddingStart = Drawable.scaleFromDensity(i, param1Int1, param1Int2, false); 
      i = this.mPaddingEnd;
      if (i > 0)
        this.mPaddingEnd = Drawable.scaleFromDensity(i, param1Int1, param1Int2, false); 
    }
    
    public boolean canApplyTheme() {
      if (this.mThemeAttrs != null || super.canApplyTheme())
        return true; 
      LayerDrawable.ChildDrawable[] arrayOfChildDrawable = this.mChildren;
      int i = this.mNumChildren;
      for (byte b = 0; b < i; b++) {
        LayerDrawable.ChildDrawable childDrawable = arrayOfChildDrawable[b];
        if (childDrawable.canApplyTheme())
          return true; 
      } 
      return false;
    }
    
    public Drawable newDrawable() {
      return new LayerDrawable(this, null);
    }
    
    public Drawable newDrawable(Resources param1Resources) {
      return new LayerDrawable(this, param1Resources);
    }
    
    public int getChangingConfigurations() {
      return this.mChangingConfigurations | this.mChildrenChangingConfigurations;
    }
    
    public final int getOpacity() {
      int m;
      if (this.mCheckedOpacity)
        return this.mOpacity; 
      int i = this.mNumChildren;
      LayerDrawable.ChildDrawable[] arrayOfChildDrawable = this.mChildren;
      int j = -1;
      int k = 0;
      while (true) {
        m = j;
        if (k < i) {
          if ((arrayOfChildDrawable[k]).mDrawable != null) {
            m = k;
            break;
          } 
          k++;
          continue;
        } 
        break;
      } 
      if (m >= 0) {
        k = (arrayOfChildDrawable[m]).mDrawable.getOpacity();
      } else {
        k = -2;
      } 
      m++;
      for (j = k; m < i; m++, j = k) {
        Drawable drawable = (arrayOfChildDrawable[m]).mDrawable;
        k = j;
        if (drawable != null)
          k = Drawable.resolveOpacity(j, drawable.getOpacity()); 
      } 
      this.mOpacity = j;
      this.mCheckedOpacity = true;
      return j;
    }
    
    public final boolean isStateful() {
      boolean bool2;
      if (this.mCheckedStateful)
        return this.mIsStateful; 
      int i = this.mNumChildren;
      LayerDrawable.ChildDrawable[] arrayOfChildDrawable = this.mChildren;
      boolean bool1 = false;
      byte b = 0;
      while (true) {
        bool2 = bool1;
        if (b < i) {
          Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
          if (drawable != null && drawable.isStateful()) {
            bool2 = true;
            break;
          } 
          b++;
          continue;
        } 
        break;
      } 
      this.mIsStateful = bool2;
      this.mCheckedStateful = true;
      return bool2;
    }
    
    public final boolean hasFocusStateSpecified() {
      int i = this.mNumChildren;
      LayerDrawable.ChildDrawable[] arrayOfChildDrawable = this.mChildren;
      for (byte b = 0; b < i; b++) {
        Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
        if (drawable != null && drawable.hasFocusStateSpecified())
          return true; 
      } 
      return false;
    }
    
    public final boolean canConstantState() {
      LayerDrawable.ChildDrawable[] arrayOfChildDrawable = this.mChildren;
      int i = this.mNumChildren;
      for (byte b = 0; b < i; b++) {
        Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
        if (drawable != null && drawable.getConstantState() == null)
          return false; 
      } 
      return true;
    }
    
    void invalidateCache() {
      this.mCheckedOpacity = false;
      this.mCheckedStateful = false;
    }
  }
}
