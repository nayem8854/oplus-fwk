package android.graphics.drawable;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.SystemClock;
import android.util.AttributeSet;
import com.android.internal.R;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class AnimationDrawable extends DrawableContainer implements Runnable, Animatable {
  private boolean mAnimating;
  
  private AnimationState mAnimationState;
  
  private int mCurFrame = 0;
  
  private boolean mMutated;
  
  private boolean mRunning;
  
  public AnimationDrawable() {
    this((AnimationState)null, (Resources)null);
  }
  
  public boolean setVisible(boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_0
    //   1: iload_1
    //   2: iload_2
    //   3: invokespecial setVisible : (ZZ)Z
    //   6: istore_3
    //   7: iload_1
    //   8: ifeq -> 109
    //   11: iload_2
    //   12: ifne -> 19
    //   15: iload_3
    //   16: ifeq -> 114
    //   19: iconst_0
    //   20: istore #4
    //   22: iload_2
    //   23: ifne -> 74
    //   26: aload_0
    //   27: getfield mRunning : Z
    //   30: ifne -> 43
    //   33: aload_0
    //   34: getfield mAnimationState : Landroid/graphics/drawable/AnimationDrawable$AnimationState;
    //   37: invokestatic access$000 : (Landroid/graphics/drawable/AnimationDrawable$AnimationState;)Z
    //   40: ifeq -> 74
    //   43: aload_0
    //   44: getfield mCurFrame : I
    //   47: istore #5
    //   49: aload_0
    //   50: getfield mAnimationState : Landroid/graphics/drawable/AnimationDrawable$AnimationState;
    //   53: astore #6
    //   55: iload #5
    //   57: aload #6
    //   59: invokevirtual getChildCount : ()I
    //   62: if_icmplt -> 68
    //   65: goto -> 74
    //   68: iconst_0
    //   69: istore #5
    //   71: goto -> 77
    //   74: iconst_1
    //   75: istore #5
    //   77: iload #5
    //   79: ifeq -> 89
    //   82: iload #4
    //   84: istore #5
    //   86: goto -> 95
    //   89: aload_0
    //   90: getfield mCurFrame : I
    //   93: istore #5
    //   95: aload_0
    //   96: iload #5
    //   98: iconst_1
    //   99: aload_0
    //   100: getfield mAnimating : Z
    //   103: invokespecial setFrame : (IZZ)V
    //   106: goto -> 114
    //   109: aload_0
    //   110: aload_0
    //   111: invokevirtual unscheduleSelf : (Ljava/lang/Runnable;)V
    //   114: iload_3
    //   115: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #124	-> 0
    //   #125	-> 7
    //   #126	-> 11
    //   #127	-> 19
    //   #128	-> 55
    //   #129	-> 77
    //   #130	-> 106
    //   #132	-> 109
    //   #134	-> 114
  }
  
  public void start() {
    boolean bool = true;
    this.mAnimating = true;
    if (!isRunning()) {
      if (this.mAnimationState.getChildCount() <= 1) {
        AnimationState animationState = this.mAnimationState;
        if (animationState.mOneShot)
          bool = false; 
      } 
      setFrame(0, false, bool);
    } 
  }
  
  public void stop() {
    this.mAnimating = false;
    if (isRunning()) {
      this.mCurFrame = 0;
      unscheduleSelf(this);
    } 
  }
  
  public boolean isRunning() {
    return this.mRunning;
  }
  
  public void run() {
    nextFrame(false);
  }
  
  public void unscheduleSelf(Runnable paramRunnable) {
    this.mRunning = false;
    super.unscheduleSelf(paramRunnable);
  }
  
  public int getNumberOfFrames() {
    return this.mAnimationState.getChildCount();
  }
  
  public Drawable getFrame(int paramInt) {
    return this.mAnimationState.getChild(paramInt);
  }
  
  public int getDuration(int paramInt) {
    return this.mAnimationState.mDurations[paramInt];
  }
  
  public boolean isOneShot() {
    return this.mAnimationState.mOneShot;
  }
  
  public void setOneShot(boolean paramBoolean) {
    AnimationState.access$002(this.mAnimationState, paramBoolean);
  }
  
  public void addFrame(Drawable paramDrawable, int paramInt) {
    this.mAnimationState.addFrame(paramDrawable, paramInt);
    if (!this.mRunning)
      setFrame(0, true, false); 
  }
  
  private void nextFrame(boolean paramBoolean) {
    int i = this.mCurFrame;
    boolean bool = true;
    int j = i + 1;
    int k = this.mAnimationState.getChildCount();
    if (this.mAnimationState.mOneShot && j >= k - 1) {
      i = 1;
    } else {
      i = 0;
    } 
    int m = j;
    if (!this.mAnimationState.mOneShot) {
      m = j;
      if (j >= k)
        m = 0; 
    } 
    if (i != 0)
      bool = false; 
    setFrame(m, paramBoolean, bool);
  }
  
  private void setFrame(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    if (paramInt >= this.mAnimationState.getChildCount())
      return; 
    this.mAnimating = paramBoolean2;
    this.mCurFrame = paramInt;
    selectDrawable(paramInt);
    if (paramBoolean1 || paramBoolean2)
      unscheduleSelf(this); 
    if (paramBoolean2) {
      this.mCurFrame = paramInt;
      this.mRunning = true;
      scheduleSelf(this, SystemClock.uptimeMillis() + this.mAnimationState.mDurations[paramInt]);
    } 
  }
  
  public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme) throws XmlPullParserException, IOException {
    TypedArray typedArray = obtainAttributes(paramResources, paramTheme, paramAttributeSet, R.styleable.AnimationDrawable);
    inflateWithAttributes(paramResources, paramXmlPullParser, typedArray, 0);
    updateStateFromTypedArray(typedArray);
    updateDensity(paramResources);
    typedArray.recycle();
    inflateChildElements(paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
    setFrame(0, true, false);
  }
  
  private void inflateChildElements(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme) throws XmlPullParserException, IOException {
    int i = paramXmlPullParser.getDepth() + 1;
    while (true) {
      int j = paramXmlPullParser.next();
      if (j != 1) {
        int k = paramXmlPullParser.getDepth();
        if (k >= i || j != 3) {
          if (j != 2)
            continue; 
          if (k > i || !paramXmlPullParser.getName().equals("item"))
            continue; 
          TypedArray typedArray = obtainAttributes(paramResources, paramTheme, paramAttributeSet, R.styleable.AnimationDrawableItem);
          j = typedArray.getInt(0, -1);
          if (j >= 0) {
            Drawable drawable2 = typedArray.getDrawable(1);
            typedArray.recycle();
            Drawable drawable1 = drawable2;
            if (drawable2 == null) {
              while (true) {
                k = paramXmlPullParser.next();
                if (k == 4)
                  continue; 
                break;
              } 
              if (k == 2) {
                drawable1 = Drawable.createFromXmlInner(paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
              } else {
                StringBuilder stringBuilder1 = new StringBuilder();
                stringBuilder1.append(paramXmlPullParser.getPositionDescription());
                stringBuilder1.append(": <item> tag requires a 'drawable' attribute or child tag defining a drawable");
                throw new XmlPullParserException(stringBuilder1.toString());
              } 
            } 
            this.mAnimationState.addFrame(drawable1, j);
            if (drawable1 != null)
              drawable1.setCallback(this); 
            continue;
          } 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(paramXmlPullParser.getPositionDescription());
          stringBuilder.append(": <item> tag requires a 'duration' attribute");
          throw new XmlPullParserException(stringBuilder.toString());
        } 
      } 
      break;
    } 
  }
  
  private void updateStateFromTypedArray(TypedArray paramTypedArray) {
    AnimationState animationState = this.mAnimationState;
    animationState.mVariablePadding = paramTypedArray.getBoolean(1, animationState.mVariablePadding);
    animationState = this.mAnimationState;
    boolean bool = animationState.mOneShot;
    AnimationState.access$002(animationState, paramTypedArray.getBoolean(2, bool));
  }
  
  public Drawable mutate() {
    if (!this.mMutated && super.mutate() == this) {
      this.mAnimationState.mutate();
      this.mMutated = true;
    } 
    return this;
  }
  
  AnimationState cloneConstantState() {
    return new AnimationState(this.mAnimationState, this, null);
  }
  
  public void clearMutated() {
    super.clearMutated();
    this.mMutated = false;
  }
  
  class AnimationState extends DrawableContainer.DrawableContainerState {
    private int[] mDurations;
    
    private boolean mOneShot = false;
    
    AnimationState(AnimationDrawable this$0, AnimationDrawable param1AnimationDrawable, Resources param1Resources) {
      super((DrawableContainer.DrawableContainerState)this$0, param1AnimationDrawable, param1Resources);
      if (this$0 != null) {
        this.mDurations = ((AnimationState)this$0).mDurations;
        this.mOneShot = ((AnimationState)this$0).mOneShot;
      } else {
        this.mDurations = new int[getCapacity()];
        this.mOneShot = false;
      } 
    }
    
    private void mutate() {
      this.mDurations = (int[])this.mDurations.clone();
    }
    
    public Drawable newDrawable() {
      return new AnimationDrawable(this, null);
    }
    
    public Drawable newDrawable(Resources param1Resources) {
      return new AnimationDrawable(this, param1Resources);
    }
    
    public void addFrame(Drawable param1Drawable, int param1Int) {
      int i = addChild(param1Drawable);
      this.mDurations[i] = param1Int;
    }
    
    public void growArray(int param1Int1, int param1Int2) {
      super.growArray(param1Int1, param1Int2);
      int[] arrayOfInt = new int[param1Int2];
      System.arraycopy(this.mDurations, 0, arrayOfInt, 0, param1Int1);
      this.mDurations = arrayOfInt;
    }
  }
  
  protected void setConstantState(DrawableContainer.DrawableContainerState paramDrawableContainerState) {
    super.setConstantState(paramDrawableContainerState);
    if (paramDrawableContainerState instanceof AnimationState)
      this.mAnimationState = (AnimationState)paramDrawableContainerState; 
  }
  
  private AnimationDrawable(AnimationState paramAnimationState, Resources paramResources) {
    AnimationState animationState = new AnimationState(paramAnimationState, this, paramResources);
    setConstantState(animationState);
    if (paramAnimationState != null)
      setFrame(0, true, false); 
  }
}
