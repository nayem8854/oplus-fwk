package android.graphics.drawable;

import android.content.res.ColorStateList;
import android.content.res.ComplexColor;
import android.content.res.GradientColor;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Insets;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.Shader;
import android.os.Trace;
import android.util.ArrayMap;
import android.util.AttributeSet;
import android.util.FloatProperty;
import android.util.IntProperty;
import android.util.Log;
import android.util.PathParser;
import android.util.Property;
import android.util.Xml;
import com.android.internal.R;
import com.android.internal.util.VirtualRefBasePtr;
import dalvik.annotation.optimization.FastNative;
import dalvik.system.VMRuntime;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class VectorDrawable extends Drawable {
  private static final String LOGTAG = VectorDrawable.class.getSimpleName();
  
  private int mDpiScaledWidth = 0;
  
  private int mDpiScaledHeight = 0;
  
  private Insets mDpiScaledInsets = Insets.NONE;
  
  private boolean mDpiScaledDirty = true;
  
  private final Rect mTmpBounds = new Rect();
  
  private static final String SHAPE_CLIP_PATH = "clip-path";
  
  private static final String SHAPE_GROUP = "group";
  
  private static final String SHAPE_PATH = "path";
  
  private static final String SHAPE_VECTOR = "vector";
  
  private BlendModeColorFilter mBlendModeColorFilter;
  
  private ColorFilter mColorFilter;
  
  private boolean mMutated;
  
  private int mTargetDensity;
  
  private PorterDuffColorFilter mTintFilter;
  
  private VectorDrawableState mVectorState;
  
  public VectorDrawable() {
    this((VectorDrawableState)null, (Resources)null);
  }
  
  private VectorDrawable(VectorDrawableState paramVectorDrawableState, Resources paramResources) {
    this.mVectorState = new VectorDrawableState(paramVectorDrawableState);
    updateLocalState(paramResources);
  }
  
  private void updateLocalState(Resources paramResources) {
    int i = Drawable.resolveDensity(paramResources, this.mVectorState.mDensity);
    if (this.mTargetDensity != i) {
      this.mTargetDensity = i;
      this.mDpiScaledDirty = true;
    } 
    updateColorFilters(this.mVectorState.mBlendMode, this.mVectorState.mTint);
  }
  
  public Drawable mutate() {
    if (!this.mMutated && super.mutate() == this) {
      this.mVectorState = new VectorDrawableState(this.mVectorState);
      this.mMutated = true;
    } 
    return this;
  }
  
  public void clearMutated() {
    super.clearMutated();
    this.mMutated = false;
  }
  
  Object getTargetByName(String paramString) {
    return this.mVectorState.mVGTargetsMap.get(paramString);
  }
  
  public Drawable.ConstantState getConstantState() {
    this.mVectorState.mChangingConfigurations = getChangingConfigurations();
    return this.mVectorState;
  }
  
  public void draw(Canvas paramCanvas) {
    int j;
    copyBounds(this.mTmpBounds);
    if (this.mTmpBounds.width() <= 0 || this.mTmpBounds.height() <= 0)
      return; 
    ColorFilter colorFilter = this.mColorFilter;
    if (colorFilter == null)
      colorFilter = this.mBlendModeColorFilter; 
    long l1 = changePaintWhenVectorDraw(this, paramCanvas, colorFilter);
    boolean bool1 = this.mVectorState.canReuseCache();
    long l2 = this.mVectorState.getNativeRenderer(), l3 = paramCanvas.getNativeCanvasWrapper();
    Rect rect = this.mTmpBounds;
    boolean bool2 = needMirroring();
    int i = nDraw(l2, l3, l1, rect, bool2, bool1);
    resetPaintWhenVectorDraw(this, paramCanvas, colorFilter);
    if (i == 0)
      return; 
    if (paramCanvas.isHardwareAccelerated()) {
      j = (i - this.mVectorState.mLastHWCachePixelCount) * 4;
      this.mVectorState.mLastHWCachePixelCount = i;
    } else {
      j = (i - this.mVectorState.mLastSWCachePixelCount) * 4;
      this.mVectorState.mLastSWCachePixelCount = i;
    } 
    if (j > 0) {
      VMRuntime.getRuntime().registerNativeAllocation(j);
    } else if (j < 0) {
      VMRuntime.getRuntime().registerNativeFree(-j);
    } 
  }
  
  public int getAlpha() {
    return (int)(this.mVectorState.getAlpha() * 255.0F);
  }
  
  public void setAlpha(int paramInt) {
    if (this.mVectorState.setAlpha(paramInt / 255.0F))
      invalidateSelf(); 
  }
  
  public void setColorFilter(ColorFilter paramColorFilter) {
    this.mColorFilter = paramColorFilter;
    invalidateSelf();
  }
  
  public ColorFilter getColorFilter() {
    return this.mColorFilter;
  }
  
  public void setTintList(ColorStateList paramColorStateList) {
    VectorDrawableState vectorDrawableState = this.mVectorState;
    if (vectorDrawableState.mTint != paramColorStateList) {
      vectorDrawableState.mTint = paramColorStateList;
      updateColorFilters(this.mVectorState.mBlendMode, paramColorStateList);
      invalidateSelf();
    } 
  }
  
  public void setTintBlendMode(BlendMode paramBlendMode) {
    VectorDrawableState vectorDrawableState = this.mVectorState;
    if (vectorDrawableState.mBlendMode != paramBlendMode) {
      vectorDrawableState.mBlendMode = paramBlendMode;
      updateColorFilters(vectorDrawableState.mBlendMode, vectorDrawableState.mTint);
      invalidateSelf();
    } 
  }
  
  public boolean isStateful() {
    if (!super.isStateful()) {
      VectorDrawableState vectorDrawableState = this.mVectorState;
      return (vectorDrawableState != null && vectorDrawableState.isStateful());
    } 
    return true;
  }
  
  public boolean hasFocusStateSpecified() {
    boolean bool;
    VectorDrawableState vectorDrawableState = this.mVectorState;
    if (vectorDrawableState != null && vectorDrawableState.hasFocusStateSpecified()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected boolean onStateChange(int[] paramArrayOfint) {
    boolean bool1 = false;
    if (isStateful())
      mutate(); 
    VectorDrawableState vectorDrawableState = this.mVectorState;
    if (vectorDrawableState.onStateChange(paramArrayOfint)) {
      bool1 = true;
      vectorDrawableState.mCacheDirty = true;
    } 
    boolean bool2 = bool1;
    if (vectorDrawableState.mTint != null) {
      bool2 = bool1;
      if (vectorDrawableState.mBlendMode != null) {
        BlendMode blendMode = vectorDrawableState.mBlendMode;
        ColorStateList colorStateList = vectorDrawableState.mTint;
        updateColorFilters(blendMode, colorStateList);
        bool2 = true;
      } 
    } 
    return bool2;
  }
  
  private void updateColorFilters(BlendMode paramBlendMode, ColorStateList paramColorStateList) {
    PorterDuff.Mode mode = BlendMode.blendModeToPorterDuffMode(paramBlendMode);
    this.mTintFilter = updateTintFilter(this.mTintFilter, paramColorStateList, mode);
    this.mBlendModeColorFilter = updateBlendModeFilter(this.mBlendModeColorFilter, paramColorStateList, paramBlendMode);
  }
  
  public int getOpacity() {
    byte b;
    if (getAlpha() == 0) {
      b = -2;
    } else {
      b = -3;
    } 
    return b;
  }
  
  public int getIntrinsicWidth() {
    if (this.mDpiScaledDirty)
      computeVectorSize(); 
    return this.mDpiScaledWidth;
  }
  
  public int getIntrinsicHeight() {
    if (this.mDpiScaledDirty)
      computeVectorSize(); 
    return this.mDpiScaledHeight;
  }
  
  public Insets getOpticalInsets() {
    if (this.mDpiScaledDirty)
      computeVectorSize(); 
    return this.mDpiScaledInsets;
  }
  
  void computeVectorSize() {
    Insets insets = this.mVectorState.mOpticalInsets;
    int i = this.mVectorState.mDensity;
    int j = this.mTargetDensity;
    if (j != i) {
      this.mDpiScaledWidth = Drawable.scaleFromDensity(this.mVectorState.mBaseWidth, i, j, true);
      this.mDpiScaledHeight = Drawable.scaleFromDensity(this.mVectorState.mBaseHeight, i, j, true);
      int k = Drawable.scaleFromDensity(insets.left, i, j, false);
      int m = Drawable.scaleFromDensity(insets.right, i, j, false);
      int n = Drawable.scaleFromDensity(insets.top, i, j, false);
      i = Drawable.scaleFromDensity(insets.bottom, i, j, false);
      this.mDpiScaledInsets = Insets.of(k, n, m, i);
    } else {
      this.mDpiScaledWidth = this.mVectorState.mBaseWidth;
      this.mDpiScaledHeight = this.mVectorState.mBaseHeight;
      this.mDpiScaledInsets = insets;
    } 
    this.mDpiScaledDirty = false;
  }
  
  public boolean canApplyTheme() {
    boolean bool;
    VectorDrawableState vectorDrawableState = this.mVectorState;
    if ((vectorDrawableState != null && vectorDrawableState.canApplyTheme()) || super.canApplyTheme()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void applyTheme(Resources.Theme paramTheme) {
    RuntimeException runtimeException;
    super.applyTheme(paramTheme);
    VectorDrawableState vectorDrawableState1 = this.mVectorState;
    if (vectorDrawableState1 == null)
      return; 
    VectorDrawableState vectorDrawableState2 = this.mVectorState;
    int i = Drawable.resolveDensity(paramTheme.getResources(), 0);
    boolean bool = vectorDrawableState2.setDensity(i);
    this.mDpiScaledDirty |= bool;
    if (vectorDrawableState1.mThemeAttrs != null) {
      TypedArray typedArray = paramTheme.resolveAttributes(vectorDrawableState1.mThemeAttrs, R.styleable.VectorDrawable);
      try {
        vectorDrawableState1.mCacheDirty = true;
        updateStateFromTypedArray(typedArray);
        typedArray.recycle();
        this.mDpiScaledDirty = true;
      } catch (XmlPullParserException xmlPullParserException) {
        runtimeException = new RuntimeException();
        this((Throwable)xmlPullParserException);
        throw runtimeException;
      } finally {}
    } 
    if (((VectorDrawableState)runtimeException).mTint != null && ((VectorDrawableState)runtimeException).mTint.canApplyTheme())
      ((VectorDrawableState)runtimeException).mTint = ((VectorDrawableState)runtimeException).mTint.obtainForTheme(paramTheme); 
    vectorDrawableState2 = this.mVectorState;
    if (vectorDrawableState2 != null && vectorDrawableState2.canApplyTheme())
      this.mVectorState.applyTheme(paramTheme); 
    updateLocalState(paramTheme.getResources());
  }
  
  public float getPixelSize() {
    VectorDrawableState vectorDrawableState = this.mVectorState;
    if (vectorDrawableState == null || vectorDrawableState.mBaseWidth == 0 || this.mVectorState.mBaseHeight == 0 || this.mVectorState.mViewportHeight == 0.0F || this.mVectorState.mViewportWidth == 0.0F)
      return 1.0F; 
    float f1 = this.mVectorState.mBaseWidth;
    float f2 = this.mVectorState.mBaseHeight;
    float f3 = this.mVectorState.mViewportWidth;
    float f4 = this.mVectorState.mViewportHeight;
    f1 = f3 / f1;
    f2 = f4 / f2;
    return Math.min(f1, f2);
  }
  
  public static VectorDrawable create(Resources paramResources, int paramInt) {
    try {
      XmlResourceParser xmlResourceParser = paramResources.getXml(paramInt);
      AttributeSet attributeSet = Xml.asAttributeSet(xmlResourceParser);
      while (true) {
        paramInt = xmlResourceParser.next();
        if (paramInt != 2 && paramInt != 1)
          continue; 
        break;
      } 
      if (paramInt == 2) {
        VectorDrawable vectorDrawable = new VectorDrawable();
        this();
        vectorDrawable.inflate(paramResources, xmlResourceParser, attributeSet);
        return vectorDrawable;
      } 
      XmlPullParserException xmlPullParserException = new XmlPullParserException();
      this("No start tag found");
      throw xmlPullParserException;
    } catch (XmlPullParserException xmlPullParserException) {
      Log.e(LOGTAG, "parser error", (Throwable)xmlPullParserException);
    } catch (IOException iOException) {
      Log.e(LOGTAG, "parser error", iOException);
    } 
    return null;
  }
  
  public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme) throws XmlPullParserException, IOException {
    try {
      Trace.traceBegin(8192L, "VectorDrawable#inflate");
      if (this.mVectorState.mRootGroup != null || this.mVectorState.mNativeTree != null) {
        if (this.mVectorState.mRootGroup != null) {
          VMRuntime vMRuntime = VMRuntime.getRuntime();
          VGroup vGroup1 = this.mVectorState.mRootGroup;
          int i = vGroup1.getNativeSize();
          vMRuntime.registerNativeFree(i);
          this.mVectorState.mRootGroup.setTree(null);
        } 
        VectorDrawableState vectorDrawableState1 = this.mVectorState;
        VGroup vGroup = new VGroup();
        this();
        vectorDrawableState1.mRootGroup = vGroup;
        if (this.mVectorState.mNativeTree != null) {
          VMRuntime.getRuntime().registerNativeFree(316);
          this.mVectorState.mNativeTree.release();
        } 
        this.mVectorState.createNativeTree(this.mVectorState.mRootGroup);
      } 
      VectorDrawableState vectorDrawableState = this.mVectorState;
      vectorDrawableState.setDensity(Drawable.resolveDensity(paramResources, 0));
      TypedArray typedArray = obtainAttributes(paramResources, paramTheme, paramAttributeSet, R.styleable.VectorDrawable);
      updateStateFromTypedArray(typedArray);
      typedArray.recycle();
      this.mDpiScaledDirty = true;
      vectorDrawableState.mCacheDirty = true;
      inflateChildElements(paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
      vectorDrawableState.onTreeConstructionFinished();
      updateLocalState(paramResources);
      return;
    } finally {
      Trace.traceEnd(8192L);
    } 
  }
  
  private void updateStateFromTypedArray(TypedArray paramTypedArray) throws XmlPullParserException {
    String str;
    VectorDrawableState vectorDrawableState = this.mVectorState;
    vectorDrawableState.mChangingConfigurations |= paramTypedArray.getChangingConfigurations();
    vectorDrawableState.mThemeAttrs = paramTypedArray.extractThemeAttrs();
    int i = paramTypedArray.getInt(6, -1);
    if (i != -1)
      vectorDrawableState.mBlendMode = Drawable.parseBlendMode(i, BlendMode.SRC_IN); 
    ColorStateList colorStateList = paramTypedArray.getColorStateList(1);
    if (colorStateList != null)
      vectorDrawableState.mTint = colorStateList; 
    vectorDrawableState.mAutoMirrored = paramTypedArray.getBoolean(5, vectorDrawableState.mAutoMirrored);
    float f1 = paramTypedArray.getFloat(7, vectorDrawableState.mViewportWidth);
    float f2 = paramTypedArray.getFloat(8, vectorDrawableState.mViewportHeight);
    vectorDrawableState.setViewportSize(f1, f2);
    if (vectorDrawableState.mViewportWidth > 0.0F) {
      if (vectorDrawableState.mViewportHeight > 0.0F) {
        vectorDrawableState.mBaseWidth = paramTypedArray.getDimensionPixelSize(3, vectorDrawableState.mBaseWidth);
        vectorDrawableState.mBaseHeight = paramTypedArray.getDimensionPixelSize(2, vectorDrawableState.mBaseHeight);
        if (vectorDrawableState.mBaseWidth > 0) {
          if (vectorDrawableState.mBaseHeight > 0) {
            i = paramTypedArray.getDimensionPixelOffset(9, vectorDrawableState.mOpticalInsets.left);
            int j = paramTypedArray.getDimensionPixelOffset(10, vectorDrawableState.mOpticalInsets.top);
            int k = paramTypedArray.getDimensionPixelOffset(11, vectorDrawableState.mOpticalInsets.right);
            int m = paramTypedArray.getDimensionPixelOffset(12, vectorDrawableState.mOpticalInsets.bottom);
            vectorDrawableState.mOpticalInsets = Insets.of(i, j, k, m);
            f2 = vectorDrawableState.getAlpha();
            f2 = paramTypedArray.getFloat(4, f2);
            vectorDrawableState.setAlpha(f2);
            str = paramTypedArray.getString(0);
            if (str != null) {
              vectorDrawableState.mRootName = str;
              vectorDrawableState.mVGTargetsMap.put(str, vectorDrawableState);
            } 
            return;
          } 
          StringBuilder stringBuilder3 = new StringBuilder();
          stringBuilder3.append(str.getPositionDescription());
          stringBuilder3.append("<vector> tag requires height > 0");
          throw new XmlPullParserException(stringBuilder3.toString());
        } 
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append(str.getPositionDescription());
        stringBuilder2.append("<vector> tag requires width > 0");
        throw new XmlPullParserException(stringBuilder2.toString());
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(str.getPositionDescription());
      stringBuilder1.append("<vector> tag requires viewportHeight > 0");
      throw new XmlPullParserException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str.getPositionDescription());
    stringBuilder.append("<vector> tag requires viewportWidth > 0");
    throw new XmlPullParserException(stringBuilder.toString());
  }
  
  private void inflateChildElements(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: aload_0
    //   1: getfield mVectorState : Landroid/graphics/drawable/VectorDrawable$VectorDrawableState;
    //   4: astore #5
    //   6: iconst_1
    //   7: istore #6
    //   9: new java/util/Stack
    //   12: dup
    //   13: invokespecial <init> : ()V
    //   16: astore #7
    //   18: aload #7
    //   20: aload #5
    //   22: getfield mRootGroup : Landroid/graphics/drawable/VectorDrawable$VGroup;
    //   25: invokevirtual push : (Ljava/lang/Object;)Ljava/lang/Object;
    //   28: pop
    //   29: aload_2
    //   30: invokeinterface getEventType : ()I
    //   35: istore #8
    //   37: aload_2
    //   38: invokeinterface getDepth : ()I
    //   43: istore #9
    //   45: iload #8
    //   47: iconst_1
    //   48: if_icmpeq -> 407
    //   51: aload_2
    //   52: invokeinterface getDepth : ()I
    //   57: iload #9
    //   59: iconst_1
    //   60: iadd
    //   61: if_icmpge -> 70
    //   64: iload #8
    //   66: iconst_3
    //   67: if_icmpeq -> 407
    //   70: iload #8
    //   72: iconst_2
    //   73: if_icmpne -> 350
    //   76: aload_2
    //   77: invokeinterface getName : ()Ljava/lang/String;
    //   82: astore #10
    //   84: aload #7
    //   86: invokevirtual peek : ()Ljava/lang/Object;
    //   89: checkcast android/graphics/drawable/VectorDrawable$VGroup
    //   92: astore #11
    //   94: ldc 'path'
    //   96: aload #10
    //   98: invokevirtual equals : (Ljava/lang/Object;)Z
    //   101: ifeq -> 175
    //   104: new android/graphics/drawable/VectorDrawable$VFullPath
    //   107: dup
    //   108: invokespecial <init> : ()V
    //   111: astore #10
    //   113: aload #10
    //   115: aload_1
    //   116: aload_3
    //   117: aload #4
    //   119: invokevirtual inflate : (Landroid/content/res/Resources;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
    //   122: aload #11
    //   124: aload #10
    //   126: invokevirtual addChild : (Landroid/graphics/drawable/VectorDrawable$VObject;)V
    //   129: aload #10
    //   131: invokevirtual getPathName : ()Ljava/lang/String;
    //   134: ifnull -> 153
    //   137: aload #5
    //   139: getfield mVGTargetsMap : Landroid/util/ArrayMap;
    //   142: aload #10
    //   144: invokevirtual getPathName : ()Ljava/lang/String;
    //   147: aload #10
    //   149: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   152: pop
    //   153: iconst_0
    //   154: istore #12
    //   156: aload #5
    //   158: aload #5
    //   160: getfield mChangingConfigurations : I
    //   163: aload #10
    //   165: getfield mChangingConfigurations : I
    //   168: ior
    //   169: putfield mChangingConfigurations : I
    //   172: goto -> 347
    //   175: ldc 'clip-path'
    //   177: aload #10
    //   179: invokevirtual equals : (Ljava/lang/Object;)Z
    //   182: ifeq -> 253
    //   185: new android/graphics/drawable/VectorDrawable$VClipPath
    //   188: dup
    //   189: invokespecial <init> : ()V
    //   192: astore #10
    //   194: aload #10
    //   196: aload_1
    //   197: aload_3
    //   198: aload #4
    //   200: invokevirtual inflate : (Landroid/content/res/Resources;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
    //   203: aload #11
    //   205: aload #10
    //   207: invokevirtual addChild : (Landroid/graphics/drawable/VectorDrawable$VObject;)V
    //   210: aload #10
    //   212: invokevirtual getPathName : ()Ljava/lang/String;
    //   215: ifnull -> 234
    //   218: aload #5
    //   220: getfield mVGTargetsMap : Landroid/util/ArrayMap;
    //   223: aload #10
    //   225: invokevirtual getPathName : ()Ljava/lang/String;
    //   228: aload #10
    //   230: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   233: pop
    //   234: aload #5
    //   236: aload #5
    //   238: getfield mChangingConfigurations : I
    //   241: aload #10
    //   243: getfield mChangingConfigurations : I
    //   246: ior
    //   247: putfield mChangingConfigurations : I
    //   250: goto -> 343
    //   253: ldc 'group'
    //   255: aload #10
    //   257: invokevirtual equals : (Ljava/lang/Object;)Z
    //   260: ifeq -> 343
    //   263: new android/graphics/drawable/VectorDrawable$VGroup
    //   266: dup
    //   267: invokespecial <init> : ()V
    //   270: astore #10
    //   272: aload #10
    //   274: aload_1
    //   275: aload_3
    //   276: aload #4
    //   278: invokevirtual inflate : (Landroid/content/res/Resources;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
    //   281: aload #11
    //   283: aload #10
    //   285: invokevirtual addChild : (Landroid/graphics/drawable/VectorDrawable$VObject;)V
    //   288: aload #7
    //   290: aload #10
    //   292: invokevirtual push : (Ljava/lang/Object;)Ljava/lang/Object;
    //   295: pop
    //   296: aload #10
    //   298: invokevirtual getGroupName : ()Ljava/lang/String;
    //   301: ifnull -> 320
    //   304: aload #5
    //   306: getfield mVGTargetsMap : Landroid/util/ArrayMap;
    //   309: aload #10
    //   311: invokevirtual getGroupName : ()Ljava/lang/String;
    //   314: aload #10
    //   316: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   319: pop
    //   320: aload #5
    //   322: aload #5
    //   324: getfield mChangingConfigurations : I
    //   327: aload #10
    //   329: invokestatic access$100 : (Landroid/graphics/drawable/VectorDrawable$VGroup;)I
    //   332: ior
    //   333: putfield mChangingConfigurations : I
    //   336: iload #6
    //   338: istore #12
    //   340: goto -> 347
    //   343: iload #6
    //   345: istore #12
    //   347: goto -> 392
    //   350: iload #6
    //   352: istore #12
    //   354: iload #8
    //   356: iconst_3
    //   357: if_icmpne -> 347
    //   360: aload_2
    //   361: invokeinterface getName : ()Ljava/lang/String;
    //   366: astore #11
    //   368: iload #6
    //   370: istore #12
    //   372: ldc 'group'
    //   374: aload #11
    //   376: invokevirtual equals : (Ljava/lang/Object;)Z
    //   379: ifeq -> 392
    //   382: aload #7
    //   384: invokevirtual pop : ()Ljava/lang/Object;
    //   387: pop
    //   388: iload #6
    //   390: istore #12
    //   392: aload_2
    //   393: invokeinterface next : ()I
    //   398: istore #8
    //   400: iload #12
    //   402: istore #6
    //   404: goto -> 45
    //   407: iload #6
    //   409: ifeq -> 484
    //   412: new java/lang/StringBuffer
    //   415: dup
    //   416: invokespecial <init> : ()V
    //   419: astore_2
    //   420: aload_2
    //   421: invokevirtual length : ()I
    //   424: ifle -> 435
    //   427: aload_2
    //   428: ldc_w ' or '
    //   431: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   434: pop
    //   435: aload_2
    //   436: ldc 'path'
    //   438: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   441: pop
    //   442: new java/lang/StringBuilder
    //   445: dup
    //   446: invokespecial <init> : ()V
    //   449: astore_1
    //   450: aload_1
    //   451: ldc_w 'no '
    //   454: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   457: pop
    //   458: aload_1
    //   459: aload_2
    //   460: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   463: pop
    //   464: aload_1
    //   465: ldc_w ' defined'
    //   468: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   471: pop
    //   472: new org/xmlpull/v1/XmlPullParserException
    //   475: dup
    //   476: aload_1
    //   477: invokevirtual toString : ()Ljava/lang/String;
    //   480: invokespecial <init> : (Ljava/lang/String;)V
    //   483: athrow
    //   484: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #828	-> 0
    //   #829	-> 6
    //   #833	-> 9
    //   #834	-> 18
    //   #836	-> 29
    //   #837	-> 37
    //   #840	-> 45
    //   #841	-> 51
    //   #842	-> 70
    //   #843	-> 76
    //   #844	-> 84
    //   #846	-> 94
    //   #847	-> 104
    //   #848	-> 113
    //   #849	-> 122
    //   #850	-> 129
    //   #851	-> 137
    //   #853	-> 153
    //   #854	-> 156
    //   #855	-> 172
    //   #856	-> 185
    //   #857	-> 194
    //   #858	-> 203
    //   #859	-> 210
    //   #860	-> 218
    //   #862	-> 234
    //   #863	-> 253
    //   #864	-> 263
    //   #865	-> 272
    //   #866	-> 281
    //   #867	-> 288
    //   #868	-> 296
    //   #869	-> 304
    //   #872	-> 320
    //   #863	-> 343
    //   #874	-> 347
    //   #875	-> 360
    //   #876	-> 368
    //   #877	-> 382
    //   #880	-> 392
    //   #883	-> 407
    //   #884	-> 412
    //   #886	-> 420
    //   #887	-> 427
    //   #889	-> 435
    //   #891	-> 442
    //   #893	-> 484
  }
  
  public int getChangingConfigurations() {
    return super.getChangingConfigurations() | this.mVectorState.getChangingConfigurations();
  }
  
  void setAllowCaching(boolean paramBoolean) {
    nSetAllowCaching(this.mVectorState.getNativeRenderer(), paramBoolean);
  }
  
  private boolean needMirroring() {
    boolean bool = isAutoMirrored();
    boolean bool1 = true;
    if (!bool || getLayoutDirection() != 1)
      bool1 = false; 
    return bool1;
  }
  
  public void setAutoMirrored(boolean paramBoolean) {
    if (this.mVectorState.mAutoMirrored != paramBoolean) {
      this.mVectorState.mAutoMirrored = paramBoolean;
      invalidateSelf();
    } 
  }
  
  public boolean isAutoMirrored() {
    return this.mVectorState.mAutoMirrored;
  }
  
  public long getNativeTree() {
    return this.mVectorState.getNativeRenderer();
  }
  
  public void setAntiAlias(boolean paramBoolean) {
    nSetAntiAlias(this.mVectorState.mNativeTree.get(), paramBoolean);
  }
  
  class VectorDrawableState extends Drawable.ConstantState {
    ColorStateList mTint = null;
    
    BlendMode mBlendMode = Drawable.DEFAULT_BLEND_MODE;
    
    int mBaseWidth = 0;
    
    int mBaseHeight = 0;
    
    float mViewportWidth = 0.0F;
    
    float mViewportHeight = 0.0F;
    
    Insets mOpticalInsets = Insets.NONE;
    
    String mRootName = null;
    
    VirtualRefBasePtr mNativeTree = null;
    
    int mDensity = 160;
    
    final ArrayMap<String, Object> mVGTargetsMap = new ArrayMap();
    
    int mLastSWCachePixelCount = 0;
    
    int mLastHWCachePixelCount = 0;
    
    static final Property<VectorDrawableState, Float> ALPHA = (Property)new FloatProperty<VectorDrawableState>("alpha") {
        FloatProperty(VectorDrawable this$0) {
          super((String)this$0);
        }
        
        public void setValue(VectorDrawable.VectorDrawableState param1VectorDrawableState, float param1Float) {
          param1VectorDrawableState.setAlpha(param1Float);
        }
        
        public Float get(VectorDrawable.VectorDrawableState param1VectorDrawableState) {
          return Float.valueOf(param1VectorDrawableState.getAlpha());
        }
      };
    
    Property getProperty(String param1String) {
      if (ALPHA.getName().equals(param1String))
        return ALPHA; 
      return null;
    }
    
    private int mAllocationOfAllNodes = 0;
    
    private static final int NATIVE_ALLOCATION_SIZE = 316;
    
    boolean mAutoMirrored;
    
    boolean mCacheDirty;
    
    boolean mCachedAutoMirrored;
    
    BlendMode mCachedBlendMode;
    
    int[] mCachedThemeAttrs;
    
    ColorStateList mCachedTint;
    
    int mChangingConfigurations;
    
    VectorDrawable.VGroup mRootGroup;
    
    int[] mThemeAttrs;
    
    public VectorDrawableState(VectorDrawable this$0) {
      if (this$0 != null) {
        this.mThemeAttrs = ((VectorDrawableState)this$0).mThemeAttrs;
        this.mChangingConfigurations = ((VectorDrawableState)this$0).mChangingConfigurations;
        this.mTint = ((VectorDrawableState)this$0).mTint;
        this.mBlendMode = ((VectorDrawableState)this$0).mBlendMode;
        this.mAutoMirrored = ((VectorDrawableState)this$0).mAutoMirrored;
        VectorDrawable.VGroup vGroup = new VectorDrawable.VGroup(((VectorDrawableState)this$0).mRootGroup, this.mVGTargetsMap);
        createNativeTreeFromCopy((VectorDrawableState)this$0, vGroup);
        this.mBaseWidth = ((VectorDrawableState)this$0).mBaseWidth;
        this.mBaseHeight = ((VectorDrawableState)this$0).mBaseHeight;
        setViewportSize(((VectorDrawableState)this$0).mViewportWidth, ((VectorDrawableState)this$0).mViewportHeight);
        this.mOpticalInsets = ((VectorDrawableState)this$0).mOpticalInsets;
        this.mRootName = ((VectorDrawableState)this$0).mRootName;
        this.mDensity = ((VectorDrawableState)this$0).mDensity;
        String str = ((VectorDrawableState)this$0).mRootName;
        if (str != null)
          this.mVGTargetsMap.put(str, this); 
      } else {
        VectorDrawable.VGroup vGroup = new VectorDrawable.VGroup();
        createNativeTree(vGroup);
      } 
      onTreeConstructionFinished();
    }
    
    private void createNativeTree(VectorDrawable.VGroup param1VGroup) {
      this.mNativeTree = new VirtualRefBasePtr(VectorDrawable.nCreateTree(param1VGroup.mNativePtr));
      VMRuntime.getRuntime().registerNativeAllocation(316);
    }
    
    private void createNativeTreeFromCopy(VectorDrawableState param1VectorDrawableState, VectorDrawable.VGroup param1VGroup) {
      VirtualRefBasePtr virtualRefBasePtr = param1VectorDrawableState.mNativeTree;
      long l1 = virtualRefBasePtr.get(), l2 = param1VGroup.mNativePtr;
      this.mNativeTree = new VirtualRefBasePtr(VectorDrawable.nCreateTreeFromCopy(l1, l2));
      VMRuntime.getRuntime().registerNativeAllocation(316);
    }
    
    void onTreeConstructionFinished() {
      this.mRootGroup.setTree(this.mNativeTree);
      this.mAllocationOfAllNodes = this.mRootGroup.getNativeSize();
      VMRuntime.getRuntime().registerNativeAllocation(this.mAllocationOfAllNodes);
    }
    
    long getNativeRenderer() {
      VirtualRefBasePtr virtualRefBasePtr = this.mNativeTree;
      if (virtualRefBasePtr == null)
        return 0L; 
      return virtualRefBasePtr.get();
    }
    
    public boolean canReuseCache() {
      if (!this.mCacheDirty && this.mCachedThemeAttrs == this.mThemeAttrs && this.mCachedTint == this.mTint && this.mCachedBlendMode == this.mBlendMode && this.mCachedAutoMirrored == this.mAutoMirrored)
        return true; 
      updateCacheStates();
      return false;
    }
    
    public void updateCacheStates() {
      this.mCachedThemeAttrs = this.mThemeAttrs;
      this.mCachedTint = this.mTint;
      this.mCachedBlendMode = this.mBlendMode;
      this.mCachedAutoMirrored = this.mAutoMirrored;
      this.mCacheDirty = false;
    }
    
    public void applyTheme(Resources.Theme param1Theme) {
      this.mRootGroup.applyTheme(param1Theme);
    }
    
    public boolean canApplyTheme() {
      if (this.mThemeAttrs == null) {
        VectorDrawable.VGroup vGroup = this.mRootGroup;
        if (vGroup == null || 
          !vGroup.canApplyTheme()) {
          ColorStateList colorStateList = this.mTint;
          return ((colorStateList != null && 
            colorStateList.canApplyTheme()) || 
            super.canApplyTheme());
        } 
      } 
      return true;
    }
    
    public Drawable newDrawable() {
      return new VectorDrawable(this, null);
    }
    
    public Drawable newDrawable(Resources param1Resources) {
      return new VectorDrawable(this, param1Resources);
    }
    
    public int getChangingConfigurations() {
      byte b;
      int i = this.mChangingConfigurations;
      ColorStateList colorStateList = this.mTint;
      if (colorStateList != null) {
        b = colorStateList.getChangingConfigurations();
      } else {
        b = 0;
      } 
      return i | b;
    }
    
    public boolean isStateful() {
      ColorStateList colorStateList = this.mTint;
      if (colorStateList == null || !colorStateList.isStateful()) {
        VectorDrawable.VGroup vGroup = this.mRootGroup;
        return (vGroup != null && 
          vGroup.isStateful());
      } 
      return true;
    }
    
    public boolean hasFocusStateSpecified() {
      ColorStateList colorStateList = this.mTint;
      if (colorStateList == null || !colorStateList.hasFocusStateSpecified()) {
        VectorDrawable.VGroup vGroup = this.mRootGroup;
        return (vGroup != null && 
          vGroup.hasFocusStateSpecified());
      } 
      return true;
    }
    
    void setViewportSize(float param1Float1, float param1Float2) {
      this.mViewportWidth = param1Float1;
      this.mViewportHeight = param1Float2;
      VectorDrawable.nSetRendererViewportSize(getNativeRenderer(), param1Float1, param1Float2);
    }
    
    public final boolean setDensity(int param1Int) {
      if (this.mDensity != param1Int) {
        int i = this.mDensity;
        this.mDensity = param1Int;
        applyDensityScaling(i, param1Int);
        return true;
      } 
      return false;
    }
    
    private void applyDensityScaling(int param1Int1, int param1Int2) {
      this.mBaseWidth = Drawable.scaleFromDensity(this.mBaseWidth, param1Int1, param1Int2, true);
      this.mBaseHeight = Drawable.scaleFromDensity(this.mBaseHeight, param1Int1, param1Int2, true);
      int i = Drawable.scaleFromDensity(this.mOpticalInsets.left, param1Int1, param1Int2, false);
      int j = Drawable.scaleFromDensity(this.mOpticalInsets.top, param1Int1, param1Int2, false);
      int k = Drawable.scaleFromDensity(this.mOpticalInsets.right, param1Int1, param1Int2, false);
      param1Int1 = Drawable.scaleFromDensity(this.mOpticalInsets.bottom, param1Int1, param1Int2, false);
      this.mOpticalInsets = Insets.of(i, j, k, param1Int1);
    }
    
    public boolean onStateChange(int[] param1ArrayOfint) {
      return this.mRootGroup.onStateChange(param1ArrayOfint);
    }
    
    public void finalize() throws Throwable {
      super.finalize();
      int i = this.mLastHWCachePixelCount, j = this.mLastSWCachePixelCount;
      VMRuntime.getRuntime().registerNativeFree(this.mAllocationOfAllNodes + 316 + i * 4 + j * 4);
    }
    
    public boolean setAlpha(float param1Float) {
      return VectorDrawable.nSetRootAlpha(this.mNativeTree.get(), param1Float);
    }
    
    public float getAlpha() {
      return VectorDrawable.nGetRootAlpha(this.mNativeTree.get());
    }
  }
  
  class null extends FloatProperty<VectorDrawableState> {
    null(VectorDrawable this$0) {
      super((String)this$0);
    }
    
    public void setValue(VectorDrawable.VectorDrawableState param1VectorDrawableState, float param1Float) {
      param1VectorDrawableState.setAlpha(param1Float);
    }
    
    public Float get(VectorDrawable.VectorDrawableState param1VectorDrawableState) {
      return Float.valueOf(param1VectorDrawableState.getAlpha());
    }
  }
  
  static class VGroup extends VObject {
    private static final int NATIVE_ALLOCATION_SIZE = 100;
    
    private static final Property<VGroup, Float> PIVOT_X;
    
    private static final int PIVOT_X_INDEX = 1;
    
    private static final Property<VGroup, Float> PIVOT_Y;
    
    private static final int PIVOT_Y_INDEX = 2;
    
    private static final Property<VGroup, Float> ROTATION;
    
    private static final int ROTATION_INDEX = 0;
    
    private static final Property<VGroup, Float> SCALE_X;
    
    private static final int SCALE_X_INDEX = 3;
    
    private static final Property<VGroup, Float> SCALE_Y;
    
    private static final int SCALE_Y_INDEX = 4;
    
    private static final int TRANSFORM_PROPERTY_COUNT = 7;
    
    private static final Property<VGroup, Float> TRANSLATE_X = (Property)new FloatProperty<VGroup>("translateX") {
        public void setValue(VectorDrawable.VGroup param2VGroup, float param2Float) {
          param2VGroup.setTranslateX(param2Float);
        }
        
        public Float get(VectorDrawable.VGroup param2VGroup) {
          return Float.valueOf(param2VGroup.getTranslateX());
        }
      };
    
    private static final int TRANSLATE_X_INDEX = 5;
    
    private static final Property<VGroup, Float> TRANSLATE_Y = (Property)new FloatProperty<VGroup>("translateY") {
        public void setValue(VectorDrawable.VGroup param2VGroup, float param2Float) {
          param2VGroup.setTranslateY(param2Float);
        }
        
        public Float get(VectorDrawable.VGroup param2VGroup) {
          return Float.valueOf(param2VGroup.getTranslateY());
        }
      };
    
    private static final int TRANSLATE_Y_INDEX = 6;
    
    private static final HashMap<String, Integer> sPropertyIndexMap = (HashMap<String, Integer>)new Object();
    
    private static final HashMap<String, Property> sPropertyMap;
    
    private int mChangingConfigurations;
    
    static int getPropertyIndex(String param1String) {
      if (sPropertyIndexMap.containsKey(param1String))
        return ((Integer)sPropertyIndexMap.get(param1String)).intValue(); 
      return -1;
    }
    
    static {
      SCALE_X = (Property)new FloatProperty<VGroup>("scaleX") {
          public void setValue(VectorDrawable.VGroup param2VGroup, float param2Float) {
            param2VGroup.setScaleX(param2Float);
          }
          
          public Float get(VectorDrawable.VGroup param2VGroup) {
            return Float.valueOf(param2VGroup.getScaleX());
          }
        };
      SCALE_Y = (Property)new FloatProperty<VGroup>("scaleY") {
          public void setValue(VectorDrawable.VGroup param2VGroup, float param2Float) {
            param2VGroup.setScaleY(param2Float);
          }
          
          public Float get(VectorDrawable.VGroup param2VGroup) {
            return Float.valueOf(param2VGroup.getScaleY());
          }
        };
      PIVOT_X = (Property)new FloatProperty<VGroup>("pivotX") {
          public void setValue(VectorDrawable.VGroup param2VGroup, float param2Float) {
            param2VGroup.setPivotX(param2Float);
          }
          
          public Float get(VectorDrawable.VGroup param2VGroup) {
            return Float.valueOf(param2VGroup.getPivotX());
          }
        };
      PIVOT_Y = (Property)new FloatProperty<VGroup>("pivotY") {
          public void setValue(VectorDrawable.VGroup param2VGroup, float param2Float) {
            param2VGroup.setPivotY(param2Float);
          }
          
          public Float get(VectorDrawable.VGroup param2VGroup) {
            return Float.valueOf(param2VGroup.getPivotY());
          }
        };
      ROTATION = (Property)new FloatProperty<VGroup>("rotation") {
          public void setValue(VectorDrawable.VGroup param2VGroup, float param2Float) {
            param2VGroup.setRotation(param2Float);
          }
          
          public Float get(VectorDrawable.VGroup param2VGroup) {
            return Float.valueOf(param2VGroup.getRotation());
          }
        };
      sPropertyMap = (HashMap<String, Property>)new Object();
    }
    
    private final ArrayList<VectorDrawable.VObject> mChildren = new ArrayList<>();
    
    private String mGroupName = null;
    
    private boolean mIsStateful;
    
    private final long mNativePtr;
    
    private int[] mThemeAttrs;
    
    private float[] mTransform;
    
    public VGroup(VGroup param1VGroup, ArrayMap<String, Object> param1ArrayMap) {
      this.mIsStateful = param1VGroup.mIsStateful;
      this.mThemeAttrs = param1VGroup.mThemeAttrs;
      String str = param1VGroup.mGroupName;
      this.mChangingConfigurations = param1VGroup.mChangingConfigurations;
      if (str != null)
        param1ArrayMap.put(str, this); 
      this.mNativePtr = VectorDrawable.nCreateGroup(param1VGroup.mNativePtr);
      ArrayList<VectorDrawable.VObject> arrayList = param1VGroup.mChildren;
      for (byte b = 0; b < arrayList.size(); b++) {
        VectorDrawable.VObject vObject = arrayList.get(b);
        if (vObject instanceof VGroup) {
          vObject = vObject;
          addChild(new VGroup((VGroup)vObject, param1ArrayMap));
        } else {
          if (vObject instanceof VectorDrawable.VFullPath) {
            vObject = new VectorDrawable.VFullPath((VectorDrawable.VFullPath)vObject);
          } else if (vObject instanceof VectorDrawable.VClipPath) {
            vObject = new VectorDrawable.VClipPath((VectorDrawable.VClipPath)vObject);
          } else {
            throw new IllegalStateException("Unknown object in the tree!");
          } 
          addChild(vObject);
          if (((VectorDrawable.VPath)vObject).mPathName != null)
            param1ArrayMap.put(((VectorDrawable.VPath)vObject).mPathName, vObject); 
        } 
      } 
    }
    
    public VGroup() {
      this.mNativePtr = VectorDrawable.nCreateGroup();
    }
    
    Property getProperty(String param1String) {
      if (sPropertyMap.containsKey(param1String))
        return sPropertyMap.get(param1String); 
      return null;
    }
    
    public String getGroupName() {
      return this.mGroupName;
    }
    
    public void addChild(VectorDrawable.VObject param1VObject) {
      VectorDrawable.nAddChild(this.mNativePtr, param1VObject.getNativePtr());
      this.mChildren.add(param1VObject);
      this.mIsStateful |= param1VObject.isStateful();
    }
    
    public void setTree(VirtualRefBasePtr param1VirtualRefBasePtr) {
      super.setTree(param1VirtualRefBasePtr);
      for (byte b = 0; b < this.mChildren.size(); b++)
        ((VectorDrawable.VObject)this.mChildren.get(b)).setTree(param1VirtualRefBasePtr); 
    }
    
    public long getNativePtr() {
      return this.mNativePtr;
    }
    
    public void inflate(Resources param1Resources, AttributeSet param1AttributeSet, Resources.Theme param1Theme) {
      TypedArray typedArray = Drawable.obtainAttributes(param1Resources, param1Theme, param1AttributeSet, R.styleable.VectorDrawableGroup);
      updateStateFromTypedArray(typedArray);
      typedArray.recycle();
    }
    
    void updateStateFromTypedArray(TypedArray param1TypedArray) {
      this.mChangingConfigurations |= param1TypedArray.getChangingConfigurations();
      this.mThemeAttrs = param1TypedArray.extractThemeAttrs();
      if (this.mTransform == null)
        this.mTransform = new float[7]; 
      boolean bool = VectorDrawable.nGetGroupProperties(this.mNativePtr, this.mTransform, 7);
      if (bool) {
        float f1 = param1TypedArray.getFloat(5, this.mTransform[0]);
        float f2 = param1TypedArray.getFloat(1, this.mTransform[1]);
        float f3 = param1TypedArray.getFloat(2, this.mTransform[2]);
        float f4 = param1TypedArray.getFloat(3, this.mTransform[3]);
        float f5 = param1TypedArray.getFloat(4, this.mTransform[4]);
        float f6 = param1TypedArray.getFloat(6, this.mTransform[5]);
        float f7 = param1TypedArray.getFloat(7, this.mTransform[6]);
        String str = param1TypedArray.getString(0);
        if (str != null) {
          this.mGroupName = str;
          VectorDrawable.nSetName(this.mNativePtr, str);
        } 
        VectorDrawable.nUpdateGroupProperties(this.mNativePtr, f1, f2, f3, f4, f5, f6, f7);
        return;
      } 
      throw new RuntimeException("Error: inconsistent property count");
    }
    
    public boolean onStateChange(int[] param1ArrayOfint) {
      boolean bool = false;
      ArrayList<VectorDrawable.VObject> arrayList = this.mChildren;
      byte b;
      int i;
      for (b = 0, i = arrayList.size(); b < i; b++, bool = bool1) {
        VectorDrawable.VObject vObject = arrayList.get(b);
        boolean bool1 = bool;
        if (vObject.isStateful())
          bool1 = bool | vObject.onStateChange(param1ArrayOfint); 
      } 
      return bool;
    }
    
    public boolean isStateful() {
      return this.mIsStateful;
    }
    
    public boolean hasFocusStateSpecified() {
      boolean bool = false;
      ArrayList<VectorDrawable.VObject> arrayList = this.mChildren;
      byte b;
      int i;
      for (b = 0, i = arrayList.size(); b < i; b++, bool = bool1) {
        VectorDrawable.VObject vObject = arrayList.get(b);
        boolean bool1 = bool;
        if (vObject.isStateful())
          bool1 = bool | vObject.hasFocusStateSpecified(); 
      } 
      return bool;
    }
    
    int getNativeSize() {
      int i = 100;
      for (byte b = 0; b < this.mChildren.size(); b++)
        i += ((VectorDrawable.VObject)this.mChildren.get(b)).getNativeSize(); 
      return i;
    }
    
    public boolean canApplyTheme() {
      if (this.mThemeAttrs != null)
        return true; 
      ArrayList<VectorDrawable.VObject> arrayList = this.mChildren;
      byte b;
      int i;
      for (b = 0, i = arrayList.size(); b < i; b++) {
        VectorDrawable.VObject vObject = arrayList.get(b);
        if (vObject.canApplyTheme())
          return true; 
      } 
      return false;
    }
    
    public void applyTheme(Resources.Theme param1Theme) {
      int[] arrayOfInt = this.mThemeAttrs;
      if (arrayOfInt != null) {
        TypedArray typedArray = param1Theme.resolveAttributes(arrayOfInt, R.styleable.VectorDrawableGroup);
        updateStateFromTypedArray(typedArray);
        typedArray.recycle();
      } 
      ArrayList<VectorDrawable.VObject> arrayList = this.mChildren;
      byte b;
      int i;
      for (b = 0, i = arrayList.size(); b < i; b++) {
        VectorDrawable.VObject vObject = arrayList.get(b);
        if (vObject.canApplyTheme()) {
          vObject.applyTheme(param1Theme);
          this.mIsStateful |= vObject.isStateful();
        } 
      } 
    }
    
    public float getRotation() {
      float f;
      if (isTreeValid()) {
        f = VectorDrawable.nGetRotation(this.mNativePtr);
      } else {
        f = 0.0F;
      } 
      return f;
    }
    
    public void setRotation(float param1Float) {
      if (isTreeValid())
        VectorDrawable.nSetRotation(this.mNativePtr, param1Float); 
    }
    
    public float getPivotX() {
      float f;
      if (isTreeValid()) {
        f = VectorDrawable.nGetPivotX(this.mNativePtr);
      } else {
        f = 0.0F;
      } 
      return f;
    }
    
    public void setPivotX(float param1Float) {
      if (isTreeValid())
        VectorDrawable.nSetPivotX(this.mNativePtr, param1Float); 
    }
    
    public float getPivotY() {
      float f;
      if (isTreeValid()) {
        f = VectorDrawable.nGetPivotY(this.mNativePtr);
      } else {
        f = 0.0F;
      } 
      return f;
    }
    
    public void setPivotY(float param1Float) {
      if (isTreeValid())
        VectorDrawable.nSetPivotY(this.mNativePtr, param1Float); 
    }
    
    public float getScaleX() {
      float f;
      if (isTreeValid()) {
        f = VectorDrawable.nGetScaleX(this.mNativePtr);
      } else {
        f = 0.0F;
      } 
      return f;
    }
    
    public void setScaleX(float param1Float) {
      if (isTreeValid())
        VectorDrawable.nSetScaleX(this.mNativePtr, param1Float); 
    }
    
    public float getScaleY() {
      float f;
      if (isTreeValid()) {
        f = VectorDrawable.nGetScaleY(this.mNativePtr);
      } else {
        f = 0.0F;
      } 
      return f;
    }
    
    public void setScaleY(float param1Float) {
      if (isTreeValid())
        VectorDrawable.nSetScaleY(this.mNativePtr, param1Float); 
    }
    
    public float getTranslateX() {
      float f;
      if (isTreeValid()) {
        f = VectorDrawable.nGetTranslateX(this.mNativePtr);
      } else {
        f = 0.0F;
      } 
      return f;
    }
    
    public void setTranslateX(float param1Float) {
      if (isTreeValid())
        VectorDrawable.nSetTranslateX(this.mNativePtr, param1Float); 
    }
    
    public float getTranslateY() {
      float f;
      if (isTreeValid()) {
        f = VectorDrawable.nGetTranslateY(this.mNativePtr);
      } else {
        f = 0.0F;
      } 
      return f;
    }
    
    public void setTranslateY(float param1Float) {
      if (isTreeValid())
        VectorDrawable.nSetTranslateY(this.mNativePtr, param1Float); 
    }
  }
  
  class null extends FloatProperty<VGroup> {
    null(VectorDrawable this$0) {
      super((String)this$0);
    }
    
    public void setValue(VectorDrawable.VGroup param1VGroup, float param1Float) {
      param1VGroup.setTranslateX(param1Float);
    }
    
    public Float get(VectorDrawable.VGroup param1VGroup) {
      return Float.valueOf(param1VGroup.getTranslateX());
    }
  }
  
  class null extends FloatProperty<VGroup> {
    null(VectorDrawable this$0) {
      super((String)this$0);
    }
    
    public void setValue(VectorDrawable.VGroup param1VGroup, float param1Float) {
      param1VGroup.setTranslateY(param1Float);
    }
    
    public Float get(VectorDrawable.VGroup param1VGroup) {
      return Float.valueOf(param1VGroup.getTranslateY());
    }
  }
  
  class null extends FloatProperty<VGroup> {
    null(VectorDrawable this$0) {
      super((String)this$0);
    }
    
    public void setValue(VectorDrawable.VGroup param1VGroup, float param1Float) {
      param1VGroup.setScaleX(param1Float);
    }
    
    public Float get(VectorDrawable.VGroup param1VGroup) {
      return Float.valueOf(param1VGroup.getScaleX());
    }
  }
  
  class null extends FloatProperty<VGroup> {
    null(VectorDrawable this$0) {
      super((String)this$0);
    }
    
    public void setValue(VectorDrawable.VGroup param1VGroup, float param1Float) {
      param1VGroup.setScaleY(param1Float);
    }
    
    public Float get(VectorDrawable.VGroup param1VGroup) {
      return Float.valueOf(param1VGroup.getScaleY());
    }
  }
  
  class null extends FloatProperty<VGroup> {
    null(VectorDrawable this$0) {
      super((String)this$0);
    }
    
    public void setValue(VectorDrawable.VGroup param1VGroup, float param1Float) {
      param1VGroup.setPivotX(param1Float);
    }
    
    public Float get(VectorDrawable.VGroup param1VGroup) {
      return Float.valueOf(param1VGroup.getPivotX());
    }
  }
  
  class null extends FloatProperty<VGroup> {
    null(VectorDrawable this$0) {
      super((String)this$0);
    }
    
    public void setValue(VectorDrawable.VGroup param1VGroup, float param1Float) {
      param1VGroup.setPivotY(param1Float);
    }
    
    public Float get(VectorDrawable.VGroup param1VGroup) {
      return Float.valueOf(param1VGroup.getPivotY());
    }
  }
  
  class null extends FloatProperty<VGroup> {
    null(VectorDrawable this$0) {
      super((String)this$0);
    }
    
    public void setValue(VectorDrawable.VGroup param1VGroup, float param1Float) {
      param1VGroup.setRotation(param1Float);
    }
    
    public Float get(VectorDrawable.VGroup param1VGroup) {
      return Float.valueOf(param1VGroup.getRotation());
    }
  }
  
  static abstract class VPath extends VObject {
    private static final Property<VPath, PathParser.PathData> PATH_DATA = (Property<VPath, PathParser.PathData>)new Object(PathParser.PathData.class, "pathData");
    
    int mChangingConfigurations;
    
    protected PathParser.PathData mPathData;
    
    String mPathName;
    
    Property getProperty(String param1String) {
      if (PATH_DATA.getName().equals(param1String))
        return PATH_DATA; 
      return null;
    }
    
    public VPath() {
      this.mPathData = null;
    }
    
    public VPath(VPath param1VPath) {
      PathParser.PathData pathData2 = null;
      this.mPathData = null;
      this.mPathName = param1VPath.mPathName;
      this.mChangingConfigurations = param1VPath.mChangingConfigurations;
      PathParser.PathData pathData1 = param1VPath.mPathData;
      if (pathData1 == null) {
        pathData1 = pathData2;
      } else {
        pathData1 = new PathParser.PathData(pathData1);
      } 
      this.mPathData = pathData1;
    }
    
    public String getPathName() {
      return this.mPathName;
    }
    
    public PathParser.PathData getPathData() {
      return this.mPathData;
    }
    
    public void setPathData(PathParser.PathData param1PathData) {
      this.mPathData.setPathData(param1PathData);
      if (isTreeValid())
        VectorDrawable.nSetPathData(getNativePtr(), this.mPathData.getNativePtr()); 
    }
  }
  
  class VClipPath extends VPath {
    private static final int NATIVE_ALLOCATION_SIZE = 120;
    
    private final long mNativePtr;
    
    public VClipPath() {
      this.mNativePtr = VectorDrawable.nCreateClipPath();
    }
    
    public VClipPath(VectorDrawable this$0) {
      super((VectorDrawable.VPath)this$0);
      this.mNativePtr = VectorDrawable.nCreateClipPath(((VClipPath)this$0).mNativePtr);
    }
    
    public long getNativePtr() {
      return this.mNativePtr;
    }
    
    public void inflate(Resources param1Resources, AttributeSet param1AttributeSet, Resources.Theme param1Theme) {
      TypedArray typedArray = Drawable.obtainAttributes(param1Resources, param1Theme, param1AttributeSet, R.styleable.VectorDrawableClipPath);
      updateStateFromTypedArray(typedArray);
      typedArray.recycle();
    }
    
    public boolean canApplyTheme() {
      return false;
    }
    
    public void applyTheme(Resources.Theme param1Theme) {}
    
    public boolean onStateChange(int[] param1ArrayOfint) {
      return false;
    }
    
    public boolean isStateful() {
      return false;
    }
    
    public boolean hasFocusStateSpecified() {
      return false;
    }
    
    int getNativeSize() {
      return 120;
    }
    
    private void updateStateFromTypedArray(TypedArray param1TypedArray) {
      this.mChangingConfigurations |= param1TypedArray.getChangingConfigurations();
      String str2 = param1TypedArray.getString(0);
      if (str2 != null) {
        this.mPathName = str2;
        VectorDrawable.nSetName(this.mNativePtr, this.mPathName);
      } 
      String str1 = param1TypedArray.getString(1);
      if (str1 != null) {
        this.mPathData = new PathParser.PathData(str1);
        VectorDrawable.nSetPathString(this.mNativePtr, str1, str1.length());
      } 
    }
  }
  
  class VFullPath extends VPath {
    private static final HashMap<String, Integer> sPropertyIndexMap = (HashMap<String, Integer>)new Object();
    
    static {
      STROKE_COLOR = (Property)new IntProperty<VFullPath>("strokeColor") {
          IntProperty(VectorDrawable this$0) {
            super((String)this$0);
          }
          
          public void setValue(VectorDrawable.VFullPath param1VFullPath, int param1Int) {
            param1VFullPath.setStrokeColor(param1Int);
          }
          
          public Integer get(VectorDrawable.VFullPath param1VFullPath) {
            return Integer.valueOf(param1VFullPath.getStrokeColor());
          }
        };
      STROKE_ALPHA = (Property)new FloatProperty<VFullPath>("strokeAlpha") {
          FloatProperty(VectorDrawable this$0) {
            super((String)this$0);
          }
          
          public void setValue(VectorDrawable.VFullPath param1VFullPath, float param1Float) {
            param1VFullPath.setStrokeAlpha(param1Float);
          }
          
          public Float get(VectorDrawable.VFullPath param1VFullPath) {
            return Float.valueOf(param1VFullPath.getStrokeAlpha());
          }
        };
      FILL_COLOR = (Property)new IntProperty<VFullPath>("fillColor") {
          IntProperty(VectorDrawable this$0) {
            super((String)this$0);
          }
          
          public void setValue(VectorDrawable.VFullPath param1VFullPath, int param1Int) {
            param1VFullPath.setFillColor(param1Int);
          }
          
          public Integer get(VectorDrawable.VFullPath param1VFullPath) {
            return Integer.valueOf(param1VFullPath.getFillColor());
          }
        };
      FILL_ALPHA = (Property)new FloatProperty<VFullPath>("fillAlpha") {
          FloatProperty(VectorDrawable this$0) {
            super((String)this$0);
          }
          
          public void setValue(VectorDrawable.VFullPath param1VFullPath, float param1Float) {
            param1VFullPath.setFillAlpha(param1Float);
          }
          
          public Float get(VectorDrawable.VFullPath param1VFullPath) {
            return Float.valueOf(param1VFullPath.getFillAlpha());
          }
        };
      TRIM_PATH_START = (Property)new FloatProperty<VFullPath>("trimPathStart") {
          FloatProperty(VectorDrawable this$0) {
            super((String)this$0);
          }
          
          public void setValue(VectorDrawable.VFullPath param1VFullPath, float param1Float) {
            param1VFullPath.setTrimPathStart(param1Float);
          }
          
          public Float get(VectorDrawable.VFullPath param1VFullPath) {
            return Float.valueOf(param1VFullPath.getTrimPathStart());
          }
        };
      TRIM_PATH_END = (Property)new FloatProperty<VFullPath>("trimPathEnd") {
          FloatProperty(VectorDrawable this$0) {
            super((String)this$0);
          }
          
          public void setValue(VectorDrawable.VFullPath param1VFullPath, float param1Float) {
            param1VFullPath.setTrimPathEnd(param1Float);
          }
          
          public Float get(VectorDrawable.VFullPath param1VFullPath) {
            return Float.valueOf(param1VFullPath.getTrimPathEnd());
          }
        };
      TRIM_PATH_OFFSET = (Property)new FloatProperty<VFullPath>("trimPathOffset") {
          FloatProperty(VectorDrawable this$0) {
            super((String)this$0);
          }
          
          public void setValue(VectorDrawable.VFullPath param1VFullPath, float param1Float) {
            param1VFullPath.setTrimPathOffset(param1Float);
          }
          
          public Float get(VectorDrawable.VFullPath param1VFullPath) {
            return Float.valueOf(param1VFullPath.getTrimPathOffset());
          }
        };
      sPropertyMap = (HashMap<String, Property>)new Object();
    }
    
    ComplexColor mStrokeColors = null;
    
    ComplexColor mFillColors = null;
    
    private static final float CHANGE_UNIT = 0.5F;
    
    private static final Property<VFullPath, Float> FILL_ALPHA;
    
    private static final int FILL_ALPHA_INDEX = 4;
    
    private static final Property<VFullPath, Integer> FILL_COLOR;
    
    private static final int FILL_COLOR_INDEX = 3;
    
    private static final int FILL_TYPE_INDEX = 11;
    
    private static final float MIN_CHANGE_WIDTH = 1.5F;
    
    private static final int MODE_UNIT = 16;
    
    private static final int MODE_WIDTH_FLAG = 128;
    
    private static final int NATIVE_ALLOCATION_SIZE = 264;
    
    private static final Property<VFullPath, Float> STROKE_ALPHA;
    
    private static final int STROKE_ALPHA_INDEX = 2;
    
    private static final Property<VFullPath, Integer> STROKE_COLOR;
    
    private static final int STROKE_COLOR_INDEX = 1;
    
    private static final int STROKE_LINE_CAP_INDEX = 8;
    
    private static final int STROKE_LINE_JOIN_INDEX = 9;
    
    private static final int STROKE_MITER_LIMIT_INDEX = 10;
    
    private static final Property<VFullPath, Float> STROKE_WIDTH = (Property)new FloatProperty<VFullPath>("strokeWidth") {
        FloatProperty(VectorDrawable this$0) {
          super((String)this$0);
        }
        
        public void setValue(VectorDrawable.VFullPath param1VFullPath, float param1Float) {
          param1VFullPath.setStrokeWidth(param1Float);
        }
        
        public Float get(VectorDrawable.VFullPath param1VFullPath) {
          return Float.valueOf(param1VFullPath.getStrokeWidth());
        }
      };
    
    private static final int STROKE_WIDTH_INDEX = 0;
    
    private static final int TOTAL_PROPERTY_COUNT = 12;
    
    private static final Property<VFullPath, Float> TRIM_PATH_END;
    
    private static final int TRIM_PATH_END_INDEX = 6;
    
    private static final Property<VFullPath, Float> TRIM_PATH_OFFSET;
    
    private static final int TRIM_PATH_OFFSET_INDEX = 7;
    
    private static final Property<VFullPath, Float> TRIM_PATH_START;
    
    private static final int TRIM_PATH_START_INDEX = 5;
    
    private static final HashMap<String, Property> sPropertyMap;
    
    private final long mNativePtr;
    
    private byte[] mPropertyData;
    
    private int[] mThemeAttrs;
    
    public VFullPath() {
      this.mNativePtr = VectorDrawable.nCreateFullPath();
    }
    
    public VFullPath(VectorDrawable this$0) {
      super((VectorDrawable.VPath)this$0);
      this.mNativePtr = VectorDrawable.nCreateFullPath(((VFullPath)this$0).mNativePtr);
      this.mThemeAttrs = ((VFullPath)this$0).mThemeAttrs;
      this.mStrokeColors = ((VFullPath)this$0).mStrokeColors;
      this.mFillColors = ((VFullPath)this$0).mFillColors;
    }
    
    Property getProperty(String param1String) {
      Property property = super.getProperty(param1String);
      if (property != null)
        return property; 
      if (sPropertyMap.containsKey(param1String))
        return sPropertyMap.get(param1String); 
      return null;
    }
    
    int getPropertyIndex(String param1String) {
      if (!sPropertyIndexMap.containsKey(param1String))
        return -1; 
      return ((Integer)sPropertyIndexMap.get(param1String)).intValue();
    }
    
    public boolean onStateChange(int[] param1ArrayOfint) {
      int i = 0;
      ComplexColor complexColor = this.mStrokeColors;
      boolean bool = true;
      int j = i;
      if (complexColor != null) {
        j = i;
        if (complexColor instanceof ColorStateList) {
          boolean bool1;
          int k = getStrokeColor();
          complexColor = this.mStrokeColors;
          int m = complexColor.getColorForState(param1ArrayOfint, k);
          if (k != m) {
            bool1 = true;
          } else {
            bool1 = false;
          } 
          i = false | bool1;
          j = i;
          if (k != m) {
            VectorDrawable.nSetStrokeColor(this.mNativePtr, m);
            j = i;
          } 
        } 
      } 
      complexColor = this.mFillColors;
      i = j;
      if (complexColor != null) {
        i = j;
        if (complexColor instanceof ColorStateList) {
          byte b;
          int k = getFillColor();
          int m = ((ColorStateList)this.mFillColors).getColorForState(param1ArrayOfint, k);
          if (k != m) {
            b = bool;
          } else {
            b = 0;
          } 
          j |= b;
          i = j;
          if (k != m) {
            VectorDrawable.nSetFillColor(this.mNativePtr, m);
            i = j;
          } 
        } 
      } 
      return i;
    }
    
    public boolean isStateful() {
      return (this.mStrokeColors != null || this.mFillColors != null);
    }
    
    public boolean hasFocusStateSpecified() {
      ComplexColor complexColor = this.mStrokeColors;
      if (complexColor != null && complexColor instanceof ColorStateList) {
        complexColor = complexColor;
        if (complexColor.hasFocusStateSpecified()) {
          complexColor = this.mFillColors;
          if (complexColor != null && complexColor instanceof ColorStateList) {
            complexColor = complexColor;
            if (complexColor.hasFocusStateSpecified())
              return true; 
          } 
        } 
      } 
      return false;
    }
    
    int getNativeSize() {
      return 264;
    }
    
    public long getNativePtr() {
      return this.mNativePtr;
    }
    
    public void inflate(Resources param1Resources, AttributeSet param1AttributeSet, Resources.Theme param1Theme) {
      TypedArray typedArray = Drawable.obtainAttributes(param1Resources, param1Theme, param1AttributeSet, R.styleable.VectorDrawablePath);
      hookVFullInflate(param1Resources);
      updateStateFromTypedArray(typedArray);
      typedArray.recycle();
    }
    
    private void updateStateFromTypedArray(TypedArray param1TypedArray) {
      if (this.mPropertyData == null)
        this.mPropertyData = new byte[48]; 
      boolean bool = VectorDrawable.nGetFullPathProperties(this.mNativePtr, this.mPropertyData, 48);
      if (bool) {
        ByteBuffer byteBuffer1 = ByteBuffer.wrap(this.mPropertyData);
        byteBuffer1.order(ByteOrder.nativeOrder());
        float f1 = byteBuffer1.getFloat(0);
        int i = byteBuffer1.getInt(4);
        float f2 = byteBuffer1.getFloat(8);
        int j = byteBuffer1.getInt(12);
        float f3 = byteBuffer1.getFloat(16);
        float f4 = byteBuffer1.getFloat(20);
        float f5 = byteBuffer1.getFloat(24);
        float f6 = byteBuffer1.getFloat(28);
        int k = byteBuffer1.getInt(32);
        int m = byteBuffer1.getInt(36);
        float f7 = byteBuffer1.getFloat(40);
        int n = byteBuffer1.getInt(44);
        Shader shader = null;
        byteBuffer1 = null;
        ByteBuffer byteBuffer2 = null, byteBuffer3 = null;
        this.mChangingConfigurations |= param1TypedArray.getChangingConfigurations();
        this.mThemeAttrs = param1TypedArray.extractThemeAttrs();
        String str1 = param1TypedArray.getString(0);
        if (str1 != null) {
          this.mPathName = str1;
          VectorDrawable.nSetName(this.mNativePtr, this.mPathName);
        } 
        String str2 = param1TypedArray.getString(2);
        if (str2 != null) {
          this.mPathData = new PathParser.PathData(str2);
          VectorDrawable.nSetPathString(this.mNativePtr, str2, str2.length());
        } 
        ComplexColor complexColor = param1TypedArray.getComplexColor(1);
        if (complexColor != null) {
          Shader shader1;
          if (complexColor instanceof GradientColor) {
            this.mFillColors = complexColor;
            shader1 = ((GradientColor)complexColor).getShader();
          } else if (complexColor.isStateful() || complexColor.canApplyTheme()) {
            this.mFillColors = complexColor;
          } else {
            this.mFillColors = null;
          } 
          j = complexColor.getDefaultColor();
          shader = shader1;
        } 
        complexColor = param1TypedArray.getComplexColor(3);
        if (complexColor != null) {
          if (complexColor instanceof GradientColor) {
            this.mStrokeColors = complexColor;
            Shader shader1 = ((GradientColor)complexColor).getShader();
          } else if (complexColor.isStateful() || complexColor.canApplyTheme()) {
            this.mStrokeColors = complexColor;
            byteBuffer1 = byteBuffer3;
          } else {
            this.mStrokeColors = null;
            byteBuffer1 = byteBuffer3;
          } 
          i = complexColor.getDefaultColor();
        } else {
          byteBuffer1 = byteBuffer2;
        } 
        long l1 = this.mNativePtr;
        long l2 = 0L;
        if (shader != null) {
          l3 = shader.getNativeInstance();
        } else {
          l3 = 0L;
        } 
        VectorDrawable.nUpdateFullPathFillGradient(l1, l3);
        l1 = this.mNativePtr;
        long l3 = l2;
        if (byteBuffer1 != null)
          l3 = byteBuffer1.getNativeInstance(); 
        VectorDrawable.nUpdateFullPathStrokeGradient(l1, l3);
        f3 = param1TypedArray.getFloat(12, f3);
        k = param1TypedArray.getInt(8, k);
        m = param1TypedArray.getInt(9, m);
        f7 = param1TypedArray.getFloat(10, f7);
        f2 = param1TypedArray.getFloat(11, f2);
        f1 = param1TypedArray.getFloat(4, f1);
        f1 = calculateStrokeWidth(str1, f1).floatValue();
        f5 = param1TypedArray.getFloat(6, f5);
        f6 = param1TypedArray.getFloat(7, f6);
        f4 = param1TypedArray.getFloat(5, f4);
        n = param1TypedArray.getInt(13, n);
        VectorDrawable.nUpdateFullPathProperties(this.mNativePtr, f1, i, f2, j, f3, f4, f5, f6, f7, k, m, n);
        return;
      } 
      throw new RuntimeException("Error: inconsistent property count");
    }
    
    public boolean canApplyTheme() {
      if (this.mThemeAttrs != null)
        return true; 
      boolean bool1 = canComplexColorApplyTheme(this.mFillColors);
      boolean bool2 = canComplexColorApplyTheme(this.mStrokeColors);
      if (bool1 || bool2)
        return true; 
      return false;
    }
    
    public void applyTheme(Resources.Theme param1Theme) {
      int[] arrayOfInt = this.mThemeAttrs;
      if (arrayOfInt != null) {
        TypedArray typedArray = param1Theme.resolveAttributes(arrayOfInt, R.styleable.VectorDrawablePath);
        updateStateFromTypedArray(typedArray);
        typedArray.recycle();
      } 
      boolean bool1 = canComplexColorApplyTheme(this.mFillColors);
      boolean bool2 = canComplexColorApplyTheme(this.mStrokeColors);
      if (bool1) {
        ComplexColor complexColor = this.mFillColors.obtainForTheme(param1Theme);
        if (complexColor instanceof GradientColor) {
          long l1 = this.mNativePtr;
          complexColor = complexColor;
          long l2 = complexColor.getShader().getNativeInstance();
          VectorDrawable.nUpdateFullPathFillGradient(l1, l2);
        } else if (complexColor instanceof ColorStateList) {
          VectorDrawable.nSetFillColor(this.mNativePtr, complexColor.getDefaultColor());
        } 
      } 
      if (bool2) {
        ComplexColor complexColor = this.mStrokeColors.obtainForTheme(param1Theme);
        if (complexColor instanceof GradientColor) {
          long l1 = this.mNativePtr;
          complexColor = complexColor;
          long l2 = complexColor.getShader().getNativeInstance();
          VectorDrawable.nUpdateFullPathStrokeGradient(l1, l2);
        } else if (complexColor instanceof ColorStateList) {
          VectorDrawable.nSetStrokeColor(this.mNativePtr, complexColor.getDefaultColor());
        } 
      } 
    }
    
    private boolean canComplexColorApplyTheme(ComplexColor param1ComplexColor) {
      boolean bool;
      if (param1ComplexColor != null && param1ComplexColor.canApplyTheme()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    int getStrokeColor() {
      boolean bool;
      if (isTreeValid()) {
        bool = VectorDrawable.nGetStrokeColor(this.mNativePtr);
      } else {
        bool = false;
      } 
      return bool;
    }
    
    void setStrokeColor(int param1Int) {
      this.mStrokeColors = null;
      if (isTreeValid())
        VectorDrawable.nSetStrokeColor(this.mNativePtr, param1Int); 
    }
    
    float getStrokeWidth() {
      float f;
      if (isTreeValid()) {
        f = VectorDrawable.nGetStrokeWidth(this.mNativePtr);
      } else {
        f = 0.0F;
      } 
      return f;
    }
    
    void setStrokeWidth(float param1Float) {
      if (isTreeValid())
        VectorDrawable.nSetStrokeWidth(this.mNativePtr, param1Float); 
    }
    
    float getStrokeAlpha() {
      float f;
      if (isTreeValid()) {
        f = VectorDrawable.nGetStrokeAlpha(this.mNativePtr);
      } else {
        f = 0.0F;
      } 
      return f;
    }
    
    void setStrokeAlpha(float param1Float) {
      if (isTreeValid())
        VectorDrawable.nSetStrokeAlpha(this.mNativePtr, param1Float); 
    }
    
    int getFillColor() {
      boolean bool;
      if (isTreeValid()) {
        bool = VectorDrawable.nGetFillColor(this.mNativePtr);
      } else {
        bool = false;
      } 
      return bool;
    }
    
    void setFillColor(int param1Int) {
      this.mFillColors = null;
      if (isTreeValid())
        VectorDrawable.nSetFillColor(this.mNativePtr, param1Int); 
    }
    
    float getFillAlpha() {
      float f;
      if (isTreeValid()) {
        f = VectorDrawable.nGetFillAlpha(this.mNativePtr);
      } else {
        f = 0.0F;
      } 
      return f;
    }
    
    void setFillAlpha(float param1Float) {
      if (isTreeValid())
        VectorDrawable.nSetFillAlpha(this.mNativePtr, param1Float); 
    }
    
    float getTrimPathStart() {
      float f;
      if (isTreeValid()) {
        f = VectorDrawable.nGetTrimPathStart(this.mNativePtr);
      } else {
        f = 0.0F;
      } 
      return f;
    }
    
    void setTrimPathStart(float param1Float) {
      if (isTreeValid())
        VectorDrawable.nSetTrimPathStart(this.mNativePtr, param1Float); 
    }
    
    float getTrimPathEnd() {
      float f;
      if (isTreeValid()) {
        f = VectorDrawable.nGetTrimPathEnd(this.mNativePtr);
      } else {
        f = 0.0F;
      } 
      return f;
    }
    
    void setTrimPathEnd(float param1Float) {
      if (isTreeValid())
        VectorDrawable.nSetTrimPathEnd(this.mNativePtr, param1Float); 
    }
    
    float getTrimPathOffset() {
      float f;
      if (isTreeValid()) {
        f = VectorDrawable.nGetTrimPathOffset(this.mNativePtr);
      } else {
        f = 0.0F;
      } 
      return f;
    }
    
    void setTrimPathOffset(float param1Float) {
      if (isTreeValid())
        VectorDrawable.nSetTrimPathOffset(this.mNativePtr, param1Float); 
    }
  }
  
  class null extends FloatProperty<VFullPath> {
    null(VectorDrawable this$0) {
      super((String)this$0);
    }
    
    public void setValue(VectorDrawable.VFullPath param1VFullPath, float param1Float) {
      param1VFullPath.setStrokeWidth(param1Float);
    }
    
    public Float get(VectorDrawable.VFullPath param1VFullPath) {
      return Float.valueOf(param1VFullPath.getStrokeWidth());
    }
  }
  
  class null extends IntProperty<VFullPath> {
    null(VectorDrawable this$0) {
      super((String)this$0);
    }
    
    public void setValue(VectorDrawable.VFullPath param1VFullPath, int param1Int) {
      param1VFullPath.setStrokeColor(param1Int);
    }
    
    public Integer get(VectorDrawable.VFullPath param1VFullPath) {
      return Integer.valueOf(param1VFullPath.getStrokeColor());
    }
  }
  
  class null extends FloatProperty<VFullPath> {
    null(VectorDrawable this$0) {
      super((String)this$0);
    }
    
    public void setValue(VectorDrawable.VFullPath param1VFullPath, float param1Float) {
      param1VFullPath.setStrokeAlpha(param1Float);
    }
    
    public Float get(VectorDrawable.VFullPath param1VFullPath) {
      return Float.valueOf(param1VFullPath.getStrokeAlpha());
    }
  }
  
  class null extends IntProperty<VFullPath> {
    null(VectorDrawable this$0) {
      super((String)this$0);
    }
    
    public void setValue(VectorDrawable.VFullPath param1VFullPath, int param1Int) {
      param1VFullPath.setFillColor(param1Int);
    }
    
    public Integer get(VectorDrawable.VFullPath param1VFullPath) {
      return Integer.valueOf(param1VFullPath.getFillColor());
    }
  }
  
  class null extends FloatProperty<VFullPath> {
    null(VectorDrawable this$0) {
      super((String)this$0);
    }
    
    public void setValue(VectorDrawable.VFullPath param1VFullPath, float param1Float) {
      param1VFullPath.setFillAlpha(param1Float);
    }
    
    public Float get(VectorDrawable.VFullPath param1VFullPath) {
      return Float.valueOf(param1VFullPath.getFillAlpha());
    }
  }
  
  class null extends FloatProperty<VFullPath> {
    null(VectorDrawable this$0) {
      super((String)this$0);
    }
    
    public void setValue(VectorDrawable.VFullPath param1VFullPath, float param1Float) {
      param1VFullPath.setTrimPathStart(param1Float);
    }
    
    public Float get(VectorDrawable.VFullPath param1VFullPath) {
      return Float.valueOf(param1VFullPath.getTrimPathStart());
    }
  }
  
  class null extends FloatProperty<VFullPath> {
    null(VectorDrawable this$0) {
      super((String)this$0);
    }
    
    public void setValue(VectorDrawable.VFullPath param1VFullPath, float param1Float) {
      param1VFullPath.setTrimPathEnd(param1Float);
    }
    
    public Float get(VectorDrawable.VFullPath param1VFullPath) {
      return Float.valueOf(param1VFullPath.getTrimPathEnd());
    }
  }
  
  class null extends FloatProperty<VFullPath> {
    null(VectorDrawable this$0) {
      super((String)this$0);
    }
    
    public void setValue(VectorDrawable.VFullPath param1VFullPath, float param1Float) {
      param1VFullPath.setTrimPathOffset(param1Float);
    }
    
    public Float get(VectorDrawable.VFullPath param1VFullPath) {
      return Float.valueOf(param1VFullPath.getTrimPathOffset());
    }
  }
  
  void calculateVectorColor() {
    calculateVectorColor(this.mVectorState.mRootGroup);
  }
  
  private void calculateVectorColor(VGroup paramVGroup) {
    if (paramVGroup == null || paramVGroup.mChildren.size() == 0)
      return; 
    for (byte b = 0; b < paramVGroup.mChildren.size(); b++) {
      if (paramVGroup.mChildren.get(b) instanceof VGroup) {
        calculateVectorColor(paramVGroup.mChildren.get(b));
      } else if (paramVGroup.mChildren.get(b) instanceof VFullPath) {
        calculatePathColor(paramVGroup.mChildren.get(b));
      } 
    } 
  }
  
  @FastNative
  private static native void nAddChild(long paramLong1, long paramLong2);
  
  @FastNative
  private static native long nCreateClipPath();
  
  @FastNative
  private static native long nCreateClipPath(long paramLong);
  
  @FastNative
  private static native long nCreateFullPath();
  
  @FastNative
  private static native long nCreateFullPath(long paramLong);
  
  @FastNative
  private static native long nCreateGroup();
  
  @FastNative
  private static native long nCreateGroup(long paramLong);
  
  @FastNative
  private static native long nCreateTree(long paramLong);
  
  @FastNative
  private static native long nCreateTreeFromCopy(long paramLong1, long paramLong2);
  
  private static native int nDraw(long paramLong1, long paramLong2, long paramLong3, Rect paramRect, boolean paramBoolean1, boolean paramBoolean2);
  
  @FastNative
  private static native float nGetFillAlpha(long paramLong);
  
  @FastNative
  private static native int nGetFillColor(long paramLong);
  
  private static native boolean nGetFullPathProperties(long paramLong, byte[] paramArrayOfbyte, int paramInt);
  
  private static native boolean nGetGroupProperties(long paramLong, float[] paramArrayOffloat, int paramInt);
  
  @FastNative
  private static native float nGetPivotX(long paramLong);
  
  @FastNative
  private static native float nGetPivotY(long paramLong);
  
  @FastNative
  private static native float nGetRootAlpha(long paramLong);
  
  @FastNative
  private static native float nGetRotation(long paramLong);
  
  @FastNative
  private static native float nGetScaleX(long paramLong);
  
  @FastNative
  private static native float nGetScaleY(long paramLong);
  
  @FastNative
  private static native float nGetStrokeAlpha(long paramLong);
  
  @FastNative
  private static native int nGetStrokeColor(long paramLong);
  
  @FastNative
  private static native float nGetStrokeWidth(long paramLong);
  
  @FastNative
  private static native float nGetTranslateX(long paramLong);
  
  @FastNative
  private static native float nGetTranslateY(long paramLong);
  
  @FastNative
  private static native float nGetTrimPathEnd(long paramLong);
  
  @FastNative
  private static native float nGetTrimPathOffset(long paramLong);
  
  @FastNative
  private static native float nGetTrimPathStart(long paramLong);
  
  @FastNative
  private static native void nSetAllowCaching(long paramLong, boolean paramBoolean);
  
  @FastNative
  private static native void nSetAntiAlias(long paramLong, boolean paramBoolean);
  
  @FastNative
  private static native void nSetFillAlpha(long paramLong, float paramFloat);
  
  @FastNative
  private static native void nSetFillColor(long paramLong, int paramInt);
  
  private static native void nSetName(long paramLong, String paramString);
  
  @FastNative
  private static native void nSetPathData(long paramLong1, long paramLong2);
  
  private static native void nSetPathString(long paramLong, String paramString, int paramInt);
  
  @FastNative
  private static native void nSetPivotX(long paramLong, float paramFloat);
  
  @FastNative
  private static native void nSetPivotY(long paramLong, float paramFloat);
  
  @FastNative
  private static native void nSetRendererViewportSize(long paramLong, float paramFloat1, float paramFloat2);
  
  @FastNative
  private static native boolean nSetRootAlpha(long paramLong, float paramFloat);
  
  @FastNative
  private static native void nSetRotation(long paramLong, float paramFloat);
  
  @FastNative
  private static native void nSetScaleX(long paramLong, float paramFloat);
  
  @FastNative
  private static native void nSetScaleY(long paramLong, float paramFloat);
  
  @FastNative
  private static native void nSetStrokeAlpha(long paramLong, float paramFloat);
  
  @FastNative
  private static native void nSetStrokeColor(long paramLong, int paramInt);
  
  @FastNative
  private static native void nSetStrokeWidth(long paramLong, float paramFloat);
  
  @FastNative
  private static native void nSetTranslateX(long paramLong, float paramFloat);
  
  @FastNative
  private static native void nSetTranslateY(long paramLong, float paramFloat);
  
  @FastNative
  private static native void nSetTrimPathEnd(long paramLong, float paramFloat);
  
  @FastNative
  private static native void nSetTrimPathOffset(long paramLong, float paramFloat);
  
  @FastNative
  private static native void nSetTrimPathStart(long paramLong, float paramFloat);
  
  @FastNative
  private static native void nUpdateFullPathFillGradient(long paramLong1, long paramLong2);
  
  @FastNative
  private static native void nUpdateFullPathProperties(long paramLong, float paramFloat1, int paramInt1, float paramFloat2, int paramInt2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, int paramInt3, int paramInt4, int paramInt5);
  
  @FastNative
  private static native void nUpdateFullPathStrokeGradient(long paramLong1, long paramLong2);
  
  @FastNative
  private static native void nUpdateGroupProperties(long paramLong, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7);
  
  class VObject extends OplusBaseVObject {
    VirtualRefBasePtr mTreePtr;
    
    VObject() {
      this.mTreePtr = null;
    }
    
    abstract void applyTheme(Resources.Theme param1Theme);
    
    abstract boolean canApplyTheme();
    
    abstract long getNativePtr();
    
    abstract int getNativeSize();
    
    abstract Property getProperty(String param1String);
    
    abstract boolean hasFocusStateSpecified();
    
    abstract void inflate(Resources param1Resources, AttributeSet param1AttributeSet, Resources.Theme param1Theme);
    
    abstract boolean isStateful();
    
    boolean isTreeValid() {
      boolean bool;
      VirtualRefBasePtr virtualRefBasePtr = this.mTreePtr;
      if (virtualRefBasePtr != null && virtualRefBasePtr.get() != 0L) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    abstract boolean onStateChange(int[] param1ArrayOfint);
    
    void setTree(VirtualRefBasePtr param1VirtualRefBasePtr) {
      this.mTreePtr = param1VirtualRefBasePtr;
    }
  }
}
