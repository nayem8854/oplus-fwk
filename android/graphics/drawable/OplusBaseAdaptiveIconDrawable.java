package android.graphics.drawable;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Path;
import android.graphics.Rect;

public class OplusBaseAdaptiveIconDrawable extends Drawable {
  public void draw(Canvas paramCanvas) {}
  
  public void setAlpha(int paramInt) {}
  
  public void setColorFilter(ColorFilter paramColorFilter) {}
  
  public int getOpacity() {
    return 0;
  }
  
  protected boolean hookOnBoundsChange(Rect paramRect) {
    return false;
  }
  
  protected boolean hookDraw(Canvas paramCanvas) {
    return false;
  }
  
  protected Path hookGetIconMask() {
    return null;
  }
  
  protected boolean hookGetIntrinsicHeight() {
    return false;
  }
  
  protected boolean hookGetIntrinsicWidth() {
    return false;
  }
}
