package android.graphics.drawable;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.DashPathEffect;
import android.graphics.Insets;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Xfermode;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import com.android.internal.R;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class GradientDrawable extends Drawable {
  public static boolean sWrapNegativeAngleMeasurements = true;
  
  static {
    DEFAULT_ORIENTATION = Orientation.TOP_BOTTOM;
  }
  
  private final Paint mFillPaint = new Paint(1);
  
  private int mAlpha = 255;
  
  private final Path mPath = new Path();
  
  private final RectF mRect = new RectF();
  
  private boolean mPathIsDirty = true;
  
  private boolean mSmoothRound = false;
  
  private static final float DEFAULT_INNER_RADIUS_RATIO = 3.0F;
  
  private static final Orientation DEFAULT_ORIENTATION;
  
  private static final float DEFAULT_THICKNESS_RATIO = 9.0F;
  
  public static final int LINE = 2;
  
  public static final int LINEAR_GRADIENT = 0;
  
  public static final int OVAL = 1;
  
  public static final int RADIAL_GRADIENT = 1;
  
  private static final int RADIUS_TYPE_FRACTION = 1;
  
  private static final int RADIUS_TYPE_FRACTION_PARENT = 2;
  
  private static final int RADIUS_TYPE_PIXELS = 0;
  
  public static final int RECTANGLE = 0;
  
  public static final int RING = 3;
  
  public static final int SWEEP_GRADIENT = 2;
  
  private BlendModeColorFilter mBlendModeColorFilter;
  
  private ColorFilter mColorFilter;
  
  private boolean mGradientIsDirty;
  
  private float mGradientRadius;
  
  private GradientState mGradientState;
  
  private Paint mLayerPaint;
  
  private boolean mMutated;
  
  private Rect mPadding;
  
  private Path mRingPath;
  
  private Paint mStrokePaint;
  
  @Retention(RetentionPolicy.SOURCE)
  class Shape implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class RadiusType implements Annotation {}
  
  class Orientation extends Enum<Orientation> {
    private static final Orientation[] $VALUES;
    
    public static final Orientation BL_TR;
    
    public static final Orientation BOTTOM_TOP;
    
    public static final Orientation BR_TL;
    
    public static final Orientation LEFT_RIGHT;
    
    public static final Orientation RIGHT_LEFT = new Orientation("RIGHT_LEFT", 2);
    
    public static final Orientation TL_BR;
    
    public static Orientation[] values() {
      return (Orientation[])$VALUES.clone();
    }
    
    public static Orientation valueOf(String param1String) {
      return Enum.<Orientation>valueOf(Orientation.class, param1String);
    }
    
    private Orientation(GradientDrawable this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static final Orientation TOP_BOTTOM = new Orientation("TOP_BOTTOM", 0);
    
    public static final Orientation TR_BL = new Orientation("TR_BL", 1);
    
    static {
      BR_TL = new Orientation("BR_TL", 3);
      BOTTOM_TOP = new Orientation("BOTTOM_TOP", 4);
      BL_TR = new Orientation("BL_TR", 5);
      LEFT_RIGHT = new Orientation("LEFT_RIGHT", 6);
      Orientation orientation = new Orientation("TL_BR", 7);
      $VALUES = new Orientation[] { TOP_BOTTOM, TR_BL, RIGHT_LEFT, BR_TL, BOTTOM_TOP, BL_TR, LEFT_RIGHT, orientation };
    }
  }
  
  public GradientDrawable() {
    this(new GradientState(DEFAULT_ORIENTATION, null), (Resources)null);
  }
  
  public GradientDrawable(Orientation paramOrientation, int[] paramArrayOfint) {
    this(new GradientState(paramOrientation, paramArrayOfint), (Resources)null);
  }
  
  public boolean getPadding(Rect paramRect) {
    Rect rect = this.mPadding;
    if (rect != null) {
      paramRect.set(rect);
      return true;
    } 
    return super.getPadding(paramRect);
  }
  
  public void setCornerRadii(float[] paramArrayOffloat) {
    this.mGradientState.setCornerRadii(paramArrayOffloat);
    this.mPathIsDirty = true;
    invalidateSelf();
  }
  
  public float[] getCornerRadii() {
    return (float[])this.mGradientState.mRadiusArray.clone();
  }
  
  public void setCornerRadius(float paramFloat) {
    this.mGradientState.setCornerRadius(paramFloat);
    this.mPathIsDirty = true;
    invalidateSelf();
  }
  
  public float getCornerRadius() {
    return this.mGradientState.mRadius;
  }
  
  public void setStroke(int paramInt1, int paramInt2) {
    setStroke(paramInt1, paramInt2, 0.0F, 0.0F);
  }
  
  public void setStroke(int paramInt, ColorStateList paramColorStateList) {
    setStroke(paramInt, paramColorStateList, 0.0F, 0.0F);
  }
  
  public void setStroke(int paramInt1, int paramInt2, float paramFloat1, float paramFloat2) {
    this.mGradientState.setStroke(paramInt1, ColorStateList.valueOf(paramInt2), paramFloat1, paramFloat2);
    setStrokeInternal(paramInt1, paramInt2, paramFloat1, paramFloat2);
  }
  
  public void setStroke(int paramInt, ColorStateList paramColorStateList, float paramFloat1, float paramFloat2) {
    int i;
    this.mGradientState.setStroke(paramInt, paramColorStateList, paramFloat1, paramFloat2);
    if (paramColorStateList == null) {
      i = 0;
    } else {
      int[] arrayOfInt = getState();
      i = paramColorStateList.getColorForState(arrayOfInt, 0);
    } 
    setStrokeInternal(paramInt, i, paramFloat1, paramFloat2);
  }
  
  private void setStrokeInternal(int paramInt1, int paramInt2, float paramFloat1, float paramFloat2) {
    if (this.mStrokePaint == null) {
      Paint paint = new Paint(1);
      paint.setStyle(Paint.Style.STROKE);
    } 
    this.mStrokePaint.setStrokeWidth(paramInt1);
    this.mStrokePaint.setColor(paramInt2);
    DashPathEffect dashPathEffect = null;
    if (paramFloat1 > 0.0F)
      dashPathEffect = new DashPathEffect(new float[] { paramFloat1, paramFloat2 }, 0.0F); 
    this.mStrokePaint.setPathEffect(dashPathEffect);
    this.mGradientIsDirty = true;
    invalidateSelf();
  }
  
  public void setSize(int paramInt1, int paramInt2) {
    this.mGradientState.setSize(paramInt1, paramInt2);
    this.mPathIsDirty = true;
    invalidateSelf();
  }
  
  public void setShape(int paramInt) {
    this.mRingPath = null;
    this.mPathIsDirty = true;
    this.mGradientState.setShape(paramInt);
    invalidateSelf();
  }
  
  public int getShape() {
    return this.mGradientState.mShape;
  }
  
  public void setGradientType(int paramInt) {
    this.mGradientState.setGradientType(paramInt);
    this.mGradientIsDirty = true;
    invalidateSelf();
  }
  
  public int getGradientType() {
    return this.mGradientState.mGradient;
  }
  
  public void setGradientCenter(float paramFloat1, float paramFloat2) {
    this.mGradientState.setGradientCenter(paramFloat1, paramFloat2);
    this.mGradientIsDirty = true;
    invalidateSelf();
  }
  
  public float getGradientCenterX() {
    return this.mGradientState.mCenterX;
  }
  
  public float getGradientCenterY() {
    return this.mGradientState.mCenterY;
  }
  
  public void setGradientRadius(float paramFloat) {
    this.mGradientState.setGradientRadius(paramFloat, 0);
    this.mGradientIsDirty = true;
    invalidateSelf();
  }
  
  public float getGradientRadius() {
    if (this.mGradientState.mGradient != 1)
      return 0.0F; 
    ensureValidRect();
    return this.mGradientRadius;
  }
  
  public void setUseLevel(boolean paramBoolean) {
    this.mGradientState.mUseLevel = paramBoolean;
    this.mGradientIsDirty = true;
    invalidateSelf();
  }
  
  public boolean getUseLevel() {
    return this.mGradientState.mUseLevel;
  }
  
  private int modulateAlpha(int paramInt) {
    int i = this.mAlpha;
    return paramInt * (i + (i >> 7)) >> 8;
  }
  
  public Orientation getOrientation() {
    return this.mGradientState.mOrientation;
  }
  
  public void setOrientation(Orientation paramOrientation) {
    this.mGradientState.mOrientation = paramOrientation;
    this.mGradientIsDirty = true;
    invalidateSelf();
  }
  
  public void setColors(int[] paramArrayOfint) {
    setColors(paramArrayOfint, (float[])null);
  }
  
  public void setColors(int[] paramArrayOfint, float[] paramArrayOffloat) {
    this.mGradientState.setGradientColors(paramArrayOfint);
    this.mGradientState.mPositions = paramArrayOffloat;
    this.mGradientIsDirty = true;
    invalidateSelf();
  }
  
  public int[] getColors() {
    int[] arrayOfInt;
    if (this.mGradientState.mGradientColors == null) {
      arrayOfInt = null;
    } else {
      arrayOfInt = (int[])this.mGradientState.mGradientColors.clone();
    } 
    return arrayOfInt;
  }
  
  public void draw(Canvas paramCanvas) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial ensureValidRect : ()Z
    //   4: ifne -> 8
    //   7: return
    //   8: aload_0
    //   9: getfield mFillPaint : Landroid/graphics/Paint;
    //   12: invokevirtual getAlpha : ()I
    //   15: istore_2
    //   16: aload_0
    //   17: getfield mStrokePaint : Landroid/graphics/Paint;
    //   20: astore_3
    //   21: aload_3
    //   22: ifnull -> 34
    //   25: aload_3
    //   26: invokevirtual getAlpha : ()I
    //   29: istore #4
    //   31: goto -> 37
    //   34: iconst_0
    //   35: istore #4
    //   37: aload_0
    //   38: iload_2
    //   39: invokespecial modulateAlpha : (I)I
    //   42: istore #5
    //   44: aload_0
    //   45: iload #4
    //   47: invokespecial modulateAlpha : (I)I
    //   50: istore #6
    //   52: iload #6
    //   54: ifle -> 81
    //   57: aload_0
    //   58: getfield mStrokePaint : Landroid/graphics/Paint;
    //   61: astore_3
    //   62: aload_3
    //   63: ifnull -> 81
    //   66: aload_3
    //   67: invokevirtual getStrokeWidth : ()F
    //   70: fconst_0
    //   71: fcmpl
    //   72: ifle -> 81
    //   75: iconst_1
    //   76: istore #7
    //   78: goto -> 84
    //   81: iconst_0
    //   82: istore #7
    //   84: iload #5
    //   86: ifle -> 95
    //   89: iconst_1
    //   90: istore #8
    //   92: goto -> 98
    //   95: iconst_0
    //   96: istore #8
    //   98: aload_0
    //   99: getfield mGradientState : Landroid/graphics/drawable/GradientDrawable$GradientState;
    //   102: astore #9
    //   104: aload_0
    //   105: getfield mColorFilter : Landroid/graphics/ColorFilter;
    //   108: astore_3
    //   109: aload_3
    //   110: ifnull -> 116
    //   113: goto -> 121
    //   116: aload_0
    //   117: getfield mBlendModeColorFilter : Landroid/graphics/BlendModeColorFilter;
    //   120: astore_3
    //   121: iload #7
    //   123: ifeq -> 168
    //   126: iload #8
    //   128: ifeq -> 168
    //   131: aload #9
    //   133: getfield mShape : I
    //   136: iconst_2
    //   137: if_icmpeq -> 168
    //   140: iload #6
    //   142: sipush #255
    //   145: if_icmpge -> 168
    //   148: aload_0
    //   149: getfield mAlpha : I
    //   152: sipush #255
    //   155: if_icmplt -> 162
    //   158: aload_3
    //   159: ifnull -> 168
    //   162: iconst_1
    //   163: istore #8
    //   165: goto -> 171
    //   168: iconst_0
    //   169: istore #8
    //   171: iload #8
    //   173: ifeq -> 305
    //   176: aload_0
    //   177: getfield mLayerPaint : Landroid/graphics/Paint;
    //   180: ifnonnull -> 194
    //   183: aload_0
    //   184: new android/graphics/Paint
    //   187: dup
    //   188: invokespecial <init> : ()V
    //   191: putfield mLayerPaint : Landroid/graphics/Paint;
    //   194: aload_0
    //   195: getfield mLayerPaint : Landroid/graphics/Paint;
    //   198: aload #9
    //   200: getfield mDither : Z
    //   203: invokevirtual setDither : (Z)V
    //   206: aload_0
    //   207: getfield mLayerPaint : Landroid/graphics/Paint;
    //   210: aload_0
    //   211: getfield mAlpha : I
    //   214: invokevirtual setAlpha : (I)V
    //   217: aload_0
    //   218: getfield mLayerPaint : Landroid/graphics/Paint;
    //   221: aload_3
    //   222: invokevirtual setColorFilter : (Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
    //   225: pop
    //   226: aload_0
    //   227: getfield mStrokePaint : Landroid/graphics/Paint;
    //   230: invokevirtual getStrokeWidth : ()F
    //   233: fstore #10
    //   235: aload_1
    //   236: aload_0
    //   237: getfield mRect : Landroid/graphics/RectF;
    //   240: getfield left : F
    //   243: fload #10
    //   245: fsub
    //   246: aload_0
    //   247: getfield mRect : Landroid/graphics/RectF;
    //   250: getfield top : F
    //   253: fload #10
    //   255: fsub
    //   256: aload_0
    //   257: getfield mRect : Landroid/graphics/RectF;
    //   260: getfield right : F
    //   263: fload #10
    //   265: fadd
    //   266: aload_0
    //   267: getfield mRect : Landroid/graphics/RectF;
    //   270: getfield bottom : F
    //   273: fload #10
    //   275: fadd
    //   276: aload_0
    //   277: getfield mLayerPaint : Landroid/graphics/Paint;
    //   280: invokevirtual saveLayer : (FFFFLandroid/graphics/Paint;)I
    //   283: pop
    //   284: aload_0
    //   285: getfield mFillPaint : Landroid/graphics/Paint;
    //   288: aconst_null
    //   289: invokevirtual setColorFilter : (Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
    //   292: pop
    //   293: aload_0
    //   294: getfield mStrokePaint : Landroid/graphics/Paint;
    //   297: aconst_null
    //   298: invokevirtual setColorFilter : (Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
    //   301: pop
    //   302: goto -> 396
    //   305: aload_0
    //   306: getfield mFillPaint : Landroid/graphics/Paint;
    //   309: iload #5
    //   311: invokevirtual setAlpha : (I)V
    //   314: aload_0
    //   315: getfield mFillPaint : Landroid/graphics/Paint;
    //   318: aload #9
    //   320: getfield mDither : Z
    //   323: invokevirtual setDither : (Z)V
    //   326: aload_0
    //   327: getfield mFillPaint : Landroid/graphics/Paint;
    //   330: aload_3
    //   331: invokevirtual setColorFilter : (Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
    //   334: pop
    //   335: aload_3
    //   336: ifnull -> 361
    //   339: aload #9
    //   341: getfield mSolidColors : Landroid/content/res/ColorStateList;
    //   344: ifnonnull -> 361
    //   347: aload_0
    //   348: getfield mFillPaint : Landroid/graphics/Paint;
    //   351: aload_0
    //   352: getfield mAlpha : I
    //   355: bipush #24
    //   357: ishl
    //   358: invokevirtual setColor : (I)V
    //   361: iload #7
    //   363: ifeq -> 396
    //   366: aload_0
    //   367: getfield mStrokePaint : Landroid/graphics/Paint;
    //   370: iload #6
    //   372: invokevirtual setAlpha : (I)V
    //   375: aload_0
    //   376: getfield mStrokePaint : Landroid/graphics/Paint;
    //   379: aload #9
    //   381: getfield mDither : Z
    //   384: invokevirtual setDither : (Z)V
    //   387: aload_0
    //   388: getfield mStrokePaint : Landroid/graphics/Paint;
    //   391: aload_3
    //   392: invokevirtual setColorFilter : (Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
    //   395: pop
    //   396: aload #9
    //   398: getfield mShape : I
    //   401: istore #5
    //   403: iload #5
    //   405: ifeq -> 542
    //   408: iload #5
    //   410: iconst_1
    //   411: if_icmpeq -> 507
    //   414: iload #5
    //   416: iconst_2
    //   417: if_icmpeq -> 465
    //   420: iload #5
    //   422: iconst_3
    //   423: if_icmpeq -> 429
    //   426: goto -> 737
    //   429: aload_0
    //   430: aload #9
    //   432: invokespecial buildRing : (Landroid/graphics/drawable/GradientDrawable$GradientState;)Landroid/graphics/Path;
    //   435: astore_3
    //   436: aload_1
    //   437: aload_3
    //   438: aload_0
    //   439: getfield mFillPaint : Landroid/graphics/Paint;
    //   442: invokevirtual drawPath : (Landroid/graphics/Path;Landroid/graphics/Paint;)V
    //   445: iload #7
    //   447: ifeq -> 462
    //   450: aload_1
    //   451: aload_3
    //   452: aload_0
    //   453: getfield mStrokePaint : Landroid/graphics/Paint;
    //   456: invokevirtual drawPath : (Landroid/graphics/Path;Landroid/graphics/Paint;)V
    //   459: goto -> 737
    //   462: goto -> 737
    //   465: aload_0
    //   466: getfield mRect : Landroid/graphics/RectF;
    //   469: astore_3
    //   470: aload_3
    //   471: invokevirtual centerY : ()F
    //   474: fstore #10
    //   476: iload #7
    //   478: ifeq -> 504
    //   481: aload_1
    //   482: aload_3
    //   483: getfield left : F
    //   486: fload #10
    //   488: aload_3
    //   489: getfield right : F
    //   492: fload #10
    //   494: aload_0
    //   495: getfield mStrokePaint : Landroid/graphics/Paint;
    //   498: invokevirtual drawLine : (FFFFLandroid/graphics/Paint;)V
    //   501: goto -> 737
    //   504: goto -> 737
    //   507: aload_1
    //   508: aload_0
    //   509: getfield mRect : Landroid/graphics/RectF;
    //   512: aload_0
    //   513: getfield mFillPaint : Landroid/graphics/Paint;
    //   516: invokevirtual drawOval : (Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    //   519: iload #7
    //   521: ifeq -> 539
    //   524: aload_1
    //   525: aload_0
    //   526: getfield mRect : Landroid/graphics/RectF;
    //   529: aload_0
    //   530: getfield mStrokePaint : Landroid/graphics/Paint;
    //   533: invokevirtual drawOval : (Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    //   536: goto -> 737
    //   539: goto -> 737
    //   542: aload #9
    //   544: getfield mRadiusArray : [F
    //   547: ifnull -> 589
    //   550: aload_0
    //   551: invokespecial buildPathIfDirty : ()V
    //   554: aload_1
    //   555: aload_0
    //   556: getfield mPath : Landroid/graphics/Path;
    //   559: aload_0
    //   560: getfield mFillPaint : Landroid/graphics/Paint;
    //   563: invokevirtual drawPath : (Landroid/graphics/Path;Landroid/graphics/Paint;)V
    //   566: iload #7
    //   568: ifeq -> 586
    //   571: aload_1
    //   572: aload_0
    //   573: getfield mPath : Landroid/graphics/Path;
    //   576: aload_0
    //   577: getfield mStrokePaint : Landroid/graphics/Paint;
    //   580: invokevirtual drawPath : (Landroid/graphics/Path;Landroid/graphics/Paint;)V
    //   583: goto -> 737
    //   586: goto -> 737
    //   589: aload #9
    //   591: getfield mRadius : F
    //   594: fconst_0
    //   595: fcmpl
    //   596: ifle -> 682
    //   599: aload #9
    //   601: getfield mRadius : F
    //   604: fstore #11
    //   606: aload_0
    //   607: getfield mRect : Landroid/graphics/RectF;
    //   610: astore_3
    //   611: aload_3
    //   612: invokevirtual width : ()F
    //   615: aload_0
    //   616: getfield mRect : Landroid/graphics/RectF;
    //   619: invokevirtual height : ()F
    //   622: invokestatic min : (FF)F
    //   625: fstore #10
    //   627: fload #11
    //   629: fload #10
    //   631: ldc_w 0.5
    //   634: fmul
    //   635: invokestatic min : (FF)F
    //   638: fstore #10
    //   640: getstatic android/drawable/IOplusGradientHooks.DEFAULT : Landroid/drawable/IOplusGradientHooks;
    //   643: iconst_0
    //   644: anewarray java/lang/Object
    //   647: invokestatic getOrCreate : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   650: checkcast android/drawable/IOplusGradientHooks
    //   653: aload_0
    //   654: getfield mSmoothRound : Z
    //   657: aload_1
    //   658: aload_0
    //   659: getfield mRect : Landroid/graphics/RectF;
    //   662: fload #10
    //   664: iload #7
    //   666: aload_0
    //   667: getfield mFillPaint : Landroid/graphics/Paint;
    //   670: aload_0
    //   671: getfield mStrokePaint : Landroid/graphics/Paint;
    //   674: invokeinterface drawRoundRect : (ZLandroid/graphics/Canvas;Landroid/graphics/RectF;FZLandroid/graphics/Paint;Landroid/graphics/Paint;)V
    //   679: goto -> 737
    //   682: aload_0
    //   683: getfield mFillPaint : Landroid/graphics/Paint;
    //   686: invokevirtual getColor : ()I
    //   689: ifne -> 708
    //   692: aload_3
    //   693: ifnonnull -> 708
    //   696: aload_0
    //   697: getfield mFillPaint : Landroid/graphics/Paint;
    //   700: astore_3
    //   701: aload_3
    //   702: invokevirtual getShader : ()Landroid/graphics/Shader;
    //   705: ifnull -> 720
    //   708: aload_1
    //   709: aload_0
    //   710: getfield mRect : Landroid/graphics/RectF;
    //   713: aload_0
    //   714: getfield mFillPaint : Landroid/graphics/Paint;
    //   717: invokevirtual drawRect : (Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    //   720: iload #7
    //   722: ifeq -> 737
    //   725: aload_1
    //   726: aload_0
    //   727: getfield mRect : Landroid/graphics/RectF;
    //   730: aload_0
    //   731: getfield mStrokePaint : Landroid/graphics/Paint;
    //   734: invokevirtual drawRect : (Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    //   737: iload #8
    //   739: ifeq -> 749
    //   742: aload_1
    //   743: invokevirtual restore : ()V
    //   746: goto -> 771
    //   749: aload_0
    //   750: getfield mFillPaint : Landroid/graphics/Paint;
    //   753: iload_2
    //   754: invokevirtual setAlpha : (I)V
    //   757: iload #7
    //   759: ifeq -> 771
    //   762: aload_0
    //   763: getfield mStrokePaint : Landroid/graphics/Paint;
    //   766: iload #4
    //   768: invokevirtual setAlpha : (I)V
    //   771: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #742	-> 0
    //   #744	-> 7
    //   #749	-> 8
    //   #750	-> 16
    //   #752	-> 37
    //   #753	-> 44
    //   #755	-> 52
    //   #756	-> 66
    //   #757	-> 84
    //   #758	-> 98
    //   #759	-> 104
    //   #766	-> 121
    //   #776	-> 171
    //   #777	-> 176
    //   #778	-> 183
    //   #780	-> 194
    //   #781	-> 206
    //   #782	-> 217
    //   #784	-> 226
    //   #785	-> 235
    //   #791	-> 284
    //   #792	-> 293
    //   #793	-> 302
    //   #797	-> 305
    //   #798	-> 314
    //   #799	-> 326
    //   #800	-> 335
    //   #801	-> 347
    //   #803	-> 361
    //   #804	-> 366
    //   #805	-> 375
    //   #806	-> 387
    //   #810	-> 396
    //   #862	-> 429
    //   #863	-> 436
    //   #864	-> 445
    //   #865	-> 450
    //   #864	-> 462
    //   #854	-> 465
    //   #855	-> 470
    //   #856	-> 476
    //   #857	-> 481
    //   #856	-> 504
    //   #848	-> 507
    //   #849	-> 519
    //   #850	-> 524
    //   #849	-> 539
    //   #812	-> 542
    //   #813	-> 550
    //   #814	-> 554
    //   #815	-> 566
    //   #816	-> 571
    //   #815	-> 586
    //   #818	-> 589
    //   #824	-> 599
    //   #825	-> 611
    //   #824	-> 627
    //   #835	-> 640
    //   #837	-> 679
    //   #838	-> 682
    //   #839	-> 701
    //   #840	-> 708
    //   #842	-> 720
    //   #843	-> 725
    //   #870	-> 737
    //   #871	-> 742
    //   #873	-> 749
    //   #874	-> 757
    //   #875	-> 762
    //   #878	-> 771
  }
  
  public void setXfermode(Xfermode paramXfermode) {
    super.setXfermode(paramXfermode);
    this.mFillPaint.setXfermode(paramXfermode);
  }
  
  public void setAntiAlias(boolean paramBoolean) {
    this.mFillPaint.setAntiAlias(paramBoolean);
  }
  
  private void buildPathIfDirty() {
    GradientState gradientState = this.mGradientState;
    if (this.mPathIsDirty) {
      ensureValidRect();
      this.mPath.reset();
      this.mPath.addRoundRect(this.mRect, gradientState.mRadiusArray, Path.Direction.CW);
      this.mPathIsDirty = false;
    } 
  }
  
  public void setInnerRadiusRatio(float paramFloat) {
    if (paramFloat > 0.0F) {
      this.mGradientState.mInnerRadiusRatio = paramFloat;
      this.mPathIsDirty = true;
      invalidateSelf();
      return;
    } 
    throw new IllegalArgumentException("Ratio must be greater than zero");
  }
  
  public float getInnerRadiusRatio() {
    return this.mGradientState.mInnerRadiusRatio;
  }
  
  public void setInnerRadius(int paramInt) {
    this.mGradientState.mInnerRadius = paramInt;
    this.mPathIsDirty = true;
    invalidateSelf();
  }
  
  public int getInnerRadius() {
    return this.mGradientState.mInnerRadius;
  }
  
  public void setThicknessRatio(float paramFloat) {
    if (paramFloat > 0.0F) {
      this.mGradientState.mThicknessRatio = paramFloat;
      this.mPathIsDirty = true;
      invalidateSelf();
      return;
    } 
    throw new IllegalArgumentException("Ratio must be greater than zero");
  }
  
  public float getThicknessRatio() {
    return this.mGradientState.mThicknessRatio;
  }
  
  public void setThickness(int paramInt) {
    this.mGradientState.mThickness = paramInt;
    this.mPathIsDirty = true;
    invalidateSelf();
  }
  
  public int getThickness() {
    return this.mGradientState.mThickness;
  }
  
  public void setPadding(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (this.mGradientState.mPadding == null)
      this.mGradientState.mPadding = new Rect(); 
    this.mGradientState.mPadding.set(paramInt1, paramInt2, paramInt3, paramInt4);
    this.mPadding = this.mGradientState.mPadding;
    invalidateSelf();
  }
  
  private Path buildRing(GradientState paramGradientState) {
    float f1, f4, f5;
    if (this.mRingPath != null && (!paramGradientState.mUseLevelForShape || !this.mPathIsDirty))
      return this.mRingPath; 
    this.mPathIsDirty = false;
    if (paramGradientState.mUseLevelForShape) {
      f1 = getLevel() * 360.0F / 10000.0F;
    } else {
      f1 = 360.0F;
    } 
    RectF rectF2 = new RectF(this.mRect);
    float f2 = rectF2.width() / 2.0F;
    float f3 = rectF2.height() / 2.0F;
    if (paramGradientState.mThickness != -1) {
      f4 = paramGradientState.mThickness;
    } else {
      f4 = rectF2.width() / paramGradientState.mThicknessRatio;
    } 
    if (paramGradientState.mInnerRadius != -1) {
      f5 = paramGradientState.mInnerRadius;
    } else {
      f5 = rectF2.width() / paramGradientState.mInnerRadiusRatio;
    } 
    RectF rectF1 = new RectF(rectF2);
    rectF1.inset(f2 - f5, f3 - f5);
    rectF2 = new RectF(rectF1);
    rectF2.inset(-f4, -f4);
    Path path = this.mRingPath;
    if (path == null) {
      this.mRingPath = new Path();
    } else {
      path.reset();
    } 
    path = this.mRingPath;
    if (f1 < 360.0F && f1 > -360.0F) {
      path.setFillType(Path.FillType.EVEN_ODD);
      path.moveTo(f2 + f5, f3);
      path.lineTo(f2 + f5 + f4, f3);
      path.arcTo(rectF2, 0.0F, f1, false);
      path.arcTo(rectF1, f1, -f1, false);
      path.close();
    } else {
      path.addOval(rectF2, Path.Direction.CW);
      path.addOval(rectF1, Path.Direction.CCW);
    } 
    return path;
  }
  
  public void setColor(int paramInt) {
    this.mGradientState.setSolidColors(ColorStateList.valueOf(paramInt));
    this.mFillPaint.setColor(paramInt);
    invalidateSelf();
  }
  
  public void setColor(ColorStateList paramColorStateList) {
    if (paramColorStateList == null) {
      setColor(0);
    } else {
      int[] arrayOfInt = getState();
      int i = paramColorStateList.getColorForState(arrayOfInt, 0);
      this.mGradientState.setSolidColors(paramColorStateList);
      this.mFillPaint.setColor(i);
      invalidateSelf();
    } 
  }
  
  public ColorStateList getColor() {
    return this.mGradientState.mSolidColors;
  }
  
  public void setSmoothRoundStyle(boolean paramBoolean) {
    this.mSmoothRound = paramBoolean;
  }
  
  protected boolean onStateChange(int[] paramArrayOfint) {
    boolean bool1 = false;
    GradientState gradientState = this.mGradientState;
    ColorStateList colorStateList = gradientState.mSolidColors;
    boolean bool2 = bool1;
    if (colorStateList != null) {
      int i = colorStateList.getColorForState(paramArrayOfint, 0);
      int j = this.mFillPaint.getColor();
      bool2 = bool1;
      if (j != i) {
        this.mFillPaint.setColor(i);
        bool2 = true;
      } 
    } 
    Paint paint = this.mStrokePaint;
    bool1 = bool2;
    if (paint != null) {
      colorStateList = gradientState.mStrokeColors;
      bool1 = bool2;
      if (colorStateList != null) {
        int i = colorStateList.getColorForState(paramArrayOfint, 0);
        int j = paint.getColor();
        bool1 = bool2;
        if (j != i) {
          paint.setColor(i);
          bool1 = true;
        } 
      } 
    } 
    bool2 = bool1;
    if (gradientState.mTint != null) {
      bool2 = bool1;
      if (gradientState.mBlendMode != null) {
        this.mBlendModeColorFilter = updateBlendModeFilter(this.mBlendModeColorFilter, gradientState.mTint, gradientState.mBlendMode);
        bool2 = true;
      } 
    } 
    if (bool2) {
      invalidateSelf();
      return true;
    } 
    return false;
  }
  
  public boolean isStateful() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mGradientState : Landroid/graphics/drawable/GradientDrawable$GradientState;
    //   4: astore_1
    //   5: aload_0
    //   6: invokespecial isStateful : ()Z
    //   9: ifne -> 77
    //   12: aload_1
    //   13: getfield mSolidColors : Landroid/content/res/ColorStateList;
    //   16: ifnull -> 31
    //   19: aload_1
    //   20: getfield mSolidColors : Landroid/content/res/ColorStateList;
    //   23: astore_2
    //   24: aload_2
    //   25: invokevirtual isStateful : ()Z
    //   28: ifne -> 77
    //   31: aload_1
    //   32: getfield mStrokeColors : Landroid/content/res/ColorStateList;
    //   35: ifnull -> 50
    //   38: aload_1
    //   39: getfield mStrokeColors : Landroid/content/res/ColorStateList;
    //   42: astore_2
    //   43: aload_2
    //   44: invokevirtual isStateful : ()Z
    //   47: ifne -> 77
    //   50: aload_1
    //   51: getfield mTint : Landroid/content/res/ColorStateList;
    //   54: ifnull -> 72
    //   57: aload_1
    //   58: getfield mTint : Landroid/content/res/ColorStateList;
    //   61: astore_1
    //   62: aload_1
    //   63: invokevirtual isStateful : ()Z
    //   66: ifeq -> 72
    //   69: goto -> 77
    //   72: iconst_0
    //   73: istore_3
    //   74: goto -> 79
    //   77: iconst_1
    //   78: istore_3
    //   79: iload_3
    //   80: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1187	-> 0
    //   #1188	-> 5
    //   #1189	-> 24
    //   #1190	-> 43
    //   #1191	-> 62
    //   #1188	-> 79
  }
  
  public boolean hasFocusStateSpecified() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mGradientState : Landroid/graphics/drawable/GradientDrawable$GradientState;
    //   4: astore_1
    //   5: aload_1
    //   6: getfield mSolidColors : Landroid/content/res/ColorStateList;
    //   9: ifnull -> 22
    //   12: aload_1
    //   13: getfield mSolidColors : Landroid/content/res/ColorStateList;
    //   16: invokevirtual hasFocusStateSpecified : ()Z
    //   19: ifne -> 60
    //   22: aload_1
    //   23: getfield mStrokeColors : Landroid/content/res/ColorStateList;
    //   26: ifnull -> 41
    //   29: aload_1
    //   30: getfield mStrokeColors : Landroid/content/res/ColorStateList;
    //   33: astore_2
    //   34: aload_2
    //   35: invokevirtual hasFocusStateSpecified : ()Z
    //   38: ifne -> 60
    //   41: aload_1
    //   42: getfield mTint : Landroid/content/res/ColorStateList;
    //   45: ifnull -> 65
    //   48: aload_1
    //   49: getfield mTint : Landroid/content/res/ColorStateList;
    //   52: astore_2
    //   53: aload_2
    //   54: invokevirtual hasFocusStateSpecified : ()Z
    //   57: ifeq -> 65
    //   60: iconst_1
    //   61: istore_3
    //   62: goto -> 67
    //   65: iconst_0
    //   66: istore_3
    //   67: iload_3
    //   68: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1197	-> 0
    //   #1198	-> 5
    //   #1199	-> 34
    //   #1200	-> 53
    //   #1198	-> 67
  }
  
  public int getChangingConfigurations() {
    return super.getChangingConfigurations() | this.mGradientState.getChangingConfigurations();
  }
  
  public void setAlpha(int paramInt) {
    if (paramInt != this.mAlpha) {
      this.mAlpha = paramInt;
      invalidateSelf();
    } 
  }
  
  public int getAlpha() {
    return this.mAlpha;
  }
  
  public void setDither(boolean paramBoolean) {
    if (paramBoolean != this.mGradientState.mDither) {
      this.mGradientState.mDither = paramBoolean;
      invalidateSelf();
    } 
  }
  
  public ColorFilter getColorFilter() {
    return this.mColorFilter;
  }
  
  public void setColorFilter(ColorFilter paramColorFilter) {
    if (paramColorFilter != this.mColorFilter) {
      this.mColorFilter = paramColorFilter;
      invalidateSelf();
    } 
  }
  
  public void setTintList(ColorStateList paramColorStateList) {
    this.mGradientState.mTint = paramColorStateList;
    BlendModeColorFilter blendModeColorFilter = this.mBlendModeColorFilter;
    BlendMode blendMode = this.mGradientState.mBlendMode;
    this.mBlendModeColorFilter = updateBlendModeFilter(blendModeColorFilter, paramColorStateList, blendMode);
    invalidateSelf();
  }
  
  public void setTintBlendMode(BlendMode paramBlendMode) {
    this.mGradientState.mBlendMode = paramBlendMode;
    this.mBlendModeColorFilter = updateBlendModeFilter(this.mBlendModeColorFilter, this.mGradientState.mTint, paramBlendMode);
    invalidateSelf();
  }
  
  public int getOpacity() {
    byte b;
    if (this.mAlpha == 255 && this.mGradientState.mOpaqueOverBounds && isOpaqueForState()) {
      b = -1;
    } else {
      b = -3;
    } 
    return b;
  }
  
  protected void onBoundsChange(Rect paramRect) {
    super.onBoundsChange(paramRect);
    this.mRingPath = null;
    this.mPathIsDirty = true;
    this.mGradientIsDirty = true;
  }
  
  protected boolean onLevelChange(int paramInt) {
    super.onLevelChange(paramInt);
    this.mGradientIsDirty = true;
    this.mPathIsDirty = true;
    invalidateSelf();
    return true;
  }
  
  private boolean ensureValidRect() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mGradientIsDirty : Z
    //   4: ifeq -> 1029
    //   7: aload_0
    //   8: iconst_0
    //   9: putfield mGradientIsDirty : Z
    //   12: aload_0
    //   13: invokevirtual getBounds : ()Landroid/graphics/Rect;
    //   16: astore_1
    //   17: fconst_0
    //   18: fstore_2
    //   19: aload_0
    //   20: getfield mStrokePaint : Landroid/graphics/Paint;
    //   23: astore_3
    //   24: aload_3
    //   25: ifnull -> 37
    //   28: aload_3
    //   29: invokevirtual getStrokeWidth : ()F
    //   32: ldc_w 0.5
    //   35: fmul
    //   36: fstore_2
    //   37: aload_0
    //   38: getfield mGradientState : Landroid/graphics/drawable/GradientDrawable$GradientState;
    //   41: astore #4
    //   43: aload_0
    //   44: getfield mRect : Landroid/graphics/RectF;
    //   47: aload_1
    //   48: getfield left : I
    //   51: i2f
    //   52: fload_2
    //   53: fadd
    //   54: aload_1
    //   55: getfield top : I
    //   58: i2f
    //   59: fload_2
    //   60: fadd
    //   61: aload_1
    //   62: getfield right : I
    //   65: i2f
    //   66: fload_2
    //   67: fsub
    //   68: aload_1
    //   69: getfield bottom : I
    //   72: i2f
    //   73: fload_2
    //   74: fsub
    //   75: invokevirtual set : (FFFF)V
    //   78: aload #4
    //   80: getfield mGradientColors : [I
    //   83: astore #5
    //   85: aload #5
    //   87: ifnull -> 1029
    //   90: aload_0
    //   91: getfield mRect : Landroid/graphics/RectF;
    //   94: astore_1
    //   95: aload #4
    //   97: getfield mGradient : I
    //   100: istore #6
    //   102: fconst_1
    //   103: fstore_2
    //   104: iload #6
    //   106: ifne -> 492
    //   109: aload #4
    //   111: getfield mUseLevel : Z
    //   114: ifeq -> 126
    //   117: aload_0
    //   118: invokevirtual getLevel : ()I
    //   121: i2f
    //   122: ldc 10000.0
    //   124: fdiv
    //   125: fstore_2
    //   126: getstatic android/graphics/drawable/GradientDrawable$1.$SwitchMap$android$graphics$drawable$GradientDrawable$Orientation : [I
    //   129: aload #4
    //   131: getfield mOrientation : Landroid/graphics/drawable/GradientDrawable$Orientation;
    //   134: invokevirtual ordinal : ()I
    //   137: iaload
    //   138: tableswitch default -> 180, 1 -> 426, 2 -> 388, 3 -> 358, 4 -> 320, 5 -> 282, 6 -> 244, 7 -> 214
    //   180: aload_1
    //   181: getfield left : F
    //   184: fstore #7
    //   186: aload_1
    //   187: getfield top : F
    //   190: fstore #8
    //   192: aload_1
    //   193: getfield right : F
    //   196: fload_2
    //   197: fmul
    //   198: fstore #9
    //   200: aload_1
    //   201: getfield bottom : F
    //   204: fload_2
    //   205: fmul
    //   206: fstore #10
    //   208: fload #7
    //   210: fstore_2
    //   211: goto -> 457
    //   214: aload_1
    //   215: getfield left : F
    //   218: fstore #7
    //   220: aload_1
    //   221: getfield top : F
    //   224: fstore #8
    //   226: aload_1
    //   227: getfield right : F
    //   230: fload_2
    //   231: fmul
    //   232: fstore #9
    //   234: fload #8
    //   236: fstore #10
    //   238: fload #7
    //   240: fstore_2
    //   241: goto -> 457
    //   244: aload_1
    //   245: getfield left : F
    //   248: fstore #7
    //   250: aload_1
    //   251: getfield bottom : F
    //   254: fstore #8
    //   256: aload_1
    //   257: getfield right : F
    //   260: fload_2
    //   261: fmul
    //   262: fstore #9
    //   264: aload_1
    //   265: getfield top : F
    //   268: fstore #10
    //   270: fload #10
    //   272: fload_2
    //   273: fmul
    //   274: fstore #10
    //   276: fload #7
    //   278: fstore_2
    //   279: goto -> 457
    //   282: aload_1
    //   283: getfield left : F
    //   286: fstore #8
    //   288: aload_1
    //   289: getfield bottom : F
    //   292: fstore #7
    //   294: fload #8
    //   296: fstore #9
    //   298: aload_1
    //   299: getfield top : F
    //   302: fstore #10
    //   304: fload #10
    //   306: fload_2
    //   307: fmul
    //   308: fstore #10
    //   310: fload #8
    //   312: fstore_2
    //   313: fload #7
    //   315: fstore #8
    //   317: goto -> 457
    //   320: aload_1
    //   321: getfield right : F
    //   324: fstore #7
    //   326: aload_1
    //   327: getfield bottom : F
    //   330: fstore #8
    //   332: aload_1
    //   333: getfield left : F
    //   336: fload_2
    //   337: fmul
    //   338: fstore #9
    //   340: aload_1
    //   341: getfield top : F
    //   344: fstore #10
    //   346: fload #10
    //   348: fload_2
    //   349: fmul
    //   350: fstore #10
    //   352: fload #7
    //   354: fstore_2
    //   355: goto -> 457
    //   358: aload_1
    //   359: getfield right : F
    //   362: fstore #7
    //   364: aload_1
    //   365: getfield top : F
    //   368: fstore #8
    //   370: aload_1
    //   371: getfield left : F
    //   374: fload_2
    //   375: fmul
    //   376: fstore #9
    //   378: fload #8
    //   380: fstore #10
    //   382: fload #7
    //   384: fstore_2
    //   385: goto -> 457
    //   388: aload_1
    //   389: getfield right : F
    //   392: fstore #7
    //   394: aload_1
    //   395: getfield top : F
    //   398: fstore #8
    //   400: aload_1
    //   401: getfield left : F
    //   404: fload_2
    //   405: fmul
    //   406: fstore #9
    //   408: aload_1
    //   409: getfield bottom : F
    //   412: fstore #10
    //   414: fload #10
    //   416: fload_2
    //   417: fmul
    //   418: fstore #10
    //   420: fload #7
    //   422: fstore_2
    //   423: goto -> 457
    //   426: aload_1
    //   427: getfield left : F
    //   430: fstore #7
    //   432: aload_1
    //   433: getfield top : F
    //   436: fstore #8
    //   438: fload #7
    //   440: fstore #9
    //   442: aload_1
    //   443: getfield bottom : F
    //   446: fstore #10
    //   448: fload #10
    //   450: fload_2
    //   451: fmul
    //   452: fstore #10
    //   454: fload #7
    //   456: fstore_2
    //   457: aload_0
    //   458: getfield mFillPaint : Landroid/graphics/Paint;
    //   461: new android/graphics/LinearGradient
    //   464: dup
    //   465: fload_2
    //   466: fload #8
    //   468: fload #9
    //   470: fload #10
    //   472: aload #5
    //   474: aload #4
    //   476: getfield mPositions : [F
    //   479: getstatic android/graphics/Shader$TileMode.CLAMP : Landroid/graphics/Shader$TileMode;
    //   482: invokespecial <init> : (FFFF[I[FLandroid/graphics/Shader$TileMode;)V
    //   485: invokevirtual setShader : (Landroid/graphics/Shader;)Landroid/graphics/Shader;
    //   488: pop
    //   489: goto -> 1011
    //   492: aload #4
    //   494: getfield mGradient : I
    //   497: iconst_1
    //   498: if_icmpne -> 751
    //   501: aload_1
    //   502: getfield left : F
    //   505: fstore #11
    //   507: aload_1
    //   508: getfield right : F
    //   511: fstore #12
    //   513: aload_1
    //   514: getfield left : F
    //   517: fstore #13
    //   519: aload #4
    //   521: getfield mCenterX : F
    //   524: fstore #7
    //   526: aload_1
    //   527: getfield top : F
    //   530: fstore #14
    //   532: aload_1
    //   533: getfield bottom : F
    //   536: fstore #15
    //   538: aload_1
    //   539: getfield top : F
    //   542: fstore #10
    //   544: aload #4
    //   546: getfield mCenterY : F
    //   549: fstore #16
    //   551: aload #4
    //   553: getfield mGradientRadius : F
    //   556: fstore #9
    //   558: aload #4
    //   560: getfield mGradientRadiusType : I
    //   563: iconst_1
    //   564: if_icmpne -> 629
    //   567: aload #4
    //   569: getfield mWidth : I
    //   572: iflt -> 585
    //   575: aload #4
    //   577: getfield mWidth : I
    //   580: i2f
    //   581: fstore_2
    //   582: goto -> 590
    //   585: aload_1
    //   586: invokevirtual width : ()F
    //   589: fstore_2
    //   590: aload #4
    //   592: getfield mHeight : I
    //   595: iflt -> 609
    //   598: aload #4
    //   600: getfield mHeight : I
    //   603: i2f
    //   604: fstore #8
    //   606: goto -> 615
    //   609: aload_1
    //   610: invokevirtual height : ()F
    //   613: fstore #8
    //   615: fload #9
    //   617: fload_2
    //   618: fload #8
    //   620: invokestatic min : (FF)F
    //   623: fmul
    //   624: fstore #8
    //   626: goto -> 661
    //   629: fload #9
    //   631: fstore #8
    //   633: aload #4
    //   635: getfield mGradientRadiusType : I
    //   638: iconst_2
    //   639: if_icmpne -> 661
    //   642: fload #9
    //   644: aload_1
    //   645: invokevirtual width : ()F
    //   648: aload_1
    //   649: invokevirtual height : ()F
    //   652: invokestatic min : (FF)F
    //   655: fmul
    //   656: fstore #8
    //   658: goto -> 661
    //   661: fload #8
    //   663: fstore_2
    //   664: aload #4
    //   666: getfield mUseLevel : Z
    //   669: ifeq -> 684
    //   672: fload #8
    //   674: aload_0
    //   675: invokevirtual getLevel : ()I
    //   678: i2f
    //   679: ldc 10000.0
    //   681: fdiv
    //   682: fmul
    //   683: fstore_2
    //   684: aload_0
    //   685: fload_2
    //   686: putfield mGradientRadius : F
    //   689: fload_2
    //   690: fstore #8
    //   692: fload_2
    //   693: fconst_0
    //   694: fcmpg
    //   695: ifgt -> 703
    //   698: ldc_w 0.001
    //   701: fstore #8
    //   703: aload_0
    //   704: getfield mFillPaint : Landroid/graphics/Paint;
    //   707: new android/graphics/RadialGradient
    //   710: dup
    //   711: fload #11
    //   713: fload #12
    //   715: fload #13
    //   717: fsub
    //   718: fload #7
    //   720: fmul
    //   721: fadd
    //   722: fload #14
    //   724: fload #15
    //   726: fload #10
    //   728: fsub
    //   729: fload #16
    //   731: fmul
    //   732: fadd
    //   733: fload #8
    //   735: aload #5
    //   737: aconst_null
    //   738: getstatic android/graphics/Shader$TileMode.CLAMP : Landroid/graphics/Shader$TileMode;
    //   741: invokespecial <init> : (FFF[I[FLandroid/graphics/Shader$TileMode;)V
    //   744: invokevirtual setShader : (Landroid/graphics/Shader;)Landroid/graphics/Shader;
    //   747: pop
    //   748: goto -> 1011
    //   751: aload #4
    //   753: getfield mGradient : I
    //   756: iconst_2
    //   757: if_icmpne -> 1011
    //   760: aload_1
    //   761: getfield left : F
    //   764: fstore #14
    //   766: aload_1
    //   767: getfield right : F
    //   770: fstore #8
    //   772: aload_1
    //   773: getfield left : F
    //   776: fstore #11
    //   778: aload #4
    //   780: getfield mCenterX : F
    //   783: fstore_2
    //   784: aload_1
    //   785: getfield top : F
    //   788: fstore #10
    //   790: aload_1
    //   791: getfield bottom : F
    //   794: fstore #9
    //   796: aload_1
    //   797: getfield top : F
    //   800: fstore #13
    //   802: aload #4
    //   804: getfield mCenterY : F
    //   807: fstore #7
    //   809: aload #5
    //   811: astore_1
    //   812: aconst_null
    //   813: astore_3
    //   814: aload #4
    //   816: getfield mUseLevel : Z
    //   819: ifeq -> 973
    //   822: aload #4
    //   824: getfield mTempColors : [I
    //   827: astore_3
    //   828: aload #5
    //   830: arraylength
    //   831: istore #17
    //   833: aload_3
    //   834: ifnull -> 848
    //   837: aload_3
    //   838: astore_1
    //   839: aload_3
    //   840: arraylength
    //   841: iload #17
    //   843: iconst_1
    //   844: iadd
    //   845: if_icmpeq -> 861
    //   848: iload #17
    //   850: iconst_1
    //   851: iadd
    //   852: newarray int
    //   854: astore_1
    //   855: aload #4
    //   857: aload_1
    //   858: putfield mTempColors : [I
    //   861: aload #5
    //   863: iconst_0
    //   864: aload_1
    //   865: iconst_0
    //   866: iload #17
    //   868: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   871: aload_1
    //   872: iload #17
    //   874: aload #5
    //   876: iload #17
    //   878: iconst_1
    //   879: isub
    //   880: iaload
    //   881: iastore
    //   882: aload #4
    //   884: getfield mTempPositions : [F
    //   887: astore #5
    //   889: fconst_1
    //   890: iload #17
    //   892: iconst_1
    //   893: isub
    //   894: i2f
    //   895: fdiv
    //   896: fstore #15
    //   898: aload #5
    //   900: ifnull -> 916
    //   903: aload #5
    //   905: astore_3
    //   906: aload #5
    //   908: arraylength
    //   909: iload #17
    //   911: iconst_1
    //   912: iadd
    //   913: if_icmpeq -> 929
    //   916: iload #17
    //   918: iconst_1
    //   919: iadd
    //   920: newarray float
    //   922: astore_3
    //   923: aload #4
    //   925: aload_3
    //   926: putfield mTempPositions : [F
    //   929: aload_0
    //   930: invokevirtual getLevel : ()I
    //   933: i2f
    //   934: ldc 10000.0
    //   936: fdiv
    //   937: fstore #12
    //   939: iconst_0
    //   940: istore #6
    //   942: iload #6
    //   944: iload #17
    //   946: if_icmpge -> 968
    //   949: aload_3
    //   950: iload #6
    //   952: iload #6
    //   954: i2f
    //   955: fload #15
    //   957: fmul
    //   958: fload #12
    //   960: fmul
    //   961: fastore
    //   962: iinc #6, 1
    //   965: goto -> 942
    //   968: aload_3
    //   969: iload #17
    //   971: fconst_1
    //   972: fastore
    //   973: aload_0
    //   974: getfield mFillPaint : Landroid/graphics/Paint;
    //   977: new android/graphics/SweepGradient
    //   980: dup
    //   981: fload #14
    //   983: fload #8
    //   985: fload #11
    //   987: fsub
    //   988: fload_2
    //   989: fmul
    //   990: fadd
    //   991: fload #10
    //   993: fload #9
    //   995: fload #13
    //   997: fsub
    //   998: fload #7
    //   1000: fmul
    //   1001: fadd
    //   1002: aload_1
    //   1003: aload_3
    //   1004: invokespecial <init> : (FF[I[F)V
    //   1007: invokevirtual setShader : (Landroid/graphics/Shader;)Landroid/graphics/Shader;
    //   1010: pop
    //   1011: aload #4
    //   1013: getfield mSolidColors : Landroid/content/res/ColorStateList;
    //   1016: ifnonnull -> 1029
    //   1019: aload_0
    //   1020: getfield mFillPaint : Landroid/graphics/Paint;
    //   1023: ldc_w -16777216
    //   1026: invokevirtual setColor : (I)V
    //   1029: aload_0
    //   1030: getfield mRect : Landroid/graphics/RectF;
    //   1033: invokevirtual isEmpty : ()Z
    //   1036: iconst_1
    //   1037: ixor
    //   1038: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1289	-> 0
    //   #1290	-> 7
    //   #1292	-> 12
    //   #1293	-> 17
    //   #1295	-> 19
    //   #1296	-> 28
    //   #1299	-> 37
    //   #1301	-> 43
    //   #1304	-> 78
    //   #1305	-> 85
    //   #1306	-> 90
    //   #1309	-> 95
    //   #1310	-> 109
    //   #1311	-> 126
    //   #1341	-> 180
    //   #1342	-> 192
    //   #1337	-> 214
    //   #1338	-> 226
    //   #1339	-> 234
    //   #1333	-> 244
    //   #1334	-> 256
    //   #1335	-> 270
    //   #1329	-> 282
    //   #1330	-> 294
    //   #1331	-> 304
    //   #1325	-> 320
    //   #1326	-> 332
    //   #1327	-> 346
    //   #1321	-> 358
    //   #1322	-> 370
    //   #1323	-> 378
    //   #1317	-> 388
    //   #1318	-> 400
    //   #1319	-> 414
    //   #1313	-> 426
    //   #1314	-> 438
    //   #1315	-> 448
    //   #1346	-> 457
    //   #1348	-> 489
    //   #1349	-> 501
    //   #1350	-> 526
    //   #1352	-> 551
    //   #1353	-> 558
    //   #1356	-> 567
    //   #1357	-> 590
    //   #1358	-> 615
    //   #1359	-> 629
    //   #1360	-> 642
    //   #1359	-> 661
    //   #1363	-> 661
    //   #1364	-> 672
    //   #1367	-> 684
    //   #1369	-> 689
    //   #1372	-> 698
    //   #1375	-> 703
    //   #1377	-> 748
    //   #1378	-> 760
    //   #1379	-> 784
    //   #1381	-> 809
    //   #1382	-> 812
    //   #1384	-> 814
    //   #1385	-> 822
    //   #1386	-> 828
    //   #1387	-> 833
    //   #1388	-> 848
    //   #1390	-> 861
    //   #1391	-> 871
    //   #1393	-> 882
    //   #1394	-> 889
    //   #1395	-> 898
    //   #1396	-> 916
    //   #1399	-> 929
    //   #1400	-> 939
    //   #1401	-> 949
    //   #1400	-> 962
    //   #1403	-> 968
    //   #1406	-> 973
    //   #1411	-> 1011
    //   #1412	-> 1019
    //   #1416	-> 1029
  }
  
  public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme) throws XmlPullParserException, IOException {
    super.inflate(paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
    this.mGradientState.setDensity(Drawable.resolveDensity(paramResources, 0));
    TypedArray typedArray = obtainAttributes(paramResources, paramTheme, paramAttributeSet, R.styleable.GradientDrawable);
    updateStateFromTypedArray(typedArray);
    typedArray.recycle();
    inflateChildElements(paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
    updateLocalState(paramResources);
  }
  
  public void applyTheme(Resources.Theme paramTheme) {
    super.applyTheme(paramTheme);
    GradientState gradientState = this.mGradientState;
    if (gradientState == null)
      return; 
    gradientState.setDensity(Drawable.resolveDensity(paramTheme.getResources(), 0));
    if (gradientState.mThemeAttrs != null) {
      TypedArray typedArray = paramTheme.resolveAttributes(gradientState.mThemeAttrs, R.styleable.GradientDrawable);
      updateStateFromTypedArray(typedArray);
      typedArray.recycle();
    } 
    if (gradientState.mTint != null && gradientState.mTint.canApplyTheme())
      gradientState.mTint = gradientState.mTint.obtainForTheme(paramTheme); 
    if (gradientState.mSolidColors != null && gradientState.mSolidColors.canApplyTheme())
      gradientState.mSolidColors = gradientState.mSolidColors.obtainForTheme(paramTheme); 
    if (gradientState.mStrokeColors != null && gradientState.mStrokeColors.canApplyTheme())
      gradientState.mStrokeColors = gradientState.mStrokeColors.obtainForTheme(paramTheme); 
    applyThemeChildElements(paramTheme);
    updateLocalState(paramTheme.getResources());
  }
  
  private void updateStateFromTypedArray(TypedArray paramTypedArray) {
    GradientState gradientState = this.mGradientState;
    gradientState.mChangingConfigurations |= paramTypedArray.getChangingConfigurations();
    gradientState.mThemeAttrs = paramTypedArray.extractThemeAttrs();
    gradientState.mShape = paramTypedArray.getInt(3, gradientState.mShape);
    gradientState.mDither = paramTypedArray.getBoolean(0, gradientState.mDither);
    if (gradientState.mShape == 3) {
      gradientState.mInnerRadius = paramTypedArray.getDimensionPixelSize(7, gradientState.mInnerRadius);
      if (gradientState.mInnerRadius == -1)
        gradientState.mInnerRadiusRatio = paramTypedArray.getFloat(4, gradientState.mInnerRadiusRatio); 
      gradientState.mThickness = paramTypedArray.getDimensionPixelSize(8, gradientState.mThickness);
      if (gradientState.mThickness == -1)
        gradientState.mThicknessRatio = paramTypedArray.getFloat(5, gradientState.mThicknessRatio); 
      gradientState.mUseLevelForShape = paramTypedArray.getBoolean(6, gradientState.mUseLevelForShape);
    } 
    int i = paramTypedArray.getInt(9, -1);
    if (i != -1)
      gradientState.mBlendMode = Drawable.parseBlendMode(i, BlendMode.SRC_IN); 
    ColorStateList colorStateList = paramTypedArray.getColorStateList(1);
    if (colorStateList != null)
      gradientState.mTint = colorStateList; 
    i = paramTypedArray.getDimensionPixelSize(10, gradientState.mOpticalInsets.left);
    int j = paramTypedArray.getDimensionPixelSize(11, gradientState.mOpticalInsets.top);
    int k = paramTypedArray.getDimensionPixelSize(12, gradientState.mOpticalInsets.right);
    int m = paramTypedArray.getDimensionPixelSize(13, gradientState.mOpticalInsets.bottom);
    gradientState.mOpticalInsets = Insets.of(i, j, k, m);
  }
  
  public boolean canApplyTheme() {
    boolean bool;
    GradientState gradientState = this.mGradientState;
    if ((gradientState != null && gradientState.canApplyTheme()) || super.canApplyTheme()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void applyThemeChildElements(Resources.Theme paramTheme) {
    GradientState gradientState = this.mGradientState;
    if (gradientState.mAttrSize != null) {
      TypedArray typedArray = paramTheme.resolveAttributes(gradientState.mAttrSize, R.styleable.GradientDrawableSize);
      updateGradientDrawableSize(typedArray);
      typedArray.recycle();
    } 
    if (gradientState.mAttrGradient != null) {
      TypedArray typedArray = paramTheme.resolveAttributes(gradientState.mAttrGradient, R.styleable.GradientDrawableGradient);
      try {
        updateGradientDrawableGradient(paramTheme.getResources(), typedArray);
      } finally {
        typedArray.recycle();
      } 
    } 
    if (gradientState.mAttrSolid != null) {
      TypedArray typedArray = paramTheme.resolveAttributes(gradientState.mAttrSolid, R.styleable.GradientDrawableSolid);
      updateGradientDrawableSolid(typedArray);
      typedArray.recycle();
    } 
    if (gradientState.mAttrStroke != null) {
      TypedArray typedArray = paramTheme.resolveAttributes(gradientState.mAttrStroke, R.styleable.GradientDrawableStroke);
      updateGradientDrawableStroke(typedArray);
      typedArray.recycle();
    } 
    if (gradientState.mAttrCorners != null) {
      TypedArray typedArray = paramTheme.resolveAttributes(gradientState.mAttrCorners, R.styleable.DrawableCorners);
      updateDrawableCorners(typedArray);
      typedArray.recycle();
    } 
    if (gradientState.mAttrPadding != null) {
      TypedArray typedArray = paramTheme.resolveAttributes(gradientState.mAttrPadding, R.styleable.GradientDrawablePadding);
      updateGradientDrawablePadding(typedArray);
      typedArray.recycle();
    } 
  }
  
  private void inflateChildElements(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme) throws XmlPullParserException, IOException {
    int i = paramXmlPullParser.getDepth() + 1;
    while (true) {
      int j = paramXmlPullParser.next();
      if (j != 1) {
        int k = paramXmlPullParser.getDepth();
        if (k >= i || j != 3) {
          if (j != 2)
            continue; 
          if (k > i)
            continue; 
          String str = paramXmlPullParser.getName();
          if (str.equals("size")) {
            TypedArray typedArray = obtainAttributes(paramResources, paramTheme, paramAttributeSet, R.styleable.GradientDrawableSize);
            updateGradientDrawableSize(typedArray);
            typedArray.recycle();
            continue;
          } 
          if (str.equals("gradient")) {
            TypedArray typedArray = obtainAttributes(paramResources, paramTheme, paramAttributeSet, R.styleable.GradientDrawableGradient);
            updateGradientDrawableGradient(paramResources, typedArray);
            typedArray.recycle();
            continue;
          } 
          if (str.equals("solid")) {
            TypedArray typedArray = obtainAttributes(paramResources, paramTheme, paramAttributeSet, R.styleable.GradientDrawableSolid);
            updateGradientDrawableSolid(typedArray);
            typedArray.recycle();
            continue;
          } 
          if (str.equals("stroke")) {
            TypedArray typedArray = obtainAttributes(paramResources, paramTheme, paramAttributeSet, R.styleable.GradientDrawableStroke);
            updateGradientDrawableStroke(typedArray);
            typedArray.recycle();
            continue;
          } 
          if (str.equals("corners")) {
            TypedArray typedArray = obtainAttributes(paramResources, paramTheme, paramAttributeSet, R.styleable.DrawableCorners);
            updateDrawableCorners(typedArray);
            typedArray.recycle();
            continue;
          } 
          if (str.equals("padding")) {
            TypedArray typedArray = obtainAttributes(paramResources, paramTheme, paramAttributeSet, R.styleable.GradientDrawablePadding);
            updateGradientDrawablePadding(typedArray);
            typedArray.recycle();
            continue;
          } 
          if (str.trim().equals("smooth-rect")) {
            this.mSmoothRound = true;
            this.mGradientState.mSmoothRound = true;
            continue;
          } 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Bad element under <shape>: ");
          stringBuilder.append(str);
          Log.w("drawable", stringBuilder.toString());
          continue;
        } 
      } 
      break;
    } 
  }
  
  private void updateGradientDrawablePadding(TypedArray paramTypedArray) {
    GradientState gradientState = this.mGradientState;
    gradientState.mChangingConfigurations |= paramTypedArray.getChangingConfigurations();
    gradientState.mAttrPadding = paramTypedArray.extractThemeAttrs();
    if (gradientState.mPadding == null)
      gradientState.mPadding = new Rect(); 
    Rect rect = gradientState.mPadding;
    int i = paramTypedArray.getDimensionPixelOffset(0, rect.left), j = rect.top;
    j = paramTypedArray.getDimensionPixelOffset(1, j);
    int k = rect.right;
    k = paramTypedArray.getDimensionPixelOffset(2, k);
    int m = rect.bottom;
    m = paramTypedArray.getDimensionPixelOffset(3, m);
    rect.set(i, j, k, m);
    this.mPadding = rect;
  }
  
  private void updateDrawableCorners(TypedArray paramTypedArray) {
    GradientState gradientState = this.mGradientState;
    gradientState.mChangingConfigurations |= paramTypedArray.getChangingConfigurations();
    gradientState.mAttrCorners = paramTypedArray.extractThemeAttrs();
    int i = paramTypedArray.getDimensionPixelSize(0, (int)gradientState.mRadius);
    setCornerRadius(i);
    int j = paramTypedArray.getDimensionPixelSize(1, i);
    int k = paramTypedArray.getDimensionPixelSize(2, i);
    int m = paramTypedArray.getDimensionPixelSize(3, i);
    int n = paramTypedArray.getDimensionPixelSize(4, i);
    if (j != i || k != i || m != i || n != i)
      setCornerRadii(new float[] { j, j, k, k, n, n, m, m }); 
  }
  
  private void updateGradientDrawableStroke(TypedArray paramTypedArray) {
    GradientState gradientState = this.mGradientState;
    gradientState.mChangingConfigurations |= paramTypedArray.getChangingConfigurations();
    gradientState.mAttrStroke = paramTypedArray.extractThemeAttrs();
    int i = Math.max(0, gradientState.mStrokeWidth);
    i = paramTypedArray.getDimensionPixelSize(0, i);
    float f = paramTypedArray.getDimension(2, gradientState.mStrokeDashWidth);
    ColorStateList colorStateList1 = paramTypedArray.getColorStateList(1);
    ColorStateList colorStateList2 = colorStateList1;
    if (colorStateList1 == null)
      colorStateList2 = gradientState.mStrokeColors; 
    if (f != 0.0F) {
      float f1 = paramTypedArray.getDimension(3, gradientState.mStrokeDashGap);
      setStroke(i, colorStateList2, f, f1);
    } else {
      setStroke(i, colorStateList2);
    } 
  }
  
  private void updateGradientDrawableSolid(TypedArray paramTypedArray) {
    GradientState gradientState = this.mGradientState;
    gradientState.mChangingConfigurations |= paramTypedArray.getChangingConfigurations();
    gradientState.mAttrSolid = paramTypedArray.extractThemeAttrs();
    ColorStateList colorStateList = paramTypedArray.getColorStateList(0);
    if (colorStateList != null)
      setColor(colorStateList); 
  }
  
  private void updateGradientDrawableGradient(Resources paramResources, TypedArray paramTypedArray) {
    boolean bool1;
    GradientState gradientState = this.mGradientState;
    gradientState.mChangingConfigurations |= paramTypedArray.getChangingConfigurations();
    gradientState.mAttrGradient = paramTypedArray.extractThemeAttrs();
    gradientState.mCenterX = getFloatOrFraction(paramTypedArray, 5, gradientState.mCenterX);
    gradientState.mCenterY = getFloatOrFraction(paramTypedArray, 6, gradientState.mCenterY);
    gradientState.mUseLevel = paramTypedArray.getBoolean(2, gradientState.mUseLevel);
    gradientState.mGradient = paramTypedArray.getInt(4, gradientState.mGradient);
    if (gradientState.mGradientColors != null) {
      i = 1;
    } else {
      i = 0;
    } 
    boolean bool = gradientState.hasCenterColor();
    if (i) {
      bool1 = gradientState.mGradientColors[0];
    } else {
      bool1 = false;
    } 
    if (bool) {
      j = gradientState.mGradientColors[1];
    } else {
      j = 0;
    } 
    if (gradientState.hasCenterColor()) {
      i = gradientState.mGradientColors[2];
    } else if (i != 0) {
      i = gradientState.mGradientColors[1];
    } else {
      i = 0;
    } 
    int k = paramTypedArray.getColor(0, bool1);
    if (paramTypedArray.hasValue(8) || bool) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    int j = paramTypedArray.getColor(8, j);
    int i = paramTypedArray.getColor(1, i);
    if (bool1) {
      float f;
      gradientState.mGradientColors = new int[3];
      gradientState.mGradientColors[0] = k;
      gradientState.mGradientColors[1] = j;
      gradientState.mGradientColors[2] = i;
      gradientState.mPositions = new float[3];
      gradientState.mPositions[0] = 0.0F;
      float[] arrayOfFloat = gradientState.mPositions;
      if (gradientState.mCenterX != 0.5F) {
        f = gradientState.mCenterX;
      } else {
        f = gradientState.mCenterY;
      } 
      arrayOfFloat[1] = f;
      gradientState.mPositions[2] = 1.0F;
    } else {
      gradientState.mGradientColors = new int[2];
      gradientState.mGradientColors[0] = k;
      gradientState.mGradientColors[1] = i;
    } 
    i = (int)paramTypedArray.getFloat(3, gradientState.mAngle);
    if (sWrapNegativeAngleMeasurements) {
      gradientState.mAngle = (i % 360 + 360) % 360;
    } else {
      gradientState.mAngle = i % 360;
    } 
    if (gradientState.mAngle >= 0) {
      i = gradientState.mAngle;
      if (i != 0) {
        if (i != 45) {
          if (i != 90) {
            if (i != 135) {
              if (i != 180) {
                if (i != 225) {
                  if (i != 270) {
                    if (i == 315)
                      gradientState.mOrientation = Orientation.TL_BR; 
                  } else {
                    gradientState.mOrientation = Orientation.TOP_BOTTOM;
                  } 
                } else {
                  gradientState.mOrientation = Orientation.TR_BL;
                } 
              } else {
                gradientState.mOrientation = Orientation.RIGHT_LEFT;
              } 
            } else {
              gradientState.mOrientation = Orientation.BR_TL;
            } 
          } else {
            gradientState.mOrientation = Orientation.BOTTOM_TOP;
          } 
        } else {
          gradientState.mOrientation = Orientation.BL_TR;
        } 
      } else {
        gradientState.mOrientation = Orientation.LEFT_RIGHT;
      } 
    } else {
      gradientState.mOrientation = DEFAULT_ORIENTATION;
    } 
    TypedValue typedValue = paramTypedArray.peekValue(7);
    if (typedValue != null) {
      float f;
      if (typedValue.type == 6) {
        f = typedValue.getFraction(1.0F, 1.0F);
        i = typedValue.data;
        if ((i >> 0 & 0xF) == 1) {
          i = 2;
        } else {
          i = 1;
        } 
      } else if (typedValue.type == 5) {
        f = typedValue.getDimension(paramResources.getDisplayMetrics());
        i = 0;
      } else {
        f = typedValue.getFloat();
        i = 0;
      } 
      gradientState.mGradientRadius = f;
      gradientState.mGradientRadiusType = i;
    } 
  }
  
  private void updateGradientDrawableSize(TypedArray paramTypedArray) {
    GradientState gradientState = this.mGradientState;
    gradientState.mChangingConfigurations |= paramTypedArray.getChangingConfigurations();
    gradientState.mAttrSize = paramTypedArray.extractThemeAttrs();
    gradientState.mWidth = paramTypedArray.getDimensionPixelSize(1, gradientState.mWidth);
    gradientState.mHeight = paramTypedArray.getDimensionPixelSize(0, gradientState.mHeight);
  }
  
  private static float getFloatOrFraction(TypedArray paramTypedArray, int paramInt, float paramFloat) {
    TypedValue typedValue = paramTypedArray.peekValue(paramInt);
    if (typedValue != null) {
      if (typedValue.type == 6) {
        paramInt = 1;
      } else {
        paramInt = 0;
      } 
      if (paramInt != 0) {
        paramFloat = typedValue.getFraction(1.0F, 1.0F);
      } else {
        paramFloat = typedValue.getFloat();
      } 
    } 
    return paramFloat;
  }
  
  public int getIntrinsicWidth() {
    return this.mGradientState.mWidth;
  }
  
  public int getIntrinsicHeight() {
    return this.mGradientState.mHeight;
  }
  
  public Insets getOpticalInsets() {
    return this.mGradientState.mOpticalInsets;
  }
  
  public Drawable.ConstantState getConstantState() {
    this.mGradientState.mChangingConfigurations = getChangingConfigurations();
    return this.mGradientState;
  }
  
  private boolean isOpaqueForState() {
    if (this.mGradientState.mStrokeWidth >= 0) {
      Paint paint = this.mStrokePaint;
      if (paint != null && 
        !isOpaque(paint.getColor()))
        return false; 
    } 
    if (this.mGradientState.mGradientColors == null && !isOpaque(this.mFillPaint.getColor()))
      return false; 
    return true;
  }
  
  public void getOutline(Outline paramOutline) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mGradientState : Landroid/graphics/drawable/GradientDrawable$GradientState;
    //   4: astore_2
    //   5: aload_0
    //   6: invokevirtual getBounds : ()Landroid/graphics/Rect;
    //   9: astore_3
    //   10: aload_2
    //   11: getfield mOpaqueOverShape : Z
    //   14: ifeq -> 59
    //   17: aload_0
    //   18: getfield mGradientState : Landroid/graphics/drawable/GradientDrawable$GradientState;
    //   21: getfield mStrokeWidth : I
    //   24: ifle -> 53
    //   27: aload_0
    //   28: getfield mStrokePaint : Landroid/graphics/Paint;
    //   31: astore #4
    //   33: aload #4
    //   35: ifnull -> 53
    //   38: aload #4
    //   40: invokevirtual getAlpha : ()I
    //   43: aload_0
    //   44: getfield mFillPaint : Landroid/graphics/Paint;
    //   47: invokevirtual getAlpha : ()I
    //   50: if_icmpne -> 59
    //   53: iconst_1
    //   54: istore #5
    //   56: goto -> 62
    //   59: iconst_0
    //   60: istore #5
    //   62: iload #5
    //   64: ifeq -> 88
    //   67: aload_0
    //   68: aload_0
    //   69: getfield mFillPaint : Landroid/graphics/Paint;
    //   72: invokevirtual getAlpha : ()I
    //   75: invokespecial modulateAlpha : (I)I
    //   78: i2f
    //   79: ldc_w 255.0
    //   82: fdiv
    //   83: fstore #6
    //   85: goto -> 91
    //   88: fconst_0
    //   89: fstore #6
    //   91: aload_1
    //   92: fload #6
    //   94: invokevirtual setAlpha : (F)V
    //   97: aload_2
    //   98: getfield mShape : I
    //   101: istore #5
    //   103: iload #5
    //   105: ifeq -> 202
    //   108: iload #5
    //   110: iconst_1
    //   111: if_icmpeq -> 196
    //   114: iload #5
    //   116: iconst_2
    //   117: if_icmpeq -> 121
    //   120: return
    //   121: aload_0
    //   122: getfield mStrokePaint : Landroid/graphics/Paint;
    //   125: astore_2
    //   126: aload_2
    //   127: ifnonnull -> 138
    //   130: ldc_w 1.0E-4
    //   133: fstore #6
    //   135: goto -> 148
    //   138: aload_2
    //   139: invokevirtual getStrokeWidth : ()F
    //   142: ldc_w 0.5
    //   145: fmul
    //   146: fstore #6
    //   148: aload_3
    //   149: invokevirtual centerY : ()I
    //   152: i2f
    //   153: fstore #7
    //   155: fload #7
    //   157: fload #6
    //   159: fsub
    //   160: f2d
    //   161: invokestatic floor : (D)D
    //   164: d2i
    //   165: istore #8
    //   167: fload #7
    //   169: fload #6
    //   171: fadd
    //   172: f2d
    //   173: invokestatic ceil : (D)D
    //   176: d2i
    //   177: istore #5
    //   179: aload_1
    //   180: aload_3
    //   181: getfield left : I
    //   184: iload #8
    //   186: aload_3
    //   187: getfield right : I
    //   190: iload #5
    //   192: invokevirtual setRect : (IIII)V
    //   195: return
    //   196: aload_1
    //   197: aload_3
    //   198: invokevirtual setOval : (Landroid/graphics/Rect;)V
    //   201: return
    //   202: aload_2
    //   203: getfield mRadiusArray : [F
    //   206: ifnull -> 222
    //   209: aload_0
    //   210: invokespecial buildPathIfDirty : ()V
    //   213: aload_1
    //   214: aload_0
    //   215: getfield mPath : Landroid/graphics/Path;
    //   218: invokevirtual setPath : (Landroid/graphics/Path;)V
    //   221: return
    //   222: fconst_0
    //   223: fstore #6
    //   225: aload_2
    //   226: getfield mRadius : F
    //   229: fconst_0
    //   230: fcmpl
    //   231: ifle -> 267
    //   234: aload_2
    //   235: getfield mRadius : F
    //   238: fstore #6
    //   240: aload_3
    //   241: invokevirtual width : ()I
    //   244: aload_3
    //   245: invokevirtual height : ()I
    //   248: invokestatic min : (II)I
    //   251: i2f
    //   252: fstore #7
    //   254: fload #6
    //   256: fload #7
    //   258: ldc_w 0.5
    //   261: fmul
    //   262: invokestatic min : (FF)F
    //   265: fstore #6
    //   267: aload_1
    //   268: aload_3
    //   269: fload #6
    //   271: invokevirtual setRoundRect : (Landroid/graphics/Rect;F)V
    //   274: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1938	-> 0
    //   #1939	-> 5
    //   #1942	-> 10
    //   #1944	-> 38
    //   #1945	-> 62
    //   #1946	-> 67
    //   #1947	-> 88
    //   #1945	-> 91
    //   #1949	-> 97
    //   #1982	-> 120
    //   #1971	-> 121
    //   #1972	-> 130
    //   #1973	-> 148
    //   #1974	-> 155
    //   #1975	-> 167
    //   #1977	-> 179
    //   #1978	-> 195
    //   #1966	-> 196
    //   #1967	-> 201
    //   #1951	-> 202
    //   #1952	-> 209
    //   #1953	-> 213
    //   #1954	-> 221
    //   #1957	-> 222
    //   #1958	-> 225
    //   #1960	-> 234
    //   #1961	-> 240
    //   #1960	-> 254
    //   #1963	-> 267
    //   #1964	-> 274
  }
  
  public Drawable mutate() {
    if (!this.mMutated && super.mutate() == this) {
      this.mGradientState = new GradientState(this.mGradientState, null);
      updateLocalState((Resources)null);
      this.mMutated = true;
    } 
    return this;
  }
  
  public void clearMutated() {
    super.clearMutated();
    this.mMutated = false;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class GradientType implements Annotation {}
  
  class GradientState extends Drawable.ConstantState {
    public int mShape = 0;
    
    public int mGradient = 0;
    
    public int mAngle = 0;
    
    public int mStrokeWidth = -1;
    
    public float mStrokeDashWidth = 0.0F;
    
    public float mStrokeDashGap = 0.0F;
    
    public float mRadius = 0.0F;
    
    public float[] mRadiusArray = null;
    
    public Rect mPadding = null;
    
    public int mWidth = -1;
    
    public int mHeight = -1;
    
    public float mInnerRadiusRatio = 3.0F;
    
    public float mThicknessRatio = 9.0F;
    
    public int mInnerRadius = -1;
    
    public int mThickness = -1;
    
    public boolean mDither = false;
    
    public Insets mOpticalInsets = Insets.NONE;
    
    float mCenterX = 0.5F;
    
    float mCenterY = 0.5F;
    
    float mGradientRadius = 0.5F;
    
    int mGradientRadiusType = 0;
    
    boolean mUseLevel = false;
    
    boolean mUseLevelForShape = true;
    
    ColorStateList mTint = null;
    
    BlendMode mBlendMode = Drawable.DEFAULT_BLEND_MODE;
    
    int mDensity = 160;
    
    int[] mAttrCorners;
    
    int[] mAttrGradient;
    
    int[] mAttrPadding;
    
    int[] mAttrSize;
    
    int[] mAttrSolid;
    
    int[] mAttrStroke;
    
    public int mChangingConfigurations;
    
    public int[] mGradientColors;
    
    boolean mOpaqueOverBounds;
    
    boolean mOpaqueOverShape;
    
    public GradientDrawable.Orientation mOrientation;
    
    public float[] mPositions;
    
    public boolean mSmoothRound;
    
    public ColorStateList mSolidColors;
    
    public ColorStateList mStrokeColors;
    
    public int[] mTempColors;
    
    public float[] mTempPositions;
    
    int[] mThemeAttrs;
    
    public GradientState(GradientDrawable this$0, int[] param1ArrayOfint) {
      this.mOrientation = (GradientDrawable.Orientation)this$0;
      setGradientColors(param1ArrayOfint);
    }
    
    public GradientState(GradientDrawable this$0, Resources param1Resources) {
      this.mChangingConfigurations = ((GradientState)this$0).mChangingConfigurations;
      this.mShape = ((GradientState)this$0).mShape;
      this.mGradient = ((GradientState)this$0).mGradient;
      this.mAngle = ((GradientState)this$0).mAngle;
      this.mOrientation = ((GradientState)this$0).mOrientation;
      this.mSolidColors = ((GradientState)this$0).mSolidColors;
      int[] arrayOfInt = ((GradientState)this$0).mGradientColors;
      if (arrayOfInt != null)
        this.mGradientColors = (int[])arrayOfInt.clone(); 
      float[] arrayOfFloat = ((GradientState)this$0).mPositions;
      if (arrayOfFloat != null)
        this.mPositions = (float[])arrayOfFloat.clone(); 
      this.mStrokeColors = ((GradientState)this$0).mStrokeColors;
      this.mStrokeWidth = ((GradientState)this$0).mStrokeWidth;
      this.mStrokeDashWidth = ((GradientState)this$0).mStrokeDashWidth;
      this.mStrokeDashGap = ((GradientState)this$0).mStrokeDashGap;
      this.mRadius = ((GradientState)this$0).mRadius;
      arrayOfFloat = ((GradientState)this$0).mRadiusArray;
      if (arrayOfFloat != null)
        this.mRadiusArray = (float[])arrayOfFloat.clone(); 
      if (((GradientState)this$0).mPadding != null)
        this.mPadding = new Rect(((GradientState)this$0).mPadding); 
      this.mWidth = ((GradientState)this$0).mWidth;
      this.mHeight = ((GradientState)this$0).mHeight;
      this.mInnerRadiusRatio = ((GradientState)this$0).mInnerRadiusRatio;
      this.mThicknessRatio = ((GradientState)this$0).mThicknessRatio;
      this.mInnerRadius = ((GradientState)this$0).mInnerRadius;
      this.mThickness = ((GradientState)this$0).mThickness;
      this.mDither = ((GradientState)this$0).mDither;
      this.mOpticalInsets = ((GradientState)this$0).mOpticalInsets;
      this.mCenterX = ((GradientState)this$0).mCenterX;
      this.mCenterY = ((GradientState)this$0).mCenterY;
      this.mGradientRadius = ((GradientState)this$0).mGradientRadius;
      this.mGradientRadiusType = ((GradientState)this$0).mGradientRadiusType;
      this.mUseLevel = ((GradientState)this$0).mUseLevel;
      this.mUseLevelForShape = ((GradientState)this$0).mUseLevelForShape;
      this.mOpaqueOverBounds = ((GradientState)this$0).mOpaqueOverBounds;
      this.mOpaqueOverShape = ((GradientState)this$0).mOpaqueOverShape;
      this.mTint = ((GradientState)this$0).mTint;
      this.mBlendMode = ((GradientState)this$0).mBlendMode;
      this.mThemeAttrs = ((GradientState)this$0).mThemeAttrs;
      this.mAttrSize = ((GradientState)this$0).mAttrSize;
      this.mAttrGradient = ((GradientState)this$0).mAttrGradient;
      this.mAttrSolid = ((GradientState)this$0).mAttrSolid;
      this.mAttrStroke = ((GradientState)this$0).mAttrStroke;
      this.mAttrCorners = ((GradientState)this$0).mAttrCorners;
      this.mAttrPadding = ((GradientState)this$0).mAttrPadding;
      int i = Drawable.resolveDensity(param1Resources, ((GradientState)this$0).mDensity);
      int j = ((GradientState)this$0).mDensity;
      if (j != i)
        applyDensityScaling(j, i); 
    }
    
    public final void setDensity(int param1Int) {
      if (this.mDensity != param1Int) {
        int i = this.mDensity;
        this.mDensity = param1Int;
        applyDensityScaling(i, param1Int);
      } 
    }
    
    public boolean hasCenterColor() {
      boolean bool;
      int[] arrayOfInt = this.mGradientColors;
      if (arrayOfInt != null && arrayOfInt.length == 3) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private void applyDensityScaling(int param1Int1, int param1Int2) {
      int i = this.mInnerRadius;
      if (i > 0)
        this.mInnerRadius = Drawable.scaleFromDensity(i, param1Int1, param1Int2, true); 
      i = this.mThickness;
      if (i > 0)
        this.mThickness = Drawable.scaleFromDensity(i, param1Int1, param1Int2, true); 
      if (this.mOpticalInsets != Insets.NONE) {
        int j = Drawable.scaleFromDensity(this.mOpticalInsets.left, param1Int1, param1Int2, true);
        int k = Drawable.scaleFromDensity(this.mOpticalInsets.top, param1Int1, param1Int2, true);
        i = Drawable.scaleFromDensity(this.mOpticalInsets.right, param1Int1, param1Int2, true);
        int m = Drawable.scaleFromDensity(this.mOpticalInsets.bottom, param1Int1, param1Int2, true);
        this.mOpticalInsets = Insets.of(j, k, i, m);
      } 
      Rect rect = this.mPadding;
      if (rect != null) {
        rect.left = Drawable.scaleFromDensity(rect.left, param1Int1, param1Int2, false);
        rect = this.mPadding;
        rect.top = Drawable.scaleFromDensity(rect.top, param1Int1, param1Int2, false);
        rect = this.mPadding;
        rect.right = Drawable.scaleFromDensity(rect.right, param1Int1, param1Int2, false);
        rect = this.mPadding;
        rect.bottom = Drawable.scaleFromDensity(rect.bottom, param1Int1, param1Int2, false);
      } 
      float f = this.mRadius;
      if (f > 0.0F)
        this.mRadius = Drawable.scaleFromDensity(f, param1Int1, param1Int2); 
      float[] arrayOfFloat = this.mRadiusArray;
      if (arrayOfFloat != null) {
        arrayOfFloat[0] = Drawable.scaleFromDensity((int)arrayOfFloat[0], param1Int1, param1Int2, true);
        arrayOfFloat = this.mRadiusArray;
        arrayOfFloat[1] = Drawable.scaleFromDensity((int)arrayOfFloat[1], param1Int1, param1Int2, true);
        arrayOfFloat = this.mRadiusArray;
        arrayOfFloat[2] = Drawable.scaleFromDensity((int)arrayOfFloat[2], param1Int1, param1Int2, true);
        arrayOfFloat = this.mRadiusArray;
        arrayOfFloat[3] = Drawable.scaleFromDensity((int)arrayOfFloat[3], param1Int1, param1Int2, true);
      } 
      i = this.mStrokeWidth;
      if (i > 0)
        this.mStrokeWidth = Drawable.scaleFromDensity(i, param1Int1, param1Int2, true); 
      if (this.mStrokeDashWidth > 0.0F)
        this.mStrokeDashWidth = Drawable.scaleFromDensity(this.mStrokeDashGap, param1Int1, param1Int2); 
      f = this.mStrokeDashGap;
      if (f > 0.0F)
        this.mStrokeDashGap = Drawable.scaleFromDensity(f, param1Int1, param1Int2); 
      if (this.mGradientRadiusType == 0)
        this.mGradientRadius = Drawable.scaleFromDensity(this.mGradientRadius, param1Int1, param1Int2); 
      i = this.mWidth;
      if (i > 0)
        this.mWidth = Drawable.scaleFromDensity(i, param1Int1, param1Int2, true); 
      i = this.mHeight;
      if (i > 0)
        this.mHeight = Drawable.scaleFromDensity(i, param1Int1, param1Int2, true); 
    }
    
    public boolean canApplyTheme() {
      // Byte code:
      //   0: aload_0
      //   1: getfield mThemeAttrs : [I
      //   4: ifnonnull -> 112
      //   7: aload_0
      //   8: getfield mAttrSize : [I
      //   11: ifnonnull -> 112
      //   14: aload_0
      //   15: getfield mAttrGradient : [I
      //   18: ifnonnull -> 112
      //   21: aload_0
      //   22: getfield mAttrSolid : [I
      //   25: ifnonnull -> 112
      //   28: aload_0
      //   29: getfield mAttrStroke : [I
      //   32: ifnonnull -> 112
      //   35: aload_0
      //   36: getfield mAttrCorners : [I
      //   39: ifnonnull -> 112
      //   42: aload_0
      //   43: getfield mAttrPadding : [I
      //   46: ifnonnull -> 112
      //   49: aload_0
      //   50: getfield mTint : Landroid/content/res/ColorStateList;
      //   53: astore_1
      //   54: aload_1
      //   55: ifnull -> 65
      //   58: aload_1
      //   59: invokevirtual canApplyTheme : ()Z
      //   62: ifne -> 112
      //   65: aload_0
      //   66: getfield mStrokeColors : Landroid/content/res/ColorStateList;
      //   69: astore_1
      //   70: aload_1
      //   71: ifnull -> 81
      //   74: aload_1
      //   75: invokevirtual canApplyTheme : ()Z
      //   78: ifne -> 112
      //   81: aload_0
      //   82: getfield mSolidColors : Landroid/content/res/ColorStateList;
      //   85: astore_1
      //   86: aload_1
      //   87: ifnull -> 97
      //   90: aload_1
      //   91: invokevirtual canApplyTheme : ()Z
      //   94: ifne -> 112
      //   97: aload_0
      //   98: invokespecial canApplyTheme : ()Z
      //   101: ifeq -> 107
      //   104: goto -> 112
      //   107: iconst_0
      //   108: istore_2
      //   109: goto -> 114
      //   112: iconst_1
      //   113: istore_2
      //   114: iload_2
      //   115: ireturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2228	-> 0
      //   #2232	-> 58
      //   #2233	-> 74
      //   #2234	-> 90
      //   #2235	-> 97
      //   #2228	-> 114
    }
    
    public Drawable newDrawable() {
      return new GradientDrawable(this, null);
    }
    
    public Drawable newDrawable(Resources param1Resources) {
      GradientState gradientState;
      int i = Drawable.resolveDensity(param1Resources, this.mDensity);
      if (i != this.mDensity) {
        gradientState = new GradientState(this, param1Resources);
      } else {
        gradientState = this;
      } 
      return new GradientDrawable(gradientState, param1Resources);
    }
    
    public int getChangingConfigurations() {
      byte b;
      boolean bool;
      int i = this.mChangingConfigurations;
      ColorStateList colorStateList = this.mStrokeColors;
      int j = 0;
      if (colorStateList != null) {
        b = colorStateList.getChangingConfigurations();
      } else {
        b = 0;
      } 
      colorStateList = this.mSolidColors;
      if (colorStateList != null) {
        bool = colorStateList.getChangingConfigurations();
      } else {
        bool = false;
      } 
      colorStateList = this.mTint;
      if (colorStateList != null)
        j = colorStateList.getChangingConfigurations(); 
      return i | b | bool | j;
    }
    
    public void setShape(int param1Int) {
      this.mShape = param1Int;
      computeOpacity();
    }
    
    public void setGradientType(int param1Int) {
      this.mGradient = param1Int;
    }
    
    public void setGradientCenter(float param1Float1, float param1Float2) {
      this.mCenterX = param1Float1;
      this.mCenterY = param1Float2;
    }
    
    public GradientDrawable.Orientation getOrientation() {
      return this.mOrientation;
    }
    
    public void setGradientColors(int[] param1ArrayOfint) {
      this.mGradientColors = param1ArrayOfint;
      this.mSolidColors = null;
      computeOpacity();
    }
    
    public void setSolidColors(ColorStateList param1ColorStateList) {
      this.mGradientColors = null;
      this.mSolidColors = param1ColorStateList;
      computeOpacity();
    }
    
    private void computeOpacity() {
      boolean bool1 = false;
      this.mOpaqueOverBounds = false;
      this.mOpaqueOverShape = false;
      if (this.mGradientColors != null) {
        byte b = 0;
        while (true) {
          int[] arrayOfInt = this.mGradientColors;
          if (b < arrayOfInt.length) {
            if (!GradientDrawable.isOpaque(arrayOfInt[b]))
              return; 
            b++;
            continue;
          } 
          break;
        } 
      } 
      if (this.mGradientColors == null && this.mSolidColors == null)
        return; 
      this.mOpaqueOverShape = true;
      boolean bool2 = bool1;
      if (this.mShape == 0) {
        bool2 = bool1;
        if (this.mRadius <= 0.0F) {
          bool2 = bool1;
          if (this.mRadiusArray == null)
            bool2 = true; 
        } 
      } 
      this.mOpaqueOverBounds = bool2;
    }
    
    public void setStroke(int param1Int, ColorStateList param1ColorStateList, float param1Float1, float param1Float2) {
      this.mStrokeWidth = param1Int;
      this.mStrokeColors = param1ColorStateList;
      this.mStrokeDashWidth = param1Float1;
      this.mStrokeDashGap = param1Float2;
      computeOpacity();
    }
    
    public void setCornerRadius(float param1Float) {
      float f = param1Float;
      if (param1Float < 0.0F)
        f = 0.0F; 
      this.mRadius = f;
      this.mRadiusArray = null;
      computeOpacity();
    }
    
    public void setCornerRadii(float[] param1ArrayOffloat) {
      this.mRadiusArray = param1ArrayOffloat;
      if (param1ArrayOffloat == null)
        this.mRadius = 0.0F; 
      computeOpacity();
    }
    
    public void setSize(int param1Int1, int param1Int2) {
      this.mWidth = param1Int1;
      this.mHeight = param1Int2;
    }
    
    public void setGradientRadius(float param1Float, int param1Int) {
      this.mGradientRadius = param1Float;
      this.mGradientRadiusType = param1Int;
    }
  }
  
  static boolean isOpaque(int paramInt) {
    boolean bool;
    if ((paramInt >> 24 & 0xFF) == 255) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private GradientDrawable(GradientState paramGradientState, Resources paramResources) {
    this.mGradientState = paramGradientState;
    updateLocalState(paramResources);
  }
  
  private void updateLocalState(Resources paramResources) {
    GradientState gradientState = this.mGradientState;
    if (gradientState.mSolidColors != null) {
      int[] arrayOfInt = getState();
      int i = gradientState.mSolidColors.getColorForState(arrayOfInt, 0);
      this.mFillPaint.setColor(i);
    } else if (gradientState.mGradientColors == null) {
      this.mFillPaint.setColor(0);
    } else {
      this.mFillPaint.setColor(-16777216);
    } 
    this.mPadding = gradientState.mPadding;
    this.mSmoothRound = gradientState.mSmoothRound;
    if (gradientState.mStrokeWidth >= 0) {
      Paint paint = new Paint(1);
      paint.setStyle(Paint.Style.STROKE);
      this.mStrokePaint.setStrokeWidth(gradientState.mStrokeWidth);
      if (gradientState.mStrokeColors != null) {
        int[] arrayOfInt = getState();
        int i = gradientState.mStrokeColors.getColorForState(arrayOfInt, 0);
        this.mStrokePaint.setColor(i);
      } 
      if (gradientState.mStrokeDashWidth != 0.0F) {
        DashPathEffect dashPathEffect = new DashPathEffect(new float[] { gradientState.mStrokeDashWidth, gradientState.mStrokeDashGap }, 0.0F);
        this.mStrokePaint.setPathEffect(dashPathEffect);
      } 
    } 
    this.mBlendModeColorFilter = updateBlendModeFilter(this.mBlendModeColorFilter, gradientState.mTint, gradientState.mBlendMode);
    this.mGradientIsDirty = true;
    gradientState.computeOpacity();
  }
}
