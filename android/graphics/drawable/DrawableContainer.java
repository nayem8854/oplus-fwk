package android.graphics.drawable;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.BlendMode;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Insets;
import android.graphics.Outline;
import android.graphics.Rect;
import android.os.SystemClock;
import android.util.SparseArray;

public class DrawableContainer extends Drawable implements Drawable.Callback {
  private static final boolean DEBUG = false;
  
  private static final boolean DEFAULT_DITHER = true;
  
  private static final String TAG = "DrawableContainer";
  
  private int mAlpha = 255;
  
  private Runnable mAnimationRunnable;
  
  private BlockInvalidateCallback mBlockInvalidateCallback;
  
  private int mCurIndex = -1;
  
  private Drawable mCurrDrawable;
  
  private DrawableContainerState mDrawableContainerState;
  
  private long mEnterAnimationEnd;
  
  private long mExitAnimationEnd;
  
  private boolean mHasAlpha;
  
  private Rect mHotspotBounds;
  
  private Drawable mLastDrawable;
  
  private int mLastIndex = -1;
  
  private boolean mMutated;
  
  public void draw(Canvas paramCanvas) {
    Drawable drawable = this.mCurrDrawable;
    if (drawable != null)
      drawable.draw(paramCanvas); 
    drawable = this.mLastDrawable;
    if (drawable != null)
      drawable.draw(paramCanvas); 
  }
  
  public int getChangingConfigurations() {
    int i = super.getChangingConfigurations();
    DrawableContainerState drawableContainerState = this.mDrawableContainerState;
    int j = drawableContainerState.getChangingConfigurations();
    return i | j;
  }
  
  private boolean needsMirroring() {
    boolean bool = isAutoMirrored();
    boolean bool1 = true;
    if (!bool || getLayoutDirection() != 1)
      bool1 = false; 
    return bool1;
  }
  
  public boolean getPadding(Rect paramRect) {
    boolean bool;
    Rect rect = this.mDrawableContainerState.getConstantPadding();
    if (rect != null) {
      paramRect.set(rect);
      if ((rect.left | rect.top | rect.bottom | rect.right) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
    } else {
      Drawable drawable = this.mCurrDrawable;
      if (drawable != null) {
        bool = drawable.getPadding(paramRect);
      } else {
        bool = super.getPadding(paramRect);
      } 
    } 
    if (needsMirroring()) {
      int i = paramRect.left;
      int j = paramRect.right;
      paramRect.left = j;
      paramRect.right = i;
    } 
    return bool;
  }
  
  public Insets getOpticalInsets() {
    Drawable drawable = this.mCurrDrawable;
    if (drawable != null)
      return drawable.getOpticalInsets(); 
    return Insets.NONE;
  }
  
  public void getOutline(Outline paramOutline) {
    Drawable drawable = this.mCurrDrawable;
    if (drawable != null)
      drawable.getOutline(paramOutline); 
  }
  
  public void setAlpha(int paramInt) {
    if (!this.mHasAlpha || this.mAlpha != paramInt) {
      this.mHasAlpha = true;
      this.mAlpha = paramInt;
      Drawable drawable = this.mCurrDrawable;
      if (drawable != null)
        if (this.mEnterAnimationEnd == 0L) {
          drawable.setAlpha(paramInt);
        } else {
          animate(false);
        }  
    } 
  }
  
  public int getAlpha() {
    return this.mAlpha;
  }
  
  public void setDither(boolean paramBoolean) {
    if (this.mDrawableContainerState.mDither != paramBoolean) {
      this.mDrawableContainerState.mDither = paramBoolean;
      Drawable drawable = this.mCurrDrawable;
      if (drawable != null)
        drawable.setDither(this.mDrawableContainerState.mDither); 
    } 
  }
  
  public void setColorFilter(ColorFilter paramColorFilter) {
    this.mDrawableContainerState.mHasColorFilter = true;
    if (this.mDrawableContainerState.mColorFilter != paramColorFilter) {
      this.mDrawableContainerState.mColorFilter = paramColorFilter;
      Drawable drawable = this.mCurrDrawable;
      if (drawable != null)
        drawable.setColorFilter(paramColorFilter); 
    } 
  }
  
  public void setTintList(ColorStateList paramColorStateList) {
    this.mDrawableContainerState.mHasTintList = true;
    if (this.mDrawableContainerState.mTintList != paramColorStateList) {
      this.mDrawableContainerState.mTintList = paramColorStateList;
      Drawable drawable = this.mCurrDrawable;
      if (drawable != null)
        drawable.setTintList(paramColorStateList); 
    } 
  }
  
  public void setTintBlendMode(BlendMode paramBlendMode) {
    this.mDrawableContainerState.mHasTintMode = true;
    if (this.mDrawableContainerState.mBlendMode != paramBlendMode) {
      this.mDrawableContainerState.mBlendMode = paramBlendMode;
      Drawable drawable = this.mCurrDrawable;
      if (drawable != null)
        drawable.setTintBlendMode(paramBlendMode); 
    } 
  }
  
  public void setEnterFadeDuration(int paramInt) {
    this.mDrawableContainerState.mEnterFadeDuration = paramInt;
  }
  
  public void setExitFadeDuration(int paramInt) {
    this.mDrawableContainerState.mExitFadeDuration = paramInt;
  }
  
  protected void onBoundsChange(Rect paramRect) {
    Drawable drawable = this.mLastDrawable;
    if (drawable != null)
      drawable.setBounds(paramRect); 
    drawable = this.mCurrDrawable;
    if (drawable != null)
      drawable.setBounds(paramRect); 
  }
  
  public boolean isStateful() {
    return this.mDrawableContainerState.isStateful();
  }
  
  public boolean hasFocusStateSpecified() {
    Drawable drawable = this.mCurrDrawable;
    if (drawable != null)
      return drawable.hasFocusStateSpecified(); 
    drawable = this.mLastDrawable;
    if (drawable != null)
      return drawable.hasFocusStateSpecified(); 
    return false;
  }
  
  public void setAutoMirrored(boolean paramBoolean) {
    if (this.mDrawableContainerState.mAutoMirrored != paramBoolean) {
      this.mDrawableContainerState.mAutoMirrored = paramBoolean;
      Drawable drawable = this.mCurrDrawable;
      if (drawable != null)
        drawable.setAutoMirrored(this.mDrawableContainerState.mAutoMirrored); 
    } 
  }
  
  public boolean isAutoMirrored() {
    return this.mDrawableContainerState.mAutoMirrored;
  }
  
  public void jumpToCurrentState() {
    boolean bool = false;
    Drawable drawable = this.mLastDrawable;
    if (drawable != null) {
      drawable.jumpToCurrentState();
      this.mLastDrawable = null;
      this.mLastIndex = -1;
      bool = true;
    } 
    drawable = this.mCurrDrawable;
    if (drawable != null) {
      drawable.jumpToCurrentState();
      if (this.mHasAlpha)
        this.mCurrDrawable.setAlpha(this.mAlpha); 
    } 
    if (this.mExitAnimationEnd != 0L) {
      this.mExitAnimationEnd = 0L;
      bool = true;
    } 
    if (this.mEnterAnimationEnd != 0L) {
      this.mEnterAnimationEnd = 0L;
      bool = true;
    } 
    if (bool)
      invalidateSelf(); 
  }
  
  public void setHotspot(float paramFloat1, float paramFloat2) {
    Drawable drawable = this.mCurrDrawable;
    if (drawable != null)
      drawable.setHotspot(paramFloat1, paramFloat2); 
  }
  
  public void setHotspotBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    Rect rect = this.mHotspotBounds;
    if (rect == null) {
      this.mHotspotBounds = new Rect(paramInt1, paramInt2, paramInt3, paramInt4);
    } else {
      rect.set(paramInt1, paramInt2, paramInt3, paramInt4);
    } 
    Drawable drawable = this.mCurrDrawable;
    if (drawable != null)
      drawable.setHotspotBounds(paramInt1, paramInt2, paramInt3, paramInt4); 
  }
  
  public void getHotspotBounds(Rect paramRect) {
    Rect rect = this.mHotspotBounds;
    if (rect != null) {
      paramRect.set(rect);
    } else {
      super.getHotspotBounds(paramRect);
    } 
  }
  
  protected boolean onStateChange(int[] paramArrayOfint) {
    Drawable drawable = this.mLastDrawable;
    if (drawable != null)
      return drawable.setState(paramArrayOfint); 
    drawable = this.mCurrDrawable;
    if (drawable != null)
      return drawable.setState(paramArrayOfint); 
    return false;
  }
  
  protected boolean onLevelChange(int paramInt) {
    Drawable drawable = this.mLastDrawable;
    if (drawable != null)
      return drawable.setLevel(paramInt); 
    drawable = this.mCurrDrawable;
    if (drawable != null)
      return drawable.setLevel(paramInt); 
    return false;
  }
  
  public boolean onLayoutDirectionChanged(int paramInt) {
    return this.mDrawableContainerState.setLayoutDirection(paramInt, getCurrentIndex());
  }
  
  public int getIntrinsicWidth() {
    byte b;
    if (this.mDrawableContainerState.isConstantSize())
      return this.mDrawableContainerState.getConstantWidth(); 
    Drawable drawable = this.mCurrDrawable;
    if (drawable != null) {
      b = drawable.getIntrinsicWidth();
    } else {
      b = -1;
    } 
    return b;
  }
  
  public int getIntrinsicHeight() {
    byte b;
    if (this.mDrawableContainerState.isConstantSize())
      return this.mDrawableContainerState.getConstantHeight(); 
    Drawable drawable = this.mCurrDrawable;
    if (drawable != null) {
      b = drawable.getIntrinsicHeight();
    } else {
      b = -1;
    } 
    return b;
  }
  
  public int getMinimumWidth() {
    boolean bool;
    if (this.mDrawableContainerState.isConstantSize())
      return this.mDrawableContainerState.getConstantMinimumWidth(); 
    Drawable drawable = this.mCurrDrawable;
    if (drawable != null) {
      bool = drawable.getMinimumWidth();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getMinimumHeight() {
    boolean bool;
    if (this.mDrawableContainerState.isConstantSize())
      return this.mDrawableContainerState.getConstantMinimumHeight(); 
    Drawable drawable = this.mCurrDrawable;
    if (drawable != null) {
      bool = drawable.getMinimumHeight();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void invalidateDrawable(Drawable paramDrawable) {
    DrawableContainerState drawableContainerState = this.mDrawableContainerState;
    if (drawableContainerState != null)
      drawableContainerState.invalidateCache(); 
    if (paramDrawable == this.mCurrDrawable && getCallback() != null)
      getCallback().invalidateDrawable(this); 
  }
  
  public void scheduleDrawable(Drawable paramDrawable, Runnable paramRunnable, long paramLong) {
    if (paramDrawable == this.mCurrDrawable && getCallback() != null)
      getCallback().scheduleDrawable(this, paramRunnable, paramLong); 
  }
  
  public void unscheduleDrawable(Drawable paramDrawable, Runnable paramRunnable) {
    if (paramDrawable == this.mCurrDrawable && getCallback() != null)
      getCallback().unscheduleDrawable(this, paramRunnable); 
  }
  
  public boolean setVisible(boolean paramBoolean1, boolean paramBoolean2) {
    boolean bool = super.setVisible(paramBoolean1, paramBoolean2);
    Drawable drawable = this.mLastDrawable;
    if (drawable != null)
      drawable.setVisible(paramBoolean1, paramBoolean2); 
    drawable = this.mCurrDrawable;
    if (drawable != null)
      drawable.setVisible(paramBoolean1, paramBoolean2); 
    return bool;
  }
  
  public int getOpacity() {
    Drawable drawable = this.mCurrDrawable;
    return (drawable == null || !drawable.isVisible()) ? -2 : 
      this.mDrawableContainerState.getOpacity();
  }
  
  public void setCurrentIndex(int paramInt) {
    selectDrawable(paramInt);
  }
  
  public int getCurrentIndex() {
    return this.mCurIndex;
  }
  
  public boolean selectDrawable(int paramInt) {
    if (paramInt == this.mCurIndex)
      return false; 
    long l = SystemClock.uptimeMillis();
    if (this.mDrawableContainerState.mExitFadeDuration > 0) {
      Drawable drawable = this.mLastDrawable;
      if (drawable != null)
        drawable.setVisible(false, false); 
      drawable = this.mCurrDrawable;
      if (drawable != null) {
        this.mLastDrawable = drawable;
        this.mLastIndex = this.mCurIndex;
        this.mExitAnimationEnd = this.mDrawableContainerState.mExitFadeDuration + l;
      } else {
        this.mLastDrawable = null;
        this.mLastIndex = -1;
        this.mExitAnimationEnd = 0L;
      } 
    } else {
      Drawable drawable = this.mCurrDrawable;
      if (drawable != null)
        drawable.setVisible(false, false); 
    } 
    if (paramInt >= 0 && paramInt < this.mDrawableContainerState.mNumChildren) {
      Drawable drawable = this.mDrawableContainerState.getChild(paramInt);
      this.mCurrDrawable = drawable;
      this.mCurIndex = paramInt;
      if (drawable != null) {
        if (this.mDrawableContainerState.mEnterFadeDuration > 0)
          this.mEnterAnimationEnd = this.mDrawableContainerState.mEnterFadeDuration + l; 
        initializeDrawableForDisplay(drawable);
      } 
    } else {
      this.mCurrDrawable = null;
      this.mCurIndex = -1;
    } 
    if (this.mEnterAnimationEnd != 0L || this.mExitAnimationEnd != 0L) {
      Runnable runnable = this.mAnimationRunnable;
      if (runnable == null) {
        this.mAnimationRunnable = (Runnable)new Object(this);
      } else {
        unscheduleSelf(runnable);
      } 
      animate(true);
    } 
    invalidateSelf();
    return true;
  }
  
  private void initializeDrawableForDisplay(Drawable paramDrawable) {
    if (this.mBlockInvalidateCallback == null)
      this.mBlockInvalidateCallback = new BlockInvalidateCallback(); 
    paramDrawable.setCallback(this.mBlockInvalidateCallback.wrap(paramDrawable.getCallback()));
    try {
      if (this.mDrawableContainerState.mEnterFadeDuration <= 0 && this.mHasAlpha)
        paramDrawable.setAlpha(this.mAlpha); 
      if (this.mDrawableContainerState.mHasColorFilter) {
        paramDrawable.setColorFilter(this.mDrawableContainerState.mColorFilter);
      } else {
        if (this.mDrawableContainerState.mHasTintList)
          paramDrawable.setTintList(this.mDrawableContainerState.mTintList); 
        if (this.mDrawableContainerState.mHasTintMode)
          paramDrawable.setTintBlendMode(this.mDrawableContainerState.mBlendMode); 
      } 
      paramDrawable.setVisible(isVisible(), true);
      paramDrawable.setDither(this.mDrawableContainerState.mDither);
      paramDrawable.setState(getState());
      paramDrawable.setLevel(getLevel());
      paramDrawable.setBounds(getBounds());
      paramDrawable.setLayoutDirection(getLayoutDirection());
      paramDrawable.setAutoMirrored(this.mDrawableContainerState.mAutoMirrored);
      Rect rect = this.mHotspotBounds;
      if (rect != null)
        paramDrawable.setHotspotBounds(rect.left, rect.top, rect.right, rect.bottom); 
      return;
    } finally {
      paramDrawable.setCallback(this.mBlockInvalidateCallback.unwrap());
    } 
  }
  
  void animate(boolean paramBoolean) {
    int j;
    this.mHasAlpha = true;
    long l = SystemClock.uptimeMillis();
    int i = 0;
    Drawable drawable = this.mCurrDrawable;
    if (drawable != null) {
      long l1 = this.mEnterAnimationEnd;
      j = i;
      if (l1 != 0L)
        if (l1 <= l) {
          drawable.setAlpha(this.mAlpha);
          this.mEnterAnimationEnd = 0L;
          j = i;
        } else {
          j = (int)((l1 - l) * 255L) / this.mDrawableContainerState.mEnterFadeDuration;
          this.mCurrDrawable.setAlpha((255 - j) * this.mAlpha / 255);
          j = 1;
        }  
    } else {
      this.mEnterAnimationEnd = 0L;
      j = i;
    } 
    drawable = this.mLastDrawable;
    if (drawable != null) {
      long l1 = this.mExitAnimationEnd;
      i = j;
      if (l1 != 0L)
        if (l1 <= l) {
          drawable.setVisible(false, false);
          this.mLastDrawable = null;
          this.mLastIndex = -1;
          this.mExitAnimationEnd = 0L;
          i = j;
        } else {
          j = (int)((l1 - l) * 255L) / this.mDrawableContainerState.mExitFadeDuration;
          this.mLastDrawable.setAlpha(this.mAlpha * j / 255);
          i = 1;
        }  
    } else {
      this.mExitAnimationEnd = 0L;
      i = j;
    } 
    if (paramBoolean && i != 0)
      scheduleSelf(this.mAnimationRunnable, 16L + l); 
  }
  
  public Drawable getCurrent() {
    return this.mCurrDrawable;
  }
  
  protected final void updateDensity(Resources paramResources) {
    this.mDrawableContainerState.updateDensity(paramResources);
  }
  
  public void applyTheme(Resources.Theme paramTheme) {
    this.mDrawableContainerState.applyTheme(paramTheme);
  }
  
  public boolean canApplyTheme() {
    return this.mDrawableContainerState.canApplyTheme();
  }
  
  public Drawable.ConstantState getConstantState() {
    if (this.mDrawableContainerState.canConstantState()) {
      this.mDrawableContainerState.mChangingConfigurations = getChangingConfigurations();
      return this.mDrawableContainerState;
    } 
    return null;
  }
  
  public Drawable mutate() {
    if (!this.mMutated && super.mutate() == this) {
      DrawableContainerState drawableContainerState = cloneConstantState();
      drawableContainerState.mutate();
      setConstantState(drawableContainerState);
      this.mMutated = true;
    } 
    return this;
  }
  
  DrawableContainerState cloneConstantState() {
    return this.mDrawableContainerState;
  }
  
  public void clearMutated() {
    super.clearMutated();
    this.mDrawableContainerState.clearMutated();
    this.mMutated = false;
  }
  
  class DrawableContainerState extends Drawable.ConstantState {
    int mDensity = 160;
    
    boolean mVariablePadding = false;
    
    boolean mConstantSize = false;
    
    boolean mDither = true;
    
    int mEnterFadeDuration = 0;
    
    int mExitFadeDuration = 0;
    
    boolean mAutoMirrored;
    
    BlendMode mBlendMode;
    
    boolean mCanConstantState;
    
    int mChangingConfigurations;
    
    boolean mCheckedConstantSize;
    
    boolean mCheckedConstantState;
    
    boolean mCheckedOpacity;
    
    boolean mCheckedPadding;
    
    boolean mCheckedStateful;
    
    int mChildrenChangingConfigurations;
    
    ColorFilter mColorFilter;
    
    int mConstantHeight;
    
    int mConstantMinimumHeight;
    
    int mConstantMinimumWidth;
    
    Rect mConstantPadding;
    
    int mConstantWidth;
    
    SparseArray<Drawable.ConstantState> mDrawableFutures;
    
    Drawable[] mDrawables;
    
    boolean mHasColorFilter;
    
    boolean mHasTintList;
    
    boolean mHasTintMode;
    
    int mLayoutDirection;
    
    boolean mMutated;
    
    int mNumChildren;
    
    int mOpacity;
    
    final DrawableContainer mOwner;
    
    Resources mSourceRes;
    
    boolean mStateful;
    
    ColorStateList mTintList;
    
    protected DrawableContainerState(DrawableContainer this$0, DrawableContainer param1DrawableContainer, Resources param1Resources) {
      this.mOwner = param1DrawableContainer;
      if (param1Resources != null) {
        Resources resources = param1Resources;
      } else if (this$0 != null) {
        Resources resources = ((DrawableContainerState)this$0).mSourceRes;
      } else {
        param1DrawableContainer = null;
      } 
      this.mSourceRes = (Resources)param1DrawableContainer;
      if (this$0 != null) {
        i = ((DrawableContainerState)this$0).mDensity;
      } else {
        i = 0;
      } 
      int i = Drawable.resolveDensity(param1Resources, i);
      if (this$0 != null) {
        this.mChangingConfigurations = ((DrawableContainerState)this$0).mChangingConfigurations;
        this.mChildrenChangingConfigurations = ((DrawableContainerState)this$0).mChildrenChangingConfigurations;
        this.mCheckedConstantState = true;
        this.mCanConstantState = true;
        this.mVariablePadding = ((DrawableContainerState)this$0).mVariablePadding;
        this.mConstantSize = ((DrawableContainerState)this$0).mConstantSize;
        this.mDither = ((DrawableContainerState)this$0).mDither;
        this.mMutated = ((DrawableContainerState)this$0).mMutated;
        this.mLayoutDirection = ((DrawableContainerState)this$0).mLayoutDirection;
        this.mEnterFadeDuration = ((DrawableContainerState)this$0).mEnterFadeDuration;
        this.mExitFadeDuration = ((DrawableContainerState)this$0).mExitFadeDuration;
        this.mAutoMirrored = ((DrawableContainerState)this$0).mAutoMirrored;
        this.mColorFilter = ((DrawableContainerState)this$0).mColorFilter;
        this.mHasColorFilter = ((DrawableContainerState)this$0).mHasColorFilter;
        this.mTintList = ((DrawableContainerState)this$0).mTintList;
        this.mBlendMode = ((DrawableContainerState)this$0).mBlendMode;
        this.mHasTintList = ((DrawableContainerState)this$0).mHasTintList;
        this.mHasTintMode = ((DrawableContainerState)this$0).mHasTintMode;
        if (((DrawableContainerState)this$0).mDensity == i) {
          if (((DrawableContainerState)this$0).mCheckedPadding) {
            this.mConstantPadding = new Rect(((DrawableContainerState)this$0).mConstantPadding);
            this.mCheckedPadding = true;
          } 
          if (((DrawableContainerState)this$0).mCheckedConstantSize) {
            this.mConstantWidth = ((DrawableContainerState)this$0).mConstantWidth;
            this.mConstantHeight = ((DrawableContainerState)this$0).mConstantHeight;
            this.mConstantMinimumWidth = ((DrawableContainerState)this$0).mConstantMinimumWidth;
            this.mConstantMinimumHeight = ((DrawableContainerState)this$0).mConstantMinimumHeight;
            this.mCheckedConstantSize = true;
          } 
        } 
        if (((DrawableContainerState)this$0).mCheckedOpacity) {
          this.mOpacity = ((DrawableContainerState)this$0).mOpacity;
          this.mCheckedOpacity = true;
        } 
        if (((DrawableContainerState)this$0).mCheckedStateful) {
          this.mStateful = ((DrawableContainerState)this$0).mStateful;
          this.mCheckedStateful = true;
        } 
        Drawable[] arrayOfDrawable = ((DrawableContainerState)this$0).mDrawables;
        this.mDrawables = new Drawable[arrayOfDrawable.length];
        this.mNumChildren = ((DrawableContainerState)this$0).mNumChildren;
        SparseArray<Drawable.ConstantState> sparseArray = ((DrawableContainerState)this$0).mDrawableFutures;
        if (sparseArray != null) {
          this.mDrawableFutures = sparseArray.clone();
        } else {
          this.mDrawableFutures = new SparseArray(this.mNumChildren);
        } 
        int j = this.mNumChildren;
        for (i = 0; i < j; i++) {
          if (arrayOfDrawable[i] != null) {
            Drawable.ConstantState constantState = arrayOfDrawable[i].getConstantState();
            if (constantState != null) {
              this.mDrawableFutures.put(i, constantState);
            } else {
              this.mDrawables[i] = arrayOfDrawable[i];
            } 
          } 
        } 
      } else {
        this.mDrawables = new Drawable[10];
        this.mNumChildren = 0;
      } 
    }
    
    public int getChangingConfigurations() {
      return this.mChangingConfigurations | this.mChildrenChangingConfigurations;
    }
    
    public final int addChild(Drawable param1Drawable) {
      int i = this.mNumChildren;
      if (i >= this.mDrawables.length)
        growArray(i, i + 10); 
      param1Drawable.mutate();
      param1Drawable.setVisible(false, true);
      param1Drawable.setCallback(this.mOwner);
      this.mDrawables[i] = param1Drawable;
      this.mNumChildren++;
      this.mChildrenChangingConfigurations |= param1Drawable.getChangingConfigurations();
      invalidateCache();
      this.mConstantPadding = null;
      this.mCheckedPadding = false;
      this.mCheckedConstantSize = false;
      this.mCheckedConstantState = false;
      return i;
    }
    
    void invalidateCache() {
      this.mCheckedOpacity = false;
      this.mCheckedStateful = false;
    }
    
    final int getCapacity() {
      return this.mDrawables.length;
    }
    
    private void createAllFutures() {
      SparseArray<Drawable.ConstantState> sparseArray = this.mDrawableFutures;
      if (sparseArray != null) {
        int i = sparseArray.size();
        for (byte b = 0; b < i; b++) {
          int j = this.mDrawableFutures.keyAt(b);
          Drawable.ConstantState constantState = (Drawable.ConstantState)this.mDrawableFutures.valueAt(b);
          this.mDrawables[j] = prepareDrawable(constantState.newDrawable(this.mSourceRes));
        } 
        this.mDrawableFutures = null;
      } 
    }
    
    private Drawable prepareDrawable(Drawable param1Drawable) {
      param1Drawable.setLayoutDirection(this.mLayoutDirection);
      param1Drawable = param1Drawable.mutate();
      param1Drawable.setCallback(this.mOwner);
      return param1Drawable;
    }
    
    public final int getChildCount() {
      return this.mNumChildren;
    }
    
    public final Drawable[] getChildren() {
      createAllFutures();
      return this.mDrawables;
    }
    
    public final Drawable getChild(int param1Int) {
      Drawable drawable = this.mDrawables[param1Int];
      if (drawable != null)
        return drawable; 
      SparseArray<Drawable.ConstantState> sparseArray = this.mDrawableFutures;
      if (sparseArray != null) {
        int i = sparseArray.indexOfKey(param1Int);
        if (i >= 0) {
          Drawable.ConstantState constantState = (Drawable.ConstantState)this.mDrawableFutures.valueAt(i);
          Drawable drawable1 = prepareDrawable(constantState.newDrawable(this.mSourceRes));
          this.mDrawables[param1Int] = drawable1;
          this.mDrawableFutures.removeAt(i);
          if (this.mDrawableFutures.size() == 0)
            this.mDrawableFutures = null; 
          return drawable1;
        } 
      } 
      return null;
    }
    
    final boolean setLayoutDirection(int param1Int1, int param1Int2) {
      boolean bool = false;
      int i = this.mNumChildren;
      Drawable[] arrayOfDrawable = this.mDrawables;
      for (byte b = 0; b < i; b++, bool = bool1) {
        boolean bool1 = bool;
        if (arrayOfDrawable[b] != null) {
          boolean bool2 = arrayOfDrawable[b].setLayoutDirection(param1Int1);
          bool1 = bool;
          if (b == param1Int2)
            bool1 = bool2; 
        } 
      } 
      this.mLayoutDirection = param1Int1;
      return bool;
    }
    
    final void updateDensity(Resources param1Resources) {
      if (param1Resources != null) {
        this.mSourceRes = param1Resources;
        int i = Drawable.resolveDensity(param1Resources, this.mDensity);
        int j = this.mDensity;
        this.mDensity = i;
        if (j != i) {
          this.mCheckedConstantSize = false;
          this.mCheckedPadding = false;
        } 
      } 
    }
    
    final void applyTheme(Resources.Theme param1Theme) {
      if (param1Theme != null) {
        createAllFutures();
        int i = this.mNumChildren;
        Drawable[] arrayOfDrawable = this.mDrawables;
        for (byte b = 0; b < i; b++) {
          if (arrayOfDrawable[b] != null && arrayOfDrawable[b].canApplyTheme()) {
            arrayOfDrawable[b].applyTheme(param1Theme);
            this.mChildrenChangingConfigurations |= arrayOfDrawable[b].getChangingConfigurations();
          } 
        } 
        updateDensity(param1Theme.getResources());
      } 
    }
    
    public boolean canApplyTheme() {
      int i = this.mNumChildren;
      Drawable[] arrayOfDrawable = this.mDrawables;
      for (byte b = 0; b < i; b++) {
        Drawable drawable = arrayOfDrawable[b];
        if (drawable != null) {
          if (drawable.canApplyTheme())
            return true; 
        } else {
          Drawable.ConstantState constantState = (Drawable.ConstantState)this.mDrawableFutures.get(b);
          if (constantState != null && constantState.canApplyTheme())
            return true; 
        } 
      } 
      return false;
    }
    
    private void mutate() {
      int i = this.mNumChildren;
      Drawable[] arrayOfDrawable = this.mDrawables;
      for (byte b = 0; b < i; b++) {
        if (arrayOfDrawable[b] != null)
          arrayOfDrawable[b].mutate(); 
      } 
      this.mMutated = true;
    }
    
    final void clearMutated() {
      int i = this.mNumChildren;
      Drawable[] arrayOfDrawable = this.mDrawables;
      for (byte b = 0; b < i; b++) {
        if (arrayOfDrawable[b] != null)
          arrayOfDrawable[b].clearMutated(); 
      } 
      this.mMutated = false;
    }
    
    public final void setVariablePadding(boolean param1Boolean) {
      this.mVariablePadding = param1Boolean;
    }
    
    public final Rect getConstantPadding() {
      if (this.mVariablePadding)
        return null; 
      if (this.mConstantPadding != null || this.mCheckedPadding)
        return this.mConstantPadding; 
      createAllFutures();
      Rect rect1 = null;
      Rect rect2 = new Rect();
      int i = this.mNumChildren;
      Drawable[] arrayOfDrawable = this.mDrawables;
      for (byte b = 0; b < i; b++, rect1 = rect) {
        Rect rect = rect1;
        if (arrayOfDrawable[b].getPadding(rect2)) {
          Rect rect3 = rect1;
          if (rect1 == null)
            rect3 = new Rect(0, 0, 0, 0); 
          if (rect2.left > rect3.left)
            rect3.left = rect2.left; 
          if (rect2.top > rect3.top)
            rect3.top = rect2.top; 
          if (rect2.right > rect3.right)
            rect3.right = rect2.right; 
          rect = rect3;
          if (rect2.bottom > rect3.bottom) {
            rect3.bottom = rect2.bottom;
            rect = rect3;
          } 
        } 
      } 
      this.mCheckedPadding = true;
      this.mConstantPadding = rect1;
      return rect1;
    }
    
    public final void setConstantSize(boolean param1Boolean) {
      this.mConstantSize = param1Boolean;
    }
    
    public final boolean isConstantSize() {
      return this.mConstantSize;
    }
    
    public final int getConstantWidth() {
      if (!this.mCheckedConstantSize)
        computeConstantSize(); 
      return this.mConstantWidth;
    }
    
    public final int getConstantHeight() {
      if (!this.mCheckedConstantSize)
        computeConstantSize(); 
      return this.mConstantHeight;
    }
    
    public final int getConstantMinimumWidth() {
      if (!this.mCheckedConstantSize)
        computeConstantSize(); 
      return this.mConstantMinimumWidth;
    }
    
    public final int getConstantMinimumHeight() {
      if (!this.mCheckedConstantSize)
        computeConstantSize(); 
      return this.mConstantMinimumHeight;
    }
    
    protected void computeConstantSize() {
      this.mCheckedConstantSize = true;
      createAllFutures();
      int i = this.mNumChildren;
      Drawable[] arrayOfDrawable = this.mDrawables;
      this.mConstantHeight = -1;
      this.mConstantWidth = -1;
      this.mConstantMinimumHeight = 0;
      this.mConstantMinimumWidth = 0;
      for (byte b = 0; b < i; b++) {
        Drawable drawable = arrayOfDrawable[b];
        int j = drawable.getIntrinsicWidth();
        if (j > this.mConstantWidth)
          this.mConstantWidth = j; 
        j = drawable.getIntrinsicHeight();
        if (j > this.mConstantHeight)
          this.mConstantHeight = j; 
        j = drawable.getMinimumWidth();
        if (j > this.mConstantMinimumWidth)
          this.mConstantMinimumWidth = j; 
        j = drawable.getMinimumHeight();
        if (j > this.mConstantMinimumHeight)
          this.mConstantMinimumHeight = j; 
      } 
    }
    
    public final void setEnterFadeDuration(int param1Int) {
      this.mEnterFadeDuration = param1Int;
    }
    
    public final int getEnterFadeDuration() {
      return this.mEnterFadeDuration;
    }
    
    public final void setExitFadeDuration(int param1Int) {
      this.mExitFadeDuration = param1Int;
    }
    
    public final int getExitFadeDuration() {
      return this.mExitFadeDuration;
    }
    
    public final int getOpacity() {
      int j;
      if (this.mCheckedOpacity)
        return this.mOpacity; 
      createAllFutures();
      int i = this.mNumChildren;
      Drawable[] arrayOfDrawable = this.mDrawables;
      if (i > 0) {
        j = arrayOfDrawable[0].getOpacity();
      } else {
        j = -2;
      } 
      for (byte b = 1; b < i; b++)
        j = Drawable.resolveOpacity(j, arrayOfDrawable[b].getOpacity()); 
      this.mOpacity = j;
      this.mCheckedOpacity = true;
      return j;
    }
    
    public final boolean isStateful() {
      boolean bool2;
      if (this.mCheckedStateful)
        return this.mStateful; 
      createAllFutures();
      int i = this.mNumChildren;
      Drawable[] arrayOfDrawable = this.mDrawables;
      boolean bool1 = false;
      byte b = 0;
      while (true) {
        bool2 = bool1;
        if (b < i) {
          if (arrayOfDrawable[b].isStateful()) {
            bool2 = true;
            break;
          } 
          b++;
          continue;
        } 
        break;
      } 
      this.mStateful = bool2;
      this.mCheckedStateful = true;
      return bool2;
    }
    
    public void growArray(int param1Int1, int param1Int2) {
      Drawable[] arrayOfDrawable = new Drawable[param1Int2];
      System.arraycopy(this.mDrawables, 0, arrayOfDrawable, 0, param1Int1);
      this.mDrawables = arrayOfDrawable;
    }
    
    public boolean canConstantState() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mCheckedConstantState : Z
      //   6: ifeq -> 18
      //   9: aload_0
      //   10: getfield mCanConstantState : Z
      //   13: istore_1
      //   14: aload_0
      //   15: monitorexit
      //   16: iload_1
      //   17: ireturn
      //   18: aload_0
      //   19: invokespecial createAllFutures : ()V
      //   22: aload_0
      //   23: iconst_1
      //   24: putfield mCheckedConstantState : Z
      //   27: aload_0
      //   28: getfield mNumChildren : I
      //   31: istore_2
      //   32: aload_0
      //   33: getfield mDrawables : [Landroid/graphics/drawable/Drawable;
      //   36: astore_3
      //   37: iconst_0
      //   38: istore #4
      //   40: iload #4
      //   42: iload_2
      //   43: if_icmpge -> 71
      //   46: aload_3
      //   47: iload #4
      //   49: aaload
      //   50: invokevirtual getConstantState : ()Landroid/graphics/drawable/Drawable$ConstantState;
      //   53: ifnonnull -> 65
      //   56: aload_0
      //   57: iconst_0
      //   58: putfield mCanConstantState : Z
      //   61: aload_0
      //   62: monitorexit
      //   63: iconst_0
      //   64: ireturn
      //   65: iinc #4, 1
      //   68: goto -> 40
      //   71: aload_0
      //   72: iconst_1
      //   73: putfield mCanConstantState : Z
      //   76: aload_0
      //   77: monitorexit
      //   78: iconst_1
      //   79: ireturn
      //   80: astore_3
      //   81: aload_0
      //   82: monitorexit
      //   83: aload_3
      //   84: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1206	-> 2
      //   #1207	-> 9
      //   #1210	-> 18
      //   #1212	-> 22
      //   #1214	-> 27
      //   #1215	-> 32
      //   #1216	-> 37
      //   #1217	-> 46
      //   #1218	-> 56
      //   #1219	-> 61
      //   #1216	-> 65
      //   #1223	-> 71
      //   #1224	-> 76
      //   #1205	-> 80
      // Exception table:
      //   from	to	target	type
      //   2	9	80	finally
      //   9	14	80	finally
      //   18	22	80	finally
      //   22	27	80	finally
      //   27	32	80	finally
      //   32	37	80	finally
      //   46	56	80	finally
      //   56	61	80	finally
      //   71	76	80	finally
    }
  }
  
  protected void setConstantState(DrawableContainerState paramDrawableContainerState) {
    this.mDrawableContainerState = paramDrawableContainerState;
    int i = this.mCurIndex;
    if (i >= 0) {
      Drawable drawable = paramDrawableContainerState.getChild(i);
      if (drawable != null)
        initializeDrawableForDisplay(drawable); 
    } 
    this.mLastIndex = -1;
    this.mLastDrawable = null;
  }
  
  class BlockInvalidateCallback implements Drawable.Callback {
    private Drawable.Callback mCallback;
    
    private BlockInvalidateCallback() {}
    
    public BlockInvalidateCallback wrap(Drawable.Callback param1Callback) {
      this.mCallback = param1Callback;
      return this;
    }
    
    public Drawable.Callback unwrap() {
      Drawable.Callback callback = this.mCallback;
      this.mCallback = null;
      return callback;
    }
    
    public void invalidateDrawable(Drawable param1Drawable) {}
    
    public void scheduleDrawable(Drawable param1Drawable, Runnable param1Runnable, long param1Long) {
      Drawable.Callback callback = this.mCallback;
      if (callback != null)
        callback.scheduleDrawable(param1Drawable, param1Runnable, param1Long); 
    }
    
    public void unscheduleDrawable(Drawable param1Drawable, Runnable param1Runnable) {
      Drawable.Callback callback = this.mCallback;
      if (callback != null)
        callback.unscheduleDrawable(param1Drawable, param1Runnable); 
    }
  }
}
