package android.graphics.drawable;

import android.common.OplusFeatureCache;
import android.graphics.Bitmap;
import android.graphics.BlendModeColorFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.OplusBaseBaseCanvas;
import android.graphics.OplusBaseBitmap;
import android.graphics.OplusBaseOplusFilter;
import android.graphics.PorterDuffColorFilter;
import com.android.internal.graphics.ColorUtils;
import com.oplus.darkmode.IOplusDarkModeManager;
import com.oplus.util.OplusTypeCastingHelper;

public abstract class OplusBaseDrawable {
  boolean shouldRestoreFilterColor = false;
  
  private boolean mHasSetColor = false;
  
  public void changeVectorColor(boolean paramBoolean) {}
  
  void handleVectorDraw(Canvas paramCanvas, ColorFilter paramColorFilter) {
    if (isDarkMode(paramCanvas))
      if (paramColorFilter != null) {
        changeFilter(paramColorFilter);
      } else if (!hasSetColor()) {
        changeVectorColor(false);
        setHasSetColor(true);
      }  
  }
  
  void restoreVectorDraw(Canvas paramCanvas, ColorFilter paramColorFilter) {
    if (isDarkMode(paramCanvas))
      if (paramColorFilter != null) {
        restoreFilter(paramColorFilter);
      } else if (hasSetColor()) {
        changeVectorColor(true);
        setHasSetColor(false);
      }  
  }
  
  private void changeFilter(ColorFilter paramColorFilter) {
    if (paramColorFilter instanceof BlendModeColorFilter) {
      this.mFilterColor = ((BlendModeColorFilter)paramColorFilter).getColor();
      this.shouldRestoreFilterColor = true;
      ((IOplusDarkModeManager)OplusFeatureCache.<IOplusDarkModeManager>getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0])).changeColorFilterInDarkMode(paramColorFilter);
    } else if (paramColorFilter instanceof PorterDuffColorFilter) {
      this.mFilterColor = ((PorterDuffColorFilter)paramColorFilter).getColor();
      this.shouldRestoreFilterColor = true;
      ((IOplusDarkModeManager)OplusFeatureCache.<IOplusDarkModeManager>getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0])).changeColorFilterInDarkMode(paramColorFilter);
    } 
  }
  
  private void restoreFilter(ColorFilter paramColorFilter) {
    if (paramColorFilter != null && this.shouldRestoreFilterColor) {
      this.shouldRestoreFilterColor = false;
      OplusBaseOplusFilter oplusBaseOplusFilter = (OplusBaseOplusFilter)OplusTypeCastingHelper.typeCasting(OplusBaseOplusFilter.class, paramColorFilter);
      if (oplusBaseOplusFilter != null)
        oplusBaseOplusFilter.setColor(this.mFilterColor); 
    } 
  }
  
  public void changePath(OplusBaseVObject paramOplusBaseVObject, boolean paramBoolean) {
    if (paramBoolean) {
      paramOplusBaseVObject.setFillColor(paramOplusBaseVObject.mShouldRestoreFillColor);
      paramOplusBaseVObject.setStrokeColor(paramOplusBaseVObject.mShouldRestoreStrokeColor);
    } else {
      paramOplusBaseVObject.mShouldRestoreFillColor = paramOplusBaseVObject.getFillColor();
      paramOplusBaseVObject.mShouldRestoreStrokeColor = paramOplusBaseVObject.getStrokeColor();
      IOplusDarkModeManager iOplusDarkModeManager = OplusFeatureCache.<IOplusDarkModeManager>getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0]);
      paramOplusBaseVObject.setFillColor(iOplusDarkModeManager.getVectorColor(paramOplusBaseVObject.getFillColor()));
      paramOplusBaseVObject.setStrokeColor(iOplusDarkModeManager.getVectorColor(paramOplusBaseVObject.getStrokeColor()));
    } 
  }
  
  private boolean isDarkMode(Canvas paramCanvas) {
    if (paramCanvas != null) {
      OplusBaseBaseCanvas oplusBaseBaseCanvas = (OplusBaseBaseCanvas)OplusTypeCastingHelper.typeCasting(OplusBaseBaseCanvas.class, paramCanvas);
      if (oplusBaseBaseCanvas != null)
        return oplusBaseBaseCanvas.isDarkMode(); 
    } 
    return false;
  }
  
  protected void setBitmapIsViewSrc(Bitmap paramBitmap, boolean paramBoolean) {
    if (paramBitmap != null) {
      OplusBaseBitmap oplusBaseBitmap = (OplusBaseBitmap)OplusTypeCastingHelper.typeCasting(OplusBaseBitmap.class, paramBitmap);
      if (oplusBaseBitmap != null && 
        oplusBaseBitmap.isCanvasBaseBitmap())
        oplusBaseBitmap.setIsViewSrc(paramBoolean); 
    } 
  }
  
  protected void setHasSetColor(boolean paramBoolean) {
    this.mHasSetColor = paramBoolean;
  }
  
  protected boolean hasSetColor() {
    return this.mHasSetColor;
  }
  
  private SumEntity mHEntity = new SumEntity();
  
  private SumEntity mSEntity = new SumEntity();
  
  private SumEntity mLEntity = new SumEntity();
  
  int mFilterColor;
  
  public static class SumEntity {
    float mMin = 0.0F;
    
    float mMax = 0.0F;
    
    float mTotal = 0.0F;
    
    int mCount = 0;
    
    public void reset() {
      this.mMin = 0.0F;
      this.mMax = 0.0F;
      this.mTotal = 0.0F;
      this.mCount = 0;
    }
    
    public void add(float param1Float) {
      if (this.mCount == 0) {
        this.mMin = param1Float;
        this.mMax = param1Float;
      } else {
        this.mMin = Math.min(this.mMin, param1Float);
        this.mMax = Math.max(this.mMax, param1Float);
      } 
      this.mTotal += param1Float;
      this.mCount++;
    }
    
    public float average() {
      return this.mTotal / this.mCount;
    }
    
    public float min() {
      return this.mMin;
    }
    
    public float max() {
      return this.mMax;
    }
    
    public float delta() {
      return this.mMax - this.mMin;
    }
    
    public int count() {
      return this.mCount;
    }
  }
  
  void calculateVectorColor() {}
  
  void calculatePathColor(OplusBaseVObject paramOplusBaseVObject) {
    int i = paramOplusBaseVObject.getFillColor();
    float f = paramOplusBaseVObject.getFillAlpha();
    if (Color.alpha(i) * f >= 75.0F) {
      float[] arrayOfFloat = new float[3];
      ColorUtils.colorToHSL(i, arrayOfFloat);
      this.mHEntity.add(arrayOfFloat[0]);
      this.mSEntity.add(arrayOfFloat[1]);
      this.mLEntity.add(arrayOfFloat[2]);
    } 
    i = paramOplusBaseVObject.getStrokeColor();
    f = paramOplusBaseVObject.getStrokeAlpha();
    if (Color.alpha(i) * f >= 75.0F) {
      float[] arrayOfFloat = new float[3];
      ColorUtils.colorToHSL(i, arrayOfFloat);
      this.mHEntity.add(arrayOfFloat[0]);
      this.mSEntity.add(arrayOfFloat[1]);
      this.mLEntity.add(arrayOfFloat[2]);
    } 
  }
  
  long changePaintWhenVectorDraw(VectorDrawable paramVectorDrawable, Canvas paramCanvas, ColorFilter paramColorFilter) {
    boolean bool = isDarkMode(paramCanvas);
    long l = 0L;
    if (bool)
      if (paramColorFilter != null) {
        changeFilter(paramColorFilter);
      } else {
        calculateVectorColor();
        ColorFilter colorFilter = ((IOplusDarkModeManager)OplusFeatureCache.<IOplusDarkModeManager>getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0])).getColorFilterWhenDrawVectorDrawable(this.mHEntity, this.mSEntity, this.mLEntity);
        this.mHEntity.reset();
        this.mSEntity.reset();
        this.mLEntity.reset();
        if (colorFilter != null)
          l = colorFilter.getNativeInstance(); 
        return l;
      }  
    if (paramColorFilter != null)
      l = paramColorFilter.getNativeInstance(); 
    return l;
  }
  
  void resetPaintWhenVectorDraw(VectorDrawable paramVectorDrawable, Canvas paramCanvas, ColorFilter paramColorFilter) {
    if (isDarkMode(paramCanvas) && 
      paramColorFilter != null)
      restoreFilter(paramColorFilter); 
  }
}
