package android.graphics.drawable;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.BlendMode;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.util.MathUtils;

public class ColorStateListDrawable extends Drawable implements Drawable.Callback {
  private ColorDrawable mColorDrawable;
  
  private boolean mMutated = false;
  
  private ColorStateListDrawableState mState;
  
  public ColorStateListDrawable() {
    this.mState = new ColorStateListDrawableState();
    initializeColorDrawable();
  }
  
  public ColorStateListDrawable(ColorStateList paramColorStateList) {
    this.mState = new ColorStateListDrawableState();
    initializeColorDrawable();
    setColorStateList(paramColorStateList);
  }
  
  public void draw(Canvas paramCanvas) {
    this.mColorDrawable.draw(paramCanvas);
  }
  
  public int getAlpha() {
    return this.mColorDrawable.getAlpha();
  }
  
  public boolean isStateful() {
    return this.mState.isStateful();
  }
  
  public boolean hasFocusStateSpecified() {
    return this.mState.hasFocusStateSpecified();
  }
  
  public Drawable getCurrent() {
    return this.mColorDrawable;
  }
  
  public void applyTheme(Resources.Theme paramTheme) {
    super.applyTheme(paramTheme);
    if (this.mState.mColor != null)
      setColorStateList(this.mState.mColor.obtainForTheme(paramTheme)); 
    if (this.mState.mTint != null)
      setTintList(this.mState.mTint.obtainForTheme(paramTheme)); 
  }
  
  public boolean canApplyTheme() {
    return (super.canApplyTheme() || this.mState.canApplyTheme());
  }
  
  public void setAlpha(int paramInt) {
    this.mState.mAlpha = paramInt;
    onStateChange(getState());
  }
  
  public void clearAlpha() {
    this.mState.mAlpha = -1;
    onStateChange(getState());
  }
  
  public void setTintList(ColorStateList paramColorStateList) {
    this.mState.mTint = paramColorStateList;
    this.mColorDrawable.setTintList(paramColorStateList);
    onStateChange(getState());
  }
  
  public void setTintBlendMode(BlendMode paramBlendMode) {
    this.mState.mBlendMode = paramBlendMode;
    this.mColorDrawable.setTintBlendMode(paramBlendMode);
    onStateChange(getState());
  }
  
  public ColorFilter getColorFilter() {
    return this.mColorDrawable.getColorFilter();
  }
  
  public void setColorFilter(ColorFilter paramColorFilter) {
    this.mColorDrawable.setColorFilter(paramColorFilter);
  }
  
  public int getOpacity() {
    return this.mColorDrawable.getOpacity();
  }
  
  protected void onBoundsChange(Rect paramRect) {
    super.onBoundsChange(paramRect);
    this.mColorDrawable.setBounds(paramRect);
  }
  
  protected boolean onStateChange(int[] paramArrayOfint) {
    if (this.mState.mColor != null) {
      int i = this.mState.mColor.getColorForState(paramArrayOfint, this.mState.mColor.getDefaultColor());
      int j = i;
      if (this.mState.mAlpha != -1)
        j = 0xFFFFFF & i | MathUtils.constrain(this.mState.mAlpha, 0, 255) << 24; 
      if (j != this.mColorDrawable.getColor()) {
        this.mColorDrawable.setColor(j);
        this.mColorDrawable.setState(paramArrayOfint);
        return true;
      } 
      return this.mColorDrawable.setState(paramArrayOfint);
    } 
    return false;
  }
  
  public void invalidateDrawable(Drawable paramDrawable) {
    if (paramDrawable == this.mColorDrawable && getCallback() != null)
      getCallback().invalidateDrawable(this); 
  }
  
  public void scheduleDrawable(Drawable paramDrawable, Runnable paramRunnable, long paramLong) {
    if (paramDrawable == this.mColorDrawable && getCallback() != null)
      getCallback().scheduleDrawable(this, paramRunnable, paramLong); 
  }
  
  public void unscheduleDrawable(Drawable paramDrawable, Runnable paramRunnable) {
    if (paramDrawable == this.mColorDrawable && getCallback() != null)
      getCallback().unscheduleDrawable(this, paramRunnable); 
  }
  
  public Drawable.ConstantState getConstantState() {
    ColorStateListDrawableState colorStateListDrawableState = this.mState;
    int i = colorStateListDrawableState.mChangingConfigurations;
    colorStateListDrawableState.mChangingConfigurations = i | getChangingConfigurations() & (this.mState.getChangingConfigurations() ^ 0xFFFFFFFF);
    return this.mState;
  }
  
  public ColorStateList getColorStateList() {
    if (this.mState.mColor == null)
      return ColorStateList.valueOf(this.mColorDrawable.getColor()); 
    return this.mState.mColor;
  }
  
  public int getChangingConfigurations() {
    return super.getChangingConfigurations() | this.mState.getChangingConfigurations();
  }
  
  public Drawable mutate() {
    if (!this.mMutated && super.mutate() == this) {
      this.mState = new ColorStateListDrawableState(this.mState);
      this.mMutated = true;
    } 
    return this;
  }
  
  public void clearMutated() {
    super.clearMutated();
    this.mMutated = false;
  }
  
  public void setColorStateList(ColorStateList paramColorStateList) {
    this.mState.mColor = paramColorStateList;
    onStateChange(getState());
  }
  
  class ColorStateListDrawableState extends Drawable.ConstantState {
    ColorStateList mColor = null;
    
    ColorStateList mTint = null;
    
    int mAlpha = -1;
    
    BlendMode mBlendMode = Drawable.DEFAULT_BLEND_MODE;
    
    int mChangingConfigurations = 0;
    
    ColorStateListDrawableState(ColorStateListDrawable this$0) {
      this.mColor = ((ColorStateListDrawableState)this$0).mColor;
      this.mTint = ((ColorStateListDrawableState)this$0).mTint;
      this.mAlpha = ((ColorStateListDrawableState)this$0).mAlpha;
      this.mBlendMode = ((ColorStateListDrawableState)this$0).mBlendMode;
      this.mChangingConfigurations = ((ColorStateListDrawableState)this$0).mChangingConfigurations;
    }
    
    public Drawable newDrawable() {
      return new ColorStateListDrawable(this);
    }
    
    public int getChangingConfigurations() {
      byte b;
      int i = this.mChangingConfigurations;
      ColorStateList colorStateList = this.mColor;
      int j = 0;
      if (colorStateList != null) {
        b = colorStateList.getChangingConfigurations();
      } else {
        b = 0;
      } 
      colorStateList = this.mTint;
      if (colorStateList != null)
        j = colorStateList.getChangingConfigurations(); 
      return i | b | j;
    }
    
    public boolean isStateful() {
      ColorStateList colorStateList = this.mColor;
      if (colorStateList == null || !colorStateList.isStateful()) {
        colorStateList = this.mTint;
        return (colorStateList != null && 
          colorStateList.isStateful());
      } 
      return true;
    }
    
    public boolean hasFocusStateSpecified() {
      ColorStateList colorStateList = this.mColor;
      if (colorStateList == null || !colorStateList.hasFocusStateSpecified()) {
        colorStateList = this.mTint;
        return (colorStateList != null && 
          colorStateList.hasFocusStateSpecified());
      } 
      return true;
    }
    
    public boolean canApplyTheme() {
      ColorStateList colorStateList = this.mColor;
      if (colorStateList == null || !colorStateList.canApplyTheme()) {
        colorStateList = this.mTint;
        return (colorStateList != null && 
          colorStateList.canApplyTheme());
      } 
      return true;
    }
    
    ColorStateListDrawableState() {}
  }
  
  private ColorStateListDrawable(ColorStateListDrawableState paramColorStateListDrawableState) {
    this.mState = paramColorStateListDrawableState;
    initializeColorDrawable();
  }
  
  private void initializeColorDrawable() {
    ColorDrawable colorDrawable = new ColorDrawable();
    colorDrawable.setCallback(this);
    if (this.mState.mTint != null)
      this.mColorDrawable.setTintList(this.mState.mTint); 
    if (this.mState.mBlendMode != DEFAULT_BLEND_MODE)
      this.mColorDrawable.setTintBlendMode(this.mState.mBlendMode); 
  }
}
