package android.graphics.drawable;

import android.app.ActivityThread;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.BlendMode;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.PathParser;
import com.android.internal.R;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class AdaptiveIconDrawable extends OplusBaseAdaptiveIconDrawable implements Drawable.Callback {
  private static final int BACKGROUND_ID = 0;
  
  private static final float DEFAULT_VIEW_PORT_SCALE = 0.6666667F;
  
  private static final float EXTRA_INSET_PERCENTAGE = 0.25F;
  
  private static final int FOREGROUND_ID = 1;
  
  public static final float MASK_SIZE = 100.0F;
  
  private static final float SAFEZONE_SCALE = 0.9166667F;
  
  private static Path sMask;
  
  private final Canvas mCanvas;
  
  private boolean mChildRequestedInvalidation;
  
  private Rect mHotspotBounds;
  
  LayerState mLayerState;
  
  private Bitmap mLayersBitmap;
  
  private Shader mLayersShader;
  
  private final Path mMask;
  
  private final Matrix mMaskMatrix;
  
  private final Path mMaskScaleOnly;
  
  private boolean mMutated;
  
  private Paint mPaint;
  
  private boolean mSuspendChildInvalidation;
  
  private final Rect mTmpOutRect;
  
  private final Region mTransparentRegion;
  
  AdaptiveIconDrawable() {
    this((LayerState)null, (Resources)null);
  }
  
  AdaptiveIconDrawable(LayerState paramLayerState, Resources paramResources) {
    Resources resources;
    this.mTmpOutRect = new Rect();
    this.mPaint = new Paint(7);
    this.mLayerState = createConstantState(paramLayerState, paramResources);
    if (ActivityThread.currentActivityThread() == null) {
      resources = Resources.getSystem();
    } else {
      resources = ActivityThread.currentActivityThread().getApplication().getResources();
    } 
    sMask = PathParser.createPathFromPathData(resources.getString(17039918));
    this.mMask = new Path(sMask);
    this.mMaskScaleOnly = new Path(this.mMask);
    this.mMaskMatrix = new Matrix();
    this.mCanvas = new Canvas();
    this.mTransparentRegion = new Region();
  }
  
  private ChildDrawable createChildDrawable(Drawable paramDrawable) {
    ChildDrawable childDrawable = new ChildDrawable(this.mLayerState.mDensity);
    childDrawable.mDrawable = paramDrawable;
    childDrawable.mDrawable.setCallback(this);
    LayerState layerState = this.mLayerState;
    int i = layerState.mChildrenChangingConfigurations;
    Drawable drawable = childDrawable.mDrawable;
    layerState.mChildrenChangingConfigurations = i | drawable.getChangingConfigurations();
    return childDrawable;
  }
  
  LayerState createConstantState(LayerState paramLayerState, Resources paramResources) {
    return new LayerState(paramLayerState, this, paramResources);
  }
  
  public AdaptiveIconDrawable(Drawable paramDrawable1, Drawable paramDrawable2) {
    this((LayerState)null, (Resources)null);
    if (paramDrawable1 != null)
      addLayer(0, createChildDrawable(paramDrawable1)); 
    if (paramDrawable2 != null)
      addLayer(1, createChildDrawable(paramDrawable2)); 
  }
  
  private void addLayer(int paramInt, ChildDrawable paramChildDrawable) {
    this.mLayerState.mChildren[paramInt] = paramChildDrawable;
    this.mLayerState.invalidateCache();
  }
  
  public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme) throws XmlPullParserException, IOException {
    super.inflate(paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
    LayerState layerState = this.mLayerState;
    if (layerState == null)
      return; 
    int i = Drawable.resolveDensity(paramResources, 0);
    layerState.setDensity(i);
    layerState.mSrcDensityOverride = this.mSrcDensityOverride;
    layerState.mSourceDrawableId = Resources.getAttributeSetSourceResId(paramAttributeSet);
    ChildDrawable[] arrayOfChildDrawable = layerState.mChildren;
    for (byte b = 0; b < layerState.mChildren.length; b++) {
      ChildDrawable childDrawable = arrayOfChildDrawable[b];
      childDrawable.setDensity(i);
    } 
    inflateLayers(paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
  }
  
  public static float getExtraInsetFraction() {
    return 0.25F;
  }
  
  public static float getExtraInsetPercentage() {
    return 0.25F;
  }
  
  public Path getIconMask() {
    if (hookGetIconMask() != null)
      return hookGetIconMask(); 
    return this.mMask;
  }
  
  public Drawable getForeground() {
    return (this.mLayerState.mChildren[1]).mDrawable;
  }
  
  public Drawable getBackground() {
    return (this.mLayerState.mChildren[0]).mDrawable;
  }
  
  protected void onBoundsChange(Rect paramRect) {
    if (paramRect.isEmpty())
      return; 
    updateLayerBounds(paramRect);
  }
  
  private void updateLayerBounds(Rect paramRect) {
    if (paramRect.isEmpty())
      return; 
    try {
      suspendChildInvalidation();
      if (!hookOnBoundsChange(paramRect)) {
        updateLayerBoundsInternal(paramRect);
        updateMaskBoundsInternal(paramRect);
      } 
      return;
    } finally {
      resumeChildInvalidation();
    } 
  }
  
  private void updateLayerBoundsInternal(Rect paramRect) {
    int i = paramRect.width() / 2;
    int j = paramRect.height() / 2;
    for (byte b = 0; b < 2; b++) {
      ChildDrawable childDrawable = this.mLayerState.mChildren[b];
      if (childDrawable != null) {
        Drawable drawable = childDrawable.mDrawable;
        if (drawable != null) {
          int k = (int)(paramRect.width() / 1.3333334F);
          int m = (int)(paramRect.height() / 1.3333334F);
          Rect rect = this.mTmpOutRect;
          rect.set(i - k, j - m, i + k, j + m);
          drawable.setBounds(rect);
        } 
      } 
    } 
  }
  
  private void updateMaskBoundsInternal(Rect paramRect) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mMaskMatrix : Landroid/graphics/Matrix;
    //   4: aload_1
    //   5: invokevirtual width : ()I
    //   8: i2f
    //   9: ldc 100.0
    //   11: fdiv
    //   12: aload_1
    //   13: invokevirtual height : ()I
    //   16: i2f
    //   17: ldc 100.0
    //   19: fdiv
    //   20: invokevirtual setScale : (FF)V
    //   23: getstatic android/graphics/drawable/AdaptiveIconDrawable.sMask : Landroid/graphics/Path;
    //   26: aload_0
    //   27: getfield mMaskMatrix : Landroid/graphics/Matrix;
    //   30: aload_0
    //   31: getfield mMaskScaleOnly : Landroid/graphics/Path;
    //   34: invokevirtual transform : (Landroid/graphics/Matrix;Landroid/graphics/Path;)V
    //   37: aload_0
    //   38: getfield mMaskMatrix : Landroid/graphics/Matrix;
    //   41: aload_1
    //   42: getfield left : I
    //   45: i2f
    //   46: aload_1
    //   47: getfield top : I
    //   50: i2f
    //   51: invokevirtual postTranslate : (FF)Z
    //   54: pop
    //   55: getstatic android/graphics/drawable/AdaptiveIconDrawable.sMask : Landroid/graphics/Path;
    //   58: aload_0
    //   59: getfield mMaskMatrix : Landroid/graphics/Matrix;
    //   62: aload_0
    //   63: getfield mMask : Landroid/graphics/Path;
    //   66: invokevirtual transform : (Landroid/graphics/Matrix;Landroid/graphics/Path;)V
    //   69: aload_0
    //   70: getfield mLayersBitmap : Landroid/graphics/Bitmap;
    //   73: astore_2
    //   74: aload_2
    //   75: ifnull -> 105
    //   78: aload_2
    //   79: invokevirtual getWidth : ()I
    //   82: aload_1
    //   83: invokevirtual width : ()I
    //   86: if_icmpne -> 105
    //   89: aload_0
    //   90: getfield mLayersBitmap : Landroid/graphics/Bitmap;
    //   93: astore_2
    //   94: aload_2
    //   95: invokevirtual getHeight : ()I
    //   98: aload_1
    //   99: invokevirtual height : ()I
    //   102: if_icmpeq -> 123
    //   105: aload_0
    //   106: aload_1
    //   107: invokevirtual width : ()I
    //   110: aload_1
    //   111: invokevirtual height : ()I
    //   114: getstatic android/graphics/Bitmap$Config.ARGB_8888 : Landroid/graphics/Bitmap$Config;
    //   117: invokestatic createBitmap : (IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    //   120: putfield mLayersBitmap : Landroid/graphics/Bitmap;
    //   123: aload_0
    //   124: getfield mPaint : Landroid/graphics/Paint;
    //   127: aconst_null
    //   128: invokevirtual setShader : (Landroid/graphics/Shader;)Landroid/graphics/Shader;
    //   131: pop
    //   132: aload_0
    //   133: getfield mTransparentRegion : Landroid/graphics/Region;
    //   136: invokevirtual setEmpty : ()V
    //   139: aload_0
    //   140: aconst_null
    //   141: putfield mLayersShader : Landroid/graphics/Shader;
    //   144: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #356	-> 0
    //   #357	-> 23
    //   #359	-> 37
    //   #360	-> 55
    //   #362	-> 69
    //   #363	-> 94
    //   #364	-> 105
    //   #367	-> 123
    //   #368	-> 132
    //   #369	-> 139
    //   #370	-> 144
  }
  
  public void draw(Canvas paramCanvas) {
    if (hookDraw(paramCanvas))
      return; 
    Bitmap bitmap = this.mLayersBitmap;
    if (bitmap == null)
      return; 
    if (this.mLayersShader == null) {
      this.mCanvas.setBitmap(bitmap);
      this.mCanvas.drawColor(-16777216);
      byte b = 0;
      while (true) {
        LayerState layerState = this.mLayerState;
        if (b < 2) {
          if (layerState.mChildren[b] != null) {
            Drawable drawable = (this.mLayerState.mChildren[b]).mDrawable;
            if (drawable != null)
              drawable.draw(this.mCanvas); 
          } 
          b++;
          continue;
        } 
        break;
      } 
      BitmapShader bitmapShader = new BitmapShader(this.mLayersBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
      this.mPaint.setShader(bitmapShader);
    } 
    if (this.mMaskScaleOnly != null) {
      Rect rect = getBounds();
      paramCanvas.translate(rect.left, rect.top);
      paramCanvas.drawPath(this.mMaskScaleOnly, this.mPaint);
      paramCanvas.translate(-rect.left, -rect.top);
    } 
  }
  
  public void invalidateSelf() {
    this.mLayersShader = null;
    super.invalidateSelf();
  }
  
  public void getOutline(Outline paramOutline) {
    paramOutline.setPath(this.mMask);
  }
  
  public Region getSafeZone() {
    this.mMaskMatrix.reset();
    this.mMaskMatrix.setScale(0.9166667F, 0.9166667F, getBounds().centerX(), getBounds().centerY());
    Path path = new Path();
    this.mMask.transform(this.mMaskMatrix, path);
    Region region = new Region(getBounds());
    region.setPath(path, region);
    return region;
  }
  
  public Region getTransparentRegion() {
    if (this.mTransparentRegion.isEmpty()) {
      this.mMask.toggleInverseFillType();
      this.mTransparentRegion.set(getBounds());
      Region region = this.mTransparentRegion;
      region.setPath(this.mMask, region);
      this.mMask.toggleInverseFillType();
    } 
    return this.mTransparentRegion;
  }
  
  public void applyTheme(Resources.Theme paramTheme) {
    super.applyTheme(paramTheme);
    LayerState layerState = this.mLayerState;
    if (layerState == null)
      return; 
    int i = Drawable.resolveDensity(paramTheme.getResources(), 0);
    layerState.setDensity(i);
    ChildDrawable[] arrayOfChildDrawable = layerState.mChildren;
    for (byte b = 0; b < 2; b++) {
      ChildDrawable childDrawable = arrayOfChildDrawable[b];
      childDrawable.setDensity(i);
      if (childDrawable.mThemeAttrs != null) {
        TypedArray typedArray = paramTheme.resolveAttributes(childDrawable.mThemeAttrs, R.styleable.AdaptiveIconDrawableLayer);
        updateLayerFromTypedArray(childDrawable, typedArray);
        typedArray.recycle();
      } 
      Drawable drawable = childDrawable.mDrawable;
      if (drawable != null && drawable.canApplyTheme()) {
        drawable.applyTheme(paramTheme);
        layerState.mChildrenChangingConfigurations |= drawable.getChangingConfigurations();
      } 
    } 
  }
  
  public int getSourceDrawableResId() {
    int i;
    LayerState layerState = this.mLayerState;
    if (layerState == null) {
      i = 0;
    } else {
      i = layerState.mSourceDrawableId;
    } 
    return i;
  }
  
  private void inflateLayers(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme) throws XmlPullParserException, IOException {
    LayerState layerState = this.mLayerState;
    int i = paramXmlPullParser.getDepth() + 1;
    while (true) {
      int j = paramXmlPullParser.next();
      if (j != 1) {
        int k = paramXmlPullParser.getDepth();
        if (k >= i || j != 3) {
          if (j != 2)
            continue; 
          if (k > i)
            continue; 
          String str = paramXmlPullParser.getName();
          if (str.equals("background")) {
            j = 0;
          } else if (str.equals("foreground")) {
            j = 1;
          } else {
            continue;
          } 
          ChildDrawable childDrawable = new ChildDrawable(layerState.mDensity);
          TypedArray typedArray = obtainAttributes(paramResources, paramTheme, paramAttributeSet, R.styleable.AdaptiveIconDrawableLayer);
          updateLayerFromTypedArray(childDrawable, typedArray);
          typedArray.recycle();
          if (childDrawable.mDrawable == null && childDrawable.mThemeAttrs == null) {
            while (true) {
              k = paramXmlPullParser.next();
              if (k == 4)
                continue; 
              break;
            } 
            if (k == 2) {
              childDrawable.mDrawable = Drawable.createFromXmlInnerForDensity(paramResources, paramXmlPullParser, paramAttributeSet, this.mLayerState.mSrcDensityOverride, paramTheme);
              childDrawable.mDrawable.setCallback(this);
              k = layerState.mChildrenChangingConfigurations;
              Drawable drawable = childDrawable.mDrawable;
              layerState.mChildrenChangingConfigurations = k | drawable.getChangingConfigurations();
            } else {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append(paramXmlPullParser.getPositionDescription());
              stringBuilder.append(": <foreground> or <background> tag requires a 'drawable'attribute or child tag defining a drawable");
              throw new XmlPullParserException(stringBuilder.toString());
            } 
          } 
          addLayer(j, childDrawable);
          continue;
        } 
      } 
      break;
    } 
  }
  
  private void updateLayerFromTypedArray(ChildDrawable paramChildDrawable, TypedArray paramTypedArray) {
    LayerState layerState = this.mLayerState;
    layerState.mChildrenChangingConfigurations |= paramTypedArray.getChangingConfigurations();
    paramChildDrawable.mThemeAttrs = paramTypedArray.extractThemeAttrs();
    Drawable drawable = paramTypedArray.getDrawableForDensity(0, layerState.mSrcDensityOverride);
    if (drawable != null) {
      if (paramChildDrawable.mDrawable != null)
        paramChildDrawable.mDrawable.setCallback(null); 
      paramChildDrawable.mDrawable = drawable;
      paramChildDrawable.mDrawable.setCallback(this);
      int i = layerState.mChildrenChangingConfigurations;
      Drawable drawable1 = paramChildDrawable.mDrawable;
      layerState.mChildrenChangingConfigurations = i | drawable1.getChangingConfigurations();
    } 
  }
  
  public boolean canApplyTheme() {
    boolean bool;
    LayerState layerState = this.mLayerState;
    if ((layerState != null && layerState.canApplyTheme()) || super.canApplyTheme()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isProjected() {
    if (super.isProjected())
      return true; 
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    for (byte b = 0; b < 2; b++) {
      if ((arrayOfChildDrawable[b]).mDrawable != null && (arrayOfChildDrawable[b]).mDrawable.isProjected())
        return true; 
    } 
    return false;
  }
  
  private void suspendChildInvalidation() {
    this.mSuspendChildInvalidation = true;
  }
  
  private void resumeChildInvalidation() {
    this.mSuspendChildInvalidation = false;
    if (this.mChildRequestedInvalidation) {
      this.mChildRequestedInvalidation = false;
      invalidateSelf();
    } 
  }
  
  public void invalidateDrawable(Drawable paramDrawable) {
    if (this.mSuspendChildInvalidation) {
      this.mChildRequestedInvalidation = true;
    } else {
      invalidateSelf();
    } 
  }
  
  public void scheduleDrawable(Drawable paramDrawable, Runnable paramRunnable, long paramLong) {
    scheduleSelf(paramRunnable, paramLong);
  }
  
  public void unscheduleDrawable(Drawable paramDrawable, Runnable paramRunnable) {
    unscheduleSelf(paramRunnable);
  }
  
  public int getChangingConfigurations() {
    return super.getChangingConfigurations() | this.mLayerState.getChangingConfigurations();
  }
  
  public void setHotspot(float paramFloat1, float paramFloat2) {
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    for (byte b = 0; b < 2; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.setHotspot(paramFloat1, paramFloat2); 
    } 
  }
  
  public void setHotspotBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    for (byte b = 0; b < 2; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.setHotspotBounds(paramInt1, paramInt2, paramInt3, paramInt4); 
    } 
    Rect rect = this.mHotspotBounds;
    if (rect == null) {
      this.mHotspotBounds = new Rect(paramInt1, paramInt2, paramInt3, paramInt4);
    } else {
      rect.set(paramInt1, paramInt2, paramInt3, paramInt4);
    } 
  }
  
  public void getHotspotBounds(Rect paramRect) {
    Rect rect = this.mHotspotBounds;
    if (rect != null) {
      paramRect.set(rect);
    } else {
      super.getHotspotBounds(paramRect);
    } 
  }
  
  public boolean setVisible(boolean paramBoolean1, boolean paramBoolean2) {
    boolean bool = super.setVisible(paramBoolean1, paramBoolean2);
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    for (byte b = 0; b < 2; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.setVisible(paramBoolean1, paramBoolean2); 
    } 
    return bool;
  }
  
  public void setDither(boolean paramBoolean) {
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    for (byte b = 0; b < 2; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.setDither(paramBoolean); 
    } 
  }
  
  public void setAlpha(int paramInt) {
    this.mPaint.setAlpha(paramInt);
  }
  
  public int getAlpha() {
    return this.mPaint.getAlpha();
  }
  
  public void setColorFilter(ColorFilter paramColorFilter) {
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    for (byte b = 0; b < 2; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.setColorFilter(paramColorFilter); 
    } 
  }
  
  public void setTintList(ColorStateList paramColorStateList) {
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    for (byte b = 0; b < 2; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.setTintList(paramColorStateList); 
    } 
  }
  
  public void setTintBlendMode(BlendMode paramBlendMode) {
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    for (byte b = 0; b < 2; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.setTintBlendMode(paramBlendMode); 
    } 
  }
  
  public void setOpacity(int paramInt) {
    this.mLayerState.mOpacityOverride = paramInt;
  }
  
  public int getOpacity() {
    return -3;
  }
  
  public void setAutoMirrored(boolean paramBoolean) {
    LayerState.access$002(this.mLayerState, paramBoolean);
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    for (byte b = 0; b < 2; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.setAutoMirrored(paramBoolean); 
    } 
  }
  
  public boolean isAutoMirrored() {
    return this.mLayerState.mAutoMirrored;
  }
  
  public void jumpToCurrentState() {
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    for (byte b = 0; b < 2; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.jumpToCurrentState(); 
    } 
  }
  
  public boolean isStateful() {
    return this.mLayerState.isStateful();
  }
  
  public boolean hasFocusStateSpecified() {
    return this.mLayerState.hasFocusStateSpecified();
  }
  
  protected boolean onStateChange(int[] paramArrayOfint) {
    boolean bool = false;
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    for (byte b = 0; b < 2; b++, bool = bool1) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      boolean bool1 = bool;
      if (drawable != null) {
        bool1 = bool;
        if (drawable.isStateful()) {
          bool1 = bool;
          if (drawable.setState(paramArrayOfint))
            bool1 = true; 
        } 
      } 
    } 
    if (bool)
      updateLayerBounds(getBounds()); 
    return bool;
  }
  
  protected boolean onLevelChange(int paramInt) {
    boolean bool = false;
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    for (byte b = 0; b < 2; b++, bool = bool1) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      boolean bool1 = bool;
      if (drawable != null) {
        bool1 = bool;
        if (drawable.setLevel(paramInt))
          bool1 = true; 
      } 
    } 
    if (bool)
      updateLayerBounds(getBounds()); 
    return bool;
  }
  
  public int getIntrinsicWidth() {
    if (hookGetIntrinsicWidth())
      return getMaxIntrinsicWidth(); 
    return (int)(getMaxIntrinsicWidth() * 0.6666667F);
  }
  
  private int getMaxIntrinsicWidth() {
    int i = -1;
    byte b = 0;
    while (true) {
      LayerState layerState = this.mLayerState;
      if (b < 2) {
        int j;
        ChildDrawable childDrawable = layerState.mChildren[b];
        if (childDrawable.mDrawable == null) {
          j = i;
        } else {
          int k = childDrawable.mDrawable.getIntrinsicWidth();
          j = i;
          if (k > i)
            j = k; 
        } 
        b++;
        i = j;
        continue;
      } 
      break;
    } 
    return i;
  }
  
  public int getIntrinsicHeight() {
    if (hookGetIntrinsicHeight())
      return getMaxIntrinsicHeight(); 
    return (int)(getMaxIntrinsicHeight() * 0.6666667F);
  }
  
  private int getMaxIntrinsicHeight() {
    int i = -1;
    byte b = 0;
    while (true) {
      LayerState layerState = this.mLayerState;
      if (b < 2) {
        int j;
        ChildDrawable childDrawable = layerState.mChildren[b];
        if (childDrawable.mDrawable == null) {
          j = i;
        } else {
          int k = childDrawable.mDrawable.getIntrinsicHeight();
          j = i;
          if (k > i)
            j = k; 
        } 
        b++;
        i = j;
        continue;
      } 
      break;
    } 
    return i;
  }
  
  public Drawable.ConstantState getConstantState() {
    if (this.mLayerState.canConstantState()) {
      this.mLayerState.mChangingConfigurations = getChangingConfigurations();
      return this.mLayerState;
    } 
    return null;
  }
  
  public Drawable mutate() {
    if (!this.mMutated && super.mutate() == this) {
      this.mLayerState = createConstantState(this.mLayerState, (Resources)null);
      byte b = 0;
      while (true) {
        LayerState layerState = this.mLayerState;
        if (b < 2) {
          Drawable drawable = (layerState.mChildren[b]).mDrawable;
          if (drawable != null)
            drawable.mutate(); 
          b++;
          continue;
        } 
        break;
      } 
      this.mMutated = true;
    } 
    return this;
  }
  
  public void clearMutated() {
    super.clearMutated();
    ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    for (byte b = 0; b < 2; b++) {
      Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
      if (drawable != null)
        drawable.clearMutated(); 
    } 
    this.mMutated = false;
  }
  
  class ChildDrawable {
    public int mDensity;
    
    public Drawable mDrawable;
    
    public int[] mThemeAttrs;
    
    ChildDrawable(AdaptiveIconDrawable this$0) {
      this.mDensity = 160;
      this.mDensity = this$0;
    }
    
    ChildDrawable(AdaptiveIconDrawable this$0, AdaptiveIconDrawable param1AdaptiveIconDrawable, Resources param1Resources) {
      Drawable drawable2;
      this.mDensity = 160;
      Drawable drawable1 = ((ChildDrawable)this$0).mDrawable;
      if (drawable1 != null) {
        Drawable.ConstantState constantState = drawable1.getConstantState();
        if (constantState == null) {
          drawable2 = drawable1;
        } else if (param1Resources != null) {
          drawable2 = drawable2.newDrawable(param1Resources);
        } else {
          drawable2 = drawable2.newDrawable();
        } 
        drawable2.setCallback(param1AdaptiveIconDrawable);
        drawable2.setBounds(drawable1.getBounds());
        drawable2.setLevel(drawable1.getLevel());
      } else {
        drawable2 = null;
      } 
      this.mDrawable = drawable2;
      this.mThemeAttrs = ((ChildDrawable)this$0).mThemeAttrs;
      this.mDensity = Drawable.resolveDensity(param1Resources, ((ChildDrawable)this$0).mDensity);
    }
    
    public boolean canApplyTheme() {
      if (this.mThemeAttrs == null) {
        Drawable drawable = this.mDrawable;
        return (drawable != null && 
          drawable.canApplyTheme());
      } 
      return true;
    }
    
    public final void setDensity(int param1Int) {
      if (this.mDensity != param1Int)
        this.mDensity = param1Int; 
    }
  }
  
  class LayerState extends Drawable.ConstantState {
    static final int N_CHILDREN = 2;
    
    private boolean mAutoMirrored;
    
    int mChangingConfigurations;
    
    private boolean mCheckedOpacity;
    
    private boolean mCheckedStateful;
    
    AdaptiveIconDrawable.ChildDrawable[] mChildren;
    
    int mChildrenChangingConfigurations;
    
    int mDensity;
    
    private boolean mIsStateful;
    
    private int mOpacity;
    
    int mOpacityOverride;
    
    int mSourceDrawableId;
    
    int mSrcDensityOverride;
    
    private int[] mThemeAttrs;
    
    LayerState(AdaptiveIconDrawable this$0, AdaptiveIconDrawable param1AdaptiveIconDrawable, Resources param1Resources) {
      int i = 0;
      this.mSrcDensityOverride = 0;
      this.mOpacityOverride = 0;
      this.mSourceDrawableId = 0;
      this.mAutoMirrored = false;
      if (this$0 != null)
        i = ((LayerState)this$0).mDensity; 
      this.mDensity = Drawable.resolveDensity(param1Resources, i);
      this.mChildren = new AdaptiveIconDrawable.ChildDrawable[2];
      if (this$0 != null) {
        AdaptiveIconDrawable.ChildDrawable[] arrayOfChildDrawable = ((LayerState)this$0).mChildren;
        this.mChangingConfigurations = ((LayerState)this$0).mChangingConfigurations;
        this.mChildrenChangingConfigurations = ((LayerState)this$0).mChildrenChangingConfigurations;
        this.mSourceDrawableId = ((LayerState)this$0).mSourceDrawableId;
        for (i = 0; i < 2; i++) {
          AdaptiveIconDrawable.ChildDrawable childDrawable = arrayOfChildDrawable[i];
          this.mChildren[i] = new AdaptiveIconDrawable.ChildDrawable(childDrawable, param1AdaptiveIconDrawable, param1Resources);
        } 
        this.mCheckedOpacity = ((LayerState)this$0).mCheckedOpacity;
        this.mOpacity = ((LayerState)this$0).mOpacity;
        this.mCheckedStateful = ((LayerState)this$0).mCheckedStateful;
        this.mIsStateful = ((LayerState)this$0).mIsStateful;
        this.mAutoMirrored = ((LayerState)this$0).mAutoMirrored;
        this.mThemeAttrs = ((LayerState)this$0).mThemeAttrs;
        this.mOpacityOverride = ((LayerState)this$0).mOpacityOverride;
        this.mSrcDensityOverride = ((LayerState)this$0).mSrcDensityOverride;
      } else {
        for (i = 0; i < 2; i++)
          this.mChildren[i] = new AdaptiveIconDrawable.ChildDrawable(this.mDensity); 
      } 
    }
    
    public final void setDensity(int param1Int) {
      if (this.mDensity != param1Int)
        this.mDensity = param1Int; 
    }
    
    public boolean canApplyTheme() {
      if (this.mThemeAttrs != null || super.canApplyTheme())
        return true; 
      AdaptiveIconDrawable.ChildDrawable[] arrayOfChildDrawable = this.mChildren;
      for (byte b = 0; b < 2; b++) {
        AdaptiveIconDrawable.ChildDrawable childDrawable = arrayOfChildDrawable[b];
        if (childDrawable.canApplyTheme())
          return true; 
      } 
      return false;
    }
    
    public Drawable newDrawable() {
      return new AdaptiveIconDrawable(this, null);
    }
    
    public Drawable newDrawable(Resources param1Resources) {
      return new AdaptiveIconDrawable(this, param1Resources);
    }
    
    public int getChangingConfigurations() {
      return this.mChangingConfigurations | this.mChildrenChangingConfigurations;
    }
    
    public final int getOpacity() {
      int k;
      if (this.mCheckedOpacity)
        return this.mOpacity; 
      AdaptiveIconDrawable.ChildDrawable[] arrayOfChildDrawable = this.mChildren;
      int i = -1;
      int j = 0;
      while (true) {
        k = i;
        if (j < 2) {
          if ((arrayOfChildDrawable[j]).mDrawable != null) {
            k = j;
            break;
          } 
          j++;
          continue;
        } 
        break;
      } 
      if (k >= 0) {
        j = (arrayOfChildDrawable[k]).mDrawable.getOpacity();
      } else {
        j = -2;
      } 
      for (; ++k < 2; k++, j = i) {
        Drawable drawable = (arrayOfChildDrawable[k]).mDrawable;
        i = j;
        if (drawable != null)
          i = Drawable.resolveOpacity(j, drawable.getOpacity()); 
      } 
      this.mOpacity = j;
      this.mCheckedOpacity = true;
      return j;
    }
    
    public final boolean isStateful() {
      boolean bool2;
      if (this.mCheckedStateful)
        return this.mIsStateful; 
      AdaptiveIconDrawable.ChildDrawable[] arrayOfChildDrawable = this.mChildren;
      boolean bool1 = false;
      byte b = 0;
      while (true) {
        bool2 = bool1;
        if (b < 2) {
          Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
          if (drawable != null && drawable.isStateful()) {
            bool2 = true;
            break;
          } 
          b++;
          continue;
        } 
        break;
      } 
      this.mIsStateful = bool2;
      this.mCheckedStateful = true;
      return bool2;
    }
    
    public final boolean hasFocusStateSpecified() {
      AdaptiveIconDrawable.ChildDrawable[] arrayOfChildDrawable = this.mChildren;
      for (byte b = 0; b < 2; b++) {
        Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
        if (drawable != null && drawable.hasFocusStateSpecified())
          return true; 
      } 
      return false;
    }
    
    public final boolean canConstantState() {
      AdaptiveIconDrawable.ChildDrawable[] arrayOfChildDrawable = this.mChildren;
      for (byte b = 0; b < 2; b++) {
        Drawable drawable = (arrayOfChildDrawable[b]).mDrawable;
        if (drawable != null && drawable.getConstantState() == null)
          return false; 
      } 
      return true;
    }
    
    public void invalidateCache() {
      this.mCheckedOpacity = false;
      this.mCheckedStateful = false;
    }
  }
}
