package android.graphics.drawable;

import android.content.res.OplusBaseResources;
import android.text.TextUtils;
import oplus.content.res.OplusExtraConfiguration;

public abstract class OplusBaseVObject {
  int mShouldRestoreFillColor = -1;
  
  int mShouldRestoreStrokeColor = -1;
  
  boolean mHasOriginColor = false;
  
  private static final float CHANGE_UNIT = 0.5F;
  
  private static final int MODE_FLAG = 16711680;
  
  private static final String WIDTH_SYMBOL = "path_width";
  
  private OplusBaseResources mBaseResources;
  
  public void hookVFullInflate(OplusBaseResources paramOplusBaseResources) {
    this.mBaseResources = paramOplusBaseResources;
  }
  
  public Float calculateStrokeWidth(String paramString, float paramFloat) {
    if (!TextUtils.isEmpty(paramString) && 
      paramString.startsWith("path_width")) {
      OplusBaseResources oplusBaseResources = this.mBaseResources;
      if (oplusBaseResources != null) {
        OplusExtraConfiguration oplusExtraConfiguration = oplusBaseResources.getConfiguration().getOplusExtraConfiguration();
        if (oplusExtraConfiguration == null)
          return Float.valueOf(paramFloat); 
        int i = oplusExtraConfiguration.mFontVariationSettings;
        float f = ((0xFF0000 & i) >> 16) * 0.5F;
        if (f > 0.0F)
          paramFloat = f; 
        return Float.valueOf(paramFloat);
      } 
    } 
    return Float.valueOf(paramFloat);
  }
  
  void setFillColor(int paramInt) {}
  
  void setStrokeColor(int paramInt) {}
  
  int getFillColor() {
    return 0;
  }
  
  int getStrokeColor() {
    return 0;
  }
  
  float getStrokeAlpha() {
    return 0.0F;
  }
  
  float getFillAlpha() {
    return 0.0F;
  }
}
