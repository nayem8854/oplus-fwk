package android.graphics.drawable;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.os.SystemClock;

public class TransitionDrawable extends LayerDrawable implements Drawable.Callback {
  private int mTransitionState = 2;
  
  private int mTo;
  
  private long mStartTimeMillis;
  
  private boolean mReverse;
  
  private int mOriginalDuration;
  
  private int mFrom;
  
  private int mDuration;
  
  private boolean mCrossFade;
  
  private int mAlpha = 0;
  
  private static final int TRANSITION_STARTING = 0;
  
  private static final int TRANSITION_RUNNING = 1;
  
  private static final int TRANSITION_NONE = 2;
  
  public TransitionDrawable(Drawable[] paramArrayOfDrawable) {
    this(new TransitionState(null, null, null), paramArrayOfDrawable);
  }
  
  TransitionDrawable() {
    this(new TransitionState(null, null, null), (Resources)null);
  }
  
  private TransitionDrawable(TransitionState paramTransitionState, Resources paramResources) {
    super(paramTransitionState, paramResources);
  }
  
  private TransitionDrawable(TransitionState paramTransitionState, Drawable[] paramArrayOfDrawable) {
    super(paramArrayOfDrawable, paramTransitionState);
  }
  
  LayerDrawable.LayerState createConstantState(LayerDrawable.LayerState paramLayerState, Resources paramResources) {
    return new TransitionState((TransitionState)paramLayerState, this, paramResources);
  }
  
  public void startTransition(int paramInt) {
    this.mFrom = 0;
    this.mTo = 255;
    this.mAlpha = 0;
    this.mOriginalDuration = paramInt;
    this.mDuration = paramInt;
    this.mReverse = false;
    this.mTransitionState = 0;
    invalidateSelf();
  }
  
  public void showSecondLayer() {
    this.mAlpha = 255;
    this.mReverse = false;
    this.mTransitionState = 2;
    invalidateSelf();
  }
  
  public void resetTransition() {
    this.mAlpha = 0;
    this.mTransitionState = 2;
    invalidateSelf();
  }
  
  public void reverseTransition(int paramInt) {
    long l1 = SystemClock.uptimeMillis();
    long l2 = this.mStartTimeMillis, l3 = this.mDuration;
    char c = 'ÿ';
    if (l1 - l2 > l3) {
      if (this.mTo == 0) {
        this.mFrom = 0;
        this.mTo = 255;
        this.mAlpha = 0;
        this.mReverse = false;
      } else {
        this.mFrom = 255;
        this.mTo = 0;
        this.mAlpha = 255;
        this.mReverse = true;
      } 
      this.mOriginalDuration = paramInt;
      this.mDuration = paramInt;
      this.mTransitionState = 0;
      invalidateSelf();
      return;
    } 
    int i = this.mReverse ^ true;
    this.mFrom = this.mAlpha;
    paramInt = c;
    if (i != 0)
      paramInt = 0; 
    this.mTo = paramInt;
    if (this.mReverse) {
      l1 -= this.mStartTimeMillis;
    } else {
      l1 = this.mOriginalDuration - l1 - this.mStartTimeMillis;
    } 
    this.mDuration = (int)l1;
    this.mTransitionState = 0;
  }
  
  public void draw(Canvas paramCanvas) {
    boolean bool = true;
    int i = this.mTransitionState;
    if (i != 0) {
      if (i == 1)
        if (this.mStartTimeMillis >= 0L) {
          float f = (float)(SystemClock.uptimeMillis() - this.mStartTimeMillis) / this.mDuration;
          if (f >= 1.0F) {
            bool = true;
          } else {
            bool = false;
          } 
          f = Math.min(f, 1.0F);
          i = this.mFrom;
          this.mAlpha = (int)(i + (this.mTo - i) * f);
        }  
    } else {
      this.mStartTimeMillis = SystemClock.uptimeMillis();
      bool = false;
      this.mTransitionState = 1;
    } 
    i = this.mAlpha;
    boolean bool1 = this.mCrossFade;
    LayerDrawable.ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
    if (bool) {
      if (!bool1 || i == 0)
        (arrayOfChildDrawable[0]).mDrawable.draw(paramCanvas); 
      if (i == 255)
        (arrayOfChildDrawable[1]).mDrawable.draw(paramCanvas); 
      return;
    } 
    Drawable drawable = (arrayOfChildDrawable[0]).mDrawable;
    if (bool1)
      drawable.setAlpha(255 - i); 
    drawable.draw(paramCanvas);
    if (bool1)
      drawable.setAlpha(255); 
    if (i > 0) {
      drawable = (arrayOfChildDrawable[1]).mDrawable;
      drawable.setAlpha(i);
      drawable.draw(paramCanvas);
      drawable.setAlpha(255);
    } 
    if (!bool)
      invalidateSelf(); 
  }
  
  public void setCrossFadeEnabled(boolean paramBoolean) {
    this.mCrossFade = paramBoolean;
  }
  
  public boolean isCrossFadeEnabled() {
    return this.mCrossFade;
  }
  
  class TransitionState extends LayerDrawable.LayerState {
    TransitionState(TransitionDrawable this$0, TransitionDrawable param1TransitionDrawable, Resources param1Resources) {
      super((LayerDrawable.LayerState)this$0, param1TransitionDrawable, param1Resources);
    }
    
    public Drawable newDrawable() {
      return new TransitionDrawable(this, (Resources)null);
    }
    
    public Drawable newDrawable(Resources param1Resources) {
      return new TransitionDrawable(this, param1Resources);
    }
    
    public int getChangingConfigurations() {
      return this.mChangingConfigurations;
    }
  }
}
