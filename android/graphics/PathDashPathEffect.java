package android.graphics;

public class PathDashPathEffect extends PathEffect {
  class Style extends Enum<Style> {
    private static final Style[] $VALUES;
    
    public static final Style MORPH;
    
    public static final Style ROTATE = new Style("ROTATE", 1, 1);
    
    public static Style valueOf(String param1String) {
      return Enum.<Style>valueOf(Style.class, param1String);
    }
    
    public static Style[] values() {
      return (Style[])$VALUES.clone();
    }
    
    public static final Style TRANSLATE = new Style("TRANSLATE", 0, 0);
    
    int native_style;
    
    static {
      Style style = new Style("MORPH", 2, 2);
      $VALUES = new Style[] { TRANSLATE, ROTATE, style };
    }
    
    private Style(PathDashPathEffect this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.native_style = param1Int2;
    }
  }
  
  public PathDashPathEffect(Path paramPath, float paramFloat1, float paramFloat2, Style paramStyle) {
    this.native_instance = nativeCreate(paramPath.readOnlyNI(), paramFloat1, paramFloat2, paramStyle.native_style);
  }
  
  private static native long nativeCreate(long paramLong, float paramFloat1, float paramFloat2, int paramInt);
}
