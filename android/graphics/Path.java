package android.graphics;

import dalvik.annotation.optimization.CriticalNative;
import dalvik.annotation.optimization.FastNative;
import libcore.util.NativeAllocationRegistry;

public class Path extends OplusBasePath {
  static final FillType[] sFillTypeArray;
  
  private static final NativeAllocationRegistry sRegistry;
  
  static {
    ClassLoader classLoader = Path.class.getClassLoader();
    long l = nGetFinalizer();
    sRegistry = NativeAllocationRegistry.createMalloced(classLoader, l);
    sFillTypeArray = new FillType[] { FillType.WINDING, FillType.EVEN_ODD, FillType.INVERSE_WINDING, FillType.INVERSE_EVEN_ODD };
  }
  
  public boolean isSimplePath = true;
  
  private Direction mLastDirection = null;
  
  public final long mNativePath;
  
  public Region rects;
  
  public Path() {
    long l = nInit();
    sRegistry.registerNativeAllocation(this, l);
  }
  
  public Path(Path paramPath) {
    long l = 0L;
    if (paramPath != null) {
      l = paramPath.mNativePath;
      this.isSimplePath = paramPath.isSimplePath;
      if (paramPath.rects != null)
        this.rects = new Region(paramPath.rects); 
      setIsAddRect(paramPath);
    } 
    this.mNativePath = l = nInit(l);
    sRegistry.registerNativeAllocation(this, l);
  }
  
  public void reset() {
    this.isSimplePath = true;
    this.mLastDirection = null;
    Region region = this.rects;
    if (region != null)
      region.setEmpty(); 
    FillType fillType = getFillType();
    nReset(this.mNativePath);
    setFillType(fillType);
  }
  
  public void rewind() {
    this.isSimplePath = true;
    this.mLastDirection = null;
    Region region = this.rects;
    if (region != null)
      region.setEmpty(); 
    nRewind(this.mNativePath);
  }
  
  public void set(Path paramPath) {
    if (this == paramPath)
      return; 
    this.isSimplePath = paramPath.isSimplePath;
    nSet(this.mNativePath, paramPath.mNativePath);
    if (!this.isSimplePath)
      return; 
    Region region1 = this.rects;
    if (region1 != null) {
      Region region = paramPath.rects;
      if (region != null) {
        region1.set(region);
        return;
      } 
    } 
    Region region2 = this.rects;
    if (region2 != null && paramPath.rects == null) {
      region2.setEmpty();
    } else if (paramPath.rects != null) {
      this.rects = new Region(paramPath.rects);
    } 
  }
  
  class Op extends Enum<Op> {
    private static final Op[] $VALUES;
    
    public static Op[] values() {
      return (Op[])$VALUES.clone();
    }
    
    public static Op valueOf(String param1String) {
      return Enum.<Op>valueOf(Op.class, param1String);
    }
    
    private Op(Path this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static final Op DIFFERENCE = new Op("DIFFERENCE", 0);
    
    public static final Op INTERSECT = new Op("INTERSECT", 1);
    
    public static final Op REVERSE_DIFFERENCE;
    
    public static final Op UNION = new Op("UNION", 2);
    
    public static final Op XOR = new Op("XOR", 3);
    
    static {
      Op op = new Op("REVERSE_DIFFERENCE", 4);
      $VALUES = new Op[] { DIFFERENCE, INTERSECT, UNION, XOR, op };
    }
  }
  
  public boolean op(Path paramPath, Op paramOp) {
    return op(this, paramPath, paramOp);
  }
  
  public boolean op(Path paramPath1, Path paramPath2, Op paramOp) {
    if (nOp(paramPath1.mNativePath, paramPath2.mNativePath, paramOp.ordinal(), this.mNativePath)) {
      this.isSimplePath = false;
      this.rects = null;
      return true;
    } 
    return false;
  }
  
  @Deprecated
  public boolean isConvex() {
    return nIsConvex(this.mNativePath);
  }
  
  class FillType extends Enum<FillType> {
    private static final FillType[] $VALUES;
    
    public static final FillType EVEN_ODD = new FillType("EVEN_ODD", 1, 1);
    
    public static final FillType INVERSE_EVEN_ODD;
    
    public static final FillType INVERSE_WINDING = new FillType("INVERSE_WINDING", 2, 2);
    
    public static FillType valueOf(String param1String) {
      return Enum.<FillType>valueOf(FillType.class, param1String);
    }
    
    public static FillType[] values() {
      return (FillType[])$VALUES.clone();
    }
    
    public static final FillType WINDING = new FillType("WINDING", 0, 0);
    
    final int nativeInt;
    
    static {
      FillType fillType = new FillType("INVERSE_EVEN_ODD", 3, 3);
      $VALUES = new FillType[] { WINDING, EVEN_ODD, INVERSE_WINDING, fillType };
    }
    
    private FillType(Path this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.nativeInt = param1Int2;
    }
  }
  
  public FillType getFillType() {
    return sFillTypeArray[nGetFillType(this.mNativePath)];
  }
  
  public void setFillType(FillType paramFillType) {
    nSetFillType(this.mNativePath, paramFillType.nativeInt);
  }
  
  public boolean isInverseFillType() {
    boolean bool;
    int i = nGetFillType(this.mNativePath);
    if ((FillType.INVERSE_WINDING.nativeInt & i) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void toggleInverseFillType() {
    int i = nGetFillType(this.mNativePath);
    int j = FillType.INVERSE_WINDING.nativeInt;
    nSetFillType(this.mNativePath, i ^ j);
  }
  
  public boolean isEmpty() {
    return nIsEmpty(this.mNativePath);
  }
  
  public boolean isRect(RectF paramRectF) {
    return nIsRect(this.mNativePath, paramRectF);
  }
  
  public void computeBounds(RectF paramRectF, boolean paramBoolean) {
    nComputeBounds(this.mNativePath, paramRectF);
  }
  
  public void incReserve(int paramInt) {
    nIncReserve(this.mNativePath, paramInt);
  }
  
  public void moveTo(float paramFloat1, float paramFloat2) {
    nMoveTo(this.mNativePath, paramFloat1, paramFloat2);
  }
  
  public void rMoveTo(float paramFloat1, float paramFloat2) {
    nRMoveTo(this.mNativePath, paramFloat1, paramFloat2);
  }
  
  public void lineTo(float paramFloat1, float paramFloat2) {
    this.isSimplePath = false;
    nLineTo(this.mNativePath, paramFloat1, paramFloat2);
  }
  
  public void rLineTo(float paramFloat1, float paramFloat2) {
    this.isSimplePath = false;
    nRLineTo(this.mNativePath, paramFloat1, paramFloat2);
  }
  
  public void quadTo(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    this.isSimplePath = false;
    nQuadTo(this.mNativePath, paramFloat1, paramFloat2, paramFloat3, paramFloat4);
  }
  
  public void rQuadTo(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    this.isSimplePath = false;
    nRQuadTo(this.mNativePath, paramFloat1, paramFloat2, paramFloat3, paramFloat4);
  }
  
  public void cubicTo(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6) {
    this.isSimplePath = false;
    nCubicTo(this.mNativePath, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6);
  }
  
  public void rCubicTo(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6) {
    this.isSimplePath = false;
    nRCubicTo(this.mNativePath, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6);
  }
  
  public void arcTo(RectF paramRectF, float paramFloat1, float paramFloat2, boolean paramBoolean) {
    arcTo(paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramFloat1, paramFloat2, paramBoolean);
  }
  
  public void arcTo(RectF paramRectF, float paramFloat1, float paramFloat2) {
    arcTo(paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramFloat1, paramFloat2, false);
  }
  
  public void arcTo(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, boolean paramBoolean) {
    this.isSimplePath = false;
    nArcTo(this.mNativePath, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6, paramBoolean);
  }
  
  public void close() {
    this.isSimplePath = false;
    nClose(this.mNativePath);
  }
  
  class Direction extends Enum<Direction> {
    private static final Direction[] $VALUES;
    
    public static final Direction CCW;
    
    public static Direction valueOf(String param1String) {
      return Enum.<Direction>valueOf(Direction.class, param1String);
    }
    
    public static Direction[] values() {
      return (Direction[])$VALUES.clone();
    }
    
    public static final Direction CW = new Direction("CW", 0, 0);
    
    final int nativeInt;
    
    static {
      Direction direction = new Direction("CCW", 1, 1);
      $VALUES = new Direction[] { CW, direction };
    }
    
    private Direction(Path this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.nativeInt = param1Int2;
    }
  }
  
  private void detectSimplePath(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Direction paramDirection) {
    if (this.mLastDirection == null)
      this.mLastDirection = paramDirection; 
    if (this.mLastDirection != paramDirection) {
      this.isSimplePath = false;
    } else {
      if (this.rects == null)
        this.rects = new Region(); 
      this.rects.op((int)paramFloat1, (int)paramFloat2, (int)paramFloat3, (int)paramFloat4, Region.Op.UNION);
    } 
  }
  
  public void addRect(RectF paramRectF, Direction paramDirection) {
    addRect(paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramDirection);
  }
  
  public void addRect(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Direction paramDirection) {
    detectSimplePath(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramDirection);
    setIsAddRect(true);
    nAddRect(this.mNativePath, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramDirection.nativeInt);
  }
  
  public void addOval(RectF paramRectF, Direction paramDirection) {
    addOval(paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramDirection);
  }
  
  public void addOval(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Direction paramDirection) {
    this.isSimplePath = false;
    nAddOval(this.mNativePath, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramDirection.nativeInt);
  }
  
  public void addCircle(float paramFloat1, float paramFloat2, float paramFloat3, Direction paramDirection) {
    this.isSimplePath = false;
    nAddCircle(this.mNativePath, paramFloat1, paramFloat2, paramFloat3, paramDirection.nativeInt);
  }
  
  public void addArc(RectF paramRectF, float paramFloat1, float paramFloat2) {
    addArc(paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramFloat1, paramFloat2);
  }
  
  public void addArc(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6) {
    this.isSimplePath = false;
    nAddArc(this.mNativePath, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6);
  }
  
  public void addRoundRect(RectF paramRectF, float paramFloat1, float paramFloat2, Direction paramDirection) {
    addRoundRect(paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramFloat1, paramFloat2, paramDirection);
  }
  
  public void addRoundRect(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, Direction paramDirection) {
    this.isSimplePath = false;
    setIsAddRect(true);
    nAddRoundRect(this.mNativePath, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6, paramDirection.nativeInt);
  }
  
  public void addRoundRect(RectF paramRectF, float[] paramArrayOffloat, Direction paramDirection) {
    if (paramRectF != null) {
      addRoundRect(paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramArrayOffloat, paramDirection);
      return;
    } 
    throw new NullPointerException("need rect parameter");
  }
  
  public void addRoundRect(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float[] paramArrayOffloat, Direction paramDirection) {
    if (paramArrayOffloat.length >= 8) {
      this.isSimplePath = false;
      setIsAddRect(true);
      nAddRoundRect(this.mNativePath, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramArrayOffloat, paramDirection.nativeInt);
      return;
    } 
    throw new ArrayIndexOutOfBoundsException("radii[] needs 8 values");
  }
  
  public void addPath(Path paramPath, float paramFloat1, float paramFloat2) {
    this.isSimplePath = false;
    nAddPath(this.mNativePath, paramPath.mNativePath, paramFloat1, paramFloat2);
  }
  
  public void addPath(Path paramPath) {
    this.isSimplePath = false;
    nAddPath(this.mNativePath, paramPath.mNativePath);
  }
  
  public void addPath(Path paramPath, Matrix paramMatrix) {
    if (!paramPath.isSimplePath)
      this.isSimplePath = false; 
    nAddPath(this.mNativePath, paramPath.mNativePath, paramMatrix.native_instance);
  }
  
  public void offset(float paramFloat1, float paramFloat2, Path paramPath) {
    if (paramPath != null) {
      paramPath.set(this);
    } else {
      paramPath = this;
    } 
    paramPath.offset(paramFloat1, paramFloat2);
  }
  
  public void offset(float paramFloat1, float paramFloat2) {
    if (this.isSimplePath && this.rects == null)
      return; 
    if (this.isSimplePath && paramFloat1 == Math.rint(paramFloat1) && paramFloat2 == Math.rint(paramFloat2)) {
      this.rects.translate((int)paramFloat1, (int)paramFloat2);
    } else {
      this.isSimplePath = false;
    } 
    nOffset(this.mNativePath, paramFloat1, paramFloat2);
  }
  
  public void setLastPoint(float paramFloat1, float paramFloat2) {
    this.isSimplePath = false;
    nSetLastPoint(this.mNativePath, paramFloat1, paramFloat2);
  }
  
  public void transform(Matrix paramMatrix, Path paramPath) {
    long l = 0L;
    if (paramPath != null) {
      paramPath.isSimplePath = false;
      l = paramPath.mNativePath;
    } 
    nTransform(this.mNativePath, paramMatrix.native_instance, l);
  }
  
  public void transform(Matrix paramMatrix) {
    this.isSimplePath = false;
    nTransform(this.mNativePath, paramMatrix.native_instance);
  }
  
  public final long readOnlyNI() {
    return this.mNativePath;
  }
  
  final long mutateNI() {
    this.isSimplePath = false;
    return this.mNativePath;
  }
  
  public float[] approximate(float paramFloat) {
    if (paramFloat >= 0.0F)
      return nApproximate(this.mNativePath, paramFloat); 
    throw new IllegalArgumentException("AcceptableError must be greater than or equal to 0");
  }
  
  private static native void nAddArc(long paramLong, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6);
  
  private static native void nAddCircle(long paramLong, float paramFloat1, float paramFloat2, float paramFloat3, int paramInt);
  
  private static native void nAddOval(long paramLong, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt);
  
  private static native void nAddPath(long paramLong1, long paramLong2);
  
  private static native void nAddPath(long paramLong1, long paramLong2, float paramFloat1, float paramFloat2);
  
  private static native void nAddPath(long paramLong1, long paramLong2, long paramLong3);
  
  private static native void nAddRect(long paramLong, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt);
  
  private static native void nAddRoundRect(long paramLong, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, int paramInt);
  
  private static native void nAddRoundRect(long paramLong, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float[] paramArrayOffloat, int paramInt);
  
  private static native float[] nApproximate(long paramLong, float paramFloat);
  
  private static native void nArcTo(long paramLong, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, boolean paramBoolean);
  
  private static native void nClose(long paramLong);
  
  private static native void nComputeBounds(long paramLong, RectF paramRectF);
  
  private static native void nCubicTo(long paramLong, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6);
  
  @CriticalNative
  private static native int nGetFillType(long paramLong);
  
  private static native long nGetFinalizer();
  
  private static native void nIncReserve(long paramLong, int paramInt);
  
  private static native long nInit();
  
  private static native long nInit(long paramLong);
  
  @CriticalNative
  private static native boolean nIsConvex(long paramLong);
  
  @CriticalNative
  private static native boolean nIsEmpty(long paramLong);
  
  @FastNative
  private static native boolean nIsRect(long paramLong, RectF paramRectF);
  
  private static native void nLineTo(long paramLong, float paramFloat1, float paramFloat2);
  
  private static native void nMoveTo(long paramLong, float paramFloat1, float paramFloat2);
  
  private static native void nOffset(long paramLong, float paramFloat1, float paramFloat2);
  
  private static native boolean nOp(long paramLong1, long paramLong2, int paramInt, long paramLong3);
  
  private static native void nQuadTo(long paramLong, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4);
  
  private static native void nRCubicTo(long paramLong, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6);
  
  private static native void nRLineTo(long paramLong, float paramFloat1, float paramFloat2);
  
  private static native void nRMoveTo(long paramLong, float paramFloat1, float paramFloat2);
  
  private static native void nRQuadTo(long paramLong, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4);
  
  @CriticalNative
  private static native void nReset(long paramLong);
  
  @CriticalNative
  private static native void nRewind(long paramLong);
  
  private static native void nSet(long paramLong1, long paramLong2);
  
  @CriticalNative
  private static native void nSetFillType(long paramLong, int paramInt);
  
  private static native void nSetLastPoint(long paramLong, float paramFloat1, float paramFloat2);
  
  private static native void nTransform(long paramLong1, long paramLong2);
  
  private static native void nTransform(long paramLong1, long paramLong2, long paramLong3);
}
