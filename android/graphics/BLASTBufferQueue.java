package android.graphics;

import android.view.Surface;
import android.view.SurfaceControl;

public final class BLASTBufferQueue {
  private long mNativeObject;
  
  public BLASTBufferQueue(SurfaceControl paramSurfaceControl, int paramInt1, int paramInt2, boolean paramBoolean) {
    this.mNativeObject = nativeCreate(paramSurfaceControl.mNativeObject, paramInt1, paramInt2, paramBoolean);
  }
  
  public void destroy() {
    nativeDestroy(this.mNativeObject);
  }
  
  public Surface getSurface() {
    return nativeGetSurface(this.mNativeObject);
  }
  
  public void setNextTransaction(SurfaceControl.Transaction paramTransaction) {
    nativeSetNextTransaction(this.mNativeObject, paramTransaction.mNativeObject);
  }
  
  public void update(SurfaceControl paramSurfaceControl, int paramInt1, int paramInt2) {
    nativeUpdate(this.mNativeObject, paramSurfaceControl.mNativeObject, paramInt1, paramInt2);
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mNativeObject != 0L)
        nativeDestroy(this.mNativeObject); 
      return;
    } finally {
      super.finalize();
    } 
  }
  
  private static native long nativeCreate(long paramLong1, long paramLong2, long paramLong3, boolean paramBoolean);
  
  private static native void nativeDestroy(long paramLong);
  
  private static native Surface nativeGetSurface(long paramLong);
  
  private static native void nativeSetNextTransaction(long paramLong1, long paramLong2);
  
  private static native void nativeUpdate(long paramLong1, long paramLong2, long paramLong3, long paramLong4);
}
