package android.graphics;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class BitmapRegionDecoder {
  private long mNativeBitmapRegionDecoder;
  
  private final Object mNativeLock = new Object();
  
  private boolean mRecycled;
  
  public static BitmapRegionDecoder newInstance(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, boolean paramBoolean) throws IOException {
    if ((paramInt1 | paramInt2) >= 0 && paramArrayOfbyte.length >= paramInt1 + paramInt2)
      return nativeNewInstance(paramArrayOfbyte, paramInt1, paramInt2, paramBoolean); 
    throw new ArrayIndexOutOfBoundsException();
  }
  
  public static BitmapRegionDecoder newInstance(FileDescriptor paramFileDescriptor, boolean paramBoolean) throws IOException {
    return nativeNewInstance(paramFileDescriptor, paramBoolean);
  }
  
  public static BitmapRegionDecoder newInstance(InputStream paramInputStream, boolean paramBoolean) throws IOException {
    if (paramInputStream instanceof android.content.res.AssetManager.AssetInputStream) {
      paramInputStream = paramInputStream;
      long l = paramInputStream.getNativeAsset();
      return nativeNewInstance(l, paramBoolean);
    } 
    byte[] arrayOfByte = new byte[16384];
    return nativeNewInstance(paramInputStream, arrayOfByte, paramBoolean);
  }
  
  public static BitmapRegionDecoder newInstance(String paramString, boolean paramBoolean) throws IOException {
    FileInputStream fileInputStream1 = null;
    FileInputStream fileInputStream2 = fileInputStream1;
    try {
      FileInputStream fileInputStream4 = new FileInputStream();
      fileInputStream2 = fileInputStream1;
      this(paramString);
      FileInputStream fileInputStream3 = fileInputStream4;
      fileInputStream2 = fileInputStream3;
      return newInstance(fileInputStream3, paramBoolean);
    } finally {
      if (fileInputStream2 != null)
        try {
          fileInputStream2.close();
        } catch (IOException iOException) {} 
    } 
  }
  
  private BitmapRegionDecoder(long paramLong) {
    this.mNativeBitmapRegionDecoder = paramLong;
    this.mRecycled = false;
  }
  
  public Bitmap decodeRegion(Rect paramRect, BitmapFactory.Options paramOptions) {
    BitmapFactory.Options.validate(paramOptions);
    synchronized (this.mNativeLock) {
      checkRecycled("decodeRegion called on recycled region decoder");
      if (paramRect.right > 0 && paramRect.bottom > 0 && paramRect.left < getWidth()) {
        int i = paramRect.top;
        if (i < getHeight()) {
          long l1 = this.mNativeBitmapRegionDecoder;
          int j = paramRect.left;
          i = paramRect.top;
          int k = paramRect.right, m = paramRect.left, n = paramRect.bottom, i1 = paramRect.top;
          long l2 = BitmapFactory.Options.nativeInBitmap(paramOptions);
          long l3 = BitmapFactory.Options.nativeColorSpace(paramOptions);
          return nativeDecodeRegion(l1, j, i, k - m, n - i1, paramOptions, l2, l3);
        } 
      } 
      IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
      this("rectangle is outside the image");
      throw illegalArgumentException;
    } 
  }
  
  public int getWidth() {
    synchronized (this.mNativeLock) {
      checkRecycled("getWidth called on recycled region decoder");
      return nativeGetWidth(this.mNativeBitmapRegionDecoder);
    } 
  }
  
  public int getHeight() {
    synchronized (this.mNativeLock) {
      checkRecycled("getHeight called on recycled region decoder");
      return nativeGetHeight(this.mNativeBitmapRegionDecoder);
    } 
  }
  
  public void recycle() {
    synchronized (this.mNativeLock) {
      if (!this.mRecycled) {
        nativeClean(this.mNativeBitmapRegionDecoder);
        this.mRecycled = true;
      } 
      return;
    } 
  }
  
  public final boolean isRecycled() {
    return this.mRecycled;
  }
  
  private void checkRecycled(String paramString) {
    if (!this.mRecycled)
      return; 
    throw new IllegalStateException(paramString);
  }
  
  protected void finalize() throws Throwable {
    try {
      recycle();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  private static native void nativeClean(long paramLong);
  
  private static native Bitmap nativeDecodeRegion(long paramLong1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, BitmapFactory.Options paramOptions, long paramLong2, long paramLong3);
  
  private static native int nativeGetHeight(long paramLong);
  
  private static native int nativeGetWidth(long paramLong);
  
  private static native BitmapRegionDecoder nativeNewInstance(long paramLong, boolean paramBoolean);
  
  private static native BitmapRegionDecoder nativeNewInstance(FileDescriptor paramFileDescriptor, boolean paramBoolean);
  
  private static native BitmapRegionDecoder nativeNewInstance(InputStream paramInputStream, byte[] paramArrayOfbyte, boolean paramBoolean);
  
  private static native BitmapRegionDecoder nativeNewInstance(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, boolean paramBoolean);
}
