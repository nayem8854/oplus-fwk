package android.graphics;

@Deprecated
public class AvoidXfermode extends Xfermode {
  class Mode extends Enum<Mode> {
    private static final Mode[] $VALUES;
    
    public static Mode valueOf(String param1String) {
      return Enum.<Mode>valueOf(Mode.class, param1String);
    }
    
    public static Mode[] values() {
      return (Mode[])$VALUES.clone();
    }
    
    public static final Mode AVOID = new Mode("AVOID", 0, 0);
    
    public static final Mode TARGET;
    
    final int nativeInt;
    
    static {
      Mode mode = new Mode("TARGET", 1, 1);
      $VALUES = new Mode[] { AVOID, mode };
    }
    
    private Mode(AvoidXfermode this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.nativeInt = param1Int2;
    }
  }
  
  public AvoidXfermode(int paramInt1, int paramInt2, Mode paramMode) {
    if (paramInt2 >= 0 && paramInt2 <= 255)
      return; 
    throw new IllegalArgumentException("tolerance must be 0..255");
  }
}
