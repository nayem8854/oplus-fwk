package android.graphics;

import com.oplus.util.OplusTypeCastingHelper;

public class OplusBasePath {
  private boolean mIsAddArea = false;
  
  public boolean isAddArea() {
    return this.mIsAddArea;
  }
  
  public void setIsAddRect(boolean paramBoolean) {
    this.mIsAddArea = paramBoolean;
  }
  
  public void setIsAddRect(Path paramPath) {
    if (paramPath != null) {
      OplusBasePath oplusBasePath = (OplusBasePath)OplusTypeCastingHelper.typeCasting(OplusBasePath.class, paramPath);
      if (oplusBasePath != null)
        this.mIsAddArea = oplusBasePath.isAddArea(); 
    } 
  }
}
