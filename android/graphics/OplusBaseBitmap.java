package android.graphics;

public class OplusBaseBitmap {
  private int mColorState;
  
  private boolean mIsAssetSource;
  
  private boolean mIsCalculatedColor;
  
  private boolean mIsCanvasBaseBitmap = false;
  
  private boolean mIsViewSrc = false;
  
  public boolean hasCalculatedColor() {
    return this.mIsCalculatedColor;
  }
  
  public void setHasCalculatedColor(boolean paramBoolean) {
    this.mIsCalculatedColor = paramBoolean;
  }
  
  public boolean isAssetSource() {
    return this.mIsAssetSource;
  }
  
  public void setIsAssetSource(boolean paramBoolean) {
    this.mIsAssetSource = paramBoolean;
  }
  
  public int getColorState() {
    return this.mColorState;
  }
  
  public void setColorState(int paramInt) {
    this.mColorState = paramInt;
  }
  
  public void setIsCanvasBaseBitmap(boolean paramBoolean) {
    this.mIsCanvasBaseBitmap = paramBoolean;
  }
  
  public boolean isCanvasBaseBitmap() {
    return this.mIsCanvasBaseBitmap;
  }
  
  public boolean isViewSrc() {
    return this.mIsViewSrc;
  }
  
  public void setIsViewSrc(boolean paramBoolean) {
    this.mIsViewSrc = paramBoolean;
  }
  
  public boolean checkLM(byte[] paramArrayOfbyte, boolean paramBoolean, int paramInt1, int paramInt2) {
    return false;
  }
}
