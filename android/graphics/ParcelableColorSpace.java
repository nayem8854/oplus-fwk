package android.graphics;

import android.os.Parcel;
import android.os.Parcelable;

public final class ParcelableColorSpace extends ColorSpace implements Parcelable {
  public static boolean isParcelable(ColorSpace paramColorSpace) {
    if (paramColorSpace.getId() == -1) {
      if (!(paramColorSpace instanceof ColorSpace.Rgb))
        return false; 
      paramColorSpace = paramColorSpace;
      if (paramColorSpace.getTransferParameters() == null)
        return false; 
    } 
    return true;
  }
  
  public ParcelableColorSpace(ColorSpace paramColorSpace) {
    super(paramColorSpace.getName(), paramColorSpace.getModel(), paramColorSpace.getId());
    this.mColorSpace = paramColorSpace;
    if (paramColorSpace.getId() == -1) {
      paramColorSpace = this.mColorSpace;
      if (paramColorSpace instanceof ColorSpace.Rgb) {
        paramColorSpace = paramColorSpace;
        if (paramColorSpace.getTransferParameters() == null)
          throw new IllegalArgumentException("ColorSpace must use an ICC parametric transfer function to be parcelable"); 
      } else {
        throw new IllegalArgumentException("Unable to parcel unknown ColorSpaces that are not ColorSpace.Rgb");
      } 
    } 
  }
  
  public ColorSpace getColorSpace() {
    return this.mColorSpace;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramInt = this.mColorSpace.getId();
    paramParcel.writeInt(paramInt);
    if (paramInt == -1) {
      ColorSpace.Rgb rgb = (ColorSpace.Rgb)this.mColorSpace;
      paramParcel.writeString(rgb.getName());
      paramParcel.writeFloatArray(rgb.getPrimaries());
      paramParcel.writeFloatArray(rgb.getWhitePoint());
      ColorSpace.Rgb.TransferParameters transferParameters = rgb.getTransferParameters();
      paramParcel.writeDouble(transferParameters.a);
      paramParcel.writeDouble(transferParameters.b);
      paramParcel.writeDouble(transferParameters.c);
      paramParcel.writeDouble(transferParameters.d);
      paramParcel.writeDouble(transferParameters.e);
      paramParcel.writeDouble(transferParameters.f);
      paramParcel.writeDouble(transferParameters.g);
    } 
  }
  
  public static final Parcelable.Creator<ParcelableColorSpace> CREATOR = new Parcelable.Creator<ParcelableColorSpace>() {
      public ParcelableColorSpace createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        if (i == -1) {
          String str = param1Parcel.readString();
          float[] arrayOfFloat1 = param1Parcel.createFloatArray();
          float[] arrayOfFloat2 = param1Parcel.createFloatArray();
          double d1 = param1Parcel.readDouble();
          double d2 = param1Parcel.readDouble();
          double d3 = param1Parcel.readDouble();
          double d4 = param1Parcel.readDouble();
          double d5 = param1Parcel.readDouble();
          double d6 = param1Parcel.readDouble();
          double d7 = param1Parcel.readDouble();
          ColorSpace.Rgb.TransferParameters transferParameters = new ColorSpace.Rgb.TransferParameters(d1, d2, d3, d4, d5, d6, d7);
          return new ParcelableColorSpace(new ColorSpace.Rgb(str, arrayOfFloat1, arrayOfFloat2, transferParameters));
        } 
        return new ParcelableColorSpace(ColorSpace.get(i));
      }
      
      public ParcelableColorSpace[] newArray(int param1Int) {
        return new ParcelableColorSpace[param1Int];
      }
    };
  
  private final ColorSpace mColorSpace;
  
  public boolean isWideGamut() {
    return this.mColorSpace.isWideGamut();
  }
  
  public float getMinValue(int paramInt) {
    return this.mColorSpace.getMinValue(paramInt);
  }
  
  public float getMaxValue(int paramInt) {
    return this.mColorSpace.getMaxValue(paramInt);
  }
  
  public float[] toXyz(float[] paramArrayOffloat) {
    return this.mColorSpace.toXyz(paramArrayOffloat);
  }
  
  public float[] fromXyz(float[] paramArrayOffloat) {
    return this.mColorSpace.fromXyz(paramArrayOffloat);
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    return this.mColorSpace.equals(((ParcelableColorSpace)paramObject).mColorSpace);
  }
  
  public int hashCode() {
    return this.mColorSpace.hashCode();
  }
  
  long getNativeInstance() {
    return this.mColorSpace.getNativeInstance();
  }
}
