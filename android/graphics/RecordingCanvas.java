package android.graphics;

import android.util.Pools;
import android.view.DisplayListCanvas;
import android.view.TextureLayer;
import dalvik.annotation.optimization.CriticalNative;
import dalvik.annotation.optimization.FastNative;

public final class RecordingCanvas extends DisplayListCanvas {
  public static final int MAX_BITMAP_SIZE = 104857600;
  
  private static final int MAX_BITMAP_SIZE_OPPO = 157286400;
  
  private static final int POOL_LIMIT = 25;
  
  private static final Pools.SynchronizedPool<RecordingCanvas> sPool = new Pools.SynchronizedPool(25);
  
  private int mHeight;
  
  public RenderNode mNode;
  
  private int mWidth;
  
  static RecordingCanvas obtain(RenderNode paramRenderNode, int paramInt1, int paramInt2) {
    if (paramRenderNode != null) {
      RecordingCanvas recordingCanvas = (RecordingCanvas)sPool.acquire();
      if (recordingCanvas == null) {
        recordingCanvas = new RecordingCanvas(paramRenderNode, paramInt1, paramInt2);
      } else {
        nResetDisplayListCanvas(recordingCanvas.mNativeCanvasWrapper, paramRenderNode.mNativeRenderNode, paramInt1, paramInt2);
      } 
      recordingCanvas.mNode = paramRenderNode;
      recordingCanvas.mWidth = paramInt1;
      recordingCanvas.mHeight = paramInt2;
      return recordingCanvas;
    } 
    throw new IllegalArgumentException("node cannot be null");
  }
  
  void recycle() {
    this.mNode = null;
    sPool.release(this);
  }
  
  long finishRecording() {
    return nFinishRecording(this.mNativeCanvasWrapper);
  }
  
  public boolean isRecordingFor(Object paramObject) {
    boolean bool;
    if (paramObject == this.mNode) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected RecordingCanvas(RenderNode paramRenderNode, int paramInt1, int paramInt2) {
    super(nCreateDisplayListCanvas(paramRenderNode.mNativeRenderNode, paramInt1, paramInt2));
    this.mDensity = 0;
  }
  
  public void setDensity(int paramInt) {}
  
  public boolean isHardwareAccelerated() {
    return true;
  }
  
  public void setBitmap(Bitmap paramBitmap) {
    throw new UnsupportedOperationException();
  }
  
  public boolean isOpaque() {
    return false;
  }
  
  public int getWidth() {
    return this.mWidth;
  }
  
  public int getHeight() {
    return this.mHeight;
  }
  
  public int getMaximumBitmapWidth() {
    return nGetMaximumTextureWidth();
  }
  
  public int getMaximumBitmapHeight() {
    return nGetMaximumTextureHeight();
  }
  
  public void enableZ() {
    nInsertReorderBarrier(this.mNativeCanvasWrapper, true);
  }
  
  public void disableZ() {
    nInsertReorderBarrier(this.mNativeCanvasWrapper, false);
  }
  
  @Deprecated
  public void callDrawGLFunction2(long paramLong) {
    nCallDrawGLFunction(this.mNativeCanvasWrapper, paramLong, (Runnable)null);
  }
  
  @Deprecated
  public void drawGLFunctor2(long paramLong, Runnable paramRunnable) {
    nCallDrawGLFunction(this.mNativeCanvasWrapper, paramLong, paramRunnable);
  }
  
  public void drawWebViewFunctor(int paramInt) {
    nDrawWebViewFunctor(this.mNativeCanvasWrapper, paramInt);
  }
  
  public void drawRenderNode(RenderNode paramRenderNode) {
    nDrawRenderNode(this.mNativeCanvasWrapper, paramRenderNode.mNativeRenderNode);
  }
  
  public void drawTextureLayer(TextureLayer paramTextureLayer) {
    nDrawTextureLayer(this.mNativeCanvasWrapper, paramTextureLayer.getLayerHandle());
  }
  
  public void drawCircle(CanvasProperty<Float> paramCanvasProperty1, CanvasProperty<Float> paramCanvasProperty2, CanvasProperty<Float> paramCanvasProperty3, CanvasProperty<Paint> paramCanvasProperty) {
    long l1 = this.mNativeCanvasWrapper, l2 = paramCanvasProperty1.getNativeContainer(), l3 = paramCanvasProperty2.getNativeContainer();
    long l4 = paramCanvasProperty3.getNativeContainer(), l5 = paramCanvasProperty.getNativeContainer();
    nDrawCircle(l1, l2, l3, l4, l5);
  }
  
  public void drawRoundRect(CanvasProperty<Float> paramCanvasProperty1, CanvasProperty<Float> paramCanvasProperty2, CanvasProperty<Float> paramCanvasProperty3, CanvasProperty<Float> paramCanvasProperty4, CanvasProperty<Float> paramCanvasProperty5, CanvasProperty<Float> paramCanvasProperty6, CanvasProperty<Paint> paramCanvasProperty) {
    long l1 = this.mNativeCanvasWrapper, l2 = paramCanvasProperty1.getNativeContainer(), l3 = paramCanvasProperty2.getNativeContainer();
    long l4 = paramCanvasProperty3.getNativeContainer(), l5 = paramCanvasProperty4.getNativeContainer();
    long l6 = paramCanvasProperty5.getNativeContainer(), l7 = paramCanvasProperty6.getNativeContainer();
    long l8 = paramCanvasProperty.getNativeContainer();
    nDrawRoundRect(l1, l2, l3, l4, l5, l6, l7, l8);
  }
  
  protected void throwIfCannotDraw(Bitmap paramBitmap) {
    super.throwIfCannotDraw(paramBitmap);
    int i = paramBitmap.getByteCount();
    if (i > 104857600)
      if (i <= 157286400) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Canvas: trying to draw too large(");
        stringBuilder.append(i);
        stringBuilder.append("bytes) bitmap.");
        RuntimeException runtimeException = new RuntimeException(stringBuilder.toString());
        runtimeException.printStackTrace();
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Canvas: trying to draw too large(");
        stringBuilder.append(i);
        stringBuilder.append("bytes) bitmap.");
        throw new RuntimeException(stringBuilder.toString());
      }  
  }
  
  @FastNative
  private static native void nCallDrawGLFunction(long paramLong1, long paramLong2, Runnable paramRunnable);
  
  @CriticalNative
  private static native long nCreateDisplayListCanvas(long paramLong, int paramInt1, int paramInt2);
  
  @CriticalNative
  private static native void nDrawCircle(long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5);
  
  @CriticalNative
  private static native void nDrawRenderNode(long paramLong1, long paramLong2);
  
  @CriticalNative
  private static native void nDrawRoundRect(long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5, long paramLong6, long paramLong7, long paramLong8);
  
  @CriticalNative
  private static native void nDrawTextureLayer(long paramLong1, long paramLong2);
  
  @CriticalNative
  private static native void nDrawWebViewFunctor(long paramLong, int paramInt);
  
  @CriticalNative
  private static native long nFinishRecording(long paramLong);
  
  @CriticalNative
  private static native int nGetMaximumTextureHeight();
  
  @CriticalNative
  private static native int nGetMaximumTextureWidth();
  
  @CriticalNative
  private static native void nInsertReorderBarrier(long paramLong, boolean paramBoolean);
  
  @CriticalNative
  private static native void nResetDisplayListCanvas(long paramLong1, long paramLong2, int paramInt1, int paramInt2);
}
