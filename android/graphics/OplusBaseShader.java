package android.graphics;

import com.oplus.util.OplusTypeCastingHelper;
import java.util.ArrayList;
import java.util.List;

public class OplusBaseShader {
  public void setColors(long[] paramArrayOflong) {}
  
  public long[] getColorLongs() {
    return null;
  }
  
  public List<long[]> getComposeShaderColor() {
    return null;
  }
  
  public void setComposeShaderColor(List<long[]> paramList) {}
  
  public List<long[]> getComposeShaderColor(Shader paramShader1, Shader paramShader2) {
    ArrayList<long[]> arrayList = new ArrayList();
    OplusBaseShader oplusBaseShader1 = (OplusBaseShader)OplusTypeCastingHelper.typeCasting(OplusBaseShader.class, paramShader1);
    OplusBaseShader oplusBaseShader2 = (OplusBaseShader)OplusTypeCastingHelper.typeCasting(OplusBaseShader.class, paramShader2);
    if (oplusBaseShader1 != null && oplusBaseShader2 != null) {
      arrayList.add(oplusBaseShader1.getColorLongs());
      arrayList.add(oplusBaseShader2.getColorLongs());
    } 
    return (List<long[]>)arrayList;
  }
  
  public void setComposeShaderColor(Shader paramShader1, Shader paramShader2, List<long[]> paramList) {
    if (paramList != null && paramList.size() == 2) {
      OplusBaseShader oplusBaseShader1 = (OplusBaseShader)OplusTypeCastingHelper.typeCasting(OplusBaseShader.class, paramShader1);
      OplusBaseShader oplusBaseShader2 = (OplusBaseShader)OplusTypeCastingHelper.typeCasting(OplusBaseShader.class, paramShader2);
      if (oplusBaseShader1 != null && oplusBaseShader2 != null) {
        oplusBaseShader1.setColors(paramList.get(0));
        oplusBaseShader2.setColors(paramList.get(1));
        discardNativeInstance();
      } 
    } 
  }
  
  protected void discardNativeInstance() {}
}
