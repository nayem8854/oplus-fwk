package android.graphics;

import libcore.util.NativeAllocationRegistry;

public class Shader extends OplusBaseShader {
  private Runnable mCleaner;
  
  private final ColorSpace mColorSpace;
  
  private Matrix mLocalMatrix;
  
  private long mNativeInstance;
  
  class NoImagePreloadHolder {
    public static final NativeAllocationRegistry sRegistry;
    
    static {
      ClassLoader classLoader = Shader.class.getClassLoader();
      long l = Shader.nativeGetFinalizer();
      sRegistry = NativeAllocationRegistry.createMalloced(classLoader, l);
    }
  }
  
  @Deprecated
  public Shader() {
    this.mColorSpace = null;
  }
  
  public Shader(ColorSpace paramColorSpace) {
    this.mColorSpace = paramColorSpace;
    if (paramColorSpace != null) {
      paramColorSpace.getNativeInstance();
      return;
    } 
    throw new IllegalArgumentException("Use Shader() to create a Shader with no ColorSpace");
  }
  
  protected ColorSpace colorSpace() {
    return this.mColorSpace;
  }
  
  class TileMode extends Enum<TileMode> {
    private static final TileMode[] $VALUES;
    
    public static TileMode valueOf(String param1String) {
      return Enum.<TileMode>valueOf(TileMode.class, param1String);
    }
    
    public static TileMode[] values() {
      return (TileMode[])$VALUES.clone();
    }
    
    public static final TileMode CLAMP = new TileMode("CLAMP", 0, 0);
    
    public static final TileMode MIRROR;
    
    public static final TileMode REPEAT = new TileMode("REPEAT", 1, 1);
    
    final int nativeInt;
    
    static {
      TileMode tileMode = new TileMode("MIRROR", 2, 2);
      $VALUES = new TileMode[] { CLAMP, REPEAT, tileMode };
    }
    
    private TileMode(Shader this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.nativeInt = param1Int2;
    }
  }
  
  public boolean getLocalMatrix(Matrix paramMatrix) {
    Matrix matrix = this.mLocalMatrix;
    if (matrix != null) {
      paramMatrix.set(matrix);
      return true;
    } 
    return false;
  }
  
  public void setLocalMatrix(Matrix paramMatrix) {
    if (paramMatrix == null || paramMatrix.isIdentity()) {
      if (this.mLocalMatrix != null) {
        this.mLocalMatrix = null;
        discardNativeInstance();
      } 
      return;
    } 
    Matrix matrix = this.mLocalMatrix;
    if (matrix == null) {
      this.mLocalMatrix = new Matrix(paramMatrix);
      discardNativeInstance();
    } else if (!matrix.equals(paramMatrix)) {
      this.mLocalMatrix.set(paramMatrix);
      discardNativeInstance();
    } 
  }
  
  long createNativeInstance(long paramLong) {
    return 0L;
  }
  
  protected final void discardNativeInstance() {
    if (this.mNativeInstance != 0L) {
      this.mCleaner.run();
      this.mCleaner = null;
      this.mNativeInstance = 0L;
    } 
  }
  
  protected void verifyNativeInstance() {}
  
  public final long getNativeInstance() {
    verifyNativeInstance();
    if (this.mNativeInstance == 0L) {
      Matrix matrix = this.mLocalMatrix;
      if (matrix == null) {
        l = 0L;
      } else {
        l = matrix.native_instance;
      } 
      long l = createNativeInstance(l);
      if (l != 0L)
        this.mCleaner = NoImagePreloadHolder.sRegistry.registerNativeAllocation(this, this.mNativeInstance); 
    } 
    return this.mNativeInstance;
  }
  
  public static long[] convertColors(int[] paramArrayOfint) {
    if (paramArrayOfint.length >= 2) {
      long[] arrayOfLong = new long[paramArrayOfint.length];
      for (byte b = 0; b < paramArrayOfint.length; b++)
        arrayOfLong[b] = Color.pack(paramArrayOfint[b]); 
      return arrayOfLong;
    } 
    throw new IllegalArgumentException("needs >= 2 number of colors");
  }
  
  public static ColorSpace detectColorSpace(long[] paramArrayOflong) {
    if (paramArrayOflong.length >= 2) {
      ColorSpace colorSpace = Color.colorSpace(paramArrayOflong[0]);
      for (byte b = 1; b < paramArrayOflong.length; ) {
        if (Color.colorSpace(paramArrayOflong[b]) == colorSpace) {
          b++;
          continue;
        } 
        throw new IllegalArgumentException("All colors must be in the same ColorSpace!");
      } 
      return colorSpace;
    } 
    throw new IllegalArgumentException("needs >= 2 number of colors");
  }
  
  private static native long nativeGetFinalizer();
}
