package android.graphics;

import java.util.HashMap;

public final class OplusSmartCutQuantizer {
  private final float[] mTempHsl = new float[3];
  
  private HashMap<Integer, Integer> mColorMap = new HashMap<>();
  
  int mDominantColor = 0;
  
  private static final String LOG_TAG = "OplusSmartCutQuantizer";
  
  private static final boolean LOG_TIMINGS = false;
  
  private static final int QUANTIZE_WORD_MASK = 255;
  
  private static final int QUANTIZE_WORD_WIDTH = 8;
  
  public OplusSmartCutQuantizer(int[] paramArrayOfint) {
    int i = 0;
    int j = 0;
    for (byte b = 0; b < paramArrayOfint.length; b += 3) {
      int k = quantizeFromRgb888(paramArrayOfint[b]);
      paramArrayOfint[b] = k;
      if (this.mColorMap.containsKey(Integer.valueOf(paramArrayOfint[b]))) {
        int m = ((Integer)this.mColorMap.get(Integer.valueOf(paramArrayOfint[b]))).intValue();
        this.mColorMap.put(Integer.valueOf(paramArrayOfint[b]), Integer.valueOf(m + 1));
        int n = i;
        if (m + 1 > i) {
          n = m + 1;
          j = k;
        } 
        i = n;
      } else {
        this.mColorMap.put(Integer.valueOf(paramArrayOfint[b]), Integer.valueOf(1));
      } 
    } 
    this.mDominantColor = j;
  }
  
  public HashMap<Integer, Integer> getQuantizedColors() {
    return this.mColorMap;
  }
  
  public int getDominantColor() {
    return this.mDominantColor;
  }
  
  private static int quantizeFromRgb888(int paramInt) {
    int i = modifyWordWidth(Color.red(paramInt), 8, 8);
    int j = modifyWordWidth(Color.green(paramInt), 8, 8);
    paramInt = modifyWordWidth(Color.blue(paramInt), 8, 8);
    return i << 16 | j << 8 | paramInt;
  }
  
  static int approximateToRgb888(int paramInt1, int paramInt2, int paramInt3) {
    paramInt1 = modifyWordWidth(paramInt1, 8, 8);
    paramInt2 = modifyWordWidth(paramInt2, 8, 8);
    paramInt3 = modifyWordWidth(paramInt3, 8, 8);
    return Color.rgb(paramInt1, paramInt2, paramInt3);
  }
  
  private static int approximateToRgb888(int paramInt) {
    return approximateToRgb888(quantizedRed(paramInt), quantizedGreen(paramInt), quantizedBlue(paramInt));
  }
  
  static int quantizedRed(int paramInt) {
    return paramInt >> 16 & 0xFF;
  }
  
  static int quantizedGreen(int paramInt) {
    return paramInt >> 8 & 0xFF;
  }
  
  static int quantizedBlue(int paramInt) {
    return paramInt & 0xFF;
  }
  
  private static int modifyWordWidth(int paramInt1, int paramInt2, int paramInt3) {
    if (paramInt3 > paramInt2) {
      paramInt1 <<= paramInt3 - paramInt2;
    } else {
      paramInt1 >>= paramInt2 - paramInt3;
    } 
    return paramInt1 & (1 << paramInt3) - 1;
  }
}
