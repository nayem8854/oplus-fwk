package android.graphics;

import android.common.OplusFeatureCache;
import com.oplus.darkmode.IOplusDarkModeManager;
import com.oplus.util.OplusTypeCastingHelper;
import java.util.List;

public abstract class OplusBaseBaseCanvas {
  private int mViewWidth;
  
  private int mViewType;
  
  private int mViewHeight;
  
  private int mTransformType = 2;
  
  private RectF mRectF = new RectF();
  
  private Rect mClipChildRect;
  
  public static final int TYPE_UNKNOWN = 0;
  
  public static final int TYPE_MAYBE_SMALL_VIEW = 1;
  
  public static final int TYPE_MAYBE_FULL_HORIZONTAL_RECT = 2;
  
  public static final int TYPE_MAYBE_FULL_HORIZONTAL_LINE = 3;
  
  public static final int TRANSFORM_UNKNOWN = 0;
  
  public static final int TRANSFORM_FOREGROUND = 2;
  
  public static final int TRANSFORM_BACKGROUND = 1;
  
  public void setViewArea(int paramInt1, int paramInt2) {
    this.mViewWidth = paramInt1;
    this.mViewHeight = paramInt2;
  }
  
  public int getViewAreaWidth() {
    return this.mViewWidth;
  }
  
  public int getViewAreaHeight() {
    return this.mViewHeight;
  }
  
  public int getOplusViewType() {
    return this.mViewType;
  }
  
  public void setOplusViewTypeLocked(int paramInt) {
    this.mViewType = paramInt;
  }
  
  public int getTransformType() {
    return this.mTransformType;
  }
  
  public void setTransformType(int paramInt) {
    this.mTransformType = paramInt;
  }
  
  public static class RealPaintState {
    public int color;
    
    public ColorFilter colorFilter;
    
    public int colorFilterColor;
    
    public List<long[]> composeShaderColors;
    
    public long[] shaderColors;
  }
  
  public static class Entity {
    public boolean isDarkMode;
    
    public Paint newPaint;
    
    public OplusBaseBaseCanvas.RealPaintState realPaintState;
  }
  
  public RectF getRectF(float paramFloat1, float paramFloat2) {
    this.mRectF.set(0.0F, 0.0F, paramFloat1, paramFloat2);
    return this.mRectF;
  }
  
  public RectF getRectF(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    this.mRectF.set(paramFloat1, paramFloat2, paramFloat3, paramFloat4);
    return this.mRectF;
  }
  
  public static RectF getRectF(Rect paramRect) {
    return new RectF(paramRect);
  }
  
  protected void setIsCanvasBaseBitmap(Bitmap paramBitmap, boolean paramBoolean) {
    if (paramBitmap != null) {
      OplusBaseBitmap oplusBaseBitmap = (OplusBaseBitmap)OplusTypeCastingHelper.typeCasting(OplusBaseBitmap.class, paramBitmap);
      if (oplusBaseBitmap != null)
        oplusBaseBitmap.setIsCanvasBaseBitmap(paramBoolean); 
    } 
  }
  
  public boolean isHardwareAccelerated() {
    return false;
  }
  
  public boolean isDarkMode() {
    return ((IOplusDarkModeManager)OplusFeatureCache.<IOplusDarkModeManager>getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0])).isInDarkMode(isHardwareAccelerated());
  }
  
  public int getWidth() {
    return 0;
  }
  
  public int getHeight() {
    return 0;
  }
  
  protected int[] changeColors(int[] paramArrayOfint) {
    return paramArrayOfint;
  }
  
  protected Entity changeBitmap(Paint paramPaint, Bitmap paramBitmap, RectF paramRectF) {
    boolean bool = isDarkMode();
    Entity entity = null;
    if (bool) {
      entity = new Entity();
      RealPaintState realPaintState = ((IOplusDarkModeManager)OplusFeatureCache.<IOplusDarkModeManager>getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0])).getRealPaintState(paramPaint);
      entity.newPaint = ((IOplusDarkModeManager)OplusFeatureCache.<IOplusDarkModeManager>getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0])).getPaintWhenDrawBitmap(paramPaint, paramBitmap, ensureRect(paramRectF), this);
      entity.realPaintState = realPaintState;
      entity.isDarkMode = bool;
    } 
    return entity;
  }
  
  protected int changeColor(int paramInt) {
    if (isDarkMode())
      return ((IOplusDarkModeManager)OplusFeatureCache.<IOplusDarkModeManager>getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0])).changeWhenDrawColor(paramInt, isDarkMode(), this); 
    return paramInt;
  }
  
  protected Entity changePatch(NinePatch paramNinePatch, Paint paramPaint, RectF paramRectF) {
    boolean bool = isDarkMode();
    Entity entity = null;
    if (bool) {
      entity = new Entity();
      RealPaintState realPaintState = ((IOplusDarkModeManager)OplusFeatureCache.<IOplusDarkModeManager>getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0])).getRealPaintState(paramPaint);
      entity.newPaint = ((IOplusDarkModeManager)OplusFeatureCache.<IOplusDarkModeManager>getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0])).getPaintWhenDrawPatch(paramNinePatch, paramPaint, ensureRect(paramRectF), this);
      entity.realPaintState = realPaintState;
      entity.isDarkMode = bool;
    } 
    return entity;
  }
  
  protected Entity changeArea(Paint paramPaint, RectF paramRectF) {
    boolean bool = isDarkMode();
    Entity entity = null;
    if (bool) {
      entity = new Entity();
      RealPaintState realPaintState = ((IOplusDarkModeManager)OplusFeatureCache.<IOplusDarkModeManager>getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0])).getRealPaintState(paramPaint);
      ((IOplusDarkModeManager)OplusFeatureCache.<IOplusDarkModeManager>getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0])).changePaintWhenDrawArea(paramPaint, ensureRect(paramRectF), this);
      entity.realPaintState = realPaintState;
      entity.isDarkMode = bool;
    } 
    return entity;
  }
  
  protected Entity changeArea(Paint paramPaint, RectF paramRectF, Path paramPath) {
    boolean bool = isDarkMode();
    Entity entity = null;
    if (bool) {
      entity = new Entity();
      RealPaintState realPaintState = ((IOplusDarkModeManager)OplusFeatureCache.<IOplusDarkModeManager>getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0])).getRealPaintState(paramPaint);
      ((IOplusDarkModeManager)OplusFeatureCache.<IOplusDarkModeManager>getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0])).changePaintWhenDrawArea(paramPaint, ensureRect(paramRectF), paramPath, this);
      entity.realPaintState = realPaintState;
      entity.isDarkMode = bool;
    } 
    return entity;
  }
  
  protected Entity changeText(Paint paramPaint) {
    boolean bool = isDarkMode();
    Entity entity = null;
    if (bool) {
      entity = new Entity();
      RealPaintState realPaintState = ((IOplusDarkModeManager)OplusFeatureCache.<IOplusDarkModeManager>getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0])).getRealPaintState(paramPaint);
      ((IOplusDarkModeManager)OplusFeatureCache.<IOplusDarkModeManager>getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0])).changePaintWhenDrawText(paramPaint, this);
      entity.realPaintState = realPaintState;
      entity.isDarkMode = bool;
    } 
    return entity;
  }
  
  protected void resetEntity(Entity paramEntity, Paint paramPaint) {
    if (paramEntity != null && paramEntity.isDarkMode && paramEntity.realPaintState != null && paramPaint != null)
      ((IOplusDarkModeManager)OplusFeatureCache.<IOplusDarkModeManager>getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0])).resetRealPaintIfNeed(paramPaint, paramEntity.realPaintState); 
  }
  
  private RectF ensureRect(RectF paramRectF) {
    if (paramRectF == null) {
      this.mRectF.set(0.0F, 0.0F, getWidth(), getHeight());
      return this.mRectF;
    } 
    return paramRectF;
  }
  
  public void setClipChildRect(Rect paramRect) {
    if (paramRect == null) {
      this.mClipChildRect = null;
    } else {
      Rect rect = this.mClipChildRect;
      if (rect == null) {
        this.mClipChildRect = paramRect;
      } else {
        rect.set(paramRect);
      } 
    } 
  }
  
  public Rect getClipChildRect() {
    return this.mClipChildRect;
  }
}
