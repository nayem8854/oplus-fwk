package android.graphics;

import android.annotation.SystemApi;
import android.app.AlarmManager;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.os.SharedMemory;
import android.os.Trace;
import android.os.UserHandle;
import android.system.ErrnoException;
import android.util.Log;
import android.view.IGraphicsStats;
import android.view.IGraphicsStatsCallback;
import com.android.internal.util.DumpUtils;
import com.android.internal.util.FastPrintWriter;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.TimeZone;

public class GraphicsStatsService extends IGraphicsStats.Stub {
  private static final int AID_STATSD = 1066;
  
  private static final int DELETE_OLD = 2;
  
  public static final String GRAPHICS_STATS_SERVICE = "graphicsstats";
  
  private static final int SAVE_BUFFER = 1;
  
  private static final String TAG = "GraphicsStatsService";
  
  private ArrayList<ActiveBuffer> mActive;
  
  private final AlarmManager mAlarmManager;
  
  private final AppOpsManager mAppOps;
  
  private final int mAshmemSize;
  
  private final Context mContext;
  
  private final Object mFileAccessLock;
  
  private File mGraphicsStatsDir;
  
  private final Object mLock;
  
  private boolean mRotateIsScheduled;
  
  private Handler mWriteOutHandler;
  
  private final byte[] mZeroData;
  
  @SystemApi
  public GraphicsStatsService(Context paramContext) {
    int i = nGetAshmemSize();
    this.mZeroData = new byte[i];
    this.mLock = new Object();
    this.mActive = new ArrayList<>();
    this.mFileAccessLock = new Object();
    this.mRotateIsScheduled = false;
    this.mContext = paramContext;
    this.mAppOps = paramContext.<AppOpsManager>getSystemService(AppOpsManager.class);
    this.mAlarmManager = paramContext.<AlarmManager>getSystemService(AlarmManager.class);
    File file1 = new File(Environment.getDataDirectory(), "system");
    this.mGraphicsStatsDir = file1 = new File(file1, "graphicsstats");
    file1.mkdirs();
    if (this.mGraphicsStatsDir.exists()) {
      HandlerThread handlerThread = new HandlerThread("GraphicsStats-disk", 10);
      handlerThread.start();
      this.mWriteOutHandler = new Handler(handlerThread.getLooper(), (Handler.Callback)new Object(this));
      nativeInit();
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Graphics stats directory does not exist: ");
    File file2 = this.mGraphicsStatsDir;
    stringBuilder.append(file2.getAbsolutePath());
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  private void scheduleRotateLocked() {
    if (this.mRotateIsScheduled)
      return; 
    this.mRotateIsScheduled = true;
    Calendar calendar = normalizeDate(System.currentTimeMillis());
    calendar.add(5, 1);
    this.mAlarmManager.setExact(1, calendar.getTimeInMillis(), "GraphicsStatsService", new _$$Lambda$GraphicsStatsService$an_DvOX2nWltnD5OBOre5S9EpXs(this), this.mWriteOutHandler);
  }
  
  private void onAlarm() {
    synchronized (this.mLock) {
      this.mRotateIsScheduled = false;
      scheduleRotateLocked();
      ActiveBuffer[] arrayOfActiveBuffer = this.mActive.<ActiveBuffer>toArray(new ActiveBuffer[0]);
      int i;
      byte b;
      for (i = arrayOfActiveBuffer.length, b = 0; b < i; ) {
        ActiveBuffer activeBuffer = arrayOfActiveBuffer[b];
        try {
          activeBuffer.mCallback.onRotateGraphicsStatsBuffer();
        } catch (RemoteException remoteException) {
          null = activeBuffer.mInfo.mPackageName;
          int j = activeBuffer.mPid;
          Log.w("GraphicsStatsService", String.format("Failed to notify '%s' (pid=%d) to rotate buffers", new Object[] { null, Integer.valueOf(j) }), (Throwable)remoteException);
        } 
        b++;
      } 
      this.mWriteOutHandler.sendEmptyMessageDelayed(2, 10000L);
      return;
    } 
  }
  
  public ParcelFileDescriptor requestBufferForProcess(String paramString, IGraphicsStatsCallback paramIGraphicsStatsCallback) throws RemoteException {
    int i = Binder.getCallingUid();
    int j = Binder.getCallingPid();
    long l = Binder.clearCallingIdentity();
    try {
      Object object1;
      this.mAppOps.checkPackage(i, paramString);
      PackageManager packageManager = this.mContext.getPackageManager();
      int k = UserHandle.getUserId(i);
      PackageInfo packageInfo = packageManager.getPackageInfoAsUser(paramString, 0, k);
      Object object2 = this.mLock;
      /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      try {
        long l1 = packageInfo.getLongVersionCode();
        Object object = object2;
      } finally {
        packageInfo = null;
      } 
      object2 = object1;
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      throw packageInfo;
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      RemoteException remoteException = new RemoteException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Unable to find package: '");
      stringBuilder.append(paramString);
      stringBuilder.append("'");
      this(stringBuilder.toString());
      throw remoteException;
    } finally {}
    Binder.restoreCallingIdentity(l);
    throw paramString;
  }
  
  private void pullGraphicsStats(boolean paramBoolean, long paramLong) throws RemoteException {
    int i = Binder.getCallingUid();
    if (i != 1066) {
      StringWriter stringWriter = new StringWriter();
      FastPrintWriter fastPrintWriter = new FastPrintWriter(stringWriter);
      if (!DumpUtils.checkDumpAndUsageStatsPermission(this.mContext, "GraphicsStatsService", (PrintWriter)fastPrintWriter)) {
        fastPrintWriter.flush();
        throw new RemoteException(stringWriter.toString());
      } 
    } 
    long l = Binder.clearCallingIdentity();
    try {
      pullGraphicsStatsImpl(paramBoolean, paramLong);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  private void pullGraphicsStatsImpl(boolean paramBoolean, long paramLong) {
    // Byte code:
    //   0: iload_1
    //   1: ifeq -> 23
    //   4: aload_0
    //   5: invokestatic currentTimeMillis : ()J
    //   8: ldc2_w 86400000
    //   11: lsub
    //   12: invokespecial normalizeDate : (J)Ljava/util/Calendar;
    //   15: invokevirtual getTimeInMillis : ()J
    //   18: lstore #4
    //   20: goto -> 35
    //   23: aload_0
    //   24: invokestatic currentTimeMillis : ()J
    //   27: invokespecial normalizeDate : (J)Ljava/util/Calendar;
    //   30: invokevirtual getTimeInMillis : ()J
    //   33: lstore #4
    //   35: aload_0
    //   36: getfield mLock : Ljava/lang/Object;
    //   39: astore #6
    //   41: aload #6
    //   43: monitorenter
    //   44: new java/util/ArrayList
    //   47: astore #7
    //   49: aload #7
    //   51: aload_0
    //   52: getfield mActive : Ljava/util/ArrayList;
    //   55: invokevirtual size : ()I
    //   58: invokespecial <init> : (I)V
    //   61: iconst_0
    //   62: istore #8
    //   64: aload_0
    //   65: getfield mActive : Ljava/util/ArrayList;
    //   68: invokevirtual size : ()I
    //   71: istore #9
    //   73: iload #8
    //   75: iload #9
    //   77: if_icmpge -> 149
    //   80: aload_0
    //   81: getfield mActive : Ljava/util/ArrayList;
    //   84: iload #8
    //   86: invokevirtual get : (I)Ljava/lang/Object;
    //   89: checkcast android/graphics/GraphicsStatsService$ActiveBuffer
    //   92: astore #10
    //   94: aload #10
    //   96: getfield mInfo : Landroid/graphics/GraphicsStatsService$BufferInfo;
    //   99: getfield mStartTime : J
    //   102: lstore #11
    //   104: lload #11
    //   106: lload #4
    //   108: lcmp
    //   109: ifne -> 138
    //   112: new android/graphics/GraphicsStatsService$HistoricalBuffer
    //   115: astore #13
    //   117: aload #13
    //   119: aload_0
    //   120: aload #10
    //   122: invokespecial <init> : (Landroid/graphics/GraphicsStatsService;Landroid/graphics/GraphicsStatsService$ActiveBuffer;)V
    //   125: aload #7
    //   127: aload #13
    //   129: invokevirtual add : (Ljava/lang/Object;)Z
    //   132: pop
    //   133: goto -> 138
    //   136: astore #10
    //   138: iinc #8, 1
    //   141: goto -> 64
    //   144: astore #7
    //   146: goto -> 417
    //   149: aload #6
    //   151: monitorexit
    //   152: iconst_m1
    //   153: iconst_1
    //   154: invokestatic nCreateDump : (IZ)J
    //   157: lstore #14
    //   159: aload_0
    //   160: getfield mFileAccessLock : Ljava/lang/Object;
    //   163: astore #10
    //   165: aload #10
    //   167: monitorenter
    //   168: lload #4
    //   170: lstore #11
    //   172: aload_0
    //   173: lload #14
    //   175: aload #7
    //   177: invokespecial dumpActiveLocked : (JLjava/util/ArrayList;)Ljava/util/HashSet;
    //   180: astore #13
    //   182: lload #4
    //   184: lstore #11
    //   186: aload #7
    //   188: invokevirtual clear : ()V
    //   191: iconst_0
    //   192: istore #8
    //   194: lload #4
    //   196: lstore #11
    //   198: ldc_w '%d'
    //   201: iconst_1
    //   202: anewarray java/lang/Object
    //   205: dup
    //   206: iconst_0
    //   207: lload #4
    //   209: invokestatic valueOf : (J)Ljava/lang/Long;
    //   212: aastore
    //   213: invokestatic format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   216: astore #7
    //   218: lload #4
    //   220: lstore #11
    //   222: new java/io/File
    //   225: astore #6
    //   227: lload #4
    //   229: lstore #11
    //   231: aload #6
    //   233: aload_0
    //   234: getfield mGraphicsStatsDir : Ljava/io/File;
    //   237: aload #7
    //   239: invokespecial <init> : (Ljava/io/File;Ljava/lang/String;)V
    //   242: lload #4
    //   244: lstore #11
    //   246: aload #6
    //   248: invokevirtual exists : ()Z
    //   251: ifeq -> 374
    //   254: lload #4
    //   256: lstore #11
    //   258: aload #6
    //   260: invokevirtual listFiles : ()[Ljava/io/File;
    //   263: astore #16
    //   265: lload #4
    //   267: lstore #11
    //   269: aload #16
    //   271: arraylength
    //   272: istore #17
    //   274: iload #8
    //   276: iload #17
    //   278: if_icmpge -> 371
    //   281: aload #16
    //   283: iload #8
    //   285: aaload
    //   286: astore #6
    //   288: lload #4
    //   290: lstore #11
    //   292: aload #6
    //   294: invokevirtual listFiles : ()[Ljava/io/File;
    //   297: astore #6
    //   299: aload #6
    //   301: arraylength
    //   302: istore #9
    //   304: iconst_0
    //   305: istore #18
    //   307: iload #18
    //   309: iload #9
    //   311: if_icmpge -> 365
    //   314: aload #6
    //   316: iload #18
    //   318: aaload
    //   319: astore #19
    //   321: new java/io/File
    //   324: astore #20
    //   326: aload #20
    //   328: aload #19
    //   330: ldc_w 'total'
    //   333: invokespecial <init> : (Ljava/io/File;Ljava/lang/String;)V
    //   336: aload #13
    //   338: aload #20
    //   340: invokevirtual contains : (Ljava/lang/Object;)Z
    //   343: ifeq -> 349
    //   346: goto -> 359
    //   349: lload #14
    //   351: aload #20
    //   353: invokevirtual getAbsolutePath : ()Ljava/lang/String;
    //   356: invokestatic nAddToDump : (JLjava/lang/String;)V
    //   359: iinc #18, 1
    //   362: goto -> 307
    //   365: iinc #8, 1
    //   368: goto -> 274
    //   371: goto -> 374
    //   374: aload #10
    //   376: monitorexit
    //   377: lload #14
    //   379: lload_2
    //   380: iload_1
    //   381: invokestatic nFinishDumpInMemory : (JJZ)V
    //   384: return
    //   385: astore #7
    //   387: aload #10
    //   389: monitorexit
    //   390: aload #7
    //   392: athrow
    //   393: astore #7
    //   395: goto -> 405
    //   398: astore #7
    //   400: goto -> 387
    //   403: astore #7
    //   405: lload #14
    //   407: lload_2
    //   408: iload_1
    //   409: invokestatic nFinishDumpInMemory : (JJZ)V
    //   412: aload #7
    //   414: athrow
    //   415: astore #7
    //   417: aload #6
    //   419: monitorexit
    //   420: aload #7
    //   422: athrow
    //   423: astore #7
    //   425: goto -> 417
    // Line number table:
    //   Java source line number -> byte code offset
    //   #229	-> 0
    //   #231	-> 4
    //   #234	-> 23
    //   #239	-> 35
    //   #240	-> 44
    //   #241	-> 61
    //   #242	-> 80
    //   #243	-> 94
    //   #245	-> 112
    //   #248	-> 133
    //   #246	-> 136
    //   #241	-> 138
    //   #251	-> 144
    //   #255	-> 152
    //   #257	-> 159
    //   #258	-> 168
    //   #259	-> 182
    //   #260	-> 191
    //   #261	-> 218
    //   #262	-> 242
    //   #263	-> 254
    //   #264	-> 288
    //   #265	-> 321
    //   #266	-> 336
    //   #267	-> 346
    //   #269	-> 349
    //   #264	-> 359
    //   #263	-> 365
    //   #262	-> 374
    //   #273	-> 374
    //   #275	-> 377
    //   #276	-> 384
    //   #277	-> 384
    //   #273	-> 385
    //   #275	-> 393
    //   #273	-> 398
    //   #275	-> 403
    //   #276	-> 412
    //   #251	-> 415
    // Exception table:
    //   from	to	target	type
    //   44	61	415	finally
    //   64	73	415	finally
    //   80	94	144	finally
    //   94	104	144	finally
    //   112	133	136	java/io/IOException
    //   112	133	144	finally
    //   149	152	415	finally
    //   159	168	403	finally
    //   172	182	385	finally
    //   186	191	385	finally
    //   198	218	385	finally
    //   222	227	385	finally
    //   231	242	385	finally
    //   246	254	385	finally
    //   258	265	385	finally
    //   269	274	385	finally
    //   292	299	385	finally
    //   299	304	398	finally
    //   321	336	398	finally
    //   336	346	398	finally
    //   349	359	398	finally
    //   374	377	398	finally
    //   387	390	398	finally
    //   390	393	393	finally
    //   417	420	423	finally
  }
  
  private ParcelFileDescriptor requestBufferForProcessLocked(IGraphicsStatsCallback paramIGraphicsStatsCallback, int paramInt1, int paramInt2, String paramString, long paramLong) throws RemoteException {
    ActiveBuffer activeBuffer = fetchActiveBuffersLocked(paramIGraphicsStatsCallback, paramInt1, paramInt2, paramString, paramLong);
    scheduleRotateLocked();
    return activeBuffer.getPfd();
  }
  
  private Calendar normalizeDate(long paramLong) {
    Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    calendar.setTimeInMillis(paramLong);
    calendar.set(11, 0);
    calendar.set(12, 0);
    calendar.set(13, 0);
    calendar.set(14, 0);
    return calendar;
  }
  
  private File pathForApp(BufferInfo paramBufferInfo) {
    long l1 = paramBufferInfo.mStartTime;
    long l2 = normalizeDate(l1).getTimeInMillis();
    String str2 = paramBufferInfo.mPackageName;
    l1 = paramBufferInfo.mVersionCode;
    String str1 = String.format("%d/%s/%d/total", new Object[] { Long.valueOf(l2), str2, Long.valueOf(l1) });
    return new File(this.mGraphicsStatsDir, str1);
  }
  
  private void saveBuffer(HistoricalBuffer paramHistoricalBuffer) {
    if (Trace.isTagEnabled(524288L)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("saving graphicsstats for ");
      stringBuilder.append(paramHistoricalBuffer.mInfo.mPackageName);
      Trace.traceBegin(524288L, stringBuilder.toString());
    } 
    synchronized (this.mFileAccessLock) {
      StringBuilder stringBuilder;
      File file1 = pathForApp(paramHistoricalBuffer.mInfo);
      File file2 = file1.getParentFile();
      file2.mkdirs();
      if (!file2.exists()) {
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Unable to create path: '");
        stringBuilder.append(file2.getAbsolutePath());
        stringBuilder.append("'");
        Log.w("GraphicsStatsService", stringBuilder.toString());
        return;
      } 
      nSaveBuffer(file1.getAbsolutePath(), ((HistoricalBuffer)stringBuilder).mInfo.mPackageName, ((HistoricalBuffer)stringBuilder).mInfo.mVersionCode, ((HistoricalBuffer)stringBuilder).mInfo.mStartTime, ((HistoricalBuffer)stringBuilder).mInfo.mEndTime, ((HistoricalBuffer)stringBuilder).mData);
      Trace.traceEnd(524288L);
      return;
    } 
  }
  
  private void deleteRecursiveLocked(File paramFile) {
    if (paramFile.isDirectory())
      for (File file : paramFile.listFiles())
        deleteRecursiveLocked(file);  
    if (!paramFile.delete()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to delete '");
      stringBuilder.append(paramFile.getAbsolutePath());
      stringBuilder.append("'!");
      Log.w("GraphicsStatsService", stringBuilder.toString());
    } 
  }
  
  private void deleteOldBuffers() {
    Trace.traceBegin(524288L, "deleting old graphicsstats buffers");
    synchronized (this.mFileAccessLock) {
      File[] arrayOfFile = this.mGraphicsStatsDir.listFiles();
      if (arrayOfFile == null || arrayOfFile.length <= 3)
        return; 
      long[] arrayOfLong = new long[arrayOfFile.length];
      byte b = 0;
      while (true) {
        int i = arrayOfFile.length;
        if (b < i) {
          try {
            arrayOfLong[b] = Long.parseLong(arrayOfFile[b].getName());
          } catch (NumberFormatException numberFormatException) {}
          b++;
          continue;
        } 
        break;
      } 
      if (arrayOfLong.length <= 3)
        return; 
      Arrays.sort(arrayOfLong);
      for (b = 0; b < arrayOfLong.length - 3; b++) {
        File file = new File();
        this(this.mGraphicsStatsDir, Long.toString(arrayOfLong[b]));
        deleteRecursiveLocked(file);
      } 
      Trace.traceEnd(524288L);
      return;
    } 
  }
  
  private void addToSaveQueue(ActiveBuffer paramActiveBuffer) {
    try {
      HistoricalBuffer historicalBuffer = new HistoricalBuffer();
      this(this, paramActiveBuffer);
      Message.obtain(this.mWriteOutHandler, 1, historicalBuffer).sendToTarget();
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to copy graphicsstats from ");
      stringBuilder.append(paramActiveBuffer.mInfo.mPackageName);
      Log.w("GraphicsStatsService", stringBuilder.toString(), iOException);
    } 
    paramActiveBuffer.closeAllBuffers();
  }
  
  private void processDied(ActiveBuffer paramActiveBuffer) {
    synchronized (this.mLock) {
      this.mActive.remove(paramActiveBuffer);
      addToSaveQueue(paramActiveBuffer);
      return;
    } 
  }
  
  private ActiveBuffer fetchActiveBuffersLocked(IGraphicsStatsCallback paramIGraphicsStatsCallback, int paramInt1, int paramInt2, String paramString, long paramLong) throws RemoteException {
    int i = this.mActive.size();
    long l = normalizeDate(System.currentTimeMillis()).getTimeInMillis();
    for (byte b = 0; b < i; b++) {
      ActiveBuffer activeBuffer = this.mActive.get(b);
      if (activeBuffer.mPid == paramInt2 && activeBuffer.mUid == paramInt1) {
        if (activeBuffer.mInfo.mStartTime < l) {
          activeBuffer.binderDied();
          break;
        } 
        return activeBuffer;
      } 
    } 
    try {
      ActiveBuffer activeBuffer = new ActiveBuffer();
      this(this, paramIGraphicsStatsCallback, paramInt1, paramInt2, paramString, paramLong);
      this.mActive.add(activeBuffer);
      return activeBuffer;
    } catch (IOException iOException) {
      throw new RemoteException("Failed to allocate space");
    } 
  }
  
  private HashSet<File> dumpActiveLocked(long paramLong, ArrayList<HistoricalBuffer> paramArrayList) {
    HashSet<File> hashSet = new HashSet(paramArrayList.size());
    for (byte b = 0; b < paramArrayList.size(); b++) {
      HistoricalBuffer historicalBuffer = paramArrayList.get(b);
      File file = pathForApp(historicalBuffer.mInfo);
      hashSet.add(file);
      nAddToDump(paramLong, file.getAbsolutePath(), historicalBuffer.mInfo.mPackageName, historicalBuffer.mInfo.mVersionCode, historicalBuffer.mInfo.mStartTime, historicalBuffer.mInfo.mEndTime, historicalBuffer.mData);
    } 
    return hashSet;
  }
  
  private void dumpHistoricalLocked(long paramLong, HashSet<File> paramHashSet) {
    for (File file : this.mGraphicsStatsDir.listFiles()) {
      for (File file1 : file.listFiles()) {
        for (File file2 : file1.listFiles()) {
          file2 = new File(file2, "total");
          if (!paramHashSet.contains(file2))
            nAddToDump(paramLong, file2.getAbsolutePath()); 
        } 
      } 
    } 
  }
  
  protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    boolean bool2;
    if (!DumpUtils.checkDumpAndUsageStatsPermission(this.mContext, "GraphicsStatsService", paramPrintWriter))
      return; 
    boolean bool1 = false;
    int i = paramArrayOfString.length;
    byte b = 0;
    while (true) {
      bool2 = bool1;
      if (b < i) {
        String str = paramArrayOfString[b];
        if ("--proto".equals(str)) {
          bool2 = true;
          break;
        } 
        b++;
        continue;
      } 
      break;
    } 
    synchronized (this.mLock) {
      ArrayList<HistoricalBuffer> arrayList = new ArrayList();
      this(this.mActive.size());
      b = 0;
      while (true) {
        i = this.mActive.size();
        if (b < i) {
          try {
            HistoricalBuffer historicalBuffer = new HistoricalBuffer();
            this(this, this.mActive.get(b));
            arrayList.add(historicalBuffer);
          } catch (IOException iOException) {}
          b++;
          continue;
        } 
        break;
      } 
      long l = nCreateDump(paramFileDescriptor.getInt$(), bool2);
      try {
      
      } finally {
        nFinishDump(l);
      } 
    } 
  }
  
  protected void finalize() throws Throwable {
    nativeDestructor();
  }
  
  private static native void nAddToDump(long paramLong, String paramString);
  
  private static native void nAddToDump(long paramLong1, String paramString1, String paramString2, long paramLong2, long paramLong3, long paramLong4, byte[] paramArrayOfbyte);
  
  private static native long nCreateDump(int paramInt, boolean paramBoolean);
  
  private static native void nFinishDump(long paramLong);
  
  private static native void nFinishDumpInMemory(long paramLong1, long paramLong2, boolean paramBoolean);
  
  private static native int nGetAshmemSize();
  
  private static native void nSaveBuffer(String paramString1, String paramString2, long paramLong1, long paramLong2, long paramLong3, byte[] paramArrayOfbyte);
  
  private static native void nativeDestructor();
  
  private native void nativeInit();
  
  class BufferInfo {
    long mEndTime;
    
    final String mPackageName;
    
    long mStartTime;
    
    final long mVersionCode;
    
    final GraphicsStatsService this$0;
    
    BufferInfo(String param1String, long param1Long1, long param1Long2) {
      this.mPackageName = param1String;
      this.mVersionCode = param1Long1;
      this.mStartTime = param1Long2;
    }
  }
  
  class ActiveBuffer implements IBinder.DeathRecipient {
    final IGraphicsStatsCallback mCallback;
    
    final GraphicsStatsService.BufferInfo mInfo;
    
    ByteBuffer mMapping;
    
    final int mPid;
    
    SharedMemory mProcessBuffer;
    
    final IBinder mToken;
    
    final int mUid;
    
    final GraphicsStatsService this$0;
    
    ActiveBuffer(IGraphicsStatsCallback param1IGraphicsStatsCallback, int param1Int1, int param1Int2, String param1String, long param1Long) throws RemoteException, IOException {
      this.mInfo = new GraphicsStatsService.BufferInfo(param1String, param1Long, System.currentTimeMillis());
      this.mUid = param1Int1;
      this.mPid = param1Int2;
      this.mCallback = param1IGraphicsStatsCallback;
      IBinder iBinder = param1IGraphicsStatsCallback.asBinder();
      iBinder.linkToDeath(this, 0);
      try {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("GFXStats-");
        stringBuilder.append(param1Int2);
        SharedMemory sharedMemory = SharedMemory.create(stringBuilder.toString(), GraphicsStatsService.this.mAshmemSize);
        this.mMapping = sharedMemory.mapReadWrite();
      } catch (ErrnoException errnoException) {
        errnoException.rethrowAsIOException();
      } 
      this.mMapping.position(0);
      this.mMapping.put(GraphicsStatsService.this.mZeroData, 0, GraphicsStatsService.this.mAshmemSize);
    }
    
    public void binderDied() {
      this.mToken.unlinkToDeath(this, 0);
      GraphicsStatsService.this.processDied(this);
    }
    
    void closeAllBuffers() {
      ByteBuffer byteBuffer = this.mMapping;
      if (byteBuffer != null) {
        SharedMemory.unmap(byteBuffer);
        this.mMapping = null;
      } 
      SharedMemory sharedMemory = this.mProcessBuffer;
      if (sharedMemory != null) {
        sharedMemory.close();
        this.mProcessBuffer = null;
      } 
    }
    
    ParcelFileDescriptor getPfd() {
      try {
        return this.mProcessBuffer.getFdDup();
      } catch (IOException iOException) {
        throw new IllegalStateException("Failed to get PFD from memory file", iOException);
      } 
    }
    
    void readBytes(byte[] param1ArrayOfbyte, int param1Int) throws IOException {
      ByteBuffer byteBuffer = this.mMapping;
      if (byteBuffer != null) {
        byteBuffer.position(0);
        this.mMapping.get(param1ArrayOfbyte, 0, param1Int);
        return;
      } 
      throw new IOException("SharedMemory has been deactivated");
    }
  }
  
  class HistoricalBuffer {
    final byte[] mData = new byte[GraphicsStatsService.this.mAshmemSize];
    
    final GraphicsStatsService.BufferInfo mInfo;
    
    final GraphicsStatsService this$0;
    
    HistoricalBuffer(GraphicsStatsService.ActiveBuffer param1ActiveBuffer) throws IOException {
      GraphicsStatsService.BufferInfo bufferInfo = param1ActiveBuffer.mInfo;
      bufferInfo.mEndTime = System.currentTimeMillis();
      param1ActiveBuffer.readBytes(this.mData, GraphicsStatsService.this.mAshmemSize);
    }
  }
}
