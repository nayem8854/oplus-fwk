package android.graphics;

public class OplusDarkModeThirdPartyFilter extends ColorFilter {
  public static final int TYPE_MAKE_DARK_RANGE = 2;
  
  public static final int TYPE_MAKE_FULL_DARK = 3;
  
  public static final int TYPE_MAKE_FULL_LIGHT = 4;
  
  public static final int TYPE_MAKE_LIGHT_RANGE = 1;
  
  public static final int TYPE_UNKNOWN = 0;
  
  private float mHSLBgMaxL;
  
  private float mHSLFgMinL;
  
  private float mLABBgMaxL;
  
  private float mLABFgMinL;
  
  private int mType;
  
  public OplusDarkModeThirdPartyFilter(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    this.mType = paramInt;
    this.mHSLBgMaxL = paramFloat1;
    this.mHSLFgMinL = paramFloat2;
    this.mLABBgMaxL = paramFloat3;
    this.mLABFgMinL = paramFloat4;
  }
  
  public float getLABBgMaxL() {
    return this.mLABBgMaxL;
  }
  
  public float getLABFgMinL() {
    return this.mLABFgMinL;
  }
  
  public int getType() {
    return this.mType;
  }
  
  long createNativeInstance() {
    int i = this.mType;
    if (i != 1 && i != 2 && i != 3 && i != 4)
      return 0L; 
    return native_CreateDarkModeThirdPartyFilter(this.mType, this.mHSLBgMaxL, this.mHSLFgMinL);
  }
  
  private static native long native_CreateDarkModeThirdPartyFilter(int paramInt, float paramFloat1, float paramFloat2);
}
