package android.graphics;

import android.os.Handler;
import android.os.Looper;
import android.view.Surface;
import java.lang.ref.WeakReference;

public class SurfaceTexture {
  private final Looper mCreatorLooper;
  
  private long mFrameAvailableListener;
  
  private boolean mIsSingleBuffered;
  
  private Handler mOnFrameAvailableHandler;
  
  private long mProducer;
  
  private long mSurfaceTexture;
  
  public static interface OnFrameAvailableListener {
    void onFrameAvailable(SurfaceTexture param1SurfaceTexture);
  }
  
  @Deprecated
  public static class OutOfResourcesException extends Exception {
    public OutOfResourcesException() {}
    
    public OutOfResourcesException(String param1String) {
      super(param1String);
    }
  }
  
  public SurfaceTexture(int paramInt) {
    this(paramInt, false);
  }
  
  public SurfaceTexture(int paramInt, boolean paramBoolean) {
    this.mCreatorLooper = Looper.myLooper();
    this.mIsSingleBuffered = paramBoolean;
    nativeInit(false, paramInt, paramBoolean, new WeakReference<>(this));
  }
  
  public SurfaceTexture(boolean paramBoolean) {
    this.mCreatorLooper = Looper.myLooper();
    this.mIsSingleBuffered = paramBoolean;
    nativeInit(true, 0, paramBoolean, new WeakReference<>(this));
  }
  
  public void setOnFrameAvailableListener(OnFrameAvailableListener paramOnFrameAvailableListener) {
    setOnFrameAvailableListener(paramOnFrameAvailableListener, null);
  }
  
  public void setOnFrameAvailableListener(OnFrameAvailableListener paramOnFrameAvailableListener, Handler paramHandler) {
    if (paramOnFrameAvailableListener != null) {
      Looper looper;
      if (paramHandler != null) {
        looper = paramHandler.getLooper();
      } else {
        looper = this.mCreatorLooper;
        if (looper == null)
          looper = Looper.getMainLooper(); 
      } 
      this.mOnFrameAvailableHandler = (Handler)new Object(this, looper, null, true, paramOnFrameAvailableListener);
    } else {
      this.mOnFrameAvailableHandler = null;
    } 
  }
  
  public void setDefaultBufferSize(int paramInt1, int paramInt2) {
    nativeSetDefaultBufferSize(paramInt1, paramInt2);
  }
  
  public void updateTexImage() {
    nativeUpdateTexImage();
  }
  
  public void releaseTexImage() {
    nativeReleaseTexImage();
  }
  
  public void detachFromGLContext() {
    int i = nativeDetachFromGLContext();
    if (i == 0)
      return; 
    throw new RuntimeException("Error during detachFromGLContext (see logcat for details)");
  }
  
  public void attachToGLContext(int paramInt) {
    paramInt = nativeAttachToGLContext(paramInt);
    if (paramInt == 0)
      return; 
    throw new RuntimeException("Error during attachToGLContext (see logcat for details)");
  }
  
  public void getTransformMatrix(float[] paramArrayOffloat) {
    if (paramArrayOffloat.length == 16) {
      nativeGetTransformMatrix(paramArrayOffloat);
      return;
    } 
    throw new IllegalArgumentException();
  }
  
  public long getTimestamp() {
    return nativeGetTimestamp();
  }
  
  public void release() {
    nativeRelease();
  }
  
  public boolean isReleased() {
    return nativeIsReleased();
  }
  
  protected void finalize() throws Throwable {
    try {
      nativeFinalize();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  private static void postEventFromNative(WeakReference<SurfaceTexture> paramWeakReference) {
    if (paramWeakReference != null) {
      SurfaceTexture surfaceTexture = paramWeakReference.get();
    } else {
      paramWeakReference = null;
    } 
    if (paramWeakReference != null) {
      Handler handler = ((SurfaceTexture)paramWeakReference).mOnFrameAvailableHandler;
      if (handler != null)
        handler.sendEmptyMessage(0); 
    } 
  }
  
  public boolean isSingleBuffered() {
    return this.mIsSingleBuffered;
  }
  
  private native int nativeAttachToGLContext(int paramInt);
  
  private native int nativeDetachFromGLContext();
  
  private native void nativeFinalize();
  
  private native long nativeGetTimestamp();
  
  private native void nativeGetTransformMatrix(float[] paramArrayOffloat);
  
  private native void nativeInit(boolean paramBoolean1, int paramInt, boolean paramBoolean2, WeakReference<SurfaceTexture> paramWeakReference) throws Surface.OutOfResourcesException;
  
  private native boolean nativeIsReleased();
  
  private native void nativeRelease();
  
  private native void nativeReleaseTexImage();
  
  private native void nativeSetDefaultBufferSize(int paramInt1, int paramInt2);
  
  private native void nativeUpdateTexImage();
}
