package android.graphics;

import android.app.ActivityThread;
import android.common.OplusFrameworkFactory;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Trace;
import android.util.Log;
import android.util.TypedValue;
import com.oplus.benchmark.OplusBenchHelper;
import com.oplus.drmDecoder.IOplusDrmDecoderFeature;
import com.oplus.orms.OplusResourceManager;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class BitmapFactory extends OplusBaseBitmapFactory {
  private static final int DECODE_BUFFER_SIZE = 16384;
  
  private static IOplusDrmDecoderFeature mOplusDrmDecoderFeature = OplusFrameworkFactory.getInstance().<IOplusDrmDecoderFeature>getFeature(IOplusDrmDecoderFeature.DEFAULT, new Object[0]);
  
  private static OplusResourceManager mOrmsManager = null;
  
  class Options {
    public int outWidth;
    
    public String outMimeType;
    
    public int outHeight;
    
    public Bitmap.Config outConfig;
    
    public ColorSpace outColorSpace;
    
    @Deprecated
    public boolean mCancel;
    
    public byte[] inTempStorage;
    
    public int inTargetDensity;
    
    public int inScreenDensity;
    
    public boolean inScaled = true;
    
    public int inSampleSize;
    
    @Deprecated
    public boolean inPurgeable;
    
    public boolean inPremultiplied = true;
    
    public Bitmap.Config inPreferredConfig = Bitmap.Config.ARGB_8888;
    
    public ColorSpace inPreferredColorSpace = null;
    
    @Deprecated
    public boolean inPreferQualityOverSpeed;
    
    public boolean inMutable;
    
    public boolean inJustDecodeBounds;
    
    @Deprecated
    public boolean inInputShareable;
    
    public boolean inDither;
    
    public int inDensity;
    
    public Bitmap inBitmap;
    
    @Deprecated
    public void requestCancelDecode() {
      this.mCancel = true;
    }
    
    static void validate(Options param1Options) {
      if (param1Options == null)
        return; 
      Bitmap bitmap = param1Options.inBitmap;
      if (bitmap != null)
        if (bitmap.getConfig() != Bitmap.Config.HARDWARE) {
          if (param1Options.inBitmap.isRecycled())
            throw new IllegalArgumentException("Cannot reuse a recycled Bitmap"); 
        } else {
          throw new IllegalArgumentException("Bitmaps with Config.HARDWARE are always immutable");
        }  
      if (!param1Options.inMutable || param1Options.inPreferredConfig != Bitmap.Config.HARDWARE) {
        ColorSpace colorSpace = param1Options.inPreferredColorSpace;
        if (colorSpace != null)
          if (colorSpace instanceof ColorSpace.Rgb) {
            if (((ColorSpace.Rgb)colorSpace).getTransferParameters() == null)
              throw new IllegalArgumentException("The destination color space must use an ICC parametric transfer function"); 
          } else {
            throw new IllegalArgumentException("The destination color space must use the RGB color model");
          }  
        return;
      } 
      throw new IllegalArgumentException("Bitmaps with Config.HARDWARE cannot be decoded into - they are immutable");
    }
    
    static long nativeInBitmap(Options param1Options) {
      if (param1Options != null) {
        Bitmap bitmap = param1Options.inBitmap;
        if (bitmap != null)
          return bitmap.getNativeInstance(); 
      } 
      return 0L;
    }
    
    static long nativeColorSpace(Options param1Options) {
      if (param1Options != null) {
        ColorSpace colorSpace = param1Options.inPreferredColorSpace;
        if (colorSpace != null)
          return colorSpace.getNativeInstance(); 
      } 
      return 0L;
    }
  }
  
  public static Bitmap decodeFile(String paramString, Options paramOptions) {
    Bitmap bitmap;
    Options.validate(paramOptions);
    Exception exception = null;
    StringBuilder stringBuilder1 = null, stringBuilder2 = null;
    FileInputStream fileInputStream1 = null, fileInputStream2 = null;
    FileInputStream fileInputStream3 = fileInputStream2, fileInputStream4 = fileInputStream1;
    try {
      FileInputStream fileInputStream6 = new FileInputStream();
      fileInputStream3 = fileInputStream2;
      fileInputStream4 = fileInputStream1;
      this(paramString);
      FileInputStream fileInputStream5 = fileInputStream6;
      fileInputStream3 = fileInputStream5;
      fileInputStream4 = fileInputStream5;
      Bitmap bitmap1 = decodeStream(fileInputStream5, null, paramOptions);
      bitmap1 = bitmap;
      try {
        fileInputStream5.close();
        Bitmap bitmap2 = bitmap;
      } catch (IOException iOException) {
        Bitmap bitmap2 = bitmap1;
      } 
    } catch (Exception exception1) {
      Bitmap bitmap1 = bitmap;
      StringBuilder stringBuilder = new StringBuilder();
      bitmap1 = bitmap;
      this();
      bitmap1 = bitmap;
      stringBuilder.append("Unable to decode stream: ");
      bitmap1 = bitmap;
      stringBuilder.append(exception1);
      bitmap1 = bitmap;
      Log.e("BitmapFactory", stringBuilder.toString());
      stringBuilder = stringBuilder1;
      if (bitmap != null) {
        exception1 = exception;
        bitmap.close();
        stringBuilder = stringBuilder2;
      } 
    } finally {}
    return (Bitmap)paramString;
  }
  
  public static Bitmap decodeFile(String paramString) {
    return decodeFile(paramString, null);
  }
  
  public static Bitmap decodeResourceStream(Resources paramResources, TypedValue paramTypedValue, InputStream paramInputStream, Rect paramRect, Options paramOptions) {
    Options.validate(paramOptions);
    Options options = paramOptions;
    if (paramOptions == null)
      options = new Options(); 
    if (options.inDensity == 0 && paramTypedValue != null) {
      int i = paramTypedValue.density;
      if (i == 0) {
        options.inDensity = 160;
      } else if (i != 65535) {
        options.inDensity = i;
      } 
    } 
    if (options.inTargetDensity == 0 && paramResources != null)
      options.inTargetDensity = (paramResources.getDisplayMetrics()).densityDpi; 
    return decodeStream(paramInputStream, paramRect, options);
  }
  
  public static Bitmap decodeResource(Resources paramResources, int paramInt, Options paramOptions) {
    Options.validate(paramOptions);
    Bitmap bitmap1 = null;
    InputStream inputStream1 = null, inputStream2 = null;
    InputStream inputStream3 = inputStream2;
    Bitmap bitmap2 = bitmap1;
    InputStream inputStream4 = inputStream1;
    try {
      TypedValue typedValue = new TypedValue();
      inputStream3 = inputStream2;
      bitmap2 = bitmap1;
      inputStream4 = inputStream1;
      this();
      inputStream3 = inputStream2;
      bitmap2 = bitmap1;
      inputStream4 = inputStream1;
      inputStream2 = paramResources.openRawResource(paramInt, typedValue);
      inputStream3 = inputStream2;
      bitmap2 = bitmap1;
      inputStream4 = inputStream2;
      bitmap1 = OplusBenchHelper.getInstance().getBitmapCache(paramResources, paramInt, paramOptions);
      bitmap2 = bitmap1;
      if (bitmap1 == null) {
        inputStream3 = inputStream2;
        bitmap2 = bitmap1;
        inputStream4 = inputStream2;
        bitmap2 = bitmap1 = decodeResourceStream(paramResources, typedValue, inputStream2, null, paramOptions);
      } 
      Bitmap bitmap = bitmap2;
    } catch (Exception exception) {
      exception = iOException2;
    } finally {
      if (exception != null)
        try {
          exception.close();
        } catch (IOException iOException1) {} 
    } 
    if (exception != null || iOException1 == null || ((Options)iOException1).inBitmap == null) {
      OplusBenchHelper.getInstance().setBitmapCache((Bitmap)exception, paramResources, paramInt);
      if (exception == null) {
        String str = ActivityThread.currentPackageName();
        if (str != null && str.equals("com.omichsoft.gallery")) {
          Bitmap bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ALPHA_8);
          if (bitmap != null) {
            byte[] arrayOfByte = new byte[32];
            bitmap.setNinePatchChunk(arrayOfByte);
            return bitmap;
          } 
        } 
      } 
      return (Bitmap)exception;
    } 
    throw new IllegalArgumentException("Problem decoding into existing bitmap");
  }
  
  public static Bitmap decodeResource(Resources paramResources, int paramInt) {
    return decodeResource(paramResources, paramInt, null);
  }
  
  public static Bitmap decodeByteArray(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, Options paramOptions) {
    // Byte code:
    //   0: iload_1
    //   1: iload_2
    //   2: ior
    //   3: iflt -> 183
    //   6: aload_0
    //   7: arraylength
    //   8: iload_1
    //   9: iload_2
    //   10: iadd
    //   11: if_icmplt -> 183
    //   14: aload_3
    //   15: invokestatic validate : (Landroid/graphics/BitmapFactory$Options;)V
    //   18: ldc2_w 2
    //   21: ldc 'decodeBitmap'
    //   23: invokestatic traceBegin : (JLjava/lang/String;)V
    //   26: getstatic android/graphics/BitmapFactory.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   29: ifnonnull -> 49
    //   32: ldc android/graphics/BitmapFactory
    //   34: invokestatic getInstance : (Ljava/lang/Class;)Lcom/oplus/orms/OplusResourceManager;
    //   37: astore #4
    //   39: aload #4
    //   41: putstatic android/graphics/BitmapFactory.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   44: aload #4
    //   46: ifnull -> 88
    //   49: aload_3
    //   50: ifnull -> 88
    //   53: aload_3
    //   54: getfield inJustDecodeBounds : Z
    //   57: ifne -> 88
    //   60: iload_2
    //   61: ldc 524288
    //   63: if_icmple -> 88
    //   66: getstatic android/graphics/BitmapFactory.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   69: new com/oplus/orms/info/OrmsSaParam
    //   72: dup
    //   73: ldc 'ORMS_SYSTEM_SCENE_BITMAP'
    //   75: ldc 'ORMS_ACTION_DECODE'
    //   77: iload_2
    //   78: bipush #13
    //   80: ishr
    //   81: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;I)V
    //   84: invokevirtual ormsSetSceneAction : (Lcom/oplus/orms/info/OrmsSaParam;)J
    //   87: pop2
    //   88: aload_3
    //   89: invokestatic nativeInBitmap : (Landroid/graphics/BitmapFactory$Options;)J
    //   92: lstore #5
    //   94: aload_3
    //   95: invokestatic nativeColorSpace : (Landroid/graphics/BitmapFactory$Options;)J
    //   98: lstore #7
    //   100: aload_0
    //   101: iload_1
    //   102: iload_2
    //   103: aload_3
    //   104: lload #5
    //   106: lload #7
    //   108: invokestatic nativeDecodeByteArray : ([BIILandroid/graphics/BitmapFactory$Options;JJ)Landroid/graphics/Bitmap;
    //   111: astore_0
    //   112: aload_0
    //   113: ifnonnull -> 142
    //   116: aload_3
    //   117: ifnull -> 142
    //   120: aload_3
    //   121: getfield inBitmap : Landroid/graphics/Bitmap;
    //   124: ifnonnull -> 130
    //   127: goto -> 142
    //   130: new java/lang/IllegalArgumentException
    //   133: astore_0
    //   134: aload_0
    //   135: ldc 'Problem decoding into existing bitmap'
    //   137: invokespecial <init> : (Ljava/lang/String;)V
    //   140: aload_0
    //   141: athrow
    //   142: aload_0
    //   143: aload_3
    //   144: invokestatic setDensityFromOptions : (Landroid/graphics/Bitmap;Landroid/graphics/BitmapFactory$Options;)V
    //   147: ldc2_w 2
    //   150: invokestatic traceEnd : (J)V
    //   153: aload_0
    //   154: ifnonnull -> 172
    //   157: getstatic android/graphics/BitmapFactory.mOplusDrmDecoderFeature : Lcom/oplus/drmDecoder/IOplusDrmDecoderFeature;
    //   160: astore_3
    //   161: aload_3
    //   162: ifnull -> 172
    //   165: aload_3
    //   166: invokeinterface isDrmLoop : ()Z
    //   171: pop
    //   172: aload_0
    //   173: areturn
    //   174: astore_0
    //   175: ldc2_w 2
    //   178: invokestatic traceEnd : (J)V
    //   181: aload_0
    //   182: athrow
    //   183: new java/lang/ArrayIndexOutOfBoundsException
    //   186: dup
    //   187: invokespecial <init> : ()V
    //   190: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #718	-> 0
    //   #721	-> 14
    //   #725	-> 18
    //   #728	-> 26
    //   #729	-> 49
    //   #730	-> 66
    //   #736	-> 88
    //   #737	-> 88
    //   #738	-> 94
    //   #736	-> 100
    //   #740	-> 112
    //   #741	-> 130
    //   #743	-> 142
    //   #745	-> 147
    //   #746	-> 153
    //   #750	-> 153
    //   #752	-> 165
    //   #757	-> 172
    //   #745	-> 174
    //   #746	-> 181
    //   #718	-> 183
    //   #719	-> 183
    // Exception table:
    //   from	to	target	type
    //   88	94	174	finally
    //   94	100	174	finally
    //   100	112	174	finally
    //   120	127	174	finally
    //   130	142	174	finally
    //   142	147	174	finally
  }
  
  public static Bitmap decodeByteArray(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    return decodeByteArray(paramArrayOfbyte, paramInt1, paramInt2, null);
  }
  
  private static void setDensityFromOptions(Bitmap paramBitmap, Options paramOptions) {
    if (paramBitmap == null || paramOptions == null)
      return; 
    int i = paramOptions.inDensity;
    if (i != 0) {
      paramBitmap.setDensity(i);
      int j = paramOptions.inTargetDensity;
      if (j == 0 || i == j || i == paramOptions.inScreenDensity)
        return; 
      byte[] arrayOfByte = paramBitmap.getNinePatchChunk();
      if (arrayOfByte != null && NinePatch.isNinePatchChunk(arrayOfByte)) {
        i = 1;
      } else {
        i = 0;
      } 
      if (paramOptions.inScaled || i != 0)
        paramBitmap.setDensity(j); 
    } else if (paramOptions.inBitmap != null) {
      paramBitmap.setDensity(Bitmap.getDefaultDensity());
    } 
  }
  
  public static Bitmap decodeStream(InputStream paramInputStream, Rect paramRect, Options paramOptions) {
    if (paramInputStream == null)
      return null; 
    Options.validate(paramOptions);
    Trace.traceBegin(2L, "decodeBitmap");
    try {
      Bitmap bitmap;
      if (paramInputStream instanceof AssetManager.AssetInputStream) {
        long l1 = ((AssetManager.AssetInputStream)paramInputStream).getNativeAsset();
        long l2 = Options.nativeInBitmap(paramOptions);
        long l3 = Options.nativeColorSpace(paramOptions);
        bitmap = nativeDecodeAsset(l1, paramRect, paramOptions, l2, l3);
      } else {
        bitmap = decodeStreamInternal((InputStream)bitmap, paramRect, paramOptions);
      } 
      if (bitmap != null || paramOptions == null || paramOptions.inBitmap == null) {
        setDensityFromOptions(bitmap, paramOptions);
        return bitmap;
      } 
      IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
      this("Problem decoding into existing bitmap");
      throw illegalArgumentException;
    } finally {
      Trace.traceEnd(2L);
    } 
  }
  
  private static Bitmap decodeStreamInternal(InputStream paramInputStream, Rect paramRect, Options paramOptions) {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aload_2
    //   3: ifnull -> 11
    //   6: aload_2
    //   7: getfield inTempStorage : [B
    //   10: astore_3
    //   11: aload_3
    //   12: astore #4
    //   14: aload_3
    //   15: ifnonnull -> 25
    //   18: sipush #16384
    //   21: newarray byte
    //   23: astore #4
    //   25: aload_0
    //   26: invokevirtual available : ()I
    //   29: istore #5
    //   31: goto -> 47
    //   34: astore_3
    //   35: ldc 'BitmapFactory'
    //   37: ldc_w 'decodeStreamInternal is.available err!'
    //   40: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   43: pop
    //   44: iconst_0
    //   45: istore #5
    //   47: getstatic android/graphics/BitmapFactory.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   50: ifnonnull -> 67
    //   53: ldc android/graphics/BitmapFactory
    //   55: invokestatic getInstance : (Ljava/lang/Class;)Lcom/oplus/orms/OplusResourceManager;
    //   58: astore_3
    //   59: aload_3
    //   60: putstatic android/graphics/BitmapFactory.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   63: aload_3
    //   64: ifnull -> 108
    //   67: aload_2
    //   68: ifnull -> 108
    //   71: aload_2
    //   72: getfield inJustDecodeBounds : Z
    //   75: ifne -> 108
    //   78: iload #5
    //   80: ldc 524288
    //   82: if_icmple -> 108
    //   85: getstatic android/graphics/BitmapFactory.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   88: new com/oplus/orms/info/OrmsSaParam
    //   91: dup
    //   92: ldc 'ORMS_SYSTEM_SCENE_BITMAP'
    //   94: ldc 'ORMS_ACTION_DECODE'
    //   96: iload #5
    //   98: bipush #13
    //   100: ishr
    //   101: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;I)V
    //   104: invokevirtual ormsSetSceneAction : (Lcom/oplus/orms/info/OrmsSaParam;)J
    //   107: pop2
    //   108: aload_2
    //   109: invokestatic nativeInBitmap : (Landroid/graphics/BitmapFactory$Options;)J
    //   112: lstore #6
    //   114: aload_2
    //   115: invokestatic nativeColorSpace : (Landroid/graphics/BitmapFactory$Options;)J
    //   118: lstore #8
    //   120: aload_0
    //   121: aload #4
    //   123: aload_1
    //   124: aload_2
    //   125: lload #6
    //   127: lload #8
    //   129: invokestatic nativeDecodeStream : (Ljava/io/InputStream;[BLandroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;JJ)Landroid/graphics/Bitmap;
    //   132: astore_3
    //   133: aload_3
    //   134: astore_1
    //   135: aload_3
    //   136: ifnonnull -> 176
    //   139: getstatic android/graphics/BitmapFactory.mOplusDrmDecoderFeature : Lcom/oplus/drmDecoder/IOplusDrmDecoderFeature;
    //   142: astore #10
    //   144: aload_3
    //   145: astore_1
    //   146: aload #10
    //   148: ifnull -> 176
    //   151: aload_3
    //   152: astore_1
    //   153: aload #10
    //   155: invokeinterface isDrmLoop : ()Z
    //   160: ifne -> 176
    //   163: getstatic android/graphics/BitmapFactory.mOplusDrmDecoderFeature : Lcom/oplus/drmDecoder/IOplusDrmDecoderFeature;
    //   166: aload #4
    //   168: aload_0
    //   169: aload_2
    //   170: invokeinterface decodeDrmImageIfNeededImpl : ([BLjava/io/InputStream;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   175: astore_1
    //   176: aload_1
    //   177: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #867	-> 0
    //   #868	-> 2
    //   #869	-> 11
    //   #872	-> 25
    //   #874	-> 25
    //   #878	-> 31
    //   #875	-> 34
    //   #876	-> 35
    //   #877	-> 35
    //   #879	-> 47
    //   #880	-> 67
    //   #881	-> 85
    //   #893	-> 108
    //   #894	-> 108
    //   #895	-> 114
    //   #893	-> 120
    //   #896	-> 133
    //   #898	-> 151
    //   #899	-> 163
    //   #901	-> 176
    // Exception table:
    //   from	to	target	type
    //   25	31	34	java/io/IOException
  }
  
  public static Bitmap decodeStream(InputStream paramInputStream) {
    return decodeStream(paramInputStream, null, null);
  }
  
  public static Bitmap decodeFileDescriptor(FileDescriptor paramFileDescriptor, Rect paramRect, Options paramOptions) {
    // Byte code:
    //   0: aload_2
    //   1: invokestatic validate : (Landroid/graphics/BitmapFactory$Options;)V
    //   4: ldc2_w 2
    //   7: ldc 'decodeFileDescriptor'
    //   9: invokestatic traceBegin : (JLjava/lang/String;)V
    //   12: aload_0
    //   13: invokestatic nativeIsSeekable : (Ljava/io/FileDescriptor;)Z
    //   16: ifeq -> 43
    //   19: aload_2
    //   20: invokestatic nativeInBitmap : (Landroid/graphics/BitmapFactory$Options;)J
    //   23: lstore_3
    //   24: aload_2
    //   25: invokestatic nativeColorSpace : (Landroid/graphics/BitmapFactory$Options;)J
    //   28: lstore #5
    //   30: aload_0
    //   31: aload_1
    //   32: aload_2
    //   33: lload_3
    //   34: lload #5
    //   36: invokestatic nativeDecodeFileDescriptor : (Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;JJ)Landroid/graphics/Bitmap;
    //   39: astore_1
    //   40: goto -> 72
    //   43: new java/io/FileInputStream
    //   46: astore #7
    //   48: aload #7
    //   50: aload_0
    //   51: invokespecial <init> : (Ljava/io/FileDescriptor;)V
    //   54: aload #7
    //   56: aload_1
    //   57: aload_2
    //   58: invokestatic decodeStreamInternal : (Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   61: astore_1
    //   62: aload #7
    //   64: invokevirtual close : ()V
    //   67: goto -> 72
    //   70: astore #7
    //   72: aload_1
    //   73: ifnonnull -> 102
    //   76: aload_2
    //   77: ifnull -> 102
    //   80: aload_2
    //   81: getfield inBitmap : Landroid/graphics/Bitmap;
    //   84: ifnonnull -> 90
    //   87: goto -> 102
    //   90: new java/lang/IllegalArgumentException
    //   93: astore_0
    //   94: aload_0
    //   95: ldc 'Problem decoding into existing bitmap'
    //   97: invokespecial <init> : (Ljava/lang/String;)V
    //   100: aload_0
    //   101: athrow
    //   102: aload_1
    //   103: aload_2
    //   104: invokestatic setDensityFromOptions : (Landroid/graphics/Bitmap;Landroid/graphics/BitmapFactory$Options;)V
    //   107: ldc2_w 2
    //   110: invokestatic traceEnd : (J)V
    //   113: aload_1
    //   114: astore #7
    //   116: aload_1
    //   117: ifnonnull -> 158
    //   120: getstatic android/graphics/BitmapFactory.mOplusDrmDecoderFeature : Lcom/oplus/drmDecoder/IOplusDrmDecoderFeature;
    //   123: astore #8
    //   125: aload_1
    //   126: astore #7
    //   128: aload #8
    //   130: ifnull -> 158
    //   133: aload_1
    //   134: astore #7
    //   136: aload #8
    //   138: invokeinterface isDrmLoop : ()Z
    //   143: ifne -> 158
    //   146: getstatic android/graphics/BitmapFactory.mOplusDrmDecoderFeature : Lcom/oplus/drmDecoder/IOplusDrmDecoderFeature;
    //   149: aload_0
    //   150: aload_2
    //   151: invokeinterface decodeDrmImageIfNeededImpl : (Ljava/io/FileDescriptor;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   156: astore #7
    //   158: aload #7
    //   160: areturn
    //   161: astore_0
    //   162: aload #7
    //   164: invokevirtual close : ()V
    //   167: goto -> 174
    //   170: astore_1
    //   171: goto -> 167
    //   174: aload_0
    //   175: athrow
    //   176: astore_0
    //   177: ldc2_w 2
    //   180: invokestatic traceEnd : (J)V
    //   183: aload_0
    //   184: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #939	-> 0
    //   #942	-> 4
    //   #944	-> 12
    //   #945	-> 19
    //   #946	-> 19
    //   #947	-> 24
    //   #945	-> 30
    //   #949	-> 43
    //   #951	-> 54
    //   #954	-> 62
    //   #955	-> 67
    //   #956	-> 72
    //   #959	-> 72
    //   #960	-> 90
    //   #963	-> 102
    //   #965	-> 107
    //   #966	-> 113
    //   #970	-> 113
    //   #972	-> 133
    //   #973	-> 146
    //   #976	-> 158
    //   #953	-> 161
    //   #954	-> 162
    //   #955	-> 167
    //   #956	-> 174
    //   #965	-> 176
    //   #966	-> 183
    // Exception table:
    //   from	to	target	type
    //   12	19	176	finally
    //   19	24	176	finally
    //   24	30	176	finally
    //   30	40	176	finally
    //   43	54	176	finally
    //   54	62	161	finally
    //   62	67	70	finally
    //   80	87	176	finally
    //   90	102	176	finally
    //   102	107	176	finally
    //   162	167	170	finally
    //   174	176	176	finally
  }
  
  public static Bitmap decodeFileDescriptor(FileDescriptor paramFileDescriptor) {
    return decodeFileDescriptor(paramFileDescriptor, null, null);
  }
  
  private static native Bitmap nativeDecodeAsset(long paramLong1, Rect paramRect, Options paramOptions, long paramLong2, long paramLong3);
  
  private static native Bitmap nativeDecodeByteArray(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, Options paramOptions, long paramLong1, long paramLong2);
  
  private static native Bitmap nativeDecodeFileDescriptor(FileDescriptor paramFileDescriptor, Rect paramRect, Options paramOptions, long paramLong1, long paramLong2);
  
  private static native Bitmap nativeDecodeStream(InputStream paramInputStream, byte[] paramArrayOfbyte, Rect paramRect, Options paramOptions, long paramLong1, long paramLong2);
  
  private static native boolean nativeIsSeekable(FileDescriptor paramFileDescriptor);
}
