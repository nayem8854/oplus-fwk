package android.graphics;

import java.io.OutputStream;

public class YuvImage {
  private static final int WORKING_COMPRESS_STORAGE = 4096;
  
  private byte[] mData;
  
  private int mFormat;
  
  private int mHeight;
  
  private int[] mStrides;
  
  private int mWidth;
  
  public YuvImage(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3, int[] paramArrayOfint) {
    if (paramInt1 == 17 || paramInt1 == 20) {
      if (paramInt2 > 0 && paramInt3 > 0) {
        if (paramArrayOfbyte != null) {
          if (paramArrayOfint == null) {
            this.mStrides = calculateStrides(paramInt2, paramInt1);
          } else {
            this.mStrides = paramArrayOfint;
          } 
          this.mData = paramArrayOfbyte;
          this.mFormat = paramInt1;
          this.mWidth = paramInt2;
          this.mHeight = paramInt3;
          return;
        } 
        throw new IllegalArgumentException("yuv cannot be null");
      } 
      throw new IllegalArgumentException("width and height must large than 0");
    } 
    throw new IllegalArgumentException("only support ImageFormat.NV21 and ImageFormat.YUY2 for now");
  }
  
  public boolean compressToJpeg(Rect paramRect, int paramInt, OutputStream paramOutputStream) {
    Rect rect = new Rect(0, 0, this.mWidth, this.mHeight);
    if (rect.contains(paramRect)) {
      if (paramInt >= 0 && paramInt <= 100) {
        if (paramOutputStream != null) {
          adjustRectangle(paramRect);
          int[] arrayOfInt2 = calculateOffsets(paramRect.left, paramRect.top);
          byte[] arrayOfByte1 = this.mData;
          int i = this.mFormat, j = paramRect.width();
          int k = paramRect.height(), arrayOfInt1[] = this.mStrides;
          byte[] arrayOfByte2 = new byte[4096];
          return nativeCompressToJpeg(arrayOfByte1, i, j, k, arrayOfInt2, arrayOfInt1, paramInt, paramOutputStream, arrayOfByte2);
        } 
        throw new IllegalArgumentException("stream cannot be null");
      } 
      throw new IllegalArgumentException("quality must be 0..100");
    } 
    throw new IllegalArgumentException("rectangle is not inside the image");
  }
  
  public byte[] getYuvData() {
    return this.mData;
  }
  
  public int getYuvFormat() {
    return this.mFormat;
  }
  
  public int[] getStrides() {
    return this.mStrides;
  }
  
  public int getWidth() {
    return this.mWidth;
  }
  
  public int getHeight() {
    return this.mHeight;
  }
  
  int[] calculateOffsets(int paramInt1, int paramInt2) {
    int i = this.mFormat;
    if (i == 17) {
      int[] arrayOfInt = this.mStrides;
      i = arrayOfInt[0];
      int j = this.mHeight, k = arrayOfInt[0], m = paramInt2 / 2, n = arrayOfInt[1], i1 = paramInt1 / 2;
      return new int[] { i * paramInt2 + paramInt1, j * k + m * n + i1 * 2 };
    } 
    if (i == 20) {
      i = this.mStrides[0];
      paramInt1 /= 2;
      return new int[] { i * paramInt2 + paramInt1 * 4 };
    } 
    return null;
  }
  
  private int[] calculateStrides(int paramInt1, int paramInt2) {
    if (paramInt2 == 17)
      return new int[] { paramInt1, paramInt1 }; 
    if (paramInt2 == 20)
      return new int[] { paramInt1 * 2 }; 
    return null;
  }
  
  private void adjustRectangle(Rect paramRect) {
    int i = paramRect.width();
    int j = paramRect.height();
    int k = i;
    if (this.mFormat == 17) {
      k = i & 0xFFFFFFFE;
      paramRect.left &= 0xFFFFFFFE;
      paramRect.top &= 0xFFFFFFFE;
      paramRect.right = paramRect.left + k;
      paramRect.bottom = paramRect.top + (j & 0xFFFFFFFE);
    } 
    if (this.mFormat == 20) {
      paramRect.left &= 0xFFFFFFFE;
      paramRect.right = paramRect.left + (k & 0xFFFFFFFE);
    } 
  }
  
  private static native boolean nativeCompressToJpeg(byte[] paramArrayOfbyte1, int paramInt1, int paramInt2, int paramInt3, int[] paramArrayOfint1, int[] paramArrayOfint2, int paramInt4, OutputStream paramOutputStream, byte[] paramArrayOfbyte2);
}
