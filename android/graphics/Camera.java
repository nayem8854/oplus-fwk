package android.graphics;

public class Camera {
  long native_instance;
  
  public Camera() {
    nativeConstructor();
  }
  
  public void getMatrix(Matrix paramMatrix) {
    nativeGetMatrix(paramMatrix.native_instance);
  }
  
  public void applyToCanvas(Canvas paramCanvas) {
    nativeApplyToCanvas(paramCanvas.getNativeCanvasWrapper());
  }
  
  protected void finalize() throws Throwable {
    try {
      nativeDestructor();
      this.native_instance = 0L;
      return;
    } finally {
      super.finalize();
    } 
  }
  
  private native void nativeApplyToCanvas(long paramLong);
  
  private native void nativeConstructor();
  
  private native void nativeDestructor();
  
  private native void nativeGetMatrix(long paramLong);
  
  public native float dotWithNormal(float paramFloat1, float paramFloat2, float paramFloat3);
  
  public native float getLocationX();
  
  public native float getLocationY();
  
  public native float getLocationZ();
  
  public native void restore();
  
  public native void rotate(float paramFloat1, float paramFloat2, float paramFloat3);
  
  public native void rotateX(float paramFloat);
  
  public native void rotateY(float paramFloat);
  
  public native void rotateZ(float paramFloat);
  
  public native void save();
  
  public native void setLocation(float paramFloat1, float paramFloat2, float paramFloat3);
  
  public native void translate(float paramFloat1, float paramFloat2, float paramFloat3);
}
