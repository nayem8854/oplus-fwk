package android.graphics;

public class CornerPathEffect extends PathEffect {
  public CornerPathEffect(float paramFloat) {
    this.native_instance = nativeCreate(paramFloat);
  }
  
  private static native long nativeCreate(float paramFloat);
}
