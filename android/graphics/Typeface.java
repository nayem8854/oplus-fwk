package android.graphics;

import android.content.res.AssetManager;
import android.content.res.FontResourcesParser;
import android.graphics.fonts.Font;
import android.graphics.fonts.FontFamily;
import android.graphics.fonts.FontStyle;
import android.graphics.fonts.FontVariationAxis;
import android.graphics.fonts.SystemFonts;
import android.os.ParcelFileDescriptor;
import android.text.FontConfig;
import android.util.LongSparseArray;
import android.util.LruCache;
import android.util.SparseArray;
import com.android.internal.util.Preconditions;
import dalvik.annotation.optimization.CriticalNative;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import libcore.util.NativeAllocationRegistry;

public class Typeface extends OplusBaseTypeface {
  public static final int BOLD = 1;
  
  public static final int BOLD_ITALIC = 3;
  
  public static final Typeface DEFAULT;
  
  public static final Typeface DEFAULT_BOLD;
  
  private static final String DEFAULT_FAMILY = "sans-serif";
  
  private static final int[] EMPTY_AXES;
  
  public static final int ITALIC = 2;
  
  public static final Typeface MONOSPACE;
  
  public static final int NORMAL = 0;
  
  public static final int RESOLVE_BY_FONT_TABLE = -1;
  
  public static final Typeface SANS_SERIF;
  
  public static final Typeface SERIF;
  
  private static final int STYLE_ITALIC = 1;
  
  public static final int STYLE_MASK = 3;
  
  private static final int STYLE_NORMAL = 0;
  
  private static String TAG = "Typeface";
  
  static Typeface sDefaultTypeface;
  
  static Typeface[] sDefaults;
  
  private static final Object sDynamicCacheLock;
  
  private static final LruCache<String, Typeface> sDynamicTypefaceCache;
  
  private static final NativeAllocationRegistry sRegistry;
  
  private static final Object sStyledCacheLock;
  
  static {
    ClassLoader classLoader = Typeface.class.getClassLoader();
    long l = nativeGetReleaseFunc();
    sRegistry = NativeAllocationRegistry.createMalloced(classLoader, l);
  }
  
  private static final LongSparseArray<SparseArray<Typeface>> sStyledTypefaceCache = new LongSparseArray(3);
  
  @Deprecated
  static final Map<String, FontFamily[]> sSystemFallbackMap;
  
  static final Map<String, Typeface> sSystemFontMap;
  
  private static final Object sWeightCacheLock;
  
  private static final LongSparseArray<SparseArray<Typeface>> sWeightTypefaceCache;
  
  static {
    sStyledCacheLock = new Object();
    sWeightTypefaceCache = new LongSparseArray(3);
    sWeightCacheLock = new Object();
    sDynamicTypefaceCache = new LruCache(16);
    sDynamicCacheLock = new Object();
    sSystemFallbackMap = Collections.emptyMap();
    byte b = 0;
    EMPTY_AXES = new int[0];
    HashMap<Object, Object> hashMap = new HashMap<>();
    Map<String, FontFamily[]> map = SystemFonts.getRawSystemFallbackMap();
    FontConfig.Alias[] arrayOfAlias = SystemFonts.getAliases();
    initSystemDefaultTypefaces((Map)hashMap, map, arrayOfAlias);
    Map<Object, Object> map1 = Collections.unmodifiableMap(hashMap);
    if (map1.containsKey("sans-serif"))
      setDefault(sSystemFontMap.get("sans-serif")); 
    String str = (String)null;
    DEFAULT = create(str, 0);
    DEFAULT_BOLD = create(str, 1);
    SANS_SERIF = create("sans-serif", 0);
    SERIF = create("serif", 0);
    MONOSPACE = create("monospace", 0);
    Typeface typeface2 = DEFAULT, typeface1 = DEFAULT_BOLD;
    Typeface typeface3 = create(str, 2);
    sDefaults = new Typeface[] { typeface2, typeface1, typeface3, create(str, 3) };
    String[] arrayOfString = new String[6];
    arrayOfString[0] = "serif";
    arrayOfString[1] = "sans-serif";
    arrayOfString[2] = "cursive";
    arrayOfString[3] = "fantasy";
    arrayOfString[4] = "monospace";
    arrayOfString[5] = "system-ui";
    for (int i = arrayOfString.length; b < i; ) {
      String str1 = arrayOfString[b];
      registerGenericFamilyNative(str1, (Typeface)hashMap.get(str1));
      b++;
    } 
    sInstance = DEFAULT;
    addIsLikeDefaultFlag(sSystemFontMap);
  }
  
  private int mStyle = 0;
  
  private int[] mSupportedAxes;
  
  private int mWeight = 0;
  
  public long native_instance;
  
  private static void setDefault(Typeface paramTypeface) {
    sDefaultTypeface = paramTypeface;
    nativeSetDefault(paramTypeface.native_instance);
  }
  
  public int getWeight() {
    return this.mWeight;
  }
  
  public int getStyle() {
    return this.mStyle;
  }
  
  public final boolean isBold() {
    int i = this.mStyle;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public final boolean isItalic() {
    boolean bool;
    if ((this.mStyle & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static Typeface createFromResources(FontResourcesParser.FamilyResourceEntry paramFamilyResourceEntry, AssetManager paramAssetManager, String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: instanceof android/content/res/FontResourcesParser$ProviderResourceEntry
    //   4: ifeq -> 163
    //   7: aload_0
    //   8: checkcast android/content/res/FontResourcesParser$ProviderResourceEntry
    //   11: astore_0
    //   12: aload_0
    //   13: invokevirtual getCerts : ()Ljava/util/List;
    //   16: astore_3
    //   17: new java/util/ArrayList
    //   20: dup
    //   21: invokespecial <init> : ()V
    //   24: astore_1
    //   25: aload_3
    //   26: ifnull -> 122
    //   29: iconst_0
    //   30: istore #4
    //   32: iload #4
    //   34: aload_3
    //   35: invokeinterface size : ()I
    //   40: if_icmpge -> 122
    //   43: aload_3
    //   44: iload #4
    //   46: invokeinterface get : (I)Ljava/lang/Object;
    //   51: checkcast java/util/List
    //   54: astore_2
    //   55: new java/util/ArrayList
    //   58: dup
    //   59: invokespecial <init> : ()V
    //   62: astore #5
    //   64: iconst_0
    //   65: istore #6
    //   67: iload #6
    //   69: aload_2
    //   70: invokeinterface size : ()I
    //   75: if_icmpge -> 107
    //   78: aload #5
    //   80: aload_2
    //   81: iload #6
    //   83: invokeinterface get : (I)Ljava/lang/Object;
    //   88: checkcast java/lang/String
    //   91: iconst_0
    //   92: invokestatic decode : (Ljava/lang/String;I)[B
    //   95: invokeinterface add : (Ljava/lang/Object;)Z
    //   100: pop
    //   101: iinc #6, 1
    //   104: goto -> 67
    //   107: aload_1
    //   108: aload #5
    //   110: invokeinterface add : (Ljava/lang/Object;)Z
    //   115: pop
    //   116: iinc #4, 1
    //   119: goto -> 32
    //   122: aload_0
    //   123: invokevirtual getAuthority : ()Ljava/lang/String;
    //   126: astore_2
    //   127: new android/provider/FontRequest
    //   130: dup
    //   131: aload_2
    //   132: aload_0
    //   133: invokevirtual getPackage : ()Ljava/lang/String;
    //   136: aload_0
    //   137: invokevirtual getQuery : ()Ljava/lang/String;
    //   140: aload_1
    //   141: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    //   144: astore_0
    //   145: aload_0
    //   146: invokestatic getFontSync : (Landroid/provider/FontRequest;)Landroid/graphics/Typeface;
    //   149: astore_0
    //   150: aload_0
    //   151: ifnonnull -> 161
    //   154: getstatic android/graphics/Typeface.DEFAULT : Landroid/graphics/Typeface;
    //   157: astore_0
    //   158: goto -> 161
    //   161: aload_0
    //   162: areturn
    //   163: aload_1
    //   164: aload_2
    //   165: invokestatic findFromCache : (Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;
    //   168: astore_3
    //   169: aload_3
    //   170: ifnull -> 175
    //   173: aload_3
    //   174: areturn
    //   175: aload_0
    //   176: checkcast android/content/res/FontResourcesParser$FontFamilyFilesResourceEntry
    //   179: astore_3
    //   180: aconst_null
    //   181: astore_0
    //   182: aload_3
    //   183: invokevirtual getEntries : ()[Landroid/content/res/FontResourcesParser$FontFileResourceEntry;
    //   186: astore_3
    //   187: aload_3
    //   188: arraylength
    //   189: istore #7
    //   191: iconst_0
    //   192: istore #4
    //   194: iload #4
    //   196: iload #7
    //   198: if_icmpge -> 344
    //   201: aload_3
    //   202: iload #4
    //   204: aaload
    //   205: astore #5
    //   207: new android/graphics/fonts/Font$Builder
    //   210: astore #8
    //   212: aload #8
    //   214: aload_1
    //   215: aload #5
    //   217: invokevirtual getFileName : ()Ljava/lang/String;
    //   220: iconst_0
    //   221: iconst_0
    //   222: invokespecial <init> : (Landroid/content/res/AssetManager;Ljava/lang/String;ZI)V
    //   225: aload #8
    //   227: aload #5
    //   229: invokevirtual getTtcIndex : ()I
    //   232: invokevirtual setTtcIndex : (I)Landroid/graphics/fonts/Font$Builder;
    //   235: astore #8
    //   237: aload #8
    //   239: aload #5
    //   241: invokevirtual getVariationSettings : ()Ljava/lang/String;
    //   244: invokevirtual setFontVariationSettings : (Ljava/lang/String;)Landroid/graphics/fonts/Font$Builder;
    //   247: astore #8
    //   249: aload #5
    //   251: invokevirtual getWeight : ()I
    //   254: iconst_m1
    //   255: if_icmpeq -> 269
    //   258: aload #8
    //   260: aload #5
    //   262: invokevirtual getWeight : ()I
    //   265: invokevirtual setWeight : (I)Landroid/graphics/fonts/Font$Builder;
    //   268: pop
    //   269: aload #5
    //   271: invokevirtual getItalic : ()I
    //   274: iconst_m1
    //   275: if_icmpeq -> 308
    //   278: aload #5
    //   280: invokevirtual getItalic : ()I
    //   283: istore #9
    //   285: iconst_1
    //   286: istore #6
    //   288: iload #9
    //   290: iconst_1
    //   291: if_icmpne -> 297
    //   294: goto -> 300
    //   297: iconst_0
    //   298: istore #6
    //   300: aload #8
    //   302: iload #6
    //   304: invokevirtual setSlant : (I)Landroid/graphics/fonts/Font$Builder;
    //   307: pop
    //   308: aload_0
    //   309: ifnonnull -> 328
    //   312: new android/graphics/fonts/FontFamily$Builder
    //   315: dup
    //   316: aload #8
    //   318: invokevirtual build : ()Landroid/graphics/fonts/Font;
    //   321: invokespecial <init> : (Landroid/graphics/fonts/Font;)V
    //   324: astore_0
    //   325: goto -> 338
    //   328: aload_0
    //   329: aload #8
    //   331: invokevirtual build : ()Landroid/graphics/fonts/Font;
    //   334: invokevirtual addFont : (Landroid/graphics/fonts/Font;)Landroid/graphics/fonts/FontFamily$Builder;
    //   337: pop
    //   338: iinc #4, 1
    //   341: goto -> 194
    //   344: aload_0
    //   345: ifnonnull -> 352
    //   348: getstatic android/graphics/Typeface.DEFAULT : Landroid/graphics/Typeface;
    //   351: areturn
    //   352: aload_0
    //   353: invokevirtual build : ()Landroid/graphics/fonts/FontFamily;
    //   356: astore #5
    //   358: new android/graphics/fonts/FontStyle
    //   361: astore #8
    //   363: aload #8
    //   365: sipush #400
    //   368: iconst_0
    //   369: invokespecial <init> : (II)V
    //   372: aload #5
    //   374: iconst_0
    //   375: invokevirtual getFont : (I)Landroid/graphics/fonts/Font;
    //   378: astore_0
    //   379: aload #8
    //   381: aload_0
    //   382: invokevirtual getStyle : ()Landroid/graphics/fonts/FontStyle;
    //   385: invokevirtual getMatchScore : (Landroid/graphics/fonts/FontStyle;)I
    //   388: istore #7
    //   390: iconst_1
    //   391: istore #4
    //   393: iload #4
    //   395: aload #5
    //   397: invokevirtual getSize : ()I
    //   400: if_icmpge -> 449
    //   403: aload #5
    //   405: iload #4
    //   407: invokevirtual getFont : (I)Landroid/graphics/fonts/Font;
    //   410: astore_3
    //   411: aload #8
    //   413: aload_3
    //   414: invokevirtual getStyle : ()Landroid/graphics/fonts/FontStyle;
    //   417: invokevirtual getMatchScore : (Landroid/graphics/fonts/FontStyle;)I
    //   420: istore #9
    //   422: iload #7
    //   424: istore #6
    //   426: iload #9
    //   428: iload #7
    //   430: if_icmpge -> 439
    //   433: aload_3
    //   434: astore_0
    //   435: iload #9
    //   437: istore #6
    //   439: iinc #4, 1
    //   442: iload #6
    //   444: istore #7
    //   446: goto -> 393
    //   449: new android/graphics/Typeface$CustomFallbackBuilder
    //   452: astore_3
    //   453: aload_3
    //   454: aload #5
    //   456: invokespecial <init> : (Landroid/graphics/fonts/FontFamily;)V
    //   459: aload_3
    //   460: aload_0
    //   461: invokevirtual getStyle : ()Landroid/graphics/fonts/FontStyle;
    //   464: invokevirtual setStyle : (Landroid/graphics/fonts/FontStyle;)Landroid/graphics/Typeface$CustomFallbackBuilder;
    //   467: astore_0
    //   468: aload_0
    //   469: invokevirtual build : ()Landroid/graphics/Typeface;
    //   472: astore_0
    //   473: goto -> 481
    //   476: astore_0
    //   477: getstatic android/graphics/Typeface.DEFAULT : Landroid/graphics/Typeface;
    //   480: astore_0
    //   481: getstatic android/graphics/Typeface.sDynamicCacheLock : Ljava/lang/Object;
    //   484: astore_3
    //   485: aload_3
    //   486: monitorenter
    //   487: aload_1
    //   488: aload_2
    //   489: iconst_0
    //   490: aconst_null
    //   491: iconst_m1
    //   492: iconst_m1
    //   493: ldc 'sans-serif'
    //   495: invokestatic access$000 : (Landroid/content/res/AssetManager;Ljava/lang/String;I[Landroid/graphics/fonts/FontVariationAxis;IILjava/lang/String;)Ljava/lang/String;
    //   498: astore_1
    //   499: getstatic android/graphics/Typeface.sDynamicTypefaceCache : Landroid/util/LruCache;
    //   502: aload_1
    //   503: aload_0
    //   504: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   507: pop
    //   508: aload_3
    //   509: monitorexit
    //   510: aload_0
    //   511: areturn
    //   512: astore_0
    //   513: aload_3
    //   514: monitorexit
    //   515: aload_0
    //   516: athrow
    //   517: astore_0
    //   518: aconst_null
    //   519: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #223	-> 0
    //   #224	-> 7
    //   #226	-> 12
    //   #227	-> 17
    //   #228	-> 25
    //   #229	-> 29
    //   #230	-> 43
    //   #231	-> 55
    //   #232	-> 64
    //   #233	-> 78
    //   #232	-> 101
    //   #235	-> 107
    //   #229	-> 116
    //   #240	-> 122
    //   #241	-> 127
    //   #242	-> 145
    //   #243	-> 150
    //   #246	-> 163
    //   #247	-> 169
    //   #250	-> 175
    //   #253	-> 180
    //   #254	-> 182
    //   #255	-> 207
    //   #257	-> 225
    //   #258	-> 237
    //   #259	-> 249
    //   #260	-> 258
    //   #262	-> 269
    //   #263	-> 278
    //   #264	-> 294
    //   #263	-> 300
    //   #267	-> 308
    //   #268	-> 312
    //   #270	-> 328
    //   #254	-> 338
    //   #273	-> 344
    //   #274	-> 348
    //   #276	-> 352
    //   #277	-> 358
    //   #279	-> 372
    //   #280	-> 379
    //   #281	-> 390
    //   #282	-> 403
    //   #283	-> 411
    //   #284	-> 422
    //   #285	-> 433
    //   #286	-> 435
    //   #281	-> 439
    //   #289	-> 449
    //   #290	-> 459
    //   #291	-> 468
    //   #298	-> 473
    //   #296	-> 476
    //   #297	-> 477
    //   #299	-> 481
    //   #300	-> 487
    //   #303	-> 499
    //   #304	-> 508
    //   #305	-> 510
    //   #304	-> 512
    //   #292	-> 517
    //   #295	-> 518
    // Exception table:
    //   from	to	target	type
    //   182	191	517	java/lang/IllegalArgumentException
    //   182	191	476	java/io/IOException
    //   207	225	517	java/lang/IllegalArgumentException
    //   207	225	476	java/io/IOException
    //   225	237	517	java/lang/IllegalArgumentException
    //   225	237	476	java/io/IOException
    //   237	249	517	java/lang/IllegalArgumentException
    //   237	249	476	java/io/IOException
    //   249	258	517	java/lang/IllegalArgumentException
    //   249	258	476	java/io/IOException
    //   258	269	517	java/lang/IllegalArgumentException
    //   258	269	476	java/io/IOException
    //   269	278	517	java/lang/IllegalArgumentException
    //   269	278	476	java/io/IOException
    //   278	285	517	java/lang/IllegalArgumentException
    //   278	285	476	java/io/IOException
    //   300	308	517	java/lang/IllegalArgumentException
    //   300	308	476	java/io/IOException
    //   312	325	517	java/lang/IllegalArgumentException
    //   312	325	476	java/io/IOException
    //   328	338	517	java/lang/IllegalArgumentException
    //   328	338	476	java/io/IOException
    //   348	352	517	java/lang/IllegalArgumentException
    //   348	352	476	java/io/IOException
    //   352	358	517	java/lang/IllegalArgumentException
    //   352	358	476	java/io/IOException
    //   358	372	517	java/lang/IllegalArgumentException
    //   358	372	476	java/io/IOException
    //   372	379	517	java/lang/IllegalArgumentException
    //   372	379	476	java/io/IOException
    //   379	390	517	java/lang/IllegalArgumentException
    //   379	390	476	java/io/IOException
    //   393	403	517	java/lang/IllegalArgumentException
    //   393	403	476	java/io/IOException
    //   403	411	517	java/lang/IllegalArgumentException
    //   403	411	476	java/io/IOException
    //   411	422	517	java/lang/IllegalArgumentException
    //   411	422	476	java/io/IOException
    //   449	459	517	java/lang/IllegalArgumentException
    //   449	459	476	java/io/IOException
    //   459	468	517	java/lang/IllegalArgumentException
    //   459	468	476	java/io/IOException
    //   468	473	517	java/lang/IllegalArgumentException
    //   468	473	476	java/io/IOException
    //   487	499	512	finally
    //   499	508	512	finally
    //   508	510	512	finally
    //   513	515	512	finally
  }
  
  public static Typeface findFromCache(AssetManager paramAssetManager, String paramString) {
    synchronized (sDynamicCacheLock) {
      String str = Builder.createAssetUid(paramAssetManager, paramString, 0, null, -1, -1, "sans-serif");
      Typeface typeface = (Typeface)sDynamicTypefaceCache.get(str);
      if (typeface != null)
        return typeface; 
      return null;
    } 
  }
  
  class Builder {
    private int mWeight = -1;
    
    private final String mPath;
    
    private int mItalic = -1;
    
    private final Font.Builder mFontBuilder;
    
    private String mFallbackFamilyName;
    
    private final AssetManager mAssetManager;
    
    public static final int NORMAL_WEIGHT = 400;
    
    public static final int BOLD_WEIGHT = 700;
    
    public Builder() {
      this.mFontBuilder = new Font.Builder((File)this$0);
      this.mAssetManager = null;
      this.mPath = null;
    }
    
    public Builder() {
      try {
        Font.Builder builder2 = new Font.Builder();
        this(ParcelFileDescriptor.dup((FileDescriptor)this$0));
        Font.Builder builder1 = builder2;
      } catch (IOException iOException) {
        iOException = null;
      } 
      this.mFontBuilder = (Font.Builder)iOException;
      this.mAssetManager = null;
      this.mPath = null;
    }
    
    public Builder() {
      this.mFontBuilder = new Font.Builder(new File((String)this$0));
      this.mAssetManager = null;
      this.mPath = null;
    }
    
    public Builder(String param1String) {
      this((AssetManager)this$0, param1String, true, 0);
    }
    
    public Builder(String param1String, boolean param1Boolean, int param1Int) {
      this.mFontBuilder = new Font.Builder((AssetManager)this$0, param1String, param1Boolean, param1Int);
      this.mAssetManager = (AssetManager)this$0;
      this.mPath = param1String;
    }
    
    public Builder setWeight(int param1Int) {
      this.mWeight = param1Int;
      this.mFontBuilder.setWeight(param1Int);
      return this;
    }
    
    public Builder setItalic(boolean param1Boolean) {
      this.mItalic = param1Boolean;
      this.mFontBuilder.setSlant(param1Boolean);
      return this;
    }
    
    public Builder setTtcIndex(int param1Int) {
      this.mFontBuilder.setTtcIndex(param1Int);
      return this;
    }
    
    public Builder setFontVariationSettings(String param1String) {
      this.mFontBuilder.setFontVariationSettings(param1String);
      return this;
    }
    
    public Builder setFontVariationSettings(FontVariationAxis[] param1ArrayOfFontVariationAxis) {
      this.mFontBuilder.setFontVariationSettings(param1ArrayOfFontVariationAxis);
      return this;
    }
    
    public Builder setFallback(String param1String) {
      this.mFallbackFamilyName = param1String;
      return this;
    }
    
    private static String createAssetUid(AssetManager param1AssetManager, String param1String1, int param1Int1, FontVariationAxis[] param1ArrayOfFontVariationAxis, int param1Int2, int param1Int3, String param1String2) {
      SparseArray<String> sparseArray = param1AssetManager.getAssignedPackageIdentifiers();
      StringBuilder stringBuilder = new StringBuilder();
      int i = sparseArray.size();
      for (byte b = 0; b < i; b++) {
        stringBuilder.append((String)sparseArray.valueAt(b));
        stringBuilder.append("-");
      } 
      stringBuilder.append(param1String1);
      stringBuilder.append("-");
      stringBuilder.append(Integer.toString(param1Int1));
      stringBuilder.append("-");
      stringBuilder.append(Integer.toString(param1Int2));
      stringBuilder.append("-");
      stringBuilder.append(Integer.toString(param1Int3));
      stringBuilder.append("--");
      stringBuilder.append(param1String2);
      stringBuilder.append("--");
      if (param1ArrayOfFontVariationAxis != null)
        for (param1Int2 = param1ArrayOfFontVariationAxis.length, param1Int1 = 0; param1Int1 < param1Int2; ) {
          FontVariationAxis fontVariationAxis = param1ArrayOfFontVariationAxis[param1Int1];
          stringBuilder.append(fontVariationAxis.getTag());
          stringBuilder.append("-");
          stringBuilder.append(Float.toString(fontVariationAxis.getStyleValue()));
          param1Int1++;
        }  
      return stringBuilder.toString();
    }
    
    private Typeface resolveFallbackTypeface() {
      String str = this.mFallbackFamilyName;
      if (str == null)
        return null; 
      Typeface typeface = Typeface.getSystemDefaultTypeface(str);
      if (this.mWeight == -1 && this.mItalic == -1)
        return typeface; 
      int i = this.mWeight, j = i;
      if (i == -1)
        j = typeface.mWeight; 
      i = this.mItalic;
      boolean bool = false;
      if ((i == -1) ? ((typeface.mStyle & 0x2) != 0) : (i == 1))
        bool = true; 
      return Typeface.createWeightStyle(typeface, j, bool);
    }
    
    public Typeface build() {
      Font.Builder builder = this.mFontBuilder;
      if (builder == null)
        return resolveFallbackTypeface(); 
      try {
        String str;
        int i, j;
        Font font = builder.build();
        if (this.mAssetManager == null) {
          builder = null;
        } else {
          AssetManager assetManager = this.mAssetManager;
          String str1 = this.mPath;
          i = font.getTtcIndex();
          FontVariationAxis[] arrayOfFontVariationAxis = font.getAxes();
          j = this.mWeight;
          int k = this.mItalic;
          if (this.mFallbackFamilyName == null) {
            str = "sans-serif";
          } else {
            str = this.mFallbackFamilyName;
          } 
          str = createAssetUid(assetManager, str1, i, arrayOfFontVariationAxis, j, k, str);
        } 
        if (str != null)
          synchronized (Typeface.sDynamicCacheLock) {
            Typeface typeface1 = (Typeface)Typeface.sDynamicTypefaceCache.get(str);
            if (typeface1 != null)
              return typeface1; 
          }  
        FontFamily.Builder builder1 = new FontFamily.Builder();
        this(font);
        FontFamily fontFamily = builder1.build();
        if (this.mWeight == -1) {
          j = font.getStyle().getWeight();
        } else {
          j = this.mWeight;
        } 
        if (this.mItalic == -1) {
          i = font.getStyle().getSlant();
        } else {
          i = this.mItalic;
        } 
        Typeface.CustomFallbackBuilder customFallbackBuilder = new Typeface.CustomFallbackBuilder();
        this(fontFamily);
        FontStyle fontStyle = new FontStyle();
        this(j, i);
        customFallbackBuilder = customFallbackBuilder.setStyle(fontStyle);
        if (this.mFallbackFamilyName != null)
          customFallbackBuilder.setSystemFallback(this.mFallbackFamilyName); 
        Typeface typeface = customFallbackBuilder.build();
        if (str != null)
          synchronized (Typeface.sDynamicCacheLock) {
            Typeface.sDynamicTypefaceCache.put(str, typeface);
          }  
        return typeface;
      } catch (IOException|IllegalArgumentException iOException) {
        return resolveFallbackTypeface();
      } 
    }
  }
  
  class CustomFallbackBuilder {
    private FontStyle mStyle;
    
    private final ArrayList<FontFamily> mFamilies = new ArrayList<>();
    
    private String mFallbackName = null;
    
    private static final int MAX_CUSTOM_FALLBACK = 64;
    
    public static int getMaxCustomFallbackCount() {
      return 64;
    }
    
    public CustomFallbackBuilder(Typeface this$0) {
      Preconditions.checkNotNull(this$0);
      this.mFamilies.add(this$0);
    }
    
    public CustomFallbackBuilder setSystemFallback(String param1String) {
      Preconditions.checkNotNull(param1String);
      this.mFallbackName = param1String;
      return this;
    }
    
    public CustomFallbackBuilder setStyle(FontStyle param1FontStyle) {
      this.mStyle = param1FontStyle;
      return this;
    }
    
    public CustomFallbackBuilder addCustomFallback(FontFamily param1FontFamily) {
      boolean bool;
      Preconditions.checkNotNull(param1FontFamily);
      if (this.mFamilies.size() < getMaxCustomFallbackCount()) {
        bool = true;
      } else {
        bool = false;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Custom fallback limit exceeded(");
      stringBuilder.append(getMaxCustomFallbackCount());
      stringBuilder.append(")");
      String str = stringBuilder.toString();
      Preconditions.checkArgument(bool, str);
      this.mFamilies.add(param1FontFamily);
      return this;
    }
    
    public Typeface build() {
      int i = this.mFamilies.size();
      FontFamily[] arrayOfFontFamily = SystemFonts.getSystemFallback(this.mFallbackName);
      long[] arrayOfLong = new long[arrayOfFontFamily.length + i];
      int j;
      for (j = 0; j < i; j++)
        arrayOfLong[j] = ((FontFamily)this.mFamilies.get(j)).getNativePtr(); 
      for (j = 0; j < arrayOfFontFamily.length; j++)
        arrayOfLong[j + i] = arrayOfFontFamily[j].getNativePtr(); 
      FontStyle fontStyle = this.mStyle;
      if (fontStyle == null) {
        j = 400;
      } else {
        j = fontStyle.getWeight();
      } 
      fontStyle = this.mStyle;
      if (fontStyle == null || fontStyle.getSlant() == 0) {
        i = 0;
        return new Typeface(Typeface.nativeCreateFromArray(arrayOfLong, j, i));
      } 
      i = 1;
      return new Typeface(Typeface.nativeCreateFromArray(arrayOfLong, j, i));
    }
  }
  
  public static Typeface create(String paramString, int paramInt) {
    return create(getSystemDefaultTypeface(paramString), paramInt);
  }
  
  public static Typeface create(Typeface paramTypeface, int paramInt) {
    int i = paramInt;
    if ((paramInt & 0xFFFFFFFC) != 0)
      i = 0; 
    Typeface typeface = paramTypeface;
    if (paramTypeface == null)
      typeface = sDefaultTypeface; 
    if (typeface.mStyle == i)
      return typeface; 
    long l = typeface.native_instance;
    synchronized (sStyledCacheLock) {
      SparseArray sparseArray = (SparseArray)sStyledTypefaceCache.get(l);
      if (sparseArray == null) {
        sparseArray = new SparseArray();
        this(4);
        sStyledTypefaceCache.put(l, sparseArray);
      } else {
        Typeface typeface2 = (Typeface)sparseArray.get(i);
        if (typeface2 != null)
          return typeface2; 
      } 
      Typeface typeface1 = new Typeface();
      this(nativeCreateFromTypeface(l, i));
      sparseArray.put(i, typeface1);
      typeface1.isLikeDefault = typeface.isLikeDefault;
      return typeface1;
    } 
  }
  
  public static Typeface create(Typeface paramTypeface, int paramInt, boolean paramBoolean) {
    Preconditions.checkArgumentInRange(paramInt, 0, 1000, "weight");
    Typeface typeface = paramTypeface;
    if (paramTypeface == null)
      typeface = sDefaultTypeface; 
    return createWeightStyle(typeface, paramInt, paramBoolean);
  }
  
  private static Typeface createWeightStyle(Typeface paramTypeface, int paramInt, boolean paramBoolean) {
    int i = paramInt << 1 | paramBoolean;
    synchronized (sWeightCacheLock) {
      SparseArray sparseArray = (SparseArray)sWeightTypefaceCache.get(paramTypeface.native_instance);
      if (sparseArray == null) {
        sparseArray = new SparseArray();
        this(4);
        sWeightTypefaceCache.put(paramTypeface.native_instance, sparseArray);
      } else {
        Typeface typeface1 = (Typeface)sparseArray.get(i);
        if (typeface1 != null)
          return typeface1; 
      } 
      Typeface typeface = new Typeface();
      long l = paramTypeface.native_instance;
      this(nativeCreateFromTypefaceWithExactStyle(l, paramInt, paramBoolean));
      sparseArray.put(i, typeface);
      typeface.isLikeDefault = paramTypeface.isLikeDefault;
      return typeface;
    } 
  }
  
  public static Typeface createFromTypefaceWithVariation(Typeface paramTypeface, List<FontVariationAxis> paramList) {
    if (paramTypeface == null)
      paramTypeface = DEFAULT; 
    return new Typeface(nativeCreateFromTypefaceWithVariation(paramTypeface.native_instance, paramList));
  }
  
  public static Typeface defaultFromStyle(int paramInt) {
    return sDefaults[paramInt];
  }
  
  public static Typeface createFromAsset(AssetManager paramAssetManager, String paramString) {
    Preconditions.checkNotNull(paramString);
    Preconditions.checkNotNull(paramAssetManager);
    Typeface typeface = (new Builder(paramString)).build();
    if (typeface != null)
      return typeface; 
    try {
      InputStream inputStream = paramAssetManager.open(paramString);
      if (inputStream != null)
        inputStream.close(); 
      return DEFAULT;
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Font asset not found ");
      stringBuilder.append(paramString);
      throw new RuntimeException(stringBuilder.toString());
    } 
  }
  
  private static String createProviderUid(String paramString1, String paramString2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("provider:");
    stringBuilder.append(paramString1);
    stringBuilder.append("-");
    stringBuilder.append(paramString2);
    return stringBuilder.toString();
  }
  
  public static Typeface createFromFile(File paramFile) {
    Typeface typeface = (new Builder()).build();
    if (typeface != null)
      return typeface; 
    if (paramFile.exists())
      return DEFAULT; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Font asset not found ");
    stringBuilder.append(paramFile.getAbsolutePath());
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public static Typeface createFromFile(String paramString) {
    Preconditions.checkNotNull(paramString);
    return createFromFile(new File(paramString));
  }
  
  @Deprecated
  private static Typeface createFromFamilies(FontFamily[] paramArrayOfFontFamily) {
    long[] arrayOfLong = new long[paramArrayOfFontFamily.length];
    for (byte b = 0; b < paramArrayOfFontFamily.length; b++)
      arrayOfLong[b] = (paramArrayOfFontFamily[b]).mNativePtr; 
    return new Typeface(nativeCreateFromArray(arrayOfLong, -1, -1));
  }
  
  private static Typeface createFromFamilies(FontFamily[] paramArrayOfFontFamily) {
    long[] arrayOfLong = new long[paramArrayOfFontFamily.length];
    for (byte b = 0; b < paramArrayOfFontFamily.length; b++)
      arrayOfLong[b] = paramArrayOfFontFamily[b].getNativePtr(); 
    return new Typeface(nativeCreateFromArray(arrayOfLong, -1, -1));
  }
  
  @Deprecated
  private static Typeface createFromFamiliesWithDefault(FontFamily[] paramArrayOfFontFamily, int paramInt1, int paramInt2) {
    return createFromFamiliesWithDefault(paramArrayOfFontFamily, "sans-serif", paramInt1, paramInt2);
  }
  
  @Deprecated
  private static Typeface createFromFamiliesWithDefault(FontFamily[] paramArrayOfFontFamily, String paramString, int paramInt1, int paramInt2) {
    FontFamily[] arrayOfFontFamily = SystemFonts.getSystemFallback(paramString);
    long[] arrayOfLong = new long[paramArrayOfFontFamily.length + arrayOfFontFamily.length];
    byte b;
    for (b = 0; b < paramArrayOfFontFamily.length; b++)
      arrayOfLong[b] = (paramArrayOfFontFamily[b]).mNativePtr; 
    for (b = 0; b < arrayOfFontFamily.length; b++)
      arrayOfLong[paramArrayOfFontFamily.length + b] = arrayOfFontFamily[b].getNativePtr(); 
    return new Typeface(nativeCreateFromArray(arrayOfLong, paramInt1, paramInt2));
  }
  
  private Typeface(long paramLong) {
    if (paramLong != 0L) {
      this.native_instance = paramLong;
      sRegistry.registerNativeAllocation(this, paramLong);
      this.mStyle = nativeGetStyle(paramLong);
      this.mWeight = nativeGetWeight(paramLong);
      return;
    } 
    throw new RuntimeException("native typeface cannot be made");
  }
  
  private static Typeface getSystemDefaultTypeface(String paramString) {
    Typeface typeface = sSystemFontMap.get(paramString);
    if (typeface == null)
      typeface = DEFAULT; 
    return typeface;
  }
  
  public static void initSystemDefaultTypefaces(Map<String, Typeface> paramMap, Map<String, FontFamily[]> paramMap1, FontConfig.Alias[] paramArrayOfAlias) {
    for (Map.Entry<String, FontFamily> entry : paramMap1.entrySet())
      paramMap.put((String)entry.getKey(), createFromFamilies((FontFamily[])entry.getValue())); 
    int i;
    byte b;
    for (i = paramArrayOfAlias.length, b = 0; b < i; ) {
      FontConfig.Alias alias = paramArrayOfAlias[b];
      if (!paramMap.containsKey(alias.getName())) {
        Typeface typeface = paramMap.get(alias.getToName());
        if (typeface != null) {
          int j = alias.getWeight();
          if (j != 400)
            typeface = new Typeface(nativeCreateWeightAlias(typeface.native_instance, j)); 
          paramMap.put(alias.getName(), typeface);
        } 
      } 
      b++;
    } 
  }
  
  private static void registerGenericFamilyNative(String paramString, Typeface paramTypeface) {
    if (paramTypeface != null)
      nativeRegisterGenericFamily(paramString, paramTypeface.native_instance); 
  }
  
  public OplusBaseTypeface getOplusBaseTypeface(Typeface paramTypeface) {
    return paramTypeface;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mStyle != ((Typeface)paramObject).mStyle || this.native_instance != ((Typeface)paramObject).native_instance)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    long l = this.native_instance;
    int i = (int)(l ^ l >>> 32L);
    int j = this.mStyle;
    return (17 * 31 + i) * 31 + j;
  }
  
  public boolean isSupportedAxes(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mSupportedAxes : [I
    //   4: ifnonnull -> 50
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mSupportedAxes : [I
    //   13: ifnonnull -> 40
    //   16: aload_0
    //   17: getfield native_instance : J
    //   20: invokestatic nativeGetSupportedAxes : (J)[I
    //   23: astore_2
    //   24: aload_0
    //   25: aload_2
    //   26: putfield mSupportedAxes : [I
    //   29: aload_2
    //   30: ifnonnull -> 40
    //   33: aload_0
    //   34: getstatic android/graphics/Typeface.EMPTY_AXES : [I
    //   37: putfield mSupportedAxes : [I
    //   40: aload_0
    //   41: monitorexit
    //   42: goto -> 50
    //   45: astore_2
    //   46: aload_0
    //   47: monitorexit
    //   48: aload_2
    //   49: athrow
    //   50: aload_0
    //   51: getfield mSupportedAxes : [I
    //   54: iload_1
    //   55: invokestatic binarySearch : ([II)I
    //   58: iflt -> 66
    //   61: iconst_1
    //   62: istore_3
    //   63: goto -> 68
    //   66: iconst_0
    //   67: istore_3
    //   68: iload_3
    //   69: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1224	-> 0
    //   #1225	-> 7
    //   #1226	-> 9
    //   #1227	-> 16
    //   #1228	-> 29
    //   #1229	-> 33
    //   #1232	-> 40
    //   #1234	-> 50
    // Exception table:
    //   from	to	target	type
    //   9	16	45	finally
    //   16	29	45	finally
    //   33	40	45	finally
    //   40	42	45	finally
    //   46	48	45	finally
  }
  
  private static native long nativeCreateFromArray(long[] paramArrayOflong, int paramInt1, int paramInt2);
  
  private static native long nativeCreateFromTypeface(long paramLong, int paramInt);
  
  private static native long nativeCreateFromTypefaceWithExactStyle(long paramLong, int paramInt, boolean paramBoolean);
  
  private static native long nativeCreateFromTypefaceWithVariation(long paramLong, List<FontVariationAxis> paramList);
  
  private static native long nativeCreateWeightAlias(long paramLong, int paramInt);
  
  @CriticalNative
  private static native long nativeGetReleaseFunc();
  
  @CriticalNative
  private static native int nativeGetStyle(long paramLong);
  
  private static native int[] nativeGetSupportedAxes(long paramLong);
  
  @CriticalNative
  private static native int nativeGetWeight(long paramLong);
  
  private static native void nativeRegisterGenericFamily(String paramString, long paramLong);
  
  @CriticalNative
  private static native void nativeSetDefault(long paramLong);
  
  @Retention(RetentionPolicy.SOURCE)
  class Style implements Annotation {}
}
