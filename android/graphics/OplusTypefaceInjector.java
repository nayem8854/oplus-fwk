package android.graphics;

import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.Log;
import java.util.Iterator;
import java.util.Map;

public final class OplusTypefaceInjector {
  public static final String DEFAULT_FONT_CONFIG_FILE = "/system/etc/fonts.xml";
  
  public static final String FBE_FONT_CONFIG_FILE = "/system_ext/etc/fonts_base.xml";
  
  public static Typeface OPLUSUI_MEDIUM;
  
  public static Typeface OPLUSUI_VF;
  
  public static Typeface OPLUSUI_VF_BOLD;
  
  public static Typeface OPLUSUI_VF_BOLD_ITALIC;
  
  public static Typeface OPLUSUI_VF_ITALIC;
  
  public static final String SECOND_FONT_CONFIG_FILE = "/system_ext/etc/fonts_base.xml";
  
  public static final boolean sIsFBESupport = "file".equals(SystemProperties.get("ro.crypto.type", ""));
  
  public static boolean isSystemTypeface(Typeface paramTypeface) {
    Typeface typeface = paramTypeface;
    if (paramTypeface == null)
      typeface = Typeface.DEFAULT; 
    if (typeface != null && Typeface.sSystemFontMap != null && Typeface.sSystemFontMap.containsValue(typeface))
      return true; 
    return false;
  }
  
  public static boolean isSystemTypeface(String paramString) {
    if (!TextUtils.isEmpty(paramString) && Typeface.sSystemFontMap != null && Typeface.sSystemFontMap.containsKey(paramString))
      return true; 
    return false;
  }
  
  public static Typeface[] getSystemDefaultTypefaces() {
    return Typeface.sDefaults;
  }
  
  public static void dumpSysTypeface() {
    Map<String, Typeface> map = Typeface.sSystemFontMap;
    if (map != null) {
      Iterator<Map.Entry> iterator = map.entrySet().iterator();
      while (iterator.hasNext()) {
        Map.Entry entry = iterator.next();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("System typeface  family = ");
        stringBuilder.append((String)entry.getKey());
        stringBuilder.append(" : ");
        stringBuilder.append(entry.getValue());
        Log.d("OppoFontUtilsCTI", stringBuilder.toString());
      } 
    } 
  }
}
