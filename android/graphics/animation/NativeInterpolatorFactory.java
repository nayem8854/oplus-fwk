package android.graphics.animation;

import android.animation.TimeInterpolator;

public final class NativeInterpolatorFactory {
  public static native long createAccelerateDecelerateInterpolator();
  
  public static native long createAccelerateInterpolator(float paramFloat);
  
  public static native long createAnticipateInterpolator(float paramFloat);
  
  public static native long createAnticipateOvershootInterpolator(float paramFloat);
  
  public static native long createBounceInterpolator();
  
  public static native long createCycleInterpolator(float paramFloat);
  
  public static native long createDecelerateInterpolator(float paramFloat);
  
  public static native long createLinearInterpolator();
  
  public static native long createLutInterpolator(float[] paramArrayOffloat);
  
  public static long createNativeInterpolator(TimeInterpolator paramTimeInterpolator, long paramLong) {
    if (paramTimeInterpolator == null)
      return createLinearInterpolator(); 
    if (RenderNodeAnimator.isNativeInterpolator(paramTimeInterpolator))
      return ((NativeInterpolator)paramTimeInterpolator).createNativeInterpolator(); 
    return FallbackLUTInterpolator.createNativeInterpolator(paramTimeInterpolator, paramLong);
  }
  
  public static native long createOvershootInterpolator(float paramFloat);
  
  public static native long createPathInterpolator(float[] paramArrayOffloat1, float[] paramArrayOffloat2);
}
