package android.graphics.animation;

import android.animation.TimeInterpolator;
import android.view.Choreographer;

@HasNativeInterpolator
public class FallbackLUTInterpolator implements NativeInterpolator, TimeInterpolator {
  private static final int MAX_SAMPLE_POINTS = 300;
  
  private final float[] mLut;
  
  private TimeInterpolator mSourceInterpolator;
  
  public FallbackLUTInterpolator(TimeInterpolator paramTimeInterpolator, long paramLong) {
    this.mSourceInterpolator = paramTimeInterpolator;
    this.mLut = createLUT(paramTimeInterpolator, paramLong);
  }
  
  private static float[] createLUT(TimeInterpolator paramTimeInterpolator, long paramLong) {
    long l = Choreographer.getInstance().getFrameIntervalNanos();
    int i = (int)(l / 1000000L);
    i = Math.max(2, (int)Math.ceil(paramLong / i));
    int j = Math.min(i, 300);
    float[] arrayOfFloat = new float[j];
    float f = (j - 1);
    for (i = 0; i < j; i++) {
      float f1 = i / f;
      arrayOfFloat[i] = paramTimeInterpolator.getInterpolation(f1);
    } 
    return arrayOfFloat;
  }
  
  public long createNativeInterpolator() {
    return NativeInterpolatorFactory.createLutInterpolator(this.mLut);
  }
  
  public static long createNativeInterpolator(TimeInterpolator paramTimeInterpolator, long paramLong) {
    float[] arrayOfFloat = createLUT(paramTimeInterpolator, paramLong);
    return NativeInterpolatorFactory.createLutInterpolator(arrayOfFloat);
  }
  
  public float getInterpolation(float paramFloat) {
    return this.mSourceInterpolator.getInterpolation(paramFloat);
  }
}
