package android.graphics.animation;

public interface NativeInterpolator {
  long createNativeInterpolator();
}
