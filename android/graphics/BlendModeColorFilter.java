package android.graphics;

public final class BlendModeColorFilter extends ColorFilter {
  int mColor;
  
  private final BlendMode mMode;
  
  public BlendModeColorFilter(int paramInt, BlendMode paramBlendMode) {
    this.mColor = paramInt;
    this.mMode = paramBlendMode;
  }
  
  public int getColor() {
    return this.mColor;
  }
  
  public BlendMode getMode() {
    return this.mMode;
  }
  
  long createNativeInstance() {
    return native_CreateBlendModeFilter(this.mColor, (this.mMode.getXfermode()).porterDuffMode);
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (((BlendModeColorFilter)paramObject).mMode != this.mMode)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    return this.mMode.hashCode() * 31 + this.mColor;
  }
  
  public void setColor(int paramInt) {
    if (this.mColor != paramInt) {
      this.mColor = paramInt;
      discardNativeInstance();
    } 
  }
  
  private static native long native_CreateBlendModeFilter(int paramInt1, int paramInt2);
}
