package android.graphics.fonts;

import android.common.OplusFeatureCache;
import android.graphics.FontListParser;
import android.text.FontConfig;
import android.util.ArrayMap;
import android.util.Log;
import com.android.internal.util.ArrayUtils;
import com.oplus.font.IOplusFontManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.xmlpull.v1.XmlPullParserException;

public final class SystemFonts {
  private static final String DEFAULT_FAMILY = "sans-serif";
  
  private static final String TAG = "SystemFonts";
  
  private static final FontConfig.Alias[] sAliases;
  
  private static final List<Font> sAvailableFonts;
  
  private static final Map<String, FontFamily[]> sSystemFallbackMap;
  
  public static Set<Font> getAvailableFonts() {
    HashSet<Font> hashSet = new HashSet();
    hashSet.addAll(sAvailableFonts);
    return hashSet;
  }
  
  public static FontFamily[] getSystemFallback(String paramString) {
    FontFamily[] arrayOfFontFamily = sSystemFallbackMap.get(paramString);
    if (arrayOfFontFamily == null)
      arrayOfFontFamily = sSystemFallbackMap.get("sans-serif"); 
    return arrayOfFontFamily;
  }
  
  public static Map<String, FontFamily[]> getRawSystemFallbackMap() {
    return sSystemFallbackMap;
  }
  
  public static FontConfig.Alias[] getAliases() {
    return sAliases;
  }
  
  private static ByteBuffer mmap(String paramString) {
    try {
      FileInputStream fileInputStream = new FileInputStream();
      this(paramString);
      try {
        FileChannel fileChannel = fileInputStream.getChannel();
        long l = fileChannel.size();
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, 0L, l);
      } finally {
        try {
          fileInputStream.close();
        } finally {
          fileInputStream = null;
        } 
      } 
    } catch (IOException iOException) {
      return null;
    } 
  }
  
  private static void pushFamilyToFallback(FontConfig.Family paramFamily, ArrayMap<String, ArrayList<FontFamily>> paramArrayMap, Map<String, ByteBuffer> paramMap, ArrayList<Font> paramArrayList) {
    FontFamily fontFamily;
    String str = paramFamily.getLanguages();
    int i = paramFamily.getVariant();
    ArrayList<FontConfig.Font> arrayList = new ArrayList();
    ArrayMap arrayMap = new ArrayMap();
    for (FontConfig.Font font : paramFamily.getFonts()) {
      String str1 = font.getFallbackFor();
      if (str1 == null) {
        arrayList.add(font);
      } else {
        ArrayList<FontConfig.Font> arrayList1 = (ArrayList)arrayMap.get(str1);
        ArrayList<FontConfig.Font> arrayList2 = arrayList1;
        if (arrayList1 == null) {
          arrayList2 = new ArrayList();
          arrayMap.put(str1, arrayList2);
        } 
        arrayList2.add(font);
      } 
    } 
    if (arrayList.isEmpty()) {
      fontFamily = null;
    } else {
      String str1 = paramFamily.getName();
      fontFamily = createFontFamily(str1, arrayList, str, i, paramMap, paramArrayList);
    } 
    for (byte b = 0; b < paramArrayMap.size(); b++) {
      ArrayList<FontConfig.Font> arrayList1 = (ArrayList)arrayMap.get(paramArrayMap.keyAt(b));
      if (arrayList1 == null) {
        if (fontFamily != null)
          ((ArrayList<FontFamily>)paramArrayMap.valueAt(b)).add(fontFamily); 
      } else {
        String str1 = paramFamily.getName();
        FontFamily fontFamily1 = createFontFamily(str1, arrayList1, str, i, paramMap, paramArrayList);
        if (fontFamily1 != null) {
          ((ArrayList<FontFamily>)paramArrayMap.valueAt(b)).add(fontFamily1);
        } else if (fontFamily != null) {
          ((ArrayList<FontFamily>)paramArrayMap.valueAt(b)).add(fontFamily);
        } 
      } 
    } 
  }
  
  private static FontFamily createFontFamily(String paramString1, List<FontConfig.Font> paramList, String paramString2, int paramInt, Map<String, ByteBuffer> paramMap, ArrayList<Font> paramArrayList) {
    FontFamily fontFamily;
    int i = paramList.size();
    IOException iOException2 = null;
    if (i == 0)
      return null; 
    paramString1 = null;
    i = 0;
    while (true) {
      int j = paramList.size();
      boolean bool = false;
      if (i < j) {
        FontConfig.Font font = paramList.get(i);
        String str = font.getFontName();
        ByteBuffer byteBuffer1 = paramMap.get(str);
        ByteBuffer byteBuffer2 = byteBuffer1;
        if (byteBuffer1 == null) {
          if (paramMap.containsKey(str))
            continue; 
          byteBuffer1 = mmap(str);
          paramMap.put(str, byteBuffer1);
          byteBuffer2 = byteBuffer1;
          if (byteBuffer1 == null)
            continue; 
        } 
        try {
          FontFamily.Builder builder;
          Font.Builder builder1 = new Font.Builder();
          File file = new File();
          this(str);
          this(byteBuffer2, file, paramString2);
          Font.Builder builder2 = builder1.setWeight(font.getWeight());
          if (font.isItalic())
            bool = true; 
          builder2 = builder2.setSlant(bool);
          builder2 = builder2.setTtcIndex(font.getTtcIndex());
          builder2 = builder2.setFontVariationSettings(font.getAxes());
          Font font1 = builder2.build();
          paramArrayList.add(font1);
          if (paramString1 == null) {
            builder = new FontFamily.Builder(font1);
          } else {
            builder.addFont(font1);
          } 
          continue;
        } catch (IOException iOException1) {
          throw new RuntimeException(iOException1);
        } 
        continue;
      } 
      break;
      i++;
    } 
    if (iOException1 == null) {
      iOException1 = iOException2;
    } else {
      fontFamily = iOException1.build(paramString2, paramInt, false);
    } 
    return fontFamily;
  }
  
  private static void appendNamedFamily(FontConfig.Family paramFamily, HashMap<String, ByteBuffer> paramHashMap, ArrayMap<String, ArrayList<FontFamily>> paramArrayMap, ArrayList<Font> paramArrayList) {
    String str1 = paramFamily.getName();
    List<FontConfig.Font> list = Arrays.asList(paramFamily.getFonts());
    String str2 = paramFamily.getLanguages();
    int i = paramFamily.getVariant();
    FontFamily fontFamily = createFontFamily(str1, list, str2, i, paramHashMap, paramArrayList);
    if (fontFamily == null)
      return; 
    ArrayList<FontFamily> arrayList = new ArrayList();
    arrayList.add(fontFamily);
    paramArrayMap.put(str1, arrayList);
  }
  
  public static FontConfig.Alias[] buildSystemFallback(String paramString1, String paramString2, FontCustomizationParser.Result paramResult, ArrayMap<String, FontFamily[]> paramArrayMap, ArrayList<Font> paramArrayList) {
    try {
      FileInputStream fileInputStream = new FileInputStream();
      this(paramString1);
      FontConfig fontConfig = FontListParser.parse(fileInputStream, paramString2);
      HashMap<Object, Object> hashMap = new HashMap<>();
      this();
      FontConfig.Family[] arrayOfFamily = fontConfig.getFamilies();
      ArrayMap<String, ArrayList<FontFamily>> arrayMap = new ArrayMap();
      this();
      byte b;
      for (int i = arrayOfFamily.length; b < i; ) {
        FontConfig.Family family = arrayOfFamily[b];
        String str = family.getName();
        if (str != null)
          appendNamedFamily(family, (HashMap)hashMap, arrayMap, paramArrayList); 
        b++;
      } 
      for (b = 0; b < paramResult.mAdditionalNamedFamilies.size(); b++)
        appendNamedFamily(paramResult.mAdditionalNamedFamilies.get(b), (HashMap)hashMap, arrayMap, paramArrayList); 
      for (b = 0; b < arrayOfFamily.length; b++) {
        FontConfig.Family family = arrayOfFamily[b];
        if (b == 0 || family.getName() == null)
          pushFamilyToFallback(family, arrayMap, (Map)hashMap, paramArrayList); 
      } 
      for (b = 0; b < arrayMap.size(); b++) {
        String str = (String)arrayMap.keyAt(b);
        List list = (List)arrayMap.valueAt(b);
        FontFamily[] arrayOfFontFamily = (FontFamily[])list.toArray((Object[])new FontFamily[list.size()]);
        paramArrayMap.put(str, arrayOfFontFamily);
      } 
      ArrayList<FontConfig.Alias> arrayList = new ArrayList();
      this();
      arrayList.addAll(Arrays.asList(fontConfig.getAliases()));
      arrayList.addAll(paramResult.mAdditionalAliases);
      return arrayList.<FontConfig.Alias>toArray(new FontConfig.Alias[arrayList.size()]);
    } catch (IOException|XmlPullParserException iOException) {
      Log.e("SystemFonts", "Failed initialize system fallbacks.", iOException);
      return (FontConfig.Alias[])ArrayUtils.emptyArray(FontConfig.Alias.class);
    } 
  }
  
  private static FontCustomizationParser.Result readFontCustomization(String paramString1, String paramString2) {
    try {
      FileInputStream fileInputStream = new FileInputStream();
      this(paramString1);
      try {
        return FontCustomizationParser.parse(fileInputStream, paramString2);
      } finally {
        try {
          fileInputStream.close();
        } finally {
          paramString2 = null;
        } 
      } 
    } catch (IOException iOException) {
      return new FontCustomizationParser.Result();
    } catch (XmlPullParserException xmlPullParserException) {
      Log.e("SystemFonts", "Failed to parse font customization XML", (Throwable)xmlPullParserException);
      return new FontCustomizationParser.Result();
    } 
  }
  
  static {
    ArrayMap<String, FontFamily[]> arrayMap = new ArrayMap();
    ArrayList<Font> arrayList = new ArrayList();
    FontCustomizationParser.Result result = readFontCustomization("/product/etc/fonts_customization.xml", "/product/fonts/");
    sAliases = buildSystemFallback(((IOplusFontManager)OplusFeatureCache.<IOplusFontManager>getOrCreate(IOplusFontManager.DEFAULT, new Object[0])).getSystemFontConfig(), "/system/fonts/", result, arrayMap, arrayList);
    sSystemFallbackMap = Collections.unmodifiableMap((Map)arrayMap);
    sAvailableFonts = Collections.unmodifiableList(arrayList);
  }
}
