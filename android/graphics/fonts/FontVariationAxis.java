package android.graphics.fonts;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Objects;
import java.util.regex.Pattern;

public final class FontVariationAxis {
  private static final Pattern STYLE_VALUE_PATTERN;
  
  public FontVariationAxis(String paramString, float paramFloat) {
    if (isValidTag(paramString)) {
      this.mTag = makeTag(paramString);
      this.mTagString = paramString;
      this.mStyleValue = paramFloat;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Illegal tag pattern: ");
    stringBuilder.append(paramString);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public int getOpenTypeTagValue() {
    return this.mTag;
  }
  
  public String getTag() {
    return this.mTagString;
  }
  
  public float getStyleValue() {
    return this.mStyleValue;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("'");
    stringBuilder.append(this.mTagString);
    stringBuilder.append("' ");
    stringBuilder.append(Float.toString(this.mStyleValue));
    return stringBuilder.toString();
  }
  
  private static final Pattern TAG_PATTERN = Pattern.compile("[ -~]{4}");
  
  private final float mStyleValue;
  
  private final int mTag;
  
  private final String mTagString;
  
  private static boolean isValidTag(String paramString) {
    boolean bool;
    if (paramString != null && TAG_PATTERN.matcher(paramString).matches()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  static {
    STYLE_VALUE_PATTERN = Pattern.compile("-?(([0-9]+(\\.[0-9]+)?)|(\\.[0-9]+))");
  }
  
  private static boolean isValidValueFormat(String paramString) {
    boolean bool;
    if (paramString != null && STYLE_VALUE_PATTERN.matcher(paramString).matches()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static int makeTag(String paramString) {
    char c1 = paramString.charAt(0);
    char c2 = paramString.charAt(1);
    char c3 = paramString.charAt(2);
    char c4 = paramString.charAt(3);
    return c1 << 24 | c2 << 16 | c3 << 8 | c4;
  }
  
  public static FontVariationAxis[] fromFontVariationSettings(String paramString) {
    if (paramString == null || paramString.isEmpty())
      return null; 
    ArrayList<FontVariationAxis> arrayList = new ArrayList();
    int i = paramString.length();
    int j = 0;
    while (true) {
      if (j < i) {
        char c = paramString.charAt(j);
        if (!Character.isWhitespace(c)) {
          if ((c == '\'' || c == '"') && i >= j + 6 && paramString.charAt(j + 5) == c) {
            String str = paramString.substring(j + 1, j + 5);
            int k = j + 6;
            int m = paramString.indexOf(',', k);
            j = m;
            if (m == -1)
              j = i; 
            try {
              float f = Float.parseFloat(paramString.substring(k, j));
              arrayList.add(new FontVariationAxis(str, f));
              j++;
            } catch (NumberFormatException numberFormatException) {
              StringBuilder stringBuilder1 = new StringBuilder();
              stringBuilder1.append("Failed to parse float string: ");
              stringBuilder1.append(numberFormatException.getMessage());
              throw new IllegalArgumentException(stringBuilder1.toString());
            } 
            continue;
          } 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Tag should be wrapped with double or single quote: ");
          stringBuilder.append((String)numberFormatException);
          throw new IllegalArgumentException(stringBuilder.toString());
        } 
      } else {
        break;
      } 
      j++;
    } 
    if (arrayList.isEmpty())
      return null; 
    return arrayList.<FontVariationAxis>toArray(new FontVariationAxis[0]);
  }
  
  public static String toFontVariationSettings(FontVariationAxis[] paramArrayOfFontVariationAxis) {
    if (paramArrayOfFontVariationAxis == null)
      return ""; 
    return TextUtils.join(",", (Object[])paramArrayOfFontVariationAxis);
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (paramObject == this)
      return true; 
    if (paramObject == null || !(paramObject instanceof FontVariationAxis))
      return false; 
    paramObject = paramObject;
    if (((FontVariationAxis)paramObject).mTag != this.mTag || ((FontVariationAxis)paramObject).mStyleValue != this.mStyleValue)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mTag), Float.valueOf(this.mStyleValue) });
  }
}
