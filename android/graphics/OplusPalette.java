package android.graphics;

import android.os.AsyncTask;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

public class OplusPalette {
  private static final String LOG_TAG = "OplusPalette";
  
  private List<Integer> mSwatches;
  
  OplusPalette(List<Integer> paramList) {
    this.mSwatches = paramList;
  }
  
  public static Builder from(Bitmap paramBitmap) {
    return new Builder(paramBitmap);
  }
  
  public List<Integer> getSortedColorList() {
    return this.mSwatches;
  }
  
  public int getTransMaxColor(int paramInt) {
    int i;
    float[] arrayOfFloat = new float[3];
    Iterator<Integer> iterator = this.mSwatches.iterator();
    while (true) {
      i = paramInt;
      if (iterator.hasNext()) {
        i = ((Integer)iterator.next()).intValue();
        Color.colorToHSV(i, arrayOfFloat);
        float f1 = arrayOfFloat[1];
        float f2 = arrayOfFloat[2];
        if ((f1 >= 0.0F && f1 <= 0.05D && f2 >= 0.0F && f2 <= 1.0F) || (f1 >= 0.0F && f1 <= 1.0F && f2 >= 0.0F && f2 <= 0.05D))
          continue; 
        i = Color.HSVToColor(arrayOfFloat);
      } 
      break;
    } 
    return i;
  }
  
  public int[] getTransTwoColor() {
    int[] arrayOfInt = new int[2];
    float[] arrayOfFloat = new float[3];
    boolean bool1 = false;
    boolean bool2 = false;
    int i = -16777216;
    int j = -1;
    for (Iterator<Integer> iterator = this.mSwatches.iterator(); iterator.hasNext(); ) {
      int n = ((Integer)iterator.next()).intValue();
      Color.colorToHSV(n, arrayOfFloat);
      if (bool1 && bool2)
        break; 
      Color.colorToHSV(0xFFFFFF & n | 0xFF000000, arrayOfFloat);
      float f1 = arrayOfFloat[1];
      float f2 = arrayOfFloat[2];
      if ((f1 >= 0.0F && f1 <= 0.05D && f2 >= 0.0F && f2 <= 1.0F) || (f1 >= 0.0F && f1 <= 1.0F && f2 >= 0.0F && f2 <= 0.05D))
        continue; 
      if (!bool1) {
        arrayOfFloat[1] = 0.2F;
        arrayOfFloat[2] = 1.0F;
        i = Color.HSVToColor(arrayOfFloat);
        bool1 = true;
        continue;
      } 
      if (!bool2) {
        arrayOfFloat[1] = 0.05F;
        arrayOfFloat[2] = 1.0F;
        j = Color.HSVToColor(arrayOfFloat);
        bool2 = true;
      } 
    } 
    boolean bool3 = bool2;
    int m = i, k = j;
    if (!bool2) {
      bool3 = bool2;
      m = i;
      k = j;
      if (!bool1) {
        arrayOfFloat[0] = 0.0F;
        arrayOfFloat[1] = 0.0F;
        arrayOfFloat[2] = 0.9F;
        m = Color.HSVToColor(arrayOfFloat);
        arrayOfFloat[2] = 0.98F;
        k = Color.HSVToColor(arrayOfFloat);
        bool3 = true;
      } 
    } 
    if (!bool3) {
      Color.colorToHSV(m, arrayOfFloat);
      arrayOfFloat[1] = 0.05F;
      arrayOfFloat[2] = 1.0F;
      k = Color.HSVToColor(arrayOfFloat);
    } 
    arrayOfInt[0] = m;
    arrayOfInt[1] = k;
    return arrayOfInt;
  }
  
  class Builder {
    private final Bitmap mBitmap;
    
    public Builder(OplusPalette this$0) {
      if (this$0 != null && !this$0.isRecycled()) {
        this.mBitmap = (Bitmap)this$0;
        return;
      } 
      throw new IllegalArgumentException("Bitmap is not valid");
    }
    
    private int[] getPixelsInStep(int param1Int) {
      if (param1Int <= 0)
        return null; 
      int i = (int)(this.mBitmap.getWidth() * 1.0F / param1Int);
      int j = (int)(this.mBitmap.getHeight() * 1.0F / param1Int);
      int[] arrayOfInt = new int[i * j];
      int k;
      for (k = (int)(param1Int / 2.0F); k < this.mBitmap.getWidth(); k += param1Int) {
        int m;
        for (m = (int)(param1Int / 2.0F); m < this.mBitmap.getHeight(); m += param1Int) {
          int n = k / param1Int;
          int i1 = m / param1Int;
          if (n < i && i1 < j)
            arrayOfInt[i1 * i + n] = this.mBitmap.getPixel(k, m); 
        } 
      } 
      return arrayOfInt;
    }
    
    private int[] getEdgePixelsInStep(int param1Int1, int param1Int2) {
      if (param1Int1 <= 0 || param1Int2 <= 0)
        return null; 
      int i = (int)(this.mBitmap.getWidth() * 1.0F / param1Int1);
      int j = (int)(this.mBitmap.getHeight() * 1.0F / param1Int1);
      int[] arrayOfInt = new int[(i + j) * 2];
      int k;
      for (k = (int)(param1Int1 / 2.0F); k < this.mBitmap.getWidth(); k += param1Int1) {
        int m = k / param1Int1;
        if (m < i)
          arrayOfInt[m] = this.mBitmap.getPixel(k, param1Int2); 
      } 
      for (k = (int)(param1Int1 / 2.0F); k < this.mBitmap.getWidth(); k += param1Int1) {
        int m = k / param1Int1;
        if (m < i) {
          Bitmap bitmap = this.mBitmap;
          arrayOfInt[m + i] = bitmap.getPixel(k, bitmap.getHeight() - param1Int2);
        } 
      } 
      for (k = (int)(param1Int1 / 2.0F); k < this.mBitmap.getHeight(); k += param1Int1) {
        int m = k / param1Int1;
        if (m < j)
          arrayOfInt[i * 2 + m] = this.mBitmap.getPixel(param1Int2, k); 
      } 
      for (k = (int)(param1Int1 / 2.0F); k < this.mBitmap.getHeight(); k += param1Int1) {
        int m = k / param1Int1;
        if (m < j) {
          Bitmap bitmap = this.mBitmap;
          arrayOfInt[i * 2 + j + m] = bitmap.getPixel(bitmap.getWidth() - param1Int2, k);
        } 
      } 
      return arrayOfInt;
    }
    
    private int[] getEveryPixels() {
      int i = this.mBitmap.getWidth();
      int j = this.mBitmap.getHeight();
      int[] arrayOfInt = new int[i * j];
      this.mBitmap.getPixels(arrayOfInt, 0, i, 0, 0, i, j);
      return arrayOfInt;
    }
    
    private List<Integer> genetateSwatches(int[] param1ArrayOfint) {
      OplusSmartCutQuantizer oplusSmartCutQuantizer = new OplusSmartCutQuantizer(param1ArrayOfint);
      HashMap<Integer, Integer> hashMap = oplusSmartCutQuantizer.getQuantizedColors();
      ArrayList<?> arrayList = new ArrayList();
      arrayList.addAll(hashMap.entrySet());
      Collections.sort(arrayList, new Comparator<Map.Entry<Integer, Integer>>() {
            final OplusPalette.Builder this$0;
            
            public int compare(Map.Entry<Integer, Integer> param2Entry1, Map.Entry<Integer, Integer> param2Entry2) {
              return ((Integer)param2Entry2.getValue()).intValue() - ((Integer)param2Entry1.getValue()).intValue();
            }
          });
      ArrayList<Integer> arrayList1 = new ArrayList();
      for (byte b = 0; b < arrayList.size(); b++)
        arrayList1.add((Integer)((Map.Entry)arrayList.get(b)).getKey()); 
      return arrayList1;
    }
    
    public OplusPalette generate() {
      List<Integer> list = genetateSwatches(getEveryPixels());
      return new OplusPalette(list);
    }
    
    public OplusPalette generateWithStep(int param1Int) {
      List<Integer> list = genetateSwatches(getPixelsInStep(param1Int));
      return new OplusPalette(list);
    }
    
    public OplusPalette generateEdageWithStep(int param1Int1, int param1Int2) {
      List<Integer> list = genetateSwatches(getEdgePixelsInStep(param1Int1, param1Int2));
      return new OplusPalette(list);
    }
    
    public AsyncTask<Bitmap, Void, OplusPalette> generate(OplusPalette.PaletteAsyncListener param1PaletteAsyncListener) {
      if (param1PaletteAsyncListener != null) {
        null = new Object(this, param1PaletteAsyncListener);
        Executor executor = AsyncTask.THREAD_POOL_EXECUTOR;
        Bitmap bitmap = this.mBitmap;
        return 













          
          null.executeOnExecutor(executor, (Object[])new Bitmap[] { bitmap });
      } 
      throw new IllegalArgumentException("listener can not be null");
    }
  }
  
  class PaletteAsyncListener {
    public abstract void onGenerated(OplusPalette param1OplusPalette);
  }
}
