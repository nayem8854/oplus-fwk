package android.graphics;

import java.util.List;

public class ComposeShader extends Shader {
  private long mNativeInstanceShaderA;
  
  private long mNativeInstanceShaderB;
  
  private int mPorterDuffMode;
  
  Shader mShaderA;
  
  Shader mShaderB;
  
  @Deprecated
  public ComposeShader(Shader paramShader1, Shader paramShader2, Xfermode paramXfermode) {
    this(paramShader1, paramShader2, paramXfermode.porterDuffMode);
  }
  
  public ComposeShader(Shader paramShader1, Shader paramShader2, PorterDuff.Mode paramMode) {
    this(paramShader1, paramShader2, paramMode.nativeInt);
  }
  
  public ComposeShader(Shader paramShader1, Shader paramShader2, BlendMode paramBlendMode) {
    this(paramShader1, paramShader2, (paramBlendMode.getXfermode()).porterDuffMode);
  }
  
  private ComposeShader(Shader paramShader1, Shader paramShader2, int paramInt) {
    if (paramShader1 != null && paramShader2 != null) {
      this.mShaderA = paramShader1;
      this.mShaderB = paramShader2;
      this.mPorterDuffMode = paramInt;
      return;
    } 
    throw new IllegalArgumentException("Shader parameters must not be null");
  }
  
  long createNativeInstance(long paramLong) {
    this.mNativeInstanceShaderA = this.mShaderA.getNativeInstance();
    this.mNativeInstanceShaderB = this.mShaderB.getNativeInstance();
    Shader shader = this.mShaderA;
    long l1 = shader.getNativeInstance(), l2 = this.mShaderB.getNativeInstance();
    int i = this.mPorterDuffMode;
    return nativeCreate(paramLong, l1, l2, i);
  }
  
  protected void verifyNativeInstance() {
    if (this.mShaderA.getNativeInstance() == this.mNativeInstanceShaderA) {
      Shader shader = this.mShaderB;
      if (shader.getNativeInstance() != this.mNativeInstanceShaderB) {
        discardNativeInstance();
        return;
      } 
      return;
    } 
    discardNativeInstance();
  }
  
  public List<long[]> getComposeShaderColor() {
    return getComposeShaderColor(this.mShaderA, this.mShaderB);
  }
  
  public void setComposeShaderColor(List<long[]> paramList) {
    setComposeShaderColor(this.mShaderA, this.mShaderB, paramList);
  }
  
  private static native long nativeCreate(long paramLong1, long paramLong2, long paramLong3, int paramInt);
}
