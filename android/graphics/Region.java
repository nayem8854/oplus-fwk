package android.graphics;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pools;

public class Region implements Parcelable {
  public static final Parcelable.Creator<Region> CREATOR;
  
  private static final int MAX_POOL_SIZE = 10;
  
  private static final Pools.SynchronizedPool<Region> sPool = new Pools.SynchronizedPool(10);
  
  public long mNativeRegion;
  
  class Op extends Enum<Op> {
    private static final Op[] $VALUES;
    
    public static Op valueOf(String param1String) {
      return Enum.<Op>valueOf(Op.class, param1String);
    }
    
    public static Op[] values() {
      return (Op[])$VALUES.clone();
    }
    
    public static final Op DIFFERENCE = new Op("DIFFERENCE", 0, 0);
    
    public static final Op INTERSECT = new Op("INTERSECT", 1, 1);
    
    public static final Op REPLACE;
    
    public static final Op REVERSE_DIFFERENCE = new Op("REVERSE_DIFFERENCE", 4, 4);
    
    public static final Op UNION = new Op("UNION", 2, 2);
    
    public static final Op XOR = new Op("XOR", 3, 3);
    
    public final int nativeInt;
    
    static {
      Op op = new Op("REPLACE", 5, 5);
      $VALUES = new Op[] { DIFFERENCE, INTERSECT, UNION, XOR, REVERSE_DIFFERENCE, op };
    }
    
    private Op(Region this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.nativeInt = param1Int2;
    }
  }
  
  public Region() {
    this(nativeConstructor());
  }
  
  public Region(Region paramRegion) {
    this(nativeConstructor());
    nativeSetRegion(this.mNativeRegion, paramRegion.mNativeRegion);
  }
  
  public Region(Rect paramRect) {
    long l = nativeConstructor();
    nativeSetRect(l, paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
  }
  
  public Region(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    long l = nativeConstructor();
    nativeSetRect(l, paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public void setEmpty() {
    nativeSetRect(this.mNativeRegion, 0, 0, 0, 0);
  }
  
  public boolean set(Region paramRegion) {
    nativeSetRegion(this.mNativeRegion, paramRegion.mNativeRegion);
    return true;
  }
  
  public boolean set(Rect paramRect) {
    return nativeSetRect(this.mNativeRegion, paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
  }
  
  public boolean set(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    return nativeSetRect(this.mNativeRegion, paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public boolean setPath(Path paramPath, Region paramRegion) {
    return nativeSetPath(this.mNativeRegion, paramPath.readOnlyNI(), paramRegion.mNativeRegion);
  }
  
  public Rect getBounds() {
    Rect rect = new Rect();
    nativeGetBounds(this.mNativeRegion, rect);
    return rect;
  }
  
  public boolean getBounds(Rect paramRect) {
    if (paramRect != null)
      return nativeGetBounds(this.mNativeRegion, paramRect); 
    throw null;
  }
  
  public Path getBoundaryPath() {
    Path path = new Path();
    nativeGetBoundaryPath(this.mNativeRegion, path.mutateNI());
    return path;
  }
  
  public boolean getBoundaryPath(Path paramPath) {
    return nativeGetBoundaryPath(this.mNativeRegion, paramPath.mutateNI());
  }
  
  public boolean quickContains(Rect paramRect) {
    return quickContains(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
  }
  
  public boolean quickReject(Rect paramRect) {
    return quickReject(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
  }
  
  public void translate(int paramInt1, int paramInt2) {
    translate(paramInt1, paramInt2, null);
  }
  
  public void scale(float paramFloat) {
    scale(paramFloat, null);
  }
  
  public final boolean union(Rect paramRect) {
    return op(paramRect, Op.UNION);
  }
  
  public boolean op(Rect paramRect, Op paramOp) {
    return nativeOp(this.mNativeRegion, paramRect.left, paramRect.top, paramRect.right, paramRect.bottom, paramOp.nativeInt);
  }
  
  public boolean op(int paramInt1, int paramInt2, int paramInt3, int paramInt4, Op paramOp) {
    return nativeOp(this.mNativeRegion, paramInt1, paramInt2, paramInt3, paramInt4, paramOp.nativeInt);
  }
  
  public boolean op(Region paramRegion, Op paramOp) {
    return op(this, paramRegion, paramOp);
  }
  
  public boolean op(Rect paramRect, Region paramRegion, Op paramOp) {
    return nativeOp(this.mNativeRegion, paramRect, paramRegion.mNativeRegion, paramOp.nativeInt);
  }
  
  public boolean op(Region paramRegion1, Region paramRegion2, Op paramOp) {
    return nativeOp(this.mNativeRegion, paramRegion1.mNativeRegion, paramRegion2.mNativeRegion, paramOp.nativeInt);
  }
  
  public String toString() {
    return nativeToString(this.mNativeRegion);
  }
  
  public static Region obtain() {
    Region region = (Region)sPool.acquire();
    if (region == null)
      region = new Region(); 
    return region;
  }
  
  public static Region obtain(Region paramRegion) {
    Region region = obtain();
    region.set(paramRegion);
    return region;
  }
  
  public void recycle() {
    setEmpty();
    sPool.release(this);
  }
  
  static {
    CREATOR = new Parcelable.Creator<Region>() {
        public Region createFromParcel(Parcel param1Parcel) {
          long l = Region.nativeCreateFromParcel(param1Parcel);
          if (l != 0L)
            return new Region(l); 
          throw new RuntimeException();
        }
        
        public Region[] newArray(int param1Int) {
          return new Region[param1Int];
        }
      };
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (nativeWriteToParcel(this.mNativeRegion, paramParcel))
      return; 
    throw new RuntimeException();
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject == null || !(paramObject instanceof Region))
      return false; 
    paramObject = paramObject;
    return nativeEquals(this.mNativeRegion, ((Region)paramObject).mNativeRegion);
  }
  
  protected void finalize() throws Throwable {
    try {
      nativeDestructor(this.mNativeRegion);
      this.mNativeRegion = 0L;
      return;
    } finally {
      super.finalize();
    } 
  }
  
  Region(long paramLong) {
    if (paramLong != 0L) {
      this.mNativeRegion = paramLong;
      return;
    } 
    throw new RuntimeException();
  }
  
  private Region(long paramLong, int paramInt) {
    this(paramLong);
  }
  
  final long ni() {
    return this.mNativeRegion;
  }
  
  private static native long nativeConstructor();
  
  private static native long nativeCreateFromParcel(Parcel paramParcel);
  
  private static native void nativeDestructor(long paramLong);
  
  private static native boolean nativeEquals(long paramLong1, long paramLong2);
  
  private static native boolean nativeGetBoundaryPath(long paramLong1, long paramLong2);
  
  private static native boolean nativeGetBounds(long paramLong, Rect paramRect);
  
  private static native boolean nativeOp(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5);
  
  private static native boolean nativeOp(long paramLong1, long paramLong2, long paramLong3, int paramInt);
  
  private static native boolean nativeOp(long paramLong1, Rect paramRect, long paramLong2, int paramInt);
  
  private static native boolean nativeSetPath(long paramLong1, long paramLong2, long paramLong3);
  
  private static native boolean nativeSetRect(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  private static native void nativeSetRegion(long paramLong1, long paramLong2);
  
  private static native String nativeToString(long paramLong);
  
  private static native boolean nativeWriteToParcel(long paramLong, Parcel paramParcel);
  
  public native boolean contains(int paramInt1, int paramInt2);
  
  public native boolean isComplex();
  
  public native boolean isEmpty();
  
  public native boolean isRect();
  
  public native boolean quickContains(int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  public native boolean quickReject(int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  public native boolean quickReject(Region paramRegion);
  
  public native void scale(float paramFloat, Region paramRegion);
  
  public native void translate(int paramInt1, int paramInt2, Region paramRegion);
}
