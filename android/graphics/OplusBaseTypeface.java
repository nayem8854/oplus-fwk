package android.graphics;

import java.util.Map;

public class OplusBaseTypeface {
  protected static OplusBaseTypeface sInstance;
  
  public boolean isLikeDefault = false;
  
  public static void addIsLikeDefaultFlag(Map<String, Typeface> paramMap) {
    if (paramMap == null || Typeface.sDefaults == null)
      return; 
    for (Map.Entry<String, Typeface> entry : paramMap.entrySet()) {
      Typeface typeface = (Typeface)entry.getValue();
      OplusBaseTypeface oplusBaseTypeface = getBaseType(typeface);
      if (oplusBaseTypeface != null)
        oplusBaseTypeface.isLikeDefault = true; 
    } 
    for (Typeface typeface : Typeface.sDefaults) {
      OplusBaseTypeface oplusBaseTypeface = getBaseType(typeface);
      if (oplusBaseTypeface != null)
        oplusBaseTypeface.isLikeDefault = true; 
    } 
  }
  
  public static OplusBaseTypeface getBaseType(Typeface paramTypeface) {
    OplusBaseTypeface oplusBaseTypeface = sInstance;
    if (oplusBaseTypeface != null) {
      OplusBaseTypeface oplusBaseTypeface1 = oplusBaseTypeface.getOplusBaseTypeface(paramTypeface);
    } else {
      paramTypeface = null;
    } 
    return paramTypeface;
  }
  
  public OplusBaseTypeface getOplusBaseTypeface(Typeface paramTypeface) {
    return null;
  }
}
