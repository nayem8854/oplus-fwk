package android.graphics;

public class DashPathEffect extends PathEffect {
  public DashPathEffect(float[] paramArrayOffloat, float paramFloat) {
    if (paramArrayOffloat.length >= 2) {
      this.native_instance = nativeCreate(paramArrayOffloat, paramFloat);
      return;
    } 
    throw new ArrayIndexOutOfBoundsException();
  }
  
  private static native long nativeCreate(float[] paramArrayOffloat, float paramFloat);
}
