package android.graphics.pdf;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.ParcelFileDescriptor;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import dalvik.system.CloseGuard;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import libcore.io.IoUtils;

public final class PdfRenderer implements AutoCloseable {
  static final Object sPdfiumLock = new Object();
  
  private final CloseGuard mCloseGuard = CloseGuard.get();
  
  private Page mCurrentPage;
  
  private ParcelFileDescriptor mInput;
  
  private long mNativeDocument;
  
  private final int mPageCount;
  
  private final Point mTempPoint = new Point();
  
  public PdfRenderer(ParcelFileDescriptor paramParcelFileDescriptor) throws IOException {
    if (paramParcelFileDescriptor != null)
      try {
        Os.lseek(paramParcelFileDescriptor.getFileDescriptor(), 0L, OsConstants.SEEK_SET);
        long l = (Os.fstat(paramParcelFileDescriptor.getFileDescriptor())).st_size;
        this.mInput = paramParcelFileDescriptor;
        synchronized (sPdfiumLock) {
          this.mNativeDocument = l = nativeCreate(this.mInput.getFd(), l);
          try {
            return;
          } finally {
            nativeClose(this.mNativeDocument);
            this.mNativeDocument = 0L;
          } 
        } 
      } catch (ErrnoException errnoException) {
        throw new IllegalArgumentException("file descriptor not seekable");
      }  
    throw new NullPointerException("input cannot be null");
  }
  
  public void close() {
    throwIfClosed();
    throwIfPageOpened();
    doClose();
  }
  
  public int getPageCount() {
    throwIfClosed();
    return this.mPageCount;
  }
  
  public boolean shouldScaleForPrinting() {
    throwIfClosed();
    synchronized (sPdfiumLock) {
      return nativeScaleForPrinting(this.mNativeDocument);
    } 
  }
  
  public Page openPage(int paramInt) {
    throwIfClosed();
    throwIfPageOpened();
    throwIfPageNotInDocument(paramInt);
    Page page = new Page(paramInt);
    return page;
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mCloseGuard != null)
        this.mCloseGuard.warnIfOpen(); 
      doClose();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  private void doClose() {
    Page page = this.mCurrentPage;
    if (page != null) {
      page.close();
      this.mCurrentPage = null;
    } 
    if (this.mNativeDocument != 0L)
      synchronized (sPdfiumLock) {
        nativeClose(this.mNativeDocument);
        this.mNativeDocument = 0L;
      }  
    ParcelFileDescriptor parcelFileDescriptor = this.mInput;
    if (parcelFileDescriptor != null) {
      IoUtils.closeQuietly((AutoCloseable)parcelFileDescriptor);
      this.mInput = null;
    } 
    this.mCloseGuard.close();
  }
  
  private void throwIfClosed() {
    if (this.mInput != null)
      return; 
    throw new IllegalStateException("Already closed");
  }
  
  private void throwIfPageOpened() {
    if (this.mCurrentPage == null)
      return; 
    throw new IllegalStateException("Current page not closed");
  }
  
  private void throwIfPageNotInDocument(int paramInt) {
    if (paramInt >= 0 && paramInt < this.mPageCount)
      return; 
    throw new IllegalArgumentException("Invalid page index");
  }
  
  private static native void nativeClose(long paramLong);
  
  private static native void nativeClosePage(long paramLong);
  
  private static native long nativeCreate(int paramInt, long paramLong);
  
  private static native int nativeGetPageCount(long paramLong);
  
  private static native long nativeOpenPageAndGetSize(long paramLong, int paramInt, Point paramPoint);
  
  private static native void nativeRenderPage(long paramLong1, long paramLong2, long paramLong3, int paramInt1, int paramInt2, int paramInt3, int paramInt4, long paramLong4, int paramInt5);
  
  private static native boolean nativeScaleForPrinting(long paramLong);
  
  public final class Page implements AutoCloseable {
    public static final int RENDER_MODE_FOR_DISPLAY = 1;
    
    public static final int RENDER_MODE_FOR_PRINT = 2;
    
    private final CloseGuard mCloseGuard = CloseGuard.get();
    
    private final int mHeight;
    
    private final int mIndex;
    
    private long mNativePage;
    
    private final int mWidth;
    
    final PdfRenderer this$0;
    
    private Page(int param1Int) {
      Point point = PdfRenderer.this.mTempPoint;
      synchronized (PdfRenderer.sPdfiumLock) {
        this.mNativePage = PdfRenderer.nativeOpenPageAndGetSize(PdfRenderer.this.mNativeDocument, param1Int, point);
        this.mIndex = param1Int;
        this.mWidth = point.x;
        this.mHeight = point.y;
        this.mCloseGuard.open("close");
        return;
      } 
    }
    
    public int getIndex() {
      return this.mIndex;
    }
    
    public int getWidth() {
      return this.mWidth;
    }
    
    public int getHeight() {
      return this.mHeight;
    }
    
    public void render(Bitmap param1Bitmap, Rect param1Rect, Matrix param1Matrix, int param1Int) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mNativePage : J
      //   4: lconst_0
      //   5: lcmp
      //   6: ifeq -> 383
      //   9: aload_1
      //   10: ldc 'bitmap null'
      //   12: invokestatic checkNotNull : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   15: checkcast android/graphics/Bitmap
      //   18: astore #5
      //   20: aload #5
      //   22: invokevirtual getConfig : ()Landroid/graphics/Bitmap$Config;
      //   25: getstatic android/graphics/Bitmap$Config.ARGB_8888 : Landroid/graphics/Bitmap$Config;
      //   28: if_acmpne -> 373
      //   31: aload_2
      //   32: ifnull -> 94
      //   35: aload_2
      //   36: getfield left : I
      //   39: iflt -> 84
      //   42: aload_2
      //   43: getfield top : I
      //   46: iflt -> 84
      //   49: aload_2
      //   50: getfield right : I
      //   53: istore #6
      //   55: iload #6
      //   57: aload #5
      //   59: invokevirtual getWidth : ()I
      //   62: if_icmpgt -> 84
      //   65: aload_2
      //   66: getfield bottom : I
      //   69: istore #6
      //   71: iload #6
      //   73: aload #5
      //   75: invokevirtual getHeight : ()I
      //   78: if_icmpgt -> 84
      //   81: goto -> 94
      //   84: new java/lang/IllegalArgumentException
      //   87: dup
      //   88: ldc 'destBounds not in destination'
      //   90: invokespecial <init> : (Ljava/lang/String;)V
      //   93: athrow
      //   94: aload_3
      //   95: ifnull -> 118
      //   98: aload_3
      //   99: invokevirtual isAffine : ()Z
      //   102: ifeq -> 108
      //   105: goto -> 118
      //   108: new java/lang/IllegalArgumentException
      //   111: dup
      //   112: ldc 'transform not affine'
      //   114: invokespecial <init> : (Ljava/lang/String;)V
      //   117: athrow
      //   118: iload #4
      //   120: iconst_2
      //   121: if_icmpeq -> 143
      //   124: iload #4
      //   126: iconst_1
      //   127: if_icmpne -> 133
      //   130: goto -> 143
      //   133: new java/lang/IllegalArgumentException
      //   136: dup
      //   137: ldc 'Unsupported render mode'
      //   139: invokespecial <init> : (Ljava/lang/String;)V
      //   142: athrow
      //   143: iload #4
      //   145: iconst_2
      //   146: if_icmpne -> 168
      //   149: iload #4
      //   151: iconst_1
      //   152: if_icmpeq -> 158
      //   155: goto -> 168
      //   158: new java/lang/IllegalArgumentException
      //   161: dup
      //   162: ldc 'Only single render mode supported'
      //   164: invokespecial <init> : (Ljava/lang/String;)V
      //   167: athrow
      //   168: iconst_0
      //   169: istore #7
      //   171: aload_2
      //   172: ifnull -> 184
      //   175: aload_2
      //   176: getfield left : I
      //   179: istore #6
      //   181: goto -> 187
      //   184: iconst_0
      //   185: istore #6
      //   187: aload_2
      //   188: ifnull -> 197
      //   191: aload_2
      //   192: getfield top : I
      //   195: istore #7
      //   197: aload_2
      //   198: ifnull -> 210
      //   201: aload_2
      //   202: getfield right : I
      //   205: istore #8
      //   207: goto -> 217
      //   210: aload #5
      //   212: invokevirtual getWidth : ()I
      //   215: istore #8
      //   217: aload_2
      //   218: ifnull -> 230
      //   221: aload_2
      //   222: getfield bottom : I
      //   225: istore #9
      //   227: goto -> 237
      //   230: aload #5
      //   232: invokevirtual getHeight : ()I
      //   235: istore #9
      //   237: aload_3
      //   238: ifnonnull -> 304
      //   241: new android/graphics/Matrix
      //   244: dup
      //   245: invokespecial <init> : ()V
      //   248: astore_1
      //   249: iload #8
      //   251: iload #6
      //   253: isub
      //   254: i2f
      //   255: aload_0
      //   256: invokevirtual getWidth : ()I
      //   259: i2f
      //   260: fdiv
      //   261: fstore #10
      //   263: iload #9
      //   265: iload #7
      //   267: isub
      //   268: i2f
      //   269: fstore #11
      //   271: fload #11
      //   273: aload_0
      //   274: invokevirtual getHeight : ()I
      //   277: i2f
      //   278: fdiv
      //   279: fstore #11
      //   281: aload_1
      //   282: fload #10
      //   284: fload #11
      //   286: invokevirtual postScale : (FF)Z
      //   289: pop
      //   290: aload_1
      //   291: iload #6
      //   293: i2f
      //   294: iload #7
      //   296: i2f
      //   297: invokevirtual postTranslate : (FF)Z
      //   300: pop
      //   301: goto -> 306
      //   304: aload_3
      //   305: astore_1
      //   306: aload_1
      //   307: getfield native_instance : J
      //   310: lstore #12
      //   312: getstatic android/graphics/pdf/PdfRenderer.sPdfiumLock : Ljava/lang/Object;
      //   315: astore_2
      //   316: aload_2
      //   317: monitorenter
      //   318: aload_0
      //   319: getfield this$0 : Landroid/graphics/pdf/PdfRenderer;
      //   322: invokestatic access$200 : (Landroid/graphics/pdf/PdfRenderer;)J
      //   325: lstore #14
      //   327: aload_0
      //   328: getfield mNativePage : J
      //   331: lstore #16
      //   333: aload #5
      //   335: invokevirtual getNativeInstance : ()J
      //   338: lstore #18
      //   340: lload #14
      //   342: lload #16
      //   344: lload #18
      //   346: iload #6
      //   348: iload #7
      //   350: iload #8
      //   352: iload #9
      //   354: lload #12
      //   356: iload #4
      //   358: invokestatic access$400 : (JJJIIIIJI)V
      //   361: aload_2
      //   362: monitorexit
      //   363: return
      //   364: astore_1
      //   365: aload_2
      //   366: monitorexit
      //   367: aload_1
      //   368: athrow
      //   369: astore_1
      //   370: goto -> 365
      //   373: new java/lang/IllegalArgumentException
      //   376: dup
      //   377: ldc 'Unsupported pixel format'
      //   379: invokespecial <init> : (Ljava/lang/String;)V
      //   382: athrow
      //   383: aconst_null
      //   384: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #389	-> 0
      //   #393	-> 9
      //   #395	-> 20
      //   #399	-> 31
      //   #400	-> 35
      //   #401	-> 55
      //   #402	-> 71
      //   #403	-> 84
      //   #407	-> 94
      //   #408	-> 108
      //   #411	-> 118
      //   #412	-> 133
      //   #415	-> 143
      //   #416	-> 158
      //   #419	-> 168
      //   #420	-> 187
      //   #421	-> 197
      //   #422	-> 210
      //   #423	-> 217
      //   #424	-> 230
      //   #427	-> 237
      //   #428	-> 241
      //   #429	-> 241
      //   #431	-> 241
      //   #432	-> 249
      //   #433	-> 271
      //   #432	-> 281
      //   #434	-> 290
      //   #427	-> 304
      //   #437	-> 306
      //   #439	-> 312
      //   #440	-> 318
      //   #443	-> 361
      //   #444	-> 363
      //   #443	-> 364
      //   #396	-> 373
      //   #390	-> 383
      // Exception table:
      //   from	to	target	type
      //   318	340	364	finally
      //   340	361	369	finally
      //   361	363	369	finally
      //   365	367	369	finally
    }
    
    public void close() {
      throwIfClosed();
      doClose();
    }
    
    protected void finalize() throws Throwable {
      try {
        if (this.mCloseGuard != null)
          this.mCloseGuard.warnIfOpen(); 
        doClose();
        return;
      } finally {
        super.finalize();
      } 
    }
    
    private void doClose() {
      if (this.mNativePage != 0L)
        synchronized (PdfRenderer.sPdfiumLock) {
          PdfRenderer.nativeClosePage(this.mNativePage);
          this.mNativePage = 0L;
        }  
      this.mCloseGuard.close();
      PdfRenderer.access$602(PdfRenderer.this, null);
    }
    
    private void throwIfClosed() {
      if (this.mNativePage != 0L)
        return; 
      throw new IllegalStateException("Already closed");
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface RenderMode {}
}
