package android.graphics;

import android.graphics.text.MeasuredText;
import android.text.GraphicsOperations;
import android.text.MeasuredParagraph;
import android.text.PrecomputedText;
import android.text.TextUtils;

public abstract class BaseCanvas extends OplusBaseBaseCanvas {
  protected int mScreenDensity = 0;
  
  protected long mNativeCanvasWrapper;
  
  protected int mDensity = 0;
  
  private boolean mAllowHwBitmapsInSwMode = false;
  
  protected void throwIfCannotDraw(Bitmap paramBitmap) {
    if (!paramBitmap.isRecycled()) {
      if (paramBitmap.isPremultiplied() || paramBitmap.getConfig() != Bitmap.Config.ARGB_8888 || 
        !paramBitmap.hasAlpha()) {
        throwIfHwBitmapInSwMode(paramBitmap);
        return;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Canvas: trying to use a non-premultiplied bitmap ");
      stringBuilder1.append(paramBitmap);
      throw new RuntimeException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Canvas: trying to use a recycled bitmap ");
    stringBuilder.append(paramBitmap);
    throw new RuntimeException(stringBuilder.toString());
  }
  
  protected static final void checkRange(int paramInt1, int paramInt2, int paramInt3) {
    if ((paramInt2 | paramInt3) >= 0 && paramInt2 + paramInt3 <= paramInt1)
      return; 
    throw new ArrayIndexOutOfBoundsException();
  }
  
  public boolean isHardwareAccelerated() {
    return false;
  }
  
  public void drawArc(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, boolean paramBoolean, Paint paramPaint) {
    throwIfHasHwBitmapInSwMode(paramPaint);
    OplusBaseBaseCanvas.Entity entity = changeArea(paramPaint, getRectF(paramFloat1, paramFloat2, paramFloat3, paramFloat4));
    long l1 = this.mNativeCanvasWrapper;
    long l2 = paramPaint.getNativeInstance();
    nDrawArc(l1, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6, paramBoolean, l2);
    resetEntity(entity, paramPaint);
  }
  
  public void drawArc(RectF paramRectF, float paramFloat1, float paramFloat2, boolean paramBoolean, Paint paramPaint) {
    throwIfHasHwBitmapInSwMode(paramPaint);
    drawArc(paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramFloat1, paramFloat2, paramBoolean, paramPaint);
  }
  
  public void drawARGB(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    drawColor(Color.argb(paramInt1, paramInt2, paramInt3, paramInt4));
  }
  
  public void drawBitmap(Bitmap paramBitmap, float paramFloat1, float paramFloat2, Paint paramPaint) {
    long l3;
    throwIfCannotDraw(paramBitmap);
    throwIfHasHwBitmapInSwMode(paramPaint);
    OplusBaseBaseCanvas.Entity entity = changeBitmap(paramPaint, paramBitmap, getRectF(paramBitmap.getWidth(), paramBitmap.getHeight()));
    Paint paint = paramPaint;
    if (entity != null) {
      paint = paramPaint;
      if (entity.isDarkMode)
        paint = entity.newPaint; 
    } 
    long l1 = this.mNativeCanvasWrapper, l2 = paramBitmap.getNativeInstance();
    if (paint != null) {
      l3 = paint.getNativeInstance();
    } else {
      l3 = 0L;
    } 
    int i = this.mDensity, j = this.mScreenDensity, k = paramBitmap.mDensity;
    nDrawBitmap(l1, l2, paramFloat1, paramFloat2, l3, i, j, k);
    resetEntity(entity, paint);
  }
  
  public void drawBitmap(Bitmap paramBitmap, Matrix paramMatrix, Paint paramPaint) {
    long l4;
    throwIfHasHwBitmapInSwMode(paramPaint);
    OplusBaseBaseCanvas.Entity entity = changeBitmap(paramPaint, paramBitmap, getRectF(paramBitmap.getWidth(), paramBitmap.getHeight()));
    Paint paint = paramPaint;
    if (entity != null) {
      paint = paramPaint;
      if (entity.isDarkMode)
        paint = entity.newPaint; 
    } 
    long l1 = this.mNativeCanvasWrapper, l2 = paramBitmap.getNativeInstance(), l3 = paramMatrix.ni();
    if (paint != null) {
      l4 = paint.getNativeInstance();
    } else {
      l4 = 0L;
    } 
    nDrawBitmapMatrix(l1, l2, l3, l4);
    resetEntity(entity, paint);
  }
  
  public void drawBitmap(Bitmap paramBitmap, Rect paramRect1, Rect paramRect2, Paint paramPaint) {
    Paint paint = paramPaint;
    if (paramRect2 != null) {
      long l;
      int i, j, k, m;
      throwIfCannotDraw(paramBitmap);
      throwIfHasHwBitmapInSwMode(paint);
      OplusBaseBaseCanvas.Entity entity = changeBitmap(paint, paramBitmap, getRectF(paramRect2));
      paramPaint = paint;
      if (entity != null) {
        paramPaint = paint;
        if (entity.isDarkMode)
          paramPaint = entity.newPaint; 
      } 
      if (paramPaint == null) {
        l = 0L;
      } else {
        l = paramPaint.getNativeInstance();
      } 
      if (paramRect1 == null) {
        i = 0;
        j = 0;
        k = paramBitmap.getWidth();
        m = paramBitmap.getHeight();
      } else {
        i = paramRect1.left;
        k = paramRect1.right;
        j = paramRect1.top;
        m = paramRect1.bottom;
      } 
      nDrawBitmap(this.mNativeCanvasWrapper, paramBitmap.getNativeInstance(), i, j, k, m, paramRect2.left, paramRect2.top, paramRect2.right, paramRect2.bottom, l, this.mScreenDensity, paramBitmap.mDensity);
      resetEntity(entity, paramPaint);
      return;
    } 
    throw null;
  }
  
  public void drawBitmap(Bitmap paramBitmap, Rect paramRect, RectF paramRectF, Paint paramPaint) {
    Paint paint = paramPaint;
    if (paramRectF != null) {
      long l;
      float f1, f2, f3, f4;
      throwIfCannotDraw(paramBitmap);
      throwIfHasHwBitmapInSwMode(paint);
      OplusBaseBaseCanvas.Entity entity = changeBitmap(paint, paramBitmap, paramRectF);
      paramPaint = paint;
      if (entity != null) {
        paramPaint = paint;
        if (entity.isDarkMode)
          paramPaint = entity.newPaint; 
      } 
      if (paramPaint == null) {
        l = 0L;
      } else {
        l = paramPaint.getNativeInstance();
      } 
      if (paramRect == null) {
        f1 = 0.0F;
        f2 = 0.0F;
        f3 = paramBitmap.getWidth();
        f4 = paramBitmap.getHeight();
      } else {
        f1 = paramRect.left;
        f3 = paramRect.right;
        f2 = paramRect.top;
        f4 = paramRect.bottom;
      } 
      nDrawBitmap(this.mNativeCanvasWrapper, paramBitmap.getNativeInstance(), f1, f2, f3, f4, paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, l, this.mScreenDensity, paramBitmap.mDensity);
      resetEntity(entity, paramPaint);
      return;
    } 
    throw null;
  }
  
  @Deprecated
  public void drawBitmap(int[] paramArrayOfint, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, int paramInt3, int paramInt4, boolean paramBoolean, Paint paramPaint) {
    if (paramInt3 >= 0) {
      if (paramInt4 >= 0) {
        if (Math.abs(paramInt2) >= paramInt3) {
          int i = paramInt1 + (paramInt4 - 1) * paramInt2;
          int j = paramArrayOfint.length;
          if (paramInt1 >= 0 && paramInt1 + paramInt3 <= j && i >= 0 && i + paramInt3 <= j) {
            long l2;
            throwIfHasHwBitmapInSwMode(paramPaint);
            if (paramInt3 == 0 || paramInt4 == 0)
              return; 
            paramArrayOfint = changeColors(paramArrayOfint);
            long l1 = this.mNativeCanvasWrapper;
            if (paramPaint != null) {
              l2 = paramPaint.getNativeInstance();
            } else {
              l2 = 0L;
            } 
            nDrawBitmap(l1, paramArrayOfint, paramInt1, paramInt2, paramFloat1, paramFloat2, paramInt3, paramInt4, paramBoolean, l2);
            return;
          } 
          throw new ArrayIndexOutOfBoundsException();
        } 
        throw new IllegalArgumentException("abs(stride) must be >= width");
      } 
      throw new IllegalArgumentException("height must be >= 0");
    } 
    throw new IllegalArgumentException("width must be >= 0");
  }
  
  @Deprecated
  public void drawBitmap(int[] paramArrayOfint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, boolean paramBoolean, Paint paramPaint) {
    drawBitmap(paramArrayOfint, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramBoolean, paramPaint);
  }
  
  public void drawBitmapMesh(Bitmap paramBitmap, int paramInt1, int paramInt2, float[] paramArrayOffloat, int paramInt3, int[] paramArrayOfint, int paramInt4, Paint paramPaint) {
    if ((paramInt1 | paramInt2 | paramInt3 | paramInt4) >= 0) {
      long l3;
      throwIfHasHwBitmapInSwMode(paramPaint);
      if (paramInt1 == 0 || paramInt2 == 0)
        return; 
      int i = (paramInt1 + 1) * (paramInt2 + 1);
      checkRange(paramArrayOffloat.length, paramInt3, i * 2);
      if (paramArrayOfint != null)
        checkRange(paramArrayOfint.length, paramInt4, i); 
      OplusBaseBaseCanvas.Entity entity = changeBitmap(paramPaint, paramBitmap, getRectF(paramBitmap.getWidth(), paramBitmap.getHeight()));
      if (entity != null && entity.isDarkMode)
        paramPaint = entity.newPaint; 
      long l1 = this.mNativeCanvasWrapper, l2 = paramBitmap.getNativeInstance();
      if (paramPaint != null) {
        l3 = paramPaint.getNativeInstance();
      } else {
        l3 = 0L;
      } 
      nDrawBitmapMesh(l1, l2, paramInt1, paramInt2, paramArrayOffloat, paramInt3, paramArrayOfint, paramInt4, l3);
      resetEntity(entity, paramPaint);
      return;
    } 
    throw new ArrayIndexOutOfBoundsException();
  }
  
  public void drawCircle(float paramFloat1, float paramFloat2, float paramFloat3, Paint paramPaint) {
    throwIfHasHwBitmapInSwMode(paramPaint);
    OplusBaseBaseCanvas.Entity entity = changeArea(paramPaint, getRectF(paramFloat3 * 2.0F, 2.0F * paramFloat3));
    nDrawCircle(this.mNativeCanvasWrapper, paramFloat1, paramFloat2, paramFloat3, paramPaint.getNativeInstance());
    resetEntity(entity, paramPaint);
  }
  
  public void drawColor(int paramInt) {
    paramInt = changeColor(paramInt);
    nDrawColor(this.mNativeCanvasWrapper, paramInt, (BlendMode.SRC_OVER.getXfermode()).porterDuffMode);
  }
  
  public void drawColor(int paramInt, PorterDuff.Mode paramMode) {
    paramInt = changeColor(paramInt);
    nDrawColor(this.mNativeCanvasWrapper, paramInt, paramMode.nativeInt);
  }
  
  public void drawColor(int paramInt, BlendMode paramBlendMode) {
    nDrawColor(this.mNativeCanvasWrapper, paramInt, (paramBlendMode.getXfermode()).porterDuffMode);
  }
  
  public void drawColor(long paramLong, BlendMode paramBlendMode) {
    ColorSpace colorSpace = Color.colorSpace(paramLong);
    long l1 = this.mNativeCanvasWrapper, l2 = colorSpace.getNativeInstance();
    int i = (paramBlendMode.getXfermode()).porterDuffMode;
    nDrawColor(l1, l2, paramLong, i);
  }
  
  public void drawLine(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Paint paramPaint) {
    throwIfHasHwBitmapInSwMode(paramPaint);
    OplusBaseBaseCanvas.Entity entity = changeArea(paramPaint, getRectF(paramFloat1, paramFloat2, paramFloat3, paramFloat4));
    nDrawLine(this.mNativeCanvasWrapper, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramPaint.getNativeInstance());
    resetEntity(entity, paramPaint);
  }
  
  public void drawLines(float[] paramArrayOffloat, int paramInt1, int paramInt2, Paint paramPaint) {
    throwIfHasHwBitmapInSwMode(paramPaint);
    OplusBaseBaseCanvas.Entity entity = changeArea(paramPaint, null);
    nDrawLines(this.mNativeCanvasWrapper, paramArrayOffloat, paramInt1, paramInt2, paramPaint.getNativeInstance());
    resetEntity(entity, paramPaint);
  }
  
  public void drawLines(float[] paramArrayOffloat, Paint paramPaint) {
    throwIfHasHwBitmapInSwMode(paramPaint);
    drawLines(paramArrayOffloat, 0, paramArrayOffloat.length, paramPaint);
  }
  
  public void drawOval(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Paint paramPaint) {
    throwIfHasHwBitmapInSwMode(paramPaint);
    OplusBaseBaseCanvas.Entity entity = changeArea(paramPaint, getRectF(paramFloat1, paramFloat2, paramFloat3, paramFloat4));
    nDrawOval(this.mNativeCanvasWrapper, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramPaint.getNativeInstance());
    resetEntity(entity, paramPaint);
  }
  
  public void drawOval(RectF paramRectF, Paint paramPaint) {
    if (paramRectF != null) {
      throwIfHasHwBitmapInSwMode(paramPaint);
      drawOval(paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramPaint);
      return;
    } 
    throw null;
  }
  
  public void drawPaint(Paint paramPaint) {
    OplusBaseBaseCanvas.Entity entity = changeArea(paramPaint, getRectF(getWidth(), getHeight()));
    nDrawPaint(this.mNativeCanvasWrapper, paramPaint.getNativeInstance());
    resetEntity(entity, paramPaint);
  }
  
  public void drawPatch(NinePatch paramNinePatch, Rect paramRect, Paint paramPaint) {
    long l1;
    Bitmap bitmap = paramNinePatch.getBitmap();
    throwIfCannotDraw(bitmap);
    throwIfHasHwBitmapInSwMode(paramPaint);
    OplusBaseBaseCanvas.Entity entity = changePatch(paramNinePatch, paramPaint, getRectF(paramRect));
    Paint paint = paramPaint;
    if (entity != null) {
      paint = paramPaint;
      if (entity.isDarkMode)
        paint = entity.newPaint; 
    } 
    if (paint == null) {
      l1 = 0L;
    } else {
      l1 = paint.getNativeInstance();
    } 
    long l2 = this.mNativeCanvasWrapper, l3 = bitmap.getNativeInstance(), l4 = paramNinePatch.mNativeChunk;
    float f1 = paramRect.left, f2 = paramRect.top, f3 = paramRect.right, f4 = paramRect.bottom;
    int i = this.mDensity;
    int j = paramNinePatch.getDensity();
    nDrawNinePatch(l2, l3, l4, f1, f2, f3, f4, l1, i, j);
    resetEntity(entity, paint);
  }
  
  public void drawPatch(NinePatch paramNinePatch, RectF paramRectF, Paint paramPaint) {
    long l1;
    Paint paint = paramPaint;
    Bitmap bitmap = paramNinePatch.getBitmap();
    throwIfCannotDraw(bitmap);
    throwIfHasHwBitmapInSwMode(paint);
    OplusBaseBaseCanvas.Entity entity = changePatch(paramNinePatch, paint, paramRectF);
    paramPaint = paint;
    if (entity != null) {
      paramPaint = paint;
      if (entity.isDarkMode)
        paramPaint = entity.newPaint; 
    } 
    if (paramPaint == null) {
      l1 = 0L;
    } else {
      l1 = paramPaint.getNativeInstance();
    } 
    long l2 = this.mNativeCanvasWrapper, l3 = bitmap.getNativeInstance(), l4 = paramNinePatch.mNativeChunk;
    float f1 = paramRectF.left, f2 = paramRectF.top, f3 = paramRectF.right, f4 = paramRectF.bottom;
    int i = this.mDensity;
    int j = paramNinePatch.getDensity();
    nDrawNinePatch(l2, l3, l4, f1, f2, f3, f4, l1, i, j);
    resetEntity(entity, paramPaint);
  }
  
  public void drawPath(Path paramPath, Paint paramPaint) {
    throwIfHasHwBitmapInSwMode(paramPaint);
    OplusBaseBaseCanvas.Entity entity = changeArea(paramPaint, getRectF(getWidth(), getHeight()), paramPath);
    if (paramPath.isSimplePath && paramPath.rects != null) {
      nDrawRegion(this.mNativeCanvasWrapper, paramPath.rects.mNativeRegion, paramPaint.getNativeInstance());
    } else {
      nDrawPath(this.mNativeCanvasWrapper, paramPath.readOnlyNI(), paramPaint.getNativeInstance());
    } 
    resetEntity(entity, paramPaint);
  }
  
  public void drawPoint(float paramFloat1, float paramFloat2, Paint paramPaint) {
    throwIfHasHwBitmapInSwMode(paramPaint);
    OplusBaseBaseCanvas.Entity entity = changeArea(paramPaint, null);
    nDrawPoint(this.mNativeCanvasWrapper, paramFloat1, paramFloat2, paramPaint.getNativeInstance());
    resetEntity(entity, paramPaint);
  }
  
  public void drawPoints(float[] paramArrayOffloat, int paramInt1, int paramInt2, Paint paramPaint) {
    throwIfHasHwBitmapInSwMode(paramPaint);
    OplusBaseBaseCanvas.Entity entity = changeArea(paramPaint, null);
    nDrawPoints(this.mNativeCanvasWrapper, paramArrayOffloat, paramInt1, paramInt2, paramPaint.getNativeInstance());
    resetEntity(entity, paramPaint);
  }
  
  public void drawPoints(float[] paramArrayOffloat, Paint paramPaint) {
    throwIfHasHwBitmapInSwMode(paramPaint);
    drawPoints(paramArrayOffloat, 0, paramArrayOffloat.length, paramPaint);
  }
  
  @Deprecated
  public void drawPosText(char[] paramArrayOfchar, int paramInt1, int paramInt2, float[] paramArrayOffloat, Paint paramPaint) {
    if (paramInt1 >= 0 && paramInt1 + paramInt2 <= paramArrayOfchar.length && paramInt2 * 2 <= paramArrayOffloat.length) {
      throwIfHasHwBitmapInSwMode(paramPaint);
      for (byte b = 0; b < paramInt2; b++)
        drawText(paramArrayOfchar, paramInt1 + b, 1, paramArrayOffloat[b * 2], paramArrayOffloat[b * 2 + 1], paramPaint); 
      return;
    } 
    throw new IndexOutOfBoundsException();
  }
  
  @Deprecated
  public void drawPosText(String paramString, float[] paramArrayOffloat, Paint paramPaint) {
    throwIfHasHwBitmapInSwMode(paramPaint);
    drawPosText(paramString.toCharArray(), 0, paramString.length(), paramArrayOffloat, paramPaint);
  }
  
  public void drawRect(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Paint paramPaint) {
    throwIfHasHwBitmapInSwMode(paramPaint);
    OplusBaseBaseCanvas.Entity entity = changeArea(paramPaint, getRectF(paramFloat1, paramFloat2, paramFloat3, paramFloat4));
    nDrawRect(this.mNativeCanvasWrapper, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramPaint.getNativeInstance());
    resetEntity(entity, paramPaint);
  }
  
  public void drawRect(Rect paramRect, Paint paramPaint) {
    throwIfHasHwBitmapInSwMode(paramPaint);
    drawRect(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom, paramPaint);
  }
  
  public void drawRect(RectF paramRectF, Paint paramPaint) {
    throwIfHasHwBitmapInSwMode(paramPaint);
    OplusBaseBaseCanvas.Entity entity = changeArea(paramPaint, paramRectF);
    long l1 = this.mNativeCanvasWrapper;
    float f1 = paramRectF.left, f2 = paramRectF.top, f3 = paramRectF.right, f4 = paramRectF.bottom;
    long l2 = paramPaint.getNativeInstance();
    nDrawRect(l1, f1, f2, f3, f4, l2);
    resetEntity(entity, paramPaint);
  }
  
  public void drawRGB(int paramInt1, int paramInt2, int paramInt3) {
    drawColor(Color.rgb(paramInt1, paramInt2, paramInt3));
  }
  
  public void drawRoundRect(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, Paint paramPaint) {
    throwIfHasHwBitmapInSwMode(paramPaint);
    OplusBaseBaseCanvas.Entity entity = changeArea(paramPaint, getRectF(paramFloat1, paramFloat2, paramFloat3, paramFloat4));
    long l1 = this.mNativeCanvasWrapper;
    long l2 = paramPaint.getNativeInstance();
    nDrawRoundRect(l1, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6, l2);
    resetEntity(entity, paramPaint);
  }
  
  public void drawRoundRect(RectF paramRectF, float paramFloat1, float paramFloat2, Paint paramPaint) {
    throwIfHasHwBitmapInSwMode(paramPaint);
    drawRoundRect(paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramFloat1, paramFloat2, paramPaint);
  }
  
  public void drawDoubleRoundRect(RectF paramRectF1, float paramFloat1, float paramFloat2, RectF paramRectF2, float paramFloat3, float paramFloat4, Paint paramPaint) {
    throwIfHasHwBitmapInSwMode(paramPaint);
    float f1 = paramRectF1.left;
    float f2 = paramRectF1.top;
    float f3 = paramRectF1.right;
    float f4 = paramRectF1.bottom;
    float f5 = paramRectF2.left;
    float f6 = paramRectF2.top;
    float f7 = paramRectF2.right;
    float f8 = paramRectF2.bottom;
    long l1 = this.mNativeCanvasWrapper;
    long l2 = paramPaint.getNativeInstance();
    nDrawDoubleRoundRect(l1, f1, f2, f3, f4, paramFloat1, paramFloat2, f5, f6, f7, f8, paramFloat3, paramFloat4, l2);
  }
  
  public void drawDoubleRoundRect(RectF paramRectF1, float[] paramArrayOffloat1, RectF paramRectF2, float[] paramArrayOffloat2, Paint paramPaint) {
    throwIfHasHwBitmapInSwMode(paramPaint);
    if (paramArrayOffloat2 != null && paramArrayOffloat1 != null && paramArrayOffloat2.length == 8 && paramArrayOffloat1.length == 8) {
      float f1 = paramRectF1.left;
      float f2 = paramRectF1.top;
      float f3 = paramRectF1.right;
      float f4 = paramRectF1.bottom;
      float f5 = paramRectF2.left;
      float f6 = paramRectF2.top;
      float f7 = paramRectF2.right;
      float f8 = paramRectF2.bottom;
      long l1 = this.mNativeCanvasWrapper;
      long l2 = paramPaint.getNativeInstance();
      nDrawDoubleRoundRect(l1, f1, f2, f3, f4, paramArrayOffloat1, f5, f6, f7, f8, paramArrayOffloat2, l2);
      return;
    } 
    throw new IllegalArgumentException("Both inner and outer radii arrays must contain exactly 8 values");
  }
  
  public void drawText(char[] paramArrayOfchar, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, Paint paramPaint) {
    if ((paramInt1 | paramInt2 | paramInt1 + paramInt2 | paramArrayOfchar.length - paramInt1 - paramInt2) >= 0) {
      throwIfHasHwBitmapInSwMode(paramPaint);
      OplusBaseBaseCanvas.Entity entity = changeText(paramPaint);
      long l1 = this.mNativeCanvasWrapper;
      int i = paramPaint.mBidiFlags;
      long l2 = paramPaint.getNativeInstance();
      nDrawText(l1, paramArrayOfchar, paramInt1, paramInt2, paramFloat1, paramFloat2, i, l2);
      resetEntity(entity, paramPaint);
      return;
    } 
    throw new IndexOutOfBoundsException();
  }
  
  public void drawText(CharSequence paramCharSequence, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, Paint paramPaint) {
    if ((paramInt1 | paramInt2 | paramInt2 - paramInt1 | paramCharSequence.length() - paramInt2) >= 0) {
      throwIfHasHwBitmapInSwMode(paramPaint);
      OplusBaseBaseCanvas.Entity entity = changeText(paramPaint);
      if (paramCharSequence instanceof String || paramCharSequence instanceof android.text.SpannedString || paramCharSequence instanceof android.text.SpannableString) {
        long l2 = this.mNativeCanvasWrapper;
        paramCharSequence = paramCharSequence.toString();
        int i = paramPaint.mBidiFlags;
        long l1 = paramPaint.getNativeInstance();
        nDrawText(l2, (String)paramCharSequence, paramInt1, paramInt2, paramFloat1, paramFloat2, i, l1);
      } else if (paramCharSequence instanceof GraphicsOperations) {
        ((GraphicsOperations)paramCharSequence).drawText(this, paramInt1, paramInt2, paramFloat1, paramFloat2, paramPaint);
      } else {
        char[] arrayOfChar = TemporaryBuffer.obtain(paramInt2 - paramInt1);
        TextUtils.getChars(paramCharSequence, paramInt1, paramInt2, arrayOfChar, 0);
        long l1 = this.mNativeCanvasWrapper;
        int i = paramPaint.mBidiFlags;
        long l2 = paramPaint.getNativeInstance();
        nDrawText(l1, arrayOfChar, 0, paramInt2 - paramInt1, paramFloat1, paramFloat2, i, l2);
        TemporaryBuffer.recycle(arrayOfChar);
      } 
      resetEntity(entity, paramPaint);
      return;
    } 
    throw new IndexOutOfBoundsException();
  }
  
  public void drawText(String paramString, float paramFloat1, float paramFloat2, Paint paramPaint) {
    throwIfHasHwBitmapInSwMode(paramPaint);
    OplusBaseBaseCanvas.Entity entity = changeText(paramPaint);
    long l1 = this.mNativeCanvasWrapper;
    int i = paramString.length(), j = paramPaint.mBidiFlags;
    long l2 = paramPaint.getNativeInstance();
    nDrawText(l1, paramString, 0, i, paramFloat1, paramFloat2, j, l2);
    resetEntity(entity, paramPaint);
  }
  
  public void drawText(String paramString, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, Paint paramPaint) {
    if ((paramInt1 | paramInt2 | paramInt2 - paramInt1 | paramString.length() - paramInt2) >= 0) {
      throwIfHasHwBitmapInSwMode(paramPaint);
      OplusBaseBaseCanvas.Entity entity = changeText(paramPaint);
      long l1 = this.mNativeCanvasWrapper;
      int i = paramPaint.mBidiFlags;
      long l2 = paramPaint.getNativeInstance();
      nDrawText(l1, paramString, paramInt1, paramInt2, paramFloat1, paramFloat2, i, l2);
      resetEntity(entity, paramPaint);
      return;
    } 
    throw new IndexOutOfBoundsException();
  }
  
  public void drawTextOnPath(char[] paramArrayOfchar, int paramInt1, int paramInt2, Path paramPath, float paramFloat1, float paramFloat2, Paint paramPaint) {
    if (paramInt1 >= 0 && paramInt1 + paramInt2 <= paramArrayOfchar.length) {
      throwIfHasHwBitmapInSwMode(paramPaint);
      OplusBaseBaseCanvas.Entity entity = changeText(paramPaint);
      long l1 = this.mNativeCanvasWrapper;
      long l2 = paramPath.readOnlyNI();
      int i = paramPaint.mBidiFlags;
      long l3 = paramPaint.getNativeInstance();
      nDrawTextOnPath(l1, paramArrayOfchar, paramInt1, paramInt2, l2, paramFloat1, paramFloat2, i, l3);
      resetEntity(entity, paramPaint);
      return;
    } 
    throw new ArrayIndexOutOfBoundsException();
  }
  
  public void drawTextOnPath(String paramString, Path paramPath, float paramFloat1, float paramFloat2, Paint paramPaint) {
    if (paramString.length() > 0) {
      throwIfHasHwBitmapInSwMode(paramPaint);
      OplusBaseBaseCanvas.Entity entity = changeText(paramPaint);
      long l1 = this.mNativeCanvasWrapper, l2 = paramPath.readOnlyNI();
      int i = paramPaint.mBidiFlags;
      long l3 = paramPaint.getNativeInstance();
      nDrawTextOnPath(l1, paramString, l2, paramFloat1, paramFloat2, i, l3);
      resetEntity(entity, paramPaint);
    } 
  }
  
  public void drawTextRun(char[] paramArrayOfchar, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, boolean paramBoolean, Paint paramPaint) {
    if (paramArrayOfchar != null) {
      if (paramPaint != null) {
        if ((paramInt1 | paramInt2 | paramInt3 | paramInt4 | paramInt1 - paramInt3 | paramInt3 + paramInt4 - paramInt1 + paramInt2 | paramArrayOfchar.length - paramInt3 + paramInt4) >= 0) {
          throwIfHasHwBitmapInSwMode(paramPaint);
          OplusBaseBaseCanvas.Entity entity = changeText(paramPaint);
          long l1 = this.mNativeCanvasWrapper;
          long l2 = paramPaint.getNativeInstance();
          nDrawTextRun(l1, paramArrayOfchar, paramInt1, paramInt2, paramInt3, paramInt4, paramFloat1, paramFloat2, paramBoolean, l2, 0L);
          resetEntity(entity, paramPaint);
          return;
        } 
        throw new IndexOutOfBoundsException();
      } 
      throw new NullPointerException("paint is null");
    } 
    throw new NullPointerException("text is null");
  }
  
  public void drawTextRun(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, boolean paramBoolean, Paint paramPaint) {
    if (paramCharSequence != null) {
      if (paramPaint != null) {
        if ((paramInt1 | paramInt2 | paramInt3 | paramInt4 | paramInt1 - paramInt3 | paramInt2 - paramInt1 | paramInt4 - paramInt2 | paramCharSequence.length() - paramInt4) >= 0) {
          throwIfHasHwBitmapInSwMode(paramPaint);
          OplusBaseBaseCanvas.Entity entity = changeText(paramPaint);
          if (paramCharSequence instanceof String || paramCharSequence instanceof android.text.SpannedString || paramCharSequence instanceof android.text.SpannableString) {
            long l2 = this.mNativeCanvasWrapper;
            paramCharSequence = paramCharSequence.toString();
            long l1 = paramPaint.getNativeInstance();
            nDrawTextRun(l2, (String)paramCharSequence, paramInt1, paramInt2, paramInt3, paramInt4, paramFloat1, paramFloat2, paramBoolean, l1);
          } else if (paramCharSequence instanceof GraphicsOperations) {
            ((GraphicsOperations)paramCharSequence).drawTextRun(this, paramInt1, paramInt2, paramInt3, paramInt4, paramFloat1, paramFloat2, paramBoolean, paramPaint);
          } else {
            MeasuredParagraph measuredParagraph;
            if (paramCharSequence instanceof PrecomputedText) {
              PrecomputedText precomputedText = (PrecomputedText)paramCharSequence;
              int j = precomputedText.findParaIndex(paramInt1);
              if (paramInt2 <= precomputedText.getParagraphEnd(j)) {
                int k = precomputedText.getParagraphStart(j);
                measuredParagraph = precomputedText.getMeasuredParagraph(j);
                drawTextRun(measuredParagraph.getMeasuredText(), paramInt1 - k, paramInt2 - k, paramInt3 - k, paramInt4 - k, paramFloat1, paramFloat2, paramBoolean, paramPaint);
                return;
              } 
            } 
            int i = paramInt4 - paramInt3;
            char[] arrayOfChar = TemporaryBuffer.obtain(i);
            TextUtils.getChars((CharSequence)measuredParagraph, paramInt3, paramInt4, arrayOfChar, 0);
            long l1 = this.mNativeCanvasWrapper;
            long l2 = paramPaint.getNativeInstance();
            nDrawTextRun(l1, arrayOfChar, paramInt1 - paramInt3, paramInt2 - paramInt1, 0, i, paramFloat1, paramFloat2, paramBoolean, l2, 0L);
            TemporaryBuffer.recycle(arrayOfChar);
          } 
          resetEntity(entity, paramPaint);
          return;
        } 
        throw new IndexOutOfBoundsException();
      } 
      throw new NullPointerException("paint is null");
    } 
    throw new NullPointerException("text is null");
  }
  
  public void drawTextRun(MeasuredText paramMeasuredText, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, boolean paramBoolean, Paint paramPaint) {
    long l1 = this.mNativeCanvasWrapper;
    char[] arrayOfChar = paramMeasuredText.getChars();
    long l2 = paramPaint.getNativeInstance();
    long l3 = paramMeasuredText.getNativePtr();
    nDrawTextRun(l1, arrayOfChar, paramInt1, paramInt2 - paramInt1, paramInt3, paramInt4 - paramInt3, paramFloat1, paramFloat2, paramBoolean, l2, l3);
  }
  
  public void drawVertices(Canvas.VertexMode paramVertexMode, int paramInt1, float[] paramArrayOffloat1, int paramInt2, float[] paramArrayOffloat2, int paramInt3, int[] paramArrayOfint, int paramInt4, short[] paramArrayOfshort, int paramInt5, int paramInt6, Paint paramPaint) {
    checkRange(paramArrayOffloat1.length, paramInt2, paramInt1);
    if (paramArrayOffloat2 != null)
      checkRange(paramArrayOffloat2.length, paramInt3, paramInt1); 
    if (paramArrayOfint != null)
      checkRange(paramArrayOfint.length, paramInt4, paramInt1 / 2); 
    if (paramArrayOfshort != null)
      checkRange(paramArrayOfshort.length, paramInt5, paramInt6); 
    throwIfHasHwBitmapInSwMode(paramPaint);
    long l1 = this.mNativeCanvasWrapper;
    int i = paramVertexMode.nativeInt;
    long l2 = paramPaint.getNativeInstance();
    nDrawVertices(l1, i, paramInt1, paramArrayOffloat1, paramInt2, paramArrayOffloat2, paramInt3, paramArrayOfint, paramInt4, paramArrayOfshort, paramInt5, paramInt6, l2);
  }
  
  public void setHwBitmapsInSwModeEnabled(boolean paramBoolean) {
    this.mAllowHwBitmapsInSwMode = paramBoolean;
  }
  
  public boolean isHwBitmapsInSwModeEnabled() {
    return this.mAllowHwBitmapsInSwMode;
  }
  
  protected void onHwBitmapInSwMode() {
    if (this.mAllowHwBitmapsInSwMode)
      return; 
    throw new IllegalArgumentException("Software rendering doesn't support hardware bitmaps");
  }
  
  private void throwIfHwBitmapInSwMode(Bitmap paramBitmap) {
    if (!isHardwareAccelerated() && paramBitmap.getConfig() == Bitmap.Config.HARDWARE)
      onHwBitmapInSwMode(); 
  }
  
  private void throwIfHasHwBitmapInSwMode(Paint paramPaint) {
    if (isHardwareAccelerated() || paramPaint == null)
      return; 
    throwIfHasHwBitmapInSwMode(paramPaint.getShader());
  }
  
  private void throwIfHasHwBitmapInSwMode(Shader paramShader) {
    if (paramShader == null)
      return; 
    if (paramShader instanceof BitmapShader)
      throwIfHwBitmapInSwMode(((BitmapShader)paramShader).mBitmap); 
    if (paramShader instanceof ComposeShader) {
      throwIfHasHwBitmapInSwMode(((ComposeShader)paramShader).mShaderA);
      throwIfHasHwBitmapInSwMode(((ComposeShader)paramShader).mShaderB);
    } 
  }
  
  private static native void nDrawArc(long paramLong1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, boolean paramBoolean, long paramLong2);
  
  private static native void nDrawBitmap(long paramLong1, long paramLong2, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8, long paramLong3, int paramInt1, int paramInt2);
  
  private static native void nDrawBitmap(long paramLong1, long paramLong2, float paramFloat1, float paramFloat2, long paramLong3, int paramInt1, int paramInt2, int paramInt3);
  
  private static native void nDrawBitmap(long paramLong1, int[] paramArrayOfint, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, int paramInt3, int paramInt4, boolean paramBoolean, long paramLong2);
  
  private static native void nDrawBitmapMatrix(long paramLong1, long paramLong2, long paramLong3, long paramLong4);
  
  private static native void nDrawBitmapMesh(long paramLong1, long paramLong2, int paramInt1, int paramInt2, float[] paramArrayOffloat, int paramInt3, int[] paramArrayOfint, int paramInt4, long paramLong3);
  
  private static native void nDrawCircle(long paramLong1, float paramFloat1, float paramFloat2, float paramFloat3, long paramLong2);
  
  private static native void nDrawColor(long paramLong, int paramInt1, int paramInt2);
  
  private static native void nDrawColor(long paramLong1, long paramLong2, long paramLong3, int paramInt);
  
  private static native void nDrawDoubleRoundRect(long paramLong1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8, float paramFloat9, float paramFloat10, float paramFloat11, float paramFloat12, long paramLong2);
  
  private static native void nDrawDoubleRoundRect(long paramLong1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float[] paramArrayOffloat1, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8, float[] paramArrayOffloat2, long paramLong2);
  
  private static native void nDrawLine(long paramLong1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, long paramLong2);
  
  private static native void nDrawLines(long paramLong1, float[] paramArrayOffloat, int paramInt1, int paramInt2, long paramLong2);
  
  private static native void nDrawNinePatch(long paramLong1, long paramLong2, long paramLong3, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, long paramLong4, int paramInt1, int paramInt2);
  
  private static native void nDrawOval(long paramLong1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, long paramLong2);
  
  private static native void nDrawPaint(long paramLong1, long paramLong2);
  
  private static native void nDrawPath(long paramLong1, long paramLong2, long paramLong3);
  
  private static native void nDrawPoint(long paramLong1, float paramFloat1, float paramFloat2, long paramLong2);
  
  private static native void nDrawPoints(long paramLong1, float[] paramArrayOffloat, int paramInt1, int paramInt2, long paramLong2);
  
  private static native void nDrawRect(long paramLong1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, long paramLong2);
  
  private static native void nDrawRegion(long paramLong1, long paramLong2, long paramLong3);
  
  private static native void nDrawRoundRect(long paramLong1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, long paramLong2);
  
  private static native void nDrawText(long paramLong1, String paramString, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, int paramInt3, long paramLong2);
  
  private static native void nDrawText(long paramLong1, char[] paramArrayOfchar, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, int paramInt3, long paramLong2);
  
  private static native void nDrawTextOnPath(long paramLong1, String paramString, long paramLong2, float paramFloat1, float paramFloat2, int paramInt, long paramLong3);
  
  private static native void nDrawTextOnPath(long paramLong1, char[] paramArrayOfchar, int paramInt1, int paramInt2, long paramLong2, float paramFloat1, float paramFloat2, int paramInt3, long paramLong3);
  
  private static native void nDrawTextRun(long paramLong1, String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, boolean paramBoolean, long paramLong2);
  
  private static native void nDrawTextRun(long paramLong1, char[] paramArrayOfchar, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, boolean paramBoolean, long paramLong2, long paramLong3);
  
  private static native void nDrawVertices(long paramLong1, int paramInt1, int paramInt2, float[] paramArrayOffloat1, int paramInt3, float[] paramArrayOffloat2, int paramInt4, int[] paramArrayOfint, int paramInt5, short[] paramArrayOfshort, int paramInt6, int paramInt7, long paramLong2);
}
