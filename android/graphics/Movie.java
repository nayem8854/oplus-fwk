package android.graphics;

import android.content.res.AssetManager;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

@Deprecated
public class Movie {
  private long mNativeMovie;
  
  private Movie(long paramLong) {
    if (paramLong != 0L) {
      this.mNativeMovie = paramLong;
      return;
    } 
    throw new RuntimeException("native movie creation failed");
  }
  
  public void draw(Canvas paramCanvas, float paramFloat1, float paramFloat2, Paint paramPaint) {
    long l2, l1 = paramCanvas.getNativeCanvasWrapper();
    if (paramPaint != null) {
      l2 = paramPaint.getNativeInstance();
    } else {
      l2 = 0L;
    } 
    nDraw(l1, paramFloat1, paramFloat2, l2);
  }
  
  public void draw(Canvas paramCanvas, float paramFloat1, float paramFloat2) {
    nDraw(paramCanvas.getNativeCanvasWrapper(), paramFloat1, paramFloat2, 0L);
  }
  
  public static Movie decodeStream(InputStream paramInputStream) {
    if (paramInputStream == null)
      return null; 
    if (paramInputStream instanceof AssetManager.AssetInputStream) {
      long l = ((AssetManager.AssetInputStream)paramInputStream).getNativeAsset();
      return nativeDecodeAsset(l);
    } 
    return nativeDecodeStream(paramInputStream);
  }
  
  public static Movie decodeFile(String paramString) {
    try {
      FileInputStream fileInputStream = new FileInputStream(paramString);
      return decodeTempStream(fileInputStream);
    } catch (FileNotFoundException fileNotFoundException) {
      return null;
    } 
  }
  
  protected void finalize() throws Throwable {
    try {
      nativeDestructor(this.mNativeMovie);
      this.mNativeMovie = 0L;
      return;
    } finally {
      super.finalize();
    } 
  }
  
  private static Movie decodeTempStream(InputStream paramInputStream) {
    Movie movie = null;
    try {
      Movie movie1 = decodeStream(paramInputStream);
      movie = movie1;
      paramInputStream.close();
      movie = movie1;
    } catch (IOException iOException) {}
    return movie;
  }
  
  public static native Movie decodeByteArray(byte[] paramArrayOfbyte, int paramInt1, int paramInt2);
  
  private native void nDraw(long paramLong1, float paramFloat1, float paramFloat2, long paramLong2);
  
  private static native Movie nativeDecodeAsset(long paramLong);
  
  private static native Movie nativeDecodeStream(InputStream paramInputStream);
  
  private static native void nativeDestructor(long paramLong);
  
  public native int duration();
  
  public native int height();
  
  public native boolean isOpaque();
  
  public native boolean setTime(int paramInt);
  
  public native int width();
}
