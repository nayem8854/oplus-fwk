package android.graphics;

public class LightingColorFilter extends ColorFilter {
  private int mAdd;
  
  private int mMul;
  
  public LightingColorFilter(int paramInt1, int paramInt2) {
    this.mMul = paramInt1;
    this.mAdd = paramInt2;
  }
  
  public int getColorMultiply() {
    return this.mMul;
  }
  
  public void setColorMultiply(int paramInt) {
    if (this.mMul != paramInt) {
      this.mMul = paramInt;
      discardNativeInstance();
    } 
  }
  
  public int getColorAdd() {
    return this.mAdd;
  }
  
  public void setColorAdd(int paramInt) {
    if (this.mAdd != paramInt) {
      this.mAdd = paramInt;
      discardNativeInstance();
    } 
  }
  
  long createNativeInstance() {
    return native_CreateLightingFilter(this.mMul, this.mAdd);
  }
  
  private static native long native_CreateLightingFilter(int paramInt1, int paramInt2);
}
