package android.graphics;

import dalvik.annotation.optimization.CriticalNative;

public abstract class OplusBaseRenderNode {
  public static final int USAGE_OPLUS_FORCE_BACKGROUND = 9;
  
  public static final int USAGE_OPLUS_FORCE_FOREGROUND = 10;
  
  public static final int USAGE_OPLUS_FORCE_UNKNOWN = 8;
  
  private int mAlgorithmType = 0;
  
  private int mBackgroundUsageHint = 1;
  
  @CriticalNative
  private static native void nSetForceDarkNodeType(long paramLong, int paramInt);
  
  @CriticalNative
  private static native void nSetUsageForceDarkAlgorithmType(long paramLong, int paramInt);
  
  public int getBackgroundUsageHint() {
    return this.mBackgroundUsageHint;
  }
  
  protected abstract long getNativeRenderNode();
  
  public void setBackgroundUsageHint(int paramInt) {
    this.mBackgroundUsageHint = paramInt;
  }
  
  public void setUsageForceDarkAlgorithmType(int paramInt) {
    if (this.mAlgorithmType != paramInt) {
      nSetUsageForceDarkAlgorithmType(getNativeRenderNode(), paramInt);
      this.mAlgorithmType = paramInt;
    } 
  }
  
  public abstract void setUsageHint(int paramInt);
}
