package android.metrics;

import android.annotation.SystemApi;
import android.content.ComponentName;
import android.util.Log;
import android.util.SparseArray;

@SystemApi
public class LogMaker {
  public static final int MAX_SERIALIZED_SIZE = 4000;
  
  private static final String TAG = "LogBuilder";
  
  private SparseArray<Object> entries = new SparseArray();
  
  public LogMaker(int paramInt) {
    setCategory(paramInt);
  }
  
  public LogMaker(Object[] paramArrayOfObject) {
    if (paramArrayOfObject != null) {
      deserialize(paramArrayOfObject);
    } else {
      setCategory(0);
    } 
  }
  
  public LogMaker setCategory(int paramInt) {
    this.entries.put(757, Integer.valueOf(paramInt));
    return this;
  }
  
  public LogMaker clearCategory() {
    this.entries.remove(757);
    return this;
  }
  
  public LogMaker setType(int paramInt) {
    this.entries.put(758, Integer.valueOf(paramInt));
    return this;
  }
  
  public LogMaker clearType() {
    this.entries.remove(758);
    return this;
  }
  
  public LogMaker setSubtype(int paramInt) {
    this.entries.put(759, Integer.valueOf(paramInt));
    return this;
  }
  
  public LogMaker clearSubtype() {
    this.entries.remove(759);
    return this;
  }
  
  public LogMaker setLatency(long paramLong) {
    this.entries.put(1359, Long.valueOf(paramLong));
    return this;
  }
  
  public LogMaker setTimestamp(long paramLong) {
    this.entries.put(805, Long.valueOf(paramLong));
    return this;
  }
  
  public LogMaker clearTimestamp() {
    this.entries.remove(805);
    return this;
  }
  
  public LogMaker setPackageName(String paramString) {
    this.entries.put(806, paramString);
    return this;
  }
  
  public LogMaker setComponentName(ComponentName paramComponentName) {
    this.entries.put(806, paramComponentName.getPackageName());
    this.entries.put(871, paramComponentName.getClassName());
    return this;
  }
  
  public LogMaker clearPackageName() {
    this.entries.remove(806);
    return this;
  }
  
  public LogMaker setProcessId(int paramInt) {
    this.entries.put(865, Integer.valueOf(paramInt));
    return this;
  }
  
  public LogMaker clearProcessId() {
    this.entries.remove(865);
    return this;
  }
  
  public LogMaker setUid(int paramInt) {
    this.entries.put(943, Integer.valueOf(paramInt));
    return this;
  }
  
  public LogMaker clearUid() {
    this.entries.remove(943);
    return this;
  }
  
  public LogMaker setCounterName(String paramString) {
    this.entries.put(799, paramString);
    return this;
  }
  
  public LogMaker setCounterBucket(int paramInt) {
    this.entries.put(801, Integer.valueOf(paramInt));
    return this;
  }
  
  public LogMaker setCounterBucket(long paramLong) {
    this.entries.put(801, Long.valueOf(paramLong));
    return this;
  }
  
  public LogMaker setCounterValue(int paramInt) {
    this.entries.put(802, Integer.valueOf(paramInt));
    return this;
  }
  
  public LogMaker addTaggedData(int paramInt, Object paramObject) {
    if (paramObject == null)
      return clearTaggedData(paramInt); 
    if (isValidValue(paramObject)) {
      if ((paramObject.toString().getBytes()).length > 4000) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Log value too long, omitted: ");
        stringBuilder.append(paramObject.toString());
        Log.i("LogBuilder", stringBuilder.toString());
      } else {
        this.entries.put(paramInt, paramObject);
      } 
      return this;
    } 
    throw new IllegalArgumentException("Value must be loggable type - int, long, float, String");
  }
  
  public LogMaker clearTaggedData(int paramInt) {
    this.entries.delete(paramInt);
    return this;
  }
  
  public boolean isValidValue(Object paramObject) {
    return (paramObject instanceof Integer || paramObject instanceof String || paramObject instanceof Long || paramObject instanceof Float);
  }
  
  public Object getTaggedData(int paramInt) {
    return this.entries.get(paramInt);
  }
  
  public int getCategory() {
    Object object = this.entries.get(757);
    if (object instanceof Integer)
      return ((Integer)object).intValue(); 
    return 0;
  }
  
  public int getType() {
    Object object = this.entries.get(758);
    if (object instanceof Integer)
      return ((Integer)object).intValue(); 
    return 0;
  }
  
  public int getSubtype() {
    Object object = this.entries.get(759);
    if (object instanceof Integer)
      return ((Integer)object).intValue(); 
    return 0;
  }
  
  public long getTimestamp() {
    Object object = this.entries.get(805);
    if (object instanceof Long)
      return ((Long)object).longValue(); 
    return 0L;
  }
  
  public String getPackageName() {
    Object object = this.entries.get(806);
    if (object instanceof String)
      return (String)object; 
    return null;
  }
  
  public int getProcessId() {
    Object object = this.entries.get(865);
    if (object instanceof Integer)
      return ((Integer)object).intValue(); 
    return -1;
  }
  
  public int getUid() {
    Object object = this.entries.get(943);
    if (object instanceof Integer)
      return ((Integer)object).intValue(); 
    return -1;
  }
  
  public String getCounterName() {
    Object object = this.entries.get(799);
    if (object instanceof String)
      return (String)object; 
    return null;
  }
  
  public long getCounterBucket() {
    Object object = this.entries.get(801);
    if (object instanceof Number)
      return ((Number)object).longValue(); 
    return 0L;
  }
  
  public boolean isLongCounterBucket() {
    Object object = this.entries.get(801);
    return object instanceof Long;
  }
  
  public int getCounterValue() {
    Object object = this.entries.get(802);
    if (object instanceof Integer)
      return ((Integer)object).intValue(); 
    return 0;
  }
  
  public Object[] serialize() {
    Object[] arrayOfObject = new Object[this.entries.size() * 2];
    int i;
    for (i = 0; i < this.entries.size(); i++) {
      arrayOfObject[i * 2] = Integer.valueOf(this.entries.keyAt(i));
      arrayOfObject[i * 2 + 1] = this.entries.valueAt(i);
    } 
    i = (arrayOfObject.toString().getBytes()).length;
    if (i <= 4000)
      return arrayOfObject; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Log line too long, did not emit: ");
    stringBuilder.append(i);
    stringBuilder.append(" bytes.");
    Log.i("LogBuilder", stringBuilder.toString());
    throw new RuntimeException();
  }
  
  public void deserialize(Object[] paramArrayOfObject) {
    int i = 0;
    while (paramArrayOfObject != null && i < paramArrayOfObject.length) {
      String str;
      int j = i + 1;
      Object object = paramArrayOfObject[i];
      if (j < paramArrayOfObject.length) {
        str = (String)paramArrayOfObject[j];
        i = j + 1;
      } else {
        str = null;
        i = j;
      } 
      if (object instanceof Integer) {
        this.entries.put(((Integer)object).intValue(), str);
        continue;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid key ");
      if (object == null) {
        str = "null";
      } else {
        str = object.toString();
      } 
      stringBuilder.append(str);
      Log.i("LogBuilder", stringBuilder.toString());
    } 
  }
  
  public boolean isSubsetOf(LogMaker paramLogMaker) {
    if (paramLogMaker == null)
      return false; 
    for (byte b = 0; b < this.entries.size(); b++) {
      int i = this.entries.keyAt(b);
      Object object1 = this.entries.valueAt(b);
      Object object2 = paramLogMaker.entries.get(i);
      if ((object1 == null && object2 != null) || !object1.equals(object2))
        return false; 
    } 
    return true;
  }
  
  public SparseArray<Object> getEntries() {
    return this.entries;
  }
}
