package android.metrics;

import android.annotation.SystemApi;
import android.util.EventLog;
import com.android.internal.logging.MetricsLogger;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

@SystemApi
public class MetricsReader {
  private Queue<LogMaker> mPendingQueue = new LinkedList<>();
  
  private Queue<LogMaker> mSeenQueue = new LinkedList<>();
  
  private int[] LOGTAGS = new int[] { 524292 };
  
  private LogReader mReader = new LogReader();
  
  private int mCheckpointTag = -1;
  
  public void setLogReader(LogReader paramLogReader) {
    this.mReader = paramLogReader;
  }
  
  public void read(long paramLong) {
    ArrayList<Event> arrayList = new ArrayList();
    try {
      this.mReader.readEvents(this.LOGTAGS, paramLong, arrayList);
    } catch (IOException iOException) {
      iOException.printStackTrace();
    } 
    this.mPendingQueue.clear();
    this.mSeenQueue.clear();
    for (Event event : arrayList) {
      Object[] arrayOfObject;
      paramLong = event.getTimeMillis();
      Object object = event.getData();
      if (object instanceof Object[]) {
        arrayOfObject = (Object[])object;
      } else {
        arrayOfObject = new Object[1];
        arrayOfObject[0] = object;
      } 
      LogMaker logMaker = new LogMaker(arrayOfObject);
      logMaker = logMaker.setTimestamp(paramLong);
      logMaker = logMaker.setUid(event.getUid());
      logMaker = logMaker.setProcessId(event.getProcessId());
      if (logMaker.getCategory() == 920) {
        if (logMaker.getSubtype() == this.mCheckpointTag)
          this.mPendingQueue.clear(); 
        continue;
      } 
      this.mPendingQueue.offer(logMaker);
    } 
  }
  
  public void checkpoint() {
    int i = (int)(System.currentTimeMillis() % 2147483647L);
    this.mReader.writeCheckpoint(i);
    this.mPendingQueue.clear();
    this.mSeenQueue.clear();
  }
  
  public void reset() {
    this.mSeenQueue.addAll(this.mPendingQueue);
    this.mPendingQueue.clear();
    this.mCheckpointTag = -1;
    Queue<LogMaker> queue = this.mPendingQueue;
    this.mPendingQueue = this.mSeenQueue;
    this.mSeenQueue = queue;
  }
  
  public boolean hasNext() {
    return this.mPendingQueue.isEmpty() ^ true;
  }
  
  public LogMaker next() {
    LogMaker logMaker = this.mPendingQueue.poll();
    if (logMaker != null)
      this.mSeenQueue.offer(logMaker); 
    return logMaker;
  }
  
  public static class Event {
    Object mData;
    
    int mPid;
    
    long mTimeMillis;
    
    int mUid;
    
    public Event(long param1Long, int param1Int1, int param1Int2, Object param1Object) {
      this.mTimeMillis = param1Long;
      this.mPid = param1Int1;
      this.mUid = param1Int2;
      this.mData = param1Object;
    }
    
    Event(EventLog.Event param1Event) {
      TimeUnit timeUnit1 = TimeUnit.MILLISECONDS;
      long l = param1Event.getTimeNanos();
      TimeUnit timeUnit2 = TimeUnit.NANOSECONDS;
      this.mTimeMillis = timeUnit1.convert(l, timeUnit2);
      this.mPid = param1Event.getProcessId();
      this.mUid = param1Event.getUid();
      this.mData = param1Event.getData();
    }
    
    public long getTimeMillis() {
      return this.mTimeMillis;
    }
    
    public int getProcessId() {
      return this.mPid;
    }
    
    public int getUid() {
      return this.mUid;
    }
    
    public Object getData() {
      return this.mData;
    }
    
    public void setData(Object param1Object) {
      this.mData = param1Object;
    }
  }
  
  public static class LogReader {
    public void readEvents(int[] param1ArrayOfint, long param1Long, Collection<MetricsReader.Event> param1Collection) throws IOException {
      ArrayList arrayList = new ArrayList();
      param1Long = TimeUnit.NANOSECONDS.convert(param1Long, TimeUnit.MILLISECONDS);
      EventLog.readEventsOnWrapping(param1ArrayOfint, param1Long, arrayList);
      for (EventLog.Event event1 : arrayList) {
        MetricsReader.Event event = new MetricsReader.Event(event1);
        param1Collection.add(event);
      } 
    }
    
    public void writeCheckpoint(int param1Int) {
      MetricsLogger metricsLogger = new MetricsLogger();
      metricsLogger.action(920, param1Int);
    }
  }
}
