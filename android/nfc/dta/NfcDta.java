package android.nfc.dta;

import android.content.Context;
import android.nfc.INfcDta;
import android.nfc.NfcAdapter;
import android.os.RemoteException;
import java.util.HashMap;

public final class NfcDta {
  private static final String TAG = "NfcDta";
  
  private static HashMap<Context, NfcDta> sNfcDtas = new HashMap<>();
  
  private static INfcDta sService;
  
  private final Context mContext;
  
  private NfcDta(Context paramContext, INfcDta paramINfcDta) {
    this.mContext = paramContext.getApplicationContext();
    sService = paramINfcDta;
  }
  
  public static NfcDta getInstance(NfcAdapter paramNfcAdapter) {
    // Byte code:
    //   0: ldc android/nfc/dta/NfcDta
    //   2: monitorenter
    //   3: aload_0
    //   4: ifnull -> 109
    //   7: aload_0
    //   8: invokevirtual getContext : ()Landroid/content/Context;
    //   11: astore_1
    //   12: aload_1
    //   13: ifnull -> 87
    //   16: getstatic android/nfc/dta/NfcDta.sNfcDtas : Ljava/util/HashMap;
    //   19: aload_1
    //   20: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   23: checkcast android/nfc/dta/NfcDta
    //   26: astore_2
    //   27: aload_2
    //   28: astore_3
    //   29: aload_2
    //   30: ifnonnull -> 82
    //   33: aload_0
    //   34: invokevirtual getNfcDtaInterface : ()Landroid/nfc/INfcDta;
    //   37: astore_0
    //   38: aload_0
    //   39: ifnull -> 64
    //   42: new android/nfc/dta/NfcDta
    //   45: astore_3
    //   46: aload_3
    //   47: aload_1
    //   48: aload_0
    //   49: invokespecial <init> : (Landroid/content/Context;Landroid/nfc/INfcDta;)V
    //   52: getstatic android/nfc/dta/NfcDta.sNfcDtas : Ljava/util/HashMap;
    //   55: aload_1
    //   56: aload_3
    //   57: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   60: pop
    //   61: goto -> 82
    //   64: ldc 'NfcDta'
    //   66: ldc 'This device does not implement the INfcDta interface.'
    //   68: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   71: pop
    //   72: new java/lang/UnsupportedOperationException
    //   75: astore_0
    //   76: aload_0
    //   77: invokespecial <init> : ()V
    //   80: aload_0
    //   81: athrow
    //   82: ldc android/nfc/dta/NfcDta
    //   84: monitorexit
    //   85: aload_3
    //   86: areturn
    //   87: ldc 'NfcDta'
    //   89: ldc 'NfcAdapter context is null.'
    //   91: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   94: pop
    //   95: new java/lang/UnsupportedOperationException
    //   98: astore_0
    //   99: aload_0
    //   100: invokespecial <init> : ()V
    //   103: aload_0
    //   104: athrow
    //   105: astore_0
    //   106: goto -> 121
    //   109: new java/lang/NullPointerException
    //   112: astore_0
    //   113: aload_0
    //   114: ldc 'NfcAdapter is null'
    //   116: invokespecial <init> : (Ljava/lang/String;)V
    //   119: aload_0
    //   120: athrow
    //   121: ldc android/nfc/dta/NfcDta
    //   123: monitorexit
    //   124: aload_0
    //   125: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #51	-> 3
    //   #52	-> 7
    //   #53	-> 12
    //   #58	-> 16
    //   #59	-> 27
    //   #60	-> 33
    //   #61	-> 38
    //   #65	-> 42
    //   #66	-> 52
    //   #62	-> 64
    //   #63	-> 72
    //   #68	-> 82
    //   #54	-> 87
    //   #55	-> 95
    //   #50	-> 105
    //   #51	-> 109
    //   #50	-> 121
    // Exception table:
    //   from	to	target	type
    //   7	12	105	finally
    //   16	27	105	finally
    //   33	38	105	finally
    //   42	52	105	finally
    //   52	61	105	finally
    //   64	72	105	finally
    //   72	82	105	finally
    //   87	95	105	finally
    //   95	105	105	finally
    //   109	121	105	finally
  }
  
  public boolean enableDta() {
    try {
      sService.enableDta();
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean disableDta() {
    try {
      sService.disableDta();
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean enableServer(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    try {
      return sService.enableServer(paramString, paramInt1, paramInt2, paramInt3, paramInt4);
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean disableServer() {
    try {
      sService.disableServer();
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean enableClient(String paramString, int paramInt1, int paramInt2, int paramInt3) {
    try {
      return sService.enableClient(paramString, paramInt1, paramInt2, paramInt3);
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean disableClient() {
    try {
      sService.disableClient();
      return true;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean registerMessageService(String paramString) {
    try {
      return sService.registerMessageService(paramString);
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
}
