package android.nfc;

import android.annotation.SystemApi;
import android.app.Activity;
import android.app.ActivityThread;
import android.app.OnActivityPausedListener;
import android.app.PendingIntent;
import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.IPackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.OplusSystemProperties;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class NfcAdapter {
  public static final String ACTION_ADAPTER_STATE_CHANGED = "android.nfc.action.ADAPTER_STATE_CHANGED";
  
  public static final String ACTION_HANDOVER_TRANSFER_DONE = "android.nfc.action.HANDOVER_TRANSFER_DONE";
  
  public static final String ACTION_HANDOVER_TRANSFER_STARTED = "android.nfc.action.HANDOVER_TRANSFER_STARTED";
  
  public static final String ACTION_NDEF_DISCOVERED = "android.nfc.action.NDEF_DISCOVERED";
  
  public static final String ACTION_PREFERRED_PAYMENT_CHANGED = "android.nfc.action.PREFERRED_PAYMENT_CHANGED";
  
  public static final String ACTION_TAG_DISCOVERED = "android.nfc.action.TAG_DISCOVERED";
  
  public static final String ACTION_TAG_LEFT_FIELD = "android.nfc.action.TAG_LOST";
  
  public static final String ACTION_TECH_DISCOVERED = "android.nfc.action.TECH_DISCOVERED";
  
  public static final String ACTION_TRANSACTION_DETECTED = "android.nfc.action.TRANSACTION_DETECTED";
  
  public static final String EXTRA_ADAPTER_STATE = "android.nfc.extra.ADAPTER_STATE";
  
  public static final String EXTRA_AID = "android.nfc.extra.AID";
  
  public static final String EXTRA_DATA = "android.nfc.extra.DATA";
  
  public static final String EXTRA_HANDOVER_TRANSFER_STATUS = "android.nfc.extra.HANDOVER_TRANSFER_STATUS";
  
  public static final String EXTRA_HANDOVER_TRANSFER_URI = "android.nfc.extra.HANDOVER_TRANSFER_URI";
  
  public static final String EXTRA_ID = "android.nfc.extra.ID";
  
  public static final String EXTRA_NDEF_MESSAGES = "android.nfc.extra.NDEF_MESSAGES";
  
  public static final String EXTRA_PREFERRED_PAYMENT_CHANGED_REASON = "android.nfc.extra.PREFERRED_PAYMENT_CHANGED_REASON";
  
  public static final String EXTRA_READER_PRESENCE_CHECK_DELAY = "presence";
  
  public static final String EXTRA_SECURE_ELEMENT_NAME = "android.nfc.extra.SECURE_ELEMENT_NAME";
  
  public static final String EXTRA_TAG = "android.nfc.extra.TAG";
  
  private static final String FELICA_LOCK_PACKAGE_NAME = "jp.co.fsi.felicalock";
  
  private static final String FELICA_NETWORK_TEST_PACKAGE_NAME = "com.felicanetworks.test.android.nfcservice.settings";
  
  @SystemApi
  public static final int FLAG_NDEF_PUSH_NO_CONFIRM = 1;
  
  public static final int FLAG_READER_NFC_A = 1;
  
  public static final int FLAG_READER_NFC_B = 2;
  
  public static final int FLAG_READER_NFC_BARCODE = 16;
  
  public static final int FLAG_READER_NFC_F = 4;
  
  public static final int FLAG_READER_NFC_V = 8;
  
  public static final int FLAG_READER_NO_PLATFORM_SOUNDS = 256;
  
  public static final int FLAG_READER_SKIP_NDEF_CHECK = 128;
  
  public static final int HANDOVER_TRANSFER_STATUS_FAILURE = 1;
  
  public static final int HANDOVER_TRANSFER_STATUS_SUCCESS = 0;
  
  public static final int PREFERRED_PAYMENT_CHANGED = 2;
  
  public static final int PREFERRED_PAYMENT_LOADED = 1;
  
  public static final int PREFERRED_PAYMENT_UPDATED = 3;
  
  public static final int STATE_OFF = 1;
  
  public static final int STATE_ON = 3;
  
  public static final int STATE_TURNING_OFF = 4;
  
  public static final int STATE_TURNING_ON = 2;
  
  static final String TAG = "NFC";
  
  private static final String WIRELESS_SETTINGS_PACKAGE_NAME = "com.coloros.wirelesssettings";
  
  static INfcCardEmulation sCardEmulationService;
  
  static boolean sHasBeamFeature;
  
  static boolean sHasNfcFeature;
  
  static boolean sIsInitialized = false;
  
  static HashMap<Context, NfcAdapter> sNfcAdapters = new HashMap<>();
  
  static INfcFCardEmulation sNfcFCardEmulationService;
  
  static NfcAdapter sNullContextNfcAdapter;
  
  static INfcAdapter sService;
  
  static INfcTag sTagService;
  
  final Context mContext;
  
  OnActivityPausedListener mForegroundDispatchListener;
  
  private boolean mIsSupportJapanFelica = OplusSystemProperties.getBoolean("ro.oplus.nfc.support_japan_felica", false);
  
  final Object mLock;
  
  final NfcActivityManager mNfcActivityManager;
  
  final HashMap<NfcUnlockHandler, INfcUnlockHandler> mNfcUnlockHandlers;
  
  ITagRemovedCallback mTagRemovedListener;
  
  private static boolean hasBeamFeature() {
    IPackageManager iPackageManager = ActivityThread.getPackageManager();
    if (iPackageManager == null) {
      Log.e("NFC", "Cannot get package manager, assuming no Android Beam feature");
      return false;
    } 
    try {
      return iPackageManager.hasSystemFeature("android.sofware.nfc.beam", 0);
    } catch (RemoteException remoteException) {
      Log.e("NFC", "Package manager query failed, assuming no Android Beam feature", (Throwable)remoteException);
      return false;
    } 
  }
  
  private static boolean hasNfcFeature() {
    IPackageManager iPackageManager = ActivityThread.getPackageManager();
    if (iPackageManager == null) {
      Log.e("NFC", "Cannot get package manager, assuming no NFC feature");
      return false;
    } 
    try {
      return iPackageManager.hasSystemFeature("android.hardware.nfc", 0);
    } catch (RemoteException remoteException) {
      Log.e("NFC", "Package manager query failed, assuming no NFC feature", (Throwable)remoteException);
      return false;
    } 
  }
  
  private static boolean hasNfcHceFeature() {
    IPackageManager iPackageManager = ActivityThread.getPackageManager();
    boolean bool = false;
    if (iPackageManager == null) {
      Log.e("NFC", "Cannot get package manager, assuming no NFC feature");
      return false;
    } 
    try {
      if (!iPackageManager.hasSystemFeature("android.hardware.nfc.hce", 0)) {
        boolean bool1 = iPackageManager.hasSystemFeature("android.hardware.nfc.hcef", 0);
        return bool1 ? true : bool;
      } 
      return true;
    } catch (RemoteException remoteException) {
      Log.e("NFC", "Package manager query failed, assuming no NFC feature", (Throwable)remoteException);
      return false;
    } 
  }
  
  public List<String> getSupportedOffHostSecureElements() {
    ArrayList<String> arrayList = new ArrayList();
    IPackageManager iPackageManager = ActivityThread.getPackageManager();
    if (iPackageManager == null) {
      Log.e("NFC", "Cannot get package manager, assuming no off-host CE feature");
      return arrayList;
    } 
    try {
      if (iPackageManager.hasSystemFeature("android.hardware.nfc.uicc", 0))
        arrayList.add("SIM"); 
      if (iPackageManager.hasSystemFeature("android.hardware.nfc.ese", 0))
        arrayList.add("eSE"); 
      return arrayList;
    } catch (RemoteException remoteException) {
      Log.e("NFC", "Package manager query failed, assuming no off-host CE feature", (Throwable)remoteException);
      arrayList.clear();
      return arrayList;
    } 
  }
  
  public static NfcAdapter getNfcAdapter(Context paramContext) {
    // Byte code:
    //   0: ldc android/nfc/NfcAdapter
    //   2: monitorenter
    //   3: getstatic android/nfc/NfcAdapter.sIsInitialized : Z
    //   6: ifne -> 204
    //   9: invokestatic hasNfcFeature : ()Z
    //   12: putstatic android/nfc/NfcAdapter.sHasNfcFeature : Z
    //   15: invokestatic hasBeamFeature : ()Z
    //   18: putstatic android/nfc/NfcAdapter.sHasBeamFeature : Z
    //   21: invokestatic hasNfcHceFeature : ()Z
    //   24: istore_1
    //   25: getstatic android/nfc/NfcAdapter.sHasNfcFeature : Z
    //   28: ifne -> 57
    //   31: iload_1
    //   32: ifeq -> 38
    //   35: goto -> 57
    //   38: ldc 'NFC'
    //   40: ldc_w 'this device does not have NFC support'
    //   43: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   46: pop
    //   47: new java/lang/UnsupportedOperationException
    //   50: astore_0
    //   51: aload_0
    //   52: invokespecial <init> : ()V
    //   55: aload_0
    //   56: athrow
    //   57: invokestatic getServiceInterface : ()Landroid/nfc/INfcAdapter;
    //   60: astore_2
    //   61: aload_2
    //   62: putstatic android/nfc/NfcAdapter.sService : Landroid/nfc/INfcAdapter;
    //   65: aload_2
    //   66: ifnull -> 185
    //   69: getstatic android/nfc/NfcAdapter.sHasNfcFeature : Z
    //   72: istore_3
    //   73: iload_3
    //   74: ifeq -> 109
    //   77: aload_2
    //   78: invokeinterface getNfcTagInterface : ()Landroid/nfc/INfcTag;
    //   83: putstatic android/nfc/NfcAdapter.sTagService : Landroid/nfc/INfcTag;
    //   86: goto -> 109
    //   89: astore_0
    //   90: ldc 'NFC'
    //   92: ldc_w 'could not retrieve NFC Tag service'
    //   95: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   98: pop
    //   99: new java/lang/UnsupportedOperationException
    //   102: astore_0
    //   103: aload_0
    //   104: invokespecial <init> : ()V
    //   107: aload_0
    //   108: athrow
    //   109: iload_1
    //   110: ifeq -> 178
    //   113: getstatic android/nfc/NfcAdapter.sService : Landroid/nfc/INfcAdapter;
    //   116: invokeinterface getNfcFCardEmulationInterface : ()Landroid/nfc/INfcFCardEmulation;
    //   121: putstatic android/nfc/NfcAdapter.sNfcFCardEmulationService : Landroid/nfc/INfcFCardEmulation;
    //   124: getstatic android/nfc/NfcAdapter.sService : Landroid/nfc/INfcAdapter;
    //   127: invokeinterface getNfcCardEmulationInterface : ()Landroid/nfc/INfcCardEmulation;
    //   132: putstatic android/nfc/NfcAdapter.sCardEmulationService : Landroid/nfc/INfcCardEmulation;
    //   135: goto -> 178
    //   138: astore_0
    //   139: ldc 'NFC'
    //   141: ldc_w 'could not retrieve card emulation service'
    //   144: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   147: pop
    //   148: new java/lang/UnsupportedOperationException
    //   151: astore_0
    //   152: aload_0
    //   153: invokespecial <init> : ()V
    //   156: aload_0
    //   157: athrow
    //   158: astore_0
    //   159: ldc 'NFC'
    //   161: ldc_w 'could not retrieve NFC-F card emulation service'
    //   164: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   167: pop
    //   168: new java/lang/UnsupportedOperationException
    //   171: astore_0
    //   172: aload_0
    //   173: invokespecial <init> : ()V
    //   176: aload_0
    //   177: athrow
    //   178: iconst_1
    //   179: putstatic android/nfc/NfcAdapter.sIsInitialized : Z
    //   182: goto -> 204
    //   185: ldc 'NFC'
    //   187: ldc_w 'could not retrieve NFC service'
    //   190: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   193: pop
    //   194: new java/lang/UnsupportedOperationException
    //   197: astore_0
    //   198: aload_0
    //   199: invokespecial <init> : ()V
    //   202: aload_0
    //   203: athrow
    //   204: aload_0
    //   205: ifnonnull -> 236
    //   208: getstatic android/nfc/NfcAdapter.sNullContextNfcAdapter : Landroid/nfc/NfcAdapter;
    //   211: ifnonnull -> 227
    //   214: new android/nfc/NfcAdapter
    //   217: astore_0
    //   218: aload_0
    //   219: aconst_null
    //   220: invokespecial <init> : (Landroid/content/Context;)V
    //   223: aload_0
    //   224: putstatic android/nfc/NfcAdapter.sNullContextNfcAdapter : Landroid/nfc/NfcAdapter;
    //   227: getstatic android/nfc/NfcAdapter.sNullContextNfcAdapter : Landroid/nfc/NfcAdapter;
    //   230: astore_0
    //   231: ldc android/nfc/NfcAdapter
    //   233: monitorexit
    //   234: aload_0
    //   235: areturn
    //   236: getstatic android/nfc/NfcAdapter.sNfcAdapters : Ljava/util/HashMap;
    //   239: aload_0
    //   240: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   243: checkcast android/nfc/NfcAdapter
    //   246: astore #4
    //   248: aload #4
    //   250: astore_2
    //   251: aload #4
    //   253: ifnonnull -> 274
    //   256: new android/nfc/NfcAdapter
    //   259: astore_2
    //   260: aload_2
    //   261: aload_0
    //   262: invokespecial <init> : (Landroid/content/Context;)V
    //   265: getstatic android/nfc/NfcAdapter.sNfcAdapters : Ljava/util/HashMap;
    //   268: aload_0
    //   269: aload_2
    //   270: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   273: pop
    //   274: ldc android/nfc/NfcAdapter
    //   276: monitorexit
    //   277: aload_2
    //   278: areturn
    //   279: astore_0
    //   280: ldc android/nfc/NfcAdapter
    //   282: monitorexit
    //   283: aload_0
    //   284: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #614	-> 3
    //   #615	-> 9
    //   #616	-> 15
    //   #617	-> 21
    //   #619	-> 25
    //   #620	-> 38
    //   #621	-> 47
    //   #623	-> 57
    //   #624	-> 65
    //   #628	-> 69
    //   #630	-> 77
    //   #634	-> 86
    //   #631	-> 89
    //   #632	-> 90
    //   #633	-> 99
    //   #636	-> 109
    //   #638	-> 113
    //   #642	-> 124
    //   #644	-> 124
    //   #648	-> 135
    //   #645	-> 138
    //   #646	-> 139
    //   #647	-> 148
    //   #639	-> 158
    //   #640	-> 159
    //   #641	-> 168
    //   #651	-> 178
    //   #625	-> 185
    //   #626	-> 194
    //   #653	-> 204
    //   #654	-> 208
    //   #655	-> 214
    //   #657	-> 227
    //   #659	-> 236
    //   #660	-> 248
    //   #661	-> 256
    //   #662	-> 265
    //   #664	-> 274
    //   #613	-> 279
    // Exception table:
    //   from	to	target	type
    //   3	9	279	finally
    //   9	15	279	finally
    //   15	21	279	finally
    //   21	25	279	finally
    //   25	31	279	finally
    //   38	47	279	finally
    //   47	57	279	finally
    //   57	65	279	finally
    //   69	73	279	finally
    //   77	86	89	android/os/RemoteException
    //   77	86	279	finally
    //   90	99	279	finally
    //   99	109	279	finally
    //   113	124	158	android/os/RemoteException
    //   113	124	279	finally
    //   124	135	138	android/os/RemoteException
    //   124	135	279	finally
    //   139	148	279	finally
    //   148	158	279	finally
    //   159	168	279	finally
    //   168	178	279	finally
    //   178	182	279	finally
    //   185	194	279	finally
    //   194	204	279	finally
    //   208	214	279	finally
    //   214	227	279	finally
    //   227	231	279	finally
    //   236	248	279	finally
    //   256	265	279	finally
    //   265	274	279	finally
  }
  
  private static INfcAdapter getServiceInterface() {
    IBinder iBinder = ServiceManager.getService("nfc");
    if (iBinder == null)
      return null; 
    return INfcAdapter.Stub.asInterface(iBinder);
  }
  
  public static NfcAdapter getDefaultAdapter(Context paramContext) {
    if (paramContext != null) {
      paramContext = paramContext.getApplicationContext();
      if (paramContext != null) {
        NfcManager nfcManager = (NfcManager)paramContext.getSystemService("nfc");
        if (nfcManager == null)
          return null; 
        return nfcManager.getDefaultAdapter();
      } 
      throw new IllegalArgumentException("context not associated with any application (using a mock context?)");
    } 
    throw new IllegalArgumentException("context cannot be null");
  }
  
  @Deprecated
  public static NfcAdapter getDefaultAdapter() {
    Log.w("NFC", "WARNING: NfcAdapter.getDefaultAdapter() is deprecated, use NfcAdapter.getDefaultAdapter(Context) instead", new Exception());
    return getNfcAdapter(null);
  }
  
  public Context getContext() {
    return this.mContext;
  }
  
  public INfcAdapter getService() {
    isEnabled();
    return sService;
  }
  
  public INfcTag getTagService() {
    isEnabled();
    return sTagService;
  }
  
  public INfcCardEmulation getCardEmulationService() {
    isEnabled();
    return sCardEmulationService;
  }
  
  public INfcFCardEmulation getNfcFCardEmulationService() {
    isEnabled();
    return sNfcFCardEmulationService;
  }
  
  public INfcDta getNfcDtaInterface() {
    Context context = this.mContext;
    if (context != null)
      try {
        return sService.getNfcDtaInterface(context.getPackageName());
      } catch (RemoteException remoteException) {
        attemptDeadServiceRecovery((Exception)remoteException);
        return null;
      }  
    throw new UnsupportedOperationException("You need a context on NfcAdapter to use the  NFC extras APIs");
  }
  
  public void attemptDeadServiceRecovery(Exception paramException) {
    Log.e("NFC", "NFC service dead - attempting to recover", paramException);
    INfcAdapter iNfcAdapter = getServiceInterface();
    if (iNfcAdapter == null) {
      Log.e("NFC", "could not retrieve NFC service during service recovery");
      return;
    } 
    sService = iNfcAdapter;
    try {
      sTagService = iNfcAdapter.getNfcTagInterface();
      try {
        sCardEmulationService = iNfcAdapter.getNfcCardEmulationInterface();
      } catch (RemoteException remoteException) {
        Log.e("NFC", "could not retrieve NFC card emulation service during service recovery");
      } 
      try {
        sNfcFCardEmulationService = iNfcAdapter.getNfcFCardEmulationInterface();
      } catch (RemoteException remoteException) {
        Log.e("NFC", "could not retrieve NFC-F card emulation service during service recovery");
      } 
      return;
    } catch (RemoteException remoteException) {
      Log.e("NFC", "could not retrieve NFC tag service during service recovery");
      return;
    } 
  }
  
  public boolean isEnabled() {
    boolean bool1 = true, bool2 = true;
    try {
      int i = sService.getState();
      if (i != 3)
        bool2 = false; 
      return bool2;
    } catch (RemoteException remoteException) {
      attemptDeadServiceRecovery((Exception)remoteException);
      try {
        int i = sService.getState();
        if (i == 3) {
          bool2 = bool1;
        } else {
          bool2 = false;
        } 
        return bool2;
      } catch (RemoteException remoteException1) {
        attemptDeadServiceRecovery((Exception)remoteException1);
        return false;
      } 
    } 
  }
  
  public int getAdapterState() {
    try {
      return sService.getState();
    } catch (RemoteException remoteException) {
      attemptDeadServiceRecovery((Exception)remoteException);
      try {
        return sService.getState();
      } catch (RemoteException remoteException1) {
        attemptDeadServiceRecovery((Exception)remoteException1);
        return 1;
      } 
    } 
  }
  
  @SystemApi
  public boolean enable() {
    try {
      if (this.mIsSupportJapanFelica) {
        if (!"jp.co.fsi.felicalock".equals(this.mContext.getPackageName())) {
          Context context = this.mContext;
          if (!"com.coloros.wirelesssettings".equals(context.getPackageName())) {
            context = this.mContext;
            if (!"com.felicanetworks.test.android.nfcservice.settings".equals(context.getPackageName())) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("NfcAdapter enable() cannot run , packageName =  ");
              stringBuilder.append(this.mContext.getPackageName());
              Log.e("NFC", stringBuilder.toString());
              return false;
            } 
          } 
        } 
        return sService.enable();
      } 
      return sService.enable();
    } catch (RemoteException remoteException) {
      attemptDeadServiceRecovery((Exception)remoteException);
      try {
        return sService.enable();
      } catch (RemoteException remoteException1) {
        attemptDeadServiceRecovery((Exception)remoteException1);
        return false;
      } 
    } 
  }
  
  @SystemApi
  public boolean disable() {
    try {
      if (this.mIsSupportJapanFelica) {
        if (!"jp.co.fsi.felicalock".equals(this.mContext.getPackageName())) {
          Context context = this.mContext;
          if (!"com.coloros.wirelesssettings".equals(context.getPackageName())) {
            context = this.mContext;
            if (!"com.felicanetworks.test.android.nfcservice.settings".equals(context.getPackageName())) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("NfcAdapter disable() cannot run , packageName =  ");
              stringBuilder.append(this.mContext.getPackageName());
              Log.e("NFC", stringBuilder.toString());
              return false;
            } 
          } 
        } 
        return sService.disable(true);
      } 
      return sService.disable(true);
    } catch (RemoteException remoteException) {
      attemptDeadServiceRecovery((Exception)remoteException);
      try {
        return sService.disable(true);
      } catch (RemoteException remoteException1) {
        attemptDeadServiceRecovery((Exception)remoteException1);
        return false;
      } 
    } 
  }
  
  @SystemApi
  public boolean disable(boolean paramBoolean) {
    try {
      if (this.mIsSupportJapanFelica) {
        if (!"jp.co.fsi.felicalock".equals(this.mContext.getPackageName())) {
          Context context = this.mContext;
          if (!"com.coloros.wirelesssettings".equals(context.getPackageName())) {
            context = this.mContext;
            if (!"com.felicanetworks.test.android.nfcservice.settings".equals(context.getPackageName())) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("NfcAdapter disable() cannot run , packageName =  ");
              stringBuilder.append(this.mContext.getPackageName());
              Log.e("NFC", stringBuilder.toString());
              return false;
            } 
          } 
        } 
        return sService.disable(paramBoolean);
      } 
      return sService.disable(paramBoolean);
    } catch (RemoteException remoteException) {
      attemptDeadServiceRecovery((Exception)remoteException);
      try {
        return sService.disable(paramBoolean);
      } catch (RemoteException remoteException1) {
        attemptDeadServiceRecovery((Exception)remoteException1);
        return false;
      } 
    } 
  }
  
  public void pausePolling(int paramInt) {
    try {
      sService.pausePolling(paramInt);
    } catch (RemoteException remoteException) {
      attemptDeadServiceRecovery((Exception)remoteException);
    } 
  }
  
  public void resumePolling() {
    try {
      sService.resumePolling();
    } catch (RemoteException remoteException) {
      attemptDeadServiceRecovery((Exception)remoteException);
    } 
  }
  
  @Deprecated
  public void setBeamPushUris(Uri[] paramArrayOfUri, Activity paramActivity) {
    // Byte code:
    //   0: ldc android/nfc/NfcAdapter
    //   2: monitorenter
    //   3: getstatic android/nfc/NfcAdapter.sHasNfcFeature : Z
    //   6: ifeq -> 136
    //   9: getstatic android/nfc/NfcAdapter.sHasBeamFeature : Z
    //   12: ifne -> 19
    //   15: ldc android/nfc/NfcAdapter
    //   17: monitorexit
    //   18: return
    //   19: ldc android/nfc/NfcAdapter
    //   21: monitorexit
    //   22: aload_2
    //   23: ifnull -> 125
    //   26: aload_1
    //   27: ifnull -> 115
    //   30: aload_1
    //   31: arraylength
    //   32: istore_3
    //   33: iconst_0
    //   34: istore #4
    //   36: iload #4
    //   38: iload_3
    //   39: if_icmpge -> 115
    //   42: aload_1
    //   43: iload #4
    //   45: aaload
    //   46: astore #5
    //   48: aload #5
    //   50: ifnull -> 104
    //   53: aload #5
    //   55: invokevirtual getScheme : ()Ljava/lang/String;
    //   58: astore #5
    //   60: aload #5
    //   62: ifnull -> 93
    //   65: aload #5
    //   67: ldc_w 'file'
    //   70: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   73: ifne -> 87
    //   76: aload #5
    //   78: ldc_w 'content'
    //   81: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   84: ifeq -> 93
    //   87: iinc #4, 1
    //   90: goto -> 36
    //   93: new java/lang/IllegalArgumentException
    //   96: dup
    //   97: ldc_w 'URI needs to have either scheme file or scheme content'
    //   100: invokespecial <init> : (Ljava/lang/String;)V
    //   103: athrow
    //   104: new java/lang/NullPointerException
    //   107: dup
    //   108: ldc_w 'Uri not allowed to be null'
    //   111: invokespecial <init> : (Ljava/lang/String;)V
    //   114: athrow
    //   115: aload_0
    //   116: getfield mNfcActivityManager : Landroid/nfc/NfcActivityManager;
    //   119: aload_2
    //   120: aload_1
    //   121: invokevirtual setNdefPushContentUri : (Landroid/app/Activity;[Landroid/net/Uri;)V
    //   124: return
    //   125: new java/lang/NullPointerException
    //   128: dup
    //   129: ldc_w 'activity cannot be null'
    //   132: invokespecial <init> : (Ljava/lang/String;)V
    //   135: athrow
    //   136: new java/lang/UnsupportedOperationException
    //   139: astore_1
    //   140: aload_1
    //   141: invokespecial <init> : ()V
    //   144: aload_1
    //   145: athrow
    //   146: astore_1
    //   147: ldc android/nfc/NfcAdapter
    //   149: monitorexit
    //   150: aload_1
    //   151: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1141	-> 0
    //   #1142	-> 3
    //   #1145	-> 9
    //   #1146	-> 15
    //   #1148	-> 19
    //   #1149	-> 22
    //   #1152	-> 26
    //   #1153	-> 30
    //   #1154	-> 48
    //   #1156	-> 53
    //   #1157	-> 60
    //   #1158	-> 76
    //   #1153	-> 87
    //   #1159	-> 93
    //   #1154	-> 104
    //   #1164	-> 115
    //   #1165	-> 124
    //   #1150	-> 125
    //   #1143	-> 136
    //   #1148	-> 146
    // Exception table:
    //   from	to	target	type
    //   3	9	146	finally
    //   9	15	146	finally
    //   15	18	146	finally
    //   19	22	146	finally
    //   136	146	146	finally
    //   147	150	146	finally
  }
  
  @Deprecated
  public void setBeamPushUrisCallback(CreateBeamUrisCallback paramCreateBeamUrisCallback, Activity paramActivity) {
    // Byte code:
    //   0: ldc android/nfc/NfcAdapter
    //   2: monitorenter
    //   3: getstatic android/nfc/NfcAdapter.sHasNfcFeature : Z
    //   6: ifeq -> 47
    //   9: getstatic android/nfc/NfcAdapter.sHasBeamFeature : Z
    //   12: ifne -> 19
    //   15: ldc android/nfc/NfcAdapter
    //   17: monitorexit
    //   18: return
    //   19: ldc android/nfc/NfcAdapter
    //   21: monitorexit
    //   22: aload_2
    //   23: ifnull -> 36
    //   26: aload_0
    //   27: getfield mNfcActivityManager : Landroid/nfc/NfcActivityManager;
    //   30: aload_2
    //   31: aload_1
    //   32: invokevirtual setNdefPushContentUriCallback : (Landroid/app/Activity;Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;)V
    //   35: return
    //   36: new java/lang/NullPointerException
    //   39: dup
    //   40: ldc_w 'activity cannot be null'
    //   43: invokespecial <init> : (Ljava/lang/String;)V
    //   46: athrow
    //   47: new java/lang/UnsupportedOperationException
    //   50: astore_1
    //   51: aload_1
    //   52: invokespecial <init> : ()V
    //   55: aload_1
    //   56: athrow
    //   57: astore_1
    //   58: ldc android/nfc/NfcAdapter
    //   60: monitorexit
    //   61: aload_1
    //   62: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1229	-> 0
    //   #1230	-> 3
    //   #1233	-> 9
    //   #1234	-> 15
    //   #1236	-> 19
    //   #1237	-> 22
    //   #1240	-> 26
    //   #1241	-> 35
    //   #1238	-> 36
    //   #1231	-> 47
    //   #1236	-> 57
    // Exception table:
    //   from	to	target	type
    //   3	9	57	finally
    //   9	15	57	finally
    //   15	18	57	finally
    //   19	22	57	finally
    //   47	57	57	finally
    //   58	61	57	finally
  }
  
  @Deprecated
  public void setNdefPushMessage(NdefMessage paramNdefMessage, Activity paramActivity, Activity... paramVarArgs) {
    // Byte code:
    //   0: ldc android/nfc/NfcAdapter
    //   2: monitorenter
    //   3: getstatic android/nfc/NfcAdapter.sHasNfcFeature : Z
    //   6: ifeq -> 134
    //   9: getstatic android/nfc/NfcAdapter.sHasBeamFeature : Z
    //   12: ifne -> 19
    //   15: ldc android/nfc/NfcAdapter
    //   17: monitorexit
    //   18: return
    //   19: ldc android/nfc/NfcAdapter
    //   21: monitorexit
    //   22: aload_0
    //   23: invokevirtual getSdkVersion : ()I
    //   26: istore #4
    //   28: aload_2
    //   29: ifnull -> 101
    //   32: aload_0
    //   33: getfield mNfcActivityManager : Landroid/nfc/NfcActivityManager;
    //   36: aload_2
    //   37: aload_1
    //   38: iconst_0
    //   39: invokevirtual setNdefPushMessage : (Landroid/app/Activity;Landroid/nfc/NdefMessage;I)V
    //   42: aload_3
    //   43: arraylength
    //   44: istore #5
    //   46: iconst_0
    //   47: istore #6
    //   49: iload #6
    //   51: iload #5
    //   53: if_icmpge -> 94
    //   56: aload_3
    //   57: iload #6
    //   59: aaload
    //   60: astore_2
    //   61: aload_2
    //   62: ifnull -> 81
    //   65: aload_0
    //   66: getfield mNfcActivityManager : Landroid/nfc/NfcActivityManager;
    //   69: aload_2
    //   70: aload_1
    //   71: iconst_0
    //   72: invokevirtual setNdefPushMessage : (Landroid/app/Activity;Landroid/nfc/NdefMessage;I)V
    //   75: iinc #6, 1
    //   78: goto -> 49
    //   81: new java/lang/NullPointerException
    //   84: astore_1
    //   85: aload_1
    //   86: ldc_w 'activities cannot contain null'
    //   89: invokespecial <init> : (Ljava/lang/String;)V
    //   92: aload_1
    //   93: athrow
    //   94: goto -> 131
    //   97: astore_1
    //   98: goto -> 114
    //   101: new java/lang/NullPointerException
    //   104: astore_1
    //   105: aload_1
    //   106: ldc_w 'activity cannot be null'
    //   109: invokespecial <init> : (Ljava/lang/String;)V
    //   112: aload_1
    //   113: athrow
    //   114: iload #4
    //   116: bipush #16
    //   118: if_icmpge -> 132
    //   121: ldc 'NFC'
    //   123: ldc_w 'Cannot call API with Activity that has already been destroyed'
    //   126: aload_1
    //   127: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   130: pop
    //   131: return
    //   132: aload_1
    //   133: athrow
    //   134: new java/lang/UnsupportedOperationException
    //   137: astore_1
    //   138: aload_1
    //   139: invokespecial <init> : ()V
    //   142: aload_1
    //   143: athrow
    //   144: astore_1
    //   145: ldc android/nfc/NfcAdapter
    //   147: monitorexit
    //   148: aload_1
    //   149: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1320	-> 0
    //   #1321	-> 3
    //   #1324	-> 9
    //   #1325	-> 15
    //   #1327	-> 19
    //   #1328	-> 22
    //   #1330	-> 28
    //   #1333	-> 32
    //   #1334	-> 42
    //   #1335	-> 61
    //   #1338	-> 65
    //   #1334	-> 75
    //   #1336	-> 81
    //   #1349	-> 94
    //   #1340	-> 97
    //   #1331	-> 101
    //   #1341	-> 114
    //   #1343	-> 121
    //   #1350	-> 131
    //   #1347	-> 132
    //   #1322	-> 134
    //   #1327	-> 144
    // Exception table:
    //   from	to	target	type
    //   3	9	144	finally
    //   9	15	144	finally
    //   15	18	144	finally
    //   19	22	144	finally
    //   32	42	97	java/lang/IllegalStateException
    //   42	46	97	java/lang/IllegalStateException
    //   65	75	97	java/lang/IllegalStateException
    //   81	94	97	java/lang/IllegalStateException
    //   101	114	97	java/lang/IllegalStateException
    //   134	144	144	finally
    //   145	148	144	finally
  }
  
  @SystemApi
  public void setNdefPushMessage(NdefMessage paramNdefMessage, Activity paramActivity, int paramInt) {
    // Byte code:
    //   0: ldc android/nfc/NfcAdapter
    //   2: monitorenter
    //   3: getstatic android/nfc/NfcAdapter.sHasNfcFeature : Z
    //   6: ifeq -> 38
    //   9: ldc android/nfc/NfcAdapter
    //   11: monitorexit
    //   12: aload_2
    //   13: ifnull -> 27
    //   16: aload_0
    //   17: getfield mNfcActivityManager : Landroid/nfc/NfcActivityManager;
    //   20: aload_2
    //   21: aload_1
    //   22: iload_3
    //   23: invokevirtual setNdefPushMessage : (Landroid/app/Activity;Landroid/nfc/NdefMessage;I)V
    //   26: return
    //   27: new java/lang/NullPointerException
    //   30: dup
    //   31: ldc_w 'activity cannot be null'
    //   34: invokespecial <init> : (Ljava/lang/String;)V
    //   37: athrow
    //   38: new java/lang/UnsupportedOperationException
    //   41: astore_1
    //   42: aload_1
    //   43: invokespecial <init> : ()V
    //   46: aload_1
    //   47: athrow
    //   48: astore_1
    //   49: ldc android/nfc/NfcAdapter
    //   51: monitorexit
    //   52: aload_1
    //   53: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1357	-> 0
    //   #1358	-> 3
    //   #1361	-> 9
    //   #1362	-> 12
    //   #1365	-> 16
    //   #1366	-> 26
    //   #1363	-> 27
    //   #1359	-> 38
    //   #1361	-> 48
    // Exception table:
    //   from	to	target	type
    //   3	9	48	finally
    //   9	12	48	finally
    //   38	48	48	finally
    //   49	52	48	finally
  }
  
  @Deprecated
  public void setNdefPushMessageCallback(CreateNdefMessageCallback paramCreateNdefMessageCallback, Activity paramActivity, Activity... paramVarArgs) {
    // Byte code:
    //   0: ldc android/nfc/NfcAdapter
    //   2: monitorenter
    //   3: getstatic android/nfc/NfcAdapter.sHasNfcFeature : Z
    //   6: ifeq -> 134
    //   9: getstatic android/nfc/NfcAdapter.sHasBeamFeature : Z
    //   12: ifne -> 19
    //   15: ldc android/nfc/NfcAdapter
    //   17: monitorexit
    //   18: return
    //   19: ldc android/nfc/NfcAdapter
    //   21: monitorexit
    //   22: aload_0
    //   23: invokevirtual getSdkVersion : ()I
    //   26: istore #4
    //   28: aload_2
    //   29: ifnull -> 101
    //   32: aload_0
    //   33: getfield mNfcActivityManager : Landroid/nfc/NfcActivityManager;
    //   36: aload_2
    //   37: aload_1
    //   38: iconst_0
    //   39: invokevirtual setNdefPushMessageCallback : (Landroid/app/Activity;Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;I)V
    //   42: aload_3
    //   43: arraylength
    //   44: istore #5
    //   46: iconst_0
    //   47: istore #6
    //   49: iload #6
    //   51: iload #5
    //   53: if_icmpge -> 94
    //   56: aload_3
    //   57: iload #6
    //   59: aaload
    //   60: astore_2
    //   61: aload_2
    //   62: ifnull -> 81
    //   65: aload_0
    //   66: getfield mNfcActivityManager : Landroid/nfc/NfcActivityManager;
    //   69: aload_2
    //   70: aload_1
    //   71: iconst_0
    //   72: invokevirtual setNdefPushMessageCallback : (Landroid/app/Activity;Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;I)V
    //   75: iinc #6, 1
    //   78: goto -> 49
    //   81: new java/lang/NullPointerException
    //   84: astore_1
    //   85: aload_1
    //   86: ldc_w 'activities cannot contain null'
    //   89: invokespecial <init> : (Ljava/lang/String;)V
    //   92: aload_1
    //   93: athrow
    //   94: goto -> 131
    //   97: astore_1
    //   98: goto -> 114
    //   101: new java/lang/NullPointerException
    //   104: astore_1
    //   105: aload_1
    //   106: ldc_w 'activity cannot be null'
    //   109: invokespecial <init> : (Ljava/lang/String;)V
    //   112: aload_1
    //   113: athrow
    //   114: iload #4
    //   116: bipush #16
    //   118: if_icmpge -> 132
    //   121: ldc 'NFC'
    //   123: ldc_w 'Cannot call API with Activity that has already been destroyed'
    //   126: aload_1
    //   127: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   130: pop
    //   131: return
    //   132: aload_1
    //   133: athrow
    //   134: new java/lang/UnsupportedOperationException
    //   137: astore_1
    //   138: aload_1
    //   139: invokespecial <init> : ()V
    //   142: aload_1
    //   143: athrow
    //   144: astore_1
    //   145: ldc android/nfc/NfcAdapter
    //   147: monitorexit
    //   148: aload_1
    //   149: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1439	-> 0
    //   #1440	-> 3
    //   #1443	-> 9
    //   #1444	-> 15
    //   #1446	-> 19
    //   #1447	-> 22
    //   #1449	-> 28
    //   #1452	-> 32
    //   #1453	-> 42
    //   #1454	-> 61
    //   #1457	-> 65
    //   #1453	-> 75
    //   #1455	-> 81
    //   #1468	-> 94
    //   #1459	-> 97
    //   #1450	-> 101
    //   #1460	-> 114
    //   #1462	-> 121
    //   #1469	-> 131
    //   #1466	-> 132
    //   #1441	-> 134
    //   #1446	-> 144
    // Exception table:
    //   from	to	target	type
    //   3	9	144	finally
    //   9	15	144	finally
    //   15	18	144	finally
    //   19	22	144	finally
    //   32	42	97	java/lang/IllegalStateException
    //   42	46	97	java/lang/IllegalStateException
    //   65	75	97	java/lang/IllegalStateException
    //   81	94	97	java/lang/IllegalStateException
    //   101	114	97	java/lang/IllegalStateException
    //   134	144	144	finally
    //   145	148	144	finally
  }
  
  public void setNdefPushMessageCallback(CreateNdefMessageCallback paramCreateNdefMessageCallback, Activity paramActivity, int paramInt) {
    if (paramActivity != null) {
      this.mNfcActivityManager.setNdefPushMessageCallback(paramActivity, paramCreateNdefMessageCallback, paramInt);
      return;
    } 
    throw new NullPointerException("activity cannot be null");
  }
  
  @Deprecated
  public void setOnNdefPushCompleteCallback(OnNdefPushCompleteCallback paramOnNdefPushCompleteCallback, Activity paramActivity, Activity... paramVarArgs) {
    // Byte code:
    //   0: ldc android/nfc/NfcAdapter
    //   2: monitorenter
    //   3: getstatic android/nfc/NfcAdapter.sHasNfcFeature : Z
    //   6: ifeq -> 132
    //   9: getstatic android/nfc/NfcAdapter.sHasBeamFeature : Z
    //   12: ifne -> 19
    //   15: ldc android/nfc/NfcAdapter
    //   17: monitorexit
    //   18: return
    //   19: ldc android/nfc/NfcAdapter
    //   21: monitorexit
    //   22: aload_0
    //   23: invokevirtual getSdkVersion : ()I
    //   26: istore #4
    //   28: aload_2
    //   29: ifnull -> 99
    //   32: aload_0
    //   33: getfield mNfcActivityManager : Landroid/nfc/NfcActivityManager;
    //   36: aload_2
    //   37: aload_1
    //   38: invokevirtual setOnNdefPushCompleteCallback : (Landroid/app/Activity;Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;)V
    //   41: aload_3
    //   42: arraylength
    //   43: istore #5
    //   45: iconst_0
    //   46: istore #6
    //   48: iload #6
    //   50: iload #5
    //   52: if_icmpge -> 92
    //   55: aload_3
    //   56: iload #6
    //   58: aaload
    //   59: astore_2
    //   60: aload_2
    //   61: ifnull -> 79
    //   64: aload_0
    //   65: getfield mNfcActivityManager : Landroid/nfc/NfcActivityManager;
    //   68: aload_2
    //   69: aload_1
    //   70: invokevirtual setOnNdefPushCompleteCallback : (Landroid/app/Activity;Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;)V
    //   73: iinc #6, 1
    //   76: goto -> 48
    //   79: new java/lang/NullPointerException
    //   82: astore_1
    //   83: aload_1
    //   84: ldc_w 'activities cannot contain null'
    //   87: invokespecial <init> : (Ljava/lang/String;)V
    //   90: aload_1
    //   91: athrow
    //   92: goto -> 129
    //   95: astore_1
    //   96: goto -> 112
    //   99: new java/lang/NullPointerException
    //   102: astore_1
    //   103: aload_1
    //   104: ldc_w 'activity cannot be null'
    //   107: invokespecial <init> : (Ljava/lang/String;)V
    //   110: aload_1
    //   111: athrow
    //   112: iload #4
    //   114: bipush #16
    //   116: if_icmpge -> 130
    //   119: ldc 'NFC'
    //   121: ldc_w 'Cannot call API with Activity that has already been destroyed'
    //   124: aload_1
    //   125: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   128: pop
    //   129: return
    //   130: aload_1
    //   131: athrow
    //   132: new java/lang/UnsupportedOperationException
    //   135: astore_1
    //   136: aload_1
    //   137: invokespecial <init> : ()V
    //   140: aload_1
    //   141: athrow
    //   142: astore_1
    //   143: ldc android/nfc/NfcAdapter
    //   145: monitorexit
    //   146: aload_1
    //   147: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1526	-> 0
    //   #1527	-> 3
    //   #1530	-> 9
    //   #1531	-> 15
    //   #1533	-> 19
    //   #1534	-> 22
    //   #1536	-> 28
    //   #1539	-> 32
    //   #1540	-> 41
    //   #1541	-> 60
    //   #1544	-> 64
    //   #1540	-> 73
    //   #1542	-> 79
    //   #1555	-> 92
    //   #1546	-> 95
    //   #1537	-> 99
    //   #1547	-> 112
    //   #1549	-> 119
    //   #1556	-> 129
    //   #1553	-> 130
    //   #1528	-> 132
    //   #1533	-> 142
    // Exception table:
    //   from	to	target	type
    //   3	9	142	finally
    //   9	15	142	finally
    //   15	18	142	finally
    //   19	22	142	finally
    //   32	41	95	java/lang/IllegalStateException
    //   41	45	95	java/lang/IllegalStateException
    //   64	73	95	java/lang/IllegalStateException
    //   79	92	95	java/lang/IllegalStateException
    //   99	112	95	java/lang/IllegalStateException
    //   132	142	142	finally
    //   143	146	142	finally
  }
  
  public void enableForegroundDispatch(Activity paramActivity, PendingIntent paramPendingIntent, IntentFilter[] paramArrayOfIntentFilter, String[][] paramArrayOfString) {
    // Byte code:
    //   0: ldc android/nfc/NfcAdapter
    //   2: monitorenter
    //   3: getstatic android/nfc/NfcAdapter.sHasNfcFeature : Z
    //   6: ifeq -> 107
    //   9: ldc android/nfc/NfcAdapter
    //   11: monitorexit
    //   12: aload_1
    //   13: ifnull -> 105
    //   16: aload_2
    //   17: ifnull -> 105
    //   20: aload_1
    //   21: invokevirtual isResumed : ()Z
    //   24: ifeq -> 94
    //   27: aconst_null
    //   28: astore #5
    //   30: aload #5
    //   32: astore #6
    //   34: aload #4
    //   36: ifnull -> 61
    //   39: aload #5
    //   41: astore #6
    //   43: aload #4
    //   45: arraylength
    //   46: ifle -> 61
    //   49: new android/nfc/TechListParcel
    //   52: astore #6
    //   54: aload #6
    //   56: aload #4
    //   58: invokespecial <init> : ([[Ljava/lang/String;)V
    //   61: invokestatic currentActivityThread : ()Landroid/app/ActivityThread;
    //   64: aload_1
    //   65: aload_0
    //   66: getfield mForegroundDispatchListener : Landroid/app/OnActivityPausedListener;
    //   69: invokevirtual registerOnActivityPausedListener : (Landroid/app/Activity;Landroid/app/OnActivityPausedListener;)V
    //   72: getstatic android/nfc/NfcAdapter.sService : Landroid/nfc/INfcAdapter;
    //   75: aload_2
    //   76: aload_3
    //   77: aload #6
    //   79: invokeinterface setForegroundDispatch : (Landroid/app/PendingIntent;[Landroid/content/IntentFilter;Landroid/nfc/TechListParcel;)V
    //   84: goto -> 93
    //   87: astore_1
    //   88: aload_0
    //   89: aload_1
    //   90: invokevirtual attemptDeadServiceRecovery : (Ljava/lang/Exception;)V
    //   93: return
    //   94: new java/lang/IllegalStateException
    //   97: dup
    //   98: ldc_w 'Foreground dispatch can only be enabled when your activity is resumed'
    //   101: invokespecial <init> : (Ljava/lang/String;)V
    //   104: athrow
    //   105: aconst_null
    //   106: athrow
    //   107: new java/lang/UnsupportedOperationException
    //   110: astore_1
    //   111: aload_1
    //   112: invokespecial <init> : ()V
    //   115: aload_1
    //   116: athrow
    //   117: astore_1
    //   118: ldc android/nfc/NfcAdapter
    //   120: monitorexit
    //   121: aload_1
    //   122: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1594	-> 0
    //   #1595	-> 3
    //   #1598	-> 9
    //   #1599	-> 12
    //   #1602	-> 20
    //   #1607	-> 27
    //   #1608	-> 30
    //   #1609	-> 49
    //   #1611	-> 61
    //   #1613	-> 72
    //   #1616	-> 84
    //   #1614	-> 87
    //   #1615	-> 88
    //   #1617	-> 93
    //   #1603	-> 94
    //   #1600	-> 105
    //   #1596	-> 107
    //   #1598	-> 117
    // Exception table:
    //   from	to	target	type
    //   3	9	117	finally
    //   9	12	117	finally
    //   43	49	87	android/os/RemoteException
    //   49	61	87	android/os/RemoteException
    //   61	72	87	android/os/RemoteException
    //   72	84	87	android/os/RemoteException
    //   107	117	117	finally
    //   118	121	117	finally
  }
  
  public void disableForegroundDispatch(Activity paramActivity) {
    // Byte code:
    //   0: ldc android/nfc/NfcAdapter
    //   2: monitorenter
    //   3: getstatic android/nfc/NfcAdapter.sHasNfcFeature : Z
    //   6: ifeq -> 30
    //   9: ldc android/nfc/NfcAdapter
    //   11: monitorexit
    //   12: invokestatic currentActivityThread : ()Landroid/app/ActivityThread;
    //   15: aload_1
    //   16: aload_0
    //   17: getfield mForegroundDispatchListener : Landroid/app/OnActivityPausedListener;
    //   20: invokevirtual unregisterOnActivityPausedListener : (Landroid/app/Activity;Landroid/app/OnActivityPausedListener;)V
    //   23: aload_0
    //   24: aload_1
    //   25: iconst_0
    //   26: invokevirtual disableForegroundDispatchInternal : (Landroid/app/Activity;Z)V
    //   29: return
    //   30: new java/lang/UnsupportedOperationException
    //   33: astore_1
    //   34: aload_1
    //   35: invokespecial <init> : ()V
    //   38: aload_1
    //   39: athrow
    //   40: astore_1
    //   41: ldc android/nfc/NfcAdapter
    //   43: monitorexit
    //   44: aload_1
    //   45: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1635	-> 0
    //   #1636	-> 3
    //   #1639	-> 9
    //   #1640	-> 12
    //   #1642	-> 23
    //   #1643	-> 29
    //   #1637	-> 30
    //   #1639	-> 40
    // Exception table:
    //   from	to	target	type
    //   3	9	40	finally
    //   9	12	40	finally
    //   30	40	40	finally
    //   41	44	40	finally
  }
  
  NfcAdapter(Context paramContext) {
    this.mForegroundDispatchListener = (OnActivityPausedListener)new Object(this);
    this.mContext = paramContext;
    this.mNfcActivityManager = new NfcActivityManager(this);
    this.mNfcUnlockHandlers = new HashMap<>();
    this.mTagRemovedListener = null;
    this.mLock = new Object();
  }
  
  void disableForegroundDispatchInternal(Activity paramActivity, boolean paramBoolean) {
    try {
      sService.setForegroundDispatch(null, null, null);
      if (!paramBoolean && !paramActivity.isResumed()) {
        IllegalStateException illegalStateException = new IllegalStateException();
        this("You must disable foreground dispatching while your activity is still resumed");
        throw illegalStateException;
      } 
    } catch (RemoteException remoteException) {
      attemptDeadServiceRecovery((Exception)remoteException);
    } 
  }
  
  public void enableReaderMode(Activity paramActivity, ReaderCallback paramReaderCallback, int paramInt, Bundle paramBundle) {
    // Byte code:
    //   0: ldc android/nfc/NfcAdapter
    //   2: monitorenter
    //   3: getstatic android/nfc/NfcAdapter.sHasNfcFeature : Z
    //   6: ifeq -> 25
    //   9: ldc android/nfc/NfcAdapter
    //   11: monitorexit
    //   12: aload_0
    //   13: getfield mNfcActivityManager : Landroid/nfc/NfcActivityManager;
    //   16: aload_1
    //   17: aload_2
    //   18: iload_3
    //   19: aload #4
    //   21: invokevirtual enableReaderMode : (Landroid/app/Activity;Landroid/nfc/NfcAdapter$ReaderCallback;ILandroid/os/Bundle;)V
    //   24: return
    //   25: new java/lang/UnsupportedOperationException
    //   28: astore_1
    //   29: aload_1
    //   30: invokespecial <init> : ()V
    //   33: aload_1
    //   34: athrow
    //   35: astore_1
    //   36: ldc android/nfc/NfcAdapter
    //   38: monitorexit
    //   39: aload_1
    //   40: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1688	-> 0
    //   #1689	-> 3
    //   #1692	-> 9
    //   #1693	-> 12
    //   #1694	-> 24
    //   #1690	-> 25
    //   #1692	-> 35
    // Exception table:
    //   from	to	target	type
    //   3	9	35	finally
    //   9	12	35	finally
    //   25	35	35	finally
    //   36	39	35	finally
  }
  
  public void disableReaderMode(Activity paramActivity) {
    // Byte code:
    //   0: ldc android/nfc/NfcAdapter
    //   2: monitorenter
    //   3: getstatic android/nfc/NfcAdapter.sHasNfcFeature : Z
    //   6: ifeq -> 21
    //   9: ldc android/nfc/NfcAdapter
    //   11: monitorexit
    //   12: aload_0
    //   13: getfield mNfcActivityManager : Landroid/nfc/NfcActivityManager;
    //   16: aload_1
    //   17: invokevirtual disableReaderMode : (Landroid/app/Activity;)V
    //   20: return
    //   21: new java/lang/UnsupportedOperationException
    //   24: astore_1
    //   25: aload_1
    //   26: invokespecial <init> : ()V
    //   29: aload_1
    //   30: athrow
    //   31: astore_1
    //   32: ldc android/nfc/NfcAdapter
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1705	-> 0
    //   #1706	-> 3
    //   #1709	-> 9
    //   #1710	-> 12
    //   #1711	-> 20
    //   #1707	-> 21
    //   #1709	-> 31
    // Exception table:
    //   from	to	target	type
    //   3	9	31	finally
    //   9	12	31	finally
    //   21	31	31	finally
    //   32	35	31	finally
  }
  
  @Deprecated
  public boolean invokeBeam(Activity paramActivity) {
    // Byte code:
    //   0: ldc android/nfc/NfcAdapter
    //   2: monitorenter
    //   3: getstatic android/nfc/NfcAdapter.sHasNfcFeature : Z
    //   6: ifeq -> 70
    //   9: getstatic android/nfc/NfcAdapter.sHasBeamFeature : Z
    //   12: ifne -> 20
    //   15: ldc android/nfc/NfcAdapter
    //   17: monitorexit
    //   18: iconst_0
    //   19: ireturn
    //   20: ldc android/nfc/NfcAdapter
    //   22: monitorexit
    //   23: aload_1
    //   24: ifnull -> 59
    //   27: aload_0
    //   28: aload_1
    //   29: invokevirtual enforceResumed : (Landroid/app/Activity;)V
    //   32: getstatic android/nfc/NfcAdapter.sService : Landroid/nfc/INfcAdapter;
    //   35: invokeinterface invokeBeam : ()V
    //   40: iconst_1
    //   41: ireturn
    //   42: astore_1
    //   43: ldc 'NFC'
    //   45: ldc_w 'invokeBeam: NFC process has died.'
    //   48: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   51: pop
    //   52: aload_0
    //   53: aload_1
    //   54: invokevirtual attemptDeadServiceRecovery : (Ljava/lang/Exception;)V
    //   57: iconst_0
    //   58: ireturn
    //   59: new java/lang/NullPointerException
    //   62: dup
    //   63: ldc_w 'activity may not be null.'
    //   66: invokespecial <init> : (Ljava/lang/String;)V
    //   69: athrow
    //   70: new java/lang/UnsupportedOperationException
    //   73: astore_1
    //   74: aload_1
    //   75: invokespecial <init> : ()V
    //   78: aload_1
    //   79: athrow
    //   80: astore_1
    //   81: ldc android/nfc/NfcAdapter
    //   83: monitorexit
    //   84: aload_1
    //   85: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1742	-> 0
    //   #1743	-> 3
    //   #1746	-> 9
    //   #1747	-> 15
    //   #1749	-> 20
    //   #1750	-> 23
    //   #1753	-> 27
    //   #1755	-> 32
    //   #1756	-> 40
    //   #1757	-> 42
    //   #1758	-> 43
    //   #1759	-> 52
    //   #1760	-> 57
    //   #1751	-> 59
    //   #1744	-> 70
    //   #1749	-> 80
    // Exception table:
    //   from	to	target	type
    //   3	9	80	finally
    //   9	15	80	finally
    //   15	18	80	finally
    //   20	23	80	finally
    //   32	40	42	android/os/RemoteException
    //   70	80	80	finally
    //   81	84	80	finally
  }
  
  public boolean invokeBeam(BeamShareData paramBeamShareData) {
    try {
      Log.e("NFC", "invokeBeamInternal()");
      sService.invokeBeamInternal(paramBeamShareData);
      return true;
    } catch (RemoteException remoteException) {
      Log.e("NFC", "invokeBeam: NFC process has died.");
      attemptDeadServiceRecovery((Exception)remoteException);
      return false;
    } 
  }
  
  @Deprecated
  public void enableForegroundNdefPush(Activity paramActivity, NdefMessage paramNdefMessage) {
    // Byte code:
    //   0: ldc android/nfc/NfcAdapter
    //   2: monitorenter
    //   3: getstatic android/nfc/NfcAdapter.sHasNfcFeature : Z
    //   6: ifeq -> 48
    //   9: getstatic android/nfc/NfcAdapter.sHasBeamFeature : Z
    //   12: ifne -> 19
    //   15: ldc android/nfc/NfcAdapter
    //   17: monitorexit
    //   18: return
    //   19: ldc android/nfc/NfcAdapter
    //   21: monitorexit
    //   22: aload_1
    //   23: ifnull -> 46
    //   26: aload_2
    //   27: ifnull -> 46
    //   30: aload_0
    //   31: aload_1
    //   32: invokevirtual enforceResumed : (Landroid/app/Activity;)V
    //   35: aload_0
    //   36: getfield mNfcActivityManager : Landroid/nfc/NfcActivityManager;
    //   39: aload_1
    //   40: aload_2
    //   41: iconst_0
    //   42: invokevirtual setNdefPushMessage : (Landroid/app/Activity;Landroid/nfc/NdefMessage;I)V
    //   45: return
    //   46: aconst_null
    //   47: athrow
    //   48: new java/lang/UnsupportedOperationException
    //   51: astore_1
    //   52: aload_1
    //   53: invokespecial <init> : ()V
    //   56: aload_1
    //   57: athrow
    //   58: astore_1
    //   59: ldc android/nfc/NfcAdapter
    //   61: monitorexit
    //   62: aload_1
    //   63: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1808	-> 0
    //   #1809	-> 3
    //   #1812	-> 9
    //   #1813	-> 15
    //   #1815	-> 19
    //   #1816	-> 22
    //   #1819	-> 30
    //   #1820	-> 35
    //   #1821	-> 45
    //   #1817	-> 46
    //   #1810	-> 48
    //   #1815	-> 58
    // Exception table:
    //   from	to	target	type
    //   3	9	58	finally
    //   9	15	58	finally
    //   15	18	58	finally
    //   19	22	58	finally
    //   48	58	58	finally
    //   59	62	58	finally
  }
  
  @Deprecated
  public void disableForegroundNdefPush(Activity paramActivity) {
    // Byte code:
    //   0: ldc android/nfc/NfcAdapter
    //   2: monitorenter
    //   3: getstatic android/nfc/NfcAdapter.sHasNfcFeature : Z
    //   6: ifeq -> 63
    //   9: getstatic android/nfc/NfcAdapter.sHasBeamFeature : Z
    //   12: ifne -> 19
    //   15: ldc android/nfc/NfcAdapter
    //   17: monitorexit
    //   18: return
    //   19: ldc android/nfc/NfcAdapter
    //   21: monitorexit
    //   22: aload_1
    //   23: ifnull -> 61
    //   26: aload_0
    //   27: aload_1
    //   28: invokevirtual enforceResumed : (Landroid/app/Activity;)V
    //   31: aload_0
    //   32: getfield mNfcActivityManager : Landroid/nfc/NfcActivityManager;
    //   35: aload_1
    //   36: aconst_null
    //   37: iconst_0
    //   38: invokevirtual setNdefPushMessage : (Landroid/app/Activity;Landroid/nfc/NdefMessage;I)V
    //   41: aload_0
    //   42: getfield mNfcActivityManager : Landroid/nfc/NfcActivityManager;
    //   45: aload_1
    //   46: aconst_null
    //   47: iconst_0
    //   48: invokevirtual setNdefPushMessageCallback : (Landroid/app/Activity;Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;I)V
    //   51: aload_0
    //   52: getfield mNfcActivityManager : Landroid/nfc/NfcActivityManager;
    //   55: aload_1
    //   56: aconst_null
    //   57: invokevirtual setOnNdefPushCompleteCallback : (Landroid/app/Activity;Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;)V
    //   60: return
    //   61: aconst_null
    //   62: athrow
    //   63: new java/lang/UnsupportedOperationException
    //   66: astore_1
    //   67: aload_1
    //   68: invokespecial <init> : ()V
    //   71: aload_1
    //   72: athrow
    //   73: astore_1
    //   74: ldc android/nfc/NfcAdapter
    //   76: monitorexit
    //   77: aload_1
    //   78: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1845	-> 0
    //   #1846	-> 3
    //   #1849	-> 9
    //   #1850	-> 15
    //   #1852	-> 19
    //   #1853	-> 22
    //   #1856	-> 26
    //   #1857	-> 31
    //   #1858	-> 41
    //   #1859	-> 51
    //   #1860	-> 60
    //   #1854	-> 61
    //   #1847	-> 63
    //   #1852	-> 73
    // Exception table:
    //   from	to	target	type
    //   3	9	73	finally
    //   9	15	73	finally
    //   15	18	73	finally
    //   19	22	73	finally
    //   63	73	73	finally
    //   74	77	73	finally
  }
  
  @SystemApi
  public boolean enableSecureNfc(boolean paramBoolean) {
    if (sHasNfcFeature)
      try {
        return sService.setNfcSecure(paramBoolean);
      } catch (RemoteException remoteException) {
        attemptDeadServiceRecovery((Exception)remoteException);
        return false;
      }  
    throw new UnsupportedOperationException();
  }
  
  public boolean isSecureNfcSupported() {
    if (sHasNfcFeature)
      try {
        return sService.deviceSupportsNfcSecure();
      } catch (RemoteException remoteException) {
        attemptDeadServiceRecovery((Exception)remoteException);
        return false;
      }  
    throw new UnsupportedOperationException();
  }
  
  public boolean isSecureNfcEnabled() {
    if (sHasNfcFeature)
      try {
        return sService.isNfcSecureEnabled();
      } catch (RemoteException remoteException) {
        attemptDeadServiceRecovery((Exception)remoteException);
        return false;
      }  
    throw new UnsupportedOperationException();
  }
  
  @SystemApi
  public boolean enableNdefPush() {
    if (sHasNfcFeature)
      try {
        return sService.enableNdefPush();
      } catch (RemoteException remoteException) {
        attemptDeadServiceRecovery((Exception)remoteException);
        return false;
      }  
    throw new UnsupportedOperationException();
  }
  
  @SystemApi
  public boolean disableNdefPush() {
    // Byte code:
    //   0: ldc android/nfc/NfcAdapter
    //   2: monitorenter
    //   3: getstatic android/nfc/NfcAdapter.sHasNfcFeature : Z
    //   6: ifeq -> 31
    //   9: ldc android/nfc/NfcAdapter
    //   11: monitorexit
    //   12: getstatic android/nfc/NfcAdapter.sService : Landroid/nfc/INfcAdapter;
    //   15: invokeinterface disableNdefPush : ()Z
    //   20: istore_1
    //   21: iload_1
    //   22: ireturn
    //   23: astore_2
    //   24: aload_0
    //   25: aload_2
    //   26: invokevirtual attemptDeadServiceRecovery : (Ljava/lang/Exception;)V
    //   29: iconst_0
    //   30: ireturn
    //   31: new java/lang/UnsupportedOperationException
    //   34: astore_2
    //   35: aload_2
    //   36: invokespecial <init> : ()V
    //   39: aload_2
    //   40: athrow
    //   41: astore_2
    //   42: ldc android/nfc/NfcAdapter
    //   44: monitorexit
    //   45: aload_2
    //   46: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1947	-> 0
    //   #1948	-> 3
    //   #1951	-> 9
    //   #1953	-> 12
    //   #1954	-> 23
    //   #1955	-> 24
    //   #1956	-> 29
    //   #1949	-> 31
    //   #1951	-> 41
    // Exception table:
    //   from	to	target	type
    //   3	9	41	finally
    //   9	12	41	finally
    //   12	21	23	android/os/RemoteException
    //   31	41	41	finally
    //   42	45	41	finally
  }
  
  @Deprecated
  public boolean isNdefPushEnabled() {
    // Byte code:
    //   0: ldc android/nfc/NfcAdapter
    //   2: monitorenter
    //   3: getstatic android/nfc/NfcAdapter.sHasNfcFeature : Z
    //   6: ifeq -> 42
    //   9: getstatic android/nfc/NfcAdapter.sHasBeamFeature : Z
    //   12: ifne -> 20
    //   15: ldc android/nfc/NfcAdapter
    //   17: monitorexit
    //   18: iconst_0
    //   19: ireturn
    //   20: ldc android/nfc/NfcAdapter
    //   22: monitorexit
    //   23: getstatic android/nfc/NfcAdapter.sService : Landroid/nfc/INfcAdapter;
    //   26: invokeinterface isNdefPushEnabled : ()Z
    //   31: istore_1
    //   32: iload_1
    //   33: ireturn
    //   34: astore_2
    //   35: aload_0
    //   36: aload_2
    //   37: invokevirtual attemptDeadServiceRecovery : (Ljava/lang/Exception;)V
    //   40: iconst_0
    //   41: ireturn
    //   42: new java/lang/UnsupportedOperationException
    //   45: astore_2
    //   46: aload_2
    //   47: invokespecial <init> : ()V
    //   50: aload_2
    //   51: athrow
    //   52: astore_2
    //   53: ldc android/nfc/NfcAdapter
    //   55: monitorexit
    //   56: aload_2
    //   57: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1989	-> 0
    //   #1990	-> 3
    //   #1993	-> 9
    //   #1994	-> 15
    //   #1996	-> 20
    //   #1998	-> 23
    //   #1999	-> 34
    //   #2000	-> 35
    //   #2001	-> 40
    //   #1991	-> 42
    //   #1996	-> 52
    // Exception table:
    //   from	to	target	type
    //   3	9	52	finally
    //   9	15	52	finally
    //   15	18	52	finally
    //   20	23	52	finally
    //   23	32	34	android/os/RemoteException
    //   42	52	52	finally
    //   53	56	52	finally
  }
  
  public boolean ignore(Tag paramTag, int paramInt, OnTagRemovedListener paramOnTagRemovedListener, Handler paramHandler) {
    Object object = null;
    if (paramOnTagRemovedListener != null)
      object = new Object(this, paramHandler, paramOnTagRemovedListener); 
    synchronized (this.mLock) {
      this.mTagRemovedListener = (ITagRemovedCallback)object;
      try {
        return sService.ignore(paramTag.getServiceHandle(), paramInt, (ITagRemovedCallback)object);
      } catch (RemoteException remoteException) {
        return false;
      } 
    } 
  }
  
  public void dispatch(Tag paramTag) {
    if (paramTag != null) {
      try {
        sService.dispatch(paramTag);
      } catch (RemoteException remoteException) {
        attemptDeadServiceRecovery((Exception)remoteException);
      } 
      return;
    } 
    throw new NullPointerException("tag cannot be null");
  }
  
  public void setP2pModes(int paramInt1, int paramInt2) {
    try {
      sService.setP2pModes(paramInt1, paramInt2);
    } catch (RemoteException remoteException) {
      attemptDeadServiceRecovery((Exception)remoteException);
    } 
  }
  
  @SystemApi
  public boolean addNfcUnlockHandler(NfcUnlockHandler paramNfcUnlockHandler, String[] paramArrayOfString) {
    // Byte code:
    //   0: ldc android/nfc/NfcAdapter
    //   2: monitorenter
    //   3: getstatic android/nfc/NfcAdapter.sHasNfcFeature : Z
    //   6: ifeq -> 138
    //   9: ldc android/nfc/NfcAdapter
    //   11: monitorexit
    //   12: aload_2
    //   13: arraylength
    //   14: ifne -> 19
    //   17: iconst_0
    //   18: ireturn
    //   19: aload_0
    //   20: getfield mLock : Ljava/lang/Object;
    //   23: astore_3
    //   24: aload_3
    //   25: monitorenter
    //   26: aload_0
    //   27: getfield mNfcUnlockHandlers : Ljava/util/HashMap;
    //   30: aload_1
    //   31: invokevirtual containsKey : (Ljava/lang/Object;)Z
    //   34: ifeq -> 65
    //   37: getstatic android/nfc/NfcAdapter.sService : Landroid/nfc/INfcAdapter;
    //   40: aload_0
    //   41: getfield mNfcUnlockHandlers : Ljava/util/HashMap;
    //   44: aload_1
    //   45: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   48: checkcast android/nfc/INfcUnlockHandler
    //   51: invokeinterface removeNfcUnlockHandler : (Landroid/nfc/INfcUnlockHandler;)V
    //   56: aload_0
    //   57: getfield mNfcUnlockHandlers : Ljava/util/HashMap;
    //   60: aload_1
    //   61: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   64: pop
    //   65: new android/nfc/NfcAdapter$3
    //   68: astore #4
    //   70: aload #4
    //   72: aload_0
    //   73: aload_1
    //   74: invokespecial <init> : (Landroid/nfc/NfcAdapter;Landroid/nfc/NfcAdapter$NfcUnlockHandler;)V
    //   77: getstatic android/nfc/NfcAdapter.sService : Landroid/nfc/INfcAdapter;
    //   80: astore #5
    //   82: aload_2
    //   83: invokestatic getTechCodesFromStrings : ([Ljava/lang/String;)[I
    //   86: astore_2
    //   87: aload #5
    //   89: aload #4
    //   91: aload_2
    //   92: invokeinterface addNfcUnlockHandler : (Landroid/nfc/INfcUnlockHandler;[I)V
    //   97: aload_0
    //   98: getfield mNfcUnlockHandlers : Ljava/util/HashMap;
    //   101: aload_1
    //   102: aload #4
    //   104: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   107: pop
    //   108: aload_3
    //   109: monitorexit
    //   110: iconst_1
    //   111: ireturn
    //   112: astore_1
    //   113: aload_3
    //   114: monitorexit
    //   115: aload_1
    //   116: athrow
    //   117: astore_1
    //   118: ldc 'NFC'
    //   120: ldc_w 'Unable to register LockscreenDispatch'
    //   123: aload_1
    //   124: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   127: pop
    //   128: iconst_0
    //   129: ireturn
    //   130: astore_1
    //   131: aload_0
    //   132: aload_1
    //   133: invokevirtual attemptDeadServiceRecovery : (Ljava/lang/Exception;)V
    //   136: iconst_0
    //   137: ireturn
    //   138: new java/lang/UnsupportedOperationException
    //   141: astore_1
    //   142: aload_1
    //   143: invokespecial <init> : ()V
    //   146: aload_1
    //   147: athrow
    //   148: astore_1
    //   149: ldc android/nfc/NfcAdapter
    //   151: monitorexit
    //   152: aload_1
    //   153: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2122	-> 0
    //   #2123	-> 3
    //   #2126	-> 9
    //   #2128	-> 12
    //   #2129	-> 17
    //   #2133	-> 19
    //   #2134	-> 26
    //   #2136	-> 37
    //   #2137	-> 56
    //   #2140	-> 65
    //   #2147	-> 77
    //   #2148	-> 82
    //   #2147	-> 87
    //   #2149	-> 97
    //   #2150	-> 108
    //   #2157	-> 110
    //   #2159	-> 110
    //   #2150	-> 112
    //   #2154	-> 117
    //   #2155	-> 118
    //   #2156	-> 128
    //   #2151	-> 130
    //   #2152	-> 131
    //   #2153	-> 136
    //   #2124	-> 138
    //   #2126	-> 148
    // Exception table:
    //   from	to	target	type
    //   3	9	148	finally
    //   9	12	148	finally
    //   19	26	130	android/os/RemoteException
    //   19	26	117	java/lang/IllegalArgumentException
    //   26	37	112	finally
    //   37	56	112	finally
    //   56	65	112	finally
    //   65	77	112	finally
    //   77	82	112	finally
    //   82	87	112	finally
    //   87	97	112	finally
    //   97	108	112	finally
    //   108	110	112	finally
    //   113	115	112	finally
    //   115	117	130	android/os/RemoteException
    //   115	117	117	java/lang/IllegalArgumentException
    //   138	148	148	finally
    //   149	152	148	finally
  }
  
  @SystemApi
  public boolean removeNfcUnlockHandler(NfcUnlockHandler paramNfcUnlockHandler) {
    // Byte code:
    //   0: ldc android/nfc/NfcAdapter
    //   2: monitorenter
    //   3: getstatic android/nfc/NfcAdapter.sHasNfcFeature : Z
    //   6: ifeq -> 66
    //   9: ldc android/nfc/NfcAdapter
    //   11: monitorexit
    //   12: aload_0
    //   13: getfield mLock : Ljava/lang/Object;
    //   16: astore_2
    //   17: aload_2
    //   18: monitorenter
    //   19: aload_0
    //   20: getfield mNfcUnlockHandlers : Ljava/util/HashMap;
    //   23: aload_1
    //   24: invokevirtual containsKey : (Ljava/lang/Object;)Z
    //   27: ifeq -> 49
    //   30: getstatic android/nfc/NfcAdapter.sService : Landroid/nfc/INfcAdapter;
    //   33: aload_0
    //   34: getfield mNfcUnlockHandlers : Ljava/util/HashMap;
    //   37: aload_1
    //   38: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   41: checkcast android/nfc/INfcUnlockHandler
    //   44: invokeinterface removeNfcUnlockHandler : (Landroid/nfc/INfcUnlockHandler;)V
    //   49: aload_2
    //   50: monitorexit
    //   51: iconst_1
    //   52: ireturn
    //   53: astore_1
    //   54: aload_2
    //   55: monitorexit
    //   56: aload_1
    //   57: athrow
    //   58: astore_1
    //   59: aload_0
    //   60: aload_1
    //   61: invokevirtual attemptDeadServiceRecovery : (Ljava/lang/Exception;)V
    //   64: iconst_0
    //   65: ireturn
    //   66: new java/lang/UnsupportedOperationException
    //   69: astore_1
    //   70: aload_1
    //   71: invokespecial <init> : ()V
    //   74: aload_1
    //   75: athrow
    //   76: astore_1
    //   77: ldc android/nfc/NfcAdapter
    //   79: monitorexit
    //   80: aload_1
    //   81: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2171	-> 0
    //   #2172	-> 3
    //   #2175	-> 9
    //   #2177	-> 12
    //   #2178	-> 19
    //   #2179	-> 30
    //   #2182	-> 49
    //   #2183	-> 53
    //   #2184	-> 58
    //   #2185	-> 59
    //   #2186	-> 64
    //   #2173	-> 66
    //   #2175	-> 76
    // Exception table:
    //   from	to	target	type
    //   3	9	76	finally
    //   9	12	76	finally
    //   12	19	58	android/os/RemoteException
    //   19	30	53	finally
    //   30	49	53	finally
    //   49	51	53	finally
    //   54	56	53	finally
    //   56	58	58	android/os/RemoteException
    //   66	76	76	finally
    //   77	80	76	finally
  }
  
  public INfcAdapterExtras getNfcAdapterExtrasInterface() {
    Context context = this.mContext;
    if (context != null)
      try {
        return sService.getNfcAdapterExtrasInterface(context.getPackageName());
      } catch (RemoteException remoteException) {
        attemptDeadServiceRecovery((Exception)remoteException);
        return null;
      }  
    throw new UnsupportedOperationException("You need a context on NfcAdapter to use the  NFC extras APIs");
  }
  
  void enforceResumed(Activity paramActivity) {
    if (paramActivity.isResumed())
      return; 
    throw new IllegalStateException("API cannot be called while activity is paused");
  }
  
  int getSdkVersion() {
    Context context = this.mContext;
    if (context == null)
      return 9; 
    return (context.getApplicationInfo()).targetSdkVersion;
  }
  
  @Deprecated
  public static interface CreateBeamUrisCallback {
    Uri[] createBeamUris(NfcEvent param1NfcEvent);
  }
  
  @Deprecated
  public static interface CreateNdefMessageCallback {
    NdefMessage createNdefMessage(NfcEvent param1NfcEvent);
  }
  
  @SystemApi
  public static interface NfcUnlockHandler {
    boolean onUnlockAttempted(Tag param1Tag);
  }
  
  @Deprecated
  public static interface OnNdefPushCompleteCallback {
    void onNdefPushComplete(NfcEvent param1NfcEvent);
  }
  
  public static interface OnTagRemovedListener {
    void onTagRemoved();
  }
  
  public static interface ReaderCallback {
    void onTagDiscovered(Tag param1Tag);
  }
}
