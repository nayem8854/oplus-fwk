package android.nfc;

import android.content.Intent;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.proto.ProtoOutputStream;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

public final class NdefRecord implements Parcelable {
  public static final Parcelable.Creator<NdefRecord> CREATOR;
  
  private static final byte[] EMPTY_BYTE_ARRAY;
  
  private static final byte FLAG_CF = 32;
  
  private static final byte FLAG_IL = 8;
  
  private static final byte FLAG_MB = -128;
  
  private static final byte FLAG_ME = 64;
  
  private static final byte FLAG_SR = 16;
  
  private static final int MAX_PAYLOAD_SIZE = 10485760;
  
  public static final byte[] RTD_ALTERNATIVE_CARRIER;
  
  public static final byte[] RTD_ANDROID_APP;
  
  public static final byte[] RTD_HANDOVER_CARRIER;
  
  public static final byte[] RTD_HANDOVER_REQUEST;
  
  public static final byte[] RTD_HANDOVER_SELECT;
  
  public static final byte[] RTD_SMART_POSTER;
  
  public static final byte[] RTD_TEXT = new byte[] { 84 };
  
  public static final byte[] RTD_URI = new byte[] { 85 };
  
  public static final short TNF_ABSOLUTE_URI = 3;
  
  public static final short TNF_EMPTY = 0;
  
  public static final short TNF_EXTERNAL_TYPE = 4;
  
  public static final short TNF_MIME_MEDIA = 2;
  
  public static final short TNF_RESERVED = 7;
  
  public static final short TNF_UNCHANGED = 6;
  
  public static final short TNF_UNKNOWN = 5;
  
  public static final short TNF_WELL_KNOWN = 1;
  
  private static final String[] URI_PREFIX_MAP;
  
  private final byte[] mId;
  
  private final byte[] mPayload;
  
  private final short mTnf;
  
  private final byte[] mType;
  
  static {
    RTD_SMART_POSTER = new byte[] { 83, 112 };
    RTD_ALTERNATIVE_CARRIER = new byte[] { 97, 99 };
    RTD_HANDOVER_CARRIER = new byte[] { 72, 99 };
    RTD_HANDOVER_REQUEST = new byte[] { 72, 114 };
    RTD_HANDOVER_SELECT = new byte[] { 72, 115 };
    RTD_ANDROID_APP = "android.com:pkg".getBytes();
    URI_PREFIX_MAP = new String[] { 
        "", "http://www.", "https://www.", "http://", "https://", "tel:", "mailto:", "ftp://anonymous:anonymous@", "ftp://ftp.", "ftps://", 
        "sftp://", "smb://", "nfs://", "ftp://", "dav://", "news:", "telnet://", "imap:", "rtsp://", "urn:", 
        "pop:", "sip:", "sips:", "tftp:", "btspp://", "btl2cap://", "btgoep://", "tcpobex://", "irdaobex://", "file://", 
        "urn:epc:id:", "urn:epc:tag:", "urn:epc:pat:", "urn:epc:raw:", "urn:epc:", "urn:nfc:" };
    EMPTY_BYTE_ARRAY = new byte[0];
    CREATOR = new Parcelable.Creator<NdefRecord>() {
        public NdefRecord createFromParcel(Parcel param1Parcel) {
          short s = (short)param1Parcel.readInt();
          int i = param1Parcel.readInt();
          byte[] arrayOfByte1 = new byte[i];
          param1Parcel.readByteArray(arrayOfByte1);
          i = param1Parcel.readInt();
          byte[] arrayOfByte2 = new byte[i];
          param1Parcel.readByteArray(arrayOfByte2);
          i = param1Parcel.readInt();
          byte[] arrayOfByte3 = new byte[i];
          param1Parcel.readByteArray(arrayOfByte3);
          return new NdefRecord(s, arrayOfByte1, arrayOfByte2, arrayOfByte3);
        }
        
        public NdefRecord[] newArray(int param1Int) {
          return new NdefRecord[param1Int];
        }
      };
  }
  
  public static NdefRecord createApplicationRecord(String paramString) {
    if (paramString != null) {
      if (paramString.length() != 0) {
        byte[] arrayOfByte = RTD_ANDROID_APP;
        Charset charset = StandardCharsets.UTF_8;
        return new NdefRecord((short)4, arrayOfByte, null, paramString.getBytes(charset));
      } 
      throw new IllegalArgumentException("packageName is empty");
    } 
    throw new NullPointerException("packageName is null");
  }
  
  public static NdefRecord createUri(Uri paramUri) {
    if (paramUri != null) {
      paramUri = paramUri.normalizeScheme();
      String str = paramUri.toString();
      if (str.length() != 0) {
        String str1;
        byte b2, b1 = 0;
        byte b = 1;
        while (true) {
          String[] arrayOfString = URI_PREFIX_MAP;
          str1 = str;
          b2 = b1;
          if (b < arrayOfString.length) {
            if (str.startsWith(arrayOfString[b])) {
              b2 = (byte)b;
              str1 = str.substring(URI_PREFIX_MAP[b].length());
              break;
            } 
            b++;
            continue;
          } 
          break;
        } 
        byte[] arrayOfByte2 = str1.getBytes(StandardCharsets.UTF_8);
        byte[] arrayOfByte1 = new byte[arrayOfByte2.length + 1];
        arrayOfByte1[0] = b2;
        System.arraycopy(arrayOfByte2, 0, arrayOfByte1, 1, arrayOfByte2.length);
        return new NdefRecord((short)1, RTD_URI, null, arrayOfByte1);
      } 
      throw new IllegalArgumentException("uri is empty");
    } 
    throw new NullPointerException("uri is null");
  }
  
  public static NdefRecord createUri(String paramString) {
    return createUri(Uri.parse(paramString));
  }
  
  public static NdefRecord createMime(String paramString, byte[] paramArrayOfbyte) {
    if (paramString != null) {
      paramString = Intent.normalizeMimeType(paramString);
      if (paramString.length() != 0) {
        int i = paramString.indexOf('/');
        if (i != 0) {
          if (i != paramString.length() - 1) {
            byte[] arrayOfByte = paramString.getBytes(StandardCharsets.US_ASCII);
            return new NdefRecord((short)2, arrayOfByte, null, paramArrayOfbyte);
          } 
          throw new IllegalArgumentException("mimeType must have minor type");
        } 
        throw new IllegalArgumentException("mimeType must have major type");
      } 
      throw new IllegalArgumentException("mimeType is empty");
    } 
    throw new NullPointerException("mimeType is null");
  }
  
  public static NdefRecord createExternal(String paramString1, String paramString2, byte[] paramArrayOfbyte) {
    if (paramString1 != null) {
      if (paramString2 != null) {
        paramString1 = paramString1.trim().toLowerCase(Locale.ROOT);
        paramString2 = paramString2.trim().toLowerCase(Locale.ROOT);
        if (paramString1.length() != 0) {
          if (paramString2.length() != 0) {
            byte[] arrayOfByte1 = paramString1.getBytes(StandardCharsets.UTF_8);
            byte[] arrayOfByte2 = paramString2.getBytes(StandardCharsets.UTF_8);
            byte[] arrayOfByte3 = new byte[arrayOfByte1.length + 1 + arrayOfByte2.length];
            System.arraycopy(arrayOfByte1, 0, arrayOfByte3, 0, arrayOfByte1.length);
            arrayOfByte3[arrayOfByte1.length] = 58;
            System.arraycopy(arrayOfByte2, 0, arrayOfByte3, arrayOfByte1.length + 1, arrayOfByte2.length);
            return new NdefRecord((short)4, arrayOfByte3, null, paramArrayOfbyte);
          } 
          throw new IllegalArgumentException("type is empty");
        } 
        throw new IllegalArgumentException("domain is empty");
      } 
      throw new NullPointerException("type is null");
    } 
    throw new NullPointerException("domain is null");
  }
  
  public static NdefRecord createTextRecord(String paramString1, String paramString2) {
    if (paramString2 != null) {
      byte[] arrayOfByte1, arrayOfByte2 = paramString2.getBytes(StandardCharsets.UTF_8);
      if (paramString1 != null && !paramString1.isEmpty()) {
        arrayOfByte1 = paramString1.getBytes(StandardCharsets.US_ASCII);
      } else {
        paramString1 = Locale.getDefault().getLanguage();
        Charset charset = StandardCharsets.US_ASCII;
        arrayOfByte1 = paramString1.getBytes(charset);
      } 
      if (arrayOfByte1.length < 64) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(arrayOfByte1.length + 1 + arrayOfByte2.length);
        byte b = (byte)(arrayOfByte1.length & 0xFF);
        byteBuffer.put(b);
        byteBuffer.put(arrayOfByte1);
        byteBuffer.put(arrayOfByte2);
        return new NdefRecord((short)1, RTD_TEXT, null, byteBuffer.array());
      } 
      throw new IllegalArgumentException("language code is too long, must be <64 bytes.");
    } 
    throw new NullPointerException("text is null");
  }
  
  public NdefRecord(short paramShort, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3) {
    byte[] arrayOfByte = paramArrayOfbyte1;
    if (paramArrayOfbyte1 == null)
      arrayOfByte = EMPTY_BYTE_ARRAY; 
    paramArrayOfbyte1 = paramArrayOfbyte2;
    if (paramArrayOfbyte2 == null)
      paramArrayOfbyte1 = EMPTY_BYTE_ARRAY; 
    paramArrayOfbyte2 = paramArrayOfbyte3;
    if (paramArrayOfbyte3 == null)
      paramArrayOfbyte2 = EMPTY_BYTE_ARRAY; 
    String str = validateTnf(paramShort, arrayOfByte, paramArrayOfbyte1, paramArrayOfbyte2);
    if (str == null) {
      this.mTnf = paramShort;
      this.mType = arrayOfByte;
      this.mId = paramArrayOfbyte1;
      this.mPayload = paramArrayOfbyte2;
      return;
    } 
    throw new IllegalArgumentException(str);
  }
  
  @Deprecated
  public NdefRecord(byte[] paramArrayOfbyte) throws FormatException {
    ByteBuffer byteBuffer = ByteBuffer.wrap(paramArrayOfbyte);
    NdefRecord[] arrayOfNdefRecord = parse(byteBuffer, true);
    if (byteBuffer.remaining() <= 0) {
      this.mTnf = (arrayOfNdefRecord[0]).mTnf;
      this.mType = (arrayOfNdefRecord[0]).mType;
      this.mId = (arrayOfNdefRecord[0]).mId;
      this.mPayload = (arrayOfNdefRecord[0]).mPayload;
      return;
    } 
    throw new FormatException("data too long");
  }
  
  public short getTnf() {
    return this.mTnf;
  }
  
  public byte[] getType() {
    return (byte[])this.mType.clone();
  }
  
  public byte[] getId() {
    return (byte[])this.mId.clone();
  }
  
  public byte[] getPayload() {
    return (byte[])this.mPayload.clone();
  }
  
  @Deprecated
  public byte[] toByteArray() {
    ByteBuffer byteBuffer = ByteBuffer.allocate(getByteLength());
    writeToByteBuffer(byteBuffer, true, true);
    return byteBuffer.array();
  }
  
  public String toMimeType() {
    short s = this.mTnf;
    if (s != 1) {
      if (s == 2) {
        String str = new String(this.mType, StandardCharsets.US_ASCII);
        return Intent.normalizeMimeType(str);
      } 
    } else if (Arrays.equals(this.mType, RTD_TEXT)) {
      return "text/plain";
    } 
    return null;
  }
  
  public Uri toUri() {
    return toUri(false);
  }
  
  private Uri toUri(boolean paramBoolean) {
    short s = this.mTnf;
    StringBuilder stringBuilder = null;
    if (s != 1) {
      if (s != 3) {
        if (s == 4)
          if (!paramBoolean) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("vnd.android.nfc://ext/");
            stringBuilder.append(new String(this.mType, StandardCharsets.US_ASCII));
            return Uri.parse(stringBuilder.toString());
          }  
      } else {
        Uri uri = Uri.parse(new String(this.mType, StandardCharsets.UTF_8));
        return uri.normalizeScheme();
      } 
    } else if (Arrays.equals(this.mType, RTD_SMART_POSTER) && !paramBoolean) {
      try {
        NdefMessage ndefMessage = new NdefMessage();
        this(this.mPayload);
        for (NdefRecord ndefRecord : ndefMessage.getRecords()) {
          Uri uri = ndefRecord.toUri(true);
          if (uri != null)
            return uri; 
        } 
      } catch (FormatException formatException) {}
    } else if (Arrays.equals(this.mType, RTD_URI)) {
      Uri uri1, uri2 = parseWktUri();
      if (uri2 != null)
        uri1 = uri2.normalizeScheme(); 
      return uri1;
    } 
    return null;
  }
  
  private Uri parseWktUri() {
    byte[] arrayOfByte = this.mPayload;
    if (arrayOfByte.length < 2)
      return null; 
    int i = arrayOfByte[0] & 0xFFFFFFFF;
    if (i >= 0) {
      String[] arrayOfString = URI_PREFIX_MAP;
      if (i < arrayOfString.length) {
        String str1 = arrayOfString[i];
        byte[] arrayOfByte1 = this.mPayload;
        String str2 = new String(Arrays.copyOfRange(arrayOfByte1, 1, arrayOfByte1.length), StandardCharsets.UTF_8);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str1);
        stringBuilder.append(str2);
        return Uri.parse(stringBuilder.toString());
      } 
    } 
    return null;
  }
  
  static NdefRecord[] parse(ByteBuffer paramByteBuffer, boolean paramBoolean) throws FormatException {
    ArrayList<NdefRecord> arrayList = new ArrayList();
    byte[] arrayOfByte1 = null;
    byte[] arrayOfByte2 = null;
    byte[] arrayOfByte3 = null;
    try {
      ArrayList<byte[]> arrayList1 = new ArrayList();
      this();
      int i = 0;
      short s = -1;
      boolean bool = false;
      while (!bool) {
        boolean bool2, bool3;
        int j;
        byte b = paramByteBuffer.get();
        boolean bool1 = true;
        if ((b & Byte.MIN_VALUE) != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        if ((b & 0x40) != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        if ((b & 0x20) != 0) {
          bool3 = true;
        } else {
          bool3 = false;
        } 
        if ((b & 0x10) != 0) {
          j = 1;
        } else {
          j = 0;
        } 
        if ((b & 0x8) == 0)
          bool1 = false; 
        short s1 = (short)(b & 0x7);
        if (bool2 || arrayList.size() != 0 || i || paramBoolean) {
          if (!bool2 || (arrayList.size() == 0 && !i) || paramBoolean) {
            if (!i || !bool1) {
              if (!bool3 || !bool) {
                if (!i || s1 == 6) {
                  if (i || s1 != 6) {
                    long l;
                    int k = paramByteBuffer.get() & 0xFF;
                    if (j) {
                      l = (paramByteBuffer.get() & 0xFF);
                    } else {
                      l = paramByteBuffer.getInt() & 0xFFFFFFFFL;
                    } 
                    if (bool1) {
                      j = paramByteBuffer.get() & 0xFF;
                    } else {
                      j = 0;
                    } 
                    if (!i || k == 0) {
                      byte[] arrayOfByte;
                      if (!i) {
                        if (k > 0) {
                          arrayOfByte2 = new byte[k];
                        } else {
                          arrayOfByte2 = EMPTY_BYTE_ARRAY;
                        } 
                        if (j > 0) {
                          arrayOfByte1 = new byte[j];
                        } else {
                          arrayOfByte1 = EMPTY_BYTE_ARRAY;
                        } 
                        paramByteBuffer.get(arrayOfByte2);
                        paramByteBuffer.get(arrayOfByte1);
                      } else {
                        arrayOfByte3 = arrayOfByte1;
                        arrayOfByte1 = arrayOfByte2;
                        arrayOfByte2 = arrayOfByte3;
                      } 
                      ensureSanePayloadSize(l);
                      if (l > 0L) {
                        arrayOfByte3 = new byte[(int)l];
                      } else {
                        arrayOfByte3 = EMPTY_BYTE_ARRAY;
                      } 
                      paramByteBuffer.get(arrayOfByte3);
                      if (bool3 && !i)
                        if (k != 0 || s1 == 5) {
                          arrayList1.clear();
                          s = s1;
                        } else {
                          FormatException formatException8 = new FormatException();
                          this("expected non-zero type length in first chunk");
                          throw formatException8;
                        }  
                      if (bool3 || i)
                        arrayList1.add(arrayOfByte3); 
                      if (!bool3 && i) {
                        l = 0L;
                        for (byte[] arrayOfByte4 : arrayList1)
                          l += arrayOfByte4.length; 
                        ensureSanePayloadSize(l);
                        arrayOfByte3 = new byte[(int)l];
                        i = 0;
                        for (byte[] arrayOfByte4 : arrayList1) {
                          System.arraycopy(arrayOfByte4, 0, arrayOfByte3, i, arrayOfByte4.length);
                          i += arrayOfByte4.length;
                        } 
                        s1 = s;
                      } 
                      if (bool3) {
                        i = 1;
                        arrayOfByte = arrayOfByte1;
                        arrayOfByte1 = arrayOfByte2;
                        arrayOfByte2 = arrayOfByte;
                        continue;
                      } 
                      i = 0;
                      String str = validateTnf(s1, arrayOfByte2, arrayOfByte1, arrayOfByte3);
                      if (str == null) {
                        NdefRecord ndefRecord = new NdefRecord();
                        this(s1, arrayOfByte2, arrayOfByte1, arrayOfByte3);
                        arrayList.add(ndefRecord);
                        if (paramBoolean)
                          break; 
                        arrayOfByte = arrayOfByte1;
                        arrayOfByte1 = arrayOfByte2;
                        arrayOfByte2 = arrayOfByte;
                        continue;
                      } 
                      FormatException formatException7 = new FormatException();
                      this((String)arrayOfByte);
                      throw formatException7;
                    } 
                    FormatException formatException6 = new FormatException();
                    this("expected zero-length type in non-leading chunk");
                    throw formatException6;
                  } 
                  FormatException formatException5 = new FormatException();
                  this("unexpected TNF_UNCHANGED in first chunk or unchunked record");
                  throw formatException5;
                } 
                FormatException formatException4 = new FormatException();
                this("expected TNF_UNCHANGED in non-leading chunk");
                throw formatException4;
              } 
              FormatException formatException3 = new FormatException();
              this("unexpected ME flag in non-trailing chunk");
              throw formatException3;
            } 
            FormatException formatException2 = new FormatException();
            this("unexpected IL flag in non-leading chunk");
            throw formatException2;
          } 
          FormatException formatException1 = new FormatException();
          this("unexpected MB flag");
          throw formatException1;
        } 
        FormatException formatException = new FormatException();
        this("expected MB flag");
        throw formatException;
      } 
      return arrayList.<NdefRecord>toArray(new NdefRecord[arrayList.size()]);
    } catch (BufferUnderflowException bufferUnderflowException) {
      throw new FormatException("expected more data", bufferUnderflowException);
    } 
  }
  
  private static void ensureSanePayloadSize(long paramLong) throws FormatException {
    if (paramLong <= 10485760L)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("payload above max limit: ");
    stringBuilder.append(paramLong);
    stringBuilder.append(" > ");
    stringBuilder.append(10485760);
    throw new FormatException(stringBuilder.toString());
  }
  
  static String validateTnf(short paramShort, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3) {
    switch (paramShort) {
      default:
        return String.format("unexpected tnf value: 0x%02x", new Object[] { Short.valueOf(paramShort) });
      case 6:
        return "unexpected TNF_UNCHANGED in first chunk or logical record";
      case 5:
      case 7:
        if (paramArrayOfbyte1.length != 0)
          return "unexpected type field in TNF_UNKNOWN or TNF_RESERVEd record"; 
        return null;
      case 1:
      case 2:
      case 3:
      case 4:
        return null;
      case 0:
        break;
    } 
    if (paramArrayOfbyte1.length != 0 || paramArrayOfbyte2.length != 0 || paramArrayOfbyte3.length != 0)
      return "unexpected data in TNF_EMPTY record"; 
    return null;
  }
  
  void writeToByteBuffer(ByteBuffer paramByteBuffer, boolean paramBoolean1, boolean paramBoolean2) {
    boolean bool2, bool3, bool4;
    int i = this.mPayload.length;
    boolean bool1 = true;
    byte b = 0;
    if (i < 256) {
      i = 1;
    } else {
      i = 0;
    } 
    if (this.mTnf != 0 && this.mId.length <= 0)
      bool1 = false; 
    if (paramBoolean1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (paramBoolean2) {
      bool3 = true;
    } else {
      bool3 = false;
    } 
    if (i != 0) {
      bool4 = true;
    } else {
      bool4 = false;
    } 
    if (bool1)
      b = 8; 
    byte b1 = (byte)(b | bool2 | bool3 | bool4 | this.mTnf);
    paramByteBuffer.put(b1);
    paramByteBuffer.put((byte)this.mType.length);
    if (i != 0) {
      paramByteBuffer.put((byte)this.mPayload.length);
    } else {
      paramByteBuffer.putInt(this.mPayload.length);
    } 
    if (bool1)
      paramByteBuffer.put((byte)this.mId.length); 
    paramByteBuffer.put(this.mType);
    paramByteBuffer.put(this.mId);
    paramByteBuffer.put(this.mPayload);
  }
  
  int getByteLength() {
    int i = this.mType.length, j = this.mId.length;
    byte[] arrayOfByte = this.mPayload;
    int k = i + 3 + j + arrayOfByte.length;
    j = arrayOfByte.length;
    i = 0;
    if (j < 256) {
      m = 1;
    } else {
      m = 0;
    } 
    if (this.mTnf == 0 || this.mId.length > 0)
      i = 1; 
    j = k;
    if (!m)
      j = k + 3; 
    int m = j;
    if (i != 0)
      m = j + 1; 
    return m;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mTnf);
    paramParcel.writeInt(this.mType.length);
    paramParcel.writeByteArray(this.mType);
    paramParcel.writeInt(this.mId.length);
    paramParcel.writeByteArray(this.mId);
    paramParcel.writeInt(this.mPayload.length);
    paramParcel.writeByteArray(this.mPayload);
  }
  
  public int hashCode() {
    int i = Arrays.hashCode(this.mId);
    int j = Arrays.hashCode(this.mPayload);
    short s = this.mTnf;
    int k = Arrays.hashCode(this.mType);
    return (((1 * 31 + i) * 31 + j) * 31 + s) * 31 + k;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (!Arrays.equals(this.mId, ((NdefRecord)paramObject).mId))
      return false; 
    if (!Arrays.equals(this.mPayload, ((NdefRecord)paramObject).mPayload))
      return false; 
    if (this.mTnf != ((NdefRecord)paramObject).mTnf)
      return false; 
    return Arrays.equals(this.mType, ((NdefRecord)paramObject).mType);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(String.format("NdefRecord tnf=%X", new Object[] { Short.valueOf(this.mTnf) }));
    if (this.mType.length > 0) {
      stringBuilder.append(" type=");
      stringBuilder.append(bytesToString(this.mType));
    } 
    if (this.mId.length > 0) {
      stringBuilder.append(" id=");
      stringBuilder.append(bytesToString(this.mId));
    } 
    if (this.mPayload.length > 0) {
      stringBuilder.append(" payload=");
      stringBuilder.append(bytesToString(this.mPayload));
    } 
    return stringBuilder.toString();
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream) {
    paramProtoOutputStream.write(1151051235329L, this.mType);
    paramProtoOutputStream.write(1151051235330L, this.mId);
    paramProtoOutputStream.write(1120986464259L, this.mPayload.length);
  }
  
  private static StringBuilder bytesToString(byte[] paramArrayOfbyte) {
    StringBuilder stringBuilder = new StringBuilder();
    int i;
    byte b;
    for (i = paramArrayOfbyte.length, b = 0; b < i; ) {
      byte b1 = paramArrayOfbyte[b];
      stringBuilder.append(String.format("%02X", new Object[] { Byte.valueOf(b1) }));
      b++;
    } 
    return stringBuilder;
  }
}
