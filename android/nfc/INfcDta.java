package android.nfc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INfcDta extends IInterface {
  void disableClient() throws RemoteException;
  
  void disableDta() throws RemoteException;
  
  void disableServer() throws RemoteException;
  
  boolean enableClient(String paramString, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void enableDta() throws RemoteException;
  
  boolean enableServer(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  boolean registerMessageService(String paramString) throws RemoteException;
  
  class Default implements INfcDta {
    public void enableDta() throws RemoteException {}
    
    public void disableDta() throws RemoteException {}
    
    public boolean enableServer(String param1String, int param1Int1, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {
      return false;
    }
    
    public void disableServer() throws RemoteException {}
    
    public boolean enableClient(String param1String, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return false;
    }
    
    public void disableClient() throws RemoteException {}
    
    public boolean registerMessageService(String param1String) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INfcDta {
    private static final String DESCRIPTOR = "android.nfc.INfcDta";
    
    static final int TRANSACTION_disableClient = 6;
    
    static final int TRANSACTION_disableDta = 2;
    
    static final int TRANSACTION_disableServer = 4;
    
    static final int TRANSACTION_enableClient = 5;
    
    static final int TRANSACTION_enableDta = 1;
    
    static final int TRANSACTION_enableServer = 3;
    
    static final int TRANSACTION_registerMessageService = 7;
    
    public Stub() {
      attachInterface(this, "android.nfc.INfcDta");
    }
    
    public static INfcDta asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.nfc.INfcDta");
      if (iInterface != null && iInterface instanceof INfcDta)
        return (INfcDta)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "registerMessageService";
        case 6:
          return "disableClient";
        case 5:
          return "enableClient";
        case 4:
          return "disableServer";
        case 3:
          return "enableServer";
        case 2:
          return "disableDta";
        case 1:
          break;
      } 
      return "enableDta";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool3;
        int j;
        boolean bool2;
        int i;
        boolean bool1;
        String str1, str2;
        int k, m;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("android.nfc.INfcDta");
            str1 = param1Parcel1.readString();
            bool3 = registerMessageService(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 6:
            str1.enforceInterface("android.nfc.INfcDta");
            disableClient();
            param1Parcel2.writeNoException();
            return true;
          case 5:
            str1.enforceInterface("android.nfc.INfcDta");
            str2 = str1.readString();
            param1Int2 = str1.readInt();
            k = str1.readInt();
            j = str1.readInt();
            bool2 = enableClient(str2, param1Int2, k, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 4:
            str1.enforceInterface("android.nfc.INfcDta");
            disableServer();
            param1Parcel2.writeNoException();
            return true;
          case 3:
            str1.enforceInterface("android.nfc.INfcDta");
            str2 = str1.readString();
            k = str1.readInt();
            i = str1.readInt();
            param1Int2 = str1.readInt();
            m = str1.readInt();
            bool1 = enableServer(str2, k, i, param1Int2, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 2:
            str1.enforceInterface("android.nfc.INfcDta");
            disableDta();
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.nfc.INfcDta");
        enableDta();
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.nfc.INfcDta");
      return true;
    }
    
    private static class Proxy implements INfcDta {
      public static INfcDta sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.nfc.INfcDta";
      }
      
      public void enableDta() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcDta");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && INfcDta.Stub.getDefaultImpl() != null) {
            INfcDta.Stub.getDefaultImpl().enableDta();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disableDta() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcDta");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && INfcDta.Stub.getDefaultImpl() != null) {
            INfcDta.Stub.getDefaultImpl().disableDta();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean enableServer(String param2String, int param2Int1, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcDta");
          try {
            parcel1.writeString(param2String);
            try {
              parcel1.writeInt(param2Int1);
              try {
                parcel1.writeInt(param2Int2);
                try {
                  parcel1.writeInt(param2Int3);
                  try {
                    parcel1.writeInt(param2Int4);
                    try {
                      IBinder iBinder = this.mRemote;
                      boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
                      if (!bool2 && INfcDta.Stub.getDefaultImpl() != null) {
                        bool1 = INfcDta.Stub.getDefaultImpl().enableServer(param2String, param2Int1, param2Int2, param2Int3, param2Int4);
                        parcel2.recycle();
                        parcel1.recycle();
                        return bool1;
                      } 
                      parcel2.readException();
                      param2Int1 = parcel2.readInt();
                      if (param2Int1 != 0)
                        bool1 = true; 
                      parcel2.recycle();
                      parcel1.recycle();
                      return bool1;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public void disableServer() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcDta");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && INfcDta.Stub.getDefaultImpl() != null) {
            INfcDta.Stub.getDefaultImpl().disableServer();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean enableClient(String param2String, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcDta");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && INfcDta.Stub.getDefaultImpl() != null) {
            bool1 = INfcDta.Stub.getDefaultImpl().enableClient(param2String, param2Int1, param2Int2, param2Int3);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disableClient() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcDta");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && INfcDta.Stub.getDefaultImpl() != null) {
            INfcDta.Stub.getDefaultImpl().disableClient();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean registerMessageService(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcDta");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && INfcDta.Stub.getDefaultImpl() != null) {
            bool1 = INfcDta.Stub.getDefaultImpl().registerMessageService(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INfcDta param1INfcDta) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INfcDta != null) {
          Proxy.sDefaultImpl = param1INfcDta;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INfcDta getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
