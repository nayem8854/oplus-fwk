package android.nfc;

import android.app.Activity;
import android.app.Application;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public final class NfcActivityManager extends IAppCallback.Stub implements Application.ActivityLifecycleCallbacks {
  static final Boolean DBG = Boolean.valueOf(false);
  
  static final String TAG = "NFC";
  
  final List<NfcActivityState> mActivities;
  
  final NfcAdapter mAdapter;
  
  final List<NfcApplicationState> mApps;
  
  class NfcApplicationState {
    final Application app;
    
    int refCount = 0;
    
    final NfcActivityManager this$0;
    
    public NfcApplicationState(Application param1Application) {
      this.app = param1Application;
    }
    
    public void register() {
      int i = this.refCount + 1;
      if (i == 1)
        this.app.registerActivityLifecycleCallbacks(NfcActivityManager.this); 
    }
    
    public void unregister() {
      int i = this.refCount - 1;
      if (i == 0) {
        this.app.unregisterActivityLifecycleCallbacks(NfcActivityManager.this);
      } else if (i < 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("-ve refcount for ");
        stringBuilder.append(this.app);
        Log.e("NFC", stringBuilder.toString());
      } 
    }
  }
  
  NfcApplicationState findAppState(Application paramApplication) {
    for (NfcApplicationState nfcApplicationState : this.mApps) {
      if (nfcApplicationState.app == paramApplication)
        return nfcApplicationState; 
    } 
    return null;
  }
  
  void registerApplication(Application paramApplication) {
    NfcApplicationState nfcApplicationState1 = findAppState(paramApplication);
    NfcApplicationState nfcApplicationState2 = nfcApplicationState1;
    if (nfcApplicationState1 == null) {
      nfcApplicationState2 = new NfcApplicationState(paramApplication);
      this.mApps.add(nfcApplicationState2);
    } 
    nfcApplicationState2.register();
  }
  
  void unregisterApplication(Application paramApplication) {
    StringBuilder stringBuilder;
    NfcApplicationState nfcApplicationState = findAppState(paramApplication);
    if (nfcApplicationState == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("app was not registered ");
      stringBuilder.append(paramApplication);
      Log.e("NFC", stringBuilder.toString());
      return;
    } 
    stringBuilder.unregister();
  }
  
  class NfcActivityState {
    boolean resumed = false;
    
    NdefMessage ndefMessage = null;
    
    NfcAdapter.CreateNdefMessageCallback ndefMessageCallback = null;
    
    NfcAdapter.OnNdefPushCompleteCallback onNdefPushCompleteCallback = null;
    
    NfcAdapter.CreateBeamUrisCallback uriCallback = null;
    
    Uri[] uris = null;
    
    int flags = 0;
    
    int readerModeFlags = 0;
    
    NfcAdapter.ReaderCallback readerCallback = null;
    
    Bundle readerModeExtras = null;
    
    Activity activity;
    
    final NfcActivityManager this$0;
    
    Binder token;
    
    public NfcActivityState(Activity param1Activity) {
      if (!param1Activity.getWindow().isDestroyed()) {
        this.resumed = param1Activity.isResumed();
        this.activity = param1Activity;
        this.token = new Binder();
        NfcActivityManager.this.registerApplication(param1Activity.getApplication());
        return;
      } 
      throw new IllegalStateException("activity is already destroyed");
    }
    
    public void destroy() {
      NfcActivityManager.this.unregisterApplication(this.activity.getApplication());
      this.resumed = false;
      this.activity = null;
      this.ndefMessage = null;
      this.ndefMessageCallback = null;
      this.onNdefPushCompleteCallback = null;
      this.uriCallback = null;
      this.uris = null;
      this.readerModeFlags = 0;
      this.token = null;
    }
    
    public String toString() {
      StringBuilder stringBuilder = (new StringBuilder("[")).append(" ");
      stringBuilder.append(this.ndefMessage);
      stringBuilder.append(" ");
      stringBuilder.append(this.ndefMessageCallback);
      stringBuilder.append(" ");
      stringBuilder.append(this.uriCallback);
      stringBuilder.append(" ");
      Uri[] arrayOfUri = this.uris;
      if (arrayOfUri != null) {
        int i;
        byte b;
        for (i = arrayOfUri.length, b = 0; b < i; ) {
          Uri uri = arrayOfUri[b];
          stringBuilder.append(this.onNdefPushCompleteCallback);
          stringBuilder.append(" ");
          stringBuilder.append(uri);
          stringBuilder.append("]");
          b++;
        } 
      } 
      return stringBuilder.toString();
    }
  }
  
  NfcActivityState findActivityState(Activity paramActivity) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mActivities : Ljava/util/List;
    //   6: invokeinterface iterator : ()Ljava/util/Iterator;
    //   11: astore_2
    //   12: aload_2
    //   13: invokeinterface hasNext : ()Z
    //   18: ifeq -> 50
    //   21: aload_2
    //   22: invokeinterface next : ()Ljava/lang/Object;
    //   27: checkcast android/nfc/NfcActivityManager$NfcActivityState
    //   30: astore_3
    //   31: aload_3
    //   32: getfield activity : Landroid/app/Activity;
    //   35: astore #4
    //   37: aload #4
    //   39: aload_1
    //   40: if_acmpne -> 47
    //   43: aload_0
    //   44: monitorexit
    //   45: aload_3
    //   46: areturn
    //   47: goto -> 12
    //   50: aload_0
    //   51: monitorexit
    //   52: aconst_null
    //   53: areturn
    //   54: astore_1
    //   55: aload_0
    //   56: monitorexit
    //   57: aload_1
    //   58: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #164	-> 2
    //   #165	-> 31
    //   #166	-> 43
    //   #168	-> 47
    //   #169	-> 50
    //   #163	-> 54
    // Exception table:
    //   from	to	target	type
    //   2	12	54	finally
    //   12	31	54	finally
    //   31	37	54	finally
  }
  
  NfcActivityState getActivityState(Activity paramActivity) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokevirtual findActivityState : (Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;
    //   7: astore_2
    //   8: aload_2
    //   9: astore_3
    //   10: aload_2
    //   11: ifnonnull -> 35
    //   14: new android/nfc/NfcActivityManager$NfcActivityState
    //   17: astore_3
    //   18: aload_3
    //   19: aload_0
    //   20: aload_1
    //   21: invokespecial <init> : (Landroid/nfc/NfcActivityManager;Landroid/app/Activity;)V
    //   24: aload_0
    //   25: getfield mActivities : Ljava/util/List;
    //   28: aload_3
    //   29: invokeinterface add : (Ljava/lang/Object;)Z
    //   34: pop
    //   35: aload_0
    //   36: monitorexit
    //   37: aload_3
    //   38: areturn
    //   39: astore_1
    //   40: aload_0
    //   41: monitorexit
    //   42: aload_1
    //   43: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #174	-> 2
    //   #175	-> 8
    //   #176	-> 14
    //   #177	-> 24
    //   #179	-> 35
    //   #173	-> 39
    // Exception table:
    //   from	to	target	type
    //   2	8	39	finally
    //   14	24	39	finally
    //   24	35	39	finally
  }
  
  NfcActivityState findResumedActivityState() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mActivities : Ljava/util/List;
    //   6: invokeinterface iterator : ()Ljava/util/Iterator;
    //   11: astore_1
    //   12: aload_1
    //   13: invokeinterface hasNext : ()Z
    //   18: ifeq -> 47
    //   21: aload_1
    //   22: invokeinterface next : ()Ljava/lang/Object;
    //   27: checkcast android/nfc/NfcActivityManager$NfcActivityState
    //   30: astore_2
    //   31: aload_2
    //   32: getfield resumed : Z
    //   35: istore_3
    //   36: iload_3
    //   37: ifeq -> 44
    //   40: aload_0
    //   41: monitorexit
    //   42: aload_2
    //   43: areturn
    //   44: goto -> 12
    //   47: aload_0
    //   48: monitorexit
    //   49: aconst_null
    //   50: areturn
    //   51: astore_1
    //   52: aload_0
    //   53: monitorexit
    //   54: aload_1
    //   55: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #183	-> 2
    //   #184	-> 31
    //   #185	-> 40
    //   #187	-> 44
    //   #188	-> 47
    //   #182	-> 51
    // Exception table:
    //   from	to	target	type
    //   2	12	51	finally
    //   12	31	51	finally
    //   31	36	51	finally
  }
  
  void destroyActivityState(Activity paramActivity) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokevirtual findActivityState : (Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;
    //   7: astore_1
    //   8: aload_1
    //   9: ifnull -> 27
    //   12: aload_1
    //   13: invokevirtual destroy : ()V
    //   16: aload_0
    //   17: getfield mActivities : Ljava/util/List;
    //   20: aload_1
    //   21: invokeinterface remove : (Ljava/lang/Object;)Z
    //   26: pop
    //   27: aload_0
    //   28: monitorexit
    //   29: return
    //   30: astore_1
    //   31: aload_0
    //   32: monitorexit
    //   33: aload_1
    //   34: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #192	-> 2
    //   #193	-> 8
    //   #194	-> 12
    //   #195	-> 16
    //   #197	-> 27
    //   #191	-> 30
    // Exception table:
    //   from	to	target	type
    //   2	8	30	finally
    //   12	16	30	finally
    //   16	27	30	finally
  }
  
  public NfcActivityManager(NfcAdapter paramNfcAdapter) {
    this.mAdapter = paramNfcAdapter;
    this.mActivities = new LinkedList<>();
    this.mApps = new ArrayList<>(1);
  }
  
  public void enableReaderMode(Activity paramActivity, NfcAdapter.ReaderCallback paramReaderCallback, int paramInt, Bundle paramBundle) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokevirtual getActivityState : (Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;
    //   7: astore_1
    //   8: aload_1
    //   9: aload_2
    //   10: putfield readerCallback : Landroid/nfc/NfcAdapter$ReaderCallback;
    //   13: aload_1
    //   14: iload_3
    //   15: putfield readerModeFlags : I
    //   18: aload_1
    //   19: aload #4
    //   21: putfield readerModeExtras : Landroid/os/Bundle;
    //   24: aload_1
    //   25: getfield token : Landroid/os/Binder;
    //   28: astore_2
    //   29: aload_1
    //   30: getfield resumed : Z
    //   33: istore #5
    //   35: aload_0
    //   36: monitorexit
    //   37: iload #5
    //   39: ifeq -> 50
    //   42: aload_0
    //   43: aload_2
    //   44: iload_3
    //   45: aload #4
    //   47: invokevirtual setReaderMode : (Landroid/os/Binder;ILandroid/os/Bundle;)V
    //   50: return
    //   51: astore_1
    //   52: aload_0
    //   53: monitorexit
    //   54: aload_1
    //   55: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #209	-> 0
    //   #210	-> 2
    //   #211	-> 8
    //   #212	-> 13
    //   #213	-> 18
    //   #214	-> 24
    //   #215	-> 29
    //   #216	-> 35
    //   #217	-> 37
    //   #218	-> 42
    //   #220	-> 50
    //   #216	-> 51
    // Exception table:
    //   from	to	target	type
    //   2	8	51	finally
    //   8	13	51	finally
    //   13	18	51	finally
    //   18	24	51	finally
    //   24	29	51	finally
    //   29	35	51	finally
    //   35	37	51	finally
    //   52	54	51	finally
  }
  
  public void disableReaderMode(Activity paramActivity) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokevirtual getActivityState : (Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;
    //   7: astore_2
    //   8: aload_2
    //   9: aconst_null
    //   10: putfield readerCallback : Landroid/nfc/NfcAdapter$ReaderCallback;
    //   13: aload_2
    //   14: iconst_0
    //   15: putfield readerModeFlags : I
    //   18: aload_2
    //   19: aconst_null
    //   20: putfield readerModeExtras : Landroid/os/Bundle;
    //   23: aload_2
    //   24: getfield token : Landroid/os/Binder;
    //   27: astore_1
    //   28: aload_2
    //   29: getfield resumed : Z
    //   32: istore_3
    //   33: aload_0
    //   34: monitorexit
    //   35: iload_3
    //   36: ifeq -> 46
    //   39: aload_0
    //   40: aload_1
    //   41: iconst_0
    //   42: aconst_null
    //   43: invokevirtual setReaderMode : (Landroid/os/Binder;ILandroid/os/Bundle;)V
    //   46: return
    //   47: astore_1
    //   48: aload_0
    //   49: monitorexit
    //   50: aload_1
    //   51: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #225	-> 0
    //   #226	-> 2
    //   #227	-> 8
    //   #228	-> 13
    //   #229	-> 18
    //   #230	-> 23
    //   #231	-> 28
    //   #232	-> 33
    //   #233	-> 35
    //   #234	-> 39
    //   #237	-> 46
    //   #232	-> 47
    // Exception table:
    //   from	to	target	type
    //   2	8	47	finally
    //   8	13	47	finally
    //   13	18	47	finally
    //   18	23	47	finally
    //   23	28	47	finally
    //   28	33	47	finally
    //   33	35	47	finally
    //   48	50	47	finally
  }
  
  public void setReaderMode(Binder paramBinder, int paramInt, Bundle paramBundle) {
    if (DBG.booleanValue())
      Log.d("NFC", "Setting reader mode"); 
    try {
      NfcAdapter.sService.setReaderMode(paramBinder, this, paramInt, paramBundle);
    } catch (RemoteException remoteException) {
      this.mAdapter.attemptDeadServiceRecovery((Exception)remoteException);
    } 
  }
  
  public void setNdefPushContentUri(Activity paramActivity, Uri[] paramArrayOfUri) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokevirtual getActivityState : (Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;
    //   7: astore_1
    //   8: aload_1
    //   9: aload_2
    //   10: putfield uris : [Landroid/net/Uri;
    //   13: aload_1
    //   14: getfield resumed : Z
    //   17: istore_3
    //   18: aload_0
    //   19: monitorexit
    //   20: iload_3
    //   21: ifeq -> 31
    //   24: aload_0
    //   25: invokevirtual requestNfcServiceCallback : ()V
    //   28: goto -> 35
    //   31: aload_0
    //   32: invokevirtual verifyNfcPermission : ()V
    //   35: return
    //   36: astore_1
    //   37: aload_0
    //   38: monitorexit
    //   39: aload_1
    //   40: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #250	-> 0
    //   #251	-> 2
    //   #252	-> 8
    //   #253	-> 13
    //   #254	-> 18
    //   #255	-> 20
    //   #257	-> 24
    //   #260	-> 31
    //   #262	-> 35
    //   #254	-> 36
    // Exception table:
    //   from	to	target	type
    //   2	8	36	finally
    //   8	13	36	finally
    //   13	18	36	finally
    //   18	20	36	finally
    //   37	39	36	finally
  }
  
  public void setNdefPushContentUriCallback(Activity paramActivity, NfcAdapter.CreateBeamUrisCallback paramCreateBeamUrisCallback) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokevirtual getActivityState : (Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;
    //   7: astore_1
    //   8: aload_1
    //   9: aload_2
    //   10: putfield uriCallback : Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;
    //   13: aload_1
    //   14: getfield resumed : Z
    //   17: istore_3
    //   18: aload_0
    //   19: monitorexit
    //   20: iload_3
    //   21: ifeq -> 31
    //   24: aload_0
    //   25: invokevirtual requestNfcServiceCallback : ()V
    //   28: goto -> 35
    //   31: aload_0
    //   32: invokevirtual verifyNfcPermission : ()V
    //   35: return
    //   36: astore_1
    //   37: aload_0
    //   38: monitorexit
    //   39: aload_1
    //   40: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #268	-> 0
    //   #269	-> 2
    //   #270	-> 8
    //   #271	-> 13
    //   #272	-> 18
    //   #273	-> 20
    //   #275	-> 24
    //   #278	-> 31
    //   #280	-> 35
    //   #272	-> 36
    // Exception table:
    //   from	to	target	type
    //   2	8	36	finally
    //   8	13	36	finally
    //   13	18	36	finally
    //   18	20	36	finally
    //   37	39	36	finally
  }
  
  public void setNdefPushMessage(Activity paramActivity, NdefMessage paramNdefMessage, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokevirtual getActivityState : (Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;
    //   7: astore_1
    //   8: aload_1
    //   9: aload_2
    //   10: putfield ndefMessage : Landroid/nfc/NdefMessage;
    //   13: aload_1
    //   14: iload_3
    //   15: putfield flags : I
    //   18: aload_1
    //   19: getfield resumed : Z
    //   22: istore #4
    //   24: aload_0
    //   25: monitorexit
    //   26: iload #4
    //   28: ifeq -> 38
    //   31: aload_0
    //   32: invokevirtual requestNfcServiceCallback : ()V
    //   35: goto -> 42
    //   38: aload_0
    //   39: invokevirtual verifyNfcPermission : ()V
    //   42: return
    //   43: astore_1
    //   44: aload_0
    //   45: monitorexit
    //   46: aload_1
    //   47: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #284	-> 0
    //   #285	-> 2
    //   #286	-> 8
    //   #287	-> 13
    //   #288	-> 18
    //   #289	-> 24
    //   #290	-> 26
    //   #292	-> 31
    //   #295	-> 38
    //   #297	-> 42
    //   #289	-> 43
    // Exception table:
    //   from	to	target	type
    //   2	8	43	finally
    //   8	13	43	finally
    //   13	18	43	finally
    //   18	24	43	finally
    //   24	26	43	finally
    //   44	46	43	finally
  }
  
  public void setNdefPushMessageCallback(Activity paramActivity, NfcAdapter.CreateNdefMessageCallback paramCreateNdefMessageCallback, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokevirtual getActivityState : (Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;
    //   7: astore_1
    //   8: aload_1
    //   9: aload_2
    //   10: putfield ndefMessageCallback : Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;
    //   13: aload_1
    //   14: iload_3
    //   15: putfield flags : I
    //   18: aload_1
    //   19: getfield resumed : Z
    //   22: istore #4
    //   24: aload_0
    //   25: monitorexit
    //   26: iload #4
    //   28: ifeq -> 38
    //   31: aload_0
    //   32: invokevirtual requestNfcServiceCallback : ()V
    //   35: goto -> 42
    //   38: aload_0
    //   39: invokevirtual verifyNfcPermission : ()V
    //   42: return
    //   43: astore_1
    //   44: aload_0
    //   45: monitorexit
    //   46: aload_1
    //   47: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #302	-> 0
    //   #303	-> 2
    //   #304	-> 8
    //   #305	-> 13
    //   #306	-> 18
    //   #307	-> 24
    //   #308	-> 26
    //   #310	-> 31
    //   #313	-> 38
    //   #315	-> 42
    //   #307	-> 43
    // Exception table:
    //   from	to	target	type
    //   2	8	43	finally
    //   8	13	43	finally
    //   13	18	43	finally
    //   18	24	43	finally
    //   24	26	43	finally
    //   44	46	43	finally
  }
  
  public void setOnNdefPushCompleteCallback(Activity paramActivity, NfcAdapter.OnNdefPushCompleteCallback paramOnNdefPushCompleteCallback) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokevirtual getActivityState : (Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;
    //   7: astore_1
    //   8: aload_1
    //   9: aload_2
    //   10: putfield onNdefPushCompleteCallback : Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;
    //   13: aload_1
    //   14: getfield resumed : Z
    //   17: istore_3
    //   18: aload_0
    //   19: monitorexit
    //   20: iload_3
    //   21: ifeq -> 31
    //   24: aload_0
    //   25: invokevirtual requestNfcServiceCallback : ()V
    //   28: goto -> 35
    //   31: aload_0
    //   32: invokevirtual verifyNfcPermission : ()V
    //   35: return
    //   36: astore_1
    //   37: aload_0
    //   38: monitorexit
    //   39: aload_1
    //   40: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #320	-> 0
    //   #321	-> 2
    //   #322	-> 8
    //   #323	-> 13
    //   #324	-> 18
    //   #325	-> 20
    //   #327	-> 24
    //   #330	-> 31
    //   #332	-> 35
    //   #324	-> 36
    // Exception table:
    //   from	to	target	type
    //   2	8	36	finally
    //   8	13	36	finally
    //   13	18	36	finally
    //   18	20	36	finally
    //   37	39	36	finally
  }
  
  void requestNfcServiceCallback() {
    try {
      NfcAdapter.sService.setAppCallback(this);
    } catch (RemoteException remoteException) {
      this.mAdapter.attemptDeadServiceRecovery((Exception)remoteException);
    } 
  }
  
  void verifyNfcPermission() {
    try {
      NfcAdapter.sService.verifyNfcPermission();
    } catch (RemoteException remoteException) {
      this.mAdapter.attemptDeadServiceRecovery((Exception)remoteException);
    } 
  }
  
  public BeamShareData createBeamShareData(byte paramByte) {
    // Byte code:
    //   0: new android/nfc/NfcEvent
    //   3: dup
    //   4: aload_0
    //   5: getfield mAdapter : Landroid/nfc/NfcAdapter;
    //   8: iload_1
    //   9: invokespecial <init> : (Landroid/nfc/NfcAdapter;B)V
    //   12: astore_2
    //   13: aload_0
    //   14: monitorenter
    //   15: aload_0
    //   16: invokevirtual findResumedActivityState : ()Landroid/nfc/NfcActivityManager$NfcActivityState;
    //   19: astore_3
    //   20: aload_3
    //   21: ifnonnull -> 32
    //   24: aload_0
    //   25: monitorexit
    //   26: aconst_null
    //   27: areturn
    //   28: astore_2
    //   29: goto -> 355
    //   32: aload_3
    //   33: getfield ndefMessageCallback : Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;
    //   36: astore #4
    //   38: aload_3
    //   39: getfield uriCallback : Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;
    //   42: astore #5
    //   44: aload_3
    //   45: getfield ndefMessage : Landroid/nfc/NdefMessage;
    //   48: astore #6
    //   50: aload_3
    //   51: getfield uris : [Landroid/net/Uri;
    //   54: astore #7
    //   56: aload_3
    //   57: getfield flags : I
    //   60: istore #8
    //   62: aload_3
    //   63: getfield activity : Landroid/app/Activity;
    //   66: astore_3
    //   67: aload_0
    //   68: monitorexit
    //   69: invokestatic clearCallingIdentity : ()J
    //   72: lstore #9
    //   74: aload #4
    //   76: ifnull -> 96
    //   79: aload #4
    //   81: aload_2
    //   82: invokeinterface createNdefMessage : (Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    //   87: astore #6
    //   89: goto -> 96
    //   92: astore_2
    //   93: goto -> 325
    //   96: aload #5
    //   98: ifnull -> 277
    //   101: aload_2
    //   102: astore #4
    //   104: aload #5
    //   106: aload_2
    //   107: invokeinterface createBeamUris : (Landroid/nfc/NfcEvent;)[Landroid/net/Uri;
    //   112: astore #7
    //   114: aload #7
    //   116: ifnull -> 267
    //   119: aload_2
    //   120: astore #4
    //   122: new java/util/ArrayList
    //   125: astore #5
    //   127: aload_2
    //   128: astore #4
    //   130: aload #5
    //   132: invokespecial <init> : ()V
    //   135: aload_2
    //   136: astore #4
    //   138: aload #7
    //   140: arraylength
    //   141: istore #11
    //   143: iconst_0
    //   144: istore #12
    //   146: iload #12
    //   148: iload #11
    //   150: if_icmpge -> 247
    //   153: aload #7
    //   155: iload #12
    //   157: aaload
    //   158: astore #13
    //   160: aload #13
    //   162: ifnonnull -> 176
    //   165: ldc 'NFC'
    //   167: ldc 'Uri not allowed to be null.'
    //   169: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   172: pop
    //   173: goto -> 241
    //   176: aload #13
    //   178: invokevirtual getScheme : ()Ljava/lang/String;
    //   181: astore #4
    //   183: aload #4
    //   185: ifnull -> 233
    //   188: aload #4
    //   190: ldc 'file'
    //   192: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   195: ifne -> 211
    //   198: aload #4
    //   200: ldc 'content'
    //   202: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   205: ifne -> 211
    //   208: goto -> 233
    //   211: aload #13
    //   213: aload_3
    //   214: invokevirtual getUserId : ()I
    //   217: invokestatic maybeAddUserId : (Landroid/net/Uri;I)Landroid/net/Uri;
    //   220: astore #4
    //   222: aload #5
    //   224: aload #4
    //   226: invokevirtual add : (Ljava/lang/Object;)Z
    //   229: pop
    //   230: goto -> 241
    //   233: ldc 'NFC'
    //   235: ldc 'Uri needs to have either scheme file or scheme content'
    //   237: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   240: pop
    //   241: iinc #12, 1
    //   244: goto -> 146
    //   247: aload #5
    //   249: aload #5
    //   251: invokevirtual size : ()I
    //   254: anewarray android/net/Uri
    //   257: invokevirtual toArray : ([Ljava/lang/Object;)[Ljava/lang/Object;
    //   260: checkcast [Landroid/net/Uri;
    //   263: astore_2
    //   264: goto -> 280
    //   267: aload #7
    //   269: astore_2
    //   270: goto -> 280
    //   273: astore_2
    //   274: goto -> 325
    //   277: aload #7
    //   279: astore_2
    //   280: aload_2
    //   281: ifnull -> 332
    //   284: aload_2
    //   285: arraylength
    //   286: ifle -> 332
    //   289: aload_2
    //   290: arraylength
    //   291: istore #11
    //   293: iconst_0
    //   294: istore #12
    //   296: iload #12
    //   298: iload #11
    //   300: if_icmpge -> 332
    //   303: aload_2
    //   304: iload #12
    //   306: aaload
    //   307: astore #7
    //   309: aload_3
    //   310: ldc 'com.android.nfc'
    //   312: aload #7
    //   314: iconst_1
    //   315: invokevirtual grantUriPermission : (Ljava/lang/String;Landroid/net/Uri;I)V
    //   318: iinc #12, 1
    //   321: goto -> 296
    //   324: astore_2
    //   325: lload #9
    //   327: invokestatic restoreCallingIdentity : (J)V
    //   330: aload_2
    //   331: athrow
    //   332: lload #9
    //   334: invokestatic restoreCallingIdentity : (J)V
    //   337: new android/nfc/BeamShareData
    //   340: dup
    //   341: aload #6
    //   343: aload_2
    //   344: aload_3
    //   345: invokevirtual getUser : ()Landroid/os/UserHandle;
    //   348: iload #8
    //   350: invokespecial <init> : (Landroid/nfc/NdefMessage;[Landroid/net/Uri;Landroid/os/UserHandle;I)V
    //   353: areturn
    //   354: astore_2
    //   355: aload_0
    //   356: monitorexit
    //   357: aload_2
    //   358: athrow
    //   359: astore_2
    //   360: goto -> 355
    // Line number table:
    //   Java source line number -> byte code offset
    //   #363	-> 0
    //   #364	-> 13
    //   #365	-> 15
    //   #366	-> 20
    //   #374	-> 28
    //   #368	-> 32
    //   #369	-> 38
    //   #370	-> 44
    //   #371	-> 50
    //   #372	-> 56
    //   #373	-> 62
    //   #374	-> 67
    //   #375	-> 69
    //   #378	-> 74
    //   #379	-> 79
    //   #412	-> 92
    //   #381	-> 96
    //   #382	-> 101
    //   #383	-> 114
    //   #384	-> 119
    //   #385	-> 135
    //   #386	-> 160
    //   #387	-> 165
    //   #388	-> 173
    //   #390	-> 176
    //   #391	-> 183
    //   #392	-> 198
    //   #397	-> 211
    //   #398	-> 222
    //   #393	-> 233
    //   #395	-> 241
    //   #385	-> 241
    //   #401	-> 247
    //   #383	-> 267
    //   #412	-> 273
    //   #381	-> 277
    //   #404	-> 280
    //   #405	-> 289
    //   #407	-> 309
    //   #405	-> 318
    //   #412	-> 324
    //   #413	-> 330
    //   #412	-> 332
    //   #413	-> 337
    //   #414	-> 337
    //   #374	-> 354
    // Exception table:
    //   from	to	target	type
    //   15	20	354	finally
    //   24	26	28	finally
    //   32	38	354	finally
    //   38	44	354	finally
    //   44	50	354	finally
    //   50	56	354	finally
    //   56	62	354	finally
    //   62	67	354	finally
    //   67	69	354	finally
    //   79	89	92	finally
    //   104	114	273	finally
    //   122	127	273	finally
    //   130	135	273	finally
    //   138	143	273	finally
    //   165	173	324	finally
    //   176	183	324	finally
    //   188	198	324	finally
    //   198	208	324	finally
    //   211	222	324	finally
    //   222	230	324	finally
    //   233	241	324	finally
    //   247	264	324	finally
    //   284	289	324	finally
    //   289	293	324	finally
    //   309	318	324	finally
    //   355	357	359	finally
  }
  
  public void onNdefPushComplete(byte paramByte) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual findResumedActivityState : ()Landroid/nfc/NfcActivityManager$NfcActivityState;
    //   6: astore_2
    //   7: aload_2
    //   8: ifnonnull -> 14
    //   11: aload_0
    //   12: monitorexit
    //   13: return
    //   14: aload_2
    //   15: getfield onNdefPushCompleteCallback : Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;
    //   18: astore_2
    //   19: aload_0
    //   20: monitorexit
    //   21: new android/nfc/NfcEvent
    //   24: dup
    //   25: aload_0
    //   26: getfield mAdapter : Landroid/nfc/NfcAdapter;
    //   29: iload_1
    //   30: invokespecial <init> : (Landroid/nfc/NfcAdapter;B)V
    //   33: astore_3
    //   34: aload_2
    //   35: ifnull -> 45
    //   38: aload_2
    //   39: aload_3
    //   40: invokeinterface onNdefPushComplete : (Landroid/nfc/NfcEvent;)V
    //   45: return
    //   46: astore_2
    //   47: aload_0
    //   48: monitorexit
    //   49: aload_2
    //   50: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #421	-> 0
    //   #422	-> 2
    //   #423	-> 7
    //   #425	-> 14
    //   #426	-> 19
    //   #427	-> 21
    //   #429	-> 34
    //   #430	-> 38
    //   #432	-> 45
    //   #426	-> 46
    // Exception table:
    //   from	to	target	type
    //   2	7	46	finally
    //   11	13	46	finally
    //   14	19	46	finally
    //   19	21	46	finally
    //   47	49	46	finally
  }
  
  public void onTagDiscovered(Tag paramTag) throws RemoteException {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual findResumedActivityState : ()Landroid/nfc/NfcActivityManager$NfcActivityState;
    //   6: astore_2
    //   7: aload_2
    //   8: ifnonnull -> 14
    //   11: aload_0
    //   12: monitorexit
    //   13: return
    //   14: aload_2
    //   15: getfield readerCallback : Landroid/nfc/NfcAdapter$ReaderCallback;
    //   18: astore_2
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_2
    //   22: ifnull -> 32
    //   25: aload_2
    //   26: aload_1
    //   27: invokeinterface onTagDiscovered : (Landroid/nfc/Tag;)V
    //   32: return
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #437	-> 0
    //   #438	-> 2
    //   #439	-> 7
    //   #441	-> 14
    //   #442	-> 19
    //   #445	-> 21
    //   #446	-> 25
    //   #449	-> 32
    //   #442	-> 33
    // Exception table:
    //   from	to	target	type
    //   2	7	33	finally
    //   11	13	33	finally
    //   14	19	33	finally
    //   19	21	33	finally
    //   34	36	33	finally
  }
  
  public void onActivityCreated(Activity paramActivity, Bundle paramBundle) {}
  
  public void onActivityStarted(Activity paramActivity) {}
  
  public void onActivityResumed(Activity paramActivity) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokevirtual findActivityState : (Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;
    //   7: astore_2
    //   8: getstatic android/nfc/NfcActivityManager.DBG : Ljava/lang/Boolean;
    //   11: invokevirtual booleanValue : ()Z
    //   14: ifeq -> 63
    //   17: new java/lang/StringBuilder
    //   20: astore_3
    //   21: aload_3
    //   22: invokespecial <init> : ()V
    //   25: aload_3
    //   26: ldc_w 'onResume() for '
    //   29: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   32: pop
    //   33: aload_3
    //   34: aload_1
    //   35: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   38: pop
    //   39: aload_3
    //   40: ldc_w ' '
    //   43: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   46: pop
    //   47: aload_3
    //   48: aload_2
    //   49: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   52: pop
    //   53: ldc 'NFC'
    //   55: aload_3
    //   56: invokevirtual toString : ()Ljava/lang/String;
    //   59: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   62: pop
    //   63: aload_2
    //   64: ifnonnull -> 70
    //   67: aload_0
    //   68: monitorexit
    //   69: return
    //   70: aload_2
    //   71: iconst_1
    //   72: putfield resumed : Z
    //   75: aload_2
    //   76: getfield token : Landroid/os/Binder;
    //   79: astore_1
    //   80: aload_2
    //   81: getfield readerModeFlags : I
    //   84: istore #4
    //   86: aload_2
    //   87: getfield readerModeExtras : Landroid/os/Bundle;
    //   90: astore_2
    //   91: aload_0
    //   92: monitorexit
    //   93: iload #4
    //   95: ifeq -> 106
    //   98: aload_0
    //   99: aload_1
    //   100: iload #4
    //   102: aload_2
    //   103: invokevirtual setReaderMode : (Landroid/os/Binder;ILandroid/os/Bundle;)V
    //   106: aload_0
    //   107: invokevirtual requestNfcServiceCallback : ()V
    //   110: return
    //   111: astore_1
    //   112: aload_0
    //   113: monitorexit
    //   114: aload_1
    //   115: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #461	-> 0
    //   #462	-> 0
    //   #464	-> 0
    //   #465	-> 2
    //   #466	-> 8
    //   #467	-> 63
    //   #468	-> 70
    //   #469	-> 75
    //   #470	-> 80
    //   #471	-> 86
    //   #472	-> 91
    //   #473	-> 93
    //   #474	-> 98
    //   #476	-> 106
    //   #477	-> 110
    //   #472	-> 111
    // Exception table:
    //   from	to	target	type
    //   2	8	111	finally
    //   8	63	111	finally
    //   67	69	111	finally
    //   70	75	111	finally
    //   75	80	111	finally
    //   80	86	111	finally
    //   86	91	111	finally
    //   91	93	111	finally
    //   112	114	111	finally
  }
  
  public void onActivityPaused(Activity paramActivity) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokevirtual findActivityState : (Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;
    //   7: astore_2
    //   8: getstatic android/nfc/NfcActivityManager.DBG : Ljava/lang/Boolean;
    //   11: invokevirtual booleanValue : ()Z
    //   14: ifeq -> 63
    //   17: new java/lang/StringBuilder
    //   20: astore_3
    //   21: aload_3
    //   22: invokespecial <init> : ()V
    //   25: aload_3
    //   26: ldc_w 'onPause() for '
    //   29: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   32: pop
    //   33: aload_3
    //   34: aload_1
    //   35: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   38: pop
    //   39: aload_3
    //   40: ldc_w ' '
    //   43: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   46: pop
    //   47: aload_3
    //   48: aload_2
    //   49: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   52: pop
    //   53: ldc 'NFC'
    //   55: aload_3
    //   56: invokevirtual toString : ()Ljava/lang/String;
    //   59: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   62: pop
    //   63: aload_2
    //   64: ifnonnull -> 70
    //   67: aload_0
    //   68: monitorexit
    //   69: return
    //   70: aload_2
    //   71: iconst_0
    //   72: putfield resumed : Z
    //   75: aload_2
    //   76: getfield token : Landroid/os/Binder;
    //   79: astore_1
    //   80: aload_2
    //   81: getfield readerModeFlags : I
    //   84: ifeq -> 93
    //   87: iconst_1
    //   88: istore #4
    //   90: goto -> 96
    //   93: iconst_0
    //   94: istore #4
    //   96: aload_0
    //   97: monitorexit
    //   98: iload #4
    //   100: ifeq -> 110
    //   103: aload_0
    //   104: aload_1
    //   105: iconst_0
    //   106: aconst_null
    //   107: invokevirtual setReaderMode : (Landroid/os/Binder;ILandroid/os/Bundle;)V
    //   110: return
    //   111: astore_1
    //   112: aload_0
    //   113: monitorexit
    //   114: aload_1
    //   115: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #484	-> 0
    //   #485	-> 2
    //   #486	-> 8
    //   #487	-> 63
    //   #488	-> 70
    //   #489	-> 75
    //   #490	-> 80
    //   #491	-> 96
    //   #492	-> 98
    //   #494	-> 103
    //   #496	-> 110
    //   #491	-> 111
    // Exception table:
    //   from	to	target	type
    //   2	8	111	finally
    //   8	63	111	finally
    //   67	69	111	finally
    //   70	75	111	finally
    //   75	80	111	finally
    //   80	87	111	finally
    //   96	98	111	finally
    //   112	114	111	finally
  }
  
  public void onActivityStopped(Activity paramActivity) {}
  
  public void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
  
  public void onActivityDestroyed(Activity paramActivity) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokevirtual findActivityState : (Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;
    //   7: astore_2
    //   8: getstatic android/nfc/NfcActivityManager.DBG : Ljava/lang/Boolean;
    //   11: invokevirtual booleanValue : ()Z
    //   14: ifeq -> 62
    //   17: new java/lang/StringBuilder
    //   20: astore_3
    //   21: aload_3
    //   22: invokespecial <init> : ()V
    //   25: aload_3
    //   26: ldc 'onDestroy() for '
    //   28: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   31: pop
    //   32: aload_3
    //   33: aload_1
    //   34: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   37: pop
    //   38: aload_3
    //   39: ldc_w ' '
    //   42: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   45: pop
    //   46: aload_3
    //   47: aload_2
    //   48: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   51: pop
    //   52: ldc 'NFC'
    //   54: aload_3
    //   55: invokevirtual toString : ()Ljava/lang/String;
    //   58: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   61: pop
    //   62: aload_2
    //   63: ifnull -> 71
    //   66: aload_0
    //   67: aload_1
    //   68: invokevirtual destroyActivityState : (Landroid/app/Activity;)V
    //   71: aload_0
    //   72: monitorexit
    //   73: return
    //   74: astore_1
    //   75: aload_0
    //   76: monitorexit
    //   77: aload_1
    //   78: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #509	-> 0
    //   #510	-> 2
    //   #511	-> 8
    //   #512	-> 62
    //   #514	-> 66
    //   #516	-> 71
    //   #517	-> 73
    //   #516	-> 74
    // Exception table:
    //   from	to	target	type
    //   2	8	74	finally
    //   8	62	74	finally
    //   66	71	74	finally
    //   71	73	74	finally
    //   75	77	74	finally
  }
}
