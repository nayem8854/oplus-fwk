package android.nfc;

import android.content.ComponentName;
import android.nfc.cardemulation.AidGroup;
import android.nfc.cardemulation.ApduServiceInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface INfcCardEmulation extends IInterface {
  AidGroup getAidGroupForService(int paramInt, ComponentName paramComponentName, String paramString) throws RemoteException;
  
  ApduServiceInfo getPreferredPaymentService(int paramInt) throws RemoteException;
  
  List<ApduServiceInfo> getServices(int paramInt, String paramString) throws RemoteException;
  
  boolean isDefaultServiceForAid(int paramInt, ComponentName paramComponentName, String paramString) throws RemoteException;
  
  boolean isDefaultServiceForCategory(int paramInt, ComponentName paramComponentName, String paramString) throws RemoteException;
  
  boolean registerAidGroupForService(int paramInt, ComponentName paramComponentName, AidGroup paramAidGroup) throws RemoteException;
  
  boolean removeAidGroupForService(int paramInt, ComponentName paramComponentName, String paramString) throws RemoteException;
  
  boolean setDefaultForNextTap(int paramInt, ComponentName paramComponentName) throws RemoteException;
  
  boolean setDefaultServiceForCategory(int paramInt, ComponentName paramComponentName, String paramString) throws RemoteException;
  
  boolean setOffHostForService(int paramInt, ComponentName paramComponentName, String paramString) throws RemoteException;
  
  boolean setPreferredService(ComponentName paramComponentName) throws RemoteException;
  
  boolean supportsAidPrefixRegistration() throws RemoteException;
  
  boolean unsetOffHostForService(int paramInt, ComponentName paramComponentName) throws RemoteException;
  
  boolean unsetPreferredService() throws RemoteException;
  
  class Default implements INfcCardEmulation {
    public boolean isDefaultServiceForCategory(int param1Int, ComponentName param1ComponentName, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean isDefaultServiceForAid(int param1Int, ComponentName param1ComponentName, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean setDefaultServiceForCategory(int param1Int, ComponentName param1ComponentName, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean setDefaultForNextTap(int param1Int, ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean registerAidGroupForService(int param1Int, ComponentName param1ComponentName, AidGroup param1AidGroup) throws RemoteException {
      return false;
    }
    
    public boolean setOffHostForService(int param1Int, ComponentName param1ComponentName, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean unsetOffHostForService(int param1Int, ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public AidGroup getAidGroupForService(int param1Int, ComponentName param1ComponentName, String param1String) throws RemoteException {
      return null;
    }
    
    public boolean removeAidGroupForService(int param1Int, ComponentName param1ComponentName, String param1String) throws RemoteException {
      return false;
    }
    
    public List<ApduServiceInfo> getServices(int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public boolean setPreferredService(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean unsetPreferredService() throws RemoteException {
      return false;
    }
    
    public boolean supportsAidPrefixRegistration() throws RemoteException {
      return false;
    }
    
    public ApduServiceInfo getPreferredPaymentService(int param1Int) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INfcCardEmulation {
    private static final String DESCRIPTOR = "android.nfc.INfcCardEmulation";
    
    static final int TRANSACTION_getAidGroupForService = 8;
    
    static final int TRANSACTION_getPreferredPaymentService = 14;
    
    static final int TRANSACTION_getServices = 10;
    
    static final int TRANSACTION_isDefaultServiceForAid = 2;
    
    static final int TRANSACTION_isDefaultServiceForCategory = 1;
    
    static final int TRANSACTION_registerAidGroupForService = 5;
    
    static final int TRANSACTION_removeAidGroupForService = 9;
    
    static final int TRANSACTION_setDefaultForNextTap = 4;
    
    static final int TRANSACTION_setDefaultServiceForCategory = 3;
    
    static final int TRANSACTION_setOffHostForService = 6;
    
    static final int TRANSACTION_setPreferredService = 11;
    
    static final int TRANSACTION_supportsAidPrefixRegistration = 13;
    
    static final int TRANSACTION_unsetOffHostForService = 7;
    
    static final int TRANSACTION_unsetPreferredService = 12;
    
    public Stub() {
      attachInterface(this, "android.nfc.INfcCardEmulation");
    }
    
    public static INfcCardEmulation asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.nfc.INfcCardEmulation");
      if (iInterface != null && iInterface instanceof INfcCardEmulation)
        return (INfcCardEmulation)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 14:
          return "getPreferredPaymentService";
        case 13:
          return "supportsAidPrefixRegistration";
        case 12:
          return "unsetPreferredService";
        case 11:
          return "setPreferredService";
        case 10:
          return "getServices";
        case 9:
          return "removeAidGroupForService";
        case 8:
          return "getAidGroupForService";
        case 7:
          return "unsetOffHostForService";
        case 6:
          return "setOffHostForService";
        case 5:
          return "registerAidGroupForService";
        case 4:
          return "setDefaultForNextTap";
        case 3:
          return "setDefaultServiceForCategory";
        case 2:
          return "isDefaultServiceForAid";
        case 1:
          break;
      } 
      return "isDefaultServiceForCategory";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool9;
        int i3;
        boolean bool8;
        int i2;
        boolean bool7;
        int i1;
        boolean bool6;
        int n;
        boolean bool5;
        int m;
        boolean bool4;
        int k;
        boolean bool3;
        int j;
        boolean bool2;
        ApduServiceInfo apduServiceInfo;
        String str3;
        List<ApduServiceInfo> list;
        String str2;
        AidGroup aidGroup;
        ComponentName componentName;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 14:
            param1Parcel1.enforceInterface("android.nfc.INfcCardEmulation");
            param1Int1 = param1Parcel1.readInt();
            apduServiceInfo = getPreferredPaymentService(param1Int1);
            param1Parcel2.writeNoException();
            if (apduServiceInfo != null) {
              param1Parcel2.writeInt(1);
              apduServiceInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 13:
            apduServiceInfo.enforceInterface("android.nfc.INfcCardEmulation");
            bool9 = supportsAidPrefixRegistration();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 12:
            apduServiceInfo.enforceInterface("android.nfc.INfcCardEmulation");
            bool9 = unsetPreferredService();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 11:
            apduServiceInfo.enforceInterface("android.nfc.INfcCardEmulation");
            if (apduServiceInfo.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)apduServiceInfo);
            } else {
              apduServiceInfo = null;
            } 
            bool9 = setPreferredService((ComponentName)apduServiceInfo);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 10:
            apduServiceInfo.enforceInterface("android.nfc.INfcCardEmulation");
            i3 = apduServiceInfo.readInt();
            str3 = apduServiceInfo.readString();
            list = getServices(i3, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 9:
            list.enforceInterface("android.nfc.INfcCardEmulation");
            i3 = list.readInt();
            if (list.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)list);
            } else {
              componentName = null;
            } 
            str2 = list.readString();
            bool8 = removeAidGroupForService(i3, componentName, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 8:
            str2.enforceInterface("android.nfc.INfcCardEmulation");
            i2 = str2.readInt();
            if (str2.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)str2);
            } else {
              componentName = null;
            } 
            str2 = str2.readString();
            aidGroup = getAidGroupForService(i2, componentName, str2);
            param1Parcel2.writeNoException();
            if (aidGroup != null) {
              param1Parcel2.writeInt(1);
              aidGroup.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 7:
            aidGroup.enforceInterface("android.nfc.INfcCardEmulation");
            i2 = aidGroup.readInt();
            if (aidGroup.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)aidGroup);
            } else {
              aidGroup = null;
            } 
            bool7 = unsetOffHostForService(i2, (ComponentName)aidGroup);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 6:
            aidGroup.enforceInterface("android.nfc.INfcCardEmulation");
            i1 = aidGroup.readInt();
            if (aidGroup.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)aidGroup);
            } else {
              componentName = null;
            } 
            str1 = aidGroup.readString();
            bool6 = setOffHostForService(i1, componentName, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 5:
            str1.enforceInterface("android.nfc.INfcCardEmulation");
            n = str1.readInt();
            if (str1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              componentName = null;
            } 
            if (str1.readInt() != 0) {
              AidGroup aidGroup1 = AidGroup.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            bool5 = registerAidGroupForService(n, componentName, (AidGroup)str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 4:
            str1.enforceInterface("android.nfc.INfcCardEmulation");
            m = str1.readInt();
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            bool4 = setDefaultForNextTap(m, (ComponentName)str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 3:
            str1.enforceInterface("android.nfc.INfcCardEmulation");
            k = str1.readInt();
            if (str1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              componentName = null;
            } 
            str1 = str1.readString();
            bool3 = setDefaultServiceForCategory(k, componentName, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 2:
            str1.enforceInterface("android.nfc.INfcCardEmulation");
            j = str1.readInt();
            if (str1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              componentName = null;
            } 
            str1 = str1.readString();
            bool2 = isDefaultServiceForAid(j, componentName, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.nfc.INfcCardEmulation");
        int i = str1.readInt();
        if (str1.readInt() != 0) {
          componentName = ComponentName.CREATOR.createFromParcel((Parcel)str1);
        } else {
          componentName = null;
        } 
        String str1 = str1.readString();
        boolean bool1 = isDefaultServiceForCategory(i, componentName, str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("android.nfc.INfcCardEmulation");
      return true;
    }
    
    private static class Proxy implements INfcCardEmulation {
      public static INfcCardEmulation sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.nfc.INfcCardEmulation";
      }
      
      public boolean isDefaultServiceForCategory(int param2Int, ComponentName param2ComponentName, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcCardEmulation");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool2 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool2 && INfcCardEmulation.Stub.getDefaultImpl() != null) {
            bool1 = INfcCardEmulation.Stub.getDefaultImpl().isDefaultServiceForCategory(param2Int, param2ComponentName, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDefaultServiceForAid(int param2Int, ComponentName param2ComponentName, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcCardEmulation");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool2 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool2 && INfcCardEmulation.Stub.getDefaultImpl() != null) {
            bool1 = INfcCardEmulation.Stub.getDefaultImpl().isDefaultServiceForAid(param2Int, param2ComponentName, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setDefaultServiceForCategory(int param2Int, ComponentName param2ComponentName, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcCardEmulation");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool2 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool2 && INfcCardEmulation.Stub.getDefaultImpl() != null) {
            bool1 = INfcCardEmulation.Stub.getDefaultImpl().setDefaultServiceForCategory(param2Int, param2ComponentName, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setDefaultForNextTap(int param2Int, ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcCardEmulation");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool2 && INfcCardEmulation.Stub.getDefaultImpl() != null) {
            bool1 = INfcCardEmulation.Stub.getDefaultImpl().setDefaultForNextTap(param2Int, param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean registerAidGroupForService(int param2Int, ComponentName param2ComponentName, AidGroup param2AidGroup) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcCardEmulation");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2AidGroup != null) {
            parcel1.writeInt(1);
            param2AidGroup.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool2 && INfcCardEmulation.Stub.getDefaultImpl() != null) {
            bool1 = INfcCardEmulation.Stub.getDefaultImpl().registerAidGroupForService(param2Int, param2ComponentName, param2AidGroup);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setOffHostForService(int param2Int, ComponentName param2ComponentName, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcCardEmulation");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool2 = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool2 && INfcCardEmulation.Stub.getDefaultImpl() != null) {
            bool1 = INfcCardEmulation.Stub.getDefaultImpl().setOffHostForService(param2Int, param2ComponentName, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean unsetOffHostForService(int param2Int, ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcCardEmulation");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool2 && INfcCardEmulation.Stub.getDefaultImpl() != null) {
            bool1 = INfcCardEmulation.Stub.getDefaultImpl().unsetOffHostForService(param2Int, param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public AidGroup getAidGroupForService(int param2Int, ComponentName param2ComponentName, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcCardEmulation");
          parcel1.writeInt(param2Int);
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && INfcCardEmulation.Stub.getDefaultImpl() != null)
            return INfcCardEmulation.Stub.getDefaultImpl().getAidGroupForService(param2Int, param2ComponentName, param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            AidGroup aidGroup = AidGroup.CREATOR.createFromParcel(parcel2);
          } else {
            param2ComponentName = null;
          } 
          return (AidGroup)param2ComponentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeAidGroupForService(int param2Int, ComponentName param2ComponentName, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcCardEmulation");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool2 = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool2 && INfcCardEmulation.Stub.getDefaultImpl() != null) {
            bool1 = INfcCardEmulation.Stub.getDefaultImpl().removeAidGroupForService(param2Int, param2ComponentName, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<ApduServiceInfo> getServices(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcCardEmulation");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && INfcCardEmulation.Stub.getDefaultImpl() != null)
            return INfcCardEmulation.Stub.getDefaultImpl().getServices(param2Int, param2String); 
          parcel2.readException();
          return parcel2.createTypedArrayList(ApduServiceInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setPreferredService(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcCardEmulation");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool2 && INfcCardEmulation.Stub.getDefaultImpl() != null) {
            bool1 = INfcCardEmulation.Stub.getDefaultImpl().setPreferredService(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean unsetPreferredService() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcCardEmulation");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(12, parcel1, parcel2, 0);
          if (!bool2 && INfcCardEmulation.Stub.getDefaultImpl() != null) {
            bool1 = INfcCardEmulation.Stub.getDefaultImpl().unsetPreferredService();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean supportsAidPrefixRegistration() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcCardEmulation");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(13, parcel1, parcel2, 0);
          if (!bool2 && INfcCardEmulation.Stub.getDefaultImpl() != null) {
            bool1 = INfcCardEmulation.Stub.getDefaultImpl().supportsAidPrefixRegistration();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ApduServiceInfo getPreferredPaymentService(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ApduServiceInfo apduServiceInfo;
          parcel1.writeInterfaceToken("android.nfc.INfcCardEmulation");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && INfcCardEmulation.Stub.getDefaultImpl() != null) {
            apduServiceInfo = INfcCardEmulation.Stub.getDefaultImpl().getPreferredPaymentService(param2Int);
            return apduServiceInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            apduServiceInfo = ApduServiceInfo.CREATOR.createFromParcel(parcel2);
          } else {
            apduServiceInfo = null;
          } 
          return apduServiceInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INfcCardEmulation param1INfcCardEmulation) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INfcCardEmulation != null) {
          Proxy.sDefaultImpl = param1INfcCardEmulation;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INfcCardEmulation getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
