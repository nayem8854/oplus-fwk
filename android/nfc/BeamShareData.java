package android.nfc;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.UserHandle;

public final class BeamShareData implements Parcelable {
  public BeamShareData(NdefMessage paramNdefMessage, Uri[] paramArrayOfUri, UserHandle paramUserHandle, int paramInt) {
    this.ndefMessage = paramNdefMessage;
    this.uris = paramArrayOfUri;
    this.userHandle = paramUserHandle;
    this.flags = paramInt;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    Uri[] arrayOfUri = this.uris;
    if (arrayOfUri != null) {
      paramInt = arrayOfUri.length;
    } else {
      paramInt = 0;
    } 
    paramParcel.writeParcelable(this.ndefMessage, 0);
    paramParcel.writeInt(paramInt);
    if (paramInt > 0)
      paramParcel.writeTypedArray(this.uris, 0); 
    paramParcel.writeParcelable(this.userHandle, 0);
    paramParcel.writeInt(this.flags);
  }
  
  public static final Parcelable.Creator<BeamShareData> CREATOR = new Parcelable.Creator<BeamShareData>() {
      public BeamShareData createFromParcel(Parcel param1Parcel) {
        Uri[] arrayOfUri = null;
        NdefMessage ndefMessage = param1Parcel.<NdefMessage>readParcelable(NdefMessage.class.getClassLoader());
        int i = param1Parcel.readInt();
        if (i > 0) {
          arrayOfUri = new Uri[i];
          param1Parcel.readTypedArray(arrayOfUri, Uri.CREATOR);
        } 
        UserHandle userHandle = param1Parcel.<UserHandle>readParcelable(UserHandle.class.getClassLoader());
        i = param1Parcel.readInt();
        return new BeamShareData(ndefMessage, arrayOfUri, userHandle, i);
      }
      
      public BeamShareData[] newArray(int param1Int) {
        return new BeamShareData[param1Int];
      }
    };
  
  public final int flags;
  
  public final NdefMessage ndefMessage;
  
  public final Uri[] uris;
  
  public final UserHandle userHandle;
}
