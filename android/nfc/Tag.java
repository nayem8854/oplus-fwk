package android.nfc;

import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcB;
import android.nfc.tech.NfcBarcode;
import android.nfc.tech.NfcF;
import android.nfc.tech.NfcV;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

public final class Tag implements Parcelable {
  public Tag(byte[] paramArrayOfbyte, int[] paramArrayOfint, Bundle[] paramArrayOfBundle, int paramInt, INfcTag paramINfcTag) {
    if (paramArrayOfint != null) {
      this.mId = paramArrayOfbyte;
      this.mTechList = Arrays.copyOf(paramArrayOfint, paramArrayOfint.length);
      this.mTechStringList = generateTechStringList(paramArrayOfint);
      this.mTechExtras = Arrays.<Bundle>copyOf(paramArrayOfBundle, paramArrayOfint.length);
      this.mServiceHandle = paramInt;
      this.mTagService = paramINfcTag;
      this.mConnectedTechnology = -1;
      return;
    } 
    throw new IllegalArgumentException("rawTargets cannot be null");
  }
  
  public static Tag createMockTag(byte[] paramArrayOfbyte, int[] paramArrayOfint, Bundle[] paramArrayOfBundle) {
    return new Tag(paramArrayOfbyte, paramArrayOfint, paramArrayOfBundle, 0, null);
  }
  
  private String[] generateTechStringList(int[] paramArrayOfint) {
    StringBuilder stringBuilder;
    int i = paramArrayOfint.length;
    String[] arrayOfString = new String[i];
    for (byte b = 0; b < i; b++) {
      switch (paramArrayOfint[b]) {
        default:
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown tech type ");
          stringBuilder.append(paramArrayOfint[b]);
          throw new IllegalArgumentException(stringBuilder.toString());
        case 10:
          stringBuilder[b] = (StringBuilder)NfcBarcode.class.getName();
          break;
        case 9:
          stringBuilder[b] = (StringBuilder)MifareUltralight.class.getName();
          break;
        case 8:
          stringBuilder[b] = (StringBuilder)MifareClassic.class.getName();
          break;
        case 7:
          stringBuilder[b] = (StringBuilder)NdefFormatable.class.getName();
          break;
        case 6:
          stringBuilder[b] = (StringBuilder)Ndef.class.getName();
          break;
        case 5:
          stringBuilder[b] = (StringBuilder)NfcV.class.getName();
          break;
        case 4:
          stringBuilder[b] = (StringBuilder)NfcF.class.getName();
          break;
        case 3:
          stringBuilder[b] = (StringBuilder)IsoDep.class.getName();
          break;
        case 2:
          stringBuilder[b] = (StringBuilder)NfcB.class.getName();
          break;
        case 1:
          stringBuilder[b] = (StringBuilder)NfcA.class.getName();
          break;
      } 
    } 
    return (String[])stringBuilder;
  }
  
  static int[] getTechCodesFromStrings(String[] paramArrayOfString) throws IllegalArgumentException {
    if (paramArrayOfString != null) {
      int[] arrayOfInt = new int[paramArrayOfString.length];
      HashMap<String, Integer> hashMap = getTechStringToCodeMap();
      for (byte b = 0; b < paramArrayOfString.length; ) {
        Integer integer = hashMap.get(paramArrayOfString[b]);
        if (integer != null) {
          arrayOfInt[b] = integer.intValue();
          b++;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown tech type ");
        stringBuilder.append(paramArrayOfString[b]);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      return arrayOfInt;
    } 
    throw new IllegalArgumentException("List cannot be null");
  }
  
  private static HashMap<String, Integer> getTechStringToCodeMap() {
    HashMap<Object, Object> hashMap = new HashMap<>();
    hashMap.put(IsoDep.class.getName(), Integer.valueOf(3));
    hashMap.put(MifareClassic.class.getName(), Integer.valueOf(8));
    hashMap.put(MifareUltralight.class.getName(), Integer.valueOf(9));
    hashMap.put(Ndef.class.getName(), Integer.valueOf(6));
    hashMap.put(NdefFormatable.class.getName(), Integer.valueOf(7));
    hashMap.put(NfcA.class.getName(), Integer.valueOf(1));
    hashMap.put(NfcB.class.getName(), Integer.valueOf(2));
    hashMap.put(NfcF.class.getName(), Integer.valueOf(4));
    hashMap.put(NfcV.class.getName(), Integer.valueOf(5));
    hashMap.put(NfcBarcode.class.getName(), Integer.valueOf(10));
    return (HashMap)hashMap;
  }
  
  public int getServiceHandle() {
    return this.mServiceHandle;
  }
  
  public int[] getTechCodeList() {
    return this.mTechList;
  }
  
  public byte[] getId() {
    return this.mId;
  }
  
  public String[] getTechList() {
    return this.mTechStringList;
  }
  
  public Tag rediscover() throws IOException {
    if (getConnectedTechnology() == -1) {
      INfcTag iNfcTag = this.mTagService;
      if (iNfcTag != null)
        try {
          Tag tag = iNfcTag.rediscover(getServiceHandle());
          if (tag != null)
            return tag; 
          IOException iOException = new IOException();
          this("Failed to rediscover tag");
          throw iOException;
        } catch (RemoteException remoteException) {
          throw new IOException("NFC service dead");
        }  
      throw new IOException("Mock tags don't support this operation.");
    } 
    throw new IllegalStateException("Close connection to the technology first!");
  }
  
  public boolean hasTech(int paramInt) {
    for (int i : this.mTechList) {
      if (i == paramInt)
        return true; 
    } 
    return false;
  }
  
  public Bundle getTechExtras(int paramInt) {
    byte b2, b1 = -1;
    byte b = 0;
    while (true) {
      int[] arrayOfInt = this.mTechList;
      b2 = b1;
      if (b < arrayOfInt.length) {
        if (arrayOfInt[b] == paramInt) {
          b2 = b;
          break;
        } 
        b++;
        continue;
      } 
      break;
    } 
    if (b2 < 0)
      return null; 
    return this.mTechExtras[b2];
  }
  
  public INfcTag getTagService() {
    return this.mTagService;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("TAG: Tech [");
    String[] arrayOfString = getTechList();
    int i = arrayOfString.length;
    for (byte b = 0; b < i; b++) {
      stringBuilder.append(arrayOfString[b]);
      if (b < i - 1)
        stringBuilder.append(", "); 
    } 
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  static byte[] readBytesWithNull(Parcel paramParcel) {
    int i = paramParcel.readInt();
    byte[] arrayOfByte = null;
    if (i >= 0) {
      arrayOfByte = new byte[i];
      paramParcel.readByteArray(arrayOfByte);
    } 
    return arrayOfByte;
  }
  
  static void writeBytesWithNull(Parcel paramParcel, byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte == null) {
      paramParcel.writeInt(-1);
      return;
    } 
    paramParcel.writeInt(paramArrayOfbyte.length);
    paramParcel.writeByteArray(paramArrayOfbyte);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (this.mTagService == null) {
      paramInt = 1;
    } else {
      paramInt = 0;
    } 
    writeBytesWithNull(paramParcel, this.mId);
    paramParcel.writeInt(this.mTechList.length);
    paramParcel.writeIntArray(this.mTechList);
    paramParcel.writeTypedArray(this.mTechExtras, 0);
    paramParcel.writeInt(this.mServiceHandle);
    paramParcel.writeInt(paramInt);
    if (paramInt == 0)
      paramParcel.writeStrongBinder(this.mTagService.asBinder()); 
  }
  
  public static final Parcelable.Creator<Tag> CREATOR = new Parcelable.Creator<Tag>() {
      public Tag createFromParcel(Parcel param1Parcel) {
        byte[] arrayOfByte = Tag.readBytesWithNull(param1Parcel);
        int[] arrayOfInt = new int[param1Parcel.readInt()];
        param1Parcel.readIntArray(arrayOfInt);
        Bundle[] arrayOfBundle = param1Parcel.<Bundle>createTypedArray(Bundle.CREATOR);
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        if (j == 0) {
          INfcTag iNfcTag = INfcTag.Stub.asInterface(param1Parcel.readStrongBinder());
        } else {
          param1Parcel = null;
        } 
        return new Tag(arrayOfByte, arrayOfInt, arrayOfBundle, i, (INfcTag)param1Parcel);
      }
      
      public Tag[] newArray(int param1Int) {
        return new Tag[param1Int];
      }
    };
  
  int mConnectedTechnology;
  
  final byte[] mId;
  
  final int mServiceHandle;
  
  final INfcTag mTagService;
  
  final Bundle[] mTechExtras;
  
  final int[] mTechList;
  
  final String[] mTechStringList;
  
  public boolean setConnectedTechnology(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mConnectedTechnology : I
    //   6: istore_2
    //   7: iload_2
    //   8: iconst_m1
    //   9: if_icmpeq -> 16
    //   12: aload_0
    //   13: monitorexit
    //   14: iconst_0
    //   15: ireturn
    //   16: aload_0
    //   17: iload_1
    //   18: putfield mConnectedTechnology : I
    //   21: aload_0
    //   22: monitorexit
    //   23: iconst_1
    //   24: ireturn
    //   25: astore_3
    //   26: aload_0
    //   27: monitorexit
    //   28: aload_3
    //   29: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #459	-> 2
    //   #460	-> 12
    //   #462	-> 16
    //   #463	-> 21
    //   #458	-> 25
    // Exception table:
    //   from	to	target	type
    //   2	7	25	finally
    //   16	21	25	finally
  }
  
  public int getConnectedTechnology() {
    return this.mConnectedTechnology;
  }
  
  public void setTechnologyDisconnected() {
    this.mConnectedTechnology = -1;
  }
}
