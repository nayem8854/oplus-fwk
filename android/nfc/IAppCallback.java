package android.nfc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAppCallback extends IInterface {
  BeamShareData createBeamShareData(byte paramByte) throws RemoteException;
  
  void onNdefPushComplete(byte paramByte) throws RemoteException;
  
  void onTagDiscovered(Tag paramTag) throws RemoteException;
  
  class Default implements IAppCallback {
    public BeamShareData createBeamShareData(byte param1Byte) throws RemoteException {
      return null;
    }
    
    public void onNdefPushComplete(byte param1Byte) throws RemoteException {}
    
    public void onTagDiscovered(Tag param1Tag) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAppCallback {
    private static final String DESCRIPTOR = "android.nfc.IAppCallback";
    
    static final int TRANSACTION_createBeamShareData = 1;
    
    static final int TRANSACTION_onNdefPushComplete = 2;
    
    static final int TRANSACTION_onTagDiscovered = 3;
    
    public Stub() {
      attachInterface(this, "android.nfc.IAppCallback");
    }
    
    public static IAppCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.nfc.IAppCallback");
      if (iInterface != null && iInterface instanceof IAppCallback)
        return (IAppCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onTagDiscovered";
        } 
        return "onNdefPushComplete";
      } 
      return "createBeamShareData";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.nfc.IAppCallback");
            return true;
          } 
          param1Parcel1.enforceInterface("android.nfc.IAppCallback");
          if (param1Parcel1.readInt() != 0) {
            Tag tag = Tag.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          onTagDiscovered((Tag)param1Parcel1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.nfc.IAppCallback");
        byte b1 = param1Parcel1.readByte();
        onNdefPushComplete(b1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.nfc.IAppCallback");
      byte b = param1Parcel1.readByte();
      BeamShareData beamShareData = createBeamShareData(b);
      param1Parcel2.writeNoException();
      if (beamShareData != null) {
        param1Parcel2.writeInt(1);
        beamShareData.writeToParcel(param1Parcel2, 1);
      } else {
        param1Parcel2.writeInt(0);
      } 
      return true;
    }
    
    private static class Proxy implements IAppCallback {
      public static IAppCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.nfc.IAppCallback";
      }
      
      public BeamShareData createBeamShareData(byte param2Byte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          BeamShareData beamShareData;
          parcel1.writeInterfaceToken("android.nfc.IAppCallback");
          parcel1.writeByte(param2Byte);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IAppCallback.Stub.getDefaultImpl() != null) {
            beamShareData = IAppCallback.Stub.getDefaultImpl().createBeamShareData(param2Byte);
            return beamShareData;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            beamShareData = BeamShareData.CREATOR.createFromParcel(parcel2);
          } else {
            beamShareData = null;
          } 
          return beamShareData;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onNdefPushComplete(byte param2Byte) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.nfc.IAppCallback");
          parcel.writeByte(param2Byte);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IAppCallback.Stub.getDefaultImpl() != null) {
            IAppCallback.Stub.getDefaultImpl().onNdefPushComplete(param2Byte);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTagDiscovered(Tag param2Tag) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.nfc.IAppCallback");
          if (param2Tag != null) {
            parcel.writeInt(1);
            param2Tag.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IAppCallback.Stub.getDefaultImpl() != null) {
            IAppCallback.Stub.getDefaultImpl().onTagDiscovered(param2Tag);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAppCallback param1IAppCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAppCallback != null) {
          Proxy.sDefaultImpl = param1IAppCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAppCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
