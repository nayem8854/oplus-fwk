package android.nfc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INfcUnlockHandler extends IInterface {
  boolean onUnlockAttempted(Tag paramTag) throws RemoteException;
  
  class Default implements INfcUnlockHandler {
    public boolean onUnlockAttempted(Tag param1Tag) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INfcUnlockHandler {
    private static final String DESCRIPTOR = "android.nfc.INfcUnlockHandler";
    
    static final int TRANSACTION_onUnlockAttempted = 1;
    
    public Stub() {
      attachInterface(this, "android.nfc.INfcUnlockHandler");
    }
    
    public static INfcUnlockHandler asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.nfc.INfcUnlockHandler");
      if (iInterface != null && iInterface instanceof INfcUnlockHandler)
        return (INfcUnlockHandler)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onUnlockAttempted";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.nfc.INfcUnlockHandler");
        return true;
      } 
      param1Parcel1.enforceInterface("android.nfc.INfcUnlockHandler");
      if (param1Parcel1.readInt() != 0) {
        Tag tag = Tag.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      boolean bool = onUnlockAttempted((Tag)param1Parcel1);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements INfcUnlockHandler {
      public static INfcUnlockHandler sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.nfc.INfcUnlockHandler";
      }
      
      public boolean onUnlockAttempted(Tag param2Tag) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcUnlockHandler");
          boolean bool1 = true;
          if (param2Tag != null) {
            parcel1.writeInt(1);
            param2Tag.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool2 && INfcUnlockHandler.Stub.getDefaultImpl() != null) {
            bool1 = INfcUnlockHandler.Stub.getDefaultImpl().onUnlockAttempted(param2Tag);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INfcUnlockHandler param1INfcUnlockHandler) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INfcUnlockHandler != null) {
          Proxy.sDefaultImpl = param1INfcUnlockHandler;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INfcUnlockHandler getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
