package android.nfc;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public class ApduList implements Parcelable {
  private ArrayList<byte[]> commands = (ArrayList)new ArrayList<>();
  
  public void add(byte[] paramArrayOfbyte) {
    this.commands.add(paramArrayOfbyte);
  }
  
  public List<byte[]> get() {
    return (List<byte[]>)this.commands;
  }
  
  public static final Parcelable.Creator<ApduList> CREATOR = new Parcelable.Creator<ApduList>() {
      public ApduList createFromParcel(Parcel param1Parcel) {
        return new ApduList(param1Parcel);
      }
      
      public ApduList[] newArray(int param1Int) {
        return new ApduList[param1Int];
      }
    };
  
  private ApduList(Parcel paramParcel) {
    int i = paramParcel.readInt();
    for (byte b = 0; b < i; b++) {
      int j = paramParcel.readInt();
      byte[] arrayOfByte = new byte[j];
      paramParcel.readByteArray(arrayOfByte);
      this.commands.add(arrayOfByte);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.commands.size());
    for (byte[] arrayOfByte : this.commands) {
      paramParcel.writeInt(arrayOfByte.length);
      paramParcel.writeByteArray(arrayOfByte);
    } 
  }
  
  public ApduList() {}
}
