package android.nfc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INfcTag extends IInterface {
  boolean canMakeReadOnly(int paramInt) throws RemoteException;
  
  int connect(int paramInt1, int paramInt2) throws RemoteException;
  
  int formatNdef(int paramInt, byte[] paramArrayOfbyte) throws RemoteException;
  
  boolean getExtendedLengthApdusSupported() throws RemoteException;
  
  int getMaxTransceiveLength(int paramInt) throws RemoteException;
  
  int[] getTechList(int paramInt) throws RemoteException;
  
  int getTimeout(int paramInt) throws RemoteException;
  
  boolean isNdef(int paramInt) throws RemoteException;
  
  boolean isPresent(int paramInt) throws RemoteException;
  
  boolean ndefIsWritable(int paramInt) throws RemoteException;
  
  int ndefMakeReadOnly(int paramInt) throws RemoteException;
  
  NdefMessage ndefRead(int paramInt) throws RemoteException;
  
  int ndefWrite(int paramInt, NdefMessage paramNdefMessage) throws RemoteException;
  
  int reconnect(int paramInt) throws RemoteException;
  
  Tag rediscover(int paramInt) throws RemoteException;
  
  void resetTimeouts() throws RemoteException;
  
  int setTimeout(int paramInt1, int paramInt2) throws RemoteException;
  
  TransceiveResult transceive(int paramInt, byte[] paramArrayOfbyte, boolean paramBoolean) throws RemoteException;
  
  class Default implements INfcTag {
    public int connect(int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int reconnect(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int[] getTechList(int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean isNdef(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isPresent(int param1Int) throws RemoteException {
      return false;
    }
    
    public TransceiveResult transceive(int param1Int, byte[] param1ArrayOfbyte, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public NdefMessage ndefRead(int param1Int) throws RemoteException {
      return null;
    }
    
    public int ndefWrite(int param1Int, NdefMessage param1NdefMessage) throws RemoteException {
      return 0;
    }
    
    public int ndefMakeReadOnly(int param1Int) throws RemoteException {
      return 0;
    }
    
    public boolean ndefIsWritable(int param1Int) throws RemoteException {
      return false;
    }
    
    public int formatNdef(int param1Int, byte[] param1ArrayOfbyte) throws RemoteException {
      return 0;
    }
    
    public Tag rediscover(int param1Int) throws RemoteException {
      return null;
    }
    
    public int setTimeout(int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int getTimeout(int param1Int) throws RemoteException {
      return 0;
    }
    
    public void resetTimeouts() throws RemoteException {}
    
    public boolean canMakeReadOnly(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getMaxTransceiveLength(int param1Int) throws RemoteException {
      return 0;
    }
    
    public boolean getExtendedLengthApdusSupported() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INfcTag {
    private static final String DESCRIPTOR = "android.nfc.INfcTag";
    
    static final int TRANSACTION_canMakeReadOnly = 16;
    
    static final int TRANSACTION_connect = 1;
    
    static final int TRANSACTION_formatNdef = 11;
    
    static final int TRANSACTION_getExtendedLengthApdusSupported = 18;
    
    static final int TRANSACTION_getMaxTransceiveLength = 17;
    
    static final int TRANSACTION_getTechList = 3;
    
    static final int TRANSACTION_getTimeout = 14;
    
    static final int TRANSACTION_isNdef = 4;
    
    static final int TRANSACTION_isPresent = 5;
    
    static final int TRANSACTION_ndefIsWritable = 10;
    
    static final int TRANSACTION_ndefMakeReadOnly = 9;
    
    static final int TRANSACTION_ndefRead = 7;
    
    static final int TRANSACTION_ndefWrite = 8;
    
    static final int TRANSACTION_reconnect = 2;
    
    static final int TRANSACTION_rediscover = 12;
    
    static final int TRANSACTION_resetTimeouts = 15;
    
    static final int TRANSACTION_setTimeout = 13;
    
    static final int TRANSACTION_transceive = 6;
    
    public Stub() {
      attachInterface(this, "android.nfc.INfcTag");
    }
    
    public static INfcTag asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.nfc.INfcTag");
      if (iInterface != null && iInterface instanceof INfcTag)
        return (INfcTag)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 18:
          return "getExtendedLengthApdusSupported";
        case 17:
          return "getMaxTransceiveLength";
        case 16:
          return "canMakeReadOnly";
        case 15:
          return "resetTimeouts";
        case 14:
          return "getTimeout";
        case 13:
          return "setTimeout";
        case 12:
          return "rediscover";
        case 11:
          return "formatNdef";
        case 10:
          return "ndefIsWritable";
        case 9:
          return "ndefMakeReadOnly";
        case 8:
          return "ndefWrite";
        case 7:
          return "ndefRead";
        case 6:
          return "transceive";
        case 5:
          return "isPresent";
        case 4:
          return "isNdef";
        case 3:
          return "getTechList";
        case 2:
          return "reconnect";
        case 1:
          break;
      } 
      return "connect";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        Tag tag;
        byte[] arrayOfByte1;
        NdefMessage ndefMessage;
        TransceiveResult transceiveResult;
        int[] arrayOfInt;
        byte[] arrayOfByte2;
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 18:
            param1Parcel1.enforceInterface("android.nfc.INfcTag");
            bool5 = getExtendedLengthApdusSupported();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 17:
            param1Parcel1.enforceInterface("android.nfc.INfcTag");
            n = param1Parcel1.readInt();
            n = getMaxTransceiveLength(n);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(n);
            return true;
          case 16:
            param1Parcel1.enforceInterface("android.nfc.INfcTag");
            n = param1Parcel1.readInt();
            bool4 = canMakeReadOnly(n);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 15:
            param1Parcel1.enforceInterface("android.nfc.INfcTag");
            resetTimeouts();
            param1Parcel2.writeNoException();
            return true;
          case 14:
            param1Parcel1.enforceInterface("android.nfc.INfcTag");
            m = param1Parcel1.readInt();
            m = getTimeout(m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 13:
            param1Parcel1.enforceInterface("android.nfc.INfcTag");
            param1Int2 = param1Parcel1.readInt();
            m = param1Parcel1.readInt();
            m = setTimeout(param1Int2, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 12:
            param1Parcel1.enforceInterface("android.nfc.INfcTag");
            m = param1Parcel1.readInt();
            tag = rediscover(m);
            param1Parcel2.writeNoException();
            if (tag != null) {
              param1Parcel2.writeInt(1);
              tag.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 11:
            tag.enforceInterface("android.nfc.INfcTag");
            m = tag.readInt();
            arrayOfByte1 = tag.createByteArray();
            m = formatNdef(m, arrayOfByte1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 10:
            arrayOfByte1.enforceInterface("android.nfc.INfcTag");
            m = arrayOfByte1.readInt();
            bool3 = ndefIsWritable(m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 9:
            arrayOfByte1.enforceInterface("android.nfc.INfcTag");
            k = arrayOfByte1.readInt();
            k = ndefMakeReadOnly(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 8:
            arrayOfByte1.enforceInterface("android.nfc.INfcTag");
            k = arrayOfByte1.readInt();
            if (arrayOfByte1.readInt() != 0) {
              NdefMessage ndefMessage1 = NdefMessage.CREATOR.createFromParcel((Parcel)arrayOfByte1);
            } else {
              arrayOfByte1 = null;
            } 
            k = ndefWrite(k, (NdefMessage)arrayOfByte1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 7:
            arrayOfByte1.enforceInterface("android.nfc.INfcTag");
            k = arrayOfByte1.readInt();
            ndefMessage = ndefRead(k);
            param1Parcel2.writeNoException();
            if (ndefMessage != null) {
              param1Parcel2.writeInt(1);
              ndefMessage.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 6:
            ndefMessage.enforceInterface("android.nfc.INfcTag");
            k = ndefMessage.readInt();
            arrayOfByte2 = ndefMessage.createByteArray();
            if (ndefMessage.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            transceiveResult = transceive(k, arrayOfByte2, bool);
            param1Parcel2.writeNoException();
            if (transceiveResult != null) {
              param1Parcel2.writeInt(1);
              transceiveResult.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 5:
            transceiveResult.enforceInterface("android.nfc.INfcTag");
            k = transceiveResult.readInt();
            bool2 = isPresent(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 4:
            transceiveResult.enforceInterface("android.nfc.INfcTag");
            j = transceiveResult.readInt();
            bool1 = isNdef(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 3:
            transceiveResult.enforceInterface("android.nfc.INfcTag");
            i = transceiveResult.readInt();
            arrayOfInt = getTechList(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt);
            return true;
          case 2:
            arrayOfInt.enforceInterface("android.nfc.INfcTag");
            i = arrayOfInt.readInt();
            i = reconnect(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 1:
            break;
        } 
        arrayOfInt.enforceInterface("android.nfc.INfcTag");
        int i = arrayOfInt.readInt();
        param1Int2 = arrayOfInt.readInt();
        i = connect(i, param1Int2);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(i);
        return true;
      } 
      param1Parcel2.writeString("android.nfc.INfcTag");
      return true;
    }
    
    private static class Proxy implements INfcTag {
      public static INfcTag sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.nfc.INfcTag";
      }
      
      public int connect(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcTag");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && INfcTag.Stub.getDefaultImpl() != null) {
            param2Int1 = INfcTag.Stub.getDefaultImpl().connect(param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int reconnect(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcTag");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && INfcTag.Stub.getDefaultImpl() != null) {
            param2Int = INfcTag.Stub.getDefaultImpl().reconnect(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getTechList(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcTag");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && INfcTag.Stub.getDefaultImpl() != null)
            return INfcTag.Stub.getDefaultImpl().getTechList(param2Int); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isNdef(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcTag");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && INfcTag.Stub.getDefaultImpl() != null) {
            bool1 = INfcTag.Stub.getDefaultImpl().isNdef(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isPresent(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcTag");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && INfcTag.Stub.getDefaultImpl() != null) {
            bool1 = INfcTag.Stub.getDefaultImpl().isPresent(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public TransceiveResult transceive(int param2Int, byte[] param2ArrayOfbyte, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.nfc.INfcTag");
          parcel1.writeInt(param2Int);
          parcel1.writeByteArray(param2ArrayOfbyte);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool1 && INfcTag.Stub.getDefaultImpl() != null)
            return INfcTag.Stub.getDefaultImpl().transceive(param2Int, param2ArrayOfbyte, param2Boolean); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            TransceiveResult transceiveResult = TransceiveResult.CREATOR.createFromParcel(parcel2);
          } else {
            param2ArrayOfbyte = null;
          } 
          return (TransceiveResult)param2ArrayOfbyte;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NdefMessage ndefRead(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          NdefMessage ndefMessage;
          parcel1.writeInterfaceToken("android.nfc.INfcTag");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && INfcTag.Stub.getDefaultImpl() != null) {
            ndefMessage = INfcTag.Stub.getDefaultImpl().ndefRead(param2Int);
            return ndefMessage;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ndefMessage = NdefMessage.CREATOR.createFromParcel(parcel2);
          } else {
            ndefMessage = null;
          } 
          return ndefMessage;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int ndefWrite(int param2Int, NdefMessage param2NdefMessage) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcTag");
          parcel1.writeInt(param2Int);
          if (param2NdefMessage != null) {
            parcel1.writeInt(1);
            param2NdefMessage.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && INfcTag.Stub.getDefaultImpl() != null) {
            param2Int = INfcTag.Stub.getDefaultImpl().ndefWrite(param2Int, param2NdefMessage);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int ndefMakeReadOnly(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcTag");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && INfcTag.Stub.getDefaultImpl() != null) {
            param2Int = INfcTag.Stub.getDefaultImpl().ndefMakeReadOnly(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean ndefIsWritable(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcTag");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
          if (!bool2 && INfcTag.Stub.getDefaultImpl() != null) {
            bool1 = INfcTag.Stub.getDefaultImpl().ndefIsWritable(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int formatNdef(int param2Int, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcTag");
          parcel1.writeInt(param2Int);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && INfcTag.Stub.getDefaultImpl() != null) {
            param2Int = INfcTag.Stub.getDefaultImpl().formatNdef(param2Int, param2ArrayOfbyte);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Tag rediscover(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Tag tag;
          parcel1.writeInterfaceToken("android.nfc.INfcTag");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && INfcTag.Stub.getDefaultImpl() != null) {
            tag = INfcTag.Stub.getDefaultImpl().rediscover(param2Int);
            return tag;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            tag = Tag.CREATOR.createFromParcel(parcel2);
          } else {
            tag = null;
          } 
          return tag;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setTimeout(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcTag");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && INfcTag.Stub.getDefaultImpl() != null) {
            param2Int1 = INfcTag.Stub.getDefaultImpl().setTimeout(param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getTimeout(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcTag");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && INfcTag.Stub.getDefaultImpl() != null) {
            param2Int = INfcTag.Stub.getDefaultImpl().getTimeout(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resetTimeouts() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcTag");
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && INfcTag.Stub.getDefaultImpl() != null) {
            INfcTag.Stub.getDefaultImpl().resetTimeouts();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean canMakeReadOnly(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcTag");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(16, parcel1, parcel2, 0);
          if (!bool2 && INfcTag.Stub.getDefaultImpl() != null) {
            bool1 = INfcTag.Stub.getDefaultImpl().canMakeReadOnly(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getMaxTransceiveLength(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcTag");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && INfcTag.Stub.getDefaultImpl() != null) {
            param2Int = INfcTag.Stub.getDefaultImpl().getMaxTransceiveLength(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getExtendedLengthApdusSupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcTag");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(18, parcel1, parcel2, 0);
          if (!bool2 && INfcTag.Stub.getDefaultImpl() != null) {
            bool1 = INfcTag.Stub.getDefaultImpl().getExtendedLengthApdusSupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INfcTag param1INfcTag) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INfcTag != null) {
          Proxy.sDefaultImpl = param1INfcTag;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INfcTag getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
