package android.nfc;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INfcAdapterExtras extends IInterface {
  void authenticate(String paramString, byte[] paramArrayOfbyte) throws RemoteException;
  
  Bundle close(String paramString, IBinder paramIBinder) throws RemoteException;
  
  int getCardEmulationRoute(String paramString) throws RemoteException;
  
  String getDriverName(String paramString) throws RemoteException;
  
  Bundle open(String paramString, IBinder paramIBinder) throws RemoteException;
  
  void setCardEmulationRoute(String paramString, int paramInt) throws RemoteException;
  
  Bundle transceive(String paramString, byte[] paramArrayOfbyte) throws RemoteException;
  
  class Default implements INfcAdapterExtras {
    public Bundle open(String param1String, IBinder param1IBinder) throws RemoteException {
      return null;
    }
    
    public Bundle close(String param1String, IBinder param1IBinder) throws RemoteException {
      return null;
    }
    
    public Bundle transceive(String param1String, byte[] param1ArrayOfbyte) throws RemoteException {
      return null;
    }
    
    public int getCardEmulationRoute(String param1String) throws RemoteException {
      return 0;
    }
    
    public void setCardEmulationRoute(String param1String, int param1Int) throws RemoteException {}
    
    public void authenticate(String param1String, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public String getDriverName(String param1String) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INfcAdapterExtras {
    private static final String DESCRIPTOR = "android.nfc.INfcAdapterExtras";
    
    static final int TRANSACTION_authenticate = 6;
    
    static final int TRANSACTION_close = 2;
    
    static final int TRANSACTION_getCardEmulationRoute = 4;
    
    static final int TRANSACTION_getDriverName = 7;
    
    static final int TRANSACTION_open = 1;
    
    static final int TRANSACTION_setCardEmulationRoute = 5;
    
    static final int TRANSACTION_transceive = 3;
    
    public Stub() {
      attachInterface(this, "android.nfc.INfcAdapterExtras");
    }
    
    public static INfcAdapterExtras asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.nfc.INfcAdapterExtras");
      if (iInterface != null && iInterface instanceof INfcAdapterExtras)
        return (INfcAdapterExtras)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "getDriverName";
        case 6:
          return "authenticate";
        case 5:
          return "setCardEmulationRoute";
        case 4:
          return "getCardEmulationRoute";
        case 3:
          return "transceive";
        case 2:
          return "close";
        case 1:
          break;
      } 
      return "open";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String str2;
        byte[] arrayOfByte2;
        String str1;
        byte[] arrayOfByte1;
        Bundle bundle3;
        IBinder iBinder2;
        Bundle bundle2;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("android.nfc.INfcAdapterExtras");
            str2 = param1Parcel1.readString();
            str2 = getDriverName(str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 6:
            str2.enforceInterface("android.nfc.INfcAdapterExtras");
            str3 = str2.readString();
            arrayOfByte2 = str2.createByteArray();
            authenticate(str3, arrayOfByte2);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            arrayOfByte2.enforceInterface("android.nfc.INfcAdapterExtras");
            str3 = arrayOfByte2.readString();
            param1Int1 = arrayOfByte2.readInt();
            setCardEmulationRoute(str3, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            arrayOfByte2.enforceInterface("android.nfc.INfcAdapterExtras");
            str1 = arrayOfByte2.readString();
            param1Int1 = getCardEmulationRoute(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 3:
            str1.enforceInterface("android.nfc.INfcAdapterExtras");
            str3 = str1.readString();
            arrayOfByte1 = str1.createByteArray();
            bundle3 = transceive(str3, arrayOfByte1);
            param1Parcel2.writeNoException();
            if (bundle3 != null) {
              param1Parcel2.writeInt(1);
              bundle3.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 2:
            bundle3.enforceInterface("android.nfc.INfcAdapterExtras");
            str3 = bundle3.readString();
            iBinder2 = bundle3.readStrongBinder();
            bundle2 = close(str3, iBinder2);
            param1Parcel2.writeNoException();
            if (bundle2 != null) {
              param1Parcel2.writeInt(1);
              bundle2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        bundle2.enforceInterface("android.nfc.INfcAdapterExtras");
        String str3 = bundle2.readString();
        IBinder iBinder1 = bundle2.readStrongBinder();
        Bundle bundle1 = open(str3, iBinder1);
        param1Parcel2.writeNoException();
        if (bundle1 != null) {
          param1Parcel2.writeInt(1);
          bundle1.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      param1Parcel2.writeString("android.nfc.INfcAdapterExtras");
      return true;
    }
    
    private static class Proxy implements INfcAdapterExtras {
      public static INfcAdapterExtras sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.nfc.INfcAdapterExtras";
      }
      
      public Bundle open(String param2String, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapterExtras");
          parcel1.writeString(param2String);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && INfcAdapterExtras.Stub.getDefaultImpl() != null)
            return INfcAdapterExtras.Stub.getDefaultImpl().open(param2String, param2IBinder); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Bundle bundle = Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (Bundle)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle close(String param2String, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapterExtras");
          parcel1.writeString(param2String);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && INfcAdapterExtras.Stub.getDefaultImpl() != null)
            return INfcAdapterExtras.Stub.getDefaultImpl().close(param2String, param2IBinder); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Bundle bundle = Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (Bundle)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle transceive(String param2String, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapterExtras");
          parcel1.writeString(param2String);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && INfcAdapterExtras.Stub.getDefaultImpl() != null)
            return INfcAdapterExtras.Stub.getDefaultImpl().transceive(param2String, param2ArrayOfbyte); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Bundle bundle = Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (Bundle)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCardEmulationRoute(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapterExtras");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && INfcAdapterExtras.Stub.getDefaultImpl() != null)
            return INfcAdapterExtras.Stub.getDefaultImpl().getCardEmulationRoute(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setCardEmulationRoute(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapterExtras");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && INfcAdapterExtras.Stub.getDefaultImpl() != null) {
            INfcAdapterExtras.Stub.getDefaultImpl().setCardEmulationRoute(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void authenticate(String param2String, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapterExtras");
          parcel1.writeString(param2String);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && INfcAdapterExtras.Stub.getDefaultImpl() != null) {
            INfcAdapterExtras.Stub.getDefaultImpl().authenticate(param2String, param2ArrayOfbyte);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDriverName(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapterExtras");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && INfcAdapterExtras.Stub.getDefaultImpl() != null) {
            param2String = INfcAdapterExtras.Stub.getDefaultImpl().getDriverName(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INfcAdapterExtras param1INfcAdapterExtras) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INfcAdapterExtras != null) {
          Proxy.sDefaultImpl = param1INfcAdapterExtras;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INfcAdapterExtras getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
