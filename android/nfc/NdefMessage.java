package android.nfc;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.proto.ProtoOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

public final class NdefMessage implements Parcelable {
  public NdefMessage(byte[] paramArrayOfbyte) throws FormatException {
    if (paramArrayOfbyte != null) {
      ByteBuffer byteBuffer = ByteBuffer.wrap(paramArrayOfbyte);
      this.mRecords = NdefRecord.parse(byteBuffer, false);
      if (byteBuffer.remaining() <= 0)
        return; 
      throw new FormatException("trailing data");
    } 
    throw new NullPointerException("data is null");
  }
  
  public NdefMessage(NdefRecord paramNdefRecord, NdefRecord... paramVarArgs) {
    if (paramNdefRecord != null) {
      int i;
      byte b;
      for (i = paramVarArgs.length, b = 0; b < i; ) {
        NdefRecord ndefRecord = paramVarArgs[b];
        if (ndefRecord != null) {
          b++;
          continue;
        } 
        throw new NullPointerException("record cannot be null");
      } 
      NdefRecord[] arrayOfNdefRecord = new NdefRecord[paramVarArgs.length + 1];
      arrayOfNdefRecord[0] = paramNdefRecord;
      System.arraycopy(paramVarArgs, 0, arrayOfNdefRecord, 1, paramVarArgs.length);
      return;
    } 
    throw new NullPointerException("record cannot be null");
  }
  
  public NdefMessage(NdefRecord[] paramArrayOfNdefRecord) {
    if (paramArrayOfNdefRecord.length >= 1) {
      int i;
      byte b;
      for (i = paramArrayOfNdefRecord.length, b = 0; b < i; ) {
        NdefRecord ndefRecord = paramArrayOfNdefRecord[b];
        if (ndefRecord != null) {
          b++;
          continue;
        } 
        throw new NullPointerException("records cannot contain null");
      } 
      this.mRecords = paramArrayOfNdefRecord;
      return;
    } 
    throw new IllegalArgumentException("must have at least one record");
  }
  
  public NdefRecord[] getRecords() {
    return this.mRecords;
  }
  
  public int getByteArrayLength() {
    int i = 0;
    for (NdefRecord ndefRecord : this.mRecords)
      i += ndefRecord.getByteLength(); 
    return i;
  }
  
  public byte[] toByteArray() {
    int i = getByteArrayLength();
    ByteBuffer byteBuffer = ByteBuffer.allocate(i);
    for (i = 0; i < this.mRecords.length; i++) {
      boolean bool2, bool1 = false;
      if (i == 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (i == this.mRecords.length - 1)
        bool1 = true; 
      this.mRecords[i].writeToByteBuffer(byteBuffer, bool2, bool1);
    } 
    return byteBuffer.array();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mRecords.length);
    paramParcel.writeTypedArray(this.mRecords, paramInt);
  }
  
  public static final Parcelable.Creator<NdefMessage> CREATOR = new Parcelable.Creator<NdefMessage>() {
      public NdefMessage createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        NdefRecord[] arrayOfNdefRecord = new NdefRecord[i];
        param1Parcel.readTypedArray(arrayOfNdefRecord, NdefRecord.CREATOR);
        return new NdefMessage(arrayOfNdefRecord);
      }
      
      public NdefMessage[] newArray(int param1Int) {
        return new NdefMessage[param1Int];
      }
    };
  
  private final NdefRecord[] mRecords;
  
  public int hashCode() {
    return Arrays.hashCode((Object[])this.mRecords);
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    return Arrays.equals((Object[])this.mRecords, (Object[])((NdefMessage)paramObject).mRecords);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("NdefMessage ");
    stringBuilder.append(Arrays.toString((Object[])this.mRecords));
    return stringBuilder.toString();
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream) {
    for (NdefRecord ndefRecord : this.mRecords) {
      long l = paramProtoOutputStream.start(2246267895809L);
      ndefRecord.dumpDebug(paramProtoOutputStream);
      paramProtoOutputStream.end(l);
    } 
  }
}
