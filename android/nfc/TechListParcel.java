package android.nfc;

import android.os.Parcel;
import android.os.Parcelable;

public class TechListParcel implements Parcelable {
  public TechListParcel(String[]... paramVarArgs) {
    this.mTechLists = paramVarArgs;
  }
  
  public String[][] getTechLists() {
    return this.mTechLists;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    int i = this.mTechLists.length;
    paramParcel.writeInt(i);
    for (paramInt = 0; paramInt < i; paramInt++) {
      String[] arrayOfString = this.mTechLists[paramInt];
      paramParcel.writeStringArray(arrayOfString);
    } 
  }
  
  public static final Parcelable.Creator<TechListParcel> CREATOR = new Parcelable.Creator<TechListParcel>() {
      public TechListParcel createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        String[][] arrayOfString = new String[i][];
        for (byte b = 0; b < i; b++)
          arrayOfString[b] = param1Parcel.readStringArray(); 
        return new TechListParcel(arrayOfString);
      }
      
      public TechListParcel[] newArray(int param1Int) {
        return new TechListParcel[param1Int];
      }
    };
  
  private String[][] mTechLists;
}
