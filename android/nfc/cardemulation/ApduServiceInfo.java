package android.nfc.cardemulation;

import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.util.proto.ProtoOutputStream;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParserException;

public class ApduServiceInfo extends OplusBaseApduServiceInfo implements Parcelable {
  protected int mUid;
  
  final String mStaticOffHostName;
  
  protected HashMap<String, AidGroup> mStaticAidGroups;
  
  protected String mSettingsActivityName;
  
  protected int mServiceState;
  
  protected ResolveInfo mService;
  
  protected boolean mRequiresDeviceUnlock;
  
  protected boolean mOnHost;
  
  String mOffHostName;
  
  protected HashMap<String, AidGroup> mDynamicAidGroups;
  
  protected String mDescription;
  
  byte[] mByteArrayBanner = new byte[] { 0 };
  
  protected int mBannerResourceId;
  
  static final String TAG = "ApduServiceInfo";
  
  public ApduServiceInfo(ResolveInfo paramResolveInfo, boolean paramBoolean1, String paramString1, ArrayList<AidGroup> paramArrayList1, ArrayList<AidGroup> paramArrayList2, boolean paramBoolean2, int paramInt1, int paramInt2, String paramString2, String paramString3, String paramString4) {
    this.mService = paramResolveInfo;
    this.mServiceState = 2;
    this.mDescription = paramString1;
    this.mStaticAidGroups = new HashMap<>();
    this.mDynamicAidGroups = new HashMap<>();
    this.mOffHostName = paramString3;
    this.mStaticOffHostName = paramString4;
    this.mOnHost = paramBoolean1;
    this.mRequiresDeviceUnlock = paramBoolean2;
    for (AidGroup aidGroup : paramArrayList1)
      this.mStaticAidGroups.put(aidGroup.category, aidGroup); 
    for (AidGroup aidGroup : paramArrayList2)
      this.mDynamicAidGroups.put(aidGroup.category, aidGroup); 
    this.mBannerResourceId = paramInt1;
    this.mUid = paramInt2;
    this.mSettingsActivityName = paramString2;
  }
  
  public ApduServiceInfo(ResolveInfo paramResolveInfo, boolean paramBoolean1, String paramString1, ArrayList<AidGroup> paramArrayList1, ArrayList<AidGroup> paramArrayList2, boolean paramBoolean2, int paramInt1, int paramInt2, String paramString2, String paramString3, String paramString4, int paramInt3, byte[] paramArrayOfbyte) {
    this.mService = paramResolveInfo;
    this.mServiceState = 2;
    this.mDescription = paramString1;
    this.mStaticAidGroups = new HashMap<>();
    this.mDynamicAidGroups = new HashMap<>();
    this.mOffHostName = paramString3;
    this.mStaticOffHostName = paramString4;
    this.mOnHost = paramBoolean1;
    this.mRequiresDeviceUnlock = paramBoolean2;
    for (AidGroup aidGroup : paramArrayList1)
      this.mStaticAidGroups.put(aidGroup.category, aidGroup); 
    for (AidGroup aidGroup : paramArrayList2)
      this.mDynamicAidGroups.put(aidGroup.category, aidGroup); 
    if (paramArrayOfbyte != null) {
      this.mByteArrayBanner = paramArrayOfbyte;
    } else {
      this.mByteArrayBanner = null;
    } 
    this.mBannerResourceId = paramInt1;
    this.mUid = paramInt2;
    this.mSettingsActivityName = paramString2;
    this.mServiceState = paramInt3;
  }
  
  public ApduServiceInfo(PackageManager paramPackageManager, ResolveInfo paramResolveInfo, boolean paramBoolean) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial <init> : ()V
    //   4: aload_0
    //   5: iconst_1
    //   6: newarray byte
    //   8: dup
    //   9: iconst_0
    //   10: iconst_0
    //   11: bastore
    //   12: putfield mByteArrayBanner : [B
    //   15: aload_2
    //   16: getfield serviceInfo : Landroid/content/pm/ServiceInfo;
    //   19: astore #4
    //   21: aload_0
    //   22: iconst_2
    //   23: putfield mServiceState : I
    //   26: aconst_null
    //   27: astore #5
    //   29: aconst_null
    //   30: astore #6
    //   32: iload_3
    //   33: ifeq -> 90
    //   36: aload #4
    //   38: aload_1
    //   39: ldc 'android.nfc.cardemulation.host_apdu_service'
    //   41: invokevirtual loadXmlMetaData : (Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;
    //   44: astore #7
    //   46: aload #7
    //   48: ifnull -> 54
    //   51: goto -> 105
    //   54: aload #7
    //   56: astore #6
    //   58: aload #7
    //   60: astore #5
    //   62: new org/xmlpull/v1/XmlPullParserException
    //   65: astore_1
    //   66: aload #7
    //   68: astore #6
    //   70: aload #7
    //   72: astore #5
    //   74: aload_1
    //   75: ldc 'No android.nfc.cardemulation.host_apdu_service meta-data'
    //   77: invokespecial <init> : (Ljava/lang/String;)V
    //   80: aload #7
    //   82: astore #6
    //   84: aload #7
    //   86: astore #5
    //   88: aload_1
    //   89: athrow
    //   90: aload #4
    //   92: aload_1
    //   93: ldc 'android.nfc.cardemulation.off_host_apdu_service'
    //   95: invokevirtual loadXmlMetaData : (Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;
    //   98: astore #7
    //   100: aload #7
    //   102: ifnull -> 2011
    //   105: aload #7
    //   107: astore #6
    //   109: aload #7
    //   111: astore #5
    //   113: aload #7
    //   115: invokeinterface getEventType : ()I
    //   120: istore #8
    //   122: iload #8
    //   124: iconst_2
    //   125: if_icmpeq -> 154
    //   128: iload #8
    //   130: iconst_1
    //   131: if_icmpeq -> 154
    //   134: aload #7
    //   136: astore #6
    //   138: aload #7
    //   140: astore #5
    //   142: aload #7
    //   144: invokeinterface next : ()I
    //   149: istore #8
    //   151: goto -> 122
    //   154: aload #7
    //   156: astore #6
    //   158: aload #7
    //   160: astore #5
    //   162: aload #7
    //   164: invokeinterface getName : ()Ljava/lang/String;
    //   169: astore #9
    //   171: iload_3
    //   172: ifeq -> 232
    //   175: aload #7
    //   177: astore #6
    //   179: aload #7
    //   181: astore #5
    //   183: ldc 'host-apdu-service'
    //   185: aload #9
    //   187: invokevirtual equals : (Ljava/lang/Object;)Z
    //   190: ifeq -> 196
    //   193: goto -> 232
    //   196: aload #7
    //   198: astore #6
    //   200: aload #7
    //   202: astore #5
    //   204: new org/xmlpull/v1/XmlPullParserException
    //   207: astore_1
    //   208: aload #7
    //   210: astore #6
    //   212: aload #7
    //   214: astore #5
    //   216: aload_1
    //   217: ldc 'Meta-data does not start with <host-apdu-service> tag'
    //   219: invokespecial <init> : (Ljava/lang/String;)V
    //   222: aload #7
    //   224: astore #6
    //   226: aload #7
    //   228: astore #5
    //   230: aload_1
    //   231: athrow
    //   232: iload_3
    //   233: ifne -> 293
    //   236: aload #7
    //   238: astore #6
    //   240: aload #7
    //   242: astore #5
    //   244: ldc 'offhost-apdu-service'
    //   246: aload #9
    //   248: invokevirtual equals : (Ljava/lang/Object;)Z
    //   251: ifeq -> 257
    //   254: goto -> 293
    //   257: aload #7
    //   259: astore #6
    //   261: aload #7
    //   263: astore #5
    //   265: new org/xmlpull/v1/XmlPullParserException
    //   268: astore_1
    //   269: aload #7
    //   271: astore #6
    //   273: aload #7
    //   275: astore #5
    //   277: aload_1
    //   278: ldc 'Meta-data does not start with <offhost-apdu-service> tag'
    //   280: invokespecial <init> : (Ljava/lang/String;)V
    //   283: aload #7
    //   285: astore #6
    //   287: aload #7
    //   289: astore #5
    //   291: aload_1
    //   292: athrow
    //   293: aload #7
    //   295: astore #6
    //   297: aload #7
    //   299: astore #5
    //   301: aload_1
    //   302: aload #4
    //   304: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   307: invokevirtual getResourcesForApplication : (Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
    //   310: astore #10
    //   312: aload #7
    //   314: astore #6
    //   316: aload #7
    //   318: astore #5
    //   320: aload #7
    //   322: invokestatic asAttributeSet : (Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
    //   325: astore #9
    //   327: iload_3
    //   328: ifeq -> 474
    //   331: aload #7
    //   333: astore #6
    //   335: aload #7
    //   337: astore #5
    //   339: aload #10
    //   341: aload #9
    //   343: getstatic com/android/internal/R$styleable.HostApduService : [I
    //   346: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   349: astore_1
    //   350: aload #7
    //   352: astore #6
    //   354: aload #7
    //   356: astore #5
    //   358: aload_0
    //   359: aload_2
    //   360: putfield mService : Landroid/content/pm/ResolveInfo;
    //   363: aload #7
    //   365: astore #6
    //   367: aload #7
    //   369: astore #5
    //   371: aload_0
    //   372: aload_1
    //   373: iconst_0
    //   374: invokevirtual getString : (I)Ljava/lang/String;
    //   377: putfield mDescription : Ljava/lang/String;
    //   380: aload #7
    //   382: astore #6
    //   384: aload #7
    //   386: astore #5
    //   388: aload_0
    //   389: aload_1
    //   390: iconst_2
    //   391: iconst_0
    //   392: invokevirtual getBoolean : (IZ)Z
    //   395: putfield mRequiresDeviceUnlock : Z
    //   398: aload #7
    //   400: astore #6
    //   402: aload #7
    //   404: astore #5
    //   406: aload_0
    //   407: aload_1
    //   408: iconst_3
    //   409: iconst_m1
    //   410: invokevirtual getResourceId : (II)I
    //   413: putfield mBannerResourceId : I
    //   416: aload #7
    //   418: astore #6
    //   420: aload #7
    //   422: astore #5
    //   424: aload_0
    //   425: aload_1
    //   426: iconst_1
    //   427: invokevirtual getString : (I)Ljava/lang/String;
    //   430: putfield mSettingsActivityName : Ljava/lang/String;
    //   433: aload #7
    //   435: astore #6
    //   437: aload #7
    //   439: astore #5
    //   441: aload_0
    //   442: aconst_null
    //   443: putfield mOffHostName : Ljava/lang/String;
    //   446: aload #7
    //   448: astore #6
    //   450: aload #7
    //   452: astore #5
    //   454: aload_0
    //   455: aconst_null
    //   456: putfield mStaticOffHostName : Ljava/lang/String;
    //   459: aload #7
    //   461: astore #6
    //   463: aload #7
    //   465: astore #5
    //   467: aload_1
    //   468: invokevirtual recycle : ()V
    //   471: goto -> 698
    //   474: aload #7
    //   476: astore #6
    //   478: aload #7
    //   480: astore #5
    //   482: aload #10
    //   484: aload #9
    //   486: getstatic com/android/internal/R$styleable.OffHostApduService : [I
    //   489: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   492: astore_1
    //   493: aload #7
    //   495: astore #6
    //   497: aload #7
    //   499: astore #5
    //   501: aload_0
    //   502: aload_2
    //   503: putfield mService : Landroid/content/pm/ResolveInfo;
    //   506: aload #7
    //   508: astore #6
    //   510: aload #7
    //   512: astore #5
    //   514: aload_0
    //   515: aload_1
    //   516: iconst_0
    //   517: invokevirtual getString : (I)Ljava/lang/String;
    //   520: putfield mDescription : Ljava/lang/String;
    //   523: aload #7
    //   525: astore #6
    //   527: aload #7
    //   529: astore #5
    //   531: aload_0
    //   532: iconst_0
    //   533: putfield mRequiresDeviceUnlock : Z
    //   536: aload #7
    //   538: astore #6
    //   540: aload #7
    //   542: astore #5
    //   544: aload_0
    //   545: aload_1
    //   546: iconst_2
    //   547: iconst_m1
    //   548: invokevirtual getResourceId : (II)I
    //   551: putfield mBannerResourceId : I
    //   554: aload #7
    //   556: astore #6
    //   558: aload #7
    //   560: astore #5
    //   562: aload_0
    //   563: aload_1
    //   564: iconst_1
    //   565: invokevirtual getString : (I)Ljava/lang/String;
    //   568: putfield mSettingsActivityName : Ljava/lang/String;
    //   571: aload #7
    //   573: astore #6
    //   575: aload #7
    //   577: astore #5
    //   579: aload_1
    //   580: iconst_3
    //   581: invokevirtual getString : (I)Ljava/lang/String;
    //   584: astore_2
    //   585: aload #7
    //   587: astore #6
    //   589: aload #7
    //   591: astore #5
    //   593: aload_0
    //   594: aload_2
    //   595: putfield mOffHostName : Ljava/lang/String;
    //   598: aload_2
    //   599: ifnull -> 670
    //   602: aload #7
    //   604: astore #6
    //   606: aload #7
    //   608: astore #5
    //   610: aload_2
    //   611: ldc 'eSE'
    //   613: invokevirtual equals : (Ljava/lang/Object;)Z
    //   616: ifeq -> 636
    //   619: aload #7
    //   621: astore #6
    //   623: aload #7
    //   625: astore #5
    //   627: aload_0
    //   628: ldc 'eSE1'
    //   630: putfield mOffHostName : Ljava/lang/String;
    //   633: goto -> 670
    //   636: aload #7
    //   638: astore #6
    //   640: aload #7
    //   642: astore #5
    //   644: aload_0
    //   645: getfield mOffHostName : Ljava/lang/String;
    //   648: ldc 'SIM'
    //   650: invokevirtual equals : (Ljava/lang/Object;)Z
    //   653: ifeq -> 670
    //   656: aload #7
    //   658: astore #6
    //   660: aload #7
    //   662: astore #5
    //   664: aload_0
    //   665: ldc 'SIM1'
    //   667: putfield mOffHostName : Ljava/lang/String;
    //   670: aload #7
    //   672: astore #6
    //   674: aload #7
    //   676: astore #5
    //   678: aload_0
    //   679: aload_0
    //   680: getfield mOffHostName : Ljava/lang/String;
    //   683: putfield mStaticOffHostName : Ljava/lang/String;
    //   686: aload #7
    //   688: astore #6
    //   690: aload #7
    //   692: astore #5
    //   694: aload_1
    //   695: invokevirtual recycle : ()V
    //   698: aload #7
    //   700: astore #6
    //   702: aload #7
    //   704: astore #5
    //   706: new java/util/HashMap
    //   709: astore_1
    //   710: aload #7
    //   712: astore #6
    //   714: aload #7
    //   716: astore #5
    //   718: aload_1
    //   719: invokespecial <init> : ()V
    //   722: aload #7
    //   724: astore #6
    //   726: aload #7
    //   728: astore #5
    //   730: aload_0
    //   731: aload_1
    //   732: putfield mStaticAidGroups : Ljava/util/HashMap;
    //   735: aload #7
    //   737: astore #6
    //   739: aload #7
    //   741: astore #5
    //   743: new java/util/HashMap
    //   746: astore_1
    //   747: aload #7
    //   749: astore #6
    //   751: aload #7
    //   753: astore #5
    //   755: aload_1
    //   756: invokespecial <init> : ()V
    //   759: aload #7
    //   761: astore #6
    //   763: aload #7
    //   765: astore #5
    //   767: aload_0
    //   768: aload_1
    //   769: putfield mDynamicAidGroups : Ljava/util/HashMap;
    //   772: aload #7
    //   774: astore #6
    //   776: aload #7
    //   778: astore #5
    //   780: aload_0
    //   781: iload_3
    //   782: putfield mOnHost : Z
    //   785: aload #7
    //   787: astore #6
    //   789: aload #7
    //   791: astore #5
    //   793: aload #7
    //   795: invokeinterface getDepth : ()I
    //   800: istore #8
    //   802: aconst_null
    //   803: astore_1
    //   804: aload #7
    //   806: astore #6
    //   808: aload #7
    //   810: astore #5
    //   812: aload #7
    //   814: invokeinterface next : ()I
    //   819: istore #11
    //   821: iload #11
    //   823: iconst_3
    //   824: if_icmpne -> 847
    //   827: aload #7
    //   829: astore #6
    //   831: aload #7
    //   833: astore #5
    //   835: aload #7
    //   837: invokeinterface getDepth : ()I
    //   842: iload #8
    //   844: if_icmple -> 1986
    //   847: iload #11
    //   849: iconst_1
    //   850: if_icmpeq -> 1986
    //   853: aload #7
    //   855: astore #6
    //   857: aload #7
    //   859: astore #5
    //   861: aload #7
    //   863: invokeinterface getName : ()Ljava/lang/String;
    //   868: astore_2
    //   869: iload #11
    //   871: iconst_2
    //   872: if_icmpne -> 1141
    //   875: aload #7
    //   877: astore #6
    //   879: aload #7
    //   881: astore #5
    //   883: ldc 'aid-group'
    //   885: aload_2
    //   886: invokevirtual equals : (Ljava/lang/Object;)Z
    //   889: ifeq -> 1141
    //   892: aload_1
    //   893: ifnonnull -> 1141
    //   896: aload #7
    //   898: astore #6
    //   900: aload #7
    //   902: astore #5
    //   904: aload #10
    //   906: aload #9
    //   908: getstatic com/android/internal/R$styleable.AidGroup : [I
    //   911: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   914: astore #12
    //   916: aload #7
    //   918: astore #6
    //   920: aload #7
    //   922: astore #5
    //   924: aload #12
    //   926: iconst_1
    //   927: invokevirtual getString : (I)Ljava/lang/String;
    //   930: astore_1
    //   931: aload #7
    //   933: astore #6
    //   935: aload #7
    //   937: astore #5
    //   939: aload #12
    //   941: iconst_0
    //   942: invokevirtual getString : (I)Ljava/lang/String;
    //   945: astore #13
    //   947: aload #7
    //   949: astore #6
    //   951: aload #7
    //   953: astore #5
    //   955: ldc 'payment'
    //   957: aload_1
    //   958: invokevirtual equals : (Ljava/lang/Object;)Z
    //   961: istore_3
    //   962: iload_3
    //   963: ifne -> 969
    //   966: ldc 'other'
    //   968: astore_1
    //   969: aload #7
    //   971: astore #6
    //   973: aload #7
    //   975: astore #5
    //   977: aload_0
    //   978: getfield mStaticAidGroups : Ljava/util/HashMap;
    //   981: aload_1
    //   982: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   985: checkcast android/nfc/cardemulation/AidGroup
    //   988: astore_2
    //   989: aload_2
    //   990: ifnull -> 1106
    //   993: aload #7
    //   995: astore #6
    //   997: aload #7
    //   999: astore #5
    //   1001: ldc 'other'
    //   1003: aload_1
    //   1004: invokevirtual equals : (Ljava/lang/Object;)Z
    //   1007: ifne -> 1101
    //   1010: aload #7
    //   1012: astore #6
    //   1014: aload #7
    //   1016: astore #5
    //   1018: new java/lang/StringBuilder
    //   1021: astore_2
    //   1022: aload #7
    //   1024: astore #6
    //   1026: aload #7
    //   1028: astore #5
    //   1030: aload_2
    //   1031: invokespecial <init> : ()V
    //   1034: aload #7
    //   1036: astore #6
    //   1038: aload #7
    //   1040: astore #5
    //   1042: aload_2
    //   1043: ldc 'Not allowing multiple aid-groups in the '
    //   1045: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1048: pop
    //   1049: aload #7
    //   1051: astore #6
    //   1053: aload #7
    //   1055: astore #5
    //   1057: aload_2
    //   1058: aload_1
    //   1059: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1062: pop
    //   1063: aload #7
    //   1065: astore #6
    //   1067: aload #7
    //   1069: astore #5
    //   1071: aload_2
    //   1072: ldc ' category'
    //   1074: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1077: pop
    //   1078: aload #7
    //   1080: astore #6
    //   1082: aload #7
    //   1084: astore #5
    //   1086: ldc 'ApduServiceInfo'
    //   1088: aload_2
    //   1089: invokevirtual toString : ()Ljava/lang/String;
    //   1092: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1095: pop
    //   1096: aconst_null
    //   1097: astore_1
    //   1098: goto -> 1125
    //   1101: aload_2
    //   1102: astore_1
    //   1103: goto -> 1125
    //   1106: aload #7
    //   1108: astore #6
    //   1110: aload #7
    //   1112: astore #5
    //   1114: new android/nfc/cardemulation/AidGroup
    //   1117: dup
    //   1118: aload_1
    //   1119: aload #13
    //   1121: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;)V
    //   1124: astore_1
    //   1125: aload #7
    //   1127: astore #6
    //   1129: aload #7
    //   1131: astore #5
    //   1133: aload #12
    //   1135: invokevirtual recycle : ()V
    //   1138: goto -> 804
    //   1141: iload #11
    //   1143: iconst_3
    //   1144: if_icmpne -> 1255
    //   1147: aload #7
    //   1149: astore #6
    //   1151: aload #7
    //   1153: astore #5
    //   1155: ldc 'aid-group'
    //   1157: aload_2
    //   1158: invokevirtual equals : (Ljava/lang/Object;)Z
    //   1161: ifeq -> 1255
    //   1164: aload_1
    //   1165: ifnull -> 1255
    //   1168: aload #7
    //   1170: astore #6
    //   1172: aload #7
    //   1174: astore #5
    //   1176: aload_1
    //   1177: getfield aids : Ljava/util/List;
    //   1180: invokeinterface size : ()I
    //   1185: ifle -> 1234
    //   1188: aload #7
    //   1190: astore #6
    //   1192: aload #7
    //   1194: astore #5
    //   1196: aload_0
    //   1197: getfield mStaticAidGroups : Ljava/util/HashMap;
    //   1200: aload_1
    //   1201: getfield category : Ljava/lang/String;
    //   1204: invokevirtual containsKey : (Ljava/lang/Object;)Z
    //   1207: ifne -> 1250
    //   1210: aload #7
    //   1212: astore #6
    //   1214: aload #7
    //   1216: astore #5
    //   1218: aload_0
    //   1219: getfield mStaticAidGroups : Ljava/util/HashMap;
    //   1222: aload_1
    //   1223: getfield category : Ljava/lang/String;
    //   1226: aload_1
    //   1227: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1230: pop
    //   1231: goto -> 1250
    //   1234: aload #7
    //   1236: astore #6
    //   1238: aload #7
    //   1240: astore #5
    //   1242: ldc 'ApduServiceInfo'
    //   1244: ldc 'Not adding <aid-group> with empty or invalid AIDs'
    //   1246: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1249: pop
    //   1250: aconst_null
    //   1251: astore_1
    //   1252: goto -> 804
    //   1255: iload #11
    //   1257: iconst_2
    //   1258: if_icmpne -> 1485
    //   1261: aload #7
    //   1263: astore #6
    //   1265: aload #7
    //   1267: astore #5
    //   1269: ldc 'aid-filter'
    //   1271: aload_2
    //   1272: invokevirtual equals : (Ljava/lang/Object;)Z
    //   1275: ifeq -> 1485
    //   1278: aload_1
    //   1279: ifnull -> 1485
    //   1282: aload #7
    //   1284: astore #6
    //   1286: aload #7
    //   1288: astore #5
    //   1290: aload #10
    //   1292: aload #9
    //   1294: getstatic com/android/internal/R$styleable.AidFilter : [I
    //   1297: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   1300: astore_2
    //   1301: aload #7
    //   1303: astore #6
    //   1305: aload #7
    //   1307: astore #5
    //   1309: aload_2
    //   1310: iconst_0
    //   1311: invokevirtual getString : (I)Ljava/lang/String;
    //   1314: astore #12
    //   1316: aload #7
    //   1318: astore #6
    //   1320: aload #7
    //   1322: astore #5
    //   1324: aload #12
    //   1326: invokevirtual toUpperCase : ()Ljava/lang/String;
    //   1329: astore #13
    //   1331: aload #7
    //   1333: astore #6
    //   1335: aload #7
    //   1337: astore #5
    //   1339: aload #13
    //   1341: invokestatic isValidAid : (Ljava/lang/String;)Z
    //   1344: ifeq -> 1392
    //   1347: aload #7
    //   1349: astore #6
    //   1351: aload #7
    //   1353: astore #5
    //   1355: aload_1
    //   1356: getfield aids : Ljava/util/List;
    //   1359: aload #13
    //   1361: invokeinterface contains : (Ljava/lang/Object;)Z
    //   1366: ifne -> 1392
    //   1369: aload #7
    //   1371: astore #6
    //   1373: aload #7
    //   1375: astore #5
    //   1377: aload_1
    //   1378: getfield aids : Ljava/util/List;
    //   1381: aload #13
    //   1383: invokeinterface add : (Ljava/lang/Object;)Z
    //   1388: pop
    //   1389: goto -> 1470
    //   1392: aload #7
    //   1394: astore #6
    //   1396: aload #7
    //   1398: astore #5
    //   1400: new java/lang/StringBuilder
    //   1403: astore #12
    //   1405: aload #7
    //   1407: astore #6
    //   1409: aload #7
    //   1411: astore #5
    //   1413: aload #12
    //   1415: invokespecial <init> : ()V
    //   1418: aload #7
    //   1420: astore #6
    //   1422: aload #7
    //   1424: astore #5
    //   1426: aload #12
    //   1428: ldc_w 'Ignoring invalid or duplicate aid: '
    //   1431: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1434: pop
    //   1435: aload #7
    //   1437: astore #6
    //   1439: aload #7
    //   1441: astore #5
    //   1443: aload #12
    //   1445: aload #13
    //   1447: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1450: pop
    //   1451: aload #7
    //   1453: astore #6
    //   1455: aload #7
    //   1457: astore #5
    //   1459: ldc 'ApduServiceInfo'
    //   1461: aload #12
    //   1463: invokevirtual toString : ()Ljava/lang/String;
    //   1466: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1469: pop
    //   1470: aload #7
    //   1472: astore #6
    //   1474: aload #7
    //   1476: astore #5
    //   1478: aload_2
    //   1479: invokevirtual recycle : ()V
    //   1482: goto -> 1983
    //   1485: iload #11
    //   1487: iconst_2
    //   1488: if_icmpne -> 1734
    //   1491: aload #7
    //   1493: astore #6
    //   1495: aload #7
    //   1497: astore #5
    //   1499: ldc_w 'aid-prefix-filter'
    //   1502: aload_2
    //   1503: invokevirtual equals : (Ljava/lang/Object;)Z
    //   1506: ifeq -> 1734
    //   1509: aload_1
    //   1510: ifnull -> 1734
    //   1513: aload #7
    //   1515: astore #6
    //   1517: aload #7
    //   1519: astore #5
    //   1521: aload #10
    //   1523: aload #9
    //   1525: getstatic com/android/internal/R$styleable.AidFilter : [I
    //   1528: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   1531: astore_2
    //   1532: aload #7
    //   1534: astore #6
    //   1536: aload #7
    //   1538: astore #5
    //   1540: aload_2
    //   1541: iconst_0
    //   1542: invokevirtual getString : (I)Ljava/lang/String;
    //   1545: astore #12
    //   1547: aload #7
    //   1549: astore #6
    //   1551: aload #7
    //   1553: astore #5
    //   1555: aload #12
    //   1557: invokevirtual toUpperCase : ()Ljava/lang/String;
    //   1560: astore #12
    //   1562: aload #7
    //   1564: astore #6
    //   1566: aload #7
    //   1568: astore #5
    //   1570: aload #12
    //   1572: ldc_w '*'
    //   1575: invokevirtual concat : (Ljava/lang/String;)Ljava/lang/String;
    //   1578: astore #12
    //   1580: aload #7
    //   1582: astore #6
    //   1584: aload #7
    //   1586: astore #5
    //   1588: aload #12
    //   1590: invokestatic isValidAid : (Ljava/lang/String;)Z
    //   1593: ifeq -> 1641
    //   1596: aload #7
    //   1598: astore #6
    //   1600: aload #7
    //   1602: astore #5
    //   1604: aload_1
    //   1605: getfield aids : Ljava/util/List;
    //   1608: aload #12
    //   1610: invokeinterface contains : (Ljava/lang/Object;)Z
    //   1615: ifne -> 1641
    //   1618: aload #7
    //   1620: astore #6
    //   1622: aload #7
    //   1624: astore #5
    //   1626: aload_1
    //   1627: getfield aids : Ljava/util/List;
    //   1630: aload #12
    //   1632: invokeinterface add : (Ljava/lang/Object;)Z
    //   1637: pop
    //   1638: goto -> 1719
    //   1641: aload #7
    //   1643: astore #6
    //   1645: aload #7
    //   1647: astore #5
    //   1649: new java/lang/StringBuilder
    //   1652: astore #13
    //   1654: aload #7
    //   1656: astore #6
    //   1658: aload #7
    //   1660: astore #5
    //   1662: aload #13
    //   1664: invokespecial <init> : ()V
    //   1667: aload #7
    //   1669: astore #6
    //   1671: aload #7
    //   1673: astore #5
    //   1675: aload #13
    //   1677: ldc_w 'Ignoring invalid or duplicate aid: '
    //   1680: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1683: pop
    //   1684: aload #7
    //   1686: astore #6
    //   1688: aload #7
    //   1690: astore #5
    //   1692: aload #13
    //   1694: aload #12
    //   1696: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1699: pop
    //   1700: aload #7
    //   1702: astore #6
    //   1704: aload #7
    //   1706: astore #5
    //   1708: ldc 'ApduServiceInfo'
    //   1710: aload #13
    //   1712: invokevirtual toString : ()Ljava/lang/String;
    //   1715: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1718: pop
    //   1719: aload #7
    //   1721: astore #6
    //   1723: aload #7
    //   1725: astore #5
    //   1727: aload_2
    //   1728: invokevirtual recycle : ()V
    //   1731: goto -> 1983
    //   1734: iload #11
    //   1736: iconst_2
    //   1737: if_icmpne -> 1983
    //   1740: aload #7
    //   1742: astore #6
    //   1744: aload #7
    //   1746: astore #5
    //   1748: aload_2
    //   1749: ldc_w 'aid-suffix-filter'
    //   1752: invokevirtual equals : (Ljava/lang/Object;)Z
    //   1755: ifeq -> 1983
    //   1758: aload_1
    //   1759: ifnull -> 1983
    //   1762: aload #7
    //   1764: astore #6
    //   1766: aload #7
    //   1768: astore #5
    //   1770: aload #10
    //   1772: aload #9
    //   1774: getstatic com/android/internal/R$styleable.AidFilter : [I
    //   1777: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   1780: astore_2
    //   1781: aload #7
    //   1783: astore #6
    //   1785: aload #7
    //   1787: astore #5
    //   1789: aload_2
    //   1790: iconst_0
    //   1791: invokevirtual getString : (I)Ljava/lang/String;
    //   1794: astore #12
    //   1796: aload #7
    //   1798: astore #6
    //   1800: aload #7
    //   1802: astore #5
    //   1804: aload #12
    //   1806: invokevirtual toUpperCase : ()Ljava/lang/String;
    //   1809: astore #12
    //   1811: aload #7
    //   1813: astore #6
    //   1815: aload #7
    //   1817: astore #5
    //   1819: aload #12
    //   1821: ldc_w '#'
    //   1824: invokevirtual concat : (Ljava/lang/String;)Ljava/lang/String;
    //   1827: astore #12
    //   1829: aload #7
    //   1831: astore #6
    //   1833: aload #7
    //   1835: astore #5
    //   1837: aload #12
    //   1839: invokestatic isValidAid : (Ljava/lang/String;)Z
    //   1842: ifeq -> 1890
    //   1845: aload #7
    //   1847: astore #6
    //   1849: aload #7
    //   1851: astore #5
    //   1853: aload_1
    //   1854: getfield aids : Ljava/util/List;
    //   1857: aload #12
    //   1859: invokeinterface contains : (Ljava/lang/Object;)Z
    //   1864: ifne -> 1890
    //   1867: aload #7
    //   1869: astore #6
    //   1871: aload #7
    //   1873: astore #5
    //   1875: aload_1
    //   1876: getfield aids : Ljava/util/List;
    //   1879: aload #12
    //   1881: invokeinterface add : (Ljava/lang/Object;)Z
    //   1886: pop
    //   1887: goto -> 1968
    //   1890: aload #7
    //   1892: astore #6
    //   1894: aload #7
    //   1896: astore #5
    //   1898: new java/lang/StringBuilder
    //   1901: astore #13
    //   1903: aload #7
    //   1905: astore #6
    //   1907: aload #7
    //   1909: astore #5
    //   1911: aload #13
    //   1913: invokespecial <init> : ()V
    //   1916: aload #7
    //   1918: astore #6
    //   1920: aload #7
    //   1922: astore #5
    //   1924: aload #13
    //   1926: ldc_w 'Ignoring invalid or duplicate aid: '
    //   1929: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1932: pop
    //   1933: aload #7
    //   1935: astore #6
    //   1937: aload #7
    //   1939: astore #5
    //   1941: aload #13
    //   1943: aload #12
    //   1945: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1948: pop
    //   1949: aload #7
    //   1951: astore #6
    //   1953: aload #7
    //   1955: astore #5
    //   1957: ldc 'ApduServiceInfo'
    //   1959: aload #13
    //   1961: invokevirtual toString : ()Ljava/lang/String;
    //   1964: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1967: pop
    //   1968: aload #7
    //   1970: astore #6
    //   1972: aload #7
    //   1974: astore #5
    //   1976: aload_2
    //   1977: invokevirtual recycle : ()V
    //   1980: goto -> 1983
    //   1983: goto -> 804
    //   1986: aload #7
    //   1988: ifnull -> 1998
    //   1991: aload #7
    //   1993: invokeinterface close : ()V
    //   1998: aload_0
    //   1999: aload #4
    //   2001: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   2004: getfield uid : I
    //   2007: putfield mUid : I
    //   2010: return
    //   2011: aload #7
    //   2013: astore #6
    //   2015: aload #7
    //   2017: astore #5
    //   2019: new org/xmlpull/v1/XmlPullParserException
    //   2022: astore_1
    //   2023: aload #7
    //   2025: astore #6
    //   2027: aload #7
    //   2029: astore #5
    //   2031: aload_1
    //   2032: ldc_w 'No android.nfc.cardemulation.off_host_apdu_service meta-data'
    //   2035: invokespecial <init> : (Ljava/lang/String;)V
    //   2038: aload #7
    //   2040: astore #6
    //   2042: aload #7
    //   2044: astore #5
    //   2046: aload_1
    //   2047: athrow
    //   2048: astore_1
    //   2049: goto -> 2121
    //   2052: astore_1
    //   2053: aload #5
    //   2055: astore #6
    //   2057: new org/xmlpull/v1/XmlPullParserException
    //   2060: astore_1
    //   2061: aload #5
    //   2063: astore #6
    //   2065: new java/lang/StringBuilder
    //   2068: astore_2
    //   2069: aload #5
    //   2071: astore #6
    //   2073: aload_2
    //   2074: invokespecial <init> : ()V
    //   2077: aload #5
    //   2079: astore #6
    //   2081: aload_2
    //   2082: ldc_w 'Unable to create context for: '
    //   2085: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2088: pop
    //   2089: aload #5
    //   2091: astore #6
    //   2093: aload_2
    //   2094: aload #4
    //   2096: getfield packageName : Ljava/lang/String;
    //   2099: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2102: pop
    //   2103: aload #5
    //   2105: astore #6
    //   2107: aload_1
    //   2108: aload_2
    //   2109: invokevirtual toString : ()Ljava/lang/String;
    //   2112: invokespecial <init> : (Ljava/lang/String;)V
    //   2115: aload #5
    //   2117: astore #6
    //   2119: aload_1
    //   2120: athrow
    //   2121: aload #6
    //   2123: ifnull -> 2133
    //   2126: aload #6
    //   2128: invokeinterface close : ()V
    //   2133: aload_1
    //   2134: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #223	-> 0
    //   #149	-> 4
    //   #224	-> 15
    //   #228	-> 21
    //   #230	-> 26
    //   #232	-> 32
    //   #233	-> 36
    //   #234	-> 46
    //   #235	-> 54
    //   #239	-> 90
    //   #240	-> 100
    //   #246	-> 105
    //   #247	-> 122
    //   #248	-> 134
    //   #251	-> 154
    //   #252	-> 171
    //   #253	-> 196
    //   #255	-> 232
    //   #256	-> 257
    //   #260	-> 293
    //   #261	-> 312
    //   #262	-> 327
    //   #263	-> 331
    //   #265	-> 350
    //   #266	-> 363
    //   #268	-> 380
    //   #271	-> 398
    //   #273	-> 416
    //   #275	-> 433
    //   #276	-> 446
    //   #277	-> 459
    //   #278	-> 471
    //   #279	-> 474
    //   #281	-> 493
    //   #282	-> 506
    //   #284	-> 523
    //   #285	-> 536
    //   #287	-> 554
    //   #289	-> 571
    //   #291	-> 598
    //   #292	-> 602
    //   #293	-> 619
    //   #294	-> 636
    //   #295	-> 656
    //   #298	-> 670
    //   #299	-> 686
    //   #302	-> 698
    //   #303	-> 735
    //   #304	-> 772
    //   #306	-> 785
    //   #307	-> 802
    //   #310	-> 804
    //   #312	-> 853
    //   #313	-> 869
    //   #315	-> 896
    //   #318	-> 916
    //   #320	-> 931
    //   #322	-> 947
    //   #323	-> 966
    //   #325	-> 969
    //   #326	-> 989
    //   #327	-> 993
    //   #328	-> 1010
    //   #330	-> 1096
    //   #327	-> 1101
    //   #333	-> 1106
    //   #335	-> 1125
    //   #336	-> 1138
    //   #313	-> 1141
    //   #336	-> 1141
    //   #338	-> 1168
    //   #339	-> 1188
    //   #340	-> 1210
    //   #343	-> 1234
    //   #345	-> 1250
    //   #346	-> 1255
    //   #348	-> 1282
    //   #350	-> 1301
    //   #351	-> 1316
    //   #352	-> 1331
    //   #353	-> 1369
    //   #355	-> 1392
    //   #357	-> 1470
    //   #358	-> 1482
    //   #359	-> 1491
    //   #360	-> 1513
    //   #362	-> 1532
    //   #363	-> 1547
    //   #365	-> 1562
    //   #366	-> 1580
    //   #367	-> 1618
    //   #369	-> 1641
    //   #371	-> 1719
    //   #372	-> 1734
    //   #373	-> 1740
    //   #374	-> 1762
    //   #376	-> 1781
    //   #377	-> 1796
    //   #379	-> 1811
    //   #380	-> 1829
    //   #381	-> 1867
    //   #383	-> 1890
    //   #385	-> 1968
    //   #386	-> 1980
    //   #372	-> 1983
    //   #310	-> 1983
    //   #391	-> 1986
    //   #394	-> 1998
    //   #395	-> 2010
    //   #241	-> 2011
    //   #391	-> 2048
    //   #388	-> 2052
    //   #389	-> 2053
    //   #391	-> 2121
    //   #392	-> 2133
    // Exception table:
    //   from	to	target	type
    //   36	46	2052	android/content/pm/PackageManager$NameNotFoundException
    //   36	46	2048	finally
    //   62	66	2052	android/content/pm/PackageManager$NameNotFoundException
    //   62	66	2048	finally
    //   74	80	2052	android/content/pm/PackageManager$NameNotFoundException
    //   74	80	2048	finally
    //   88	90	2052	android/content/pm/PackageManager$NameNotFoundException
    //   88	90	2048	finally
    //   90	100	2052	android/content/pm/PackageManager$NameNotFoundException
    //   90	100	2048	finally
    //   113	122	2052	android/content/pm/PackageManager$NameNotFoundException
    //   113	122	2048	finally
    //   142	151	2052	android/content/pm/PackageManager$NameNotFoundException
    //   142	151	2048	finally
    //   162	171	2052	android/content/pm/PackageManager$NameNotFoundException
    //   162	171	2048	finally
    //   183	193	2052	android/content/pm/PackageManager$NameNotFoundException
    //   183	193	2048	finally
    //   204	208	2052	android/content/pm/PackageManager$NameNotFoundException
    //   204	208	2048	finally
    //   216	222	2052	android/content/pm/PackageManager$NameNotFoundException
    //   216	222	2048	finally
    //   230	232	2052	android/content/pm/PackageManager$NameNotFoundException
    //   230	232	2048	finally
    //   244	254	2052	android/content/pm/PackageManager$NameNotFoundException
    //   244	254	2048	finally
    //   265	269	2052	android/content/pm/PackageManager$NameNotFoundException
    //   265	269	2048	finally
    //   277	283	2052	android/content/pm/PackageManager$NameNotFoundException
    //   277	283	2048	finally
    //   291	293	2052	android/content/pm/PackageManager$NameNotFoundException
    //   291	293	2048	finally
    //   301	312	2052	android/content/pm/PackageManager$NameNotFoundException
    //   301	312	2048	finally
    //   320	327	2052	android/content/pm/PackageManager$NameNotFoundException
    //   320	327	2048	finally
    //   339	350	2052	android/content/pm/PackageManager$NameNotFoundException
    //   339	350	2048	finally
    //   358	363	2052	android/content/pm/PackageManager$NameNotFoundException
    //   358	363	2048	finally
    //   371	380	2052	android/content/pm/PackageManager$NameNotFoundException
    //   371	380	2048	finally
    //   388	398	2052	android/content/pm/PackageManager$NameNotFoundException
    //   388	398	2048	finally
    //   406	416	2052	android/content/pm/PackageManager$NameNotFoundException
    //   406	416	2048	finally
    //   424	433	2052	android/content/pm/PackageManager$NameNotFoundException
    //   424	433	2048	finally
    //   441	446	2052	android/content/pm/PackageManager$NameNotFoundException
    //   441	446	2048	finally
    //   454	459	2052	android/content/pm/PackageManager$NameNotFoundException
    //   454	459	2048	finally
    //   467	471	2052	android/content/pm/PackageManager$NameNotFoundException
    //   467	471	2048	finally
    //   482	493	2052	android/content/pm/PackageManager$NameNotFoundException
    //   482	493	2048	finally
    //   501	506	2052	android/content/pm/PackageManager$NameNotFoundException
    //   501	506	2048	finally
    //   514	523	2052	android/content/pm/PackageManager$NameNotFoundException
    //   514	523	2048	finally
    //   531	536	2052	android/content/pm/PackageManager$NameNotFoundException
    //   531	536	2048	finally
    //   544	554	2052	android/content/pm/PackageManager$NameNotFoundException
    //   544	554	2048	finally
    //   562	571	2052	android/content/pm/PackageManager$NameNotFoundException
    //   562	571	2048	finally
    //   579	585	2052	android/content/pm/PackageManager$NameNotFoundException
    //   579	585	2048	finally
    //   593	598	2052	android/content/pm/PackageManager$NameNotFoundException
    //   593	598	2048	finally
    //   610	619	2052	android/content/pm/PackageManager$NameNotFoundException
    //   610	619	2048	finally
    //   627	633	2052	android/content/pm/PackageManager$NameNotFoundException
    //   627	633	2048	finally
    //   644	656	2052	android/content/pm/PackageManager$NameNotFoundException
    //   644	656	2048	finally
    //   664	670	2052	android/content/pm/PackageManager$NameNotFoundException
    //   664	670	2048	finally
    //   678	686	2052	android/content/pm/PackageManager$NameNotFoundException
    //   678	686	2048	finally
    //   694	698	2052	android/content/pm/PackageManager$NameNotFoundException
    //   694	698	2048	finally
    //   706	710	2052	android/content/pm/PackageManager$NameNotFoundException
    //   706	710	2048	finally
    //   718	722	2052	android/content/pm/PackageManager$NameNotFoundException
    //   718	722	2048	finally
    //   730	735	2052	android/content/pm/PackageManager$NameNotFoundException
    //   730	735	2048	finally
    //   743	747	2052	android/content/pm/PackageManager$NameNotFoundException
    //   743	747	2048	finally
    //   755	759	2052	android/content/pm/PackageManager$NameNotFoundException
    //   755	759	2048	finally
    //   767	772	2052	android/content/pm/PackageManager$NameNotFoundException
    //   767	772	2048	finally
    //   780	785	2052	android/content/pm/PackageManager$NameNotFoundException
    //   780	785	2048	finally
    //   793	802	2052	android/content/pm/PackageManager$NameNotFoundException
    //   793	802	2048	finally
    //   812	821	2052	android/content/pm/PackageManager$NameNotFoundException
    //   812	821	2048	finally
    //   835	847	2052	android/content/pm/PackageManager$NameNotFoundException
    //   835	847	2048	finally
    //   861	869	2052	android/content/pm/PackageManager$NameNotFoundException
    //   861	869	2048	finally
    //   883	892	2052	android/content/pm/PackageManager$NameNotFoundException
    //   883	892	2048	finally
    //   904	916	2052	android/content/pm/PackageManager$NameNotFoundException
    //   904	916	2048	finally
    //   924	931	2052	android/content/pm/PackageManager$NameNotFoundException
    //   924	931	2048	finally
    //   939	947	2052	android/content/pm/PackageManager$NameNotFoundException
    //   939	947	2048	finally
    //   955	962	2052	android/content/pm/PackageManager$NameNotFoundException
    //   955	962	2048	finally
    //   977	989	2052	android/content/pm/PackageManager$NameNotFoundException
    //   977	989	2048	finally
    //   1001	1010	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1001	1010	2048	finally
    //   1018	1022	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1018	1022	2048	finally
    //   1030	1034	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1030	1034	2048	finally
    //   1042	1049	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1042	1049	2048	finally
    //   1057	1063	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1057	1063	2048	finally
    //   1071	1078	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1071	1078	2048	finally
    //   1086	1096	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1086	1096	2048	finally
    //   1114	1125	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1114	1125	2048	finally
    //   1133	1138	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1133	1138	2048	finally
    //   1155	1164	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1155	1164	2048	finally
    //   1176	1188	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1176	1188	2048	finally
    //   1196	1210	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1196	1210	2048	finally
    //   1218	1231	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1218	1231	2048	finally
    //   1242	1250	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1242	1250	2048	finally
    //   1269	1278	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1269	1278	2048	finally
    //   1290	1301	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1290	1301	2048	finally
    //   1309	1316	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1309	1316	2048	finally
    //   1324	1331	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1324	1331	2048	finally
    //   1339	1347	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1339	1347	2048	finally
    //   1355	1369	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1355	1369	2048	finally
    //   1377	1389	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1377	1389	2048	finally
    //   1400	1405	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1400	1405	2048	finally
    //   1413	1418	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1413	1418	2048	finally
    //   1426	1435	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1426	1435	2048	finally
    //   1443	1451	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1443	1451	2048	finally
    //   1459	1470	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1459	1470	2048	finally
    //   1478	1482	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1478	1482	2048	finally
    //   1499	1509	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1499	1509	2048	finally
    //   1521	1532	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1521	1532	2048	finally
    //   1540	1547	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1540	1547	2048	finally
    //   1555	1562	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1555	1562	2048	finally
    //   1570	1580	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1570	1580	2048	finally
    //   1588	1596	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1588	1596	2048	finally
    //   1604	1618	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1604	1618	2048	finally
    //   1626	1638	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1626	1638	2048	finally
    //   1649	1654	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1649	1654	2048	finally
    //   1662	1667	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1662	1667	2048	finally
    //   1675	1684	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1675	1684	2048	finally
    //   1692	1700	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1692	1700	2048	finally
    //   1708	1719	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1708	1719	2048	finally
    //   1727	1731	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1727	1731	2048	finally
    //   1748	1758	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1748	1758	2048	finally
    //   1770	1781	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1770	1781	2048	finally
    //   1789	1796	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1789	1796	2048	finally
    //   1804	1811	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1804	1811	2048	finally
    //   1819	1829	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1819	1829	2048	finally
    //   1837	1845	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1837	1845	2048	finally
    //   1853	1867	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1853	1867	2048	finally
    //   1875	1887	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1875	1887	2048	finally
    //   1898	1903	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1898	1903	2048	finally
    //   1911	1916	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1911	1916	2048	finally
    //   1924	1933	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1924	1933	2048	finally
    //   1941	1949	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1941	1949	2048	finally
    //   1957	1968	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1957	1968	2048	finally
    //   1976	1980	2052	android/content/pm/PackageManager$NameNotFoundException
    //   1976	1980	2048	finally
    //   2019	2023	2052	android/content/pm/PackageManager$NameNotFoundException
    //   2019	2023	2048	finally
    //   2031	2038	2052	android/content/pm/PackageManager$NameNotFoundException
    //   2031	2038	2048	finally
    //   2046	2048	2052	android/content/pm/PackageManager$NameNotFoundException
    //   2046	2048	2048	finally
    //   2057	2061	2048	finally
    //   2065	2069	2048	finally
    //   2073	2077	2048	finally
    //   2081	2089	2048	finally
    //   2093	2103	2048	finally
    //   2107	2115	2048	finally
    //   2119	2121	2048	finally
  }
  
  public ComponentName getComponent() {
    return new ComponentName(this.mService.serviceInfo.packageName, this.mService.serviceInfo.name);
  }
  
  public String getOffHostSecureElement() {
    return this.mOffHostName;
  }
  
  public List<String> getAids() {
    ArrayList<String> arrayList = new ArrayList();
    for (AidGroup aidGroup : getAidGroups())
      arrayList.addAll(aidGroup.aids); 
    return arrayList;
  }
  
  public List<String> getPrefixAids() {
    ArrayList<String> arrayList = new ArrayList();
    for (AidGroup aidGroup : getAidGroups()) {
      for (String str : aidGroup.aids) {
        if (str.endsWith("*"))
          arrayList.add(str); 
      } 
    } 
    return arrayList;
  }
  
  public List<String> getSubsetAids() {
    ArrayList<String> arrayList = new ArrayList();
    for (AidGroup aidGroup : getAidGroups()) {
      for (String str : aidGroup.aids) {
        if (str.endsWith("#"))
          arrayList.add(str); 
      } 
    } 
    return arrayList;
  }
  
  public AidGroup getDynamicAidGroupForCategory(String paramString) {
    return this.mDynamicAidGroups.get(paramString);
  }
  
  public boolean removeDynamicAidGroupForCategory(String paramString) {
    boolean bool;
    if (this.mDynamicAidGroups.remove(paramString) != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public ArrayList<AidGroup> getAidGroups() {
    ArrayList<AidGroup> arrayList = new ArrayList();
    for (Map.Entry<String, AidGroup> entry : this.mDynamicAidGroups.entrySet())
      arrayList.add((AidGroup)entry.getValue()); 
    for (Map.Entry<String, AidGroup> entry : this.mStaticAidGroups.entrySet()) {
      if (!this.mDynamicAidGroups.containsKey(entry.getKey()))
        arrayList.add((AidGroup)entry.getValue()); 
    } 
    return arrayList;
  }
  
  public Bitmap getBitmapBanner() {
    if (this.mByteArrayBanner == null)
      return null; 
    Log.d("ApduServiceInfo", "getBitmapBanner++");
    byte[] arrayOfByte = this.mByteArrayBanner;
    return BitmapFactory.decodeByteArray(arrayOfByte, 0, arrayOfByte.length);
  }
  
  public String getCategoryForAid(String paramString) {
    ArrayList<AidGroup> arrayList = getAidGroups();
    for (AidGroup aidGroup : arrayList) {
      if (aidGroup.aids.contains(paramString.toUpperCase()))
        return aidGroup.category; 
    } 
    return null;
  }
  
  public boolean hasCategory(String paramString) {
    return (this.mStaticAidGroups.containsKey(paramString) || this.mDynamicAidGroups.containsKey(paramString));
  }
  
  public boolean isOnHost() {
    return this.mOnHost;
  }
  
  public boolean requiresUnlock() {
    return this.mRequiresDeviceUnlock;
  }
  
  public String getDescription() {
    return this.mDescription;
  }
  
  public int getUid() {
    return this.mUid;
  }
  
  public void setOrReplaceDynamicAidGroup(AidGroup paramAidGroup) {
    this.mDynamicAidGroups.put(paramAidGroup.getCategory(), paramAidGroup);
  }
  
  public void setOffHostSecureElement(String paramString) {
    this.mOffHostName = paramString;
  }
  
  public void unsetOffHostSecureElement() {
    this.mOffHostName = this.mStaticOffHostName;
  }
  
  public CharSequence loadLabel(PackageManager paramPackageManager) {
    return this.mService.loadLabel(paramPackageManager);
  }
  
  public CharSequence loadAppLabel(PackageManager paramPackageManager) {
    try {
      return paramPackageManager.getApplicationLabel(paramPackageManager.getApplicationInfo(this.mService.resolvePackageName, 128));
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      return null;
    } 
  }
  
  public Drawable loadIcon(PackageManager paramPackageManager) {
    return this.mService.loadIcon(paramPackageManager);
  }
  
  public Drawable loadBanner(PackageManager paramPackageManager) {
    try {
      BitmapDrawable bitmapDrawable;
      Drawable drawable;
      Resources resources = paramPackageManager.getResourcesForApplication(this.mService.serviceInfo.packageName);
      if (this.mBannerResourceId == -1) {
        bitmapDrawable = new BitmapDrawable(getBitmapBanner());
      } else {
        drawable = bitmapDrawable.getDrawable(this.mBannerResourceId);
      } 
      return drawable;
    } catch (android.content.res.Resources.NotFoundException notFoundException) {
      Log.e("ApduServiceInfo", "Could not load banner.");
      return null;
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      Log.e("ApduServiceInfo", "Could not load banner.");
      return null;
    } 
  }
  
  public String getSettingsActivityName() {
    return this.mSettingsActivityName;
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder("ApduService: ");
    stringBuilder1.append(getComponent());
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", description: ");
    stringBuilder2.append(this.mDescription);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append(", Static AID Groups: ");
    for (AidGroup aidGroup : this.mStaticAidGroups.values())
      stringBuilder1.append(aidGroup.toString()); 
    stringBuilder1.append(", Dynamic AID Groups: ");
    for (AidGroup aidGroup : this.mDynamicAidGroups.values())
      stringBuilder1.append(aidGroup.toString()); 
    return stringBuilder1.toString();
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof ApduServiceInfo))
      return false; 
    paramObject = paramObject;
    return paramObject.getComponent().equals(getComponent());
  }
  
  public int hashCode() {
    return getComponent().hashCode();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.mService.writeToParcel(paramParcel, paramInt);
    paramParcel.writeString(this.mDescription);
    paramParcel.writeInt(this.mOnHost);
    paramParcel.writeString(this.mOffHostName);
    paramParcel.writeString(this.mStaticOffHostName);
    paramParcel.writeInt(this.mStaticAidGroups.size());
    if (this.mStaticAidGroups.size() > 0)
      paramParcel.writeTypedList(new ArrayList<>(this.mStaticAidGroups.values())); 
    paramParcel.writeInt(this.mDynamicAidGroups.size());
    if (this.mDynamicAidGroups.size() > 0)
      paramParcel.writeTypedList(new ArrayList<>(this.mDynamicAidGroups.values())); 
    paramParcel.writeInt(this.mRequiresDeviceUnlock);
    paramParcel.writeInt(this.mBannerResourceId);
    paramParcel.writeInt(this.mUid);
    paramParcel.writeString(this.mSettingsActivityName);
    paramParcel.writeInt(this.mServiceState);
    paramParcel.writeByteArray(this.mByteArrayBanner);
  }
  
  public static final Parcelable.Creator<ApduServiceInfo> CREATOR = new Parcelable.Creator<ApduServiceInfo>() {
      public ApduServiceInfo createFromParcel(Parcel param1Parcel) {
        boolean bool1, bool2;
        ResolveInfo resolveInfo = ResolveInfo.CREATOR.createFromParcel(param1Parcel);
        String str1 = param1Parcel.readString();
        if (param1Parcel.readInt() != 0) {
          bool1 = true;
        } else {
          bool1 = false;
        } 
        String str2 = param1Parcel.readString();
        String str3 = param1Parcel.readString();
        ArrayList<AidGroup> arrayList1 = new ArrayList();
        int i = param1Parcel.readInt();
        if (i > 0)
          param1Parcel.readTypedList(arrayList1, AidGroup.CREATOR); 
        ArrayList<AidGroup> arrayList2 = new ArrayList();
        i = param1Parcel.readInt();
        if (i > 0)
          param1Parcel.readTypedList(arrayList2, AidGroup.CREATOR); 
        if (param1Parcel.readInt() != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        int j = param1Parcel.readInt();
        int k = param1Parcel.readInt();
        String str4 = param1Parcel.readString();
        i = param1Parcel.readInt();
        byte[] arrayOfByte = param1Parcel.createByteArray();
        return new ApduServiceInfo(resolveInfo, bool1, str1, arrayList1, arrayList2, bool2, j, k, str4, str2, str3, i, arrayOfByte);
      }
      
      public ApduServiceInfo[] newArray(int param1Int) {
        return new ApduServiceInfo[param1Int];
      }
    };
  
  public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("    ");
    stringBuilder2.append(getComponent());
    stringBuilder2.append(" (Description: ");
    stringBuilder2.append(getDescription());
    stringBuilder2.append(")");
    String str = stringBuilder2.toString();
    paramPrintWriter.println(str);
    if (this.mOnHost) {
      paramPrintWriter.println("    On Host Service");
    } else {
      paramPrintWriter.println("    Off-host Service");
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("        Current off-host SE:");
      stringBuilder.append(this.mOffHostName);
      stringBuilder.append(" static off-host SE:");
      stringBuilder.append(this.mStaticOffHostName);
      paramPrintWriter.println(stringBuilder.toString());
    } 
    paramPrintWriter.println("    Static AID groups:");
    for (AidGroup aidGroup : this.mStaticAidGroups.values()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("        Category: ");
      stringBuilder.append(aidGroup.category);
      paramPrintWriter.println(stringBuilder.toString());
      for (String str1 : aidGroup.aids) {
        StringBuilder stringBuilder3 = new StringBuilder();
        stringBuilder3.append("            AID: ");
        stringBuilder3.append(str1);
        paramPrintWriter.println(stringBuilder3.toString());
      } 
    } 
    paramPrintWriter.println("    Dynamic AID groups:");
    for (AidGroup aidGroup : this.mDynamicAidGroups.values()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("        Category: ");
      stringBuilder.append(aidGroup.category);
      paramPrintWriter.println(stringBuilder.toString());
      for (String str1 : aidGroup.aids) {
        StringBuilder stringBuilder3 = new StringBuilder();
        stringBuilder3.append("            AID: ");
        stringBuilder3.append(str1);
        paramPrintWriter.println(stringBuilder3.toString());
      } 
    } 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("    Settings Activity: ");
    stringBuilder1.append(this.mSettingsActivityName);
    paramPrintWriter.println(stringBuilder1.toString());
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream) {
    getComponent().dumpDebug(paramProtoOutputStream, 1146756268033L);
    paramProtoOutputStream.write(1138166333442L, getDescription());
    paramProtoOutputStream.write(1133871366147L, this.mOnHost);
    if (!this.mOnHost) {
      paramProtoOutputStream.write(1138166333444L, this.mOffHostName);
      paramProtoOutputStream.write(1138166333445L, this.mStaticOffHostName);
    } 
    for (AidGroup aidGroup : this.mStaticAidGroups.values()) {
      long l = paramProtoOutputStream.start(2246267895814L);
      aidGroup.dump(paramProtoOutputStream);
      paramProtoOutputStream.end(l);
    } 
    for (AidGroup aidGroup : this.mDynamicAidGroups.values()) {
      long l = paramProtoOutputStream.start(2246267895814L);
      aidGroup.dump(paramProtoOutputStream);
      paramProtoOutputStream.end(l);
    } 
    paramProtoOutputStream.write(1138166333448L, this.mSettingsActivityName);
  }
}
