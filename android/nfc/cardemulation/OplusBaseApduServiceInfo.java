package android.nfc.cardemulation;

public class OplusBaseApduServiceInfo {
  byte[] mByteArrayBanner = new byte[] { 0 };
  
  protected int mServiceState;
  
  public boolean isServiceEnabled(String paramString) {
    if (!"other".equals(paramString))
      return true; 
    int i = this.mServiceState;
    if (i == 1 || i == 3)
      return true; 
    return false;
  }
  
  public int setServiceState(String paramString, int paramInt) {
    if (paramString != "other")
      return 1; 
    this.mServiceState = paramInt;
    return paramInt;
  }
}
