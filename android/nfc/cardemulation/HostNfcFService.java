package android.nfc.cardemulation;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

public abstract class HostNfcFService extends Service {
  Messenger mNfcService = null;
  
  final Messenger mMessenger = new Messenger(new MsgHandler());
  
  static final String TAG = "NfcFService";
  
  public static final String SERVICE_META_DATA = "android.nfc.cardemulation.host_nfcf_service";
  
  public static final String SERVICE_INTERFACE = "android.nfc.cardemulation.action.HOST_NFCF_SERVICE";
  
  public static final int MSG_RESPONSE_PACKET = 1;
  
  public static final int MSG_DEACTIVATED = 2;
  
  public static final int MSG_COMMAND_PACKET = 0;
  
  public static final String KEY_MESSENGER = "messenger";
  
  public static final String KEY_DATA = "data";
  
  public static final int DEACTIVATION_LINK_LOSS = 0;
  
  class MsgHandler extends Handler {
    final HostNfcFService this$0;
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            super.handleMessage(param1Message);
          } else {
            HostNfcFService.this.mNfcService = null;
            HostNfcFService.this.onDeactivated(param1Message.arg1);
          } 
        } else {
          if (HostNfcFService.this.mNfcService == null) {
            Log.e("NfcFService", "Response not sent; service was deactivated.");
            return;
          } 
          try {
            param1Message.replyTo = HostNfcFService.this.mMessenger;
            HostNfcFService.this.mNfcService.send(param1Message);
          } catch (RemoteException remoteException) {
            Log.e("NfcFService", "RemoteException calling into NfcService.");
          } 
        } 
      } else {
        Bundle bundle = remoteException.getData();
        if (bundle == null)
          return; 
        if (HostNfcFService.this.mNfcService == null)
          HostNfcFService.this.mNfcService = ((Message)remoteException).replyTo; 
        byte[] arrayOfByte = bundle.getByteArray("data");
        if (arrayOfByte != null) {
          byte[] arrayOfByte1 = HostNfcFService.this.processNfcFPacket(arrayOfByte, null);
          if (arrayOfByte1 != null) {
            if (HostNfcFService.this.mNfcService == null) {
              Log.e("NfcFService", "Response not sent; service was deactivated.");
              return;
            } 
            Message message = Message.obtain((Handler)null, 1);
            Bundle bundle1 = new Bundle();
            bundle1.putByteArray("data", arrayOfByte1);
            message.setData(bundle1);
            message.replyTo = HostNfcFService.this.mMessenger;
            try {
              HostNfcFService.this.mNfcService.send(message);
            } catch (RemoteException remoteException1) {
              Log.e("TAG", "Response not sent; RemoteException calling into NfcService.");
            } 
          } 
        } else {
          Log.e("NfcFService", "Received MSG_COMMAND_PACKET without data.");
        } 
      } 
    }
  }
  
  public final IBinder onBind(Intent paramIntent) {
    return this.mMessenger.getBinder();
  }
  
  public abstract void onDeactivated(int paramInt);
  
  public abstract byte[] processNfcFPacket(byte[] paramArrayOfbyte, Bundle paramBundle);
  
  public final void sendResponsePacket(byte[] paramArrayOfbyte) {
    Message message = Message.obtain((Handler)null, 1);
    Bundle bundle = new Bundle();
    bundle.putByteArray("data", paramArrayOfbyte);
    message.setData(bundle);
    try {
      this.mMessenger.send(message);
    } catch (RemoteException remoteException) {
      Log.e("TAG", "Local messenger has died.");
    } 
  }
}
