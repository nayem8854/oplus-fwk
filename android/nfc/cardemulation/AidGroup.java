package android.nfc.cardemulation;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.util.proto.ProtoOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class AidGroup implements Parcelable {
  public AidGroup(List<String> paramList, String paramString) {
    if (paramList != null && paramList.size() != 0) {
      if (paramList.size() <= 256) {
        StringBuilder stringBuilder;
        for (String str : paramList) {
          if (CardEmulation.isValidAid(str))
            continue; 
          stringBuilder = new StringBuilder();
          stringBuilder.append("AID ");
          stringBuilder.append(str);
          stringBuilder.append(" is not a valid AID.");
          throw new IllegalArgumentException(stringBuilder.toString());
        } 
        if (isValidCategory(paramString)) {
          this.category = paramString;
        } else {
          this.category = "other";
        } 
        this.aids = new ArrayList<>(stringBuilder.size());
        for (String str : stringBuilder)
          this.aids.add(str.toUpperCase()); 
        this.description = null;
        return;
      } 
      throw new IllegalArgumentException("Too many AIDs in AID group.");
    } 
    throw new IllegalArgumentException("No AIDS in AID group.");
  }
  
  AidGroup(String paramString1, String paramString2) {
    this.aids = new ArrayList<>();
    this.category = paramString1;
    this.description = paramString2;
  }
  
  public String getCategory() {
    return this.category;
  }
  
  public List<String> getAids() {
    return this.aids;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Category: ");
    stringBuilder.append(this.category);
    stringBuilder.append(", AIDs:");
    stringBuilder = new StringBuilder(stringBuilder.toString());
    for (String str : this.aids) {
      stringBuilder.append(str);
      stringBuilder.append(", ");
    } 
    return stringBuilder.toString();
  }
  
  public void dump(ProtoOutputStream paramProtoOutputStream) {
    paramProtoOutputStream.write(1138166333441L, this.category);
    for (String str : this.aids)
      paramProtoOutputStream.write(2237677961218L, str); 
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.category);
    paramParcel.writeInt(this.aids.size());
    if (this.aids.size() > 0)
      paramParcel.writeStringList(this.aids); 
  }
  
  public static final Parcelable.Creator<AidGroup> CREATOR = new Parcelable.Creator<AidGroup>() {
      public AidGroup createFromParcel(Parcel param1Parcel) {
        String str = param1Parcel.readString();
        int i = param1Parcel.readInt();
        ArrayList<String> arrayList = new ArrayList();
        if (i > 0)
          param1Parcel.readStringList(arrayList); 
        return new AidGroup(arrayList, str);
      }
      
      public AidGroup[] newArray(int param1Int) {
        return new AidGroup[param1Int];
      }
    };
  
  public static final int MAX_NUM_AIDS = 256;
  
  static final String TAG = "AidGroup";
  
  protected List<String> aids;
  
  protected String category;
  
  protected String description;
  
  public static AidGroup createFromXml(XmlPullParser paramXmlPullParser) throws XmlPullParserException, IOException {
    AidGroup aidGroup;
    String str1 = null;
    ArrayList<String> arrayList = new ArrayList();
    String str2 = null;
    boolean bool = false;
    int i = paramXmlPullParser.getEventType();
    int j = paramXmlPullParser.getDepth();
    while (true) {
      String str = str2;
      if (i != 1) {
        str = str2;
        if (paramXmlPullParser.getDepth() >= j) {
          boolean bool1;
          String str3 = paramXmlPullParser.getName();
          if (i == 2) {
            if (str3.equals("aid")) {
              if (bool) {
                str = paramXmlPullParser.getAttributeValue(null, "value");
                if (str != null)
                  arrayList.add(str.toUpperCase()); 
                str = str1;
                bool1 = bool;
              } else {
                Log.d("AidGroup", "Ignoring <aid> tag while not in group");
                str = str1;
                bool1 = bool;
              } 
            } else if (str3.equals("aid-group")) {
              str = paramXmlPullParser.getAttributeValue(null, "category");
              if (str == null) {
                Log.e("AidGroup", "<aid-group> tag without valid category");
                return null;
              } 
              bool1 = true;
            } else {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Ignoring unexpected tag: ");
              stringBuilder.append(str3);
              Log.d("AidGroup", stringBuilder.toString());
              String str4 = str1;
              bool1 = bool;
            } 
          } else {
            str = str1;
            bool1 = bool;
            if (i == 3) {
              str = str1;
              bool1 = bool;
              if (str3.equals("aid-group")) {
                str = str1;
                bool1 = bool;
                if (bool) {
                  str = str1;
                  bool1 = bool;
                  if (arrayList.size() > 0) {
                    aidGroup = new AidGroup(arrayList, str1);
                    break;
                  } 
                } 
              } 
            } 
          } 
          i = paramXmlPullParser.next();
          AidGroup aidGroup1 = aidGroup;
          bool = bool1;
          continue;
        } 
      } 
      break;
    } 
    return aidGroup;
  }
  
  public void writeAsXml(XmlSerializer paramXmlSerializer) throws IOException {
    paramXmlSerializer.startTag(null, "aid-group");
    paramXmlSerializer.attribute(null, "category", this.category);
    for (String str : this.aids) {
      paramXmlSerializer.startTag(null, "aid");
      paramXmlSerializer.attribute(null, "value", str);
      paramXmlSerializer.endTag(null, "aid");
    } 
    paramXmlSerializer.endTag(null, "aid-group");
  }
  
  static boolean isValidCategory(String paramString) {
    return ("payment".equals(paramString) || 
      "other".equals(paramString));
  }
}
