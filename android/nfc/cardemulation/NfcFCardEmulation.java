package android.nfc.cardemulation;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.nfc.INfcFCardEmulation;
import android.nfc.NfcAdapter;
import android.os.RemoteException;
import android.util.Log;
import java.util.HashMap;
import java.util.List;

public final class NfcFCardEmulation {
  static final String TAG = "NfcFCardEmulation";
  
  static HashMap<Context, NfcFCardEmulation> sCardEmus;
  
  static boolean sIsInitialized = false;
  
  static INfcFCardEmulation sService;
  
  final Context mContext;
  
  static {
    sCardEmus = new HashMap<>();
  }
  
  private NfcFCardEmulation(Context paramContext, INfcFCardEmulation paramINfcFCardEmulation) {
    this.mContext = paramContext.getApplicationContext();
    sService = paramINfcFCardEmulation;
  }
  
  public static NfcFCardEmulation getInstance(NfcAdapter paramNfcAdapter) {
    // Byte code:
    //   0: ldc android/nfc/cardemulation/NfcFCardEmulation
    //   2: monitorenter
    //   3: aload_0
    //   4: ifnull -> 202
    //   7: aload_0
    //   8: invokevirtual getContext : ()Landroid/content/Context;
    //   11: astore_1
    //   12: aload_1
    //   13: ifnull -> 180
    //   16: getstatic android/nfc/cardemulation/NfcFCardEmulation.sIsInitialized : Z
    //   19: ifne -> 106
    //   22: invokestatic getPackageManager : ()Landroid/content/pm/IPackageManager;
    //   25: astore_2
    //   26: aload_2
    //   27: ifnull -> 88
    //   30: aload_2
    //   31: ldc 'android.hardware.nfc.hcef'
    //   33: iconst_0
    //   34: invokeinterface hasSystemFeature : (Ljava/lang/String;I)Z
    //   39: istore_3
    //   40: iload_3
    //   41: ifeq -> 51
    //   44: iconst_1
    //   45: putstatic android/nfc/cardemulation/NfcFCardEmulation.sIsInitialized : Z
    //   48: goto -> 106
    //   51: ldc 'NfcFCardEmulation'
    //   53: ldc 'This device does not support NFC-F card emulation'
    //   55: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   58: pop
    //   59: new java/lang/UnsupportedOperationException
    //   62: astore_0
    //   63: aload_0
    //   64: invokespecial <init> : ()V
    //   67: aload_0
    //   68: athrow
    //   69: astore_0
    //   70: ldc 'NfcFCardEmulation'
    //   72: ldc 'PackageManager query failed.'
    //   74: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   77: pop
    //   78: new java/lang/UnsupportedOperationException
    //   81: astore_0
    //   82: aload_0
    //   83: invokespecial <init> : ()V
    //   86: aload_0
    //   87: athrow
    //   88: ldc 'NfcFCardEmulation'
    //   90: ldc 'Cannot get PackageManager'
    //   92: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   95: pop
    //   96: new java/lang/UnsupportedOperationException
    //   99: astore_0
    //   100: aload_0
    //   101: invokespecial <init> : ()V
    //   104: aload_0
    //   105: athrow
    //   106: getstatic android/nfc/cardemulation/NfcFCardEmulation.sCardEmus : Ljava/util/HashMap;
    //   109: aload_1
    //   110: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   113: checkcast android/nfc/cardemulation/NfcFCardEmulation
    //   116: astore #4
    //   118: aload #4
    //   120: astore_2
    //   121: aload #4
    //   123: ifnonnull -> 175
    //   126: aload_0
    //   127: invokevirtual getNfcFCardEmulationService : ()Landroid/nfc/INfcFCardEmulation;
    //   130: astore_0
    //   131: aload_0
    //   132: ifnull -> 157
    //   135: new android/nfc/cardemulation/NfcFCardEmulation
    //   138: astore_2
    //   139: aload_2
    //   140: aload_1
    //   141: aload_0
    //   142: invokespecial <init> : (Landroid/content/Context;Landroid/nfc/INfcFCardEmulation;)V
    //   145: getstatic android/nfc/cardemulation/NfcFCardEmulation.sCardEmus : Ljava/util/HashMap;
    //   148: aload_1
    //   149: aload_2
    //   150: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   153: pop
    //   154: goto -> 175
    //   157: ldc 'NfcFCardEmulation'
    //   159: ldc 'This device does not implement the INfcFCardEmulation interface.'
    //   161: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   164: pop
    //   165: new java/lang/UnsupportedOperationException
    //   168: astore_0
    //   169: aload_0
    //   170: invokespecial <init> : ()V
    //   173: aload_0
    //   174: athrow
    //   175: ldc android/nfc/cardemulation/NfcFCardEmulation
    //   177: monitorexit
    //   178: aload_2
    //   179: areturn
    //   180: ldc 'NfcFCardEmulation'
    //   182: ldc 'NfcAdapter context is null.'
    //   184: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   187: pop
    //   188: new java/lang/UnsupportedOperationException
    //   191: astore_0
    //   192: aload_0
    //   193: invokespecial <init> : ()V
    //   196: aload_0
    //   197: athrow
    //   198: astore_0
    //   199: goto -> 214
    //   202: new java/lang/NullPointerException
    //   205: astore_0
    //   206: aload_0
    //   207: ldc 'NfcAdapter is null'
    //   209: invokespecial <init> : (Ljava/lang/String;)V
    //   212: aload_0
    //   213: athrow
    //   214: ldc android/nfc/cardemulation/NfcFCardEmulation
    //   216: monitorexit
    //   217: aload_0
    //   218: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #67	-> 3
    //   #68	-> 7
    //   #69	-> 12
    //   #73	-> 16
    //   #74	-> 22
    //   #75	-> 26
    //   #80	-> 30
    //   #87	-> 44
    //   #88	-> 44
    //   #81	-> 51
    //   #82	-> 59
    //   #84	-> 69
    //   #85	-> 70
    //   #86	-> 78
    //   #76	-> 88
    //   #77	-> 96
    //   #90	-> 106
    //   #91	-> 118
    //   #93	-> 126
    //   #94	-> 131
    //   #98	-> 135
    //   #99	-> 145
    //   #95	-> 157
    //   #96	-> 165
    //   #101	-> 175
    //   #70	-> 180
    //   #71	-> 188
    //   #66	-> 198
    //   #67	-> 202
    //   #66	-> 214
    // Exception table:
    //   from	to	target	type
    //   7	12	198	finally
    //   16	22	198	finally
    //   22	26	198	finally
    //   30	40	69	android/os/RemoteException
    //   30	40	198	finally
    //   44	48	198	finally
    //   51	59	69	android/os/RemoteException
    //   51	59	198	finally
    //   59	69	69	android/os/RemoteException
    //   59	69	198	finally
    //   70	78	198	finally
    //   78	88	198	finally
    //   88	96	198	finally
    //   96	106	198	finally
    //   106	118	198	finally
    //   126	131	198	finally
    //   135	145	198	finally
    //   145	154	198	finally
    //   157	165	198	finally
    //   165	175	198	finally
    //   180	188	198	finally
    //   188	198	198	finally
    //   202	214	198	finally
  }
  
  public String getSystemCodeForService(ComponentName paramComponentName) throws RuntimeException {
    if (paramComponentName != null)
      try {
        return sService.getSystemCodeForService(this.mContext.getUserId(), paramComponentName);
      } catch (RemoteException remoteException) {
        recoverService();
        INfcFCardEmulation iNfcFCardEmulation = sService;
        if (iNfcFCardEmulation == null) {
          Log.e("NfcFCardEmulation", "Failed to recover CardEmulationService.");
          return null;
        } 
        try {
          return iNfcFCardEmulation.getSystemCodeForService(this.mContext.getUserId(), paramComponentName);
        } catch (RemoteException remoteException1) {
          Log.e("NfcFCardEmulation", "Failed to reach CardEmulationService.");
          remoteException1.rethrowAsRuntimeException();
          return null;
        } 
      }  
    throw new NullPointerException("service is null");
  }
  
  public boolean registerSystemCodeForService(ComponentName paramComponentName, String paramString) throws RuntimeException {
    if (paramComponentName != null && paramString != null)
      try {
        return sService.registerSystemCodeForService(this.mContext.getUserId(), paramComponentName, paramString);
      } catch (RemoteException remoteException) {
        recoverService();
        INfcFCardEmulation iNfcFCardEmulation = sService;
        if (iNfcFCardEmulation == null) {
          Log.e("NfcFCardEmulation", "Failed to recover CardEmulationService.");
          return false;
        } 
        try {
          return iNfcFCardEmulation.registerSystemCodeForService(this.mContext.getUserId(), paramComponentName, paramString);
        } catch (RemoteException remoteException1) {
          Log.e("NfcFCardEmulation", "Failed to reach CardEmulationService.");
          remoteException1.rethrowAsRuntimeException();
          return false;
        } 
      }  
    throw new NullPointerException("service or systemCode is null");
  }
  
  public boolean unregisterSystemCodeForService(ComponentName paramComponentName) throws RuntimeException {
    if (paramComponentName != null)
      try {
        return sService.removeSystemCodeForService(this.mContext.getUserId(), paramComponentName);
      } catch (RemoteException remoteException) {
        recoverService();
        INfcFCardEmulation iNfcFCardEmulation = sService;
        if (iNfcFCardEmulation == null) {
          Log.e("NfcFCardEmulation", "Failed to recover CardEmulationService.");
          return false;
        } 
        try {
          return iNfcFCardEmulation.removeSystemCodeForService(this.mContext.getUserId(), paramComponentName);
        } catch (RemoteException remoteException1) {
          Log.e("NfcFCardEmulation", "Failed to reach CardEmulationService.");
          remoteException1.rethrowAsRuntimeException();
          return false;
        } 
      }  
    throw new NullPointerException("service is null");
  }
  
  public String getNfcid2ForService(ComponentName paramComponentName) throws RuntimeException {
    if (paramComponentName != null)
      try {
        return sService.getNfcid2ForService(this.mContext.getUserId(), paramComponentName);
      } catch (RemoteException remoteException) {
        recoverService();
        INfcFCardEmulation iNfcFCardEmulation = sService;
        if (iNfcFCardEmulation == null) {
          Log.e("NfcFCardEmulation", "Failed to recover CardEmulationService.");
          return null;
        } 
        try {
          return iNfcFCardEmulation.getNfcid2ForService(this.mContext.getUserId(), paramComponentName);
        } catch (RemoteException remoteException1) {
          Log.e("NfcFCardEmulation", "Failed to reach CardEmulationService.");
          remoteException1.rethrowAsRuntimeException();
          return null;
        } 
      }  
    throw new NullPointerException("service is null");
  }
  
  public boolean setNfcid2ForService(ComponentName paramComponentName, String paramString) throws RuntimeException {
    if (paramComponentName != null && paramString != null)
      try {
        return sService.setNfcid2ForService(this.mContext.getUserId(), paramComponentName, paramString);
      } catch (RemoteException remoteException) {
        recoverService();
        INfcFCardEmulation iNfcFCardEmulation = sService;
        if (iNfcFCardEmulation == null) {
          Log.e("NfcFCardEmulation", "Failed to recover CardEmulationService.");
          return false;
        } 
        try {
          return iNfcFCardEmulation.setNfcid2ForService(this.mContext.getUserId(), paramComponentName, paramString);
        } catch (RemoteException remoteException1) {
          Log.e("NfcFCardEmulation", "Failed to reach CardEmulationService.");
          remoteException1.rethrowAsRuntimeException();
          return false;
        } 
      }  
    throw new NullPointerException("service or nfcid2 is null");
  }
  
  public boolean enableService(Activity paramActivity, ComponentName paramComponentName) throws RuntimeException {
    if (paramActivity != null && paramComponentName != null) {
      if (paramActivity.isResumed())
        try {
          return sService.enableNfcFForegroundService(paramComponentName);
        } catch (RemoteException remoteException) {
          recoverService();
          INfcFCardEmulation iNfcFCardEmulation = sService;
          if (iNfcFCardEmulation == null) {
            Log.e("NfcFCardEmulation", "Failed to recover CardEmulationService.");
            return false;
          } 
          try {
            return iNfcFCardEmulation.enableNfcFForegroundService(paramComponentName);
          } catch (RemoteException remoteException1) {
            Log.e("NfcFCardEmulation", "Failed to reach CardEmulationService.");
            remoteException1.rethrowAsRuntimeException();
            return false;
          } 
        }  
      throw new IllegalArgumentException("Activity must be resumed.");
    } 
    throw new NullPointerException("activity or service is null");
  }
  
  public boolean disableService(Activity paramActivity) throws RuntimeException {
    if (paramActivity != null) {
      if (paramActivity.isResumed())
        try {
          return sService.disableNfcFForegroundService();
        } catch (RemoteException remoteException) {
          recoverService();
          INfcFCardEmulation iNfcFCardEmulation = sService;
          if (iNfcFCardEmulation == null) {
            Log.e("NfcFCardEmulation", "Failed to recover CardEmulationService.");
            return false;
          } 
          try {
            return iNfcFCardEmulation.disableNfcFForegroundService();
          } catch (RemoteException remoteException1) {
            Log.e("NfcFCardEmulation", "Failed to reach CardEmulationService.");
            remoteException1.rethrowAsRuntimeException();
            return false;
          } 
        }  
      throw new IllegalArgumentException("Activity must be resumed.");
    } 
    throw new NullPointerException("activity is null");
  }
  
  public List<NfcFServiceInfo> getNfcFServices() {
    try {
      return sService.getNfcFServices(this.mContext.getUserId());
    } catch (RemoteException remoteException) {
      recoverService();
      INfcFCardEmulation iNfcFCardEmulation = sService;
      if (iNfcFCardEmulation == null) {
        Log.e("NfcFCardEmulation", "Failed to recover CardEmulationService.");
        return null;
      } 
      try {
        return iNfcFCardEmulation.getNfcFServices(this.mContext.getUserId());
      } catch (RemoteException remoteException1) {
        Log.e("NfcFCardEmulation", "Failed to reach CardEmulationService.");
        return null;
      } 
    } 
  }
  
  public int getMaxNumOfRegisterableSystemCodes() {
    try {
      return sService.getMaxNumOfRegisterableSystemCodes();
    } catch (RemoteException remoteException) {
      recoverService();
      INfcFCardEmulation iNfcFCardEmulation = sService;
      if (iNfcFCardEmulation == null) {
        Log.e("NfcFCardEmulation", "Failed to recover CardEmulationService.");
        return -1;
      } 
      try {
        return iNfcFCardEmulation.getMaxNumOfRegisterableSystemCodes();
      } catch (RemoteException remoteException1) {
        Log.e("NfcFCardEmulation", "Failed to reach CardEmulationService.");
        return -1;
      } 
    } 
  }
  
  public static boolean isValidSystemCode(String paramString) {
    if (paramString == null)
      return false; 
    if (paramString.length() != 4) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("System Code ");
      stringBuilder.append(paramString);
      stringBuilder.append(" is not a valid System Code.");
      Log.e("NfcFCardEmulation", stringBuilder.toString());
      return false;
    } 
    if (!paramString.startsWith("4") || paramString.toUpperCase().endsWith("FF")) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("System Code ");
      stringBuilder.append(paramString);
      stringBuilder.append(" is not a valid System Code.");
      Log.e("NfcFCardEmulation", stringBuilder.toString());
      return false;
    } 
    try {
      Integer.parseInt(paramString, 16);
      return true;
    } catch (NumberFormatException numberFormatException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("System Code ");
      stringBuilder.append(paramString);
      stringBuilder.append(" is not a valid System Code.");
      Log.e("NfcFCardEmulation", stringBuilder.toString());
      return false;
    } 
  }
  
  public static boolean isValidNfcid2(String paramString) {
    if (paramString == null)
      return false; 
    if (paramString.length() != 16) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("NFCID2 ");
      stringBuilder.append(paramString);
      stringBuilder.append(" is not a valid NFCID2.");
      Log.e("NfcFCardEmulation", stringBuilder.toString());
      return false;
    } 
    if (!paramString.toUpperCase().startsWith("02FE")) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("NFCID2 ");
      stringBuilder.append(paramString);
      stringBuilder.append(" is not a valid NFCID2.");
      Log.e("NfcFCardEmulation", stringBuilder.toString());
      return false;
    } 
    try {
      Long.parseLong(paramString, 16);
      return true;
    } catch (NumberFormatException numberFormatException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("NFCID2 ");
      stringBuilder.append(paramString);
      stringBuilder.append(" is not a valid NFCID2.");
      Log.e("NfcFCardEmulation", stringBuilder.toString());
      return false;
    } 
  }
  
  void recoverService() {
    NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this.mContext);
    sService = nfcAdapter.getNfcFCardEmulationService();
  }
}
