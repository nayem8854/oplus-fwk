package android.nfc.cardemulation;

import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.proto.ProtoOutputStream;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.PrintWriter;
import org.xmlpull.v1.XmlPullParserException;

public final class NfcFServiceInfo implements Parcelable {
  public NfcFServiceInfo(ResolveInfo paramResolveInfo, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt, String paramString6) {
    this.mService = paramResolveInfo;
    this.mDescription = paramString1;
    this.mSystemCode = paramString2;
    this.mDynamicSystemCode = paramString3;
    this.mNfcid2 = paramString4;
    this.mDynamicNfcid2 = paramString5;
    this.mUid = paramInt;
    this.mT3tPmm = paramString6;
  }
  
  public NfcFServiceInfo(PackageManager paramPackageManager, ResolveInfo paramResolveInfo) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial <init> : ()V
    //   4: aload_2
    //   5: getfield serviceInfo : Landroid/content/pm/ServiceInfo;
    //   8: astore_3
    //   9: aconst_null
    //   10: astore #4
    //   12: aconst_null
    //   13: astore #5
    //   15: aload_3
    //   16: aload_1
    //   17: ldc 'android.nfc.cardemulation.host_nfcf_service'
    //   19: invokevirtual loadXmlMetaData : (Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;
    //   22: astore #6
    //   24: aload #6
    //   26: ifnull -> 992
    //   29: aload #6
    //   31: astore #5
    //   33: aload #6
    //   35: astore #4
    //   37: aload #6
    //   39: invokeinterface getEventType : ()I
    //   44: istore #7
    //   46: iload #7
    //   48: iconst_2
    //   49: if_icmpeq -> 78
    //   52: iload #7
    //   54: iconst_1
    //   55: if_icmpeq -> 78
    //   58: aload #6
    //   60: astore #5
    //   62: aload #6
    //   64: astore #4
    //   66: aload #6
    //   68: invokeinterface next : ()I
    //   73: istore #7
    //   75: goto -> 46
    //   78: aload #6
    //   80: astore #5
    //   82: aload #6
    //   84: astore #4
    //   86: aload #6
    //   88: invokeinterface getName : ()Ljava/lang/String;
    //   93: astore #8
    //   95: aload #6
    //   97: astore #5
    //   99: aload #6
    //   101: astore #4
    //   103: ldc 'host-nfcf-service'
    //   105: aload #8
    //   107: invokevirtual equals : (Ljava/lang/Object;)Z
    //   110: ifeq -> 956
    //   113: aload #6
    //   115: astore #5
    //   117: aload #6
    //   119: astore #4
    //   121: aload_1
    //   122: aload_3
    //   123: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   126: invokevirtual getResourcesForApplication : (Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
    //   129: astore #9
    //   131: aload #6
    //   133: astore #5
    //   135: aload #6
    //   137: astore #4
    //   139: aload #6
    //   141: invokestatic asAttributeSet : (Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
    //   144: astore #10
    //   146: aload #6
    //   148: astore #5
    //   150: aload #6
    //   152: astore #4
    //   154: aload #9
    //   156: aload #10
    //   158: getstatic com/android/internal/R$styleable.HostNfcFService : [I
    //   161: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   164: astore_1
    //   165: aload #6
    //   167: astore #5
    //   169: aload #6
    //   171: astore #4
    //   173: aload_0
    //   174: aload_2
    //   175: putfield mService : Landroid/content/pm/ResolveInfo;
    //   178: aload #6
    //   180: astore #5
    //   182: aload #6
    //   184: astore #4
    //   186: aload_0
    //   187: aload_1
    //   188: iconst_0
    //   189: invokevirtual getString : (I)Ljava/lang/String;
    //   192: putfield mDescription : Ljava/lang/String;
    //   195: aload #6
    //   197: astore #5
    //   199: aload #6
    //   201: astore #4
    //   203: aload_0
    //   204: aconst_null
    //   205: putfield mDynamicSystemCode : Ljava/lang/String;
    //   208: aload #6
    //   210: astore #5
    //   212: aload #6
    //   214: astore #4
    //   216: aload_0
    //   217: aconst_null
    //   218: putfield mDynamicNfcid2 : Ljava/lang/String;
    //   221: aload #6
    //   223: astore #5
    //   225: aload #6
    //   227: astore #4
    //   229: aload_1
    //   230: invokevirtual recycle : ()V
    //   233: aconst_null
    //   234: astore #8
    //   236: aconst_null
    //   237: astore_2
    //   238: aconst_null
    //   239: astore_1
    //   240: aload #6
    //   242: astore #5
    //   244: aload #6
    //   246: astore #4
    //   248: aload #6
    //   250: invokeinterface getDepth : ()I
    //   255: istore #7
    //   257: aload #6
    //   259: astore #5
    //   261: aload #6
    //   263: astore #4
    //   265: aload #6
    //   267: invokeinterface next : ()I
    //   272: istore #11
    //   274: ldc 'NULL'
    //   276: astore #12
    //   278: iload #11
    //   280: iconst_3
    //   281: if_icmpne -> 307
    //   284: aload #6
    //   286: astore #5
    //   288: aload #6
    //   290: astore #4
    //   292: aload #6
    //   294: invokeinterface getDepth : ()I
    //   299: iload #7
    //   301: if_icmple -> 860
    //   304: goto -> 307
    //   307: iload #11
    //   309: iconst_1
    //   310: if_icmpeq -> 860
    //   313: aload #6
    //   315: astore #5
    //   317: aload #6
    //   319: astore #4
    //   321: aload #6
    //   323: invokeinterface getName : ()Ljava/lang/String;
    //   328: astore #12
    //   330: iload #11
    //   332: iconst_2
    //   333: if_icmpne -> 543
    //   336: aload #6
    //   338: astore #5
    //   340: aload #6
    //   342: astore #4
    //   344: ldc 'system-code-filter'
    //   346: aload #12
    //   348: invokevirtual equals : (Ljava/lang/Object;)Z
    //   351: ifeq -> 543
    //   354: aload #8
    //   356: ifnonnull -> 543
    //   359: aload #6
    //   361: astore #5
    //   363: aload #6
    //   365: astore #4
    //   367: aload #9
    //   369: aload #10
    //   371: getstatic com/android/internal/R$styleable.SystemCodeFilter : [I
    //   374: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   377: astore #12
    //   379: aload #6
    //   381: astore #5
    //   383: aload #6
    //   385: astore #4
    //   387: aload #12
    //   389: iconst_0
    //   390: invokevirtual getString : (I)Ljava/lang/String;
    //   393: astore #8
    //   395: aload #6
    //   397: astore #5
    //   399: aload #6
    //   401: astore #4
    //   403: aload #8
    //   405: invokevirtual toUpperCase : ()Ljava/lang/String;
    //   408: astore #8
    //   410: aload #6
    //   412: astore #5
    //   414: aload #6
    //   416: astore #4
    //   418: aload #8
    //   420: invokestatic isValidSystemCode : (Ljava/lang/String;)Z
    //   423: ifne -> 527
    //   426: aload #6
    //   428: astore #5
    //   430: aload #6
    //   432: astore #4
    //   434: aload #8
    //   436: ldc 'NULL'
    //   438: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   441: ifne -> 527
    //   444: aload #6
    //   446: astore #5
    //   448: aload #6
    //   450: astore #4
    //   452: new java/lang/StringBuilder
    //   455: astore #13
    //   457: aload #6
    //   459: astore #5
    //   461: aload #6
    //   463: astore #4
    //   465: aload #13
    //   467: invokespecial <init> : ()V
    //   470: aload #6
    //   472: astore #5
    //   474: aload #6
    //   476: astore #4
    //   478: aload #13
    //   480: ldc 'Invalid System Code: '
    //   482: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   485: pop
    //   486: aload #6
    //   488: astore #5
    //   490: aload #6
    //   492: astore #4
    //   494: aload #13
    //   496: aload #8
    //   498: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   501: pop
    //   502: aload #6
    //   504: astore #5
    //   506: aload #6
    //   508: astore #4
    //   510: ldc 'NfcFServiceInfo'
    //   512: aload #13
    //   514: invokevirtual toString : ()Ljava/lang/String;
    //   517: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   520: pop
    //   521: aconst_null
    //   522: astore #8
    //   524: goto -> 527
    //   527: aload #6
    //   529: astore #5
    //   531: aload #6
    //   533: astore #4
    //   535: aload #12
    //   537: invokevirtual recycle : ()V
    //   540: goto -> 257
    //   543: iload #11
    //   545: iconst_2
    //   546: if_icmpne -> 765
    //   549: aload #6
    //   551: astore #5
    //   553: aload #6
    //   555: astore #4
    //   557: ldc 'nfcid2-filter'
    //   559: aload #12
    //   561: invokevirtual equals : (Ljava/lang/Object;)Z
    //   564: ifeq -> 765
    //   567: aload_2
    //   568: ifnonnull -> 765
    //   571: aload #6
    //   573: astore #5
    //   575: aload #6
    //   577: astore #4
    //   579: aload #9
    //   581: aload #10
    //   583: getstatic com/android/internal/R$styleable.Nfcid2Filter : [I
    //   586: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   589: astore #12
    //   591: aload #6
    //   593: astore #5
    //   595: aload #6
    //   597: astore #4
    //   599: aload #12
    //   601: iconst_0
    //   602: invokevirtual getString : (I)Ljava/lang/String;
    //   605: astore_2
    //   606: aload #6
    //   608: astore #5
    //   610: aload #6
    //   612: astore #4
    //   614: aload_2
    //   615: invokevirtual toUpperCase : ()Ljava/lang/String;
    //   618: astore_2
    //   619: aload #6
    //   621: astore #5
    //   623: aload #6
    //   625: astore #4
    //   627: aload_2
    //   628: ldc 'RANDOM'
    //   630: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   633: ifne -> 749
    //   636: aload #6
    //   638: astore #5
    //   640: aload #6
    //   642: astore #4
    //   644: aload_2
    //   645: ldc 'NULL'
    //   647: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   650: ifne -> 749
    //   653: aload #6
    //   655: astore #5
    //   657: aload #6
    //   659: astore #4
    //   661: aload_2
    //   662: invokestatic isValidNfcid2 : (Ljava/lang/String;)Z
    //   665: ifne -> 749
    //   668: aload #6
    //   670: astore #5
    //   672: aload #6
    //   674: astore #4
    //   676: new java/lang/StringBuilder
    //   679: astore #13
    //   681: aload #6
    //   683: astore #5
    //   685: aload #6
    //   687: astore #4
    //   689: aload #13
    //   691: invokespecial <init> : ()V
    //   694: aload #6
    //   696: astore #5
    //   698: aload #6
    //   700: astore #4
    //   702: aload #13
    //   704: ldc 'Invalid NFCID2: '
    //   706: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   709: pop
    //   710: aload #6
    //   712: astore #5
    //   714: aload #6
    //   716: astore #4
    //   718: aload #13
    //   720: aload_2
    //   721: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   724: pop
    //   725: aload #6
    //   727: astore #5
    //   729: aload #6
    //   731: astore #4
    //   733: ldc 'NfcFServiceInfo'
    //   735: aload #13
    //   737: invokevirtual toString : ()Ljava/lang/String;
    //   740: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   743: pop
    //   744: aconst_null
    //   745: astore_2
    //   746: goto -> 749
    //   749: aload #6
    //   751: astore #5
    //   753: aload #6
    //   755: astore #4
    //   757: aload #12
    //   759: invokevirtual recycle : ()V
    //   762: goto -> 257
    //   765: iload #11
    //   767: iconst_2
    //   768: if_icmpne -> 857
    //   771: aload #6
    //   773: astore #5
    //   775: aload #6
    //   777: astore #4
    //   779: aload #12
    //   781: ldc 't3tPmm-filter'
    //   783: invokevirtual equals : (Ljava/lang/Object;)Z
    //   786: ifeq -> 857
    //   789: aload_1
    //   790: ifnonnull -> 857
    //   793: aload #6
    //   795: astore #5
    //   797: aload #6
    //   799: astore #4
    //   801: aload #9
    //   803: aload #10
    //   805: getstatic com/android/internal/R$styleable.T3tPmmFilter : [I
    //   808: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   811: astore #12
    //   813: aload #6
    //   815: astore #5
    //   817: aload #6
    //   819: astore #4
    //   821: aload #12
    //   823: iconst_0
    //   824: invokevirtual getString : (I)Ljava/lang/String;
    //   827: astore_1
    //   828: aload #6
    //   830: astore #5
    //   832: aload #6
    //   834: astore #4
    //   836: aload_1
    //   837: invokevirtual toUpperCase : ()Ljava/lang/String;
    //   840: astore_1
    //   841: aload #6
    //   843: astore #5
    //   845: aload #6
    //   847: astore #4
    //   849: aload #12
    //   851: invokevirtual recycle : ()V
    //   854: goto -> 257
    //   857: goto -> 257
    //   860: aload #8
    //   862: ifnonnull -> 872
    //   865: ldc 'NULL'
    //   867: astore #8
    //   869: goto -> 872
    //   872: aload #6
    //   874: astore #5
    //   876: aload #6
    //   878: astore #4
    //   880: aload_0
    //   881: aload #8
    //   883: putfield mSystemCode : Ljava/lang/String;
    //   886: aload_2
    //   887: ifnonnull -> 896
    //   890: aload #12
    //   892: astore_2
    //   893: goto -> 896
    //   896: aload #6
    //   898: astore #5
    //   900: aload #6
    //   902: astore #4
    //   904: aload_0
    //   905: aload_2
    //   906: putfield mNfcid2 : Ljava/lang/String;
    //   909: aload_1
    //   910: ifnonnull -> 919
    //   913: ldc 'FFFFFFFFFFFFFFFF'
    //   915: astore_1
    //   916: goto -> 919
    //   919: aload #6
    //   921: astore #5
    //   923: aload #6
    //   925: astore #4
    //   927: aload_0
    //   928: aload_1
    //   929: putfield mT3tPmm : Ljava/lang/String;
    //   932: aload #6
    //   934: ifnull -> 944
    //   937: aload #6
    //   939: invokeinterface close : ()V
    //   944: aload_0
    //   945: aload_3
    //   946: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   949: getfield uid : I
    //   952: putfield mUid : I
    //   955: return
    //   956: aload #6
    //   958: astore #5
    //   960: aload #6
    //   962: astore #4
    //   964: new org/xmlpull/v1/XmlPullParserException
    //   967: astore_1
    //   968: aload #6
    //   970: astore #5
    //   972: aload #6
    //   974: astore #4
    //   976: aload_1
    //   977: ldc 'Meta-data does not start with <host-nfcf-service> tag'
    //   979: invokespecial <init> : (Ljava/lang/String;)V
    //   982: aload #6
    //   984: astore #5
    //   986: aload #6
    //   988: astore #4
    //   990: aload_1
    //   991: athrow
    //   992: aload #6
    //   994: astore #5
    //   996: aload #6
    //   998: astore #4
    //   1000: new org/xmlpull/v1/XmlPullParserException
    //   1003: astore_1
    //   1004: aload #6
    //   1006: astore #5
    //   1008: aload #6
    //   1010: astore #4
    //   1012: aload_1
    //   1013: ldc 'No android.nfc.cardemulation.host_nfcf_service meta-data'
    //   1015: invokespecial <init> : (Ljava/lang/String;)V
    //   1018: aload #6
    //   1020: astore #5
    //   1022: aload #6
    //   1024: astore #4
    //   1026: aload_1
    //   1027: athrow
    //   1028: astore_1
    //   1029: goto -> 1099
    //   1032: astore_1
    //   1033: aload #4
    //   1035: astore #5
    //   1037: new org/xmlpull/v1/XmlPullParserException
    //   1040: astore_2
    //   1041: aload #4
    //   1043: astore #5
    //   1045: new java/lang/StringBuilder
    //   1048: astore_1
    //   1049: aload #4
    //   1051: astore #5
    //   1053: aload_1
    //   1054: invokespecial <init> : ()V
    //   1057: aload #4
    //   1059: astore #5
    //   1061: aload_1
    //   1062: ldc 'Unable to create context for: '
    //   1064: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1067: pop
    //   1068: aload #4
    //   1070: astore #5
    //   1072: aload_1
    //   1073: aload_3
    //   1074: getfield packageName : Ljava/lang/String;
    //   1077: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1080: pop
    //   1081: aload #4
    //   1083: astore #5
    //   1085: aload_2
    //   1086: aload_1
    //   1087: invokevirtual toString : ()Ljava/lang/String;
    //   1090: invokespecial <init> : (Ljava/lang/String;)V
    //   1093: aload #4
    //   1095: astore #5
    //   1097: aload_2
    //   1098: athrow
    //   1099: aload #5
    //   1101: ifnull -> 1111
    //   1104: aload #5
    //   1106: invokeinterface close : ()V
    //   1111: aload_1
    //   1112: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #107	-> 0
    //   #108	-> 4
    //   #109	-> 9
    //   #111	-> 15
    //   #112	-> 24
    //   #117	-> 29
    //   #118	-> 46
    //   #120	-> 58
    //   #123	-> 78
    //   #124	-> 95
    //   #129	-> 113
    //   #130	-> 131
    //   #131	-> 146
    //   #133	-> 165
    //   #134	-> 178
    //   #136	-> 195
    //   #137	-> 208
    //   #138	-> 221
    //   #140	-> 233
    //   #141	-> 236
    //   #142	-> 238
    //   #143	-> 240
    //   #145	-> 257
    //   #146	-> 284
    //   #145	-> 307
    //   #146	-> 307
    //   #147	-> 313
    //   #148	-> 330
    //   #149	-> 336
    //   #150	-> 359
    //   #152	-> 379
    //   #153	-> 395
    //   #154	-> 410
    //   #155	-> 426
    //   #156	-> 444
    //   #157	-> 521
    //   #159	-> 527
    //   #160	-> 540
    //   #161	-> 549
    //   #162	-> 571
    //   #164	-> 591
    //   #165	-> 606
    //   #166	-> 619
    //   #167	-> 636
    //   #168	-> 653
    //   #169	-> 668
    //   #170	-> 744
    //   #172	-> 749
    //   #173	-> 762
    //   #175	-> 793
    //   #177	-> 813
    //   #178	-> 828
    //   #179	-> 841
    //   #180	-> 854
    //   #173	-> 857
    //   #145	-> 857
    //   #182	-> 860
    //   #183	-> 886
    //   #184	-> 909
    //   #188	-> 932
    //   #191	-> 944
    //   #192	-> 955
    //   #125	-> 956
    //   #113	-> 992
    //   #188	-> 1028
    //   #185	-> 1032
    //   #186	-> 1033
    //   #188	-> 1099
    //   #189	-> 1111
    // Exception table:
    //   from	to	target	type
    //   15	24	1032	android/content/pm/PackageManager$NameNotFoundException
    //   15	24	1028	finally
    //   37	46	1032	android/content/pm/PackageManager$NameNotFoundException
    //   37	46	1028	finally
    //   66	75	1032	android/content/pm/PackageManager$NameNotFoundException
    //   66	75	1028	finally
    //   86	95	1032	android/content/pm/PackageManager$NameNotFoundException
    //   86	95	1028	finally
    //   103	113	1032	android/content/pm/PackageManager$NameNotFoundException
    //   103	113	1028	finally
    //   121	131	1032	android/content/pm/PackageManager$NameNotFoundException
    //   121	131	1028	finally
    //   139	146	1032	android/content/pm/PackageManager$NameNotFoundException
    //   139	146	1028	finally
    //   154	165	1032	android/content/pm/PackageManager$NameNotFoundException
    //   154	165	1028	finally
    //   173	178	1032	android/content/pm/PackageManager$NameNotFoundException
    //   173	178	1028	finally
    //   186	195	1032	android/content/pm/PackageManager$NameNotFoundException
    //   186	195	1028	finally
    //   203	208	1032	android/content/pm/PackageManager$NameNotFoundException
    //   203	208	1028	finally
    //   216	221	1032	android/content/pm/PackageManager$NameNotFoundException
    //   216	221	1028	finally
    //   229	233	1032	android/content/pm/PackageManager$NameNotFoundException
    //   229	233	1028	finally
    //   248	257	1032	android/content/pm/PackageManager$NameNotFoundException
    //   248	257	1028	finally
    //   265	274	1032	android/content/pm/PackageManager$NameNotFoundException
    //   265	274	1028	finally
    //   292	304	1032	android/content/pm/PackageManager$NameNotFoundException
    //   292	304	1028	finally
    //   321	330	1032	android/content/pm/PackageManager$NameNotFoundException
    //   321	330	1028	finally
    //   344	354	1032	android/content/pm/PackageManager$NameNotFoundException
    //   344	354	1028	finally
    //   367	379	1032	android/content/pm/PackageManager$NameNotFoundException
    //   367	379	1028	finally
    //   387	395	1032	android/content/pm/PackageManager$NameNotFoundException
    //   387	395	1028	finally
    //   403	410	1032	android/content/pm/PackageManager$NameNotFoundException
    //   403	410	1028	finally
    //   418	426	1032	android/content/pm/PackageManager$NameNotFoundException
    //   418	426	1028	finally
    //   434	444	1032	android/content/pm/PackageManager$NameNotFoundException
    //   434	444	1028	finally
    //   452	457	1032	android/content/pm/PackageManager$NameNotFoundException
    //   452	457	1028	finally
    //   465	470	1032	android/content/pm/PackageManager$NameNotFoundException
    //   465	470	1028	finally
    //   478	486	1032	android/content/pm/PackageManager$NameNotFoundException
    //   478	486	1028	finally
    //   494	502	1032	android/content/pm/PackageManager$NameNotFoundException
    //   494	502	1028	finally
    //   510	521	1032	android/content/pm/PackageManager$NameNotFoundException
    //   510	521	1028	finally
    //   535	540	1032	android/content/pm/PackageManager$NameNotFoundException
    //   535	540	1028	finally
    //   557	567	1032	android/content/pm/PackageManager$NameNotFoundException
    //   557	567	1028	finally
    //   579	591	1032	android/content/pm/PackageManager$NameNotFoundException
    //   579	591	1028	finally
    //   599	606	1032	android/content/pm/PackageManager$NameNotFoundException
    //   599	606	1028	finally
    //   614	619	1032	android/content/pm/PackageManager$NameNotFoundException
    //   614	619	1028	finally
    //   627	636	1032	android/content/pm/PackageManager$NameNotFoundException
    //   627	636	1028	finally
    //   644	653	1032	android/content/pm/PackageManager$NameNotFoundException
    //   644	653	1028	finally
    //   661	668	1032	android/content/pm/PackageManager$NameNotFoundException
    //   661	668	1028	finally
    //   676	681	1032	android/content/pm/PackageManager$NameNotFoundException
    //   676	681	1028	finally
    //   689	694	1032	android/content/pm/PackageManager$NameNotFoundException
    //   689	694	1028	finally
    //   702	710	1032	android/content/pm/PackageManager$NameNotFoundException
    //   702	710	1028	finally
    //   718	725	1032	android/content/pm/PackageManager$NameNotFoundException
    //   718	725	1028	finally
    //   733	744	1032	android/content/pm/PackageManager$NameNotFoundException
    //   733	744	1028	finally
    //   757	762	1032	android/content/pm/PackageManager$NameNotFoundException
    //   757	762	1028	finally
    //   779	789	1032	android/content/pm/PackageManager$NameNotFoundException
    //   779	789	1028	finally
    //   801	813	1032	android/content/pm/PackageManager$NameNotFoundException
    //   801	813	1028	finally
    //   821	828	1032	android/content/pm/PackageManager$NameNotFoundException
    //   821	828	1028	finally
    //   836	841	1032	android/content/pm/PackageManager$NameNotFoundException
    //   836	841	1028	finally
    //   849	854	1032	android/content/pm/PackageManager$NameNotFoundException
    //   849	854	1028	finally
    //   880	886	1032	android/content/pm/PackageManager$NameNotFoundException
    //   880	886	1028	finally
    //   904	909	1032	android/content/pm/PackageManager$NameNotFoundException
    //   904	909	1028	finally
    //   927	932	1032	android/content/pm/PackageManager$NameNotFoundException
    //   927	932	1028	finally
    //   964	968	1032	android/content/pm/PackageManager$NameNotFoundException
    //   964	968	1028	finally
    //   976	982	1032	android/content/pm/PackageManager$NameNotFoundException
    //   976	982	1028	finally
    //   990	992	1032	android/content/pm/PackageManager$NameNotFoundException
    //   990	992	1028	finally
    //   1000	1004	1032	android/content/pm/PackageManager$NameNotFoundException
    //   1000	1004	1028	finally
    //   1012	1018	1032	android/content/pm/PackageManager$NameNotFoundException
    //   1012	1018	1028	finally
    //   1026	1028	1032	android/content/pm/PackageManager$NameNotFoundException
    //   1026	1028	1028	finally
    //   1037	1041	1028	finally
    //   1045	1049	1028	finally
    //   1053	1057	1028	finally
    //   1061	1068	1028	finally
    //   1072	1081	1028	finally
    //   1085	1093	1028	finally
    //   1097	1099	1028	finally
  }
  
  public ComponentName getComponent() {
    return new ComponentName(this.mService.serviceInfo.packageName, this.mService.serviceInfo.name);
  }
  
  public String getSystemCode() {
    String str1 = this.mDynamicSystemCode, str2 = str1;
    if (str1 == null)
      str2 = this.mSystemCode; 
    return str2;
  }
  
  public void setOrReplaceDynamicSystemCode(String paramString) {
    this.mDynamicSystemCode = paramString;
  }
  
  public String getNfcid2() {
    String str1 = this.mDynamicNfcid2, str2 = str1;
    if (str1 == null)
      str2 = this.mNfcid2; 
    return str2;
  }
  
  public void setOrReplaceDynamicNfcid2(String paramString) {
    this.mDynamicNfcid2 = paramString;
  }
  
  public String getDescription() {
    return this.mDescription;
  }
  
  public int getUid() {
    return this.mUid;
  }
  
  public String getT3tPmm() {
    return this.mT3tPmm;
  }
  
  public CharSequence loadLabel(PackageManager paramPackageManager) {
    return this.mService.loadLabel(paramPackageManager);
  }
  
  public Drawable loadIcon(PackageManager paramPackageManager) {
    return this.mService.loadIcon(paramPackageManager);
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder("NfcFService: ");
    stringBuilder1.append(getComponent());
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", description: ");
    stringBuilder2.append(this.mDescription);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", System Code: ");
    stringBuilder2.append(this.mSystemCode);
    stringBuilder1.append(stringBuilder2.toString());
    if (this.mDynamicSystemCode != null) {
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append(", dynamic System Code: ");
      stringBuilder2.append(this.mDynamicSystemCode);
      stringBuilder1.append(stringBuilder2.toString());
    } 
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", NFCID2: ");
    stringBuilder2.append(this.mNfcid2);
    stringBuilder1.append(stringBuilder2.toString());
    if (this.mDynamicNfcid2 != null) {
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append(", dynamic NFCID2: ");
      stringBuilder2.append(this.mDynamicNfcid2);
      stringBuilder1.append(stringBuilder2.toString());
    } 
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", T3T PMM:");
    stringBuilder2.append(this.mT3tPmm);
    stringBuilder1.append(stringBuilder2.toString());
    return stringBuilder1.toString();
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof NfcFServiceInfo))
      return false; 
    paramObject = paramObject;
    if (!paramObject.getComponent().equals(getComponent()))
      return false; 
    if (!((NfcFServiceInfo)paramObject).mSystemCode.equalsIgnoreCase(this.mSystemCode))
      return false; 
    if (!((NfcFServiceInfo)paramObject).mNfcid2.equalsIgnoreCase(this.mNfcid2))
      return false; 
    if (!((NfcFServiceInfo)paramObject).mT3tPmm.equalsIgnoreCase(this.mT3tPmm))
      return false; 
    return true;
  }
  
  public int hashCode() {
    return getComponent().hashCode();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.mService.writeToParcel(paramParcel, paramInt);
    paramParcel.writeString(this.mDescription);
    paramParcel.writeString(this.mSystemCode);
    String str = this.mDynamicSystemCode;
    boolean bool = true;
    if (str != null) {
      paramInt = 1;
    } else {
      paramInt = 0;
    } 
    paramParcel.writeInt(paramInt);
    str = this.mDynamicSystemCode;
    if (str != null)
      paramParcel.writeString(str); 
    paramParcel.writeString(this.mNfcid2);
    if (this.mDynamicNfcid2 != null) {
      paramInt = bool;
    } else {
      paramInt = 0;
    } 
    paramParcel.writeInt(paramInt);
    str = this.mDynamicNfcid2;
    if (str != null)
      paramParcel.writeString(str); 
    paramParcel.writeInt(this.mUid);
    paramParcel.writeString(this.mT3tPmm);
  }
  
  public static final Parcelable.Creator<NfcFServiceInfo> CREATOR = new Parcelable.Creator<NfcFServiceInfo>() {
      public NfcFServiceInfo createFromParcel(Parcel param1Parcel) {
        String str4, str6;
        ResolveInfo resolveInfo = ResolveInfo.CREATOR.createFromParcel(param1Parcel);
        String str2 = param1Parcel.readString();
        String str3 = param1Parcel.readString();
        if (param1Parcel.readInt() != 0) {
          str4 = param1Parcel.readString();
        } else {
          str4 = null;
        } 
        String str5 = param1Parcel.readString();
        if (param1Parcel.readInt() != 0) {
          str6 = param1Parcel.readString();
        } else {
          str6 = null;
        } 
        int i = param1Parcel.readInt();
        String str1 = param1Parcel.readString();
        return new NfcFServiceInfo(resolveInfo, str2, str3, str4, str5, str6, i, str1);
      }
      
      public NfcFServiceInfo[] newArray(int param1Int) {
        return new NfcFServiceInfo[param1Int];
      }
    };
  
  private static final String DEFAULT_T3T_PMM = "FFFFFFFFFFFFFFFF";
  
  static final String TAG = "NfcFServiceInfo";
  
  final String mDescription;
  
  String mDynamicNfcid2;
  
  String mDynamicSystemCode;
  
  final String mNfcid2;
  
  final ResolveInfo mService;
  
  final String mSystemCode;
  
  final String mT3tPmm;
  
  final int mUid;
  
  public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("    ");
    stringBuilder2.append(getComponent());
    stringBuilder2.append(" (Description: ");
    stringBuilder2.append(getDescription());
    stringBuilder2.append(")");
    String str = stringBuilder2.toString();
    paramPrintWriter.println(str);
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("    System Code: ");
    stringBuilder1.append(getSystemCode());
    paramPrintWriter.println(stringBuilder1.toString());
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append("    NFCID2: ");
    stringBuilder1.append(getNfcid2());
    paramPrintWriter.println(stringBuilder1.toString());
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append("    T3tPmm: ");
    stringBuilder1.append(getT3tPmm());
    paramPrintWriter.println(stringBuilder1.toString());
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream) {
    getComponent().dumpDebug(paramProtoOutputStream, 1146756268033L);
    paramProtoOutputStream.write(1138166333442L, getDescription());
    paramProtoOutputStream.write(1138166333443L, getSystemCode());
    paramProtoOutputStream.write(1138166333444L, getNfcid2());
    paramProtoOutputStream.write(1138166333445L, getT3tPmm());
  }
}
