package android.nfc.cardemulation;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.nfc.INfcCardEmulation;
import android.nfc.NfcAdapter;
import android.os.RemoteException;
import android.provider.Settings;
import android.util.Log;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

public final class CardEmulation implements IOplusCardEmulationEx {
  public static final String ACTION_CHANGE_DEFAULT = "android.nfc.cardemulation.action.ACTION_CHANGE_DEFAULT";
  
  private static final Pattern AID_PATTERN = Pattern.compile("[0-9A-Fa-f]{10,32}\\*?\\#?");
  
  public static final String CATEGORY_OTHER = "other";
  
  public static final String CATEGORY_PAYMENT = "payment";
  
  public static final String EXTRA_CATEGORY = "category";
  
  public static final String EXTRA_SERVICE_COMPONENT = "component";
  
  public static final int SELECTION_MODE_ALWAYS_ASK = 1;
  
  public static final int SELECTION_MODE_ASK_IF_CONFLICT = 2;
  
  public static final int SELECTION_MODE_PREFER_DEFAULT = 0;
  
  static final String TAG = "CardEmulation";
  
  static HashMap<Context, CardEmulation> sCardEmus;
  
  static boolean sIsInitialized = false;
  
  static INfcCardEmulation sService;
  
  final Context mContext;
  
  static {
    sCardEmus = new HashMap<>();
  }
  
  private CardEmulation(Context paramContext, INfcCardEmulation paramINfcCardEmulation) {
    this.mContext = paramContext.getApplicationContext();
    sService = paramINfcCardEmulation;
  }
  
  public static CardEmulation getInstance(NfcAdapter paramNfcAdapter) {
    // Byte code:
    //   0: ldc android/nfc/cardemulation/CardEmulation
    //   2: monitorenter
    //   3: aload_0
    //   4: ifnull -> 202
    //   7: aload_0
    //   8: invokevirtual getContext : ()Landroid/content/Context;
    //   11: astore_1
    //   12: aload_1
    //   13: ifnull -> 180
    //   16: getstatic android/nfc/cardemulation/CardEmulation.sIsInitialized : Z
    //   19: ifne -> 106
    //   22: invokestatic getPackageManager : ()Landroid/content/pm/IPackageManager;
    //   25: astore_2
    //   26: aload_2
    //   27: ifnull -> 88
    //   30: aload_2
    //   31: ldc 'android.hardware.nfc.hce'
    //   33: iconst_0
    //   34: invokeinterface hasSystemFeature : (Ljava/lang/String;I)Z
    //   39: istore_3
    //   40: iload_3
    //   41: ifeq -> 51
    //   44: iconst_1
    //   45: putstatic android/nfc/cardemulation/CardEmulation.sIsInitialized : Z
    //   48: goto -> 106
    //   51: ldc 'CardEmulation'
    //   53: ldc 'This device does not support card emulation'
    //   55: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   58: pop
    //   59: new java/lang/UnsupportedOperationException
    //   62: astore_0
    //   63: aload_0
    //   64: invokespecial <init> : ()V
    //   67: aload_0
    //   68: athrow
    //   69: astore_0
    //   70: ldc 'CardEmulation'
    //   72: ldc 'PackageManager query failed.'
    //   74: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   77: pop
    //   78: new java/lang/UnsupportedOperationException
    //   81: astore_0
    //   82: aload_0
    //   83: invokespecial <init> : ()V
    //   86: aload_0
    //   87: athrow
    //   88: ldc 'CardEmulation'
    //   90: ldc 'Cannot get PackageManager'
    //   92: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   95: pop
    //   96: new java/lang/UnsupportedOperationException
    //   99: astore_0
    //   100: aload_0
    //   101: invokespecial <init> : ()V
    //   104: aload_0
    //   105: athrow
    //   106: getstatic android/nfc/cardemulation/CardEmulation.sCardEmus : Ljava/util/HashMap;
    //   109: aload_1
    //   110: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   113: checkcast android/nfc/cardemulation/CardEmulation
    //   116: astore #4
    //   118: aload #4
    //   120: astore_2
    //   121: aload #4
    //   123: ifnonnull -> 175
    //   126: aload_0
    //   127: invokevirtual getCardEmulationService : ()Landroid/nfc/INfcCardEmulation;
    //   130: astore_0
    //   131: aload_0
    //   132: ifnull -> 157
    //   135: new android/nfc/cardemulation/CardEmulation
    //   138: astore_2
    //   139: aload_2
    //   140: aload_1
    //   141: aload_0
    //   142: invokespecial <init> : (Landroid/content/Context;Landroid/nfc/INfcCardEmulation;)V
    //   145: getstatic android/nfc/cardemulation/CardEmulation.sCardEmus : Ljava/util/HashMap;
    //   148: aload_1
    //   149: aload_2
    //   150: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   153: pop
    //   154: goto -> 175
    //   157: ldc 'CardEmulation'
    //   159: ldc 'This device does not implement the INfcCardEmulation interface.'
    //   161: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   164: pop
    //   165: new java/lang/UnsupportedOperationException
    //   168: astore_0
    //   169: aload_0
    //   170: invokespecial <init> : ()V
    //   173: aload_0
    //   174: athrow
    //   175: ldc android/nfc/cardemulation/CardEmulation
    //   177: monitorexit
    //   178: aload_2
    //   179: areturn
    //   180: ldc 'CardEmulation'
    //   182: ldc 'NfcAdapter context is null.'
    //   184: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   187: pop
    //   188: new java/lang/UnsupportedOperationException
    //   191: astore_0
    //   192: aload_0
    //   193: invokespecial <init> : ()V
    //   196: aload_0
    //   197: athrow
    //   198: astore_0
    //   199: goto -> 214
    //   202: new java/lang/NullPointerException
    //   205: astore_0
    //   206: aload_0
    //   207: ldc 'NfcAdapter is null'
    //   209: invokespecial <init> : (Ljava/lang/String;)V
    //   212: aload_0
    //   213: athrow
    //   214: ldc android/nfc/cardemulation/CardEmulation
    //   216: monitorexit
    //   217: aload_0
    //   218: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #155	-> 3
    //   #156	-> 7
    //   #157	-> 12
    //   #161	-> 16
    //   #162	-> 22
    //   #163	-> 26
    //   #168	-> 30
    //   #175	-> 44
    //   #176	-> 44
    //   #169	-> 51
    //   #170	-> 59
    //   #172	-> 69
    //   #173	-> 70
    //   #174	-> 78
    //   #164	-> 88
    //   #165	-> 96
    //   #178	-> 106
    //   #179	-> 118
    //   #181	-> 126
    //   #182	-> 131
    //   #186	-> 135
    //   #187	-> 145
    //   #183	-> 157
    //   #184	-> 165
    //   #189	-> 175
    //   #158	-> 180
    //   #159	-> 188
    //   #154	-> 198
    //   #155	-> 202
    //   #154	-> 214
    // Exception table:
    //   from	to	target	type
    //   7	12	198	finally
    //   16	22	198	finally
    //   22	26	198	finally
    //   30	40	69	android/os/RemoteException
    //   30	40	198	finally
    //   44	48	198	finally
    //   51	59	69	android/os/RemoteException
    //   51	59	198	finally
    //   59	69	69	android/os/RemoteException
    //   59	69	198	finally
    //   70	78	198	finally
    //   78	88	198	finally
    //   88	96	198	finally
    //   96	106	198	finally
    //   106	118	198	finally
    //   126	131	198	finally
    //   135	145	198	finally
    //   145	154	198	finally
    //   157	165	198	finally
    //   165	175	198	finally
    //   180	188	198	finally
    //   188	198	198	finally
    //   202	214	198	finally
  }
  
  public boolean isDefaultServiceForCategory(ComponentName paramComponentName, String paramString) {
    try {
      return sService.isDefaultServiceForCategory(this.mContext.getUserId(), paramComponentName, paramString);
    } catch (RemoteException remoteException) {
      recoverService();
      INfcCardEmulation iNfcCardEmulation = sService;
      if (iNfcCardEmulation == null) {
        Log.e("CardEmulation", "Failed to recover CardEmulationService.");
        return false;
      } 
      try {
        return iNfcCardEmulation.isDefaultServiceForCategory(this.mContext.getUserId(), paramComponentName, paramString);
      } catch (RemoteException remoteException1) {
        Log.e("CardEmulation", "Failed to recover CardEmulationService.");
        return false;
      } 
    } 
  }
  
  public boolean isDefaultServiceForAid(ComponentName paramComponentName, String paramString) {
    try {
      return sService.isDefaultServiceForAid(this.mContext.getUserId(), paramComponentName, paramString);
    } catch (RemoteException remoteException) {
      recoverService();
      INfcCardEmulation iNfcCardEmulation = sService;
      if (iNfcCardEmulation == null) {
        Log.e("CardEmulation", "Failed to recover CardEmulationService.");
        return false;
      } 
      try {
        return iNfcCardEmulation.isDefaultServiceForAid(this.mContext.getUserId(), paramComponentName, paramString);
      } catch (RemoteException remoteException1) {
        Log.e("CardEmulation", "Failed to reach CardEmulationService.");
        return false;
      } 
    } 
  }
  
  public boolean categoryAllowsForegroundPreference(String paramString) {
    boolean bool1 = "payment".equals(paramString), bool2 = true;
    if (bool1) {
      bool1 = false;
      try {
        int i = Settings.Secure.getInt(this.mContext.getContentResolver(), "nfc_payment_foreground");
        if (i == 0)
          bool2 = false; 
      } catch (android.provider.Settings.SettingNotFoundException settingNotFoundException) {
        bool2 = bool1;
      } 
      return bool2;
    } 
    return true;
  }
  
  public int getSelectionModeForCategory(String paramString) {
    if ("payment".equals(paramString)) {
      paramString = Settings.Secure.getString(this.mContext.getContentResolver(), "nfc_payment_default_component");
      if (paramString != null)
        return 0; 
      return 1;
    } 
    return 2;
  }
  
  public boolean registerAidsForService(ComponentName paramComponentName, String paramString, List<String> paramList) {
    AidGroup aidGroup = new AidGroup(paramList, paramString);
    try {
      return sService.registerAidGroupForService(this.mContext.getUserId(), paramComponentName, aidGroup);
    } catch (RemoteException remoteException) {
      recoverService();
      INfcCardEmulation iNfcCardEmulation = sService;
      if (iNfcCardEmulation == null) {
        Log.e("CardEmulation", "Failed to recover CardEmulationService.");
        return false;
      } 
      try {
        return iNfcCardEmulation.registerAidGroupForService(this.mContext.getUserId(), paramComponentName, aidGroup);
      } catch (RemoteException remoteException1) {
        Log.e("CardEmulation", "Failed to reach CardEmulationService.");
        return false;
      } 
    } 
  }
  
  public boolean unsetOffHostForService(ComponentName paramComponentName) {
    NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this.mContext);
    if (nfcAdapter == null)
      return false; 
    try {
      return sService.unsetOffHostForService(this.mContext.getUserId(), paramComponentName);
    } catch (RemoteException remoteException) {
      recoverService();
      INfcCardEmulation iNfcCardEmulation = sService;
      if (iNfcCardEmulation == null) {
        Log.e("CardEmulation", "Failed to recover CardEmulationService.");
        return false;
      } 
      try {
        return iNfcCardEmulation.unsetOffHostForService(this.mContext.getUserId(), paramComponentName);
      } catch (RemoteException remoteException1) {
        Log.e("CardEmulation", "Failed to reach CardEmulationService.");
        return false;
      } 
    } 
  }
  
  public boolean setOffHostForService(ComponentName paramComponentName, String paramString) {
    String str;
    NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this.mContext);
    if (nfcAdapter == null || paramString == null)
      return false; 
    List<String> list = nfcAdapter.getSupportedOffHostSecureElements();
    if ((paramString.startsWith("eSE") && !list.contains("eSE")) || (
      paramString.startsWith("SIM") && !list.contains("SIM")))
      return false; 
    if (!paramString.startsWith("eSE") && !paramString.startsWith("SIM"))
      return false; 
    if (paramString.equals("eSE")) {
      str = "eSE1";
    } else {
      str = paramString;
      if (paramString.equals("SIM"))
        str = "SIM1"; 
    } 
    try {
      return sService.setOffHostForService(this.mContext.getUserId(), paramComponentName, str);
    } catch (RemoteException remoteException) {
      recoverService();
      INfcCardEmulation iNfcCardEmulation = sService;
      if (iNfcCardEmulation == null) {
        Log.e("CardEmulation", "Failed to recover CardEmulationService.");
        return false;
      } 
      try {
        return iNfcCardEmulation.setOffHostForService(this.mContext.getUserId(), paramComponentName, str);
      } catch (RemoteException remoteException1) {
        Log.e("CardEmulation", "Failed to reach CardEmulationService.");
        return false;
      } 
    } 
  }
  
  public List<String> getAidsForService(ComponentName paramComponentName, String paramString) {
    ComponentName componentName = null;
    List<String> list = null;
    try {
      AidGroup aidGroup = sService.getAidGroupForService(this.mContext.getUserId(), paramComponentName, paramString);
      if (aidGroup != null)
        list = aidGroup.getAids(); 
      return list;
    } catch (RemoteException remoteException) {
      recoverService();
      INfcCardEmulation iNfcCardEmulation = sService;
      if (iNfcCardEmulation == null) {
        Log.e("CardEmulation", "Failed to recover CardEmulationService.");
        return null;
      } 
      try {
        List<String> list1;
        AidGroup aidGroup = iNfcCardEmulation.getAidGroupForService(this.mContext.getUserId(), paramComponentName, paramString);
        paramComponentName = componentName;
        if (aidGroup != null)
          list1 = aidGroup.getAids(); 
        return list1;
      } catch (RemoteException remoteException1) {
        Log.e("CardEmulation", "Failed to recover CardEmulationService.");
        return null;
      } 
    } 
  }
  
  public boolean removeAidsForService(ComponentName paramComponentName, String paramString) {
    try {
      return sService.removeAidGroupForService(this.mContext.getUserId(), paramComponentName, paramString);
    } catch (RemoteException remoteException) {
      recoverService();
      INfcCardEmulation iNfcCardEmulation = sService;
      if (iNfcCardEmulation == null) {
        Log.e("CardEmulation", "Failed to recover CardEmulationService.");
        return false;
      } 
      try {
        return iNfcCardEmulation.removeAidGroupForService(this.mContext.getUserId(), paramComponentName, paramString);
      } catch (RemoteException remoteException1) {
        Log.e("CardEmulation", "Failed to reach CardEmulationService.");
        return false;
      } 
    } 
  }
  
  public boolean setPreferredService(Activity paramActivity, ComponentName paramComponentName) {
    if (paramActivity != null && paramComponentName != null) {
      if (paramActivity.isResumed())
        try {
          return sService.setPreferredService(paramComponentName);
        } catch (RemoteException remoteException) {
          recoverService();
          INfcCardEmulation iNfcCardEmulation = sService;
          if (iNfcCardEmulation == null) {
            Log.e("CardEmulation", "Failed to recover CardEmulationService.");
            return false;
          } 
          try {
            return iNfcCardEmulation.setPreferredService(paramComponentName);
          } catch (RemoteException remoteException1) {
            Log.e("CardEmulation", "Failed to reach CardEmulationService.");
            return false;
          } 
        }  
      throw new IllegalArgumentException("Activity must be resumed.");
    } 
    throw new NullPointerException("activity or service or category is null");
  }
  
  public boolean unsetPreferredService(Activity paramActivity) {
    if (paramActivity != null) {
      if (paramActivity.isResumed())
        try {
          return sService.unsetPreferredService();
        } catch (RemoteException remoteException) {
          recoverService();
          INfcCardEmulation iNfcCardEmulation = sService;
          if (iNfcCardEmulation == null) {
            Log.e("CardEmulation", "Failed to recover CardEmulationService.");
            return false;
          } 
          try {
            return iNfcCardEmulation.unsetPreferredService();
          } catch (RemoteException remoteException1) {
            Log.e("CardEmulation", "Failed to reach CardEmulationService.");
            return false;
          } 
        }  
      throw new IllegalArgumentException("Activity must be resumed.");
    } 
    throw new NullPointerException("activity is null");
  }
  
  public boolean supportsAidPrefixRegistration() {
    try {
      return sService.supportsAidPrefixRegistration();
    } catch (RemoteException remoteException) {
      recoverService();
      INfcCardEmulation iNfcCardEmulation = sService;
      if (iNfcCardEmulation == null) {
        Log.e("CardEmulation", "Failed to recover CardEmulationService.");
        return false;
      } 
      try {
        return iNfcCardEmulation.supportsAidPrefixRegistration();
      } catch (RemoteException remoteException1) {
        Log.e("CardEmulation", "Failed to reach CardEmulationService.");
        return false;
      } 
    } 
  }
  
  public List<String> getAidsForPreferredPaymentService() {
    INfcCardEmulation iNfcCardEmulation = null;
    List<String> list = null;
    try {
      ApduServiceInfo apduServiceInfo = sService.getPreferredPaymentService(this.mContext.getUserId());
      if (apduServiceInfo != null)
        list = apduServiceInfo.getAids(); 
      return list;
    } catch (RemoteException remoteException) {
      recoverService();
      INfcCardEmulation iNfcCardEmulation1 = sService;
      if (iNfcCardEmulation1 != null)
        try {
          List<String> list1;
          Context context = this.mContext;
          ApduServiceInfo apduServiceInfo = iNfcCardEmulation1.getPreferredPaymentService(context.getUserId());
          iNfcCardEmulation1 = iNfcCardEmulation;
          if (apduServiceInfo != null)
            list1 = apduServiceInfo.getAids(); 
          return list1;
        } catch (RemoteException remoteException1) {
          Log.e("CardEmulation", "Failed to recover CardEmulationService.");
          throw remoteException.rethrowFromSystemServer();
        }  
      Log.e("CardEmulation", "Failed to recover CardEmulationService.");
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String getRouteDestinationForPreferredPaymentService() {
    try {
      ApduServiceInfo apduServiceInfo = sService.getPreferredPaymentService(this.mContext.getUserId());
      if (apduServiceInfo != null) {
        if (!apduServiceInfo.isOnHost()) {
          String str;
          if (apduServiceInfo.getOffHostSecureElement() == null) {
            str = "OffHost";
          } else {
            str = str.getOffHostSecureElement();
          } 
          return str;
        } 
        return "Host";
      } 
      return null;
    } catch (RemoteException remoteException) {
      recoverService();
      INfcCardEmulation iNfcCardEmulation = sService;
      if (iNfcCardEmulation != null)
        try {
          Context context = this.mContext;
          ApduServiceInfo apduServiceInfo = iNfcCardEmulation.getPreferredPaymentService(context.getUserId());
          if (apduServiceInfo != null) {
            if (!apduServiceInfo.isOnHost()) {
              String str;
              if (apduServiceInfo.getOffHostSecureElement() == null) {
                str = "Offhost";
              } else {
                str = str.getOffHostSecureElement();
              } 
              return str;
            } 
            return "Host";
          } 
          return null;
        } catch (RemoteException remoteException1) {
          Log.e("CardEmulation", "Failed to recover CardEmulationService.");
          throw remoteException.rethrowFromSystemServer();
        }  
      Log.e("CardEmulation", "Failed to recover CardEmulationService.");
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public CharSequence getDescriptionForPreferredPaymentService() {
    INfcCardEmulation iNfcCardEmulation = null;
    String str = null;
    try {
      ApduServiceInfo apduServiceInfo = sService.getPreferredPaymentService(this.mContext.getUserId());
      if (apduServiceInfo != null)
        str = apduServiceInfo.getDescription(); 
      return str;
    } catch (RemoteException remoteException) {
      recoverService();
      INfcCardEmulation iNfcCardEmulation1 = sService;
      if (iNfcCardEmulation1 != null)
        try {
          String str1;
          Context context = this.mContext;
          ApduServiceInfo apduServiceInfo = iNfcCardEmulation1.getPreferredPaymentService(context.getUserId());
          iNfcCardEmulation1 = iNfcCardEmulation;
          if (apduServiceInfo != null)
            str1 = apduServiceInfo.getDescription(); 
          return str1;
        } catch (RemoteException remoteException1) {
          Log.e("CardEmulation", "Failed to recover CardEmulationService.");
          throw remoteException.rethrowFromSystemServer();
        }  
      Log.e("CardEmulation", "Failed to recover CardEmulationService.");
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean setDefaultServiceForCategory(ComponentName paramComponentName, String paramString) {
    try {
      return sService.setDefaultServiceForCategory(this.mContext.getUserId(), paramComponentName, paramString);
    } catch (RemoteException remoteException) {
      recoverService();
      INfcCardEmulation iNfcCardEmulation = sService;
      if (iNfcCardEmulation == null) {
        Log.e("CardEmulation", "Failed to recover CardEmulationService.");
        return false;
      } 
      try {
        return iNfcCardEmulation.setDefaultServiceForCategory(this.mContext.getUserId(), paramComponentName, paramString);
      } catch (RemoteException remoteException1) {
        Log.e("CardEmulation", "Failed to reach CardEmulationService.");
        return false;
      } 
    } 
  }
  
  public boolean setDefaultForNextTap(ComponentName paramComponentName) {
    try {
      return sService.setDefaultForNextTap(this.mContext.getUserId(), paramComponentName);
    } catch (RemoteException remoteException) {
      recoverService();
      INfcCardEmulation iNfcCardEmulation = sService;
      if (iNfcCardEmulation == null) {
        Log.e("CardEmulation", "Failed to recover CardEmulationService.");
        return false;
      } 
      try {
        return iNfcCardEmulation.setDefaultForNextTap(this.mContext.getUserId(), paramComponentName);
      } catch (RemoteException remoteException1) {
        Log.e("CardEmulation", "Failed to reach CardEmulationService.");
        return false;
      } 
    } 
  }
  
  public List<ApduServiceInfo> getServices(String paramString) {
    try {
      return sService.getServices(this.mContext.getUserId(), paramString);
    } catch (RemoteException remoteException) {
      recoverService();
      INfcCardEmulation iNfcCardEmulation = sService;
      if (iNfcCardEmulation == null) {
        Log.e("CardEmulation", "Failed to recover CardEmulationService.");
        return null;
      } 
      try {
        return iNfcCardEmulation.getServices(this.mContext.getUserId(), paramString);
      } catch (RemoteException remoteException1) {
        Log.e("CardEmulation", "Failed to reach CardEmulationService.");
        return null;
      } 
    } 
  }
  
  public static boolean isValidAid(String paramString) {
    if (paramString == null)
      return false; 
    if ((paramString.endsWith("*") || paramString.endsWith("#")) && paramString.length() % 2 == 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("AID ");
      stringBuilder.append(paramString);
      stringBuilder.append(" is not a valid AID.");
      Log.e("CardEmulation", stringBuilder.toString());
      return false;
    } 
    if (!paramString.endsWith("*") && !paramString.endsWith("#") && paramString.length() % 2 != 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("AID ");
      stringBuilder.append(paramString);
      stringBuilder.append(" is not a valid AID.");
      Log.e("CardEmulation", stringBuilder.toString());
      return false;
    } 
    if (!AID_PATTERN.matcher(paramString).matches()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("AID ");
      stringBuilder.append(paramString);
      stringBuilder.append(" is not a valid AID.");
      Log.e("CardEmulation", stringBuilder.toString());
      return false;
    } 
    return true;
  }
  
  void recoverService() {
    NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this.mContext);
    sService = nfcAdapter.getCardEmulationService();
  }
}
