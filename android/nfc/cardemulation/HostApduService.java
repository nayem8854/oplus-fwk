package android.nfc.cardemulation;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

public abstract class HostApduService extends Service {
  Messenger mNfcService = null;
  
  final Messenger mMessenger = new Messenger(new MsgHandler());
  
  static final String TAG = "ApduService";
  
  public static final String SERVICE_META_DATA = "android.nfc.cardemulation.host_apdu_service";
  
  public static final String SERVICE_INTERFACE = "android.nfc.cardemulation.action.HOST_APDU_SERVICE";
  
  public static final int MSG_UNHANDLED = 3;
  
  public static final int MSG_RESPONSE_APDU = 1;
  
  public static final int MSG_DEACTIVATED = 2;
  
  public static final int MSG_COMMAND_APDU = 0;
  
  public static final String KEY_DATA = "data";
  
  public static final int DEACTIVATION_LINK_LOSS = 0;
  
  public static final int DEACTIVATION_DESELECTED = 1;
  
  class MsgHandler extends Handler {
    final HostApduService this$0;
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            if (i != 3) {
              super.handleMessage(param1Message);
            } else {
              if (HostApduService.this.mNfcService == null) {
                Log.e("ApduService", "notifyUnhandled not sent; service was deactivated.");
                return;
              } 
              try {
                param1Message.replyTo = HostApduService.this.mMessenger;
                HostApduService.this.mNfcService.send(param1Message);
              } catch (RemoteException null) {
                Log.e("ApduService", "RemoteException calling into NfcService.");
              } 
            } 
          } else {
            HostApduService.this.mNfcService = null;
            HostApduService.this.onDeactivated(((Message)remoteException).arg1);
          } 
        } else {
          if (HostApduService.this.mNfcService == null) {
            Log.e("ApduService", "Response not sent; service was deactivated.");
            return;
          } 
          try {
            ((Message)remoteException).replyTo = HostApduService.this.mMessenger;
            HostApduService.this.mNfcService.send((Message)remoteException);
          } catch (RemoteException remoteException) {
            Log.e("ApduService", "RemoteException calling into NfcService.");
          } 
        } 
      } else {
        Bundle bundle = remoteException.getData();
        if (bundle == null)
          return; 
        if (HostApduService.this.mNfcService == null)
          HostApduService.this.mNfcService = ((Message)remoteException).replyTo; 
        byte[] arrayOfByte = bundle.getByteArray("data");
        if (arrayOfByte != null) {
          arrayOfByte = HostApduService.this.processCommandApdu(arrayOfByte, null);
          if (arrayOfByte != null) {
            if (HostApduService.this.mNfcService == null) {
              Log.e("ApduService", "Response not sent; service was deactivated.");
              return;
            } 
            Message message = Message.obtain((Handler)null, 1);
            bundle = new Bundle();
            bundle.putByteArray("data", arrayOfByte);
            message.setData(bundle);
            message.replyTo = HostApduService.this.mMessenger;
            try {
              HostApduService.this.mNfcService.send(message);
            } catch (RemoteException remoteException1) {
              Log.e("TAG", "Response not sent; RemoteException calling into NfcService.");
            } 
          } 
        } else {
          Log.e("ApduService", "Received MSG_COMMAND_APDU without data.");
        } 
      } 
    }
  }
  
  public final IBinder onBind(Intent paramIntent) {
    return this.mMessenger.getBinder();
  }
  
  public final void sendResponseApdu(byte[] paramArrayOfbyte) {
    Message message = Message.obtain((Handler)null, 1);
    Bundle bundle = new Bundle();
    bundle.putByteArray("data", paramArrayOfbyte);
    message.setData(bundle);
    try {
      this.mMessenger.send(message);
    } catch (RemoteException remoteException) {
      Log.e("TAG", "Local messenger has died.");
    } 
  }
  
  public final void notifyUnhandled() {
    Message message = Message.obtain((Handler)null, 3);
    try {
      this.mMessenger.send(message);
    } catch (RemoteException remoteException) {
      Log.e("TAG", "Local messenger has died.");
    } 
  }
  
  public abstract void onDeactivated(int paramInt);
  
  public abstract byte[] processCommandApdu(byte[] paramArrayOfbyte, Bundle paramBundle);
}
