package android.nfc;

import android.content.Context;

public final class NfcManager {
  private NfcAdapter mAdapter;
  
  private final Context mContext;
  
  public NfcManager(Context paramContext) {
    this.mContext = paramContext.getApplicationContext();
    init();
  }
  
  private void init() {
    Context context = this.mContext;
    if (context != null) {
      try {
        NfcAdapter nfcAdapter = NfcAdapter.getNfcAdapter(context);
      } catch (UnsupportedOperationException unsupportedOperationException) {
        unsupportedOperationException = null;
      } 
      this.mAdapter = (NfcAdapter)unsupportedOperationException;
      return;
    } 
    throw new IllegalArgumentException("context not associated with any application (using a mock context?)");
  }
  
  public NfcAdapter getDefaultAdapter() {
    NfcAdapter nfcAdapter = this.mAdapter;
    if (nfcAdapter == null) {
      init();
    } else {
      nfcAdapter.getAdapterState();
    } 
    return this.mAdapter;
  }
}
