package android.nfc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ITagRemovedCallback extends IInterface {
  void onTagRemoved() throws RemoteException;
  
  class Default implements ITagRemovedCallback {
    public void onTagRemoved() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITagRemovedCallback {
    private static final String DESCRIPTOR = "android.nfc.ITagRemovedCallback";
    
    static final int TRANSACTION_onTagRemoved = 1;
    
    public Stub() {
      attachInterface(this, "android.nfc.ITagRemovedCallback");
    }
    
    public static ITagRemovedCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.nfc.ITagRemovedCallback");
      if (iInterface != null && iInterface instanceof ITagRemovedCallback)
        return (ITagRemovedCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onTagRemoved";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.nfc.ITagRemovedCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.nfc.ITagRemovedCallback");
      onTagRemoved();
      return true;
    }
    
    private static class Proxy implements ITagRemovedCallback {
      public static ITagRemovedCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.nfc.ITagRemovedCallback";
      }
      
      public void onTagRemoved() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.nfc.ITagRemovedCallback");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ITagRemovedCallback.Stub.getDefaultImpl() != null) {
            ITagRemovedCallback.Stub.getDefaultImpl().onTagRemoved();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITagRemovedCallback param1ITagRemovedCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITagRemovedCallback != null) {
          Proxy.sDefaultImpl = param1ITagRemovedCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITagRemovedCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
