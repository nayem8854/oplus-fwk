package android.nfc;

import android.app.PendingIntent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INfcAdapter extends IInterface {
  void addNfcUnlockHandler(INfcUnlockHandler paramINfcUnlockHandler, int[] paramArrayOfint) throws RemoteException;
  
  boolean deviceSupportsNfcSecure() throws RemoteException;
  
  boolean disable(boolean paramBoolean) throws RemoteException;
  
  boolean disableNdefPush() throws RemoteException;
  
  void dispatch(Tag paramTag) throws RemoteException;
  
  boolean enable() throws RemoteException;
  
  boolean enableNdefPush() throws RemoteException;
  
  INfcAdapterExtras getNfcAdapterExtrasInterface(String paramString) throws RemoteException;
  
  IBinder getNfcAdapterVendorInterface(String paramString) throws RemoteException;
  
  INfcCardEmulation getNfcCardEmulationInterface() throws RemoteException;
  
  INfcDta getNfcDtaInterface(String paramString) throws RemoteException;
  
  INfcFCardEmulation getNfcFCardEmulationInterface() throws RemoteException;
  
  INfcTag getNfcTagInterface() throws RemoteException;
  
  int getState() throws RemoteException;
  
  boolean ignore(int paramInt1, int paramInt2, ITagRemovedCallback paramITagRemovedCallback) throws RemoteException;
  
  void invokeBeam() throws RemoteException;
  
  void invokeBeamInternal(BeamShareData paramBeamShareData) throws RemoteException;
  
  boolean isNdefPushEnabled() throws RemoteException;
  
  boolean isNfcSecureEnabled() throws RemoteException;
  
  void pausePolling(int paramInt) throws RemoteException;
  
  void removeNfcUnlockHandler(INfcUnlockHandler paramINfcUnlockHandler) throws RemoteException;
  
  void resumePolling() throws RemoteException;
  
  void setAppCallback(IAppCallback paramIAppCallback) throws RemoteException;
  
  void setForegroundDispatch(PendingIntent paramPendingIntent, IntentFilter[] paramArrayOfIntentFilter, TechListParcel paramTechListParcel) throws RemoteException;
  
  boolean setNfcSecure(boolean paramBoolean) throws RemoteException;
  
  void setP2pModes(int paramInt1, int paramInt2) throws RemoteException;
  
  void setReaderMode(IBinder paramIBinder, IAppCallback paramIAppCallback, int paramInt, Bundle paramBundle) throws RemoteException;
  
  void verifyNfcPermission() throws RemoteException;
  
  class Default implements INfcAdapter {
    public INfcTag getNfcTagInterface() throws RemoteException {
      return null;
    }
    
    public INfcCardEmulation getNfcCardEmulationInterface() throws RemoteException {
      return null;
    }
    
    public INfcFCardEmulation getNfcFCardEmulationInterface() throws RemoteException {
      return null;
    }
    
    public INfcAdapterExtras getNfcAdapterExtrasInterface(String param1String) throws RemoteException {
      return null;
    }
    
    public INfcDta getNfcDtaInterface(String param1String) throws RemoteException {
      return null;
    }
    
    public IBinder getNfcAdapterVendorInterface(String param1String) throws RemoteException {
      return null;
    }
    
    public int getState() throws RemoteException {
      return 0;
    }
    
    public boolean disable(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean enable() throws RemoteException {
      return false;
    }
    
    public boolean enableNdefPush() throws RemoteException {
      return false;
    }
    
    public boolean disableNdefPush() throws RemoteException {
      return false;
    }
    
    public boolean isNdefPushEnabled() throws RemoteException {
      return false;
    }
    
    public void pausePolling(int param1Int) throws RemoteException {}
    
    public void resumePolling() throws RemoteException {}
    
    public void setForegroundDispatch(PendingIntent param1PendingIntent, IntentFilter[] param1ArrayOfIntentFilter, TechListParcel param1TechListParcel) throws RemoteException {}
    
    public void setAppCallback(IAppCallback param1IAppCallback) throws RemoteException {}
    
    public void invokeBeam() throws RemoteException {}
    
    public void invokeBeamInternal(BeamShareData param1BeamShareData) throws RemoteException {}
    
    public boolean ignore(int param1Int1, int param1Int2, ITagRemovedCallback param1ITagRemovedCallback) throws RemoteException {
      return false;
    }
    
    public void dispatch(Tag param1Tag) throws RemoteException {}
    
    public void setReaderMode(IBinder param1IBinder, IAppCallback param1IAppCallback, int param1Int, Bundle param1Bundle) throws RemoteException {}
    
    public void setP2pModes(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void addNfcUnlockHandler(INfcUnlockHandler param1INfcUnlockHandler, int[] param1ArrayOfint) throws RemoteException {}
    
    public void removeNfcUnlockHandler(INfcUnlockHandler param1INfcUnlockHandler) throws RemoteException {}
    
    public void verifyNfcPermission() throws RemoteException {}
    
    public boolean isNfcSecureEnabled() throws RemoteException {
      return false;
    }
    
    public boolean deviceSupportsNfcSecure() throws RemoteException {
      return false;
    }
    
    public boolean setNfcSecure(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INfcAdapter {
    private static final String DESCRIPTOR = "android.nfc.INfcAdapter";
    
    static final int TRANSACTION_addNfcUnlockHandler = 23;
    
    static final int TRANSACTION_deviceSupportsNfcSecure = 27;
    
    static final int TRANSACTION_disable = 8;
    
    static final int TRANSACTION_disableNdefPush = 11;
    
    static final int TRANSACTION_dispatch = 20;
    
    static final int TRANSACTION_enable = 9;
    
    static final int TRANSACTION_enableNdefPush = 10;
    
    static final int TRANSACTION_getNfcAdapterExtrasInterface = 4;
    
    static final int TRANSACTION_getNfcAdapterVendorInterface = 6;
    
    static final int TRANSACTION_getNfcCardEmulationInterface = 2;
    
    static final int TRANSACTION_getNfcDtaInterface = 5;
    
    static final int TRANSACTION_getNfcFCardEmulationInterface = 3;
    
    static final int TRANSACTION_getNfcTagInterface = 1;
    
    static final int TRANSACTION_getState = 7;
    
    static final int TRANSACTION_ignore = 19;
    
    static final int TRANSACTION_invokeBeam = 17;
    
    static final int TRANSACTION_invokeBeamInternal = 18;
    
    static final int TRANSACTION_isNdefPushEnabled = 12;
    
    static final int TRANSACTION_isNfcSecureEnabled = 26;
    
    static final int TRANSACTION_pausePolling = 13;
    
    static final int TRANSACTION_removeNfcUnlockHandler = 24;
    
    static final int TRANSACTION_resumePolling = 14;
    
    static final int TRANSACTION_setAppCallback = 16;
    
    static final int TRANSACTION_setForegroundDispatch = 15;
    
    static final int TRANSACTION_setNfcSecure = 28;
    
    static final int TRANSACTION_setP2pModes = 22;
    
    static final int TRANSACTION_setReaderMode = 21;
    
    static final int TRANSACTION_verifyNfcPermission = 25;
    
    public Stub() {
      attachInterface(this, "android.nfc.INfcAdapter");
    }
    
    public static INfcAdapter asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.nfc.INfcAdapter");
      if (iInterface != null && iInterface instanceof INfcAdapter)
        return (INfcAdapter)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 28:
          return "setNfcSecure";
        case 27:
          return "deviceSupportsNfcSecure";
        case 26:
          return "isNfcSecureEnabled";
        case 25:
          return "verifyNfcPermission";
        case 24:
          return "removeNfcUnlockHandler";
        case 23:
          return "addNfcUnlockHandler";
        case 22:
          return "setP2pModes";
        case 21:
          return "setReaderMode";
        case 20:
          return "dispatch";
        case 19:
          return "ignore";
        case 18:
          return "invokeBeamInternal";
        case 17:
          return "invokeBeam";
        case 16:
          return "setAppCallback";
        case 15:
          return "setForegroundDispatch";
        case 14:
          return "resumePolling";
        case 13:
          return "pausePolling";
        case 12:
          return "isNdefPushEnabled";
        case 11:
          return "disableNdefPush";
        case 10:
          return "enableNdefPush";
        case 9:
          return "enable";
        case 8:
          return "disable";
        case 7:
          return "getState";
        case 6:
          return "getNfcAdapterVendorInterface";
        case 5:
          return "getNfcDtaInterface";
        case 4:
          return "getNfcAdapterExtrasInterface";
        case 3:
          return "getNfcFCardEmulationInterface";
        case 2:
          return "getNfcCardEmulationInterface";
        case 1:
          break;
      } 
      return "getNfcTagInterface";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        int i;
        INfcUnlockHandler iNfcUnlockHandler1;
        int[] arrayOfInt;
        ITagRemovedCallback iTagRemovedCallback;
        IAppCallback iAppCallback2;
        String str3;
        IBinder iBinder3;
        String str2;
        IAppCallback iAppCallback1;
        IBinder iBinder2;
        String str1;
        INfcDta iNfcDta1;
        IntentFilter[] arrayOfIntentFilter;
        INfcDta iNfcDta2;
        IAppCallback iAppCallback3;
        INfcAdapterExtras iNfcAdapterExtras;
        INfcFCardEmulation iNfcFCardEmulation;
        INfcCardEmulation iNfcCardEmulation;
        boolean bool4 = false, bool5 = false;
        IBinder iBinder4 = null, iBinder5 = null, iBinder6 = null, iBinder7 = null;
        INfcUnlockHandler iNfcUnlockHandler2 = null;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 28:
            param1Parcel1.enforceInterface("android.nfc.INfcAdapter");
            bool4 = bool5;
            if (param1Parcel1.readInt() != 0)
              bool4 = true; 
            bool3 = setNfcSecure(bool4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 27:
            param1Parcel1.enforceInterface("android.nfc.INfcAdapter");
            bool3 = deviceSupportsNfcSecure();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 26:
            param1Parcel1.enforceInterface("android.nfc.INfcAdapter");
            bool3 = isNfcSecureEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 25:
            param1Parcel1.enforceInterface("android.nfc.INfcAdapter");
            verifyNfcPermission();
            param1Parcel2.writeNoException();
            return true;
          case 24:
            param1Parcel1.enforceInterface("android.nfc.INfcAdapter");
            iNfcUnlockHandler1 = INfcUnlockHandler.Stub.asInterface(param1Parcel1.readStrongBinder());
            removeNfcUnlockHandler(iNfcUnlockHandler1);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            iNfcUnlockHandler1.enforceInterface("android.nfc.INfcAdapter");
            iNfcUnlockHandler2 = INfcUnlockHandler.Stub.asInterface(iNfcUnlockHandler1.readStrongBinder());
            arrayOfInt = iNfcUnlockHandler1.createIntArray();
            addNfcUnlockHandler(iNfcUnlockHandler2, arrayOfInt);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            arrayOfInt.enforceInterface("android.nfc.INfcAdapter");
            param1Int2 = arrayOfInt.readInt();
            k = arrayOfInt.readInt();
            setP2pModes(param1Int2, k);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            arrayOfInt.enforceInterface("android.nfc.INfcAdapter");
            iBinder4 = arrayOfInt.readStrongBinder();
            iAppCallback3 = IAppCallback.Stub.asInterface(arrayOfInt.readStrongBinder());
            k = arrayOfInt.readInt();
            if (arrayOfInt.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)arrayOfInt);
            } else {
              arrayOfInt = null;
            } 
            setReaderMode(iBinder4, iAppCallback3, k, (Bundle)arrayOfInt);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            arrayOfInt.enforceInterface("android.nfc.INfcAdapter");
            if (arrayOfInt.readInt() != 0) {
              Tag tag = Tag.CREATOR.createFromParcel((Parcel)arrayOfInt);
            } else {
              arrayOfInt = null;
            } 
            dispatch((Tag)arrayOfInt);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            arrayOfInt.enforceInterface("android.nfc.INfcAdapter");
            param1Int2 = arrayOfInt.readInt();
            k = arrayOfInt.readInt();
            iTagRemovedCallback = ITagRemovedCallback.Stub.asInterface(arrayOfInt.readStrongBinder());
            bool2 = ignore(param1Int2, k, iTagRemovedCallback);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 18:
            iTagRemovedCallback.enforceInterface("android.nfc.INfcAdapter");
            if (iTagRemovedCallback.readInt() != 0) {
              BeamShareData beamShareData = BeamShareData.CREATOR.createFromParcel((Parcel)iTagRemovedCallback);
            } else {
              iTagRemovedCallback = null;
            } 
            invokeBeamInternal((BeamShareData)iTagRemovedCallback);
            return true;
          case 17:
            iTagRemovedCallback.enforceInterface("android.nfc.INfcAdapter");
            invokeBeam();
            return true;
          case 16:
            iTagRemovedCallback.enforceInterface("android.nfc.INfcAdapter");
            iAppCallback2 = IAppCallback.Stub.asInterface(iTagRemovedCallback.readStrongBinder());
            setAppCallback(iAppCallback2);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            iAppCallback2.enforceInterface("android.nfc.INfcAdapter");
            if (iAppCallback2.readInt() != 0) {
              PendingIntent pendingIntent = PendingIntent.CREATOR.createFromParcel((Parcel)iAppCallback2);
            } else {
              iAppCallback3 = null;
            } 
            arrayOfIntentFilter = iAppCallback2.<IntentFilter>createTypedArray(IntentFilter.CREATOR);
            if (iAppCallback2.readInt() != 0) {
              TechListParcel techListParcel = TechListParcel.CREATOR.createFromParcel((Parcel)iAppCallback2);
            } else {
              iAppCallback2 = null;
            } 
            setForegroundDispatch((PendingIntent)iAppCallback3, arrayOfIntentFilter, (TechListParcel)iAppCallback2);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            iAppCallback2.enforceInterface("android.nfc.INfcAdapter");
            resumePolling();
            param1Parcel2.writeNoException();
            return true;
          case 13:
            iAppCallback2.enforceInterface("android.nfc.INfcAdapter");
            j = iAppCallback2.readInt();
            pausePolling(j);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            iAppCallback2.enforceInterface("android.nfc.INfcAdapter");
            bool1 = isNdefPushEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 11:
            iAppCallback2.enforceInterface("android.nfc.INfcAdapter");
            bool1 = disableNdefPush();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 10:
            iAppCallback2.enforceInterface("android.nfc.INfcAdapter");
            bool1 = enableNdefPush();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 9:
            iAppCallback2.enforceInterface("android.nfc.INfcAdapter");
            bool1 = enable();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 8:
            iAppCallback2.enforceInterface("android.nfc.INfcAdapter");
            if (iAppCallback2.readInt() != 0)
              bool4 = true; 
            bool1 = disable(bool4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 7:
            iAppCallback2.enforceInterface("android.nfc.INfcAdapter");
            i = getState();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 6:
            iAppCallback2.enforceInterface("android.nfc.INfcAdapter");
            str3 = iAppCallback2.readString();
            iBinder3 = getNfcAdapterVendorInterface(str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStrongBinder(iBinder3);
            return true;
          case 5:
            iBinder3.enforceInterface("android.nfc.INfcAdapter");
            str2 = iBinder3.readString();
            iNfcDta2 = getNfcDtaInterface(str2);
            param1Parcel2.writeNoException();
            iAppCallback1 = iAppCallback3;
            if (iNfcDta2 != null)
              iBinder2 = iNfcDta2.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder2);
            return true;
          case 4:
            iBinder2.enforceInterface("android.nfc.INfcAdapter");
            str1 = iBinder2.readString();
            iNfcAdapterExtras = getNfcAdapterExtrasInterface(str1);
            param1Parcel2.writeNoException();
            iNfcDta1 = iNfcDta2;
            if (iNfcAdapterExtras != null)
              iBinder1 = iNfcAdapterExtras.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder1);
            return true;
          case 3:
            iBinder1.enforceInterface("android.nfc.INfcAdapter");
            iNfcFCardEmulation = getNfcFCardEmulationInterface();
            param1Parcel2.writeNoException();
            iBinder1 = iBinder5;
            if (iNfcFCardEmulation != null)
              iBinder1 = iNfcFCardEmulation.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder1);
            return true;
          case 2:
            iBinder1.enforceInterface("android.nfc.INfcAdapter");
            iNfcCardEmulation = getNfcCardEmulationInterface();
            param1Parcel2.writeNoException();
            iBinder1 = iBinder6;
            if (iNfcCardEmulation != null)
              iBinder1 = iNfcCardEmulation.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder1);
            return true;
          case 1:
            break;
        } 
        iBinder1.enforceInterface("android.nfc.INfcAdapter");
        INfcTag iNfcTag = getNfcTagInterface();
        param1Parcel2.writeNoException();
        IBinder iBinder1 = iBinder7;
        if (iNfcTag != null)
          iBinder1 = iNfcTag.asBinder(); 
        param1Parcel2.writeStrongBinder(iBinder1);
        return true;
      } 
      param1Parcel2.writeString("android.nfc.INfcAdapter");
      return true;
    }
    
    private static class Proxy implements INfcAdapter {
      public static INfcAdapter sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.nfc.INfcAdapter";
      }
      
      public INfcTag getNfcTagInterface() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && INfcAdapter.Stub.getDefaultImpl() != null)
            return INfcAdapter.Stub.getDefaultImpl().getNfcTagInterface(); 
          parcel2.readException();
          return INfcTag.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public INfcCardEmulation getNfcCardEmulationInterface() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && INfcAdapter.Stub.getDefaultImpl() != null)
            return INfcAdapter.Stub.getDefaultImpl().getNfcCardEmulationInterface(); 
          parcel2.readException();
          return INfcCardEmulation.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public INfcFCardEmulation getNfcFCardEmulationInterface() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && INfcAdapter.Stub.getDefaultImpl() != null)
            return INfcAdapter.Stub.getDefaultImpl().getNfcFCardEmulationInterface(); 
          parcel2.readException();
          return INfcFCardEmulation.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public INfcAdapterExtras getNfcAdapterExtrasInterface(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && INfcAdapter.Stub.getDefaultImpl() != null)
            return INfcAdapter.Stub.getDefaultImpl().getNfcAdapterExtrasInterface(param2String); 
          parcel2.readException();
          return INfcAdapterExtras.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public INfcDta getNfcDtaInterface(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && INfcAdapter.Stub.getDefaultImpl() != null)
            return INfcAdapter.Stub.getDefaultImpl().getNfcDtaInterface(param2String); 
          parcel2.readException();
          return INfcDta.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IBinder getNfcAdapterVendorInterface(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && INfcAdapter.Stub.getDefaultImpl() != null)
            return INfcAdapter.Stub.getDefaultImpl().getNfcAdapterVendorInterface(param2String); 
          parcel2.readException();
          return parcel2.readStrongBinder();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && INfcAdapter.Stub.getDefaultImpl() != null)
            return INfcAdapter.Stub.getDefaultImpl().getState(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean disable(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool1 && INfcAdapter.Stub.getDefaultImpl() != null) {
            param2Boolean = INfcAdapter.Stub.getDefaultImpl().disable(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean enable() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && INfcAdapter.Stub.getDefaultImpl() != null) {
            bool1 = INfcAdapter.Stub.getDefaultImpl().enable();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean enableNdefPush() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
          if (!bool2 && INfcAdapter.Stub.getDefaultImpl() != null) {
            bool1 = INfcAdapter.Stub.getDefaultImpl().enableNdefPush();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean disableNdefPush() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(11, parcel1, parcel2, 0);
          if (!bool2 && INfcAdapter.Stub.getDefaultImpl() != null) {
            bool1 = INfcAdapter.Stub.getDefaultImpl().disableNdefPush();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isNdefPushEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(12, parcel1, parcel2, 0);
          if (!bool2 && INfcAdapter.Stub.getDefaultImpl() != null) {
            bool1 = INfcAdapter.Stub.getDefaultImpl().isNdefPushEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void pausePolling(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && INfcAdapter.Stub.getDefaultImpl() != null) {
            INfcAdapter.Stub.getDefaultImpl().pausePolling(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resumePolling() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && INfcAdapter.Stub.getDefaultImpl() != null) {
            INfcAdapter.Stub.getDefaultImpl().resumePolling();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setForegroundDispatch(PendingIntent param2PendingIntent, IntentFilter[] param2ArrayOfIntentFilter, TechListParcel param2TechListParcel) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeTypedArray(param2ArrayOfIntentFilter, 0);
          if (param2TechListParcel != null) {
            parcel1.writeInt(1);
            param2TechListParcel.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && INfcAdapter.Stub.getDefaultImpl() != null) {
            INfcAdapter.Stub.getDefaultImpl().setForegroundDispatch(param2PendingIntent, param2ArrayOfIntentFilter, param2TechListParcel);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAppCallback(IAppCallback param2IAppCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          if (param2IAppCallback != null) {
            iBinder = param2IAppCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && INfcAdapter.Stub.getDefaultImpl() != null) {
            INfcAdapter.Stub.getDefaultImpl().setAppCallback(param2IAppCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void invokeBeam() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.nfc.INfcAdapter");
          boolean bool = this.mRemote.transact(17, parcel, (Parcel)null, 1);
          if (!bool && INfcAdapter.Stub.getDefaultImpl() != null) {
            INfcAdapter.Stub.getDefaultImpl().invokeBeam();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void invokeBeamInternal(BeamShareData param2BeamShareData) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.nfc.INfcAdapter");
          if (param2BeamShareData != null) {
            parcel.writeInt(1);
            param2BeamShareData.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(18, parcel, (Parcel)null, 1);
          if (!bool && INfcAdapter.Stub.getDefaultImpl() != null) {
            INfcAdapter.Stub.getDefaultImpl().invokeBeamInternal(param2BeamShareData);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean ignore(int param2Int1, int param2Int2, ITagRemovedCallback param2ITagRemovedCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2ITagRemovedCallback != null) {
            iBinder = param2ITagRemovedCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(19, parcel1, parcel2, 0);
          if (!bool2 && INfcAdapter.Stub.getDefaultImpl() != null) {
            bool1 = INfcAdapter.Stub.getDefaultImpl().ignore(param2Int1, param2Int2, param2ITagRemovedCallback);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void dispatch(Tag param2Tag) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          if (param2Tag != null) {
            parcel1.writeInt(1);
            param2Tag.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && INfcAdapter.Stub.getDefaultImpl() != null) {
            INfcAdapter.Stub.getDefaultImpl().dispatch(param2Tag);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setReaderMode(IBinder param2IBinder, IAppCallback param2IAppCallback, int param2Int, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2IAppCallback != null) {
            iBinder = param2IAppCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && INfcAdapter.Stub.getDefaultImpl() != null) {
            INfcAdapter.Stub.getDefaultImpl().setReaderMode(param2IBinder, param2IAppCallback, param2Int, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setP2pModes(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && INfcAdapter.Stub.getDefaultImpl() != null) {
            INfcAdapter.Stub.getDefaultImpl().setP2pModes(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addNfcUnlockHandler(INfcUnlockHandler param2INfcUnlockHandler, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          if (param2INfcUnlockHandler != null) {
            iBinder = param2INfcUnlockHandler.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && INfcAdapter.Stub.getDefaultImpl() != null) {
            INfcAdapter.Stub.getDefaultImpl().addNfcUnlockHandler(param2INfcUnlockHandler, param2ArrayOfint);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeNfcUnlockHandler(INfcUnlockHandler param2INfcUnlockHandler) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          if (param2INfcUnlockHandler != null) {
            iBinder = param2INfcUnlockHandler.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && INfcAdapter.Stub.getDefaultImpl() != null) {
            INfcAdapter.Stub.getDefaultImpl().removeNfcUnlockHandler(param2INfcUnlockHandler);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void verifyNfcPermission() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && INfcAdapter.Stub.getDefaultImpl() != null) {
            INfcAdapter.Stub.getDefaultImpl().verifyNfcPermission();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isNfcSecureEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(26, parcel1, parcel2, 0);
          if (!bool2 && INfcAdapter.Stub.getDefaultImpl() != null) {
            bool1 = INfcAdapter.Stub.getDefaultImpl().isNfcSecureEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean deviceSupportsNfcSecure() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(27, parcel1, parcel2, 0);
          if (!bool2 && INfcAdapter.Stub.getDefaultImpl() != null) {
            bool1 = INfcAdapter.Stub.getDefaultImpl().deviceSupportsNfcSecure();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setNfcSecure(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcAdapter");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool1 && INfcAdapter.Stub.getDefaultImpl() != null) {
            param2Boolean = INfcAdapter.Stub.getDefaultImpl().setNfcSecure(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INfcAdapter param1INfcAdapter) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INfcAdapter != null) {
          Proxy.sDefaultImpl = param1INfcAdapter;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INfcAdapter getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
