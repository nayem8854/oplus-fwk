package android.nfc;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.IOException;

public final class TransceiveResult implements Parcelable {
  public TransceiveResult(int paramInt, byte[] paramArrayOfbyte) {
    this.mResult = paramInt;
    this.mResponseData = paramArrayOfbyte;
  }
  
  public byte[] getResponseOrThrow() throws IOException {
    int i = this.mResult;
    if (i != 0) {
      if (i != 2) {
        if (i != 3)
          throw new IOException("Transceive failed"); 
        throw new IOException("Transceive length exceeds supported maximum");
      } 
      throw new TagLostException("Tag was lost.");
    } 
    return this.mResponseData;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mResult);
    if (this.mResult == 0) {
      paramParcel.writeInt(this.mResponseData.length);
      paramParcel.writeByteArray(this.mResponseData);
    } 
  }
  
  public static final Parcelable.Creator<TransceiveResult> CREATOR = new Parcelable.Creator<TransceiveResult>() {
      public TransceiveResult createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        if (i == 0) {
          int j = param1Parcel.readInt();
          byte[] arrayOfByte2 = new byte[j];
          param1Parcel.readByteArray(arrayOfByte2);
          byte[] arrayOfByte1 = arrayOfByte2;
        } else {
          param1Parcel = null;
        } 
        return new TransceiveResult(i, (byte[])param1Parcel);
      }
      
      public TransceiveResult[] newArray(int param1Int) {
        return new TransceiveResult[param1Int];
      }
    };
  
  public static final int RESULT_EXCEEDED_LENGTH = 3;
  
  public static final int RESULT_FAILURE = 1;
  
  public static final int RESULT_SUCCESS = 0;
  
  public static final int RESULT_TAGLOST = 2;
  
  final byte[] mResponseData;
  
  final int mResult;
}
