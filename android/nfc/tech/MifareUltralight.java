package android.nfc.tech;

import android.nfc.Tag;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import java.io.IOException;

public final class MifareUltralight extends BasicTagTechnology {
  public static final String EXTRA_IS_UL_C = "isulc";
  
  private static final int MAX_PAGE_COUNT = 256;
  
  private static final int NXP_MANUFACTURER_ID = 4;
  
  public static final int PAGE_SIZE = 4;
  
  private static final String TAG = "NFC";
  
  public static final int TYPE_ULTRALIGHT = 1;
  
  public static final int TYPE_ULTRALIGHT_C = 2;
  
  public static final int TYPE_UNKNOWN = -1;
  
  private int mType;
  
  public static MifareUltralight get(Tag paramTag) {
    if (!paramTag.hasTech(9))
      return null; 
    try {
      return new MifareUltralight(paramTag);
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public MifareUltralight(Tag paramTag) throws RemoteException {
    super(paramTag, 9);
    NfcA nfcA = NfcA.get(paramTag);
    this.mType = -1;
    if (nfcA.getSak() == 0 && paramTag.getId()[0] == 4) {
      Bundle bundle = paramTag.getTechExtras(9);
      if (bundle.getBoolean("isulc")) {
        this.mType = 2;
      } else {
        this.mType = 1;
      } 
    } 
  }
  
  public int getType() {
    return this.mType;
  }
  
  public byte[] readPages(int paramInt) throws IOException {
    validatePageIndex(paramInt);
    checkConnected();
    byte b = (byte)paramInt;
    return transceive(new byte[] { 48, b }, false);
  }
  
  public void writePage(int paramInt, byte[] paramArrayOfbyte) throws IOException {
    validatePageIndex(paramInt);
    checkConnected();
    byte[] arrayOfByte = new byte[paramArrayOfbyte.length + 2];
    arrayOfByte[0] = -94;
    arrayOfByte[1] = (byte)paramInt;
    System.arraycopy(paramArrayOfbyte, 0, arrayOfByte, 2, paramArrayOfbyte.length);
    transceive(arrayOfByte, false);
  }
  
  public byte[] transceive(byte[] paramArrayOfbyte) throws IOException {
    return transceive(paramArrayOfbyte, true);
  }
  
  public int getMaxTransceiveLength() {
    return getMaxTransceiveLengthInternal();
  }
  
  public void setTimeout(int paramInt) {
    try {
      paramInt = this.mTag.getTagService().setTimeout(9, paramInt);
      if (paramInt != 0) {
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        this("The supplied timeout is not valid");
        throw illegalArgumentException;
      } 
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
    } 
  }
  
  public int getTimeout() {
    try {
      return this.mTag.getTagService().getTimeout(9);
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
      return 0;
    } 
  }
  
  private static void validatePageIndex(int paramInt) {
    if (paramInt >= 0 && paramInt < 256)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("page out of bounds: ");
    stringBuilder.append(paramInt);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
}
