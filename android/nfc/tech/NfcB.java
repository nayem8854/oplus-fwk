package android.nfc.tech;

import android.nfc.Tag;
import android.os.Bundle;
import android.os.RemoteException;
import java.io.IOException;

public final class NfcB extends BasicTagTechnology {
  public static final String EXTRA_APPDATA = "appdata";
  
  public static final String EXTRA_PROTINFO = "protinfo";
  
  private byte[] mAppData;
  
  private byte[] mProtInfo;
  
  public static NfcB get(Tag paramTag) {
    if (!paramTag.hasTech(2))
      return null; 
    try {
      return new NfcB(paramTag);
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public NfcB(Tag paramTag) throws RemoteException {
    super(paramTag, 2);
    Bundle bundle = paramTag.getTechExtras(2);
    this.mAppData = bundle.getByteArray("appdata");
    this.mProtInfo = bundle.getByteArray("protinfo");
  }
  
  public byte[] getApplicationData() {
    return this.mAppData;
  }
  
  public byte[] getProtocolInfo() {
    return this.mProtInfo;
  }
  
  public byte[] transceive(byte[] paramArrayOfbyte) throws IOException {
    return transceive(paramArrayOfbyte, true);
  }
  
  public int getMaxTransceiveLength() {
    return getMaxTransceiveLengthInternal();
  }
}
