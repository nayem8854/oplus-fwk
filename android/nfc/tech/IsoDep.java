package android.nfc.tech;

import android.nfc.Tag;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import java.io.IOException;

public final class IsoDep extends BasicTagTechnology {
  public static final String EXTRA_HIST_BYTES = "histbytes";
  
  public static final String EXTRA_HI_LAYER_RESP = "hiresp";
  
  private static final String TAG = "NFC";
  
  private byte[] mHiLayerResponse = null;
  
  private byte[] mHistBytes = null;
  
  public static IsoDep get(Tag paramTag) {
    if (!paramTag.hasTech(3))
      return null; 
    try {
      return new IsoDep(paramTag);
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public IsoDep(Tag paramTag) throws RemoteException {
    super(paramTag, 3);
    Bundle bundle = paramTag.getTechExtras(3);
    if (bundle != null) {
      this.mHiLayerResponse = bundle.getByteArray("hiresp");
      this.mHistBytes = bundle.getByteArray("histbytes");
    } 
  }
  
  public void setTimeout(int paramInt) {
    try {
      paramInt = this.mTag.getTagService().setTimeout(3, paramInt);
      if (paramInt != 0) {
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        this("The supplied timeout is not valid");
        throw illegalArgumentException;
      } 
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
    } 
  }
  
  public int getTimeout() {
    try {
      return this.mTag.getTagService().getTimeout(3);
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
      return 0;
    } 
  }
  
  public byte[] getHistoricalBytes() {
    return this.mHistBytes;
  }
  
  public byte[] getHiLayerResponse() {
    return this.mHiLayerResponse;
  }
  
  public byte[] transceive(byte[] paramArrayOfbyte) throws IOException {
    return transceive(paramArrayOfbyte, true);
  }
  
  public int getMaxTransceiveLength() {
    return getMaxTransceiveLengthInternal();
  }
  
  public boolean isExtendedLengthApduSupported() {
    try {
      return this.mTag.getTagService().getExtendedLengthApdusSupported();
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
      return false;
    } 
  }
}
