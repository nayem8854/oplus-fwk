package android.nfc.tech;

import android.nfc.Tag;
import android.os.Bundle;
import android.os.RemoteException;
import java.io.IOException;

public final class NfcBarcode extends BasicTagTechnology {
  public static final String EXTRA_BARCODE_TYPE = "barcodetype";
  
  public static final int TYPE_KOVIO = 1;
  
  public static final int TYPE_UNKNOWN = -1;
  
  private int mType;
  
  public static NfcBarcode get(Tag paramTag) {
    if (!paramTag.hasTech(10))
      return null; 
    try {
      return new NfcBarcode(paramTag);
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public NfcBarcode(Tag paramTag) throws RemoteException {
    super(paramTag, 10);
    Bundle bundle = paramTag.getTechExtras(10);
    if (bundle != null) {
      this.mType = bundle.getInt("barcodetype");
      return;
    } 
    throw new NullPointerException("NfcBarcode tech extras are null.");
  }
  
  public int getType() {
    return this.mType;
  }
  
  public byte[] getBarcode() {
    if (this.mType != 1)
      return null; 
    return this.mTag.getId();
  }
}
