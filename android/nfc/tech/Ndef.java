package android.nfc.tech;

import android.nfc.FormatException;
import android.nfc.INfcTag;
import android.nfc.NdefMessage;
import android.nfc.Tag;
import android.nfc.TagLostException;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import java.io.IOException;

public final class Ndef extends BasicTagTechnology {
  public static final String EXTRA_NDEF_CARDSTATE = "ndefcardstate";
  
  public static final String EXTRA_NDEF_MAXLENGTH = "ndefmaxlength";
  
  public static final String EXTRA_NDEF_MSG = "ndefmsg";
  
  public static final String EXTRA_NDEF_TYPE = "ndeftype";
  
  public static final String ICODE_SLI = "com.nxp.ndef.icodesli";
  
  public static final String MIFARE_CLASSIC = "com.nxp.ndef.mifareclassic";
  
  public static final int NDEF_MODE_READ_ONLY = 1;
  
  public static final int NDEF_MODE_READ_WRITE = 2;
  
  public static final int NDEF_MODE_UNKNOWN = 3;
  
  public static final String NFC_FORUM_TYPE_1 = "org.nfcforum.ndef.type1";
  
  public static final String NFC_FORUM_TYPE_2 = "org.nfcforum.ndef.type2";
  
  public static final String NFC_FORUM_TYPE_3 = "org.nfcforum.ndef.type3";
  
  public static final String NFC_FORUM_TYPE_4 = "org.nfcforum.ndef.type4";
  
  private static final String TAG = "NFC";
  
  public static final int TYPE_1 = 1;
  
  public static final int TYPE_2 = 2;
  
  public static final int TYPE_3 = 3;
  
  public static final int TYPE_4 = 4;
  
  public static final int TYPE_ICODE_SLI = 102;
  
  public static final int TYPE_MIFARE_CLASSIC = 101;
  
  public static final int TYPE_OTHER = -1;
  
  public static final String UNKNOWN = "android.ndef.unknown";
  
  private final int mCardState;
  
  private final int mMaxNdefSize;
  
  private final NdefMessage mNdefMsg;
  
  private final int mNdefType;
  
  public static Ndef get(Tag paramTag) {
    if (!paramTag.hasTech(6))
      return null; 
    try {
      return new Ndef(paramTag);
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public Ndef(Tag paramTag) throws RemoteException {
    super(paramTag, 6);
    Bundle bundle = paramTag.getTechExtras(6);
    if (bundle != null) {
      this.mMaxNdefSize = bundle.getInt("ndefmaxlength");
      this.mCardState = bundle.getInt("ndefcardstate");
      this.mNdefMsg = bundle.<NdefMessage>getParcelable("ndefmsg");
      this.mNdefType = bundle.getInt("ndeftype");
      return;
    } 
    throw new NullPointerException("NDEF tech extras are null.");
  }
  
  public NdefMessage getCachedNdefMessage() {
    return this.mNdefMsg;
  }
  
  public String getType() {
    int i = this.mNdefType;
    if (i != 1) {
      if (i != 2) {
        if (i != 3) {
          if (i != 4) {
            if (i != 101) {
              if (i != 102)
                return "android.ndef.unknown"; 
              return "com.nxp.ndef.icodesli";
            } 
            return "com.nxp.ndef.mifareclassic";
          } 
          return "org.nfcforum.ndef.type4";
        } 
        return "org.nfcforum.ndef.type3";
      } 
      return "org.nfcforum.ndef.type2";
    } 
    return "org.nfcforum.ndef.type1";
  }
  
  public int getMaxSize() {
    return this.mMaxNdefSize;
  }
  
  public boolean isWritable() {
    boolean bool;
    if (this.mCardState == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public NdefMessage getNdefMessage() throws IOException, FormatException {
    checkConnected();
    try {
      INfcTag iNfcTag = this.mTag.getTagService();
      if (iNfcTag != null) {
        int i = this.mTag.getServiceHandle();
        if (iNfcTag.isNdef(i)) {
          NdefMessage ndefMessage = iNfcTag.ndefRead(i);
          if (ndefMessage != null || iNfcTag.isPresent(i))
            return ndefMessage; 
          TagLostException tagLostException1 = new TagLostException();
          this();
          throw tagLostException1;
        } 
        if (iNfcTag.isPresent(i))
          return null; 
        TagLostException tagLostException = new TagLostException();
        this();
        throw tagLostException;
      } 
      IOException iOException = new IOException();
      this("Mock tags don't support this operation.");
      throw iOException;
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
      return null;
    } 
  }
  
  public void writeNdefMessage(NdefMessage paramNdefMessage) throws IOException, FormatException {
    checkConnected();
    try {
      INfcTag iNfcTag = this.mTag.getTagService();
      if (iNfcTag != null) {
        int i = this.mTag.getServiceHandle();
        if (iNfcTag.isNdef(i)) {
          i = iNfcTag.ndefWrite(i, paramNdefMessage);
          if (i != -8) {
            if (i != -1) {
              if (i != 0) {
                IOException iOException = new IOException();
                this();
                throw iOException;
              } 
            } else {
              IOException iOException = new IOException();
              this();
              throw iOException;
            } 
          } else {
            FormatException formatException = new FormatException();
            this();
            throw formatException;
          } 
        } else {
          IOException iOException = new IOException();
          this("Tag is not ndef");
          throw iOException;
        } 
      } else {
        IOException iOException = new IOException();
        this("Mock tags don't support this operation.");
        throw iOException;
      } 
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
    } 
  }
  
  public boolean canMakeReadOnly() {
    INfcTag iNfcTag = this.mTag.getTagService();
    if (iNfcTag == null)
      return false; 
    try {
      return iNfcTag.canMakeReadOnly(this.mNdefType);
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
      return false;
    } 
  }
  
  public boolean makeReadOnly() throws IOException {
    checkConnected();
    try {
      INfcTag iNfcTag = this.mTag.getTagService();
      if (iNfcTag == null)
        return false; 
      if (iNfcTag.isNdef(this.mTag.getServiceHandle())) {
        int i = iNfcTag.ndefMakeReadOnly(this.mTag.getServiceHandle());
        if (i != -8) {
          if (i != -1) {
            if (i == 0)
              return true; 
            IOException iOException2 = new IOException();
            this();
            throw iOException2;
          } 
          IOException iOException1 = new IOException();
          this();
          throw iOException1;
        } 
        return false;
      } 
      IOException iOException = new IOException();
      this("Tag is not ndef");
      throw iOException;
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
      return false;
    } 
  }
}
