package android.nfc.tech;

import android.nfc.Tag;
import android.nfc.TagLostException;
import android.os.RemoteException;
import android.util.Log;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class MifareClassic extends BasicTagTechnology {
  public static final int BLOCK_SIZE = 16;
  
  public static final byte[] KEY_DEFAULT = new byte[] { -1, -1, -1, -1, -1, -1 };
  
  public static final byte[] KEY_MIFARE_APPLICATION_DIRECTORY = new byte[] { -96, -95, -94, -93, -92, -91 };
  
  public static final byte[] KEY_NFC_FORUM = new byte[] { -45, -9, -45, -9, -45, -9 };
  
  private static final int MAX_BLOCK_COUNT = 256;
  
  private static final int MAX_SECTOR_COUNT = 40;
  
  public static final int SIZE_1K = 1024;
  
  public static final int SIZE_2K = 2048;
  
  public static final int SIZE_4K = 4096;
  
  public static final int SIZE_MINI = 320;
  
  private static final String TAG = "NFC";
  
  public static final int TYPE_CLASSIC = 0;
  
  public static final int TYPE_PLUS = 1;
  
  public static final int TYPE_PRO = 2;
  
  public static final int TYPE_UNKNOWN = -1;
  
  private boolean mIsEmulated;
  
  private int mSize;
  
  private int mType;
  
  public static MifareClassic get(Tag paramTag) {
    if (!paramTag.hasTech(8))
      return null; 
    try {
      return new MifareClassic(paramTag);
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public MifareClassic(Tag paramTag) throws RemoteException {
    super(paramTag, 8);
    NfcA nfcA = NfcA.get(paramTag);
    this.mIsEmulated = false;
    short s = nfcA.getSak();
    if (s != 1)
      if (s != 40) {
        if (s != 56) {
          if (s != 136) {
            if (s != 152 && s != 184) {
              if (s != 8) {
                if (s != 9) {
                  if (s != 16) {
                    if (s != 17) {
                      if (s != 24) {
                        if (s == 25) {
                          this.mType = 0;
                          this.mSize = 2048;
                        } else {
                          StringBuilder stringBuilder = new StringBuilder();
                          stringBuilder.append("Tag incorrectly enumerated as MIFARE Classic, SAK = ");
                          stringBuilder.append(nfcA.getSak());
                          throw new RuntimeException(stringBuilder.toString());
                        } 
                      } else {
                        this.mType = 0;
                        this.mSize = 4096;
                      } 
                    } else {
                      this.mType = 1;
                      this.mSize = 4096;
                    } 
                  } else {
                    this.mType = 1;
                    this.mSize = 2048;
                  } 
                } else {
                  this.mType = 0;
                  this.mSize = 320;
                } 
                return;
              } 
            } else {
              this.mType = 2;
              this.mSize = 4096;
              return;
            } 
          } else {
            this.mType = 0;
            this.mSize = 1024;
            return;
          } 
        } else {
          this.mType = 0;
          this.mSize = 4096;
          this.mIsEmulated = true;
          return;
        } 
      } else {
        this.mType = 0;
        this.mSize = 1024;
        this.mIsEmulated = true;
        return;
      }  
    this.mType = 0;
    this.mSize = 1024;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public int getSize() {
    return this.mSize;
  }
  
  public boolean isEmulated() {
    return this.mIsEmulated;
  }
  
  public int getSectorCount() {
    int i = this.mSize;
    if (i != 320) {
      if (i != 1024) {
        if (i != 2048) {
          if (i != 4096)
            return 0; 
          return 40;
        } 
        return 32;
      } 
      return 16;
    } 
    return 5;
  }
  
  public int getBlockCount() {
    return this.mSize / 16;
  }
  
  public int getBlockCountInSector(int paramInt) {
    validateSector(paramInt);
    if (paramInt < 32)
      return 4; 
    return 16;
  }
  
  public int blockToSector(int paramInt) {
    validateBlock(paramInt);
    if (paramInt < 128)
      return paramInt / 4; 
    return (paramInt - 128) / 16 + 32;
  }
  
  public int sectorToBlock(int paramInt) {
    if (paramInt < 32)
      return paramInt * 4; 
    return (paramInt - 32) * 16 + 128;
  }
  
  public boolean authenticateSectorWithKeyA(int paramInt, byte[] paramArrayOfbyte) throws IOException {
    return authenticate(paramInt, paramArrayOfbyte, true);
  }
  
  public boolean authenticateSectorWithKeyB(int paramInt, byte[] paramArrayOfbyte) throws IOException {
    return authenticate(paramInt, paramArrayOfbyte, false);
  }
  
  private boolean authenticate(int paramInt, byte[] paramArrayOfbyte, boolean paramBoolean) throws IOException {
    validateSector(paramInt);
    checkConnected();
    byte[] arrayOfByte1 = new byte[12];
    if (paramBoolean) {
      arrayOfByte1[0] = 96;
    } else {
      arrayOfByte1[0] = 97;
    } 
    arrayOfByte1[1] = (byte)sectorToBlock(paramInt);
    byte[] arrayOfByte2 = getTag().getId();
    System.arraycopy(arrayOfByte2, arrayOfByte2.length - 4, arrayOfByte1, 2, 4);
    System.arraycopy(paramArrayOfbyte, 0, arrayOfByte1, 6, 6);
    try {
      paramArrayOfbyte = transceive(arrayOfByte1, false);
      if (paramArrayOfbyte != null)
        return true; 
      return false;
    } catch (TagLostException tagLostException) {
      throw tagLostException;
    } catch (IOException iOException) {
      return false;
    } 
  }
  
  public byte[] readBlock(int paramInt) throws IOException {
    validateBlock(paramInt);
    checkConnected();
    byte b = (byte)paramInt;
    return transceive(new byte[] { 48, b }, false);
  }
  
  public void writeBlock(int paramInt, byte[] paramArrayOfbyte) throws IOException {
    validateBlock(paramInt);
    checkConnected();
    if (paramArrayOfbyte.length == 16) {
      byte[] arrayOfByte = new byte[paramArrayOfbyte.length + 2];
      arrayOfByte[0] = -96;
      arrayOfByte[1] = (byte)paramInt;
      System.arraycopy(paramArrayOfbyte, 0, arrayOfByte, 2, paramArrayOfbyte.length);
      transceive(arrayOfByte, false);
      return;
    } 
    throw new IllegalArgumentException("must write 16-bytes");
  }
  
  public void increment(int paramInt1, int paramInt2) throws IOException {
    validateBlock(paramInt1);
    validateValueOperand(paramInt2);
    checkConnected();
    ByteBuffer byteBuffer = ByteBuffer.allocate(6);
    byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
    byteBuffer.put((byte)-63);
    byteBuffer.put((byte)paramInt1);
    byteBuffer.putInt(paramInt2);
    transceive(byteBuffer.array(), false);
  }
  
  public void decrement(int paramInt1, int paramInt2) throws IOException {
    validateBlock(paramInt1);
    validateValueOperand(paramInt2);
    checkConnected();
    ByteBuffer byteBuffer = ByteBuffer.allocate(6);
    byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
    byteBuffer.put((byte)-64);
    byteBuffer.put((byte)paramInt1);
    byteBuffer.putInt(paramInt2);
    transceive(byteBuffer.array(), false);
  }
  
  public void transfer(int paramInt) throws IOException {
    validateBlock(paramInt);
    checkConnected();
    byte b = (byte)paramInt;
    transceive(new byte[] { -80, b }, false);
  }
  
  public void restore(int paramInt) throws IOException {
    validateBlock(paramInt);
    checkConnected();
    byte b = (byte)paramInt;
    transceive(new byte[] { -62, b }, false);
  }
  
  public byte[] transceive(byte[] paramArrayOfbyte) throws IOException {
    return transceive(paramArrayOfbyte, true);
  }
  
  public int getMaxTransceiveLength() {
    return getMaxTransceiveLengthInternal();
  }
  
  public void setTimeout(int paramInt) {
    try {
      paramInt = this.mTag.getTagService().setTimeout(8, paramInt);
      if (paramInt != 0) {
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        this("The supplied timeout is not valid");
        throw illegalArgumentException;
      } 
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
    } 
  }
  
  public int getTimeout() {
    try {
      return this.mTag.getTagService().getTimeout(8);
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
      return 0;
    } 
  }
  
  private static void validateSector(int paramInt) {
    if (paramInt >= 0 && paramInt < 40)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("sector out of bounds: ");
    stringBuilder.append(paramInt);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  private static void validateBlock(int paramInt) {
    if (paramInt >= 0 && paramInt < 256)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("block out of bounds: ");
    stringBuilder.append(paramInt);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  private static void validateValueOperand(int paramInt) {
    if (paramInt >= 0)
      return; 
    throw new IllegalArgumentException("value operand negative");
  }
}
