package android.nfc.tech;

import android.nfc.Tag;
import android.os.Bundle;
import android.os.RemoteException;
import java.io.IOException;

public final class NfcV extends BasicTagTechnology {
  public static final String EXTRA_DSFID = "dsfid";
  
  public static final String EXTRA_RESP_FLAGS = "respflags";
  
  private byte mDsfId;
  
  private byte mRespFlags;
  
  public static NfcV get(Tag paramTag) {
    if (!paramTag.hasTech(5))
      return null; 
    try {
      return new NfcV(paramTag);
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public NfcV(Tag paramTag) throws RemoteException {
    super(paramTag, 5);
    Bundle bundle = paramTag.getTechExtras(5);
    this.mRespFlags = bundle.getByte("respflags");
    this.mDsfId = bundle.getByte("dsfid");
  }
  
  public byte getResponseFlags() {
    return this.mRespFlags;
  }
  
  public byte getDsfId() {
    return this.mDsfId;
  }
  
  public byte[] transceive(byte[] paramArrayOfbyte) throws IOException {
    return transceive(paramArrayOfbyte, true);
  }
  
  public int getMaxTransceiveLength() {
    return getMaxTransceiveLengthInternal();
  }
}
