package android.nfc.tech;

import android.nfc.Tag;
import android.nfc.TransceiveResult;
import android.os.RemoteException;
import android.util.Log;
import java.io.IOException;

abstract class BasicTagTechnology implements TagTechnology {
  private static final String TAG = "NFC";
  
  boolean mIsConnected;
  
  int mSelectedTechnology;
  
  final Tag mTag;
  
  BasicTagTechnology(Tag paramTag, int paramInt) throws RemoteException {
    this.mTag = paramTag;
    this.mSelectedTechnology = paramInt;
  }
  
  public Tag getTag() {
    return this.mTag;
  }
  
  void checkConnected() {
    if (this.mTag.getConnectedTechnology() == this.mSelectedTechnology) {
      Tag tag = this.mTag;
      if (tag.getConnectedTechnology() != -1)
        return; 
    } 
    throw new IllegalStateException("Call connect() first!");
  }
  
  public boolean isConnected() {
    if (!this.mIsConnected)
      return false; 
    try {
      return this.mTag.getTagService().isPresent(this.mTag.getServiceHandle());
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
      return false;
    } 
  }
  
  public void connect() throws IOException {
    try {
      int i = this.mTag.getTagService().connect(this.mTag.getServiceHandle(), this.mSelectedTechnology);
      if (i == 0) {
        if (this.mTag.setConnectedTechnology(this.mSelectedTechnology)) {
          this.mIsConnected = true;
          return;
        } 
        Log.e("NFC", "Close other technology first!");
        IOException iOException1 = new IOException();
        this("Only one TagTechnology can be connected at a time.");
        throw iOException1;
      } 
      if (i == -21) {
        UnsupportedOperationException unsupportedOperationException = new UnsupportedOperationException();
        this("Connecting to this technology is not supported by the NFC adapter.");
        throw unsupportedOperationException;
      } 
      IOException iOException = new IOException();
      this();
      throw iOException;
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
      throw new IOException("NFC service died");
    } 
  }
  
  public void reconnect() throws IOException {
    if (this.mIsConnected)
      try {
        int i = this.mTag.getTagService().reconnect(this.mTag.getServiceHandle());
        if (i == 0)
          return; 
        this.mIsConnected = false;
        this.mTag.setTechnologyDisconnected();
        IOException iOException = new IOException();
        this();
        throw iOException;
      } catch (RemoteException remoteException) {
        this.mIsConnected = false;
        this.mTag.setTechnologyDisconnected();
        Log.e("NFC", "NFC service dead", (Throwable)remoteException);
        throw new IOException("NFC service died");
      }  
    throw new IllegalStateException("Technology not connected yet");
  }
  
  public void close() throws IOException {
    try {
      this.mTag.getTagService().resetTimeouts();
      this.mTag.getTagService().reconnect(this.mTag.getServiceHandle());
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
    } finally {
      Exception exception;
    } 
    this.mIsConnected = false;
    this.mTag.setTechnologyDisconnected();
  }
  
  int getMaxTransceiveLengthInternal() {
    try {
      return this.mTag.getTagService().getMaxTransceiveLength(this.mSelectedTechnology);
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
      return 0;
    } 
  }
  
  byte[] transceive(byte[] paramArrayOfbyte, boolean paramBoolean) throws IOException {
    checkConnected();
    try {
      TransceiveResult transceiveResult = this.mTag.getTagService().transceive(this.mTag.getServiceHandle(), paramArrayOfbyte, paramBoolean);
      if (transceiveResult != null)
        return transceiveResult.getResponseOrThrow(); 
      IOException iOException = new IOException();
      this("transceive failed");
      throw iOException;
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
      throw new IOException("NFC service died");
    } 
  }
}
