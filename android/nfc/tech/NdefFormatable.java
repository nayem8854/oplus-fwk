package android.nfc.tech;

import android.nfc.FormatException;
import android.nfc.INfcTag;
import android.nfc.NdefMessage;
import android.nfc.Tag;
import android.os.RemoteException;
import android.util.Log;
import java.io.IOException;

public final class NdefFormatable extends BasicTagTechnology {
  private static final String TAG = "NFC";
  
  public static NdefFormatable get(Tag paramTag) {
    if (!paramTag.hasTech(7))
      return null; 
    try {
      return new NdefFormatable(paramTag);
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public NdefFormatable(Tag paramTag) throws RemoteException {
    super(paramTag, 7);
  }
  
  public void format(NdefMessage paramNdefMessage) throws IOException, FormatException {
    format(paramNdefMessage, false);
  }
  
  public void formatReadOnly(NdefMessage paramNdefMessage) throws IOException, FormatException {
    format(paramNdefMessage, true);
  }
  
  void format(NdefMessage paramNdefMessage, boolean paramBoolean) throws IOException, FormatException {
    checkConnected();
    try {
      int i = this.mTag.getServiceHandle();
      INfcTag iNfcTag = this.mTag.getTagService();
      int j = iNfcTag.formatNdef(i, MifareClassic.KEY_DEFAULT);
      if (j != -8) {
        if (j != -1) {
          if (j == 0) {
            if (iNfcTag.isNdef(i)) {
              if (paramNdefMessage != null) {
                j = iNfcTag.ndefWrite(i, paramNdefMessage);
                if (j != -8) {
                  if (j != -1) {
                    if (j != 0) {
                      IOException iOException = new IOException();
                      this();
                      throw iOException;
                    } 
                  } else {
                    IOException iOException = new IOException();
                    this();
                    throw iOException;
                  } 
                } else {
                  FormatException formatException = new FormatException();
                  this();
                  throw formatException;
                } 
              } 
              if (paramBoolean) {
                i = iNfcTag.ndefMakeReadOnly(i);
                if (i != -8) {
                  if (i != -1) {
                    if (i != 0) {
                      IOException iOException = new IOException();
                      this();
                      throw iOException;
                    } 
                  } else {
                    IOException iOException = new IOException();
                    this();
                    throw iOException;
                  } 
                } else {
                  IOException iOException = new IOException();
                  this();
                  throw iOException;
                } 
              } 
            } else {
              IOException iOException = new IOException();
              this();
              throw iOException;
            } 
          } else {
            IOException iOException = new IOException();
            this();
            throw iOException;
          } 
        } else {
          IOException iOException = new IOException();
          this();
          throw iOException;
        } 
      } else {
        FormatException formatException = new FormatException();
        this();
        throw formatException;
      } 
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
    } 
  }
}
