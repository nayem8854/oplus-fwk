package android.nfc.tech;

import android.nfc.Tag;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import java.io.IOException;

public final class NfcF extends BasicTagTechnology {
  private byte[] mSystemCode = null;
  
  private byte[] mManufacturer = null;
  
  private static final String TAG = "NFC";
  
  public static final String EXTRA_SC = "systemcode";
  
  public static final String EXTRA_PMM = "pmm";
  
  public static NfcF get(Tag paramTag) {
    if (!paramTag.hasTech(4))
      return null; 
    try {
      return new NfcF(paramTag);
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public NfcF(Tag paramTag) throws RemoteException {
    super(paramTag, 4);
    Bundle bundle = paramTag.getTechExtras(4);
    if (bundle != null) {
      this.mSystemCode = bundle.getByteArray("systemcode");
      this.mManufacturer = bundle.getByteArray("pmm");
    } 
  }
  
  public byte[] getSystemCode() {
    return this.mSystemCode;
  }
  
  public byte[] getManufacturer() {
    return this.mManufacturer;
  }
  
  public byte[] transceive(byte[] paramArrayOfbyte) throws IOException {
    return transceive(paramArrayOfbyte, true);
  }
  
  public int getMaxTransceiveLength() {
    return getMaxTransceiveLengthInternal();
  }
  
  public void setTimeout(int paramInt) {
    try {
      paramInt = this.mTag.getTagService().setTimeout(4, paramInt);
      if (paramInt != 0) {
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        this("The supplied timeout is not valid");
        throw illegalArgumentException;
      } 
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
    } 
  }
  
  public int getTimeout() {
    try {
      return this.mTag.getTagService().getTimeout(4);
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
      return 0;
    } 
  }
}
