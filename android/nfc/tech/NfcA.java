package android.nfc.tech;

import android.nfc.Tag;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import java.io.IOException;

public final class NfcA extends BasicTagTechnology {
  public static final String EXTRA_ATQA = "atqa";
  
  public static final String EXTRA_SAK = "sak";
  
  private static final String TAG = "NFC";
  
  private byte[] mAtqa;
  
  private short mSak;
  
  public static NfcA get(Tag paramTag) {
    if (!paramTag.hasTech(1))
      return null; 
    try {
      return new NfcA(paramTag);
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public NfcA(Tag paramTag) throws RemoteException {
    super(paramTag, 1);
    this.mSak = 0;
    if (paramTag.hasTech(8)) {
      Bundle bundle1 = paramTag.getTechExtras(8);
      this.mSak = bundle1.getShort("sak");
    } 
    Bundle bundle = paramTag.getTechExtras(1);
    this.mSak = (short)(this.mSak | bundle.getShort("sak"));
    this.mAtqa = bundle.getByteArray("atqa");
  }
  
  public byte[] getAtqa() {
    return this.mAtqa;
  }
  
  public short getSak() {
    return this.mSak;
  }
  
  public byte[] transceive(byte[] paramArrayOfbyte) throws IOException {
    return transceive(paramArrayOfbyte, true);
  }
  
  public int getMaxTransceiveLength() {
    return getMaxTransceiveLengthInternal();
  }
  
  public void setTimeout(int paramInt) {
    try {
      paramInt = this.mTag.getTagService().setTimeout(1, paramInt);
      if (paramInt != 0) {
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        this("The supplied timeout is not valid");
        throw illegalArgumentException;
      } 
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
    } 
  }
  
  public int getTimeout() {
    try {
      return this.mTag.getTagService().getTimeout(1);
    } catch (RemoteException remoteException) {
      Log.e("NFC", "NFC service dead", (Throwable)remoteException);
      return 0;
    } 
  }
}
