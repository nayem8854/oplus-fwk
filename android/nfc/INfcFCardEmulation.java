package android.nfc;

import android.content.ComponentName;
import android.nfc.cardemulation.NfcFServiceInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface INfcFCardEmulation extends IInterface {
  boolean disableNfcFForegroundService() throws RemoteException;
  
  boolean enableNfcFForegroundService(ComponentName paramComponentName) throws RemoteException;
  
  int getMaxNumOfRegisterableSystemCodes() throws RemoteException;
  
  List<NfcFServiceInfo> getNfcFServices(int paramInt) throws RemoteException;
  
  String getNfcid2ForService(int paramInt, ComponentName paramComponentName) throws RemoteException;
  
  String getSystemCodeForService(int paramInt, ComponentName paramComponentName) throws RemoteException;
  
  boolean registerSystemCodeForService(int paramInt, ComponentName paramComponentName, String paramString) throws RemoteException;
  
  boolean removeSystemCodeForService(int paramInt, ComponentName paramComponentName) throws RemoteException;
  
  boolean setNfcid2ForService(int paramInt, ComponentName paramComponentName, String paramString) throws RemoteException;
  
  class Default implements INfcFCardEmulation {
    public String getSystemCodeForService(int param1Int, ComponentName param1ComponentName) throws RemoteException {
      return null;
    }
    
    public boolean registerSystemCodeForService(int param1Int, ComponentName param1ComponentName, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean removeSystemCodeForService(int param1Int, ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public String getNfcid2ForService(int param1Int, ComponentName param1ComponentName) throws RemoteException {
      return null;
    }
    
    public boolean setNfcid2ForService(int param1Int, ComponentName param1ComponentName, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean enableNfcFForegroundService(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean disableNfcFForegroundService() throws RemoteException {
      return false;
    }
    
    public List<NfcFServiceInfo> getNfcFServices(int param1Int) throws RemoteException {
      return null;
    }
    
    public int getMaxNumOfRegisterableSystemCodes() throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INfcFCardEmulation {
    private static final String DESCRIPTOR = "android.nfc.INfcFCardEmulation";
    
    static final int TRANSACTION_disableNfcFForegroundService = 7;
    
    static final int TRANSACTION_enableNfcFForegroundService = 6;
    
    static final int TRANSACTION_getMaxNumOfRegisterableSystemCodes = 9;
    
    static final int TRANSACTION_getNfcFServices = 8;
    
    static final int TRANSACTION_getNfcid2ForService = 4;
    
    static final int TRANSACTION_getSystemCodeForService = 1;
    
    static final int TRANSACTION_registerSystemCodeForService = 2;
    
    static final int TRANSACTION_removeSystemCodeForService = 3;
    
    static final int TRANSACTION_setNfcid2ForService = 5;
    
    public Stub() {
      attachInterface(this, "android.nfc.INfcFCardEmulation");
    }
    
    public static INfcFCardEmulation asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.nfc.INfcFCardEmulation");
      if (iInterface != null && iInterface instanceof INfcFCardEmulation)
        return (INfcFCardEmulation)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 9:
          return "getMaxNumOfRegisterableSystemCodes";
        case 8:
          return "getNfcFServices";
        case 7:
          return "disableNfcFForegroundService";
        case 6:
          return "enableNfcFForegroundService";
        case 5:
          return "setNfcid2ForService";
        case 4:
          return "getNfcid2ForService";
        case 3:
          return "removeSystemCodeForService";
        case 2:
          return "registerSystemCodeForService";
        case 1:
          break;
      } 
      return "getSystemCodeForService";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        List<NfcFServiceInfo> list;
        ComponentName componentName;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 9:
            param1Parcel1.enforceInterface("android.nfc.INfcFCardEmulation");
            param1Int1 = getMaxNumOfRegisterableSystemCodes();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.nfc.INfcFCardEmulation");
            param1Int1 = param1Parcel1.readInt();
            list = getNfcFServices(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 7:
            list.enforceInterface("android.nfc.INfcFCardEmulation");
            bool4 = disableNfcFForegroundService();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 6:
            list.enforceInterface("android.nfc.INfcFCardEmulation");
            if (list.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)list);
            } else {
              list = null;
            } 
            bool4 = enableNfcFForegroundService((ComponentName)list);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 5:
            list.enforceInterface("android.nfc.INfcFCardEmulation");
            m = list.readInt();
            if (list.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)list);
            } else {
              componentName = null;
            } 
            str = list.readString();
            bool3 = setNfcid2ForService(m, componentName, str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 4:
            str.enforceInterface("android.nfc.INfcFCardEmulation");
            k = str.readInt();
            if (str.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            str = getNfcid2ForService(k, (ComponentName)str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str);
            return true;
          case 3:
            str.enforceInterface("android.nfc.INfcFCardEmulation");
            k = str.readInt();
            if (str.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            bool2 = removeSystemCodeForService(k, (ComponentName)str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 2:
            str.enforceInterface("android.nfc.INfcFCardEmulation");
            j = str.readInt();
            if (str.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)str);
            } else {
              componentName = null;
            } 
            str = str.readString();
            bool1 = registerSystemCodeForService(j, componentName, str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        str.enforceInterface("android.nfc.INfcFCardEmulation");
        int i = str.readInt();
        if (str.readInt() != 0) {
          ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str);
        } else {
          str = null;
        } 
        String str = getSystemCodeForService(i, (ComponentName)str);
        param1Parcel2.writeNoException();
        param1Parcel2.writeString(str);
        return true;
      } 
      param1Parcel2.writeString("android.nfc.INfcFCardEmulation");
      return true;
    }
    
    private static class Proxy implements INfcFCardEmulation {
      public static INfcFCardEmulation sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.nfc.INfcFCardEmulation";
      }
      
      public String getSystemCodeForService(int param2Int, ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcFCardEmulation");
          parcel1.writeInt(param2Int);
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && INfcFCardEmulation.Stub.getDefaultImpl() != null)
            return INfcFCardEmulation.Stub.getDefaultImpl().getSystemCodeForService(param2Int, param2ComponentName); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean registerSystemCodeForService(int param2Int, ComponentName param2ComponentName, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcFCardEmulation");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool2 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool2 && INfcFCardEmulation.Stub.getDefaultImpl() != null) {
            bool1 = INfcFCardEmulation.Stub.getDefaultImpl().registerSystemCodeForService(param2Int, param2ComponentName, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeSystemCodeForService(int param2Int, ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcFCardEmulation");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool2 && INfcFCardEmulation.Stub.getDefaultImpl() != null) {
            bool1 = INfcFCardEmulation.Stub.getDefaultImpl().removeSystemCodeForService(param2Int, param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getNfcid2ForService(int param2Int, ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcFCardEmulation");
          parcel1.writeInt(param2Int);
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && INfcFCardEmulation.Stub.getDefaultImpl() != null)
            return INfcFCardEmulation.Stub.getDefaultImpl().getNfcid2ForService(param2Int, param2ComponentName); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setNfcid2ForService(int param2Int, ComponentName param2ComponentName, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcFCardEmulation");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool2 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool2 && INfcFCardEmulation.Stub.getDefaultImpl() != null) {
            bool1 = INfcFCardEmulation.Stub.getDefaultImpl().setNfcid2ForService(param2Int, param2ComponentName, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean enableNfcFForegroundService(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcFCardEmulation");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool2 && INfcFCardEmulation.Stub.getDefaultImpl() != null) {
            bool1 = INfcFCardEmulation.Stub.getDefaultImpl().enableNfcFForegroundService(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean disableNfcFForegroundService() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcFCardEmulation");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && INfcFCardEmulation.Stub.getDefaultImpl() != null) {
            bool1 = INfcFCardEmulation.Stub.getDefaultImpl().disableNfcFForegroundService();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<NfcFServiceInfo> getNfcFServices(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcFCardEmulation");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && INfcFCardEmulation.Stub.getDefaultImpl() != null)
            return INfcFCardEmulation.Stub.getDefaultImpl().getNfcFServices(param2Int); 
          parcel2.readException();
          return parcel2.createTypedArrayList(NfcFServiceInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getMaxNumOfRegisterableSystemCodes() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.nfc.INfcFCardEmulation");
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && INfcFCardEmulation.Stub.getDefaultImpl() != null)
            return INfcFCardEmulation.Stub.getDefaultImpl().getMaxNumOfRegisterableSystemCodes(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INfcFCardEmulation param1INfcFCardEmulation) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INfcFCardEmulation != null) {
          Proxy.sDefaultImpl = param1INfcFCardEmulation;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INfcFCardEmulation getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
