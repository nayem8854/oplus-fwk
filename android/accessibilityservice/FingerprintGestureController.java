package android.accessibilityservice;

import android.os.Handler;
import android.os.RemoteException;
import android.util.ArrayMap;
import android.util.Log;

public final class FingerprintGestureController {
  private final Object mLock = new Object();
  
  private final ArrayMap<FingerprintGestureCallback, Handler> mCallbackHandlerMap = new ArrayMap(1);
  
  private final IAccessibilityServiceConnection mAccessibilityServiceConnection;
  
  private static final String LOG_TAG = "FingerprintGestureController";
  
  public static final int FINGERPRINT_GESTURE_SWIPE_UP = 4;
  
  public static final int FINGERPRINT_GESTURE_SWIPE_RIGHT = 1;
  
  public static final int FINGERPRINT_GESTURE_SWIPE_LEFT = 2;
  
  public static final int FINGERPRINT_GESTURE_SWIPE_DOWN = 8;
  
  public FingerprintGestureController(IAccessibilityServiceConnection paramIAccessibilityServiceConnection) {
    this.mAccessibilityServiceConnection = paramIAccessibilityServiceConnection;
  }
  
  public boolean isGestureDetectionAvailable() {
    try {
      return this.mAccessibilityServiceConnection.isFingerprintGestureDetectionAvailable();
    } catch (RemoteException remoteException) {
      Log.w("FingerprintGestureController", "Failed to check if fingerprint gestures are active", (Throwable)remoteException);
      remoteException.rethrowFromSystemServer();
      return false;
    } 
  }
  
  public void registerFingerprintGestureCallback(FingerprintGestureCallback paramFingerprintGestureCallback, Handler paramHandler) {
    synchronized (this.mLock) {
      this.mCallbackHandlerMap.put(paramFingerprintGestureCallback, paramHandler);
      return;
    } 
  }
  
  public void unregisterFingerprintGestureCallback(FingerprintGestureCallback paramFingerprintGestureCallback) {
    synchronized (this.mLock) {
      this.mCallbackHandlerMap.remove(paramFingerprintGestureCallback);
      return;
    } 
  }
  
  public void onGestureDetectionActiveChanged(boolean paramBoolean) {
    synchronized (this.mLock) {
      ArrayMap arrayMap = new ArrayMap();
      this(this.mCallbackHandlerMap);
      int i = arrayMap.size();
      for (byte b = 0; b < i; b++) {
        null = arrayMap.keyAt(b);
        Handler handler = (Handler)arrayMap.valueAt(b);
        if (handler != null) {
          handler.post(new _$$Lambda$FingerprintGestureController$M_ZApqp96G6ZF2WdWrGDJ8Qsfck((FingerprintGestureCallback)null, paramBoolean));
        } else {
          null.onGestureDetectionAvailabilityChanged(paramBoolean);
        } 
      } 
      return;
    } 
  }
  
  public void onGesture(int paramInt) {
    synchronized (this.mLock) {
      ArrayMap arrayMap = new ArrayMap();
      this(this.mCallbackHandlerMap);
      int i = arrayMap.size();
      for (byte b = 0; b < i; b++) {
        null = arrayMap.keyAt(b);
        Handler handler = (Handler)arrayMap.valueAt(b);
        if (handler != null) {
          handler.post(new _$$Lambda$FingerprintGestureController$BQjrQQom4K3C98FNiI0fi7SvHfY((FingerprintGestureCallback)null, paramInt));
        } else {
          null.onGestureDetected(paramInt);
        } 
      } 
      return;
    } 
  }
  
  public static abstract class FingerprintGestureCallback {
    public void onGestureDetectionAvailabilityChanged(boolean param1Boolean) {}
    
    public void onGestureDetected(int param1Int) {}
  }
}
