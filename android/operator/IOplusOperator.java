package android.operator;

import android.os.Bundle;
import java.util.Map;

public interface IOplusOperator {
  String getActiveSimOperator();
  
  String getActiveSimRegion();
  
  Map getConfigMap(String paramString);
  
  void grantCustomizedRuntimePermissions();
  
  boolean isInSimTriggeredSystemBlackList(String paramString);
  
  void notifyRegionSwitch(Bundle paramBundle);
  
  void notifySimSwitch(Bundle paramBundle);
  
  void notifySmartCustomizationStart();
  
  void testAidl();
}
