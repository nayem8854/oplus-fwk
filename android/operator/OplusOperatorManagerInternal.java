package android.operator;

import java.util.HashSet;

public abstract class OplusOperatorManagerInternal {
  public abstract HashSet<String> getGrantedRuntimePermissionsPostInstall(String paramString);
  
  public abstract HashSet<String> getGrantedRuntimePermissionsPreload(String paramString, boolean paramBoolean);
  
  public abstract void testInternal();
}
