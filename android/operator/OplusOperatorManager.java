package android.operator;

import android.os.Bundle;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.util.Log;
import java.util.Collections;
import java.util.Map;

public class OplusOperatorManager implements IOplusOperator {
  @Deprecated
  public static final boolean SERVICE_ENABLED = true;
  
  public static final String SERVICE_NAME = "operator";
  
  private static final String TAG = "OplusOperatorManager";
  
  private static final Map mEmptyMap;
  
  private static volatile OplusOperatorManager sManager = null;
  
  private IOplusOperatorManager mService;
  
  static {
    mEmptyMap = Collections.emptyMap();
  }
  
  private OplusOperatorManager(IOplusOperatorManager paramIOplusOperatorManager) {
    this.mService = paramIOplusOperatorManager;
  }
  
  public static OplusOperatorManager getInstance() {
    // Byte code:
    //   0: getstatic android/operator/OplusOperatorManager.sManager : Landroid/operator/OplusOperatorManager;
    //   3: ifnonnull -> 98
    //   6: ldc android/operator/OplusOperatorManager
    //   8: monitorenter
    //   9: getstatic android/operator/OplusOperatorManager.sManager : Landroid/operator/OplusOperatorManager;
    //   12: ifnonnull -> 86
    //   15: ldc 'operator'
    //   17: invokestatic getService : (Ljava/lang/String;)Landroid/os/IBinder;
    //   20: invokestatic asInterface : (Landroid/os/IBinder;)Landroid/operator/IOplusOperatorManager;
    //   23: astore_0
    //   24: aload_0
    //   25: ifnull -> 44
    //   28: new android/operator/OplusOperatorManager
    //   31: astore_1
    //   32: aload_1
    //   33: aload_0
    //   34: invokespecial <init> : (Landroid/operator/IOplusOperatorManager;)V
    //   37: aload_1
    //   38: putstatic android/operator/OplusOperatorManager.sManager : Landroid/operator/OplusOperatorManager;
    //   41: goto -> 86
    //   44: new java/lang/StringBuilder
    //   47: astore_1
    //   48: aload_1
    //   49: invokespecial <init> : ()V
    //   52: aload_1
    //   53: ldc 'Whoops, service not initiated yet , print caller stack '
    //   55: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   58: pop
    //   59: aload_1
    //   60: bipush #9
    //   62: invokestatic getCallers : (I)Ljava/lang/String;
    //   65: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   68: pop
    //   69: aload_1
    //   70: invokevirtual toString : ()Ljava/lang/String;
    //   73: astore_1
    //   74: ldc 'OplusOperatorManager'
    //   76: aload_1
    //   77: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   80: pop
    //   81: ldc android/operator/OplusOperatorManager
    //   83: monitorexit
    //   84: aconst_null
    //   85: areturn
    //   86: ldc android/operator/OplusOperatorManager
    //   88: monitorexit
    //   89: goto -> 98
    //   92: astore_1
    //   93: ldc android/operator/OplusOperatorManager
    //   95: monitorexit
    //   96: aload_1
    //   97: athrow
    //   98: getstatic android/operator/OplusOperatorManager.sManager : Landroid/operator/OplusOperatorManager;
    //   101: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #49	-> 0
    //   #50	-> 6
    //   #51	-> 9
    //   #52	-> 15
    //   #53	-> 15
    //   #54	-> 24
    //   #55	-> 28
    //   #57	-> 44
    //   #58	-> 59
    //   #57	-> 74
    //   #59	-> 81
    //   #62	-> 86
    //   #64	-> 98
    // Exception table:
    //   from	to	target	type
    //   9	15	92	finally
    //   15	24	92	finally
    //   28	41	92	finally
    //   44	59	92	finally
    //   59	74	92	finally
    //   74	81	92	finally
    //   81	84	92	finally
    //   86	89	92	finally
    //   93	96	92	finally
  }
  
  public void testAidl() {
    try {
      if (this.mService != null)
        this.mService.testAidl(); 
    } catch (RemoteException remoteException) {
      Log.e("OplusOperatorManager", "testAidl ", (Throwable)remoteException);
    } 
  }
  
  public Map getConfigMap(String paramString) {
    Map map1 = null, map2 = null;
    Bundle bundle = null;
    Map map3 = map1, map4 = map2;
    try {
      Map map;
      if (this.mService != null) {
        map3 = map1;
        map4 = map2;
        long l1 = System.currentTimeMillis();
        map3 = map1;
        map4 = map2;
        bundle = new Bundle();
        map3 = map1;
        map4 = map2;
        this();
        map3 = map1;
        map4 = map2;
        bundle.putString("config", paramString);
        map3 = map1;
        map4 = map2;
        map = this.mService.getConfigMap(bundle);
        map3 = map;
        map4 = map;
        long l2 = System.currentTimeMillis();
        map3 = map;
        map4 = map;
        StringBuilder stringBuilder = new StringBuilder();
        map3 = map;
        map4 = map;
        this();
        map3 = map;
        map4 = map;
        stringBuilder.append("getConfigMap ");
        map3 = map;
        map4 = map;
        stringBuilder.append(paramString);
        map3 = map;
        map4 = map;
        stringBuilder.append(" took ");
        map3 = map;
        map4 = map;
        stringBuilder.append(l2 - l1);
        map3 = map;
        map4 = map;
        stringBuilder.append("ms");
        map3 = map;
        map4 = map;
        Log.i("OplusOperatorManager", stringBuilder.toString());
      } 
      return map;
    } catch (RemoteException remoteException) {
      map3 = map4;
      Log.e("OplusOperatorManager", "getConfigMap ", (Throwable)remoteException);
      map3 = map4;
    } finally {}
    return map3;
  }
  
  public void grantCustomizedRuntimePermissions() {
    try {
      if (this.mService != null) {
        long l1 = System.currentTimeMillis();
        this.mService.grantCustomizedRuntimePermissions();
        long l2 = System.currentTimeMillis();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("grantCustomizedPermissions  took ");
        stringBuilder.append(l2 - l1);
        stringBuilder.append("ms");
        Log.i("OplusOperatorManager", stringBuilder.toString());
      } 
    } catch (RemoteException remoteException) {
      Log.e("OplusOperatorManager", "grantCustomizedPermissions ", (Throwable)remoteException);
    } 
  }
  
  public void notifySmartCustomizationStart() {
    try {
      if (this.mService != null)
        this.mService.notifySmartCustomizationStart(); 
    } catch (RemoteException remoteException) {
      Log.e("OplusOperatorManager", "notifySmartCustomizationStart ", (Throwable)remoteException);
    } 
  }
  
  public boolean isInSimTriggeredSystemBlackList(String paramString) {
    boolean bool = false;
    try {
      if (this.mService != null)
        bool = this.mService.isInSimTriggeredSystemBlackList(paramString); 
      return bool;
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("isInSimTriggeredSystemBlackList ");
      stringBuilder.append(paramString);
      Log.e("OplusOperatorManager", stringBuilder.toString(), (Throwable)remoteException);
    } finally {}
    return false;
  }
  
  public String getActiveSimOperator() {
    return SystemProperties.get("persist.sys.oplus.operator.opta");
  }
  
  public String getActiveSimRegion() {
    return SystemProperties.get("persist.sys.oplus.operator.optb");
  }
  
  public void notifySimSwitch(Bundle paramBundle) {
    try {
      if (this.mService != null)
        this.mService.notifySimSwitch(paramBundle); 
    } catch (RemoteException remoteException) {
      Log.e("OplusOperatorManager", "notifiySimSwitch ", (Throwable)remoteException);
    } 
  }
  
  public void notifyRegionSwitch(Bundle paramBundle) {
    try {
      if (this.mService != null)
        this.mService.notifyRegionSwitch(paramBundle); 
    } catch (RemoteException remoteException) {
      Log.e("OplusOperatorManager", "notifyRegionSwitch ", (Throwable)remoteException);
    } 
  }
  
  class OplusOperatorDummyManager extends OplusOperatorManager implements IOplusOperator {
    private OplusOperatorDummyManager(OplusOperatorManager this$0) {
      super((IOplusOperatorManager)this$0);
    }
    
    public void testAidl() {
      Log.i("OplusOperatorManager", "dummy testAidl");
    }
    
    public Map getConfigMap(String param1String) {
      return OplusOperatorManager.mEmptyMap;
    }
    
    public void grantCustomizedRuntimePermissions() {
      Log.i("OplusOperatorManager", "dummy grantCustomizedPermissions");
    }
    
    public void notifySmartCustomizationStart() {
      Log.i("OplusOperatorManager", "dummy notifySmartCustomizationStart");
    }
    
    public boolean isInSimTriggeredSystemBlackList(String param1String) {
      return false;
    }
  }
}
