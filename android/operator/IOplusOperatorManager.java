package android.operator;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.Map;

public interface IOplusOperatorManager extends IInterface {
  Map getConfigMap(Bundle paramBundle) throws RemoteException;
  
  void grantCustomizedRuntimePermissions() throws RemoteException;
  
  boolean isInSimTriggeredSystemBlackList(String paramString) throws RemoteException;
  
  void notifyRegionSwitch(Bundle paramBundle) throws RemoteException;
  
  void notifySimSwitch(Bundle paramBundle) throws RemoteException;
  
  void notifySmartCustomizationStart() throws RemoteException;
  
  void testAidl() throws RemoteException;
  
  class Default implements IOplusOperatorManager {
    public void testAidl() throws RemoteException {}
    
    public Map getConfigMap(Bundle param1Bundle) throws RemoteException {
      return null;
    }
    
    public void grantCustomizedRuntimePermissions() throws RemoteException {}
    
    public void notifySmartCustomizationStart() throws RemoteException {}
    
    public boolean isInSimTriggeredSystemBlackList(String param1String) throws RemoteException {
      return false;
    }
    
    public void notifySimSwitch(Bundle param1Bundle) throws RemoteException {}
    
    public void notifyRegionSwitch(Bundle param1Bundle) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusOperatorManager {
    private static final String DESCRIPTOR = "android.operator.IOplusOperatorManager";
    
    static final int TRANSACTION_getConfigMap = 2;
    
    static final int TRANSACTION_grantCustomizedRuntimePermissions = 3;
    
    static final int TRANSACTION_isInSimTriggeredSystemBlackList = 5;
    
    static final int TRANSACTION_notifyRegionSwitch = 7;
    
    static final int TRANSACTION_notifySimSwitch = 6;
    
    static final int TRANSACTION_notifySmartCustomizationStart = 4;
    
    static final int TRANSACTION_testAidl = 1;
    
    public Stub() {
      attachInterface(this, "android.operator.IOplusOperatorManager");
    }
    
    public static IOplusOperatorManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.operator.IOplusOperatorManager");
      if (iInterface != null && iInterface instanceof IOplusOperatorManager)
        return (IOplusOperatorManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "notifyRegionSwitch";
        case 6:
          return "notifySimSwitch";
        case 5:
          return "isInSimTriggeredSystemBlackList";
        case 4:
          return "notifySmartCustomizationStart";
        case 3:
          return "grantCustomizedRuntimePermissions";
        case 2:
          return "getConfigMap";
        case 1:
          break;
      } 
      return "testAidl";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        String str;
        Map map;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("android.operator.IOplusOperatorManager");
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifyRegionSwitch((Bundle)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.operator.IOplusOperatorManager");
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifySimSwitch((Bundle)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.operator.IOplusOperatorManager");
            str = param1Parcel1.readString();
            bool = isInSimTriggeredSystemBlackList(str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 4:
            str.enforceInterface("android.operator.IOplusOperatorManager");
            notifySmartCustomizationStart();
            param1Parcel2.writeNoException();
            return true;
          case 3:
            str.enforceInterface("android.operator.IOplusOperatorManager");
            grantCustomizedRuntimePermissions();
            return true;
          case 2:
            str.enforceInterface("android.operator.IOplusOperatorManager");
            if (str.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            map = getConfigMap((Bundle)str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeMap(map);
            return true;
          case 1:
            break;
        } 
        map.enforceInterface("android.operator.IOplusOperatorManager");
        testAidl();
        return true;
      } 
      param1Parcel2.writeString("android.operator.IOplusOperatorManager");
      return true;
    }
    
    private static class Proxy implements IOplusOperatorManager {
      public static IOplusOperatorManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.operator.IOplusOperatorManager";
      }
      
      public void testAidl() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.operator.IOplusOperatorManager");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IOplusOperatorManager.Stub.getDefaultImpl() != null) {
            IOplusOperatorManager.Stub.getDefaultImpl().testAidl();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public Map getConfigMap(Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.operator.IOplusOperatorManager");
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusOperatorManager.Stub.getDefaultImpl() != null)
            return IOplusOperatorManager.Stub.getDefaultImpl().getConfigMap(param2Bundle); 
          parcel2.readException();
          ClassLoader classLoader = getClass().getClassLoader();
          return parcel2.readHashMap(classLoader);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void grantCustomizedRuntimePermissions() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.operator.IOplusOperatorManager");
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IOplusOperatorManager.Stub.getDefaultImpl() != null) {
            IOplusOperatorManager.Stub.getDefaultImpl().grantCustomizedRuntimePermissions();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifySmartCustomizationStart() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.operator.IOplusOperatorManager");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOplusOperatorManager.Stub.getDefaultImpl() != null) {
            IOplusOperatorManager.Stub.getDefaultImpl().notifySmartCustomizationStart();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isInSimTriggeredSystemBlackList(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.operator.IOplusOperatorManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IOplusOperatorManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusOperatorManager.Stub.getDefaultImpl().isInSimTriggeredSystemBlackList(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifySimSwitch(Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.operator.IOplusOperatorManager");
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IOplusOperatorManager.Stub.getDefaultImpl() != null) {
            IOplusOperatorManager.Stub.getDefaultImpl().notifySimSwitch(param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyRegionSwitch(Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.operator.IOplusOperatorManager");
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IOplusOperatorManager.Stub.getDefaultImpl() != null) {
            IOplusOperatorManager.Stub.getDefaultImpl().notifyRegionSwitch(param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusOperatorManager param1IOplusOperatorManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusOperatorManager != null) {
          Proxy.sDefaultImpl = param1IOplusOperatorManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusOperatorManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
