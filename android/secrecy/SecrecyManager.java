package android.secrecy;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.util.Log;
import android.util.Slog;
import java.util.Map;

public class SecrecyManager {
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private Object mLock = new Object();
  
  private boolean mIsEncryptLog = false;
  
  private boolean mIsEncryptApp = false;
  
  private boolean mIsEncryptAdb = false;
  
  public static final int ADB_TYPE = 4;
  
  public static final int ALL_TYPE = 255;
  
  public static final int APP_TYPE = 2;
  
  public static final int LOG_TYPE = 1;
  
  private static final int MSG_SECRECY_STATE_CHANGED = 1;
  
  private static final String TAG = "SecrecyManager";
  
  private static final String TAG_LOG = "SecrecyLog";
  
  private Context mContext;
  
  private Handler mHandler;
  
  private ISecrecyServiceReceiver mReceiver;
  
  private SecrecyStateListener mSecrecyStateListener;
  
  private ISecrecyService mService;
  
  public boolean getSecrecyState(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 4)
          return false; 
        return this.mIsEncryptAdb;
      } 
      return this.mIsEncryptApp;
    } 
    return this.mIsEncryptLog;
  }
  
  private void updateSecrecyState(Map paramMap) {
    Integer integer = Integer.valueOf(1);
    if (paramMap.get(integer) != null) {
      boolean bool = ((Boolean)paramMap.get(integer)).booleanValue();
      if (this.mIsEncryptLog != bool) {
        this.mIsEncryptLog = bool;
        notifySecrecyStateChanged(1, bool);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("updateSecrecyState LOG_TYPE = ");
        stringBuilder.append(this.mIsEncryptLog);
        Log.d("SecrecyManager", stringBuilder.toString());
      } 
    } 
    if (paramMap.get(Integer.valueOf(2)) != null) {
      boolean bool = ((Boolean)paramMap.get(Integer.valueOf(2))).booleanValue();
      if (this.mIsEncryptApp != bool) {
        this.mIsEncryptApp = bool;
        notifySecrecyStateChanged(2, bool);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("updateSecrecyState APP_TYPE = ");
        stringBuilder.append(this.mIsEncryptApp);
        Log.d("SecrecyManager", stringBuilder.toString());
      } 
    } 
    if (paramMap.get(Integer.valueOf(4)) != null) {
      boolean bool = ((Boolean)paramMap.get(Integer.valueOf(4))).booleanValue();
      if (this.mIsEncryptAdb != bool) {
        this.mIsEncryptAdb = bool;
        notifySecrecyStateChanged(4, bool);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("updateSecrecyState ADB_TYPE = ");
        stringBuilder.append(this.mIsEncryptAdb);
        Log.d("SecrecyManager", stringBuilder.toString());
      } 
    } 
  }
  
  class MyHandler extends Handler {
    final SecrecyManager this$0;
    
    private MyHandler(Context param1Context) {
      super(param1Context.getMainLooper());
    }
    
    private MyHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      if (param1Message.what == 1)
        SecrecyManager.this.updateSecrecyState((Map)param1Message.obj); 
    }
  }
  
  private void getSecrecyStateFromService() {
    try {
      boolean bool = this.mService.getSecrecyState(1);
      if (bool != this.mIsEncryptLog) {
        this.mIsEncryptLog = bool;
        notifySecrecyStateChanged(1, bool);
      } 
    } catch (RemoteException remoteException) {}
    try {
      boolean bool = this.mService.getSecrecyState(2);
      if (bool != this.mIsEncryptApp) {
        this.mIsEncryptApp = bool;
        notifySecrecyStateChanged(2, bool);
      } 
    } catch (RemoteException remoteException) {}
    try {
      boolean bool = this.mService.getSecrecyState(4);
      if (bool != this.mIsEncryptAdb) {
        this.mIsEncryptAdb = bool;
        notifySecrecyStateChanged(4, bool);
      } 
    } catch (RemoteException remoteException) {}
  }
  
  public String generateTokenFromKey() {
    try {
      return this.mService.generateTokenFromKey();
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public boolean isKeyImported() {
    try {
      return this.mService.isKeyImported();
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public byte[] generateCipherFromKey(int paramInt) {
    try {
      return this.mService.generateCipherFromKey(paramInt);
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public SecrecyManager(Context paramContext, ISecrecyService paramISecrecyService) {
    this.mReceiver = (ISecrecyServiceReceiver)new Object(this);
    this.mContext = paramContext;
    this.mService = paramISecrecyService;
    if (paramISecrecyService == null) {
      Slog.e("SecrecyManager", "SecrecyService was null!");
      return;
    } 
    getSecrecyStateFromService();
    try {
      paramISecrecyService.registerSecrecyServiceReceiver(this.mReceiver);
    } catch (RemoteException remoteException) {}
    this.mHandler = new MyHandler(paramContext);
  }
  
  public static abstract class SecrecyStateListener {
    public void onSecrecyStateChanged(int param1Int, boolean param1Boolean) {}
  }
  
  public void setSecrecyStateListener(SecrecyStateListener paramSecrecyStateListener) {
    this.mSecrecyStateListener = paramSecrecyStateListener;
  }
  
  void notifySecrecyStateChanged(int paramInt, boolean paramBoolean) {
    SecrecyStateListener secrecyStateListener = this.mSecrecyStateListener;
    if (secrecyStateListener != null)
      secrecyStateListener.onSecrecyStateChanged(paramInt, paramBoolean); 
  }
}
