package android.secrecy;

import android.content.pm.ActivityInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISecrecyService extends IInterface {
  byte[] generateCipherFromKey(int paramInt) throws RemoteException;
  
  String generateTokenFromKey() throws RemoteException;
  
  boolean getSecrecyState(int paramInt) throws RemoteException;
  
  boolean isInEncryptedAppList(ActivityInfo paramActivityInfo, String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  boolean isKeyImported() throws RemoteException;
  
  boolean isSecrecySupport() throws RemoteException;
  
  boolean registerSecrecyServiceReceiver(ISecrecyServiceReceiver paramISecrecyServiceReceiver) throws RemoteException;
  
  class Default implements ISecrecyService {
    public boolean getSecrecyState(int param1Int) throws RemoteException {
      return false;
    }
    
    public String generateTokenFromKey() throws RemoteException {
      return null;
    }
    
    public boolean isKeyImported() throws RemoteException {
      return false;
    }
    
    public byte[] generateCipherFromKey(int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean registerSecrecyServiceReceiver(ISecrecyServiceReceiver param1ISecrecyServiceReceiver) throws RemoteException {
      return false;
    }
    
    public boolean isSecrecySupport() throws RemoteException {
      return false;
    }
    
    public boolean isInEncryptedAppList(ActivityInfo param1ActivityInfo, String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISecrecyService {
    private static final String DESCRIPTOR = "android.secrecy.ISecrecyService";
    
    static final int TRANSACTION_generateCipherFromKey = 4;
    
    static final int TRANSACTION_generateTokenFromKey = 2;
    
    static final int TRANSACTION_getSecrecyState = 1;
    
    static final int TRANSACTION_isInEncryptedAppList = 7;
    
    static final int TRANSACTION_isKeyImported = 3;
    
    static final int TRANSACTION_isSecrecySupport = 6;
    
    static final int TRANSACTION_registerSecrecyServiceReceiver = 5;
    
    public Stub() {
      attachInterface(this, "android.secrecy.ISecrecyService");
    }
    
    public static ISecrecyService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.secrecy.ISecrecyService");
      if (iInterface != null && iInterface instanceof ISecrecyService)
        return (ISecrecyService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "isInEncryptedAppList";
        case 6:
          return "isSecrecySupport";
        case 5:
          return "registerSecrecyServiceReceiver";
        case 4:
          return "generateCipherFromKey";
        case 3:
          return "isKeyImported";
        case 2:
          return "generateTokenFromKey";
        case 1:
          break;
      } 
      return "getSecrecyState";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool3;
        int j;
        boolean bool2;
        ISecrecyServiceReceiver iSecrecyServiceReceiver;
        byte[] arrayOfByte;
        String str1;
        ActivityInfo activityInfo;
        String str2;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("android.secrecy.ISecrecyService");
            if (param1Parcel1.readInt() != 0) {
              activityInfo = ActivityInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              activityInfo = null;
            } 
            str2 = param1Parcel1.readString();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            bool3 = isInEncryptedAppList(activityInfo, str2, param1Int2, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.secrecy.ISecrecyService");
            bool3 = isSecrecySupport();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.secrecy.ISecrecyService");
            iSecrecyServiceReceiver = ISecrecyServiceReceiver.Stub.asInterface(param1Parcel1.readStrongBinder());
            bool3 = registerSecrecyServiceReceiver(iSecrecyServiceReceiver);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 4:
            iSecrecyServiceReceiver.enforceInterface("android.secrecy.ISecrecyService");
            j = iSecrecyServiceReceiver.readInt();
            arrayOfByte = generateCipherFromKey(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte);
            return true;
          case 3:
            arrayOfByte.enforceInterface("android.secrecy.ISecrecyService");
            bool2 = isKeyImported();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 2:
            arrayOfByte.enforceInterface("android.secrecy.ISecrecyService");
            str1 = generateTokenFromKey();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.secrecy.ISecrecyService");
        int i = str1.readInt();
        boolean bool1 = getSecrecyState(i);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("android.secrecy.ISecrecyService");
      return true;
    }
    
    private static class Proxy implements ISecrecyService {
      public static ISecrecyService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.secrecy.ISecrecyService";
      }
      
      public boolean getSecrecyState(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.secrecy.ISecrecyService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && ISecrecyService.Stub.getDefaultImpl() != null) {
            bool1 = ISecrecyService.Stub.getDefaultImpl().getSecrecyState(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String generateTokenFromKey() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.secrecy.ISecrecyService");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ISecrecyService.Stub.getDefaultImpl() != null)
            return ISecrecyService.Stub.getDefaultImpl().generateTokenFromKey(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isKeyImported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.secrecy.ISecrecyService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && ISecrecyService.Stub.getDefaultImpl() != null) {
            bool1 = ISecrecyService.Stub.getDefaultImpl().isKeyImported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] generateCipherFromKey(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.secrecy.ISecrecyService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ISecrecyService.Stub.getDefaultImpl() != null)
            return ISecrecyService.Stub.getDefaultImpl().generateCipherFromKey(param2Int); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean registerSecrecyServiceReceiver(ISecrecyServiceReceiver param2ISecrecyServiceReceiver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.secrecy.ISecrecyService");
          if (param2ISecrecyServiceReceiver != null) {
            iBinder = param2ISecrecyServiceReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && ISecrecyService.Stub.getDefaultImpl() != null) {
            bool1 = ISecrecyService.Stub.getDefaultImpl().registerSecrecyServiceReceiver(param2ISecrecyServiceReceiver);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSecrecySupport() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.secrecy.ISecrecyService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && ISecrecyService.Stub.getDefaultImpl() != null) {
            bool1 = ISecrecyService.Stub.getDefaultImpl().isSecrecySupport();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isInEncryptedAppList(ActivityInfo param2ActivityInfo, String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.secrecy.ISecrecyService");
          boolean bool1 = true;
          if (param2ActivityInfo != null) {
            parcel1.writeInt(1);
            param2ActivityInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool2 = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool2 && ISecrecyService.Stub.getDefaultImpl() != null) {
            bool1 = ISecrecyService.Stub.getDefaultImpl().isInEncryptedAppList(param2ActivityInfo, param2String, param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISecrecyService param1ISecrecyService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISecrecyService != null) {
          Proxy.sDefaultImpl = param1ISecrecyService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISecrecyService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
