package android.secrecy;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.HashMap;
import java.util.Map;

public interface ISecrecyServiceReceiver extends IInterface {
  void onSecrecyStateChanged(Map paramMap) throws RemoteException;
  
  class Default implements ISecrecyServiceReceiver {
    public void onSecrecyStateChanged(Map param1Map) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISecrecyServiceReceiver {
    private static final String DESCRIPTOR = "android.secrecy.ISecrecyServiceReceiver";
    
    static final int TRANSACTION_onSecrecyStateChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.secrecy.ISecrecyServiceReceiver");
    }
    
    public static ISecrecyServiceReceiver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.secrecy.ISecrecyServiceReceiver");
      if (iInterface != null && iInterface instanceof ISecrecyServiceReceiver)
        return (ISecrecyServiceReceiver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onSecrecyStateChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.secrecy.ISecrecyServiceReceiver");
        return true;
      } 
      param1Parcel1.enforceInterface("android.secrecy.ISecrecyServiceReceiver");
      ClassLoader classLoader = getClass().getClassLoader();
      HashMap hashMap = param1Parcel1.readHashMap(classLoader);
      onSecrecyStateChanged(hashMap);
      return true;
    }
    
    private static class Proxy implements ISecrecyServiceReceiver {
      public static ISecrecyServiceReceiver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.secrecy.ISecrecyServiceReceiver";
      }
      
      public void onSecrecyStateChanged(Map param2Map) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.secrecy.ISecrecyServiceReceiver");
          parcel.writeMap(param2Map);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ISecrecyServiceReceiver.Stub.getDefaultImpl() != null) {
            ISecrecyServiceReceiver.Stub.getDefaultImpl().onSecrecyStateChanged(param2Map);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISecrecyServiceReceiver param1ISecrecyServiceReceiver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISecrecyServiceReceiver != null) {
          Proxy.sDefaultImpl = param1ISecrecyServiceReceiver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISecrecyServiceReceiver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
