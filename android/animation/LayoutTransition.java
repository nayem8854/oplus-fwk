package android.animation;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class LayoutTransition {
  private Animator mDisappearingAnim = null;
  
  private Animator mAppearingAnim = null;
  
  private Animator mChangingAppearingAnim = null;
  
  private Animator mChangingDisappearingAnim = null;
  
  private Animator mChangingAnim = null;
  
  private static long DEFAULT_DURATION = 300L;
  
  private static TimeInterpolator ACCEL_DECEL_INTERPOLATOR;
  
  public static final int APPEARING = 2;
  
  public static final int CHANGE_APPEARING = 0;
  
  public static final int CHANGE_DISAPPEARING = 1;
  
  public static final int CHANGING = 4;
  
  private static TimeInterpolator DECEL_INTERPOLATOR;
  
  public static final int DISAPPEARING = 3;
  
  private static final int FLAG_APPEARING = 1;
  
  private static final int FLAG_CHANGE_APPEARING = 4;
  
  private static final int FLAG_CHANGE_DISAPPEARING = 8;
  
  private static final int FLAG_CHANGING = 16;
  
  private static final int FLAG_DISAPPEARING = 2;
  
  private static ObjectAnimator defaultChange;
  
  private static ObjectAnimator defaultChangeIn;
  
  private static ObjectAnimator defaultChangeOut;
  
  private static ObjectAnimator defaultFadeIn;
  
  private static ObjectAnimator defaultFadeOut;
  
  private static TimeInterpolator sAppearingInterpolator;
  
  private static TimeInterpolator sChangingAppearingInterpolator;
  
  private static TimeInterpolator sChangingDisappearingInterpolator;
  
  private static TimeInterpolator sChangingInterpolator;
  
  private static TimeInterpolator sDisappearingInterpolator;
  
  private final LinkedHashMap<View, Animator> currentAppearingAnimations;
  
  private final LinkedHashMap<View, Animator> currentChangingAnimations;
  
  private final LinkedHashMap<View, Animator> currentDisappearingAnimations;
  
  private final HashMap<View, View.OnLayoutChangeListener> layoutChangeListenerMap;
  
  private boolean mAnimateParentHierarchy;
  
  private long mAppearingDelay;
  
  private long mAppearingDuration;
  
  private TimeInterpolator mAppearingInterpolator;
  
  private long mChangingAppearingDelay;
  
  private long mChangingAppearingDuration;
  
  private TimeInterpolator mChangingAppearingInterpolator;
  
  private long mChangingAppearingStagger;
  
  private long mChangingDelay;
  
  private long mChangingDisappearingDelay;
  
  private long mChangingDisappearingDuration;
  
  private TimeInterpolator mChangingDisappearingInterpolator;
  
  private long mChangingDisappearingStagger;
  
  private long mChangingDuration;
  
  private TimeInterpolator mChangingInterpolator;
  
  private long mChangingStagger;
  
  private long mDisappearingDelay;
  
  private long mDisappearingDuration;
  
  private TimeInterpolator mDisappearingInterpolator;
  
  private ArrayList<TransitionListener> mListeners;
  
  private int mTransitionTypes;
  
  private final HashMap<View, Animator> pendingAnimations;
  
  private long staggerDelay;
  
  public LayoutTransition() {
    long l = DEFAULT_DURATION;
    this.mChangingDisappearingDuration = l;
    this.mChangingDuration = l;
    this.mAppearingDuration = l;
    this.mDisappearingDuration = l;
    this.mAppearingDelay = l;
    this.mDisappearingDelay = 0L;
    this.mChangingAppearingDelay = 0L;
    this.mChangingDisappearingDelay = l;
    this.mChangingDelay = 0L;
    this.mChangingAppearingStagger = 0L;
    this.mChangingDisappearingStagger = 0L;
    this.mChangingStagger = 0L;
    this.mAppearingInterpolator = sAppearingInterpolator;
    this.mDisappearingInterpolator = sDisappearingInterpolator;
    this.mChangingAppearingInterpolator = sChangingAppearingInterpolator;
    this.mChangingDisappearingInterpolator = sChangingDisappearingInterpolator;
    this.mChangingInterpolator = sChangingInterpolator;
    this.pendingAnimations = new HashMap<>();
    this.currentChangingAnimations = new LinkedHashMap<>();
    this.currentAppearingAnimations = new LinkedHashMap<>();
    this.currentDisappearingAnimations = new LinkedHashMap<>();
    this.layoutChangeListenerMap = new HashMap<>();
    this.mTransitionTypes = 15;
    this.mAnimateParentHierarchy = true;
    if (defaultChangeIn == null) {
      PropertyValuesHolder propertyValuesHolder1 = PropertyValuesHolder.ofInt("left", new int[] { 0, 1 });
      PropertyValuesHolder propertyValuesHolder2 = PropertyValuesHolder.ofInt("top", new int[] { 0, 1 });
      PropertyValuesHolder propertyValuesHolder3 = PropertyValuesHolder.ofInt("right", new int[] { 0, 1 });
      PropertyValuesHolder propertyValuesHolder4 = PropertyValuesHolder.ofInt("bottom", new int[] { 0, 1 });
      PropertyValuesHolder propertyValuesHolder5 = PropertyValuesHolder.ofInt("scrollX", new int[] { 0, 1 });
      PropertyValuesHolder propertyValuesHolder6 = PropertyValuesHolder.ofInt("scrollY", new int[] { 0, 1 });
      ObjectAnimator objectAnimator = ObjectAnimator.ofPropertyValuesHolder((Object)null, new PropertyValuesHolder[] { propertyValuesHolder1, propertyValuesHolder2, propertyValuesHolder3, propertyValuesHolder4, propertyValuesHolder5, propertyValuesHolder6 });
      objectAnimator.setDuration(DEFAULT_DURATION);
      defaultChangeIn.setStartDelay(this.mChangingAppearingDelay);
      defaultChangeIn.setInterpolator(this.mChangingAppearingInterpolator);
      defaultChangeOut = objectAnimator = defaultChangeIn.clone();
      objectAnimator.setStartDelay(this.mChangingDisappearingDelay);
      defaultChangeOut.setInterpolator(this.mChangingDisappearingInterpolator);
      defaultChange = objectAnimator = defaultChangeIn.clone();
      objectAnimator.setStartDelay(this.mChangingDelay);
      defaultChange.setInterpolator(this.mChangingInterpolator);
      defaultFadeIn = objectAnimator = ObjectAnimator.ofFloat((Object)null, "alpha", new float[] { 0.0F, 1.0F });
      objectAnimator.setDuration(DEFAULT_DURATION);
      defaultFadeIn.setStartDelay(this.mAppearingDelay);
      defaultFadeIn.setInterpolator(this.mAppearingInterpolator);
      defaultFadeOut = objectAnimator = ObjectAnimator.ofFloat((Object)null, "alpha", new float[] { 1.0F, 0.0F });
      objectAnimator.setDuration(DEFAULT_DURATION);
      defaultFadeOut.setStartDelay(this.mDisappearingDelay);
      defaultFadeOut.setInterpolator(this.mDisappearingInterpolator);
    } 
    this.mChangingAppearingAnim = defaultChangeIn;
    this.mChangingDisappearingAnim = defaultChangeOut;
    this.mChangingAnim = defaultChange;
    this.mAppearingAnim = defaultFadeIn;
    this.mDisappearingAnim = defaultFadeOut;
  }
  
  static {
    ACCEL_DECEL_INTERPOLATOR = (TimeInterpolator)new AccelerateDecelerateInterpolator();
    DecelerateInterpolator decelerateInterpolator = new DecelerateInterpolator();
    TimeInterpolator timeInterpolator = ACCEL_DECEL_INTERPOLATOR;
    sDisappearingInterpolator = timeInterpolator;
    sChangingAppearingInterpolator = (TimeInterpolator)decelerateInterpolator;
    sChangingDisappearingInterpolator = (TimeInterpolator)decelerateInterpolator;
    sChangingInterpolator = (TimeInterpolator)decelerateInterpolator;
  }
  
  public void setDuration(long paramLong) {
    this.mChangingAppearingDuration = paramLong;
    this.mChangingDisappearingDuration = paramLong;
    this.mChangingDuration = paramLong;
    this.mAppearingDuration = paramLong;
    this.mDisappearingDuration = paramLong;
  }
  
  public void enableTransitionType(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt == 4)
              this.mTransitionTypes |= 0x10; 
          } else {
            this.mTransitionTypes |= 0x2;
          } 
        } else {
          this.mTransitionTypes |= 0x1;
        } 
      } else {
        this.mTransitionTypes |= 0x8;
      } 
    } else {
      this.mTransitionTypes = 0x4 | this.mTransitionTypes;
    } 
  }
  
  public void disableTransitionType(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt == 4)
              this.mTransitionTypes &= 0xFFFFFFEF; 
          } else {
            this.mTransitionTypes &= 0xFFFFFFFD;
          } 
        } else {
          this.mTransitionTypes &= 0xFFFFFFFE;
        } 
      } else {
        this.mTransitionTypes &= 0xFFFFFFF7;
      } 
    } else {
      this.mTransitionTypes &= 0xFFFFFFFB;
    } 
  }
  
  public boolean isTransitionTypeEnabled(int paramInt) {
    boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false, bool5 = false;
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt != 4)
              return false; 
            bool2 = bool5;
            if ((this.mTransitionTypes & 0x10) == 16)
              bool2 = true; 
            return bool2;
          } 
          bool2 = bool1;
          if ((this.mTransitionTypes & 0x2) == 2)
            bool2 = true; 
          return bool2;
        } 
        if ((this.mTransitionTypes & 0x1) == 1)
          bool2 = true; 
        return bool2;
      } 
      bool2 = bool3;
      if ((this.mTransitionTypes & 0x8) == 8)
        bool2 = true; 
      return bool2;
    } 
    bool2 = bool4;
    if ((this.mTransitionTypes & 0x4) == 4)
      bool2 = true; 
    return bool2;
  }
  
  public void setStartDelay(int paramInt, long paramLong) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt == 4)
              this.mChangingDelay = paramLong; 
          } else {
            this.mDisappearingDelay = paramLong;
          } 
        } else {
          this.mAppearingDelay = paramLong;
        } 
      } else {
        this.mChangingDisappearingDelay = paramLong;
      } 
    } else {
      this.mChangingAppearingDelay = paramLong;
    } 
  }
  
  public long getStartDelay(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt != 4)
              return 0L; 
            return this.mChangingDelay;
          } 
          return this.mDisappearingDelay;
        } 
        return this.mAppearingDelay;
      } 
      return this.mChangingDisappearingDelay;
    } 
    return this.mChangingAppearingDelay;
  }
  
  public void setDuration(int paramInt, long paramLong) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt == 4)
              this.mChangingDuration = paramLong; 
          } else {
            this.mDisappearingDuration = paramLong;
          } 
        } else {
          this.mAppearingDuration = paramLong;
        } 
      } else {
        this.mChangingDisappearingDuration = paramLong;
      } 
    } else {
      this.mChangingAppearingDuration = paramLong;
    } 
  }
  
  public long getDuration(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt != 4)
              return 0L; 
            return this.mChangingDuration;
          } 
          return this.mDisappearingDuration;
        } 
        return this.mAppearingDuration;
      } 
      return this.mChangingDisappearingDuration;
    } 
    return this.mChangingAppearingDuration;
  }
  
  public void setStagger(int paramInt, long paramLong) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt == 4)
          this.mChangingStagger = paramLong; 
      } else {
        this.mChangingDisappearingStagger = paramLong;
      } 
    } else {
      this.mChangingAppearingStagger = paramLong;
    } 
  }
  
  public long getStagger(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 4)
          return 0L; 
        return this.mChangingStagger;
      } 
      return this.mChangingDisappearingStagger;
    } 
    return this.mChangingAppearingStagger;
  }
  
  public void setInterpolator(int paramInt, TimeInterpolator paramTimeInterpolator) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt == 4)
              this.mChangingInterpolator = paramTimeInterpolator; 
          } else {
            this.mDisappearingInterpolator = paramTimeInterpolator;
          } 
        } else {
          this.mAppearingInterpolator = paramTimeInterpolator;
        } 
      } else {
        this.mChangingDisappearingInterpolator = paramTimeInterpolator;
      } 
    } else {
      this.mChangingAppearingInterpolator = paramTimeInterpolator;
    } 
  }
  
  public TimeInterpolator getInterpolator(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt != 4)
              return null; 
            return this.mChangingInterpolator;
          } 
          return this.mDisappearingInterpolator;
        } 
        return this.mAppearingInterpolator;
      } 
      return this.mChangingDisappearingInterpolator;
    } 
    return this.mChangingAppearingInterpolator;
  }
  
  public void setAnimator(int paramInt, Animator paramAnimator) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt == 4)
              this.mChangingAnim = paramAnimator; 
          } else {
            this.mDisappearingAnim = paramAnimator;
          } 
        } else {
          this.mAppearingAnim = paramAnimator;
        } 
      } else {
        this.mChangingDisappearingAnim = paramAnimator;
      } 
    } else {
      this.mChangingAppearingAnim = paramAnimator;
    } 
  }
  
  public Animator getAnimator(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt != 4)
              return null; 
            return this.mChangingAnim;
          } 
          return this.mDisappearingAnim;
        } 
        return this.mAppearingAnim;
      } 
      return this.mChangingDisappearingAnim;
    } 
    return this.mChangingAppearingAnim;
  }
  
  private void runChangeTransition(ViewGroup paramViewGroup, View paramView, int paramInt) {
    Animator animator;
    ObjectAnimator objectAnimator;
    long l;
    if (paramInt != 2) {
      if (paramInt != 3) {
        if (paramInt != 4) {
          animator = null;
          objectAnimator = null;
          l = 0L;
        } else {
          animator = this.mChangingAnim;
          l = this.mChangingDuration;
          objectAnimator = defaultChange;
        } 
      } else {
        animator = this.mChangingDisappearingAnim;
        l = this.mChangingDisappearingDuration;
        objectAnimator = defaultChangeOut;
      } 
    } else {
      animator = this.mChangingAppearingAnim;
      l = this.mChangingAppearingDuration;
      objectAnimator = defaultChangeIn;
    } 
    if (animator == null)
      return; 
    this.staggerDelay = 0L;
    ViewTreeObserver viewTreeObserver = paramViewGroup.getViewTreeObserver();
    if (!viewTreeObserver.isAlive())
      return; 
    int i = paramViewGroup.getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = paramViewGroup.getChildAt(b);
      if (view != paramView)
        setupChangeAnimation(paramViewGroup, paramInt, animator, l, view); 
    } 
    if (this.mAnimateParentHierarchy) {
      ViewGroup viewGroup = paramViewGroup;
      while (viewGroup != null) {
        ViewParent viewParent = viewGroup.getParent();
        if (viewParent instanceof ViewGroup) {
          setupChangeAnimation((ViewGroup)viewParent, paramInt, objectAnimator, l, (View)viewGroup);
          viewGroup = (ViewGroup)viewParent;
          continue;
        } 
        viewGroup = null;
      } 
    } 
    CleanupCallback cleanupCallback = new CleanupCallback(this.layoutChangeListenerMap, paramViewGroup);
    viewTreeObserver.addOnPreDrawListener(cleanupCallback);
    paramViewGroup.addOnAttachStateChangeListener(cleanupCallback);
  }
  
  public void setAnimateParentHierarchy(boolean paramBoolean) {
    this.mAnimateParentHierarchy = paramBoolean;
  }
  
  private void setupChangeAnimation(ViewGroup paramViewGroup, int paramInt, Animator paramAnimator, long paramLong, View paramView) {
    if (this.layoutChangeListenerMap.get(paramView) != null)
      return; 
    if (paramView.getWidth() == 0 && paramView.getHeight() == 0)
      return; 
    paramAnimator = paramAnimator.clone();
    paramAnimator.setTarget(paramView);
    paramAnimator.setupStartValues();
    Animator animator = this.pendingAnimations.get(paramView);
    if (animator != null) {
      animator.cancel();
      this.pendingAnimations.remove(paramView);
    } 
    this.pendingAnimations.put(paramView, paramAnimator);
    animator = ValueAnimator.ofFloat(new float[] { 0.0F, 1.0F });
    animator = animator.setDuration(paramLong + 100L);
    animator.addListener((Animator.AnimatorListener)new Object(this, paramView));
    animator.start();
    Object object = new Object(this, paramAnimator, paramInt, paramLong, paramView, paramViewGroup);
    paramAnimator.addListener((Animator.AnimatorListener)new Object(this, paramViewGroup, paramView, paramInt, (View.OnLayoutChangeListener)object));
    paramView.addOnLayoutChangeListener((View.OnLayoutChangeListener)object);
    this.layoutChangeListenerMap.put(paramView, object);
  }
  
  public void startChangingAnimations() {
    LinkedHashMap<View, Animator> linkedHashMap = this.currentChangingAnimations;
    linkedHashMap = (LinkedHashMap<View, Animator>)linkedHashMap.clone();
    for (Animator animator : linkedHashMap.values()) {
      if (animator instanceof ObjectAnimator)
        ((ObjectAnimator)animator).setCurrentPlayTime(0L); 
      animator.start();
    } 
  }
  
  public void endChangingAnimations() {
    LinkedHashMap<View, Animator> linkedHashMap = this.currentChangingAnimations;
    linkedHashMap = (LinkedHashMap<View, Animator>)linkedHashMap.clone();
    for (Animator animator : linkedHashMap.values()) {
      animator.start();
      animator.end();
    } 
    this.currentChangingAnimations.clear();
  }
  
  public boolean isChangingLayout() {
    boolean bool;
    if (this.currentChangingAnimations.size() > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isRunning() {
    if (this.currentChangingAnimations.size() <= 0 && this.currentAppearingAnimations.size() <= 0) {
      LinkedHashMap<View, Animator> linkedHashMap = this.currentDisappearingAnimations;
      return 
        (linkedHashMap.size() > 0);
    } 
    return true;
  }
  
  public void cancel() {
    if (this.currentChangingAnimations.size() > 0) {
      LinkedHashMap<View, Animator> linkedHashMap = this.currentChangingAnimations;
      linkedHashMap = (LinkedHashMap<View, Animator>)linkedHashMap.clone();
      for (Animator animator : linkedHashMap.values())
        animator.cancel(); 
      this.currentChangingAnimations.clear();
    } 
    if (this.currentAppearingAnimations.size() > 0) {
      LinkedHashMap<View, Animator> linkedHashMap = this.currentAppearingAnimations;
      linkedHashMap = (LinkedHashMap<View, Animator>)linkedHashMap.clone();
      for (Animator animator : linkedHashMap.values())
        animator.end(); 
      this.currentAppearingAnimations.clear();
    } 
    if (this.currentDisappearingAnimations.size() > 0) {
      LinkedHashMap<View, Animator> linkedHashMap = this.currentDisappearingAnimations;
      linkedHashMap = (LinkedHashMap<View, Animator>)linkedHashMap.clone();
      for (Animator animator : linkedHashMap.values())
        animator.end(); 
      this.currentDisappearingAnimations.clear();
    } 
  }
  
  public void cancel(int paramInt) {
    if (paramInt != 0 && paramInt != 1)
      if (paramInt != 2) {
        if (paramInt != 3) {
          if (paramInt != 4)
            return; 
        } else {
          if (this.currentDisappearingAnimations.size() > 0) {
            LinkedHashMap<View, Animator> linkedHashMap = this.currentDisappearingAnimations;
            linkedHashMap = (LinkedHashMap<View, Animator>)linkedHashMap.clone();
            for (Animator animator : linkedHashMap.values())
              animator.end(); 
            this.currentDisappearingAnimations.clear();
          } 
          return;
        } 
      } else {
        if (this.currentAppearingAnimations.size() > 0) {
          LinkedHashMap<View, Animator> linkedHashMap = this.currentAppearingAnimations;
          linkedHashMap = (LinkedHashMap<View, Animator>)linkedHashMap.clone();
          for (Animator animator : linkedHashMap.values())
            animator.end(); 
          this.currentAppearingAnimations.clear();
        } 
        return;
      }  
    if (this.currentChangingAnimations.size() > 0) {
      LinkedHashMap<View, Animator> linkedHashMap = this.currentChangingAnimations;
      linkedHashMap = (LinkedHashMap<View, Animator>)linkedHashMap.clone();
      for (Animator animator : linkedHashMap.values())
        animator.cancel(); 
      this.currentChangingAnimations.clear();
    } 
  }
  
  private void runAppearingTransition(ViewGroup paramViewGroup, View paramView) {
    Animator animator = this.currentDisappearingAnimations.get(paramView);
    if (animator != null)
      animator.cancel(); 
    animator = this.mAppearingAnim;
    if (animator == null) {
      if (hasListeners()) {
        ArrayList<TransitionListener> arrayList = this.mListeners;
        arrayList = (ArrayList<TransitionListener>)arrayList.clone();
        for (TransitionListener transitionListener : arrayList)
          transitionListener.endTransition(this, paramViewGroup, paramView, 2); 
      } 
      return;
    } 
    animator = animator.clone();
    animator.setTarget(paramView);
    animator.setStartDelay(this.mAppearingDelay);
    animator.setDuration(this.mAppearingDuration);
    TimeInterpolator timeInterpolator = this.mAppearingInterpolator;
    if (timeInterpolator != sAppearingInterpolator)
      animator.setInterpolator(timeInterpolator); 
    if (animator instanceof ObjectAnimator)
      ((ObjectAnimator)animator).setCurrentPlayTime(0L); 
    animator.addListener((Animator.AnimatorListener)new Object(this, paramView, paramViewGroup));
    this.currentAppearingAnimations.put(paramView, animator);
    animator.start();
  }
  
  private void runDisappearingTransition(ViewGroup paramViewGroup, View paramView) {
    Animator animator1 = this.currentAppearingAnimations.get(paramView);
    if (animator1 != null)
      animator1.cancel(); 
    animator1 = this.mDisappearingAnim;
    if (animator1 == null) {
      if (hasListeners()) {
        ArrayList<TransitionListener> arrayList = this.mListeners;
        arrayList = (ArrayList<TransitionListener>)arrayList.clone();
        for (TransitionListener transitionListener : arrayList)
          transitionListener.endTransition(this, paramViewGroup, paramView, 3); 
      } 
      return;
    } 
    Animator animator2 = transitionListener.clone();
    animator2.setStartDelay(this.mDisappearingDelay);
    animator2.setDuration(this.mDisappearingDuration);
    TimeInterpolator timeInterpolator = this.mDisappearingInterpolator;
    if (timeInterpolator != sDisappearingInterpolator)
      animator2.setInterpolator(timeInterpolator); 
    animator2.setTarget(paramView);
    float f = paramView.getAlpha();
    animator2.addListener((Animator.AnimatorListener)new Object(this, paramView, f, paramViewGroup));
    if (animator2 instanceof ObjectAnimator)
      ((ObjectAnimator)animator2).setCurrentPlayTime(0L); 
    this.currentDisappearingAnimations.put(paramView, animator2);
    animator2.start();
  }
  
  private void addChild(ViewGroup paramViewGroup, View paramView, boolean paramBoolean) {
    if (paramViewGroup.getWindowVisibility() != 0)
      return; 
    if ((this.mTransitionTypes & 0x1) == 1)
      cancel(3); 
    if (paramBoolean && (this.mTransitionTypes & 0x4) == 4) {
      cancel(0);
      cancel(4);
    } 
    if (hasListeners() && (this.mTransitionTypes & 0x1) == 1) {
      ArrayList<TransitionListener> arrayList = this.mListeners;
      arrayList = (ArrayList<TransitionListener>)arrayList.clone();
      for (TransitionListener transitionListener : arrayList)
        transitionListener.startTransition(this, paramViewGroup, paramView, 2); 
    } 
    if (paramBoolean && (this.mTransitionTypes & 0x4) == 4)
      runChangeTransition(paramViewGroup, paramView, 2); 
    if ((this.mTransitionTypes & 0x1) == 1)
      runAppearingTransition(paramViewGroup, paramView); 
  }
  
  private boolean hasListeners() {
    boolean bool;
    ArrayList<TransitionListener> arrayList = this.mListeners;
    if (arrayList != null && arrayList.size() > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void layoutChange(ViewGroup paramViewGroup) {
    if (paramViewGroup.getWindowVisibility() != 0)
      return; 
    if ((this.mTransitionTypes & 0x10) == 16 && !isRunning())
      runChangeTransition(paramViewGroup, null, 4); 
  }
  
  public void addChild(ViewGroup paramViewGroup, View paramView) {
    addChild(paramViewGroup, paramView, true);
  }
  
  @Deprecated
  public void showChild(ViewGroup paramViewGroup, View paramView) {
    addChild(paramViewGroup, paramView, true);
  }
  
  public void showChild(ViewGroup paramViewGroup, View paramView, int paramInt) {
    boolean bool;
    if (paramInt == 8) {
      bool = true;
    } else {
      bool = false;
    } 
    addChild(paramViewGroup, paramView, bool);
  }
  
  private void removeChild(ViewGroup paramViewGroup, View paramView, boolean paramBoolean) {
    if (paramViewGroup.getWindowVisibility() != 0)
      return; 
    if ((this.mTransitionTypes & 0x2) == 2)
      cancel(2); 
    if (paramBoolean && (this.mTransitionTypes & 0x8) == 8) {
      cancel(1);
      cancel(4);
    } 
    if (hasListeners() && (this.mTransitionTypes & 0x2) == 2) {
      ArrayList<TransitionListener> arrayList = this.mListeners;
      arrayList = (ArrayList<TransitionListener>)arrayList.clone();
      for (TransitionListener transitionListener : arrayList)
        transitionListener.startTransition(this, paramViewGroup, paramView, 3); 
    } 
    if (paramBoolean && (this.mTransitionTypes & 0x8) == 8)
      runChangeTransition(paramViewGroup, paramView, 3); 
    if ((this.mTransitionTypes & 0x2) == 2)
      runDisappearingTransition(paramViewGroup, paramView); 
  }
  
  public void removeChild(ViewGroup paramViewGroup, View paramView) {
    removeChild(paramViewGroup, paramView, true);
  }
  
  @Deprecated
  public void hideChild(ViewGroup paramViewGroup, View paramView) {
    removeChild(paramViewGroup, paramView, true);
  }
  
  public void hideChild(ViewGroup paramViewGroup, View paramView, int paramInt) {
    boolean bool;
    if (paramInt == 8) {
      bool = true;
    } else {
      bool = false;
    } 
    removeChild(paramViewGroup, paramView, bool);
  }
  
  public void addTransitionListener(TransitionListener paramTransitionListener) {
    if (this.mListeners == null)
      this.mListeners = new ArrayList<>(); 
    this.mListeners.add(paramTransitionListener);
  }
  
  public void removeTransitionListener(TransitionListener paramTransitionListener) {
    ArrayList<TransitionListener> arrayList = this.mListeners;
    if (arrayList == null)
      return; 
    arrayList.remove(paramTransitionListener);
  }
  
  public List<TransitionListener> getTransitionListeners() {
    return this.mListeners;
  }
  
  class CleanupCallback implements ViewTreeObserver.OnPreDrawListener, View.OnAttachStateChangeListener {
    final Map<View, View.OnLayoutChangeListener> layoutChangeListenerMap;
    
    final ViewGroup parent;
    
    CleanupCallback(LayoutTransition this$0, ViewGroup param1ViewGroup) {
      this.layoutChangeListenerMap = (Map<View, View.OnLayoutChangeListener>)this$0;
      this.parent = param1ViewGroup;
    }
    
    private void cleanup() {
      this.parent.getViewTreeObserver().removeOnPreDrawListener(this);
      this.parent.removeOnAttachStateChangeListener(this);
      int i = this.layoutChangeListenerMap.size();
      if (i > 0) {
        Set<View> set = this.layoutChangeListenerMap.keySet();
        for (View view : set) {
          View.OnLayoutChangeListener onLayoutChangeListener = this.layoutChangeListenerMap.get(view);
          view.removeOnLayoutChangeListener(onLayoutChangeListener);
        } 
        this.layoutChangeListenerMap.clear();
      } 
    }
    
    public void onViewAttachedToWindow(View param1View) {}
    
    public void onViewDetachedFromWindow(View param1View) {
      cleanup();
    }
    
    public boolean onPreDraw() {
      cleanup();
      return true;
    }
  }
  
  public static interface TransitionListener {
    void endTransition(LayoutTransition param1LayoutTransition, ViewGroup param1ViewGroup, View param1View, int param1Int);
    
    void startTransition(LayoutTransition param1LayoutTransition, ViewGroup param1ViewGroup, View param1View, int param1Int);
  }
}
