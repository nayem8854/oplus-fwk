package android.animation;

public interface TypeEvaluator<T> {
  T evaluate(float paramFloat, T paramT1, T paramT2);
}
