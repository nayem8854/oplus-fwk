package android.animation;

public abstract class BidirectionalTypeConverter<T, V> extends TypeConverter<T, V> {
  private BidirectionalTypeConverter mInvertedConverter;
  
  public BidirectionalTypeConverter(Class<T> paramClass, Class<V> paramClass1) {
    super(paramClass, paramClass1);
  }
  
  public abstract T convertBack(V paramV);
  
  public BidirectionalTypeConverter<V, T> invert() {
    if (this.mInvertedConverter == null)
      this.mInvertedConverter = new InvertedConverter<>(this); 
    return this.mInvertedConverter;
  }
  
  class InvertedConverter<From, To> extends BidirectionalTypeConverter<From, To> {
    private BidirectionalTypeConverter<To, From> mConverter;
    
    public InvertedConverter(BidirectionalTypeConverter<To, From> this$0) {
      super(this$0.getTargetType(), this$0.getSourceType());
      this.mConverter = this$0;
    }
    
    public From convertBack(To param1To) {
      return this.mConverter.convert(param1To);
    }
    
    public To convert(From param1From) {
      return this.mConverter.convertBack(param1From);
    }
  }
}
