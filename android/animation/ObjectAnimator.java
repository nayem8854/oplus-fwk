package android.animation;

import android.graphics.Path;
import android.graphics.PointF;
import android.util.Property;
import java.lang.ref.WeakReference;

public final class ObjectAnimator extends ValueAnimator {
  private static final boolean DBG = false;
  
  private static final String LOG_TAG = "ObjectAnimator";
  
  private boolean mAutoCancel = false;
  
  private Property mProperty;
  
  private String mPropertyName;
  
  private WeakReference<Object> mTarget;
  
  public void setPropertyName(String paramString) {
    if (this.mValues != null) {
      PropertyValuesHolder propertyValuesHolder = this.mValues[0];
      String str = propertyValuesHolder.getPropertyName();
      propertyValuesHolder.setPropertyName(paramString);
      this.mValuesMap.remove(str);
      this.mValuesMap.put(paramString, propertyValuesHolder);
    } 
    this.mPropertyName = paramString;
    this.mInitialized = false;
  }
  
  public void setProperty(Property paramProperty) {
    if (this.mValues != null) {
      PropertyValuesHolder propertyValuesHolder = this.mValues[0];
      String str = propertyValuesHolder.getPropertyName();
      propertyValuesHolder.setProperty(paramProperty);
      this.mValuesMap.remove(str);
      this.mValuesMap.put(this.mPropertyName, propertyValuesHolder);
    } 
    if (this.mProperty != null)
      this.mPropertyName = paramProperty.getName(); 
    this.mProperty = paramProperty;
    this.mInitialized = false;
  }
  
  public String getPropertyName() {
    String str;
    Property property1 = null, property2 = null;
    if (this.mPropertyName != null) {
      str = this.mPropertyName;
    } else {
      Property property = this.mProperty;
      if (property != null) {
        str = property.getName();
      } else {
        property = property1;
        if (this.mValues != null) {
          property = property1;
          if (this.mValues.length > 0) {
            byte b = 0;
            while (true) {
              property = property2;
              if (b < this.mValues.length) {
                if (b == 0) {
                  str = "";
                } else {
                  StringBuilder stringBuilder1 = new StringBuilder();
                  stringBuilder1.append((String)property2);
                  stringBuilder1.append(",");
                  str = stringBuilder1.toString();
                } 
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(str);
                stringBuilder.append(this.mValues[b].getPropertyName());
                String str1 = stringBuilder.toString();
                b++;
                continue;
              } 
              break;
            } 
          } 
        } 
      } 
    } 
    return str;
  }
  
  String getNameForTrace() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("animator:");
    stringBuilder.append(getPropertyName());
    return stringBuilder.toString();
  }
  
  private ObjectAnimator(Object paramObject, String paramString) {
    setTarget(paramObject);
    setPropertyName(paramString);
  }
  
  private <T> ObjectAnimator(T paramT, Property<T, ?> paramProperty) {
    setTarget(paramT);
    setProperty(paramProperty);
  }
  
  public static ObjectAnimator ofInt(Object paramObject, String paramString, int... paramVarArgs) {
    paramObject = new ObjectAnimator(paramObject, paramString);
    paramObject.setIntValues(paramVarArgs);
    return (ObjectAnimator)paramObject;
  }
  
  public static ObjectAnimator ofInt(Object paramObject, String paramString1, String paramString2, Path paramPath) {
    PathKeyframes pathKeyframes = KeyframeSet.ofPath(paramPath);
    Keyframes.IntKeyframes intKeyframes2 = pathKeyframes.createXIntKeyframes();
    PropertyValuesHolder propertyValuesHolder1 = PropertyValuesHolder.ofKeyframes(paramString1, intKeyframes2);
    Keyframes.IntKeyframes intKeyframes1 = pathKeyframes.createYIntKeyframes();
    PropertyValuesHolder propertyValuesHolder2 = PropertyValuesHolder.ofKeyframes(paramString2, intKeyframes1);
    return ofPropertyValuesHolder(paramObject, new PropertyValuesHolder[] { propertyValuesHolder1, propertyValuesHolder2 });
  }
  
  public static <T> ObjectAnimator ofInt(T paramT, Property<T, Integer> paramProperty, int... paramVarArgs) {
    ObjectAnimator objectAnimator = new ObjectAnimator(paramT, paramProperty);
    objectAnimator.setIntValues(paramVarArgs);
    return objectAnimator;
  }
  
  public static <T> ObjectAnimator ofInt(T paramT, Property<T, Integer> paramProperty1, Property<T, Integer> paramProperty2, Path paramPath) {
    PathKeyframes pathKeyframes = KeyframeSet.ofPath(paramPath);
    Keyframes.IntKeyframes intKeyframes2 = pathKeyframes.createXIntKeyframes();
    PropertyValuesHolder propertyValuesHolder1 = PropertyValuesHolder.ofKeyframes(paramProperty1, intKeyframes2);
    Keyframes.IntKeyframes intKeyframes1 = pathKeyframes.createYIntKeyframes();
    PropertyValuesHolder propertyValuesHolder2 = PropertyValuesHolder.ofKeyframes(paramProperty2, intKeyframes1);
    return ofPropertyValuesHolder(paramT, new PropertyValuesHolder[] { propertyValuesHolder1, propertyValuesHolder2 });
  }
  
  public static ObjectAnimator ofMultiInt(Object paramObject, String paramString, int[][] paramArrayOfint) {
    PropertyValuesHolder propertyValuesHolder = PropertyValuesHolder.ofMultiInt(paramString, paramArrayOfint);
    return ofPropertyValuesHolder(paramObject, new PropertyValuesHolder[] { propertyValuesHolder });
  }
  
  public static ObjectAnimator ofMultiInt(Object paramObject, String paramString, Path paramPath) {
    PropertyValuesHolder propertyValuesHolder = PropertyValuesHolder.ofMultiInt(paramString, paramPath);
    return ofPropertyValuesHolder(paramObject, new PropertyValuesHolder[] { propertyValuesHolder });
  }
  
  @SafeVarargs
  public static <T> ObjectAnimator ofMultiInt(Object paramObject, String paramString, TypeConverter<T, int[]> paramTypeConverter, TypeEvaluator<T> paramTypeEvaluator, T... paramVarArgs) {
    PropertyValuesHolder propertyValuesHolder = PropertyValuesHolder.ofMultiInt(paramString, paramTypeConverter, paramTypeEvaluator, paramVarArgs);
    return ofPropertyValuesHolder(paramObject, new PropertyValuesHolder[] { propertyValuesHolder });
  }
  
  public static ObjectAnimator ofArgb(Object paramObject, String paramString, int... paramVarArgs) {
    paramObject = ofInt(paramObject, paramString, paramVarArgs);
    paramObject.setEvaluator(ArgbEvaluator.getInstance());
    return (ObjectAnimator)paramObject;
  }
  
  public static <T> ObjectAnimator ofArgb(T paramT, Property<T, Integer> paramProperty, int... paramVarArgs) {
    ObjectAnimator objectAnimator = ofInt(paramT, paramProperty, paramVarArgs);
    objectAnimator.setEvaluator(ArgbEvaluator.getInstance());
    return objectAnimator;
  }
  
  public static ObjectAnimator ofFloat(Object paramObject, String paramString, float... paramVarArgs) {
    paramObject = new ObjectAnimator(paramObject, paramString);
    paramObject.setFloatValues(paramVarArgs);
    return (ObjectAnimator)paramObject;
  }
  
  public static ObjectAnimator ofFloat(Object paramObject, String paramString1, String paramString2, Path paramPath) {
    PathKeyframes pathKeyframes = KeyframeSet.ofPath(paramPath);
    Keyframes.FloatKeyframes floatKeyframes2 = pathKeyframes.createXFloatKeyframes();
    PropertyValuesHolder propertyValuesHolder1 = PropertyValuesHolder.ofKeyframes(paramString1, floatKeyframes2);
    Keyframes.FloatKeyframes floatKeyframes1 = pathKeyframes.createYFloatKeyframes();
    PropertyValuesHolder propertyValuesHolder2 = PropertyValuesHolder.ofKeyframes(paramString2, floatKeyframes1);
    return ofPropertyValuesHolder(paramObject, new PropertyValuesHolder[] { propertyValuesHolder1, propertyValuesHolder2 });
  }
  
  public static <T> ObjectAnimator ofFloat(T paramT, Property<T, Float> paramProperty, float... paramVarArgs) {
    ObjectAnimator objectAnimator = new ObjectAnimator(paramT, paramProperty);
    objectAnimator.setFloatValues(paramVarArgs);
    return objectAnimator;
  }
  
  public static <T> ObjectAnimator ofFloat(T paramT, Property<T, Float> paramProperty1, Property<T, Float> paramProperty2, Path paramPath) {
    PathKeyframes pathKeyframes = KeyframeSet.ofPath(paramPath);
    Keyframes.FloatKeyframes floatKeyframes2 = pathKeyframes.createXFloatKeyframes();
    PropertyValuesHolder propertyValuesHolder1 = PropertyValuesHolder.ofKeyframes(paramProperty1, floatKeyframes2);
    Keyframes.FloatKeyframes floatKeyframes1 = pathKeyframes.createYFloatKeyframes();
    PropertyValuesHolder propertyValuesHolder2 = PropertyValuesHolder.ofKeyframes(paramProperty2, floatKeyframes1);
    return ofPropertyValuesHolder(paramT, new PropertyValuesHolder[] { propertyValuesHolder1, propertyValuesHolder2 });
  }
  
  public static ObjectAnimator ofMultiFloat(Object paramObject, String paramString, float[][] paramArrayOffloat) {
    PropertyValuesHolder propertyValuesHolder = PropertyValuesHolder.ofMultiFloat(paramString, paramArrayOffloat);
    return ofPropertyValuesHolder(paramObject, new PropertyValuesHolder[] { propertyValuesHolder });
  }
  
  public static ObjectAnimator ofMultiFloat(Object paramObject, String paramString, Path paramPath) {
    PropertyValuesHolder propertyValuesHolder = PropertyValuesHolder.ofMultiFloat(paramString, paramPath);
    return ofPropertyValuesHolder(paramObject, new PropertyValuesHolder[] { propertyValuesHolder });
  }
  
  @SafeVarargs
  public static <T> ObjectAnimator ofMultiFloat(Object paramObject, String paramString, TypeConverter<T, float[]> paramTypeConverter, TypeEvaluator<T> paramTypeEvaluator, T... paramVarArgs) {
    PropertyValuesHolder propertyValuesHolder = PropertyValuesHolder.ofMultiFloat(paramString, paramTypeConverter, paramTypeEvaluator, paramVarArgs);
    return ofPropertyValuesHolder(paramObject, new PropertyValuesHolder[] { propertyValuesHolder });
  }
  
  public static ObjectAnimator ofObject(Object paramObject, String paramString, TypeEvaluator paramTypeEvaluator, Object... paramVarArgs) {
    paramObject = new ObjectAnimator(paramObject, paramString);
    paramObject.setObjectValues(paramVarArgs);
    paramObject.setEvaluator(paramTypeEvaluator);
    return (ObjectAnimator)paramObject;
  }
  
  public static ObjectAnimator ofObject(Object paramObject, String paramString, TypeConverter<PointF, ?> paramTypeConverter, Path paramPath) {
    PropertyValuesHolder propertyValuesHolder = PropertyValuesHolder.ofObject(paramString, paramTypeConverter, paramPath);
    return ofPropertyValuesHolder(paramObject, new PropertyValuesHolder[] { propertyValuesHolder });
  }
  
  @SafeVarargs
  public static <T, V> ObjectAnimator ofObject(T paramT, Property<T, V> paramProperty, TypeEvaluator<V> paramTypeEvaluator, V... paramVarArgs) {
    ObjectAnimator objectAnimator = new ObjectAnimator(paramT, paramProperty);
    objectAnimator.setObjectValues((Object[])paramVarArgs);
    objectAnimator.setEvaluator(paramTypeEvaluator);
    return objectAnimator;
  }
  
  @SafeVarargs
  public static <T, V, P> ObjectAnimator ofObject(T paramT, Property<T, P> paramProperty, TypeConverter<V, P> paramTypeConverter, TypeEvaluator<V> paramTypeEvaluator, V... paramVarArgs) {
    PropertyValuesHolder propertyValuesHolder = PropertyValuesHolder.ofObject(paramProperty, paramTypeConverter, paramTypeEvaluator, paramVarArgs);
    return ofPropertyValuesHolder(paramT, new PropertyValuesHolder[] { propertyValuesHolder });
  }
  
  public static <T, V> ObjectAnimator ofObject(T paramT, Property<T, V> paramProperty, TypeConverter<PointF, V> paramTypeConverter, Path paramPath) {
    PropertyValuesHolder propertyValuesHolder = PropertyValuesHolder.ofObject(paramProperty, paramTypeConverter, paramPath);
    return ofPropertyValuesHolder(paramT, new PropertyValuesHolder[] { propertyValuesHolder });
  }
  
  public static ObjectAnimator ofPropertyValuesHolder(Object paramObject, PropertyValuesHolder... paramVarArgs) {
    ObjectAnimator objectAnimator = new ObjectAnimator();
    objectAnimator.setTarget(paramObject);
    objectAnimator.setValues(paramVarArgs);
    return objectAnimator;
  }
  
  public void setIntValues(int... paramVarArgs) {
    if (this.mValues == null || this.mValues.length == 0) {
      Property<?, Integer> property = this.mProperty;
      if (property != null) {
        setValues(new PropertyValuesHolder[] { PropertyValuesHolder.ofInt(property, paramVarArgs) });
      } else {
        setValues(new PropertyValuesHolder[] { PropertyValuesHolder.ofInt(this.mPropertyName, paramVarArgs) });
      } 
      return;
    } 
    super.setIntValues(paramVarArgs);
  }
  
  public void setFloatValues(float... paramVarArgs) {
    if (this.mValues == null || this.mValues.length == 0) {
      Property<?, Float> property = this.mProperty;
      if (property != null) {
        setValues(new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(property, paramVarArgs) });
      } else {
        setValues(new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(this.mPropertyName, paramVarArgs) });
      } 
      return;
    } 
    super.setFloatValues(paramVarArgs);
  }
  
  public void setObjectValues(Object... paramVarArgs) {
    if (this.mValues == null || this.mValues.length == 0) {
      Property property = this.mProperty;
      if (property != null) {
        setValues(new PropertyValuesHolder[] { PropertyValuesHolder.ofObject(property, (TypeEvaluator)null, paramVarArgs) });
      } else {
        setValues(new PropertyValuesHolder[] { PropertyValuesHolder.ofObject(this.mPropertyName, (TypeEvaluator)null, paramVarArgs) });
      } 
      return;
    } 
    super.setObjectValues(paramVarArgs);
  }
  
  public void setAutoCancel(boolean paramBoolean) {
    this.mAutoCancel = paramBoolean;
  }
  
  private boolean hasSameTargetAndProperties(Animator paramAnimator) {
    if (paramAnimator instanceof ObjectAnimator) {
      PropertyValuesHolder[] arrayOfPropertyValuesHolder = ((ObjectAnimator)paramAnimator).getValues();
      if (((ObjectAnimator)paramAnimator).getTarget() == getTarget() && this.mValues.length == arrayOfPropertyValuesHolder.length) {
        for (byte b = 0; b < this.mValues.length; b++) {
          PropertyValuesHolder propertyValuesHolder2 = this.mValues[b];
          PropertyValuesHolder propertyValuesHolder1 = arrayOfPropertyValuesHolder[b];
          if (propertyValuesHolder2.getPropertyName() == null || 
            !propertyValuesHolder2.getPropertyName().equals(propertyValuesHolder1.getPropertyName()))
            return false; 
        } 
        return true;
      } 
    } 
    return false;
  }
  
  public void start() {
    AnimationHandler.getInstance().autoCancelBasedOn(this);
    super.start();
  }
  
  boolean shouldAutoCancel(AnimationHandler.AnimationFrameCallback paramAnimationFrameCallback) {
    if (paramAnimationFrameCallback == null)
      return false; 
    if (paramAnimationFrameCallback instanceof ObjectAnimator) {
      paramAnimationFrameCallback = paramAnimationFrameCallback;
      if (((ObjectAnimator)paramAnimationFrameCallback).mAutoCancel && hasSameTargetAndProperties((Animator)paramAnimationFrameCallback))
        return true; 
    } 
    return false;
  }
  
  void initAnimation() {
    if (!this.mInitialized) {
      Object object = getTarget();
      if (object != null) {
        int i = this.mValues.length;
        for (byte b = 0; b < i; b++)
          this.mValues[b].setupSetterAndGetter(object); 
      } 
      super.initAnimation();
    } 
  }
  
  public ObjectAnimator setDuration(long paramLong) {
    super.setDuration(paramLong);
    return this;
  }
  
  public Object getTarget() {
    WeakReference<Object> weakReference = this.mTarget;
    if (weakReference == null) {
      weakReference = null;
    } else {
      weakReference = (WeakReference<Object>)weakReference.get();
    } 
    return weakReference;
  }
  
  public void setTarget(Object paramObject) {
    Object object = getTarget();
    if (object != paramObject) {
      if (isStarted())
        cancel(); 
      if (paramObject == null) {
        paramObject = null;
      } else {
        paramObject = new WeakReference(paramObject);
      } 
      this.mTarget = (WeakReference<Object>)paramObject;
      this.mInitialized = false;
    } 
  }
  
  public void setupStartValues() {
    initAnimation();
    Object object = getTarget();
    if (object != null) {
      int i = this.mValues.length;
      for (byte b = 0; b < i; b++)
        this.mValues[b].setupStartValue(object); 
    } 
  }
  
  public void setupEndValues() {
    initAnimation();
    Object object = getTarget();
    if (object != null) {
      int i = this.mValues.length;
      for (byte b = 0; b < i; b++)
        this.mValues[b].setupEndValue(object); 
    } 
  }
  
  void animateValue(float paramFloat) {
    Object object = getTarget();
    if (this.mTarget != null && object == null) {
      cancel();
      return;
    } 
    super.animateValue(paramFloat);
    int i = this.mValues.length;
    for (byte b = 0; b < i; b++)
      this.mValues[b].setAnimatedValue(object); 
  }
  
  boolean isInitialized() {
    return this.mInitialized;
  }
  
  public ObjectAnimator clone() {
    return (ObjectAnimator)super.clone();
  }
  
  public String toString() {
    StringBuilder stringBuilder2, stringBuilder1 = new StringBuilder();
    stringBuilder1.append("ObjectAnimator@");
    stringBuilder1.append(Integer.toHexString(hashCode()));
    stringBuilder1.append(", target ");
    stringBuilder1.append(getTarget());
    String str1 = stringBuilder1.toString();
    String str2 = str1;
    if (this.mValues != null) {
      byte b = 0;
      while (true) {
        str2 = str1;
        if (b < this.mValues.length) {
          stringBuilder2 = new StringBuilder();
          stringBuilder2.append(str1);
          stringBuilder2.append("\n    ");
          stringBuilder2.append(this.mValues[b].toString());
          str1 = stringBuilder2.toString();
          b++;
          continue;
        } 
        break;
      } 
    } 
    return (String)stringBuilder2;
  }
  
  public ObjectAnimator() {}
}
