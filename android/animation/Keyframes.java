package android.animation;

import java.util.List;

public interface Keyframes extends Cloneable {
  Keyframes clone();
  
  List<Keyframe> getKeyframes();
  
  Class getType();
  
  Object getValue(float paramFloat);
  
  void setEvaluator(TypeEvaluator paramTypeEvaluator);
  
  class FloatKeyframes implements Keyframes {
    public abstract float getFloatValue(float param1Float);
  }
  
  class IntKeyframes implements Keyframes {
    public abstract int getIntValue(float param1Float);
  }
}
