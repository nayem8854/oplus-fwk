package android.animation;

import android.os.SystemClock;
import android.util.ArrayMap;
import android.view.Choreographer;
import java.util.ArrayList;

public class AnimationHandler {
  private final ArrayMap<AnimationFrameCallback, Long> mDelayedCallbackStartTime = new ArrayMap();
  
  private final ArrayList<AnimationFrameCallback> mAnimationCallbacks = new ArrayList<>();
  
  private final ArrayList<AnimationFrameCallback> mCommitCallbacks = new ArrayList<>();
  
  private final Choreographer.FrameCallback mFrameCallback = (Choreographer.FrameCallback)new Object(this);
  
  public static final ThreadLocal<AnimationHandler> sAnimatorHandler = new ThreadLocal<>();
  
  private boolean mListDirty = false;
  
  private AnimationFrameCallbackProvider mProvider;
  
  public static AnimationHandler getInstance() {
    if (sAnimatorHandler.get() == null)
      sAnimatorHandler.set(new AnimationHandler()); 
    return sAnimatorHandler.get();
  }
  
  public void setProvider(AnimationFrameCallbackProvider paramAnimationFrameCallbackProvider) {
    if (paramAnimationFrameCallbackProvider == null) {
      this.mProvider = new MyFrameCallbackProvider();
    } else {
      this.mProvider = paramAnimationFrameCallbackProvider;
    } 
  }
  
  private AnimationFrameCallbackProvider getProvider() {
    if (this.mProvider == null)
      this.mProvider = new MyFrameCallbackProvider(); 
    return this.mProvider;
  }
  
  public void addAnimationFrameCallback(AnimationFrameCallback paramAnimationFrameCallback, long paramLong) {
    if (this.mAnimationCallbacks.size() == 0)
      getProvider().postFrameCallback(this.mFrameCallback); 
    if (!this.mAnimationCallbacks.contains(paramAnimationFrameCallback))
      this.mAnimationCallbacks.add(paramAnimationFrameCallback); 
    if (paramLong > 0L)
      this.mDelayedCallbackStartTime.put(paramAnimationFrameCallback, Long.valueOf(SystemClock.uptimeMillis() + paramLong)); 
  }
  
  public void addOneShotCommitCallback(AnimationFrameCallback paramAnimationFrameCallback) {
    if (!this.mCommitCallbacks.contains(paramAnimationFrameCallback))
      this.mCommitCallbacks.add(paramAnimationFrameCallback); 
  }
  
  public void removeCallback(AnimationFrameCallback paramAnimationFrameCallback) {
    this.mCommitCallbacks.remove(paramAnimationFrameCallback);
    this.mDelayedCallbackStartTime.remove(paramAnimationFrameCallback);
    int i = this.mAnimationCallbacks.indexOf(paramAnimationFrameCallback);
    if (i >= 0) {
      this.mAnimationCallbacks.set(i, null);
      this.mListDirty = true;
    } 
  }
  
  private void doAnimationFrame(long paramLong) {
    long l = SystemClock.uptimeMillis();
    int i = this.mAnimationCallbacks.size();
    for (byte b = 0; b < i; b++) {
      final AnimationFrameCallback callback = this.mAnimationCallbacks.get(b);
      if (animationFrameCallback != null)
        if (isCallbackDue(animationFrameCallback, l)) {
          animationFrameCallback.doAnimationFrame(paramLong);
          if (this.mCommitCallbacks.contains(animationFrameCallback))
            getProvider().postCommitCallback(new Runnable() {
                  final AnimationHandler this$0;
                  
                  final AnimationHandler.AnimationFrameCallback val$callback;
                  
                  public void run() {
                    AnimationHandler animationHandler = AnimationHandler.this;
                    animationHandler.commitAnimationFrame(callback, animationHandler.getProvider().getFrameTime());
                  }
                }); 
        }  
    } 
    cleanUpList();
  }
  
  private void commitAnimationFrame(AnimationFrameCallback paramAnimationFrameCallback, long paramLong) {
    if (!this.mDelayedCallbackStartTime.containsKey(paramAnimationFrameCallback)) {
      ArrayList<AnimationFrameCallback> arrayList = this.mCommitCallbacks;
      if (arrayList.contains(paramAnimationFrameCallback)) {
        paramAnimationFrameCallback.commitAnimationFrame(paramLong);
        this.mCommitCallbacks.remove(paramAnimationFrameCallback);
      } 
    } 
  }
  
  private boolean isCallbackDue(AnimationFrameCallback paramAnimationFrameCallback, long paramLong) {
    Long long_ = (Long)this.mDelayedCallbackStartTime.get(paramAnimationFrameCallback);
    if (long_ == null)
      return true; 
    if (long_.longValue() < paramLong) {
      this.mDelayedCallbackStartTime.remove(paramAnimationFrameCallback);
      return true;
    } 
    return false;
  }
  
  public static int getAnimationCount() {
    AnimationHandler animationHandler = sAnimatorHandler.get();
    if (animationHandler == null)
      return 0; 
    return animationHandler.getCallbackSize();
  }
  
  public static void setFrameDelay(long paramLong) {
    getInstance().getProvider().setFrameDelay(paramLong);
  }
  
  public static long getFrameDelay() {
    return getInstance().getProvider().getFrameDelay();
  }
  
  void autoCancelBasedOn(ObjectAnimator paramObjectAnimator) {
    for (int i = this.mAnimationCallbacks.size() - 1; i >= 0; i--) {
      AnimationFrameCallback animationFrameCallback = this.mAnimationCallbacks.get(i);
      if (animationFrameCallback != null)
        if (paramObjectAnimator.shouldAutoCancel(animationFrameCallback))
          ((Animator)this.mAnimationCallbacks.get(i)).cancel();  
    } 
  }
  
  private void cleanUpList() {
    if (this.mListDirty) {
      for (int i = this.mAnimationCallbacks.size() - 1; i >= 0; i--) {
        if (this.mAnimationCallbacks.get(i) == null)
          this.mAnimationCallbacks.remove(i); 
      } 
      this.mListDirty = false;
    } 
  }
  
  private int getCallbackSize() {
    int i = 0;
    int j = this.mAnimationCallbacks.size();
    for (; --j >= 0; j--, i = k) {
      int k = i;
      if (this.mAnimationCallbacks.get(j) != null)
        k = i + 1; 
    } 
    return i;
  }
  
  static interface AnimationFrameCallback {
    void commitAnimationFrame(long param1Long);
    
    boolean doAnimationFrame(long param1Long);
  }
  
  public static interface AnimationFrameCallbackProvider {
    long getFrameDelay();
    
    long getFrameTime();
    
    void postCommitCallback(Runnable param1Runnable);
    
    void postFrameCallback(Choreographer.FrameCallback param1FrameCallback);
    
    void setFrameDelay(long param1Long);
  }
  
  class MyFrameCallbackProvider implements AnimationFrameCallbackProvider {
    final Choreographer mChoreographer = Choreographer.getInstance();
    
    final AnimationHandler this$0;
    
    public void postFrameCallback(Choreographer.FrameCallback param1FrameCallback) {
      this.mChoreographer.postFrameCallback(param1FrameCallback);
    }
    
    public void postCommitCallback(Runnable param1Runnable) {
      this.mChoreographer.postCallback(4, param1Runnable, null);
    }
    
    public long getFrameTime() {
      return this.mChoreographer.getFrameTime();
    }
    
    public long getFrameDelay() {
      return Choreographer.getFrameDelay();
    }
    
    public void setFrameDelay(long param1Long) {
      Choreographer.setFrameDelay(param1Long);
    }
    
    private MyFrameCallbackProvider() {}
  }
}
