package android.animation;

import android.graphics.Path;
import android.graphics.PointF;
import java.util.ArrayList;
import java.util.List;

public class PathKeyframes implements Keyframes {
  private static final ArrayList<Keyframe> EMPTY_KEYFRAMES = new ArrayList<>();
  
  private static final int FRACTION_OFFSET = 0;
  
  private static final int NUM_COMPONENTS = 3;
  
  private static final int X_OFFSET = 1;
  
  private static final int Y_OFFSET = 2;
  
  private float[] mKeyframeData;
  
  private PointF mTempPointF = new PointF();
  
  public PathKeyframes(Path paramPath) {
    this(paramPath, 0.5F);
  }
  
  public PathKeyframes(Path paramPath, float paramFloat) {
    if (paramPath != null && !paramPath.isEmpty()) {
      this.mKeyframeData = paramPath.approximate(paramFloat);
      return;
    } 
    throw new IllegalArgumentException("The path must not be null or empty");
  }
  
  public ArrayList<Keyframe> getKeyframes() {
    return EMPTY_KEYFRAMES;
  }
  
  public Object getValue(float paramFloat) {
    int i = this.mKeyframeData.length / 3;
    if (paramFloat < 0.0F)
      return interpolateInRange(paramFloat, 0, 1); 
    if (paramFloat > 1.0F)
      return interpolateInRange(paramFloat, i - 2, i - 1); 
    if (paramFloat == 0.0F)
      return pointForIndex(0); 
    if (paramFloat == 1.0F)
      return pointForIndex(i - 1); 
    int j = 0;
    i--;
    while (j <= i) {
      int k = (j + i) / 2;
      float f = this.mKeyframeData[k * 3 + 0];
      if (paramFloat < f) {
        i = k - 1;
        continue;
      } 
      if (paramFloat > f) {
        j = k + 1;
        continue;
      } 
      return pointForIndex(k);
    } 
    return interpolateInRange(paramFloat, i, j);
  }
  
  private PointF interpolateInRange(float paramFloat, int paramInt1, int paramInt2) {
    paramInt1 *= 3;
    paramInt2 *= 3;
    float arrayOfFloat[] = this.mKeyframeData, f1 = arrayOfFloat[paramInt1 + 0];
    float f2 = arrayOfFloat[paramInt2 + 0];
    f1 = (paramFloat - f1) / (f2 - f1);
    float f3 = arrayOfFloat[paramInt1 + 1];
    float f4 = arrayOfFloat[paramInt2 + 1];
    f2 = arrayOfFloat[paramInt1 + 2];
    paramFloat = arrayOfFloat[paramInt2 + 2];
    f3 = interpolate(f1, f3, f4);
    paramFloat = interpolate(f1, f2, paramFloat);
    this.mTempPointF.set(f3, paramFloat);
    return this.mTempPointF;
  }
  
  public void setEvaluator(TypeEvaluator paramTypeEvaluator) {}
  
  public Class getType() {
    return PointF.class;
  }
  
  public Keyframes clone() {
    Keyframes keyframes = null;
    try {
      Keyframes keyframes1 = (Keyframes)super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {}
    return keyframes;
  }
  
  private PointF pointForIndex(int paramInt) {
    paramInt *= 3;
    PointF pointF = this.mTempPointF;
    float[] arrayOfFloat = this.mKeyframeData;
    pointF.set(arrayOfFloat[paramInt + 1], arrayOfFloat[paramInt + 2]);
    return this.mTempPointF;
  }
  
  private static float interpolate(float paramFloat1, float paramFloat2, float paramFloat3) {
    return (paramFloat3 - paramFloat2) * paramFloat1 + paramFloat2;
  }
  
  public Keyframes.FloatKeyframes createXFloatKeyframes() {
    return (Keyframes.FloatKeyframes)new Object(this);
  }
  
  public Keyframes.FloatKeyframes createYFloatKeyframes() {
    return (Keyframes.FloatKeyframes)new Object(this);
  }
  
  public Keyframes.IntKeyframes createXIntKeyframes() {
    return (Keyframes.IntKeyframes)new Object(this);
  }
  
  public Keyframes.IntKeyframes createYIntKeyframes() {
    return (Keyframes.IntKeyframes)new Object(this);
  }
  
  private static abstract class SimpleKeyframes implements Keyframes {
    private SimpleKeyframes() {}
    
    public void setEvaluator(TypeEvaluator param1TypeEvaluator) {}
    
    public ArrayList<Keyframe> getKeyframes() {
      return PathKeyframes.EMPTY_KEYFRAMES;
    }
    
    public Keyframes clone() {
      Keyframes keyframes = null;
      try {
        Keyframes keyframes1 = (Keyframes)super.clone();
      } catch (CloneNotSupportedException cloneNotSupportedException) {}
      return keyframes;
    }
  }
  
  class IntKeyframesBase extends SimpleKeyframes implements Keyframes.IntKeyframes {
    public Class getType() {
      return Integer.class;
    }
    
    public Object getValue(float param1Float) {
      return Integer.valueOf(getIntValue(param1Float));
    }
  }
  
  class FloatKeyframesBase extends SimpleKeyframes implements Keyframes.FloatKeyframes {
    public Class getType() {
      return Float.class;
    }
    
    public Object getValue(float param1Float) {
      return Float.valueOf(getFloatValue(param1Float));
    }
  }
}
