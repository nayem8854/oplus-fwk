package android.animation;

import android.content.res.ConstantState;
import java.util.ArrayList;
import java.util.Collection;

public abstract class Animator implements Cloneable {
  ArrayList<AnimatorListener> mListeners = null;
  
  ArrayList<AnimatorPauseListener> mPauseListeners = null;
  
  boolean mPaused = false;
  
  int mChangingConfigurations = 0;
  
  public static final long DURATION_INFINITE = -1L;
  
  private AnimatorConstantState mConstantState;
  
  public void start() {}
  
  public void cancel() {}
  
  public void end() {}
  
  public void pause() {
    if (isStarted() && !this.mPaused) {
      this.mPaused = true;
      ArrayList<AnimatorPauseListener> arrayList = this.mPauseListeners;
      if (arrayList != null) {
        arrayList = (ArrayList<AnimatorPauseListener>)arrayList.clone();
        int i = arrayList.size();
        for (byte b = 0; b < i; b++)
          ((AnimatorPauseListener)arrayList.get(b)).onAnimationPause(this); 
      } 
    } 
  }
  
  public void resume() {
    if (this.mPaused) {
      this.mPaused = false;
      ArrayList<AnimatorPauseListener> arrayList = this.mPauseListeners;
      if (arrayList != null) {
        arrayList = (ArrayList<AnimatorPauseListener>)arrayList.clone();
        int i = arrayList.size();
        for (byte b = 0; b < i; b++)
          ((AnimatorPauseListener)arrayList.get(b)).onAnimationResume(this); 
      } 
    } 
  }
  
  public boolean isPaused() {
    return this.mPaused;
  }
  
  public long getTotalDuration() {
    long l = getDuration();
    if (l == -1L)
      return -1L; 
    return getStartDelay() + l;
  }
  
  public TimeInterpolator getInterpolator() {
    return null;
  }
  
  public boolean isStarted() {
    return isRunning();
  }
  
  public void addListener(AnimatorListener paramAnimatorListener) {
    if (this.mListeners == null)
      this.mListeners = new ArrayList<>(); 
    this.mListeners.add(paramAnimatorListener);
  }
  
  public void removeListener(AnimatorListener paramAnimatorListener) {
    ArrayList<AnimatorListener> arrayList = this.mListeners;
    if (arrayList == null)
      return; 
    try {
      arrayList.remove(paramAnimatorListener);
      if (this.mListeners.size() == 0)
        this.mListeners = null; 
    } catch (NullPointerException nullPointerException) {}
  }
  
  public ArrayList<AnimatorListener> getListeners() {
    return this.mListeners;
  }
  
  public void addPauseListener(AnimatorPauseListener paramAnimatorPauseListener) {
    if (this.mPauseListeners == null)
      this.mPauseListeners = new ArrayList<>(); 
    this.mPauseListeners.add(paramAnimatorPauseListener);
  }
  
  public void removePauseListener(AnimatorPauseListener paramAnimatorPauseListener) {
    ArrayList<AnimatorPauseListener> arrayList = this.mPauseListeners;
    if (arrayList == null)
      return; 
    arrayList.remove(paramAnimatorPauseListener);
    if (this.mPauseListeners.size() == 0)
      this.mPauseListeners = null; 
  }
  
  public void removeAllListeners() {
    ArrayList<AnimatorListener> arrayList1 = this.mListeners;
    if (arrayList1 != null) {
      arrayList1.clear();
      this.mListeners = null;
    } 
    ArrayList<AnimatorPauseListener> arrayList = this.mPauseListeners;
    if (arrayList != null) {
      arrayList.clear();
      this.mPauseListeners = null;
    } 
  }
  
  public int getChangingConfigurations() {
    return this.mChangingConfigurations;
  }
  
  public void setChangingConfigurations(int paramInt) {
    this.mChangingConfigurations = paramInt;
  }
  
  public void appendChangingConfigurations(int paramInt) {
    this.mChangingConfigurations |= paramInt;
  }
  
  public ConstantState<Animator> createConstantState() {
    return new AnimatorConstantState(this);
  }
  
  public Animator clone() {
    try {
      Animator animator = (Animator)super.clone();
      if (this.mListeners != null) {
        ArrayList<AnimatorListener> arrayList = new ArrayList();
        this((Collection)this.mListeners);
        animator.mListeners = arrayList;
      } 
      if (this.mPauseListeners != null) {
        ArrayList<AnimatorPauseListener> arrayList = new ArrayList();
        this((Collection)this.mPauseListeners);
        animator.mPauseListeners = arrayList;
      } 
      return animator;
    } catch (CloneNotSupportedException cloneNotSupportedException) {
      throw new AssertionError();
    } 
  }
  
  public void setupStartValues() {}
  
  public void setupEndValues() {}
  
  public void setTarget(Object paramObject) {}
  
  public boolean canReverse() {
    return false;
  }
  
  public void reverse() {
    throw new IllegalStateException("Reverse is not supported");
  }
  
  boolean pulseAnimationFrame(long paramLong) {
    return false;
  }
  
  void startWithoutPulsing(boolean paramBoolean) {
    if (paramBoolean) {
      reverse();
    } else {
      start();
    } 
  }
  
  void skipToEndValue(boolean paramBoolean) {}
  
  boolean isInitialized() {
    return true;
  }
  
  void animateBasedOnPlayTime(long paramLong1, long paramLong2, boolean paramBoolean) {}
  
  public static interface AnimatorPauseListener {
    void onAnimationPause(Animator param1Animator);
    
    void onAnimationResume(Animator param1Animator);
  }
  
  public static interface AnimatorListener {
    default void onAnimationStart(Animator param1Animator, boolean param1Boolean) {
      onAnimationStart(param1Animator);
    }
    
    void onAnimationStart(Animator param1Animator);
    
    void onAnimationRepeat(Animator param1Animator);
    
    default void onAnimationEnd(Animator param1Animator, boolean param1Boolean) {
      onAnimationEnd(param1Animator);
    }
    
    void onAnimationEnd(Animator param1Animator);
    
    void onAnimationCancel(Animator param1Animator);
  }
  
  public void setAllowRunningAsynchronously(boolean paramBoolean) {}
  
  public abstract long getDuration();
  
  public abstract long getStartDelay();
  
  public abstract boolean isRunning();
  
  public abstract Animator setDuration(long paramLong);
  
  public abstract void setInterpolator(TimeInterpolator paramTimeInterpolator);
  
  public abstract void setStartDelay(long paramLong);
  
  class AnimatorConstantState extends ConstantState<Animator> {
    final Animator mAnimator;
    
    int mChangingConf;
    
    public AnimatorConstantState(Animator this$0) {
      this.mAnimator = this$0;
      Animator.access$002(this$0, this);
      this.mChangingConf = this.mAnimator.getChangingConfigurations();
    }
    
    public int getChangingConfigurations() {
      return this.mChangingConf;
    }
    
    public Animator newInstance() {
      Animator animator = this.mAnimator.clone();
      Animator.access$002(animator, this);
      return animator;
    }
  }
}
