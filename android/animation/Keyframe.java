package android.animation;

public abstract class Keyframe implements Cloneable {
  float mFraction;
  
  boolean mHasValue;
  
  private TimeInterpolator mInterpolator = null;
  
  Class mValueType;
  
  boolean mValueWasSetOnStart;
  
  public static Keyframe ofInt(float paramFloat, int paramInt) {
    return new IntKeyframe(paramFloat, paramInt);
  }
  
  public static Keyframe ofInt(float paramFloat) {
    return new IntKeyframe(paramFloat);
  }
  
  public static Keyframe ofFloat(float paramFloat1, float paramFloat2) {
    return new FloatKeyframe(paramFloat1, paramFloat2);
  }
  
  public static Keyframe ofFloat(float paramFloat) {
    return new FloatKeyframe(paramFloat);
  }
  
  public static Keyframe ofObject(float paramFloat, Object paramObject) {
    return new ObjectKeyframe(paramFloat, paramObject);
  }
  
  public static Keyframe ofObject(float paramFloat) {
    return new ObjectKeyframe(paramFloat, null);
  }
  
  public boolean hasValue() {
    return this.mHasValue;
  }
  
  boolean valueWasSetOnStart() {
    return this.mValueWasSetOnStart;
  }
  
  void setValueWasSetOnStart(boolean paramBoolean) {
    this.mValueWasSetOnStart = paramBoolean;
  }
  
  public float getFraction() {
    return this.mFraction;
  }
  
  public void setFraction(float paramFloat) {
    this.mFraction = paramFloat;
  }
  
  public TimeInterpolator getInterpolator() {
    return this.mInterpolator;
  }
  
  public void setInterpolator(TimeInterpolator paramTimeInterpolator) {
    this.mInterpolator = paramTimeInterpolator;
  }
  
  public Class getType() {
    return this.mValueType;
  }
  
  public abstract Keyframe clone();
  
  public abstract Object getValue();
  
  public abstract void setValue(Object paramObject);
  
  class ObjectKeyframe extends Keyframe {
    Object mValue;
    
    ObjectKeyframe(Keyframe this$0, Object<?> param1Object) {
      Class<Object> clazz;
      boolean bool;
      this.mFraction = this$0;
      this.mValue = param1Object;
      if (param1Object != null) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mHasValue = bool;
      if (this.mHasValue) {
        param1Object = (Object<?>)param1Object.getClass();
      } else {
        clazz = Object.class;
      } 
      this.mValueType = clazz;
    }
    
    public Object getValue() {
      return this.mValue;
    }
    
    public void setValue(Object param1Object) {
      boolean bool;
      this.mValue = param1Object;
      if (param1Object != null) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mHasValue = bool;
    }
    
    public ObjectKeyframe clone() {
      float f = getFraction();
      if (hasValue()) {
        objectKeyframe = (ObjectKeyframe)this.mValue;
      } else {
        objectKeyframe = null;
      } 
      ObjectKeyframe objectKeyframe = new ObjectKeyframe(f, objectKeyframe);
      objectKeyframe.mValueWasSetOnStart = this.mValueWasSetOnStart;
      objectKeyframe.setInterpolator(getInterpolator());
      return objectKeyframe;
    }
  }
  
  class IntKeyframe extends Keyframe {
    int mValue;
    
    IntKeyframe(Keyframe this$0, int param1Int) {
      this.mFraction = this$0;
      this.mValue = param1Int;
      this.mValueType = int.class;
      this.mHasValue = true;
    }
    
    IntKeyframe(Keyframe this$0) {
      this.mFraction = this$0;
      this.mValueType = int.class;
    }
    
    public int getIntValue() {
      return this.mValue;
    }
    
    public Object getValue() {
      return Integer.valueOf(this.mValue);
    }
    
    public void setValue(Object param1Object) {
      if (param1Object != null && param1Object.getClass() == Integer.class) {
        this.mValue = ((Integer)param1Object).intValue();
        this.mHasValue = true;
      } 
    }
    
    public IntKeyframe clone() {
      IntKeyframe intKeyframe;
      if (this.mHasValue) {
        intKeyframe = new IntKeyframe(getFraction(), this.mValue);
      } else {
        intKeyframe = new IntKeyframe(getFraction());
      } 
      intKeyframe.setInterpolator(getInterpolator());
      intKeyframe.mValueWasSetOnStart = this.mValueWasSetOnStart;
      return intKeyframe;
    }
  }
  
  class FloatKeyframe extends Keyframe {
    float mValue;
    
    FloatKeyframe(Keyframe this$0, float param1Float1) {
      this.mFraction = this$0;
      this.mValue = param1Float1;
      this.mValueType = float.class;
      this.mHasValue = true;
    }
    
    FloatKeyframe(Keyframe this$0) {
      this.mFraction = this$0;
      this.mValueType = float.class;
    }
    
    public float getFloatValue() {
      return this.mValue;
    }
    
    public Object getValue() {
      return Float.valueOf(this.mValue);
    }
    
    public void setValue(Object param1Object) {
      if (param1Object != null && param1Object.getClass() == Float.class) {
        this.mValue = ((Float)param1Object).floatValue();
        this.mHasValue = true;
      } 
    }
    
    public FloatKeyframe clone() {
      FloatKeyframe floatKeyframe;
      if (this.mHasValue) {
        floatKeyframe = new FloatKeyframe(getFraction(), this.mValue);
      } else {
        floatKeyframe = new FloatKeyframe(getFraction());
      } 
      floatKeyframe.setInterpolator(getInterpolator());
      floatKeyframe.mValueWasSetOnStart = this.mValueWasSetOnStart;
      return floatKeyframe;
    }
  }
}
