package android.animation;

public abstract class TypeConverter<T, V> {
  private Class<T> mFromClass;
  
  private Class<V> mToClass;
  
  public TypeConverter(Class<T> paramClass, Class<V> paramClass1) {
    this.mFromClass = paramClass;
    this.mToClass = paramClass1;
  }
  
  Class<V> getTargetType() {
    return this.mToClass;
  }
  
  Class<T> getSourceType() {
    return this.mFromClass;
  }
  
  public abstract V convert(T paramT);
}
