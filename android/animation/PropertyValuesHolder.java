package android.animation;

import android.graphics.Path;
import android.graphics.PointF;
import android.util.FloatProperty;
import android.util.IntProperty;
import android.util.Log;
import android.util.PathParser;
import android.util.Property;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;

public class PropertyValuesHolder implements Cloneable {
  Method mSetter = null;
  
  private Method mGetter = null;
  
  Keyframes mKeyframes = null;
  
  private static final TypeEvaluator sIntEvaluator = new IntEvaluator();
  
  static {
    sFloatEvaluator = new FloatEvaluator();
    FLOAT_VARIANTS = new Class[] { float.class, Float.class, double.class, int.class, Double.class, Integer.class };
    INTEGER_VARIANTS = new Class[] { int.class, Integer.class, float.class, double.class, Float.class, Double.class };
    DOUBLE_VARIANTS = new Class[] { double.class, Double.class, float.class, int.class, Float.class, Integer.class };
    sSetterPropertyMap = new HashMap<>();
    sGetterPropertyMap = new HashMap<>();
  }
  
  final Object[] mTmpValueArray = new Object[1];
  
  private static Class[] DOUBLE_VARIANTS;
  
  private static Class[] FLOAT_VARIANTS;
  
  private static Class[] INTEGER_VARIANTS;
  
  private static final TypeEvaluator sFloatEvaluator;
  
  private static final HashMap<Class, HashMap<String, Method>> sGetterPropertyMap;
  
  private static final HashMap<Class, HashMap<String, Method>> sSetterPropertyMap;
  
  private Object mAnimatedValue;
  
  private TypeConverter mConverter;
  
  private TypeEvaluator mEvaluator;
  
  protected Property mProperty;
  
  String mPropertyName;
  
  Class mValueType;
  
  private PropertyValuesHolder(String paramString) {
    this.mPropertyName = paramString;
  }
  
  private PropertyValuesHolder(Property paramProperty) {
    this.mProperty = paramProperty;
    if (paramProperty != null)
      this.mPropertyName = paramProperty.getName(); 
  }
  
  public static PropertyValuesHolder ofInt(String paramString, int... paramVarArgs) {
    return new IntPropertyValuesHolder(paramString, paramVarArgs);
  }
  
  public static PropertyValuesHolder ofInt(Property<?, Integer> paramProperty, int... paramVarArgs) {
    return new IntPropertyValuesHolder(paramProperty, paramVarArgs);
  }
  
  public static PropertyValuesHolder ofMultiInt(String paramString, int[][] paramArrayOfint) {
    if (paramArrayOfint.length >= 2) {
      int i = 0;
      for (byte b = 0; b < paramArrayOfint.length; ) {
        if (paramArrayOfint[b] != null) {
          int j = (paramArrayOfint[b]).length;
          if (b == 0) {
            i = j;
          } else if (j != i) {
            throw new IllegalArgumentException("Values must all have the same length");
          } 
          b++;
        } 
        throw new IllegalArgumentException("values must not be null");
      } 
      IntArrayEvaluator intArrayEvaluator = new IntArrayEvaluator(new int[i]);
      return new MultiIntValuesHolder(paramString, null, intArrayEvaluator, (Object[])paramArrayOfint);
    } 
    throw new IllegalArgumentException("At least 2 values must be supplied");
  }
  
  public static PropertyValuesHolder ofMultiInt(String paramString, Path paramPath) {
    PathKeyframes pathKeyframes = KeyframeSet.ofPath(paramPath);
    PointFToIntArray pointFToIntArray = new PointFToIntArray();
    return new MultiIntValuesHolder(paramString, pointFToIntArray, null, pathKeyframes);
  }
  
  @SafeVarargs
  public static <V> PropertyValuesHolder ofMultiInt(String paramString, TypeConverter<V, int[]> paramTypeConverter, TypeEvaluator<V> paramTypeEvaluator, V... paramVarArgs) {
    return new MultiIntValuesHolder(paramString, paramTypeConverter, paramTypeEvaluator, (Object[])paramVarArgs);
  }
  
  public static <T> PropertyValuesHolder ofMultiInt(String paramString, TypeConverter<T, int[]> paramTypeConverter, TypeEvaluator<T> paramTypeEvaluator, Keyframe... paramVarArgs) {
    KeyframeSet keyframeSet = KeyframeSet.ofKeyframe(paramVarArgs);
    return new MultiIntValuesHolder(paramString, paramTypeConverter, paramTypeEvaluator, keyframeSet);
  }
  
  public static PropertyValuesHolder ofFloat(String paramString, float... paramVarArgs) {
    return new FloatPropertyValuesHolder(paramString, paramVarArgs);
  }
  
  public static PropertyValuesHolder ofFloat(Property<?, Float> paramProperty, float... paramVarArgs) {
    return new FloatPropertyValuesHolder(paramProperty, paramVarArgs);
  }
  
  public static PropertyValuesHolder ofMultiFloat(String paramString, float[][] paramArrayOffloat) {
    if (paramArrayOffloat.length >= 2) {
      int i = 0;
      for (byte b = 0; b < paramArrayOffloat.length; ) {
        if (paramArrayOffloat[b] != null) {
          int j = (paramArrayOffloat[b]).length;
          if (b == 0) {
            i = j;
          } else if (j != i) {
            throw new IllegalArgumentException("Values must all have the same length");
          } 
          b++;
        } 
        throw new IllegalArgumentException("values must not be null");
      } 
      FloatArrayEvaluator floatArrayEvaluator = new FloatArrayEvaluator(new float[i]);
      return new MultiFloatValuesHolder(paramString, null, floatArrayEvaluator, (Object[])paramArrayOffloat);
    } 
    throw new IllegalArgumentException("At least 2 values must be supplied");
  }
  
  public static PropertyValuesHolder ofMultiFloat(String paramString, Path paramPath) {
    PathKeyframes pathKeyframes = KeyframeSet.ofPath(paramPath);
    PointFToFloatArray pointFToFloatArray = new PointFToFloatArray();
    return new MultiFloatValuesHolder(paramString, pointFToFloatArray, null, pathKeyframes);
  }
  
  @SafeVarargs
  public static <V> PropertyValuesHolder ofMultiFloat(String paramString, TypeConverter<V, float[]> paramTypeConverter, TypeEvaluator<V> paramTypeEvaluator, V... paramVarArgs) {
    return new MultiFloatValuesHolder(paramString, paramTypeConverter, paramTypeEvaluator, (Object[])paramVarArgs);
  }
  
  public static <T> PropertyValuesHolder ofMultiFloat(String paramString, TypeConverter<T, float[]> paramTypeConverter, TypeEvaluator<T> paramTypeEvaluator, Keyframe... paramVarArgs) {
    KeyframeSet keyframeSet = KeyframeSet.ofKeyframe(paramVarArgs);
    return new MultiFloatValuesHolder(paramString, paramTypeConverter, paramTypeEvaluator, keyframeSet);
  }
  
  public static PropertyValuesHolder ofObject(String paramString, TypeEvaluator paramTypeEvaluator, Object... paramVarArgs) {
    PropertyValuesHolder propertyValuesHolder = new PropertyValuesHolder(paramString);
    propertyValuesHolder.setObjectValues(paramVarArgs);
    propertyValuesHolder.setEvaluator(paramTypeEvaluator);
    return propertyValuesHolder;
  }
  
  public static PropertyValuesHolder ofObject(String paramString, TypeConverter<PointF, ?> paramTypeConverter, Path paramPath) {
    PropertyValuesHolder propertyValuesHolder = new PropertyValuesHolder(paramString);
    propertyValuesHolder.mKeyframes = KeyframeSet.ofPath(paramPath);
    propertyValuesHolder.mValueType = PointF.class;
    propertyValuesHolder.setConverter(paramTypeConverter);
    return propertyValuesHolder;
  }
  
  @SafeVarargs
  public static <V> PropertyValuesHolder ofObject(Property paramProperty, TypeEvaluator<V> paramTypeEvaluator, V... paramVarArgs) {
    PropertyValuesHolder propertyValuesHolder = new PropertyValuesHolder(paramProperty);
    propertyValuesHolder.setObjectValues((Object[])paramVarArgs);
    propertyValuesHolder.setEvaluator(paramTypeEvaluator);
    return propertyValuesHolder;
  }
  
  @SafeVarargs
  public static <T, V> PropertyValuesHolder ofObject(Property<?, V> paramProperty, TypeConverter<T, V> paramTypeConverter, TypeEvaluator<T> paramTypeEvaluator, T... paramVarArgs) {
    PropertyValuesHolder propertyValuesHolder = new PropertyValuesHolder(paramProperty);
    propertyValuesHolder.setConverter(paramTypeConverter);
    propertyValuesHolder.setObjectValues((Object[])paramVarArgs);
    propertyValuesHolder.setEvaluator(paramTypeEvaluator);
    return propertyValuesHolder;
  }
  
  public static <V> PropertyValuesHolder ofObject(Property<?, V> paramProperty, TypeConverter<PointF, V> paramTypeConverter, Path paramPath) {
    PropertyValuesHolder propertyValuesHolder = new PropertyValuesHolder(paramProperty);
    propertyValuesHolder.mKeyframes = KeyframeSet.ofPath(paramPath);
    propertyValuesHolder.mValueType = PointF.class;
    propertyValuesHolder.setConverter(paramTypeConverter);
    return propertyValuesHolder;
  }
  
  public static PropertyValuesHolder ofKeyframe(String paramString, Keyframe... paramVarArgs) {
    KeyframeSet keyframeSet = KeyframeSet.ofKeyframe(paramVarArgs);
    return ofKeyframes(paramString, keyframeSet);
  }
  
  public static PropertyValuesHolder ofKeyframe(Property paramProperty, Keyframe... paramVarArgs) {
    KeyframeSet keyframeSet = KeyframeSet.ofKeyframe(paramVarArgs);
    return ofKeyframes(paramProperty, keyframeSet);
  }
  
  static PropertyValuesHolder ofKeyframes(String paramString, Keyframes paramKeyframes) {
    if (paramKeyframes instanceof Keyframes.IntKeyframes)
      return new IntPropertyValuesHolder(paramString, (Keyframes.IntKeyframes)paramKeyframes); 
    if (paramKeyframes instanceof Keyframes.FloatKeyframes)
      return new FloatPropertyValuesHolder(paramString, (Keyframes.FloatKeyframes)paramKeyframes); 
    PropertyValuesHolder propertyValuesHolder = new PropertyValuesHolder(paramString);
    propertyValuesHolder.mKeyframes = paramKeyframes;
    propertyValuesHolder.mValueType = paramKeyframes.getType();
    return propertyValuesHolder;
  }
  
  static PropertyValuesHolder ofKeyframes(Property paramProperty, Keyframes paramKeyframes) {
    if (paramKeyframes instanceof Keyframes.IntKeyframes)
      return new IntPropertyValuesHolder(paramProperty, (Keyframes.IntKeyframes)paramKeyframes); 
    if (paramKeyframes instanceof Keyframes.FloatKeyframes)
      return new FloatPropertyValuesHolder(paramProperty, (Keyframes.FloatKeyframes)paramKeyframes); 
    PropertyValuesHolder propertyValuesHolder = new PropertyValuesHolder(paramProperty);
    propertyValuesHolder.mKeyframes = paramKeyframes;
    propertyValuesHolder.mValueType = paramKeyframes.getType();
    return propertyValuesHolder;
  }
  
  public void setIntValues(int... paramVarArgs) {
    this.mValueType = int.class;
    this.mKeyframes = KeyframeSet.ofInt(paramVarArgs);
  }
  
  public void setFloatValues(float... paramVarArgs) {
    this.mValueType = float.class;
    this.mKeyframes = KeyframeSet.ofFloat(paramVarArgs);
  }
  
  public void setKeyframes(Keyframe... paramVarArgs) {
    int i = paramVarArgs.length;
    Keyframe[] arrayOfKeyframe = new Keyframe[Math.max(i, 2)];
    this.mValueType = paramVarArgs[0].getType();
    for (byte b = 0; b < i; b++)
      arrayOfKeyframe[b] = paramVarArgs[b]; 
    this.mKeyframes = new KeyframeSet(arrayOfKeyframe);
  }
  
  public void setObjectValues(Object... paramVarArgs) {
    this.mValueType = paramVarArgs[0].getClass();
    KeyframeSet keyframeSet = KeyframeSet.ofObject(paramVarArgs);
    TypeEvaluator typeEvaluator = this.mEvaluator;
    if (typeEvaluator != null)
      keyframeSet.setEvaluator(typeEvaluator); 
  }
  
  public void setConverter(TypeConverter paramTypeConverter) {
    this.mConverter = paramTypeConverter;
  }
  
  private Method getPropertyFunction(Class paramClass1, String paramString, Class paramClass2) {
    Method method1 = null, method2 = null;
    String str = getMethodName(paramString, this.mPropertyName);
    if (paramClass2 == null) {
      try {
        method2 = method1 = paramClass1.getMethod(str, null);
        Method method = method2;
      } catch (NoSuchMethodException noSuchMethodException1) {
        Method method = method2;
      } 
    } else {
      Class[] arrayOfClass1, arrayOfClass2 = new Class[1];
      if (paramClass2.equals(Float.class)) {
        arrayOfClass1 = FLOAT_VARIANTS;
      } else if (paramClass2.equals(Integer.class)) {
        arrayOfClass1 = INTEGER_VARIANTS;
      } else if (paramClass2.equals(Double.class)) {
        arrayOfClass1 = DOUBLE_VARIANTS;
      } else {
        arrayOfClass1 = new Class[1];
        arrayOfClass1[0] = paramClass2;
      } 
      int i = arrayOfClass1.length;
      byte b = 0;
      while (true) {
        noSuchMethodException2 = noSuchMethodException1;
        if (b < i) {
          Class clazz = arrayOfClass1[b];
          arrayOfClass2[0] = clazz;
          try {
            Method method4 = paramClass1.getMethod(str, arrayOfClass2);
            Method method3 = method4;
            if (this.mConverter == null) {
              method3 = method4;
              this.mValueType = clazz;
            } 
            return method4;
          } catch (NoSuchMethodException noSuchMethodException2) {}
          b++;
          continue;
        } 
        break;
      } 
    } 
    if (noSuchMethodException2 == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Method ");
      String str2 = this.mPropertyName;
      stringBuilder.append(getMethodName(paramString, str2));
      stringBuilder.append("() with type ");
      stringBuilder.append(paramClass2);
      stringBuilder.append(" not found on target class ");
      stringBuilder.append(paramClass1);
      String str1 = stringBuilder.toString();
      Log.w("PropertyValuesHolder", str1);
    } 
    return (Method)noSuchMethodException2;
  }
  
  private Method setupSetterOrGetter(Class paramClass1, HashMap<Class, HashMap<String, Method>> paramHashMap, String paramString, Class paramClass2) {
    synchronized (null) {
      HashMap<Object, Object> hashMap = (HashMap)paramHashMap.get(paramClass1);
      boolean bool = false;
      Method method = null;
      if (hashMap != null) {
        boolean bool1 = hashMap.containsKey(this.mPropertyName);
        method = null;
        bool = bool1;
        if (bool1) {
          method = (Method)hashMap.get(this.mPropertyName);
          bool = bool1;
        } 
      } 
      if (!bool) {
        method = getPropertyFunction(paramClass1, paramString, paramClass2);
        HashMap<Object, Object> hashMap1 = hashMap;
        if (hashMap == null) {
          hashMap1 = new HashMap<>();
          this();
          paramHashMap.put(paramClass1, hashMap1);
        } 
        hashMap1.put(this.mPropertyName, method);
      } 
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/util/HashMap<[ObjectType{java/lang/Class}, ObjectType{java/util/HashMap<[ObjectType{java/lang/String}, ObjectType{java/lang/reflect/Method}]>}]>}, name=paramHashMap} */
      return method;
    } 
  }
  
  void setupSetter(Class paramClass) {
    Class clazz;
    TypeConverter typeConverter = this.mConverter;
    if (typeConverter == null) {
      clazz = this.mValueType;
    } else {
      clazz = clazz.getTargetType();
    } 
    this.mSetter = setupSetterOrGetter(paramClass, sSetterPropertyMap, "set", clazz);
  }
  
  private void setupGetter(Class paramClass) {
    this.mGetter = setupSetterOrGetter(paramClass, sGetterPropertyMap, "get", null);
  }
  
  void setupSetterAndGetter(Object paramObject) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mProperty : Landroid/util/Property;
    //   4: ifnull -> 190
    //   7: aconst_null
    //   8: astore_2
    //   9: aload_0
    //   10: getfield mKeyframes : Landroid/animation/Keyframes;
    //   13: invokeinterface getKeyframes : ()Ljava/util/List;
    //   18: astore_3
    //   19: aload_3
    //   20: ifnonnull -> 29
    //   23: iconst_0
    //   24: istore #4
    //   26: goto -> 37
    //   29: aload_3
    //   30: invokeinterface size : ()I
    //   35: istore #4
    //   37: iconst_0
    //   38: istore #5
    //   40: iload #5
    //   42: iload #4
    //   44: if_icmpge -> 122
    //   47: aload_3
    //   48: iload #5
    //   50: invokeinterface get : (I)Ljava/lang/Object;
    //   55: checkcast android/animation/Keyframe
    //   58: astore #6
    //   60: aload #6
    //   62: invokevirtual hasValue : ()Z
    //   65: ifeq -> 79
    //   68: aload_2
    //   69: astore #7
    //   71: aload #6
    //   73: invokevirtual valueWasSetOnStart : ()Z
    //   76: ifeq -> 113
    //   79: aload_2
    //   80: astore #7
    //   82: aload_2
    //   83: ifnonnull -> 100
    //   86: aload_0
    //   87: aload_0
    //   88: getfield mProperty : Landroid/util/Property;
    //   91: aload_1
    //   92: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   95: invokespecial convertBack : (Ljava/lang/Object;)Ljava/lang/Object;
    //   98: astore #7
    //   100: aload #6
    //   102: aload #7
    //   104: invokevirtual setValue : (Ljava/lang/Object;)V
    //   107: aload #6
    //   109: iconst_1
    //   110: invokevirtual setValueWasSetOnStart : (Z)V
    //   113: iinc #5, 1
    //   116: aload #7
    //   118: astore_2
    //   119: goto -> 40
    //   122: return
    //   123: astore_2
    //   124: new java/lang/StringBuilder
    //   127: dup
    //   128: invokespecial <init> : ()V
    //   131: astore_2
    //   132: aload_2
    //   133: ldc_w 'No such property ('
    //   136: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   139: pop
    //   140: aload_2
    //   141: aload_0
    //   142: getfield mProperty : Landroid/util/Property;
    //   145: invokevirtual getName : ()Ljava/lang/String;
    //   148: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   151: pop
    //   152: aload_2
    //   153: ldc_w ') on target object '
    //   156: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   159: pop
    //   160: aload_2
    //   161: aload_1
    //   162: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   165: pop
    //   166: aload_2
    //   167: ldc_w '. Trying reflection instead'
    //   170: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   173: pop
    //   174: ldc_w 'PropertyValuesHolder'
    //   177: aload_2
    //   178: invokevirtual toString : ()Ljava/lang/String;
    //   181: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   184: pop
    //   185: aload_0
    //   186: aconst_null
    //   187: putfield mProperty : Landroid/util/Property;
    //   190: aload_0
    //   191: getfield mProperty : Landroid/util/Property;
    //   194: ifnonnull -> 370
    //   197: aload_1
    //   198: invokevirtual getClass : ()Ljava/lang/Class;
    //   201: astore_2
    //   202: aload_0
    //   203: getfield mSetter : Ljava/lang/reflect/Method;
    //   206: ifnonnull -> 214
    //   209: aload_0
    //   210: aload_2
    //   211: invokevirtual setupSetter : (Ljava/lang/Class;)V
    //   214: aload_0
    //   215: getfield mKeyframes : Landroid/animation/Keyframes;
    //   218: invokeinterface getKeyframes : ()Ljava/util/List;
    //   223: astore #7
    //   225: aload #7
    //   227: ifnonnull -> 236
    //   230: iconst_0
    //   231: istore #4
    //   233: goto -> 245
    //   236: aload #7
    //   238: invokeinterface size : ()I
    //   243: istore #4
    //   245: iconst_0
    //   246: istore #5
    //   248: iload #5
    //   250: iload #4
    //   252: if_icmpge -> 370
    //   255: aload #7
    //   257: iload #5
    //   259: invokeinterface get : (I)Ljava/lang/Object;
    //   264: checkcast android/animation/Keyframe
    //   267: astore #6
    //   269: aload #6
    //   271: invokevirtual hasValue : ()Z
    //   274: ifeq -> 285
    //   277: aload #6
    //   279: invokevirtual valueWasSetOnStart : ()Z
    //   282: ifeq -> 364
    //   285: aload_0
    //   286: getfield mGetter : Ljava/lang/reflect/Method;
    //   289: ifnonnull -> 305
    //   292: aload_0
    //   293: aload_2
    //   294: invokespecial setupGetter : (Ljava/lang/Class;)V
    //   297: aload_0
    //   298: getfield mGetter : Ljava/lang/reflect/Method;
    //   301: ifnonnull -> 305
    //   304: return
    //   305: aload_0
    //   306: aload_0
    //   307: getfield mGetter : Ljava/lang/reflect/Method;
    //   310: aload_1
    //   311: iconst_0
    //   312: anewarray java/lang/Object
    //   315: invokevirtual invoke : (Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   318: invokespecial convertBack : (Ljava/lang/Object;)Ljava/lang/Object;
    //   321: astore_3
    //   322: aload #6
    //   324: aload_3
    //   325: invokevirtual setValue : (Ljava/lang/Object;)V
    //   328: aload #6
    //   330: iconst_1
    //   331: invokevirtual setValueWasSetOnStart : (Z)V
    //   334: goto -> 364
    //   337: astore_3
    //   338: ldc_w 'PropertyValuesHolder'
    //   341: aload_3
    //   342: invokevirtual toString : ()Ljava/lang/String;
    //   345: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   348: pop
    //   349: goto -> 364
    //   352: astore_3
    //   353: ldc_w 'PropertyValuesHolder'
    //   356: aload_3
    //   357: invokevirtual toString : ()Ljava/lang/String;
    //   360: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   363: pop
    //   364: iinc #5, 1
    //   367: goto -> 248
    //   370: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #810	-> 0
    //   #813	-> 7
    //   #814	-> 9
    //   #815	-> 19
    //   #816	-> 37
    //   #817	-> 47
    //   #818	-> 60
    //   #819	-> 79
    //   #820	-> 86
    //   #822	-> 100
    //   #823	-> 107
    //   #816	-> 113
    //   #826	-> 122
    //   #827	-> 123
    //   #828	-> 124
    //   #830	-> 185
    //   #834	-> 190
    //   #835	-> 197
    //   #836	-> 202
    //   #837	-> 209
    //   #839	-> 214
    //   #840	-> 225
    //   #841	-> 245
    //   #842	-> 255
    //   #843	-> 269
    //   #844	-> 285
    //   #845	-> 292
    //   #846	-> 297
    //   #848	-> 304
    //   #852	-> 305
    //   #853	-> 322
    //   #854	-> 328
    //   #857	-> 337
    //   #858	-> 338
    //   #855	-> 352
    //   #856	-> 353
    //   #859	-> 364
    //   #841	-> 364
    //   #863	-> 370
    // Exception table:
    //   from	to	target	type
    //   9	19	123	java/lang/ClassCastException
    //   29	37	123	java/lang/ClassCastException
    //   47	60	123	java/lang/ClassCastException
    //   60	68	123	java/lang/ClassCastException
    //   71	79	123	java/lang/ClassCastException
    //   86	100	123	java/lang/ClassCastException
    //   100	107	123	java/lang/ClassCastException
    //   107	113	123	java/lang/ClassCastException
    //   305	322	352	java/lang/reflect/InvocationTargetException
    //   305	322	337	java/lang/IllegalAccessException
    //   322	328	352	java/lang/reflect/InvocationTargetException
    //   322	328	337	java/lang/IllegalAccessException
    //   328	334	352	java/lang/reflect/InvocationTargetException
    //   328	334	337	java/lang/IllegalAccessException
  }
  
  private Object convertBack(Object paramObject) {
    TypeConverter typeConverter = this.mConverter;
    Object object = paramObject;
    if (typeConverter != null)
      if (typeConverter instanceof BidirectionalTypeConverter) {
        object = ((BidirectionalTypeConverter)typeConverter).convertBack(paramObject);
      } else {
        object = new StringBuilder();
        object.append("Converter ");
        paramObject = this.mConverter;
        object.append(paramObject.getClass().getName());
        object.append(" must be a BidirectionalTypeConverter");
        throw new IllegalArgumentException(object.toString());
      }  
    return object;
  }
  
  private void setupValue(Object paramObject, Keyframe paramKeyframe) {
    Property property = this.mProperty;
    if (property != null) {
      paramObject = convertBack(property.get(paramObject));
      paramKeyframe.setValue(paramObject);
    } else {
      try {
        if (this.mGetter == null) {
          Class<?> clazz = paramObject.getClass();
          setupGetter(clazz);
          if (this.mGetter == null)
            return; 
        } 
        paramObject = convertBack(this.mGetter.invoke(paramObject, new Object[0]));
        paramKeyframe.setValue(paramObject);
      } catch (InvocationTargetException invocationTargetException) {
        Log.e("PropertyValuesHolder", invocationTargetException.toString());
      } catch (IllegalAccessException illegalAccessException) {
        Log.e("PropertyValuesHolder", illegalAccessException.toString());
      } 
    } 
  }
  
  void setupStartValue(Object paramObject) {
    List<Keyframe> list = this.mKeyframes.getKeyframes();
    if (!list.isEmpty())
      setupValue(paramObject, list.get(0)); 
  }
  
  void setupEndValue(Object paramObject) {
    List<Keyframe> list = this.mKeyframes.getKeyframes();
    if (!list.isEmpty())
      setupValue(paramObject, list.get(list.size() - 1)); 
  }
  
  public PropertyValuesHolder clone() {
    try {
      PropertyValuesHolder propertyValuesHolder = (PropertyValuesHolder)super.clone();
      propertyValuesHolder.mPropertyName = this.mPropertyName;
      propertyValuesHolder.mProperty = this.mProperty;
      propertyValuesHolder.mKeyframes = this.mKeyframes.clone();
      propertyValuesHolder.mEvaluator = this.mEvaluator;
      return propertyValuesHolder;
    } catch (CloneNotSupportedException cloneNotSupportedException) {
      return null;
    } 
  }
  
  void setAnimatedValue(Object paramObject) {
    Property property = this.mProperty;
    if (property != null)
      property.set(paramObject, getAnimatedValue()); 
    if (this.mSetter != null)
      try {
        this.mTmpValueArray[0] = getAnimatedValue();
        this.mSetter.invoke(paramObject, this.mTmpValueArray);
      } catch (InvocationTargetException invocationTargetException) {
        Log.e("PropertyValuesHolder", invocationTargetException.toString());
      } catch (IllegalAccessException illegalAccessException) {
        Log.e("PropertyValuesHolder", illegalAccessException.toString());
      }  
  }
  
  void init() {
    if (this.mEvaluator == null) {
      TypeEvaluator typeEvaluator1;
      Class<Integer> clazz = this.mValueType;
      if (clazz == Integer.class) {
        typeEvaluator1 = sIntEvaluator;
      } else if (typeEvaluator1 == Float.class) {
        typeEvaluator1 = sFloatEvaluator;
      } else {
        typeEvaluator1 = null;
      } 
      this.mEvaluator = typeEvaluator1;
    } 
    TypeEvaluator typeEvaluator = this.mEvaluator;
    if (typeEvaluator != null)
      this.mKeyframes.setEvaluator(typeEvaluator); 
  }
  
  public void setEvaluator(TypeEvaluator paramTypeEvaluator) {
    this.mEvaluator = paramTypeEvaluator;
    this.mKeyframes.setEvaluator(paramTypeEvaluator);
  }
  
  void calculateValue(float paramFloat) {
    Object object = this.mKeyframes.getValue(paramFloat);
    TypeConverter typeConverter = this.mConverter;
    if (typeConverter != null)
      object = typeConverter.convert(object); 
    this.mAnimatedValue = object;
  }
  
  public void setPropertyName(String paramString) {
    this.mPropertyName = paramString;
  }
  
  public void setProperty(Property paramProperty) {
    this.mProperty = paramProperty;
  }
  
  public String getPropertyName() {
    return this.mPropertyName;
  }
  
  Object getAnimatedValue() {
    return this.mAnimatedValue;
  }
  
  public void getPropertyValues(PropertyValues paramPropertyValues) {
    init();
    paramPropertyValues.propertyName = this.mPropertyName;
    paramPropertyValues.type = this.mValueType;
    paramPropertyValues.startValue = this.mKeyframes.getValue(0.0F);
    if (paramPropertyValues.startValue instanceof PathParser.PathData)
      paramPropertyValues.startValue = new PathParser.PathData((PathParser.PathData)paramPropertyValues.startValue); 
    paramPropertyValues.endValue = this.mKeyframes.getValue(1.0F);
    if (paramPropertyValues.endValue instanceof PathParser.PathData)
      paramPropertyValues.endValue = new PathParser.PathData((PathParser.PathData)paramPropertyValues.endValue); 
    Keyframes keyframes = this.mKeyframes;
    if (!(keyframes instanceof PathKeyframes.FloatKeyframesBase) && !(keyframes instanceof PathKeyframes.IntKeyframesBase)) {
      if (keyframes.getKeyframes() != null && this.mKeyframes.getKeyframes().size() > 2) {
        paramPropertyValues.dataSource = (PropertyValues.DataSource)new Object(this);
        return;
      } 
      paramPropertyValues.dataSource = null;
      return;
    } 
    paramPropertyValues.dataSource = (PropertyValues.DataSource)new Object(this);
  }
  
  public Class getValueType() {
    return this.mValueType;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.mPropertyName);
    stringBuilder.append(": ");
    stringBuilder.append(this.mKeyframes.toString());
    return stringBuilder.toString();
  }
  
  static String getMethodName(String paramString1, String paramString2) {
    if (paramString2 == null || paramString2.length() == 0)
      return paramString1; 
    char c = Character.toUpperCase(paramString2.charAt(0));
    String str = paramString2.substring(1);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString1);
    stringBuilder.append(c);
    stringBuilder.append(str);
    return stringBuilder.toString();
  }
  
  private static native void nCallFloatMethod(Object paramObject, long paramLong, float paramFloat);
  
  private static native void nCallFourFloatMethod(Object paramObject, long paramLong, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4);
  
  private static native void nCallFourIntMethod(Object paramObject, long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  private static native void nCallIntMethod(Object paramObject, long paramLong, int paramInt);
  
  private static native void nCallMultipleFloatMethod(Object paramObject, long paramLong, float[] paramArrayOffloat);
  
  private static native void nCallMultipleIntMethod(Object paramObject, long paramLong, int[] paramArrayOfint);
  
  private static native void nCallTwoFloatMethod(Object paramObject, long paramLong, float paramFloat1, float paramFloat2);
  
  private static native void nCallTwoIntMethod(Object paramObject, long paramLong, int paramInt1, int paramInt2);
  
  private static native long nGetFloatMethod(Class paramClass, String paramString);
  
  private static native long nGetIntMethod(Class paramClass, String paramString);
  
  private static native long nGetMultipleFloatMethod(Class paramClass, String paramString, int paramInt);
  
  private static native long nGetMultipleIntMethod(Class paramClass, String paramString, int paramInt);
  
  class IntPropertyValuesHolder extends PropertyValuesHolder {
    private static final HashMap<Class, HashMap<String, Long>> sJNISetterPropertyMap = new HashMap<>();
    
    int mIntAnimatedValue;
    
    Keyframes.IntKeyframes mIntKeyframes;
    
    private IntProperty mIntProperty;
    
    long mJniSetter;
    
    public IntPropertyValuesHolder(PropertyValuesHolder this$0, Keyframes.IntKeyframes param1IntKeyframes) {
      super((String)this$0);
      this.mValueType = int.class;
      this.mKeyframes = param1IntKeyframes;
      this.mIntKeyframes = param1IntKeyframes;
    }
    
    public IntPropertyValuesHolder(PropertyValuesHolder this$0, Keyframes.IntKeyframes param1IntKeyframes) {
      super((Property)this$0);
      this.mValueType = int.class;
      this.mKeyframes = param1IntKeyframes;
      this.mIntKeyframes = param1IntKeyframes;
      if (this$0 instanceof IntProperty)
        this.mIntProperty = (IntProperty)this.mProperty; 
    }
    
    public IntPropertyValuesHolder(PropertyValuesHolder this$0, int... param1VarArgs) {
      super((String)this$0);
      setIntValues(param1VarArgs);
    }
    
    public IntPropertyValuesHolder(PropertyValuesHolder this$0, int... param1VarArgs) {
      super((Property)this$0);
      setIntValues(param1VarArgs);
      if (this$0 instanceof IntProperty)
        this.mIntProperty = (IntProperty)this.mProperty; 
    }
    
    public void setProperty(Property param1Property) {
      if (param1Property instanceof IntProperty) {
        this.mIntProperty = (IntProperty)param1Property;
      } else {
        super.setProperty(param1Property);
      } 
    }
    
    public void setIntValues(int... param1VarArgs) {
      super.setIntValues(param1VarArgs);
      this.mIntKeyframes = (Keyframes.IntKeyframes)this.mKeyframes;
    }
    
    void calculateValue(float param1Float) {
      this.mIntAnimatedValue = this.mIntKeyframes.getIntValue(param1Float);
    }
    
    Object getAnimatedValue() {
      return Integer.valueOf(this.mIntAnimatedValue);
    }
    
    public IntPropertyValuesHolder clone() {
      IntPropertyValuesHolder intPropertyValuesHolder = (IntPropertyValuesHolder)super.clone();
      intPropertyValuesHolder.mIntKeyframes = (Keyframes.IntKeyframes)intPropertyValuesHolder.mKeyframes;
      return intPropertyValuesHolder;
    }
    
    void setAnimatedValue(Object param1Object) {
      IntProperty intProperty = this.mIntProperty;
      if (intProperty != null) {
        intProperty.setValue(param1Object, this.mIntAnimatedValue);
        return;
      } 
      if (this.mProperty != null) {
        this.mProperty.set(param1Object, Integer.valueOf(this.mIntAnimatedValue));
        return;
      } 
      long l = this.mJniSetter;
      if (l != 0L) {
        PropertyValuesHolder.nCallIntMethod(param1Object, l, this.mIntAnimatedValue);
        return;
      } 
      if (this.mSetter != null)
        try {
          this.mTmpValueArray[0] = Integer.valueOf(this.mIntAnimatedValue);
          this.mSetter.invoke(param1Object, this.mTmpValueArray);
        } catch (InvocationTargetException invocationTargetException) {
          Log.e("PropertyValuesHolder", invocationTargetException.toString());
        } catch (IllegalAccessException illegalAccessException) {
          Log.e("PropertyValuesHolder", illegalAccessException.toString());
        }  
    }
    
    void setupSetter(Class param1Class) {
      if (this.mProperty != null)
        return; 
      synchronized (sJNISetterPropertyMap) {
        HashMap<Object, Object> hashMap = (HashMap)sJNISetterPropertyMap.get(param1Class);
        boolean bool = false;
        if (hashMap != null) {
          boolean bool1 = hashMap.containsKey(this.mPropertyName);
          bool = bool1;
          if (bool1) {
            Long long_ = (Long)hashMap.get(this.mPropertyName);
            bool = bool1;
            if (long_ != null) {
              this.mJniSetter = long_.longValue();
              bool = bool1;
            } 
          } 
        } 
        if (!bool) {
          String str = getMethodName("set", this.mPropertyName);
          try {
            this.mJniSetter = PropertyValuesHolder.nGetIntMethod(param1Class, str);
          } catch (NoSuchMethodError noSuchMethodError) {}
          HashMap<Object, Object> hashMap1 = hashMap;
          if (hashMap == null) {
            hashMap1 = new HashMap<>();
            this();
            sJNISetterPropertyMap.put(param1Class, hashMap1);
          } 
          hashMap1.put(this.mPropertyName, Long.valueOf(this.mJniSetter));
        } 
        if (this.mJniSetter == 0L)
          super.setupSetter(param1Class); 
        return;
      } 
    }
  }
  
  class FloatPropertyValuesHolder extends PropertyValuesHolder {
    private static final HashMap<Class, HashMap<String, Long>> sJNISetterPropertyMap = new HashMap<>();
    
    float mFloatAnimatedValue;
    
    Keyframes.FloatKeyframes mFloatKeyframes;
    
    private FloatProperty mFloatProperty;
    
    long mJniSetter;
    
    public FloatPropertyValuesHolder(PropertyValuesHolder this$0, Keyframes.FloatKeyframes param1FloatKeyframes) {
      super((String)this$0);
      this.mValueType = float.class;
      this.mKeyframes = param1FloatKeyframes;
      this.mFloatKeyframes = param1FloatKeyframes;
    }
    
    public FloatPropertyValuesHolder(PropertyValuesHolder this$0, Keyframes.FloatKeyframes param1FloatKeyframes) {
      super((Property)this$0);
      this.mValueType = float.class;
      this.mKeyframes = param1FloatKeyframes;
      this.mFloatKeyframes = param1FloatKeyframes;
      if (this$0 instanceof FloatProperty)
        this.mFloatProperty = (FloatProperty)this.mProperty; 
    }
    
    public FloatPropertyValuesHolder(PropertyValuesHolder this$0, float... param1VarArgs) {
      super((String)this$0);
      setFloatValues(param1VarArgs);
    }
    
    public FloatPropertyValuesHolder(PropertyValuesHolder this$0, float... param1VarArgs) {
      super((Property)this$0);
      setFloatValues(param1VarArgs);
      if (this$0 instanceof FloatProperty)
        this.mFloatProperty = (FloatProperty)this.mProperty; 
    }
    
    public void setProperty(Property param1Property) {
      if (param1Property instanceof FloatProperty) {
        this.mFloatProperty = (FloatProperty)param1Property;
      } else {
        super.setProperty(param1Property);
      } 
    }
    
    public void setFloatValues(float... param1VarArgs) {
      super.setFloatValues(param1VarArgs);
      this.mFloatKeyframes = (Keyframes.FloatKeyframes)this.mKeyframes;
    }
    
    void calculateValue(float param1Float) {
      this.mFloatAnimatedValue = this.mFloatKeyframes.getFloatValue(param1Float);
    }
    
    Object getAnimatedValue() {
      return Float.valueOf(this.mFloatAnimatedValue);
    }
    
    public FloatPropertyValuesHolder clone() {
      FloatPropertyValuesHolder floatPropertyValuesHolder = (FloatPropertyValuesHolder)super.clone();
      floatPropertyValuesHolder.mFloatKeyframes = (Keyframes.FloatKeyframes)floatPropertyValuesHolder.mKeyframes;
      return floatPropertyValuesHolder;
    }
    
    void setAnimatedValue(Object param1Object) {
      FloatProperty floatProperty = this.mFloatProperty;
      if (floatProperty != null) {
        floatProperty.setValue(param1Object, this.mFloatAnimatedValue);
        return;
      } 
      if (this.mProperty != null) {
        this.mProperty.set(param1Object, Float.valueOf(this.mFloatAnimatedValue));
        return;
      } 
      long l = this.mJniSetter;
      if (l != 0L) {
        PropertyValuesHolder.nCallFloatMethod(param1Object, l, this.mFloatAnimatedValue);
        return;
      } 
      if (this.mSetter != null)
        try {
          this.mTmpValueArray[0] = Float.valueOf(this.mFloatAnimatedValue);
          this.mSetter.invoke(param1Object, this.mTmpValueArray);
        } catch (InvocationTargetException invocationTargetException) {
          Log.e("PropertyValuesHolder", invocationTargetException.toString());
        } catch (IllegalAccessException illegalAccessException) {
          Log.e("PropertyValuesHolder", illegalAccessException.toString());
        }  
    }
    
    void setupSetter(Class param1Class) {
      if (this.mProperty != null)
        return; 
      synchronized (sJNISetterPropertyMap) {
        HashMap<Object, Object> hashMap = (HashMap)sJNISetterPropertyMap.get(param1Class);
        boolean bool = false;
        if (hashMap != null) {
          boolean bool1 = hashMap.containsKey(this.mPropertyName);
          bool = bool1;
          if (bool1) {
            Long long_ = (Long)hashMap.get(this.mPropertyName);
            bool = bool1;
            if (long_ != null) {
              this.mJniSetter = long_.longValue();
              bool = bool1;
            } 
          } 
        } 
        if (!bool) {
          String str = getMethodName("set", this.mPropertyName);
          try {
            this.mJniSetter = PropertyValuesHolder.nGetFloatMethod(param1Class, str);
          } catch (NoSuchMethodError noSuchMethodError) {}
          HashMap<Object, Object> hashMap1 = hashMap;
          if (hashMap == null) {
            hashMap1 = new HashMap<>();
            this();
            sJNISetterPropertyMap.put(param1Class, hashMap1);
          } 
          hashMap1.put(this.mPropertyName, Long.valueOf(this.mJniSetter));
        } 
        if (this.mJniSetter == 0L)
          super.setupSetter(param1Class); 
        return;
      } 
    }
  }
  
  class MultiFloatValuesHolder extends PropertyValuesHolder {
    private static final HashMap<Class, HashMap<String, Long>> sJNISetterPropertyMap = new HashMap<>();
    
    private long mJniSetter;
    
    public MultiFloatValuesHolder(PropertyValuesHolder this$0, TypeConverter param1TypeConverter, TypeEvaluator param1TypeEvaluator, Object... param1VarArgs) {
      super((String)this$0);
      setConverter(param1TypeConverter);
      setObjectValues(param1VarArgs);
      setEvaluator(param1TypeEvaluator);
    }
    
    public MultiFloatValuesHolder(PropertyValuesHolder this$0, TypeConverter param1TypeConverter, TypeEvaluator param1TypeEvaluator, Keyframes param1Keyframes) {
      super((String)this$0);
      setConverter(param1TypeConverter);
      this.mKeyframes = param1Keyframes;
      setEvaluator(param1TypeEvaluator);
    }
    
    void setAnimatedValue(Object param1Object) {
      float[] arrayOfFloat = (float[])getAnimatedValue();
      int i = arrayOfFloat.length;
      long l = this.mJniSetter;
      if (l != 0L)
        if (i != 1) {
          if (i != 2) {
            if (i != 4) {
              PropertyValuesHolder.nCallMultipleFloatMethod(param1Object, l, arrayOfFloat);
            } else {
              PropertyValuesHolder.nCallFourFloatMethod(param1Object, l, arrayOfFloat[0], arrayOfFloat[1], arrayOfFloat[2], arrayOfFloat[3]);
            } 
          } else {
            PropertyValuesHolder.nCallTwoFloatMethod(param1Object, l, arrayOfFloat[0], arrayOfFloat[1]);
          } 
        } else {
          PropertyValuesHolder.nCallFloatMethod(param1Object, l, arrayOfFloat[0]);
        }  
    }
    
    void setupSetterAndGetter(Object param1Object) {
      setupSetter(param1Object.getClass());
    }
    
    void setupSetter(Class param1Class) {
      if (this.mJniSetter != 0L)
        return; 
      synchronized (sJNISetterPropertyMap) {
        HashMap<Object, Object> hashMap = (HashMap)sJNISetterPropertyMap.get(param1Class);
        boolean bool = false;
        if (hashMap != null) {
          boolean bool1 = hashMap.containsKey(this.mPropertyName);
          bool = bool1;
          if (bool1) {
            Long long_ = (Long)hashMap.get(this.mPropertyName);
            bool = bool1;
            if (long_ != null) {
              this.mJniSetter = long_.longValue();
              bool = bool1;
            } 
          } 
        } 
        if (!bool) {
          String str = getMethodName("set", this.mPropertyName);
          calculateValue(0.0F);
          float[] arrayOfFloat = (float[])getAnimatedValue();
          int i = arrayOfFloat.length;
          try {
            this.mJniSetter = PropertyValuesHolder.nGetMultipleFloatMethod(param1Class, str, i);
          } catch (NoSuchMethodError noSuchMethodError) {
            try {
              this.mJniSetter = PropertyValuesHolder.nGetMultipleFloatMethod(param1Class, this.mPropertyName, i);
            } catch (NoSuchMethodError noSuchMethodError1) {}
          } 
          HashMap<Object, Object> hashMap1 = hashMap;
          if (hashMap == null) {
            hashMap1 = new HashMap<>();
            this();
            sJNISetterPropertyMap.put(param1Class, hashMap1);
          } 
          hashMap1.put(this.mPropertyName, Long.valueOf(this.mJniSetter));
        } 
        return;
      } 
    }
  }
  
  class MultiIntValuesHolder extends PropertyValuesHolder {
    private static final HashMap<Class, HashMap<String, Long>> sJNISetterPropertyMap = new HashMap<>();
    
    private long mJniSetter;
    
    public MultiIntValuesHolder(PropertyValuesHolder this$0, TypeConverter param1TypeConverter, TypeEvaluator param1TypeEvaluator, Object... param1VarArgs) {
      super((String)this$0);
      setConverter(param1TypeConverter);
      setObjectValues(param1VarArgs);
      setEvaluator(param1TypeEvaluator);
    }
    
    public MultiIntValuesHolder(PropertyValuesHolder this$0, TypeConverter param1TypeConverter, TypeEvaluator param1TypeEvaluator, Keyframes param1Keyframes) {
      super((String)this$0);
      setConverter(param1TypeConverter);
      this.mKeyframes = param1Keyframes;
      setEvaluator(param1TypeEvaluator);
    }
    
    void setAnimatedValue(Object param1Object) {
      int[] arrayOfInt = (int[])getAnimatedValue();
      int i = arrayOfInt.length;
      long l = this.mJniSetter;
      if (l != 0L)
        if (i != 1) {
          if (i != 2) {
            if (i != 4) {
              PropertyValuesHolder.nCallMultipleIntMethod(param1Object, l, arrayOfInt);
            } else {
              PropertyValuesHolder.nCallFourIntMethod(param1Object, l, arrayOfInt[0], arrayOfInt[1], arrayOfInt[2], arrayOfInt[3]);
            } 
          } else {
            PropertyValuesHolder.nCallTwoIntMethod(param1Object, l, arrayOfInt[0], arrayOfInt[1]);
          } 
        } else {
          PropertyValuesHolder.nCallIntMethod(param1Object, l, arrayOfInt[0]);
        }  
    }
    
    void setupSetterAndGetter(Object param1Object) {
      setupSetter(param1Object.getClass());
    }
    
    void setupSetter(Class param1Class) {
      if (this.mJniSetter != 0L)
        return; 
      synchronized (sJNISetterPropertyMap) {
        HashMap<Object, Object> hashMap = (HashMap)sJNISetterPropertyMap.get(param1Class);
        boolean bool = false;
        if (hashMap != null) {
          boolean bool1 = hashMap.containsKey(this.mPropertyName);
          bool = bool1;
          if (bool1) {
            Long long_ = (Long)hashMap.get(this.mPropertyName);
            bool = bool1;
            if (long_ != null) {
              this.mJniSetter = long_.longValue();
              bool = bool1;
            } 
          } 
        } 
        if (!bool) {
          String str = getMethodName("set", this.mPropertyName);
          calculateValue(0.0F);
          int[] arrayOfInt = (int[])getAnimatedValue();
          int i = arrayOfInt.length;
          try {
            this.mJniSetter = PropertyValuesHolder.nGetMultipleIntMethod(param1Class, str, i);
          } catch (NoSuchMethodError noSuchMethodError) {
            try {
              this.mJniSetter = PropertyValuesHolder.nGetMultipleIntMethod(param1Class, this.mPropertyName, i);
            } catch (NoSuchMethodError noSuchMethodError1) {}
          } 
          HashMap<Object, Object> hashMap1 = hashMap;
          if (hashMap == null) {
            hashMap1 = new HashMap<>();
            this();
            sJNISetterPropertyMap.put(param1Class, hashMap1);
          } 
          hashMap1.put(this.mPropertyName, Long.valueOf(this.mJniSetter));
        } 
        return;
      } 
    }
  }
  
  class PointFToFloatArray extends TypeConverter<PointF, float[]> {
    private float[] mCoordinates = new float[2];
    
    public PointFToFloatArray() {
      super(PointF.class, (Class)float[].class);
    }
    
    public float[] convert(PointF param1PointF) {
      this.mCoordinates[0] = param1PointF.x;
      this.mCoordinates[1] = param1PointF.y;
      return this.mCoordinates;
    }
  }
  
  class PointFToIntArray extends TypeConverter<PointF, int[]> {
    private int[] mCoordinates = new int[2];
    
    public PointFToIntArray() {
      super(PointF.class, (Class)int[].class);
    }
    
    public int[] convert(PointF param1PointF) {
      this.mCoordinates[0] = Math.round(param1PointF.x);
      this.mCoordinates[1] = Math.round(param1PointF.y);
      return this.mCoordinates;
    }
  }
  
  public static class PropertyValues {
    public DataSource dataSource = null;
    
    public Object endValue;
    
    public String propertyName;
    
    public Object startValue;
    
    public Class type;
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("property name: ");
      stringBuilder.append(this.propertyName);
      stringBuilder.append(", type: ");
      stringBuilder.append(this.type);
      stringBuilder.append(", startValue: ");
      Object object = this.startValue;
      stringBuilder.append(object.toString());
      stringBuilder.append(", endValue: ");
      stringBuilder.append(this.endValue.toString());
      return stringBuilder.toString();
    }
    
    public static interface DataSource {
      Object getValueAtFraction(float param2Float);
    }
  }
  
  public static interface DataSource {
    Object getValueAtFraction(float param1Float);
  }
}
