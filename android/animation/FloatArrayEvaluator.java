package android.animation;

public class FloatArrayEvaluator implements TypeEvaluator<float[]> {
  private float[] mArray;
  
  public FloatArrayEvaluator() {}
  
  public FloatArrayEvaluator(float[] paramArrayOffloat) {
    this.mArray = paramArrayOffloat;
  }
  
  public float[] evaluate(float paramFloat, float[] paramArrayOffloat1, float[] paramArrayOffloat2) {
    float[] arrayOfFloat1 = this.mArray;
    float[] arrayOfFloat2 = arrayOfFloat1;
    if (arrayOfFloat1 == null)
      arrayOfFloat2 = new float[paramArrayOffloat1.length]; 
    for (byte b = 0; b < arrayOfFloat2.length; b++) {
      float f1 = paramArrayOffloat1[b];
      float f2 = paramArrayOffloat2[b];
      arrayOfFloat2[b] = (f2 - f1) * paramFloat + f1;
    } 
    return arrayOfFloat2;
  }
}
