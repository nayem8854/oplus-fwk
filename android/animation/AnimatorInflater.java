package android.animation;

import android.content.Context;
import android.content.res.ConfigurationBoundResourceCache;
import android.content.res.ConstantState;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.util.AttributeSet;
import android.util.Log;
import android.util.PathParser;
import android.util.StateSet;
import android.util.TypedValue;
import android.util.Xml;
import android.view.InflateException;
import android.view.animation.AnimationUtils;
import android.view.animation.BaseInterpolator;
import android.view.animation.Interpolator;
import com.android.internal.R;
import java.io.IOException;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class AnimatorInflater {
  private static final boolean DBG_ANIMATOR_INFLATER = false;
  
  private static final int SEQUENTIALLY = 1;
  
  private static final String TAG = "AnimatorInflater";
  
  private static final int TOGETHER = 0;
  
  private static final int VALUE_TYPE_COLOR = 3;
  
  private static final int VALUE_TYPE_FLOAT = 0;
  
  private static final int VALUE_TYPE_INT = 1;
  
  private static final int VALUE_TYPE_PATH = 2;
  
  private static final int VALUE_TYPE_UNDEFINED = 4;
  
  private static final TypedValue sTmpTypedValue = new TypedValue();
  
  public static Animator loadAnimator(Context paramContext, int paramInt) throws Resources.NotFoundException {
    return loadAnimator(paramContext.getResources(), paramContext.getTheme(), paramInt);
  }
  
  public static Animator loadAnimator(Resources paramResources, Resources.Theme paramTheme, int paramInt) throws Resources.NotFoundException {
    return loadAnimator(paramResources, paramTheme, paramInt, 1.0F);
  }
  
  public static Animator loadAnimator(Resources paramResources, Resources.Theme paramTheme, int paramInt, float paramFloat) throws Resources.NotFoundException {
    Resources.NotFoundException notFoundException1, notFoundException2;
    ConfigurationBoundResourceCache<Animator> configurationBoundResourceCache = paramResources.getAnimatorCache();
    Animator animator = configurationBoundResourceCache.getInstance(paramInt, paramResources, paramTheme);
    if (animator != null)
      return animator; 
    XmlResourceParser xmlResourceParser1 = null, xmlResourceParser2 = null;
    animator = null;
    try {
      XmlResourceParser xmlResourceParser4 = paramResources.getAnimation(paramInt);
      XmlResourceParser xmlResourceParser3 = xmlResourceParser4;
      xmlResourceParser1 = xmlResourceParser4;
      xmlResourceParser2 = xmlResourceParser4;
      Animator animator2 = createAnimatorFromXml(paramResources, paramTheme, xmlResourceParser4, paramFloat);
      Animator animator1 = animator2;
      if (animator2 != null) {
        XmlResourceParser xmlResourceParser = xmlResourceParser4;
        xmlResourceParser1 = xmlResourceParser4;
        xmlResourceParser2 = xmlResourceParser4;
        animator2.appendChangingConfigurations(getChangingConfigs(paramResources, paramInt));
        xmlResourceParser = xmlResourceParser4;
        xmlResourceParser1 = xmlResourceParser4;
        xmlResourceParser2 = xmlResourceParser4;
        ConstantState<Animator> constantState = animator2.createConstantState();
        Animator animator3 = animator2;
        if (constantState != null) {
          XmlResourceParser xmlResourceParser5 = xmlResourceParser4;
          xmlResourceParser1 = xmlResourceParser4;
          xmlResourceParser2 = xmlResourceParser4;
          configurationBoundResourceCache.put(paramInt, paramTheme, constantState);
          xmlResourceParser5 = xmlResourceParser4;
          xmlResourceParser1 = xmlResourceParser4;
          xmlResourceParser2 = xmlResourceParser4;
          Animator animator4 = constantState.newInstance(paramResources, paramTheme);
        } 
      } 
      if (xmlResourceParser4 != null)
        xmlResourceParser4.close(); 
      return animator1;
    } catch (XmlPullParserException xmlPullParserException) {
      XmlResourceParser xmlResourceParser = xmlResourceParser2;
      notFoundException2 = new Resources.NotFoundException();
      xmlResourceParser = xmlResourceParser2;
      StringBuilder stringBuilder = new StringBuilder();
      xmlResourceParser = xmlResourceParser2;
      this();
      xmlResourceParser = xmlResourceParser2;
      stringBuilder.append("Can't load animation resource ID #0x");
      xmlResourceParser = xmlResourceParser2;
      stringBuilder.append(Integer.toHexString(paramInt));
      xmlResourceParser = xmlResourceParser2;
      this(stringBuilder.toString());
      xmlResourceParser = xmlResourceParser2;
      notFoundException2.initCause((Throwable)xmlPullParserException);
      xmlResourceParser = xmlResourceParser2;
      throw notFoundException2;
    } catch (IOException iOException) {
      notFoundException1 = notFoundException2;
      Resources.NotFoundException notFoundException = new Resources.NotFoundException();
      notFoundException1 = notFoundException2;
      StringBuilder stringBuilder = new StringBuilder();
      notFoundException1 = notFoundException2;
      this();
      notFoundException1 = notFoundException2;
      stringBuilder.append("Can't load animation resource ID #0x");
      notFoundException1 = notFoundException2;
      stringBuilder.append(Integer.toHexString(paramInt));
      notFoundException1 = notFoundException2;
      this(stringBuilder.toString());
      notFoundException1 = notFoundException2;
      notFoundException.initCause(iOException);
      notFoundException1 = notFoundException2;
      throw notFoundException;
    } finally {}
    if (notFoundException1 != null)
      notFoundException1.close(); 
    throw paramResources;
  }
  
  public static StateListAnimator loadStateListAnimator(Context paramContext, int paramInt) throws Resources.NotFoundException {
    StringBuilder stringBuilder1, stringBuilder2;
    Resources resources = paramContext.getResources();
    ConfigurationBoundResourceCache<StateListAnimator> configurationBoundResourceCache = resources.getStateListAnimatorCache();
    Resources.Theme theme = paramContext.getTheme();
    StateListAnimator stateListAnimator = configurationBoundResourceCache.getInstance(paramInt, resources, theme);
    if (stateListAnimator != null)
      return stateListAnimator; 
    XmlResourceParser xmlResourceParser1 = null, xmlResourceParser2 = null;
    stateListAnimator = null;
    try {
      XmlResourceParser xmlResourceParser4 = resources.getAnimation(paramInt);
      XmlResourceParser xmlResourceParser3 = xmlResourceParser4;
      xmlResourceParser1 = xmlResourceParser4;
      xmlResourceParser2 = xmlResourceParser4;
      StateListAnimator stateListAnimator2 = createStateListAnimatorFromXml(paramContext, xmlResourceParser4, Xml.asAttributeSet(xmlResourceParser4));
      StateListAnimator stateListAnimator1 = stateListAnimator2;
      if (stateListAnimator2 != null) {
        xmlResourceParser3 = xmlResourceParser4;
        xmlResourceParser1 = xmlResourceParser4;
        xmlResourceParser2 = xmlResourceParser4;
        stateListAnimator2.appendChangingConfigurations(getChangingConfigs(resources, paramInt));
        xmlResourceParser3 = xmlResourceParser4;
        xmlResourceParser1 = xmlResourceParser4;
        xmlResourceParser2 = xmlResourceParser4;
        ConstantState<StateListAnimator> constantState = stateListAnimator2.createConstantState();
        stateListAnimator1 = stateListAnimator2;
        if (constantState != null) {
          xmlResourceParser3 = xmlResourceParser4;
          xmlResourceParser1 = xmlResourceParser4;
          xmlResourceParser2 = xmlResourceParser4;
          configurationBoundResourceCache.put(paramInt, theme, constantState);
          xmlResourceParser3 = xmlResourceParser4;
          xmlResourceParser1 = xmlResourceParser4;
          xmlResourceParser2 = xmlResourceParser4;
          stateListAnimator1 = constantState.newInstance(resources, theme);
        } 
      } 
      if (xmlResourceParser4 != null)
        xmlResourceParser4.close(); 
      return stateListAnimator1;
    } catch (XmlPullParserException xmlPullParserException) {
      XmlResourceParser xmlResourceParser = xmlResourceParser2;
      Resources.NotFoundException notFoundException = new Resources.NotFoundException();
      xmlResourceParser = xmlResourceParser2;
      stringBuilder2 = new StringBuilder();
      xmlResourceParser = xmlResourceParser2;
      this();
      xmlResourceParser = xmlResourceParser2;
      stringBuilder2.append("Can't load state list animator resource ID #0x");
      xmlResourceParser = xmlResourceParser2;
      stringBuilder2.append(Integer.toHexString(paramInt));
      xmlResourceParser = xmlResourceParser2;
      this(stringBuilder2.toString());
      xmlResourceParser = xmlResourceParser2;
      notFoundException.initCause((Throwable)xmlPullParserException);
      xmlResourceParser = xmlResourceParser2;
      throw notFoundException;
    } catch (IOException iOException) {
      stringBuilder1 = stringBuilder2;
      Resources.NotFoundException notFoundException = new Resources.NotFoundException();
      stringBuilder1 = stringBuilder2;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder1 = stringBuilder2;
      this();
      stringBuilder1 = stringBuilder2;
      stringBuilder.append("Can't load state list animator resource ID #0x");
      stringBuilder1 = stringBuilder2;
      stringBuilder.append(Integer.toHexString(paramInt));
      stringBuilder1 = stringBuilder2;
      this(stringBuilder.toString());
      stringBuilder1 = stringBuilder2;
      notFoundException.initCause(iOException);
      stringBuilder1 = stringBuilder2;
      throw notFoundException;
    } finally {}
    if (stringBuilder1 != null)
      stringBuilder1.close(); 
    throw paramContext;
  }
  
  private static StateListAnimator createStateListAnimatorFromXml(Context paramContext, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet) throws IOException, XmlPullParserException {
    StateListAnimator stateListAnimator = new StateListAnimator();
    while (true) {
      int i = paramXmlPullParser.next();
      if (i != 1) {
        if (i != 2) {
          if (i != 3)
            continue; 
          break;
        } 
        Animator animator = null;
        if ("item".equals(paramXmlPullParser.getName())) {
          int j = paramXmlPullParser.getAttributeCount();
          int[] arrayOfInt = new int[j];
          byte b = 0;
          for (i = 0; i < j; i++) {
            int k = paramAttributeSet.getAttributeNameResource(i);
            if (k == 16843213) {
              k = paramAttributeSet.getAttributeResourceValue(i, 0);
              animator = loadAnimator(paramContext, k);
            } else {
              if (!paramAttributeSet.getAttributeBooleanValue(i, false))
                k = -k; 
              arrayOfInt[b] = k;
              b++;
            } 
          } 
          Animator animator1 = animator;
          if (animator == null) {
            Resources resources = paramContext.getResources();
            Resources.Theme theme = paramContext.getTheme();
            animator1 = createAnimatorFromXml(resources, theme, paramXmlPullParser, 1.0F);
          } 
          if (animator1 != null) {
            stateListAnimator.addState(StateSet.trimStateSet(arrayOfInt, b), animator1);
            continue;
          } 
          throw new Resources.NotFoundException("animation state item must have a valid animation");
        } 
        continue;
      } 
      break;
    } 
    return stateListAnimator;
  }
  
  class PathDataEvaluator implements TypeEvaluator<PathParser.PathData> {
    private final PathParser.PathData mPathData = new PathParser.PathData();
    
    public PathParser.PathData evaluate(float param1Float, PathParser.PathData param1PathData1, PathParser.PathData param1PathData2) {
      if (PathParser.interpolatePathData(this.mPathData, param1PathData1, param1PathData2, param1Float))
        return this.mPathData; 
      throw new IllegalArgumentException("Can't interpolate between two incompatible pathData");
    }
    
    private PathDataEvaluator() {}
  }
  
  private static PropertyValuesHolder getPVH(TypedArray paramTypedArray, int paramInt1, int paramInt2, int paramInt3, String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: iload_2
    //   2: invokevirtual peekValue : (I)Landroid/util/TypedValue;
    //   5: astore #5
    //   7: aload #5
    //   9: ifnull -> 18
    //   12: iconst_1
    //   13: istore #6
    //   15: goto -> 21
    //   18: iconst_0
    //   19: istore #6
    //   21: iload #6
    //   23: ifeq -> 36
    //   26: aload #5
    //   28: getfield type : I
    //   31: istore #7
    //   33: goto -> 39
    //   36: iconst_0
    //   37: istore #7
    //   39: aload_0
    //   40: iload_3
    //   41: invokevirtual peekValue : (I)Landroid/util/TypedValue;
    //   44: astore #5
    //   46: aload #5
    //   48: ifnull -> 57
    //   51: iconst_1
    //   52: istore #8
    //   54: goto -> 60
    //   57: iconst_0
    //   58: istore #8
    //   60: iload #8
    //   62: ifeq -> 75
    //   65: aload #5
    //   67: getfield type : I
    //   70: istore #9
    //   72: goto -> 78
    //   75: iconst_0
    //   76: istore #9
    //   78: iload_1
    //   79: iconst_4
    //   80: if_icmpne -> 119
    //   83: iload #6
    //   85: ifeq -> 96
    //   88: iload #7
    //   90: invokestatic isColorType : (I)Z
    //   93: ifne -> 109
    //   96: iload #8
    //   98: ifeq -> 114
    //   101: iload #9
    //   103: invokestatic isColorType : (I)Z
    //   106: ifeq -> 114
    //   109: iconst_3
    //   110: istore_1
    //   111: goto -> 119
    //   114: iconst_0
    //   115: istore_1
    //   116: goto -> 119
    //   119: iload_1
    //   120: ifne -> 129
    //   123: iconst_1
    //   124: istore #10
    //   126: goto -> 132
    //   129: iconst_0
    //   130: istore #10
    //   132: iload_1
    //   133: iconst_2
    //   134: if_icmpne -> 370
    //   137: aload_0
    //   138: iload_2
    //   139: invokevirtual getString : (I)Ljava/lang/String;
    //   142: astore #11
    //   144: aload_0
    //   145: iload_3
    //   146: invokevirtual getString : (I)Ljava/lang/String;
    //   149: astore #12
    //   151: aload #11
    //   153: ifnonnull -> 161
    //   156: aconst_null
    //   157: astore_0
    //   158: goto -> 171
    //   161: new android/util/PathParser$PathData
    //   164: dup
    //   165: aload #11
    //   167: invokespecial <init> : (Ljava/lang/String;)V
    //   170: astore_0
    //   171: aload #12
    //   173: ifnonnull -> 182
    //   176: aconst_null
    //   177: astore #5
    //   179: goto -> 193
    //   182: new android/util/PathParser$PathData
    //   185: dup
    //   186: aload #12
    //   188: invokespecial <init> : (Ljava/lang/String;)V
    //   191: astore #5
    //   193: aload_0
    //   194: ifnonnull -> 208
    //   197: aload #5
    //   199: ifnull -> 205
    //   202: goto -> 208
    //   205: goto -> 362
    //   208: aload_0
    //   209: ifnull -> 329
    //   212: new android/animation/AnimatorInflater$PathDataEvaluator
    //   215: dup
    //   216: aconst_null
    //   217: invokespecial <init> : (Landroid/animation/AnimatorInflater$1;)V
    //   220: astore #13
    //   222: aload #5
    //   224: ifnull -> 310
    //   227: aload_0
    //   228: aload #5
    //   230: invokestatic canMorph : (Landroid/util/PathParser$PathData;Landroid/util/PathParser$PathData;)Z
    //   233: ifeq -> 260
    //   236: aload #4
    //   238: aload #13
    //   240: iconst_2
    //   241: anewarray java/lang/Object
    //   244: dup
    //   245: iconst_0
    //   246: aload_0
    //   247: aastore
    //   248: dup
    //   249: iconst_1
    //   250: aload #5
    //   252: aastore
    //   253: invokestatic ofObject : (Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/PropertyValuesHolder;
    //   256: astore_0
    //   257: goto -> 326
    //   260: new java/lang/StringBuilder
    //   263: dup
    //   264: invokespecial <init> : ()V
    //   267: astore_0
    //   268: aload_0
    //   269: ldc_w ' Can't morph from '
    //   272: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   275: pop
    //   276: aload_0
    //   277: aload #11
    //   279: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   282: pop
    //   283: aload_0
    //   284: ldc_w ' to '
    //   287: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   290: pop
    //   291: aload_0
    //   292: aload #12
    //   294: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   297: pop
    //   298: new android/view/InflateException
    //   301: dup
    //   302: aload_0
    //   303: invokevirtual toString : ()Ljava/lang/String;
    //   306: invokespecial <init> : (Ljava/lang/String;)V
    //   309: athrow
    //   310: aload #4
    //   312: aload #13
    //   314: iconst_1
    //   315: anewarray java/lang/Object
    //   318: dup
    //   319: iconst_0
    //   320: aload_0
    //   321: aastore
    //   322: invokestatic ofObject : (Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/PropertyValuesHolder;
    //   325: astore_0
    //   326: goto -> 364
    //   329: aload #5
    //   331: ifnull -> 362
    //   334: new android/animation/AnimatorInflater$PathDataEvaluator
    //   337: dup
    //   338: aconst_null
    //   339: invokespecial <init> : (Landroid/animation/AnimatorInflater$1;)V
    //   342: astore_0
    //   343: aload #4
    //   345: aload_0
    //   346: iconst_1
    //   347: anewarray java/lang/Object
    //   350: dup
    //   351: iconst_0
    //   352: aload #5
    //   354: aastore
    //   355: invokestatic ofObject : (Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/PropertyValuesHolder;
    //   358: astore_0
    //   359: goto -> 364
    //   362: aconst_null
    //   363: astore_0
    //   364: aload_0
    //   365: astore #4
    //   367: goto -> 748
    //   370: aconst_null
    //   371: astore #5
    //   373: iload_1
    //   374: iconst_3
    //   375: if_icmpne -> 383
    //   378: invokestatic getInstance : ()Landroid/animation/ArgbEvaluator;
    //   381: astore #5
    //   383: iload #10
    //   385: ifeq -> 529
    //   388: iload #6
    //   390: ifeq -> 487
    //   393: iload #7
    //   395: iconst_5
    //   396: if_icmpne -> 410
    //   399: aload_0
    //   400: iload_2
    //   401: fconst_0
    //   402: invokevirtual getDimension : (IF)F
    //   405: fstore #14
    //   407: goto -> 418
    //   410: aload_0
    //   411: iload_2
    //   412: fconst_0
    //   413: invokevirtual getFloat : (IF)F
    //   416: fstore #14
    //   418: iload #8
    //   420: ifeq -> 470
    //   423: iload #9
    //   425: iconst_5
    //   426: if_icmpne -> 440
    //   429: aload_0
    //   430: iload_3
    //   431: fconst_0
    //   432: invokevirtual getDimension : (IF)F
    //   435: fstore #15
    //   437: goto -> 448
    //   440: aload_0
    //   441: iload_3
    //   442: fconst_0
    //   443: invokevirtual getFloat : (IF)F
    //   446: fstore #15
    //   448: aload #4
    //   450: iconst_2
    //   451: newarray float
    //   453: dup
    //   454: iconst_0
    //   455: fload #14
    //   457: fastore
    //   458: dup
    //   459: iconst_1
    //   460: fload #15
    //   462: fastore
    //   463: invokestatic ofFloat : (Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;
    //   466: astore_0
    //   467: goto -> 526
    //   470: aload #4
    //   472: iconst_1
    //   473: newarray float
    //   475: dup
    //   476: iconst_0
    //   477: fload #14
    //   479: fastore
    //   480: invokestatic ofFloat : (Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;
    //   483: astore_0
    //   484: goto -> 526
    //   487: iload #9
    //   489: iconst_5
    //   490: if_icmpne -> 504
    //   493: aload_0
    //   494: iload_3
    //   495: fconst_0
    //   496: invokevirtual getDimension : (IF)F
    //   499: fstore #14
    //   501: goto -> 512
    //   504: aload_0
    //   505: iload_3
    //   506: fconst_0
    //   507: invokevirtual getFloat : (IF)F
    //   510: fstore #14
    //   512: aload #4
    //   514: iconst_1
    //   515: newarray float
    //   517: dup
    //   518: iconst_0
    //   519: fload #14
    //   521: fastore
    //   522: invokestatic ofFloat : (Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;
    //   525: astore_0
    //   526: goto -> 724
    //   529: iload #6
    //   531: ifeq -> 659
    //   534: iload #7
    //   536: iconst_5
    //   537: if_icmpne -> 551
    //   540: aload_0
    //   541: iload_2
    //   542: fconst_0
    //   543: invokevirtual getDimension : (IF)F
    //   546: f2i
    //   547: istore_1
    //   548: goto -> 576
    //   551: iload #7
    //   553: invokestatic isColorType : (I)Z
    //   556: ifeq -> 569
    //   559: aload_0
    //   560: iload_2
    //   561: iconst_0
    //   562: invokevirtual getColor : (II)I
    //   565: istore_1
    //   566: goto -> 576
    //   569: aload_0
    //   570: iload_2
    //   571: iconst_0
    //   572: invokevirtual getInt : (II)I
    //   575: istore_1
    //   576: iload #8
    //   578: ifeq -> 643
    //   581: iload #9
    //   583: iconst_5
    //   584: if_icmpne -> 598
    //   587: aload_0
    //   588: iload_3
    //   589: fconst_0
    //   590: invokevirtual getDimension : (IF)F
    //   593: f2i
    //   594: istore_2
    //   595: goto -> 623
    //   598: iload #9
    //   600: invokestatic isColorType : (I)Z
    //   603: ifeq -> 616
    //   606: aload_0
    //   607: iload_3
    //   608: iconst_0
    //   609: invokevirtual getColor : (II)I
    //   612: istore_2
    //   613: goto -> 623
    //   616: aload_0
    //   617: iload_3
    //   618: iconst_0
    //   619: invokevirtual getInt : (II)I
    //   622: istore_2
    //   623: aload #4
    //   625: iconst_2
    //   626: newarray int
    //   628: dup
    //   629: iconst_0
    //   630: iload_1
    //   631: iastore
    //   632: dup
    //   633: iconst_1
    //   634: iload_2
    //   635: iastore
    //   636: invokestatic ofInt : (Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;
    //   639: astore_0
    //   640: goto -> 724
    //   643: aload #4
    //   645: iconst_1
    //   646: newarray int
    //   648: dup
    //   649: iconst_0
    //   650: iload_1
    //   651: iastore
    //   652: invokestatic ofInt : (Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;
    //   655: astore_0
    //   656: goto -> 724
    //   659: iload #8
    //   661: ifeq -> 722
    //   664: iload #9
    //   666: iconst_5
    //   667: if_icmpne -> 681
    //   670: aload_0
    //   671: iload_3
    //   672: fconst_0
    //   673: invokevirtual getDimension : (IF)F
    //   676: f2i
    //   677: istore_1
    //   678: goto -> 706
    //   681: iload #9
    //   683: invokestatic isColorType : (I)Z
    //   686: ifeq -> 699
    //   689: aload_0
    //   690: iload_3
    //   691: iconst_0
    //   692: invokevirtual getColor : (II)I
    //   695: istore_1
    //   696: goto -> 706
    //   699: aload_0
    //   700: iload_3
    //   701: iconst_0
    //   702: invokevirtual getInt : (II)I
    //   705: istore_1
    //   706: aload #4
    //   708: iconst_1
    //   709: newarray int
    //   711: dup
    //   712: iconst_0
    //   713: iload_1
    //   714: iastore
    //   715: invokestatic ofInt : (Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;
    //   718: astore_0
    //   719: goto -> 724
    //   722: aconst_null
    //   723: astore_0
    //   724: aload_0
    //   725: astore #4
    //   727: aload_0
    //   728: ifnull -> 748
    //   731: aload_0
    //   732: astore #4
    //   734: aload #5
    //   736: ifnull -> 748
    //   739: aload_0
    //   740: aload #5
    //   742: invokevirtual setEvaluator : (Landroid/animation/TypeEvaluator;)V
    //   745: aload_0
    //   746: astore #4
    //   748: aload #4
    //   750: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #275	-> 0
    //   #276	-> 7
    //   #277	-> 21
    //   #278	-> 39
    //   #279	-> 46
    //   #280	-> 60
    //   #282	-> 78
    //   #284	-> 83
    //   #285	-> 109
    //   #287	-> 114
    //   #282	-> 119
    //   #291	-> 119
    //   #293	-> 132
    //   #295	-> 132
    //   #296	-> 137
    //   #297	-> 144
    //   #298	-> 151
    //   #299	-> 156
    //   #300	-> 171
    //   #301	-> 176
    //   #303	-> 193
    //   #304	-> 208
    //   #305	-> 212
    //   #306	-> 222
    //   #307	-> 227
    //   #311	-> 236
    //   #308	-> 260
    //   #314	-> 310
    //   #317	-> 326
    //   #318	-> 334
    //   #319	-> 343
    //   #323	-> 362
    //   #324	-> 370
    //   #326	-> 373
    //   #328	-> 378
    //   #330	-> 383
    //   #333	-> 388
    //   #334	-> 393
    //   #335	-> 399
    //   #337	-> 410
    //   #339	-> 418
    //   #340	-> 423
    //   #341	-> 429
    //   #343	-> 440
    //   #345	-> 448
    //   #348	-> 470
    //   #351	-> 487
    //   #352	-> 493
    //   #354	-> 504
    //   #356	-> 512
    //   #358	-> 526
    //   #361	-> 529
    //   #362	-> 534
    //   #363	-> 540
    //   #364	-> 551
    //   #365	-> 559
    //   #367	-> 569
    //   #369	-> 576
    //   #370	-> 581
    //   #371	-> 587
    //   #372	-> 598
    //   #373	-> 606
    //   #375	-> 616
    //   #377	-> 623
    //   #379	-> 643
    //   #382	-> 659
    //   #383	-> 664
    //   #384	-> 670
    //   #385	-> 681
    //   #386	-> 689
    //   #388	-> 699
    //   #390	-> 706
    //   #382	-> 722
    //   #394	-> 724
    //   #395	-> 739
    //   #399	-> 748
  }
  
  private static void parseAnimatorFromTypeArray(ValueAnimator paramValueAnimator, TypedArray paramTypedArray1, TypedArray paramTypedArray2, float paramFloat) {
    long l1 = paramTypedArray1.getInt(1, 300);
    long l2 = paramTypedArray1.getInt(2, 0);
    int i = paramTypedArray1.getInt(7, 4);
    int j = i;
    if (i == 4)
      j = inferValueTypeFromValues(paramTypedArray1, 5, 6); 
    PropertyValuesHolder propertyValuesHolder = getPVH(paramTypedArray1, j, 5, 6, "");
    if (propertyValuesHolder != null)
      paramValueAnimator.setValues(new PropertyValuesHolder[] { propertyValuesHolder }); 
    paramValueAnimator.setDuration(l1);
    paramValueAnimator.setStartDelay(l2);
    if (paramTypedArray1.hasValue(3)) {
      i = paramTypedArray1.getInt(3, 0);
      paramValueAnimator.setRepeatCount(i);
    } 
    if (paramTypedArray1.hasValue(4)) {
      i = paramTypedArray1.getInt(4, 1);
      paramValueAnimator.setRepeatMode(i);
    } 
    if (paramTypedArray2 != null)
      setupObjectAnimator(paramValueAnimator, paramTypedArray2, j, paramFloat); 
  }
  
  private static TypeEvaluator setupAnimatorForPath(ValueAnimator paramValueAnimator, TypedArray paramTypedArray) {
    StringBuilder stringBuilder;
    PathDataEvaluator pathDataEvaluator;
    PathParser.PathData pathData1, pathData2;
    TypedArray typedArray = null;
    String str1 = paramTypedArray.getString(5);
    String str2 = paramTypedArray.getString(6);
    if (str1 == null) {
      pathData1 = null;
    } else {
      pathData1 = new PathParser.PathData(str1);
    } 
    if (str2 == null) {
      pathData2 = null;
    } else {
      pathData2 = new PathParser.PathData(str2);
    } 
    if (pathData1 != null) {
      if (pathData2 != null) {
        paramValueAnimator.setObjectValues(new Object[] { pathData1, pathData2 });
        if (!PathParser.canMorph(pathData1, pathData2)) {
          stringBuilder = new StringBuilder();
          stringBuilder.append(paramTypedArray.getPositionDescription());
          stringBuilder.append(" Can't morph from ");
          stringBuilder.append(str1);
          stringBuilder.append(" to ");
          stringBuilder.append(str2);
          throw new InflateException(stringBuilder.toString());
        } 
      } else {
        stringBuilder.setObjectValues(new Object[] { pathData1 });
      } 
      pathDataEvaluator = new PathDataEvaluator();
    } else {
      paramTypedArray = typedArray;
      if (pathData2 != null) {
        stringBuilder.setObjectValues(new Object[] { pathData2 });
        pathDataEvaluator = new PathDataEvaluator();
      } 
    } 
    return pathDataEvaluator;
  }
  
  private static void setupObjectAnimator(ValueAnimator paramValueAnimator, TypedArray paramTypedArray, int paramInt, float paramFloat) {
    // Byte code:
    //   0: iload_2
    //   1: istore #4
    //   3: aload_0
    //   4: checkcast android/animation/ObjectAnimator
    //   7: astore #5
    //   9: aload_1
    //   10: iconst_1
    //   11: invokevirtual getString : (I)Ljava/lang/String;
    //   14: astore_0
    //   15: aload_0
    //   16: ifnull -> 239
    //   19: aload_1
    //   20: iconst_2
    //   21: invokevirtual getString : (I)Ljava/lang/String;
    //   24: astore #6
    //   26: aload_1
    //   27: iconst_3
    //   28: invokevirtual getString : (I)Ljava/lang/String;
    //   31: astore #7
    //   33: iload #4
    //   35: iconst_2
    //   36: if_icmpeq -> 48
    //   39: iload #4
    //   41: istore_2
    //   42: iload #4
    //   44: iconst_4
    //   45: if_icmpne -> 50
    //   48: iconst_0
    //   49: istore_2
    //   50: aload #6
    //   52: ifnonnull -> 100
    //   55: aload #7
    //   57: ifnull -> 63
    //   60: goto -> 100
    //   63: new java/lang/StringBuilder
    //   66: dup
    //   67: invokespecial <init> : ()V
    //   70: astore_0
    //   71: aload_0
    //   72: aload_1
    //   73: invokevirtual getPositionDescription : ()Ljava/lang/String;
    //   76: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   79: pop
    //   80: aload_0
    //   81: ldc_w ' propertyXName or propertyYName is needed for PathData'
    //   84: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   87: pop
    //   88: new android/view/InflateException
    //   91: dup
    //   92: aload_0
    //   93: invokevirtual toString : ()Ljava/lang/String;
    //   96: invokespecial <init> : (Ljava/lang/String;)V
    //   99: athrow
    //   100: aload_0
    //   101: invokestatic createPathFromPathData : (Ljava/lang/String;)Landroid/graphics/Path;
    //   104: astore_0
    //   105: aload_0
    //   106: ldc_w 0.5
    //   109: fload_3
    //   110: fmul
    //   111: invokestatic ofPath : (Landroid/graphics/Path;F)Landroid/animation/PathKeyframes;
    //   114: astore_0
    //   115: iload_2
    //   116: ifne -> 132
    //   119: aload_0
    //   120: invokevirtual createXFloatKeyframes : ()Landroid/animation/Keyframes$FloatKeyframes;
    //   123: astore_1
    //   124: aload_0
    //   125: invokevirtual createYFloatKeyframes : ()Landroid/animation/Keyframes$FloatKeyframes;
    //   128: astore_0
    //   129: goto -> 142
    //   132: aload_0
    //   133: invokevirtual createXIntKeyframes : ()Landroid/animation/Keyframes$IntKeyframes;
    //   136: astore_1
    //   137: aload_0
    //   138: invokevirtual createYIntKeyframes : ()Landroid/animation/Keyframes$IntKeyframes;
    //   141: astore_0
    //   142: aconst_null
    //   143: astore #8
    //   145: aconst_null
    //   146: astore #9
    //   148: aload #6
    //   150: ifnull -> 161
    //   153: aload #6
    //   155: aload_1
    //   156: invokestatic ofKeyframes : (Ljava/lang/String;Landroid/animation/Keyframes;)Landroid/animation/PropertyValuesHolder;
    //   159: astore #8
    //   161: aload #9
    //   163: astore_1
    //   164: aload #7
    //   166: ifnull -> 176
    //   169: aload #7
    //   171: aload_0
    //   172: invokestatic ofKeyframes : (Ljava/lang/String;Landroid/animation/Keyframes;)Landroid/animation/PropertyValuesHolder;
    //   175: astore_1
    //   176: aload #8
    //   178: ifnonnull -> 197
    //   181: aload #5
    //   183: iconst_1
    //   184: anewarray android/animation/PropertyValuesHolder
    //   187: dup
    //   188: iconst_0
    //   189: aload_1
    //   190: aastore
    //   191: invokevirtual setValues : ([Landroid/animation/PropertyValuesHolder;)V
    //   194: goto -> 236
    //   197: aload_1
    //   198: ifnonnull -> 218
    //   201: aload #5
    //   203: iconst_1
    //   204: anewarray android/animation/PropertyValuesHolder
    //   207: dup
    //   208: iconst_0
    //   209: aload #8
    //   211: aastore
    //   212: invokevirtual setValues : ([Landroid/animation/PropertyValuesHolder;)V
    //   215: goto -> 236
    //   218: aload #5
    //   220: iconst_2
    //   221: anewarray android/animation/PropertyValuesHolder
    //   224: dup
    //   225: iconst_0
    //   226: aload #8
    //   228: aastore
    //   229: dup
    //   230: iconst_1
    //   231: aload_1
    //   232: aastore
    //   233: invokevirtual setValues : ([Landroid/animation/PropertyValuesHolder;)V
    //   236: goto -> 251
    //   239: aload_1
    //   240: iconst_0
    //   241: invokevirtual getString : (I)Ljava/lang/String;
    //   244: astore_0
    //   245: aload #5
    //   247: aload_0
    //   248: invokevirtual setPropertyName : (Ljava/lang/String;)V
    //   251: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #497	-> 0
    //   #498	-> 9
    //   #508	-> 15
    //   #509	-> 19
    //   #510	-> 19
    //   #511	-> 26
    //   #512	-> 26
    //   #514	-> 33
    //   #517	-> 48
    //   #519	-> 50
    //   #520	-> 63
    //   #523	-> 100
    //   #524	-> 105
    //   #525	-> 105
    //   #528	-> 115
    //   #529	-> 119
    //   #530	-> 124
    //   #532	-> 132
    //   #533	-> 137
    //   #535	-> 142
    //   #536	-> 145
    //   #537	-> 148
    //   #538	-> 153
    //   #540	-> 161
    //   #541	-> 169
    //   #543	-> 176
    //   #544	-> 181
    //   #545	-> 197
    //   #546	-> 201
    //   #548	-> 218
    //   #551	-> 236
    //   #552	-> 239
    //   #553	-> 239
    //   #554	-> 245
    //   #556	-> 251
  }
  
  private static void setupValues(ValueAnimator paramValueAnimator, TypedArray paramTypedArray, boolean paramBoolean1, boolean paramBoolean2, int paramInt1, boolean paramBoolean3, int paramInt2) {
    if (paramBoolean1) {
      if (paramBoolean2) {
        float f;
        if (paramInt1 == 5) {
          f = paramTypedArray.getDimension(5, 0.0F);
        } else {
          f = paramTypedArray.getFloat(5, 0.0F);
        } 
        if (paramBoolean3) {
          float f1;
          if (paramInt2 == 5) {
            f1 = paramTypedArray.getDimension(6, 0.0F);
          } else {
            f1 = paramTypedArray.getFloat(6, 0.0F);
          } 
          paramValueAnimator.setFloatValues(new float[] { f, f1 });
        } else {
          paramValueAnimator.setFloatValues(new float[] { f });
        } 
      } else {
        float f;
        if (paramInt2 == 5) {
          f = paramTypedArray.getDimension(6, 0.0F);
        } else {
          f = paramTypedArray.getFloat(6, 0.0F);
        } 
        paramValueAnimator.setFloatValues(new float[] { f });
      } 
    } else if (paramBoolean2) {
      if (paramInt1 == 5) {
        paramInt1 = (int)paramTypedArray.getDimension(5, 0.0F);
      } else if (isColorType(paramInt1)) {
        paramInt1 = paramTypedArray.getColor(5, 0);
      } else {
        paramInt1 = paramTypedArray.getInt(5, 0);
      } 
      if (paramBoolean3) {
        if (paramInt2 == 5) {
          paramInt2 = (int)paramTypedArray.getDimension(6, 0.0F);
        } else if (isColorType(paramInt2)) {
          paramInt2 = paramTypedArray.getColor(6, 0);
        } else {
          paramInt2 = paramTypedArray.getInt(6, 0);
        } 
        paramValueAnimator.setIntValues(new int[] { paramInt1, paramInt2 });
      } else {
        paramValueAnimator.setIntValues(new int[] { paramInt1 });
      } 
    } else if (paramBoolean3) {
      if (paramInt2 == 5) {
        paramInt1 = (int)paramTypedArray.getDimension(6, 0.0F);
      } else if (isColorType(paramInt2)) {
        paramInt1 = paramTypedArray.getColor(6, 0);
      } else {
        paramInt1 = paramTypedArray.getInt(6, 0);
      } 
      paramValueAnimator.setIntValues(new int[] { paramInt1 });
    } 
  }
  
  private static Animator createAnimatorFromXml(Resources paramResources, Resources.Theme paramTheme, XmlPullParser paramXmlPullParser, float paramFloat) throws XmlPullParserException, IOException {
    return createAnimatorFromXml(paramResources, paramTheme, paramXmlPullParser, Xml.asAttributeSet(paramXmlPullParser), null, 0, paramFloat);
  }
  
  private static Animator createAnimatorFromXml(Resources paramResources, Resources.Theme paramTheme, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, AnimatorSet paramAnimatorSet, int paramInt, float paramFloat) throws XmlPullParserException, IOException {
    AnimatorSet animatorSet;
    ArrayList<AnimatorSet> arrayList;
    ObjectAnimator objectAnimator = null;
    int i = paramXmlPullParser.getDepth();
    String str = null;
    while (true) {
      int j = paramXmlPullParser.next();
      if ((j != 3 || paramXmlPullParser.getDepth() > i) && j != 1) {
        ArrayList<AnimatorSet> arrayList1;
        if (j != 2)
          continue; 
        String str1 = paramXmlPullParser.getName();
        j = 0;
        if (str1.equals("objectAnimator")) {
          objectAnimator = loadObjectAnimator(paramResources, paramTheme, paramAttributeSet, paramFloat);
        } else if (str1.equals("animator")) {
          ValueAnimator valueAnimator = loadAnimator(paramResources, paramTheme, paramAttributeSet, null, paramFloat);
        } else {
          AnimatorSet animatorSet1;
          if (str1.equals("set")) {
            TypedArray typedArray;
            animatorSet1 = new AnimatorSet();
            if (paramTheme != null) {
              typedArray = paramTheme.obtainStyledAttributes(paramAttributeSet, R.styleable.AnimatorSet, 0, 0);
            } else {
              typedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AnimatorSet);
            } 
            animatorSet1.appendChangingConfigurations(typedArray.getChangingConfigurations());
            int k = typedArray.getInt(0, 0);
            createAnimatorFromXml(paramResources, paramTheme, paramXmlPullParser, paramAttributeSet, animatorSet1, k, paramFloat);
            typedArray.recycle();
            animatorSet = animatorSet1;
          } else if (animatorSet1.equals("propertyValuesHolder")) {
            AttributeSet attributeSet = Xml.asAttributeSet(paramXmlPullParser);
            PropertyValuesHolder[] arrayOfPropertyValuesHolder = loadValues(paramResources, paramTheme, paramXmlPullParser, attributeSet);
            if (arrayOfPropertyValuesHolder != null && animatorSet != null && animatorSet instanceof ValueAnimator)
              ((ValueAnimator)animatorSet).setValues(arrayOfPropertyValuesHolder); 
            j = 1;
          } else {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unknown animator name: ");
            stringBuilder.append(paramXmlPullParser.getName());
            throw new RuntimeException(stringBuilder.toString());
          } 
        } 
        str1 = str;
        if (paramAnimatorSet != null) {
          str1 = str;
          if (j == 0) {
            str1 = str;
            if (str == null)
              arrayList1 = new ArrayList(); 
            arrayList1.add(animatorSet);
          } 
        } 
        arrayList = arrayList1;
        continue;
      } 
      break;
    } 
    if (paramAnimatorSet != null && arrayList != null) {
      Animator[] arrayOfAnimator = new Animator[arrayList.size()];
      byte b = 0;
      for (Animator animator : arrayList) {
        arrayOfAnimator[b] = animator;
        b++;
      } 
      if (paramInt == 0) {
        paramAnimatorSet.playTogether(arrayOfAnimator);
      } else {
        paramAnimatorSet.playSequentially(arrayOfAnimator);
      } 
    } 
    return animatorSet;
  }
  
  private static PropertyValuesHolder[] loadValues(Resources paramResources, Resources.Theme paramTheme, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet) throws XmlPullParserException, IOException {
    PropertyValuesHolder[] arrayOfPropertyValuesHolder;
    ArrayList<PropertyValuesHolder> arrayList;
    PropertyValuesHolder propertyValuesHolder = null;
    while (true) {
      int i = paramXmlPullParser.getEventType();
      if (i != 3 && i != 1) {
        ArrayList<PropertyValuesHolder> arrayList1;
        if (i != 2) {
          paramXmlPullParser.next();
          continue;
        } 
        String str = paramXmlPullParser.getName();
        PropertyValuesHolder propertyValuesHolder1 = propertyValuesHolder;
        if (str.equals("propertyValuesHolder")) {
          TypedArray typedArray;
          if (paramTheme != null) {
            typedArray = paramTheme.obtainStyledAttributes(paramAttributeSet, R.styleable.PropertyValuesHolder, 0, 0);
          } else {
            typedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.PropertyValuesHolder);
          } 
          String str1 = typedArray.getString(3);
          i = typedArray.getInt(2, 4);
          propertyValuesHolder1 = loadPvh(paramResources, paramTheme, paramXmlPullParser, str1, i);
          PropertyValuesHolder propertyValuesHolder2 = propertyValuesHolder1;
          if (propertyValuesHolder1 == null)
            propertyValuesHolder2 = getPVH(typedArray, i, 0, 1, str1); 
          propertyValuesHolder1 = propertyValuesHolder;
          if (propertyValuesHolder2 != null) {
            propertyValuesHolder1 = propertyValuesHolder;
            if (propertyValuesHolder == null)
              arrayList1 = new ArrayList(); 
            arrayList1.add(propertyValuesHolder2);
          } 
          typedArray.recycle();
        } 
        paramXmlPullParser.next();
        arrayList = arrayList1;
        continue;
      } 
      break;
    } 
    paramResources = null;
    if (arrayList != null) {
      int i = arrayList.size();
      PropertyValuesHolder[] arrayOfPropertyValuesHolder1 = new PropertyValuesHolder[i];
      byte b = 0;
      while (true) {
        arrayOfPropertyValuesHolder = arrayOfPropertyValuesHolder1;
        if (b < i) {
          arrayOfPropertyValuesHolder1[b] = arrayList.get(b);
          b++;
          continue;
        } 
        break;
      } 
    } 
    return arrayOfPropertyValuesHolder;
  }
  
  private static int inferValueTypeOfKeyframe(Resources paramResources, Resources.Theme paramTheme, AttributeSet paramAttributeSet) {
    TypedArray typedArray;
    byte b = 0;
    if (paramTheme != null) {
      typedArray = paramTheme.obtainStyledAttributes(paramAttributeSet, R.styleable.Keyframe, 0, 0);
    } else {
      typedArray = typedArray.obtainAttributes(paramAttributeSet, R.styleable.Keyframe);
    } 
    TypedValue typedValue = typedArray.peekValue(0);
    if (typedValue != null)
      b = 1; 
    if (b && isColorType(typedValue.type)) {
      b = 3;
    } else {
      b = 0;
    } 
    typedArray.recycle();
    return b;
  }
  
  private static int inferValueTypeFromValues(TypedArray paramTypedArray, int paramInt1, int paramInt2) {
    boolean bool2;
    TypedValue typedValue2 = paramTypedArray.peekValue(paramInt1);
    boolean bool1 = true;
    int i = 0;
    if (typedValue2 != null) {
      paramInt1 = 1;
    } else {
      paramInt1 = 0;
    } 
    if (paramInt1 != 0) {
      bool2 = typedValue2.type;
    } else {
      bool2 = false;
    } 
    TypedValue typedValue1 = paramTypedArray.peekValue(paramInt2);
    if (typedValue1 != null) {
      paramInt2 = bool1;
    } else {
      paramInt2 = 0;
    } 
    if (paramInt2 != 0)
      i = typedValue1.type; 
    if ((paramInt1 != 0 && isColorType(bool2)) || (paramInt2 != 0 && isColorType(i))) {
      paramInt1 = 3;
    } else {
      paramInt1 = 0;
    } 
    return paramInt1;
  }
  
  private static void dumpKeyframes(Object[] paramArrayOfObject, String paramString) {
    if (paramArrayOfObject == null || paramArrayOfObject.length == 0)
      return; 
    Log.d("AnimatorInflater", paramString);
    int i = paramArrayOfObject.length;
    for (byte b = 0; b < i; b++) {
      Float float_;
      Keyframe keyframe = (Keyframe)paramArrayOfObject[b];
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Keyframe ");
      stringBuilder.append(b);
      stringBuilder.append(": fraction ");
      float f = keyframe.getFraction();
      String str = "null";
      if (f < 0.0F) {
        paramString = "null";
      } else {
        float_ = Float.valueOf(keyframe.getFraction());
      } 
      stringBuilder.append(float_);
      stringBuilder.append(", , value : ");
      Object object = str;
      if (keyframe.hasValue())
        object = keyframe.getValue(); 
      stringBuilder.append(object);
      object = stringBuilder.toString();
      Log.d("AnimatorInflater", (String)object);
    } 
  }
  
  private static PropertyValuesHolder loadPvh(Resources paramResources, Resources.Theme paramTheme, XmlPullParser paramXmlPullParser, String paramString, int paramInt) throws XmlPullParserException, IOException {
    Resources resources = null;
    ArrayList<Keyframe> arrayList = null;
    while (true) {
      int i = paramXmlPullParser.next();
      if (i != 3 && i != 1) {
        String str = paramXmlPullParser.getName();
        i = paramInt;
        ArrayList<Keyframe> arrayList1 = arrayList;
        if (str.equals("keyframe")) {
          i = paramInt;
          if (paramInt == 4)
            i = inferValueTypeOfKeyframe(paramResources, paramTheme, Xml.asAttributeSet(paramXmlPullParser)); 
          Keyframe keyframe = loadKeyframe(paramResources, paramTheme, Xml.asAttributeSet(paramXmlPullParser), i);
          arrayList1 = arrayList;
          if (keyframe != null) {
            arrayList1 = arrayList;
            if (arrayList == null)
              arrayList1 = new ArrayList(); 
            arrayList1.add(keyframe);
          } 
          paramXmlPullParser.next();
        } 
        paramInt = i;
        arrayList = arrayList1;
        continue;
      } 
      break;
    } 
    if (arrayList != null) {
      int i = arrayList.size(), j = i;
      if (i > 0) {
        Keyframe keyframe2 = arrayList.get(0);
        Keyframe keyframe1 = arrayList.get(j - 1);
        float f1 = keyframe1.getFraction();
        float f2 = 0.0F;
        i = j;
        if (f1 < 1.0F)
          if (f1 < 0.0F) {
            keyframe1.setFraction(1.0F);
            i = j;
          } else {
            arrayList.add(arrayList.size(), createNewKeyframe(keyframe1, 1.0F));
            i = j + 1;
          }  
        f1 = keyframe2.getFraction();
        int k = i;
        if (f1 != 0.0F)
          if (f1 < 0.0F) {
            keyframe2.setFraction(0.0F);
            k = i;
          } else {
            arrayList.add(0, createNewKeyframe(keyframe2, 0.0F));
            k = i + 1;
          }  
        Keyframe[] arrayOfKeyframe = new Keyframe[k];
        arrayList.toArray(arrayOfKeyframe);
        for (i = 0; i < k; i++) {
          keyframe2 = arrayOfKeyframe[i];
          if (keyframe2.getFraction() < f2)
            if (i == 0) {
              keyframe2.setFraction(f2);
            } else if (i == k - 1) {
              keyframe2.setFraction(1.0F);
              f2 = 0.0F;
            } else {
              int m;
              for (m = i, j = i + 1; j < k - 1 && 
                arrayOfKeyframe[j].getFraction() < 0.0F; j++)
                m = j; 
              f2 = 0.0F;
              float f = arrayOfKeyframe[m + 1].getFraction();
              keyframe2 = arrayOfKeyframe[i - 1];
              f1 = keyframe2.getFraction();
              distributeKeyframes(arrayOfKeyframe, f - f1, i, m);
            }  
        } 
        PropertyValuesHolder propertyValuesHolder2 = PropertyValuesHolder.ofKeyframe(paramString, arrayOfKeyframe);
        PropertyValuesHolder propertyValuesHolder1 = propertyValuesHolder2;
        if (paramInt == 3) {
          propertyValuesHolder2.setEvaluator(ArgbEvaluator.getInstance());
          propertyValuesHolder1 = propertyValuesHolder2;
        } 
      } else {
        paramResources = resources;
      } 
    } else {
      paramResources = resources;
    } 
    return (PropertyValuesHolder)paramResources;
  }
  
  private static Keyframe createNewKeyframe(Keyframe paramKeyframe, float paramFloat) {
    if (paramKeyframe.getType() == float.class) {
      paramKeyframe = Keyframe.ofFloat(paramFloat);
    } else if (paramKeyframe.getType() == int.class) {
      paramKeyframe = Keyframe.ofInt(paramFloat);
    } else {
      paramKeyframe = Keyframe.ofObject(paramFloat);
    } 
    return paramKeyframe;
  }
  
  private static void distributeKeyframes(Keyframe[] paramArrayOfKeyframe, float paramFloat, int paramInt1, int paramInt2) {
    paramFloat /= (paramInt2 - paramInt1 + 2);
    for (; paramInt1 <= paramInt2; paramInt1++)
      paramArrayOfKeyframe[paramInt1].setFraction(paramArrayOfKeyframe[paramInt1 - 1].getFraction() + paramFloat); 
  }
  
  private static Keyframe loadKeyframe(Resources paramResources, Resources.Theme paramTheme, AttributeSet paramAttributeSet, int paramInt) throws XmlPullParserException, IOException {
    Keyframe keyframe;
    TypedArray typedArray;
    boolean bool;
    if (paramTheme != null) {
      typedArray = paramTheme.obtainStyledAttributes(paramAttributeSet, R.styleable.Keyframe, 0, 0);
    } else {
      typedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.Keyframe);
    } 
    paramAttributeSet = null;
    float f = typedArray.getFloat(3, -1.0F);
    TypedValue typedValue = typedArray.peekValue(0);
    if (typedValue != null) {
      bool = true;
    } else {
      bool = false;
    } 
    int i = paramInt;
    if (paramInt == 4)
      if (bool && isColorType(typedValue.type)) {
        i = 3;
      } else {
        i = 0;
      }  
    if (bool) {
      if (i != 0) {
        if (i == 1 || i == 3) {
          paramInt = typedArray.getInt(0, 0);
          keyframe = Keyframe.ofInt(f, paramInt);
        } 
      } else {
        float f1 = typedArray.getFloat(0, 0.0F);
        keyframe = Keyframe.ofFloat(f, f1);
      } 
    } else if (i == 0) {
      keyframe = Keyframe.ofFloat(f);
    } else {
      keyframe = Keyframe.ofInt(f);
    } 
    paramInt = typedArray.getResourceId(1, 0);
    if (paramInt > 0) {
      Interpolator interpolator = AnimationUtils.loadInterpolator(paramResources, paramTheme, paramInt);
      keyframe.setInterpolator((TimeInterpolator)interpolator);
    } 
    typedArray.recycle();
    return keyframe;
  }
  
  private static ObjectAnimator loadObjectAnimator(Resources paramResources, Resources.Theme paramTheme, AttributeSet paramAttributeSet, float paramFloat) throws Resources.NotFoundException {
    ObjectAnimator objectAnimator = new ObjectAnimator();
    loadAnimator(paramResources, paramTheme, paramAttributeSet, objectAnimator, paramFloat);
    return objectAnimator;
  }
  
  private static ValueAnimator loadAnimator(Resources paramResources, Resources.Theme paramTheme, AttributeSet paramAttributeSet, ValueAnimator paramValueAnimator, float paramFloat) throws Resources.NotFoundException {
    TypedArray typedArray2, typedArray1 = null;
    if (paramTheme != null) {
      typedArray2 = paramTheme.obtainStyledAttributes(paramAttributeSet, R.styleable.Animator, 0, 0);
    } else {
      typedArray2 = paramResources.obtainAttributes(paramAttributeSet, R.styleable.Animator);
    } 
    if (paramValueAnimator != null) {
      TypedArray typedArray;
      if (paramTheme != null) {
        typedArray = paramTheme.obtainStyledAttributes(paramAttributeSet, R.styleable.PropertyAnimator, 0, 0);
      } else {
        typedArray = paramResources.obtainAttributes((AttributeSet)typedArray, R.styleable.PropertyAnimator);
      } 
      paramValueAnimator.appendChangingConfigurations(typedArray.getChangingConfigurations());
      typedArray1 = typedArray;
    } 
    ValueAnimator valueAnimator = paramValueAnimator;
    if (paramValueAnimator == null)
      valueAnimator = new ValueAnimator(); 
    valueAnimator.appendChangingConfigurations(typedArray2.getChangingConfigurations());
    parseAnimatorFromTypeArray(valueAnimator, typedArray2, typedArray1, paramFloat);
    int i = typedArray2.getResourceId(0, 0);
    if (i > 0) {
      Interpolator interpolator = AnimationUtils.loadInterpolator(paramResources, paramTheme, i);
      if (interpolator instanceof BaseInterpolator) {
        BaseInterpolator baseInterpolator = (BaseInterpolator)interpolator;
        i = baseInterpolator.getChangingConfiguration();
        valueAnimator.appendChangingConfigurations(i);
      } 
      valueAnimator.setInterpolator((TimeInterpolator)interpolator);
    } 
    typedArray2.recycle();
    if (typedArray1 != null)
      typedArray1.recycle(); 
    return valueAnimator;
  }
  
  private static int getChangingConfigs(Resources paramResources, int paramInt) {
    synchronized (sTmpTypedValue) {
      paramResources.getValue(paramInt, sTmpTypedValue, true);
      paramInt = sTmpTypedValue.changingConfigurations;
      return paramInt;
    } 
  }
  
  private static boolean isColorType(int paramInt) {
    boolean bool;
    if (paramInt >= 28 && paramInt <= 31) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
