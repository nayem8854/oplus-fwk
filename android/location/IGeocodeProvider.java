package android.location;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IGeocodeProvider extends IInterface {
  String getFromLocation(double paramDouble1, double paramDouble2, int paramInt, GeocoderParams paramGeocoderParams, List<Address> paramList) throws RemoteException;
  
  String getFromLocationName(String paramString, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, int paramInt, GeocoderParams paramGeocoderParams, List<Address> paramList) throws RemoteException;
  
  class Default implements IGeocodeProvider {
    public String getFromLocation(double param1Double1, double param1Double2, int param1Int, GeocoderParams param1GeocoderParams, List<Address> param1List) throws RemoteException {
      return null;
    }
    
    public String getFromLocationName(String param1String, double param1Double1, double param1Double2, double param1Double3, double param1Double4, int param1Int, GeocoderParams param1GeocoderParams, List<Address> param1List) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IGeocodeProvider {
    private static final String DESCRIPTOR = "android.location.IGeocodeProvider";
    
    static final int TRANSACTION_getFromLocation = 1;
    
    static final int TRANSACTION_getFromLocationName = 2;
    
    public Stub() {
      attachInterface(this, "android.location.IGeocodeProvider");
    }
    
    public static IGeocodeProvider asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.location.IGeocodeProvider");
      if (iInterface != null && iInterface instanceof IGeocodeProvider)
        return (IGeocodeProvider)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "getFromLocationName";
      } 
      return "getFromLocation";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.location.IGeocodeProvider");
          return true;
        } 
        param1Parcel1.enforceInterface("android.location.IGeocodeProvider");
        String str1 = param1Parcel1.readString();
        double d3 = param1Parcel1.readDouble();
        double d4 = param1Parcel1.readDouble();
        double d5 = param1Parcel1.readDouble();
        double d6 = param1Parcel1.readDouble();
        param1Int1 = param1Parcel1.readInt();
        if (param1Parcel1.readInt() != 0) {
          GeocoderParams geocoderParams = GeocoderParams.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        ArrayList<Address> arrayList1 = new ArrayList();
        str = getFromLocationName(str1, d3, d4, d5, d6, param1Int1, (GeocoderParams)param1Parcel1, arrayList1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeString(str);
        param1Parcel2.writeTypedList(arrayList1);
        return true;
      } 
      str.enforceInterface("android.location.IGeocodeProvider");
      double d1 = str.readDouble();
      double d2 = str.readDouble();
      param1Int1 = str.readInt();
      if (str.readInt() != 0) {
        GeocoderParams geocoderParams = GeocoderParams.CREATOR.createFromParcel((Parcel)str);
      } else {
        str = null;
      } 
      ArrayList<Address> arrayList = new ArrayList();
      String str = getFromLocation(d1, d2, param1Int1, (GeocoderParams)str, arrayList);
      param1Parcel2.writeNoException();
      param1Parcel2.writeString(str);
      param1Parcel2.writeTypedList(arrayList);
      return true;
    }
    
    private static class Proxy implements IGeocodeProvider {
      public static IGeocodeProvider sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.location.IGeocodeProvider";
      }
      
      public String getFromLocation(double param2Double1, double param2Double2, int param2Int, GeocoderParams param2GeocoderParams, List<Address> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.IGeocodeProvider");
          try {
            parcel1.writeDouble(param2Double1);
            try {
              parcel1.writeDouble(param2Double2);
              parcel1.writeInt(param2Int);
              if (param2GeocoderParams != null) {
                parcel1.writeInt(1);
                param2GeocoderParams.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
              if (!bool && IGeocodeProvider.Stub.getDefaultImpl() != null) {
                String str1 = IGeocodeProvider.Stub.getDefaultImpl().getFromLocation(param2Double1, param2Double2, param2Int, param2GeocoderParams, param2List);
                parcel2.recycle();
                parcel1.recycle();
                return str1;
              } 
              parcel2.readException();
              String str = parcel2.readString();
              Parcelable.Creator<Address> creator = Address.CREATOR;
              try {
                parcel2.readTypedList(param2List, creator);
                parcel2.recycle();
                parcel1.recycle();
                return str;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2GeocoderParams;
      }
      
      public String getFromLocationName(String param2String, double param2Double1, double param2Double2, double param2Double3, double param2Double4, int param2Int, GeocoderParams param2GeocoderParams, List<Address> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.IGeocodeProvider");
          parcel1.writeString(param2String);
          parcel1.writeDouble(param2Double1);
          parcel1.writeDouble(param2Double2);
          parcel1.writeDouble(param2Double3);
          parcel1.writeDouble(param2Double4);
          parcel1.writeInt(param2Int);
          if (param2GeocoderParams != null) {
            try {
              parcel1.writeInt(1);
              param2GeocoderParams.writeToParcel(parcel1, 0);
            } finally {}
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool)
            try {
              if (IGeocodeProvider.Stub.getDefaultImpl() != null) {
                IGeocodeProvider iGeocodeProvider = IGeocodeProvider.Stub.getDefaultImpl();
                try {
                  param2String = iGeocodeProvider.getFromLocationName(param2String, param2Double1, param2Double2, param2Double3, param2Double4, param2Int, param2GeocoderParams, param2List);
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2String;
                } finally {}
                parcel2.recycle();
                parcel1.recycle();
                throw param2String;
              } 
            } finally {} 
          try {
            parcel2.readException();
            String str = parcel2.readString();
            Parcelable.Creator<Address> creator = Address.CREATOR;
            try {
              parcel2.readTypedList(param2List, creator);
              parcel2.recycle();
              parcel1.recycle();
              return str;
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
    }
    
    public static boolean setDefaultImpl(IGeocodeProvider param1IGeocodeProvider) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IGeocodeProvider != null) {
          Proxy.sDefaultImpl = param1IGeocodeProvider;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IGeocodeProvider getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
