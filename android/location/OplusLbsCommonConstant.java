package android.location;

import android.content.Context;
import android.os.OplusSystemProperties;

public class OplusLbsCommonConstant {
  public static final String ACTION_UPDATE_REQUIREMENTS = "oppo.location.blacklist.update.gps.requirements";
  
  public static final int NAVIGATION_STATUS_OFF = 2;
  
  public static final int NAVIGATION_STATUS_ON = 1;
  
  public static final int RUS_DEFAULT_RETURN_INT = -1;
  
  public static final String[] mOverseaNlpPackages;
  
  public static final String[] mRegionNlpPackages = new String[] { "com.baidu.map.location", "com.amap.android.location", "com.amap.android.ams", "com.tencent.android.location" };
  
  static {
    mOverseaNlpPackages = new String[] { "com.google.android.gms" };
  }
  
  public static boolean isNLP(String paramString) {
    return (isRegionNlpPackage(paramString) || isOverseaNlpPackage(paramString));
  }
  
  @Deprecated
  public static boolean isCnRom(Context paramContext) {
    return isCnRom();
  }
  
  public static boolean isCnRom() {
    return OplusSystemProperties.getBoolean("ro.oplus.connectivity.oversea", false) ^ true;
  }
  
  public static boolean isRegionNlpExist(Context paramContext) {
    for (String str : mRegionNlpPackages) {
      if (isPackageExist(paramContext, str))
        return true; 
    } 
    return false;
  }
  
  private static boolean isPackageExist(Context paramContext, String paramString) {
    // Byte code:
    //   0: iconst_1
    //   1: istore_2
    //   2: aload_1
    //   3: ifnull -> 18
    //   6: aload_1
    //   7: invokevirtual isEmpty : ()Z
    //   10: ifne -> 18
    //   13: iconst_1
    //   14: istore_3
    //   15: goto -> 20
    //   18: iconst_0
    //   19: istore_3
    //   20: iload_3
    //   21: istore #4
    //   23: aload_0
    //   24: ifnull -> 103
    //   27: iload_3
    //   28: istore #4
    //   30: iload_3
    //   31: ifeq -> 103
    //   34: invokestatic clearCallingIdentity : ()J
    //   37: lstore #5
    //   39: invokestatic getCallingUserId : ()I
    //   42: istore #7
    //   44: aload_0
    //   45: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   48: astore_0
    //   49: aload_0
    //   50: aload_1
    //   51: iconst_0
    //   52: iload #7
    //   54: invokevirtual getPackageInfoAsUser : (Ljava/lang/String;II)Landroid/content/pm/PackageInfo;
    //   57: getfield versionName : Ljava/lang/String;
    //   60: astore_0
    //   61: aload_0
    //   62: ifnull -> 79
    //   65: aload_0
    //   66: invokevirtual isEmpty : ()Z
    //   69: istore_3
    //   70: iload_3
    //   71: ifne -> 79
    //   74: iload_2
    //   75: istore_3
    //   76: goto -> 81
    //   79: iconst_0
    //   80: istore_3
    //   81: goto -> 95
    //   84: astore_0
    //   85: lload #5
    //   87: invokestatic restoreCallingIdentity : (J)V
    //   90: aload_0
    //   91: athrow
    //   92: astore_0
    //   93: iconst_0
    //   94: istore_3
    //   95: lload #5
    //   97: invokestatic restoreCallingIdentity : (J)V
    //   100: iload_3
    //   101: istore #4
    //   103: iload #4
    //   105: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #57	-> 0
    //   #58	-> 20
    //   #59	-> 34
    //   #61	-> 39
    //   #62	-> 44
    //   #63	-> 49
    //   #65	-> 61
    //   #69	-> 84
    //   #70	-> 90
    //   #66	-> 92
    //   #67	-> 93
    //   #69	-> 95
    //   #73	-> 103
    // Exception table:
    //   from	to	target	type
    //   39	44	92	android/content/pm/PackageManager$NameNotFoundException
    //   39	44	84	finally
    //   44	49	92	android/content/pm/PackageManager$NameNotFoundException
    //   44	49	84	finally
    //   49	61	92	android/content/pm/PackageManager$NameNotFoundException
    //   49	61	84	finally
    //   65	70	92	android/content/pm/PackageManager$NameNotFoundException
    //   65	70	84	finally
  }
  
  private static boolean isRegionNlpPackage(String paramString) {
    for (String str : mRegionNlpPackages) {
      if (str.equals(paramString))
        return true; 
    } 
    return false;
  }
  
  private static boolean isOverseaNlpPackage(String paramString) {
    for (String str : mOverseaNlpPackages) {
      if (str.equals(paramString))
        return true; 
    } 
    return false;
  }
}
