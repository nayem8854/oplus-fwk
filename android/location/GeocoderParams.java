package android.location;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Locale;

public class GeocoderParams implements Parcelable {
  private GeocoderParams() {}
  
  public GeocoderParams(Context paramContext, Locale paramLocale) {
    this.mLocale = paramLocale;
    this.mPackageName = paramContext.getPackageName();
  }
  
  public Locale getLocale() {
    return this.mLocale;
  }
  
  public String getClientPackage() {
    return this.mPackageName;
  }
  
  public static final Parcelable.Creator<GeocoderParams> CREATOR = new Parcelable.Creator<GeocoderParams>() {
      public GeocoderParams createFromParcel(Parcel param1Parcel) {
        GeocoderParams geocoderParams = new GeocoderParams();
        String str1 = param1Parcel.readString();
        String str2 = param1Parcel.readString();
        String str3 = param1Parcel.readString();
        GeocoderParams.access$102(geocoderParams, new Locale(str1, str2, str3));
        GeocoderParams.access$202(geocoderParams, param1Parcel.readString());
        return geocoderParams;
      }
      
      public GeocoderParams[] newArray(int param1Int) {
        return new GeocoderParams[param1Int];
      }
    };
  
  private Locale mLocale;
  
  private String mPackageName;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mLocale.getLanguage());
    paramParcel.writeString(this.mLocale.getCountry());
    paramParcel.writeString(this.mLocale.getVariant());
    paramParcel.writeString(this.mPackageName);
  }
}
