package android.location;

import android.hardware.location.GeofenceHardwareRequestParcelable;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IFusedGeofenceHardware extends IInterface {
  void addGeofences(GeofenceHardwareRequestParcelable[] paramArrayOfGeofenceHardwareRequestParcelable) throws RemoteException;
  
  boolean isSupported() throws RemoteException;
  
  void modifyGeofenceOptions(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) throws RemoteException;
  
  void pauseMonitoringGeofence(int paramInt) throws RemoteException;
  
  void removeGeofences(int[] paramArrayOfint) throws RemoteException;
  
  void resumeMonitoringGeofence(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements IFusedGeofenceHardware {
    public boolean isSupported() throws RemoteException {
      return false;
    }
    
    public void addGeofences(GeofenceHardwareRequestParcelable[] param1ArrayOfGeofenceHardwareRequestParcelable) throws RemoteException {}
    
    public void removeGeofences(int[] param1ArrayOfint) throws RemoteException {}
    
    public void pauseMonitoringGeofence(int param1Int) throws RemoteException {}
    
    public void resumeMonitoringGeofence(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void modifyGeofenceOptions(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IFusedGeofenceHardware {
    private static final String DESCRIPTOR = "android.location.IFusedGeofenceHardware";
    
    static final int TRANSACTION_addGeofences = 2;
    
    static final int TRANSACTION_isSupported = 1;
    
    static final int TRANSACTION_modifyGeofenceOptions = 6;
    
    static final int TRANSACTION_pauseMonitoringGeofence = 4;
    
    static final int TRANSACTION_removeGeofences = 3;
    
    static final int TRANSACTION_resumeMonitoringGeofence = 5;
    
    public Stub() {
      attachInterface(this, "android.location.IFusedGeofenceHardware");
    }
    
    public static IFusedGeofenceHardware asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.location.IFusedGeofenceHardware");
      if (iInterface != null && iInterface instanceof IFusedGeofenceHardware)
        return (IFusedGeofenceHardware)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "modifyGeofenceOptions";
        case 5:
          return "resumeMonitoringGeofence";
        case 4:
          return "pauseMonitoringGeofence";
        case 3:
          return "removeGeofences";
        case 2:
          return "addGeofences";
        case 1:
          break;
      } 
      return "isSupported";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        int[] arrayOfInt;
        GeofenceHardwareRequestParcelable[] arrayOfGeofenceHardwareRequestParcelable;
        int i, j, k, m;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("android.location.IFusedGeofenceHardware");
            i = param1Parcel1.readInt();
            j = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            k = param1Parcel1.readInt();
            m = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            modifyGeofenceOptions(i, j, param1Int2, k, m, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.location.IFusedGeofenceHardware");
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            resumeMonitoringGeofence(param1Int2, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.location.IFusedGeofenceHardware");
            param1Int1 = param1Parcel1.readInt();
            pauseMonitoringGeofence(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.location.IFusedGeofenceHardware");
            arrayOfInt = param1Parcel1.createIntArray();
            removeGeofences(arrayOfInt);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            arrayOfInt.enforceInterface("android.location.IFusedGeofenceHardware");
            arrayOfGeofenceHardwareRequestParcelable = arrayOfInt.<GeofenceHardwareRequestParcelable>createTypedArray(GeofenceHardwareRequestParcelable.CREATOR);
            addGeofences(arrayOfGeofenceHardwareRequestParcelable);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        arrayOfGeofenceHardwareRequestParcelable.enforceInterface("android.location.IFusedGeofenceHardware");
        boolean bool = isSupported();
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool);
        return true;
      } 
      param1Parcel2.writeString("android.location.IFusedGeofenceHardware");
      return true;
    }
    
    private static class Proxy implements IFusedGeofenceHardware {
      public static IFusedGeofenceHardware sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.location.IFusedGeofenceHardware";
      }
      
      public boolean isSupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.IFusedGeofenceHardware");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IFusedGeofenceHardware.Stub.getDefaultImpl() != null) {
            bool1 = IFusedGeofenceHardware.Stub.getDefaultImpl().isSupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addGeofences(GeofenceHardwareRequestParcelable[] param2ArrayOfGeofenceHardwareRequestParcelable) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.IFusedGeofenceHardware");
          parcel1.writeTypedArray(param2ArrayOfGeofenceHardwareRequestParcelable, 0);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IFusedGeofenceHardware.Stub.getDefaultImpl() != null) {
            IFusedGeofenceHardware.Stub.getDefaultImpl().addGeofences(param2ArrayOfGeofenceHardwareRequestParcelable);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeGeofences(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.IFusedGeofenceHardware");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IFusedGeofenceHardware.Stub.getDefaultImpl() != null) {
            IFusedGeofenceHardware.Stub.getDefaultImpl().removeGeofences(param2ArrayOfint);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void pauseMonitoringGeofence(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.IFusedGeofenceHardware");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IFusedGeofenceHardware.Stub.getDefaultImpl() != null) {
            IFusedGeofenceHardware.Stub.getDefaultImpl().pauseMonitoringGeofence(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resumeMonitoringGeofence(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.IFusedGeofenceHardware");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IFusedGeofenceHardware.Stub.getDefaultImpl() != null) {
            IFusedGeofenceHardware.Stub.getDefaultImpl().resumeMonitoringGeofence(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void modifyGeofenceOptions(int param2Int1, int param2Int2, int param2Int3, int param2Int4, int param2Int5, int param2Int6) throws RemoteException {
        Exception exception;
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.IFusedGeofenceHardware");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeInt(param2Int2);
              try {
                parcel1.writeInt(param2Int3);
                try {
                  parcel1.writeInt(param2Int4);
                  try {
                    parcel1.writeInt(param2Int5);
                    try {
                      parcel1.writeInt(param2Int6);
                      boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
                      if (!bool && IFusedGeofenceHardware.Stub.getDefaultImpl() != null) {
                        IFusedGeofenceHardware.Stub.getDefaultImpl().modifyGeofenceOptions(param2Int1, param2Int2, param2Int3, param2Int4, param2Int5, param2Int6);
                        parcel2.recycle();
                        parcel1.recycle();
                        return;
                      } 
                      parcel2.readException();
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw exception;
      }
    }
    
    public static boolean setDefaultImpl(IFusedGeofenceHardware param1IFusedGeofenceHardware) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IFusedGeofenceHardware != null) {
          Proxy.sDefaultImpl = param1IFusedGeofenceHardware;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IFusedGeofenceHardware getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
