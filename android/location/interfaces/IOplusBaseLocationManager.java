package android.location.interfaces;

import oplus.app.IOplusCommonManager;

public interface IOplusBaseLocationManager extends IOplusCommonManager {
  public static final String DESCRIPTOR = "android.location.ILocationManager";
  
  public static final int PSW_FIRST_CALL_TRANSACTION = 20001;
}
