package android.location.interfaces;

import android.location.LocAppsOp;
import android.os.RemoteException;
import java.util.List;

public interface IOplusLocationManager extends IOplusBaseLocationManager {
  public static final int FUN_GET_IN_USE_PACKAGES_LIST = 20004;
  
  public static final int FUN_GET_LOC_APPS_OP = 20002;
  
  public static final int FUN_SET_LOC_APPS_OP = 20003;
  
  List<String> getInUsePackagesList() throws RemoteException;
  
  void getLocAppsOp(int paramInt, LocAppsOp paramLocAppsOp) throws RemoteException;
  
  void setLocAppsOp(int paramInt, LocAppsOp paramLocAppsOp) throws RemoteException;
}
