package android.location;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.security.InvalidParameterException;

@SystemApi
@Deprecated
public class GpsNavigationMessage implements Parcelable {
  public static final Parcelable.Creator<GpsNavigationMessage> CREATOR;
  
  private static final byte[] EMPTY_ARRAY = new byte[0];
  
  public static final short STATUS_PARITY_PASSED = 1;
  
  public static final short STATUS_PARITY_REBUILT = 2;
  
  public static final short STATUS_UNKNOWN = 0;
  
  public static final byte TYPE_CNAV2 = 4;
  
  public static final byte TYPE_L1CA = 1;
  
  public static final byte TYPE_L2CNAV = 2;
  
  public static final byte TYPE_L5CNAV = 3;
  
  public static final byte TYPE_UNKNOWN = 0;
  
  private byte[] mData;
  
  private short mMessageId;
  
  private byte mPrn;
  
  private short mStatus;
  
  private short mSubmessageId;
  
  private byte mType;
  
  GpsNavigationMessage() {
    initialize();
  }
  
  public void set(GpsNavigationMessage paramGpsNavigationMessage) {
    this.mType = paramGpsNavigationMessage.mType;
    this.mPrn = paramGpsNavigationMessage.mPrn;
    this.mMessageId = paramGpsNavigationMessage.mMessageId;
    this.mSubmessageId = paramGpsNavigationMessage.mSubmessageId;
    this.mData = paramGpsNavigationMessage.mData;
    this.mStatus = paramGpsNavigationMessage.mStatus;
  }
  
  public void reset() {
    initialize();
  }
  
  public byte getType() {
    return this.mType;
  }
  
  public void setType(byte paramByte) {
    this.mType = paramByte;
  }
  
  private String getTypeString() {
    byte b = this.mType;
    if (b != 0) {
      if (b != 1) {
        if (b != 2) {
          if (b != 3) {
            if (b != 4) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("<Invalid:");
              stringBuilder.append(this.mType);
              stringBuilder.append(">");
              return stringBuilder.toString();
            } 
            return "CNAV-2";
          } 
          return "L5-CNAV";
        } 
        return "L2-CNAV";
      } 
      return "L1 C/A";
    } 
    return "Unknown";
  }
  
  public byte getPrn() {
    return this.mPrn;
  }
  
  public void setPrn(byte paramByte) {
    this.mPrn = paramByte;
  }
  
  public short getMessageId() {
    return this.mMessageId;
  }
  
  public void setMessageId(short paramShort) {
    this.mMessageId = paramShort;
  }
  
  public short getSubmessageId() {
    return this.mSubmessageId;
  }
  
  public void setSubmessageId(short paramShort) {
    this.mSubmessageId = paramShort;
  }
  
  public byte[] getData() {
    return this.mData;
  }
  
  public void setData(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null) {
      this.mData = paramArrayOfbyte;
      return;
    } 
    throw new InvalidParameterException("Data must be a non-null array");
  }
  
  public short getStatus() {
    return this.mStatus;
  }
  
  public void setStatus(short paramShort) {
    this.mStatus = paramShort;
  }
  
  private String getStatusString() {
    short s = this.mStatus;
    if (s != 0) {
      if (s != 1) {
        if (s != 2) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("<Invalid:");
          stringBuilder.append(this.mStatus);
          stringBuilder.append(">");
          return stringBuilder.toString();
        } 
        return "ParityRebuilt";
      } 
      return "ParityPassed";
    } 
    return "Unknown";
  }
  
  static {
    CREATOR = new Parcelable.Creator<GpsNavigationMessage>() {
        public GpsNavigationMessage createFromParcel(Parcel param1Parcel) {
          GpsNavigationMessage gpsNavigationMessage = new GpsNavigationMessage();
          gpsNavigationMessage.setType(param1Parcel.readByte());
          gpsNavigationMessage.setPrn(param1Parcel.readByte());
          gpsNavigationMessage.setMessageId((short)param1Parcel.readInt());
          gpsNavigationMessage.setSubmessageId((short)param1Parcel.readInt());
          int i = param1Parcel.readInt();
          byte[] arrayOfByte = new byte[i];
          param1Parcel.readByteArray(arrayOfByte);
          gpsNavigationMessage.setData(arrayOfByte);
          if (param1Parcel.dataAvail() >= 32) {
            i = param1Parcel.readInt();
            gpsNavigationMessage.setStatus((short)i);
          } else {
            gpsNavigationMessage.setStatus((short)0);
          } 
          return gpsNavigationMessage;
        }
        
        public GpsNavigationMessage[] newArray(int param1Int) {
          return new GpsNavigationMessage[param1Int];
        }
      };
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByte(this.mType);
    paramParcel.writeByte(this.mPrn);
    paramParcel.writeInt(this.mMessageId);
    paramParcel.writeInt(this.mSubmessageId);
    paramParcel.writeInt(this.mData.length);
    paramParcel.writeByteArray(this.mData);
    paramParcel.writeInt(this.mStatus);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("GpsNavigationMessage:\n");
    byte b = 0;
    stringBuilder.append(String.format("   %-15s = %s\n", new Object[] { "Type", getTypeString() }));
    stringBuilder.append(String.format("   %-15s = %s\n", new Object[] { "Prn", Byte.valueOf(this.mPrn) }));
    stringBuilder.append(String.format("   %-15s = %s\n", new Object[] { "Status", getStatusString() }));
    stringBuilder.append(String.format("   %-15s = %s\n", new Object[] { "MessageId", Short.valueOf(this.mMessageId) }));
    stringBuilder.append(String.format("   %-15s = %s\n", new Object[] { "SubmessageId", Short.valueOf(this.mSubmessageId) }));
    stringBuilder.append(String.format("   %-15s = %s\n", new Object[] { "Data", "{" }));
    String str = "        ";
    byte[] arrayOfByte;
    int i;
    for (arrayOfByte = this.mData, i = arrayOfByte.length; b < i; ) {
      byte b1 = arrayOfByte[b];
      stringBuilder.append(str);
      stringBuilder.append(b1);
      str = ", ";
      b++;
    } 
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  private void initialize() {
    this.mType = 0;
    this.mPrn = 0;
    this.mMessageId = -1;
    this.mSubmessageId = -1;
    this.mData = EMPTY_ARRAY;
    this.mStatus = 0;
  }
}
