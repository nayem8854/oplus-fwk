package android.location;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IGpsGeofenceHardware extends IInterface {
  boolean addCircularHardwareGeofence(int paramInt1, double paramDouble1, double paramDouble2, double paramDouble3, int paramInt2, int paramInt3, int paramInt4, int paramInt5) throws RemoteException;
  
  boolean isHardwareGeofenceSupported() throws RemoteException;
  
  boolean pauseHardwareGeofence(int paramInt) throws RemoteException;
  
  boolean removeHardwareGeofence(int paramInt) throws RemoteException;
  
  boolean resumeHardwareGeofence(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements IGpsGeofenceHardware {
    public boolean isHardwareGeofenceSupported() throws RemoteException {
      return false;
    }
    
    public boolean addCircularHardwareGeofence(int param1Int1, double param1Double1, double param1Double2, double param1Double3, int param1Int2, int param1Int3, int param1Int4, int param1Int5) throws RemoteException {
      return false;
    }
    
    public boolean removeHardwareGeofence(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean pauseHardwareGeofence(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean resumeHardwareGeofence(int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IGpsGeofenceHardware {
    private static final String DESCRIPTOR = "android.location.IGpsGeofenceHardware";
    
    static final int TRANSACTION_addCircularHardwareGeofence = 2;
    
    static final int TRANSACTION_isHardwareGeofenceSupported = 1;
    
    static final int TRANSACTION_pauseHardwareGeofence = 4;
    
    static final int TRANSACTION_removeHardwareGeofence = 3;
    
    static final int TRANSACTION_resumeHardwareGeofence = 5;
    
    public Stub() {
      attachInterface(this, "android.location.IGpsGeofenceHardware");
    }
    
    public static IGpsGeofenceHardware asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.location.IGpsGeofenceHardware");
      if (iInterface != null && iInterface instanceof IGpsGeofenceHardware)
        return (IGpsGeofenceHardware)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "resumeHardwareGeofence";
            } 
            return "pauseHardwareGeofence";
          } 
          return "removeHardwareGeofence";
        } 
        return "addCircularHardwareGeofence";
      } 
      return "isHardwareGeofenceSupported";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.location.IGpsGeofenceHardware");
                return true;
              } 
              param1Parcel1.enforceInterface("android.location.IGpsGeofenceHardware");
              param1Int2 = param1Parcel1.readInt();
              param1Int1 = param1Parcel1.readInt();
              boolean bool4 = resumeHardwareGeofence(param1Int2, param1Int1);
              param1Parcel2.writeNoException();
              param1Parcel2.writeInt(bool4);
              return true;
            } 
            param1Parcel1.enforceInterface("android.location.IGpsGeofenceHardware");
            param1Int1 = param1Parcel1.readInt();
            boolean bool3 = pauseHardwareGeofence(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          } 
          param1Parcel1.enforceInterface("android.location.IGpsGeofenceHardware");
          param1Int1 = param1Parcel1.readInt();
          boolean bool2 = removeHardwareGeofence(param1Int1);
          param1Parcel2.writeNoException();
          param1Parcel2.writeInt(bool2);
          return true;
        } 
        param1Parcel1.enforceInterface("android.location.IGpsGeofenceHardware");
        int i = param1Parcel1.readInt();
        double d1 = param1Parcel1.readDouble();
        double d2 = param1Parcel1.readDouble();
        double d3 = param1Parcel1.readDouble();
        int j = param1Parcel1.readInt();
        int k = param1Parcel1.readInt();
        param1Int2 = param1Parcel1.readInt();
        param1Int1 = param1Parcel1.readInt();
        boolean bool1 = addCircularHardwareGeofence(i, d1, d2, d3, j, k, param1Int2, param1Int1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.location.IGpsGeofenceHardware");
      boolean bool = isHardwareGeofenceSupported();
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements IGpsGeofenceHardware {
      public static IGpsGeofenceHardware sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.location.IGpsGeofenceHardware";
      }
      
      public boolean isHardwareGeofenceSupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.IGpsGeofenceHardware");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IGpsGeofenceHardware.Stub.getDefaultImpl() != null) {
            bool1 = IGpsGeofenceHardware.Stub.getDefaultImpl().isHardwareGeofenceSupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean addCircularHardwareGeofence(int param2Int1, double param2Double1, double param2Double2, double param2Double3, int param2Int2, int param2Int3, int param2Int4, int param2Int5) throws RemoteException {
        Exception exception;
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.IGpsGeofenceHardware");
          try {
            parcel1.writeInt(param2Int1);
            parcel1.writeDouble(param2Double1);
            parcel1.writeDouble(param2Double2);
            parcel1.writeDouble(param2Double3);
            parcel1.writeInt(param2Int2);
            parcel1.writeInt(param2Int3);
            parcel1.writeInt(param2Int4);
            parcel1.writeInt(param2Int5);
            IBinder iBinder = this.mRemote;
            boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
            if (!bool2 && IGpsGeofenceHardware.Stub.getDefaultImpl() != null) {
              bool1 = IGpsGeofenceHardware.Stub.getDefaultImpl().addCircularHardwareGeofence(param2Int1, param2Double1, param2Double2, param2Double3, param2Int2, param2Int3, param2Int4, param2Int5);
              parcel2.recycle();
              parcel1.recycle();
              return bool1;
            } 
            parcel2.readException();
            param2Int1 = parcel2.readInt();
            if (param2Int1 != 0)
              bool1 = true; 
            parcel2.recycle();
            parcel1.recycle();
            return bool1;
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw exception;
      }
      
      public boolean removeHardwareGeofence(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.IGpsGeofenceHardware");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IGpsGeofenceHardware.Stub.getDefaultImpl() != null) {
            bool1 = IGpsGeofenceHardware.Stub.getDefaultImpl().removeHardwareGeofence(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean pauseHardwareGeofence(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.IGpsGeofenceHardware");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IGpsGeofenceHardware.Stub.getDefaultImpl() != null) {
            bool1 = IGpsGeofenceHardware.Stub.getDefaultImpl().pauseHardwareGeofence(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean resumeHardwareGeofence(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.IGpsGeofenceHardware");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IGpsGeofenceHardware.Stub.getDefaultImpl() != null) {
            bool1 = IGpsGeofenceHardware.Stub.getDefaultImpl().resumeHardwareGeofence(param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IGpsGeofenceHardware param1IGpsGeofenceHardware) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IGpsGeofenceHardware != null) {
          Proxy.sDefaultImpl = param1IGpsGeofenceHardware;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IGpsGeofenceHardware getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
