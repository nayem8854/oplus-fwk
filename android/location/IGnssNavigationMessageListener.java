package android.location;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IGnssNavigationMessageListener extends IInterface {
  void onGnssNavigationMessageReceived(GnssNavigationMessage paramGnssNavigationMessage) throws RemoteException;
  
  void onStatusChanged(int paramInt) throws RemoteException;
  
  class Default implements IGnssNavigationMessageListener {
    public void onGnssNavigationMessageReceived(GnssNavigationMessage param1GnssNavigationMessage) throws RemoteException {}
    
    public void onStatusChanged(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IGnssNavigationMessageListener {
    private static final String DESCRIPTOR = "android.location.IGnssNavigationMessageListener";
    
    static final int TRANSACTION_onGnssNavigationMessageReceived = 1;
    
    static final int TRANSACTION_onStatusChanged = 2;
    
    public Stub() {
      attachInterface(this, "android.location.IGnssNavigationMessageListener");
    }
    
    public static IGnssNavigationMessageListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.location.IGnssNavigationMessageListener");
      if (iInterface != null && iInterface instanceof IGnssNavigationMessageListener)
        return (IGnssNavigationMessageListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onStatusChanged";
      } 
      return "onGnssNavigationMessageReceived";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.location.IGnssNavigationMessageListener");
          return true;
        } 
        param1Parcel1.enforceInterface("android.location.IGnssNavigationMessageListener");
        param1Int1 = param1Parcel1.readInt();
        onStatusChanged(param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.location.IGnssNavigationMessageListener");
      if (param1Parcel1.readInt() != 0) {
        GnssNavigationMessage gnssNavigationMessage = GnssNavigationMessage.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onGnssNavigationMessageReceived((GnssNavigationMessage)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IGnssNavigationMessageListener {
      public static IGnssNavigationMessageListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.location.IGnssNavigationMessageListener";
      }
      
      public void onGnssNavigationMessageReceived(GnssNavigationMessage param2GnssNavigationMessage) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.location.IGnssNavigationMessageListener");
          if (param2GnssNavigationMessage != null) {
            parcel.writeInt(1);
            param2GnssNavigationMessage.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IGnssNavigationMessageListener.Stub.getDefaultImpl() != null) {
            IGnssNavigationMessageListener.Stub.getDefaultImpl().onGnssNavigationMessageReceived(param2GnssNavigationMessage);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onStatusChanged(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.location.IGnssNavigationMessageListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IGnssNavigationMessageListener.Stub.getDefaultImpl() != null) {
            IGnssNavigationMessageListener.Stub.getDefaultImpl().onStatusChanged(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IGnssNavigationMessageListener param1IGnssNavigationMessageListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IGnssNavigationMessageListener != null) {
          Proxy.sDefaultImpl = param1IGnssNavigationMessageListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IGnssNavigationMessageListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
