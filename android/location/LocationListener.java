package android.location;

import android.os.Bundle;

public interface LocationListener {
  @Deprecated
  default void onStatusChanged(String paramString, int paramInt, Bundle paramBundle) {}
  
  default void onProviderEnabled(String paramString) {}
  
  default void onProviderDisabled(String paramString) {}
  
  void onLocationChanged(Location paramLocation);
}
