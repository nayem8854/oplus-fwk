package android.location;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ILocationListener extends IInterface {
  void onLocationChanged(Location paramLocation) throws RemoteException;
  
  void onProviderDisabled(String paramString) throws RemoteException;
  
  void onProviderEnabled(String paramString) throws RemoteException;
  
  void onRemoved() throws RemoteException;
  
  class Default implements ILocationListener {
    public void onLocationChanged(Location param1Location) throws RemoteException {}
    
    public void onProviderEnabled(String param1String) throws RemoteException {}
    
    public void onProviderDisabled(String param1String) throws RemoteException {}
    
    public void onRemoved() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ILocationListener {
    private static final String DESCRIPTOR = "android.location.ILocationListener";
    
    static final int TRANSACTION_onLocationChanged = 1;
    
    static final int TRANSACTION_onProviderDisabled = 3;
    
    static final int TRANSACTION_onProviderEnabled = 2;
    
    static final int TRANSACTION_onRemoved = 4;
    
    public Stub() {
      attachInterface(this, "android.location.ILocationListener");
    }
    
    public static ILocationListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.location.ILocationListener");
      if (iInterface != null && iInterface instanceof ILocationListener)
        return (ILocationListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onRemoved";
          } 
          return "onProviderDisabled";
        } 
        return "onProviderEnabled";
      } 
      return "onLocationChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.location.ILocationListener");
              return true;
            } 
            param1Parcel1.enforceInterface("android.location.ILocationListener");
            onRemoved();
            return true;
          } 
          param1Parcel1.enforceInterface("android.location.ILocationListener");
          str = param1Parcel1.readString();
          onProviderDisabled(str);
          return true;
        } 
        str.enforceInterface("android.location.ILocationListener");
        str = str.readString();
        onProviderEnabled(str);
        return true;
      } 
      str.enforceInterface("android.location.ILocationListener");
      if (str.readInt() != 0) {
        Location location = Location.CREATOR.createFromParcel((Parcel)str);
      } else {
        str = null;
      } 
      onLocationChanged((Location)str);
      return true;
    }
    
    private static class Proxy implements ILocationListener {
      public static ILocationListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.location.ILocationListener";
      }
      
      public void onLocationChanged(Location param2Location) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.location.ILocationListener");
          if (param2Location != null) {
            parcel.writeInt(1);
            param2Location.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ILocationListener.Stub.getDefaultImpl() != null) {
            ILocationListener.Stub.getDefaultImpl().onLocationChanged(param2Location);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onProviderEnabled(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.location.ILocationListener");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ILocationListener.Stub.getDefaultImpl() != null) {
            ILocationListener.Stub.getDefaultImpl().onProviderEnabled(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onProviderDisabled(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.location.ILocationListener");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && ILocationListener.Stub.getDefaultImpl() != null) {
            ILocationListener.Stub.getDefaultImpl().onProviderDisabled(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRemoved() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.location.ILocationListener");
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && ILocationListener.Stub.getDefaultImpl() != null) {
            ILocationListener.Stub.getDefaultImpl().onRemoved();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ILocationListener param1ILocationListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ILocationListener != null) {
          Proxy.sDefaultImpl = param1ILocationListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ILocationListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
