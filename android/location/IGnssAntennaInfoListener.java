package android.location;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IGnssAntennaInfoListener extends IInterface {
  void onGnssAntennaInfoReceived(List<GnssAntennaInfo> paramList) throws RemoteException;
  
  class Default implements IGnssAntennaInfoListener {
    public void onGnssAntennaInfoReceived(List<GnssAntennaInfo> param1List) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IGnssAntennaInfoListener {
    private static final String DESCRIPTOR = "android.location.IGnssAntennaInfoListener";
    
    static final int TRANSACTION_onGnssAntennaInfoReceived = 1;
    
    public Stub() {
      attachInterface(this, "android.location.IGnssAntennaInfoListener");
    }
    
    public static IGnssAntennaInfoListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.location.IGnssAntennaInfoListener");
      if (iInterface != null && iInterface instanceof IGnssAntennaInfoListener)
        return (IGnssAntennaInfoListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onGnssAntennaInfoReceived";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.location.IGnssAntennaInfoListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.location.IGnssAntennaInfoListener");
      ArrayList<GnssAntennaInfo> arrayList = param1Parcel1.createTypedArrayList(GnssAntennaInfo.CREATOR);
      onGnssAntennaInfoReceived(arrayList);
      return true;
    }
    
    private static class Proxy implements IGnssAntennaInfoListener {
      public static IGnssAntennaInfoListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.location.IGnssAntennaInfoListener";
      }
      
      public void onGnssAntennaInfoReceived(List<GnssAntennaInfo> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.location.IGnssAntennaInfoListener");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IGnssAntennaInfoListener.Stub.getDefaultImpl() != null) {
            IGnssAntennaInfoListener.Stub.getDefaultImpl().onGnssAntennaInfoReceived(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IGnssAntennaInfoListener param1IGnssAntennaInfoListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IGnssAntennaInfoListener != null) {
          Proxy.sDefaultImpl = param1IGnssAntennaInfoListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IGnssAntennaInfoListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
