package android.location;

import android.app.PendingIntent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ICancellationSignal;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.android.internal.location.ProviderProperties;
import java.util.ArrayList;
import java.util.List;

public interface ILocationManager extends IInterface {
  boolean addGnssAntennaInfoListener(IGnssAntennaInfoListener paramIGnssAntennaInfoListener, String paramString1, String paramString2) throws RemoteException;
  
  boolean addGnssBatchingCallback(IBatchedLocationCallback paramIBatchedLocationCallback, String paramString1, String paramString2) throws RemoteException;
  
  boolean addGnssMeasurementsListener(GnssRequest paramGnssRequest, IGnssMeasurementsListener paramIGnssMeasurementsListener, String paramString1, String paramString2) throws RemoteException;
  
  boolean addGnssNavigationMessageListener(IGnssNavigationMessageListener paramIGnssNavigationMessageListener, String paramString1, String paramString2) throws RemoteException;
  
  void addTestProvider(String paramString1, ProviderProperties paramProviderProperties, String paramString2, String paramString3) throws RemoteException;
  
  void flushGnssBatch(String paramString) throws RemoteException;
  
  boolean geocoderIsPresent() throws RemoteException;
  
  List<String> getAllProviders() throws RemoteException;
  
  String[] getBackgroundThrottlingWhitelist() throws RemoteException;
  
  String getBestProvider(Criteria paramCriteria, boolean paramBoolean) throws RemoteException;
  
  boolean getCurrentLocation(LocationRequest paramLocationRequest, ICancellationSignal paramICancellationSignal, ILocationListener paramILocationListener, String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  String getExtraLocationControllerPackage() throws RemoteException;
  
  String getFromLocation(double paramDouble1, double paramDouble2, int paramInt, GeocoderParams paramGeocoderParams, List<Address> paramList) throws RemoteException;
  
  String getFromLocationName(String paramString, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, int paramInt, GeocoderParams paramGeocoderParams, List<Address> paramList) throws RemoteException;
  
  int getGnssBatchSize(String paramString) throws RemoteException;
  
  long getGnssCapabilities() throws RemoteException;
  
  String getGnssHardwareModelName() throws RemoteException;
  
  LocationTime getGnssTimeMillis() throws RemoteException;
  
  int getGnssYearOfHardware() throws RemoteException;
  
  String[] getIgnoreSettingsWhitelist() throws RemoteException;
  
  Location getLastLocation(LocationRequest paramLocationRequest, String paramString1, String paramString2) throws RemoteException;
  
  List<String> getProviderPackages(String paramString) throws RemoteException;
  
  ProviderProperties getProviderProperties(String paramString) throws RemoteException;
  
  List<String> getProviders(Criteria paramCriteria, boolean paramBoolean) throws RemoteException;
  
  List<LocationRequest> getTestProviderCurrentRequests(String paramString) throws RemoteException;
  
  void injectGnssMeasurementCorrections(GnssMeasurementCorrections paramGnssMeasurementCorrections, String paramString) throws RemoteException;
  
  void injectLocation(Location paramLocation) throws RemoteException;
  
  boolean isExtraLocationControllerPackageEnabled() throws RemoteException;
  
  boolean isLocationEnabledForUser(int paramInt) throws RemoteException;
  
  boolean isProviderEnabledForUser(String paramString, int paramInt) throws RemoteException;
  
  boolean isProviderPackage(String paramString) throws RemoteException;
  
  void locationCallbackFinished(ILocationListener paramILocationListener) throws RemoteException;
  
  boolean registerGnssStatusCallback(IGnssStatusListener paramIGnssStatusListener, String paramString1, String paramString2) throws RemoteException;
  
  void removeGeofence(Geofence paramGeofence, PendingIntent paramPendingIntent, String paramString) throws RemoteException;
  
  void removeGnssAntennaInfoListener(IGnssAntennaInfoListener paramIGnssAntennaInfoListener) throws RemoteException;
  
  void removeGnssBatchingCallback() throws RemoteException;
  
  void removeGnssMeasurementsListener(IGnssMeasurementsListener paramIGnssMeasurementsListener) throws RemoteException;
  
  void removeGnssNavigationMessageListener(IGnssNavigationMessageListener paramIGnssNavigationMessageListener) throws RemoteException;
  
  void removeTestProvider(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  void removeUpdates(ILocationListener paramILocationListener, PendingIntent paramPendingIntent) throws RemoteException;
  
  void requestGeofence(LocationRequest paramLocationRequest, Geofence paramGeofence, PendingIntent paramPendingIntent, String paramString1, String paramString2) throws RemoteException;
  
  void requestLocationUpdates(LocationRequest paramLocationRequest, ILocationListener paramILocationListener, PendingIntent paramPendingIntent, String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  boolean sendExtraCommand(String paramString1, String paramString2, Bundle paramBundle) throws RemoteException;
  
  void setExtraLocationControllerPackage(String paramString) throws RemoteException;
  
  void setExtraLocationControllerPackageEnabled(boolean paramBoolean) throws RemoteException;
  
  void setLocationEnabledForUser(boolean paramBoolean, int paramInt) throws RemoteException;
  
  void setTestProviderEnabled(String paramString1, boolean paramBoolean, String paramString2, String paramString3) throws RemoteException;
  
  void setTestProviderLocation(String paramString1, Location paramLocation, String paramString2, String paramString3) throws RemoteException;
  
  boolean startGnssBatch(long paramLong, boolean paramBoolean, String paramString1, String paramString2) throws RemoteException;
  
  boolean stopGnssBatch() throws RemoteException;
  
  void unregisterGnssStatusCallback(IGnssStatusListener paramIGnssStatusListener) throws RemoteException;
  
  class Default implements ILocationManager {
    public Location getLastLocation(LocationRequest param1LocationRequest, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public boolean getCurrentLocation(LocationRequest param1LocationRequest, ICancellationSignal param1ICancellationSignal, ILocationListener param1ILocationListener, String param1String1, String param1String2, String param1String3) throws RemoteException {
      return false;
    }
    
    public void requestLocationUpdates(LocationRequest param1LocationRequest, ILocationListener param1ILocationListener, PendingIntent param1PendingIntent, String param1String1, String param1String2, String param1String3) throws RemoteException {}
    
    public void removeUpdates(ILocationListener param1ILocationListener, PendingIntent param1PendingIntent) throws RemoteException {}
    
    public void requestGeofence(LocationRequest param1LocationRequest, Geofence param1Geofence, PendingIntent param1PendingIntent, String param1String1, String param1String2) throws RemoteException {}
    
    public void removeGeofence(Geofence param1Geofence, PendingIntent param1PendingIntent, String param1String) throws RemoteException {}
    
    public boolean registerGnssStatusCallback(IGnssStatusListener param1IGnssStatusListener, String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public void unregisterGnssStatusCallback(IGnssStatusListener param1IGnssStatusListener) throws RemoteException {}
    
    public boolean geocoderIsPresent() throws RemoteException {
      return false;
    }
    
    public String getFromLocation(double param1Double1, double param1Double2, int param1Int, GeocoderParams param1GeocoderParams, List<Address> param1List) throws RemoteException {
      return null;
    }
    
    public String getFromLocationName(String param1String, double param1Double1, double param1Double2, double param1Double3, double param1Double4, int param1Int, GeocoderParams param1GeocoderParams, List<Address> param1List) throws RemoteException {
      return null;
    }
    
    public boolean addGnssMeasurementsListener(GnssRequest param1GnssRequest, IGnssMeasurementsListener param1IGnssMeasurementsListener, String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public void injectGnssMeasurementCorrections(GnssMeasurementCorrections param1GnssMeasurementCorrections, String param1String) throws RemoteException {}
    
    public long getGnssCapabilities() throws RemoteException {
      return 0L;
    }
    
    public void removeGnssMeasurementsListener(IGnssMeasurementsListener param1IGnssMeasurementsListener) throws RemoteException {}
    
    public boolean addGnssAntennaInfoListener(IGnssAntennaInfoListener param1IGnssAntennaInfoListener, String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public void removeGnssAntennaInfoListener(IGnssAntennaInfoListener param1IGnssAntennaInfoListener) throws RemoteException {}
    
    public boolean addGnssNavigationMessageListener(IGnssNavigationMessageListener param1IGnssNavigationMessageListener, String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public void removeGnssNavigationMessageListener(IGnssNavigationMessageListener param1IGnssNavigationMessageListener) throws RemoteException {}
    
    public int getGnssYearOfHardware() throws RemoteException {
      return 0;
    }
    
    public String getGnssHardwareModelName() throws RemoteException {
      return null;
    }
    
    public int getGnssBatchSize(String param1String) throws RemoteException {
      return 0;
    }
    
    public boolean addGnssBatchingCallback(IBatchedLocationCallback param1IBatchedLocationCallback, String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public void removeGnssBatchingCallback() throws RemoteException {}
    
    public boolean startGnssBatch(long param1Long, boolean param1Boolean, String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public void flushGnssBatch(String param1String) throws RemoteException {}
    
    public boolean stopGnssBatch() throws RemoteException {
      return false;
    }
    
    public void injectLocation(Location param1Location) throws RemoteException {}
    
    public List<String> getAllProviders() throws RemoteException {
      return null;
    }
    
    public List<String> getProviders(Criteria param1Criteria, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public String getBestProvider(Criteria param1Criteria, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public ProviderProperties getProviderProperties(String param1String) throws RemoteException {
      return null;
    }
    
    public boolean isProviderPackage(String param1String) throws RemoteException {
      return false;
    }
    
    public List<String> getProviderPackages(String param1String) throws RemoteException {
      return null;
    }
    
    public void setExtraLocationControllerPackage(String param1String) throws RemoteException {}
    
    public String getExtraLocationControllerPackage() throws RemoteException {
      return null;
    }
    
    public void setExtraLocationControllerPackageEnabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isExtraLocationControllerPackageEnabled() throws RemoteException {
      return false;
    }
    
    public boolean isProviderEnabledForUser(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isLocationEnabledForUser(int param1Int) throws RemoteException {
      return false;
    }
    
    public void setLocationEnabledForUser(boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public void addTestProvider(String param1String1, ProviderProperties param1ProviderProperties, String param1String2, String param1String3) throws RemoteException {}
    
    public void removeTestProvider(String param1String1, String param1String2, String param1String3) throws RemoteException {}
    
    public void setTestProviderLocation(String param1String1, Location param1Location, String param1String2, String param1String3) throws RemoteException {}
    
    public void setTestProviderEnabled(String param1String1, boolean param1Boolean, String param1String2, String param1String3) throws RemoteException {}
    
    public List<LocationRequest> getTestProviderCurrentRequests(String param1String) throws RemoteException {
      return null;
    }
    
    public LocationTime getGnssTimeMillis() throws RemoteException {
      return null;
    }
    
    public boolean sendExtraCommand(String param1String1, String param1String2, Bundle param1Bundle) throws RemoteException {
      return false;
    }
    
    public void locationCallbackFinished(ILocationListener param1ILocationListener) throws RemoteException {}
    
    public String[] getBackgroundThrottlingWhitelist() throws RemoteException {
      return null;
    }
    
    public String[] getIgnoreSettingsWhitelist() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ILocationManager {
    private static final String DESCRIPTOR = "android.location.ILocationManager";
    
    static final int TRANSACTION_addGnssAntennaInfoListener = 16;
    
    static final int TRANSACTION_addGnssBatchingCallback = 23;
    
    static final int TRANSACTION_addGnssMeasurementsListener = 12;
    
    static final int TRANSACTION_addGnssNavigationMessageListener = 18;
    
    static final int TRANSACTION_addTestProvider = 42;
    
    static final int TRANSACTION_flushGnssBatch = 26;
    
    static final int TRANSACTION_geocoderIsPresent = 9;
    
    static final int TRANSACTION_getAllProviders = 29;
    
    static final int TRANSACTION_getBackgroundThrottlingWhitelist = 50;
    
    static final int TRANSACTION_getBestProvider = 31;
    
    static final int TRANSACTION_getCurrentLocation = 2;
    
    static final int TRANSACTION_getExtraLocationControllerPackage = 36;
    
    static final int TRANSACTION_getFromLocation = 10;
    
    static final int TRANSACTION_getFromLocationName = 11;
    
    static final int TRANSACTION_getGnssBatchSize = 22;
    
    static final int TRANSACTION_getGnssCapabilities = 14;
    
    static final int TRANSACTION_getGnssHardwareModelName = 21;
    
    static final int TRANSACTION_getGnssTimeMillis = 47;
    
    static final int TRANSACTION_getGnssYearOfHardware = 20;
    
    static final int TRANSACTION_getIgnoreSettingsWhitelist = 51;
    
    static final int TRANSACTION_getLastLocation = 1;
    
    static final int TRANSACTION_getProviderPackages = 34;
    
    static final int TRANSACTION_getProviderProperties = 32;
    
    static final int TRANSACTION_getProviders = 30;
    
    static final int TRANSACTION_getTestProviderCurrentRequests = 46;
    
    static final int TRANSACTION_injectGnssMeasurementCorrections = 13;
    
    static final int TRANSACTION_injectLocation = 28;
    
    static final int TRANSACTION_isExtraLocationControllerPackageEnabled = 38;
    
    static final int TRANSACTION_isLocationEnabledForUser = 40;
    
    static final int TRANSACTION_isProviderEnabledForUser = 39;
    
    static final int TRANSACTION_isProviderPackage = 33;
    
    static final int TRANSACTION_locationCallbackFinished = 49;
    
    static final int TRANSACTION_registerGnssStatusCallback = 7;
    
    static final int TRANSACTION_removeGeofence = 6;
    
    static final int TRANSACTION_removeGnssAntennaInfoListener = 17;
    
    static final int TRANSACTION_removeGnssBatchingCallback = 24;
    
    static final int TRANSACTION_removeGnssMeasurementsListener = 15;
    
    static final int TRANSACTION_removeGnssNavigationMessageListener = 19;
    
    static final int TRANSACTION_removeTestProvider = 43;
    
    static final int TRANSACTION_removeUpdates = 4;
    
    static final int TRANSACTION_requestGeofence = 5;
    
    static final int TRANSACTION_requestLocationUpdates = 3;
    
    static final int TRANSACTION_sendExtraCommand = 48;
    
    static final int TRANSACTION_setExtraLocationControllerPackage = 35;
    
    static final int TRANSACTION_setExtraLocationControllerPackageEnabled = 37;
    
    static final int TRANSACTION_setLocationEnabledForUser = 41;
    
    static final int TRANSACTION_setTestProviderEnabled = 45;
    
    static final int TRANSACTION_setTestProviderLocation = 44;
    
    static final int TRANSACTION_startGnssBatch = 25;
    
    static final int TRANSACTION_stopGnssBatch = 27;
    
    static final int TRANSACTION_unregisterGnssStatusCallback = 8;
    
    public Stub() {
      attachInterface(this, "android.location.ILocationManager");
    }
    
    public static ILocationManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.location.ILocationManager");
      if (iInterface != null && iInterface instanceof ILocationManager)
        return (ILocationManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 51:
          return "getIgnoreSettingsWhitelist";
        case 50:
          return "getBackgroundThrottlingWhitelist";
        case 49:
          return "locationCallbackFinished";
        case 48:
          return "sendExtraCommand";
        case 47:
          return "getGnssTimeMillis";
        case 46:
          return "getTestProviderCurrentRequests";
        case 45:
          return "setTestProviderEnabled";
        case 44:
          return "setTestProviderLocation";
        case 43:
          return "removeTestProvider";
        case 42:
          return "addTestProvider";
        case 41:
          return "setLocationEnabledForUser";
        case 40:
          return "isLocationEnabledForUser";
        case 39:
          return "isProviderEnabledForUser";
        case 38:
          return "isExtraLocationControllerPackageEnabled";
        case 37:
          return "setExtraLocationControllerPackageEnabled";
        case 36:
          return "getExtraLocationControllerPackage";
        case 35:
          return "setExtraLocationControllerPackage";
        case 34:
          return "getProviderPackages";
        case 33:
          return "isProviderPackage";
        case 32:
          return "getProviderProperties";
        case 31:
          return "getBestProvider";
        case 30:
          return "getProviders";
        case 29:
          return "getAllProviders";
        case 28:
          return "injectLocation";
        case 27:
          return "stopGnssBatch";
        case 26:
          return "flushGnssBatch";
        case 25:
          return "startGnssBatch";
        case 24:
          return "removeGnssBatchingCallback";
        case 23:
          return "addGnssBatchingCallback";
        case 22:
          return "getGnssBatchSize";
        case 21:
          return "getGnssHardwareModelName";
        case 20:
          return "getGnssYearOfHardware";
        case 19:
          return "removeGnssNavigationMessageListener";
        case 18:
          return "addGnssNavigationMessageListener";
        case 17:
          return "removeGnssAntennaInfoListener";
        case 16:
          return "addGnssAntennaInfoListener";
        case 15:
          return "removeGnssMeasurementsListener";
        case 14:
          return "getGnssCapabilities";
        case 13:
          return "injectGnssMeasurementCorrections";
        case 12:
          return "addGnssMeasurementsListener";
        case 11:
          return "getFromLocationName";
        case 10:
          return "getFromLocation";
        case 9:
          return "geocoderIsPresent";
        case 8:
          return "unregisterGnssStatusCallback";
        case 7:
          return "registerGnssStatusCallback";
        case 6:
          return "removeGeofence";
        case 5:
          return "requestGeofence";
        case 4:
          return "removeUpdates";
        case 3:
          return "requestLocationUpdates";
        case 2:
          return "getCurrentLocation";
        case 1:
          break;
      } 
      return "getLastLocation";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool5;
        int m;
        boolean bool4;
        int k;
        boolean bool3;
        int j;
        boolean bool2;
        int i;
        boolean bool1;
        String[] arrayOfString;
        ILocationListener iLocationListener1;
        LocationTime locationTime;
        String str9;
        List<LocationRequest> list;
        String str8;
        List<String> list2;
        String str7;
        ProviderProperties providerProperties;
        String str6;
        List<String> list1;
        String str5;
        IGnssNavigationMessageListener iGnssNavigationMessageListener1;
        String str4;
        IGnssAntennaInfoListener iGnssAntennaInfoListener1;
        String str3;
        IGnssMeasurementsListener iGnssMeasurementsListener1;
        String str2;
        IGnssStatusListener iGnssStatusListener1;
        String str12;
        IBatchedLocationCallback iBatchedLocationCallback;
        String str11;
        ArrayList<Address> arrayList1;
        String str10;
        ILocationListener iLocationListener2;
        String str15;
        IGnssNavigationMessageListener iGnssNavigationMessageListener2;
        IGnssAntennaInfoListener iGnssAntennaInfoListener2;
        String str14;
        ArrayList<Address> arrayList2;
        IGnssStatusListener iGnssStatusListener2;
        String str16;
        IGnssMeasurementsListener iGnssMeasurementsListener2;
        ILocationListener iLocationListener3;
        ICancellationSignal iCancellationSignal;
        long l;
        double d1, d2, d3, d4;
        String str17, str18;
        ILocationListener iLocationListener4;
        boolean bool6 = false, bool7 = false, bool8 = false, bool9 = false, bool10 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 51:
            param1Parcel1.enforceInterface("android.location.ILocationManager");
            arrayOfString = getIgnoreSettingsWhitelist();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString);
            return true;
          case 50:
            arrayOfString.enforceInterface("android.location.ILocationManager");
            arrayOfString = getBackgroundThrottlingWhitelist();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString);
            return true;
          case 49:
            arrayOfString.enforceInterface("android.location.ILocationManager");
            iLocationListener1 = ILocationListener.Stub.asInterface(arrayOfString.readStrongBinder());
            locationCallbackFinished(iLocationListener1);
            param1Parcel2.writeNoException();
            return true;
          case 48:
            iLocationListener1.enforceInterface("android.location.ILocationManager");
            str12 = iLocationListener1.readString();
            str15 = iLocationListener1.readString();
            if (iLocationListener1.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)iLocationListener1);
            } else {
              iLocationListener1 = null;
            } 
            bool5 = sendExtraCommand(str12, str15, (Bundle)iLocationListener1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            if (iLocationListener1 != null) {
              param1Parcel2.writeInt(1);
              iLocationListener1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 47:
            iLocationListener1.enforceInterface("android.location.ILocationManager");
            locationTime = getGnssTimeMillis();
            param1Parcel2.writeNoException();
            if (locationTime != null) {
              param1Parcel2.writeInt(1);
              locationTime.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 46:
            locationTime.enforceInterface("android.location.ILocationManager");
            str9 = locationTime.readString();
            list = getTestProviderCurrentRequests(str9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 45:
            list.enforceInterface("android.location.ILocationManager");
            str12 = list.readString();
            bool6 = bool10;
            if (list.readInt() != 0)
              bool6 = true; 
            str15 = list.readString();
            str8 = list.readString();
            setTestProviderEnabled(str12, bool6, str15, str8);
            param1Parcel2.writeNoException();
            return true;
          case 44:
            str8.enforceInterface("android.location.ILocationManager");
            str15 = str8.readString();
            if (str8.readInt() != 0) {
              Location location1 = Location.CREATOR.createFromParcel((Parcel)str8);
            } else {
              str12 = null;
            } 
            str16 = str8.readString();
            str8 = str8.readString();
            setTestProviderLocation(str15, (Location)str12, str16, str8);
            param1Parcel2.writeNoException();
            return true;
          case 43:
            str8.enforceInterface("android.location.ILocationManager");
            str12 = str8.readString();
            str15 = str8.readString();
            str8 = str8.readString();
            removeTestProvider(str12, str15, str8);
            param1Parcel2.writeNoException();
            return true;
          case 42:
            str8.enforceInterface("android.location.ILocationManager");
            str15 = str8.readString();
            if (str8.readInt() != 0) {
              ProviderProperties providerProperties1 = ProviderProperties.CREATOR.createFromParcel((Parcel)str8);
            } else {
              str12 = null;
            } 
            str16 = str8.readString();
            str8 = str8.readString();
            addTestProvider(str15, (ProviderProperties)str12, str16, str8);
            param1Parcel2.writeNoException();
            return true;
          case 41:
            str8.enforceInterface("android.location.ILocationManager");
            if (str8.readInt() != 0)
              bool6 = true; 
            m = str8.readInt();
            setLocationEnabledForUser(bool6, m);
            param1Parcel2.writeNoException();
            return true;
          case 40:
            str8.enforceInterface("android.location.ILocationManager");
            m = str8.readInt();
            bool4 = isLocationEnabledForUser(m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 39:
            str8.enforceInterface("android.location.ILocationManager");
            str12 = str8.readString();
            k = str8.readInt();
            bool3 = isProviderEnabledForUser(str12, k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 38:
            str8.enforceInterface("android.location.ILocationManager");
            bool3 = isExtraLocationControllerPackageEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 37:
            str8.enforceInterface("android.location.ILocationManager");
            bool6 = bool7;
            if (str8.readInt() != 0)
              bool6 = true; 
            setExtraLocationControllerPackageEnabled(bool6);
            param1Parcel2.writeNoException();
            return true;
          case 36:
            str8.enforceInterface("android.location.ILocationManager");
            str8 = getExtraLocationControllerPackage();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str8);
            return true;
          case 35:
            str8.enforceInterface("android.location.ILocationManager");
            str8 = str8.readString();
            setExtraLocationControllerPackage(str8);
            param1Parcel2.writeNoException();
            return true;
          case 34:
            str8.enforceInterface("android.location.ILocationManager");
            str8 = str8.readString();
            list2 = getProviderPackages(str8);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list2);
            return true;
          case 33:
            list2.enforceInterface("android.location.ILocationManager");
            str7 = list2.readString();
            bool3 = isProviderPackage(str7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 32:
            str7.enforceInterface("android.location.ILocationManager");
            str7 = str7.readString();
            providerProperties = getProviderProperties(str7);
            param1Parcel2.writeNoException();
            if (providerProperties != null) {
              param1Parcel2.writeInt(1);
              providerProperties.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 31:
            providerProperties.enforceInterface("android.location.ILocationManager");
            if (providerProperties.readInt() != 0) {
              Criteria criteria = Criteria.CREATOR.createFromParcel((Parcel)providerProperties);
            } else {
              str12 = null;
            } 
            bool6 = bool8;
            if (providerProperties.readInt() != 0)
              bool6 = true; 
            str6 = getBestProvider((Criteria)str12, bool6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str6);
            return true;
          case 30:
            str6.enforceInterface("android.location.ILocationManager");
            if (str6.readInt() != 0) {
              Criteria criteria = Criteria.CREATOR.createFromParcel((Parcel)str6);
            } else {
              str12 = null;
            } 
            bool6 = bool9;
            if (str6.readInt() != 0)
              bool6 = true; 
            list1 = getProviders((Criteria)str12, bool6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 29:
            list1.enforceInterface("android.location.ILocationManager");
            list1 = getAllProviders();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 28:
            list1.enforceInterface("android.location.ILocationManager");
            if (list1.readInt() != 0) {
              Location location1 = Location.CREATOR.createFromParcel((Parcel)list1);
            } else {
              list1 = null;
            } 
            injectLocation((Location)list1);
            param1Parcel2.writeNoException();
            return true;
          case 27:
            list1.enforceInterface("android.location.ILocationManager");
            bool3 = stopGnssBatch();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 26:
            list1.enforceInterface("android.location.ILocationManager");
            str5 = list1.readString();
            flushGnssBatch(str5);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            str5.enforceInterface("android.location.ILocationManager");
            l = str5.readLong();
            if (str5.readInt() != 0) {
              bool6 = true;
            } else {
              bool6 = false;
            } 
            str12 = str5.readString();
            str5 = str5.readString();
            bool3 = startGnssBatch(l, bool6, str12, str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 24:
            str5.enforceInterface("android.location.ILocationManager");
            removeGnssBatchingCallback();
            param1Parcel2.writeNoException();
            return true;
          case 23:
            str5.enforceInterface("android.location.ILocationManager");
            iBatchedLocationCallback = IBatchedLocationCallback.Stub.asInterface(str5.readStrongBinder());
            str15 = str5.readString();
            str5 = str5.readString();
            bool3 = addGnssBatchingCallback(iBatchedLocationCallback, str15, str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 22:
            str5.enforceInterface("android.location.ILocationManager");
            str5 = str5.readString();
            j = getGnssBatchSize(str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 21:
            str5.enforceInterface("android.location.ILocationManager");
            str5 = getGnssHardwareModelName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str5);
            return true;
          case 20:
            str5.enforceInterface("android.location.ILocationManager");
            j = getGnssYearOfHardware();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 19:
            str5.enforceInterface("android.location.ILocationManager");
            iGnssNavigationMessageListener1 = IGnssNavigationMessageListener.Stub.asInterface(str5.readStrongBinder());
            removeGnssNavigationMessageListener(iGnssNavigationMessageListener1);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            iGnssNavigationMessageListener1.enforceInterface("android.location.ILocationManager");
            iGnssNavigationMessageListener2 = IGnssNavigationMessageListener.Stub.asInterface(iGnssNavigationMessageListener1.readStrongBinder());
            str11 = iGnssNavigationMessageListener1.readString();
            str4 = iGnssNavigationMessageListener1.readString();
            bool2 = addGnssNavigationMessageListener(iGnssNavigationMessageListener2, str11, str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 17:
            str4.enforceInterface("android.location.ILocationManager");
            iGnssAntennaInfoListener1 = IGnssAntennaInfoListener.Stub.asInterface(str4.readStrongBinder());
            removeGnssAntennaInfoListener(iGnssAntennaInfoListener1);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            iGnssAntennaInfoListener1.enforceInterface("android.location.ILocationManager");
            iGnssAntennaInfoListener2 = IGnssAntennaInfoListener.Stub.asInterface(iGnssAntennaInfoListener1.readStrongBinder());
            str11 = iGnssAntennaInfoListener1.readString();
            str3 = iGnssAntennaInfoListener1.readString();
            bool2 = addGnssAntennaInfoListener(iGnssAntennaInfoListener2, str11, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 15:
            str3.enforceInterface("android.location.ILocationManager");
            iGnssMeasurementsListener1 = IGnssMeasurementsListener.Stub.asInterface(str3.readStrongBinder());
            removeGnssMeasurementsListener(iGnssMeasurementsListener1);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            iGnssMeasurementsListener1.enforceInterface("android.location.ILocationManager");
            l = getGnssCapabilities();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 13:
            iGnssMeasurementsListener1.enforceInterface("android.location.ILocationManager");
            if (iGnssMeasurementsListener1.readInt() != 0) {
              GnssMeasurementCorrections gnssMeasurementCorrections = GnssMeasurementCorrections.CREATOR.createFromParcel((Parcel)iGnssMeasurementsListener1);
            } else {
              str11 = null;
            } 
            str2 = iGnssMeasurementsListener1.readString();
            injectGnssMeasurementCorrections((GnssMeasurementCorrections)str11, str2);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            str2.enforceInterface("android.location.ILocationManager");
            if (str2.readInt() != 0) {
              GnssRequest gnssRequest = GnssRequest.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str11 = null;
            } 
            iGnssMeasurementsListener2 = IGnssMeasurementsListener.Stub.asInterface(str2.readStrongBinder());
            str14 = str2.readString();
            str2 = str2.readString();
            bool2 = addGnssMeasurementsListener((GnssRequest)str11, iGnssMeasurementsListener2, str14, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 11:
            str2.enforceInterface("android.location.ILocationManager");
            str11 = str2.readString();
            d1 = str2.readDouble();
            d2 = str2.readDouble();
            d3 = str2.readDouble();
            d4 = str2.readDouble();
            i = str2.readInt();
            if (str2.readInt() != 0) {
              GeocoderParams geocoderParams = GeocoderParams.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            arrayList2 = new ArrayList();
            str2 = getFromLocationName(str11, d1, d2, d3, d4, i, (GeocoderParams)str2, arrayList2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            param1Parcel2.writeTypedList(arrayList2);
            return true;
          case 10:
            str2.enforceInterface("android.location.ILocationManager");
            d3 = str2.readDouble();
            d4 = str2.readDouble();
            i = str2.readInt();
            if (str2.readInt() != 0) {
              GeocoderParams geocoderParams = GeocoderParams.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            arrayList1 = new ArrayList();
            str2 = getFromLocation(d3, d4, i, (GeocoderParams)str2, arrayList1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            param1Parcel2.writeTypedList(arrayList1);
            return true;
          case 9:
            str2.enforceInterface("android.location.ILocationManager");
            bool1 = geocoderIsPresent();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 8:
            str2.enforceInterface("android.location.ILocationManager");
            iGnssStatusListener1 = IGnssStatusListener.Stub.asInterface(str2.readStrongBinder());
            unregisterGnssStatusCallback(iGnssStatusListener1);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            iGnssStatusListener1.enforceInterface("android.location.ILocationManager");
            iGnssStatusListener2 = IGnssStatusListener.Stub.asInterface(iGnssStatusListener1.readStrongBinder());
            str10 = iGnssStatusListener1.readString();
            str1 = iGnssStatusListener1.readString();
            bool1 = registerGnssStatusCallback(iGnssStatusListener2, str10, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 6:
            str1.enforceInterface("android.location.ILocationManager");
            if (str1.readInt() != 0) {
              Geofence geofence = Geofence.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str10 = null;
            } 
            if (str1.readInt() != 0) {
              PendingIntent pendingIntent = PendingIntent.CREATOR.createFromParcel((Parcel)str1);
            } else {
              iGnssStatusListener2 = null;
            } 
            str1 = str1.readString();
            removeGeofence((Geofence)str10, (PendingIntent)iGnssStatusListener2, str1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            str1.enforceInterface("android.location.ILocationManager");
            if (str1.readInt() != 0) {
              LocationRequest locationRequest = LocationRequest.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str10 = null;
            } 
            if (str1.readInt() != 0) {
              Geofence geofence = Geofence.CREATOR.createFromParcel((Parcel)str1);
            } else {
              iGnssStatusListener2 = null;
            } 
            if (str1.readInt() != 0) {
              PendingIntent pendingIntent = PendingIntent.CREATOR.createFromParcel((Parcel)str1);
            } else {
              iGnssMeasurementsListener2 = null;
            } 
            str17 = str1.readString();
            str1 = str1.readString();
            requestGeofence((LocationRequest)str10, (Geofence)iGnssStatusListener2, (PendingIntent)iGnssMeasurementsListener2, str17, str1);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str1.enforceInterface("android.location.ILocationManager");
            iLocationListener2 = ILocationListener.Stub.asInterface(str1.readStrongBinder());
            if (str1.readInt() != 0) {
              PendingIntent pendingIntent = PendingIntent.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            removeUpdates(iLocationListener2, (PendingIntent)str1);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            str1.enforceInterface("android.location.ILocationManager");
            if (str1.readInt() != 0) {
              LocationRequest locationRequest = LocationRequest.CREATOR.createFromParcel((Parcel)str1);
            } else {
              iLocationListener2 = null;
            } 
            iLocationListener3 = ILocationListener.Stub.asInterface(str1.readStrongBinder());
            if (str1.readInt() != 0) {
              PendingIntent pendingIntent = PendingIntent.CREATOR.createFromParcel((Parcel)str1);
            } else {
              iGnssStatusListener2 = null;
            } 
            str17 = str1.readString();
            str18 = str1.readString();
            str1 = str1.readString();
            requestLocationUpdates((LocationRequest)iLocationListener2, iLocationListener3, (PendingIntent)iGnssStatusListener2, str17, str18, str1);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str1.enforceInterface("android.location.ILocationManager");
            if (str1.readInt() != 0) {
              LocationRequest locationRequest = LocationRequest.CREATOR.createFromParcel((Parcel)str1);
            } else {
              iLocationListener2 = null;
            } 
            iCancellationSignal = ICancellationSignal.Stub.asInterface(str1.readStrongBinder());
            iLocationListener4 = ILocationListener.Stub.asInterface(str1.readStrongBinder());
            str13 = str1.readString();
            str17 = str1.readString();
            str1 = str1.readString();
            bool1 = getCurrentLocation((LocationRequest)iLocationListener2, iCancellationSignal, iLocationListener4, str13, str17, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.location.ILocationManager");
        if (str1.readInt() != 0) {
          LocationRequest locationRequest = LocationRequest.CREATOR.createFromParcel((Parcel)str1);
        } else {
          iLocationListener2 = null;
        } 
        String str13 = str1.readString();
        String str1 = str1.readString();
        Location location = getLastLocation((LocationRequest)iLocationListener2, str13, str1);
        param1Parcel2.writeNoException();
        if (location != null) {
          param1Parcel2.writeInt(1);
          location.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      param1Parcel2.writeString("android.location.ILocationManager");
      return true;
    }
    
    private static class Proxy implements ILocationManager {
      public static ILocationManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.location.ILocationManager";
      }
      
      public Location getLastLocation(LocationRequest param2LocationRequest, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          if (param2LocationRequest != null) {
            parcel1.writeInt(1);
            param2LocationRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null)
            return ILocationManager.Stub.getDefaultImpl().getLastLocation(param2LocationRequest, param2String1, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Location location = Location.CREATOR.createFromParcel(parcel2);
          } else {
            param2LocationRequest = null;
          } 
          return (Location)param2LocationRequest;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getCurrentLocation(LocationRequest param2LocationRequest, ICancellationSignal param2ICancellationSignal, ILocationListener param2ILocationListener, String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          boolean bool = true;
          if (param2LocationRequest != null) {
            parcel1.writeInt(1);
            param2LocationRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          IBinder iBinder1 = null;
          if (param2ICancellationSignal != null) {
            iBinder2 = param2ICancellationSignal.asBinder();
          } else {
            iBinder2 = null;
          } 
          parcel1.writeStrongBinder(iBinder2);
          IBinder iBinder2 = iBinder1;
          if (param2ILocationListener != null)
            iBinder2 = param2ILocationListener.asBinder(); 
          parcel1.writeStrongBinder(iBinder2);
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeString(param2String3);
                try {
                  boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
                  if (!bool1 && ILocationManager.Stub.getDefaultImpl() != null) {
                    bool = ILocationManager.Stub.getDefaultImpl().getCurrentLocation(param2LocationRequest, param2ICancellationSignal, param2ILocationListener, param2String1, param2String2, param2String3);
                    parcel2.recycle();
                    parcel1.recycle();
                    return bool;
                  } 
                  parcel2.readException();
                  int i = parcel2.readInt();
                  if (i == 0)
                    bool = false; 
                  parcel2.recycle();
                  parcel1.recycle();
                  return bool;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2LocationRequest;
      }
      
      public void requestLocationUpdates(LocationRequest param2LocationRequest, ILocationListener param2ILocationListener, PendingIntent param2PendingIntent, String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          if (param2LocationRequest != null) {
            parcel1.writeInt(1);
            param2LocationRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2ILocationListener != null) {
            iBinder = param2ILocationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeString(param2String3);
                try {
                  boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
                  if (!bool && ILocationManager.Stub.getDefaultImpl() != null) {
                    ILocationManager.Stub.getDefaultImpl().requestLocationUpdates(param2LocationRequest, param2ILocationListener, param2PendingIntent, param2String1, param2String2, param2String3);
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } 
                  parcel2.readException();
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2LocationRequest;
      }
      
      public void removeUpdates(ILocationListener param2ILocationListener, PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          if (param2ILocationListener != null) {
            iBinder = param2ILocationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null) {
            ILocationManager.Stub.getDefaultImpl().removeUpdates(param2ILocationListener, param2PendingIntent);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestGeofence(LocationRequest param2LocationRequest, Geofence param2Geofence, PendingIntent param2PendingIntent, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          if (param2LocationRequest != null) {
            parcel1.writeInt(1);
            param2LocationRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Geofence != null) {
            parcel1.writeInt(1);
            param2Geofence.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null) {
            ILocationManager.Stub.getDefaultImpl().requestGeofence(param2LocationRequest, param2Geofence, param2PendingIntent, param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeGeofence(Geofence param2Geofence, PendingIntent param2PendingIntent, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          if (param2Geofence != null) {
            parcel1.writeInt(1);
            param2Geofence.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null) {
            ILocationManager.Stub.getDefaultImpl().removeGeofence(param2Geofence, param2PendingIntent, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean registerGnssStatusCallback(IGnssStatusListener param2IGnssStatusListener, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          if (param2IGnssStatusListener != null) {
            iBinder = param2IGnssStatusListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && ILocationManager.Stub.getDefaultImpl() != null) {
            bool1 = ILocationManager.Stub.getDefaultImpl().registerGnssStatusCallback(param2IGnssStatusListener, param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterGnssStatusCallback(IGnssStatusListener param2IGnssStatusListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          if (param2IGnssStatusListener != null) {
            iBinder = param2IGnssStatusListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null) {
            ILocationManager.Stub.getDefaultImpl().unregisterGnssStatusCallback(param2IGnssStatusListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean geocoderIsPresent() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && ILocationManager.Stub.getDefaultImpl() != null) {
            bool1 = ILocationManager.Stub.getDefaultImpl().geocoderIsPresent();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getFromLocation(double param2Double1, double param2Double2, int param2Int, GeocoderParams param2GeocoderParams, List<Address> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          try {
            parcel1.writeDouble(param2Double1);
            try {
              parcel1.writeDouble(param2Double2);
              parcel1.writeInt(param2Int);
              if (param2GeocoderParams != null) {
                parcel1.writeInt(1);
                param2GeocoderParams.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
              if (!bool && ILocationManager.Stub.getDefaultImpl() != null) {
                String str1 = ILocationManager.Stub.getDefaultImpl().getFromLocation(param2Double1, param2Double2, param2Int, param2GeocoderParams, param2List);
                parcel2.recycle();
                parcel1.recycle();
                return str1;
              } 
              parcel2.readException();
              String str = parcel2.readString();
              Parcelable.Creator<Address> creator = Address.CREATOR;
              try {
                parcel2.readTypedList(param2List, creator);
                parcel2.recycle();
                parcel1.recycle();
                return str;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2GeocoderParams;
      }
      
      public String getFromLocationName(String param2String, double param2Double1, double param2Double2, double param2Double3, double param2Double4, int param2Int, GeocoderParams param2GeocoderParams, List<Address> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          parcel1.writeString(param2String);
          parcel1.writeDouble(param2Double1);
          parcel1.writeDouble(param2Double2);
          parcel1.writeDouble(param2Double3);
          parcel1.writeDouble(param2Double4);
          parcel1.writeInt(param2Int);
          if (param2GeocoderParams != null) {
            try {
              parcel1.writeInt(1);
              param2GeocoderParams.writeToParcel(parcel1, 0);
            } finally {}
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool)
            try {
              if (ILocationManager.Stub.getDefaultImpl() != null) {
                ILocationManager iLocationManager = ILocationManager.Stub.getDefaultImpl();
                try {
                  param2String = iLocationManager.getFromLocationName(param2String, param2Double1, param2Double2, param2Double3, param2Double4, param2Int, param2GeocoderParams, param2List);
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2String;
                } finally {}
                parcel2.recycle();
                parcel1.recycle();
                throw param2String;
              } 
            } finally {} 
          try {
            parcel2.readException();
            String str = parcel2.readString();
            Parcelable.Creator<Address> creator = Address.CREATOR;
            try {
              parcel2.readTypedList(param2List, creator);
              parcel2.recycle();
              parcel1.recycle();
              return str;
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public boolean addGnssMeasurementsListener(GnssRequest param2GnssRequest, IGnssMeasurementsListener param2IGnssMeasurementsListener, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          boolean bool1 = true;
          if (param2GnssRequest != null) {
            parcel1.writeInt(1);
            param2GnssRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IGnssMeasurementsListener != null) {
            iBinder = param2IGnssMeasurementsListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool2 = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool2 && ILocationManager.Stub.getDefaultImpl() != null) {
            bool1 = ILocationManager.Stub.getDefaultImpl().addGnssMeasurementsListener(param2GnssRequest, param2IGnssMeasurementsListener, param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void injectGnssMeasurementCorrections(GnssMeasurementCorrections param2GnssMeasurementCorrections, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          if (param2GnssMeasurementCorrections != null) {
            parcel1.writeInt(1);
            param2GnssMeasurementCorrections.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null) {
            ILocationManager.Stub.getDefaultImpl().injectGnssMeasurementCorrections(param2GnssMeasurementCorrections, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getGnssCapabilities() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null)
            return ILocationManager.Stub.getDefaultImpl().getGnssCapabilities(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeGnssMeasurementsListener(IGnssMeasurementsListener param2IGnssMeasurementsListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          if (param2IGnssMeasurementsListener != null) {
            iBinder = param2IGnssMeasurementsListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null) {
            ILocationManager.Stub.getDefaultImpl().removeGnssMeasurementsListener(param2IGnssMeasurementsListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean addGnssAntennaInfoListener(IGnssAntennaInfoListener param2IGnssAntennaInfoListener, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          if (param2IGnssAntennaInfoListener != null) {
            iBinder = param2IGnssAntennaInfoListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(16, parcel1, parcel2, 0);
          if (!bool2 && ILocationManager.Stub.getDefaultImpl() != null) {
            bool1 = ILocationManager.Stub.getDefaultImpl().addGnssAntennaInfoListener(param2IGnssAntennaInfoListener, param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeGnssAntennaInfoListener(IGnssAntennaInfoListener param2IGnssAntennaInfoListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          if (param2IGnssAntennaInfoListener != null) {
            iBinder = param2IGnssAntennaInfoListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null) {
            ILocationManager.Stub.getDefaultImpl().removeGnssAntennaInfoListener(param2IGnssAntennaInfoListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean addGnssNavigationMessageListener(IGnssNavigationMessageListener param2IGnssNavigationMessageListener, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          if (param2IGnssNavigationMessageListener != null) {
            iBinder = param2IGnssNavigationMessageListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(18, parcel1, parcel2, 0);
          if (!bool2 && ILocationManager.Stub.getDefaultImpl() != null) {
            bool1 = ILocationManager.Stub.getDefaultImpl().addGnssNavigationMessageListener(param2IGnssNavigationMessageListener, param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeGnssNavigationMessageListener(IGnssNavigationMessageListener param2IGnssNavigationMessageListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          if (param2IGnssNavigationMessageListener != null) {
            iBinder = param2IGnssNavigationMessageListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null) {
            ILocationManager.Stub.getDefaultImpl().removeGnssNavigationMessageListener(param2IGnssNavigationMessageListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getGnssYearOfHardware() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null)
            return ILocationManager.Stub.getDefaultImpl().getGnssYearOfHardware(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getGnssHardwareModelName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null)
            return ILocationManager.Stub.getDefaultImpl().getGnssHardwareModelName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getGnssBatchSize(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null)
            return ILocationManager.Stub.getDefaultImpl().getGnssBatchSize(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean addGnssBatchingCallback(IBatchedLocationCallback param2IBatchedLocationCallback, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          if (param2IBatchedLocationCallback != null) {
            iBinder = param2IBatchedLocationCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(23, parcel1, parcel2, 0);
          if (!bool2 && ILocationManager.Stub.getDefaultImpl() != null) {
            bool1 = ILocationManager.Stub.getDefaultImpl().addGnssBatchingCallback(param2IBatchedLocationCallback, param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeGnssBatchingCallback() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null) {
            ILocationManager.Stub.getDefaultImpl().removeGnssBatchingCallback();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean startGnssBatch(long param2Long, boolean param2Boolean, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          try {
            int i;
            parcel1.writeLong(param2Long);
            boolean bool = true;
            if (param2Boolean) {
              i = 1;
            } else {
              i = 0;
            } 
            parcel1.writeInt(i);
            try {
              parcel1.writeString(param2String1);
              try {
                parcel1.writeString(param2String2);
                try {
                  boolean bool1 = this.mRemote.transact(25, parcel1, parcel2, 0);
                  if (!bool1 && ILocationManager.Stub.getDefaultImpl() != null) {
                    param2Boolean = ILocationManager.Stub.getDefaultImpl().startGnssBatch(param2Long, param2Boolean, param2String1, param2String2);
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2Boolean;
                  } 
                  parcel2.readException();
                  i = parcel2.readInt();
                  if (i != 0) {
                    param2Boolean = bool;
                  } else {
                    param2Boolean = false;
                  } 
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2Boolean;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void flushGnssBatch(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null) {
            ILocationManager.Stub.getDefaultImpl().flushGnssBatch(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean stopGnssBatch() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(27, parcel1, parcel2, 0);
          if (!bool2 && ILocationManager.Stub.getDefaultImpl() != null) {
            bool1 = ILocationManager.Stub.getDefaultImpl().stopGnssBatch();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void injectLocation(Location param2Location) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          if (param2Location != null) {
            parcel1.writeInt(1);
            param2Location.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null) {
            ILocationManager.Stub.getDefaultImpl().injectLocation(param2Location);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getAllProviders() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null)
            return ILocationManager.Stub.getDefaultImpl().getAllProviders(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getProviders(Criteria param2Criteria, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          boolean bool = true;
          if (param2Criteria != null) {
            parcel1.writeInt(1);
            param2Criteria.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool1 && ILocationManager.Stub.getDefaultImpl() != null)
            return ILocationManager.Stub.getDefaultImpl().getProviders(param2Criteria, param2Boolean); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getBestProvider(Criteria param2Criteria, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          boolean bool = true;
          if (param2Criteria != null) {
            parcel1.writeInt(1);
            param2Criteria.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool1 && ILocationManager.Stub.getDefaultImpl() != null)
            return ILocationManager.Stub.getDefaultImpl().getBestProvider(param2Criteria, param2Boolean); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ProviderProperties getProviderProperties(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null)
            return ILocationManager.Stub.getDefaultImpl().getProviderProperties(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ProviderProperties providerProperties = ProviderProperties.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ProviderProperties)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isProviderPackage(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(33, parcel1, parcel2, 0);
          if (!bool2 && ILocationManager.Stub.getDefaultImpl() != null) {
            bool1 = ILocationManager.Stub.getDefaultImpl().isProviderPackage(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getProviderPackages(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null)
            return ILocationManager.Stub.getDefaultImpl().getProviderPackages(param2String); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setExtraLocationControllerPackage(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null) {
            ILocationManager.Stub.getDefaultImpl().setExtraLocationControllerPackage(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getExtraLocationControllerPackage() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null)
            return ILocationManager.Stub.getDefaultImpl().getExtraLocationControllerPackage(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setExtraLocationControllerPackageEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool1 && ILocationManager.Stub.getDefaultImpl() != null) {
            ILocationManager.Stub.getDefaultImpl().setExtraLocationControllerPackageEnabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isExtraLocationControllerPackageEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(38, parcel1, parcel2, 0);
          if (!bool2 && ILocationManager.Stub.getDefaultImpl() != null) {
            bool1 = ILocationManager.Stub.getDefaultImpl().isExtraLocationControllerPackageEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isProviderEnabledForUser(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(39, parcel1, parcel2, 0);
          if (!bool2 && ILocationManager.Stub.getDefaultImpl() != null) {
            bool1 = ILocationManager.Stub.getDefaultImpl().isProviderEnabledForUser(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isLocationEnabledForUser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(40, parcel1, parcel2, 0);
          if (!bool2 && ILocationManager.Stub.getDefaultImpl() != null) {
            bool1 = ILocationManager.Stub.getDefaultImpl().isLocationEnabledForUser(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setLocationEnabledForUser(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool1 && ILocationManager.Stub.getDefaultImpl() != null) {
            ILocationManager.Stub.getDefaultImpl().setLocationEnabledForUser(param2Boolean, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addTestProvider(String param2String1, ProviderProperties param2ProviderProperties, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          parcel1.writeString(param2String1);
          if (param2ProviderProperties != null) {
            parcel1.writeInt(1);
            param2ProviderProperties.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null) {
            ILocationManager.Stub.getDefaultImpl().addTestProvider(param2String1, param2ProviderProperties, param2String2, param2String3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeTestProvider(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null) {
            ILocationManager.Stub.getDefaultImpl().removeTestProvider(param2String1, param2String2, param2String3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTestProviderLocation(String param2String1, Location param2Location, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          parcel1.writeString(param2String1);
          if (param2Location != null) {
            parcel1.writeInt(1);
            param2Location.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null) {
            ILocationManager.Stub.getDefaultImpl().setTestProviderLocation(param2String1, param2Location, param2String2, param2String3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTestProviderEnabled(String param2String1, boolean param2Boolean, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          parcel1.writeString(param2String1);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool1 = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool1 && ILocationManager.Stub.getDefaultImpl() != null) {
            ILocationManager.Stub.getDefaultImpl().setTestProviderEnabled(param2String1, param2Boolean, param2String2, param2String3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<LocationRequest> getTestProviderCurrentRequests(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null)
            return ILocationManager.Stub.getDefaultImpl().getTestProviderCurrentRequests(param2String); 
          parcel2.readException();
          return parcel2.createTypedArrayList(LocationRequest.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public LocationTime getGnssTimeMillis() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          LocationTime locationTime;
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null) {
            locationTime = ILocationManager.Stub.getDefaultImpl().getGnssTimeMillis();
            return locationTime;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            locationTime = LocationTime.CREATOR.createFromParcel(parcel2);
          } else {
            locationTime = null;
          } 
          return locationTime;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean sendExtraCommand(String param2String1, String param2String2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool1 = true;
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(48, parcel1, parcel2, 0);
          if (!bool2 && ILocationManager.Stub.getDefaultImpl() != null) {
            bool1 = ILocationManager.Stub.getDefaultImpl().sendExtraCommand(param2String1, param2String2, param2Bundle);
            return bool1;
          } 
          parcel2.readException();
          if (parcel2.readInt() == 0)
            bool1 = false; 
          if (parcel2.readInt() != 0)
            param2Bundle.readFromParcel(parcel2); 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void locationCallbackFinished(ILocationListener param2ILocationListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          if (param2ILocationListener != null) {
            iBinder = param2ILocationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(49, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null) {
            ILocationManager.Stub.getDefaultImpl().locationCallbackFinished(param2ILocationListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getBackgroundThrottlingWhitelist() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          boolean bool = this.mRemote.transact(50, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null)
            return ILocationManager.Stub.getDefaultImpl().getBackgroundThrottlingWhitelist(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getIgnoreSettingsWhitelist() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.ILocationManager");
          boolean bool = this.mRemote.transact(51, parcel1, parcel2, 0);
          if (!bool && ILocationManager.Stub.getDefaultImpl() != null)
            return ILocationManager.Stub.getDefaultImpl().getIgnoreSettingsWhitelist(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ILocationManager param1ILocationManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ILocationManager != null) {
          Proxy.sDefaultImpl = param1ILocationManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ILocationManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
