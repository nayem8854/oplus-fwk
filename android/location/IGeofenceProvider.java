package android.location;

import android.hardware.location.IGeofenceHardware;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IGeofenceProvider extends IInterface {
  void setGeofenceHardware(IGeofenceHardware paramIGeofenceHardware) throws RemoteException;
  
  class Default implements IGeofenceProvider {
    public void setGeofenceHardware(IGeofenceHardware param1IGeofenceHardware) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IGeofenceProvider {
    private static final String DESCRIPTOR = "android.location.IGeofenceProvider";
    
    static final int TRANSACTION_setGeofenceHardware = 1;
    
    public Stub() {
      attachInterface(this, "android.location.IGeofenceProvider");
    }
    
    public static IGeofenceProvider asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.location.IGeofenceProvider");
      if (iInterface != null && iInterface instanceof IGeofenceProvider)
        return (IGeofenceProvider)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "setGeofenceHardware";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.location.IGeofenceProvider");
        return true;
      } 
      param1Parcel1.enforceInterface("android.location.IGeofenceProvider");
      IGeofenceHardware iGeofenceHardware = IGeofenceHardware.Stub.asInterface(param1Parcel1.readStrongBinder());
      setGeofenceHardware(iGeofenceHardware);
      return true;
    }
    
    private static class Proxy implements IGeofenceProvider {
      public static IGeofenceProvider sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.location.IGeofenceProvider";
      }
      
      public void setGeofenceHardware(IGeofenceHardware param2IGeofenceHardware) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.location.IGeofenceProvider");
          if (param2IGeofenceHardware != null) {
            iBinder = param2IGeofenceHardware.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IGeofenceProvider.Stub.getDefaultImpl() != null) {
            IGeofenceProvider.Stub.getDefaultImpl().setGeofenceHardware(param2IGeofenceHardware);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IGeofenceProvider param1IGeofenceProvider) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IGeofenceProvider != null) {
          Proxy.sDefaultImpl = param1IGeofenceProvider;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IGeofenceProvider getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
