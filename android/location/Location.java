package android.location;

import android.annotation.SystemApi;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.Printer;
import android.util.TimeUtils;
import java.util.StringTokenizer;

public class Location implements Parcelable {
  private static ThreadLocal<BearingDistanceCache> sBearingDistanceCache = (ThreadLocal<BearingDistanceCache>)new Object();
  
  private long mTime = 0L;
  
  private long mElapsedRealtimeNanos = 0L;
  
  private double mElapsedRealtimeUncertaintyNanos = 0.0D;
  
  private double mLatitude = 0.0D;
  
  private double mLongitude = 0.0D;
  
  private double mAltitude = 0.0D;
  
  private float mSpeed = 0.0F;
  
  private float mBearing = 0.0F;
  
  private float mHorizontalAccuracyMeters = 0.0F;
  
  private float mVerticalAccuracyMeters = 0.0F;
  
  private float mSpeedAccuracyMetersPerSecond = 0.0F;
  
  private float mBearingAccuracyDegrees = 0.0F;
  
  private Bundle mExtras = null;
  
  private int mFieldsMask = 0;
  
  public static final Parcelable.Creator<Location> CREATOR;
  
  @SystemApi
  @Deprecated
  public static final String EXTRA_NO_GPS_LOCATION = "noGPSLocation";
  
  public static final int FORMAT_DEGREES = 0;
  
  public static final int FORMAT_MINUTES = 1;
  
  public static final int FORMAT_SECONDS = 2;
  
  private static final int HAS_ALTITUDE_MASK = 1;
  
  private static final int HAS_BEARING_ACCURACY_MASK = 128;
  
  private static final int HAS_BEARING_MASK = 4;
  
  private static final int HAS_ELAPSED_REALTIME_UNCERTAINTY_MASK = 256;
  
  private static final int HAS_HORIZONTAL_ACCURACY_MASK = 8;
  
  private static final int HAS_MOCK_PROVIDER_MASK = 16;
  
  private static final int HAS_SPEED_ACCURACY_MASK = 64;
  
  private static final int HAS_SPEED_MASK = 2;
  
  private static final int HAS_VERTICAL_ACCURACY_MASK = 32;
  
  private String mProvider;
  
  public Location(String paramString) {
    this.mProvider = paramString;
  }
  
  public Location(Location paramLocation) {
    set(paramLocation);
  }
  
  public void set(Location paramLocation) {
    Bundle bundle;
    this.mProvider = paramLocation.mProvider;
    this.mTime = paramLocation.mTime;
    this.mElapsedRealtimeNanos = paramLocation.mElapsedRealtimeNanos;
    this.mElapsedRealtimeUncertaintyNanos = paramLocation.mElapsedRealtimeUncertaintyNanos;
    this.mFieldsMask = paramLocation.mFieldsMask;
    this.mLatitude = paramLocation.mLatitude;
    this.mLongitude = paramLocation.mLongitude;
    this.mAltitude = paramLocation.mAltitude;
    this.mSpeed = paramLocation.mSpeed;
    this.mBearing = paramLocation.mBearing;
    this.mHorizontalAccuracyMeters = paramLocation.mHorizontalAccuracyMeters;
    this.mVerticalAccuracyMeters = paramLocation.mVerticalAccuracyMeters;
    this.mSpeedAccuracyMetersPerSecond = paramLocation.mSpeedAccuracyMetersPerSecond;
    this.mBearingAccuracyDegrees = paramLocation.mBearingAccuracyDegrees;
    if (paramLocation.mExtras == null) {
      paramLocation = null;
    } else {
      bundle = new Bundle(paramLocation.mExtras);
    } 
    this.mExtras = bundle;
  }
  
  public void reset() {
    this.mProvider = null;
    this.mTime = 0L;
    this.mElapsedRealtimeNanos = 0L;
    this.mElapsedRealtimeUncertaintyNanos = 0.0D;
    this.mFieldsMask = 0;
    this.mLatitude = 0.0D;
    this.mLongitude = 0.0D;
    this.mAltitude = 0.0D;
    this.mSpeed = 0.0F;
    this.mBearing = 0.0F;
    this.mHorizontalAccuracyMeters = 0.0F;
    this.mVerticalAccuracyMeters = 0.0F;
    this.mSpeedAccuracyMetersPerSecond = 0.0F;
    this.mBearingAccuracyDegrees = 0.0F;
    this.mExtras = null;
  }
  
  public static String convert(double paramDouble, int paramInt) {
    // Byte code:
    //   0: dload_0
    //   1: ldc2_w -180.0
    //   4: dcmpg
    //   5: iflt -> 219
    //   8: dload_0
    //   9: ldc2_w 180.0
    //   12: dcmpl
    //   13: ifgt -> 219
    //   16: dload_0
    //   17: invokestatic isNaN : (D)Z
    //   20: ifne -> 219
    //   23: iload_2
    //   24: ifeq -> 74
    //   27: iload_2
    //   28: iconst_1
    //   29: if_icmpeq -> 74
    //   32: iload_2
    //   33: iconst_2
    //   34: if_icmpne -> 40
    //   37: goto -> 74
    //   40: new java/lang/StringBuilder
    //   43: dup
    //   44: invokespecial <init> : ()V
    //   47: astore_3
    //   48: aload_3
    //   49: ldc_w 'outputType='
    //   52: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   55: pop
    //   56: aload_3
    //   57: iload_2
    //   58: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   61: pop
    //   62: new java/lang/IllegalArgumentException
    //   65: dup
    //   66: aload_3
    //   67: invokevirtual toString : ()Ljava/lang/String;
    //   70: invokespecial <init> : (Ljava/lang/String;)V
    //   73: athrow
    //   74: new java/lang/StringBuilder
    //   77: dup
    //   78: invokespecial <init> : ()V
    //   81: astore_3
    //   82: dload_0
    //   83: dstore #4
    //   85: dload_0
    //   86: dconst_0
    //   87: dcmpg
    //   88: ifge -> 102
    //   91: aload_3
    //   92: bipush #45
    //   94: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   97: pop
    //   98: dload_0
    //   99: dneg
    //   100: dstore #4
    //   102: new java/text/DecimalFormat
    //   105: dup
    //   106: ldc_w '###.#####'
    //   109: invokespecial <init> : (Ljava/lang/String;)V
    //   112: astore #6
    //   114: iload_2
    //   115: iconst_1
    //   116: if_icmpeq -> 127
    //   119: dload #4
    //   121: dstore_0
    //   122: iload_2
    //   123: iconst_2
    //   124: if_icmpne -> 203
    //   127: dload #4
    //   129: invokestatic floor : (D)D
    //   132: d2i
    //   133: istore #7
    //   135: aload_3
    //   136: iload #7
    //   138: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   141: pop
    //   142: aload_3
    //   143: bipush #58
    //   145: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   148: pop
    //   149: iload #7
    //   151: i2d
    //   152: dstore_0
    //   153: dload #4
    //   155: dload_0
    //   156: dsub
    //   157: ldc2_w 60.0
    //   160: dmul
    //   161: dstore #4
    //   163: dload #4
    //   165: dstore_0
    //   166: iload_2
    //   167: iconst_2
    //   168: if_icmpne -> 203
    //   171: dload #4
    //   173: invokestatic floor : (D)D
    //   176: d2i
    //   177: istore_2
    //   178: aload_3
    //   179: iload_2
    //   180: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   183: pop
    //   184: aload_3
    //   185: bipush #58
    //   187: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   190: pop
    //   191: iload_2
    //   192: i2d
    //   193: dstore_0
    //   194: dload #4
    //   196: dload_0
    //   197: dsub
    //   198: ldc2_w 60.0
    //   201: dmul
    //   202: dstore_0
    //   203: aload_3
    //   204: aload #6
    //   206: dload_0
    //   207: invokevirtual format : (D)Ljava/lang/String;
    //   210: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   213: pop
    //   214: aload_3
    //   215: invokevirtual toString : ()Ljava/lang/String;
    //   218: areturn
    //   219: new java/lang/StringBuilder
    //   222: dup
    //   223: invokespecial <init> : ()V
    //   226: astore_3
    //   227: aload_3
    //   228: ldc_w 'coordinate='
    //   231: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   234: pop
    //   235: aload_3
    //   236: dload_0
    //   237: invokevirtual append : (D)Ljava/lang/StringBuilder;
    //   240: pop
    //   241: new java/lang/IllegalArgumentException
    //   244: dup
    //   245: aload_3
    //   246: invokevirtual toString : ()Ljava/lang/String;
    //   249: invokespecial <init> : (Ljava/lang/String;)V
    //   252: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #230	-> 0
    //   #231	-> 16
    //   #234	-> 23
    //   #237	-> 40
    //   #240	-> 74
    //   #243	-> 82
    //   #244	-> 91
    //   #245	-> 98
    //   #248	-> 102
    //   #249	-> 114
    //   #250	-> 127
    //   #251	-> 135
    //   #252	-> 142
    //   #253	-> 149
    //   #254	-> 153
    //   #255	-> 163
    //   #256	-> 171
    //   #257	-> 178
    //   #258	-> 184
    //   #259	-> 191
    //   #260	-> 194
    //   #263	-> 203
    //   #264	-> 214
    //   #232	-> 219
  }
  
  public static double convert(String paramString) {
    if (paramString != null) {
      boolean bool;
      if (paramString.charAt(0) == '-') {
        paramString = paramString.substring(1);
        bool = true;
      } else {
        bool = false;
      } 
      StringTokenizer stringTokenizer = new StringTokenizer(paramString, ":");
      int i = stringTokenizer.countTokens();
      if (i >= 1) {
        try {
          String str = stringTokenizer.nextToken();
          if (i == 1) {
            try {
              double d = Double.parseDouble(str);
              if (bool)
                d = -d; 
              return d;
            } catch (NumberFormatException numberFormatException) {}
          } else {
            double d1;
            boolean bool2;
            String str1 = stringTokenizer.nextToken();
            int j = Integer.parseInt((String)numberFormatException);
            double d2 = 0.0D;
            i = 0;
            boolean bool1 = stringTokenizer.hasMoreTokens();
            if (bool1) {
              d1 = Integer.parseInt(str1);
              String str2 = stringTokenizer.nextToken();
              d2 = Double.parseDouble(str2);
              i = 1;
            } else {
              d1 = Double.parseDouble(str1);
            } 
            if (bool && j == 180 && d1 == 0.0D && d2 == 0.0D) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            if (j >= 0.0D && (j <= 179 || bool2)) {
              if (d1 >= 0.0D && d1 < 60.0D && (i == 0 || d1 <= 59.0D)) {
                if (d2 >= 0.0D && d2 < 60.0D) {
                  double d = j;
                  d1 = (d * 3600.0D + 60.0D * d1 + d2) / 3600.0D;
                  if (bool)
                    d1 = -d1; 
                  return d1;
                } 
                try {
                  IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
                  StringBuilder stringBuilder2 = new StringBuilder();
                  this();
                  stringBuilder2.append("coordinate=");
                  stringBuilder2.append(paramString);
                  this(stringBuilder2.toString());
                  throw illegalArgumentException;
                } catch (NumberFormatException numberFormatException1) {}
              } else {
                IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
                StringBuilder stringBuilder2 = new StringBuilder();
                this();
                stringBuilder2.append("coordinate=");
                stringBuilder2.append(paramString);
                this(stringBuilder2.toString());
                throw illegalArgumentException;
              } 
            } else {
              IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
              StringBuilder stringBuilder2 = new StringBuilder();
              this();
              stringBuilder2.append("coordinate=");
              stringBuilder2.append(paramString);
              this(stringBuilder2.toString());
              throw illegalArgumentException;
            } 
          } 
        } catch (NumberFormatException numberFormatException) {}
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("coordinate=");
        stringBuilder1.append(paramString);
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("coordinate=");
      stringBuilder.append(paramString);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    throw new NullPointerException("coordinate");
  }
  
  private static void computeDistanceAndBearing(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, BearingDistanceCache paramBearingDistanceCache) {
    double d1 = paramDouble1 * 0.017453292519943295D;
    double d2 = paramDouble3 * 0.017453292519943295D;
    double d3 = paramDouble2 * 0.017453292519943295D;
    double d4 = 0.017453292519943295D * paramDouble4;
    double d5 = (6378137.0D - 6356752.3142D) / 6378137.0D;
    double d6 = (6378137.0D * 6378137.0D - 6356752.3142D * 6356752.3142D) / 6356752.3142D * 6356752.3142D;
    double d7 = d4 - d3;
    paramDouble1 = 0.0D;
    paramDouble3 = Math.atan((1.0D - d5) * Math.tan(d1));
    paramDouble2 = Math.atan((1.0D - d5) * Math.tan(d2));
    double d8 = Math.cos(paramDouble3);
    double d9 = Math.cos(paramDouble2);
    double d10 = Math.sin(paramDouble3);
    double d11 = Math.sin(paramDouble2);
    double d12 = d8 * d9;
    double d13 = d10 * d11;
    paramDouble2 = 0.0D;
    paramDouble3 = 0.0D;
    paramDouble4 = 0.0D;
    double d14 = 0.0D;
    double d15 = d7;
    byte b = 0;
    while (true) {
      double d = d15;
      if (b < 20) {
        paramDouble4 = Math.cos(d);
        d14 = Math.sin(d);
        paramDouble2 = d9 * d14;
        paramDouble1 = d8 * d11 - d10 * d9 * paramDouble4;
        double d16 = Math.sqrt(paramDouble2 * paramDouble2 + paramDouble1 * paramDouble1);
        double d17 = d13 + d12 * paramDouble4;
        paramDouble2 = Math.atan2(d16, d17);
        double d18 = 0.0D;
        if (d16 == 0.0D) {
          d15 = 0.0D;
        } else {
          d15 = d12 * d14 / d16;
        } 
        paramDouble3 = 1.0D - d15 * d15;
        if (paramDouble3 != 0.0D)
          d18 = d17 - d13 * 2.0D / paramDouble3; 
        double d19 = paramDouble3 * d6;
        paramDouble1 = d19 / 16384.0D * (((320.0D - 175.0D * d19) * d19 - 768.0D) * d19 + 4096.0D) + 1.0D;
        d19 = d19 / 1024.0D * (((74.0D - 47.0D * d19) * d19 - 128.0D) * d19 + 256.0D);
        double d20 = d5 / 16.0D * paramDouble3 * ((4.0D - 3.0D * paramDouble3) * d5 + 4.0D);
        paramDouble3 = d18 * d18;
        paramDouble3 = d19 * d16 * (d18 + d19 / 4.0D * ((paramDouble3 * 2.0D - 1.0D) * d17 - d19 / 6.0D * d18 * (d16 * 4.0D * d16 - 3.0D) * (4.0D * paramDouble3 - 3.0D)));
        d15 = d7 + (1.0D - d20) * d5 * d15 * (paramDouble2 + d20 * d16 * (d18 + d20 * d17 * (2.0D * d18 * d18 - 1.0D)));
        d18 = (d15 - d) / d15;
        if (Math.abs(d18) < 1.0E-12D)
          break; 
        b++;
        continue;
      } 
      break;
    } 
    float f = (float)(6356752.3142D * paramDouble1 * (paramDouble2 - paramDouble3));
    BearingDistanceCache.access$102(paramBearingDistanceCache, f);
    f = (float)Math.atan2(d9 * d14, d8 * d11 - d10 * d9 * paramDouble4);
    f = (float)(f * 57.29577951308232D);
    BearingDistanceCache.access$202(paramBearingDistanceCache, f);
    f = (float)Math.atan2(d8 * d14, -d10 * d9 + d8 * d11 * paramDouble4);
    f = (float)(f * 57.29577951308232D);
    BearingDistanceCache.access$302(paramBearingDistanceCache, f);
    BearingDistanceCache.access$402(paramBearingDistanceCache, d1);
    BearingDistanceCache.access$502(paramBearingDistanceCache, d2);
    BearingDistanceCache.access$602(paramBearingDistanceCache, d3);
    BearingDistanceCache.access$702(paramBearingDistanceCache, d4);
  }
  
  public static void distanceBetween(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, float[] paramArrayOffloat) {
    if (paramArrayOffloat != null && paramArrayOffloat.length >= 1) {
      BearingDistanceCache bearingDistanceCache = sBearingDistanceCache.get();
      computeDistanceAndBearing(paramDouble1, paramDouble2, paramDouble3, paramDouble4, bearingDistanceCache);
      paramArrayOffloat[0] = bearingDistanceCache.mDistance;
      if (paramArrayOffloat.length > 1) {
        paramArrayOffloat[1] = bearingDistanceCache.mInitialBearing;
        if (paramArrayOffloat.length > 2)
          paramArrayOffloat[2] = bearingDistanceCache.mFinalBearing; 
      } 
      return;
    } 
    throw new IllegalArgumentException("results is null or has length < 1");
  }
  
  public float distanceTo(Location paramLocation) {
    BearingDistanceCache bearingDistanceCache = sBearingDistanceCache.get();
    if (this.mLatitude == bearingDistanceCache.mLat1 && this.mLongitude == bearingDistanceCache.mLon1) {
      double d = paramLocation.mLatitude;
      if (d != bearingDistanceCache.mLat2 || paramLocation.mLongitude != bearingDistanceCache.mLon2) {
        computeDistanceAndBearing(this.mLatitude, this.mLongitude, paramLocation.mLatitude, paramLocation.mLongitude, bearingDistanceCache);
        return bearingDistanceCache.mDistance;
      } 
      return bearingDistanceCache.mDistance;
    } 
    computeDistanceAndBearing(this.mLatitude, this.mLongitude, paramLocation.mLatitude, paramLocation.mLongitude, bearingDistanceCache);
    return bearingDistanceCache.mDistance;
  }
  
  public float bearingTo(Location paramLocation) {
    BearingDistanceCache bearingDistanceCache = sBearingDistanceCache.get();
    if (this.mLatitude == bearingDistanceCache.mLat1 && this.mLongitude == bearingDistanceCache.mLon1) {
      double d = paramLocation.mLatitude;
      if (d != bearingDistanceCache.mLat2 || paramLocation.mLongitude != bearingDistanceCache.mLon2) {
        computeDistanceAndBearing(this.mLatitude, this.mLongitude, paramLocation.mLatitude, paramLocation.mLongitude, bearingDistanceCache);
        return bearingDistanceCache.mInitialBearing;
      } 
      return bearingDistanceCache.mInitialBearing;
    } 
    computeDistanceAndBearing(this.mLatitude, this.mLongitude, paramLocation.mLatitude, paramLocation.mLongitude, bearingDistanceCache);
    return bearingDistanceCache.mInitialBearing;
  }
  
  public String getProvider() {
    return this.mProvider;
  }
  
  public void setProvider(String paramString) {
    this.mProvider = paramString;
  }
  
  public long getTime() {
    return this.mTime;
  }
  
  public void setTime(long paramLong) {
    this.mTime = paramLong;
  }
  
  public long getElapsedRealtimeNanos() {
    return this.mElapsedRealtimeNanos;
  }
  
  public long getElapsedRealtimeAgeNanos(long paramLong) {
    return paramLong - this.mElapsedRealtimeNanos;
  }
  
  public long getElapsedRealtimeAgeNanos() {
    return getElapsedRealtimeAgeNanos(SystemClock.elapsedRealtimeNanos());
  }
  
  public void setElapsedRealtimeNanos(long paramLong) {
    this.mElapsedRealtimeNanos = paramLong;
  }
  
  public double getElapsedRealtimeUncertaintyNanos() {
    return this.mElapsedRealtimeUncertaintyNanos;
  }
  
  public void setElapsedRealtimeUncertaintyNanos(double paramDouble) {
    this.mElapsedRealtimeUncertaintyNanos = paramDouble;
    this.mFieldsMask |= 0x100;
  }
  
  public boolean hasElapsedRealtimeUncertaintyNanos() {
    boolean bool;
    if ((this.mFieldsMask & 0x100) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public double getLatitude() {
    return this.mLatitude;
  }
  
  public void setLatitude(double paramDouble) {
    this.mLatitude = paramDouble;
  }
  
  public double getLongitude() {
    return this.mLongitude;
  }
  
  public void setLongitude(double paramDouble) {
    this.mLongitude = paramDouble;
  }
  
  public boolean hasAltitude() {
    int i = this.mFieldsMask;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public double getAltitude() {
    return this.mAltitude;
  }
  
  public void setAltitude(double paramDouble) {
    this.mAltitude = paramDouble;
    this.mFieldsMask |= 0x1;
  }
  
  @Deprecated
  public void removeAltitude() {
    this.mAltitude = 0.0D;
    this.mFieldsMask &= 0xFFFFFFFE;
  }
  
  public boolean hasSpeed() {
    boolean bool;
    if ((this.mFieldsMask & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public float getSpeed() {
    return this.mSpeed;
  }
  
  public void setSpeed(float paramFloat) {
    this.mSpeed = paramFloat;
    this.mFieldsMask |= 0x2;
  }
  
  @Deprecated
  public void removeSpeed() {
    this.mSpeed = 0.0F;
    this.mFieldsMask &= 0xFFFFFFFD;
  }
  
  public boolean hasBearing() {
    boolean bool;
    if ((this.mFieldsMask & 0x4) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public float getBearing() {
    return this.mBearing;
  }
  
  public void setBearing(float paramFloat) {
    float f;
    while (true) {
      f = paramFloat;
      if (paramFloat < 0.0F) {
        paramFloat += 360.0F;
        continue;
      } 
      break;
    } 
    while (f >= 360.0F)
      f -= 360.0F; 
    this.mBearing = f;
    this.mFieldsMask |= 0x4;
  }
  
  @Deprecated
  public void removeBearing() {
    this.mBearing = 0.0F;
    this.mFieldsMask &= 0xFFFFFFFB;
  }
  
  public boolean hasAccuracy() {
    boolean bool;
    if ((this.mFieldsMask & 0x8) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public float getAccuracy() {
    return this.mHorizontalAccuracyMeters;
  }
  
  public void setAccuracy(float paramFloat) {
    this.mHorizontalAccuracyMeters = paramFloat;
    this.mFieldsMask |= 0x8;
  }
  
  @Deprecated
  public void removeAccuracy() {
    this.mHorizontalAccuracyMeters = 0.0F;
    this.mFieldsMask &= 0xFFFFFFF7;
  }
  
  public boolean hasVerticalAccuracy() {
    boolean bool;
    if ((this.mFieldsMask & 0x20) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public float getVerticalAccuracyMeters() {
    return this.mVerticalAccuracyMeters;
  }
  
  public void setVerticalAccuracyMeters(float paramFloat) {
    this.mVerticalAccuracyMeters = paramFloat;
    this.mFieldsMask |= 0x20;
  }
  
  @Deprecated
  public void removeVerticalAccuracy() {
    this.mVerticalAccuracyMeters = 0.0F;
    this.mFieldsMask &= 0xFFFFFFDF;
  }
  
  public boolean hasSpeedAccuracy() {
    boolean bool;
    if ((this.mFieldsMask & 0x40) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public float getSpeedAccuracyMetersPerSecond() {
    return this.mSpeedAccuracyMetersPerSecond;
  }
  
  public void setSpeedAccuracyMetersPerSecond(float paramFloat) {
    this.mSpeedAccuracyMetersPerSecond = paramFloat;
    this.mFieldsMask |= 0x40;
  }
  
  @Deprecated
  public void removeSpeedAccuracy() {
    this.mSpeedAccuracyMetersPerSecond = 0.0F;
    this.mFieldsMask &= 0xFFFFFFBF;
  }
  
  public boolean hasBearingAccuracy() {
    boolean bool;
    if ((this.mFieldsMask & 0x80) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public float getBearingAccuracyDegrees() {
    return this.mBearingAccuracyDegrees;
  }
  
  public void setBearingAccuracyDegrees(float paramFloat) {
    this.mBearingAccuracyDegrees = paramFloat;
    this.mFieldsMask |= 0x80;
  }
  
  @Deprecated
  public void removeBearingAccuracy() {
    this.mBearingAccuracyDegrees = 0.0F;
    this.mFieldsMask &= 0xFFFFFF7F;
  }
  
  @SystemApi
  public boolean isComplete() {
    if (this.mProvider == null)
      return false; 
    if (!hasAccuracy())
      return false; 
    if (this.mTime == 0L)
      return false; 
    if (this.mElapsedRealtimeNanos == 0L)
      return false; 
    return true;
  }
  
  @SystemApi
  public void makeComplete() {
    if (this.mProvider == null)
      this.mProvider = "?"; 
    if (!hasAccuracy()) {
      this.mFieldsMask |= 0x8;
      this.mHorizontalAccuracyMeters = 100.0F;
    } 
    if (this.mTime == 0L)
      this.mTime = System.currentTimeMillis(); 
    if (this.mElapsedRealtimeNanos == 0L)
      this.mElapsedRealtimeNanos = SystemClock.elapsedRealtimeNanos(); 
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public void setExtras(Bundle paramBundle) {
    if (paramBundle == null) {
      paramBundle = null;
    } else {
      paramBundle = new Bundle(paramBundle);
    } 
    this.mExtras = paramBundle;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Location[");
    stringBuilder.append(this.mProvider);
    if ("eng".equals(Build.TYPE) || "userdebug".equals(Build.TYPE)) {
      stringBuilder.append(String.format(" %.6f,%.6f", new Object[] { Double.valueOf(this.mLatitude), Double.valueOf(this.mLongitude) }));
    } else {
      stringBuilder.append(String.format(" %.2f****,%.2f****", new Object[] { Double.valueOf(this.mLatitude), Double.valueOf(this.mLongitude) }));
    } 
    if (hasAccuracy()) {
      stringBuilder.append(String.format(" hAcc=%.0f", new Object[] { Float.valueOf(this.mHorizontalAccuracyMeters) }));
    } else {
      stringBuilder.append(" hAcc=???");
    } 
    if (this.mTime == 0L)
      stringBuilder.append(" t=?!?"); 
    if (this.mElapsedRealtimeNanos == 0L) {
      stringBuilder.append(" et=?!?");
    } else {
      stringBuilder.append(" et=");
      TimeUtils.formatDuration(this.mElapsedRealtimeNanos / 1000000L, stringBuilder);
    } 
    if (hasElapsedRealtimeUncertaintyNanos()) {
      stringBuilder.append(" etAcc=");
      TimeUtils.formatDuration((long)(this.mElapsedRealtimeUncertaintyNanos / 1000000.0D), stringBuilder);
    } 
    if (hasAltitude()) {
      stringBuilder.append(" alt=");
      stringBuilder.append(this.mAltitude);
    } 
    if (hasSpeed()) {
      stringBuilder.append(" vel=");
      stringBuilder.append(this.mSpeed);
    } 
    if (hasBearing()) {
      stringBuilder.append(" bear=");
      stringBuilder.append(this.mBearing);
    } 
    if (hasVerticalAccuracy()) {
      stringBuilder.append(String.format(" vAcc=%.0f", new Object[] { Float.valueOf(this.mVerticalAccuracyMeters) }));
    } else {
      stringBuilder.append(" vAcc=???");
    } 
    if (hasSpeedAccuracy()) {
      stringBuilder.append(String.format(" sAcc=%.0f", new Object[] { Float.valueOf(this.mSpeedAccuracyMetersPerSecond) }));
    } else {
      stringBuilder.append(" sAcc=???");
    } 
    if (hasBearingAccuracy()) {
      stringBuilder.append(String.format(" bAcc=%.0f", new Object[] { Float.valueOf(this.mBearingAccuracyDegrees) }));
    } else {
      stringBuilder.append(" bAcc=???");
    } 
    if (isFromMockProvider())
      stringBuilder.append(" mock"); 
    if (this.mExtras != null) {
      if ("eng".equals(Build.TYPE) || "userdebug".equals(Build.TYPE)) {
        stringBuilder.append(" {");
        stringBuilder.append(this.mExtras);
        stringBuilder.append('}');
        stringBuilder.append(']');
        return stringBuilder.toString();
      } 
      if (!"network".equals(this.mProvider)) {
        stringBuilder.append(" {");
        stringBuilder.append(this.mExtras);
        stringBuilder.append('}');
      } 
    } 
    stringBuilder.append(']');
    return stringBuilder.toString();
  }
  
  public void dump(Printer paramPrinter, String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(toString());
    paramPrinter.println(stringBuilder.toString());
  }
  
  static {
    CREATOR = new Parcelable.Creator<Location>() {
        public Location createFromParcel(Parcel param1Parcel) {
          String str = param1Parcel.readString();
          Location location = new Location(str);
          Location.access$802(location, param1Parcel.readLong());
          Location.access$902(location, param1Parcel.readLong());
          Location.access$1002(location, param1Parcel.readDouble());
          Location.access$1102(location, param1Parcel.readInt());
          Location.access$1202(location, param1Parcel.readDouble());
          Location.access$1302(location, param1Parcel.readDouble());
          Location.access$1402(location, param1Parcel.readDouble());
          Location.access$1502(location, param1Parcel.readFloat());
          Location.access$1602(location, param1Parcel.readFloat());
          Location.access$1702(location, param1Parcel.readFloat());
          Location.access$1802(location, param1Parcel.readFloat());
          Location.access$1902(location, param1Parcel.readFloat());
          Location.access$2002(location, param1Parcel.readFloat());
          Location.access$2102(location, Bundle.setDefusable(param1Parcel.readBundle(), true));
          return location;
        }
        
        public Location[] newArray(int param1Int) {
          return new Location[param1Int];
        }
      };
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mProvider);
    paramParcel.writeLong(this.mTime);
    paramParcel.writeLong(this.mElapsedRealtimeNanos);
    paramParcel.writeDouble(this.mElapsedRealtimeUncertaintyNanos);
    paramParcel.writeInt(this.mFieldsMask);
    paramParcel.writeDouble(this.mLatitude);
    paramParcel.writeDouble(this.mLongitude);
    paramParcel.writeDouble(this.mAltitude);
    paramParcel.writeFloat(this.mSpeed);
    paramParcel.writeFloat(this.mBearing);
    paramParcel.writeFloat(this.mHorizontalAccuracyMeters);
    paramParcel.writeFloat(this.mVerticalAccuracyMeters);
    paramParcel.writeFloat(this.mSpeedAccuracyMetersPerSecond);
    paramParcel.writeFloat(this.mBearingAccuracyDegrees);
    paramParcel.writeBundle(this.mExtras);
  }
  
  public Location getExtraLocation(String paramString) {
    Bundle bundle = this.mExtras;
    if (bundle != null)
      try {
        paramString = bundle.getParcelable(paramString);
        if (paramString instanceof Location)
          return (Location)paramString; 
      } catch (NullPointerException nullPointerException) {
        this.mExtras = null;
      }  
    return null;
  }
  
  public boolean isFromMockProvider() {
    boolean bool;
    if ((this.mFieldsMask & 0x10) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @SystemApi
  public void setIsFromMockProvider(boolean paramBoolean) {
    if (paramBoolean) {
      this.mFieldsMask |= 0x10;
    } else {
      this.mFieldsMask &= 0xFFFFFFEF;
    } 
  }
  
  class BearingDistanceCache {
    private float mDistance;
    
    private float mFinalBearing;
    
    private float mInitialBearing;
    
    private double mLat1;
    
    private double mLat2;
    
    private double mLon1;
    
    private double mLon2;
    
    private BearingDistanceCache() {
      this.mLat1 = 0.0D;
      this.mLon1 = 0.0D;
      this.mLat2 = 0.0D;
      this.mLon2 = 0.0D;
      this.mDistance = 0.0F;
      this.mInitialBearing = 0.0F;
      this.mFinalBearing = 0.0F;
    }
  }
}
