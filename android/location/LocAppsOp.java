package android.location;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashMap;

public class LocAppsOp implements Parcelable {
  private int mOpLevel = Integer.MAX_VALUE;
  
  private HashMap<String, Integer> mOpList = new HashMap<>();
  
  public LocAppsOp(Parcel paramParcel) {
    this();
    readFromParcel(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mOpLevel);
    paramParcel.writeMap(this.mOpList);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mOpLevel = paramParcel.readInt();
    paramParcel.readMap(this.mOpList, getClass().getClassLoader());
  }
  
  public static final Parcelable.Creator<LocAppsOp> CREATOR = new Parcelable.Creator<LocAppsOp>() {
      public LocAppsOp createFromParcel(Parcel param1Parcel) {
        return new LocAppsOp(param1Parcel);
      }
      
      public LocAppsOp[] newArray(int param1Int) {
        return new LocAppsOp[param1Int];
      }
    };
  
  public static final int ALLOW = 1;
  
  public static final int CMD_REMOVE = 0;
  
  public static final int CMD_UPDATE = 1;
  
  public static final int FG_ONLY = 2;
  
  public static final int GET_CFG_OP = 1;
  
  public static final int GET_OP_LEVEL_ONLY = 3;
  
  public static final int GET_SET_OP = 2;
  
  public static final int PROHIBIT = 3;
  
  public int getOpLevel() {
    return this.mOpLevel;
  }
  
  public void setOpLevel(int paramInt) {
    this.mOpLevel = paramInt;
  }
  
  public HashMap<String, Integer> getAppsOp() {
    return this.mOpList;
  }
  
  public void setAppsOp(HashMap<String, Integer> paramHashMap) {
    this.mOpList = paramHashMap;
  }
  
  public void setAppOp(String paramString, int paramInt) {
    this.mOpList.put(paramString, Integer.valueOf(paramInt));
  }
  
  public LocAppsOp() {}
}
