package android.location;

import com.android.internal.location.ProviderProperties;

public class LocationProvider {
  @Deprecated
  public static final int AVAILABLE = 2;
  
  @Deprecated
  public static final int OUT_OF_SERVICE = 0;
  
  @Deprecated
  public static final int TEMPORARILY_UNAVAILABLE = 1;
  
  private final String mName;
  
  private final ProviderProperties mProperties;
  
  LocationProvider(String paramString, ProviderProperties paramProviderProperties) {
    this.mName = paramString;
    this.mProperties = paramProviderProperties;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public boolean meetsCriteria(Criteria paramCriteria) {
    return propertiesMeetCriteria(this.mName, this.mProperties, paramCriteria);
  }
  
  public static boolean propertiesMeetCriteria(String paramString, ProviderProperties paramProviderProperties, Criteria paramCriteria) {
    if ("passive".equals(paramString))
      return false; 
    if (paramProviderProperties == null)
      return false; 
    if (paramCriteria.getAccuracy() != 0 && 
      paramCriteria.getAccuracy() < paramProviderProperties.mAccuracy)
      return false; 
    if (paramCriteria.getPowerRequirement() != 0 && 
      paramCriteria.getPowerRequirement() < paramProviderProperties.mPowerRequirement)
      return false; 
    if (paramCriteria.isAltitudeRequired() && !paramProviderProperties.mSupportsAltitude)
      return false; 
    if (paramCriteria.isSpeedRequired() && !paramProviderProperties.mSupportsSpeed)
      return false; 
    if (paramCriteria.isBearingRequired() && !paramProviderProperties.mSupportsBearing)
      return false; 
    if (!paramCriteria.isCostAllowed() && paramProviderProperties.mHasMonetaryCost)
      return false; 
    return true;
  }
  
  public boolean requiresNetwork() {
    return this.mProperties.mRequiresNetwork;
  }
  
  public boolean requiresSatellite() {
    return this.mProperties.mRequiresSatellite;
  }
  
  public boolean requiresCell() {
    return this.mProperties.mRequiresCell;
  }
  
  public boolean hasMonetaryCost() {
    return this.mProperties.mHasMonetaryCost;
  }
  
  public boolean supportsAltitude() {
    return this.mProperties.mSupportsAltitude;
  }
  
  public boolean supportsSpeed() {
    return this.mProperties.mSupportsSpeed;
  }
  
  public boolean supportsBearing() {
    return this.mProperties.mSupportsBearing;
  }
  
  public int getPowerRequirement() {
    return this.mProperties.mPowerRequirement;
  }
  
  public int getAccuracy() {
    return this.mProperties.mAccuracy;
  }
}
