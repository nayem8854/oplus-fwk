package android.location;

import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import java.util.HashMap;

public class CountryDetector {
  private static final String TAG = "CountryDetector";
  
  private final HashMap<CountryListener, ListenerTransport> mListeners;
  
  private final ICountryDetector mService;
  
  class ListenerTransport extends ICountryListener.Stub {
    private final Handler mHandler;
    
    private final CountryListener mListener;
    
    public ListenerTransport(CountryDetector this$0, Looper param1Looper) {
      this.mListener = (CountryListener)this$0;
      if (param1Looper != null) {
        this.mHandler = new Handler(param1Looper);
      } else {
        this.mHandler = new Handler();
      } 
    }
    
    public void onCountryDetected(final Country country) {
      this.mHandler.post(new Runnable() {
            final CountryDetector.ListenerTransport this$0;
            
            final Country val$country;
            
            public void run() {
              this.this$0.mListener.onCountryDetected(country);
            }
          });
    }
  }
  
  class null implements Runnable {
    final CountryDetector.ListenerTransport this$0;
    
    final Country val$country;
    
    public void run() {
      this.this$0.mListener.onCountryDetected(country);
    }
  }
  
  public CountryDetector(ICountryDetector paramICountryDetector) {
    this.mService = paramICountryDetector;
    this.mListeners = new HashMap<>();
  }
  
  public Country detectCountry() {
    try {
      return this.mService.detectCountry();
    } catch (RemoteException remoteException) {
      Log.e("CountryDetector", "detectCountry: RemoteException", (Throwable)remoteException);
      return null;
    } 
  }
  
  public void addCountryListener(CountryListener paramCountryListener, Looper paramLooper) {
    synchronized (this.mListeners) {
      if (!this.mListeners.containsKey(paramCountryListener)) {
        ListenerTransport listenerTransport = new ListenerTransport();
        this(paramCountryListener, paramLooper);
        try {
          this.mService.addCountryListener(listenerTransport);
          this.mListeners.put(paramCountryListener, listenerTransport);
        } catch (RemoteException remoteException) {
          Log.e("CountryDetector", "addCountryListener: RemoteException", (Throwable)remoteException);
        } 
      } 
      return;
    } 
  }
  
  public void removeCountryListener(CountryListener paramCountryListener) {
    synchronized (this.mListeners) {
      ListenerTransport listenerTransport = this.mListeners.get(paramCountryListener);
      if (listenerTransport != null)
        try {
          this.mListeners.remove(paramCountryListener);
          this.mService.removeCountryListener(listenerTransport);
        } catch (RemoteException remoteException) {
          Log.e("CountryDetector", "removeCountryListener: RemoteException", (Throwable)remoteException);
        }  
      return;
    } 
  }
}
