package android.location;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import java.util.Locale;

public class Country implements Parcelable {
  public static final int COUNTRY_SOURCE_LOCALE = 3;
  
  public static final int COUNTRY_SOURCE_LOCATION = 1;
  
  public static final int COUNTRY_SOURCE_NETWORK = 0;
  
  public static final int COUNTRY_SOURCE_SIM = 2;
  
  public Country(String paramString, int paramInt) {
    if (paramString != null && paramInt >= 0 && paramInt <= 3) {
      this.mCountryIso = paramString.toUpperCase(Locale.US);
      this.mSource = paramInt;
      this.mTimestamp = SystemClock.elapsedRealtime();
      return;
    } 
    throw new IllegalArgumentException();
  }
  
  private Country(String paramString, int paramInt, long paramLong) {
    if (paramString != null && paramInt >= 0 && paramInt <= 3) {
      this.mCountryIso = paramString.toUpperCase(Locale.US);
      this.mSource = paramInt;
      this.mTimestamp = paramLong;
      return;
    } 
    throw new IllegalArgumentException();
  }
  
  public Country(Country paramCountry) {
    this.mCountryIso = paramCountry.mCountryIso;
    this.mSource = paramCountry.mSource;
    this.mTimestamp = paramCountry.mTimestamp;
  }
  
  public final String getCountryIso() {
    return this.mCountryIso;
  }
  
  public final int getSource() {
    return this.mSource;
  }
  
  public final long getTimestamp() {
    return this.mTimestamp;
  }
  
  public static final Parcelable.Creator<Country> CREATOR = new Parcelable.Creator<Country>() {
      public Country createFromParcel(Parcel param1Parcel) {
        return new Country(param1Parcel.readString(), param1Parcel.readInt(), param1Parcel.readLong());
      }
      
      public Country[] newArray(int param1Int) {
        return new Country[param1Int];
      }
    };
  
  private final String mCountryIso;
  
  private int mHashCode;
  
  private final int mSource;
  
  private final long mTimestamp;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mCountryIso);
    paramParcel.writeInt(this.mSource);
    paramParcel.writeLong(this.mTimestamp);
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (paramObject == this)
      return true; 
    if (paramObject instanceof Country) {
      paramObject = paramObject;
      if (!this.mCountryIso.equals(paramObject.getCountryIso()) || this.mSource != paramObject.getSource())
        bool = false; 
      return bool;
    } 
    return false;
  }
  
  public int hashCode() {
    int i = this.mHashCode;
    if (i == 0) {
      i = this.mCountryIso.hashCode();
      int j = this.mSource;
      this.mHashCode = (17 * 13 + i) * 13 + j;
    } 
    return this.mHashCode;
  }
  
  public boolean equalsIgnoreSource(Country paramCountry) {
    boolean bool;
    if (paramCountry != null && this.mCountryIso.equals(paramCountry.getCountryIso())) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Country {ISO=");
    stringBuilder.append(this.mCountryIso);
    stringBuilder.append(", source=");
    stringBuilder.append(this.mSource);
    stringBuilder.append(", time=");
    stringBuilder.append(this.mTimestamp);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
}
