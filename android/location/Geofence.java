package android.location;

import android.os.Parcel;
import android.os.Parcelable;

public final class Geofence implements Parcelable {
  public static Geofence createCircle(double paramDouble1, double paramDouble2, float paramFloat) {
    return new Geofence(paramDouble1, paramDouble2, paramFloat);
  }
  
  private Geofence(double paramDouble1, double paramDouble2, float paramFloat) {
    checkRadius(paramFloat);
    checkLatLong(paramDouble1, paramDouble2);
    this.mType = 1;
    this.mLatitude = paramDouble1;
    this.mLongitude = paramDouble2;
    this.mRadius = paramFloat;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public double getLatitude() {
    return this.mLatitude;
  }
  
  public double getLongitude() {
    return this.mLongitude;
  }
  
  public float getRadius() {
    return this.mRadius;
  }
  
  private static void checkRadius(float paramFloat) {
    if (paramFloat > 0.0F)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("invalid radius: ");
    stringBuilder.append(paramFloat);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private static void checkLatLong(double paramDouble1, double paramDouble2) {
    if (paramDouble1 <= 90.0D && paramDouble1 >= -90.0D) {
      if (paramDouble2 <= 180.0D && paramDouble2 >= -180.0D)
        return; 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("invalid longitude: ");
      stringBuilder1.append(paramDouble2);
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("invalid latitude: ");
    stringBuilder.append(paramDouble1);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private static void checkType(int paramInt) {
    if (paramInt == 1)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("invalid type: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static final Parcelable.Creator<Geofence> CREATOR = new Parcelable.Creator<Geofence>() {
      public Geofence createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        double d1 = param1Parcel.readDouble();
        double d2 = param1Parcel.readDouble();
        float f = param1Parcel.readFloat();
        Geofence.checkType(i);
        return Geofence.createCircle(d1, d2, f);
      }
      
      public Geofence[] newArray(int param1Int) {
        return new Geofence[param1Int];
      }
    };
  
  public static final int TYPE_HORIZONTAL_CIRCLE = 1;
  
  private final double mLatitude;
  
  private final double mLongitude;
  
  private final float mRadius;
  
  private final int mType;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mType);
    paramParcel.writeDouble(this.mLatitude);
    paramParcel.writeDouble(this.mLongitude);
    paramParcel.writeFloat(this.mRadius);
  }
  
  private static String typeToString(int paramInt) {
    if (paramInt != 1) {
      checkType(paramInt);
      return null;
    } 
    return "CIRCLE";
  }
  
  public String toString() {
    int i = this.mType;
    String str = typeToString(i);
    double d1 = this.mLatitude, d2 = this.mLongitude;
    float f = this.mRadius;
    return String.format("Geofence[%s %.6f, %.6f %.0fm]", new Object[] { str, Double.valueOf(d1), Double.valueOf(d2), Float.valueOf(f) });
  }
  
  public int hashCode() {
    long l = Double.doubleToLongBits(this.mLatitude);
    int i = (int)(l >>> 32L ^ l);
    l = Double.doubleToLongBits(this.mLongitude);
    int j = (int)(l >>> 32L ^ l);
    int k = Float.floatToIntBits(this.mRadius);
    int m = this.mType;
    return (((1 * 31 + i) * 31 + j) * 31 + k) * 31 + m;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (!(paramObject instanceof Geofence))
      return false; 
    paramObject = paramObject;
    if (this.mRadius != ((Geofence)paramObject).mRadius)
      return false; 
    if (this.mLatitude != ((Geofence)paramObject).mLatitude)
      return false; 
    if (this.mLongitude != ((Geofence)paramObject).mLongitude)
      return false; 
    if (this.mType != ((Geofence)paramObject).mType)
      return false; 
    return true;
  }
}
