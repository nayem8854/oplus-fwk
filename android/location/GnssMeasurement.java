package android.location;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class GnssMeasurement implements Parcelable {
  public static final int ADR_STATE_ALL = 31;
  
  public static final int ADR_STATE_CYCLE_SLIP = 4;
  
  public static final int ADR_STATE_HALF_CYCLE_REPORTED = 16;
  
  public static final int ADR_STATE_HALF_CYCLE_RESOLVED = 8;
  
  public static final int ADR_STATE_RESET = 2;
  
  public static final int ADR_STATE_UNKNOWN = 0;
  
  public static final int ADR_STATE_VALID = 1;
  
  public GnssMeasurement() {
    initialize();
  }
  
  public void set(GnssMeasurement paramGnssMeasurement) {
    this.mFlags = paramGnssMeasurement.mFlags;
    this.mSvid = paramGnssMeasurement.mSvid;
    this.mConstellationType = paramGnssMeasurement.mConstellationType;
    this.mTimeOffsetNanos = paramGnssMeasurement.mTimeOffsetNanos;
    this.mState = paramGnssMeasurement.mState;
    this.mReceivedSvTimeNanos = paramGnssMeasurement.mReceivedSvTimeNanos;
    this.mReceivedSvTimeUncertaintyNanos = paramGnssMeasurement.mReceivedSvTimeUncertaintyNanos;
    this.mCn0DbHz = paramGnssMeasurement.mCn0DbHz;
    this.mBasebandCn0DbHz = paramGnssMeasurement.mBasebandCn0DbHz;
    this.mPseudorangeRateMetersPerSecond = paramGnssMeasurement.mPseudorangeRateMetersPerSecond;
    this.mPseudorangeRateUncertaintyMetersPerSecond = paramGnssMeasurement.mPseudorangeRateUncertaintyMetersPerSecond;
    this.mAccumulatedDeltaRangeState = paramGnssMeasurement.mAccumulatedDeltaRangeState;
    this.mAccumulatedDeltaRangeMeters = paramGnssMeasurement.mAccumulatedDeltaRangeMeters;
    this.mAccumulatedDeltaRangeUncertaintyMeters = paramGnssMeasurement.mAccumulatedDeltaRangeUncertaintyMeters;
    this.mCarrierFrequencyHz = paramGnssMeasurement.mCarrierFrequencyHz;
    this.mCarrierCycles = paramGnssMeasurement.mCarrierCycles;
    this.mCarrierPhase = paramGnssMeasurement.mCarrierPhase;
    this.mCarrierPhaseUncertainty = paramGnssMeasurement.mCarrierPhaseUncertainty;
    this.mMultipathIndicator = paramGnssMeasurement.mMultipathIndicator;
    this.mSnrInDb = paramGnssMeasurement.mSnrInDb;
    this.mAutomaticGainControlLevelInDb = paramGnssMeasurement.mAutomaticGainControlLevelInDb;
    this.mCodeType = paramGnssMeasurement.mCodeType;
    this.mFullInterSignalBiasNanos = paramGnssMeasurement.mFullInterSignalBiasNanos;
    this.mFullInterSignalBiasUncertaintyNanos = paramGnssMeasurement.mFullInterSignalBiasUncertaintyNanos;
    this.mSatelliteInterSignalBiasNanos = paramGnssMeasurement.mSatelliteInterSignalBiasNanos;
    this.mSatelliteInterSignalBiasUncertaintyNanos = paramGnssMeasurement.mSatelliteInterSignalBiasUncertaintyNanos;
  }
  
  public void reset() {
    initialize();
  }
  
  public int getSvid() {
    return this.mSvid;
  }
  
  public void setSvid(int paramInt) {
    this.mSvid = paramInt;
  }
  
  public int getConstellationType() {
    return this.mConstellationType;
  }
  
  public void setConstellationType(int paramInt) {
    this.mConstellationType = paramInt;
  }
  
  public double getTimeOffsetNanos() {
    return this.mTimeOffsetNanos;
  }
  
  public void setTimeOffsetNanos(double paramDouble) {
    this.mTimeOffsetNanos = paramDouble;
  }
  
  public int getState() {
    return this.mState;
  }
  
  public void setState(int paramInt) {
    this.mState = paramInt;
  }
  
  private String getStateString() {
    if (this.mState == 0)
      return "Unknown"; 
    StringBuilder stringBuilder = new StringBuilder();
    if ((this.mState & 0x1) != 0)
      stringBuilder.append("CodeLock|"); 
    if ((this.mState & 0x2) != 0)
      stringBuilder.append("BitSync|"); 
    if ((this.mState & 0x4) != 0)
      stringBuilder.append("SubframeSync|"); 
    if ((this.mState & 0x8) != 0)
      stringBuilder.append("TowDecoded|"); 
    if ((this.mState & 0x4000) != 0)
      stringBuilder.append("TowKnown|"); 
    if ((this.mState & 0x10) != 0)
      stringBuilder.append("MsecAmbiguous|"); 
    if ((this.mState & 0x20) != 0)
      stringBuilder.append("SymbolSync|"); 
    if ((this.mState & 0x40) != 0)
      stringBuilder.append("GloStringSync|"); 
    if ((this.mState & 0x80) != 0)
      stringBuilder.append("GloTodDecoded|"); 
    if ((this.mState & 0x8000) != 0)
      stringBuilder.append("GloTodKnown|"); 
    if ((this.mState & 0x100) != 0)
      stringBuilder.append("BdsD2BitSync|"); 
    if ((this.mState & 0x200) != 0)
      stringBuilder.append("BdsD2SubframeSync|"); 
    if ((this.mState & 0x400) != 0)
      stringBuilder.append("GalE1bcCodeLock|"); 
    if ((this.mState & 0x800) != 0)
      stringBuilder.append("E1c2ndCodeLock|"); 
    if ((this.mState & 0x1000) != 0)
      stringBuilder.append("GalE1bPageSync|"); 
    if ((this.mState & 0x2000) != 0)
      stringBuilder.append("SbasSync|"); 
    if ((this.mState & 0x10000) != 0)
      stringBuilder.append("2ndCodeLock|"); 
    int i = this.mState & 0xFFFFC000;
    if (i > 0) {
      stringBuilder.append("Other(");
      stringBuilder.append(Integer.toBinaryString(i));
      stringBuilder.append(")|");
    } 
    stringBuilder.setLength(stringBuilder.length() - 1);
    return stringBuilder.toString();
  }
  
  public long getReceivedSvTimeNanos() {
    return this.mReceivedSvTimeNanos;
  }
  
  public void setReceivedSvTimeNanos(long paramLong) {
    this.mReceivedSvTimeNanos = paramLong;
  }
  
  public long getReceivedSvTimeUncertaintyNanos() {
    return this.mReceivedSvTimeUncertaintyNanos;
  }
  
  public void setReceivedSvTimeUncertaintyNanos(long paramLong) {
    this.mReceivedSvTimeUncertaintyNanos = paramLong;
  }
  
  public double getCn0DbHz() {
    return this.mCn0DbHz;
  }
  
  public void setCn0DbHz(double paramDouble) {
    this.mCn0DbHz = paramDouble;
  }
  
  public boolean hasBasebandCn0DbHz() {
    return isFlagSet(32768);
  }
  
  public double getBasebandCn0DbHz() {
    return this.mBasebandCn0DbHz;
  }
  
  public void setBasebandCn0DbHz(double paramDouble) {
    setFlag(32768);
    this.mBasebandCn0DbHz = paramDouble;
  }
  
  public void resetBasebandCn0DbHz() {
    resetFlag(32768);
  }
  
  public double getPseudorangeRateMetersPerSecond() {
    return this.mPseudorangeRateMetersPerSecond;
  }
  
  public void setPseudorangeRateMetersPerSecond(double paramDouble) {
    this.mPseudorangeRateMetersPerSecond = paramDouble;
  }
  
  public double getPseudorangeRateUncertaintyMetersPerSecond() {
    return this.mPseudorangeRateUncertaintyMetersPerSecond;
  }
  
  public void setPseudorangeRateUncertaintyMetersPerSecond(double paramDouble) {
    this.mPseudorangeRateUncertaintyMetersPerSecond = paramDouble;
  }
  
  public int getAccumulatedDeltaRangeState() {
    return this.mAccumulatedDeltaRangeState;
  }
  
  public void setAccumulatedDeltaRangeState(int paramInt) {
    this.mAccumulatedDeltaRangeState = paramInt;
  }
  
  private String getAccumulatedDeltaRangeStateString() {
    if (this.mAccumulatedDeltaRangeState == 0)
      return "Unknown"; 
    StringBuilder stringBuilder = new StringBuilder();
    if ((this.mAccumulatedDeltaRangeState & 0x1) == 1)
      stringBuilder.append("Valid|"); 
    if ((this.mAccumulatedDeltaRangeState & 0x2) == 2)
      stringBuilder.append("Reset|"); 
    if ((this.mAccumulatedDeltaRangeState & 0x4) == 4)
      stringBuilder.append("CycleSlip|"); 
    if ((this.mAccumulatedDeltaRangeState & 0x8) == 8)
      stringBuilder.append("HalfCycleResolved|"); 
    if ((this.mAccumulatedDeltaRangeState & 0x10) == 16)
      stringBuilder.append("HalfCycleReported|"); 
    int i = this.mAccumulatedDeltaRangeState & 0xFFFFFFE0;
    if (i > 0) {
      stringBuilder.append("Other(");
      stringBuilder.append(Integer.toBinaryString(i));
      stringBuilder.append(")|");
    } 
    stringBuilder.deleteCharAt(stringBuilder.length() - 1);
    return stringBuilder.toString();
  }
  
  public double getAccumulatedDeltaRangeMeters() {
    return this.mAccumulatedDeltaRangeMeters;
  }
  
  public void setAccumulatedDeltaRangeMeters(double paramDouble) {
    this.mAccumulatedDeltaRangeMeters = paramDouble;
  }
  
  public double getAccumulatedDeltaRangeUncertaintyMeters() {
    return this.mAccumulatedDeltaRangeUncertaintyMeters;
  }
  
  public void setAccumulatedDeltaRangeUncertaintyMeters(double paramDouble) {
    this.mAccumulatedDeltaRangeUncertaintyMeters = paramDouble;
  }
  
  public boolean hasCarrierFrequencyHz() {
    return isFlagSet(512);
  }
  
  public float getCarrierFrequencyHz() {
    return this.mCarrierFrequencyHz;
  }
  
  public void setCarrierFrequencyHz(float paramFloat) {
    setFlag(512);
    this.mCarrierFrequencyHz = paramFloat;
  }
  
  public void resetCarrierFrequencyHz() {
    resetFlag(512);
    this.mCarrierFrequencyHz = Float.NaN;
  }
  
  @Deprecated
  public boolean hasCarrierCycles() {
    return isFlagSet(1024);
  }
  
  @Deprecated
  public long getCarrierCycles() {
    return this.mCarrierCycles;
  }
  
  @Deprecated
  public void setCarrierCycles(long paramLong) {
    setFlag(1024);
    this.mCarrierCycles = paramLong;
  }
  
  @Deprecated
  public void resetCarrierCycles() {
    resetFlag(1024);
    this.mCarrierCycles = Long.MIN_VALUE;
  }
  
  @Deprecated
  public boolean hasCarrierPhase() {
    return isFlagSet(2048);
  }
  
  @Deprecated
  public double getCarrierPhase() {
    return this.mCarrierPhase;
  }
  
  @Deprecated
  public void setCarrierPhase(double paramDouble) {
    setFlag(2048);
    this.mCarrierPhase = paramDouble;
  }
  
  @Deprecated
  public void resetCarrierPhase() {
    resetFlag(2048);
  }
  
  @Deprecated
  public boolean hasCarrierPhaseUncertainty() {
    return isFlagSet(4096);
  }
  
  @Deprecated
  public double getCarrierPhaseUncertainty() {
    return this.mCarrierPhaseUncertainty;
  }
  
  @Deprecated
  public void setCarrierPhaseUncertainty(double paramDouble) {
    setFlag(4096);
    this.mCarrierPhaseUncertainty = paramDouble;
  }
  
  @Deprecated
  public void resetCarrierPhaseUncertainty() {
    resetFlag(4096);
  }
  
  public int getMultipathIndicator() {
    return this.mMultipathIndicator;
  }
  
  public void setMultipathIndicator(int paramInt) {
    this.mMultipathIndicator = paramInt;
  }
  
  private String getMultipathIndicatorString() {
    int i = this.mMultipathIndicator;
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("<Invalid: ");
          stringBuilder.append(this.mMultipathIndicator);
          stringBuilder.append(">");
          return stringBuilder.toString();
        } 
        return "NotDetected";
      } 
      return "Detected";
    } 
    return "Unknown";
  }
  
  public boolean hasSnrInDb() {
    return isFlagSet(1);
  }
  
  public double getSnrInDb() {
    return this.mSnrInDb;
  }
  
  public void setSnrInDb(double paramDouble) {
    setFlag(1);
    this.mSnrInDb = paramDouble;
  }
  
  public void resetSnrInDb() {
    resetFlag(1);
  }
  
  public boolean hasAutomaticGainControlLevelDb() {
    return isFlagSet(8192);
  }
  
  public double getAutomaticGainControlLevelDb() {
    return this.mAutomaticGainControlLevelInDb;
  }
  
  public void setAutomaticGainControlLevelInDb(double paramDouble) {
    setFlag(8192);
    this.mAutomaticGainControlLevelInDb = paramDouble;
  }
  
  public void resetAutomaticGainControlLevel() {
    resetFlag(8192);
  }
  
  public boolean hasCodeType() {
    return isFlagSet(16384);
  }
  
  public String getCodeType() {
    return this.mCodeType;
  }
  
  public void setCodeType(String paramString) {
    setFlag(16384);
    this.mCodeType = paramString;
  }
  
  public void resetCodeType() {
    resetFlag(16384);
    this.mCodeType = "UNKNOWN";
  }
  
  public boolean hasFullInterSignalBiasNanos() {
    return isFlagSet(65536);
  }
  
  public double getFullInterSignalBiasNanos() {
    return this.mFullInterSignalBiasNanos;
  }
  
  public void setFullInterSignalBiasNanos(double paramDouble) {
    setFlag(65536);
    this.mFullInterSignalBiasNanos = paramDouble;
  }
  
  public void resetFullInterSignalBiasNanos() {
    resetFlag(65536);
  }
  
  public boolean hasFullInterSignalBiasUncertaintyNanos() {
    return isFlagSet(131072);
  }
  
  public double getFullInterSignalBiasUncertaintyNanos() {
    return this.mFullInterSignalBiasUncertaintyNanos;
  }
  
  public void setFullInterSignalBiasUncertaintyNanos(double paramDouble) {
    setFlag(131072);
    this.mFullInterSignalBiasUncertaintyNanos = paramDouble;
  }
  
  public void resetFullInterSignalBiasUncertaintyNanos() {
    resetFlag(131072);
  }
  
  public boolean hasSatelliteInterSignalBiasNanos() {
    return isFlagSet(262144);
  }
  
  public double getSatelliteInterSignalBiasNanos() {
    return this.mSatelliteInterSignalBiasNanos;
  }
  
  public void setSatelliteInterSignalBiasNanos(double paramDouble) {
    setFlag(262144);
    this.mSatelliteInterSignalBiasNanos = paramDouble;
  }
  
  public void resetSatelliteInterSignalBiasNanos() {
    resetFlag(262144);
  }
  
  public boolean hasSatelliteInterSignalBiasUncertaintyNanos() {
    return isFlagSet(524288);
  }
  
  public double getSatelliteInterSignalBiasUncertaintyNanos() {
    return this.mSatelliteInterSignalBiasUncertaintyNanos;
  }
  
  public void setSatelliteInterSignalBiasUncertaintyNanos(double paramDouble) {
    setFlag(524288);
    this.mSatelliteInterSignalBiasUncertaintyNanos = paramDouble;
  }
  
  public void resetSatelliteInterSignalBiasUncertaintyNanos() {
    resetFlag(524288);
  }
  
  public static final Parcelable.Creator<GnssMeasurement> CREATOR = new Parcelable.Creator<GnssMeasurement>() {
      public GnssMeasurement createFromParcel(Parcel param1Parcel) {
        GnssMeasurement gnssMeasurement = new GnssMeasurement();
        GnssMeasurement.access$002(gnssMeasurement, param1Parcel.readInt());
        GnssMeasurement.access$102(gnssMeasurement, param1Parcel.readInt());
        GnssMeasurement.access$202(gnssMeasurement, param1Parcel.readInt());
        GnssMeasurement.access$302(gnssMeasurement, param1Parcel.readDouble());
        GnssMeasurement.access$402(gnssMeasurement, param1Parcel.readInt());
        GnssMeasurement.access$502(gnssMeasurement, param1Parcel.readLong());
        GnssMeasurement.access$602(gnssMeasurement, param1Parcel.readLong());
        GnssMeasurement.access$702(gnssMeasurement, param1Parcel.readDouble());
        GnssMeasurement.access$802(gnssMeasurement, param1Parcel.readDouble());
        GnssMeasurement.access$902(gnssMeasurement, param1Parcel.readDouble());
        GnssMeasurement.access$1002(gnssMeasurement, param1Parcel.readInt());
        GnssMeasurement.access$1102(gnssMeasurement, param1Parcel.readDouble());
        GnssMeasurement.access$1202(gnssMeasurement, param1Parcel.readDouble());
        GnssMeasurement.access$1302(gnssMeasurement, param1Parcel.readFloat());
        GnssMeasurement.access$1402(gnssMeasurement, param1Parcel.readLong());
        GnssMeasurement.access$1502(gnssMeasurement, param1Parcel.readDouble());
        GnssMeasurement.access$1602(gnssMeasurement, param1Parcel.readDouble());
        GnssMeasurement.access$1702(gnssMeasurement, param1Parcel.readInt());
        GnssMeasurement.access$1802(gnssMeasurement, param1Parcel.readDouble());
        GnssMeasurement.access$1902(gnssMeasurement, param1Parcel.readDouble());
        GnssMeasurement.access$2002(gnssMeasurement, param1Parcel.readString());
        GnssMeasurement.access$2102(gnssMeasurement, param1Parcel.readDouble());
        GnssMeasurement.access$2202(gnssMeasurement, param1Parcel.readDouble());
        GnssMeasurement.access$2302(gnssMeasurement, param1Parcel.readDouble());
        GnssMeasurement.access$2402(gnssMeasurement, param1Parcel.readDouble());
        GnssMeasurement.access$2502(gnssMeasurement, param1Parcel.readDouble());
        return gnssMeasurement;
      }
      
      public GnssMeasurement[] newArray(int param1Int) {
        return new GnssMeasurement[param1Int];
      }
    };
  
  private static final int HAS_BASEBAND_CN0 = 32768;
  
  private static final int HAS_CODE_TYPE = 16384;
  
  private static final int HAS_NO_FLAGS = 0;
  
  public static final int MULTIPATH_INDICATOR_DETECTED = 1;
  
  public static final int MULTIPATH_INDICATOR_NOT_DETECTED = 2;
  
  public static final int MULTIPATH_INDICATOR_UNKNOWN = 0;
  
  public static final int STATE_2ND_CODE_LOCK = 65536;
  
  private static final int STATE_ALL = 16383;
  
  public static final int STATE_BDS_D2_BIT_SYNC = 256;
  
  public static final int STATE_BDS_D2_SUBFRAME_SYNC = 512;
  
  public static final int STATE_BIT_SYNC = 2;
  
  public static final int STATE_CODE_LOCK = 1;
  
  public static final int STATE_GAL_E1BC_CODE_LOCK = 1024;
  
  public static final int STATE_GAL_E1B_PAGE_SYNC = 4096;
  
  public static final int STATE_GAL_E1C_2ND_CODE_LOCK = 2048;
  
  public static final int STATE_GLO_STRING_SYNC = 64;
  
  public static final int STATE_GLO_TOD_DECODED = 128;
  
  public static final int STATE_GLO_TOD_KNOWN = 32768;
  
  public static final int STATE_MSEC_AMBIGUOUS = 16;
  
  public static final int STATE_SBAS_SYNC = 8192;
  
  public static final int STATE_SUBFRAME_SYNC = 4;
  
  public static final int STATE_SYMBOL_SYNC = 32;
  
  public static final int STATE_TOW_DECODED = 8;
  
  public static final int STATE_TOW_KNOWN = 16384;
  
  public static final int STATE_UNKNOWN = 0;
  
  private double mAccumulatedDeltaRangeMeters;
  
  private int mAccumulatedDeltaRangeState;
  
  private double mAccumulatedDeltaRangeUncertaintyMeters;
  
  private double mAutomaticGainControlLevelInDb;
  
  private double mBasebandCn0DbHz;
  
  private long mCarrierCycles;
  
  private float mCarrierFrequencyHz;
  
  private double mCarrierPhase;
  
  private double mCarrierPhaseUncertainty;
  
  private double mCn0DbHz;
  
  private String mCodeType;
  
  private int mConstellationType;
  
  private int mFlags;
  
  private double mFullInterSignalBiasNanos;
  
  private double mFullInterSignalBiasUncertaintyNanos;
  
  private int mMultipathIndicator;
  
  private double mPseudorangeRateMetersPerSecond;
  
  private double mPseudorangeRateUncertaintyMetersPerSecond;
  
  private long mReceivedSvTimeNanos;
  
  private long mReceivedSvTimeUncertaintyNanos;
  
  private double mSatelliteInterSignalBiasNanos;
  
  private double mSatelliteInterSignalBiasUncertaintyNanos;
  
  private double mSnrInDb;
  
  private int mState;
  
  private int mSvid;
  
  private double mTimeOffsetNanos;
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mFlags);
    paramParcel.writeInt(this.mSvid);
    paramParcel.writeInt(this.mConstellationType);
    paramParcel.writeDouble(this.mTimeOffsetNanos);
    paramParcel.writeInt(this.mState);
    paramParcel.writeLong(this.mReceivedSvTimeNanos);
    paramParcel.writeLong(this.mReceivedSvTimeUncertaintyNanos);
    paramParcel.writeDouble(this.mCn0DbHz);
    paramParcel.writeDouble(this.mPseudorangeRateMetersPerSecond);
    paramParcel.writeDouble(this.mPseudorangeRateUncertaintyMetersPerSecond);
    paramParcel.writeInt(this.mAccumulatedDeltaRangeState);
    paramParcel.writeDouble(this.mAccumulatedDeltaRangeMeters);
    paramParcel.writeDouble(this.mAccumulatedDeltaRangeUncertaintyMeters);
    paramParcel.writeFloat(this.mCarrierFrequencyHz);
    paramParcel.writeLong(this.mCarrierCycles);
    paramParcel.writeDouble(this.mCarrierPhase);
    paramParcel.writeDouble(this.mCarrierPhaseUncertainty);
    paramParcel.writeInt(this.mMultipathIndicator);
    paramParcel.writeDouble(this.mSnrInDb);
    paramParcel.writeDouble(this.mAutomaticGainControlLevelInDb);
    paramParcel.writeString(this.mCodeType);
    paramParcel.writeDouble(this.mBasebandCn0DbHz);
    paramParcel.writeDouble(this.mFullInterSignalBiasNanos);
    paramParcel.writeDouble(this.mFullInterSignalBiasUncertaintyNanos);
    paramParcel.writeDouble(this.mSatelliteInterSignalBiasNanos);
    paramParcel.writeDouble(this.mSatelliteInterSignalBiasUncertaintyNanos);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("GnssMeasurement:\n");
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "Svid", Integer.valueOf(this.mSvid) }));
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "ConstellationType", Integer.valueOf(this.mConstellationType) }));
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "TimeOffsetNanos", Double.valueOf(this.mTimeOffsetNanos) }));
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "State", getStateString() }));
    long l1 = this.mReceivedSvTimeNanos;
    long l2 = this.mReceivedSvTimeUncertaintyNanos;
    stringBuilder.append(String.format("   %-29s = %-25s   %-40s = %s\n", new Object[] { "ReceivedSvTimeNanos", Long.valueOf(l1), "ReceivedSvTimeUncertaintyNanos", Long.valueOf(l2) }));
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "Cn0DbHz", Double.valueOf(this.mCn0DbHz) }));
    if (hasBasebandCn0DbHz())
      stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "BasebandCn0DbHz", Double.valueOf(this.mBasebandCn0DbHz) })); 
    double d1 = this.mPseudorangeRateMetersPerSecond;
    double d2 = this.mPseudorangeRateUncertaintyMetersPerSecond;
    stringBuilder.append(String.format("   %-29s = %-25s   %-40s = %s\n", new Object[] { "PseudorangeRateMetersPerSecond", Double.valueOf(d1), "PseudorangeRateUncertaintyMetersPerSecond", Double.valueOf(d2) }));
    String str = getAccumulatedDeltaRangeStateString();
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "AccumulatedDeltaRangeState", str }));
    d1 = this.mAccumulatedDeltaRangeMeters;
    d2 = this.mAccumulatedDeltaRangeUncertaintyMeters;
    stringBuilder.append(String.format("   %-29s = %-25s   %-40s = %s\n", new Object[] { "AccumulatedDeltaRangeMeters", Double.valueOf(d1), "AccumulatedDeltaRangeUncertaintyMeters", Double.valueOf(d2) }));
    if (hasCarrierFrequencyHz())
      stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "CarrierFrequencyHz", Float.valueOf(this.mCarrierFrequencyHz) })); 
    if (hasCarrierCycles())
      stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "CarrierCycles", Long.valueOf(this.mCarrierCycles) })); 
    boolean bool = hasCarrierPhase();
    Object object = null;
    if (bool || hasCarrierPhaseUncertainty()) {
      Object object1;
      if (hasCarrierPhase()) {
        Double double_ = Double.valueOf(this.mCarrierPhase);
      } else {
        str = null;
      } 
      if (hasCarrierPhaseUncertainty()) {
        object1 = Double.valueOf(this.mCarrierPhaseUncertainty);
      } else {
        object1 = null;
      } 
      stringBuilder.append(String.format("   %-29s = %-25s   %-40s = %s\n", new Object[] { "CarrierPhase", str, "CarrierPhaseUncertainty", object1 }));
    } 
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "MultipathIndicator", getMultipathIndicatorString() }));
    if (hasSnrInDb())
      stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "SnrInDb", Double.valueOf(this.mSnrInDb) })); 
    if (hasAutomaticGainControlLevelDb())
      stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "AgcLevelDb", Double.valueOf(this.mAutomaticGainControlLevelInDb) })); 
    if (hasCodeType())
      stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "CodeType", this.mCodeType })); 
    if (hasFullInterSignalBiasNanos() || hasFullInterSignalBiasUncertaintyNanos()) {
      Object object1;
      if (hasFullInterSignalBiasNanos()) {
        Double double_ = Double.valueOf(this.mFullInterSignalBiasNanos);
      } else {
        str = null;
      } 
      if (hasFullInterSignalBiasUncertaintyNanos()) {
        object1 = Double.valueOf(this.mFullInterSignalBiasUncertaintyNanos);
      } else {
        object1 = null;
      } 
      stringBuilder.append(String.format("   %-29s = %-25s   %-40s = %s\n", new Object[] { "InterSignalBiasNs", str, "InterSignalBiasUncertaintyNs", object1 }));
    } 
    if (hasSatelliteInterSignalBiasNanos() || hasSatelliteInterSignalBiasUncertaintyNanos()) {
      Object object1;
      if (hasSatelliteInterSignalBiasNanos()) {
        Double double_ = Double.valueOf(this.mSatelliteInterSignalBiasNanos);
      } else {
        str = null;
      } 
      if (hasSatelliteInterSignalBiasUncertaintyNanos()) {
        object1 = Double.valueOf(this.mSatelliteInterSignalBiasUncertaintyNanos);
      } else {
        object1 = object;
      } 
      stringBuilder.append(String.format("   %-29s = %-25s   %-40s = %s\n", new Object[] { "SatelliteInterSignalBiasNs", str, "SatelliteInterSignalBiasUncertaintyNs", object1 }));
    } 
    return stringBuilder.toString();
  }
  
  private void initialize() {
    this.mFlags = 0;
    setSvid(0);
    setTimeOffsetNanos(-9.223372036854776E18D);
    setState(0);
    setReceivedSvTimeNanos(Long.MIN_VALUE);
    setReceivedSvTimeUncertaintyNanos(Long.MAX_VALUE);
    setCn0DbHz(Double.MIN_VALUE);
    setPseudorangeRateMetersPerSecond(Double.MIN_VALUE);
    setPseudorangeRateUncertaintyMetersPerSecond(Double.MIN_VALUE);
    setAccumulatedDeltaRangeState(0);
    setAccumulatedDeltaRangeMeters(Double.MIN_VALUE);
    setAccumulatedDeltaRangeUncertaintyMeters(Double.MIN_VALUE);
    resetCarrierFrequencyHz();
    resetCarrierCycles();
    resetCarrierPhase();
    resetCarrierPhaseUncertainty();
    setMultipathIndicator(0);
    resetSnrInDb();
    resetAutomaticGainControlLevel();
    resetCodeType();
    resetBasebandCn0DbHz();
    resetFullInterSignalBiasNanos();
    resetFullInterSignalBiasUncertaintyNanos();
    resetSatelliteInterSignalBiasNanos();
    resetSatelliteInterSignalBiasUncertaintyNanos();
  }
  
  private void setFlag(int paramInt) {
    this.mFlags |= paramInt;
  }
  
  private void resetFlag(int paramInt) {
    this.mFlags &= paramInt ^ 0xFFFFFFFF;
  }
  
  private boolean isFlagSet(int paramInt) {
    boolean bool;
    if ((this.mFlags & paramInt) == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class AdrState implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class MultipathIndicator implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class State implements Annotation {}
}
