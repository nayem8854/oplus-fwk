package android.location;

import oplus.app.OplusCommonManager;

public abstract class OplusBaseLocationManager extends OplusCommonManager {
  public OplusBaseLocationManager() {
    super("location");
  }
}
