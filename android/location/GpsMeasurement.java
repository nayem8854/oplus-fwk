package android.location;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
@Deprecated
public class GpsMeasurement implements Parcelable {
  private static final short ADR_ALL = 7;
  
  public static final short ADR_STATE_CYCLE_SLIP = 4;
  
  public static final short ADR_STATE_RESET = 2;
  
  public static final short ADR_STATE_UNKNOWN = 0;
  
  public static final short ADR_STATE_VALID = 1;
  
  GpsMeasurement() {
    initialize();
  }
  
  public void set(GpsMeasurement paramGpsMeasurement) {
    this.mFlags = paramGpsMeasurement.mFlags;
    this.mPrn = paramGpsMeasurement.mPrn;
    this.mTimeOffsetInNs = paramGpsMeasurement.mTimeOffsetInNs;
    this.mState = paramGpsMeasurement.mState;
    this.mReceivedGpsTowInNs = paramGpsMeasurement.mReceivedGpsTowInNs;
    this.mReceivedGpsTowUncertaintyInNs = paramGpsMeasurement.mReceivedGpsTowUncertaintyInNs;
    this.mCn0InDbHz = paramGpsMeasurement.mCn0InDbHz;
    this.mPseudorangeRateInMetersPerSec = paramGpsMeasurement.mPseudorangeRateInMetersPerSec;
    this.mPseudorangeRateUncertaintyInMetersPerSec = paramGpsMeasurement.mPseudorangeRateUncertaintyInMetersPerSec;
    this.mAccumulatedDeltaRangeState = paramGpsMeasurement.mAccumulatedDeltaRangeState;
    this.mAccumulatedDeltaRangeInMeters = paramGpsMeasurement.mAccumulatedDeltaRangeInMeters;
    this.mAccumulatedDeltaRangeUncertaintyInMeters = paramGpsMeasurement.mAccumulatedDeltaRangeUncertaintyInMeters;
    this.mPseudorangeInMeters = paramGpsMeasurement.mPseudorangeInMeters;
    this.mPseudorangeUncertaintyInMeters = paramGpsMeasurement.mPseudorangeUncertaintyInMeters;
    this.mCodePhaseInChips = paramGpsMeasurement.mCodePhaseInChips;
    this.mCodePhaseUncertaintyInChips = paramGpsMeasurement.mCodePhaseUncertaintyInChips;
    this.mCarrierFrequencyInHz = paramGpsMeasurement.mCarrierFrequencyInHz;
    this.mCarrierCycles = paramGpsMeasurement.mCarrierCycles;
    this.mCarrierPhase = paramGpsMeasurement.mCarrierPhase;
    this.mCarrierPhaseUncertainty = paramGpsMeasurement.mCarrierPhaseUncertainty;
    this.mLossOfLock = paramGpsMeasurement.mLossOfLock;
    this.mBitNumber = paramGpsMeasurement.mBitNumber;
    this.mTimeFromLastBitInMs = paramGpsMeasurement.mTimeFromLastBitInMs;
    this.mDopplerShiftInHz = paramGpsMeasurement.mDopplerShiftInHz;
    this.mDopplerShiftUncertaintyInHz = paramGpsMeasurement.mDopplerShiftUncertaintyInHz;
    this.mMultipathIndicator = paramGpsMeasurement.mMultipathIndicator;
    this.mSnrInDb = paramGpsMeasurement.mSnrInDb;
    this.mElevationInDeg = paramGpsMeasurement.mElevationInDeg;
    this.mElevationUncertaintyInDeg = paramGpsMeasurement.mElevationUncertaintyInDeg;
    this.mAzimuthInDeg = paramGpsMeasurement.mAzimuthInDeg;
    this.mAzimuthUncertaintyInDeg = paramGpsMeasurement.mAzimuthUncertaintyInDeg;
    this.mUsedInFix = paramGpsMeasurement.mUsedInFix;
  }
  
  public void reset() {
    initialize();
  }
  
  public byte getPrn() {
    return this.mPrn;
  }
  
  public void setPrn(byte paramByte) {
    this.mPrn = paramByte;
  }
  
  public double getTimeOffsetInNs() {
    return this.mTimeOffsetInNs;
  }
  
  public void setTimeOffsetInNs(double paramDouble) {
    this.mTimeOffsetInNs = paramDouble;
  }
  
  public short getState() {
    return this.mState;
  }
  
  public void setState(short paramShort) {
    this.mState = paramShort;
  }
  
  private String getStateString() {
    if (this.mState == 0)
      return "Unknown"; 
    StringBuilder stringBuilder = new StringBuilder();
    if ((this.mState & 0x1) == 1)
      stringBuilder.append("CodeLock|"); 
    if ((this.mState & 0x2) == 2)
      stringBuilder.append("BitSync|"); 
    if ((this.mState & 0x4) == 4)
      stringBuilder.append("SubframeSync|"); 
    if ((this.mState & 0x8) == 8)
      stringBuilder.append("TowDecoded|"); 
    if ((this.mState & 0x10) == 16)
      stringBuilder.append("MsecAmbiguous"); 
    int i = this.mState & 0xFFFFFFE0;
    if (i > 0) {
      stringBuilder.append("Other(");
      stringBuilder.append(Integer.toBinaryString(i));
      stringBuilder.append(")|");
    } 
    stringBuilder.deleteCharAt(stringBuilder.length() - 1);
    return stringBuilder.toString();
  }
  
  public long getReceivedGpsTowInNs() {
    return this.mReceivedGpsTowInNs;
  }
  
  public void setReceivedGpsTowInNs(long paramLong) {
    this.mReceivedGpsTowInNs = paramLong;
  }
  
  public long getReceivedGpsTowUncertaintyInNs() {
    return this.mReceivedGpsTowUncertaintyInNs;
  }
  
  public void setReceivedGpsTowUncertaintyInNs(long paramLong) {
    this.mReceivedGpsTowUncertaintyInNs = paramLong;
  }
  
  public double getCn0InDbHz() {
    return this.mCn0InDbHz;
  }
  
  public void setCn0InDbHz(double paramDouble) {
    this.mCn0InDbHz = paramDouble;
  }
  
  public double getPseudorangeRateInMetersPerSec() {
    return this.mPseudorangeRateInMetersPerSec;
  }
  
  public void setPseudorangeRateInMetersPerSec(double paramDouble) {
    this.mPseudorangeRateInMetersPerSec = paramDouble;
  }
  
  public boolean isPseudorangeRateCorrected() {
    return isFlagSet(262144) ^ true;
  }
  
  public double getPseudorangeRateUncertaintyInMetersPerSec() {
    return this.mPseudorangeRateUncertaintyInMetersPerSec;
  }
  
  public void setPseudorangeRateUncertaintyInMetersPerSec(double paramDouble) {
    this.mPseudorangeRateUncertaintyInMetersPerSec = paramDouble;
  }
  
  public short getAccumulatedDeltaRangeState() {
    return this.mAccumulatedDeltaRangeState;
  }
  
  public void setAccumulatedDeltaRangeState(short paramShort) {
    this.mAccumulatedDeltaRangeState = paramShort;
  }
  
  private String getAccumulatedDeltaRangeStateString() {
    if (this.mAccumulatedDeltaRangeState == 0)
      return "Unknown"; 
    StringBuilder stringBuilder = new StringBuilder();
    if ((this.mAccumulatedDeltaRangeState & 0x1) == 1)
      stringBuilder.append("Valid|"); 
    if ((this.mAccumulatedDeltaRangeState & 0x2) == 2)
      stringBuilder.append("Reset|"); 
    if ((this.mAccumulatedDeltaRangeState & 0x4) == 4)
      stringBuilder.append("CycleSlip|"); 
    int i = this.mAccumulatedDeltaRangeState & 0xFFFFFFF8;
    if (i > 0) {
      stringBuilder.append("Other(");
      stringBuilder.append(Integer.toBinaryString(i));
      stringBuilder.append(")|");
    } 
    stringBuilder.deleteCharAt(stringBuilder.length() - 1);
    return stringBuilder.toString();
  }
  
  public double getAccumulatedDeltaRangeInMeters() {
    return this.mAccumulatedDeltaRangeInMeters;
  }
  
  public void setAccumulatedDeltaRangeInMeters(double paramDouble) {
    this.mAccumulatedDeltaRangeInMeters = paramDouble;
  }
  
  public double getAccumulatedDeltaRangeUncertaintyInMeters() {
    return this.mAccumulatedDeltaRangeUncertaintyInMeters;
  }
  
  public void setAccumulatedDeltaRangeUncertaintyInMeters(double paramDouble) {
    this.mAccumulatedDeltaRangeUncertaintyInMeters = paramDouble;
  }
  
  public boolean hasPseudorangeInMeters() {
    return isFlagSet(32);
  }
  
  public double getPseudorangeInMeters() {
    return this.mPseudorangeInMeters;
  }
  
  public void setPseudorangeInMeters(double paramDouble) {
    setFlag(32);
    this.mPseudorangeInMeters = paramDouble;
  }
  
  public void resetPseudorangeInMeters() {
    resetFlag(32);
    this.mPseudorangeInMeters = Double.NaN;
  }
  
  public boolean hasPseudorangeUncertaintyInMeters() {
    return isFlagSet(64);
  }
  
  public double getPseudorangeUncertaintyInMeters() {
    return this.mPseudorangeUncertaintyInMeters;
  }
  
  public void setPseudorangeUncertaintyInMeters(double paramDouble) {
    setFlag(64);
    this.mPseudorangeUncertaintyInMeters = paramDouble;
  }
  
  public void resetPseudorangeUncertaintyInMeters() {
    resetFlag(64);
    this.mPseudorangeUncertaintyInMeters = Double.NaN;
  }
  
  public boolean hasCodePhaseInChips() {
    return isFlagSet(128);
  }
  
  public double getCodePhaseInChips() {
    return this.mCodePhaseInChips;
  }
  
  public void setCodePhaseInChips(double paramDouble) {
    setFlag(128);
    this.mCodePhaseInChips = paramDouble;
  }
  
  public void resetCodePhaseInChips() {
    resetFlag(128);
    this.mCodePhaseInChips = Double.NaN;
  }
  
  public boolean hasCodePhaseUncertaintyInChips() {
    return isFlagSet(256);
  }
  
  public double getCodePhaseUncertaintyInChips() {
    return this.mCodePhaseUncertaintyInChips;
  }
  
  public void setCodePhaseUncertaintyInChips(double paramDouble) {
    setFlag(256);
    this.mCodePhaseUncertaintyInChips = paramDouble;
  }
  
  public void resetCodePhaseUncertaintyInChips() {
    resetFlag(256);
    this.mCodePhaseUncertaintyInChips = Double.NaN;
  }
  
  public boolean hasCarrierFrequencyInHz() {
    return isFlagSet(512);
  }
  
  public float getCarrierFrequencyInHz() {
    return this.mCarrierFrequencyInHz;
  }
  
  public void setCarrierFrequencyInHz(float paramFloat) {
    setFlag(512);
    this.mCarrierFrequencyInHz = paramFloat;
  }
  
  public void resetCarrierFrequencyInHz() {
    resetFlag(512);
    this.mCarrierFrequencyInHz = Float.NaN;
  }
  
  public boolean hasCarrierCycles() {
    return isFlagSet(1024);
  }
  
  public long getCarrierCycles() {
    return this.mCarrierCycles;
  }
  
  public void setCarrierCycles(long paramLong) {
    setFlag(1024);
    this.mCarrierCycles = paramLong;
  }
  
  public void resetCarrierCycles() {
    resetFlag(1024);
    this.mCarrierCycles = Long.MIN_VALUE;
  }
  
  public boolean hasCarrierPhase() {
    return isFlagSet(2048);
  }
  
  public double getCarrierPhase() {
    return this.mCarrierPhase;
  }
  
  public void setCarrierPhase(double paramDouble) {
    setFlag(2048);
    this.mCarrierPhase = paramDouble;
  }
  
  public void resetCarrierPhase() {
    resetFlag(2048);
    this.mCarrierPhase = Double.NaN;
  }
  
  public boolean hasCarrierPhaseUncertainty() {
    return isFlagSet(4096);
  }
  
  public double getCarrierPhaseUncertainty() {
    return this.mCarrierPhaseUncertainty;
  }
  
  public void setCarrierPhaseUncertainty(double paramDouble) {
    setFlag(4096);
    this.mCarrierPhaseUncertainty = paramDouble;
  }
  
  public void resetCarrierPhaseUncertainty() {
    resetFlag(4096);
    this.mCarrierPhaseUncertainty = Double.NaN;
  }
  
  public byte getLossOfLock() {
    return this.mLossOfLock;
  }
  
  public void setLossOfLock(byte paramByte) {
    this.mLossOfLock = paramByte;
  }
  
  private String getLossOfLockString() {
    byte b = this.mLossOfLock;
    if (b != 0) {
      if (b != 1) {
        if (b != 2) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("<Invalid:");
          stringBuilder.append(this.mLossOfLock);
          stringBuilder.append(">");
          return stringBuilder.toString();
        } 
        return "CycleSlip";
      } 
      return "Ok";
    } 
    return "Unknown";
  }
  
  public boolean hasBitNumber() {
    return isFlagSet(8192);
  }
  
  public int getBitNumber() {
    return this.mBitNumber;
  }
  
  public void setBitNumber(int paramInt) {
    setFlag(8192);
    this.mBitNumber = paramInt;
  }
  
  public void resetBitNumber() {
    resetFlag(8192);
    this.mBitNumber = Integer.MIN_VALUE;
  }
  
  public boolean hasTimeFromLastBitInMs() {
    return isFlagSet(16384);
  }
  
  public short getTimeFromLastBitInMs() {
    return this.mTimeFromLastBitInMs;
  }
  
  public void setTimeFromLastBitInMs(short paramShort) {
    setFlag(16384);
    this.mTimeFromLastBitInMs = paramShort;
  }
  
  public void resetTimeFromLastBitInMs() {
    resetFlag(16384);
    this.mTimeFromLastBitInMs = Short.MIN_VALUE;
  }
  
  public boolean hasDopplerShiftInHz() {
    return isFlagSet(32768);
  }
  
  public double getDopplerShiftInHz() {
    return this.mDopplerShiftInHz;
  }
  
  public void setDopplerShiftInHz(double paramDouble) {
    setFlag(32768);
    this.mDopplerShiftInHz = paramDouble;
  }
  
  public void resetDopplerShiftInHz() {
    resetFlag(32768);
    this.mDopplerShiftInHz = Double.NaN;
  }
  
  public boolean hasDopplerShiftUncertaintyInHz() {
    return isFlagSet(65536);
  }
  
  public double getDopplerShiftUncertaintyInHz() {
    return this.mDopplerShiftUncertaintyInHz;
  }
  
  public void setDopplerShiftUncertaintyInHz(double paramDouble) {
    setFlag(65536);
    this.mDopplerShiftUncertaintyInHz = paramDouble;
  }
  
  public void resetDopplerShiftUncertaintyInHz() {
    resetFlag(65536);
    this.mDopplerShiftUncertaintyInHz = Double.NaN;
  }
  
  public byte getMultipathIndicator() {
    return this.mMultipathIndicator;
  }
  
  public void setMultipathIndicator(byte paramByte) {
    this.mMultipathIndicator = paramByte;
  }
  
  private String getMultipathIndicatorString() {
    byte b = this.mMultipathIndicator;
    if (b != 0) {
      if (b != 1) {
        if (b != 2) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("<Invalid:");
          stringBuilder.append(this.mMultipathIndicator);
          stringBuilder.append(">");
          return stringBuilder.toString();
        } 
        return "NotUsed";
      } 
      return "Detected";
    } 
    return "Unknown";
  }
  
  public boolean hasSnrInDb() {
    return isFlagSet(1);
  }
  
  public double getSnrInDb() {
    return this.mSnrInDb;
  }
  
  public void setSnrInDb(double paramDouble) {
    setFlag(1);
    this.mSnrInDb = paramDouble;
  }
  
  public void resetSnrInDb() {
    resetFlag(1);
    this.mSnrInDb = Double.NaN;
  }
  
  public boolean hasElevationInDeg() {
    return isFlagSet(2);
  }
  
  public double getElevationInDeg() {
    return this.mElevationInDeg;
  }
  
  public void setElevationInDeg(double paramDouble) {
    setFlag(2);
    this.mElevationInDeg = paramDouble;
  }
  
  public void resetElevationInDeg() {
    resetFlag(2);
    this.mElevationInDeg = Double.NaN;
  }
  
  public boolean hasElevationUncertaintyInDeg() {
    return isFlagSet(4);
  }
  
  public double getElevationUncertaintyInDeg() {
    return this.mElevationUncertaintyInDeg;
  }
  
  public void setElevationUncertaintyInDeg(double paramDouble) {
    setFlag(4);
    this.mElevationUncertaintyInDeg = paramDouble;
  }
  
  public void resetElevationUncertaintyInDeg() {
    resetFlag(4);
    this.mElevationUncertaintyInDeg = Double.NaN;
  }
  
  public boolean hasAzimuthInDeg() {
    return isFlagSet(8);
  }
  
  public double getAzimuthInDeg() {
    return this.mAzimuthInDeg;
  }
  
  public void setAzimuthInDeg(double paramDouble) {
    setFlag(8);
    this.mAzimuthInDeg = paramDouble;
  }
  
  public void resetAzimuthInDeg() {
    resetFlag(8);
    this.mAzimuthInDeg = Double.NaN;
  }
  
  public boolean hasAzimuthUncertaintyInDeg() {
    return isFlagSet(16);
  }
  
  public double getAzimuthUncertaintyInDeg() {
    return this.mAzimuthUncertaintyInDeg;
  }
  
  public void setAzimuthUncertaintyInDeg(double paramDouble) {
    setFlag(16);
    this.mAzimuthUncertaintyInDeg = paramDouble;
  }
  
  public void resetAzimuthUncertaintyInDeg() {
    resetFlag(16);
    this.mAzimuthUncertaintyInDeg = Double.NaN;
  }
  
  public boolean isUsedInFix() {
    return this.mUsedInFix;
  }
  
  public void setUsedInFix(boolean paramBoolean) {
    this.mUsedInFix = paramBoolean;
  }
  
  public static final Parcelable.Creator<GpsMeasurement> CREATOR = new Parcelable.Creator<GpsMeasurement>() {
      public GpsMeasurement createFromParcel(Parcel param1Parcel) {
        boolean bool;
        GpsMeasurement gpsMeasurement = new GpsMeasurement();
        GpsMeasurement.access$002(gpsMeasurement, param1Parcel.readInt());
        GpsMeasurement.access$102(gpsMeasurement, param1Parcel.readByte());
        GpsMeasurement.access$202(gpsMeasurement, param1Parcel.readDouble());
        GpsMeasurement.access$302(gpsMeasurement, (short)param1Parcel.readInt());
        GpsMeasurement.access$402(gpsMeasurement, param1Parcel.readLong());
        GpsMeasurement.access$502(gpsMeasurement, param1Parcel.readLong());
        GpsMeasurement.access$602(gpsMeasurement, param1Parcel.readDouble());
        GpsMeasurement.access$702(gpsMeasurement, param1Parcel.readDouble());
        GpsMeasurement.access$802(gpsMeasurement, param1Parcel.readDouble());
        GpsMeasurement.access$902(gpsMeasurement, (short)param1Parcel.readInt());
        GpsMeasurement.access$1002(gpsMeasurement, param1Parcel.readDouble());
        GpsMeasurement.access$1102(gpsMeasurement, param1Parcel.readDouble());
        GpsMeasurement.access$1202(gpsMeasurement, param1Parcel.readDouble());
        GpsMeasurement.access$1302(gpsMeasurement, param1Parcel.readDouble());
        GpsMeasurement.access$1402(gpsMeasurement, param1Parcel.readDouble());
        GpsMeasurement.access$1502(gpsMeasurement, param1Parcel.readDouble());
        GpsMeasurement.access$1602(gpsMeasurement, param1Parcel.readFloat());
        GpsMeasurement.access$1702(gpsMeasurement, param1Parcel.readLong());
        GpsMeasurement.access$1802(gpsMeasurement, param1Parcel.readDouble());
        GpsMeasurement.access$1902(gpsMeasurement, param1Parcel.readDouble());
        GpsMeasurement.access$2002(gpsMeasurement, param1Parcel.readByte());
        GpsMeasurement.access$2102(gpsMeasurement, param1Parcel.readInt());
        GpsMeasurement.access$2202(gpsMeasurement, (short)param1Parcel.readInt());
        GpsMeasurement.access$2302(gpsMeasurement, param1Parcel.readDouble());
        GpsMeasurement.access$2402(gpsMeasurement, param1Parcel.readDouble());
        GpsMeasurement.access$2502(gpsMeasurement, param1Parcel.readByte());
        GpsMeasurement.access$2602(gpsMeasurement, param1Parcel.readDouble());
        GpsMeasurement.access$2702(gpsMeasurement, param1Parcel.readDouble());
        GpsMeasurement.access$2802(gpsMeasurement, param1Parcel.readDouble());
        GpsMeasurement.access$2902(gpsMeasurement, param1Parcel.readDouble());
        GpsMeasurement.access$3002(gpsMeasurement, param1Parcel.readDouble());
        if (param1Parcel.readInt() != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        GpsMeasurement.access$3102(gpsMeasurement, bool);
        return gpsMeasurement;
      }
      
      public GpsMeasurement[] newArray(int param1Int) {
        return new GpsMeasurement[param1Int];
      }
    };
  
  private static final int GPS_MEASUREMENT_HAS_UNCORRECTED_PSEUDORANGE_RATE = 262144;
  
  private static final int HAS_AZIMUTH = 8;
  
  private static final int HAS_AZIMUTH_UNCERTAINTY = 16;
  
  private static final int HAS_BIT_NUMBER = 8192;
  
  private static final int HAS_CARRIER_CYCLES = 1024;
  
  private static final int HAS_CARRIER_FREQUENCY = 512;
  
  private static final int HAS_CARRIER_PHASE = 2048;
  
  private static final int HAS_CARRIER_PHASE_UNCERTAINTY = 4096;
  
  private static final int HAS_CODE_PHASE = 128;
  
  private static final int HAS_CODE_PHASE_UNCERTAINTY = 256;
  
  private static final int HAS_DOPPLER_SHIFT = 32768;
  
  private static final int HAS_DOPPLER_SHIFT_UNCERTAINTY = 65536;
  
  private static final int HAS_ELEVATION = 2;
  
  private static final int HAS_ELEVATION_UNCERTAINTY = 4;
  
  private static final int HAS_NO_FLAGS = 0;
  
  private static final int HAS_PSEUDORANGE = 32;
  
  private static final int HAS_PSEUDORANGE_UNCERTAINTY = 64;
  
  private static final int HAS_SNR = 1;
  
  private static final int HAS_TIME_FROM_LAST_BIT = 16384;
  
  private static final int HAS_USED_IN_FIX = 131072;
  
  public static final byte LOSS_OF_LOCK_CYCLE_SLIP = 2;
  
  public static final byte LOSS_OF_LOCK_OK = 1;
  
  public static final byte LOSS_OF_LOCK_UNKNOWN = 0;
  
  public static final byte MULTIPATH_INDICATOR_DETECTED = 1;
  
  public static final byte MULTIPATH_INDICATOR_NOT_USED = 2;
  
  public static final byte MULTIPATH_INDICATOR_UNKNOWN = 0;
  
  private static final short STATE_ALL = 31;
  
  public static final short STATE_BIT_SYNC = 2;
  
  public static final short STATE_CODE_LOCK = 1;
  
  public static final short STATE_MSEC_AMBIGUOUS = 16;
  
  public static final short STATE_SUBFRAME_SYNC = 4;
  
  public static final short STATE_TOW_DECODED = 8;
  
  public static final short STATE_UNKNOWN = 0;
  
  private double mAccumulatedDeltaRangeInMeters;
  
  private short mAccumulatedDeltaRangeState;
  
  private double mAccumulatedDeltaRangeUncertaintyInMeters;
  
  private double mAzimuthInDeg;
  
  private double mAzimuthUncertaintyInDeg;
  
  private int mBitNumber;
  
  private long mCarrierCycles;
  
  private float mCarrierFrequencyInHz;
  
  private double mCarrierPhase;
  
  private double mCarrierPhaseUncertainty;
  
  private double mCn0InDbHz;
  
  private double mCodePhaseInChips;
  
  private double mCodePhaseUncertaintyInChips;
  
  private double mDopplerShiftInHz;
  
  private double mDopplerShiftUncertaintyInHz;
  
  private double mElevationInDeg;
  
  private double mElevationUncertaintyInDeg;
  
  private int mFlags;
  
  private byte mLossOfLock;
  
  private byte mMultipathIndicator;
  
  private byte mPrn;
  
  private double mPseudorangeInMeters;
  
  private double mPseudorangeRateInMetersPerSec;
  
  private double mPseudorangeRateUncertaintyInMetersPerSec;
  
  private double mPseudorangeUncertaintyInMeters;
  
  private long mReceivedGpsTowInNs;
  
  private long mReceivedGpsTowUncertaintyInNs;
  
  private double mSnrInDb;
  
  private short mState;
  
  private short mTimeFromLastBitInMs;
  
  private double mTimeOffsetInNs;
  
  private boolean mUsedInFix;
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mFlags);
    paramParcel.writeByte(this.mPrn);
    paramParcel.writeDouble(this.mTimeOffsetInNs);
    paramParcel.writeInt(this.mState);
    paramParcel.writeLong(this.mReceivedGpsTowInNs);
    paramParcel.writeLong(this.mReceivedGpsTowUncertaintyInNs);
    paramParcel.writeDouble(this.mCn0InDbHz);
    paramParcel.writeDouble(this.mPseudorangeRateInMetersPerSec);
    paramParcel.writeDouble(this.mPseudorangeRateUncertaintyInMetersPerSec);
    paramParcel.writeInt(this.mAccumulatedDeltaRangeState);
    paramParcel.writeDouble(this.mAccumulatedDeltaRangeInMeters);
    paramParcel.writeDouble(this.mAccumulatedDeltaRangeUncertaintyInMeters);
    paramParcel.writeDouble(this.mPseudorangeInMeters);
    paramParcel.writeDouble(this.mPseudorangeUncertaintyInMeters);
    paramParcel.writeDouble(this.mCodePhaseInChips);
    paramParcel.writeDouble(this.mCodePhaseUncertaintyInChips);
    paramParcel.writeFloat(this.mCarrierFrequencyInHz);
    paramParcel.writeLong(this.mCarrierCycles);
    paramParcel.writeDouble(this.mCarrierPhase);
    paramParcel.writeDouble(this.mCarrierPhaseUncertainty);
    paramParcel.writeByte(this.mLossOfLock);
    paramParcel.writeInt(this.mBitNumber);
    paramParcel.writeInt(this.mTimeFromLastBitInMs);
    paramParcel.writeDouble(this.mDopplerShiftInHz);
    paramParcel.writeDouble(this.mDopplerShiftUncertaintyInHz);
    paramParcel.writeByte(this.mMultipathIndicator);
    paramParcel.writeDouble(this.mSnrInDb);
    paramParcel.writeDouble(this.mElevationInDeg);
    paramParcel.writeDouble(this.mElevationUncertaintyInDeg);
    paramParcel.writeDouble(this.mAzimuthInDeg);
    paramParcel.writeDouble(this.mAzimuthUncertaintyInDeg);
    paramParcel.writeInt(this.mUsedInFix);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("GpsMeasurement:\n");
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "Prn", Byte.valueOf(this.mPrn) }));
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "TimeOffsetInNs", Double.valueOf(this.mTimeOffsetInNs) }));
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "State", getStateString() }));
    long l1 = this.mReceivedGpsTowInNs;
    long l2 = this.mReceivedGpsTowUncertaintyInNs;
    stringBuilder.append(String.format("   %-29s = %-25s   %-40s = %s\n", new Object[] { "ReceivedGpsTowInNs", Long.valueOf(l1), "ReceivedGpsTowUncertaintyInNs", Long.valueOf(l2) }));
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "Cn0InDbHz", Double.valueOf(this.mCn0InDbHz) }));
    double d1 = this.mPseudorangeRateInMetersPerSec;
    double d2 = this.mPseudorangeRateUncertaintyInMetersPerSec;
    stringBuilder.append(String.format("   %-29s = %-25s   %-40s = %s\n", new Object[] { "PseudorangeRateInMetersPerSec", Double.valueOf(d1), "PseudorangeRateUncertaintyInMetersPerSec", Double.valueOf(d2) }));
    boolean bool = isPseudorangeRateCorrected();
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "PseudorangeRateIsCorrected", Boolean.valueOf(bool) }));
    String str = getAccumulatedDeltaRangeStateString();
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "AccumulatedDeltaRangeState", str }));
    d1 = this.mAccumulatedDeltaRangeInMeters;
    d2 = this.mAccumulatedDeltaRangeUncertaintyInMeters;
    stringBuilder.append(String.format("   %-29s = %-25s   %-40s = %s\n", new Object[] { "AccumulatedDeltaRangeInMeters", Double.valueOf(d1), "AccumulatedDeltaRangeUncertaintyInMeters", Double.valueOf(d2) }));
    bool = hasPseudorangeInMeters();
    Double double_1 = null;
    if (bool) {
      Double double_ = Double.valueOf(this.mPseudorangeInMeters);
    } else {
      str = null;
    } 
    if (hasPseudorangeUncertaintyInMeters()) {
      double_2 = Double.valueOf(this.mPseudorangeUncertaintyInMeters);
    } else {
      double_2 = null;
    } 
    stringBuilder.append(String.format("   %-29s = %-25s   %-40s = %s\n", new Object[] { "PseudorangeInMeters", str, "PseudorangeUncertaintyInMeters", double_2 }));
    if (hasCodePhaseInChips()) {
      Double double_ = Double.valueOf(this.mCodePhaseInChips);
    } else {
      str = null;
    } 
    if (hasCodePhaseUncertaintyInChips()) {
      double_2 = Double.valueOf(this.mCodePhaseUncertaintyInChips);
    } else {
      double_2 = null;
    } 
    stringBuilder.append(String.format("   %-29s = %-25s   %-40s = %s\n", new Object[] { "CodePhaseInChips", str, "CodePhaseUncertaintyInChips", double_2 }));
    if (hasCarrierFrequencyInHz()) {
      Float float_ = Float.valueOf(this.mCarrierFrequencyInHz);
    } else {
      str = null;
    } 
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "CarrierFrequencyInHz", str }));
    if (hasCarrierCycles()) {
      Long long_ = Long.valueOf(this.mCarrierCycles);
    } else {
      str = null;
    } 
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "CarrierCycles", str }));
    if (hasCarrierPhase()) {
      Double double_ = Double.valueOf(this.mCarrierPhase);
    } else {
      str = null;
    } 
    if (hasCarrierPhaseUncertainty()) {
      double_2 = Double.valueOf(this.mCarrierPhaseUncertainty);
    } else {
      double_2 = null;
    } 
    stringBuilder.append(String.format("   %-29s = %-25s   %-40s = %s\n", new Object[] { "CarrierPhase", str, "CarrierPhaseUncertainty", double_2 }));
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "LossOfLock", getLossOfLockString() }));
    if (hasBitNumber()) {
      Integer integer = Integer.valueOf(this.mBitNumber);
    } else {
      str = null;
    } 
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "BitNumber", str }));
    if (hasTimeFromLastBitInMs()) {
      Short short_ = Short.valueOf(this.mTimeFromLastBitInMs);
    } else {
      str = null;
    } 
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "TimeFromLastBitInMs", str }));
    if (hasDopplerShiftInHz()) {
      Double double_ = Double.valueOf(this.mDopplerShiftInHz);
    } else {
      str = null;
    } 
    if (hasDopplerShiftUncertaintyInHz()) {
      double_2 = Double.valueOf(this.mDopplerShiftUncertaintyInHz);
    } else {
      double_2 = null;
    } 
    stringBuilder.append(String.format("   %-29s = %-25s   %-40s = %s\n", new Object[] { "DopplerShiftInHz", str, "DopplerShiftUncertaintyInHz", double_2 }));
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "MultipathIndicator", getMultipathIndicatorString() }));
    if (hasSnrInDb()) {
      Double double_ = Double.valueOf(this.mSnrInDb);
    } else {
      str = null;
    } 
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "SnrInDb", str }));
    if (hasElevationInDeg()) {
      Double double_ = Double.valueOf(this.mElevationInDeg);
    } else {
      str = null;
    } 
    if (hasElevationUncertaintyInDeg()) {
      double_2 = Double.valueOf(this.mElevationUncertaintyInDeg);
    } else {
      double_2 = null;
    } 
    stringBuilder.append(String.format("   %-29s = %-25s   %-40s = %s\n", new Object[] { "ElevationInDeg", str, "ElevationUncertaintyInDeg", double_2 }));
    if (hasAzimuthInDeg()) {
      Double double_ = Double.valueOf(this.mAzimuthInDeg);
    } else {
      str = null;
    } 
    Double double_2 = double_1;
    if (hasAzimuthUncertaintyInDeg())
      double_2 = Double.valueOf(this.mAzimuthUncertaintyInDeg); 
    stringBuilder.append(String.format("   %-29s = %-25s   %-40s = %s\n", new Object[] { "AzimuthInDeg", str, "AzimuthUncertaintyInDeg", double_2 }));
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "UsedInFix", Boolean.valueOf(this.mUsedInFix) }));
    return stringBuilder.toString();
  }
  
  private void initialize() {
    this.mFlags = 0;
    setPrn(-128);
    setTimeOffsetInNs(-9.223372036854776E18D);
    setState((short)0);
    setReceivedGpsTowInNs(Long.MIN_VALUE);
    setReceivedGpsTowUncertaintyInNs(Long.MAX_VALUE);
    setCn0InDbHz(Double.MIN_VALUE);
    setPseudorangeRateInMetersPerSec(Double.MIN_VALUE);
    setPseudorangeRateUncertaintyInMetersPerSec(Double.MIN_VALUE);
    setAccumulatedDeltaRangeState((short)0);
    setAccumulatedDeltaRangeInMeters(Double.MIN_VALUE);
    setAccumulatedDeltaRangeUncertaintyInMeters(Double.MIN_VALUE);
    resetPseudorangeInMeters();
    resetPseudorangeUncertaintyInMeters();
    resetCodePhaseInChips();
    resetCodePhaseUncertaintyInChips();
    resetCarrierFrequencyInHz();
    resetCarrierCycles();
    resetCarrierPhase();
    resetCarrierPhaseUncertainty();
    setLossOfLock((byte)0);
    resetBitNumber();
    resetTimeFromLastBitInMs();
    resetDopplerShiftInHz();
    resetDopplerShiftUncertaintyInHz();
    setMultipathIndicator((byte)0);
    resetSnrInDb();
    resetElevationInDeg();
    resetElevationUncertaintyInDeg();
    resetAzimuthInDeg();
    resetAzimuthUncertaintyInDeg();
    setUsedInFix(false);
  }
  
  private void setFlag(int paramInt) {
    this.mFlags |= paramInt;
  }
  
  private void resetFlag(int paramInt) {
    this.mFlags &= paramInt ^ 0xFFFFFFFF;
  }
  
  private boolean isFlagSet(int paramInt) {
    boolean bool;
    if ((this.mFlags & paramInt) == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
