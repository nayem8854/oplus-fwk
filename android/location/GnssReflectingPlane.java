package android.location;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class GnssReflectingPlane implements Parcelable {
  private GnssReflectingPlane(Builder paramBuilder) {
    this.mLatitudeDegrees = paramBuilder.mLatitudeDegrees;
    this.mLongitudeDegrees = paramBuilder.mLongitudeDegrees;
    this.mAltitudeMeters = paramBuilder.mAltitudeMeters;
    this.mAzimuthDegrees = paramBuilder.mAzimuthDegrees;
  }
  
  public double getLatitudeDegrees() {
    return this.mLatitudeDegrees;
  }
  
  public double getLongitudeDegrees() {
    return this.mLongitudeDegrees;
  }
  
  public double getAltitudeMeters() {
    return this.mAltitudeMeters;
  }
  
  public double getAzimuthDegrees() {
    return this.mAzimuthDegrees;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<GnssReflectingPlane> CREATOR = new Parcelable.Creator<GnssReflectingPlane>() {
      public GnssReflectingPlane createFromParcel(Parcel param1Parcel) {
        GnssReflectingPlane.Builder builder2 = new GnssReflectingPlane.Builder();
        builder2 = builder2.setLatitudeDegrees(param1Parcel.readDouble());
        builder2 = builder2.setLongitudeDegrees(param1Parcel.readDouble());
        builder2 = builder2.setAltitudeMeters(param1Parcel.readDouble());
        GnssReflectingPlane.Builder builder1 = builder2.setAzimuthDegrees(param1Parcel.readDouble());
        return builder1.build();
      }
      
      public GnssReflectingPlane[] newArray(int param1Int) {
        return new GnssReflectingPlane[param1Int];
      }
    };
  
  private final double mAltitudeMeters;
  
  private final double mAzimuthDegrees;
  
  private final double mLatitudeDegrees;
  
  private final double mLongitudeDegrees;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("ReflectingPlane:\n");
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "LatitudeDegrees = ", Double.valueOf(this.mLatitudeDegrees) }));
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "LongitudeDegrees = ", Double.valueOf(this.mLongitudeDegrees) }));
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "AltitudeMeters = ", Double.valueOf(this.mAltitudeMeters) }));
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "AzimuthDegrees = ", Double.valueOf(this.mAzimuthDegrees) }));
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeDouble(this.mLatitudeDegrees);
    paramParcel.writeDouble(this.mLongitudeDegrees);
    paramParcel.writeDouble(this.mAltitudeMeters);
    paramParcel.writeDouble(this.mAzimuthDegrees);
  }
  
  class Builder {
    private double mAltitudeMeters;
    
    private double mAzimuthDegrees;
    
    private double mLatitudeDegrees;
    
    private double mLongitudeDegrees;
    
    public Builder setLatitudeDegrees(double param1Double) {
      this.mLatitudeDegrees = param1Double;
      return this;
    }
    
    public Builder setLongitudeDegrees(double param1Double) {
      this.mLongitudeDegrees = param1Double;
      return this;
    }
    
    public Builder setAltitudeMeters(double param1Double) {
      this.mAltitudeMeters = param1Double;
      return this;
    }
    
    public Builder setAzimuthDegrees(double param1Double) {
      this.mAzimuthDegrees = param1Double;
      return this;
    }
    
    public GnssReflectingPlane build() {
      return new GnssReflectingPlane(this);
    }
  }
}
