package android.location;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@SystemApi
@Deprecated
public class GpsMeasurementsEvent implements Parcelable {
  public GpsMeasurementsEvent(GpsClock paramGpsClock, GpsMeasurement[] paramArrayOfGpsMeasurement) {
    if (paramGpsClock != null) {
      if (paramArrayOfGpsMeasurement != null && paramArrayOfGpsMeasurement.length != 0) {
        this.mClock = paramGpsClock;
        List<GpsMeasurement> list = Arrays.asList(paramArrayOfGpsMeasurement);
        this.mReadOnlyMeasurements = Collections.unmodifiableCollection(list);
        return;
      } 
      throw new InvalidParameterException("Parameter 'measurements' must not be null or empty.");
    } 
    throw new InvalidParameterException("Parameter 'clock' must not be null.");
  }
  
  public GpsClock getClock() {
    return this.mClock;
  }
  
  public Collection<GpsMeasurement> getMeasurements() {
    return this.mReadOnlyMeasurements;
  }
  
  public static final Parcelable.Creator<GpsMeasurementsEvent> CREATOR = new Parcelable.Creator<GpsMeasurementsEvent>() {
      public GpsMeasurementsEvent createFromParcel(Parcel param1Parcel) {
        ClassLoader classLoader = getClass().getClassLoader();
        GpsClock gpsClock = param1Parcel.<GpsClock>readParcelable(classLoader);
        int i = param1Parcel.readInt();
        GpsMeasurement[] arrayOfGpsMeasurement = new GpsMeasurement[i];
        param1Parcel.readTypedArray(arrayOfGpsMeasurement, GpsMeasurement.CREATOR);
        return new GpsMeasurementsEvent(gpsClock, arrayOfGpsMeasurement);
      }
      
      public GpsMeasurementsEvent[] newArray(int param1Int) {
        return new GpsMeasurementsEvent[param1Int];
      }
    };
  
  public static final int STATUS_GPS_LOCATION_DISABLED = 2;
  
  public static final int STATUS_NOT_SUPPORTED = 0;
  
  public static final int STATUS_READY = 1;
  
  private final GpsClock mClock;
  
  private final Collection<GpsMeasurement> mReadOnlyMeasurements;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.mClock, paramInt);
    int i = this.mReadOnlyMeasurements.size();
    Collection<GpsMeasurement> collection = this.mReadOnlyMeasurements;
    GpsMeasurement[] arrayOfGpsMeasurement2 = new GpsMeasurement[i];
    GpsMeasurement[] arrayOfGpsMeasurement1 = collection.<GpsMeasurement>toArray(arrayOfGpsMeasurement2);
    paramParcel.writeInt(arrayOfGpsMeasurement1.length);
    paramParcel.writeTypedArray(arrayOfGpsMeasurement1, paramInt);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("[ GpsMeasurementsEvent:\n\n");
    stringBuilder.append(this.mClock.toString());
    stringBuilder.append("\n");
    for (GpsMeasurement gpsMeasurement : this.mReadOnlyMeasurements) {
      stringBuilder.append(gpsMeasurement.toString());
      stringBuilder.append("\n");
    } 
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  @SystemApi
  class Listener {
    public abstract void onGpsMeasurementsReceived(GpsMeasurementsEvent param1GpsMeasurementsEvent);
    
    public abstract void onStatusChanged(int param1Int);
  }
}
