package android.location;

import com.oplus.reflect.MethodParams;
import com.oplus.reflect.RefClass;
import com.oplus.reflect.RefMethod;

public class OplusMirrorLocationManager {
  public static Class<?> TYPE = RefClass.load(OplusMirrorLocationManager.class, LocationManager.class);
  
  @MethodParams({int.class, LocAppsOp.class})
  public static RefMethod<Void> getLocAppsOp;
  
  @MethodParams({int.class, LocAppsOp.class})
  public static RefMethod<Void> setLocAppsOp;
}
