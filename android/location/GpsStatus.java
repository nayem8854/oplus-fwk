package android.location;

import android.util.SparseArray;
import java.util.Iterator;
import java.util.NoSuchElementException;

@Deprecated
public final class GpsStatus {
  private int mTimeToFirstFix;
  
  private final SparseArray<GpsSatellite> mSatellites = new SparseArray();
  
  @Deprecated
  public static interface Listener {
    void onGpsStatusChanged(int param1Int);
  }
  
  @Deprecated
  public static interface NmeaListener {
    void onNmeaReceived(long param1Long, String param1String);
  }
  
  private final class SatelliteIterator implements Iterator<GpsSatellite> {
    private int mIndex = 0;
    
    private final int mSatellitesCount;
    
    final GpsStatus this$0;
    
    SatelliteIterator() {
      this.mSatellitesCount = GpsStatus.this.mSatellites.size();
    }
    
    public boolean hasNext() {
      for (; this.mIndex < this.mSatellitesCount; this.mIndex++) {
        GpsSatellite gpsSatellite = (GpsSatellite)GpsStatus.this.mSatellites.valueAt(this.mIndex);
        if (gpsSatellite.mValid)
          return true; 
      } 
      return false;
    }
    
    public GpsSatellite next() {
      while (this.mIndex < this.mSatellitesCount) {
        GpsSatellite gpsSatellite = (GpsSatellite)GpsStatus.this.mSatellites.valueAt(this.mIndex);
        this.mIndex++;
        if (gpsSatellite.mValid)
          return gpsSatellite; 
      } 
      throw new NoSuchElementException();
    }
    
    public void remove() {
      throw new UnsupportedOperationException();
    }
  }
  
  private Iterable<GpsSatellite> mSatelliteList = new _$$Lambda$GpsStatus$RTSonBp9m0T0NWA3SCfYgWf1mTo(this);
  
  private static final int SBAS_SVID_OFFSET = -87;
  
  private static final int MAX_SATELLITES = 255;
  
  public static final int GPS_EVENT_STOPPED = 2;
  
  public static final int GPS_EVENT_STARTED = 1;
  
  public static final int GPS_EVENT_SATELLITE_STATUS = 4;
  
  public static final int GPS_EVENT_FIRST_FIX = 3;
  
  private static final int GLONASS_SVID_OFFSET = 64;
  
  private static final int BEIDOU_SVID_OFFSET = 200;
  
  public static GpsStatus create(GnssStatus paramGnssStatus, int paramInt) {
    GpsStatus gpsStatus = new GpsStatus();
    gpsStatus.setStatus(paramGnssStatus, paramInt);
    return gpsStatus;
  }
  
  static GpsStatus createEmpty() {
    return new GpsStatus();
  }
  
  void setStatus(GnssStatus paramGnssStatus, int paramInt) {
    byte b;
    for (b = 0; b < this.mSatellites.size(); b++)
      ((GpsSatellite)this.mSatellites.valueAt(b)).mValid = false; 
    this.mTimeToFirstFix = paramInt;
    for (b = 0; b < paramGnssStatus.getSatelliteCount(); b++) {
      if (0.0D >= paramGnssStatus.getCn0DbHz(b))
        continue; 
      int i = paramGnssStatus.getConstellationType(b);
      int j = paramGnssStatus.getSvid(b);
      if (i == 3) {
        paramInt = j + 64;
      } else if (i == 5) {
        paramInt = j + 200;
      } else if (i == 2) {
        paramInt = j - 87;
      } else {
        paramInt = j;
        if (i != 1) {
          paramInt = j;
          if (i != 4)
            continue; 
        } 
      } 
      if (paramInt > 0 && paramInt <= 255) {
        GpsSatellite gpsSatellite1 = (GpsSatellite)this.mSatellites.get(paramInt);
        GpsSatellite gpsSatellite2 = gpsSatellite1;
        if (gpsSatellite1 == null) {
          gpsSatellite2 = new GpsSatellite(paramInt);
          this.mSatellites.put(paramInt, gpsSatellite2);
        } 
        gpsSatellite2.mValid = true;
        gpsSatellite2.mSnr = paramGnssStatus.getCn0DbHz(b);
        gpsSatellite2.mElevation = paramGnssStatus.getElevationDegrees(b);
        gpsSatellite2.mAzimuth = paramGnssStatus.getAzimuthDegrees(b);
        gpsSatellite2.mHasEphemeris = paramGnssStatus.hasEphemerisData(b);
        gpsSatellite2.mHasAlmanac = paramGnssStatus.hasAlmanacData(b);
        gpsSatellite2.mUsedInFix = paramGnssStatus.usedInFix(b);
      } 
      continue;
    } 
  }
  
  public int getTimeToFirstFix() {
    return this.mTimeToFirstFix;
  }
  
  public Iterable<GpsSatellite> getSatellites() {
    return this.mSatelliteList;
  }
  
  public int getMaxSatellites() {
    return this.mSatellites.size();
  }
}
