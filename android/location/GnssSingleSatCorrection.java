package android.location;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;

@SystemApi
public final class GnssSingleSatCorrection implements Parcelable {
  private GnssSingleSatCorrection(Builder paramBuilder) {
    this.mSingleSatCorrectionFlags = paramBuilder.mSingleSatCorrectionFlags;
    this.mSatId = paramBuilder.mSatId;
    this.mConstellationType = paramBuilder.mConstellationType;
    this.mCarrierFrequencyHz = paramBuilder.mCarrierFrequencyHz;
    this.mProbSatIsLos = paramBuilder.mProbSatIsLos;
    this.mExcessPathLengthMeters = paramBuilder.mExcessPathLengthMeters;
    this.mExcessPathLengthUncertaintyMeters = paramBuilder.mExcessPathLengthUncertaintyMeters;
    this.mReflectingPlane = paramBuilder.mReflectingPlane;
  }
  
  public int getSingleSatelliteCorrectionFlags() {
    return this.mSingleSatCorrectionFlags;
  }
  
  public int getConstellationType() {
    return this.mConstellationType;
  }
  
  public int getSatelliteId() {
    return this.mSatId;
  }
  
  public float getCarrierFrequencyHz() {
    return this.mCarrierFrequencyHz;
  }
  
  public float getProbabilityLineOfSight() {
    return this.mProbSatIsLos;
  }
  
  public float getExcessPathLengthMeters() {
    return this.mExcessPathLengthMeters;
  }
  
  public float getExcessPathLengthUncertaintyMeters() {
    return this.mExcessPathLengthUncertaintyMeters;
  }
  
  public GnssReflectingPlane getReflectingPlane() {
    return this.mReflectingPlane;
  }
  
  public boolean hasValidSatelliteLineOfSight() {
    int i = this.mSingleSatCorrectionFlags;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public boolean hasExcessPathLength() {
    boolean bool;
    if ((this.mSingleSatCorrectionFlags & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean hasExcessPathLengthUncertainty() {
    boolean bool;
    if ((this.mSingleSatCorrectionFlags & 0x4) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean hasReflectingPlane() {
    boolean bool;
    if ((this.mSingleSatCorrectionFlags & 0x8) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<GnssSingleSatCorrection> CREATOR = new Parcelable.Creator<GnssSingleSatCorrection>() {
      public GnssSingleSatCorrection createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        if ((i & 0x8) != 0) {
          i = 1;
        } else {
          i = 0;
        } 
        GnssSingleSatCorrection.Builder builder = new GnssSingleSatCorrection.Builder();
        builder = builder.setConstellationType(param1Parcel.readInt());
        builder = builder.setSatelliteId(param1Parcel.readInt());
        builder = builder.setCarrierFrequencyHz(param1Parcel.readFloat());
        builder = builder.setProbabilityLineOfSight(param1Parcel.readFloat());
        builder = builder.setExcessPathLengthMeters(param1Parcel.readFloat());
        builder = builder.setExcessPathLengthUncertaintyMeters(param1Parcel.readFloat());
        if (i != 0) {
          Parcelable.Creator<GnssReflectingPlane> creator = GnssReflectingPlane.CREATOR;
          GnssReflectingPlane gnssReflectingPlane = creator.createFromParcel(param1Parcel);
          builder.setReflectingPlane(gnssReflectingPlane);
        } 
        return builder.build();
      }
      
      public GnssSingleSatCorrection[] newArray(int param1Int) {
        return new GnssSingleSatCorrection[param1Int];
      }
    };
  
  public static final int HAS_EXCESS_PATH_LENGTH_MASK = 2;
  
  public static final int HAS_EXCESS_PATH_LENGTH_UNC_MASK = 4;
  
  public static final int HAS_PROB_SAT_IS_LOS_MASK = 1;
  
  public static final int HAS_REFLECTING_PLANE_MASK = 8;
  
  private final float mCarrierFrequencyHz;
  
  private final int mConstellationType;
  
  private final float mExcessPathLengthMeters;
  
  private final float mExcessPathLengthUncertaintyMeters;
  
  private final float mProbSatIsLos;
  
  private final GnssReflectingPlane mReflectingPlane;
  
  private final int mSatId;
  
  private final int mSingleSatCorrectionFlags;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("GnssSingleSatCorrection:\n");
    int i = this.mSingleSatCorrectionFlags;
    String str = String.format("   %-29s = %s\n", new Object[] { "SingleSatCorrectionFlags = ", Integer.valueOf(i) });
    stringBuilder.append(str);
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "ConstellationType = ", Integer.valueOf(this.mConstellationType) }));
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "SatId = ", Integer.valueOf(this.mSatId) }));
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "CarrierFrequencyHz = ", Float.valueOf(this.mCarrierFrequencyHz) }));
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "ProbSatIsLos = ", Float.valueOf(this.mProbSatIsLos) }));
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "ExcessPathLengthMeters = ", Float.valueOf(this.mExcessPathLengthMeters) }));
    float f = this.mExcessPathLengthUncertaintyMeters;
    str = String.format("   %-29s = %s\n", new Object[] { "ExcessPathLengthUncertaintyMeters = ", Float.valueOf(f) });
    stringBuilder.append(str);
    if (hasReflectingPlane())
      stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "ReflectingPlane = ", this.mReflectingPlane })); 
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mSingleSatCorrectionFlags);
    paramParcel.writeInt(this.mConstellationType);
    paramParcel.writeInt(this.mSatId);
    paramParcel.writeFloat(this.mCarrierFrequencyHz);
    paramParcel.writeFloat(this.mProbSatIsLos);
    paramParcel.writeFloat(this.mExcessPathLengthMeters);
    paramParcel.writeFloat(this.mExcessPathLengthUncertaintyMeters);
    if (hasReflectingPlane())
      this.mReflectingPlane.writeToParcel(paramParcel, paramInt); 
  }
  
  class Builder {
    private float mCarrierFrequencyHz;
    
    private int mConstellationType;
    
    private float mExcessPathLengthMeters;
    
    private float mExcessPathLengthUncertaintyMeters;
    
    private float mProbSatIsLos;
    
    private GnssReflectingPlane mReflectingPlane;
    
    private int mSatId;
    
    private int mSingleSatCorrectionFlags;
    
    public Builder setConstellationType(int param1Int) {
      this.mConstellationType = param1Int;
      return this;
    }
    
    public Builder setSatelliteId(int param1Int) {
      this.mSatId = param1Int;
      return this;
    }
    
    public Builder setCarrierFrequencyHz(float param1Float) {
      this.mCarrierFrequencyHz = param1Float;
      return this;
    }
    
    public Builder setProbabilityLineOfSight(float param1Float) {
      Preconditions.checkArgumentInRange(param1Float, 0.0F, 1.0F, "probSatIsLos should be between 0 and 1.");
      this.mProbSatIsLos = param1Float;
      this.mSingleSatCorrectionFlags = (byte)(this.mSingleSatCorrectionFlags | 0x1);
      return this;
    }
    
    public Builder setExcessPathLengthMeters(float param1Float) {
      this.mExcessPathLengthMeters = param1Float;
      this.mSingleSatCorrectionFlags = (byte)(this.mSingleSatCorrectionFlags | 0x2);
      return this;
    }
    
    public Builder setExcessPathLengthUncertaintyMeters(float param1Float) {
      this.mExcessPathLengthUncertaintyMeters = param1Float;
      this.mSingleSatCorrectionFlags = (byte)(this.mSingleSatCorrectionFlags | 0x4);
      return this;
    }
    
    public Builder setReflectingPlane(GnssReflectingPlane param1GnssReflectingPlane) {
      this.mReflectingPlane = param1GnssReflectingPlane;
      if (param1GnssReflectingPlane != null) {
        this.mSingleSatCorrectionFlags = (byte)(this.mSingleSatCorrectionFlags | 0x8);
      } else {
        this.mSingleSatCorrectionFlags = (byte)(this.mSingleSatCorrectionFlags & 0xFFFFFFF7);
      } 
      return this;
    }
    
    public GnssSingleSatCorrection build() {
      return new GnssSingleSatCorrection(this);
    }
  }
}
