package android.location;

import android.os.Parcel;
import android.os.Parcelable;

public final class LocationTime implements Parcelable {
  public LocationTime(long paramLong1, long paramLong2) {
    this.mTime = paramLong1;
    this.mElapsedRealtimeNanos = paramLong2;
  }
  
  public long getTime() {
    return this.mTime;
  }
  
  public long getElapsedRealtimeNanos() {
    return this.mElapsedRealtimeNanos;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mTime);
    paramParcel.writeLong(this.mElapsedRealtimeNanos);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<LocationTime> CREATOR = new Parcelable.Creator<LocationTime>() {
      public LocationTime createFromParcel(Parcel param1Parcel) {
        long l1 = param1Parcel.readLong();
        long l2 = param1Parcel.readLong();
        return new LocationTime(l1, l2);
      }
      
      public LocationTime[] newArray(int param1Int) {
        return new LocationTime[param1Int];
      }
    };
  
  private final long mElapsedRealtimeNanos;
  
  private final long mTime;
}
