package android.location;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ICountryListener extends IInterface {
  void onCountryDetected(Country paramCountry) throws RemoteException;
  
  class Default implements ICountryListener {
    public void onCountryDetected(Country param1Country) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICountryListener {
    private static final String DESCRIPTOR = "android.location.ICountryListener";
    
    static final int TRANSACTION_onCountryDetected = 1;
    
    public Stub() {
      attachInterface(this, "android.location.ICountryListener");
    }
    
    public static ICountryListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.location.ICountryListener");
      if (iInterface != null && iInterface instanceof ICountryListener)
        return (ICountryListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onCountryDetected";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.location.ICountryListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.location.ICountryListener");
      if (param1Parcel1.readInt() != 0) {
        Country country = Country.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onCountryDetected((Country)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements ICountryListener {
      public static ICountryListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.location.ICountryListener";
      }
      
      public void onCountryDetected(Country param2Country) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.location.ICountryListener");
          if (param2Country != null) {
            parcel.writeInt(1);
            param2Country.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ICountryListener.Stub.getDefaultImpl() != null) {
            ICountryListener.Stub.getDefaultImpl().onCountryDetected(param2Country);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICountryListener param1ICountryListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICountryListener != null) {
          Proxy.sDefaultImpl = param1ICountryListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICountryListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
