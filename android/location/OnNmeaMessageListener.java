package android.location;

public interface OnNmeaMessageListener {
  void onNmeaMessage(String paramString, long paramLong);
}
