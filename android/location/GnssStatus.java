package android.location;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public final class GnssStatus {
  public static final int CONSTELLATION_BEIDOU = 5;
  
  public static final int CONSTELLATION_COUNT = 8;
  
  public static final int CONSTELLATION_GALILEO = 6;
  
  public static final int CONSTELLATION_GLONASS = 3;
  
  public static final int CONSTELLATION_GPS = 1;
  
  public static final int CONSTELLATION_IRNSS = 7;
  
  public static final int CONSTELLATION_QZSS = 4;
  
  public static final int CONSTELLATION_SBAS = 2;
  
  private static final int CONSTELLATION_TYPE_MASK = 15;
  
  private static final int CONSTELLATION_TYPE_SHIFT_WIDTH = 8;
  
  public static final int CONSTELLATION_UNKNOWN = 0;
  
  private static final int SVID_FLAGS_HAS_ALMANAC_DATA = 2;
  
  private static final int SVID_FLAGS_HAS_BASEBAND_CN0 = 16;
  
  private static final int SVID_FLAGS_HAS_CARRIER_FREQUENCY = 8;
  
  private static final int SVID_FLAGS_HAS_EPHEMERIS_DATA = 1;
  
  private static final int SVID_FLAGS_NONE = 0;
  
  private static final int SVID_FLAGS_USED_IN_FIX = 4;
  
  private static final int SVID_SHIFT_WIDTH = 12;
  
  private final float[] mAzimuths;
  
  private final float[] mBasebandCn0DbHzs;
  
  private final float[] mCarrierFrequencies;
  
  private final float[] mCn0DbHzs;
  
  private final float[] mElevations;
  
  private final int mSvCount;
  
  private final int[] mSvidWithFlags;
  
  public static abstract class Callback {
    public void onStarted() {}
    
    public void onStopped() {}
    
    public void onFirstFix(int param1Int) {}
    
    public void onSatelliteStatusChanged(GnssStatus param1GnssStatus) {}
  }
  
  public static GnssStatus wrap(int paramInt, int[] paramArrayOfint, float[] paramArrayOffloat1, float[] paramArrayOffloat2, float[] paramArrayOffloat3, float[] paramArrayOffloat4, float[] paramArrayOffloat5) {
    return new GnssStatus(paramInt, paramArrayOfint, paramArrayOffloat1, paramArrayOffloat2, paramArrayOffloat3, paramArrayOffloat4, paramArrayOffloat5);
  }
  
  private GnssStatus(int paramInt, int[] paramArrayOfint, float[] paramArrayOffloat1, float[] paramArrayOffloat2, float[] paramArrayOffloat3, float[] paramArrayOffloat4, float[] paramArrayOffloat5) {
    this.mSvCount = paramInt;
    this.mSvidWithFlags = paramArrayOfint;
    this.mCn0DbHzs = paramArrayOffloat1;
    this.mElevations = paramArrayOffloat2;
    this.mAzimuths = paramArrayOffloat3;
    this.mCarrierFrequencies = paramArrayOffloat4;
    this.mBasebandCn0DbHzs = paramArrayOffloat5;
  }
  
  public int getSatelliteCount() {
    return this.mSvCount;
  }
  
  public int getConstellationType(int paramInt) {
    return this.mSvidWithFlags[paramInt] >> 8 & 0xF;
  }
  
  public int getSvid(int paramInt) {
    return this.mSvidWithFlags[paramInt] >> 12;
  }
  
  public float getCn0DbHz(int paramInt) {
    return this.mCn0DbHzs[paramInt];
  }
  
  public float getElevationDegrees(int paramInt) {
    return this.mElevations[paramInt];
  }
  
  public float getAzimuthDegrees(int paramInt) {
    return this.mAzimuths[paramInt];
  }
  
  public boolean hasEphemerisData(int paramInt) {
    paramInt = this.mSvidWithFlags[paramInt];
    boolean bool = true;
    if ((paramInt & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public boolean hasAlmanacData(int paramInt) {
    boolean bool;
    if ((this.mSvidWithFlags[paramInt] & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean usedInFix(int paramInt) {
    boolean bool;
    if ((this.mSvidWithFlags[paramInt] & 0x4) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean hasCarrierFrequencyHz(int paramInt) {
    boolean bool;
    if ((this.mSvidWithFlags[paramInt] & 0x8) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public float getCarrierFrequencyHz(int paramInt) {
    return this.mCarrierFrequencies[paramInt];
  }
  
  public boolean hasBasebandCn0DbHz(int paramInt) {
    boolean bool;
    if ((this.mSvidWithFlags[paramInt] & 0x10) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public float getBasebandCn0DbHz(int paramInt) {
    return this.mBasebandCn0DbHzs[paramInt];
  }
  
  public static String constellationTypeToString(int paramInt) {
    switch (paramInt) {
      default:
        return Integer.toString(paramInt);
      case 7:
        return "IRNSS";
      case 6:
        return "GALILEO";
      case 5:
        return "BEIDOU";
      case 4:
        return "QZSS";
      case 3:
        return "GLONASS";
      case 2:
        return "SBAS";
      case 1:
        return "GPS";
      case 0:
        break;
    } 
    return "UNKNOWN";
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof GnssStatus))
      return false; 
    paramObject = paramObject;
    if (this.mSvCount == ((GnssStatus)paramObject).mSvCount) {
      int[] arrayOfInt1 = this.mSvidWithFlags, arrayOfInt2 = ((GnssStatus)paramObject).mSvidWithFlags;
      if (Arrays.equals(arrayOfInt1, arrayOfInt2)) {
        float[] arrayOfFloat2 = this.mCn0DbHzs, arrayOfFloat1 = ((GnssStatus)paramObject).mCn0DbHzs;
        if (Arrays.equals(arrayOfFloat2, arrayOfFloat1)) {
          arrayOfFloat2 = this.mElevations;
          arrayOfFloat1 = ((GnssStatus)paramObject).mElevations;
          if (Arrays.equals(arrayOfFloat2, arrayOfFloat1)) {
            arrayOfFloat2 = this.mAzimuths;
            arrayOfFloat1 = ((GnssStatus)paramObject).mAzimuths;
            if (Arrays.equals(arrayOfFloat2, arrayOfFloat1)) {
              arrayOfFloat1 = this.mCarrierFrequencies;
              arrayOfFloat2 = ((GnssStatus)paramObject).mCarrierFrequencies;
              if (Arrays.equals(arrayOfFloat1, arrayOfFloat2)) {
                arrayOfFloat1 = this.mBasebandCn0DbHzs;
                paramObject = ((GnssStatus)paramObject).mBasebandCn0DbHzs;
                if (Arrays.equals(arrayOfFloat1, (float[])paramObject))
                  return null; 
              } 
            } 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = Objects.hash(new Object[] { Integer.valueOf(this.mSvCount) });
    int j = Arrays.hashCode(this.mSvidWithFlags);
    int k = Arrays.hashCode(this.mCn0DbHzs);
    return (i * 31 + j) * 31 + k;
  }
  
  public static final class Builder {
    private final ArrayList<GnssStatus.GnssSvInfo> mSatellites = new ArrayList<>();
    
    public Builder addSatellite(int param1Int1, int param1Int2, float param1Float1, float param1Float2, float param1Float3, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, boolean param1Boolean4, float param1Float4, boolean param1Boolean5, float param1Float5) {
      this.mSatellites.add(new GnssStatus.GnssSvInfo(param1Int1, param1Int2, param1Float1, param1Float2, param1Float3, param1Boolean1, param1Boolean2, param1Boolean3, param1Boolean4, param1Float4, param1Boolean5, param1Float5));
      return this;
    }
    
    public Builder clearSatellites() {
      this.mSatellites.clear();
      return this;
    }
    
    public GnssStatus build() {
      int i = this.mSatellites.size();
      int[] arrayOfInt = new int[i];
      float[] arrayOfFloat1 = new float[i];
      float[] arrayOfFloat2 = new float[i];
      float[] arrayOfFloat3 = new float[i];
      float[] arrayOfFloat4 = new float[i];
      float[] arrayOfFloat5 = new float[i];
      byte b;
      for (b = 0; b < arrayOfInt.length; b++)
        arrayOfInt[b] = ((GnssStatus.GnssSvInfo)this.mSatellites.get(b)).mSvidWithFlags; 
      for (b = 0; b < arrayOfFloat1.length; b++)
        arrayOfFloat1[b] = ((GnssStatus.GnssSvInfo)this.mSatellites.get(b)).mCn0DbHz; 
      for (b = 0; b < arrayOfFloat2.length; b++)
        arrayOfFloat2[b] = ((GnssStatus.GnssSvInfo)this.mSatellites.get(b)).mElevation; 
      for (b = 0; b < arrayOfFloat3.length; b++)
        arrayOfFloat3[b] = ((GnssStatus.GnssSvInfo)this.mSatellites.get(b)).mAzimuth; 
      for (b = 0; b < arrayOfFloat4.length; b++)
        arrayOfFloat4[b] = ((GnssStatus.GnssSvInfo)this.mSatellites.get(b)).mCarrierFrequency; 
      for (b = 0; b < arrayOfFloat5.length; b++)
        arrayOfFloat5[b] = ((GnssStatus.GnssSvInfo)this.mSatellites.get(b)).mBasebandCn0DbHz; 
      return GnssStatus.wrap(i, arrayOfInt, arrayOfFloat1, arrayOfFloat2, arrayOfFloat3, arrayOfFloat4, arrayOfFloat5);
    }
  }
  
  private static class GnssSvInfo {
    private final float mAzimuth;
    
    private final float mBasebandCn0DbHz;
    
    private final float mCarrierFrequency;
    
    private final float mCn0DbHz;
    
    private final float mElevation;
    
    private final int mSvidWithFlags;
    
    private GnssSvInfo(int param1Int1, int param1Int2, float param1Float1, float param1Float2, float param1Float3, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, boolean param1Boolean4, float param1Float4, boolean param1Boolean5, float param1Float5) {
      boolean bool1, bool2;
      byte b1 = 8;
      byte b2 = 0;
      if (param1Boolean2) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      if (param1Boolean3) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (!param1Boolean4)
        b1 = 0; 
      if (param1Boolean5)
        b2 = 16; 
      this.mSvidWithFlags = param1Int2 << 12 | (param1Int1 & 0xF) << 8 | param1Boolean1 | bool1 | bool2 | b1 | b2;
      this.mCn0DbHz = param1Float1;
      this.mElevation = param1Float2;
      this.mAzimuth = param1Float3;
      param1Float2 = 0.0F;
      if (param1Boolean4) {
        param1Float1 = param1Float4;
      } else {
        param1Float1 = 0.0F;
      } 
      this.mCarrierFrequency = param1Float1;
      param1Float1 = param1Float2;
      if (param1Boolean5)
        param1Float1 = param1Float5; 
      this.mBasebandCn0DbHz = param1Float1;
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ConstellationType {}
}
