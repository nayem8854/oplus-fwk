package android.location;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IGnssMeasurementsListener extends IInterface {
  void onGnssMeasurementsReceived(GnssMeasurementsEvent paramGnssMeasurementsEvent) throws RemoteException;
  
  void onStatusChanged(int paramInt) throws RemoteException;
  
  class Default implements IGnssMeasurementsListener {
    public void onGnssMeasurementsReceived(GnssMeasurementsEvent param1GnssMeasurementsEvent) throws RemoteException {}
    
    public void onStatusChanged(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IGnssMeasurementsListener {
    private static final String DESCRIPTOR = "android.location.IGnssMeasurementsListener";
    
    static final int TRANSACTION_onGnssMeasurementsReceived = 1;
    
    static final int TRANSACTION_onStatusChanged = 2;
    
    public Stub() {
      attachInterface(this, "android.location.IGnssMeasurementsListener");
    }
    
    public static IGnssMeasurementsListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.location.IGnssMeasurementsListener");
      if (iInterface != null && iInterface instanceof IGnssMeasurementsListener)
        return (IGnssMeasurementsListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onStatusChanged";
      } 
      return "onGnssMeasurementsReceived";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.location.IGnssMeasurementsListener");
          return true;
        } 
        param1Parcel1.enforceInterface("android.location.IGnssMeasurementsListener");
        param1Int1 = param1Parcel1.readInt();
        onStatusChanged(param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.location.IGnssMeasurementsListener");
      if (param1Parcel1.readInt() != 0) {
        GnssMeasurementsEvent gnssMeasurementsEvent = GnssMeasurementsEvent.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onGnssMeasurementsReceived((GnssMeasurementsEvent)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IGnssMeasurementsListener {
      public static IGnssMeasurementsListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.location.IGnssMeasurementsListener";
      }
      
      public void onGnssMeasurementsReceived(GnssMeasurementsEvent param2GnssMeasurementsEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.location.IGnssMeasurementsListener");
          if (param2GnssMeasurementsEvent != null) {
            parcel.writeInt(1);
            param2GnssMeasurementsEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IGnssMeasurementsListener.Stub.getDefaultImpl() != null) {
            IGnssMeasurementsListener.Stub.getDefaultImpl().onGnssMeasurementsReceived(param2GnssMeasurementsEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onStatusChanged(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.location.IGnssMeasurementsListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IGnssMeasurementsListener.Stub.getDefaultImpl() != null) {
            IGnssMeasurementsListener.Stub.getDefaultImpl().onStatusChanged(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IGnssMeasurementsListener param1IGnssMeasurementsListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IGnssMeasurementsListener != null) {
          Proxy.sDefaultImpl = param1IGnssMeasurementsListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IGnssMeasurementsListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
