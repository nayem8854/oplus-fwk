package android.location;

import android.os.Parcel;
import android.os.Parcelable;

public class FusedBatchOptions implements Parcelable {
  private volatile long mPeriodInNS = 0L;
  
  private volatile int mSourcesToUse = 0;
  
  private volatile int mFlags = 0;
  
  private volatile double mMaxPowerAllocationInMW = 0.0D;
  
  private volatile float mSmallestDisplacementMeters = 0.0F;
  
  public void setMaxPowerAllocationInMW(double paramDouble) {
    this.mMaxPowerAllocationInMW = paramDouble;
  }
  
  public double getMaxPowerAllocationInMW() {
    return this.mMaxPowerAllocationInMW;
  }
  
  public void setPeriodInNS(long paramLong) {
    this.mPeriodInNS = paramLong;
  }
  
  public long getPeriodInNS() {
    return this.mPeriodInNS;
  }
  
  public void setSmallestDisplacementMeters(float paramFloat) {
    this.mSmallestDisplacementMeters = paramFloat;
  }
  
  public float getSmallestDisplacementMeters() {
    return this.mSmallestDisplacementMeters;
  }
  
  public void setSourceToUse(int paramInt) {
    this.mSourcesToUse |= paramInt;
  }
  
  public void resetSourceToUse(int paramInt) {
    this.mSourcesToUse &= paramInt ^ 0xFFFFFFFF;
  }
  
  public boolean isSourceToUseSet(int paramInt) {
    boolean bool;
    if ((this.mSourcesToUse & paramInt) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getSourcesToUse() {
    return this.mSourcesToUse;
  }
  
  public void setFlag(int paramInt) {
    this.mFlags |= paramInt;
  }
  
  public void resetFlag(int paramInt) {
    this.mFlags &= paramInt ^ 0xFFFFFFFF;
  }
  
  public boolean isFlagSet(int paramInt) {
    boolean bool;
    if ((this.mFlags & paramInt) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getFlags() {
    return this.mFlags;
  }
  
  class SourceTechnologies {
    public static int BLUETOOTH;
    
    public static int CELL;
    
    public static int GNSS = 1;
    
    public static int SENSORS = 4;
    
    public static int WIFI = 2;
    
    static {
      CELL = 8;
      BLUETOOTH = 16;
    }
  }
  
  class BatchFlags {
    public static int CALLBACK_ON_LOCATION_FIX = 2;
    
    public static int WAKEUP_ON_FIFO_FULL = 1;
    
    static {
    
    }
  }
  
  public static final Parcelable.Creator<FusedBatchOptions> CREATOR = new Parcelable.Creator<FusedBatchOptions>() {
      public FusedBatchOptions createFromParcel(Parcel param1Parcel) {
        FusedBatchOptions fusedBatchOptions = new FusedBatchOptions();
        fusedBatchOptions.setMaxPowerAllocationInMW(param1Parcel.readDouble());
        fusedBatchOptions.setPeriodInNS(param1Parcel.readLong());
        fusedBatchOptions.setSourceToUse(param1Parcel.readInt());
        fusedBatchOptions.setFlag(param1Parcel.readInt());
        fusedBatchOptions.setSmallestDisplacementMeters(param1Parcel.readFloat());
        return fusedBatchOptions;
      }
      
      public FusedBatchOptions[] newArray(int param1Int) {
        return new FusedBatchOptions[param1Int];
      }
    };
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeDouble(this.mMaxPowerAllocationInMW);
    paramParcel.writeLong(this.mPeriodInNS);
    paramParcel.writeInt(this.mSourcesToUse);
    paramParcel.writeInt(this.mFlags);
    paramParcel.writeFloat(this.mSmallestDisplacementMeters);
  }
}
