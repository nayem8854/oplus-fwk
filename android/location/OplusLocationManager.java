package android.location;

import android.content.Context;
import android.location.interfaces.IOplusLocationManager;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public class OplusLocationManager extends OplusBaseLocationManager implements IOplusLocationManager {
  private final String TAG = "OplusLocationManager";
  
  private Context mContext = null;
  
  public OplusLocationManager(Context paramContext) {
    this.mContext = paramContext;
  }
  
  public OplusLocationManager() {
    this.mContext = null;
  }
  
  public void getLocAppsOp(int paramInt, LocAppsOp paramLocAppsOp) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.location.ILocationManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(20002, parcel1, parcel2, 0);
      parcel2.readException();
      if (parcel2.readInt() != 0)
        paramLocAppsOp.readFromParcel(parcel2); 
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setLocAppsOp(int paramInt, LocAppsOp paramLocAppsOp) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.location.ILocationManager");
      parcel1.writeInt(paramInt);
      if (paramLocAppsOp != null) {
        parcel1.writeInt(1);
        paramLocAppsOp.writeToParcel(parcel1, 0);
      } else {
        parcel1.writeInt(0);
      } 
      this.mRemote.transact(20003, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<String> getInUsePackagesList() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.location.ILocationManager");
      this.mRemote.transact(20004, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.createStringArrayList();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
}
