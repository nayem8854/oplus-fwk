package android.location;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.security.InvalidParameterException;

@SystemApi
@Deprecated
public class GpsNavigationMessageEvent implements Parcelable {
  public static final Parcelable.Creator<GpsNavigationMessageEvent> CREATOR;
  
  public static int STATUS_GPS_LOCATION_DISABLED;
  
  public static int STATUS_NOT_SUPPORTED = 0;
  
  public static int STATUS_READY = 1;
  
  private final GpsNavigationMessage mNavigationMessage;
  
  static {
    STATUS_GPS_LOCATION_DISABLED = 2;
    CREATOR = new Parcelable.Creator<GpsNavigationMessageEvent>() {
        public GpsNavigationMessageEvent createFromParcel(Parcel param1Parcel) {
          ClassLoader classLoader = getClass().getClassLoader();
          GpsNavigationMessage gpsNavigationMessage = param1Parcel.<GpsNavigationMessage>readParcelable(classLoader);
          return new GpsNavigationMessageEvent(gpsNavigationMessage);
        }
        
        public GpsNavigationMessageEvent[] newArray(int param1Int) {
          return new GpsNavigationMessageEvent[param1Int];
        }
      };
  }
  
  public GpsNavigationMessageEvent(GpsNavigationMessage paramGpsNavigationMessage) {
    if (paramGpsNavigationMessage != null) {
      this.mNavigationMessage = paramGpsNavigationMessage;
      return;
    } 
    throw new InvalidParameterException("Parameter 'message' must not be null.");
  }
  
  public GpsNavigationMessage getNavigationMessage() {
    return this.mNavigationMessage;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.mNavigationMessage, paramInt);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("[ GpsNavigationMessageEvent:\n\n");
    stringBuilder.append(this.mNavigationMessage.toString());
    stringBuilder.append("\n]");
    return stringBuilder.toString();
  }
  
  @SystemApi
  class Listener {
    public abstract void onGpsNavigationMessageReceived(GpsNavigationMessageEvent param1GpsNavigationMessageEvent);
    
    public abstract void onStatusChanged(int param1Int);
  }
}
