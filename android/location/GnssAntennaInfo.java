package android.location;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public final class GnssAntennaInfo implements Parcelable {
  public static final class PhaseCenterOffset implements Parcelable {
    public PhaseCenterOffset(double param1Double1, double param1Double2, double param1Double3, double param1Double4, double param1Double5, double param1Double6) {
      this.mOffsetXMm = param1Double1;
      this.mOffsetYMm = param1Double3;
      this.mOffsetZMm = param1Double5;
      this.mOffsetXUncertaintyMm = param1Double2;
      this.mOffsetYUncertaintyMm = param1Double4;
      this.mOffsetZUncertaintyMm = param1Double6;
    }
    
    public static final Parcelable.Creator<PhaseCenterOffset> CREATOR = (Parcelable.Creator<PhaseCenterOffset>)new Object();
    
    private final double mOffsetXMm;
    
    private final double mOffsetXUncertaintyMm;
    
    private final double mOffsetYMm;
    
    private final double mOffsetYUncertaintyMm;
    
    private final double mOffsetZMm;
    
    private final double mOffsetZUncertaintyMm;
    
    public double getXOffsetMm() {
      return this.mOffsetXMm;
    }
    
    public double getXOffsetUncertaintyMm() {
      return this.mOffsetXUncertaintyMm;
    }
    
    public double getYOffsetMm() {
      return this.mOffsetYMm;
    }
    
    public double getYOffsetUncertaintyMm() {
      return this.mOffsetYUncertaintyMm;
    }
    
    public double getZOffsetMm() {
      return this.mOffsetZMm;
    }
    
    public double getZOffsetUncertaintyMm() {
      return this.mOffsetZUncertaintyMm;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeDouble(this.mOffsetXMm);
      param1Parcel.writeDouble(this.mOffsetXUncertaintyMm);
      param1Parcel.writeDouble(this.mOffsetYMm);
      param1Parcel.writeDouble(this.mOffsetYUncertaintyMm);
      param1Parcel.writeDouble(this.mOffsetZMm);
      param1Parcel.writeDouble(this.mOffsetZUncertaintyMm);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("PhaseCenterOffset{OffsetXMm=");
      stringBuilder.append(this.mOffsetXMm);
      stringBuilder.append(" +/-");
      stringBuilder.append(this.mOffsetXUncertaintyMm);
      stringBuilder.append(", OffsetYMm=");
      stringBuilder.append(this.mOffsetYMm);
      stringBuilder.append(" +/-");
      stringBuilder.append(this.mOffsetYUncertaintyMm);
      stringBuilder.append(", OffsetZMm=");
      stringBuilder.append(this.mOffsetZMm);
      stringBuilder.append(" +/-");
      stringBuilder.append(this.mOffsetZUncertaintyMm);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
  }
  
  public static final class SphericalCorrections implements Parcelable {
    public SphericalCorrections(double[][] param1ArrayOfdouble1, double[][] param1ArrayOfdouble2) {
      if (param1ArrayOfdouble1.length == param1ArrayOfdouble2.length && (param1ArrayOfdouble1[0]).length == (param1ArrayOfdouble2[0]).length) {
        int i = param1ArrayOfdouble1.length;
        if (i >= 1) {
          int j = (param1ArrayOfdouble1[0]).length;
          if (j >= 2) {
            this.mCorrections = param1ArrayOfdouble1;
            this.mCorrectionUncertainties = param1ArrayOfdouble2;
            this.mDeltaTheta = 360.0D / i;
            this.mDeltaPhi = 180.0D / (j - 1);
            return;
          } 
          throw new IllegalArgumentException("Arrays must have at least two columns.");
        } 
        throw new IllegalArgumentException("Arrays must have at least one row.");
      } 
      throw new IllegalArgumentException("Correction and correction uncertainty arrays must have the same dimensions.");
    }
    
    SphericalCorrections(Parcel param1Parcel) {
      int i = param1Parcel.readInt();
      int j = param1Parcel.readInt();
      double[][] arrayOfDouble1 = new double[i][j];
      double[][] arrayOfDouble2 = new double[i][j];
      byte b;
      for (b = 0; b < i; b++)
        param1Parcel.readDoubleArray(arrayOfDouble1[b]); 
      for (b = 0; b < i; b++)
        param1Parcel.readDoubleArray(arrayOfDouble2[b]); 
      this.mNumRows = i;
      this.mNumColumns = j;
      this.mCorrections = arrayOfDouble1;
      this.mCorrectionUncertainties = arrayOfDouble2;
      this.mDeltaTheta = 360.0D / i;
      this.mDeltaPhi = 180.0D / (j - 1);
    }
    
    public double[][] getCorrectionsArray() {
      return this.mCorrections;
    }
    
    public double[][] getCorrectionUncertaintiesArray() {
      return this.mCorrectionUncertainties;
    }
    
    public double getDeltaTheta() {
      return this.mDeltaTheta;
    }
    
    public double getDeltaPhi() {
      return this.mDeltaPhi;
    }
    
    public static final Parcelable.Creator<SphericalCorrections> CREATOR = new Parcelable.Creator<SphericalCorrections>() {
        public GnssAntennaInfo.SphericalCorrections createFromParcel(Parcel param2Parcel) {
          return new GnssAntennaInfo.SphericalCorrections(param2Parcel);
        }
        
        public GnssAntennaInfo.SphericalCorrections[] newArray(int param2Int) {
          return new GnssAntennaInfo.SphericalCorrections[param2Int];
        }
      };
    
    private final double[][] mCorrectionUncertainties;
    
    private final double[][] mCorrections;
    
    private final double mDeltaPhi;
    
    private final double mDeltaTheta;
    
    private final int mNumColumns;
    
    private final int mNumRows;
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mNumRows);
      param1Parcel.writeInt(this.mNumColumns);
      int i;
      boolean bool;
      for (double[][] arrayOfDouble1 = this.mCorrections; param1Int < i; ) {
        double[] arrayOfDouble = arrayOfDouble1[param1Int];
        param1Parcel.writeDoubleArray(arrayOfDouble);
        param1Int++;
      } 
      for (double[][] arrayOfDouble2 = this.mCorrectionUncertainties; param1Int < i; ) {
        double[] arrayOfDouble = arrayOfDouble2[param1Int];
        param1Parcel.writeDoubleArray(arrayOfDouble);
        param1Int++;
      } 
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("SphericalCorrections{Corrections=");
      double[][] arrayOfDouble = this.mCorrections;
      stringBuilder.append(Arrays.toString((Object[])arrayOfDouble));
      stringBuilder.append(", CorrectionUncertainties=");
      arrayOfDouble = this.mCorrectionUncertainties;
      stringBuilder.append(Arrays.toString((Object[])arrayOfDouble));
      stringBuilder.append(", DeltaTheta=");
      stringBuilder.append(this.mDeltaTheta);
      stringBuilder.append(", DeltaPhi=");
      stringBuilder.append(this.mDeltaPhi);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
  }
  
  class null implements Parcelable.Creator<SphericalCorrections> {
    public GnssAntennaInfo.SphericalCorrections createFromParcel(Parcel param1Parcel) {
      return new GnssAntennaInfo.SphericalCorrections(param1Parcel);
    }
    
    public GnssAntennaInfo.SphericalCorrections[] newArray(int param1Int) {
      return new GnssAntennaInfo.SphericalCorrections[param1Int];
    }
  }
  
  private GnssAntennaInfo(double paramDouble, PhaseCenterOffset paramPhaseCenterOffset, SphericalCorrections paramSphericalCorrections1, SphericalCorrections paramSphericalCorrections2) {
    if (paramPhaseCenterOffset != null) {
      this.mCarrierFrequencyMHz = paramDouble;
      this.mPhaseCenterOffset = paramPhaseCenterOffset;
      this.mPhaseCenterVariationCorrections = paramSphericalCorrections1;
      this.mSignalGainCorrections = paramSphericalCorrections2;
      return;
    } 
    throw new IllegalArgumentException("Phase Center Offset Coordinates cannot be null.");
  }
  
  class Builder {
    private double mCarrierFrequencyMHz;
    
    private GnssAntennaInfo.PhaseCenterOffset mPhaseCenterOffset;
    
    private GnssAntennaInfo.SphericalCorrections mPhaseCenterVariationCorrections;
    
    private GnssAntennaInfo.SphericalCorrections mSignalGainCorrections;
    
    public Builder setCarrierFrequencyMHz(double param1Double) {
      this.mCarrierFrequencyMHz = param1Double;
      return this;
    }
    
    public Builder setPhaseCenterOffset(GnssAntennaInfo.PhaseCenterOffset param1PhaseCenterOffset) {
      Objects.requireNonNull(param1PhaseCenterOffset);
      this.mPhaseCenterOffset = param1PhaseCenterOffset;
      return this;
    }
    
    public Builder setPhaseCenterVariationCorrections(GnssAntennaInfo.SphericalCorrections param1SphericalCorrections) {
      this.mPhaseCenterVariationCorrections = param1SphericalCorrections;
      return this;
    }
    
    public Builder setSignalGainCorrections(GnssAntennaInfo.SphericalCorrections param1SphericalCorrections) {
      this.mSignalGainCorrections = param1SphericalCorrections;
      return this;
    }
    
    public GnssAntennaInfo build() {
      return new GnssAntennaInfo(this.mCarrierFrequencyMHz, this.mPhaseCenterOffset, this.mPhaseCenterVariationCorrections, this.mSignalGainCorrections);
    }
  }
  
  public double getCarrierFrequencyMHz() {
    return this.mCarrierFrequencyMHz;
  }
  
  public PhaseCenterOffset getPhaseCenterOffset() {
    return this.mPhaseCenterOffset;
  }
  
  public SphericalCorrections getPhaseCenterVariationCorrections() {
    return this.mPhaseCenterVariationCorrections;
  }
  
  public SphericalCorrections getSignalGainCorrections() {
    return this.mSignalGainCorrections;
  }
  
  public static final Parcelable.Creator<GnssAntennaInfo> CREATOR = (Parcelable.Creator<GnssAntennaInfo>)new Object();
  
  private final double mCarrierFrequencyMHz;
  
  private final PhaseCenterOffset mPhaseCenterOffset;
  
  private final SphericalCorrections mPhaseCenterVariationCorrections;
  
  private final SphericalCorrections mSignalGainCorrections;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeDouble(this.mCarrierFrequencyMHz);
    paramParcel.writeParcelable(this.mPhaseCenterOffset, paramInt);
    paramParcel.writeParcelable(this.mPhaseCenterVariationCorrections, paramInt);
    paramParcel.writeParcelable(this.mSignalGainCorrections, paramInt);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("GnssAntennaInfo{CarrierFrequencyMHz=");
    stringBuilder.append(this.mCarrierFrequencyMHz);
    stringBuilder.append(", PhaseCenterOffset=");
    stringBuilder.append(this.mPhaseCenterOffset);
    stringBuilder.append(", PhaseCenterVariationCorrections=");
    stringBuilder.append(this.mPhaseCenterVariationCorrections);
    stringBuilder.append(", SignalGainCorrections=");
    stringBuilder.append(this.mSignalGainCorrections);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  class Listener {
    public abstract void onGnssAntennaInfoReceived(List<GnssAntennaInfo> param1List);
  }
}
