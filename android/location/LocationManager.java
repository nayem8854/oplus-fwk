package android.location;

import android.annotation.SystemApi;
import android.app.AlarmManager;
import android.app.AppOpsManager;
import android.app.PendingIntent;
import android.app.PropertyInvalidatedCache;
import android.compat.Compatibility;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Binder;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.HandlerExecutor;
import android.os.ICancellationSignal;
import android.os.Looper;
import android.os.Process;
import android.os.RemoteException;
import android.os.UserHandle;
import android.provider.Settings;
import android.util.ArrayMap;
import android.util.Log;
import android.util.SeempLog;
import com.android.internal.location.ProviderProperties;
import com.android.internal.util.Preconditions;
import com.android.internal.util.function.QuadConsumer;
import com.android.internal.util.function.TriConsumer;
import com.android.internal.util.function.pooled.PooledLambda;
import com.android.internal.util.function.pooled.PooledRunnable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import java.util.function.Consumer;

public class LocationManager {
  private PropertyInvalidatedCache<Integer, Boolean> mLocationEnabledCache = (PropertyInvalidatedCache<Integer, Boolean>)new Object(this, 4, "cache_key.location_enabled");
  
  private final Object mLock = new Object();
  
  private final ArrayMap<LocationListener, LocationListenerTransport> mListeners = new ArrayMap();
  
  private final BatchedLocationCallbackManager mBatchedLocationCallbackManager = new BatchedLocationCallbackManager();
  
  private final GnssStatusListenerManager mGnssStatusListenerManager = new GnssStatusListenerManager();
  
  private final GnssMeasurementsListenerManager mGnssMeasurementsListenerManager = new GnssMeasurementsListenerManager();
  
  private final GnssNavigationMessageListenerManager mGnssNavigationMessageListenerTransport = new GnssNavigationMessageListenerManager();
  
  private final GnssAntennaInfoListenerManager mGnssAntennaInfoListenerManager = new GnssAntennaInfoListenerManager();
  
  public static final String CACHE_KEY_LOCATION_ENABLED_PROPERTY = "cache_key.location_enabled";
  
  public static final String EXTRA_LOCATION_ENABLED = "android.location.extra.LOCATION_ENABLED";
  
  public static final String EXTRA_PROVIDER_ENABLED = "android.location.extra.PROVIDER_ENABLED";
  
  public static final String EXTRA_PROVIDER_NAME = "android.location.extra.PROVIDER_NAME";
  
  public static final String FUSED_PROVIDER = "fused";
  
  private static final long GET_CURRENT_LOCATION_MAX_TIMEOUT_MS = 30000L;
  
  private static final long GET_PROVIDER_SECURITY_EXCEPTIONS = 150935354L;
  
  public static final String GPS_PROVIDER = "gps";
  
  private static final long GPS_STATUS_USAGE = 144027538L;
  
  public static final String HIGH_POWER_REQUEST_CHANGE_ACTION = "android.location.HIGH_POWER_REQUEST_CHANGE";
  
  private static final long INCOMPLETE_LOCATION = 148964793L;
  
  public static final String KEY_LOCATION_CHANGED = "location";
  
  public static final String KEY_PROVIDER_ENABLED = "providerEnabled";
  
  public static final String KEY_PROXIMITY_ENTERING = "entering";
  
  @Deprecated
  public static final String KEY_STATUS_CHANGED = "status";
  
  public static final String METADATA_SETTINGS_FOOTER_STRING = "com.android.settings.location.FOOTER_STRING";
  
  public static final String MODE_CHANGED_ACTION = "android.location.MODE_CHANGED";
  
  public static final String NETWORK_PROVIDER = "network";
  
  public static final String PASSIVE_PROVIDER = "passive";
  
  public static final String PROVIDERS_CHANGED_ACTION = "android.location.PROVIDERS_CHANGED";
  
  public static final String SETTINGS_FOOTER_DISPLAYED_ACTION = "com.android.settings.location.DISPLAYED_FOOTER";
  
  private static final long TARGETED_PENDING_INTENT = 148963590L;
  
  private final Context mContext;
  
  private final ILocationManager mService;
  
  public LocationManager(Context paramContext, ILocationManager paramILocationManager) {
    this.mService = paramILocationManager;
    this.mContext = paramContext;
  }
  
  public String[] getBackgroundThrottlingWhitelist() {
    try {
      return this.mService.getBackgroundThrottlingWhitelist();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String[] getIgnoreSettingsWhitelist() {
    try {
      return this.mService.getIgnoreSettingsWhitelist();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public String getExtraLocationControllerPackage() {
    try {
      return this.mService.getExtraLocationControllerPackage();
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
      return null;
    } 
  }
  
  @SystemApi
  public void setExtraLocationControllerPackage(String paramString) {
    try {
      this.mService.setExtraLocationControllerPackage(paramString);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void setExtraLocationControllerPackageEnabled(boolean paramBoolean) {
    try {
      this.mService.setExtraLocationControllerPackageEnabled(paramBoolean);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean isExtraLocationControllerPackageEnabled() {
    try {
      return this.mService.isExtraLocationControllerPackageEnabled();
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
      return false;
    } 
  }
  
  @SystemApi
  @Deprecated
  public void setLocationControllerExtraPackage(String paramString) {
    try {
      this.mService.setExtraLocationControllerPackage(paramString);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  @Deprecated
  public void setLocationControllerExtraPackageEnabled(boolean paramBoolean) {
    try {
      this.mService.setExtraLocationControllerPackageEnabled(paramBoolean);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isLocationEnabled() {
    return isLocationEnabledForUser(Process.myUserHandle());
  }
  
  @SystemApi
  public boolean isLocationEnabledForUser(UserHandle paramUserHandle) {
    synchronized (this.mLock) {
      if (this.mLocationEnabledCache != null)
        return ((Boolean)this.mLocationEnabledCache.query(Integer.valueOf(paramUserHandle.getIdentifier()))).booleanValue(); 
      try {
        return this.mService.isLocationEnabledForUser(paramUserHandle.getIdentifier());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
  }
  
  @SystemApi
  public void setLocationEnabledForUser(boolean paramBoolean, UserHandle paramUserHandle) {
    try {
      this.mService.setLocationEnabledForUser(paramBoolean, paramUserHandle.getIdentifier());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isProviderEnabled(String paramString) {
    return isProviderEnabledForUser(paramString, Process.myUserHandle());
  }
  
  @SystemApi
  public boolean isProviderEnabledForUser(String paramString, UserHandle paramUserHandle) {
    boolean bool;
    if (paramString != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null provider");
    try {
      return this.mService.isProviderEnabledForUser(paramString, paramUserHandle.getIdentifier());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  @Deprecated
  public boolean setProviderEnabledForUser(String paramString, boolean paramBoolean, UserHandle paramUserHandle) {
    boolean bool;
    String str;
    if (paramString != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null provider");
    Context context = this.mContext;
    ContentResolver contentResolver = context.getContentResolver();
    StringBuilder stringBuilder = new StringBuilder();
    if (paramBoolean) {
      str = "+";
    } else {
      str = "-";
    } 
    stringBuilder.append(str);
    stringBuilder.append(paramString);
    paramString = stringBuilder.toString();
    int i = paramUserHandle.getIdentifier();
    return Settings.Secure.putStringForUser(contentResolver, "location_providers_allowed", paramString, i);
  }
  
  public Location getLastLocation() {
    try {
      ILocationManager iLocationManager = this.mService;
      String str1 = this.mContext.getPackageName();
      Context context = this.mContext;
      String str2 = context.getAttributionTag();
      return iLocationManager.getLastLocation(null, str1, str2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Location getLastKnownLocation(String paramString) {
    boolean bool;
    SeempLog.record(46);
    if (paramString != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null provider");
    LocationRequest locationRequest = LocationRequest.createFromDeprecatedProvider(paramString, 0L, 0.0F, true);
    try {
      ILocationManager iLocationManager = this.mService;
      paramString = this.mContext.getPackageName();
      Context context = this.mContext;
      String str = context.getAttributionTag();
      return iLocationManager.getLastLocation(locationRequest, paramString, str);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (ClassCastException classCastException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ClassCastException: ");
      stringBuilder.append(this.mContext.getPackageName());
      Log.e("LocationManager", stringBuilder.toString(), classCastException);
      throw classCastException;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Exception: ");
      stringBuilder.append(this.mContext.getPackageName());
      Log.e("LocationManager", stringBuilder.toString(), exception);
      throw exception;
    } 
  }
  
  public void getCurrentLocation(String paramString, CancellationSignal paramCancellationSignal, Executor paramExecutor, Consumer<Location> paramConsumer) {
    getCurrentLocation(LocationRequest.createFromDeprecatedProvider(paramString, 0L, 0.0F, true), paramCancellationSignal, paramExecutor, paramConsumer);
  }
  
  @SystemApi
  public void getCurrentLocation(LocationRequest paramLocationRequest, CancellationSignal paramCancellationSignal, Executor paramExecutor, Consumer<Location> paramConsumer) {
    paramLocationRequest = new LocationRequest(paramLocationRequest);
    paramLocationRequest = paramLocationRequest.setNumUpdates(1);
    if (paramLocationRequest.getExpireIn() > 30000L)
      paramLocationRequest.setExpireIn(30000L); 
    GetCurrentLocationTransport getCurrentLocationTransport = new GetCurrentLocationTransport(paramConsumer);
    if (paramCancellationSignal != null)
      paramCancellationSignal.throwIfCanceled(); 
    ICancellationSignal iCancellationSignal = CancellationSignal.createTransport();
    try {
      ILocationManager iLocationManager = this.mService;
      Context context = this.mContext;
      String str2 = context.getPackageName(), str1 = this.mContext.getAttributionTag();
      String str3 = getCurrentLocationTransport.getListenerId();
      if (iLocationManager.getCurrentLocation(paramLocationRequest, iCancellationSignal, getCurrentLocationTransport, str2, str1, str3)) {
        getCurrentLocationTransport.register((AlarmManager)this.mContext.getSystemService(AlarmManager.class), iCancellationSignal);
        if (paramCancellationSignal != null) {
          Objects.requireNonNull(getCurrentLocationTransport);
          _$$Lambda$DG2BOD_OS4BJGp02JB18JR3FZ6s _$$Lambda$DG2BOD_OS4BJGp02JB18JR3FZ6s = new _$$Lambda$DG2BOD_OS4BJGp02JB18JR3FZ6s();
          this(getCurrentLocationTransport);
          paramCancellationSignal.setOnCancelListener(_$$Lambda$DG2BOD_OS4BJGp02JB18JR3FZ6s);
        } 
      } else {
        getCurrentLocationTransport.fail();
      } 
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void requestSingleUpdate(String paramString, LocationListener paramLocationListener, Looper paramLooper) {
    SeempLog.record(64);
    boolean bool1 = false;
    if (paramString != null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid null provider");
    boolean bool2 = bool1;
    if (paramLocationListener != null)
      bool2 = true; 
    Preconditions.checkArgument(bool2, "invalid null listener");
    LocationRequest locationRequest = LocationRequest.createFromDeprecatedProvider(paramString, 0L, 0.0F, true);
    locationRequest.setExpireIn(30000L);
    requestLocationUpdates(locationRequest, paramLocationListener, paramLooper);
  }
  
  @Deprecated
  public void requestSingleUpdate(Criteria paramCriteria, LocationListener paramLocationListener, Looper paramLooper) {
    SeempLog.record(64);
    boolean bool1 = false;
    if (paramCriteria != null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid null criteria");
    boolean bool2 = bool1;
    if (paramLocationListener != null)
      bool2 = true; 
    Preconditions.checkArgument(bool2, "invalid null listener");
    LocationRequest locationRequest = LocationRequest.createFromDeprecatedCriteria(paramCriteria, 0L, 0.0F, true);
    locationRequest.setExpireIn(30000L);
    requestLocationUpdates(locationRequest, paramLocationListener, paramLooper);
  }
  
  @Deprecated
  public void requestSingleUpdate(String paramString, PendingIntent paramPendingIntent) {
    boolean bool;
    SeempLog.record(64);
    if (paramString != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null provider");
    LocationRequest locationRequest = LocationRequest.createFromDeprecatedProvider(paramString, 0L, 0.0F, true);
    locationRequest.setExpireIn(30000L);
    requestLocationUpdates(locationRequest, paramPendingIntent);
  }
  
  @Deprecated
  public void requestSingleUpdate(Criteria paramCriteria, PendingIntent paramPendingIntent) {
    boolean bool;
    SeempLog.record(64);
    if (paramCriteria != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null criteria");
    LocationRequest locationRequest = LocationRequest.createFromDeprecatedCriteria(paramCriteria, 0L, 0.0F, true);
    locationRequest.setExpireIn(30000L);
    requestLocationUpdates(locationRequest, paramPendingIntent);
  }
  
  public void requestLocationUpdates(String paramString, long paramLong, float paramFloat, LocationListener paramLocationListener) {
    boolean bool2;
    SeempLog.record(47);
    boolean bool1 = true;
    if (paramString != null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid null provider");
    if (paramLocationListener != null) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid null listener");
    LocationRequest locationRequest = LocationRequest.createFromDeprecatedProvider(paramString, paramLong, paramFloat, false);
    requestLocationUpdates(locationRequest, paramLocationListener, (Looper)null);
  }
  
  public void requestLocationUpdates(String paramString, long paramLong, float paramFloat, LocationListener paramLocationListener, Looper paramLooper) {
    boolean bool2;
    SeempLog.record(47);
    boolean bool1 = true;
    if (paramString != null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid null provider");
    if (paramLocationListener != null) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid null listener");
    LocationRequest locationRequest = LocationRequest.createFromDeprecatedProvider(paramString, paramLong, paramFloat, false);
    requestLocationUpdates(locationRequest, paramLocationListener, paramLooper);
  }
  
  public void requestLocationUpdates(String paramString, long paramLong, float paramFloat, Executor paramExecutor, LocationListener paramLocationListener) {
    SeempLog.record(47);
    LocationRequest locationRequest = LocationRequest.createFromDeprecatedProvider(paramString, paramLong, paramFloat, false);
    requestLocationUpdates(locationRequest, paramExecutor, paramLocationListener);
  }
  
  public void requestLocationUpdates(long paramLong, float paramFloat, Criteria paramCriteria, LocationListener paramLocationListener, Looper paramLooper) {
    boolean bool2;
    SeempLog.record(47);
    boolean bool1 = true;
    if (paramCriteria != null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid null criteria");
    if (paramLocationListener != null) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid null listener");
    LocationRequest locationRequest = LocationRequest.createFromDeprecatedCriteria(paramCriteria, paramLong, paramFloat, false);
    requestLocationUpdates(locationRequest, paramLocationListener, paramLooper);
  }
  
  public void requestLocationUpdates(long paramLong, float paramFloat, Criteria paramCriteria, Executor paramExecutor, LocationListener paramLocationListener) {
    SeempLog.record(47);
    LocationRequest locationRequest = LocationRequest.createFromDeprecatedCriteria(paramCriteria, paramLong, paramFloat, false);
    requestLocationUpdates(locationRequest, paramExecutor, paramLocationListener);
  }
  
  public void requestLocationUpdates(String paramString, long paramLong, float paramFloat, PendingIntent paramPendingIntent) {
    boolean bool;
    SeempLog.record(47);
    if (paramString != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null provider");
    LocationRequest locationRequest = LocationRequest.createFromDeprecatedProvider(paramString, paramLong, paramFloat, false);
    requestLocationUpdates(locationRequest, paramPendingIntent);
  }
  
  public void requestLocationUpdates(long paramLong, float paramFloat, Criteria paramCriteria, PendingIntent paramPendingIntent) {
    boolean bool;
    SeempLog.record(47);
    if (paramCriteria != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null criteria");
    LocationRequest locationRequest = LocationRequest.createFromDeprecatedCriteria(paramCriteria, paramLong, paramFloat, false);
    requestLocationUpdates(locationRequest, paramPendingIntent);
  }
  
  @SystemApi
  public void requestLocationUpdates(LocationRequest paramLocationRequest, LocationListener paramLocationListener, Looper paramLooper) {
    SeempLog.record(47);
    Handler handler = new Handler();
    if (paramLooper == null) {
      this();
    } else {
      this(paramLooper);
    } 
    requestLocationUpdates(paramLocationRequest, new HandlerExecutor(handler), paramLocationListener);
  }
  
  @SystemApi
  public void requestLocationUpdates(LocationRequest paramLocationRequest, Executor paramExecutor, LocationListener paramLocationListener) {
    SeempLog.record(47);
    synchronized (this.mListeners) {
      LocationListenerTransport locationListenerTransport = (LocationListenerTransport)this.mListeners.get(paramLocationListener);
      if (locationListenerTransport != null) {
        locationListenerTransport.unregister();
      } else {
        locationListenerTransport = new LocationListenerTransport();
        this(this, paramLocationListener);
        this.mListeners.put(paramLocationListener, locationListenerTransport);
      } 
      locationListenerTransport.register(paramExecutor);
      try {
        ILocationManager iLocationManager = this.mService;
        Context context = this.mContext;
        String str1 = context.getPackageName(), str2 = this.mContext.getAttributionTag();
        String str3 = locationListenerTransport.getListenerId();
        iLocationManager.requestLocationUpdates(paramLocationRequest, locationListenerTransport, null, str1, str2, str3);
        if (!true) {
          locationListenerTransport.unregister();
          this.mListeners.remove(paramLocationListener);
        } 
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } finally {}
      if (!false) {
        locationListenerTransport.unregister();
        this.mListeners.remove(paramLocationListener);
      } 
      throw paramLocationRequest;
    } 
  }
  
  @SystemApi
  public void requestLocationUpdates(LocationRequest paramLocationRequest, PendingIntent paramPendingIntent) {
    boolean bool2;
    SeempLog.record(47);
    boolean bool1 = true;
    if (paramLocationRequest != null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid null location request");
    if (paramPendingIntent != null) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid null pending intent");
    if (Compatibility.isChangeEnabled(148963590L))
      Preconditions.checkArgument(paramPendingIntent.isTargetedToPackage(), "pending intent must be targeted to a package"); 
    try {
      ILocationManager iLocationManager = this.mService;
      Context context = this.mContext;
      String str2 = context.getPackageName(), str1 = this.mContext.getAttributionTag();
      iLocationManager.requestLocationUpdates(paramLocationRequest, null, paramPendingIntent, str2, str1, null);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean injectLocation(Location paramLocation) {
    boolean bool;
    if (paramLocation != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null location");
    Preconditions.checkArgument(paramLocation.isComplete(), "incomplete location object, missing timestamp or accuracy?");
    try {
      this.mService.injectLocation(paramLocation);
      return true;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void removeUpdates(LocationListener paramLocationListener) {
    boolean bool;
    if (paramLocationListener != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null listener");
    synchronized (this.mListeners) {
      LocationListenerTransport locationListenerTransport = (LocationListenerTransport)this.mListeners.remove(paramLocationListener);
      if (locationListenerTransport == null)
        return; 
      locationListenerTransport.unregister();
      try {
        this.mService.removeUpdates(locationListenerTransport, null);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
  }
  
  public void removeUpdates(PendingIntent paramPendingIntent) {
    boolean bool;
    if (paramPendingIntent != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null pending intent");
    try {
      this.mService.removeUpdates(null, paramPendingIntent);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<String> getAllProviders() {
    try {
      return this.mService.getAllProviders();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<String> getProviders(boolean paramBoolean) {
    try {
      return this.mService.getProviders(null, paramBoolean);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<String> getProviders(Criteria paramCriteria, boolean paramBoolean) {
    boolean bool;
    if (paramCriteria != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null criteria");
    try {
      return this.mService.getProviders(paramCriteria, paramBoolean);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String getBestProvider(Criteria paramCriteria, boolean paramBoolean) {
    boolean bool;
    if (paramCriteria != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null criteria");
    try {
      return this.mService.getBestProvider(paramCriteria, paramBoolean);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public LocationProvider getProvider(String paramString) {
    boolean bool;
    if (paramString != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null provider");
    if (!Compatibility.isChangeEnabled(150935354L))
      if ("network".equals(paramString) || "fused".equals(paramString)) {
        try {
          Context context = this.mContext;
          int i = Process.myPid();
          int j = Process.myUid();
          context.enforcePermission("android.permission.ACCESS_FINE_LOCATION", i, j, null);
        } catch (SecurityException securityException) {
          Context context = this.mContext;
          int i = Process.myPid();
          int j = Process.myUid();
          context.enforcePermission("android.permission.ACCESS_COARSE_LOCATION", i, j, null);
        } 
      } else {
        this.mContext.enforcePermission("android.permission.ACCESS_FINE_LOCATION", Process.myPid(), Process.myUid(), null);
      }  
    try {
      ProviderProperties providerProperties = this.mService.getProviderProperties(paramString);
      if (providerProperties == null)
        return null; 
      return new LocationProvider(paramString, providerProperties);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean isProviderPackage(String paramString) {
    try {
      return this.mService.isProviderPackage(paramString);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
      return false;
    } 
  }
  
  public List<String> getProviderPackages(String paramString) {
    try {
      return this.mService.getProviderPackages(paramString);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
      return Collections.emptyList();
    } 
  }
  
  public boolean sendExtraCommand(String paramString1, String paramString2, Bundle paramBundle) {
    boolean bool2;
    SeempLog.record(48);
    boolean bool1 = true;
    if (paramString1 != null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid null provider");
    if (paramString2 != null) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid null command");
    try {
      return this.mService.sendExtraCommand(paramString1, paramString2, paramBundle);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void addTestProvider(String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, int paramInt1, int paramInt2) {
    boolean bool;
    if (paramString != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null provider");
    ProviderProperties providerProperties = new ProviderProperties(paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4, paramBoolean5, paramBoolean6, paramBoolean7, paramInt1, paramInt2);
    try {
      ILocationManager iLocationManager = this.mService;
      String str1 = this.mContext.getOpPackageName();
      Context context = this.mContext;
      String str2 = context.getAttributionTag();
      iLocationManager.addTestProvider(paramString, providerProperties, str1, str2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void removeTestProvider(String paramString) {
    boolean bool;
    if (paramString != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null provider");
    try {
      ILocationManager iLocationManager = this.mService;
      String str1 = this.mContext.getOpPackageName();
      Context context = this.mContext;
      String str2 = context.getAttributionTag();
      iLocationManager.removeTestProvider(paramString, str1, str2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setTestProviderLocation(String paramString, Location paramLocation) {
    boolean bool2, bool1 = true;
    if (paramString != null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid null provider");
    if (paramLocation != null) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid null location");
    if (Compatibility.isChangeEnabled(148964793L)) {
      Preconditions.checkArgument(paramLocation.isComplete(), "incomplete location object, missing timestamp or accuracy?");
    } else {
      paramLocation.makeComplete();
    } 
    try {
      ILocationManager iLocationManager = this.mService;
      String str1 = this.mContext.getOpPackageName();
      Context context = this.mContext;
      String str2 = context.getAttributionTag();
      iLocationManager.setTestProviderLocation(paramString, paramLocation, str1, str2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void clearTestProviderLocation(String paramString) {}
  
  public void setTestProviderEnabled(String paramString, boolean paramBoolean) {
    boolean bool;
    if (paramString != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null provider");
    try {
      ILocationManager iLocationManager = this.mService;
      String str1 = this.mContext.getOpPackageName();
      Context context = this.mContext;
      String str2 = context.getAttributionTag();
      iLocationManager.setTestProviderEnabled(paramString, paramBoolean, str1, str2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void clearTestProviderEnabled(String paramString) {
    setTestProviderEnabled(paramString, false);
  }
  
  @Deprecated
  public void setTestProviderStatus(String paramString, int paramInt, Bundle paramBundle, long paramLong) {}
  
  @Deprecated
  public void clearTestProviderStatus(String paramString) {}
  
  public List<LocationRequest> getTestProviderCurrentRequests(String paramString) {
    boolean bool;
    if (paramString != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null provider");
    try {
      return this.mService.getTestProviderCurrentRequests(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void addProximityAlert(double paramDouble1, double paramDouble2, float paramFloat, long paramLong, PendingIntent paramPendingIntent) {
    boolean bool;
    SeempLog.record(45);
    if (paramPendingIntent != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null pending intent");
    if (Compatibility.isChangeEnabled(148963590L))
      Preconditions.checkArgument(paramPendingIntent.isTargetedToPackage(), "pending intent must be targeted to a package"); 
    if (paramLong < 0L)
      paramLong = Long.MAX_VALUE; 
    Geofence geofence = Geofence.createCircle(paramDouble1, paramDouble2, paramFloat);
    LocationRequest locationRequest = (new LocationRequest()).setExpireIn(paramLong);
    try {
      ILocationManager iLocationManager = this.mService;
      String str1 = this.mContext.getPackageName();
      Context context = this.mContext;
      String str2 = context.getAttributionTag();
      iLocationManager.requestGeofence(locationRequest, geofence, paramPendingIntent, str1, str2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void removeProximityAlert(PendingIntent paramPendingIntent) {
    boolean bool;
    if (paramPendingIntent != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null pending intent");
    if (Compatibility.isChangeEnabled(148963590L))
      Preconditions.checkArgument(paramPendingIntent.isTargetedToPackage(), "pending intent must be targeted to a package"); 
    try {
      this.mService.removeGeofence(null, paramPendingIntent, this.mContext.getPackageName());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void addGeofence(LocationRequest paramLocationRequest, Geofence paramGeofence, PendingIntent paramPendingIntent) {
    boolean bool2, bool1 = true;
    if (paramLocationRequest != null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid null location request");
    if (paramGeofence != null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid null geofence");
    if (paramPendingIntent != null) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid null pending intent");
    if (Compatibility.isChangeEnabled(148963590L))
      Preconditions.checkArgument(paramPendingIntent.isTargetedToPackage(), "pending intent must be targeted to a package"); 
    try {
      ILocationManager iLocationManager = this.mService;
      String str1 = this.mContext.getPackageName();
      Context context = this.mContext;
      String str2 = context.getAttributionTag();
      iLocationManager.requestGeofence(paramLocationRequest, paramGeofence, paramPendingIntent, str1, str2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void removeGeofence(Geofence paramGeofence, PendingIntent paramPendingIntent) {
    boolean bool2, bool1 = true;
    if (paramGeofence != null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid null geofence");
    if (paramPendingIntent != null) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid null pending intent");
    if (Compatibility.isChangeEnabled(148963590L))
      Preconditions.checkArgument(paramPendingIntent.isTargetedToPackage(), "pending intent must be targeted to a package"); 
    try {
      this.mService.removeGeofence(paramGeofence, paramPendingIntent, this.mContext.getPackageName());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void removeAllGeofences(PendingIntent paramPendingIntent) {
    boolean bool;
    if (paramPendingIntent != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null pending intent");
    if (Compatibility.isChangeEnabled(148963590L))
      Preconditions.checkArgument(paramPendingIntent.isTargetedToPackage(), "pending intent must be targeted to a package"); 
    try {
      this.mService.removeGeofence(null, paramPendingIntent, this.mContext.getPackageName());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public GnssCapabilities getGnssCapabilities() {
    try {
      long l1 = this.mService.getGnssCapabilities();
      long l2 = l1;
      if (l1 == -1L)
        l2 = 0L; 
      return GnssCapabilities.of(l2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getGnssYearOfHardware() {
    try {
      return this.mService.getGnssYearOfHardware();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String getGnssHardwareModelName() {
    try {
      return this.mService.getGnssHardwareModelName();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public GpsStatus getGpsStatus(GpsStatus paramGpsStatus) {
    if (!Compatibility.isChangeEnabled(144027538L)) {
      GpsStatus gpsStatus;
      GnssStatus gnssStatus = this.mGnssStatusListenerManager.getGnssStatus();
      int i = this.mGnssStatusListenerManager.getTtff();
      if (gnssStatus != null) {
        if (paramGpsStatus == null) {
          gpsStatus = GpsStatus.create(gnssStatus, i);
        } else {
          paramGpsStatus.setStatus((GnssStatus)gpsStatus, i);
          gpsStatus = paramGpsStatus;
        } 
      } else {
        gpsStatus = paramGpsStatus;
        if (paramGpsStatus == null)
          gpsStatus = GpsStatus.createEmpty(); 
      } 
      return gpsStatus;
    } 
    throw new UnsupportedOperationException("GpsStatus APIs not supported, please use GnssStatus APIs instead");
  }
  
  @Deprecated
  public boolean addGpsStatusListener(GpsStatus.Listener paramListener) {
    SeempLog.record(43);
    if (!Compatibility.isChangeEnabled(144027538L))
      try {
        GnssStatusListenerManager gnssStatusListenerManager = this.mGnssStatusListenerManager;
        HandlerExecutor handlerExecutor = new HandlerExecutor();
        Handler handler = new Handler();
        this();
        this(handler);
        return gnssStatusListenerManager.addListener(paramListener, handlerExecutor);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    throw new UnsupportedOperationException("GpsStatus APIs not supported, please use GnssStatus APIs instead");
  }
  
  @Deprecated
  public void removeGpsStatusListener(GpsStatus.Listener paramListener) {
    if (!Compatibility.isChangeEnabled(144027538L))
      try {
        this.mGnssStatusListenerManager.removeListener(paramListener);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    throw new UnsupportedOperationException("GpsStatus APIs not supported, please use GnssStatus APIs instead");
  }
  
  @Deprecated
  public boolean registerGnssStatusCallback(GnssStatus.Callback paramCallback) {
    return registerGnssStatusCallback(paramCallback, (Handler)null);
  }
  
  public boolean registerGnssStatusCallback(GnssStatus.Callback paramCallback, Handler paramHandler) {
    Handler handler = paramHandler;
    if (paramHandler == null)
      handler = new Handler(); 
    try {
      return this.mGnssStatusListenerManager.addListener(paramCallback, handler);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean registerGnssStatusCallback(Executor paramExecutor, GnssStatus.Callback paramCallback) {
    try {
      return this.mGnssStatusListenerManager.addListener(paramCallback, paramExecutor);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void unregisterGnssStatusCallback(GnssStatus.Callback paramCallback) {
    try {
      this.mGnssStatusListenerManager.removeListener(paramCallback);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public boolean addNmeaListener(GpsStatus.NmeaListener paramNmeaListener) {
    SeempLog.record(44);
    return false;
  }
  
  @Deprecated
  public void removeNmeaListener(GpsStatus.NmeaListener paramNmeaListener) {}
  
  @Deprecated
  public boolean addNmeaListener(OnNmeaMessageListener paramOnNmeaMessageListener) {
    return addNmeaListener(paramOnNmeaMessageListener, (Handler)null);
  }
  
  public boolean addNmeaListener(OnNmeaMessageListener paramOnNmeaMessageListener, Handler paramHandler) {
    Handler handler = paramHandler;
    if (paramHandler == null)
      handler = new Handler(); 
    try {
      return this.mGnssStatusListenerManager.addListener(paramOnNmeaMessageListener, handler);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean addNmeaListener(Executor paramExecutor, OnNmeaMessageListener paramOnNmeaMessageListener) {
    try {
      return this.mGnssStatusListenerManager.addListener(paramOnNmeaMessageListener, paramExecutor);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void removeNmeaListener(OnNmeaMessageListener paramOnNmeaMessageListener) {
    try {
      this.mGnssStatusListenerManager.removeListener(paramOnNmeaMessageListener);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  @Deprecated
  public boolean addGpsMeasurementListener(GpsMeasurementsEvent.Listener paramListener) {
    return false;
  }
  
  @SystemApi
  @Deprecated
  public void removeGpsMeasurementListener(GpsMeasurementsEvent.Listener paramListener) {}
  
  @Deprecated
  public boolean registerGnssMeasurementsCallback(GnssMeasurementsEvent.Callback paramCallback) {
    return registerGnssMeasurementsCallback((Executor)_$$Lambda$_14QHG018Z6p13d3hzJuGTWnNeo.INSTANCE, paramCallback);
  }
  
  public boolean registerGnssMeasurementsCallback(GnssMeasurementsEvent.Callback paramCallback, Handler paramHandler) {
    Handler handler = paramHandler;
    if (paramHandler == null)
      handler = new Handler(); 
    try {
      return this.mGnssMeasurementsListenerManager.addListener(paramCallback, handler);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean registerGnssMeasurementsCallback(Executor paramExecutor, GnssMeasurementsEvent.Callback paramCallback) {
    try {
      return this.mGnssMeasurementsListenerManager.addListener(paramCallback, paramExecutor);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean registerGnssMeasurementsCallback(GnssRequest paramGnssRequest, Executor paramExecutor, GnssMeasurementsEvent.Callback paramCallback) {
    boolean bool;
    if (paramGnssRequest != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null request");
    try {
      return this.mGnssMeasurementsListenerManager.addListener(paramGnssRequest, paramCallback, paramExecutor);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void injectGnssMeasurementCorrections(GnssMeasurementCorrections paramGnssMeasurementCorrections) {
    boolean bool;
    if (paramGnssMeasurementCorrections != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool);
    try {
      ILocationManager iLocationManager = this.mService;
      Context context = this.mContext;
      String str = context.getPackageName();
      iLocationManager.injectGnssMeasurementCorrections(paramGnssMeasurementCorrections, str);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void unregisterGnssMeasurementsCallback(GnssMeasurementsEvent.Callback paramCallback) {
    try {
      this.mGnssMeasurementsListenerManager.removeListener(paramCallback);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean registerAntennaInfoListener(Executor paramExecutor, GnssAntennaInfo.Listener paramListener) {
    try {
      return this.mGnssAntennaInfoListenerManager.addListener(paramListener, paramExecutor);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void unregisterAntennaInfoListener(GnssAntennaInfo.Listener paramListener) {
    try {
      this.mGnssAntennaInfoListenerManager.removeListener(paramListener);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  @Deprecated
  public boolean addGpsNavigationMessageListener(GpsNavigationMessageEvent.Listener paramListener) {
    return false;
  }
  
  @SystemApi
  @Deprecated
  public void removeGpsNavigationMessageListener(GpsNavigationMessageEvent.Listener paramListener) {}
  
  @Deprecated
  public boolean registerGnssNavigationMessageCallback(GnssNavigationMessage.Callback paramCallback) {
    return registerGnssNavigationMessageCallback((Executor)_$$Lambda$_14QHG018Z6p13d3hzJuGTWnNeo.INSTANCE, paramCallback);
  }
  
  public boolean registerGnssNavigationMessageCallback(GnssNavigationMessage.Callback paramCallback, Handler paramHandler) {
    Handler handler = paramHandler;
    if (paramHandler == null)
      handler = new Handler(); 
    try {
      return this.mGnssNavigationMessageListenerTransport.addListener(paramCallback, handler);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean registerGnssNavigationMessageCallback(Executor paramExecutor, GnssNavigationMessage.Callback paramCallback) {
    try {
      return this.mGnssNavigationMessageListenerTransport.addListener(paramCallback, paramExecutor);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void unregisterGnssNavigationMessageCallback(GnssNavigationMessage.Callback paramCallback) {
    try {
      this.mGnssNavigationMessageListenerTransport.removeListener(paramCallback);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public int getGnssBatchSize() {
    try {
      return this.mService.getGnssBatchSize(this.mContext.getPackageName());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean registerGnssBatchedLocationCallback(long paramLong, boolean paramBoolean, BatchedLocationCallback paramBatchedLocationCallback, Handler paramHandler) {
    Handler handler = paramHandler;
    if (paramHandler == null)
      handler = new Handler(); 
    BatchedLocationCallbackManager batchedLocationCallbackManager = this.mBatchedLocationCallbackManager;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/location/LocationManager}.Landroid/location/LocationManager$BatchedLocationCallbackManager;}, name=null} */
    try {
      if (this.mBatchedLocationCallbackManager.addListener(paramBatchedLocationCallback, handler)) {
        ILocationManager iLocationManager = this.mService;
        Context context = this.mContext;
        String str2 = context.getPackageName(), str1 = this.mContext.getAttributionTag();
        paramBoolean = iLocationManager.startGnssBatch(paramLong, paramBoolean, str2, str1);
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/location/LocationManager}.Landroid/location/LocationManager$BatchedLocationCallbackManager;}, name=null} */
        return paramBoolean;
      } 
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/location/LocationManager}.Landroid/location/LocationManager$BatchedLocationCallbackManager;}, name=null} */
      return false;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/location/LocationManager}.Landroid/location/LocationManager$BatchedLocationCallbackManager;}, name=null} */
    throw paramBatchedLocationCallback;
  }
  
  @SystemApi
  public void flushGnssBatch() {
    try {
      this.mService.flushGnssBatch(this.mContext.getPackageName());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean unregisterGnssBatchedLocationCallback(BatchedLocationCallback paramBatchedLocationCallback) {
    BatchedLocationCallbackManager batchedLocationCallbackManager = this.mBatchedLocationCallbackManager;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/location/LocationManager}.Landroid/location/LocationManager$BatchedLocationCallbackManager;}, name=null} */
    try {
      this.mBatchedLocationCallbackManager.removeListener(paramBatchedLocationCallback);
      this.mService.stopGnssBatch();
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/location/LocationManager}.Landroid/location/LocationManager$BatchedLocationCallbackManager;}, name=null} */
      return true;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/location/LocationManager}.Landroid/location/LocationManager$BatchedLocationCallbackManager;}, name=null} */
    throw paramBatchedLocationCallback;
  }
  
  class GetCurrentLocationTransport extends ILocationListener.Stub implements AlarmManager.OnAlarmListener {
    private AlarmManager mAlarmManager;
    
    private Consumer<Location> mConsumer;
    
    private Executor mExecutor;
    
    private ICancellationSignal mRemoteCancellationSignal;
    
    private GetCurrentLocationTransport(LocationManager this$0, Consumer<Location> param1Consumer) {
      boolean bool2, bool1 = true;
      if (this$0 != null) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      Preconditions.checkArgument(bool2, "illegal null executor");
      if (param1Consumer != null) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      Preconditions.checkArgument(bool2, "illegal null consumer");
      this.mExecutor = (Executor)this$0;
      this.mConsumer = param1Consumer;
      this.mAlarmManager = null;
      this.mRemoteCancellationSignal = null;
    }
    
    public String getListenerId() {
      return AppOpsManager.toReceiverId(this.mConsumer);
    }
    
    public void register(AlarmManager param1AlarmManager, ICancellationSignal param1ICancellationSignal) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mConsumer : Ljava/util/function/Consumer;
      //   6: astore_3
      //   7: aload_3
      //   8: ifnonnull -> 14
      //   11: aload_0
      //   12: monitorexit
      //   13: return
      //   14: aload_0
      //   15: aload_1
      //   16: putfield mAlarmManager : Landroid/app/AlarmManager;
      //   19: invokestatic elapsedRealtime : ()J
      //   22: lstore #4
      //   24: aload_1
      //   25: iconst_3
      //   26: ldc2_w 30000
      //   29: lload #4
      //   31: ladd
      //   32: ldc 'GetCurrentLocation'
      //   34: aload_0
      //   35: aconst_null
      //   36: invokevirtual set : (IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V
      //   39: aload_0
      //   40: aload_2
      //   41: putfield mRemoteCancellationSignal : Landroid/os/ICancellationSignal;
      //   44: aload_0
      //   45: monitorexit
      //   46: return
      //   47: astore_1
      //   48: aload_0
      //   49: monitorexit
      //   50: aload_1
      //   51: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2609	-> 2
      //   #2610	-> 11
      //   #2613	-> 14
      //   #2614	-> 19
      //   #2616	-> 19
      //   #2614	-> 24
      //   #2621	-> 39
      //   #2622	-> 44
      //   #2608	-> 47
      // Exception table:
      //   from	to	target	type
      //   2	7	47	finally
      //   14	19	47	finally
      //   19	24	47	finally
      //   24	39	47	finally
      //   39	44	47	finally
    }
    
    public void cancel() {
      remove();
    }
    
    private Consumer<Location> remove() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: aconst_null
      //   4: putfield mExecutor : Ljava/util/concurrent/Executor;
      //   7: aload_0
      //   8: getfield mConsumer : Ljava/util/function/Consumer;
      //   11: astore_1
      //   12: aload_0
      //   13: aconst_null
      //   14: putfield mConsumer : Ljava/util/function/Consumer;
      //   17: aload_0
      //   18: getfield mAlarmManager : Landroid/app/AlarmManager;
      //   21: ifnull -> 37
      //   24: aload_0
      //   25: getfield mAlarmManager : Landroid/app/AlarmManager;
      //   28: aload_0
      //   29: invokevirtual cancel : (Landroid/app/AlarmManager$OnAlarmListener;)V
      //   32: aload_0
      //   33: aconst_null
      //   34: putfield mAlarmManager : Landroid/app/AlarmManager;
      //   37: aload_0
      //   38: getfield mRemoteCancellationSignal : Landroid/os/ICancellationSignal;
      //   41: astore_2
      //   42: aload_0
      //   43: aconst_null
      //   44: putfield mRemoteCancellationSignal : Landroid/os/ICancellationSignal;
      //   47: aload_0
      //   48: monitorexit
      //   49: aload_2
      //   50: ifnull -> 63
      //   53: aload_2
      //   54: invokeinterface cancel : ()V
      //   59: goto -> 63
      //   62: astore_2
      //   63: aload_1
      //   64: areturn
      //   65: astore_1
      //   66: aload_0
      //   67: monitorexit
      //   68: aload_1
      //   69: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2631	-> 0
      //   #2632	-> 2
      //   #2633	-> 7
      //   #2634	-> 12
      //   #2636	-> 17
      //   #2637	-> 24
      //   #2638	-> 32
      //   #2642	-> 37
      //   #2643	-> 42
      //   #2644	-> 47
      //   #2646	-> 49
      //   #2648	-> 53
      //   #2651	-> 59
      //   #2649	-> 62
      //   #2654	-> 63
      //   #2644	-> 65
      // Exception table:
      //   from	to	target	type
      //   2	7	65	finally
      //   7	12	65	finally
      //   12	17	65	finally
      //   17	24	65	finally
      //   24	32	65	finally
      //   32	37	65	finally
      //   37	42	65	finally
      //   42	47	65	finally
      //   47	49	65	finally
      //   53	59	62	android/os/RemoteException
      //   66	68	65	finally
    }
    
    public void fail() {
      deliverResult(null);
    }
    
    public void onAlarm() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: aconst_null
      //   4: putfield mAlarmManager : Landroid/app/AlarmManager;
      //   7: aload_0
      //   8: monitorexit
      //   9: aload_0
      //   10: aconst_null
      //   11: invokespecial deliverResult : (Landroid/location/Location;)V
      //   14: return
      //   15: astore_1
      //   16: aload_0
      //   17: monitorexit
      //   18: aload_1
      //   19: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2663	-> 0
      //   #2665	-> 2
      //   #2666	-> 7
      //   #2668	-> 9
      //   #2669	-> 14
      //   #2666	-> 15
      // Exception table:
      //   from	to	target	type
      //   2	7	15	finally
      //   7	9	15	finally
      //   16	18	15	finally
    }
    
    public void onLocationChanged(Location param1Location) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: aconst_null
      //   4: putfield mRemoteCancellationSignal : Landroid/os/ICancellationSignal;
      //   7: aload_0
      //   8: monitorexit
      //   9: aload_0
      //   10: aload_1
      //   11: invokespecial deliverResult : (Landroid/location/Location;)V
      //   14: return
      //   15: astore_1
      //   16: aload_0
      //   17: monitorexit
      //   18: aload_1
      //   19: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2673	-> 0
      //   #2675	-> 2
      //   #2676	-> 7
      //   #2678	-> 9
      //   #2679	-> 14
      //   #2676	-> 15
      // Exception table:
      //   from	to	target	type
      //   2	7	15	finally
      //   7	9	15	finally
      //   16	18	15	finally
    }
    
    public void onProviderEnabled(String param1String) {}
    
    public void onProviderDisabled(String param1String) {
      deliverResult(null);
    }
    
    public void onRemoved() {
      deliverResult(null);
    }
    
    private void deliverResult(Location param1Location) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mExecutor : Ljava/util/concurrent/Executor;
      //   6: astore_2
      //   7: aload_2
      //   8: ifnonnull -> 14
      //   11: aload_0
      //   12: monitorexit
      //   13: return
      //   14: getstatic android/location/_$$Lambda$LocationManager$GetCurrentLocationTransport$TwoLg_IkGQIkPn_gbFfT0g9K_Ts.INSTANCE : Landroid/location/-$$Lambda$LocationManager$GetCurrentLocationTransport$TwoLg_IkGQIkPn-gbFfT0g9K-Ts;
      //   17: astore_2
      //   18: aload_2
      //   19: aload_0
      //   20: aload_1
      //   21: invokestatic obtainRunnable : (Ljava/util/function/BiConsumer;Ljava/lang/Object;Ljava/lang/Object;)Lcom/android/internal/util/function/pooled/PooledRunnable;
      //   24: astore_1
      //   25: aload_1
      //   26: invokeinterface recycleOnUse : ()Lcom/android/internal/util/function/pooled/PooledRunnable;
      //   31: astore_2
      //   32: aload_0
      //   33: getfield mExecutor : Ljava/util/concurrent/Executor;
      //   36: aload_2
      //   37: invokeinterface execute : (Ljava/lang/Runnable;)V
      //   42: aload_0
      //   43: monitorexit
      //   44: return
      //   45: astore_1
      //   46: aload_2
      //   47: invokeinterface recycle : ()V
      //   52: aload_1
      //   53: athrow
      //   54: astore_1
      //   55: aload_0
      //   56: monitorexit
      //   57: aload_1
      //   58: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2697	-> 2
      //   #2698	-> 11
      //   #2701	-> 14
      //   #2702	-> 18
      //   #2703	-> 25
      //   #2705	-> 32
      //   #2709	-> 42
      //   #2710	-> 42
      //   #2706	-> 45
      //   #2707	-> 46
      //   #2708	-> 52
      //   #2696	-> 54
      // Exception table:
      //   from	to	target	type
      //   2	7	54	finally
      //   14	18	54	finally
      //   18	25	54	finally
      //   25	32	54	finally
      //   32	42	45	java/util/concurrent/RejectedExecutionException
      //   32	42	54	finally
      //   46	52	54	finally
      //   52	54	54	finally
    }
    
    private void acceptResult(Location param1Location) {
      Consumer<Location> consumer = remove();
      if (consumer != null)
        consumer.accept(param1Location); 
    }
  }
  
  class LocationListenerTransport extends ILocationListener.Stub {
    private volatile Executor mExecutor;
    
    private final LocationListener mListener;
    
    final LocationManager this$0;
    
    private LocationListenerTransport(LocationListener param1LocationListener) {
      boolean bool;
      this.mExecutor = null;
      if (param1LocationListener != null) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkArgument(bool, "invalid null listener");
      this.mListener = param1LocationListener;
    }
    
    public LocationListener getKey() {
      return this.mListener;
    }
    
    public String getListenerId() {
      return AppOpsManager.toReceiverId(this.mListener);
    }
    
    public void register(Executor param1Executor) {
      boolean bool;
      if (param1Executor != null) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkArgument(bool, "invalid null executor");
      this.mExecutor = param1Executor;
    }
    
    public void unregister() {
      this.mExecutor = null;
    }
    
    public void onLocationChanged(Location param1Location) {
      Executor executor = this.mExecutor;
      if (executor == null)
        return; 
      -$.Lambda.LocationManager.LocationListenerTransport.enkW18B0WwpQkSIMmVChmQ2YwC8 enkW18B0WwpQkSIMmVChmQ2YwC8 = _$$Lambda$LocationManager$LocationListenerTransport$enkW18B0WwpQkSIMmVChmQ2YwC8.INSTANCE;
      PooledRunnable pooledRunnable = PooledLambda.obtainRunnable((TriConsumer)enkW18B0WwpQkSIMmVChmQ2YwC8, this, executor, param1Location);
      pooledRunnable = pooledRunnable.recycleOnUse();
      try {
        executor.execute((Runnable)pooledRunnable);
        return;
      } catch (RejectedExecutionException rejectedExecutionException) {
        pooledRunnable.recycle();
        locationCallbackFinished();
        throw rejectedExecutionException;
      } 
    }
    
    private void acceptLocation(Executor param1Executor, Location param1Location) {
      try {
        Executor executor = this.mExecutor;
        if (param1Executor != executor)
          return; 
        long l = Binder.clearCallingIdentity();
      } finally {
        locationCallbackFinished();
      } 
    }
    
    public void onProviderEnabled(String param1String) {
      Executor executor = this.mExecutor;
      if (executor == null)
        return; 
      -$.Lambda.LocationManager.LocationListenerTransport.C3xaM63A8GAwfJNN4R634OLsvDc c3xaM63A8GAwfJNN4R634OLsvDc = _$$Lambda$LocationManager$LocationListenerTransport$C3xaM63A8GAwfJNN4R634OLsvDc.INSTANCE;
      PooledRunnable pooledRunnable = PooledLambda.obtainRunnable((QuadConsumer)c3xaM63A8GAwfJNN4R634OLsvDc, this, executor, param1String, Boolean.valueOf(true));
      pooledRunnable = pooledRunnable.recycleOnUse();
      try {
        executor.execute((Runnable)pooledRunnable);
        return;
      } catch (RejectedExecutionException rejectedExecutionException) {
        pooledRunnable.recycle();
        locationCallbackFinished();
        throw rejectedExecutionException;
      } 
    }
    
    public void onProviderDisabled(String param1String) {
      Executor executor = this.mExecutor;
      if (executor == null)
        return; 
      -$.Lambda.LocationManager.LocationListenerTransport.C3xaM63A8GAwfJNN4R634OLsvDc c3xaM63A8GAwfJNN4R634OLsvDc = _$$Lambda$LocationManager$LocationListenerTransport$C3xaM63A8GAwfJNN4R634OLsvDc.INSTANCE;
      PooledRunnable pooledRunnable = PooledLambda.obtainRunnable((QuadConsumer)c3xaM63A8GAwfJNN4R634OLsvDc, this, executor, param1String, Boolean.valueOf(false));
      pooledRunnable = pooledRunnable.recycleOnUse();
      try {
        executor.execute((Runnable)pooledRunnable);
        return;
      } catch (RejectedExecutionException rejectedExecutionException) {
        pooledRunnable.recycle();
        locationCallbackFinished();
        throw rejectedExecutionException;
      } 
    }
    
    private void acceptProviderChange(Executor param1Executor, String param1String, boolean param1Boolean) {
      try {
        Executor executor = this.mExecutor;
        if (param1Executor != executor)
          return; 
        long l = Binder.clearCallingIdentity();
        if (param1Boolean)
          try {
            this.mListener.onProviderEnabled(param1String);
            return;
          } finally {
            Binder.restoreCallingIdentity(l);
          }  
        this.mListener.onProviderDisabled(param1String);
        Binder.restoreCallingIdentity(l);
        return;
      } finally {
        locationCallbackFinished();
      } 
    }
    
    public void onRemoved() {
      Executor executor = this.mExecutor;
      if (executor == null)
        return; 
      executor.execute(new _$$Lambda$LocationManager$LocationListenerTransport$fHjQXipQePznoEyxLuCfUO_YP1Y(this, executor));
    }
    
    private void locationCallbackFinished() {
      try {
        LocationManager.this.mService.locationCallbackFinished(this);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
  }
  
  class NmeaAdapter extends GnssStatus.Callback implements OnNmeaMessageListener {
    private final OnNmeaMessageListener mListener;
    
    private NmeaAdapter(LocationManager this$0) {
      this.mListener = (OnNmeaMessageListener)this$0;
    }
    
    public void onNmeaMessage(String param1String, long param1Long) {
      this.mListener.onNmeaMessage(param1String, param1Long);
    }
  }
  
  class GnssStatusListenerManager extends AbstractListenerManager<Void, GnssStatus.Callback> {
    private volatile GnssStatus mGnssStatus;
    
    private IGnssStatusListener mListenerTransport;
    
    private volatile int mTtff;
    
    final LocationManager this$0;
    
    private GnssStatusListenerManager() {}
    
    public GnssStatus getGnssStatus() {
      return this.mGnssStatus;
    }
    
    public int getTtff() {
      return this.mTtff;
    }
    
    public boolean addListener(GpsStatus.Listener param1Listener, Executor param1Executor) throws RemoteException {
      return addInternal((Void)null, param1Listener, param1Executor);
    }
    
    public boolean addListener(OnNmeaMessageListener param1OnNmeaMessageListener, Handler param1Handler) throws RemoteException {
      return addInternal((Void)null, param1OnNmeaMessageListener, param1Handler);
    }
    
    public boolean addListener(OnNmeaMessageListener param1OnNmeaMessageListener, Executor param1Executor) throws RemoteException {
      return addInternal((Void)null, param1OnNmeaMessageListener, param1Executor);
    }
    
    protected GnssStatus.Callback convertKey(final Object listener) {
      if (listener instanceof GnssStatus.Callback)
        return (GnssStatus.Callback)listener; 
      if (listener instanceof GpsStatus.Listener)
        return new GnssStatus.Callback() {
            private final GpsStatus.Listener mGpsListener = (GpsStatus.Listener)listener;
            
            final LocationManager.GnssStatusListenerManager this$1;
            
            final Object val$listener;
            
            public void onStarted() {
              this.mGpsListener.onGpsStatusChanged(1);
            }
            
            public void onStopped() {
              this.mGpsListener.onGpsStatusChanged(2);
            }
            
            public void onFirstFix(int param2Int) {
              this.mGpsListener.onGpsStatusChanged(3);
            }
            
            public void onSatelliteStatusChanged(GnssStatus param2GnssStatus) {
              this.mGpsListener.onGpsStatusChanged(4);
            }
          }; 
      if (listener instanceof OnNmeaMessageListener)
        return new LocationManager.NmeaAdapter(); 
      throw new IllegalStateException();
    }
    
    protected boolean registerService(Void param1Void) throws RemoteException {
      boolean bool;
      if (this.mListenerTransport == null) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkState(bool);
      GnssStatusListener gnssStatusListener = new GnssStatusListener();
      ILocationManager iLocationManager = LocationManager.this.mService;
      String str1 = LocationManager.this.mContext.getPackageName();
      LocationManager locationManager = LocationManager.this;
      String str2 = locationManager.mContext.getAttributionTag();
      if (iLocationManager.registerGnssStatusCallback(gnssStatusListener, str1, str2)) {
        this.mListenerTransport = gnssStatusListener;
        return true;
      } 
      return false;
    }
    
    protected void unregisterService() throws RemoteException {
      if (this.mListenerTransport != null) {
        LocationManager.this.mService.unregisterGnssStatusCallback(this.mListenerTransport);
        this.mListenerTransport = null;
      } 
    }
    
    class GnssStatusListener extends IGnssStatusListener.Stub {
      final LocationManager.GnssStatusListenerManager this$1;
      
      private GnssStatusListener() {}
      
      public void onGnssStarted() {
        LocationManager.GnssStatusListenerManager.this.execute((Consumer<GnssStatus.Callback>)_$$Lambda$_z_Hjl12STdAybauR3BT_ftvWd0.INSTANCE);
      }
      
      public void onGnssStopped() {
        LocationManager.GnssStatusListenerManager.this.execute((Consumer<GnssStatus.Callback>)_$$Lambda$UmbtQF279SH5h72Ftfcj_s96jsY.INSTANCE);
      }
      
      public void onFirstFix(int param2Int) {
        LocationManager.GnssStatusListenerManager.access$1202(LocationManager.GnssStatusListenerManager.this, param2Int);
        LocationManager.GnssStatusListenerManager.this.execute(new _$$Lambda$LocationManager$GnssStatusListenerManager$GnssStatusListener$7Fi5XkeF81eL_OKPS2GJMvyc3_8(param2Int));
      }
      
      public void onSvStatusChanged(int param2Int, int[] param2ArrayOfint, float[] param2ArrayOffloat1, float[] param2ArrayOffloat2, float[] param2ArrayOffloat3, float[] param2ArrayOffloat4, float[] param2ArrayOffloat5) {
        GnssStatus gnssStatus = GnssStatus.wrap(param2Int, param2ArrayOfint, param2ArrayOffloat1, param2ArrayOffloat2, param2ArrayOffloat3, param2ArrayOffloat4, param2ArrayOffloat5);
        LocationManager.GnssStatusListenerManager.access$1302(LocationManager.GnssStatusListenerManager.this, gnssStatus);
        LocationManager.GnssStatusListenerManager.this.execute(new _$$Lambda$LocationManager$GnssStatusListenerManager$GnssStatusListener$4EPi22o4xuVnpNhFHnDvebH4TG8(gnssStatus));
      }
      
      public void onNmeaReceived(long param2Long, String param2String) {
        LocationManager.GnssStatusListenerManager.this.execute(new _$$Lambda$LocationManager$GnssStatusListenerManager$GnssStatusListener$gYcH61KCtV_OcJJszI1TfvnrJHY(param2String, param2Long));
      }
    }
  }
  
  class GnssMeasurementsListenerManager extends AbstractListenerManager<GnssRequest, GnssMeasurementsEvent.Callback> {
    private IGnssMeasurementsListener mListenerTransport;
    
    final LocationManager this$0;
    
    private GnssMeasurementsListenerManager() {}
    
    protected boolean registerService(GnssRequest param1GnssRequest) throws RemoteException {
      boolean bool;
      if (this.mListenerTransport == null) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkState(bool);
      GnssMeasurementsListener gnssMeasurementsListener = new GnssMeasurementsListener();
      ILocationManager iLocationManager = LocationManager.this.mService;
      String str1 = LocationManager.this.mContext.getPackageName();
      LocationManager locationManager = LocationManager.this;
      String str2 = locationManager.mContext.getAttributionTag();
      if (iLocationManager.addGnssMeasurementsListener(param1GnssRequest, gnssMeasurementsListener, str1, str2)) {
        this.mListenerTransport = gnssMeasurementsListener;
        return true;
      } 
      return false;
    }
    
    protected void unregisterService() throws RemoteException {
      if (this.mListenerTransport != null) {
        LocationManager.this.mService.removeGnssMeasurementsListener(this.mListenerTransport);
        this.mListenerTransport = null;
      } 
    }
    
    protected GnssRequest merge(List<GnssRequest> param1List) {
      Preconditions.checkArgument(param1List.isEmpty() ^ true);
      for (GnssRequest gnssRequest : param1List) {
        if (gnssRequest.isFullTracking())
          return gnssRequest; 
      } 
      return param1List.get(0);
    }
    
    class GnssMeasurementsListener extends IGnssMeasurementsListener.Stub {
      final LocationManager.GnssMeasurementsListenerManager this$1;
      
      private GnssMeasurementsListener() {}
      
      public void onGnssMeasurementsReceived(GnssMeasurementsEvent param2GnssMeasurementsEvent) {
        LocationManager.GnssMeasurementsListenerManager.this.execute(new _$$Lambda$LocationManager$GnssMeasurementsListenerManager$GnssMeasurementsListener$KpnZ7QK_0TdYNaRuvj6jEAwntwA(param2GnssMeasurementsEvent));
      }
      
      public void onStatusChanged(int param2Int) {
        LocationManager.GnssMeasurementsListenerManager.this.execute(new _$$Lambda$LocationManager$GnssMeasurementsListenerManager$GnssMeasurementsListener$VaDOMlyMw_gbfsmNktA3uK1Vz_o(param2Int));
      }
    }
  }
  
  class GnssNavigationMessageListenerManager extends AbstractListenerManager<Void, GnssNavigationMessage.Callback> {
    private IGnssNavigationMessageListener mListenerTransport;
    
    final LocationManager this$0;
    
    private GnssNavigationMessageListenerManager() {}
    
    protected boolean registerService(Void param1Void) throws RemoteException {
      boolean bool;
      if (this.mListenerTransport == null) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkState(bool);
      GnssNavigationMessageListener gnssNavigationMessageListener = new GnssNavigationMessageListener();
      ILocationManager iLocationManager = LocationManager.this.mService;
      String str1 = LocationManager.this.mContext.getPackageName();
      LocationManager locationManager = LocationManager.this;
      String str2 = locationManager.mContext.getAttributionTag();
      if (iLocationManager.addGnssNavigationMessageListener(gnssNavigationMessageListener, str1, str2)) {
        this.mListenerTransport = gnssNavigationMessageListener;
        return true;
      } 
      return false;
    }
    
    protected void unregisterService() throws RemoteException {
      if (this.mListenerTransport != null) {
        LocationManager.this.mService.removeGnssNavigationMessageListener(this.mListenerTransport);
        this.mListenerTransport = null;
      } 
    }
    
    class GnssNavigationMessageListener extends IGnssNavigationMessageListener.Stub {
      final LocationManager.GnssNavigationMessageListenerManager this$1;
      
      private GnssNavigationMessageListener() {}
      
      public void onGnssNavigationMessageReceived(GnssNavigationMessage param2GnssNavigationMessage) {
        LocationManager.GnssNavigationMessageListenerManager.this.execute(new _$$Lambda$LocationManager$GnssNavigationMessageListenerManager$GnssNavigationMessageListener$eKDrbCr3M4VciXB1DeKK_QBtkPY(param2GnssNavigationMessage));
      }
      
      public void onStatusChanged(int param2Int) {
        LocationManager.GnssNavigationMessageListenerManager.this.execute(new _$$Lambda$LocationManager$GnssNavigationMessageListenerManager$GnssNavigationMessageListener$hPtXaHVfOUh07rZHHklhJOg9b4g(param2Int));
      }
    }
  }
  
  class GnssAntennaInfoListenerManager extends AbstractListenerManager<Void, GnssAntennaInfo.Listener> {
    private IGnssAntennaInfoListener mListenerTransport;
    
    final LocationManager this$0;
    
    private GnssAntennaInfoListenerManager() {}
    
    protected boolean registerService(Void param1Void) throws RemoteException {
      boolean bool;
      if (this.mListenerTransport == null) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkState(bool);
      GnssAntennaInfoListener gnssAntennaInfoListener = new GnssAntennaInfoListener();
      ILocationManager iLocationManager = LocationManager.this.mService;
      String str1 = LocationManager.this.mContext.getPackageName();
      LocationManager locationManager = LocationManager.this;
      String str2 = locationManager.mContext.getAttributionTag();
      if (iLocationManager.addGnssAntennaInfoListener(gnssAntennaInfoListener, str1, str2)) {
        this.mListenerTransport = gnssAntennaInfoListener;
        return true;
      } 
      return false;
    }
    
    protected void unregisterService() throws RemoteException {
      if (this.mListenerTransport != null) {
        LocationManager.this.mService.removeGnssAntennaInfoListener(this.mListenerTransport);
        this.mListenerTransport = null;
      } 
    }
    
    class GnssAntennaInfoListener extends IGnssAntennaInfoListener.Stub {
      final LocationManager.GnssAntennaInfoListenerManager this$1;
      
      private GnssAntennaInfoListener() {}
      
      public void onGnssAntennaInfoReceived(List<GnssAntennaInfo> param2List) {
        LocationManager.GnssAntennaInfoListenerManager.this.execute(new _$$Lambda$LocationManager$GnssAntennaInfoListenerManager$GnssAntennaInfoListener$ZlzYuMVQsKdtVbYm_J172H_NUIc(param2List));
      }
    }
  }
  
  class BatchedLocationCallbackManager extends AbstractListenerManager<Void, BatchedLocationCallback> {
    private IBatchedLocationCallback mListenerTransport;
    
    final LocationManager this$0;
    
    private BatchedLocationCallbackManager() {}
    
    protected boolean registerService(Void param1Void) throws RemoteException {
      boolean bool;
      if (this.mListenerTransport == null) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkState(bool);
      BatchedLocationCallback batchedLocationCallback = new BatchedLocationCallback();
      ILocationManager iLocationManager = LocationManager.this.mService;
      String str1 = LocationManager.this.mContext.getPackageName();
      LocationManager locationManager = LocationManager.this;
      String str2 = locationManager.mContext.getAttributionTag();
      if (iLocationManager.addGnssBatchingCallback(batchedLocationCallback, str1, str2)) {
        this.mListenerTransport = batchedLocationCallback;
        return true;
      } 
      return false;
    }
    
    protected void unregisterService() throws RemoteException {
      if (this.mListenerTransport != null) {
        LocationManager.this.mService.removeGnssBatchingCallback();
        this.mListenerTransport = null;
      } 
    }
    
    class BatchedLocationCallback extends IBatchedLocationCallback.Stub {
      final LocationManager.BatchedLocationCallbackManager this$1;
      
      private BatchedLocationCallback() {}
      
      public void onLocationBatch(List<Location> param2List) {
        LocationManager.BatchedLocationCallbackManager.this.execute(new _$$Lambda$LocationManager$BatchedLocationCallbackManager$BatchedLocationCallback$knqSDnhpqK_1OuAgUyeo9qPYZes(param2List));
      }
    }
  }
  
  public static void invalidateLocalLocationEnabledCaches() {
    PropertyInvalidatedCache.invalidateCache("cache_key.location_enabled");
  }
  
  public void disableLocalLocationEnabledCaches() {
    synchronized (this.mLock) {
      this.mLocationEnabledCache = null;
      return;
    } 
  }
}
