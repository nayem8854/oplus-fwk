package android.location;

import android.os.Parcel;
import android.os.Parcelable;

public final class GnssClock implements Parcelable {
  public GnssClock() {
    initialize();
  }
  
  public void set(GnssClock paramGnssClock) {
    this.mFlags = paramGnssClock.mFlags;
    this.mLeapSecond = paramGnssClock.mLeapSecond;
    this.mTimeNanos = paramGnssClock.mTimeNanos;
    this.mTimeUncertaintyNanos = paramGnssClock.mTimeUncertaintyNanos;
    this.mFullBiasNanos = paramGnssClock.mFullBiasNanos;
    this.mBiasNanos = paramGnssClock.mBiasNanos;
    this.mBiasUncertaintyNanos = paramGnssClock.mBiasUncertaintyNanos;
    this.mDriftNanosPerSecond = paramGnssClock.mDriftNanosPerSecond;
    this.mDriftUncertaintyNanosPerSecond = paramGnssClock.mDriftUncertaintyNanosPerSecond;
    this.mHardwareClockDiscontinuityCount = paramGnssClock.mHardwareClockDiscontinuityCount;
    this.mElapsedRealtimeNanos = paramGnssClock.mElapsedRealtimeNanos;
    this.mElapsedRealtimeUncertaintyNanos = paramGnssClock.mElapsedRealtimeUncertaintyNanos;
    this.mReferenceConstellationTypeForIsb = paramGnssClock.mReferenceConstellationTypeForIsb;
    this.mReferenceCarrierFrequencyHzForIsb = paramGnssClock.mReferenceCarrierFrequencyHzForIsb;
    this.mReferenceCodeTypeForIsb = paramGnssClock.mReferenceCodeTypeForIsb;
  }
  
  public void reset() {
    initialize();
  }
  
  public boolean hasLeapSecond() {
    return isFlagSet(1);
  }
  
  public int getLeapSecond() {
    return this.mLeapSecond;
  }
  
  public void setLeapSecond(int paramInt) {
    setFlag(1);
    this.mLeapSecond = paramInt;
  }
  
  public void resetLeapSecond() {
    resetFlag(1);
    this.mLeapSecond = Integer.MIN_VALUE;
  }
  
  public long getTimeNanos() {
    return this.mTimeNanos;
  }
  
  public void setTimeNanos(long paramLong) {
    this.mTimeNanos = paramLong;
  }
  
  public boolean hasTimeUncertaintyNanos() {
    return isFlagSet(2);
  }
  
  public double getTimeUncertaintyNanos() {
    return this.mTimeUncertaintyNanos;
  }
  
  public void setTimeUncertaintyNanos(double paramDouble) {
    setFlag(2);
    this.mTimeUncertaintyNanos = paramDouble;
  }
  
  public void resetTimeUncertaintyNanos() {
    resetFlag(2);
  }
  
  public boolean hasFullBiasNanos() {
    return isFlagSet(4);
  }
  
  public long getFullBiasNanos() {
    return this.mFullBiasNanos;
  }
  
  public void setFullBiasNanos(long paramLong) {
    setFlag(4);
    this.mFullBiasNanos = paramLong;
  }
  
  public void resetFullBiasNanos() {
    resetFlag(4);
    this.mFullBiasNanos = Long.MIN_VALUE;
  }
  
  public boolean hasBiasNanos() {
    return isFlagSet(8);
  }
  
  public double getBiasNanos() {
    return this.mBiasNanos;
  }
  
  public void setBiasNanos(double paramDouble) {
    setFlag(8);
    this.mBiasNanos = paramDouble;
  }
  
  public void resetBiasNanos() {
    resetFlag(8);
  }
  
  public boolean hasBiasUncertaintyNanos() {
    return isFlagSet(16);
  }
  
  public double getBiasUncertaintyNanos() {
    return this.mBiasUncertaintyNanos;
  }
  
  public void setBiasUncertaintyNanos(double paramDouble) {
    setFlag(16);
    this.mBiasUncertaintyNanos = paramDouble;
  }
  
  public void resetBiasUncertaintyNanos() {
    resetFlag(16);
  }
  
  public boolean hasDriftNanosPerSecond() {
    return isFlagSet(32);
  }
  
  public double getDriftNanosPerSecond() {
    return this.mDriftNanosPerSecond;
  }
  
  public void setDriftNanosPerSecond(double paramDouble) {
    setFlag(32);
    this.mDriftNanosPerSecond = paramDouble;
  }
  
  public void resetDriftNanosPerSecond() {
    resetFlag(32);
  }
  
  public boolean hasDriftUncertaintyNanosPerSecond() {
    return isFlagSet(64);
  }
  
  public double getDriftUncertaintyNanosPerSecond() {
    return this.mDriftUncertaintyNanosPerSecond;
  }
  
  public void setDriftUncertaintyNanosPerSecond(double paramDouble) {
    setFlag(64);
    this.mDriftUncertaintyNanosPerSecond = paramDouble;
  }
  
  public void resetDriftUncertaintyNanosPerSecond() {
    resetFlag(64);
  }
  
  public boolean hasElapsedRealtimeNanos() {
    return isFlagSet(128);
  }
  
  public long getElapsedRealtimeNanos() {
    return this.mElapsedRealtimeNanos;
  }
  
  public void setElapsedRealtimeNanos(long paramLong) {
    setFlag(128);
    this.mElapsedRealtimeNanos = paramLong;
  }
  
  public void resetElapsedRealtimeNanos() {
    resetFlag(128);
    this.mElapsedRealtimeNanos = 0L;
  }
  
  public boolean hasElapsedRealtimeUncertaintyNanos() {
    return isFlagSet(256);
  }
  
  public double getElapsedRealtimeUncertaintyNanos() {
    return this.mElapsedRealtimeUncertaintyNanos;
  }
  
  public void setElapsedRealtimeUncertaintyNanos(double paramDouble) {
    setFlag(256);
    this.mElapsedRealtimeUncertaintyNanos = paramDouble;
  }
  
  public void resetElapsedRealtimeUncertaintyNanos() {
    resetFlag(256);
  }
  
  public boolean hasReferenceConstellationTypeForIsb() {
    return isFlagSet(512);
  }
  
  public int getReferenceConstellationTypeForIsb() {
    return this.mReferenceConstellationTypeForIsb;
  }
  
  public void setReferenceConstellationTypeForIsb(int paramInt) {
    setFlag(512);
    this.mReferenceConstellationTypeForIsb = paramInt;
  }
  
  public void resetReferenceConstellationTypeForIsb() {
    resetFlag(512);
    this.mReferenceConstellationTypeForIsb = 0;
  }
  
  public boolean hasReferenceCarrierFrequencyHzForIsb() {
    return isFlagSet(1024);
  }
  
  public double getReferenceCarrierFrequencyHzForIsb() {
    return this.mReferenceCarrierFrequencyHzForIsb;
  }
  
  public void setReferenceCarrierFrequencyHzForIsb(double paramDouble) {
    setFlag(1024);
    this.mReferenceCarrierFrequencyHzForIsb = paramDouble;
  }
  
  public void resetReferenceCarrierFrequencyHzForIsb() {
    resetFlag(1024);
  }
  
  public boolean hasReferenceCodeTypeForIsb() {
    return isFlagSet(2048);
  }
  
  public String getReferenceCodeTypeForIsb() {
    return this.mReferenceCodeTypeForIsb;
  }
  
  public void setReferenceCodeTypeForIsb(String paramString) {
    setFlag(2048);
    this.mReferenceCodeTypeForIsb = paramString;
  }
  
  public void resetReferenceCodeTypeForIsb() {
    resetFlag(2048);
    this.mReferenceCodeTypeForIsb = "UNKNOWN";
  }
  
  public int getHardwareClockDiscontinuityCount() {
    return this.mHardwareClockDiscontinuityCount;
  }
  
  public void setHardwareClockDiscontinuityCount(int paramInt) {
    this.mHardwareClockDiscontinuityCount = paramInt;
  }
  
  public static final Parcelable.Creator<GnssClock> CREATOR = new Parcelable.Creator<GnssClock>() {
      public GnssClock createFromParcel(Parcel param1Parcel) {
        GnssClock gnssClock = new GnssClock();
        GnssClock.access$002(gnssClock, param1Parcel.readInt());
        GnssClock.access$102(gnssClock, param1Parcel.readInt());
        GnssClock.access$202(gnssClock, param1Parcel.readLong());
        GnssClock.access$302(gnssClock, param1Parcel.readDouble());
        GnssClock.access$402(gnssClock, param1Parcel.readLong());
        GnssClock.access$502(gnssClock, param1Parcel.readDouble());
        GnssClock.access$602(gnssClock, param1Parcel.readDouble());
        GnssClock.access$702(gnssClock, param1Parcel.readDouble());
        GnssClock.access$802(gnssClock, param1Parcel.readDouble());
        GnssClock.access$902(gnssClock, param1Parcel.readInt());
        GnssClock.access$1002(gnssClock, param1Parcel.readLong());
        GnssClock.access$1102(gnssClock, param1Parcel.readDouble());
        GnssClock.access$1202(gnssClock, param1Parcel.readInt());
        GnssClock.access$1302(gnssClock, param1Parcel.readDouble());
        GnssClock.access$1402(gnssClock, param1Parcel.readString());
        return gnssClock;
      }
      
      public GnssClock[] newArray(int param1Int) {
        return new GnssClock[param1Int];
      }
    };
  
  private static final int HAS_BIAS = 8;
  
  private static final int HAS_BIAS_UNCERTAINTY = 16;
  
  private static final int HAS_DRIFT = 32;
  
  private static final int HAS_DRIFT_UNCERTAINTY = 64;
  
  private static final int HAS_ELAPSED_REALTIME_NANOS = 128;
  
  private static final int HAS_ELAPSED_REALTIME_UNCERTAINTY_NANOS = 256;
  
  private static final int HAS_FULL_BIAS = 4;
  
  private static final int HAS_LEAP_SECOND = 1;
  
  private static final int HAS_NO_FLAGS = 0;
  
  private static final int HAS_REFERENCE_CARRIER_FREQUENCY_FOR_ISB = 1024;
  
  private static final int HAS_REFERENCE_CODE_TYPE_FOR_ISB = 2048;
  
  private static final int HAS_REFERENCE_CONSTELLATION_TYPE_FOR_ISB = 512;
  
  private static final int HAS_TIME_UNCERTAINTY = 2;
  
  private double mBiasNanos;
  
  private double mBiasUncertaintyNanos;
  
  private double mDriftNanosPerSecond;
  
  private double mDriftUncertaintyNanosPerSecond;
  
  private long mElapsedRealtimeNanos;
  
  private double mElapsedRealtimeUncertaintyNanos;
  
  private int mFlags;
  
  private long mFullBiasNanos;
  
  private int mHardwareClockDiscontinuityCount;
  
  private int mLeapSecond;
  
  private double mReferenceCarrierFrequencyHzForIsb;
  
  private String mReferenceCodeTypeForIsb;
  
  private int mReferenceConstellationTypeForIsb;
  
  private long mTimeNanos;
  
  private double mTimeUncertaintyNanos;
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mFlags);
    paramParcel.writeInt(this.mLeapSecond);
    paramParcel.writeLong(this.mTimeNanos);
    paramParcel.writeDouble(this.mTimeUncertaintyNanos);
    paramParcel.writeLong(this.mFullBiasNanos);
    paramParcel.writeDouble(this.mBiasNanos);
    paramParcel.writeDouble(this.mBiasUncertaintyNanos);
    paramParcel.writeDouble(this.mDriftNanosPerSecond);
    paramParcel.writeDouble(this.mDriftUncertaintyNanosPerSecond);
    paramParcel.writeInt(this.mHardwareClockDiscontinuityCount);
    paramParcel.writeLong(this.mElapsedRealtimeNanos);
    paramParcel.writeDouble(this.mElapsedRealtimeUncertaintyNanos);
    paramParcel.writeInt(this.mReferenceConstellationTypeForIsb);
    paramParcel.writeDouble(this.mReferenceCarrierFrequencyHzForIsb);
    paramParcel.writeString(this.mReferenceCodeTypeForIsb);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    Double double_;
    StringBuilder stringBuilder = new StringBuilder("GnssClock:\n");
    if (hasLeapSecond())
      stringBuilder.append(String.format("   %-15s = %s\n", new Object[] { "LeapSecond", Integer.valueOf(this.mLeapSecond) })); 
    long l = this.mTimeNanos;
    boolean bool = hasTimeUncertaintyNanos();
    Object object = null;
    if (bool) {
      double_ = Double.valueOf(this.mTimeUncertaintyNanos);
    } else {
      double_ = null;
    } 
    stringBuilder.append(String.format("   %-15s = %-25s   %-26s = %s\n", new Object[] { "TimeNanos", Long.valueOf(l), "TimeUncertaintyNanos", double_ }));
    if (hasFullBiasNanos())
      stringBuilder.append(String.format("   %-15s = %s\n", new Object[] { "FullBiasNanos", Long.valueOf(this.mFullBiasNanos) })); 
    if (hasBiasNanos() || hasBiasUncertaintyNanos()) {
      Object object1;
      if (hasBiasNanos()) {
        double_ = Double.valueOf(this.mBiasNanos);
      } else {
        double_ = null;
      } 
      if (hasBiasUncertaintyNanos()) {
        object1 = Double.valueOf(this.mBiasUncertaintyNanos);
      } else {
        object1 = null;
      } 
      stringBuilder.append(String.format("   %-15s = %-25s   %-26s = %s\n", new Object[] { "BiasNanos", double_, "BiasUncertaintyNanos", object1 }));
    } 
    if (hasDriftNanosPerSecond() || hasDriftUncertaintyNanosPerSecond()) {
      Object object1;
      if (hasDriftNanosPerSecond()) {
        double_ = Double.valueOf(this.mDriftNanosPerSecond);
      } else {
        double_ = null;
      } 
      if (hasDriftUncertaintyNanosPerSecond()) {
        object1 = Double.valueOf(this.mDriftUncertaintyNanosPerSecond);
      } else {
        object1 = null;
      } 
      stringBuilder.append(String.format("   %-15s = %-25s   %-26s = %s\n", new Object[] { "DriftNanosPerSecond", double_, "DriftUncertaintyNanosPerSecond", object1 }));
    } 
    int i = this.mHardwareClockDiscontinuityCount;
    stringBuilder.append(String.format("   %-15s = %s\n", new Object[] { "HardwareClockDiscontinuityCount", Integer.valueOf(i) }));
    if (hasElapsedRealtimeNanos() || hasElapsedRealtimeUncertaintyNanos()) {
      Object object1;
      if (hasElapsedRealtimeNanos()) {
        Long long_ = Long.valueOf(this.mElapsedRealtimeNanos);
      } else {
        double_ = null;
      } 
      if (hasElapsedRealtimeUncertaintyNanos()) {
        object1 = Double.valueOf(this.mElapsedRealtimeUncertaintyNanos);
      } else {
        object1 = object;
      } 
      stringBuilder.append(String.format("   %-15s = %-25s   %-26s = %s\n", new Object[] { "ElapsedRealtimeNanos", double_, "ElapsedRealtimeUncertaintyNanos", object1 }));
    } 
    if (hasReferenceConstellationTypeForIsb()) {
      i = this.mReferenceConstellationTypeForIsb;
      stringBuilder.append(String.format("   %-15s = %s\n", new Object[] { "ReferenceConstellationTypeForIsb", Integer.valueOf(i) }));
    } 
    if (hasReferenceCarrierFrequencyHzForIsb()) {
      double d = this.mReferenceCarrierFrequencyHzForIsb;
      stringBuilder.append(String.format("   %-15s = %s\n", new Object[] { "ReferenceCarrierFrequencyHzForIsb", Double.valueOf(d) }));
    } 
    if (hasReferenceCodeTypeForIsb()) {
      String str = this.mReferenceCodeTypeForIsb;
      str = String.format("   %-15s = %s\n", new Object[] { "ReferenceCodeTypeForIsb", str });
      stringBuilder.append(str);
    } 
    return stringBuilder.toString();
  }
  
  private void initialize() {
    this.mFlags = 0;
    resetLeapSecond();
    setTimeNanos(Long.MIN_VALUE);
    resetTimeUncertaintyNanos();
    resetFullBiasNanos();
    resetBiasNanos();
    resetBiasUncertaintyNanos();
    resetDriftNanosPerSecond();
    resetDriftUncertaintyNanosPerSecond();
    setHardwareClockDiscontinuityCount(-2147483648);
    resetElapsedRealtimeNanos();
    resetElapsedRealtimeUncertaintyNanos();
    resetReferenceConstellationTypeForIsb();
    resetReferenceCarrierFrequencyHzForIsb();
    resetReferenceCodeTypeForIsb();
  }
  
  private void setFlag(int paramInt) {
    this.mFlags |= paramInt;
  }
  
  private void resetFlag(int paramInt) {
    this.mFlags &= paramInt ^ 0xFFFFFFFF;
  }
  
  private boolean isFlagSet(int paramInt) {
    boolean bool;
    if ((this.mFlags & paramInt) == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
