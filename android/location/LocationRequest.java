package android.location;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.WorkSource;
import android.util.TimeUtils;
import com.android.internal.util.Preconditions;

@SystemApi
public final class LocationRequest implements Parcelable {
  public static final int ACCURACY_BLOCK = 102;
  
  public static final int ACCURACY_CITY = 104;
  
  public static final int ACCURACY_FINE = 100;
  
  public static LocationRequest create() {
    return new LocationRequest();
  }
  
  @SystemApi
  public static LocationRequest createFromDeprecatedProvider(String paramString, long paramLong, float paramFloat, boolean paramBoolean) {
    boolean bool;
    char c;
    if (paramString != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null provider");
    long l = paramLong;
    if (paramLong < 0L)
      l = 0L; 
    float f = paramFloat;
    if (paramFloat < 0.0F)
      f = 0.0F; 
    if ("passive".equals(paramString)) {
      c = 'È';
    } else if ("gps".equals(paramString)) {
      c = 'd';
    } else {
      c = 'É';
    } 
    LocationRequest locationRequest2 = new LocationRequest();
    LocationRequest locationRequest1 = locationRequest2.setProvider(paramString);
    locationRequest1 = locationRequest1.setQuality(c);
    locationRequest1 = locationRequest1.setInterval(l);
    locationRequest1 = locationRequest1.setFastestInterval(l);
    locationRequest1 = locationRequest1.setSmallestDisplacement(f);
    if (paramBoolean)
      locationRequest1.setNumUpdates(1); 
    return locationRequest1;
  }
  
  @SystemApi
  public static LocationRequest createFromDeprecatedCriteria(Criteria paramCriteria, long paramLong, float paramFloat, boolean paramBoolean) {
    boolean bool;
    if (paramCriteria != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null criteria");
    long l = paramLong;
    if (paramLong < 0L)
      l = 0L; 
    float f = paramFloat;
    if (paramFloat < 0.0F)
      f = 0.0F; 
    int i = paramCriteria.getAccuracy();
    if (i != 1) {
      if (i != 2) {
        if (paramCriteria.getPowerRequirement() == 3) {
          i = 203;
        } else {
          i = 201;
        } 
      } else {
        i = 102;
      } 
    } else {
      i = 100;
    } 
    LocationRequest locationRequest = new LocationRequest();
    locationRequest = locationRequest.setQuality(i);
    locationRequest = locationRequest.setInterval(l);
    locationRequest = locationRequest.setFastestInterval(l);
    locationRequest = locationRequest.setSmallestDisplacement(f);
    if (paramBoolean)
      locationRequest.setNumUpdates(1); 
    return locationRequest;
  }
  
  public LocationRequest() {
    this("fused", 201, 3600000L, 600000L, false, Long.MAX_VALUE, Long.MAX_VALUE, 2147483647, 0.0F, false, false, false, null);
  }
  
  public LocationRequest(LocationRequest paramLocationRequest) {
    this(paramLocationRequest.mProvider, paramLocationRequest.mQuality, paramLocationRequest.mInterval, paramLocationRequest.mFastestInterval, paramLocationRequest.mExplicitFastestInterval, paramLocationRequest.mExpireAt, paramLocationRequest.mExpireIn, paramLocationRequest.mNumUpdates, paramLocationRequest.mSmallestDisplacement, paramLocationRequest.mHideFromAppOps, paramLocationRequest.mLocationSettingsIgnored, paramLocationRequest.mLowPowerMode, paramLocationRequest.mWorkSource);
  }
  
  private LocationRequest(String paramString, int paramInt1, long paramLong1, long paramLong2, boolean paramBoolean1, long paramLong3, long paramLong4, int paramInt2, float paramFloat, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, WorkSource paramWorkSource) {
    boolean bool;
    if (paramString != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid provider: null");
    checkQuality(paramInt1);
    this.mProvider = paramString;
    this.mQuality = paramInt1;
    this.mInterval = paramLong1;
    this.mFastestInterval = paramLong2;
    this.mExplicitFastestInterval = paramBoolean1;
    this.mExpireAt = paramLong3;
    this.mExpireIn = paramLong4;
    this.mNumUpdates = paramInt2;
    this.mSmallestDisplacement = Preconditions.checkArgumentInRange(paramFloat, 0.0F, Float.MAX_VALUE, "smallestDisplacementM");
    this.mHideFromAppOps = paramBoolean2;
    this.mLowPowerMode = paramBoolean4;
    this.mLocationSettingsIgnored = paramBoolean3;
    this.mWorkSource = paramWorkSource;
  }
  
  public LocationRequest setQuality(int paramInt) {
    checkQuality(paramInt);
    this.mQuality = paramInt;
    return this;
  }
  
  public int getQuality() {
    return this.mQuality;
  }
  
  public LocationRequest setInterval(long paramLong) {
    boolean bool;
    if (paramLong >= 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid interval: + millis");
    this.mInterval = paramLong;
    if (!this.mExplicitFastestInterval)
      this.mFastestInterval = (long)(paramLong / 6.0D); 
    return this;
  }
  
  public long getInterval() {
    return this.mInterval;
  }
  
  public LocationRequest setLowPowerMode(boolean paramBoolean) {
    this.mLowPowerMode = paramBoolean;
    return this;
  }
  
  public boolean isLowPowerMode() {
    return this.mLowPowerMode;
  }
  
  public LocationRequest setLocationSettingsIgnored(boolean paramBoolean) {
    this.mLocationSettingsIgnored = paramBoolean;
    return this;
  }
  
  public boolean isLocationSettingsIgnored() {
    return this.mLocationSettingsIgnored;
  }
  
  public LocationRequest setFastestInterval(long paramLong) {
    boolean bool;
    if (paramLong >= 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid interval: + millis");
    this.mExplicitFastestInterval = true;
    this.mFastestInterval = paramLong;
    return this;
  }
  
  public long getFastestInterval() {
    return this.mFastestInterval;
  }
  
  @Deprecated
  public LocationRequest setExpireAt(long paramLong) {
    this.mExpireAt = Math.max(paramLong, 0L);
    return this;
  }
  
  @Deprecated
  public long getExpireAt() {
    return this.mExpireAt;
  }
  
  public LocationRequest setExpireIn(long paramLong) {
    this.mExpireIn = paramLong;
    return this;
  }
  
  public long getExpireIn() {
    return this.mExpireIn;
  }
  
  public long getExpirationRealtimeMs(long paramLong) {
    long l = this.mExpireIn;
    if (l > Long.MAX_VALUE - paramLong) {
      paramLong = Long.MAX_VALUE;
    } else {
      paramLong = l + paramLong;
    } 
    return Math.min(paramLong, this.mExpireAt);
  }
  
  public LocationRequest setNumUpdates(int paramInt) {
    if (paramInt > 0) {
      this.mNumUpdates = paramInt;
      return this;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("invalid numUpdates: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public int getNumUpdates() {
    return this.mNumUpdates;
  }
  
  public void decrementNumUpdates() {
    int i = this.mNumUpdates;
    if (i != Integer.MAX_VALUE)
      this.mNumUpdates = i - 1; 
    if (this.mNumUpdates < 0)
      this.mNumUpdates = 0; 
  }
  
  public LocationRequest setProvider(String paramString) {
    boolean bool;
    if (paramString != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid provider: null");
    this.mProvider = paramString;
    return this;
  }
  
  @SystemApi
  public String getProvider() {
    return this.mProvider;
  }
  
  @SystemApi
  public LocationRequest setSmallestDisplacement(float paramFloat) {
    this.mSmallestDisplacement = Preconditions.checkArgumentInRange(paramFloat, 0.0F, Float.MAX_VALUE, "smallestDisplacementM");
    return this;
  }
  
  @SystemApi
  public float getSmallestDisplacement() {
    return this.mSmallestDisplacement;
  }
  
  @SystemApi
  public void setWorkSource(WorkSource paramWorkSource) {
    this.mWorkSource = paramWorkSource;
  }
  
  @SystemApi
  public WorkSource getWorkSource() {
    return this.mWorkSource;
  }
  
  @SystemApi
  public void setHideFromAppOps(boolean paramBoolean) {
    this.mHideFromAppOps = paramBoolean;
  }
  
  @SystemApi
  public boolean getHideFromAppOps() {
    return this.mHideFromAppOps;
  }
  
  private static void checkQuality(int paramInt) {
    if (paramInt == 100 || paramInt == 102 || paramInt == 104 || paramInt == 203 || paramInt == 200 || paramInt == 201)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("invalid quality: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static final Parcelable.Creator<LocationRequest> CREATOR = new Parcelable.Creator<LocationRequest>() {
      public LocationRequest createFromParcel(Parcel param1Parcel) {
        String str = param1Parcel.readString();
        int i = param1Parcel.readInt();
        long l1 = param1Parcel.readLong();
        long l2 = param1Parcel.readLong();
        boolean bool1 = param1Parcel.readBoolean();
        long l3 = param1Parcel.readLong();
        long l4 = param1Parcel.readLong();
        int j = param1Parcel.readInt();
        float f = param1Parcel.readFloat();
        boolean bool2 = param1Parcel.readBoolean();
        boolean bool3 = param1Parcel.readBoolean();
        boolean bool4 = param1Parcel.readBoolean();
        Parcelable.Creator<WorkSource> creator = WorkSource.CREATOR;
        return new LocationRequest(str, i, l1, l2, bool1, l3, l4, j, f, bool2, bool3, bool4, param1Parcel.<WorkSource>readTypedObject(creator));
      }
      
      public LocationRequest[] newArray(int param1Int) {
        return new LocationRequest[param1Int];
      }
    };
  
  private static final long DEFAULT_INTERVAL_MS = 3600000L;
  
  private static final double FASTEST_INTERVAL_FACTOR = 6.0D;
  
  public static final int POWER_HIGH = 203;
  
  public static final int POWER_LOW = 201;
  
  public static final int POWER_NONE = 200;
  
  private long mExpireAt;
  
  private long mExpireIn;
  
  private boolean mExplicitFastestInterval;
  
  private long mFastestInterval;
  
  private boolean mHideFromAppOps;
  
  private long mInterval;
  
  private boolean mLocationSettingsIgnored;
  
  private boolean mLowPowerMode;
  
  private int mNumUpdates;
  
  private String mProvider;
  
  private int mQuality;
  
  private float mSmallestDisplacement;
  
  private WorkSource mWorkSource;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mProvider);
    paramParcel.writeInt(this.mQuality);
    paramParcel.writeLong(this.mInterval);
    paramParcel.writeLong(this.mFastestInterval);
    paramParcel.writeBoolean(this.mExplicitFastestInterval);
    paramParcel.writeLong(this.mExpireAt);
    paramParcel.writeLong(this.mExpireIn);
    paramParcel.writeInt(this.mNumUpdates);
    paramParcel.writeFloat(this.mSmallestDisplacement);
    paramParcel.writeBoolean(this.mHideFromAppOps);
    paramParcel.writeBoolean(this.mLocationSettingsIgnored);
    paramParcel.writeBoolean(this.mLowPowerMode);
    paramParcel.writeTypedObject(this.mWorkSource, 0);
  }
  
  public static String qualityToString(int paramInt) {
    if (paramInt != 100) {
      if (paramInt != 102) {
        if (paramInt != 104) {
          if (paramInt != 203) {
            if (paramInt != 200) {
              if (paramInt != 201)
                return "???"; 
              return "POWER_LOW";
            } 
            return "POWER_NONE";
          } 
          return "POWER_HIGH";
        } 
        return "ACCURACY_CITY";
      } 
      return "ACCURACY_BLOCK";
    } 
    return "ACCURACY_FINE";
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Request[");
    stringBuilder.append(qualityToString(this.mQuality));
    stringBuilder.append(" ");
    stringBuilder.append(this.mProvider);
    if (this.mQuality != 200) {
      stringBuilder.append(" interval=");
      TimeUtils.formatDuration(this.mInterval, stringBuilder);
      if (this.mExplicitFastestInterval) {
        stringBuilder.append(" fastestInterval=");
        TimeUtils.formatDuration(this.mFastestInterval, stringBuilder);
      } 
    } 
    if (this.mExpireAt != Long.MAX_VALUE) {
      stringBuilder.append(" expireAt=");
      stringBuilder.append(TimeUtils.formatRealtime(this.mExpireAt));
    } 
    if (this.mExpireIn != Long.MAX_VALUE) {
      stringBuilder.append(" expireIn=");
      TimeUtils.formatDuration(this.mExpireIn, stringBuilder);
    } 
    if (this.mNumUpdates != Integer.MAX_VALUE) {
      stringBuilder.append(" num=");
      stringBuilder.append(this.mNumUpdates);
    } 
    if (this.mLowPowerMode)
      stringBuilder.append(" lowPowerMode"); 
    if (this.mLocationSettingsIgnored)
      stringBuilder.append(" locationSettingsIgnored"); 
    stringBuilder.append(']');
    return stringBuilder.toString();
  }
}
