package android.location;

public interface CountryListener {
  void onCountryDetected(Country paramCountry);
}
