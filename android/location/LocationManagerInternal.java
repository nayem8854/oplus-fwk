package android.location;

public abstract class LocationManagerInternal {
  public abstract boolean isProviderEnabledForUser(String paramString, int paramInt);
  
  public abstract boolean isProviderPackage(String paramString);
  
  public abstract void sendNiResponse(int paramInt1, int paramInt2);
}
