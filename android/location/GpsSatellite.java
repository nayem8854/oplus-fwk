package android.location;

@Deprecated
public final class GpsSatellite {
  float mAzimuth;
  
  float mElevation;
  
  boolean mHasAlmanac;
  
  boolean mHasEphemeris;
  
  int mPrn;
  
  float mSnr;
  
  boolean mUsedInFix;
  
  boolean mValid;
  
  GpsSatellite(int paramInt) {
    this.mPrn = paramInt;
  }
  
  void setStatus(GpsSatellite paramGpsSatellite) {
    if (paramGpsSatellite == null) {
      this.mValid = false;
    } else {
      this.mValid = paramGpsSatellite.mValid;
      this.mHasEphemeris = paramGpsSatellite.mHasEphemeris;
      this.mHasAlmanac = paramGpsSatellite.mHasAlmanac;
      this.mUsedInFix = paramGpsSatellite.mUsedInFix;
      this.mSnr = paramGpsSatellite.mSnr;
      this.mElevation = paramGpsSatellite.mElevation;
      this.mAzimuth = paramGpsSatellite.mAzimuth;
    } 
  }
  
  public int getPrn() {
    return this.mPrn;
  }
  
  public float getSnr() {
    return this.mSnr;
  }
  
  public float getElevation() {
    return this.mElevation;
  }
  
  public float getAzimuth() {
    return this.mAzimuth;
  }
  
  public boolean hasEphemeris() {
    return this.mHasEphemeris;
  }
  
  public boolean hasAlmanac() {
    return this.mHasAlmanac;
  }
  
  public boolean usedInFix() {
    return this.mUsedInFix;
  }
}
