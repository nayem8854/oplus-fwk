package android.location;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

public abstract class SettingInjectorService extends Service {
  public static final String ACTION_INJECTED_SETTING_CHANGED = "android.location.InjectedSettingChanged";
  
  public static final String ACTION_SERVICE_INTENT = "android.location.SettingInjectorService";
  
  public static final String ATTRIBUTES_NAME = "injected-location-setting";
  
  public static final String ENABLED_KEY = "enabled";
  
  public static final String MESSENGER_KEY = "messenger";
  
  public static final String META_DATA_NAME = "android.location.SettingInjectorService";
  
  public static final String SUMMARY_KEY = "summary";
  
  private static final String TAG = "SettingInjectorService";
  
  private final String mName;
  
  public SettingInjectorService(String paramString) {
    this.mName = paramString;
  }
  
  public final IBinder onBind(Intent paramIntent) {
    return null;
  }
  
  public final void onStart(Intent paramIntent, int paramInt) {
    super.onStart(paramIntent, paramInt);
  }
  
  public final int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2) {
    onHandleIntent(paramIntent);
    stopSelf(paramInt2);
    return 2;
  }
  
  private void onHandleIntent(Intent paramIntent) {
    String str = null;
    try {
      String str1 = onGetSummary();
      str = str1;
      boolean bool = onGetEnabled();
      return;
    } finally {
      sendStatus(paramIntent, str, false);
    } 
  }
  
  private void sendStatus(Intent paramIntent, String paramString, boolean paramBoolean) {
    Messenger messenger = (Messenger)paramIntent.getParcelableExtra("messenger");
    if (messenger == null)
      return; 
    Message message = Message.obtain();
    Bundle bundle = new Bundle();
    bundle.putString("summary", paramString);
    bundle.putBoolean("enabled", paramBoolean);
    message.setData(bundle);
    if (Log.isLoggable("SettingInjectorService", 3)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this.mName);
      stringBuilder.append(": received ");
      stringBuilder.append(paramIntent);
      stringBuilder.append(", summary=");
      stringBuilder.append(paramString);
      stringBuilder.append(", enabled=");
      stringBuilder.append(paramBoolean);
      stringBuilder.append(", sending message: ");
      stringBuilder.append(message);
      Log.d("SettingInjectorService", stringBuilder.toString());
    } 
    try {
      messenger.send(message);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this.mName);
      stringBuilder.append(": sending dynamic status failed");
      Log.e("SettingInjectorService", stringBuilder.toString(), (Throwable)remoteException);
    } 
  }
  
  public static final void refreshSettings(Context paramContext) {
    Intent intent = new Intent("android.location.InjectedSettingChanged");
    paramContext.sendBroadcast(intent);
  }
  
  protected abstract boolean onGetEnabled();
  
  protected abstract String onGetSummary();
}
