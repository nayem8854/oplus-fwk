package android.location;

import android.os.Binder;
import android.os.Handler;
import android.os.HandlerExecutor;
import android.os.RemoteException;
import android.util.ArrayMap;
import com.android.internal.util.Preconditions;
import com.android.internal.util.function.pooled.PooledLambda;
import com.android.internal.util.function.pooled.PooledRunnable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

abstract class AbstractListenerManager<TRequest, TListener> {
  private TRequest mMergedRequest;
  
  private static class Registration<TRequest, TListener> {
    private final Executor mExecutor;
    
    private volatile TListener mListener;
    
    private TRequest mRequest;
    
    private Registration(TRequest param1TRequest, Executor param1Executor, TListener param1TListener) {
      boolean bool2, bool1 = true;
      if (param1TListener != null) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      Preconditions.checkArgument(bool2, "invalid null listener/callback");
      if (param1Executor != null) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      Preconditions.checkArgument(bool2, "invalid null executor");
      this.mExecutor = param1Executor;
      this.mListener = param1TListener;
      this.mRequest = param1TRequest;
    }
    
    public TRequest getRequest() {
      return this.mRequest;
    }
    
    private void unregister() {
      this.mRequest = null;
      this.mListener = null;
    }
    
    private void execute(Consumer<TListener> param1Consumer) {
      Executor executor = this.mExecutor;
      -$.Lambda.AbstractListenerManager.Registration.XpiThbVaDDpOnFWIkrt38Bf4yx0 xpiThbVaDDpOnFWIkrt38Bf4yx0 = _$$Lambda$AbstractListenerManager$Registration$XpiThbVaDDpOnFWIkrt38Bf4yx0.INSTANCE;
      PooledRunnable pooledRunnable = PooledLambda.obtainRunnable((BiConsumer)xpiThbVaDDpOnFWIkrt38Bf4yx0, this, param1Consumer);
      pooledRunnable = pooledRunnable.recycleOnUse();
      executor.execute((Runnable)pooledRunnable);
    }
    
    private void accept(Consumer<TListener> param1Consumer) {
      TListener tListener = this.mListener;
      if (tListener == null)
        return; 
      long l = Binder.clearCallingIdentity();
      try {
        param1Consumer.accept(tListener);
        return;
      } finally {
        Binder.restoreCallingIdentity(l);
      } 
    }
  }
  
  private final Object mLock = new Object();
  
  private volatile ArrayMap<Object, Registration<TRequest, TListener>> mListeners = new ArrayMap();
  
  public boolean addListener(TListener paramTListener, Handler paramHandler) throws RemoteException {
    return addInternal((TRequest)null, paramTListener, paramHandler);
  }
  
  public boolean addListener(TListener paramTListener, Executor paramExecutor) throws RemoteException {
    return addInternal((TRequest)null, paramTListener, paramExecutor);
  }
  
  public boolean addListener(TRequest paramTRequest, TListener paramTListener, Handler paramHandler) throws RemoteException {
    return addInternal(paramTRequest, paramTListener, paramHandler);
  }
  
  public boolean addListener(TRequest paramTRequest, TListener paramTListener, Executor paramExecutor) throws RemoteException {
    return addInternal(paramTRequest, paramTListener, paramExecutor);
  }
  
  protected final boolean addInternal(TRequest paramTRequest, Object paramObject, Handler paramHandler) throws RemoteException {
    return addInternal(paramTRequest, paramObject, new HandlerExecutor(paramHandler));
  }
  
  protected final boolean addInternal(TRequest paramTRequest, Object paramObject, Executor paramExecutor) throws RemoteException {
    boolean bool;
    if (paramObject != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid null listener/callback");
    return addInternal(paramObject, new Registration<>(paramTRequest, paramExecutor, convertKey(paramObject)));
  }
  
  private boolean addInternal(Object paramObject, Registration<TRequest, TListener> paramRegistration) throws RemoteException {
    Preconditions.checkNotNull(paramRegistration);
    synchronized (this.mLock) {
      boolean bool = this.mListeners.isEmpty();
      ArrayMap<Object, Registration<TRequest, TListener>> arrayMap1 = new ArrayMap(), arrayMap2 = this.mListeners;
      this(arrayMap2.size() + 1);
      arrayMap1.putAll(this.mListeners);
      paramObject = arrayMap1.put(paramObject, paramRegistration);
      this.mListeners = arrayMap1;
      if (paramObject != null)
        paramObject.unregister(); 
      paramObject = mergeRequests();
      if (bool || !Objects.equals(paramObject, this.mMergedRequest)) {
        this.mMergedRequest = (TRequest)paramObject;
        if (!bool)
          unregisterService(); 
        registerService(this.mMergedRequest);
      } 
      return true;
    } 
  }
  
  public void removeListener(Object paramObject) throws RemoteException {
    synchronized (this.mLock) {
      boolean bool1;
      ArrayMap<Object, Registration<TRequest, TListener>> arrayMap = new ArrayMap();
      this(this.mListeners);
      paramObject = arrayMap.remove(paramObject);
      this.mListeners = arrayMap;
      if (paramObject == null)
        return; 
      paramObject.unregister();
      boolean bool = this.mListeners.isEmpty();
      if (bool) {
        paramObject = null;
      } else {
        paramObject = mergeRequests();
      } 
      if (!bool && !Objects.equals(paramObject, this.mMergedRequest)) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      if (bool || bool1) {
        unregisterService();
        this.mMergedRequest = (TRequest)paramObject;
        if (bool1)
          registerService((TRequest)paramObject); 
      } 
      return;
    } 
  }
  
  protected TListener convertKey(Object paramObject) {
    return (TListener)paramObject;
  }
  
  protected TRequest merge(List<TRequest> paramList) {
    // Byte code:
    //   0: aload_1
    //   1: invokeinterface iterator : ()Ljava/util/Iterator;
    //   6: astore_1
    //   7: aload_1
    //   8: invokeinterface hasNext : ()Z
    //   13: ifeq -> 43
    //   16: aload_1
    //   17: invokeinterface next : ()Ljava/lang/Object;
    //   22: astore_2
    //   23: aload_2
    //   24: ifnonnull -> 32
    //   27: iconst_1
    //   28: istore_3
    //   29: goto -> 34
    //   32: iconst_0
    //   33: istore_3
    //   34: iload_3
    //   35: ldc 'merge() has to be overridden for non-null requests.'
    //   37: invokestatic checkArgument : (ZLjava/lang/Object;)V
    //   40: goto -> 7
    //   43: aconst_null
    //   44: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #199	-> 0
    //   #200	-> 23
    //   #202	-> 40
    //   #203	-> 43
  }
  
  protected void execute(Consumer<TListener> paramConsumer) {
    for (Registration registration : this.mListeners.values())
      registration.execute(paramConsumer); 
  }
  
  private TRequest mergeRequests() {
    Preconditions.checkState(Thread.holdsLock(this.mLock));
    if (this.mListeners.isEmpty())
      return null; 
    if (this.mListeners.size() == 1)
      return (TRequest)((Registration)this.mListeners.valueAt(0)).getRequest(); 
    ArrayList<TRequest> arrayList = new ArrayList(this.mListeners.size());
    for (byte b = 0; b < this.mListeners.size(); b++)
      arrayList.add(((Registration)this.mListeners.valueAt(b)).getRequest()); 
    return merge(arrayList);
  }
  
  protected abstract boolean registerService(TRequest paramTRequest) throws RemoteException;
  
  protected abstract void unregisterService() throws RemoteException;
}
