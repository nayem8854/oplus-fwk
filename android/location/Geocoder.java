package android.location;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public final class Geocoder {
  private static final String TAG = "Geocoder";
  
  private GeocoderParams mParams;
  
  private ILocationManager mService;
  
  public static boolean isPresent() {
    IBinder iBinder = ServiceManager.getService("location");
    ILocationManager iLocationManager = ILocationManager.Stub.asInterface(iBinder);
    if (iLocationManager == null)
      return false; 
    try {
      return iLocationManager.geocoderIsPresent();
    } catch (RemoteException remoteException) {
      Log.e("Geocoder", "isPresent: got RemoteException", (Throwable)remoteException);
      return false;
    } 
  }
  
  public Geocoder(Context paramContext, Locale paramLocale) {
    if (paramLocale != null) {
      this.mParams = new GeocoderParams(paramContext, paramLocale);
      IBinder iBinder = ServiceManager.getService("location");
      this.mService = ILocationManager.Stub.asInterface(iBinder);
      return;
    } 
    throw new NullPointerException("locale == null");
  }
  
  public Geocoder(Context paramContext) {
    this(paramContext, Locale.getDefault());
  }
  
  public List<Address> getFromLocation(double paramDouble1, double paramDouble2, int paramInt) throws IOException {
    if (Double.isNaN(paramDouble1) || Double.isNaN(paramDouble2)) {
      Log.e("Geocoder", "NaN is not allowed as latitude/longitude input");
      return Collections.emptyList();
    } 
    if (paramDouble1 >= -90.0D && paramDouble1 <= 90.0D) {
      if (paramDouble2 >= -180.0D && paramDouble2 <= 180.0D)
        try {
          ArrayList<Address> arrayList = new ArrayList();
          this();
          String str = this.mService.getFromLocation(paramDouble1, paramDouble2, paramInt, this.mParams, arrayList);
          if (str == null)
            return arrayList; 
          IOException iOException = new IOException();
          this(str);
          throw iOException;
        } catch (RemoteException remoteException) {
          Log.e("Geocoder", "getFromLocation: got RemoteException", (Throwable)remoteException);
          return null;
        }  
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("longitude == ");
      stringBuilder1.append(paramDouble2);
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("latitude == ");
    stringBuilder.append(paramDouble1);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public List<Address> getFromLocationName(String paramString, int paramInt) throws IOException {
    if (paramString != null)
      try {
        ArrayList<Address> arrayList = new ArrayList();
        this();
        paramString = this.mService.getFromLocationName(paramString, 0.0D, 0.0D, 0.0D, 0.0D, paramInt, this.mParams, arrayList);
        if (paramString == null)
          return arrayList; 
        IOException iOException = new IOException();
        this(paramString);
        throw iOException;
      } catch (RemoteException remoteException) {
        Log.e("Geocoder", "getFromLocationName: got RemoteException", (Throwable)remoteException);
        return null;
      }  
    throw new IllegalArgumentException("locationName == null");
  }
  
  public List<Address> getFromLocationName(String paramString, int paramInt, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4) throws IOException {
    if (paramString != null) {
      if (paramDouble1 >= -90.0D && paramDouble1 <= 90.0D) {
        if (paramDouble2 >= -180.0D && paramDouble2 <= 180.0D) {
          if (paramDouble3 >= -90.0D && paramDouble3 <= 90.0D) {
            if (paramDouble4 >= -180.0D && paramDouble4 <= 180.0D)
              try {
                ArrayList<Address> arrayList = new ArrayList();
                this();
                paramString = this.mService.getFromLocationName(paramString, paramDouble1, paramDouble2, paramDouble3, paramDouble4, paramInt, this.mParams, arrayList);
                if (paramString == null)
                  return arrayList; 
                IOException iOException = new IOException();
                this(paramString);
                throw iOException;
              } catch (RemoteException remoteException) {
                Log.e("Geocoder", "getFromLocationName: got RemoteException", (Throwable)remoteException);
                return null;
              }  
            StringBuilder stringBuilder3 = new StringBuilder();
            stringBuilder3.append("upperRightLongitude == ");
            stringBuilder3.append(paramDouble4);
            throw new IllegalArgumentException(stringBuilder3.toString());
          } 
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("upperRightLatitude == ");
          stringBuilder2.append(paramDouble3);
          throw new IllegalArgumentException(stringBuilder2.toString());
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("lowerLeftLongitude == ");
        stringBuilder1.append(paramDouble2);
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("lowerLeftLatitude == ");
      stringBuilder.append(paramDouble1);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    throw new IllegalArgumentException("locationName == null");
  }
}
