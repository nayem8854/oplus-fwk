package android.location;

import android.annotation.SystemApi;

public final class GnssCapabilities {
  public static final long ANTENNA_INFO = 512L;
  
  public static final long GEOFENCING = 4L;
  
  public static final long INVALID_CAPABILITIES = -1L;
  
  public static final long LOW_POWER_MODE = 1L;
  
  public static final long MEASUREMENTS = 8L;
  
  public static final long MEASUREMENT_CORRECTIONS = 32L;
  
  public static final long MEASUREMENT_CORRECTIONS_EXCESS_PATH_LENGTH = 128L;
  
  public static final long MEASUREMENT_CORRECTIONS_LOS_SATS = 64L;
  
  public static final long MEASUREMENT_CORRECTIONS_REFLECTING_PLANE = 256L;
  
  public static final long NAV_MESSAGES = 16L;
  
  public static final long SATELLITE_BLACKLIST = 2L;
  
  private final long mGnssCapabilities;
  
  public static GnssCapabilities of(long paramLong) {
    return new GnssCapabilities(paramLong);
  }
  
  private GnssCapabilities(long paramLong) {
    this.mGnssCapabilities = paramLong;
  }
  
  @SystemApi
  public boolean hasLowPowerMode() {
    return hasCapability(1L);
  }
  
  @SystemApi
  public boolean hasSatelliteBlacklist() {
    return hasCapability(2L);
  }
  
  @SystemApi
  public boolean hasGeofencing() {
    return hasCapability(4L);
  }
  
  @SystemApi
  public boolean hasMeasurements() {
    return hasCapability(8L);
  }
  
  @SystemApi
  public boolean hasNavMessages() {
    return hasCapability(16L);
  }
  
  @SystemApi
  public boolean hasMeasurementCorrections() {
    return hasCapability(32L);
  }
  
  @SystemApi
  public boolean hasMeasurementCorrectionsLosSats() {
    return hasCapability(64L);
  }
  
  @SystemApi
  public boolean hasMeasurementCorrectionsExcessPathLength() {
    return hasCapability(128L);
  }
  
  @SystemApi
  public boolean hasMeasurementCorrectionsReflectingPane() {
    return hasCapability(256L);
  }
  
  public boolean hasGnssAntennaInfo() {
    return hasCapability(512L);
  }
  
  private boolean hasCapability(long paramLong) {
    boolean bool;
    if ((this.mGnssCapabilities & paramLong) == paramLong) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
