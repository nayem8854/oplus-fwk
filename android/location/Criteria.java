package android.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Criteria implements Parcelable {
  private int mHorizontalAccuracy = 0;
  
  private int mVerticalAccuracy = 0;
  
  private int mSpeedAccuracy = 0;
  
  private int mBearingAccuracy = 0;
  
  private int mPowerRequirement = 0;
  
  private boolean mAltitudeRequired = false;
  
  private boolean mBearingRequired = false;
  
  private boolean mSpeedRequired = false;
  
  private boolean mCostAllowed = false;
  
  public Criteria(Criteria paramCriteria) {
    this.mHorizontalAccuracy = paramCriteria.mHorizontalAccuracy;
    this.mVerticalAccuracy = paramCriteria.mVerticalAccuracy;
    this.mSpeedAccuracy = paramCriteria.mSpeedAccuracy;
    this.mBearingAccuracy = paramCriteria.mBearingAccuracy;
    this.mPowerRequirement = paramCriteria.mPowerRequirement;
    this.mAltitudeRequired = paramCriteria.mAltitudeRequired;
    this.mBearingRequired = paramCriteria.mBearingRequired;
    this.mSpeedRequired = paramCriteria.mSpeedRequired;
    this.mCostAllowed = paramCriteria.mCostAllowed;
  }
  
  public void setHorizontalAccuracy(int paramInt) {
    this.mHorizontalAccuracy = Preconditions.checkArgumentInRange(paramInt, 0, 3, "accuracy");
  }
  
  public int getHorizontalAccuracy() {
    return this.mHorizontalAccuracy;
  }
  
  public void setVerticalAccuracy(int paramInt) {
    this.mVerticalAccuracy = Preconditions.checkArgumentInRange(paramInt, 0, 3, "accuracy");
  }
  
  public int getVerticalAccuracy() {
    return this.mVerticalAccuracy;
  }
  
  public void setSpeedAccuracy(int paramInt) {
    this.mSpeedAccuracy = Preconditions.checkArgumentInRange(paramInt, 0, 3, "accuracy");
  }
  
  public int getSpeedAccuracy() {
    return this.mSpeedAccuracy;
  }
  
  public void setBearingAccuracy(int paramInt) {
    this.mBearingAccuracy = Preconditions.checkArgumentInRange(paramInt, 0, 3, "accuracy");
  }
  
  public int getBearingAccuracy() {
    return this.mBearingAccuracy;
  }
  
  public void setAccuracy(int paramInt) {
    Preconditions.checkArgumentInRange(paramInt, 0, 2, "accuracy");
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt == 2)
          setHorizontalAccuracy(1); 
      } else {
        setHorizontalAccuracy(3);
      } 
    } else {
      setHorizontalAccuracy(0);
    } 
  }
  
  public int getAccuracy() {
    if (this.mHorizontalAccuracy >= 3)
      return 1; 
    return 2;
  }
  
  public void setPowerRequirement(int paramInt) {
    this.mPowerRequirement = Preconditions.checkArgumentInRange(paramInt, 0, 3, "powerRequirement");
  }
  
  public int getPowerRequirement() {
    return this.mPowerRequirement;
  }
  
  public void setCostAllowed(boolean paramBoolean) {
    this.mCostAllowed = paramBoolean;
  }
  
  public boolean isCostAllowed() {
    return this.mCostAllowed;
  }
  
  public void setAltitudeRequired(boolean paramBoolean) {
    this.mAltitudeRequired = paramBoolean;
  }
  
  public boolean isAltitudeRequired() {
    return this.mAltitudeRequired;
  }
  
  public void setSpeedRequired(boolean paramBoolean) {
    this.mSpeedRequired = paramBoolean;
  }
  
  public boolean isSpeedRequired() {
    return this.mSpeedRequired;
  }
  
  public void setBearingRequired(boolean paramBoolean) {
    this.mBearingRequired = paramBoolean;
  }
  
  public boolean isBearingRequired() {
    return this.mBearingRequired;
  }
  
  public static final Parcelable.Creator<Criteria> CREATOR = new Parcelable.Creator<Criteria>() {
      public Criteria createFromParcel(Parcel param1Parcel) {
        boolean bool2;
        Criteria criteria = new Criteria();
        Criteria.access$002(criteria, param1Parcel.readInt());
        Criteria.access$102(criteria, param1Parcel.readInt());
        Criteria.access$202(criteria, param1Parcel.readInt());
        Criteria.access$302(criteria, param1Parcel.readInt());
        Criteria.access$402(criteria, param1Parcel.readInt());
        int i = param1Parcel.readInt();
        boolean bool1 = true;
        if (i != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        Criteria.access$502(criteria, bool2);
        if (param1Parcel.readInt() != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        Criteria.access$602(criteria, bool2);
        if (param1Parcel.readInt() != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        Criteria.access$702(criteria, bool2);
        if (param1Parcel.readInt() != 0) {
          bool2 = bool1;
        } else {
          bool2 = false;
        } 
        Criteria.access$802(criteria, bool2);
        return criteria;
      }
      
      public Criteria[] newArray(int param1Int) {
        return new Criteria[param1Int];
      }
    };
  
  public static final int ACCURACY_COARSE = 2;
  
  public static final int ACCURACY_FINE = 1;
  
  public static final int ACCURACY_HIGH = 3;
  
  public static final int ACCURACY_LOW = 1;
  
  public static final int ACCURACY_MEDIUM = 2;
  
  public static final int NO_REQUIREMENT = 0;
  
  public static final int POWER_HIGH = 3;
  
  public static final int POWER_LOW = 1;
  
  public static final int POWER_MEDIUM = 2;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mHorizontalAccuracy);
    paramParcel.writeInt(this.mVerticalAccuracy);
    paramParcel.writeInt(this.mSpeedAccuracy);
    paramParcel.writeInt(this.mBearingAccuracy);
    paramParcel.writeInt(this.mPowerRequirement);
    paramParcel.writeInt(this.mAltitudeRequired);
    paramParcel.writeInt(this.mBearingRequired);
    paramParcel.writeInt(this.mSpeedRequired);
    paramParcel.writeInt(this.mCostAllowed);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Criteria[");
    stringBuilder.append("power=");
    stringBuilder.append(requirementToString(this.mPowerRequirement));
    stringBuilder.append(", ");
    stringBuilder.append("accuracy=");
    stringBuilder.append(requirementToString(this.mHorizontalAccuracy));
    if (this.mVerticalAccuracy != 0) {
      stringBuilder.append(", verticalAccuracy=");
      stringBuilder.append(requirementToString(this.mVerticalAccuracy));
    } 
    if (this.mSpeedAccuracy != 0) {
      stringBuilder.append(", speedAccuracy=");
      stringBuilder.append(requirementToString(this.mSpeedAccuracy));
    } 
    if (this.mBearingAccuracy != 0) {
      stringBuilder.append(", bearingAccuracy=");
      stringBuilder.append(requirementToString(this.mBearingAccuracy));
    } 
    if (this.mAltitudeRequired || this.mBearingRequired || this.mSpeedRequired) {
      stringBuilder.append(", required=[");
      if (this.mAltitudeRequired)
        stringBuilder.append("altitude, "); 
      if (this.mBearingRequired)
        stringBuilder.append("bearing, "); 
      if (this.mSpeedRequired)
        stringBuilder.append("speed, "); 
      stringBuilder.setLength(stringBuilder.length() - 2);
      stringBuilder.append("]");
    } 
    if (this.mCostAllowed)
      stringBuilder.append(", costAllowed"); 
    stringBuilder.append(']');
    return stringBuilder.toString();
  }
  
  private static String requirementToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3)
            return "???"; 
          return "High";
        } 
        return "Medium";
      } 
      return "Low";
    } 
    return "None";
  }
  
  public Criteria() {}
  
  @Retention(RetentionPolicy.SOURCE)
  class AccuracyRequirement implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class LocationAccuracyRequirement implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class PowerRequirement implements Annotation {}
}
