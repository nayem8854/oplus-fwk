package android.location;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class Address implements Parcelable {
  private int mMaxAddressLineIndex = -1;
  
  private boolean mHasLatitude = false;
  
  private boolean mHasLongitude = false;
  
  private Bundle mExtras = null;
  
  public Address(Locale paramLocale) {
    this.mLocale = paramLocale;
  }
  
  public Locale getLocale() {
    return this.mLocale;
  }
  
  public int getMaxAddressLineIndex() {
    return this.mMaxAddressLineIndex;
  }
  
  public String getAddressLine(int paramInt) {
    if (paramInt >= 0) {
      String str;
      HashMap<Integer, String> hashMap = this.mAddressLines;
      if (hashMap == null) {
        hashMap = null;
      } else {
        str = hashMap.get(Integer.valueOf(paramInt));
      } 
      return str;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("index = ");
    stringBuilder.append(paramInt);
    stringBuilder.append(" < 0");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void setAddressLine(int paramInt, String paramString) {
    if (paramInt >= 0) {
      if (this.mAddressLines == null)
        this.mAddressLines = new HashMap<>(); 
      this.mAddressLines.put(Integer.valueOf(paramInt), paramString);
      if (paramString == null) {
        this.mMaxAddressLineIndex = -1;
        for (Integer integer : this.mAddressLines.keySet())
          this.mMaxAddressLineIndex = Math.max(this.mMaxAddressLineIndex, integer.intValue()); 
      } else {
        this.mMaxAddressLineIndex = Math.max(this.mMaxAddressLineIndex, paramInt);
      } 
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("index = ");
    stringBuilder.append(paramInt);
    stringBuilder.append(" < 0");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public String getFeatureName() {
    return this.mFeatureName;
  }
  
  public void setFeatureName(String paramString) {
    this.mFeatureName = paramString;
  }
  
  public String getAdminArea() {
    return this.mAdminArea;
  }
  
  public void setAdminArea(String paramString) {
    this.mAdminArea = paramString;
  }
  
  public String getSubAdminArea() {
    return this.mSubAdminArea;
  }
  
  public void setSubAdminArea(String paramString) {
    this.mSubAdminArea = paramString;
  }
  
  public String getLocality() {
    return this.mLocality;
  }
  
  public void setLocality(String paramString) {
    this.mLocality = paramString;
  }
  
  public String getSubLocality() {
    return this.mSubLocality;
  }
  
  public void setSubLocality(String paramString) {
    this.mSubLocality = paramString;
  }
  
  public String getThoroughfare() {
    return this.mThoroughfare;
  }
  
  public void setThoroughfare(String paramString) {
    this.mThoroughfare = paramString;
  }
  
  public String getSubThoroughfare() {
    return this.mSubThoroughfare;
  }
  
  public void setSubThoroughfare(String paramString) {
    this.mSubThoroughfare = paramString;
  }
  
  public String getPremises() {
    return this.mPremises;
  }
  
  public void setPremises(String paramString) {
    this.mPremises = paramString;
  }
  
  public String getPostalCode() {
    return this.mPostalCode;
  }
  
  public void setPostalCode(String paramString) {
    this.mPostalCode = paramString;
  }
  
  public String getCountryCode() {
    return this.mCountryCode;
  }
  
  public void setCountryCode(String paramString) {
    this.mCountryCode = paramString;
  }
  
  public String getCountryName() {
    return this.mCountryName;
  }
  
  public void setCountryName(String paramString) {
    this.mCountryName = paramString;
  }
  
  public boolean hasLatitude() {
    return this.mHasLatitude;
  }
  
  public double getLatitude() {
    if (this.mHasLatitude)
      return this.mLatitude; 
    throw new IllegalStateException();
  }
  
  public void setLatitude(double paramDouble) {
    this.mLatitude = paramDouble;
    this.mHasLatitude = true;
  }
  
  public void clearLatitude() {
    this.mHasLatitude = false;
  }
  
  public boolean hasLongitude() {
    return this.mHasLongitude;
  }
  
  public double getLongitude() {
    if (this.mHasLongitude)
      return this.mLongitude; 
    throw new IllegalStateException();
  }
  
  public void setLongitude(double paramDouble) {
    this.mLongitude = paramDouble;
    this.mHasLongitude = true;
  }
  
  public void clearLongitude() {
    this.mHasLongitude = false;
  }
  
  public String getPhone() {
    return this.mPhone;
  }
  
  public void setPhone(String paramString) {
    this.mPhone = paramString;
  }
  
  public String getUrl() {
    return this.mUrl;
  }
  
  public void setUrl(String paramString) {
    this.mUrl = paramString;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public void setExtras(Bundle paramBundle) {
    if (paramBundle == null) {
      paramBundle = null;
    } else {
      paramBundle = new Bundle(paramBundle);
    } 
    this.mExtras = paramBundle;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Address[addressLines=[");
    for (byte b = 0; b <= this.mMaxAddressLineIndex; b++) {
      if (b > 0)
        stringBuilder.append(','); 
      stringBuilder.append(b);
      stringBuilder.append(':');
      String str = this.mAddressLines.get(Integer.valueOf(b));
      if (str == null) {
        stringBuilder.append("null");
      } else {
        stringBuilder.append('"');
        stringBuilder.append(str);
        stringBuilder.append('"');
      } 
    } 
    stringBuilder.append(']');
    stringBuilder.append(",feature=");
    stringBuilder.append(this.mFeatureName);
    stringBuilder.append(",admin=");
    stringBuilder.append(this.mAdminArea);
    stringBuilder.append(",sub-admin=");
    stringBuilder.append(this.mSubAdminArea);
    stringBuilder.append(",locality=");
    stringBuilder.append(this.mLocality);
    stringBuilder.append(",thoroughfare=");
    stringBuilder.append(this.mThoroughfare);
    stringBuilder.append(",postalCode=");
    stringBuilder.append(this.mPostalCode);
    stringBuilder.append(",countryCode=");
    stringBuilder.append(this.mCountryCode);
    stringBuilder.append(",countryName=");
    stringBuilder.append(this.mCountryName);
    stringBuilder.append(",hasLatitude=");
    stringBuilder.append(this.mHasLatitude);
    stringBuilder.append(",latitude=");
    stringBuilder.append(this.mLatitude);
    stringBuilder.append(",hasLongitude=");
    stringBuilder.append(this.mHasLongitude);
    stringBuilder.append(",longitude=");
    stringBuilder.append(this.mLongitude);
    stringBuilder.append(",phone=");
    stringBuilder.append(this.mPhone);
    stringBuilder.append(",url=");
    stringBuilder.append(this.mUrl);
    stringBuilder.append(",extras=");
    stringBuilder.append(this.mExtras);
    stringBuilder.append(']');
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<Address> CREATOR = new Parcelable.Creator<Address>() {
      public Address createFromParcel(Parcel param1Parcel) {
        Locale locale;
        boolean bool2;
        String str1 = param1Parcel.readString();
        String str2 = param1Parcel.readString();
        if (str2.length() > 0) {
          locale = new Locale(str1, str2);
        } else {
          locale = new Locale(str1);
        } 
        Address address = new Address(locale);
        int i = param1Parcel.readInt();
        if (i > 0) {
          Address.access$002(address, new HashMap<>(i));
          for (byte b = 0; b < i; b++) {
            int k = param1Parcel.readInt();
            String str = param1Parcel.readString();
            address.mAddressLines.put(Integer.valueOf(k), str);
            k = Math.max(address.mMaxAddressLineIndex, k);
            Address.access$102(address, k);
          } 
        } else {
          Address.access$002(address, null);
          Address.access$102(address, -1);
        } 
        Address.access$202(address, param1Parcel.readString());
        Address.access$302(address, param1Parcel.readString());
        Address.access$402(address, param1Parcel.readString());
        Address.access$502(address, param1Parcel.readString());
        Address.access$602(address, param1Parcel.readString());
        Address.access$702(address, param1Parcel.readString());
        Address.access$802(address, param1Parcel.readString());
        Address.access$902(address, param1Parcel.readString());
        Address.access$1002(address, param1Parcel.readString());
        Address.access$1102(address, param1Parcel.readString());
        Address.access$1202(address, param1Parcel.readString());
        int j = param1Parcel.readInt();
        boolean bool1 = false;
        if (j == 0) {
          bool2 = false;
        } else {
          bool2 = true;
        } 
        Address.access$1302(address, bool2);
        if (address.mHasLatitude)
          Address.access$1402(address, param1Parcel.readDouble()); 
        if (param1Parcel.readInt() == 0) {
          bool2 = bool1;
        } else {
          bool2 = true;
        } 
        Address.access$1502(address, bool2);
        if (address.mHasLongitude)
          Address.access$1602(address, param1Parcel.readDouble()); 
        Address.access$1702(address, param1Parcel.readString());
        Address.access$1802(address, param1Parcel.readString());
        Address.access$1902(address, param1Parcel.readBundle());
        return address;
      }
      
      public Address[] newArray(int param1Int) {
        return new Address[param1Int];
      }
    };
  
  private HashMap<Integer, String> mAddressLines;
  
  private String mAdminArea;
  
  private String mCountryCode;
  
  private String mCountryName;
  
  private String mFeatureName;
  
  private double mLatitude;
  
  private Locale mLocale;
  
  private String mLocality;
  
  private double mLongitude;
  
  private String mPhone;
  
  private String mPostalCode;
  
  private String mPremises;
  
  private String mSubAdminArea;
  
  private String mSubLocality;
  
  private String mSubThoroughfare;
  
  private String mThoroughfare;
  
  private String mUrl;
  
  public int describeContents() {
    boolean bool;
    Bundle bundle = this.mExtras;
    if (bundle != null) {
      bool = bundle.describeContents();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mLocale.getLanguage());
    paramParcel.writeString(this.mLocale.getCountry());
    HashMap<Integer, String> hashMap = this.mAddressLines;
    if (hashMap == null) {
      paramParcel.writeInt(0);
    } else {
      Set<Map.Entry<Integer, String>> set = hashMap.entrySet();
      paramParcel.writeInt(set.size());
      for (Map.Entry<Integer, String> entry : set) {
        paramParcel.writeInt(((Integer)entry.getKey()).intValue());
        paramParcel.writeString((String)entry.getValue());
      } 
    } 
    paramParcel.writeString(this.mFeatureName);
    paramParcel.writeString(this.mAdminArea);
    paramParcel.writeString(this.mSubAdminArea);
    paramParcel.writeString(this.mLocality);
    paramParcel.writeString(this.mSubLocality);
    paramParcel.writeString(this.mThoroughfare);
    paramParcel.writeString(this.mSubThoroughfare);
    paramParcel.writeString(this.mPremises);
    paramParcel.writeString(this.mPostalCode);
    paramParcel.writeString(this.mCountryCode);
    paramParcel.writeString(this.mCountryName);
    paramParcel.writeInt(this.mHasLatitude);
    if (this.mHasLatitude)
      paramParcel.writeDouble(this.mLatitude); 
    paramParcel.writeInt(this.mHasLongitude);
    if (this.mHasLongitude)
      paramParcel.writeDouble(this.mLongitude); 
    paramParcel.writeString(this.mPhone);
    paramParcel.writeString(this.mUrl);
    paramParcel.writeBundle(this.mExtras);
  }
}
