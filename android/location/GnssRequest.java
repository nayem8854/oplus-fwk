package android.location;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class GnssRequest implements Parcelable {
  private GnssRequest(boolean paramBoolean) {
    this.mFullTracking = paramBoolean;
  }
  
  public boolean isFullTracking() {
    return this.mFullTracking;
  }
  
  public static final Parcelable.Creator<GnssRequest> CREATOR = new Parcelable.Creator<GnssRequest>() {
      public GnssRequest createFromParcel(Parcel param1Parcel) {
        return new GnssRequest(param1Parcel.readBoolean());
      }
      
      public GnssRequest[] newArray(int param1Int) {
        return new GnssRequest[param1Int];
      }
    };
  
  private final boolean mFullTracking;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("GnssRequest[");
    stringBuilder.append("FullTracking=");
    stringBuilder.append(this.mFullTracking);
    stringBuilder.append(']');
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (!(paramObject instanceof GnssRequest))
      return false; 
    paramObject = paramObject;
    if (this.mFullTracking != ((GnssRequest)paramObject).mFullTracking)
      return false; 
    return true;
  }
  
  public int hashCode() {
    return this.mFullTracking;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeBoolean(this.mFullTracking);
  }
  
  class Builder {
    private boolean mFullTracking;
    
    public Builder() {}
    
    public Builder(GnssRequest this$0) {
      this.mFullTracking = this$0.isFullTracking();
    }
    
    public Builder setFullTracking(boolean param1Boolean) {
      this.mFullTracking = param1Boolean;
      return this;
    }
    
    public GnssRequest build() {
      return new GnssRequest(this.mFullTracking);
    }
  }
}
