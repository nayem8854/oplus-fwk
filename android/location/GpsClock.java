package android.location;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
@Deprecated
public class GpsClock implements Parcelable {
  GpsClock() {
    initialize();
  }
  
  public void set(GpsClock paramGpsClock) {
    this.mFlags = paramGpsClock.mFlags;
    this.mLeapSecond = paramGpsClock.mLeapSecond;
    this.mType = paramGpsClock.mType;
    this.mTimeInNs = paramGpsClock.mTimeInNs;
    this.mTimeUncertaintyInNs = paramGpsClock.mTimeUncertaintyInNs;
    this.mFullBiasInNs = paramGpsClock.mFullBiasInNs;
    this.mBiasInNs = paramGpsClock.mBiasInNs;
    this.mBiasUncertaintyInNs = paramGpsClock.mBiasUncertaintyInNs;
    this.mDriftInNsPerSec = paramGpsClock.mDriftInNsPerSec;
    this.mDriftUncertaintyInNsPerSec = paramGpsClock.mDriftUncertaintyInNsPerSec;
  }
  
  public void reset() {
    initialize();
  }
  
  public byte getType() {
    return this.mType;
  }
  
  public void setType(byte paramByte) {
    this.mType = paramByte;
  }
  
  private String getTypeString() {
    byte b = this.mType;
    if (b != 0) {
      if (b != 1) {
        if (b != 2) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("<Invalid:");
          stringBuilder.append(this.mType);
          stringBuilder.append(">");
          return stringBuilder.toString();
        } 
        return "GpsTime";
      } 
      return "LocalHwClock";
    } 
    return "Unknown";
  }
  
  public boolean hasLeapSecond() {
    return isFlagSet((short)1);
  }
  
  public short getLeapSecond() {
    return this.mLeapSecond;
  }
  
  public void setLeapSecond(short paramShort) {
    setFlag((short)1);
    this.mLeapSecond = paramShort;
  }
  
  public void resetLeapSecond() {
    resetFlag((short)1);
    this.mLeapSecond = Short.MIN_VALUE;
  }
  
  public long getTimeInNs() {
    return this.mTimeInNs;
  }
  
  public void setTimeInNs(long paramLong) {
    this.mTimeInNs = paramLong;
  }
  
  public boolean hasTimeUncertaintyInNs() {
    return isFlagSet((short)2);
  }
  
  public double getTimeUncertaintyInNs() {
    return this.mTimeUncertaintyInNs;
  }
  
  public void setTimeUncertaintyInNs(double paramDouble) {
    setFlag((short)2);
    this.mTimeUncertaintyInNs = paramDouble;
  }
  
  public void resetTimeUncertaintyInNs() {
    resetFlag((short)2);
    this.mTimeUncertaintyInNs = Double.NaN;
  }
  
  public boolean hasFullBiasInNs() {
    return isFlagSet((short)4);
  }
  
  public long getFullBiasInNs() {
    return this.mFullBiasInNs;
  }
  
  public void setFullBiasInNs(long paramLong) {
    setFlag((short)4);
    this.mFullBiasInNs = paramLong;
  }
  
  public void resetFullBiasInNs() {
    resetFlag((short)4);
    this.mFullBiasInNs = Long.MIN_VALUE;
  }
  
  public boolean hasBiasInNs() {
    return isFlagSet((short)8);
  }
  
  public double getBiasInNs() {
    return this.mBiasInNs;
  }
  
  public void setBiasInNs(double paramDouble) {
    setFlag((short)8);
    this.mBiasInNs = paramDouble;
  }
  
  public void resetBiasInNs() {
    resetFlag((short)8);
    this.mBiasInNs = Double.NaN;
  }
  
  public boolean hasBiasUncertaintyInNs() {
    return isFlagSet((short)16);
  }
  
  public double getBiasUncertaintyInNs() {
    return this.mBiasUncertaintyInNs;
  }
  
  public void setBiasUncertaintyInNs(double paramDouble) {
    setFlag((short)16);
    this.mBiasUncertaintyInNs = paramDouble;
  }
  
  public void resetBiasUncertaintyInNs() {
    resetFlag((short)16);
    this.mBiasUncertaintyInNs = Double.NaN;
  }
  
  public boolean hasDriftInNsPerSec() {
    return isFlagSet((short)32);
  }
  
  public double getDriftInNsPerSec() {
    return this.mDriftInNsPerSec;
  }
  
  public void setDriftInNsPerSec(double paramDouble) {
    setFlag((short)32);
    this.mDriftInNsPerSec = paramDouble;
  }
  
  public void resetDriftInNsPerSec() {
    resetFlag((short)32);
    this.mDriftInNsPerSec = Double.NaN;
  }
  
  public boolean hasDriftUncertaintyInNsPerSec() {
    return isFlagSet((short)64);
  }
  
  public double getDriftUncertaintyInNsPerSec() {
    return this.mDriftUncertaintyInNsPerSec;
  }
  
  public void setDriftUncertaintyInNsPerSec(double paramDouble) {
    setFlag((short)64);
    this.mDriftUncertaintyInNsPerSec = paramDouble;
  }
  
  public void resetDriftUncertaintyInNsPerSec() {
    resetFlag((short)64);
    this.mDriftUncertaintyInNsPerSec = Double.NaN;
  }
  
  public static final Parcelable.Creator<GpsClock> CREATOR = new Parcelable.Creator<GpsClock>() {
      public GpsClock createFromParcel(Parcel param1Parcel) {
        GpsClock gpsClock = new GpsClock();
        GpsClock.access$002(gpsClock, (short)param1Parcel.readInt());
        GpsClock.access$102(gpsClock, (short)param1Parcel.readInt());
        GpsClock.access$202(gpsClock, param1Parcel.readByte());
        GpsClock.access$302(gpsClock, param1Parcel.readLong());
        GpsClock.access$402(gpsClock, param1Parcel.readDouble());
        GpsClock.access$502(gpsClock, param1Parcel.readLong());
        GpsClock.access$602(gpsClock, param1Parcel.readDouble());
        GpsClock.access$702(gpsClock, param1Parcel.readDouble());
        GpsClock.access$802(gpsClock, param1Parcel.readDouble());
        GpsClock.access$902(gpsClock, param1Parcel.readDouble());
        return gpsClock;
      }
      
      public GpsClock[] newArray(int param1Int) {
        return new GpsClock[param1Int];
      }
    };
  
  private static final short HAS_BIAS = 8;
  
  private static final short HAS_BIAS_UNCERTAINTY = 16;
  
  private static final short HAS_DRIFT = 32;
  
  private static final short HAS_DRIFT_UNCERTAINTY = 64;
  
  private static final short HAS_FULL_BIAS = 4;
  
  private static final short HAS_LEAP_SECOND = 1;
  
  private static final short HAS_NO_FLAGS = 0;
  
  private static final short HAS_TIME_UNCERTAINTY = 2;
  
  public static final byte TYPE_GPS_TIME = 2;
  
  public static final byte TYPE_LOCAL_HW_TIME = 1;
  
  public static final byte TYPE_UNKNOWN = 0;
  
  private double mBiasInNs;
  
  private double mBiasUncertaintyInNs;
  
  private double mDriftInNsPerSec;
  
  private double mDriftUncertaintyInNsPerSec;
  
  private short mFlags;
  
  private long mFullBiasInNs;
  
  private short mLeapSecond;
  
  private long mTimeInNs;
  
  private double mTimeUncertaintyInNs;
  
  private byte mType;
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mFlags);
    paramParcel.writeInt(this.mLeapSecond);
    paramParcel.writeByte(this.mType);
    paramParcel.writeLong(this.mTimeInNs);
    paramParcel.writeDouble(this.mTimeUncertaintyInNs);
    paramParcel.writeLong(this.mFullBiasInNs);
    paramParcel.writeDouble(this.mBiasInNs);
    paramParcel.writeDouble(this.mBiasUncertaintyInNs);
    paramParcel.writeDouble(this.mDriftInNsPerSec);
    paramParcel.writeDouble(this.mDriftUncertaintyInNsPerSec);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    Double double_2;
    StringBuilder stringBuilder = new StringBuilder("GpsClock:\n");
    stringBuilder.append(String.format("   %-15s = %s\n", new Object[] { "Type", getTypeString() }));
    boolean bool = hasLeapSecond();
    Double double_1 = null;
    if (bool) {
      double_2 = (Double)Short.valueOf(this.mLeapSecond);
    } else {
      double_2 = null;
    } 
    stringBuilder.append(String.format("   %-15s = %s\n", new Object[] { "LeapSecond", double_2 }));
    long l = this.mTimeInNs;
    if (hasTimeUncertaintyInNs()) {
      double_2 = Double.valueOf(this.mTimeUncertaintyInNs);
    } else {
      double_2 = null;
    } 
    stringBuilder.append(String.format("   %-15s = %-25s   %-26s = %s\n", new Object[] { "TimeInNs", Long.valueOf(l), "TimeUncertaintyInNs", double_2 }));
    if (hasFullBiasInNs()) {
      Long long_ = Long.valueOf(this.mFullBiasInNs);
    } else {
      double_2 = null;
    } 
    stringBuilder.append(String.format("   %-15s = %s\n", new Object[] { "FullBiasInNs", double_2 }));
    if (hasBiasInNs()) {
      double_2 = Double.valueOf(this.mBiasInNs);
    } else {
      double_2 = null;
    } 
    if (hasBiasUncertaintyInNs()) {
      double_3 = Double.valueOf(this.mBiasUncertaintyInNs);
    } else {
      double_3 = null;
    } 
    stringBuilder.append(String.format("   %-15s = %-25s   %-26s = %s\n", new Object[] { "BiasInNs", double_2, "BiasUncertaintyInNs", double_3 }));
    if (hasDriftInNsPerSec()) {
      double_2 = Double.valueOf(this.mDriftInNsPerSec);
    } else {
      double_2 = null;
    } 
    Double double_3 = double_1;
    if (hasDriftUncertaintyInNsPerSec())
      double_3 = Double.valueOf(this.mDriftUncertaintyInNsPerSec); 
    stringBuilder.append(String.format("   %-15s = %-25s   %-26s = %s\n", new Object[] { "DriftInNsPerSec", double_2, "DriftUncertaintyInNsPerSec", double_3 }));
    return stringBuilder.toString();
  }
  
  private void initialize() {
    this.mFlags = 0;
    resetLeapSecond();
    setType((byte)0);
    setTimeInNs(Long.MIN_VALUE);
    resetTimeUncertaintyInNs();
    resetFullBiasInNs();
    resetBiasInNs();
    resetBiasUncertaintyInNs();
    resetDriftInNsPerSec();
    resetDriftUncertaintyInNsPerSec();
  }
  
  private void setFlag(short paramShort) {
    this.mFlags = (short)(this.mFlags | paramShort);
  }
  
  private void resetFlag(short paramShort) {
    this.mFlags = (short)(this.mFlags & (paramShort ^ 0xFFFFFFFF));
  }
  
  private boolean isFlagSet(short paramShort) {
    boolean bool;
    if ((this.mFlags & paramShort) == paramShort) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
