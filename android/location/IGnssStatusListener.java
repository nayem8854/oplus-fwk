package android.location;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IGnssStatusListener extends IInterface {
  void onFirstFix(int paramInt) throws RemoteException;
  
  void onGnssStarted() throws RemoteException;
  
  void onGnssStopped() throws RemoteException;
  
  void onNmeaReceived(long paramLong, String paramString) throws RemoteException;
  
  void onSvStatusChanged(int paramInt, int[] paramArrayOfint, float[] paramArrayOffloat1, float[] paramArrayOffloat2, float[] paramArrayOffloat3, float[] paramArrayOffloat4, float[] paramArrayOffloat5) throws RemoteException;
  
  class Default implements IGnssStatusListener {
    public void onGnssStarted() throws RemoteException {}
    
    public void onGnssStopped() throws RemoteException {}
    
    public void onFirstFix(int param1Int) throws RemoteException {}
    
    public void onSvStatusChanged(int param1Int, int[] param1ArrayOfint, float[] param1ArrayOffloat1, float[] param1ArrayOffloat2, float[] param1ArrayOffloat3, float[] param1ArrayOffloat4, float[] param1ArrayOffloat5) throws RemoteException {}
    
    public void onNmeaReceived(long param1Long, String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IGnssStatusListener {
    private static final String DESCRIPTOR = "android.location.IGnssStatusListener";
    
    static final int TRANSACTION_onFirstFix = 3;
    
    static final int TRANSACTION_onGnssStarted = 1;
    
    static final int TRANSACTION_onGnssStopped = 2;
    
    static final int TRANSACTION_onNmeaReceived = 5;
    
    static final int TRANSACTION_onSvStatusChanged = 4;
    
    public Stub() {
      attachInterface(this, "android.location.IGnssStatusListener");
    }
    
    public static IGnssStatusListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.location.IGnssStatusListener");
      if (iInterface != null && iInterface instanceof IGnssStatusListener)
        return (IGnssStatusListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "onNmeaReceived";
            } 
            return "onSvStatusChanged";
          } 
          return "onFirstFix";
        } 
        return "onGnssStopped";
      } 
      return "onGnssStarted";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      float[] arrayOfFloat;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            String str;
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.location.IGnssStatusListener");
                return true;
              } 
              param1Parcel1.enforceInterface("android.location.IGnssStatusListener");
              long l = param1Parcel1.readLong();
              str = param1Parcel1.readString();
              onNmeaReceived(l, str);
              return true;
            } 
            str.enforceInterface("android.location.IGnssStatusListener");
            param1Int1 = str.readInt();
            int[] arrayOfInt = str.createIntArray();
            float[] arrayOfFloat2 = str.createFloatArray();
            float[] arrayOfFloat3 = str.createFloatArray();
            float[] arrayOfFloat4 = str.createFloatArray();
            float[] arrayOfFloat1 = str.createFloatArray();
            arrayOfFloat = str.createFloatArray();
            onSvStatusChanged(param1Int1, arrayOfInt, arrayOfFloat2, arrayOfFloat3, arrayOfFloat4, arrayOfFloat1, arrayOfFloat);
            return true;
          } 
          arrayOfFloat.enforceInterface("android.location.IGnssStatusListener");
          param1Int1 = arrayOfFloat.readInt();
          onFirstFix(param1Int1);
          return true;
        } 
        arrayOfFloat.enforceInterface("android.location.IGnssStatusListener");
        onGnssStopped();
        return true;
      } 
      arrayOfFloat.enforceInterface("android.location.IGnssStatusListener");
      onGnssStarted();
      return true;
    }
    
    private static class Proxy implements IGnssStatusListener {
      public static IGnssStatusListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.location.IGnssStatusListener";
      }
      
      public void onGnssStarted() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.location.IGnssStatusListener");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IGnssStatusListener.Stub.getDefaultImpl() != null) {
            IGnssStatusListener.Stub.getDefaultImpl().onGnssStarted();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onGnssStopped() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.location.IGnssStatusListener");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IGnssStatusListener.Stub.getDefaultImpl() != null) {
            IGnssStatusListener.Stub.getDefaultImpl().onGnssStopped();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onFirstFix(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.location.IGnssStatusListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IGnssStatusListener.Stub.getDefaultImpl() != null) {
            IGnssStatusListener.Stub.getDefaultImpl().onFirstFix(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSvStatusChanged(int param2Int, int[] param2ArrayOfint, float[] param2ArrayOffloat1, float[] param2ArrayOffloat2, float[] param2ArrayOffloat3, float[] param2ArrayOffloat4, float[] param2ArrayOffloat5) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.location.IGnssStatusListener");
          try {
            parcel.writeInt(param2Int);
            try {
              parcel.writeIntArray(param2ArrayOfint);
              try {
                parcel.writeFloatArray(param2ArrayOffloat1);
                try {
                  parcel.writeFloatArray(param2ArrayOffloat2);
                  try {
                    parcel.writeFloatArray(param2ArrayOffloat3);
                    try {
                      parcel.writeFloatArray(param2ArrayOffloat4);
                      parcel.writeFloatArray(param2ArrayOffloat5);
                      boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
                      if (!bool && IGnssStatusListener.Stub.getDefaultImpl() != null) {
                        IGnssStatusListener.Stub.getDefaultImpl().onSvStatusChanged(param2Int, param2ArrayOfint, param2ArrayOffloat1, param2ArrayOffloat2, param2ArrayOffloat3, param2ArrayOffloat4, param2ArrayOffloat5);
                        parcel.recycle();
                        return;
                      } 
                      parcel.recycle();
                      return;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2ArrayOfint;
      }
      
      public void onNmeaReceived(long param2Long, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.location.IGnssStatusListener");
          parcel.writeLong(param2Long);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IGnssStatusListener.Stub.getDefaultImpl() != null) {
            IGnssStatusListener.Stub.getDefaultImpl().onNmeaReceived(param2Long, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IGnssStatusListener param1IGnssStatusListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IGnssStatusListener != null) {
          Proxy.sDefaultImpl = param1IGnssStatusListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IGnssStatusListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
