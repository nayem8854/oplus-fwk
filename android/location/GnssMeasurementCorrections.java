package android.location;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SystemApi
public final class GnssMeasurementCorrections implements Parcelable {
  private GnssMeasurementCorrections(Builder paramBuilder) {
    boolean bool2;
    this.mLatitudeDegrees = paramBuilder.mLatitudeDegrees;
    this.mLongitudeDegrees = paramBuilder.mLongitudeDegrees;
    this.mAltitudeMeters = paramBuilder.mAltitudeMeters;
    this.mHorizontalPositionUncertaintyMeters = paramBuilder.mHorizontalPositionUncertaintyMeters;
    this.mVerticalPositionUncertaintyMeters = paramBuilder.mVerticalPositionUncertaintyMeters;
    this.mToaGpsNanosecondsOfWeek = paramBuilder.mToaGpsNanosecondsOfWeek;
    List<? extends GnssSingleSatCorrection> list = paramBuilder.mSingleSatCorrectionList;
    boolean bool1 = true;
    if (list != null && !list.isEmpty()) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2);
    this.mSingleSatCorrectionList = Collections.unmodifiableList(new ArrayList<>(list));
    if (paramBuilder.mEnvironmentBearingIsSet && 
      paramBuilder.mEnvironmentBearingUncertaintyIsSet) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.mHasEnvironmentBearing = bool2;
    this.mEnvironmentBearingDegrees = paramBuilder.mEnvironmentBearingDegrees;
    this.mEnvironmentBearingUncertaintyDegrees = paramBuilder.mEnvironmentBearingUncertaintyDegrees;
  }
  
  public double getLatitudeDegrees() {
    return this.mLatitudeDegrees;
  }
  
  public double getLongitudeDegrees() {
    return this.mLongitudeDegrees;
  }
  
  public double getAltitudeMeters() {
    return this.mAltitudeMeters;
  }
  
  public double getHorizontalPositionUncertaintyMeters() {
    return this.mHorizontalPositionUncertaintyMeters;
  }
  
  public double getVerticalPositionUncertaintyMeters() {
    return this.mVerticalPositionUncertaintyMeters;
  }
  
  public long getToaGpsNanosecondsOfWeek() {
    return this.mToaGpsNanosecondsOfWeek;
  }
  
  public List<GnssSingleSatCorrection> getSingleSatelliteCorrectionList() {
    return this.mSingleSatCorrectionList;
  }
  
  public boolean hasEnvironmentBearing() {
    return this.mHasEnvironmentBearing;
  }
  
  public float getEnvironmentBearingDegrees() {
    return this.mEnvironmentBearingDegrees;
  }
  
  public float getEnvironmentBearingUncertaintyDegrees() {
    return this.mEnvironmentBearingUncertaintyDegrees;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<GnssMeasurementCorrections> CREATOR = new Parcelable.Creator<GnssMeasurementCorrections>() {
      public GnssMeasurementCorrections createFromParcel(Parcel param1Parcel) {
        GnssMeasurementCorrections.Builder builder = new GnssMeasurementCorrections.Builder();
        builder = builder.setLatitudeDegrees(param1Parcel.readDouble());
        builder = builder.setLongitudeDegrees(param1Parcel.readDouble());
        builder = builder.setAltitudeMeters(param1Parcel.readDouble());
        builder = builder.setHorizontalPositionUncertaintyMeters(param1Parcel.readDouble());
        builder = builder.setVerticalPositionUncertaintyMeters(param1Parcel.readDouble());
        builder = builder.setToaGpsNanosecondsOfWeek(param1Parcel.readLong());
        ArrayList<GnssSingleSatCorrection> arrayList = new ArrayList();
        param1Parcel.readTypedList(arrayList, GnssSingleSatCorrection.CREATOR);
        builder.setSingleSatelliteCorrectionList(arrayList);
        boolean bool = param1Parcel.readBoolean();
        if (bool) {
          builder.setEnvironmentBearingDegrees(param1Parcel.readFloat());
          float f = param1Parcel.readFloat();
          builder.setEnvironmentBearingUncertaintyDegrees(f);
        } 
        return builder.build();
      }
      
      public GnssMeasurementCorrections[] newArray(int param1Int) {
        return new GnssMeasurementCorrections[param1Int];
      }
    };
  
  private final double mAltitudeMeters;
  
  private final float mEnvironmentBearingDegrees;
  
  private final float mEnvironmentBearingUncertaintyDegrees;
  
  private final boolean mHasEnvironmentBearing;
  
  private final double mHorizontalPositionUncertaintyMeters;
  
  private final double mLatitudeDegrees;
  
  private final double mLongitudeDegrees;
  
  private final List<GnssSingleSatCorrection> mSingleSatCorrectionList;
  
  private final long mToaGpsNanosecondsOfWeek;
  
  private final double mVerticalPositionUncertaintyMeters;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("GnssMeasurementCorrections:\n");
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "LatitudeDegrees = ", Double.valueOf(this.mLatitudeDegrees) }));
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "LongitudeDegrees = ", Double.valueOf(this.mLongitudeDegrees) }));
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "AltitudeMeters = ", Double.valueOf(this.mAltitudeMeters) }));
    double d = this.mHorizontalPositionUncertaintyMeters;
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "HorizontalPositionUncertaintyMeters = ", Double.valueOf(d) }));
    d = this.mVerticalPositionUncertaintyMeters;
    stringBuilder.append(String.format("   %-29s = %s\n", new Object[] { "VerticalPositionUncertaintyMeters = ", Double.valueOf(d) }));
    long l = this.mToaGpsNanosecondsOfWeek;
    String str2 = String.format("   %-29s = %s\n", new Object[] { "ToaGpsNanosecondsOfWeek = ", Long.valueOf(l) });
    stringBuilder.append(str2);
    List<GnssSingleSatCorrection> list = this.mSingleSatCorrectionList;
    String str1 = String.format("   %-29s = %s\n", new Object[] { "mSingleSatCorrectionList = ", list });
    stringBuilder.append(str1);
    boolean bool = this.mHasEnvironmentBearing;
    str1 = String.format("   %-29s = %s\n", new Object[] { "HasEnvironmentBearing = ", Boolean.valueOf(bool) });
    stringBuilder.append(str1);
    float f = this.mEnvironmentBearingDegrees;
    str1 = String.format("   %-29s = %s\n", new Object[] { "EnvironmentBearingDegrees = ", Float.valueOf(f) });
    stringBuilder.append(str1);
    f = this.mEnvironmentBearingUncertaintyDegrees;
    str1 = String.format("   %-29s = %s\n", new Object[] { "EnvironmentBearingUncertaintyDegrees = ", Float.valueOf(f) });
    stringBuilder.append(str1);
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeDouble(this.mLatitudeDegrees);
    paramParcel.writeDouble(this.mLongitudeDegrees);
    paramParcel.writeDouble(this.mAltitudeMeters);
    paramParcel.writeDouble(this.mHorizontalPositionUncertaintyMeters);
    paramParcel.writeDouble(this.mVerticalPositionUncertaintyMeters);
    paramParcel.writeLong(this.mToaGpsNanosecondsOfWeek);
    paramParcel.writeTypedList(this.mSingleSatCorrectionList);
    paramParcel.writeBoolean(this.mHasEnvironmentBearing);
    if (this.mHasEnvironmentBearing) {
      paramParcel.writeFloat(this.mEnvironmentBearingDegrees);
      paramParcel.writeFloat(this.mEnvironmentBearingUncertaintyDegrees);
    } 
  }
  
  class Builder {
    private double mAltitudeMeters;
    
    private float mEnvironmentBearingDegrees;
    
    private boolean mEnvironmentBearingIsSet;
    
    private float mEnvironmentBearingUncertaintyDegrees;
    
    private boolean mEnvironmentBearingUncertaintyIsSet;
    
    private double mHorizontalPositionUncertaintyMeters;
    
    private double mLatitudeDegrees;
    
    private double mLongitudeDegrees;
    
    private List<GnssSingleSatCorrection> mSingleSatCorrectionList;
    
    private long mToaGpsNanosecondsOfWeek;
    
    private double mVerticalPositionUncertaintyMeters;
    
    public Builder() {
      this.mEnvironmentBearingIsSet = false;
      this.mEnvironmentBearingUncertaintyIsSet = false;
    }
    
    public Builder setLatitudeDegrees(double param1Double) {
      this.mLatitudeDegrees = param1Double;
      return this;
    }
    
    public Builder setLongitudeDegrees(double param1Double) {
      this.mLongitudeDegrees = param1Double;
      return this;
    }
    
    public Builder setAltitudeMeters(double param1Double) {
      this.mAltitudeMeters = param1Double;
      return this;
    }
    
    public Builder setHorizontalPositionUncertaintyMeters(double param1Double) {
      this.mHorizontalPositionUncertaintyMeters = param1Double;
      return this;
    }
    
    public Builder setVerticalPositionUncertaintyMeters(double param1Double) {
      this.mVerticalPositionUncertaintyMeters = param1Double;
      return this;
    }
    
    public Builder setToaGpsNanosecondsOfWeek(long param1Long) {
      this.mToaGpsNanosecondsOfWeek = param1Long;
      return this;
    }
    
    public Builder setSingleSatelliteCorrectionList(List<GnssSingleSatCorrection> param1List) {
      this.mSingleSatCorrectionList = param1List;
      return this;
    }
    
    public Builder setEnvironmentBearingDegrees(float param1Float) {
      this.mEnvironmentBearingDegrees = param1Float;
      this.mEnvironmentBearingIsSet = true;
      return this;
    }
    
    public Builder setEnvironmentBearingUncertaintyDegrees(float param1Float) {
      this.mEnvironmentBearingUncertaintyDegrees = param1Float;
      this.mEnvironmentBearingUncertaintyIsSet = true;
      return this;
    }
    
    public GnssMeasurementCorrections build() {
      if ((this.mEnvironmentBearingIsSet ^ this.mEnvironmentBearingUncertaintyIsSet) == 0)
        return new GnssMeasurementCorrections(this); 
      throw new IllegalStateException("Both environment bearing and environment bearing uncertainty must be set.");
    }
  }
}
