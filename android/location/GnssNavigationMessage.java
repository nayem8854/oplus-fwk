package android.location;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.security.InvalidParameterException;

public final class GnssNavigationMessage implements Parcelable {
  public static final Parcelable.Creator<GnssNavigationMessage> CREATOR;
  
  private static final byte[] EMPTY_ARRAY = new byte[0];
  
  public static final int STATUS_PARITY_PASSED = 1;
  
  public static final int STATUS_PARITY_REBUILT = 2;
  
  public static final int STATUS_UNKNOWN = 0;
  
  public static final int TYPE_BDS_CNAV1 = 1283;
  
  public static final int TYPE_BDS_CNAV2 = 1284;
  
  public static final int TYPE_BDS_D1 = 1281;
  
  public static final int TYPE_BDS_D2 = 1282;
  
  public static final int TYPE_GAL_F = 1538;
  
  public static final int TYPE_GAL_I = 1537;
  
  public static final int TYPE_GLO_L1CA = 769;
  
  public static final int TYPE_GPS_CNAV2 = 260;
  
  public static final int TYPE_GPS_L1CA = 257;
  
  public static final int TYPE_GPS_L2CNAV = 258;
  
  public static final int TYPE_GPS_L5CNAV = 259;
  
  public static final int TYPE_IRN_L5CA = 1793;
  
  public static final int TYPE_QZS_L1CA = 1025;
  
  public static final int TYPE_SBS = 513;
  
  public static final int TYPE_UNKNOWN = 0;
  
  private byte[] mData;
  
  private int mMessageId;
  
  private int mStatus;
  
  private int mSubmessageId;
  
  private int mSvid;
  
  private int mType;
  
  @Retention(RetentionPolicy.SOURCE)
  class GnssNavigationMessageType implements Annotation {}
  
  class Callback {
    public static final int STATUS_LOCATION_DISABLED = 2;
    
    public static final int STATUS_NOT_SUPPORTED = 0;
    
    public static final int STATUS_READY = 1;
    
    public void onGnssNavigationMessageReceived(GnssNavigationMessage param1GnssNavigationMessage) {}
    
    public void onStatusChanged(int param1Int) {}
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface GnssNavigationMessageStatus {}
  }
  
  public GnssNavigationMessage() {
    initialize();
  }
  
  public void set(GnssNavigationMessage paramGnssNavigationMessage) {
    this.mType = paramGnssNavigationMessage.mType;
    this.mSvid = paramGnssNavigationMessage.mSvid;
    this.mMessageId = paramGnssNavigationMessage.mMessageId;
    this.mSubmessageId = paramGnssNavigationMessage.mSubmessageId;
    this.mData = paramGnssNavigationMessage.mData;
    this.mStatus = paramGnssNavigationMessage.mStatus;
  }
  
  public void reset() {
    initialize();
  }
  
  public int getType() {
    return this.mType;
  }
  
  public void setType(int paramInt) {
    this.mType = paramInt;
  }
  
  private String getTypeString() {
    int i = this.mType;
    if (i != 0) {
      if (i != 513) {
        if (i != 769) {
          if (i != 1025) {
            if (i != 1793) {
              if (i != 1537) {
                if (i != 1538) {
                  StringBuilder stringBuilder;
                  switch (i) {
                    default:
                      switch (i) {
                        default:
                          stringBuilder = new StringBuilder();
                          stringBuilder.append("<Invalid:");
                          stringBuilder.append(this.mType);
                          stringBuilder.append(">");
                          return stringBuilder.toString();
                        case 1284:
                          return "Beidou CNAV2";
                        case 1283:
                          return "Beidou CNAV1";
                        case 1282:
                          return "Beidou D2";
                        case 1281:
                          break;
                      } 
                      return "Beidou D1";
                    case 260:
                      return "GPS CNAV2";
                    case 259:
                      return "GPS L5-CNAV";
                    case 258:
                      return "GPS L2-CNAV";
                    case 257:
                      break;
                  } 
                  return "GPS L1 C/A";
                } 
                return "Galileo F";
              } 
              return "Galileo I";
            } 
            return "IRNSS L5 C/A";
          } 
          return "QZSS L1 C/A";
        } 
        return "Glonass L1 C/A";
      } 
      return "SBS";
    } 
    return "Unknown";
  }
  
  public int getSvid() {
    return this.mSvid;
  }
  
  public void setSvid(int paramInt) {
    this.mSvid = paramInt;
  }
  
  public int getMessageId() {
    return this.mMessageId;
  }
  
  public void setMessageId(int paramInt) {
    this.mMessageId = paramInt;
  }
  
  public int getSubmessageId() {
    return this.mSubmessageId;
  }
  
  public void setSubmessageId(int paramInt) {
    this.mSubmessageId = paramInt;
  }
  
  public byte[] getData() {
    return this.mData;
  }
  
  public void setData(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null) {
      this.mData = paramArrayOfbyte;
      return;
    } 
    throw new InvalidParameterException("Data must be a non-null array");
  }
  
  public int getStatus() {
    return this.mStatus;
  }
  
  public void setStatus(int paramInt) {
    this.mStatus = paramInt;
  }
  
  private String getStatusString() {
    int i = this.mStatus;
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("<Invalid:");
          stringBuilder.append(this.mStatus);
          stringBuilder.append(">");
          return stringBuilder.toString();
        } 
        return "ParityRebuilt";
      } 
      return "ParityPassed";
    } 
    return "Unknown";
  }
  
  static {
    CREATOR = new Parcelable.Creator<GnssNavigationMessage>() {
        public GnssNavigationMessage createFromParcel(Parcel param1Parcel) {
          GnssNavigationMessage gnssNavigationMessage = new GnssNavigationMessage();
          gnssNavigationMessage.setType(param1Parcel.readInt());
          gnssNavigationMessage.setSvid(param1Parcel.readInt());
          gnssNavigationMessage.setMessageId(param1Parcel.readInt());
          gnssNavigationMessage.setSubmessageId(param1Parcel.readInt());
          int i = param1Parcel.readInt();
          byte[] arrayOfByte = new byte[i];
          param1Parcel.readByteArray(arrayOfByte);
          gnssNavigationMessage.setData(arrayOfByte);
          gnssNavigationMessage.setStatus(param1Parcel.readInt());
          return gnssNavigationMessage;
        }
        
        public GnssNavigationMessage[] newArray(int param1Int) {
          return new GnssNavigationMessage[param1Int];
        }
      };
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mType);
    paramParcel.writeInt(this.mSvid);
    paramParcel.writeInt(this.mMessageId);
    paramParcel.writeInt(this.mSubmessageId);
    paramParcel.writeInt(this.mData.length);
    paramParcel.writeByteArray(this.mData);
    paramParcel.writeInt(this.mStatus);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("GnssNavigationMessage:\n");
    byte b = 0;
    stringBuilder.append(String.format("   %-15s = %s\n", new Object[] { "Type", getTypeString() }));
    stringBuilder.append(String.format("   %-15s = %s\n", new Object[] { "Svid", Integer.valueOf(this.mSvid) }));
    stringBuilder.append(String.format("   %-15s = %s\n", new Object[] { "Status", getStatusString() }));
    stringBuilder.append(String.format("   %-15s = %s\n", new Object[] { "MessageId", Integer.valueOf(this.mMessageId) }));
    stringBuilder.append(String.format("   %-15s = %s\n", new Object[] { "SubmessageId", Integer.valueOf(this.mSubmessageId) }));
    stringBuilder.append(String.format("   %-15s = %s\n", new Object[] { "Data", "{" }));
    String str = "        ";
    byte[] arrayOfByte;
    int i;
    for (arrayOfByte = this.mData, i = arrayOfByte.length; b < i; ) {
      byte b1 = arrayOfByte[b];
      stringBuilder.append(str);
      stringBuilder.append(b1);
      str = ", ";
      b++;
    } 
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  private void initialize() {
    this.mType = 0;
    this.mSvid = 0;
    this.mMessageId = -1;
    this.mSubmessageId = -1;
    this.mData = EMPTY_ARRAY;
    this.mStatus = 0;
  }
}
