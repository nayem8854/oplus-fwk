package android.location;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ICountryDetector extends IInterface {
  void addCountryListener(ICountryListener paramICountryListener) throws RemoteException;
  
  Country detectCountry() throws RemoteException;
  
  void removeCountryListener(ICountryListener paramICountryListener) throws RemoteException;
  
  class Default implements ICountryDetector {
    public Country detectCountry() throws RemoteException {
      return null;
    }
    
    public void addCountryListener(ICountryListener param1ICountryListener) throws RemoteException {}
    
    public void removeCountryListener(ICountryListener param1ICountryListener) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICountryDetector {
    private static final String DESCRIPTOR = "android.location.ICountryDetector";
    
    static final int TRANSACTION_addCountryListener = 2;
    
    static final int TRANSACTION_detectCountry = 1;
    
    static final int TRANSACTION_removeCountryListener = 3;
    
    public Stub() {
      attachInterface(this, "android.location.ICountryDetector");
    }
    
    public static ICountryDetector asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.location.ICountryDetector");
      if (iInterface != null && iInterface instanceof ICountryDetector)
        return (ICountryDetector)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "removeCountryListener";
        } 
        return "addCountryListener";
      } 
      return "detectCountry";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ICountryListener iCountryListener;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.location.ICountryDetector");
            return true;
          } 
          param1Parcel1.enforceInterface("android.location.ICountryDetector");
          iCountryListener = ICountryListener.Stub.asInterface(param1Parcel1.readStrongBinder());
          removeCountryListener(iCountryListener);
          param1Parcel2.writeNoException();
          return true;
        } 
        iCountryListener.enforceInterface("android.location.ICountryDetector");
        iCountryListener = ICountryListener.Stub.asInterface(iCountryListener.readStrongBinder());
        addCountryListener(iCountryListener);
        param1Parcel2.writeNoException();
        return true;
      } 
      iCountryListener.enforceInterface("android.location.ICountryDetector");
      Country country = detectCountry();
      param1Parcel2.writeNoException();
      if (country != null) {
        param1Parcel2.writeInt(1);
        country.writeToParcel(param1Parcel2, 1);
      } else {
        param1Parcel2.writeInt(0);
      } 
      return true;
    }
    
    private static class Proxy implements ICountryDetector {
      public static ICountryDetector sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.location.ICountryDetector";
      }
      
      public Country detectCountry() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Country country;
          parcel1.writeInterfaceToken("android.location.ICountryDetector");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ICountryDetector.Stub.getDefaultImpl() != null) {
            country = ICountryDetector.Stub.getDefaultImpl().detectCountry();
            return country;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            country = Country.CREATOR.createFromParcel(parcel2);
          } else {
            country = null;
          } 
          return country;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addCountryListener(ICountryListener param2ICountryListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.location.ICountryDetector");
          if (param2ICountryListener != null) {
            iBinder = param2ICountryListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ICountryDetector.Stub.getDefaultImpl() != null) {
            ICountryDetector.Stub.getDefaultImpl().addCountryListener(param2ICountryListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeCountryListener(ICountryListener param2ICountryListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.location.ICountryDetector");
          if (param2ICountryListener != null) {
            iBinder = param2ICountryListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ICountryDetector.Stub.getDefaultImpl() != null) {
            ICountryDetector.Stub.getDefaultImpl().removeCountryListener(param2ICountryListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICountryDetector param1ICountryDetector) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICountryDetector != null) {
          Proxy.sDefaultImpl = param1ICountryDetector;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICountryDetector getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
