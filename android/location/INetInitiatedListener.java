package android.location;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INetInitiatedListener extends IInterface {
  boolean sendNiResponse(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements INetInitiatedListener {
    public boolean sendNiResponse(int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INetInitiatedListener {
    private static final String DESCRIPTOR = "android.location.INetInitiatedListener";
    
    static final int TRANSACTION_sendNiResponse = 1;
    
    public Stub() {
      attachInterface(this, "android.location.INetInitiatedListener");
    }
    
    public static INetInitiatedListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.location.INetInitiatedListener");
      if (iInterface != null && iInterface instanceof INetInitiatedListener)
        return (INetInitiatedListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "sendNiResponse";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.location.INetInitiatedListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.location.INetInitiatedListener");
      param1Int2 = param1Parcel1.readInt();
      param1Int1 = param1Parcel1.readInt();
      boolean bool = sendNiResponse(param1Int2, param1Int1);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements INetInitiatedListener {
      public static INetInitiatedListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.location.INetInitiatedListener";
      }
      
      public boolean sendNiResponse(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.location.INetInitiatedListener");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && INetInitiatedListener.Stub.getDefaultImpl() != null) {
            bool1 = INetInitiatedListener.Stub.getDefaultImpl().sendNiResponse(param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INetInitiatedListener param1INetInitiatedListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INetInitiatedListener != null) {
          Proxy.sDefaultImpl = param1INetInitiatedListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INetInitiatedListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
