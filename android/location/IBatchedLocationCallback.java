package android.location;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IBatchedLocationCallback extends IInterface {
  void onLocationBatch(List<Location> paramList) throws RemoteException;
  
  class Default implements IBatchedLocationCallback {
    public void onLocationBatch(List<Location> param1List) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IBatchedLocationCallback {
    private static final String DESCRIPTOR = "android.location.IBatchedLocationCallback";
    
    static final int TRANSACTION_onLocationBatch = 1;
    
    public Stub() {
      attachInterface(this, "android.location.IBatchedLocationCallback");
    }
    
    public static IBatchedLocationCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.location.IBatchedLocationCallback");
      if (iInterface != null && iInterface instanceof IBatchedLocationCallback)
        return (IBatchedLocationCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onLocationBatch";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.location.IBatchedLocationCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.location.IBatchedLocationCallback");
      ArrayList<Location> arrayList = param1Parcel1.createTypedArrayList(Location.CREATOR);
      onLocationBatch(arrayList);
      return true;
    }
    
    private static class Proxy implements IBatchedLocationCallback {
      public static IBatchedLocationCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.location.IBatchedLocationCallback";
      }
      
      public void onLocationBatch(List<Location> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.location.IBatchedLocationCallback");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IBatchedLocationCallback.Stub.getDefaultImpl() != null) {
            IBatchedLocationCallback.Stub.getDefaultImpl().onLocationBatch(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IBatchedLocationCallback param1IBatchedLocationCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IBatchedLocationCallback != null) {
          Proxy.sDefaultImpl = param1IBatchedLocationCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IBatchedLocationCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
