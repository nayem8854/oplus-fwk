package android.location;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class GnssMeasurementsEvent implements Parcelable {
  class Callback {
    public static final int STATUS_LOCATION_DISABLED = 2;
    
    public static final int STATUS_NOT_ALLOWED = 3;
    
    public static final int STATUS_NOT_SUPPORTED = 0;
    
    public static final int STATUS_READY = 1;
    
    public void onGnssMeasurementsReceived(GnssMeasurementsEvent param1GnssMeasurementsEvent) {}
    
    public void onStatusChanged(int param1Int) {}
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface GnssMeasurementsStatus {}
  }
  
  public GnssMeasurementsEvent(GnssClock paramGnssClock, GnssMeasurement[] paramArrayOfGnssMeasurement) {
    if (paramGnssClock != null) {
      if (paramArrayOfGnssMeasurement == null || paramArrayOfGnssMeasurement.length == 0) {
        this.mReadOnlyMeasurements = Collections.emptyList();
      } else {
        List<GnssMeasurement> list = Arrays.asList(paramArrayOfGnssMeasurement);
        this.mReadOnlyMeasurements = Collections.unmodifiableCollection(list);
      } 
      this.mClock = paramGnssClock;
      return;
    } 
    throw new InvalidParameterException("Parameter 'clock' must not be null.");
  }
  
  public GnssClock getClock() {
    return this.mClock;
  }
  
  public Collection<GnssMeasurement> getMeasurements() {
    return this.mReadOnlyMeasurements;
  }
  
  public static final Parcelable.Creator<GnssMeasurementsEvent> CREATOR = new Parcelable.Creator<GnssMeasurementsEvent>() {
      public GnssMeasurementsEvent createFromParcel(Parcel param1Parcel) {
        ClassLoader classLoader = getClass().getClassLoader();
        GnssClock gnssClock = param1Parcel.<GnssClock>readParcelable(classLoader);
        int i = param1Parcel.readInt();
        GnssMeasurement[] arrayOfGnssMeasurement = new GnssMeasurement[i];
        param1Parcel.readTypedArray(arrayOfGnssMeasurement, GnssMeasurement.CREATOR);
        return new GnssMeasurementsEvent(gnssClock, arrayOfGnssMeasurement);
      }
      
      public GnssMeasurementsEvent[] newArray(int param1Int) {
        return new GnssMeasurementsEvent[param1Int];
      }
    };
  
  private final GnssClock mClock;
  
  private final Collection<GnssMeasurement> mReadOnlyMeasurements;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.mClock, paramInt);
    int i = this.mReadOnlyMeasurements.size();
    Collection<GnssMeasurement> collection = this.mReadOnlyMeasurements;
    GnssMeasurement[] arrayOfGnssMeasurement2 = new GnssMeasurement[i];
    GnssMeasurement[] arrayOfGnssMeasurement1 = collection.<GnssMeasurement>toArray(arrayOfGnssMeasurement2);
    paramParcel.writeInt(arrayOfGnssMeasurement1.length);
    paramParcel.writeTypedArray(arrayOfGnssMeasurement1, paramInt);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("[ GnssMeasurementsEvent:\n\n");
    stringBuilder.append(this.mClock.toString());
    stringBuilder.append("\n");
    for (GnssMeasurement gnssMeasurement : this.mReadOnlyMeasurements) {
      stringBuilder.append(gnssMeasurement.toString());
      stringBuilder.append("\n");
    } 
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
}
