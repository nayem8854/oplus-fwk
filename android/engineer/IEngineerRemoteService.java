package android.engineer;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IEngineerRemoteService extends IInterface {
  void sendCommand(IEngineerCommand paramIEngineerCommand, IEngineerCommandCallback paramIEngineerCommandCallback) throws RemoteException;
  
  class Default implements IEngineerRemoteService {
    public void sendCommand(IEngineerCommand param1IEngineerCommand, IEngineerCommandCallback param1IEngineerCommandCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IEngineerRemoteService {
    private static final String DESCRIPTOR = "android.engineer.IEngineerRemoteService";
    
    static final int TRANSACTION_sendCommand = 1;
    
    public Stub() {
      attachInterface(this, "android.engineer.IEngineerRemoteService");
    }
    
    public static IEngineerRemoteService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.engineer.IEngineerRemoteService");
      if (iInterface != null && iInterface instanceof IEngineerRemoteService)
        return (IEngineerRemoteService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "sendCommand";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.engineer.IEngineerRemoteService");
        return true;
      } 
      param1Parcel1.enforceInterface("android.engineer.IEngineerRemoteService");
      if (param1Parcel1.readInt() != 0) {
        IEngineerCommand iEngineerCommand = (IEngineerCommand)IEngineerCommand.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      IEngineerCommandCallback iEngineerCommandCallback = IEngineerCommandCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
      sendCommand((IEngineerCommand)param1Parcel2, iEngineerCommandCallback);
      return true;
    }
    
    private static class Proxy implements IEngineerRemoteService {
      public static IEngineerRemoteService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.engineer.IEngineerRemoteService";
      }
      
      public void sendCommand(IEngineerCommand param2IEngineerCommand, IEngineerCommandCallback param2IEngineerCommandCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.engineer.IEngineerRemoteService");
          if (param2IEngineerCommand != null) {
            parcel.writeInt(1);
            param2IEngineerCommand.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IEngineerCommandCallback != null) {
            iBinder = param2IEngineerCommandCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IEngineerRemoteService.Stub.getDefaultImpl() != null) {
            IEngineerRemoteService.Stub.getDefaultImpl().sendCommand(param2IEngineerCommand, param2IEngineerCommandCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IEngineerRemoteService param1IEngineerRemoteService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IEngineerRemoteService != null) {
          Proxy.sDefaultImpl = param1IEngineerRemoteService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IEngineerRemoteService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
