package android.engineer;

import android.os.Parcel;
import android.os.Parcelable;

public class IEngineerCommand implements Parcelable {
  public static final Parcelable.Creator<IEngineerCommand> CREATOR = new Parcelable.Creator<IEngineerCommand>() {
      public IEngineerCommand createFromParcel(Parcel param1Parcel) {
        return new IEngineerCommand(param1Parcel);
      }
      
      public IEngineerCommand[] newArray(int param1Int) {
        return new IEngineerCommand[param1Int];
      }
    };
  
  private String[] mCommand;
  
  public String[] getCommand() {
    return this.mCommand;
  }
  
  public void setCommand(String[] paramArrayOfString) {
    this.mCommand = paramArrayOfString;
  }
  
  public IEngineerCommand(String[] paramArrayOfString) {
    this.mCommand = paramArrayOfString;
  }
  
  protected IEngineerCommand(Parcel paramParcel) {
    this.mCommand = paramParcel.readStringArray();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeStringArray(this.mCommand);
  }
}
