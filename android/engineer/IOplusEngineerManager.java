package android.engineer;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusEngineerManager extends IInterface {
  boolean fastbootUnlock(byte[] paramArrayOfbyte, int paramInt) throws RemoteException;
  
  byte[] getBadBatteryConfig(int paramInt1, int paramInt2) throws RemoteException;
  
  String getBootImgWaterMark() throws RemoteException;
  
  byte[] getCalibrationStatusFromNvram() throws RemoteException;
  
  String getCarrierVersion() throws RemoteException;
  
  byte[] getCarrierVersionFromNvram() throws RemoteException;
  
  String getDownloadStatus() throws RemoteException;
  
  byte[] getEmmcHealthInfo() throws RemoteException;
  
  String getHeytapID(int paramInt) throws RemoteException;
  
  byte[] getProductLineTestResult() throws RemoteException;
  
  String getRegionNetlockStatus() throws RemoteException;
  
  String getSimOperatorSwitchStatus() throws RemoteException;
  
  String getSingleDoubleCardStatus() throws RemoteException;
  
  String getSystemProperties(String paramString1, String paramString2) throws RemoteException;
  
  boolean isEngineerItemInBlackList(int paramInt, String paramString) throws RemoteException;
  
  byte[] readEngineerData(int paramInt) throws RemoteException;
  
  boolean saveCarrierVersionToNvram(byte[] paramArrayOfbyte) throws RemoteException;
  
  boolean saveEngineerData(int paramInt1, byte[] paramArrayOfbyte, int paramInt2) throws RemoteException;
  
  boolean saveHeytapID(int paramInt, String paramString) throws RemoteException;
  
  int setBatteryBatteryConfig(int paramInt1, int paramInt2, byte[] paramArrayOfbyte) throws RemoteException;
  
  boolean setCarrierVersion(String paramString) throws RemoteException;
  
  boolean setProductLineTestResult(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean setRegionNetlock(String paramString) throws RemoteException;
  
  boolean setSimOperatorSwitch(String paramString) throws RemoteException;
  
  boolean setSingleDoubleCard(String paramString) throws RemoteException;
  
  void setSystemProperties(String paramString1, String paramString2) throws RemoteException;
  
  class Default implements IOplusEngineerManager {
    public String getDownloadStatus() throws RemoteException {
      return null;
    }
    
    public byte[] getEmmcHealthInfo() throws RemoteException {
      return null;
    }
    
    public String getCarrierVersion() throws RemoteException {
      return null;
    }
    
    public boolean setCarrierVersion(String param1String) throws RemoteException {
      return false;
    }
    
    public String getRegionNetlockStatus() throws RemoteException {
      return null;
    }
    
    public boolean setRegionNetlock(String param1String) throws RemoteException {
      return false;
    }
    
    public String getSingleDoubleCardStatus() throws RemoteException {
      return null;
    }
    
    public boolean setSingleDoubleCard(String param1String) throws RemoteException {
      return false;
    }
    
    public byte[] getBadBatteryConfig(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public int setBatteryBatteryConfig(int param1Int1, int param1Int2, byte[] param1ArrayOfbyte) throws RemoteException {
      return 0;
    }
    
    public byte[] getProductLineTestResult() throws RemoteException {
      return null;
    }
    
    public boolean setProductLineTestResult(int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public byte[] getCarrierVersionFromNvram() throws RemoteException {
      return null;
    }
    
    public boolean saveCarrierVersionToNvram(byte[] param1ArrayOfbyte) throws RemoteException {
      return false;
    }
    
    public byte[] getCalibrationStatusFromNvram() throws RemoteException {
      return null;
    }
    
    public boolean setSimOperatorSwitch(String param1String) throws RemoteException {
      return false;
    }
    
    public String getSimOperatorSwitchStatus() throws RemoteException {
      return null;
    }
    
    public String getBootImgWaterMark() throws RemoteException {
      return null;
    }
    
    public byte[] readEngineerData(int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean saveEngineerData(int param1Int1, byte[] param1ArrayOfbyte, int param1Int2) throws RemoteException {
      return false;
    }
    
    public boolean fastbootUnlock(byte[] param1ArrayOfbyte, int param1Int) throws RemoteException {
      return false;
    }
    
    public void setSystemProperties(String param1String1, String param1String2) throws RemoteException {}
    
    public String getSystemProperties(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public boolean isEngineerItemInBlackList(int param1Int, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean saveHeytapID(int param1Int, String param1String) throws RemoteException {
      return false;
    }
    
    public String getHeytapID(int param1Int) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusEngineerManager {
    private static final String DESCRIPTOR = "android.engineer.IOplusEngineerManager";
    
    static final int TRANSACTION_fastbootUnlock = 21;
    
    static final int TRANSACTION_getBadBatteryConfig = 9;
    
    static final int TRANSACTION_getBootImgWaterMark = 18;
    
    static final int TRANSACTION_getCalibrationStatusFromNvram = 15;
    
    static final int TRANSACTION_getCarrierVersion = 3;
    
    static final int TRANSACTION_getCarrierVersionFromNvram = 13;
    
    static final int TRANSACTION_getDownloadStatus = 1;
    
    static final int TRANSACTION_getEmmcHealthInfo = 2;
    
    static final int TRANSACTION_getHeytapID = 26;
    
    static final int TRANSACTION_getProductLineTestResult = 11;
    
    static final int TRANSACTION_getRegionNetlockStatus = 5;
    
    static final int TRANSACTION_getSimOperatorSwitchStatus = 17;
    
    static final int TRANSACTION_getSingleDoubleCardStatus = 7;
    
    static final int TRANSACTION_getSystemProperties = 23;
    
    static final int TRANSACTION_isEngineerItemInBlackList = 24;
    
    static final int TRANSACTION_readEngineerData = 19;
    
    static final int TRANSACTION_saveCarrierVersionToNvram = 14;
    
    static final int TRANSACTION_saveEngineerData = 20;
    
    static final int TRANSACTION_saveHeytapID = 25;
    
    static final int TRANSACTION_setBatteryBatteryConfig = 10;
    
    static final int TRANSACTION_setCarrierVersion = 4;
    
    static final int TRANSACTION_setProductLineTestResult = 12;
    
    static final int TRANSACTION_setRegionNetlock = 6;
    
    static final int TRANSACTION_setSimOperatorSwitch = 16;
    
    static final int TRANSACTION_setSingleDoubleCard = 8;
    
    static final int TRANSACTION_setSystemProperties = 22;
    
    public Stub() {
      attachInterface(this, "android.engineer.IOplusEngineerManager");
    }
    
    public static IOplusEngineerManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.engineer.IOplusEngineerManager");
      if (iInterface != null && iInterface instanceof IOplusEngineerManager)
        return (IOplusEngineerManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 26:
          return "getHeytapID";
        case 25:
          return "saveHeytapID";
        case 24:
          return "isEngineerItemInBlackList";
        case 23:
          return "getSystemProperties";
        case 22:
          return "setSystemProperties";
        case 21:
          return "fastbootUnlock";
        case 20:
          return "saveEngineerData";
        case 19:
          return "readEngineerData";
        case 18:
          return "getBootImgWaterMark";
        case 17:
          return "getSimOperatorSwitchStatus";
        case 16:
          return "setSimOperatorSwitch";
        case 15:
          return "getCalibrationStatusFromNvram";
        case 14:
          return "saveCarrierVersionToNvram";
        case 13:
          return "getCarrierVersionFromNvram";
        case 12:
          return "setProductLineTestResult";
        case 11:
          return "getProductLineTestResult";
        case 10:
          return "setBatteryBatteryConfig";
        case 9:
          return "getBadBatteryConfig";
        case 8:
          return "setSingleDoubleCard";
        case 7:
          return "getSingleDoubleCardStatus";
        case 6:
          return "setRegionNetlock";
        case 5:
          return "getRegionNetlockStatus";
        case 4:
          return "setCarrierVersion";
        case 3:
          return "getCarrierVersion";
        case 2:
          return "getEmmcHealthInfo";
        case 1:
          break;
      } 
      return "getDownloadStatus";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool7;
        int i1;
        boolean bool6;
        int n;
        boolean bool5;
        int m;
        boolean bool4;
        int k;
        boolean bool3;
        int j;
        boolean bool2;
        int i;
        boolean bool1;
        String str4;
        byte[] arrayOfByte3;
        String str3;
        byte[] arrayOfByte2;
        String str2;
        byte[] arrayOfByte1;
        String str5;
        byte[] arrayOfByte4;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 26:
            param1Parcel1.enforceInterface("android.engineer.IOplusEngineerManager");
            param1Int1 = param1Parcel1.readInt();
            str4 = getHeytapID(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str4);
            return true;
          case 25:
            str4.enforceInterface("android.engineer.IOplusEngineerManager");
            param1Int1 = str4.readInt();
            str4 = str4.readString();
            bool7 = saveHeytapID(param1Int1, str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 24:
            str4.enforceInterface("android.engineer.IOplusEngineerManager");
            i1 = str4.readInt();
            str4 = str4.readString();
            bool6 = isEngineerItemInBlackList(i1, str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 23:
            str4.enforceInterface("android.engineer.IOplusEngineerManager");
            str5 = str4.readString();
            str4 = str4.readString();
            str4 = getSystemProperties(str5, str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str4);
            return true;
          case 22:
            str4.enforceInterface("android.engineer.IOplusEngineerManager");
            str5 = str4.readString();
            str4 = str4.readString();
            setSystemProperties(str5, str4);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            str4.enforceInterface("android.engineer.IOplusEngineerManager");
            arrayOfByte4 = str4.createByteArray();
            n = str4.readInt();
            bool5 = fastbootUnlock(arrayOfByte4, n);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 20:
            str4.enforceInterface("android.engineer.IOplusEngineerManager");
            param1Int2 = str4.readInt();
            arrayOfByte4 = str4.createByteArray();
            m = str4.readInt();
            bool4 = saveEngineerData(param1Int2, arrayOfByte4, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 19:
            str4.enforceInterface("android.engineer.IOplusEngineerManager");
            k = str4.readInt();
            arrayOfByte3 = readEngineerData(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte3);
            return true;
          case 18:
            arrayOfByte3.enforceInterface("android.engineer.IOplusEngineerManager");
            str3 = getBootImgWaterMark();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str3);
            return true;
          case 17:
            str3.enforceInterface("android.engineer.IOplusEngineerManager");
            str3 = getSimOperatorSwitchStatus();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str3);
            return true;
          case 16:
            str3.enforceInterface("android.engineer.IOplusEngineerManager");
            str3 = str3.readString();
            bool3 = setSimOperatorSwitch(str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 15:
            str3.enforceInterface("android.engineer.IOplusEngineerManager");
            arrayOfByte2 = getCalibrationStatusFromNvram();
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte2);
            return true;
          case 14:
            arrayOfByte2.enforceInterface("android.engineer.IOplusEngineerManager");
            arrayOfByte2 = arrayOfByte2.createByteArray();
            bool3 = saveCarrierVersionToNvram(arrayOfByte2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 13:
            arrayOfByte2.enforceInterface("android.engineer.IOplusEngineerManager");
            arrayOfByte2 = getCarrierVersionFromNvram();
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte2);
            return true;
          case 12:
            arrayOfByte2.enforceInterface("android.engineer.IOplusEngineerManager");
            param1Int2 = arrayOfByte2.readInt();
            j = arrayOfByte2.readInt();
            bool2 = setProductLineTestResult(param1Int2, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 11:
            arrayOfByte2.enforceInterface("android.engineer.IOplusEngineerManager");
            arrayOfByte2 = getProductLineTestResult();
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte2);
            return true;
          case 10:
            arrayOfByte2.enforceInterface("android.engineer.IOplusEngineerManager");
            i = arrayOfByte2.readInt();
            param1Int2 = arrayOfByte2.readInt();
            arrayOfByte2 = arrayOfByte2.createByteArray();
            i = setBatteryBatteryConfig(i, param1Int2, arrayOfByte2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 9:
            arrayOfByte2.enforceInterface("android.engineer.IOplusEngineerManager");
            param1Int2 = arrayOfByte2.readInt();
            i = arrayOfByte2.readInt();
            arrayOfByte2 = getBadBatteryConfig(param1Int2, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte2);
            return true;
          case 8:
            arrayOfByte2.enforceInterface("android.engineer.IOplusEngineerManager");
            str2 = arrayOfByte2.readString();
            bool1 = setSingleDoubleCard(str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 7:
            str2.enforceInterface("android.engineer.IOplusEngineerManager");
            str2 = getSingleDoubleCardStatus();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 6:
            str2.enforceInterface("android.engineer.IOplusEngineerManager");
            str2 = str2.readString();
            bool1 = setRegionNetlock(str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 5:
            str2.enforceInterface("android.engineer.IOplusEngineerManager");
            str2 = getRegionNetlockStatus();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 4:
            str2.enforceInterface("android.engineer.IOplusEngineerManager");
            str2 = str2.readString();
            bool1 = setCarrierVersion(str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 3:
            str2.enforceInterface("android.engineer.IOplusEngineerManager");
            str2 = getCarrierVersion();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 2:
            str2.enforceInterface("android.engineer.IOplusEngineerManager");
            arrayOfByte1 = getEmmcHealthInfo();
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte1);
            return true;
          case 1:
            break;
        } 
        arrayOfByte1.enforceInterface("android.engineer.IOplusEngineerManager");
        String str1 = getDownloadStatus();
        param1Parcel2.writeNoException();
        param1Parcel2.writeString(str1);
        return true;
      } 
      param1Parcel2.writeString("android.engineer.IOplusEngineerManager");
      return true;
    }
    
    private static class Proxy implements IOplusEngineerManager {
      public static IOplusEngineerManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.engineer.IOplusEngineerManager";
      }
      
      public String getDownloadStatus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusEngineerManager.Stub.getDefaultImpl() != null)
            return IOplusEngineerManager.Stub.getDefaultImpl().getDownloadStatus(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getEmmcHealthInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusEngineerManager.Stub.getDefaultImpl() != null)
            return IOplusEngineerManager.Stub.getDefaultImpl().getEmmcHealthInfo(); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getCarrierVersion() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOplusEngineerManager.Stub.getDefaultImpl() != null)
            return IOplusEngineerManager.Stub.getDefaultImpl().getCarrierVersion(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setCarrierVersion(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IOplusEngineerManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusEngineerManager.Stub.getDefaultImpl().setCarrierVersion(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getRegionNetlockStatus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOplusEngineerManager.Stub.getDefaultImpl() != null)
            return IOplusEngineerManager.Stub.getDefaultImpl().getRegionNetlockStatus(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setRegionNetlock(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IOplusEngineerManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusEngineerManager.Stub.getDefaultImpl().setRegionNetlock(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSingleDoubleCardStatus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IOplusEngineerManager.Stub.getDefaultImpl() != null)
            return IOplusEngineerManager.Stub.getDefaultImpl().getSingleDoubleCardStatus(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setSingleDoubleCard(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(8, parcel1, parcel2, 0);
          if (!bool2 && IOplusEngineerManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusEngineerManager.Stub.getDefaultImpl().setSingleDoubleCard(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getBadBatteryConfig(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IOplusEngineerManager.Stub.getDefaultImpl() != null)
            return IOplusEngineerManager.Stub.getDefaultImpl().getBadBatteryConfig(param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setBatteryBatteryConfig(int param2Int1, int param2Int2, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IOplusEngineerManager.Stub.getDefaultImpl() != null) {
            param2Int1 = IOplusEngineerManager.Stub.getDefaultImpl().setBatteryBatteryConfig(param2Int1, param2Int2, param2ArrayOfbyte);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getProductLineTestResult() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IOplusEngineerManager.Stub.getDefaultImpl() != null)
            return IOplusEngineerManager.Stub.getDefaultImpl().getProductLineTestResult(); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setProductLineTestResult(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(12, parcel1, parcel2, 0);
          if (!bool2 && IOplusEngineerManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusEngineerManager.Stub.getDefaultImpl().setProductLineTestResult(param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getCarrierVersionFromNvram() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IOplusEngineerManager.Stub.getDefaultImpl() != null)
            return IOplusEngineerManager.Stub.getDefaultImpl().getCarrierVersionFromNvram(); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean saveCarrierVersionToNvram(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          parcel1.writeByteArray(param2ArrayOfbyte);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(14, parcel1, parcel2, 0);
          if (!bool2 && IOplusEngineerManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusEngineerManager.Stub.getDefaultImpl().saveCarrierVersionToNvram(param2ArrayOfbyte);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getCalibrationStatusFromNvram() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IOplusEngineerManager.Stub.getDefaultImpl() != null)
            return IOplusEngineerManager.Stub.getDefaultImpl().getCalibrationStatusFromNvram(); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setSimOperatorSwitch(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(16, parcel1, parcel2, 0);
          if (!bool2 && IOplusEngineerManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusEngineerManager.Stub.getDefaultImpl().setSimOperatorSwitch(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSimOperatorSwitchStatus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IOplusEngineerManager.Stub.getDefaultImpl() != null)
            return IOplusEngineerManager.Stub.getDefaultImpl().getSimOperatorSwitchStatus(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getBootImgWaterMark() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IOplusEngineerManager.Stub.getDefaultImpl() != null)
            return IOplusEngineerManager.Stub.getDefaultImpl().getBootImgWaterMark(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] readEngineerData(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IOplusEngineerManager.Stub.getDefaultImpl() != null)
            return IOplusEngineerManager.Stub.getDefaultImpl().readEngineerData(param2Int); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean saveEngineerData(int param2Int1, byte[] param2ArrayOfbyte, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(20, parcel1, parcel2, 0);
          if (!bool2 && IOplusEngineerManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusEngineerManager.Stub.getDefaultImpl().saveEngineerData(param2Int1, param2ArrayOfbyte, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean fastbootUnlock(byte[] param2ArrayOfbyte, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(21, parcel1, parcel2, 0);
          if (!bool2 && IOplusEngineerManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusEngineerManager.Stub.getDefaultImpl().fastbootUnlock(param2ArrayOfbyte, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSystemProperties(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IOplusEngineerManager.Stub.getDefaultImpl() != null) {
            IOplusEngineerManager.Stub.getDefaultImpl().setSystemProperties(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSystemProperties(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IOplusEngineerManager.Stub.getDefaultImpl() != null) {
            param2String1 = IOplusEngineerManager.Stub.getDefaultImpl().getSystemProperties(param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isEngineerItemInBlackList(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(24, parcel1, parcel2, 0);
          if (!bool2 && IOplusEngineerManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusEngineerManager.Stub.getDefaultImpl().isEngineerItemInBlackList(param2Int, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean saveHeytapID(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(25, parcel1, parcel2, 0);
          if (!bool2 && IOplusEngineerManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusEngineerManager.Stub.getDefaultImpl().saveHeytapID(param2Int, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getHeytapID(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.engineer.IOplusEngineerManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IOplusEngineerManager.Stub.getDefaultImpl() != null)
            return IOplusEngineerManager.Stub.getDefaultImpl().getHeytapID(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusEngineerManager param1IOplusEngineerManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusEngineerManager != null) {
          Proxy.sDefaultImpl = param1IOplusEngineerManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusEngineerManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
