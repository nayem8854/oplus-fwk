package android.engineer;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IEngineerCommandCallback extends IInterface {
  void onCommandHandled(String paramString) throws RemoteException;
  
  class Default implements IEngineerCommandCallback {
    public void onCommandHandled(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IEngineerCommandCallback {
    private static final String DESCRIPTOR = "android.engineer.IEngineerCommandCallback";
    
    static final int TRANSACTION_onCommandHandled = 1;
    
    public Stub() {
      attachInterface(this, "android.engineer.IEngineerCommandCallback");
    }
    
    public static IEngineerCommandCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.engineer.IEngineerCommandCallback");
      if (iInterface != null && iInterface instanceof IEngineerCommandCallback)
        return (IEngineerCommandCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onCommandHandled";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.engineer.IEngineerCommandCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.engineer.IEngineerCommandCallback");
      String str = param1Parcel1.readString();
      onCommandHandled(str);
      return true;
    }
    
    private static class Proxy implements IEngineerCommandCallback {
      public static IEngineerCommandCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.engineer.IEngineerCommandCallback";
      }
      
      public void onCommandHandled(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.engineer.IEngineerCommandCallback");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IEngineerCommandCallback.Stub.getDefaultImpl() != null) {
            IEngineerCommandCallback.Stub.getDefaultImpl().onCommandHandled(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IEngineerCommandCallback param1IEngineerCommandCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IEngineerCommandCallback != null) {
          Proxy.sDefaultImpl = param1IEngineerCommandCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IEngineerCommandCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
