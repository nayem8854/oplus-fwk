package android.engineer;

import android.os.RemoteException;
import android.os.ServiceManager;

public class OplusEngineerManager {
  private static final String ENGINEER_SERVICE_NAME = "engineer";
  
  private static IOplusEngineerManager init() {
    return IOplusEngineerManager.Stub.asInterface(ServiceManager.getService("engineer"));
  }
  
  public static String getDownloadStatus() {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.getDownloadStatus();
      } catch (RemoteException remoteException) {} 
    return null;
  }
  
  public static byte[] getEmmcHealthInfo() {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.getEmmcHealthInfo();
      } catch (RemoteException remoteException) {} 
    return null;
  }
  
  public static String getCarrierVersion() {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.getCarrierVersion();
      } catch (RemoteException remoteException) {} 
    return null;
  }
  
  public static boolean setCarrierVersion(String paramString) {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.setCarrierVersion(paramString);
      } catch (RemoteException remoteException) {} 
    return false;
  }
  
  public static String getRegionNetlockStatus() {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.getRegionNetlockStatus();
      } catch (RemoteException remoteException) {} 
    return null;
  }
  
  public static boolean setRegionNetlock(String paramString) {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.setRegionNetlock(paramString);
      } catch (RemoteException remoteException) {} 
    return false;
  }
  
  public static String getSingleDoubleCardStatus() {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.getSingleDoubleCardStatus();
      } catch (RemoteException remoteException) {} 
    return null;
  }
  
  public static boolean setSingleDoubleCard(String paramString) {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.setSingleDoubleCard(paramString);
      } catch (RemoteException remoteException) {} 
    return false;
  }
  
  public static byte[] getBadBatteryConfig(int paramInt1, int paramInt2) {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.getBadBatteryConfig(paramInt1, paramInt2);
      } catch (RemoteException remoteException) {} 
    return null;
  }
  
  public static int setBatteryBatteryConfig(int paramInt1, int paramInt2, byte[] paramArrayOfbyte) {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.setBatteryBatteryConfig(paramInt1, paramInt2, paramArrayOfbyte);
      } catch (RemoteException remoteException) {} 
    return -1;
  }
  
  public static byte[] getProductLineTestResult() {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.getProductLineTestResult();
      } catch (RemoteException remoteException) {} 
    return null;
  }
  
  public static boolean setProductLineTestResult(int paramInt1, int paramInt2) {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.setProductLineTestResult(paramInt1, paramInt2);
      } catch (RemoteException remoteException) {} 
    return false;
  }
  
  public static byte[] getCarrierVersionFromNvram() {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.getCarrierVersionFromNvram();
      } catch (RemoteException remoteException) {} 
    return null;
  }
  
  public static boolean saveCarrierVersionToNvram(byte[] paramArrayOfbyte) {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.saveCarrierVersionToNvram(paramArrayOfbyte);
      } catch (RemoteException remoteException) {} 
    return false;
  }
  
  public static byte[] getCalibrationStatusFromNvram() {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.getCalibrationStatusFromNvram();
      } catch (RemoteException remoteException) {} 
    return null;
  }
  
  public static String getSimOperatorSwitchStatus() {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.getSimOperatorSwitchStatus();
      } catch (RemoteException remoteException) {} 
    return null;
  }
  
  public static boolean setSimOperatorSwitch(String paramString) {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.setSimOperatorSwitch(paramString);
      } catch (RemoteException remoteException) {} 
    return false;
  }
  
  public static String getBootImgWaterMark() {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.getBootImgWaterMark();
      } catch (RemoteException remoteException) {} 
    return null;
  }
  
  public static byte[] readEngineerData(int paramInt) {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.readEngineerData(paramInt);
      } catch (RemoteException remoteException) {} 
    return null;
  }
  
  public static boolean saveEngineerData(int paramInt1, byte[] paramArrayOfbyte, int paramInt2) {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.saveEngineerData(paramInt1, paramArrayOfbyte, paramInt2);
      } catch (RemoteException remoteException) {} 
    return false;
  }
  
  public static boolean fastbootUnlock(byte[] paramArrayOfbyte, int paramInt) {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.fastbootUnlock(paramArrayOfbyte, paramInt);
      } catch (RemoteException remoteException) {} 
    return false;
  }
  
  public static void setSystemProperties(String paramString1, String paramString2) {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        iOplusEngineerManager.setSystemProperties(paramString1, paramString2);
      } catch (RemoteException remoteException) {} 
  }
  
  public static String getSystemProperties(String paramString1, String paramString2) {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.getSystemProperties(paramString1, paramString2);
      } catch (RemoteException remoteException) {} 
    return null;
  }
  
  public static boolean isEngineerItemInBlackList(int paramInt, String paramString) {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.isEngineerItemInBlackList(paramInt, paramString);
      } catch (RemoteException remoteException) {} 
    return false;
  }
  
  public static boolean saveHeytapID(int paramInt, String paramString) {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.saveHeytapID(paramInt, paramString);
      } catch (RemoteException remoteException) {} 
    return false;
  }
  
  public static String getHeytapID(int paramInt) {
    IOplusEngineerManager iOplusEngineerManager = init();
    if (iOplusEngineerManager != null)
      try {
        return iOplusEngineerManager.getHeytapID(paramInt);
      } catch (RemoteException remoteException) {} 
    return null;
  }
}
