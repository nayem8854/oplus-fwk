package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

public interface IIpSecService extends IInterface {
  void addAddressToTunnelInterface(int paramInt, LinkAddress paramLinkAddress, String paramString) throws RemoteException;
  
  IpSecSpiResponse allocateSecurityParameterIndex(String paramString, int paramInt, IBinder paramIBinder) throws RemoteException;
  
  void applyTransportModeTransform(ParcelFileDescriptor paramParcelFileDescriptor, int paramInt1, int paramInt2) throws RemoteException;
  
  void applyTunnelModeTransform(int paramInt1, int paramInt2, int paramInt3, String paramString) throws RemoteException;
  
  void closeUdpEncapsulationSocket(int paramInt) throws RemoteException;
  
  IpSecTransformResponse createTransform(IpSecConfig paramIpSecConfig, IBinder paramIBinder, String paramString) throws RemoteException;
  
  IpSecTunnelInterfaceResponse createTunnelInterface(String paramString1, String paramString2, Network paramNetwork, IBinder paramIBinder, String paramString3) throws RemoteException;
  
  void deleteTransform(int paramInt) throws RemoteException;
  
  void deleteTunnelInterface(int paramInt, String paramString) throws RemoteException;
  
  IpSecUdpEncapResponse openUdpEncapsulationSocket(int paramInt, IBinder paramIBinder) throws RemoteException;
  
  void releaseSecurityParameterIndex(int paramInt) throws RemoteException;
  
  void removeAddressFromTunnelInterface(int paramInt, LinkAddress paramLinkAddress, String paramString) throws RemoteException;
  
  void removeTransportModeTransforms(ParcelFileDescriptor paramParcelFileDescriptor) throws RemoteException;
  
  class Default implements IIpSecService {
    public IpSecSpiResponse allocateSecurityParameterIndex(String param1String, int param1Int, IBinder param1IBinder) throws RemoteException {
      return null;
    }
    
    public void releaseSecurityParameterIndex(int param1Int) throws RemoteException {}
    
    public IpSecUdpEncapResponse openUdpEncapsulationSocket(int param1Int, IBinder param1IBinder) throws RemoteException {
      return null;
    }
    
    public void closeUdpEncapsulationSocket(int param1Int) throws RemoteException {}
    
    public IpSecTunnelInterfaceResponse createTunnelInterface(String param1String1, String param1String2, Network param1Network, IBinder param1IBinder, String param1String3) throws RemoteException {
      return null;
    }
    
    public void addAddressToTunnelInterface(int param1Int, LinkAddress param1LinkAddress, String param1String) throws RemoteException {}
    
    public void removeAddressFromTunnelInterface(int param1Int, LinkAddress param1LinkAddress, String param1String) throws RemoteException {}
    
    public void deleteTunnelInterface(int param1Int, String param1String) throws RemoteException {}
    
    public IpSecTransformResponse createTransform(IpSecConfig param1IpSecConfig, IBinder param1IBinder, String param1String) throws RemoteException {
      return null;
    }
    
    public void deleteTransform(int param1Int) throws RemoteException {}
    
    public void applyTransportModeTransform(ParcelFileDescriptor param1ParcelFileDescriptor, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void applyTunnelModeTransform(int param1Int1, int param1Int2, int param1Int3, String param1String) throws RemoteException {}
    
    public void removeTransportModeTransforms(ParcelFileDescriptor param1ParcelFileDescriptor) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IIpSecService {
    private static final String DESCRIPTOR = "android.net.IIpSecService";
    
    static final int TRANSACTION_addAddressToTunnelInterface = 6;
    
    static final int TRANSACTION_allocateSecurityParameterIndex = 1;
    
    static final int TRANSACTION_applyTransportModeTransform = 11;
    
    static final int TRANSACTION_applyTunnelModeTransform = 12;
    
    static final int TRANSACTION_closeUdpEncapsulationSocket = 4;
    
    static final int TRANSACTION_createTransform = 9;
    
    static final int TRANSACTION_createTunnelInterface = 5;
    
    static final int TRANSACTION_deleteTransform = 10;
    
    static final int TRANSACTION_deleteTunnelInterface = 8;
    
    static final int TRANSACTION_openUdpEncapsulationSocket = 3;
    
    static final int TRANSACTION_releaseSecurityParameterIndex = 2;
    
    static final int TRANSACTION_removeAddressFromTunnelInterface = 7;
    
    static final int TRANSACTION_removeTransportModeTransforms = 13;
    
    public Stub() {
      attachInterface(this, "android.net.IIpSecService");
    }
    
    public static IIpSecService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.IIpSecService");
      if (iInterface != null && iInterface instanceof IIpSecService)
        return (IIpSecService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 13:
          return "removeTransportModeTransforms";
        case 12:
          return "applyTunnelModeTransform";
        case 11:
          return "applyTransportModeTransform";
        case 10:
          return "deleteTransform";
        case 9:
          return "createTransform";
        case 8:
          return "deleteTunnelInterface";
        case 7:
          return "removeAddressFromTunnelInterface";
        case 6:
          return "addAddressToTunnelInterface";
        case 5:
          return "createTunnelInterface";
        case 4:
          return "closeUdpEncapsulationSocket";
        case 3:
          return "openUdpEncapsulationSocket";
        case 2:
          return "releaseSecurityParameterIndex";
        case 1:
          break;
      } 
      return "allocateSecurityParameterIndex";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String str2;
        IpSecTransformResponse ipSecTransformResponse;
        String str1;
        IpSecTunnelInterfaceResponse ipSecTunnelInterfaceResponse;
        IBinder iBinder2;
        IpSecUdpEncapResponse ipSecUdpEncapResponse;
        int i;
        ParcelFileDescriptor parcelFileDescriptor;
        IBinder iBinder3;
        String str4, str5;
        IBinder iBinder4;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 13:
            param1Parcel1.enforceInterface("android.net.IIpSecService");
            if (param1Parcel1.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor1 = ParcelFileDescriptor.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            removeTransportModeTransforms((ParcelFileDescriptor)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            param1Parcel1.enforceInterface("android.net.IIpSecService");
            i = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            str2 = param1Parcel1.readString();
            applyTunnelModeTransform(i, param1Int2, param1Int1, str2);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            str2.enforceInterface("android.net.IIpSecService");
            if (str2.readInt() != 0) {
              parcelFileDescriptor = ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)str2);
            } else {
              parcelFileDescriptor = null;
            } 
            param1Int1 = str2.readInt();
            param1Int2 = str2.readInt();
            applyTransportModeTransform(parcelFileDescriptor, param1Int1, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            str2.enforceInterface("android.net.IIpSecService");
            param1Int1 = str2.readInt();
            deleteTransform(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            str2.enforceInterface("android.net.IIpSecService");
            if (str2.readInt() != 0) {
              IpSecConfig ipSecConfig = IpSecConfig.CREATOR.createFromParcel((Parcel)str2);
            } else {
              parcelFileDescriptor = null;
            } 
            iBinder3 = str2.readStrongBinder();
            str2 = str2.readString();
            ipSecTransformResponse = createTransform((IpSecConfig)parcelFileDescriptor, iBinder3, str2);
            param1Parcel2.writeNoException();
            if (ipSecTransformResponse != null) {
              param1Parcel2.writeInt(1);
              ipSecTransformResponse.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 8:
            ipSecTransformResponse.enforceInterface("android.net.IIpSecService");
            param1Int1 = ipSecTransformResponse.readInt();
            str1 = ipSecTransformResponse.readString();
            deleteTunnelInterface(param1Int1, str1);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            str1.enforceInterface("android.net.IIpSecService");
            param1Int1 = str1.readInt();
            if (str1.readInt() != 0) {
              LinkAddress linkAddress = LinkAddress.CREATOR.createFromParcel((Parcel)str1);
            } else {
              parcelFileDescriptor = null;
            } 
            str1 = str1.readString();
            removeAddressFromTunnelInterface(param1Int1, (LinkAddress)parcelFileDescriptor, str1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            str1.enforceInterface("android.net.IIpSecService");
            param1Int1 = str1.readInt();
            if (str1.readInt() != 0) {
              LinkAddress linkAddress = LinkAddress.CREATOR.createFromParcel((Parcel)str1);
            } else {
              parcelFileDescriptor = null;
            } 
            str1 = str1.readString();
            addAddressToTunnelInterface(param1Int1, (LinkAddress)parcelFileDescriptor, str1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            str1.enforceInterface("android.net.IIpSecService");
            str4 = str1.readString();
            str5 = str1.readString();
            if (str1.readInt() != 0) {
              Network network = Network.CREATOR.createFromParcel((Parcel)str1);
            } else {
              parcelFileDescriptor = null;
            } 
            iBinder4 = str1.readStrongBinder();
            str1 = str1.readString();
            ipSecTunnelInterfaceResponse = createTunnelInterface(str4, str5, (Network)parcelFileDescriptor, iBinder4, str1);
            param1Parcel2.writeNoException();
            if (ipSecTunnelInterfaceResponse != null) {
              param1Parcel2.writeInt(1);
              ipSecTunnelInterfaceResponse.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 4:
            ipSecTunnelInterfaceResponse.enforceInterface("android.net.IIpSecService");
            param1Int1 = ipSecTunnelInterfaceResponse.readInt();
            closeUdpEncapsulationSocket(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            ipSecTunnelInterfaceResponse.enforceInterface("android.net.IIpSecService");
            param1Int1 = ipSecTunnelInterfaceResponse.readInt();
            iBinder2 = ipSecTunnelInterfaceResponse.readStrongBinder();
            ipSecUdpEncapResponse = openUdpEncapsulationSocket(param1Int1, iBinder2);
            param1Parcel2.writeNoException();
            if (ipSecUdpEncapResponse != null) {
              param1Parcel2.writeInt(1);
              ipSecUdpEncapResponse.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 2:
            ipSecUdpEncapResponse.enforceInterface("android.net.IIpSecService");
            param1Int1 = ipSecUdpEncapResponse.readInt();
            releaseSecurityParameterIndex(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        ipSecUdpEncapResponse.enforceInterface("android.net.IIpSecService");
        String str3 = ipSecUdpEncapResponse.readString();
        param1Int1 = ipSecUdpEncapResponse.readInt();
        IBinder iBinder1 = ipSecUdpEncapResponse.readStrongBinder();
        IpSecSpiResponse ipSecSpiResponse = allocateSecurityParameterIndex(str3, param1Int1, iBinder1);
        param1Parcel2.writeNoException();
        if (ipSecSpiResponse != null) {
          param1Parcel2.writeInt(1);
          ipSecSpiResponse.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      param1Parcel2.writeString("android.net.IIpSecService");
      return true;
    }
    
    private static class Proxy implements IIpSecService {
      public static IIpSecService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.IIpSecService";
      }
      
      public IpSecSpiResponse allocateSecurityParameterIndex(String param2String, int param2Int, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IIpSecService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IIpSecService.Stub.getDefaultImpl() != null)
            return IIpSecService.Stub.getDefaultImpl().allocateSecurityParameterIndex(param2String, param2Int, param2IBinder); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            IpSecSpiResponse ipSecSpiResponse = IpSecSpiResponse.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (IpSecSpiResponse)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void releaseSecurityParameterIndex(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IIpSecService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IIpSecService.Stub.getDefaultImpl() != null) {
            IIpSecService.Stub.getDefaultImpl().releaseSecurityParameterIndex(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IpSecUdpEncapResponse openUdpEncapsulationSocket(int param2Int, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IIpSecService");
          parcel1.writeInt(param2Int);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IIpSecService.Stub.getDefaultImpl() != null)
            return IIpSecService.Stub.getDefaultImpl().openUdpEncapsulationSocket(param2Int, param2IBinder); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            IpSecUdpEncapResponse ipSecUdpEncapResponse = IpSecUdpEncapResponse.CREATOR.createFromParcel(parcel2);
          } else {
            param2IBinder = null;
          } 
          return (IpSecUdpEncapResponse)param2IBinder;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void closeUdpEncapsulationSocket(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IIpSecService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IIpSecService.Stub.getDefaultImpl() != null) {
            IIpSecService.Stub.getDefaultImpl().closeUdpEncapsulationSocket(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IpSecTunnelInterfaceResponse createTunnelInterface(String param2String1, String param2String2, Network param2Network, IBinder param2IBinder, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IIpSecService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IIpSecService.Stub.getDefaultImpl() != null)
            return IIpSecService.Stub.getDefaultImpl().createTunnelInterface(param2String1, param2String2, param2Network, param2IBinder, param2String3); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            IpSecTunnelInterfaceResponse ipSecTunnelInterfaceResponse = IpSecTunnelInterfaceResponse.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (IpSecTunnelInterfaceResponse)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addAddressToTunnelInterface(int param2Int, LinkAddress param2LinkAddress, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IIpSecService");
          parcel1.writeInt(param2Int);
          if (param2LinkAddress != null) {
            parcel1.writeInt(1);
            param2LinkAddress.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IIpSecService.Stub.getDefaultImpl() != null) {
            IIpSecService.Stub.getDefaultImpl().addAddressToTunnelInterface(param2Int, param2LinkAddress, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeAddressFromTunnelInterface(int param2Int, LinkAddress param2LinkAddress, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IIpSecService");
          parcel1.writeInt(param2Int);
          if (param2LinkAddress != null) {
            parcel1.writeInt(1);
            param2LinkAddress.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IIpSecService.Stub.getDefaultImpl() != null) {
            IIpSecService.Stub.getDefaultImpl().removeAddressFromTunnelInterface(param2Int, param2LinkAddress, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteTunnelInterface(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IIpSecService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IIpSecService.Stub.getDefaultImpl() != null) {
            IIpSecService.Stub.getDefaultImpl().deleteTunnelInterface(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IpSecTransformResponse createTransform(IpSecConfig param2IpSecConfig, IBinder param2IBinder, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IIpSecService");
          if (param2IpSecConfig != null) {
            parcel1.writeInt(1);
            param2IpSecConfig.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IIpSecService.Stub.getDefaultImpl() != null)
            return IIpSecService.Stub.getDefaultImpl().createTransform(param2IpSecConfig, param2IBinder, param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            IpSecTransformResponse ipSecTransformResponse = IpSecTransformResponse.CREATOR.createFromParcel(parcel2);
          } else {
            param2IpSecConfig = null;
          } 
          return (IpSecTransformResponse)param2IpSecConfig;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteTransform(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IIpSecService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IIpSecService.Stub.getDefaultImpl() != null) {
            IIpSecService.Stub.getDefaultImpl().deleteTransform(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void applyTransportModeTransform(ParcelFileDescriptor param2ParcelFileDescriptor, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IIpSecService");
          if (param2ParcelFileDescriptor != null) {
            parcel1.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IIpSecService.Stub.getDefaultImpl() != null) {
            IIpSecService.Stub.getDefaultImpl().applyTransportModeTransform(param2ParcelFileDescriptor, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void applyTunnelModeTransform(int param2Int1, int param2Int2, int param2Int3, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IIpSecService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IIpSecService.Stub.getDefaultImpl() != null) {
            IIpSecService.Stub.getDefaultImpl().applyTunnelModeTransform(param2Int1, param2Int2, param2Int3, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeTransportModeTransforms(ParcelFileDescriptor param2ParcelFileDescriptor) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IIpSecService");
          if (param2ParcelFileDescriptor != null) {
            parcel1.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IIpSecService.Stub.getDefaultImpl() != null) {
            IIpSecService.Stub.getDefaultImpl().removeTransportModeTransforms(param2ParcelFileDescriptor);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IIpSecService param1IIpSecService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IIpSecService != null) {
          Proxy.sDefaultImpl = param1IIpSecService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IIpSecService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
