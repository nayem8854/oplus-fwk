package android.net;

import android.os.Parcel;
import android.os.Parcelable;

public class NetworkState implements Parcelable {
  public static final Parcelable.Creator<NetworkState> CREATOR;
  
  public static final NetworkState EMPTY = new NetworkState(null, null, null, null, null, null);
  
  private static final boolean SANITY_CHECK_ROAMING = false;
  
  public final LinkProperties linkProperties;
  
  public final Network network;
  
  public final NetworkCapabilities networkCapabilities;
  
  public final String networkId;
  
  public final NetworkInfo networkInfo;
  
  public final String subscriberId;
  
  public NetworkState(NetworkInfo paramNetworkInfo, LinkProperties paramLinkProperties, NetworkCapabilities paramNetworkCapabilities, Network paramNetwork, String paramString1, String paramString2) {
    this.networkInfo = paramNetworkInfo;
    this.linkProperties = paramLinkProperties;
    this.networkCapabilities = paramNetworkCapabilities;
    this.network = paramNetwork;
    this.subscriberId = paramString1;
    this.networkId = paramString2;
  }
  
  public NetworkState(Parcel paramParcel) {
    this.networkInfo = paramParcel.<NetworkInfo>readParcelable(null);
    this.linkProperties = paramParcel.<LinkProperties>readParcelable(null);
    this.networkCapabilities = paramParcel.<NetworkCapabilities>readParcelable(null);
    this.network = paramParcel.<Network>readParcelable(null);
    this.subscriberId = paramParcel.readString();
    this.networkId = paramParcel.readString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.networkInfo, paramInt);
    paramParcel.writeParcelable(this.linkProperties, paramInt);
    paramParcel.writeParcelable(this.networkCapabilities, paramInt);
    paramParcel.writeParcelable(this.network, paramInt);
    paramParcel.writeString(this.subscriberId);
    paramParcel.writeString(this.networkId);
  }
  
  static {
    CREATOR = new Parcelable.Creator<NetworkState>() {
        public NetworkState createFromParcel(Parcel param1Parcel) {
          return new NetworkState(param1Parcel);
        }
        
        public NetworkState[] newArray(int param1Int) {
          return new NetworkState[param1Int];
        }
      };
  }
}
