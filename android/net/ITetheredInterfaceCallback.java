package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ITetheredInterfaceCallback extends IInterface {
  void onAvailable(String paramString) throws RemoteException;
  
  void onUnavailable() throws RemoteException;
  
  class Default implements ITetheredInterfaceCallback {
    public void onAvailable(String param1String) throws RemoteException {}
    
    public void onUnavailable() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITetheredInterfaceCallback {
    private static final String DESCRIPTOR = "android.net.ITetheredInterfaceCallback";
    
    static final int TRANSACTION_onAvailable = 1;
    
    static final int TRANSACTION_onUnavailable = 2;
    
    public Stub() {
      attachInterface(this, "android.net.ITetheredInterfaceCallback");
    }
    
    public static ITetheredInterfaceCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.ITetheredInterfaceCallback");
      if (iInterface != null && iInterface instanceof ITetheredInterfaceCallback)
        return (ITetheredInterfaceCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onUnavailable";
      } 
      return "onAvailable";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.net.ITetheredInterfaceCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("android.net.ITetheredInterfaceCallback");
        onUnavailable();
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel1.enforceInterface("android.net.ITetheredInterfaceCallback");
      String str = param1Parcel1.readString();
      onAvailable(str);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements ITetheredInterfaceCallback {
      public static ITetheredInterfaceCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.ITetheredInterfaceCallback";
      }
      
      public void onAvailable(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.ITetheredInterfaceCallback");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ITetheredInterfaceCallback.Stub.getDefaultImpl() != null) {
            ITetheredInterfaceCallback.Stub.getDefaultImpl().onAvailable(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onUnavailable() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.ITetheredInterfaceCallback");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ITetheredInterfaceCallback.Stub.getDefaultImpl() != null) {
            ITetheredInterfaceCallback.Stub.getDefaultImpl().onUnavailable();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITetheredInterfaceCallback param1ITetheredInterfaceCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITetheredInterfaceCallback != null) {
          Proxy.sDefaultImpl = param1ITetheredInterfaceCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITetheredInterfaceCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
