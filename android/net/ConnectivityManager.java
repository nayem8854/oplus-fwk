package android.net;

import android.annotation.SystemApi;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.INetworkActivityListener;
import android.os.INetworkManagementService;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.ParcelFileDescriptor;
import android.os.PersistableBundle;
import android.os.Process;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.os.ServiceSpecificException;
import android.os.UserHandle;
import android.os.oplusdevicepolicy.OplusDevicepolicyManager;
import android.provider.Settings;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.util.SparseIntArray;
import com.android.internal.util.Preconditions;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import libcore.net.event.NetworkEventDispatcher;

public class ConnectivityManager {
  @Deprecated
  public static final String ACTION_BACKGROUND_DATA_SETTING_CHANGED = "android.net.conn.BACKGROUND_DATA_SETTING_CHANGED";
  
  public static final String ACTION_CAPTIVE_PORTAL_SIGN_IN = "android.net.conn.CAPTIVE_PORTAL";
  
  public static final String ACTION_CAPTIVE_PORTAL_TEST_COMPLETED = "android.net.conn.CAPTIVE_PORTAL_TEST_COMPLETED";
  
  public static final String ACTION_DATA_ACTIVITY_CHANGE = "android.net.conn.DATA_ACTIVITY_CHANGE";
  
  public static final String ACTION_PROMPT_LOST_VALIDATION = "android.net.conn.PROMPT_LOST_VALIDATION";
  
  public static final String ACTION_PROMPT_PARTIAL_CONNECTIVITY = "android.net.conn.PROMPT_PARTIAL_CONNECTIVITY";
  
  public static final String ACTION_PROMPT_UNVALIDATED = "android.net.conn.PROMPT_UNVALIDATED";
  
  public static final String ACTION_RESTRICT_BACKGROUND_CHANGED = "android.net.conn.RESTRICT_BACKGROUND_CHANGED";
  
  public static final String ACTION_TETHER_STATE_CHANGED = "android.net.conn.TETHER_STATE_CHANGED";
  
  private static final NetworkRequest ALREADY_UNREGISTERED;
  
  private static final int BASE = 524288;
  
  public static final int CALLBACK_AVAILABLE = 524290;
  
  public static final int CALLBACK_BLK_CHANGED = 524299;
  
  public static final int CALLBACK_CAP_CHANGED = 524294;
  
  public static final int CALLBACK_IP_CHANGED = 524295;
  
  public static final int CALLBACK_LOSING = 524291;
  
  public static final int CALLBACK_LOST = 524292;
  
  public static final int CALLBACK_PRECHECK = 524289;
  
  public static final int CALLBACK_RESUMED = 524298;
  
  public static final int CALLBACK_SUSPENDED = 524297;
  
  public static final int CALLBACK_UNAVAIL = 524293;
  
  @Deprecated
  public static final String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
  
  private static final boolean DEBUG = Log.isLoggable("ConnectivityManager", 3);
  
  @Deprecated
  public static final int DEFAULT_NETWORK_PREFERENCE = 1;
  
  private static final int DEPRECATED_PHONE_CONSTANT_APN_ALREADY_ACTIVE = 0;
  
  private static final int DEPRECATED_PHONE_CONSTANT_APN_REQUEST_FAILED = 3;
  
  private static final int DEPRECATED_PHONE_CONSTANT_APN_REQUEST_STARTED = 1;
  
  private static final int EXPIRE_LEGACY_REQUEST = 524296;
  
  public static final String EXTRA_ACTIVE_LOCAL_ONLY = "android.net.extra.ACTIVE_LOCAL_ONLY";
  
  public static final String EXTRA_ACTIVE_TETHER = "tetherArray";
  
  public static final String EXTRA_ADD_TETHER_TYPE = "extraAddTetherType";
  
  public static final String EXTRA_AVAILABLE_TETHER = "availableArray";
  
  public static final String EXTRA_CAPTIVE_PORTAL = "android.net.extra.CAPTIVE_PORTAL";
  
  @SystemApi
  public static final String EXTRA_CAPTIVE_PORTAL_PROBE_SPEC = "android.net.extra.CAPTIVE_PORTAL_PROBE_SPEC";
  
  public static final String EXTRA_CAPTIVE_PORTAL_URL = "android.net.extra.CAPTIVE_PORTAL_URL";
  
  @SystemApi
  public static final String EXTRA_CAPTIVE_PORTAL_USER_AGENT = "android.net.extra.CAPTIVE_PORTAL_USER_AGENT";
  
  public static final String EXTRA_DEVICE_TYPE = "deviceType";
  
  public static final String EXTRA_ERRORED_TETHER = "erroredArray";
  
  @Deprecated
  public static final String EXTRA_EXTRA_INFO = "extraInfo";
  
  public static final String EXTRA_INET_CONDITION = "inetCondition";
  
  public static final String EXTRA_IS_ACTIVE = "isActive";
  
  public static final String EXTRA_IS_CAPTIVE_PORTAL = "captivePortal";
  
  @Deprecated
  public static final String EXTRA_IS_FAILOVER = "isFailover";
  
  public static final String EXTRA_NETWORK = "android.net.extra.NETWORK";
  
  @Deprecated
  public static final String EXTRA_NETWORK_INFO = "networkInfo";
  
  public static final String EXTRA_NETWORK_REQUEST = "android.net.extra.NETWORK_REQUEST";
  
  @Deprecated
  public static final String EXTRA_NETWORK_TYPE = "networkType";
  
  public static final String EXTRA_NO_CONNECTIVITY = "noConnectivity";
  
  @Deprecated
  public static final String EXTRA_OTHER_NETWORK_INFO = "otherNetwork";
  
  public static final String EXTRA_PROVISION_CALLBACK = "extraProvisionCallback";
  
  public static final String EXTRA_REALTIME_NS = "tsNanos";
  
  public static final String EXTRA_REASON = "reason";
  
  public static final String EXTRA_REM_TETHER_TYPE = "extraRemTetherType";
  
  public static final String EXTRA_RUN_PROVISION = "extraRunProvision";
  
  public static final String EXTRA_SET_ALARM = "extraSetAlarm";
  
  public static final String INET_CONDITION_ACTION = "android.net.conn.INET_CONDITION_ACTION";
  
  private static final int LISTEN = 1;
  
  public static final int MAX_NETWORK_TYPE = 18;
  
  public static final int MAX_RADIO_TYPE = 18;
  
  private static final int MIN_NETWORK_TYPE = 0;
  
  public static final int MULTIPATH_PREFERENCE_HANDOVER = 1;
  
  public static final int MULTIPATH_PREFERENCE_PERFORMANCE = 4;
  
  public static final int MULTIPATH_PREFERENCE_RELIABILITY = 2;
  
  public static final int MULTIPATH_PREFERENCE_UNMETERED = 7;
  
  public static final int NETID_UNSET = 0;
  
  public static final String PRIVATE_DNS_DEFAULT_MODE_FALLBACK = "off";
  
  public static final String PRIVATE_DNS_MODE_OFF = "off";
  
  public static final String PRIVATE_DNS_MODE_OPPORTUNISTIC = "opportunistic";
  
  public static final String PRIVATE_DNS_MODE_PROVIDER_HOSTNAME = "hostname";
  
  private static final int REQUEST = 2;
  
  public static final int REQUEST_ID_UNSET = 0;
  
  public static final int RESTRICT_BACKGROUND_STATUS_DISABLED = 1;
  
  public static final int RESTRICT_BACKGROUND_STATUS_ENABLED = 3;
  
  public static final int RESTRICT_BACKGROUND_STATUS_WHITELISTED = 2;
  
  private static final String TAG = "ConnectivityManager";
  
  @SystemApi
  public static final int TETHERING_BLUETOOTH = 2;
  
  public static final int TETHERING_INVALID = -1;
  
  @SystemApi
  public static final int TETHERING_USB = 1;
  
  @SystemApi
  public static final int TETHERING_WIFI = 0;
  
  public static final int TETHERING_WIFI_P2P = 3;
  
  @Deprecated
  public static final int TETHER_ERROR_DHCPSERVER_ERROR = 12;
  
  @Deprecated
  public static final int TETHER_ERROR_DISABLE_NAT_ERROR = 9;
  
  @Deprecated
  public static final int TETHER_ERROR_ENABLE_NAT_ERROR = 8;
  
  @SystemApi
  @Deprecated
  public static final int TETHER_ERROR_ENTITLEMENT_UNKONWN = 13;
  
  @Deprecated
  public static final int TETHER_ERROR_IFACE_CFG_ERROR = 10;
  
  @Deprecated
  public static final int TETHER_ERROR_MASTER_ERROR = 5;
  
  @SystemApi
  @Deprecated
  public static final int TETHER_ERROR_NO_ERROR = 0;
  
  @SystemApi
  @Deprecated
  public static final int TETHER_ERROR_PROVISION_FAILED = 11;
  
  @Deprecated
  public static final int TETHER_ERROR_SERVICE_UNAVAIL = 2;
  
  @Deprecated
  public static final int TETHER_ERROR_TETHER_IFACE_ERROR = 6;
  
  @Deprecated
  public static final int TETHER_ERROR_UNAVAIL_IFACE = 4;
  
  @Deprecated
  public static final int TETHER_ERROR_UNKNOWN_IFACE = 1;
  
  @Deprecated
  public static final int TETHER_ERROR_UNSUPPORTED = 3;
  
  @Deprecated
  public static final int TETHER_ERROR_UNTETHER_IFACE_ERROR = 7;
  
  @Deprecated
  public static final int TYPE_BLUETOOTH = 7;
  
  @Deprecated
  public static final int TYPE_DUMMY = 8;
  
  @Deprecated
  public static final int TYPE_ETHERNET = 9;
  
  @Deprecated
  public static final int TYPE_MOBILE = 0;
  
  @Deprecated
  public static final int TYPE_MOBILE_CBS = 12;
  
  @Deprecated
  public static final int TYPE_MOBILE_DUN = 4;
  
  @Deprecated
  public static final int TYPE_MOBILE_EMERGENCY = 15;
  
  @Deprecated
  public static final int TYPE_MOBILE_FOTA = 10;
  
  @Deprecated
  public static final int TYPE_MOBILE_HIPRI = 5;
  
  @Deprecated
  public static final int TYPE_MOBILE_IA = 14;
  
  @Deprecated
  public static final int TYPE_MOBILE_IMS = 11;
  
  @Deprecated
  public static final int TYPE_MOBILE_MMS = 2;
  
  @Deprecated
  public static final int TYPE_MOBILE_SUPL = 3;
  
  @SystemApi
  public static final int TYPE_NONE = -1;
  
  @Deprecated
  public static final int TYPE_PROXY = 16;
  
  @Deprecated
  public static final int TYPE_TEST = 18;
  
  @Deprecated
  public static final int TYPE_VPN = 17;
  
  @Deprecated
  public static final int TYPE_WIFI = 1;
  
  @SystemApi
  @Deprecated
  public static final int TYPE_WIFI_P2P = 13;
  
  @Deprecated
  public static final int TYPE_WIMAX = 6;
  
  private static final boolean VDBG = false;
  
  private static CallbackHandler sCallbackHandler;
  
  private static final HashMap<NetworkRequest, NetworkCallback> sCallbacks;
  
  private static ConnectivityManager sInstance;
  
  private static final HashMap<NetworkCapabilities, LegacyRequest> sLegacyRequests;
  
  private static final SparseIntArray sLegacyTypeToCapability;
  
  private static final SparseIntArray sLegacyTypeToTransport;
  
  private final Context mContext;
  
  private INetworkManagementService mNMService;
  
  private INetworkPolicyManager mNPManager;
  
  static {
    NetworkRequest.Builder builder = new NetworkRequest.Builder();
    ALREADY_UNREGISTERED = builder.clearCapabilities().build();
    sLegacyRequests = new HashMap<>();
    SparseIntArray sparseIntArray = new SparseIntArray();
    sparseIntArray.put(0, 0);
    sLegacyTypeToTransport.put(12, 0);
    sLegacyTypeToTransport.put(4, 0);
    sLegacyTypeToTransport.put(10, 0);
    sLegacyTypeToTransport.put(5, 0);
    sLegacyTypeToTransport.put(11, 0);
    sLegacyTypeToTransport.put(2, 0);
    sLegacyTypeToTransport.put(3, 0);
    sLegacyTypeToTransport.put(1, 1);
    sLegacyTypeToTransport.put(13, 1);
    sLegacyTypeToTransport.put(7, 2);
    sLegacyTypeToTransport.put(9, 3);
    sLegacyTypeToCapability = sparseIntArray = new SparseIntArray();
    sparseIntArray.put(12, 5);
    sLegacyTypeToCapability.put(4, 2);
    sLegacyTypeToCapability.put(10, 3);
    sLegacyTypeToCapability.put(11, 4);
    sLegacyTypeToCapability.put(2, 0);
    sLegacyTypeToCapability.put(3, 1);
    sLegacyTypeToCapability.put(13, 6);
    sCallbacks = new HashMap<>();
  }
  
  @Deprecated
  public static boolean isNetworkTypeValid(int paramInt) {
    boolean bool;
    if (paramInt >= 0 && paramInt <= 18) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @Deprecated
  public static String getNetworkTypeName(int paramInt) {
    switch (paramInt) {
      default:
        return Integer.toString(paramInt);
      case 17:
        return "VPN";
      case 16:
        return "PROXY";
      case 15:
        return "MOBILE_EMERGENCY";
      case 14:
        return "MOBILE_IA";
      case 13:
        return "WIFI_P2P";
      case 12:
        return "MOBILE_CBS";
      case 11:
        return "MOBILE_IMS";
      case 10:
        return "MOBILE_FOTA";
      case 9:
        return "ETHERNET";
      case 8:
        return "DUMMY";
      case 7:
        return "BLUETOOTH";
      case 6:
        return "WIMAX";
      case 5:
        return "MOBILE_HIPRI";
      case 4:
        return "MOBILE_DUN";
      case 3:
        return "MOBILE_SUPL";
      case 2:
        return "MOBILE_MMS";
      case 1:
        return "WIFI";
      case 0:
        return "MOBILE";
      case -1:
        break;
    } 
    return "NONE";
  }
  
  @Deprecated
  public static boolean isNetworkTypeMobile(int paramInt) {
    if (paramInt != 0 && paramInt != 2 && paramInt != 3 && paramInt != 4 && paramInt != 5 && paramInt != 14 && paramInt != 15)
      switch (paramInt) {
        default:
          return false;
        case 10:
        case 11:
        case 12:
          break;
      }  
    return true;
  }
  
  @Deprecated
  public static boolean isNetworkTypeWifi(int paramInt) {
    if (paramInt != 1 && paramInt != 13)
      return false; 
    return true;
  }
  
  @Deprecated
  public void setNetworkPreference(int paramInt) {}
  
  @Deprecated
  public int getNetworkPreference() {
    return -1;
  }
  
  @Deprecated
  public NetworkInfo getActiveNetworkInfo() {
    try {
      return this.mService.getActiveNetworkInfo();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Network getActiveNetwork() {
    try {
      return this.mService.getActiveNetwork();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Network getActiveNetworkForUid(int paramInt) {
    return getActiveNetworkForUid(paramInt, false);
  }
  
  public Network getActiveNetworkForUid(int paramInt, boolean paramBoolean) {
    try {
      return this.mService.getActiveNetworkForUid(paramInt, paramBoolean);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isAlwaysOnVpnPackageSupportedForUser(int paramInt, String paramString) {
    try {
      return this.mService.isAlwaysOnVpnPackageSupported(paramInt, paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean setAlwaysOnVpnPackageForUser(int paramInt, String paramString, boolean paramBoolean, List<String> paramList) {
    try {
      return this.mService.setAlwaysOnVpnPackage(paramInt, paramString, paramBoolean, paramList);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String getAlwaysOnVpnPackageForUser(int paramInt) {
    try {
      return this.mService.getAlwaysOnVpnPackage(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isVpnLockdownEnabled(int paramInt) {
    try {
      return this.mService.isVpnLockdownEnabled(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<String> getVpnLockdownWhitelist(int paramInt) {
    try {
      return this.mService.getVpnLockdownWhitelist(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public NetworkInfo getActiveNetworkInfoForUid(int paramInt) {
    return getActiveNetworkInfoForUid(paramInt, false);
  }
  
  public NetworkInfo getActiveNetworkInfoForUid(int paramInt, boolean paramBoolean) {
    try {
      return this.mService.getActiveNetworkInfoForUid(paramInt, paramBoolean);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public NetworkInfo getNetworkInfo(int paramInt) {
    try {
      return this.mService.getNetworkInfo(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public NetworkInfo getNetworkInfo(Network paramNetwork) {
    return getNetworkInfoForUid(paramNetwork, Process.myUid(), false);
  }
  
  public NetworkInfo getNetworkInfoForUid(Network paramNetwork, int paramInt, boolean paramBoolean) {
    try {
      return this.mService.getNetworkInfoForUid(paramNetwork, paramInt, paramBoolean);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public NetworkInfo[] getAllNetworkInfo() {
    try {
      return this.mService.getAllNetworkInfo();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public Network getNetworkForType(int paramInt) {
    try {
      return this.mService.getNetworkForType(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Network[] getAllNetworks() {
    try {
      return this.mService.getAllNetworks();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public NetworkCapabilities[] getDefaultNetworkCapabilitiesForUser(int paramInt) {
    try {
      IConnectivityManager iConnectivityManager = this.mService;
      Context context = this.mContext;
      String str = context.getOpPackageName();
      return iConnectivityManager.getDefaultNetworkCapabilitiesForUser(paramInt, str);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public LinkProperties getActiveLinkProperties() {
    try {
      return this.mService.getActiveLinkProperties();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public LinkProperties getLinkProperties(int paramInt) {
    try {
      return this.mService.getLinkPropertiesForType(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public LinkProperties getLinkProperties(Network paramNetwork) {
    try {
      return this.mService.getLinkProperties(paramNetwork);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public NetworkCapabilities getNetworkCapabilities(Network paramNetwork) {
    try {
      return this.mService.getNetworkCapabilities(paramNetwork, this.mContext.getOpPackageName());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  @Deprecated
  public String getCaptivePortalServerUrl() {
    try {
      return this.mService.getCaptivePortalServerUrl();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public int startUsingNetworkFeature(int paramInt, String paramString) {
    HashMap<NetworkCapabilities, LegacyRequest> hashMap;
    StringBuilder stringBuilder;
    checkLegacyRoutingApiAccess();
    NetworkCapabilities networkCapabilities = networkCapabilitiesForFeature(paramInt, paramString);
    if (networkCapabilities == null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Can't satisfy startUsingNetworkFeature for ");
      stringBuilder1.append(paramInt);
      stringBuilder1.append(", ");
      stringBuilder1.append(paramString);
      Log.d("ConnectivityManager", stringBuilder1.toString());
      return 3;
    } 
    synchronized (sLegacyRequests) {
      StringBuilder stringBuilder1;
      LegacyRequest legacyRequest = sLegacyRequests.get(networkCapabilities);
      if (legacyRequest != null) {
        stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("renewing startUsingNetworkFeature request ");
        stringBuilder1.append(legacyRequest.networkRequest);
        Log.d("ConnectivityManager", stringBuilder1.toString());
        renewRequestLocked(legacyRequest);
        if (legacyRequest.currentNetwork != null)
          return 0; 
        return 1;
      } 
      NetworkRequest networkRequest = requestNetworkForFeatureLocked((NetworkCapabilities)stringBuilder1);
      if (networkRequest != null) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("starting startUsingNetworkFeature for request ");
        stringBuilder.append(networkRequest);
        Log.d("ConnectivityManager", stringBuilder.toString());
        return 1;
      } 
      Log.d("ConnectivityManager", " request Failed");
      return 3;
    } 
  }
  
  @Deprecated
  public int stopUsingNetworkFeature(int paramInt, String paramString) {
    StringBuilder stringBuilder;
    checkLegacyRoutingApiAccess();
    NetworkCapabilities networkCapabilities = networkCapabilitiesForFeature(paramInt, paramString);
    if (networkCapabilities == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Can't satisfy stopUsingNetworkFeature for ");
      stringBuilder.append(paramInt);
      stringBuilder.append(", ");
      stringBuilder.append(paramString);
      Log.d("ConnectivityManager", stringBuilder.toString());
      return -1;
    } 
    if (removeRequestForFeature((NetworkCapabilities)stringBuilder)) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("stopUsingNetworkFeature for ");
      stringBuilder.append(paramInt);
      stringBuilder.append(", ");
      stringBuilder.append(paramString);
      Log.d("ConnectivityManager", stringBuilder.toString());
    } 
    return 1;
  }
  
  private NetworkCapabilities networkCapabilitiesForFeature(int paramInt, String paramString) {
    boolean bool = true;
    if (paramInt == 0) {
      switch (paramString.hashCode()) {
        default:
          paramInt = -1;
          break;
        case 1998933033:
          if (paramString.equals("enableDUNAlways")) {
            paramInt = 2;
            break;
          } 
        case 1893183457:
          if (paramString.equals("enableSUPL")) {
            paramInt = 7;
            break;
          } 
        case 1892790521:
          if (paramString.equals("enableFOTA")) {
            paramInt = 3;
            break;
          } 
        case -631672240:
          if (paramString.equals("enableMMS")) {
            paramInt = 6;
            break;
          } 
        case -631676084:
          if (paramString.equals("enableIMS")) {
            paramInt = 5;
            break;
          } 
        case -631680646:
          if (paramString.equals("enableDUN")) {
            paramInt = bool;
            break;
          } 
        case -631682191:
          if (paramString.equals("enableCBS")) {
            paramInt = 0;
            break;
          } 
        case -1451370941:
          if (paramString.equals("enableHIPRI")) {
            paramInt = 4;
            break;
          } 
      } 
      switch (paramInt) {
        default:
          return null;
        case 7:
          return networkCapabilitiesForType(3);
        case 6:
          return networkCapabilitiesForType(2);
        case 5:
          return networkCapabilitiesForType(11);
        case 4:
          return networkCapabilitiesForType(5);
        case 3:
          return networkCapabilitiesForType(10);
        case 1:
        case 2:
          return networkCapabilitiesForType(4);
        case 0:
          break;
      } 
      return networkCapabilitiesForType(12);
    } 
    if (paramInt == 1 && "p2p".equals(paramString))
      return networkCapabilitiesForType(13); 
    return null;
  }
  
  private int legacyTypeForNetworkCapabilities(NetworkCapabilities paramNetworkCapabilities) {
    if (paramNetworkCapabilities == null)
      return -1; 
    if (paramNetworkCapabilities.hasCapability(5))
      return 12; 
    if (paramNetworkCapabilities.hasCapability(4))
      return 11; 
    if (paramNetworkCapabilities.hasCapability(3))
      return 10; 
    if (paramNetworkCapabilities.hasCapability(2))
      return 4; 
    if (paramNetworkCapabilities.hasCapability(1))
      return 3; 
    if (paramNetworkCapabilities.hasCapability(0))
      return 2; 
    if (paramNetworkCapabilities.hasCapability(12))
      return 5; 
    if (paramNetworkCapabilities.hasCapability(6))
      return 13; 
    return -1;
  }
  
  private static class LegacyRequest {
    Network currentNetwork;
    
    private LegacyRequest() {}
    
    int delay = -1;
    
    int expireSequenceNumber;
    
    private void clearDnsBinding() {
      if (this.currentNetwork != null) {
        this.currentNetwork = null;
        ConnectivityManager.setProcessDefaultNetworkForHostResolution(null);
      } 
    }
    
    ConnectivityManager.NetworkCallback networkCallback = (ConnectivityManager.NetworkCallback)new Object(this);
    
    NetworkCapabilities networkCapabilities;
    
    NetworkRequest networkRequest;
  }
  
  private NetworkRequest findRequestForFeature(NetworkCapabilities paramNetworkCapabilities) {
    synchronized (sLegacyRequests) {
      LegacyRequest legacyRequest = sLegacyRequests.get(paramNetworkCapabilities);
      if (legacyRequest != null)
        return legacyRequest.networkRequest; 
      return null;
    } 
  }
  
  private void renewRequestLocked(LegacyRequest paramLegacyRequest) {
    paramLegacyRequest.expireSequenceNumber++;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("renewing request to seqNum ");
    stringBuilder.append(paramLegacyRequest.expireSequenceNumber);
    Log.d("ConnectivityManager", stringBuilder.toString());
    sendExpireMsgForFeature(paramLegacyRequest.networkCapabilities, paramLegacyRequest.expireSequenceNumber, paramLegacyRequest.delay);
  }
  
  private void expireRequest(NetworkCapabilities paramNetworkCapabilities, int paramInt) {
    synchronized (sLegacyRequests) {
      LegacyRequest legacyRequest = sLegacyRequests.get(paramNetworkCapabilities);
      if (legacyRequest == null)
        return; 
      int i = legacyRequest.expireSequenceNumber;
      if (legacyRequest.expireSequenceNumber == paramInt)
        removeRequestForFeature(paramNetworkCapabilities); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("expireRequest with ");
      stringBuilder.append(i);
      stringBuilder.append(", ");
      stringBuilder.append(paramInt);
      Log.d("ConnectivityManager", stringBuilder.toString());
      return;
    } 
  }
  
  private NetworkRequest requestNetworkForFeatureLocked(NetworkCapabilities paramNetworkCapabilities) {
    int i = legacyTypeForNetworkCapabilities(paramNetworkCapabilities);
    try {
      int j = this.mService.getRestoreDefaultNetworkDelay(i);
      LegacyRequest legacyRequest = new LegacyRequest();
      legacyRequest.networkCapabilities = paramNetworkCapabilities;
      legacyRequest.delay = j;
      legacyRequest.expireSequenceNumber = 0;
      NetworkCallback networkCallback = legacyRequest.networkCallback;
      CallbackHandler callbackHandler = getDefaultHandler();
      legacyRequest.networkRequest = sendRequestForNetwork(paramNetworkCapabilities, networkCallback, 0, 2, i, callbackHandler);
      if (legacyRequest.networkRequest == null)
        return null; 
      sLegacyRequests.put(paramNetworkCapabilities, legacyRequest);
      sendExpireMsgForFeature(paramNetworkCapabilities, legacyRequest.expireSequenceNumber, j);
      return legacyRequest.networkRequest;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private void sendExpireMsgForFeature(NetworkCapabilities paramNetworkCapabilities, int paramInt1, int paramInt2) {
    if (paramInt2 >= 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("sending expire msg with seqNum ");
      stringBuilder.append(paramInt1);
      stringBuilder.append(" and delay ");
      stringBuilder.append(paramInt2);
      Log.d("ConnectivityManager", stringBuilder.toString());
      CallbackHandler callbackHandler = getDefaultHandler();
      Message message = callbackHandler.obtainMessage(524296, paramInt1, 0, paramNetworkCapabilities);
      callbackHandler.sendMessageDelayed(message, paramInt2);
    } 
  }
  
  private boolean removeRequestForFeature(NetworkCapabilities paramNetworkCapabilities) {
    synchronized (sLegacyRequests) {
      LegacyRequest legacyRequest = sLegacyRequests.remove(paramNetworkCapabilities);
      if (legacyRequest == null)
        return false; 
      unregisterNetworkCallback(legacyRequest.networkCallback);
      legacyRequest.clearDnsBinding();
      return true;
    } 
  }
  
  public static NetworkCapabilities networkCapabilitiesForType(int paramInt) {
    boolean bool;
    NetworkCapabilities networkCapabilities = new NetworkCapabilities();
    int i = sLegacyTypeToTransport.get(paramInt, -1);
    if (i != -1) {
      bool = true;
    } else {
      bool = false;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("unknown legacy type: ");
    stringBuilder.append(paramInt);
    Preconditions.checkArgument(bool, stringBuilder.toString());
    networkCapabilities.addTransportType(i);
    networkCapabilities.addCapability(sLegacyTypeToCapability.get(paramInt, 12));
    networkCapabilities.maybeMarkCapabilitiesRestricted();
    return networkCapabilities;
  }
  
  public static class PacketKeepaliveCallback {
    public void onStarted() {}
    
    public void onStopped() {}
    
    public void onError(int param1Int) {}
  }
  
  public class PacketKeepalive {
    public static final int BINDER_DIED = -10;
    
    public static final int ERROR_HARDWARE_ERROR = -31;
    
    public static final int ERROR_HARDWARE_UNSUPPORTED = -30;
    
    public static final int ERROR_INVALID_INTERVAL = -24;
    
    public static final int ERROR_INVALID_IP_ADDRESS = -21;
    
    public static final int ERROR_INVALID_LENGTH = -23;
    
    public static final int ERROR_INVALID_NETWORK = -20;
    
    public static final int ERROR_INVALID_PORT = -22;
    
    public static final int MIN_INTERVAL = 10;
    
    public static final int NATT_PORT = 4500;
    
    public static final int NO_KEEPALIVE = -1;
    
    public static final int SUCCESS = 0;
    
    private static final String TAG = "PacketKeepalive";
    
    private final ISocketKeepaliveCallback mCallback;
    
    private final ExecutorService mExecutor;
    
    private final Network mNetwork;
    
    private volatile Integer mSlot;
    
    final ConnectivityManager this$0;
    
    public void stop() {
      try {
        ExecutorService executorService = this.mExecutor;
        _$$Lambda$ConnectivityManager$PacketKeepalive$__8nwufwzyblnuYRFEYIKx7L4Vg _$$Lambda$ConnectivityManager$PacketKeepalive$__8nwufwzyblnuYRFEYIKx7L4Vg = new _$$Lambda$ConnectivityManager$PacketKeepalive$__8nwufwzyblnuYRFEYIKx7L4Vg();
        this(this);
        executorService.execute(_$$Lambda$ConnectivityManager$PacketKeepalive$__8nwufwzyblnuYRFEYIKx7L4Vg);
      } catch (RejectedExecutionException rejectedExecutionException) {}
    }
    
    private PacketKeepalive(Network param1Network, ConnectivityManager.PacketKeepaliveCallback param1PacketKeepaliveCallback) {
      Preconditions.checkNotNull(param1Network, "network cannot be null");
      Preconditions.checkNotNull(param1PacketKeepaliveCallback, "callback cannot be null");
      this.mNetwork = param1Network;
      this.mExecutor = Executors.newSingleThreadExecutor();
      this.mCallback = (ISocketKeepaliveCallback)new Object(this, ConnectivityManager.this, param1PacketKeepaliveCallback);
    }
  }
  
  public PacketKeepalive startNattKeepalive(Network paramNetwork, int paramInt1, PacketKeepaliveCallback paramPacketKeepaliveCallback, InetAddress paramInetAddress1, int paramInt2, InetAddress paramInetAddress2) {
    PacketKeepalive packetKeepalive = new PacketKeepalive(paramNetwork, paramPacketKeepaliveCallback);
    try {
      IConnectivityManager iConnectivityManager = this.mService;
      ISocketKeepaliveCallback iSocketKeepaliveCallback = packetKeepalive.mCallback;
      String str1 = paramInetAddress1.getHostAddress(), str2 = paramInetAddress2.getHostAddress();
      iConnectivityManager.startNattKeepalive(paramNetwork, paramInt1, iSocketKeepaliveCallback, str1, paramInt2, str2);
      return packetKeepalive;
    } catch (RemoteException remoteException) {
      Log.e("ConnectivityManager", "Error starting packet keepalive: ", (Throwable)remoteException);
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public SocketKeepalive createSocketKeepalive(Network paramNetwork, IpSecManager.UdpEncapsulationSocket paramUdpEncapsulationSocket, InetAddress paramInetAddress1, InetAddress paramInetAddress2, Executor paramExecutor, SocketKeepalive.Callback paramCallback) {
    ParcelFileDescriptor parcelFileDescriptor;
    try {
      parcelFileDescriptor = ParcelFileDescriptor.dup(paramUdpEncapsulationSocket.getFileDescriptor());
    } catch (IOException iOException) {
      parcelFileDescriptor = new ParcelFileDescriptor(new FileDescriptor());
    } 
    return new NattSocketKeepalive(this.mService, paramNetwork, parcelFileDescriptor, paramUdpEncapsulationSocket.getResourceId(), paramInetAddress1, paramInetAddress2, paramExecutor, paramCallback);
  }
  
  @SystemApi
  public SocketKeepalive createNattKeepalive(Network paramNetwork, ParcelFileDescriptor paramParcelFileDescriptor, InetAddress paramInetAddress1, InetAddress paramInetAddress2, Executor paramExecutor, SocketKeepalive.Callback paramCallback) {
    ParcelFileDescriptor parcelFileDescriptor;
    try {
      paramParcelFileDescriptor = paramParcelFileDescriptor.dup();
    } catch (IOException iOException) {
      parcelFileDescriptor = new ParcelFileDescriptor(new FileDescriptor());
    } 
    return new NattSocketKeepalive(this.mService, paramNetwork, parcelFileDescriptor, -1, paramInetAddress1, paramInetAddress2, paramExecutor, paramCallback);
  }
  
  @SystemApi
  public SocketKeepalive createSocketKeepalive(Network paramNetwork, Socket paramSocket, Executor paramExecutor, SocketKeepalive.Callback paramCallback) {
    ParcelFileDescriptor parcelFileDescriptor;
    try {
      parcelFileDescriptor = ParcelFileDescriptor.fromSocket(paramSocket);
    } catch (UncheckedIOException uncheckedIOException) {
      parcelFileDescriptor = new ParcelFileDescriptor(new FileDescriptor());
    } 
    return new TcpSocketKeepalive(this.mService, paramNetwork, parcelFileDescriptor, paramExecutor, paramCallback);
  }
  
  @Deprecated
  public boolean requestRouteToHost(int paramInt1, int paramInt2) {
    return requestRouteToHostAddress(paramInt1, NetworkUtils.intToInetAddress(paramInt2));
  }
  
  @Deprecated
  public boolean requestRouteToHostAddress(int paramInt, InetAddress paramInetAddress) {
    checkLegacyRoutingApiAccess();
    try {
      return this.mService.requestRouteToHostAddress(paramInt, paramInetAddress.getAddress());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public boolean getBackgroundDataSetting() {
    return true;
  }
  
  @Deprecated
  public void setBackgroundDataSetting(boolean paramBoolean) {}
  
  @Deprecated
  public NetworkQuotaInfo getActiveNetworkQuotaInfo() {
    try {
      return this.mService.getActiveNetworkQuotaInfo();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public boolean getMobileDataEnabled() {
    TelephonyManager telephonyManager = (TelephonyManager)this.mContext.getSystemService(TelephonyManager.class);
    if (telephonyManager != null) {
      int i = SubscriptionManager.getDefaultDataSubscriptionId();
      return telephonyManager.createForSubscriptionId(i).isDataEnabled();
    } 
    Log.d("ConnectivityManager", "getMobileDataEnabled()- remote exception retVal=false");
    return false;
  }
  
  private INetworkManagementService getNetworkManagementService() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mNMService : Landroid/os/INetworkManagementService;
    //   6: ifnull -> 18
    //   9: aload_0
    //   10: getfield mNMService : Landroid/os/INetworkManagementService;
    //   13: astore_1
    //   14: aload_0
    //   15: monitorexit
    //   16: aload_1
    //   17: areturn
    //   18: ldc_w 'network_management'
    //   21: invokestatic getService : (Ljava/lang/String;)Landroid/os/IBinder;
    //   24: astore_1
    //   25: aload_1
    //   26: invokestatic asInterface : (Landroid/os/IBinder;)Landroid/os/INetworkManagementService;
    //   29: astore_1
    //   30: aload_0
    //   31: aload_1
    //   32: putfield mNMService : Landroid/os/INetworkManagementService;
    //   35: aload_0
    //   36: monitorexit
    //   37: aload_1
    //   38: areturn
    //   39: astore_1
    //   40: aload_0
    //   41: monitorexit
    //   42: aload_1
    //   43: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2165	-> 0
    //   #2166	-> 2
    //   #2167	-> 9
    //   #2169	-> 18
    //   #2170	-> 25
    //   #2171	-> 35
    //   #2172	-> 39
    // Exception table:
    //   from	to	target	type
    //   2	9	39	finally
    //   9	16	39	finally
    //   18	25	39	finally
    //   25	35	39	finally
    //   35	37	39	finally
    //   40	42	39	finally
  }
  
  private final ArrayMap<OnNetworkActiveListener, INetworkActivityListener> mNetworkActivityListeners = new ArrayMap();
  
  private final IConnectivityManager mService;
  
  private final ArrayMap<OnTetheringEventCallback, TetheringManager.TetheringEventCallback> mTetheringEventCallbacks;
  
  private final TetheringManager mTetheringManager;
  
  public void addDefaultNetworkActiveListener(OnNetworkActiveListener paramOnNetworkActiveListener) {
    Object object = new Object(this, paramOnNetworkActiveListener);
    try {
      getNetworkManagementService().registerNetworkActivityListener((INetworkActivityListener)object);
      this.mNetworkActivityListeners.put(paramOnNetworkActiveListener, object);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void removeDefaultNetworkActiveListener(OnNetworkActiveListener paramOnNetworkActiveListener) {
    boolean bool;
    INetworkActivityListener iNetworkActivityListener = (INetworkActivityListener)this.mNetworkActivityListeners.get(paramOnNetworkActiveListener);
    if (iNetworkActivityListener != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "Listener was not registered.");
    try {
      getNetworkManagementService().unregisterNetworkActivityListener(iNetworkActivityListener);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isDefaultNetworkActive() {
    try {
      return getNetworkManagementService().isNetworkActive();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static ConnectivityManager from(Context paramContext) {
    return (ConnectivityManager)paramContext.getSystemService("connectivity");
  }
  
  public NetworkRequest getDefaultRequest() {
    try {
      return this.mService.getDefaultRequest();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static final void enforceChangePermission(Context paramContext) {
    int i = Binder.getCallingUid();
    String str = Settings.getPackageNameForUid(paramContext, i);
    Settings.checkAndNoteChangeNetworkStateOperation(paramContext, i, str, true);
  }
  
  @Deprecated
  static ConnectivityManager getInstanceOrNull() {
    return sInstance;
  }
  
  @Deprecated
  private static ConnectivityManager getInstance() {
    if (getInstanceOrNull() != null)
      return getInstanceOrNull(); 
    throw new IllegalStateException("No ConnectivityManager yet constructed");
  }
  
  @Deprecated
  public String[] getTetherableIfaces() {
    return this.mTetheringManager.getTetherableIfaces();
  }
  
  @Deprecated
  public String[] getTetheredIfaces() {
    return this.mTetheringManager.getTetheredIfaces();
  }
  
  @Deprecated
  public String[] getTetheringErroredIfaces() {
    return this.mTetheringManager.getTetheringErroredIfaces();
  }
  
  @Deprecated
  public String[] getTetheredDhcpRanges() {
    throw new UnsupportedOperationException("getTetheredDhcpRanges is not supported");
  }
  
  @Deprecated
  public int tether(String paramString) {
    return this.mTetheringManager.tether(paramString);
  }
  
  @Deprecated
  public int untether(String paramString) {
    return this.mTetheringManager.untether(paramString);
  }
  
  @SystemApi
  public boolean isTetheringSupported() {
    return this.mTetheringManager.isTetheringSupported();
  }
  
  @SystemApi
  @Deprecated
  public static abstract class OnStartTetheringCallback {
    public void onTetheringStarted() {}
    
    public void onTetheringFailed() {}
  }
  
  @SystemApi
  @Deprecated
  public void startTethering(int paramInt, boolean paramBoolean, OnStartTetheringCallback paramOnStartTetheringCallback) {
    String str = this.mContext.getOpPackageName();
    long l = Binder.clearCallingIdentity();
    try {
      if ("com.coloros.backuprestore".equals(str))
        Settings.Global.putString(this.mContext.getContentResolver(), "backup_restore_hotspot", "true"); 
      Binder.restoreCallingIdentity(l);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  @SystemApi
  @Deprecated
  public void startTethering(int paramInt, boolean paramBoolean, final OnStartTetheringCallback callback, final Handler handler) {
    Preconditions.checkNotNull(callback, "OnStartTetheringCallback cannot be null.");
    Executor executor = new Executor() {
        final ConnectivityManager this$0;
        
        final Handler val$handler;
        
        public void execute(Runnable param1Runnable) {
          Handler handler = handler;
          if (handler == null) {
            param1Runnable.run();
          } else {
            handler.post(param1Runnable);
          } 
        }
      };
    TetheringManager.StartTetheringCallback startTetheringCallback = new TetheringManager.StartTetheringCallback() {
        final ConnectivityManager this$0;
        
        final ConnectivityManager.OnStartTetheringCallback val$callback;
        
        public void onTetheringStarted() {
          callback.onTetheringStarted();
        }
        
        public void onTetheringFailed(int param1Int) {
          callback.onTetheringFailed();
        }
      };
    TetheringManager.TetheringRequest.Builder builder = new TetheringManager.TetheringRequest.Builder(paramInt);
    TetheringManager.TetheringRequest tetheringRequest = builder.setShouldShowEntitlementUi(paramBoolean).build();
    this.mTetheringManager.startTethering(tetheringRequest, executor, startTetheringCallback);
  }
  
  @SystemApi
  @Deprecated
  public void stopTethering(int paramInt) {
    null = this.mContext.getOpPackageName();
    long l = Binder.clearCallingIdentity();
    try {
      if ("com.coloros.backuprestore".equals(null))
        Settings.Global.putString(this.mContext.getContentResolver(), "backup_restore_hotspot", "false"); 
      Binder.restoreCallingIdentity(l);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  @SystemApi
  @Deprecated
  public static abstract class OnTetheringEventCallback {
    public void onUpstreamChanged(Network param1Network) {}
  }
  
  public ConnectivityManager(Context paramContext, IConnectivityManager paramIConnectivityManager) {
    this.mTetheringEventCallbacks = new ArrayMap();
    this.mContext = (Context)Preconditions.checkNotNull(paramContext, "missing context");
    this.mService = (IConnectivityManager)Preconditions.checkNotNull(paramIConnectivityManager, "missing IConnectivityManager");
    this.mTetheringManager = (TetheringManager)this.mContext.getSystemService("tethering");
    sInstance = this;
  }
  
  @SystemApi
  @Deprecated
  public void registerTetheringEventCallback(Executor paramExecutor, final OnTetheringEventCallback callback) {
    Preconditions.checkNotNull(callback, "OnTetheringEventCallback cannot be null.");
    TetheringManager.TetheringEventCallback tetheringEventCallback = new TetheringManager.TetheringEventCallback() {
        final ConnectivityManager this$0;
        
        final ConnectivityManager.OnTetheringEventCallback val$callback;
        
        public void onUpstreamChanged(Network param1Network) {
          callback.onUpstreamChanged(param1Network);
        }
      };
    synchronized (this.mTetheringEventCallbacks) {
      this.mTetheringEventCallbacks.put(callback, tetheringEventCallback);
      this.mTetheringManager.registerTetheringEventCallback(paramExecutor, tetheringEventCallback);
      return;
    } 
  }
  
  @SystemApi
  @Deprecated
  public void unregisterTetheringEventCallback(OnTetheringEventCallback paramOnTetheringEventCallback) {
    Objects.requireNonNull(paramOnTetheringEventCallback, "The callback must be non-null");
    synchronized (this.mTetheringEventCallbacks) {
      ArrayMap<OnTetheringEventCallback, TetheringManager.TetheringEventCallback> arrayMap = this.mTetheringEventCallbacks;
      TetheringManager.TetheringEventCallback tetheringEventCallback = (TetheringManager.TetheringEventCallback)arrayMap.remove(paramOnTetheringEventCallback);
      this.mTetheringManager.unregisterTetheringEventCallback(tetheringEventCallback);
      return;
    } 
  }
  
  @Deprecated
  public String[] getTetherableUsbRegexs() {
    return this.mTetheringManager.getTetherableUsbRegexs();
  }
  
  @Deprecated
  public String[] getTetherableWifiRegexs() {
    return this.mTetheringManager.getTetherableWifiRegexs();
  }
  
  @Deprecated
  public String[] getTetherableBluetoothRegexs() {
    return this.mTetheringManager.getTetherableBluetoothRegexs();
  }
  
  @Deprecated
  public int setUsbTethering(boolean paramBoolean) {
    return this.mTetheringManager.setUsbTethering(paramBoolean);
  }
  
  @Deprecated
  public int getLastTetherError(String paramString) {
    int i = this.mTetheringManager.getLastTetherError(paramString);
    int j = i;
    if (i == 16)
      j = 1; 
    return j;
  }
  
  @SystemApi
  @Deprecated
  public void getLatestTetheringEntitlementResult(int paramInt, boolean paramBoolean, Executor paramExecutor, OnTetheringEntitlementResultListener paramOnTetheringEntitlementResultListener) {
    Preconditions.checkNotNull(paramOnTetheringEntitlementResultListener, "TetheringEntitlementResultListener cannot be null.");
    Object object = new Object(this, null, paramExecutor, paramOnTetheringEntitlementResultListener);
    this.mTetheringManager.requestLatestTetheringEntitlementResult(paramInt, (ResultReceiver)object, paramBoolean);
  }
  
  public void reportInetCondition(int paramInt1, int paramInt2) {
    printStackTrace();
    try {
      this.mService.reportInetCondition(paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void reportBadNetwork(Network paramNetwork) {
    printStackTrace();
    try {
      this.mService.reportNetworkConnectivity(paramNetwork, true);
      this.mService.reportNetworkConnectivity(paramNetwork, false);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportNetworkConnectivity(Network paramNetwork, boolean paramBoolean) {
    printStackTrace();
    try {
      this.mService.reportNetworkConnectivity(paramNetwork, paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setGlobalProxy(ProxyInfo paramProxyInfo) {
    try {
      this.mService.setGlobalProxy(paramProxyInfo);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public ProxyInfo getGlobalProxy() {
    try {
      return this.mService.getGlobalProxy();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public ProxyInfo getProxyForNetwork(Network paramNetwork) {
    try {
      return this.mService.getProxyForNetwork(paramNetwork);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public ProxyInfo getDefaultProxy() {
    return getProxyForNetwork(getBoundNetworkForProcess());
  }
  
  @Deprecated
  public boolean isNetworkSupported(int paramInt) {
    try {
      return this.mService.isNetworkSupported(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isActiveNetworkMetered() {
    try {
      return this.mService.isActiveNetworkMetered();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean updateLockdownVpn() {
    try {
      return this.mService.updateLockdownVpn();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int checkMobileProvisioning(int paramInt) {
    try {
      paramInt = this.mService.checkMobileProvisioning(paramInt);
      return paramInt;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String getMobileProvisioningUrl() {
    try {
      return this.mService.getMobileProvisioningUrl();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void setProvisioningNotificationVisible(boolean paramBoolean, int paramInt, String paramString) {
    try {
      this.mService.setProvisioningNotificationVisible(paramBoolean, paramInt, paramString);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void setAirplaneMode(boolean paramBoolean) {
    try {
      OplusDevicepolicyManager oplusDevicepolicyManager = OplusDevicepolicyManager.getInstance();
      if (oplusDevicepolicyManager != null) {
        int i = -1;
        String str = oplusDevicepolicyManager.getData("persist.sys.airplane_policy", 1);
        if (TextUtils.isEmpty(str)) {
          i = 2;
        } else if (TextUtils.isDigitsOnly(str)) {
          i = Integer.parseInt(str);
        } 
        if (i != -1 && ((i == 0 && paramBoolean) || (i == 1 && !paramBoolean))) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("disable change airplane mode airplaneMode = ");
          stringBuilder.append(i);
          stringBuilder.append(" enable mode = ");
          stringBuilder.append(paramBoolean);
          stringBuilder.append(" just return");
          Log.d("ConnectivityManager", stringBuilder.toString());
          return;
        } 
      } 
      this.mService.setAirplaneMode(paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int registerNetworkFactory(Messenger paramMessenger, String paramString) {
    try {
      return this.mService.registerNetworkFactory(paramMessenger, paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void unregisterNetworkFactory(Messenger paramMessenger) {
    try {
      this.mService.unregisterNetworkFactory(paramMessenger);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public int registerNetworkProvider(NetworkProvider paramNetworkProvider) {
    if (paramNetworkProvider.getProviderId() == -1)
      try {
        IConnectivityManager iConnectivityManager = this.mService;
        Messenger messenger = paramNetworkProvider.getMessenger();
        String str = paramNetworkProvider.getName();
        int i = iConnectivityManager.registerNetworkProvider(messenger, str);
        paramNetworkProvider.setProviderId(i);
        return paramNetworkProvider.getProviderId();
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    throw new IllegalStateException("NetworkProviders can only be registered once");
  }
  
  @SystemApi
  public void unregisterNetworkProvider(NetworkProvider paramNetworkProvider) {
    try {
      this.mService.unregisterNetworkProvider(paramNetworkProvider.getMessenger());
      paramNetworkProvider.setProviderId(-1);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void declareNetworkRequestUnfulfillable(NetworkRequest paramNetworkRequest) {
    try {
      this.mService.declareNetworkRequestUnfulfillable(paramNetworkRequest);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Network registerNetworkAgent(Messenger paramMessenger, NetworkInfo paramNetworkInfo, LinkProperties paramLinkProperties, NetworkCapabilities paramNetworkCapabilities, int paramInt, NetworkAgentConfig paramNetworkAgentConfig) {
    return registerNetworkAgent(paramMessenger, paramNetworkInfo, paramLinkProperties, paramNetworkCapabilities, paramInt, paramNetworkAgentConfig, -1);
  }
  
  public Network registerNetworkAgent(Messenger paramMessenger, NetworkInfo paramNetworkInfo, LinkProperties paramLinkProperties, NetworkCapabilities paramNetworkCapabilities, int paramInt1, NetworkAgentConfig paramNetworkAgentConfig, int paramInt2) {
    try {
      return this.mService.registerNetworkAgent(paramMessenger, paramNetworkInfo, paramLinkProperties, paramNetworkCapabilities, paramInt1, paramNetworkAgentConfig, paramInt2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static class NetworkCallback {
    private NetworkRequest networkRequest;
    
    public void onPreCheck(Network param1Network) {}
    
    public void onAvailable(Network param1Network, NetworkCapabilities param1NetworkCapabilities, LinkProperties param1LinkProperties, boolean param1Boolean) {
      onAvailable(param1Network);
      if (!param1NetworkCapabilities.hasCapability(21))
        onNetworkSuspended(param1Network); 
      onCapabilitiesChanged(param1Network, param1NetworkCapabilities);
      onLinkPropertiesChanged(param1Network, param1LinkProperties);
      onBlockedStatusChanged(param1Network, param1Boolean);
    }
    
    public void onAvailable(Network param1Network) {}
    
    public void onLosing(Network param1Network, int param1Int) {}
    
    public void onLost(Network param1Network) {}
    
    public void onUnavailable() {}
    
    public void onCapabilitiesChanged(Network param1Network, NetworkCapabilities param1NetworkCapabilities) {}
    
    public void onLinkPropertiesChanged(Network param1Network, LinkProperties param1LinkProperties) {}
    
    public void onNetworkSuspended(Network param1Network) {}
    
    public void onNetworkResumed(Network param1Network) {}
    
    public void onBlockedStatusChanged(Network param1Network, boolean param1Boolean) {}
  }
  
  public static class TooManyRequestsException extends RuntimeException {}
  
  private static RuntimeException convertServiceException(ServiceSpecificException paramServiceSpecificException) {
    if (paramServiceSpecificException.errorCode != 1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown service error code ");
      stringBuilder.append(paramServiceSpecificException.errorCode);
      Log.w("ConnectivityManager", stringBuilder.toString());
      return new RuntimeException(paramServiceSpecificException);
    } 
    return new TooManyRequestsException();
  }
  
  public static String getCallbackName(int paramInt) {
    switch (paramInt) {
      default:
        return Integer.toString(paramInt);
      case 524299:
        return "CALLBACK_BLK_CHANGED";
      case 524298:
        return "CALLBACK_RESUMED";
      case 524297:
        return "CALLBACK_SUSPENDED";
      case 524296:
        return "EXPIRE_LEGACY_REQUEST";
      case 524295:
        return "CALLBACK_IP_CHANGED";
      case 524294:
        return "CALLBACK_CAP_CHANGED";
      case 524293:
        return "CALLBACK_UNAVAIL";
      case 524292:
        return "CALLBACK_LOST";
      case 524291:
        return "CALLBACK_LOSING";
      case 524290:
        return "CALLBACK_AVAILABLE";
      case 524289:
        break;
    } 
    return "CALLBACK_PRECHECK";
  }
  
  class CallbackHandler extends Handler {
    private static final boolean DBG = false;
    
    private static final String TAG = "ConnectivityManager.CallbackHandler";
    
    final ConnectivityManager this$0;
    
    CallbackHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    CallbackHandler(Handler param1Handler) {
      this(((Handler)Preconditions.checkNotNull(param1Handler, "Handler cannot be null.")).getLooper());
    }
    
    public void handleMessage(Message param1Message) {
      HashMap hashMap;
      LinkProperties linkProperties;
      if (param1Message.what == 524296) {
        ConnectivityManager.this.expireRequest((NetworkCapabilities)param1Message.obj, param1Message.arg1);
        return;
      } 
      NetworkRequest networkRequest = getObject(param1Message, NetworkRequest.class);
      Network network = getObject(param1Message, Network.class);
      synchronized (ConnectivityManager.sCallbacks) {
        String str;
        LinkProperties linkProperties1;
        NetworkCapabilities networkCapabilities1, networkCapabilities2;
        StringBuilder stringBuilder;
        ConnectivityManager.NetworkCallback networkCallback = (ConnectivityManager.NetworkCallback)ConnectivityManager.sCallbacks.get(networkRequest);
        if (networkCallback == null) {
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("callback not found for ");
          int j = param1Message.what;
          stringBuilder.append(ConnectivityManager.getCallbackName(j));
          stringBuilder.append(" message");
          str = stringBuilder.toString();
          Log.w("ConnectivityManager.CallbackHandler", str);
          return;
        } 
        if (((Message)str).what == 524293) {
          ConnectivityManager.sCallbacks.remove(networkRequest);
          ConnectivityManager.NetworkCallback.access$902((ConnectivityManager.NetworkCallback)stringBuilder, ConnectivityManager.ALREADY_UNREGISTERED);
        } 
        int i = ((Message)str).what;
        boolean bool1 = true, bool2 = true;
        switch (i) {
          default:
            return;
          case 524299:
            if (((Message)str).arg1 == 0)
              bool2 = false; 
            stringBuilder.onBlockedStatusChanged(network, bool2);
          case 524298:
            stringBuilder.onNetworkResumed(network);
          case 524297:
            stringBuilder.onNetworkSuspended(network);
          case 524295:
            linkProperties1 = getObject((Message)str, LinkProperties.class);
            stringBuilder.onLinkPropertiesChanged(network, linkProperties1);
          case 524294:
            networkCapabilities1 = getObject((Message)linkProperties1, NetworkCapabilities.class);
            stringBuilder.onCapabilitiesChanged(network, networkCapabilities1);
          case 524293:
            stringBuilder.onUnavailable();
          case 524292:
            stringBuilder.onLost(network);
          case 524291:
            stringBuilder.onLosing(network, ((Message)networkCapabilities1).arg1);
          case 524290:
            networkCapabilities2 = getObject((Message)networkCapabilities1, NetworkCapabilities.class);
            linkProperties = getObject((Message)networkCapabilities1, LinkProperties.class);
            if (((Message)networkCapabilities1).arg1 != 0) {
              bool2 = bool1;
            } else {
              bool2 = false;
            } 
            stringBuilder.onAvailable(network, networkCapabilities2, linkProperties, bool2);
          case 524289:
            break;
        } 
        stringBuilder.onPreCheck(network);
      } 
    }
    
    private <T> T getObject(Message param1Message, Class<T> param1Class) {
      return (T)param1Message.getData().getParcelable(param1Class.getSimpleName());
    }
  }
  
  private CallbackHandler getDefaultHandler() {
    synchronized (sCallbacks) {
      if (sCallbackHandler == null) {
        CallbackHandler callbackHandler = new CallbackHandler();
        this(this, ConnectivityThread.getInstanceLooper());
        sCallbackHandler = callbackHandler;
      } 
      return sCallbackHandler;
    } 
  }
  
  private NetworkRequest sendRequestForNetwork(NetworkCapabilities paramNetworkCapabilities, NetworkCallback paramNetworkCallback, int paramInt1, int paramInt2, int paramInt3, CallbackHandler paramCallbackHandler) {
    boolean bool;
    printStackTrace();
    checkCallbackNotNull(paramNetworkCallback);
    if (paramInt2 == 2 || paramNetworkCapabilities != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "null NetworkCapabilities");
    String str = this.mContext.getOpPackageName();
    try {
      HashMap<NetworkRequest, NetworkCallback> hashMap = sCallbacks;
      /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/util/HashMap<[ObjectType{android/net/NetworkRequest}, InnerObjectType{ObjectType{android/net/ConnectivityManager}.Landroid/net/ConnectivityManager$NetworkCallback;}]>}, name=null} */
      try {
        if (paramNetworkCallback.networkRequest != null && 
          paramNetworkCallback.networkRequest != ALREADY_UNREGISTERED)
          Log.e("ConnectivityManager", "NetworkCallback was already registered"); 
        Messenger messenger = new Messenger();
        try {
          NetworkRequest networkRequest;
          this(paramCallbackHandler);
          Binder binder = new Binder();
          this();
          if (paramInt2 == 1) {
            networkRequest = this.mService.listenForNetwork(paramNetworkCapabilities, messenger, binder, str);
          } else {
            networkRequest = this.mService.requestNetwork((NetworkCapabilities)networkRequest, messenger, paramInt1, binder, paramInt3, str);
          } 
          if (networkRequest != null)
            sCallbacks.put(networkRequest, paramNetworkCallback); 
          NetworkCallback.access$902(paramNetworkCallback, networkRequest);
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/util/HashMap<[ObjectType{android/net/NetworkRequest}, InnerObjectType{ObjectType{android/net/ConnectivityManager}.Landroid/net/ConnectivityManager$NetworkCallback;}]>}, name=null} */
          return networkRequest;
        } finally {}
      } finally {}
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/util/HashMap<[ObjectType{android/net/NetworkRequest}, InnerObjectType{ObjectType{android/net/ConnectivityManager}.Landroid/net/ConnectivityManager$NetworkCallback;}]>}, name=null} */
      try {
        throw paramNetworkCapabilities;
      } catch (RemoteException remoteException) {
      
      } catch (ServiceSpecificException null) {}
    } catch (RemoteException remoteException) {
    
    } catch (ServiceSpecificException serviceSpecificException) {
      throw convertServiceException(serviceSpecificException);
    } 
    throw serviceSpecificException.rethrowFromSystemServer();
  }
  
  @SystemApi
  public void requestNetwork(NetworkRequest paramNetworkRequest, int paramInt1, int paramInt2, Handler paramHandler, NetworkCallback paramNetworkCallback) {
    if (paramInt2 != -1) {
      paramHandler = new CallbackHandler(paramHandler);
      NetworkCapabilities networkCapabilities = paramNetworkRequest.networkCapabilities;
      sendRequestForNetwork(networkCapabilities, paramNetworkCallback, paramInt1, 2, paramInt2, (CallbackHandler)paramHandler);
      return;
    } 
    throw new IllegalArgumentException("TYPE_NONE is meaningless legacy type");
  }
  
  public void requestNetwork(NetworkRequest paramNetworkRequest, NetworkCallback paramNetworkCallback) {
    requestNetwork(paramNetworkRequest, paramNetworkCallback, getDefaultHandler());
  }
  
  public void requestNetwork(NetworkRequest paramNetworkRequest, NetworkCallback paramNetworkCallback, Handler paramHandler) {
    paramHandler = new CallbackHandler(paramHandler);
    NetworkCapabilities networkCapabilities = paramNetworkRequest.networkCapabilities;
    sendRequestForNetwork(networkCapabilities, paramNetworkCallback, 0, 2, -1, (CallbackHandler)paramHandler);
  }
  
  public void requestNetwork(NetworkRequest paramNetworkRequest, NetworkCallback paramNetworkCallback, int paramInt) {
    checkTimeout(paramInt);
    NetworkCapabilities networkCapabilities = paramNetworkRequest.networkCapabilities;
    CallbackHandler callbackHandler = getDefaultHandler();
    sendRequestForNetwork(networkCapabilities, paramNetworkCallback, paramInt, 2, -1, callbackHandler);
  }
  
  public void requestNetwork(NetworkRequest paramNetworkRequest, NetworkCallback paramNetworkCallback, Handler paramHandler, int paramInt) {
    checkTimeout(paramInt);
    paramHandler = new CallbackHandler(paramHandler);
    NetworkCapabilities networkCapabilities = paramNetworkRequest.networkCapabilities;
    sendRequestForNetwork(networkCapabilities, paramNetworkCallback, paramInt, 2, -1, (CallbackHandler)paramHandler);
  }
  
  public void requestNetwork(NetworkRequest paramNetworkRequest, PendingIntent paramPendingIntent) {
    printStackTrace();
    checkPendingIntentNotNull(paramPendingIntent);
    try {
      IConnectivityManager iConnectivityManager = this.mService;
      NetworkCapabilities networkCapabilities = paramNetworkRequest.networkCapabilities;
      Context context = this.mContext;
      String str = context.getOpPackageName();
      iConnectivityManager.pendingRequestForNetwork(networkCapabilities, paramPendingIntent, str);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw convertServiceException(serviceSpecificException);
    } 
  }
  
  public void releaseNetworkRequest(PendingIntent paramPendingIntent) {
    printStackTrace();
    checkPendingIntentNotNull(paramPendingIntent);
    try {
      this.mService.releasePendingNetworkRequest(paramPendingIntent);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private static void checkPendingIntentNotNull(PendingIntent paramPendingIntent) {
    Preconditions.checkNotNull(paramPendingIntent, "PendingIntent cannot be null.");
  }
  
  private static void checkCallbackNotNull(NetworkCallback paramNetworkCallback) {
    Preconditions.checkNotNull(paramNetworkCallback, "null NetworkCallback");
  }
  
  private static void checkTimeout(int paramInt) {
    Preconditions.checkArgumentPositive(paramInt, "timeoutMs must be strictly positive.");
  }
  
  public void registerNetworkCallback(NetworkRequest paramNetworkRequest, NetworkCallback paramNetworkCallback) {
    registerNetworkCallback(paramNetworkRequest, paramNetworkCallback, getDefaultHandler());
  }
  
  public void registerNetworkCallback(NetworkRequest paramNetworkRequest, NetworkCallback paramNetworkCallback, Handler paramHandler) {
    paramHandler = new CallbackHandler(paramHandler);
    NetworkCapabilities networkCapabilities = paramNetworkRequest.networkCapabilities;
    sendRequestForNetwork(networkCapabilities, paramNetworkCallback, 0, 1, -1, (CallbackHandler)paramHandler);
  }
  
  public void registerNetworkCallback(NetworkRequest paramNetworkRequest, PendingIntent paramPendingIntent) {
    printStackTrace();
    checkPendingIntentNotNull(paramPendingIntent);
    try {
      IConnectivityManager iConnectivityManager = this.mService;
      NetworkCapabilities networkCapabilities = paramNetworkRequest.networkCapabilities;
      Context context = this.mContext;
      String str = context.getOpPackageName();
      iConnectivityManager.pendingListenForNetwork(networkCapabilities, paramPendingIntent, str);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw convertServiceException(serviceSpecificException);
    } 
  }
  
  public void registerDefaultNetworkCallback(NetworkCallback paramNetworkCallback) {
    registerDefaultNetworkCallback(paramNetworkCallback, getDefaultHandler());
  }
  
  public void registerDefaultNetworkCallback(NetworkCallback paramNetworkCallback, Handler paramHandler) {
    paramHandler = new CallbackHandler(paramHandler);
    sendRequestForNetwork(null, paramNetworkCallback, 0, 2, -1, (CallbackHandler)paramHandler);
  }
  
  public boolean requestBandwidthUpdate(Network paramNetwork) {
    try {
      return this.mService.requestBandwidthUpdate(paramNetwork);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void unregisterNetworkCallback(NetworkCallback paramNetworkCallback) {
    printStackTrace();
    checkCallbackNotNull(paramNetworkCallback);
    ArrayList<NetworkRequest> arrayList = new ArrayList();
    synchronized (sCallbacks) {
      boolean bool;
      if (paramNetworkCallback.networkRequest != null) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkArgument(bool, "NetworkCallback was not registered");
      if (paramNetworkCallback.networkRequest == ALREADY_UNREGISTERED) {
        Log.d("ConnectivityManager", "NetworkCallback was already unregistered");
        return;
      } 
      for (Map.Entry<NetworkRequest, NetworkCallback> entry : sCallbacks.entrySet()) {
        if (entry.getValue() == paramNetworkCallback)
          arrayList.add((NetworkRequest)entry.getKey()); 
      } 
      for (NetworkRequest networkRequest : arrayList) {
        try {
          this.mService.releaseNetworkRequest(networkRequest);
          sCallbacks.remove(networkRequest);
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        } 
      } 
      NetworkCallback.access$902((NetworkCallback)remoteException, ALREADY_UNREGISTERED);
      return;
    } 
  }
  
  public void unregisterNetworkCallback(PendingIntent paramPendingIntent) {
    releaseNetworkRequest(paramPendingIntent);
  }
  
  public void setAcceptUnvalidated(Network paramNetwork, boolean paramBoolean1, boolean paramBoolean2) {
    try {
      this.mService.setAcceptUnvalidated(paramNetwork, paramBoolean1, paramBoolean2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setAcceptPartialConnectivity(Network paramNetwork, boolean paramBoolean1, boolean paramBoolean2) {
    try {
      this.mService.setAcceptPartialConnectivity(paramNetwork, paramBoolean1, paramBoolean2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setAvoidUnvalidated(Network paramNetwork) {
    try {
      this.mService.setAvoidUnvalidated(paramNetwork);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void startCaptivePortalApp(Network paramNetwork) {
    try {
      this.mService.startCaptivePortalApp(paramNetwork);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void startCaptivePortalApp(Network paramNetwork, Bundle paramBundle) {
    try {
      this.mService.startCaptivePortalAppInternal(paramNetwork, paramBundle);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean shouldAvoidBadWifi() {
    try {
      return this.mService.shouldAvoidBadWifi();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getMultipathPreference(Network paramNetwork) {
    try {
      return this.mService.getMultipathPreference(paramNetwork);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void factoryReset() {
    try {
      this.mService.factoryReset();
      this.mTetheringManager.stopAllTethering();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean bindProcessToNetwork(Network paramNetwork) {
    return setProcessDefaultNetwork(paramNetwork);
  }
  
  @Deprecated
  public static boolean setProcessDefaultNetwork(Network paramNetwork) {
    int i;
    boolean bool;
    if (paramNetwork == null) {
      i = 0;
    } else {
      i = paramNetwork.netId;
    } 
    if (i == NetworkUtils.getBoundNetworkForProcess()) {
      bool = true;
    } else {
      bool = false;
    } 
    int j = i;
    if (i != 0)
      j = paramNetwork.getNetIdForResolv(); 
    if (!NetworkUtils.bindProcessToNetwork(j))
      return false; 
    if (!bool) {
      try {
        Proxy.setHttpProxySystemProperty(getInstance().getDefaultProxy());
      } catch (SecurityException securityException) {
        Log.e("ConnectivityManager", "Can't set proxy properties", securityException);
      } 
      InetAddress.clearDnsCache();
      NetworkEventDispatcher.getInstance().onNetworkConfigurationChanged();
    } 
    return true;
  }
  
  public Network getBoundNetworkForProcess() {
    return getProcessDefaultNetwork();
  }
  
  @Deprecated
  public static Network getProcessDefaultNetwork() {
    int i = NetworkUtils.getBoundNetworkForProcess();
    if (i == 0)
      return null; 
    return new Network(i);
  }
  
  private void unsupportedStartingFrom(int paramInt) {
    if (Process.myUid() == 1000)
      return; 
    if ((this.mContext.getApplicationInfo()).targetSdkVersion < paramInt)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("This method is not supported in target SDK version ");
    stringBuilder.append(paramInt);
    stringBuilder.append(" and above");
    throw new UnsupportedOperationException(stringBuilder.toString());
  }
  
  private void checkLegacyRoutingApiAccess() {
    unsupportedStartingFrom(23);
  }
  
  @Deprecated
  public static boolean setProcessDefaultNetworkForHostResolution(Network paramNetwork) {
    int i;
    if (paramNetwork == null) {
      i = 0;
    } else {
      i = paramNetwork.getNetIdForResolv();
    } 
    return NetworkUtils.bindProcessToNetworkForHostResolution(i);
  }
  
  private INetworkPolicyManager getNetworkPolicyManager() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mNPManager : Landroid/net/INetworkPolicyManager;
    //   6: ifnull -> 18
    //   9: aload_0
    //   10: getfield mNPManager : Landroid/net/INetworkPolicyManager;
    //   13: astore_1
    //   14: aload_0
    //   15: monitorexit
    //   16: aload_1
    //   17: areturn
    //   18: ldc_w 'netpolicy'
    //   21: invokestatic getService : (Ljava/lang/String;)Landroid/os/IBinder;
    //   24: astore_1
    //   25: aload_1
    //   26: invokestatic asInterface : (Landroid/os/IBinder;)Landroid/net/INetworkPolicyManager;
    //   29: astore_1
    //   30: aload_0
    //   31: aload_1
    //   32: putfield mNPManager : Landroid/net/INetworkPolicyManager;
    //   35: aload_0
    //   36: monitorexit
    //   37: aload_1
    //   38: areturn
    //   39: astore_1
    //   40: aload_0
    //   41: monitorexit
    //   42: aload_1
    //   43: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4735	-> 0
    //   #4736	-> 2
    //   #4737	-> 9
    //   #4739	-> 18
    //   #4740	-> 18
    //   #4739	-> 25
    //   #4741	-> 35
    //   #4742	-> 39
    // Exception table:
    //   from	to	target	type
    //   2	9	39	finally
    //   9	16	39	finally
    //   18	25	39	finally
    //   25	35	39	finally
    //   35	37	39	finally
    //   40	42	39	finally
  }
  
  public int getRestrictBackgroundStatus() {
    try {
      return getNetworkPolicyManager().getRestrictBackgroundByCaller();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public byte[] getNetworkWatchlistConfigHash() {
    try {
      return this.mService.getNetworkWatchlistConfigHash();
    } catch (RemoteException remoteException) {
      Log.e("ConnectivityManager", "Unable to get watchlist config hash");
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getConnectionOwnerUid(int paramInt, InetSocketAddress paramInetSocketAddress1, InetSocketAddress paramInetSocketAddress2) {
    ConnectionInfo connectionInfo = new ConnectionInfo(paramInt, paramInetSocketAddress1, paramInetSocketAddress2);
    try {
      return this.mService.getConnectionOwnerUid(connectionInfo);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private void printStackTrace() {
    if (DEBUG) {
      StackTraceElement[] arrayOfStackTraceElement = Thread.currentThread().getStackTrace();
      StringBuffer stringBuffer = new StringBuffer();
      for (byte b = 3; b < arrayOfStackTraceElement.length; b++) {
        String str = arrayOfStackTraceElement[b].toString();
        if (str == null || str.contains("android.os"))
          break; 
        stringBuffer.append(" [");
        stringBuffer.append(str);
        stringBuffer.append("]");
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("StackLog:");
      stringBuilder.append(stringBuffer.toString());
      Log.d("ConnectivityManager", stringBuilder.toString());
    } 
  }
  
  public void simulateDataStall(int paramInt, long paramLong, Network paramNetwork, PersistableBundle paramPersistableBundle) {
    try {
      this.mService.simulateDataStall(paramInt, paramLong, paramNetwork, paramPersistableBundle);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean measureDataState(int paramInt) {
    try {
      if (DEBUG) {
        Throwable throwable = new Throwable();
        this("measureDataState");
        Log.w("WLAN+", throwable);
      } 
      return this.mService.measureDataState(paramInt);
    } catch (RemoteException remoteException) {
      return true;
    } 
  }
  
  public NetworkRequest getCelluarNetworkRequest() {
    try {
      if (DEBUG) {
        Throwable throwable = new Throwable();
        this("getCelluarNetworkRequest");
        Log.w("WLAN+", throwable);
      } 
      return this.mService.getCelluarNetworkRequest();
    } catch (RemoteException remoteException) {
      NetworkRequest.Builder builder = new NetworkRequest.Builder();
      builder = builder.addCapability(12);
      builder = builder.addTransportType(0);
      return builder.build();
    } 
  }
  
  public boolean shouldKeepCelluarNetwork(boolean paramBoolean) {
    try {
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("shouldKeepCelluarNetwork:");
        stringBuilder.append(paramBoolean);
        Log.w("WLAN+", stringBuilder.toString());
      } 
      return this.mService.shouldKeepCelluarNetwork(paramBoolean);
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public void updateDataNetworkConfig(String paramString1, String paramString2) {
    try {
      this.mService.updateDataNetworkConfig(paramString1, paramString2);
      return;
    } catch (RemoteException remoteException) {
      return;
    } 
  }
  
  public String getTelephonyPowerState() {
    try {
      return this.mService.getTelephonyPowerState();
    } catch (RemoteException remoteException) {
      return "ERROR:0";
    } 
  }
  
  public boolean isAlreadyUpdated() {
    try {
      return this.mService.isAlreadyUpdated();
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public double getTelephonyPowerLost() {
    try {
      return this.mService.getTelephonyPowerLost();
    } catch (RemoteException remoteException) {
      return 0.0D;
    } 
  }
  
  public void setTelephonyPowerState(String paramString) {
    try {
      this.mService.setTelephonyPowerState(paramString);
    } catch (RemoteException remoteException) {}
  }
  
  public void setAlreadyUpdated(boolean paramBoolean) {
    try {
      this.mService.setAlreadyUpdated(paramBoolean);
    } catch (RemoteException remoteException) {}
  }
  
  public void setTelephonyPowerLost(double paramDouble) {
    try {
      this.mService.setTelephonyPowerLost(paramDouble);
    } catch (RemoteException remoteException) {}
  }
  
  public void setModemTxTime(long[] paramArrayOflong) {
    try {
      this.mService.setModemTxTime(paramArrayOflong);
    } catch (RemoteException remoteException) {}
  }
  
  public long[] getModemTxTime() {
    try {
      return this.mService.getModemTxTime();
    } catch (RemoteException remoteException) {
      return new long[] { 0L, 0L, 0L, 0L, 0L };
    } 
  }
  
  public boolean oppoExecuteIPtableCmd(String paramString1, String paramString2) {
    try {
      return this.mService.oppoExecuteIPtableCmd(paramString1, paramString2);
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public List<String> readArpFile() {
    int i = Process.myUid();
    String str = this.mContext.getPackageManager().getNameForUid(i);
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" readArpFile  calling from ");
      stringBuilder.append(str);
      stringBuilder.append(" uid ");
      stringBuilder.append(i);
      Log.d("ConnectivityManager", stringBuilder.toString());
    } 
    try {
      return this.mService.readArpFile();
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("readArpFile except:");
      stringBuilder.append(remoteException);
      Log.d("ConnectivityManager", stringBuilder.toString());
      return new ArrayList<>();
    } 
  }
  
  public String oppoGetUid(String paramString1, String paramString2) {
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("port: and packetName:");
      stringBuilder.append(paramString1);
      stringBuilder.append(" ");
      stringBuilder.append(paramString2);
      Log.e("ConnectivityManager", stringBuilder.toString());
      paramString1 = this.mService.oppoGetUid(paramString1, paramString2);
      return paramString1;
    } catch (RemoteException remoteException) {
      return new String();
    } 
  }
  
  public void oppoFastappDnsConfig(String[] paramArrayOfString1, String[] paramArrayOfString2, boolean paramBoolean) {
    try {
      Log.e("ConnectivityManager", "oppoFastappDnsConfig");
      this.mService.oppoFastappDnsConfig(paramArrayOfString1, paramArrayOfString2, paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      return;
    } 
  }
  
  public boolean hasCache() {
    try {
      return this.mService.hasCache();
    } catch (Exception exception) {
      exception.printStackTrace();
      return false;
    } 
  }
  
  public long getCacheAge() {
    try {
      return this.mService.getCacheAge();
    } catch (Exception exception) {
      exception.printStackTrace();
      return Long.MAX_VALUE;
    } 
  }
  
  public long getCurrentTimeMillis() {
    try {
      return this.mService.getCurrentTimeMillis();
    } catch (Exception exception) {
      exception.printStackTrace();
      return Long.MAX_VALUE;
    } 
  }
  
  public void disconnectAllVpn() {
    try {
      this.mService.prepareVpn("[Legacy VPN]", "[Legacy VPN]", UserHandle.myUserId());
      this.mService.prepareVpn(null, "[Legacy VPN]", UserHandle.myUserId());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface EntitlementResultCode {}
  
  public static interface Errors {
    public static final int TOO_MANY_REQUESTS = 1;
  }
  
  @Deprecated
  @Retention(RetentionPolicy.SOURCE)
  public static @interface LegacyNetworkType {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface MultipathPreference {}
  
  public static interface OnNetworkActiveListener {
    void onNetworkActive();
  }
  
  @SystemApi
  @Deprecated
  public static interface OnTetheringEntitlementResultListener {
    void onTetheringEntitlementResult(int param1Int);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface RestrictBackgroundStatus {}
}
