package android.net;

import android.os.Parcel;
import android.os.Parcelable;

public final class IpSecSpiResponse implements Parcelable {
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.status);
    paramParcel.writeInt(this.resourceId);
    paramParcel.writeInt(this.spi);
  }
  
  public IpSecSpiResponse(int paramInt1, int paramInt2, int paramInt3) {
    this.status = paramInt1;
    this.resourceId = paramInt2;
    this.spi = paramInt3;
  }
  
  public IpSecSpiResponse(int paramInt) {
    if (paramInt != 0) {
      this.status = paramInt;
      this.resourceId = -1;
      this.spi = 0;
      return;
    } 
    throw new IllegalArgumentException("Valid status implies other args must be provided");
  }
  
  private IpSecSpiResponse(Parcel paramParcel) {
    this.status = paramParcel.readInt();
    this.resourceId = paramParcel.readInt();
    this.spi = paramParcel.readInt();
  }
  
  public static final Parcelable.Creator<IpSecSpiResponse> CREATOR = new Parcelable.Creator<IpSecSpiResponse>() {
      public IpSecSpiResponse createFromParcel(Parcel param1Parcel) {
        return new IpSecSpiResponse(param1Parcel);
      }
      
      public IpSecSpiResponse[] newArray(int param1Int) {
        return new IpSecSpiResponse[param1Int];
      }
    };
  
  private static final String TAG = "IpSecSpiResponse";
  
  public final int resourceId;
  
  public final int spi;
  
  public final int status;
}
