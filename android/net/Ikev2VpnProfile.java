package android.net;

import android.os.Process;
import android.security.Credentials;
import android.security.KeyStore;
import android.security.keystore.AndroidKeyStoreProvider;
import com.android.internal.net.VpnProfile;
import com.android.internal.util.Preconditions;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class Ikev2VpnProfile extends PlatformVpnProfile {
  public static final List<String> DEFAULT_ALGORITHMS = Collections.unmodifiableList(Arrays.asList(new String[] { "cbc(aes)", "hmac(sha256)", "hmac(sha384)", "hmac(sha512)", "rfc4106(gcm(aes))" }));
  
  private static final String EMPTY_CERT = "";
  
  private static final String MISSING_PARAM_MSG_TMPL = "Required parameter was not provided: %s";
  
  public static final String PREFIX_INLINE = "INLINE:";
  
  public static final String PREFIX_KEYSTORE_ALIAS = "KEYSTORE_ALIAS:";
  
  private final List<String> mAllowedAlgorithms;
  
  private final boolean mIsBypassable;
  
  private final boolean mIsMetered;
  
  private final boolean mIsRestrictedToTestNetworks;
  
  private final int mMaxMtu;
  
  private final String mPassword;
  
  private final byte[] mPresharedKey;
  
  private final ProxyInfo mProxyInfo;
  
  private final PrivateKey mRsaPrivateKey;
  
  private final String mServerAddr;
  
  private final X509Certificate mServerRootCaCert;
  
  private final X509Certificate mUserCert;
  
  private final String mUserIdentity;
  
  private final String mUsername;
  
  private Ikev2VpnProfile(int paramInt1, String paramString1, String paramString2, byte[] paramArrayOfbyte, X509Certificate paramX509Certificate1, String paramString3, String paramString4, PrivateKey paramPrivateKey, X509Certificate paramX509Certificate2, ProxyInfo paramProxyInfo, List<String> paramList, boolean paramBoolean1, boolean paramBoolean2, int paramInt2, boolean paramBoolean3) {
    super(paramInt1);
    byte[] arrayOfByte;
    checkNotNull(paramString1, "Required parameter was not provided: %s", new Object[] { "Server address" });
    checkNotNull(paramString2, "Required parameter was not provided: %s", new Object[] { "User Identity" });
    checkNotNull(paramList, "Required parameter was not provided: %s", new Object[] { "Allowed Algorithms" });
    this.mServerAddr = paramString1;
    this.mUserIdentity = paramString2;
    if (paramArrayOfbyte == null) {
      paramString1 = null;
    } else {
      arrayOfByte = Arrays.copyOf(paramArrayOfbyte, paramArrayOfbyte.length);
    } 
    this.mPresharedKey = arrayOfByte;
    this.mServerRootCaCert = paramX509Certificate1;
    this.mUsername = paramString3;
    this.mPassword = paramString4;
    this.mRsaPrivateKey = paramPrivateKey;
    this.mUserCert = paramX509Certificate2;
    this.mProxyInfo = new ProxyInfo(paramProxyInfo);
    this.mAllowedAlgorithms = Collections.unmodifiableList(new ArrayList<>(paramList));
    this.mIsBypassable = paramBoolean1;
    this.mIsMetered = paramBoolean2;
    this.mMaxMtu = paramInt2;
    this.mIsRestrictedToTestNetworks = paramBoolean3;
    validate();
  }
  
  private void validate() {
    Preconditions.checkStringNotEmpty(this.mServerAddr, "Required parameter was not provided: %s", new Object[] { "Server Address" });
    Preconditions.checkStringNotEmpty(this.mUserIdentity, "Required parameter was not provided: %s", new Object[] { "User Identity" });
    if (this.mMaxMtu >= 1280) {
      int i = this.mType;
      if (i != 6) {
        if (i != 7) {
          if (i == 8) {
            checkNotNull(this.mUserCert, "Required parameter was not provided: %s", new Object[] { "User cert" });
            checkNotNull(this.mRsaPrivateKey, "Required parameter was not provided: %s", new Object[] { "RSA Private key" });
            checkCert(this.mUserCert);
            X509Certificate x509Certificate = this.mServerRootCaCert;
            if (x509Certificate != null)
              checkCert(x509Certificate); 
          } else {
            throw new IllegalArgumentException("Invalid auth method set");
          } 
        } else {
          checkNotNull(this.mPresharedKey, "Required parameter was not provided: %s", new Object[] { "Preshared Key" });
        } 
      } else {
        checkNotNull(this.mUsername, "Required parameter was not provided: %s", new Object[] { "Username" });
        checkNotNull(this.mPassword, "Required parameter was not provided: %s", new Object[] { "Password" });
        X509Certificate x509Certificate = this.mServerRootCaCert;
        if (x509Certificate != null)
          checkCert(x509Certificate); 
      } 
      validateAllowedAlgorithms(this.mAllowedAlgorithms);
      return;
    } 
    throw new IllegalArgumentException("Max MTU must be at least1280");
  }
  
  private static void validateAllowedAlgorithms(List<String> paramList) {
    VpnProfile.validateAllowedAlgorithms(paramList);
    if (!paramList.contains("hmac(md5)") && 
      !paramList.contains("hmac(sha1)")) {
      if (hasAeadAlgorithms(paramList) || hasNormalModeAlgorithms(paramList))
        return; 
      throw new IllegalArgumentException("Algorithm set missing support for Auth, Crypt or both");
    } 
    throw new IllegalArgumentException("Algorithm not supported for IKEv2 VPN profiles");
  }
  
  public static boolean hasAeadAlgorithms(List<String> paramList) {
    return paramList.contains("rfc4106(gcm(aes))");
  }
  
  public static boolean hasNormalModeAlgorithms(List<String> paramList) {
    boolean bool4;
    boolean bool1 = paramList.contains("cbc(aes)");
    boolean bool2 = paramList.contains("hmac(sha256)");
    boolean bool3 = false;
    if (bool2 || 
      paramList.contains("hmac(sha384)") || 
      paramList.contains("hmac(sha512)")) {
      bool4 = true;
    } else {
      bool4 = false;
    } 
    bool2 = bool3;
    if (bool1) {
      bool2 = bool3;
      if (bool4)
        bool2 = true; 
    } 
    return bool2;
  }
  
  public String getServerAddr() {
    return this.mServerAddr;
  }
  
  public String getUserIdentity() {
    return this.mUserIdentity;
  }
  
  public byte[] getPresharedKey() {
    byte[] arrayOfByte = this.mPresharedKey;
    if (arrayOfByte == null) {
      arrayOfByte = null;
    } else {
      arrayOfByte = Arrays.copyOf(arrayOfByte, arrayOfByte.length);
    } 
    return arrayOfByte;
  }
  
  public X509Certificate getServerRootCaCert() {
    return this.mServerRootCaCert;
  }
  
  public String getUsername() {
    return this.mUsername;
  }
  
  public String getPassword() {
    return this.mPassword;
  }
  
  public PrivateKey getRsaPrivateKey() {
    return this.mRsaPrivateKey;
  }
  
  public X509Certificate getUserCert() {
    return this.mUserCert;
  }
  
  public ProxyInfo getProxyInfo() {
    return this.mProxyInfo;
  }
  
  public List<String> getAllowedAlgorithms() {
    return this.mAllowedAlgorithms;
  }
  
  public boolean isBypassable() {
    return this.mIsBypassable;
  }
  
  public boolean isMetered() {
    return this.mIsMetered;
  }
  
  public int getMaxMtu() {
    return this.mMaxMtu;
  }
  
  public boolean isRestrictedToTestNetworks() {
    return this.mIsRestrictedToTestNetworks;
  }
  
  public int hashCode() {
    int i = this.mType;
    String str1 = this.mServerAddr, str2 = this.mUserIdentity;
    byte[] arrayOfByte = this.mPresharedKey;
    int j = Arrays.hashCode(arrayOfByte);
    X509Certificate x509Certificate1 = this.mServerRootCaCert;
    String str3 = this.mUsername, str4 = this.mPassword;
    PrivateKey privateKey = this.mRsaPrivateKey;
    X509Certificate x509Certificate2 = this.mUserCert;
    ProxyInfo proxyInfo = this.mProxyInfo;
    List<String> list = this.mAllowedAlgorithms;
    boolean bool1 = this.mIsBypassable;
    boolean bool2 = this.mIsMetered;
    int k = this.mMaxMtu;
    boolean bool3 = this.mIsRestrictedToTestNetworks;
    return Objects.hash(new Object[] { 
          Integer.valueOf(i), str1, str2, Integer.valueOf(j), x509Certificate1, str3, str4, privateKey, x509Certificate2, proxyInfo, 
          list, Boolean.valueOf(bool1), Boolean.valueOf(bool2), Integer.valueOf(k), Boolean.valueOf(bool3) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof Ikev2VpnProfile;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    if (this.mType == ((Ikev2VpnProfile)paramObject).mType) {
      String str1 = this.mServerAddr, str2 = ((Ikev2VpnProfile)paramObject).mServerAddr;
      if (Objects.equals(str1, str2)) {
        str2 = this.mUserIdentity;
        str1 = ((Ikev2VpnProfile)paramObject).mUserIdentity;
        if (Objects.equals(str2, str1)) {
          byte[] arrayOfByte2 = this.mPresharedKey, arrayOfByte1 = ((Ikev2VpnProfile)paramObject).mPresharedKey;
          if (Arrays.equals(arrayOfByte2, arrayOfByte1)) {
            X509Certificate x509Certificate2 = this.mServerRootCaCert, x509Certificate1 = ((Ikev2VpnProfile)paramObject).mServerRootCaCert;
            if (Objects.equals(x509Certificate2, x509Certificate1)) {
              String str4 = this.mUsername, str3 = ((Ikev2VpnProfile)paramObject).mUsername;
              if (Objects.equals(str4, str3)) {
                str3 = this.mPassword;
                str4 = ((Ikev2VpnProfile)paramObject).mPassword;
                if (Objects.equals(str3, str4)) {
                  PrivateKey privateKey1 = this.mRsaPrivateKey, privateKey2 = ((Ikev2VpnProfile)paramObject).mRsaPrivateKey;
                  if (Objects.equals(privateKey1, privateKey2)) {
                    X509Certificate x509Certificate4 = this.mUserCert, x509Certificate3 = ((Ikev2VpnProfile)paramObject).mUserCert;
                    if (Objects.equals(x509Certificate4, x509Certificate3)) {
                      ProxyInfo proxyInfo2 = this.mProxyInfo, proxyInfo1 = ((Ikev2VpnProfile)paramObject).mProxyInfo;
                      if (Objects.equals(proxyInfo2, proxyInfo1)) {
                        List<String> list2 = this.mAllowedAlgorithms, list1 = ((Ikev2VpnProfile)paramObject).mAllowedAlgorithms;
                        if (Objects.equals(list2, list1) && this.mIsBypassable == ((Ikev2VpnProfile)paramObject).mIsBypassable && this.mIsMetered == ((Ikev2VpnProfile)paramObject).mIsMetered && this.mMaxMtu == ((Ikev2VpnProfile)paramObject).mMaxMtu && this.mIsRestrictedToTestNetworks == ((Ikev2VpnProfile)paramObject).mIsRestrictedToTestNetworks)
                          bool1 = true; 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
    } 
    return bool1;
  }
  
  public VpnProfile toVpnProfile() throws IOException, GeneralSecurityException {
    boolean bool = this.mIsRestrictedToTestNetworks;
    String str = "";
    VpnProfile vpnProfile = new VpnProfile("", bool);
    vpnProfile.type = this.mType;
    vpnProfile.server = this.mServerAddr;
    vpnProfile.ipsecIdentifier = this.mUserIdentity;
    vpnProfile.proxy = this.mProxyInfo;
    vpnProfile.setAllowedAlgorithms(this.mAllowedAlgorithms);
    vpnProfile.isBypassable = this.mIsBypassable;
    vpnProfile.isMetered = this.mIsMetered;
    vpnProfile.maxMtu = this.mMaxMtu;
    vpnProfile.areAuthParamsInline = true;
    vpnProfile.saveLogin = true;
    int i = this.mType;
    if (i != 6) {
      if (i != 7) {
        if (i == 8) {
          vpnProfile.ipsecUserCert = certificateToPemString(this.mUserCert);
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("INLINE:");
          PrivateKey privateKey = this.mRsaPrivateKey;
          stringBuilder.append(encodeForIpsecSecret(privateKey.getEncoded()));
          vpnProfile.ipsecSecret = stringBuilder.toString();
          X509Certificate x509Certificate = this.mServerRootCaCert;
          if (x509Certificate != null)
            str = certificateToPemString(x509Certificate); 
          vpnProfile.ipsecCaCert = str;
        } else {
          throw new IllegalArgumentException("Invalid auth method set");
        } 
      } else {
        vpnProfile.ipsecSecret = encodeForIpsecSecret(this.mPresharedKey);
      } 
    } else {
      vpnProfile.username = this.mUsername;
      vpnProfile.password = this.mPassword;
      X509Certificate x509Certificate = this.mServerRootCaCert;
      if (x509Certificate != null)
        str = certificateToPemString(x509Certificate); 
      vpnProfile.ipsecCaCert = str;
    } 
    return vpnProfile;
  }
  
  public static Ikev2VpnProfile fromVpnProfile(VpnProfile paramVpnProfile) throws IOException, GeneralSecurityException {
    return fromVpnProfile(paramVpnProfile, null);
  }
  
  public static Ikev2VpnProfile fromVpnProfile(VpnProfile paramVpnProfile, KeyStore paramKeyStore) throws IOException, GeneralSecurityException {
    X509Certificate x509Certificate;
    Builder builder = new Builder(paramVpnProfile.server, paramVpnProfile.ipsecIdentifier);
    builder.setProxy(paramVpnProfile.proxy);
    builder.setAllowedAlgorithms(paramVpnProfile.getAllowedAlgorithms());
    builder.setBypassable(paramVpnProfile.isBypassable);
    builder.setMetered(paramVpnProfile.isMetered);
    builder.setMaxMtu(paramVpnProfile.maxMtu);
    if (paramVpnProfile.isRestrictedToTestNetworks)
      builder.restrictToTestNetworks(); 
    int i = paramVpnProfile.type;
    if (i != 6) {
      if (i != 7) {
        if (i == 8) {
          PrivateKey privateKey;
          if (paramVpnProfile.ipsecSecret.startsWith("KEYSTORE_ALIAS:")) {
            Objects.requireNonNull(paramKeyStore, "Missing Keystore for aliased PrivateKey");
            String str = paramVpnProfile.ipsecSecret;
            str = str.substring("KEYSTORE_ALIAS:".length());
            i = Process.myUid();
            privateKey = AndroidKeyStoreProvider.loadAndroidKeyStorePrivateKeyFromKeystore(paramKeyStore, str, i);
          } else if (paramVpnProfile.ipsecSecret.startsWith("INLINE:")) {
            privateKey = getPrivateKey(paramVpnProfile.ipsecSecret.substring("INLINE:".length()));
          } else {
            throw new IllegalArgumentException("Invalid RSA private key prefix");
          } 
          X509Certificate x509Certificate1 = certificateFromPemString(paramVpnProfile.ipsecUserCert);
          x509Certificate = certificateFromPemString(paramVpnProfile.ipsecCaCert);
          builder.setAuthDigitalSignature(x509Certificate1, privateKey, x509Certificate);
        } else {
          throw new IllegalArgumentException("Invalid auth method set");
        } 
      } else {
        builder.setAuthPsk(decodeFromIpsecSecret(((VpnProfile)x509Certificate).ipsecSecret));
      } 
    } else {
      String str3 = ((VpnProfile)x509Certificate).username, str2 = ((VpnProfile)x509Certificate).password, str1 = ((VpnProfile)x509Certificate).ipsecCaCert;
      X509Certificate x509Certificate1 = certificateFromPemString(str1);
      builder.setAuthUsernamePassword(str3, str2, x509Certificate1);
    } 
    return builder.build();
  }
  
  public static boolean isValidVpnProfile(VpnProfile paramVpnProfile) {
    if (paramVpnProfile.server.isEmpty() || paramVpnProfile.ipsecIdentifier.isEmpty())
      return false; 
    int i = paramVpnProfile.type;
    if (i != 6) {
      if (i != 7) {
        if (i != 8)
          return false; 
        if (paramVpnProfile.ipsecSecret.isEmpty() || paramVpnProfile.ipsecUserCert.isEmpty())
          return false; 
      } else if (paramVpnProfile.ipsecSecret.isEmpty()) {
        return false;
      } 
    } else if (paramVpnProfile.username.isEmpty() || paramVpnProfile.password.isEmpty()) {
      return false;
    } 
    return true;
  }
  
  public static String certificateToPemString(X509Certificate paramX509Certificate) throws IOException, CertificateEncodingException {
    if (paramX509Certificate == null)
      return ""; 
    return new String(Credentials.convertToPem(new Certificate[] { paramX509Certificate }, ), StandardCharsets.US_ASCII);
  }
  
  private static X509Certificate certificateFromPemString(String paramString) throws CertificateException {
    List list = null;
    if (paramString == null || "".equals(paramString))
      return null; 
    try {
      X509Certificate x509Certificate;
      Charset charset = StandardCharsets.US_ASCII;
      List<X509Certificate> list1 = Credentials.convertFromPem(paramString.getBytes(charset));
      if (list1.isEmpty()) {
        list1 = list;
      } else {
        x509Certificate = list1.get(0);
      } 
      return x509Certificate;
    } catch (IOException iOException) {
      throw new CertificateException(iOException);
    } 
  }
  
  public static String encodeForIpsecSecret(byte[] paramArrayOfbyte) {
    checkNotNull(paramArrayOfbyte, "Required parameter was not provided: %s", new Object[] { "secret" });
    return Base64.getEncoder().encodeToString(paramArrayOfbyte);
  }
  
  private static byte[] decodeFromIpsecSecret(String paramString) {
    checkNotNull(paramString, "Required parameter was not provided: %s", new Object[] { "encoded" });
    return Base64.getDecoder().decode(paramString);
  }
  
  private static PrivateKey getPrivateKey(String paramString) throws InvalidKeySpecException, NoSuchAlgorithmException {
    PKCS8EncodedKeySpec pKCS8EncodedKeySpec = new PKCS8EncodedKeySpec(decodeFromIpsecSecret(paramString));
    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    return keyFactory.generatePrivate(pKCS8EncodedKeySpec);
  }
  
  private static void checkCert(X509Certificate paramX509Certificate) {
    try {
      certificateToPemString(paramX509Certificate);
      return;
    } catch (GeneralSecurityException|IOException generalSecurityException) {
      throw new IllegalArgumentException("Certificate could not be encoded");
    } 
  }
  
  private static <T> T checkNotNull(T paramT, String paramString, Object... paramVarArgs) {
    Objects.requireNonNull(paramT, String.format(paramString, paramVarArgs));
    return paramT;
  }
  
  class Builder {
    private int mType = -1;
    
    private List<String> mAllowedAlgorithms = Ikev2VpnProfile.DEFAULT_ALGORITHMS;
    
    private boolean mIsBypassable = false;
    
    private boolean mIsMetered = true;
    
    private int mMaxMtu = 1360;
    
    private boolean mIsRestrictedToTestNetworks = false;
    
    private String mPassword;
    
    private byte[] mPresharedKey;
    
    private ProxyInfo mProxyInfo;
    
    private PrivateKey mRsaPrivateKey;
    
    private final String mServerAddr;
    
    private X509Certificate mServerRootCaCert;
    
    private X509Certificate mUserCert;
    
    private final String mUserIdentity;
    
    private String mUsername;
    
    public Builder(Ikev2VpnProfile this$0, String param1String1) {
      Ikev2VpnProfile.checkNotNull((T)this$0, "Required parameter was not provided: %s", new Object[] { "serverAddr" });
      Ikev2VpnProfile.checkNotNull((T)param1String1, "Required parameter was not provided: %s", new Object[] { "identity" });
      this.mServerAddr = (String)this$0;
      this.mUserIdentity = param1String1;
    }
    
    private void resetAuthParams() {
      this.mPresharedKey = null;
      this.mServerRootCaCert = null;
      this.mUsername = null;
      this.mPassword = null;
      this.mRsaPrivateKey = null;
      this.mUserCert = null;
    }
    
    public Builder setAuthUsernamePassword(String param1String1, String param1String2, X509Certificate param1X509Certificate) {
      Ikev2VpnProfile.checkNotNull((T)param1String1, "Required parameter was not provided: %s", new Object[] { "user" });
      Ikev2VpnProfile.checkNotNull((T)param1String2, "Required parameter was not provided: %s", new Object[] { "pass" });
      if (param1X509Certificate != null)
        Ikev2VpnProfile.checkCert(param1X509Certificate); 
      resetAuthParams();
      this.mUsername = param1String1;
      this.mPassword = param1String2;
      this.mServerRootCaCert = param1X509Certificate;
      this.mType = 6;
      return this;
    }
    
    public Builder setAuthDigitalSignature(X509Certificate param1X509Certificate1, PrivateKey param1PrivateKey, X509Certificate param1X509Certificate2) {
      Ikev2VpnProfile.checkNotNull((T)param1X509Certificate1, "Required parameter was not provided: %s", new Object[] { "userCert" });
      Ikev2VpnProfile.checkNotNull((T)param1PrivateKey, "Required parameter was not provided: %s", new Object[] { "key" });
      Ikev2VpnProfile.checkCert(param1X509Certificate1);
      if (param1X509Certificate2 != null)
        Ikev2VpnProfile.checkCert(param1X509Certificate2); 
      resetAuthParams();
      this.mUserCert = param1X509Certificate1;
      this.mRsaPrivateKey = param1PrivateKey;
      this.mServerRootCaCert = param1X509Certificate2;
      this.mType = 8;
      return this;
    }
    
    public Builder setAuthPsk(byte[] param1ArrayOfbyte) {
      Ikev2VpnProfile.checkNotNull((T)param1ArrayOfbyte, "Required parameter was not provided: %s", new Object[] { "psk" });
      resetAuthParams();
      this.mPresharedKey = param1ArrayOfbyte;
      this.mType = 7;
      return this;
    }
    
    public Builder setBypassable(boolean param1Boolean) {
      this.mIsBypassable = param1Boolean;
      return this;
    }
    
    public Builder setProxy(ProxyInfo param1ProxyInfo) {
      this.mProxyInfo = param1ProxyInfo;
      return this;
    }
    
    public Builder setMaxMtu(int param1Int) {
      if (param1Int >= 1280) {
        this.mMaxMtu = param1Int;
        return this;
      } 
      throw new IllegalArgumentException("Max MTU must be at least 1280");
    }
    
    public Builder setMetered(boolean param1Boolean) {
      this.mIsMetered = param1Boolean;
      return this;
    }
    
    public Builder setAllowedAlgorithms(List<String> param1List) {
      Ikev2VpnProfile.checkNotNull((T)param1List, "Required parameter was not provided: %s", new Object[] { "algorithmNames" });
      Ikev2VpnProfile.validateAllowedAlgorithms(param1List);
      this.mAllowedAlgorithms = param1List;
      return this;
    }
    
    public Builder restrictToTestNetworks() {
      this.mIsRestrictedToTestNetworks = true;
      return this;
    }
    
    public Ikev2VpnProfile build() {
      return new Ikev2VpnProfile(this.mType, this.mServerAddr, this.mUserIdentity, this.mPresharedKey, this.mServerRootCaCert, this.mUsername, this.mPassword, this.mRsaPrivateKey, this.mUserCert, this.mProxyInfo, this.mAllowedAlgorithms, this.mIsBypassable, this.mIsMetered, this.mMaxMtu, this.mIsRestrictedToTestNetworks);
    }
  }
}
