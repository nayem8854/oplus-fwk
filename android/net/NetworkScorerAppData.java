package android.net;

import android.content.ComponentName;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public final class NetworkScorerAppData implements Parcelable {
  public NetworkScorerAppData(int paramInt, ComponentName paramComponentName1, String paramString1, ComponentName paramComponentName2, String paramString2) {
    this.packageUid = paramInt;
    this.mRecommendationService = paramComponentName1;
    this.mRecommendationServiceLabel = paramString1;
    this.mEnableUseOpenWifiActivity = paramComponentName2;
    this.mNetworkAvailableNotificationChannelId = paramString2;
  }
  
  protected NetworkScorerAppData(Parcel paramParcel) {
    this.packageUid = paramParcel.readInt();
    this.mRecommendationService = ComponentName.readFromParcel(paramParcel);
    this.mRecommendationServiceLabel = paramParcel.readString();
    this.mEnableUseOpenWifiActivity = ComponentName.readFromParcel(paramParcel);
    this.mNetworkAvailableNotificationChannelId = paramParcel.readString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.packageUid);
    ComponentName.writeToParcel(this.mRecommendationService, paramParcel);
    paramParcel.writeString(this.mRecommendationServiceLabel);
    ComponentName.writeToParcel(this.mEnableUseOpenWifiActivity, paramParcel);
    paramParcel.writeString(this.mNetworkAvailableNotificationChannelId);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<NetworkScorerAppData> CREATOR = new Parcelable.Creator<NetworkScorerAppData>() {
      public NetworkScorerAppData createFromParcel(Parcel param1Parcel) {
        return new NetworkScorerAppData(param1Parcel);
      }
      
      public NetworkScorerAppData[] newArray(int param1Int) {
        return new NetworkScorerAppData[param1Int];
      }
    };
  
  private final ComponentName mEnableUseOpenWifiActivity;
  
  private final String mNetworkAvailableNotificationChannelId;
  
  private final ComponentName mRecommendationService;
  
  private final String mRecommendationServiceLabel;
  
  public final int packageUid;
  
  public String getRecommendationServicePackageName() {
    return this.mRecommendationService.getPackageName();
  }
  
  public ComponentName getRecommendationServiceComponent() {
    return this.mRecommendationService;
  }
  
  public ComponentName getEnableUseOpenWifiActivity() {
    return this.mEnableUseOpenWifiActivity;
  }
  
  public String getRecommendationServiceLabel() {
    return this.mRecommendationServiceLabel;
  }
  
  public String getNetworkAvailableNotificationChannelId() {
    return this.mNetworkAvailableNotificationChannelId;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("NetworkScorerAppData{packageUid=");
    stringBuilder.append(this.packageUid);
    stringBuilder.append(", mRecommendationService=");
    stringBuilder.append(this.mRecommendationService);
    stringBuilder.append(", mRecommendationServiceLabel=");
    stringBuilder.append(this.mRecommendationServiceLabel);
    stringBuilder.append(", mEnableUseOpenWifiActivity=");
    stringBuilder.append(this.mEnableUseOpenWifiActivity);
    stringBuilder.append(", mNetworkAvailableNotificationChannelId=");
    stringBuilder.append(this.mNetworkAvailableNotificationChannelId);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.packageUid == ((NetworkScorerAppData)paramObject).packageUid) {
      ComponentName componentName1 = this.mRecommendationService, componentName2 = ((NetworkScorerAppData)paramObject).mRecommendationService;
      if (Objects.equals(componentName1, componentName2)) {
        String str2 = this.mRecommendationServiceLabel, str1 = ((NetworkScorerAppData)paramObject).mRecommendationServiceLabel;
        if (Objects.equals(str2, str1)) {
          ComponentName componentName3 = this.mEnableUseOpenWifiActivity, componentName4 = ((NetworkScorerAppData)paramObject).mEnableUseOpenWifiActivity;
          if (Objects.equals(componentName3, componentName4)) {
            String str = this.mNetworkAvailableNotificationChannelId;
            paramObject = ((NetworkScorerAppData)paramObject).mNetworkAvailableNotificationChannelId;
            if (Objects.equals(str, paramObject))
              return null; 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.packageUid), this.mRecommendationService, this.mRecommendationServiceLabel, this.mEnableUseOpenWifiActivity, this.mNetworkAvailableNotificationChannelId });
  }
}
