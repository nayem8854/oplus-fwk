package android.net.nsd;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;
import com.android.internal.util.AsyncChannel;
import com.android.internal.util.Preconditions;
import java.util.concurrent.CountDownLatch;

public final class NsdManager {
  private static final String TAG = NsdManager.class.getSimpleName();
  
  static {
    SparseArray<String> sparseArray = new SparseArray();
    sparseArray.put(393217, "DISCOVER_SERVICES");
    EVENT_NAMES.put(393218, "DISCOVER_SERVICES_STARTED");
    EVENT_NAMES.put(393219, "DISCOVER_SERVICES_FAILED");
    EVENT_NAMES.put(393220, "SERVICE_FOUND");
    EVENT_NAMES.put(393221, "SERVICE_LOST");
    EVENT_NAMES.put(393222, "STOP_DISCOVERY");
    EVENT_NAMES.put(393223, "STOP_DISCOVERY_FAILED");
    EVENT_NAMES.put(393224, "STOP_DISCOVERY_SUCCEEDED");
    EVENT_NAMES.put(393225, "REGISTER_SERVICE");
    EVENT_NAMES.put(393226, "REGISTER_SERVICE_FAILED");
    EVENT_NAMES.put(393227, "REGISTER_SERVICE_SUCCEEDED");
    EVENT_NAMES.put(393228, "UNREGISTER_SERVICE");
    EVENT_NAMES.put(393229, "UNREGISTER_SERVICE_FAILED");
    EVENT_NAMES.put(393230, "UNREGISTER_SERVICE_SUCCEEDED");
    EVENT_NAMES.put(393234, "RESOLVE_SERVICE");
    EVENT_NAMES.put(393235, "RESOLVE_SERVICE_FAILED");
    EVENT_NAMES.put(393236, "RESOLVE_SERVICE_SUCCEEDED");
    EVENT_NAMES.put(393240, "ENABLE");
    EVENT_NAMES.put(393241, "DISABLE");
    EVENT_NAMES.put(393242, "NATIVE_DAEMON_EVENT");
  }
  
  public static String nameOf(int paramInt) {
    String str = (String)EVENT_NAMES.get(paramInt);
    if (str == null)
      return Integer.toString(paramInt); 
    return str;
  }
  
  private int mListenerKey = 1;
  
  private final SparseArray mListenerMap = new SparseArray();
  
  private final SparseArray<NsdServiceInfo> mServiceMap = new SparseArray();
  
  private final Object mMapLock = new Object();
  
  private final AsyncChannel mAsyncChannel = new AsyncChannel();
  
  private final CountDownLatch mConnected = new CountDownLatch(1);
  
  public static final String ACTION_NSD_STATE_CHANGED = "android.net.nsd.STATE_CHANGED";
  
  private static final int BASE = 393216;
  
  private static final boolean DBG = false;
  
  public static final int DISABLE = 393241;
  
  public static final int DISCOVER_SERVICES = 393217;
  
  public static final int DISCOVER_SERVICES_FAILED = 393219;
  
  public static final int DISCOVER_SERVICES_STARTED = 393218;
  
  public static final int ENABLE = 393240;
  
  private static final SparseArray<String> EVENT_NAMES;
  
  public static final String EXTRA_NSD_STATE = "nsd_state";
  
  public static final int FAILURE_ALREADY_ACTIVE = 3;
  
  public static final int FAILURE_INTERNAL_ERROR = 0;
  
  public static final int FAILURE_MAX_LIMIT = 4;
  
  private static final int FIRST_LISTENER_KEY = 1;
  
  public static final int NATIVE_DAEMON_EVENT = 393242;
  
  public static final int NSD_STATE_DISABLED = 1;
  
  public static final int NSD_STATE_ENABLED = 2;
  
  public static final int PROTOCOL_DNS_SD = 1;
  
  public static final int REGISTER_SERVICE = 393225;
  
  public static final int REGISTER_SERVICE_FAILED = 393226;
  
  public static final int REGISTER_SERVICE_SUCCEEDED = 393227;
  
  public static final int RESOLVE_SERVICE = 393234;
  
  public static final int RESOLVE_SERVICE_FAILED = 393235;
  
  public static final int RESOLVE_SERVICE_SUCCEEDED = 393236;
  
  public static final int SERVICE_FOUND = 393220;
  
  public static final int SERVICE_LOST = 393221;
  
  public static final int STOP_DISCOVERY = 393222;
  
  public static final int STOP_DISCOVERY_FAILED = 393223;
  
  public static final int STOP_DISCOVERY_SUCCEEDED = 393224;
  
  public static final int UNREGISTER_SERVICE = 393228;
  
  public static final int UNREGISTER_SERVICE_FAILED = 393229;
  
  public static final int UNREGISTER_SERVICE_SUCCEEDED = 393230;
  
  private final Context mContext;
  
  private ServiceHandler mHandler;
  
  private final INsdManager mService;
  
  public NsdManager(Context paramContext, INsdManager paramINsdManager) {
    this.mService = paramINsdManager;
    this.mContext = paramContext;
    init();
  }
  
  public void disconnect() {
    this.mAsyncChannel.disconnect();
    this.mHandler.getLooper().quitSafely();
  }
  
  public static interface DiscoveryListener {
    void onDiscoveryStarted(String param1String);
    
    void onDiscoveryStopped(String param1String);
    
    void onServiceFound(NsdServiceInfo param1NsdServiceInfo);
    
    void onServiceLost(NsdServiceInfo param1NsdServiceInfo);
    
    void onStartDiscoveryFailed(String param1String, int param1Int);
    
    void onStopDiscoveryFailed(String param1String, int param1Int);
  }
  
  public static interface RegistrationListener {
    void onRegistrationFailed(NsdServiceInfo param1NsdServiceInfo, int param1Int);
    
    void onServiceRegistered(NsdServiceInfo param1NsdServiceInfo);
    
    void onServiceUnregistered(NsdServiceInfo param1NsdServiceInfo);
    
    void onUnregistrationFailed(NsdServiceInfo param1NsdServiceInfo, int param1Int);
  }
  
  public static interface ResolveListener {
    void onResolveFailed(NsdServiceInfo param1NsdServiceInfo, int param1Int);
    
    void onServiceResolved(NsdServiceInfo param1NsdServiceInfo);
  }
  
  class ServiceHandler extends Handler {
    final NsdManager this$0;
    
    ServiceHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      int j = param1Message.arg2;
      if (i != 69632) {
        if (i != 69634) {
          if (i != 69636)
            synchronized (NsdManager.this.mMapLock) {
              Object object = NsdManager.this.mListenerMap.get(j);
              NsdServiceInfo nsdServiceInfo = (NsdServiceInfo)NsdManager.this.mServiceMap.get(j);
              if (object == null) {
                object = NsdManager.TAG;
                null = new StringBuilder();
                null.append("Stale key ");
                null.append(param1Message.arg2);
                Log.d((String)object, null.toString());
                return;
              } 
              switch (i) {
                default:
                  null = NsdManager.TAG;
                  object = new StringBuilder();
                  object.append("Ignored ");
                  object.append(param1Message);
                  Log.d((String)null, object.toString());
                  return;
                case 393236:
                  NsdManager.this.removeListener(j);
                  ((NsdManager.ResolveListener)object).onServiceResolved((NsdServiceInfo)param1Message.obj);
                  return;
                case 393235:
                  NsdManager.this.removeListener(j);
                  ((NsdManager.ResolveListener)object).onResolveFailed(nsdServiceInfo, param1Message.arg1);
                  return;
                case 393230:
                  NsdManager.this.removeListener(param1Message.arg2);
                  ((NsdManager.RegistrationListener)object).onServiceUnregistered(nsdServiceInfo);
                  return;
                case 393229:
                  NsdManager.this.removeListener(j);
                  ((NsdManager.RegistrationListener)object).onUnregistrationFailed(nsdServiceInfo, param1Message.arg1);
                  return;
                case 393227:
                  ((NsdManager.RegistrationListener)object).onServiceRegistered((NsdServiceInfo)param1Message.obj);
                  return;
                case 393226:
                  NsdManager.this.removeListener(j);
                  ((NsdManager.RegistrationListener)object).onRegistrationFailed(nsdServiceInfo, param1Message.arg1);
                  return;
                case 393224:
                  NsdManager.this.removeListener(j);
                  ((NsdManager.DiscoveryListener)object).onDiscoveryStopped(NsdManager.getNsdServiceInfoType(nsdServiceInfo));
                  return;
                case 393223:
                  NsdManager.this.removeListener(j);
                  ((NsdManager.DiscoveryListener)object).onStopDiscoveryFailed(NsdManager.getNsdServiceInfoType(nsdServiceInfo), param1Message.arg1);
                  return;
                case 393221:
                  ((NsdManager.DiscoveryListener)object).onServiceLost((NsdServiceInfo)param1Message.obj);
                  return;
                case 393220:
                  ((NsdManager.DiscoveryListener)object).onServiceFound((NsdServiceInfo)param1Message.obj);
                  return;
                case 393219:
                  NsdManager.this.removeListener(j);
                  ((NsdManager.DiscoveryListener)object).onStartDiscoveryFailed(NsdManager.getNsdServiceInfoType(nsdServiceInfo), param1Message.arg1);
                  return;
                case 393218:
                  break;
              } 
              String str = NsdManager.getNsdServiceInfoType((NsdServiceInfo)param1Message.obj);
              ((NsdManager.DiscoveryListener)object).onDiscoveryStarted(str);
              return;
            }  
          Log.e(NsdManager.TAG, "Channel lost");
          return;
        } 
        NsdManager.this.mConnected.countDown();
        return;
      } 
      NsdManager.this.mAsyncChannel.sendMessage(69633);
    }
  }
  
  private int nextListenerKey() {
    int i = Math.max(1, this.mListenerKey + 1);
    return i;
  }
  
  private int putListener(Object paramObject, NsdServiceInfo paramNsdServiceInfo) {
    checkListener(paramObject);
    synchronized (this.mMapLock) {
      boolean bool;
      int i = this.mListenerMap.indexOfValue(paramObject);
      if (i == -1) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkArgument(bool, "listener already in use");
      i = nextListenerKey();
      this.mListenerMap.put(i, paramObject);
      this.mServiceMap.put(i, paramNsdServiceInfo);
      return i;
    } 
  }
  
  private void removeListener(int paramInt) {
    synchronized (this.mMapLock) {
      this.mListenerMap.remove(paramInt);
      this.mServiceMap.remove(paramInt);
      return;
    } 
  }
  
  private int getListenerKey(Object paramObject) {
    checkListener(paramObject);
    synchronized (this.mMapLock) {
      boolean bool;
      int i = this.mListenerMap.indexOfValue(paramObject);
      if (i != -1) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkArgument(bool, "listener not registered");
      i = this.mListenerMap.keyAt(i);
      return i;
    } 
  }
  
  private static String getNsdServiceInfoType(NsdServiceInfo paramNsdServiceInfo) {
    if (paramNsdServiceInfo == null)
      return "?"; 
    return paramNsdServiceInfo.getServiceType();
  }
  
  private void init() {
    Messenger messenger = getMessenger();
    if (messenger == null)
      fatal("Failed to obtain service Messenger"); 
    HandlerThread handlerThread = new HandlerThread("NsdManager");
    handlerThread.start();
    ServiceHandler serviceHandler = new ServiceHandler(handlerThread.getLooper());
    this.mAsyncChannel.connect(this.mContext, serviceHandler, messenger);
    try {
      this.mConnected.await();
    } catch (InterruptedException interruptedException) {
      fatal("Interrupted wait at init");
    } 
  }
  
  private static void fatal(String paramString) {
    Log.e(TAG, paramString);
    throw new RuntimeException(paramString);
  }
  
  public void registerService(NsdServiceInfo paramNsdServiceInfo, int paramInt, RegistrationListener paramRegistrationListener) {
    boolean bool;
    if (paramNsdServiceInfo.getPort() > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "Invalid port number");
    checkServiceInfo(paramNsdServiceInfo);
    checkProtocol(paramInt);
    paramInt = putListener(paramRegistrationListener, paramNsdServiceInfo);
    this.mAsyncChannel.sendMessage(393225, 0, paramInt, paramNsdServiceInfo);
  }
  
  public void unregisterService(RegistrationListener paramRegistrationListener) {
    int i = getListenerKey(paramRegistrationListener);
    this.mAsyncChannel.sendMessage(393228, 0, i);
  }
  
  public void discoverServices(String paramString, int paramInt, DiscoveryListener paramDiscoveryListener) {
    Preconditions.checkStringNotEmpty(paramString, "Service type cannot be empty");
    checkProtocol(paramInt);
    NsdServiceInfo nsdServiceInfo = new NsdServiceInfo();
    nsdServiceInfo.setServiceType(paramString);
    paramInt = putListener(paramDiscoveryListener, nsdServiceInfo);
    this.mAsyncChannel.sendMessage(393217, 0, paramInt, nsdServiceInfo);
  }
  
  public void stopServiceDiscovery(DiscoveryListener paramDiscoveryListener) {
    int i = getListenerKey(paramDiscoveryListener);
    this.mAsyncChannel.sendMessage(393222, 0, i);
  }
  
  public void resolveService(NsdServiceInfo paramNsdServiceInfo, ResolveListener paramResolveListener) {
    checkServiceInfo(paramNsdServiceInfo);
    int i = putListener(paramResolveListener, paramNsdServiceInfo);
    this.mAsyncChannel.sendMessage(393234, 0, i, paramNsdServiceInfo);
  }
  
  public void setEnabled(boolean paramBoolean) {
    try {
      this.mService.setEnabled(paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private Messenger getMessenger() {
    try {
      return this.mService.getMessenger();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private static void checkListener(Object paramObject) {
    Preconditions.checkNotNull(paramObject, "listener cannot be null");
  }
  
  private static void checkProtocol(int paramInt) {
    boolean bool = true;
    if (paramInt != 1)
      bool = false; 
    Preconditions.checkArgument(bool, "Unsupported protocol");
  }
  
  private static void checkServiceInfo(NsdServiceInfo paramNsdServiceInfo) {
    Preconditions.checkNotNull(paramNsdServiceInfo, "NsdServiceInfo cannot be null");
    Preconditions.checkStringNotEmpty(paramNsdServiceInfo.getServiceName(), "Service name cannot be empty");
    Preconditions.checkStringNotEmpty(paramNsdServiceInfo.getServiceType(), "Service type cannot be empty");
  }
}
