package android.net.nsd;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Messenger;
import android.os.Parcel;
import android.os.RemoteException;

public interface INsdManager extends IInterface {
  Messenger getMessenger() throws RemoteException;
  
  void setEnabled(boolean paramBoolean) throws RemoteException;
  
  class Default implements INsdManager {
    public Messenger getMessenger() throws RemoteException {
      return null;
    }
    
    public void setEnabled(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INsdManager {
    private static final String DESCRIPTOR = "android.net.nsd.INsdManager";
    
    static final int TRANSACTION_getMessenger = 1;
    
    static final int TRANSACTION_setEnabled = 2;
    
    public Stub() {
      attachInterface(this, "android.net.nsd.INsdManager");
    }
    
    public static INsdManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.nsd.INsdManager");
      if (iInterface != null && iInterface instanceof INsdManager)
        return (INsdManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "setEnabled";
      } 
      return "getMessenger";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool = false;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.net.nsd.INsdManager");
          return true;
        } 
        param1Parcel1.enforceInterface("android.net.nsd.INsdManager");
        if (param1Parcel1.readInt() != 0)
          bool = true; 
        setEnabled(bool);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel1.enforceInterface("android.net.nsd.INsdManager");
      Messenger messenger = getMessenger();
      param1Parcel2.writeNoException();
      if (messenger != null) {
        param1Parcel2.writeInt(1);
        messenger.writeToParcel(param1Parcel2, 1);
      } else {
        param1Parcel2.writeInt(0);
      } 
      return true;
    }
    
    private static class Proxy implements INsdManager {
      public static INsdManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.nsd.INsdManager";
      }
      
      public Messenger getMessenger() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Messenger messenger;
          parcel1.writeInterfaceToken("android.net.nsd.INsdManager");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && INsdManager.Stub.getDefaultImpl() != null) {
            messenger = INsdManager.Stub.getDefaultImpl().getMessenger();
            return messenger;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            messenger = Messenger.CREATOR.createFromParcel(parcel2);
          } else {
            messenger = null;
          } 
          return messenger;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.net.nsd.INsdManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool1 && INsdManager.Stub.getDefaultImpl() != null) {
            INsdManager.Stub.getDefaultImpl().setEnabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INsdManager param1INsdManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INsdManager != null) {
          Proxy.sDefaultImpl = param1INsdManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INsdManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
