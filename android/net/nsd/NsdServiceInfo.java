package android.net.nsd;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Base64;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Map;

public final class NsdServiceInfo implements Parcelable {
  private final ArrayMap<String, byte[]> mTxtRecord = new ArrayMap();
  
  private String mServiceType;
  
  private String mServiceName;
  
  private int mPort;
  
  private InetAddress mHost;
  
  private static final String TAG = "NsdServiceInfo";
  
  public NsdServiceInfo(String paramString1, String paramString2) {
    this.mServiceName = paramString1;
    this.mServiceType = paramString2;
  }
  
  public String getServiceName() {
    return this.mServiceName;
  }
  
  public void setServiceName(String paramString) {
    this.mServiceName = paramString;
  }
  
  public String getServiceType() {
    return this.mServiceType;
  }
  
  public void setServiceType(String paramString) {
    this.mServiceType = paramString;
  }
  
  public InetAddress getHost() {
    return this.mHost;
  }
  
  public void setHost(InetAddress paramInetAddress) {
    this.mHost = paramInetAddress;
  }
  
  public int getPort() {
    return this.mPort;
  }
  
  public void setPort(int paramInt) {
    this.mPort = paramInt;
  }
  
  public void setTxtRecords(String paramString) {
    byte[] arrayOfByte = Base64.decode(paramString, 0);
    int i = 0;
    while (i < arrayOfByte.length) {
      int j = arrayOfByte[i] & 0xFF;
      int k = i + 1;
      if (j != 0) {
        i = j;
        int m = j;
        try {
          if (k + j > arrayOfByte.length) {
            m = j;
            StringBuilder stringBuilder = new StringBuilder();
            m = j;
            this();
            m = j;
            stringBuilder.append("Corrupt record length (pos = ");
            m = j;
            stringBuilder.append(k);
            m = j;
            stringBuilder.append("): ");
            m = j;
            stringBuilder.append(j);
            m = j;
            Log.w("NsdServiceInfo", stringBuilder.toString());
            m = j;
            i = arrayOfByte.length - k;
          } 
          String str1 = null;
          paramString = null;
          int n = 0;
          byte[] arrayOfByte1;
          for (j = k; j < k + i; j++, arrayOfByte1 = arrayOfByte2, n = m) {
            byte[] arrayOfByte2;
            if (str1 == null) {
              String str = paramString;
              m = n;
              if (arrayOfByte[j] == 61) {
                m = i;
                str1 = new String(arrayOfByte, k, j - k, StandardCharsets.US_ASCII);
                str = paramString;
                m = n;
              } 
            } else {
              String str = paramString;
              if (paramString == null) {
                m = i;
                arrayOfByte2 = new byte[i - str1.length() - 1];
              } 
              arrayOfByte2[n] = arrayOfByte[j];
              m = n + 1;
            } 
          } 
          String str2 = str1;
          if (str1 == null) {
            m = i;
            str2 = new String();
            m = i;
            this(arrayOfByte, k, i, StandardCharsets.US_ASCII);
          } 
          m = i;
          if (!TextUtils.isEmpty(str2)) {
            m = i;
            if (!getAttributes().containsKey(str2)) {
              m = i;
              setAttribute(str2, arrayOfByte1);
            } else {
              m = i;
              IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
              m = i;
              StringBuilder stringBuilder = new StringBuilder();
              m = i;
              this();
              m = i;
              stringBuilder.append("Invalid txt record (duplicate key \"");
              m = i;
              stringBuilder.append(str2);
              m = i;
              stringBuilder.append("\")");
              m = i;
              this(stringBuilder.toString());
              m = i;
              throw illegalArgumentException;
            } 
          } else {
            m = i;
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
            m = i;
            this("Invalid txt record (key is empty)");
            m = i;
            throw illegalArgumentException;
          } 
        } catch (IllegalArgumentException illegalArgumentException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("While parsing txt records (pos = ");
          stringBuilder.append(k);
          stringBuilder.append("): ");
          stringBuilder.append(illegalArgumentException.getMessage());
          Log.e("NsdServiceInfo", stringBuilder.toString());
          i = m;
        } 
      } else {
        int m = j;
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        m = j;
        this("Zero sized txt record");
        m = j;
        throw illegalArgumentException;
      } 
      i = k + i;
    } 
  }
  
  public void setAttribute(String paramString, byte[] paramArrayOfbyte) {
    if (!TextUtils.isEmpty(paramString)) {
      int i;
      for (i = 0; i < paramString.length(); ) {
        char c = paramString.charAt(i);
        if (c >= ' ' && c <= '~') {
          if (c != '=') {
            i++;
            continue;
          } 
          throw new IllegalArgumentException("Key strings must not include '='");
        } 
        throw new IllegalArgumentException("Key strings must be printable US-ASCII");
      } 
      int j = paramString.length();
      boolean bool = false;
      if (paramArrayOfbyte == null) {
        i = 0;
      } else {
        i = paramArrayOfbyte.length;
      } 
      if (j + i < 255) {
        if (paramString.length() > 9) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Key lengths > 9 are discouraged: ");
          stringBuilder.append(paramString);
          Log.w("NsdServiceInfo", stringBuilder.toString());
        } 
        int k = getTxtRecordSize();
        j = paramString.length();
        if (paramArrayOfbyte == null) {
          i = bool;
        } else {
          i = paramArrayOfbyte.length;
        } 
        i = j + k + i + 2;
        if (i <= 1300) {
          if (i > 400)
            Log.w("NsdServiceInfo", "Total length of all attributes exceeds 400 bytes; truncation may occur"); 
          this.mTxtRecord.put(paramString, paramArrayOfbyte);
          return;
        } 
        throw new IllegalArgumentException("Total length of attributes must be < 1300 bytes");
      } 
      throw new IllegalArgumentException("Key length + value length must be < 255 bytes");
    } 
    throw new IllegalArgumentException("Key cannot be empty");
  }
  
  public void setAttribute(String paramString1, String paramString2) {
    if (paramString2 == null)
      try {
        arrayOfByte = (byte[])null;
        setAttribute(paramString1, arrayOfByte);
        return;
      } catch (UnsupportedEncodingException unsupportedEncodingException) {
        throw new IllegalArgumentException("Value must be UTF-8");
      }  
    byte[] arrayOfByte = arrayOfByte.getBytes("UTF-8");
    setAttribute((String)unsupportedEncodingException, arrayOfByte);
  }
  
  public void removeAttribute(String paramString) {
    this.mTxtRecord.remove(paramString);
  }
  
  public Map<String, byte[]> getAttributes() {
    return (Map)Collections.unmodifiableMap((Map)this.mTxtRecord);
  }
  
  private int getTxtRecordSize() {
    int i = 0;
    for (Map.Entry entry : this.mTxtRecord.entrySet()) {
      int k, j = ((String)entry.getKey()).length();
      byte[] arrayOfByte = (byte[])entry.getValue();
      if (arrayOfByte == null) {
        k = 0;
      } else {
        k = arrayOfByte.length;
      } 
      i = i + 2 + j + k;
    } 
    return i;
  }
  
  public byte[] getTxtRecord() {
    int i = getTxtRecordSize();
    if (i == 0)
      return new byte[0]; 
    byte[] arrayOfByte = new byte[i];
    i = 0;
    for (Map.Entry entry : this.mTxtRecord.entrySet()) {
      String str = (String)entry.getKey();
      byte[] arrayOfByte2 = (byte[])entry.getValue();
      int j = i + 1, k = str.length();
      if (arrayOfByte2 == null) {
        m = 0;
      } else {
        m = arrayOfByte2.length;
      } 
      arrayOfByte[i] = (byte)(k + m + 1);
      byte[] arrayOfByte1 = str.getBytes(StandardCharsets.US_ASCII);
      i = str.length();
      System.arraycopy(arrayOfByte1, 0, arrayOfByte, j, i);
      i = j + str.length();
      int m = i + 1;
      arrayOfByte[i] = 61;
      i = m;
      if (arrayOfByte2 != null) {
        System.arraycopy(arrayOfByte2, 0, arrayOfByte, m, arrayOfByte2.length);
        i = m + arrayOfByte2.length;
      } 
    } 
    return arrayOfByte;
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append("name: ");
    stringBuffer.append(this.mServiceName);
    stringBuffer.append(", type: ");
    stringBuffer.append(this.mServiceType);
    stringBuffer.append(", host: ");
    stringBuffer.append(this.mHost);
    stringBuffer.append(", port: ");
    stringBuffer.append(this.mPort);
    byte[] arrayOfByte = getTxtRecord();
    if (arrayOfByte != null) {
      stringBuffer.append(", txtRecord: ");
      stringBuffer.append(new String(arrayOfByte, StandardCharsets.UTF_8));
    } 
    return stringBuffer.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mServiceName);
    paramParcel.writeString(this.mServiceType);
    if (this.mHost != null) {
      paramParcel.writeInt(1);
      paramParcel.writeByteArray(this.mHost.getAddress());
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeInt(this.mPort);
    paramParcel.writeInt(this.mTxtRecord.size());
    for (String str : this.mTxtRecord.keySet()) {
      byte[] arrayOfByte = (byte[])this.mTxtRecord.get(str);
      if (arrayOfByte != null) {
        paramParcel.writeInt(1);
        paramParcel.writeInt(arrayOfByte.length);
        paramParcel.writeByteArray(arrayOfByte);
      } else {
        paramParcel.writeInt(0);
      } 
      paramParcel.writeString(str);
    } 
  }
  
  public static final Parcelable.Creator<NsdServiceInfo> CREATOR = new Parcelable.Creator<NsdServiceInfo>() {
      public NsdServiceInfo createFromParcel(Parcel param1Parcel) {
        NsdServiceInfo nsdServiceInfo = new NsdServiceInfo();
        NsdServiceInfo.access$002(nsdServiceInfo, param1Parcel.readString());
        NsdServiceInfo.access$102(nsdServiceInfo, param1Parcel.readString());
        if (param1Parcel.readInt() == 1)
          try {
            NsdServiceInfo.access$202(nsdServiceInfo, InetAddress.getByAddress(param1Parcel.createByteArray()));
          } catch (UnknownHostException unknownHostException) {} 
        NsdServiceInfo.access$302(nsdServiceInfo, param1Parcel.readInt());
        int i = param1Parcel.readInt();
        for (byte b = 0; b < i; b++) {
          byte[] arrayOfByte = null;
          if (param1Parcel.readInt() == 1) {
            int j = param1Parcel.readInt();
            arrayOfByte = new byte[j];
            param1Parcel.readByteArray(arrayOfByte);
          } 
          nsdServiceInfo.mTxtRecord.put(param1Parcel.readString(), arrayOfByte);
        } 
        return nsdServiceInfo;
      }
      
      public NsdServiceInfo[] newArray(int param1Int) {
        return new NsdServiceInfo[param1Int];
      }
    };
  
  public NsdServiceInfo() {}
}
