package android.net;

public class OplusWifiCommonConstant {
  public static final String CAPTIVE_PORTAL_MODE = "captive_portal_mode";
  
  public static final int CAPTIVE_PORTAL_MODE_PROMPT = 1;
  
  public static final String OPLUS_SLA_TRAFFIC = "OPLUS_SLA_TRAFFIC";
  
  public class OplusNetworkAgent {
    public static final int AUTO_CAPTIVE_NETWORK = 3;
    
    public static final int CAPTIVE_NETWORK = 4;
    
    public static final int EVENT_NETWORK_SCORE_CHANGED_WLAN_ASSIST = 528485;
    
    public static final int WIFI_BASE_SCORE = 5;
    
    public static final int WIFI_BASE_SCORE_VALID = 79;
    
    final OplusWifiCommonConstant this$0;
  }
  
  public class OplusNetworkCapabilities {
    public static final int TRANSPORT_WIFI_EXT = 8;
    
    final OplusWifiCommonConstant this$0;
  }
}
