package android.net;

import android.annotation.SystemApi;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.IPackageManager;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.system.OsConstants;
import com.android.internal.net.VpnConfig;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class VpnService extends Service {
  public static final String SERVICE_INTERFACE = "android.net.VpnService";
  
  public static final String SERVICE_META_DATA_SUPPORTS_ALWAYS_ON = "android.net.VpnService.SUPPORTS_ALWAYS_ON";
  
  private static IConnectivityManager getService() {
    IBinder iBinder = ServiceManager.getService("connectivity");
    return IConnectivityManager.Stub.asInterface(iBinder);
  }
  
  public static Intent prepare(Context paramContext) {
    try {
      boolean bool = getService().prepareVpn(paramContext.getPackageName(), null, paramContext.getUserId());
      if (bool)
        return null; 
    } catch (RemoteException remoteException) {}
    return VpnConfig.getIntentForConfirmation();
  }
  
  @SystemApi
  public static void prepareAndAuthorize(Context paramContext) {
    IConnectivityManager iConnectivityManager = getService();
    String str = paramContext.getPackageName();
    try {
      int i = paramContext.getUserId();
      if (!iConnectivityManager.prepareVpn(str, null, i))
        iConnectivityManager.prepareVpn(null, str, i); 
      iConnectivityManager.setVpnPackageAuthorization(str, i, 1);
    } catch (RemoteException remoteException) {}
  }
  
  public boolean protect(int paramInt) {
    return NetworkUtils.protectFromVpn(paramInt);
  }
  
  public boolean protect(Socket paramSocket) {
    return protect(paramSocket.getFileDescriptor$().getInt$());
  }
  
  public boolean protect(DatagramSocket paramDatagramSocket) {
    return protect(paramDatagramSocket.getFileDescriptor$().getInt$());
  }
  
  public boolean addAddress(InetAddress paramInetAddress, int paramInt) {
    check(paramInetAddress, paramInt);
    try {
      return getService().addVpnAddress(paramInetAddress.getHostAddress(), paramInt);
    } catch (RemoteException remoteException) {
      throw new IllegalStateException(remoteException);
    } 
  }
  
  public boolean removeAddress(InetAddress paramInetAddress, int paramInt) {
    check(paramInetAddress, paramInt);
    try {
      return getService().removeVpnAddress(paramInetAddress.getHostAddress(), paramInt);
    } catch (RemoteException remoteException) {
      throw new IllegalStateException(remoteException);
    } 
  }
  
  public boolean setUnderlyingNetworks(Network[] paramArrayOfNetwork) {
    try {
      return getService().setUnderlyingNetworksForVpn(paramArrayOfNetwork);
    } catch (RemoteException remoteException) {
      throw new IllegalStateException(remoteException);
    } 
  }
  
  public final boolean isAlwaysOn() {
    try {
      return getService().isCallerCurrentAlwaysOnVpnApp();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public final boolean isLockdownEnabled() {
    try {
      return getService().isCallerCurrentAlwaysOnVpnLockdownApp();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public IBinder onBind(Intent paramIntent) {
    if (paramIntent != null && "android.net.VpnService".equals(paramIntent.getAction()))
      return new Callback(); 
    return null;
  }
  
  public void onRevoke() {
    stopSelf();
  }
  
  class Callback extends Binder {
    final VpnService this$0;
    
    private Callback() {}
    
    protected boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) {
      if (param1Int1 == 16777215) {
        VpnService.this.onRevoke();
        return true;
      } 
      return false;
    }
  }
  
  private static void check(InetAddress paramInetAddress, int paramInt) {
    if (!paramInetAddress.isLoopbackAddress()) {
      if (paramInetAddress instanceof java.net.Inet4Address) {
        if (paramInt < 0 || paramInt > 32)
          throw new IllegalArgumentException("Bad prefixLength"); 
      } else {
        if (paramInetAddress instanceof java.net.Inet6Address) {
          if (paramInt < 0 || paramInt > 128)
            throw new IllegalArgumentException("Bad prefixLength"); 
          return;
        } 
        throw new IllegalArgumentException("Unsupported family");
      } 
      return;
    } 
    throw new IllegalArgumentException("Bad address");
  }
  
  class Builder {
    private final VpnConfig mConfig = new VpnConfig();
    
    private final List<LinkAddress> mAddresses = new ArrayList<>();
    
    private final List<RouteInfo> mRoutes = new ArrayList<>();
    
    final VpnService this$0;
    
    public Builder() {
      this.mConfig.user = VpnService.this.getClass().getName();
    }
    
    public Builder setSession(String param1String) {
      this.mConfig.session = param1String;
      return this;
    }
    
    public Builder setConfigureIntent(PendingIntent param1PendingIntent) {
      this.mConfig.configureIntent = param1PendingIntent;
      return this;
    }
    
    public Builder setMtu(int param1Int) {
      if (param1Int > 0) {
        this.mConfig.mtu = param1Int;
        return this;
      } 
      throw new IllegalArgumentException("Bad mtu");
    }
    
    public Builder setHttpProxy(ProxyInfo param1ProxyInfo) {
      this.mConfig.proxyInfo = param1ProxyInfo;
      return this;
    }
    
    public Builder addAddress(InetAddress param1InetAddress, int param1Int) {
      VpnService.check(param1InetAddress, param1Int);
      if (!param1InetAddress.isAnyLocalAddress()) {
        this.mAddresses.add(new LinkAddress(param1InetAddress, param1Int));
        this.mConfig.updateAllowedFamilies(param1InetAddress);
        return this;
      } 
      throw new IllegalArgumentException("Bad address");
    }
    
    public Builder addAddress(String param1String, int param1Int) {
      return addAddress(InetAddress.parseNumericAddress(param1String), param1Int);
    }
    
    public Builder addRoute(InetAddress param1InetAddress, int param1Int) {
      VpnService.check(param1InetAddress, param1Int);
      int i = param1Int / 8;
      byte[] arrayOfByte = param1InetAddress.getAddress();
      if (i < arrayOfByte.length)
        for (arrayOfByte[i] = (byte)(arrayOfByte[i] << param1Int % 8); i < arrayOfByte.length; ) {
          if (arrayOfByte[i] == 0) {
            i++;
            continue;
          } 
          throw new IllegalArgumentException("Bad address");
        }  
      this.mRoutes.add(new RouteInfo(new IpPrefix(param1InetAddress, param1Int), null));
      this.mConfig.updateAllowedFamilies(param1InetAddress);
      return this;
    }
    
    public Builder addRoute(String param1String, int param1Int) {
      return addRoute(InetAddress.parseNumericAddress(param1String), param1Int);
    }
    
    public Builder addDnsServer(InetAddress param1InetAddress) {
      if (!param1InetAddress.isLoopbackAddress() && !param1InetAddress.isAnyLocalAddress()) {
        if (this.mConfig.dnsServers == null)
          this.mConfig.dnsServers = new ArrayList(); 
        this.mConfig.dnsServers.add(param1InetAddress.getHostAddress());
        return this;
      } 
      throw new IllegalArgumentException("Bad address");
    }
    
    public Builder addDnsServer(String param1String) {
      return addDnsServer(InetAddress.parseNumericAddress(param1String));
    }
    
    public Builder addSearchDomain(String param1String) {
      if (this.mConfig.searchDomains == null)
        this.mConfig.searchDomains = new ArrayList(); 
      this.mConfig.searchDomains.add(param1String);
      return this;
    }
    
    public Builder allowFamily(int param1Int) {
      if (param1Int == OsConstants.AF_INET) {
        this.mConfig.allowIPv4 = true;
      } else {
        if (param1Int == OsConstants.AF_INET6) {
          this.mConfig.allowIPv6 = true;
          return this;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(param1Int);
        stringBuilder.append(" is neither ");
        stringBuilder.append(OsConstants.AF_INET);
        stringBuilder.append(" nor ");
        stringBuilder.append(OsConstants.AF_INET6);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      return this;
    }
    
    private void verifyApp(String param1String) throws PackageManager.NameNotFoundException {
      IBinder iBinder = ServiceManager.getService("package");
      IPackageManager iPackageManager = IPackageManager.Stub.asInterface(iBinder);
      try {
        iPackageManager.getApplicationInfo(param1String, 0, UserHandle.getCallingUserId());
        return;
      } catch (RemoteException remoteException) {
        throw new IllegalStateException(remoteException);
      } 
    }
    
    public Builder addAllowedApplication(String param1String) throws PackageManager.NameNotFoundException {
      if (this.mConfig.disallowedApplications == null) {
        verifyApp(param1String);
        if (this.mConfig.allowedApplications == null)
          this.mConfig.allowedApplications = new ArrayList(); 
        this.mConfig.allowedApplications.add(param1String);
        return this;
      } 
      throw new UnsupportedOperationException("addDisallowedApplication already called");
    }
    
    public Builder addDisallowedApplication(String param1String) throws PackageManager.NameNotFoundException {
      if (this.mConfig.allowedApplications == null) {
        verifyApp(param1String);
        if (this.mConfig.disallowedApplications == null)
          this.mConfig.disallowedApplications = new ArrayList(); 
        this.mConfig.disallowedApplications.add(param1String);
        return this;
      } 
      throw new UnsupportedOperationException("addAllowedApplication already called");
    }
    
    public Builder allowBypass() {
      this.mConfig.allowBypass = true;
      return this;
    }
    
    public Builder setBlocking(boolean param1Boolean) {
      this.mConfig.blocking = param1Boolean;
      return this;
    }
    
    public Builder setUnderlyingNetworks(Network[] param1ArrayOfNetwork) {
      VpnConfig vpnConfig = this.mConfig;
      if (param1ArrayOfNetwork != null) {
        param1ArrayOfNetwork = (Network[])param1ArrayOfNetwork.clone();
      } else {
        param1ArrayOfNetwork = null;
      } 
      vpnConfig.underlyingNetworks = param1ArrayOfNetwork;
      return this;
    }
    
    public Builder setMetered(boolean param1Boolean) {
      this.mConfig.isMetered = param1Boolean;
      return this;
    }
    
    public ParcelFileDescriptor establish() {
      this.mConfig.addresses = this.mAddresses;
      this.mConfig.routes = this.mRoutes;
      try {
        return VpnService.getService().establishVpn(this.mConfig);
      } catch (RemoteException remoteException) {
        throw new IllegalStateException(remoteException);
      } 
    }
  }
}
