package android.net;

import android.annotation.SystemApi;
import android.net.util.LinkPropertiesUtils;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public final class LinkProperties implements Parcelable {
  private final ArrayList<LinkAddress> mLinkAddresses = new ArrayList<>();
  
  private final ArrayList<InetAddress> mDnses = new ArrayList<>();
  
  private final ArrayList<InetAddress> mPcscfs = new ArrayList<>();
  
  private final ArrayList<InetAddress> mValidatedPrivateDnses = new ArrayList<>();
  
  private ArrayList<RouteInfo> mRoutes = new ArrayList<>();
  
  private Hashtable<String, LinkProperties> mStackedLinks = new Hashtable<>();
  
  class ProvisioningChange extends Enum<ProvisioningChange> {
    private static final ProvisioningChange[] $VALUES;
    
    public static final ProvisioningChange GAINED_PROVISIONING;
    
    public static final ProvisioningChange LOST_PROVISIONING = new ProvisioningChange("LOST_PROVISIONING", 1);
    
    public static ProvisioningChange[] values() {
      return (ProvisioningChange[])$VALUES.clone();
    }
    
    public static ProvisioningChange valueOf(String param1String) {
      return Enum.<ProvisioningChange>valueOf(ProvisioningChange.class, param1String);
    }
    
    private ProvisioningChange(LinkProperties this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static final ProvisioningChange STILL_NOT_PROVISIONED = new ProvisioningChange("STILL_NOT_PROVISIONED", 0);
    
    public static final ProvisioningChange STILL_PROVISIONED;
    
    static {
      GAINED_PROVISIONING = new ProvisioningChange("GAINED_PROVISIONING", 2);
      ProvisioningChange provisioningChange = new ProvisioningChange("STILL_PROVISIONED", 3);
      $VALUES = new ProvisioningChange[] { STILL_NOT_PROVISIONED, LOST_PROVISIONING, GAINED_PROVISIONING, provisioningChange };
    }
  }
  
  public static ProvisioningChange compareProvisioning(LinkProperties paramLinkProperties1, LinkProperties paramLinkProperties2) {
    if (paramLinkProperties1.isProvisioned() && paramLinkProperties2.isProvisioned()) {
      if ((paramLinkProperties1.isIpv4Provisioned() && !paramLinkProperties2.isIpv4Provisioned()) || (
        paramLinkProperties1.isIpv6Provisioned() && !paramLinkProperties2.isIpv6Provisioned()))
        return ProvisioningChange.LOST_PROVISIONING; 
      return ProvisioningChange.STILL_PROVISIONED;
    } 
    if (paramLinkProperties1.isProvisioned() && !paramLinkProperties2.isProvisioned())
      return ProvisioningChange.LOST_PROVISIONING; 
    if (!paramLinkProperties1.isProvisioned() && paramLinkProperties2.isProvisioned())
      return ProvisioningChange.GAINED_PROVISIONING; 
    return ProvisioningChange.STILL_NOT_PROVISIONED;
  }
  
  public LinkProperties() {
    this.mParcelSensitiveFields = false;
  }
  
  @SystemApi
  public LinkProperties(LinkProperties paramLinkProperties) {
    this(paramLinkProperties, false);
  }
  
  @SystemApi
  public LinkProperties(LinkProperties paramLinkProperties, boolean paramBoolean) {
    this.mParcelSensitiveFields = paramBoolean;
    if (paramLinkProperties == null)
      return; 
    this.mIfaceName = paramLinkProperties.mIfaceName;
    synchronized (this.mLinkAddresses) {
      ArrayList<InetAddress> arrayList;
      this.mLinkAddresses.addAll(paramLinkProperties.mLinkAddresses);
      synchronized (this.mDnses) {
        ProxyInfo proxyInfo;
        this.mDnses.addAll(paramLinkProperties.mDnses);
        this.mValidatedPrivateDnses.addAll(paramLinkProperties.mValidatedPrivateDnses);
        this.mUsePrivateDns = paramLinkProperties.mUsePrivateDns;
        this.mPrivateDnsServerName = paramLinkProperties.mPrivateDnsServerName;
        this.mPcscfs.addAll(paramLinkProperties.mPcscfs);
        this.mDomains = paramLinkProperties.mDomains;
        this.mRoutes.addAll(paramLinkProperties.mRoutes);
        if (paramLinkProperties.mHttpProxy == null) {
          arrayList = null;
        } else {
          proxyInfo = new ProxyInfo(paramLinkProperties.mHttpProxy);
        } 
        this.mHttpProxy = proxyInfo;
        for (LinkProperties linkProperties : paramLinkProperties.mStackedLinks.values())
          addStackedLink(linkProperties); 
        setMtu(paramLinkProperties.mMtu);
        setDhcpServerAddress(paramLinkProperties.getDhcpServerAddress());
        this.mTcpBufferSizes = paramLinkProperties.mTcpBufferSizes;
        this.mNat64Prefix = paramLinkProperties.mNat64Prefix;
        this.mWakeOnLanSupported = paramLinkProperties.mWakeOnLanSupported;
        this.mCaptivePortalApiUrl = paramLinkProperties.mCaptivePortalApiUrl;
        this.mCaptivePortalData = paramLinkProperties.mCaptivePortalData;
        return;
      } 
    } 
  }
  
  public void setInterfaceName(String paramString) {
    this.mIfaceName = paramString;
    synchronized (this.mRoutes) {
      ArrayList<RouteInfo> arrayList = new ArrayList();
      this(this.mRoutes.size());
      for (RouteInfo routeInfo : this.mRoutes)
        arrayList.add(routeWithInterface(routeInfo)); 
      this.mRoutes = arrayList;
      return;
    } 
  }
  
  public String getInterfaceName() {
    return this.mIfaceName;
  }
  
  @SystemApi
  public List<String> getAllInterfaceNames() {
    ArrayList<String> arrayList = new ArrayList(this.mStackedLinks.size() + 1);
    String str = this.mIfaceName;
    if (str != null)
      arrayList.add(str); 
    for (LinkProperties linkProperties : this.mStackedLinks.values())
      arrayList.addAll(linkProperties.getAllInterfaceNames()); 
    return arrayList;
  }
  
  @SystemApi
  public List<InetAddress> getAddresses() {
    ArrayList<InetAddress> arrayList = new ArrayList();
    for (LinkAddress linkAddress : this.mLinkAddresses)
      arrayList.add(linkAddress.getAddress()); 
    return Collections.unmodifiableList(arrayList);
  }
  
  public List<InetAddress> getAllAddresses() {
    ArrayList<InetAddress> arrayList = new ArrayList();
    for (LinkAddress linkAddress : this.mLinkAddresses)
      arrayList.add(linkAddress.getAddress()); 
    for (LinkProperties linkProperties : this.mStackedLinks.values())
      arrayList.addAll(linkProperties.getAllAddresses()); 
    return arrayList;
  }
  
  private int findLinkAddressIndex(LinkAddress paramLinkAddress) {
    for (byte b = 0; b < this.mLinkAddresses.size(); b++) {
      if (((LinkAddress)this.mLinkAddresses.get(b)).isSameAddressAs(paramLinkAddress))
        return b; 
    } 
    return -1;
  }
  
  @SystemApi
  public boolean addLinkAddress(LinkAddress paramLinkAddress) {
    if (paramLinkAddress == null)
      return false; 
    int i = findLinkAddressIndex(paramLinkAddress);
    if (i < 0) {
      this.mLinkAddresses.add(paramLinkAddress);
      return true;
    } 
    if (((LinkAddress)this.mLinkAddresses.get(i)).equals(paramLinkAddress))
      return false; 
    this.mLinkAddresses.set(i, paramLinkAddress);
    return true;
  }
  
  @SystemApi
  public boolean removeLinkAddress(LinkAddress paramLinkAddress) {
    int i = findLinkAddressIndex(paramLinkAddress);
    if (i >= 0) {
      this.mLinkAddresses.remove(i);
      return true;
    } 
    return false;
  }
  
  public List<LinkAddress> getLinkAddresses() {
    return Collections.unmodifiableList(this.mLinkAddresses);
  }
  
  @SystemApi
  public List<LinkAddress> getAllLinkAddresses() {
    ArrayList<LinkAddress> arrayList = new ArrayList<>(this.mLinkAddresses);
    for (LinkProperties linkProperties : this.mStackedLinks.values())
      arrayList.addAll(linkProperties.getAllLinkAddresses()); 
    return arrayList;
  }
  
  public void setLinkAddresses(Collection<LinkAddress> paramCollection) {
    this.mLinkAddresses.clear();
    for (LinkAddress linkAddress : paramCollection)
      addLinkAddress(linkAddress); 
  }
  
  @SystemApi
  public boolean addDnsServer(InetAddress paramInetAddress) {
    if (paramInetAddress != null && !this.mDnses.contains(paramInetAddress)) {
      this.mDnses.add(paramInetAddress);
      return true;
    } 
    return false;
  }
  
  @SystemApi
  public boolean removeDnsServer(InetAddress paramInetAddress) {
    return this.mDnses.remove(paramInetAddress);
  }
  
  public void setDnsServers(Collection<InetAddress> paramCollection) {
    this.mDnses.clear();
    for (InetAddress inetAddress : paramCollection)
      addDnsServer(inetAddress); 
  }
  
  public List<InetAddress> getDnsServers() {
    return Collections.unmodifiableList(this.mDnses);
  }
  
  @SystemApi
  public void setUsePrivateDns(boolean paramBoolean) {
    this.mUsePrivateDns = paramBoolean;
  }
  
  public boolean isPrivateDnsActive() {
    return this.mUsePrivateDns;
  }
  
  @SystemApi
  public void setPrivateDnsServerName(String paramString) {
    this.mPrivateDnsServerName = paramString;
  }
  
  public void setDhcpServerAddress(Inet4Address paramInet4Address) {
    this.mDhcpServerAddress = paramInet4Address;
  }
  
  public Inet4Address getDhcpServerAddress() {
    return this.mDhcpServerAddress;
  }
  
  public String getPrivateDnsServerName() {
    return this.mPrivateDnsServerName;
  }
  
  public boolean addValidatedPrivateDnsServer(InetAddress paramInetAddress) {
    if (paramInetAddress != null && !this.mValidatedPrivateDnses.contains(paramInetAddress)) {
      this.mValidatedPrivateDnses.add(paramInetAddress);
      return true;
    } 
    return false;
  }
  
  public boolean removeValidatedPrivateDnsServer(InetAddress paramInetAddress) {
    return this.mValidatedPrivateDnses.remove(paramInetAddress);
  }
  
  @SystemApi
  public void setValidatedPrivateDnsServers(Collection<InetAddress> paramCollection) {
    this.mValidatedPrivateDnses.clear();
    for (InetAddress inetAddress : paramCollection)
      addValidatedPrivateDnsServer(inetAddress); 
  }
  
  @SystemApi
  public List<InetAddress> getValidatedPrivateDnsServers() {
    return Collections.unmodifiableList(this.mValidatedPrivateDnses);
  }
  
  @SystemApi
  public boolean addPcscfServer(InetAddress paramInetAddress) {
    if (paramInetAddress != null && !this.mPcscfs.contains(paramInetAddress)) {
      this.mPcscfs.add(paramInetAddress);
      return true;
    } 
    return false;
  }
  
  public boolean removePcscfServer(InetAddress paramInetAddress) {
    return this.mPcscfs.remove(paramInetAddress);
  }
  
  @SystemApi
  public void setPcscfServers(Collection<InetAddress> paramCollection) {
    this.mPcscfs.clear();
    for (InetAddress inetAddress : paramCollection)
      addPcscfServer(inetAddress); 
  }
  
  @SystemApi
  public List<InetAddress> getPcscfServers() {
    return Collections.unmodifiableList(this.mPcscfs);
  }
  
  public void setDomains(String paramString) {
    this.mDomains = paramString;
  }
  
  public String getDomains() {
    return this.mDomains;
  }
  
  public void setMtu(int paramInt) {
    this.mMtu = paramInt;
  }
  
  public int getMtu() {
    return this.mMtu;
  }
  
  @SystemApi
  public void setTcpBufferSizes(String paramString) {
    this.mTcpBufferSizes = paramString;
  }
  
  @SystemApi
  public String getTcpBufferSizes() {
    return this.mTcpBufferSizes;
  }
  
  private RouteInfo routeWithInterface(RouteInfo paramRouteInfo) {
    IpPrefix ipPrefix = paramRouteInfo.getDestination();
    InetAddress inetAddress = paramRouteInfo.getGateway();
    String str = this.mIfaceName;
    int i = paramRouteInfo.getType();
    return new RouteInfo(ipPrefix, inetAddress, str, i, paramRouteInfo.getMtu());
  }
  
  private int findRouteIndexByRouteKey(RouteInfo paramRouteInfo) {
    for (byte b = 0; b < this.mRoutes.size(); b++) {
      if (((RouteInfo)this.mRoutes.get(b)).getRouteKey().equals(paramRouteInfo.getRouteKey()))
        return b; 
    } 
    return -1;
  }
  
  public boolean addRoute(RouteInfo paramRouteInfo) {
    String str = paramRouteInfo.getInterface();
    if (str == null || str.equals(this.mIfaceName)) {
      null = routeWithInterface(paramRouteInfo);
      synchronized (this.mRoutes) {
        int i = findRouteIndexByRouteKey(null);
        if (i == -1) {
          this.mRoutes.add(null);
          return true;
        } 
        if (((RouteInfo)this.mRoutes.get(i)).equals(null))
          return false; 
        this.mRoutes.set(i, null);
        return true;
      } 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Route added with non-matching interface: ");
    stringBuilder.append(str);
    stringBuilder.append(" vs. ");
    stringBuilder.append(this.mIfaceName);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  @SystemApi
  public boolean removeRoute(RouteInfo paramRouteInfo) {
    boolean bool;
    if (Objects.equals(this.mIfaceName, paramRouteInfo.getInterface()) && this.mRoutes.remove(paramRouteInfo)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public List<RouteInfo> getRoutes() {
    synchronized (this.mRoutes) {
      return Collections.unmodifiableList(this.mRoutes);
    } 
  }
  
  public void ensureDirectlyConnectedRoutes() {
    for (LinkAddress linkAddress : this.mLinkAddresses)
      addRoute(new RouteInfo(linkAddress, null, this.mIfaceName)); 
  }
  
  @SystemApi
  public List<RouteInfo> getAllRoutes() {
    ArrayList<RouteInfo> arrayList;
    null = new ArrayList();
    synchronized (this.mRoutes) {
      null.addAll(this.mRoutes);
      for (LinkProperties linkProperties : this.mStackedLinks.values())
        null.addAll(linkProperties.getAllRoutes()); 
      return null;
    } 
  }
  
  public void setHttpProxy(ProxyInfo paramProxyInfo) {
    this.mHttpProxy = paramProxyInfo;
  }
  
  public ProxyInfo getHttpProxy() {
    return this.mHttpProxy;
  }
  
  public IpPrefix getNat64Prefix() {
    return this.mNat64Prefix;
  }
  
  public void setNat64Prefix(IpPrefix paramIpPrefix) {
    if (paramIpPrefix == null || paramIpPrefix.getPrefixLength() == 96) {
      this.mNat64Prefix = paramIpPrefix;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Only 96-bit prefixes are supported: ");
    stringBuilder.append(paramIpPrefix);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public boolean addStackedLink(LinkProperties paramLinkProperties) {
    if (paramLinkProperties.getInterfaceName() != null) {
      this.mStackedLinks.put(paramLinkProperties.getInterfaceName(), paramLinkProperties);
      return true;
    } 
    return false;
  }
  
  public boolean removeStackedLink(String paramString) {
    boolean bool;
    LinkProperties linkProperties = this.mStackedLinks.remove(paramString);
    if (linkProperties != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public List<LinkProperties> getStackedLinks() {
    if (this.mStackedLinks.isEmpty())
      return Collections.emptyList(); 
    ArrayList<LinkProperties> arrayList = new ArrayList();
    for (LinkProperties linkProperties : this.mStackedLinks.values())
      arrayList.add(new LinkProperties(linkProperties)); 
    return Collections.unmodifiableList(arrayList);
  }
  
  public void clear() {
    if (!this.mParcelSensitiveFields) {
      this.mIfaceName = null;
      this.mLinkAddresses.clear();
      this.mDnses.clear();
      this.mUsePrivateDns = false;
      this.mPrivateDnsServerName = null;
      this.mPcscfs.clear();
      this.mDomains = null;
      synchronized (this.mRoutes) {
        this.mRoutes.clear();
        this.mHttpProxy = null;
        this.mStackedLinks.clear();
        this.mMtu = 0;
        this.mDhcpServerAddress = null;
        this.mTcpBufferSizes = null;
        this.mNat64Prefix = null;
        this.mWakeOnLanSupported = false;
        this.mCaptivePortalApiUrl = null;
        this.mCaptivePortalData = null;
        return;
      } 
    } 
    throw new UnsupportedOperationException("Cannot clear LinkProperties when parcelSensitiveFields is set");
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    ArrayList<RouteInfo> arrayList;
    StringJoiner stringJoiner;
    null = new StringJoiner(" ", "{", "}");
    if (this.mIfaceName != null) {
      null.add("InterfaceName:");
      null.add(this.mIfaceName);
    } 
    null.add("LinkAddresses: [");
    if (!this.mLinkAddresses.isEmpty())
      null.add(TextUtils.join(",", this.mLinkAddresses)); 
    null.add("]");
    null.add("DnsAddresses: [");
    if (!this.mDnses.isEmpty())
      null.add(TextUtils.join(",", this.mDnses)); 
    null.add("]");
    if (this.mUsePrivateDns)
      null.add("UsePrivateDns: true"); 
    if (this.mPrivateDnsServerName != null) {
      null.add("PrivateDnsServerName:");
      null.add(this.mPrivateDnsServerName);
    } 
    if (!this.mPcscfs.isEmpty()) {
      null.add("PcscfAddresses: [");
      null.add(TextUtils.join(",", this.mPcscfs));
      null.add("]");
    } 
    if (!this.mValidatedPrivateDnses.isEmpty()) {
      StringJoiner stringJoiner1 = new StringJoiner(",", "ValidatedPrivateDnsAddresses: [", "]");
      for (InetAddress inetAddress : this.mValidatedPrivateDnses)
        stringJoiner1.add(inetAddress.getHostAddress()); 
      null.add(stringJoiner1.toString());
    } 
    null.add("Domains:");
    null.add(this.mDomains);
    null.add("MTU:");
    null.add(Integer.toString(this.mMtu));
    if (this.mWakeOnLanSupported)
      null.add("WakeOnLanSupported: true"); 
    if (this.mDhcpServerAddress != null) {
      null.add("ServerAddress:");
      null.add(this.mDhcpServerAddress.toString());
    } 
    if (this.mCaptivePortalApiUrl != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("CaptivePortalApiUrl: ");
      stringBuilder.append(this.mCaptivePortalApiUrl);
      null.add(stringBuilder.toString());
    } 
    if (this.mCaptivePortalData != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("CaptivePortalData: ");
      stringBuilder.append(this.mCaptivePortalData);
      null.add(stringBuilder.toString());
    } 
    if (this.mTcpBufferSizes != null) {
      null.add("TcpBufferSizes:");
      null.add(this.mTcpBufferSizes);
    } 
    null.add("Routes: [");
    synchronized (this.mRoutes) {
      if (!this.mRoutes.isEmpty())
        null.add(TextUtils.join(",", this.mRoutes)); 
      null.add("]");
      if (this.mHttpProxy != null) {
        null.add("HttpProxy:");
        null.add(this.mHttpProxy.toString());
      } 
      if (this.mNat64Prefix != null) {
        null.add("Nat64Prefix:");
        null.add(this.mNat64Prefix.toString());
      } 
      Collection<LinkProperties> collection = this.mStackedLinks.values();
      if (!collection.isEmpty()) {
        stringJoiner = new StringJoiner(",", "Stacked: [", "]");
        for (LinkProperties linkProperties : collection) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("[ ");
          stringBuilder.append(linkProperties);
          stringBuilder.append(" ]");
          stringJoiner.add(stringBuilder.toString());
        } 
        null.add(stringJoiner.toString());
      } 
      return null.toString();
    } 
  }
  
  @SystemApi
  public boolean hasIpv4Address() {
    for (LinkAddress linkAddress : this.mLinkAddresses) {
      if (linkAddress.getAddress() instanceof Inet4Address)
        return true; 
    } 
    return false;
  }
  
  public boolean hasIPv4Address() {
    return hasIpv4Address();
  }
  
  private boolean hasIpv4AddressOnInterface(String paramString) {
    if (!Objects.equals(paramString, this.mIfaceName) || !hasIpv4Address()) {
      if (paramString != null) {
        Hashtable<String, LinkProperties> hashtable = this.mStackedLinks;
        if (hashtable.containsKey(paramString)) {
          hashtable = this.mStackedLinks;
          if (((LinkProperties)hashtable.get(paramString)).hasIpv4Address())
            return true; 
        } 
      } 
      return false;
    } 
    return true;
  }
  
  @SystemApi
  public boolean hasGlobalIpv6Address() {
    for (LinkAddress linkAddress : this.mLinkAddresses) {
      if (linkAddress.getAddress() instanceof Inet6Address && linkAddress.isGlobalPreferred())
        return true; 
    } 
    return false;
  }
  
  public boolean hasIpv4UnreachableDefaultRoute() {
    for (RouteInfo routeInfo : this.mRoutes) {
      if (routeInfo.isIPv4UnreachableDefault())
        return true; 
    } 
    return false;
  }
  
  public boolean hasGlobalIPv6Address() {
    return hasGlobalIpv6Address();
  }
  
  @SystemApi
  public boolean hasIpv4DefaultRoute() {
    synchronized (this.mRoutes) {
      for (RouteInfo routeInfo : this.mRoutes) {
        if (routeInfo.isIPv4Default())
          return true; 
      } 
      return false;
    } 
  }
  
  public boolean hasIpv6UnreachableDefaultRoute() {
    for (RouteInfo routeInfo : this.mRoutes) {
      if (routeInfo.isIPv6UnreachableDefault())
        return true; 
    } 
    return false;
  }
  
  public boolean hasIPv4DefaultRoute() {
    return hasIpv4DefaultRoute();
  }
  
  @SystemApi
  public boolean hasIpv6DefaultRoute() {
    synchronized (this.mRoutes) {
      for (RouteInfo routeInfo : this.mRoutes) {
        if (routeInfo.isIPv6Default())
          return true; 
      } 
      return false;
    } 
  }
  
  public boolean hasIPv6DefaultRoute() {
    return hasIpv6DefaultRoute();
  }
  
  @SystemApi
  public boolean hasIpv4DnsServer() {
    for (InetAddress inetAddress : this.mDnses) {
      if (inetAddress instanceof Inet4Address)
        return true; 
    } 
    return false;
  }
  
  public boolean hasIPv4DnsServer() {
    return hasIpv4DnsServer();
  }
  
  @SystemApi
  public boolean hasIpv6DnsServer() {
    for (InetAddress inetAddress : this.mDnses) {
      if (inetAddress instanceof Inet6Address)
        return true; 
    } 
    return false;
  }
  
  public boolean hasIPv6DnsServer() {
    return hasIpv6DnsServer();
  }
  
  public boolean hasIpv4PcscfServer() {
    for (InetAddress inetAddress : this.mPcscfs) {
      if (inetAddress instanceof Inet4Address)
        return true; 
    } 
    return false;
  }
  
  public boolean hasIpv6PcscfServer() {
    for (InetAddress inetAddress : this.mPcscfs) {
      if (inetAddress instanceof Inet6Address)
        return true; 
    } 
    return false;
  }
  
  @SystemApi
  public boolean isIpv4Provisioned() {
    boolean bool;
    if (hasIpv4Address() && 
      hasIpv4DefaultRoute() && 
      hasIpv4DnsServer()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @SystemApi
  public boolean isIpv6Provisioned() {
    boolean bool;
    if (hasGlobalIpv6Address() && 
      hasIpv6DefaultRoute() && 
      hasIpv6DnsServer()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isIPv6Provisioned() {
    return isIpv6Provisioned();
  }
  
  @SystemApi
  public boolean isProvisioned() {
    return (isIpv4Provisioned() || isIpv6Provisioned());
  }
  
  @SystemApi
  public boolean isReachable(InetAddress paramInetAddress) {
    List<RouteInfo> list = getAllRoutes();
    RouteInfo routeInfo = RouteInfo.selectBestRoute(list, paramInetAddress);
    boolean bool = false;
    null = false;
    if (routeInfo == null)
      return false; 
    if (paramInetAddress instanceof Inet4Address)
      return hasIpv4AddressOnInterface(routeInfo.getInterface()); 
    if (paramInetAddress instanceof Inet6Address) {
      if (paramInetAddress.isLinkLocalAddress()) {
        if (((Inet6Address)paramInetAddress).getScopeId() != 0)
          null = true; 
        return null;
      } 
      if (routeInfo.hasGateway()) {
        null = bool;
        return hasGlobalIpv6Address() ? true : null;
      } 
    } else {
      return false;
    } 
    return true;
  }
  
  public boolean isIdenticalInterfaceName(LinkProperties paramLinkProperties) {
    return LinkPropertiesUtils.isIdenticalInterfaceName(paramLinkProperties, this);
  }
  
  public boolean isIdenticalDhcpServerAddress(LinkProperties paramLinkProperties) {
    return Objects.equals(this.mDhcpServerAddress, paramLinkProperties.mDhcpServerAddress);
  }
  
  public boolean isIdenticalAddresses(LinkProperties paramLinkProperties) {
    return LinkPropertiesUtils.isIdenticalAddresses(paramLinkProperties, this);
  }
  
  public boolean isIdenticalDnses(LinkProperties paramLinkProperties) {
    return LinkPropertiesUtils.isIdenticalDnses(paramLinkProperties, this);
  }
  
  public boolean isIdenticalPrivateDns(LinkProperties paramLinkProperties) {
    if (isPrivateDnsActive() == paramLinkProperties.isPrivateDnsActive()) {
      String str2 = getPrivateDnsServerName();
      String str1 = paramLinkProperties.getPrivateDnsServerName();
      if (TextUtils.equals(str2, str1))
        return true; 
    } 
    return false;
  }
  
  public boolean isIdenticalValidatedPrivateDnses(LinkProperties paramLinkProperties) {
    boolean bool;
    List<InetAddress> list = paramLinkProperties.getValidatedPrivateDnsServers();
    if (this.mValidatedPrivateDnses.size() == list.size()) {
      bool = this.mValidatedPrivateDnses.containsAll(list);
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isIdenticalPcscfs(LinkProperties paramLinkProperties) {
    boolean bool;
    List<InetAddress> list = paramLinkProperties.getPcscfServers();
    if (this.mPcscfs.size() == list.size()) {
      bool = this.mPcscfs.containsAll(list);
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isIdenticalRoutes(LinkProperties paramLinkProperties) {
    return LinkPropertiesUtils.isIdenticalRoutes(paramLinkProperties, this);
  }
  
  public boolean isIdenticalHttpProxy(LinkProperties paramLinkProperties) {
    return LinkPropertiesUtils.isIdenticalHttpProxy(paramLinkProperties, this);
  }
  
  public boolean isIdenticalStackedLinks(LinkProperties paramLinkProperties) {
    if (!this.mStackedLinks.keySet().equals(paramLinkProperties.mStackedLinks.keySet()))
      return false; 
    for (LinkProperties linkProperties : this.mStackedLinks.values()) {
      String str = linkProperties.getInterfaceName();
      if (!linkProperties.equals(paramLinkProperties.mStackedLinks.get(str)))
        return false; 
    } 
    return true;
  }
  
  public boolean isIdenticalMtu(LinkProperties paramLinkProperties) {
    boolean bool;
    if (getMtu() == paramLinkProperties.getMtu()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isIdenticalTcpBufferSizes(LinkProperties paramLinkProperties) {
    return Objects.equals(this.mTcpBufferSizes, paramLinkProperties.mTcpBufferSizes);
  }
  
  public boolean isIdenticalNat64Prefix(LinkProperties paramLinkProperties) {
    return Objects.equals(this.mNat64Prefix, paramLinkProperties.mNat64Prefix);
  }
  
  public boolean isIdenticalWakeOnLan(LinkProperties paramLinkProperties) {
    boolean bool;
    if (isWakeOnLanSupported() == paramLinkProperties.isWakeOnLanSupported()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isIdenticalCaptivePortalApiUrl(LinkProperties paramLinkProperties) {
    return Objects.equals(this.mCaptivePortalApiUrl, paramLinkProperties.mCaptivePortalApiUrl);
  }
  
  public boolean isIdenticalCaptivePortalData(LinkProperties paramLinkProperties) {
    return Objects.equals(this.mCaptivePortalData, paramLinkProperties.mCaptivePortalData);
  }
  
  public void setWakeOnLanSupported(boolean paramBoolean) {
    this.mWakeOnLanSupported = paramBoolean;
  }
  
  public boolean isWakeOnLanSupported() {
    return this.mWakeOnLanSupported;
  }
  
  @SystemApi
  public void setCaptivePortalApiUrl(Uri paramUri) {
    this.mCaptivePortalApiUrl = paramUri;
  }
  
  @SystemApi
  public Uri getCaptivePortalApiUrl() {
    return this.mCaptivePortalApiUrl;
  }
  
  @SystemApi
  public void setCaptivePortalData(CaptivePortalData paramCaptivePortalData) {
    this.mCaptivePortalData = paramCaptivePortalData;
  }
  
  @SystemApi
  public CaptivePortalData getCaptivePortalData() {
    return this.mCaptivePortalData;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof LinkProperties))
      return false; 
    paramObject = paramObject;
    if (!isIdenticalInterfaceName((LinkProperties)paramObject) || 
      !isIdenticalAddresses((LinkProperties)paramObject) || 
      !isIdenticalDhcpServerAddress((LinkProperties)paramObject) || 
      !isIdenticalDnses((LinkProperties)paramObject) || 
      !isIdenticalPrivateDns((LinkProperties)paramObject) || 
      !isIdenticalValidatedPrivateDnses((LinkProperties)paramObject) || 
      !isIdenticalPcscfs((LinkProperties)paramObject) || 
      !isIdenticalRoutes((LinkProperties)paramObject) || 
      !isIdenticalHttpProxy((LinkProperties)paramObject) || 
      !isIdenticalStackedLinks((LinkProperties)paramObject) || 
      !isIdenticalMtu((LinkProperties)paramObject) || 
      !isIdenticalTcpBufferSizes((LinkProperties)paramObject) || 
      !isIdenticalNat64Prefix((LinkProperties)paramObject) || 
      !isIdenticalWakeOnLan((LinkProperties)paramObject) || 
      !isIdenticalCaptivePortalApiUrl((LinkProperties)paramObject) || 
      !isIdenticalCaptivePortalData((LinkProperties)paramObject))
      bool = false; 
    return bool;
  }
  
  public LinkPropertiesUtils.CompareResult<InetAddress> compareDnses(LinkProperties paramLinkProperties) {
    ArrayList<InetAddress> arrayList = this.mDnses;
    if (paramLinkProperties != null) {
      List<InetAddress> list = paramLinkProperties.getDnsServers();
    } else {
      paramLinkProperties = null;
    } 
    return new LinkPropertiesUtils.CompareResult<>(arrayList, (Collection<InetAddress>)paramLinkProperties);
  }
  
  public LinkPropertiesUtils.CompareResult<InetAddress> compareValidatedPrivateDnses(LinkProperties paramLinkProperties) {
    ArrayList<InetAddress> arrayList = this.mValidatedPrivateDnses;
    if (paramLinkProperties != null) {
      List<InetAddress> list = paramLinkProperties.getValidatedPrivateDnsServers();
    } else {
      paramLinkProperties = null;
    } 
    return new LinkPropertiesUtils.CompareResult<>(arrayList, (Collection<InetAddress>)paramLinkProperties);
  }
  
  public LinkPropertiesUtils.CompareResult<RouteInfo> compareAllRoutes(LinkProperties paramLinkProperties) {
    List<RouteInfo> list = getAllRoutes();
    if (paramLinkProperties != null) {
      List<RouteInfo> list1 = paramLinkProperties.getAllRoutes();
    } else {
      paramLinkProperties = null;
    } 
    return new LinkPropertiesUtils.CompareResult<>(list, (Collection<RouteInfo>)paramLinkProperties);
  }
  
  public LinkPropertiesUtils.CompareResult<String> compareAllInterfaceNames(LinkProperties paramLinkProperties) {
    List<String> list = getAllInterfaceNames();
    if (paramLinkProperties != null) {
      List<String> list1 = paramLinkProperties.getAllInterfaceNames();
    } else {
      paramLinkProperties = null;
    } 
    return new LinkPropertiesUtils.CompareResult<>(list, (Collection<String>)paramLinkProperties);
  }
  
  public int hashCode() {
    int i;
    byte b1;
    int j;
    byte b2;
    int k, n;
    String str2 = this.mIfaceName;
    if (str2 == null) {
      i = 0;
    } else {
      b1 = str2.hashCode();
      ArrayList<LinkAddress> arrayList3 = this.mLinkAddresses;
      j = arrayList3.size();
      ArrayList<InetAddress> arrayList2 = this.mDnses;
      b2 = arrayList2.size();
      arrayList2 = this.mValidatedPrivateDnses;
      k = arrayList2.size();
      String str = this.mDomains;
      if (str == null) {
        i = 0;
      } else {
        i = str.hashCode();
      } 
      ArrayList<RouteInfo> arrayList1 = this.mRoutes;
      int i4 = arrayList1.size();
      ProxyInfo proxyInfo = this.mHttpProxy;
      if (proxyInfo == null) {
        n = 0;
      } else {
        n = proxyInfo.hashCode();
      } 
      Hashtable<String, LinkProperties> hashtable = this.mStackedLinks;
      i = b1 + j * 31 + b2 * 37 + k * 61 + i + i4 * 41 + n + hashtable.hashCode() * 47;
    } 
    int m = this.mMtu;
    str2 = this.mTcpBufferSizes;
    if (str2 == null) {
      n = 0;
    } else {
      n = str2.hashCode();
    } 
    if (this.mUsePrivateDns) {
      b2 = 57;
    } else {
      b2 = 0;
    } 
    Inet4Address inet4Address = this.mDhcpServerAddress;
    if (inet4Address == null) {
      k = 0;
    } else {
      k = inet4Address.hashCode();
    } 
    ArrayList<InetAddress> arrayList = this.mPcscfs;
    int i1 = arrayList.size();
    String str1 = this.mPrivateDnsServerName;
    if (str1 == null) {
      j = 0;
    } else {
      j = str1.hashCode();
    } 
    IpPrefix ipPrefix = this.mNat64Prefix;
    int i2 = Objects.hash(new Object[] { ipPrefix });
    if (this.mWakeOnLanSupported) {
      b1 = 71;
    } else {
      b1 = 0;
    } 
    Uri uri = this.mCaptivePortalApiUrl;
    CaptivePortalData captivePortalData = this.mCaptivePortalData;
    int i3 = Objects.hash(new Object[] { uri, captivePortalData });
    return i + m * 51 + n + b2 + k + i1 * 67 + j + i2 + b1 + i3;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    CaptivePortalData captivePortalData;
    paramParcel.writeString(getInterfaceName());
    paramParcel.writeInt(this.mLinkAddresses.size());
    for (LinkAddress linkAddress : this.mLinkAddresses)
      paramParcel.writeParcelable(linkAddress, paramInt); 
    writeAddresses(paramParcel, this.mDnses);
    writeAddresses(paramParcel, this.mValidatedPrivateDnses);
    paramParcel.writeBoolean(this.mUsePrivateDns);
    paramParcel.writeString(this.mPrivateDnsServerName);
    writeAddresses(paramParcel, this.mPcscfs);
    paramParcel.writeString(this.mDomains);
    writeAddress(paramParcel, this.mDhcpServerAddress);
    paramParcel.writeInt(this.mMtu);
    paramParcel.writeString(this.mTcpBufferSizes);
    paramParcel.writeInt(this.mRoutes.size());
    for (RouteInfo routeInfo : this.mRoutes)
      paramParcel.writeParcelable(routeInfo, paramInt); 
    if (this.mHttpProxy != null) {
      paramParcel.writeByte((byte)1);
      paramParcel.writeParcelable(this.mHttpProxy, paramInt);
    } else {
      paramParcel.writeByte((byte)0);
    } 
    paramParcel.writeParcelable(this.mNat64Prefix, 0);
    ArrayList arrayList1 = new ArrayList(this.mStackedLinks.values());
    paramParcel.writeList(arrayList1);
    paramParcel.writeBoolean(this.mWakeOnLanSupported);
    boolean bool = this.mParcelSensitiveFields;
    ArrayList arrayList2 = null;
    if (bool) {
      Uri uri = this.mCaptivePortalApiUrl;
    } else {
      arrayList1 = null;
    } 
    paramParcel.writeParcelable((Parcelable)arrayList1, 0);
    arrayList1 = arrayList2;
    if (this.mParcelSensitiveFields)
      captivePortalData = this.mCaptivePortalData; 
    paramParcel.writeParcelable(captivePortalData, 0);
  }
  
  private static void writeAddresses(Parcel paramParcel, List<InetAddress> paramList) {
    paramParcel.writeInt(paramList.size());
    for (InetAddress inetAddress : paramList)
      writeAddress(paramParcel, inetAddress); 
  }
  
  private static void writeAddress(Parcel paramParcel, InetAddress paramInetAddress) {
    byte[] arrayOfByte;
    if (paramInetAddress == null) {
      arrayOfByte = null;
    } else {
      arrayOfByte = paramInetAddress.getAddress();
    } 
    paramParcel.writeByteArray(arrayOfByte);
    if (paramInetAddress instanceof Inet6Address) {
      boolean bool;
      paramInetAddress = paramInetAddress;
      if (paramInetAddress.getScopeId() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      paramParcel.writeBoolean(bool);
      if (bool)
        paramParcel.writeInt(paramInetAddress.getScopeId()); 
    } 
  }
  
  private static InetAddress readAddress(Parcel paramParcel) throws UnknownHostException {
    byte[] arrayOfByte = paramParcel.createByteArray();
    if (arrayOfByte == null)
      return null; 
    if (arrayOfByte.length == 16) {
      boolean bool1;
      boolean bool = paramParcel.readBoolean();
      if (bool) {
        bool1 = paramParcel.readInt();
      } else {
        bool1 = false;
      } 
      return Inet6Address.getByAddress((String)null, arrayOfByte, bool1);
    } 
    return InetAddress.getByAddress(arrayOfByte);
  }
  
  public static final Parcelable.Creator<LinkProperties> CREATOR = new Parcelable.Creator<LinkProperties>() {
      public LinkProperties createFromParcel(Parcel param1Parcel) {
        LinkProperties linkProperties = new LinkProperties();
        String str = param1Parcel.readString();
        if (str != null)
          linkProperties.setInterfaceName(str); 
        int i = param1Parcel.readInt();
        byte b;
        for (b = 0; b < i; b++)
          linkProperties.addLinkAddress(param1Parcel.<LinkAddress>readParcelable(null)); 
        i = param1Parcel.readInt();
        for (b = 0; b < i; b++) {
          try {
            linkProperties.addDnsServer(LinkProperties.readAddress(param1Parcel));
          } catch (UnknownHostException unknownHostException) {}
        } 
        i = param1Parcel.readInt();
        for (b = 0; b < i; b++) {
          try {
            linkProperties.addValidatedPrivateDnsServer(LinkProperties.readAddress(param1Parcel));
          } catch (UnknownHostException unknownHostException) {}
        } 
        linkProperties.setUsePrivateDns(param1Parcel.readBoolean());
        linkProperties.setPrivateDnsServerName(param1Parcel.readString());
        i = param1Parcel.readInt();
        for (b = 0; b < i; b++) {
          try {
            linkProperties.addPcscfServer(LinkProperties.readAddress(param1Parcel));
          } catch (UnknownHostException unknownHostException) {}
        } 
        linkProperties.setDomains(param1Parcel.readString());
        try {
          Inet4Address inet4Address = (Inet4Address)InetAddress.getByAddress(param1Parcel.createByteArray());
          linkProperties.setDhcpServerAddress(inet4Address);
        } catch (UnknownHostException unknownHostException) {}
        linkProperties.setMtu(param1Parcel.readInt());
        linkProperties.setTcpBufferSizes(param1Parcel.readString());
        i = param1Parcel.readInt();
        for (b = 0; b < i; b++)
          linkProperties.addRoute(param1Parcel.<RouteInfo>readParcelable(null)); 
        if (param1Parcel.readByte() == 1)
          linkProperties.setHttpProxy(param1Parcel.<ProxyInfo>readParcelable(null)); 
        linkProperties.setNat64Prefix(param1Parcel.<IpPrefix>readParcelable(null));
        ArrayList arrayList = new ArrayList();
        param1Parcel.readList(arrayList, LinkProperties.class.getClassLoader());
        for (LinkProperties linkProperties1 : arrayList)
          linkProperties.addStackedLink(linkProperties1); 
        linkProperties.setWakeOnLanSupported(param1Parcel.readBoolean());
        linkProperties.setCaptivePortalApiUrl(param1Parcel.<Uri>readParcelable(null));
        linkProperties.setCaptivePortalData(param1Parcel.<CaptivePortalData>readParcelable(null));
        return linkProperties;
      }
      
      public LinkProperties[] newArray(int param1Int) {
        return new LinkProperties[param1Int];
      }
    };
  
  private static final int INET6_ADDR_LENGTH = 16;
  
  private static final int MAX_MTU = 10000;
  
  private static final int MIN_MTU = 68;
  
  static final int MIN_MTU_V6 = 1280;
  
  private Uri mCaptivePortalApiUrl;
  
  private CaptivePortalData mCaptivePortalData;
  
  private Inet4Address mDhcpServerAddress;
  
  private String mDomains;
  
  private ProxyInfo mHttpProxy;
  
  private String mIfaceName;
  
  private int mMtu;
  
  private IpPrefix mNat64Prefix;
  
  private final transient boolean mParcelSensitiveFields;
  
  private String mPrivateDnsServerName;
  
  private String mTcpBufferSizes;
  
  private boolean mUsePrivateDns;
  
  private boolean mWakeOnLanSupported;
  
  public static boolean isValidMtu(int paramInt, boolean paramBoolean) {
    boolean bool1 = true, bool2 = true;
    if (paramBoolean) {
      if (paramInt >= 1280 && paramInt <= 10000) {
        paramBoolean = bool2;
      } else {
        paramBoolean = false;
      } 
      return paramBoolean;
    } 
    if (paramInt >= 68 && paramInt <= 10000) {
      paramBoolean = bool1;
    } else {
      paramBoolean = false;
    } 
    return paramBoolean;
  }
}
