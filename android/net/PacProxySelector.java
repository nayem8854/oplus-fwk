package android.net;

import android.os.IBinder;
import android.os.ServiceManager;
import android.util.Log;
import com.android.net.IProxyService;
import com.google.android.collect.Lists;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class PacProxySelector extends ProxySelector {
  private static final String PROXY = "PROXY ";
  
  public static final String PROXY_SERVICE = "com.android.net.IProxyService";
  
  private static final String SOCKS = "SOCKS ";
  
  private static final String TAG = "PacProxySelector";
  
  private final List<Proxy> mDefaultList;
  
  private IProxyService mProxyService;
  
  public PacProxySelector() {
    IBinder iBinder = ServiceManager.getService("com.android.net.IProxyService");
    IProxyService iProxyService = IProxyService.Stub.asInterface(iBinder);
    if (iProxyService == null)
      Log.e("PacProxySelector", "PacManager: no proxy service"); 
    this.mDefaultList = Lists.newArrayList((Object[])new Proxy[] { Proxy.NO_PROXY });
  }
  
  public List<Proxy> select(URI paramURI) {
    String str;
    if (this.mProxyService == null) {
      IBinder iBinder = ServiceManager.getService("com.android.net.IProxyService");
      this.mProxyService = IProxyService.Stub.asInterface(iBinder);
    } 
    if (this.mProxyService == null) {
      Log.e("PacProxySelector", "select: no proxy service return NO_PROXY");
      return Lists.newArrayList((Object[])new Proxy[] { Proxy.NO_PROXY });
    } 
    Exception exception2 = null;
    URI uRI1 = paramURI, uRI2 = paramURI, uRI3 = paramURI;
    try {
      if (!"http".equalsIgnoreCase(paramURI.getScheme())) {
        uRI2 = paramURI;
        uRI3 = paramURI;
        uRI1 = new URI();
        uRI2 = paramURI;
        uRI3 = paramURI;
        this(paramURI.getScheme(), null, paramURI.getHost(), paramURI.getPort(), "/", null, null);
      } 
      uRI2 = uRI1;
      uRI3 = uRI1;
      str = uRI1.toURL().toString();
    } catch (URISyntaxException null) {
      str = uRI3.getHost();
      uRI1 = uRI3;
    } catch (MalformedURLException null) {
      str = uRI2.getHost();
      uRI1 = uRI2;
    } 
    try {
      str = this.mProxyService.resolvePacFile(uRI1.getHost(), str);
    } catch (Exception exception1) {
      Log.e("PacProxySelector", "Error resolving PAC File", exception1);
      exception1 = exception2;
    } 
    if (exception1 == null)
      return this.mDefaultList; 
    return parseResponse((String)exception1);
  }
  
  private static List<Proxy> parseResponse(String paramString) {
    String[] arrayOfString = paramString.split(";");
    ArrayList<Proxy> arrayList = Lists.newArrayList();
    int i;
    byte b;
    for (i = arrayOfString.length, b = 0; b < i; ) {
      String str = arrayOfString[b];
      str = str.trim();
      if (str.equals("DIRECT")) {
        arrayList.add(Proxy.NO_PROXY);
      } else {
        Proxy proxy;
        if (str.startsWith("PROXY ")) {
          proxy = proxyFromHostPort(Proxy.Type.HTTP, str.substring("PROXY ".length()));
          if (proxy != null)
            arrayList.add(proxy); 
        } else if (proxy.startsWith("SOCKS ")) {
          proxy = proxyFromHostPort(Proxy.Type.SOCKS, proxy.substring("SOCKS ".length()));
          if (proxy != null)
            arrayList.add(proxy); 
        } 
      } 
      b++;
    } 
    if (arrayList.size() == 0)
      arrayList.add(Proxy.NO_PROXY); 
    return arrayList;
  }
  
  private static Proxy proxyFromHostPort(Proxy.Type paramType, String paramString) {
    try {
      String[] arrayOfString = paramString.split(":");
      String str = arrayOfString[0];
      int i = Integer.parseInt(arrayOfString[1]);
      return new Proxy(paramType, InetSocketAddress.createUnresolved(str, i));
    } catch (NumberFormatException|ArrayIndexOutOfBoundsException numberFormatException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to parse proxy ");
      stringBuilder.append(paramString);
      stringBuilder.append(" ");
      stringBuilder.append(numberFormatException);
      Log.d("PacProxySelector", stringBuilder.toString());
      return null;
    } 
  }
  
  public void connectFailed(URI paramURI, SocketAddress paramSocketAddress, IOException paramIOException) {}
}
