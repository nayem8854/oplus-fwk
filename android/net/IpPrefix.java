package android.net;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pair;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Comparator;

public final class IpPrefix implements Parcelable {
  private void checkAndMaskAddressAndPrefixLength() {
    byte[] arrayOfByte = this.address;
    if (arrayOfByte.length == 4 || arrayOfByte.length == 16) {
      NetworkUtils.maskRawAddress(this.address, this.prefixLength);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("IpPrefix has ");
    stringBuilder.append(this.address.length);
    stringBuilder.append(" bytes which is neither 4 nor 16");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public IpPrefix(byte[] paramArrayOfbyte, int paramInt) {
    this.address = (byte[])paramArrayOfbyte.clone();
    this.prefixLength = paramInt;
    checkAndMaskAddressAndPrefixLength();
  }
  
  @SystemApi
  public IpPrefix(InetAddress paramInetAddress, int paramInt) {
    this.address = paramInetAddress.getAddress();
    this.prefixLength = paramInt;
    checkAndMaskAddressAndPrefixLength();
  }
  
  @SystemApi
  public IpPrefix(String paramString) {
    Pair<InetAddress, Integer> pair = NetworkUtils.parseIpAndMask(paramString);
    this.address = ((InetAddress)pair.first).getAddress();
    this.prefixLength = ((Integer)pair.second).intValue();
    checkAndMaskAddressAndPrefixLength();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof IpPrefix;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    bool = bool1;
    if (Arrays.equals(this.address, ((IpPrefix)paramObject).address)) {
      bool = bool1;
      if (this.prefixLength == ((IpPrefix)paramObject).prefixLength)
        bool = true; 
    } 
    return bool;
  }
  
  public int hashCode() {
    return Arrays.hashCode(this.address) + this.prefixLength * 11;
  }
  
  public InetAddress getAddress() {
    try {
      return InetAddress.getByAddress(this.address);
    } catch (UnknownHostException unknownHostException) {
      throw new IllegalArgumentException("Address is invalid");
    } 
  }
  
  public byte[] getRawAddress() {
    return (byte[])this.address.clone();
  }
  
  public int getPrefixLength() {
    return this.prefixLength;
  }
  
  public boolean contains(InetAddress paramInetAddress) {
    byte[] arrayOfByte = paramInetAddress.getAddress();
    if (arrayOfByte == null || arrayOfByte.length != this.address.length)
      return false; 
    NetworkUtils.maskRawAddress(arrayOfByte, this.prefixLength);
    return Arrays.equals(this.address, arrayOfByte);
  }
  
  public boolean containsPrefix(IpPrefix paramIpPrefix) {
    if (paramIpPrefix.getPrefixLength() < this.prefixLength)
      return false; 
    byte[] arrayOfByte = paramIpPrefix.getRawAddress();
    NetworkUtils.maskRawAddress(arrayOfByte, this.prefixLength);
    return Arrays.equals(arrayOfByte, this.address);
  }
  
  public boolean isIPv6() {
    return getAddress() instanceof java.net.Inet6Address;
  }
  
  public boolean isIPv4() {
    return getAddress() instanceof java.net.Inet4Address;
  }
  
  public String toString() {
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append(InetAddress.getByAddress(this.address).getHostAddress());
      stringBuilder.append("/");
      stringBuilder.append(this.prefixLength);
      return stringBuilder.toString();
    } catch (UnknownHostException unknownHostException) {
      throw new IllegalStateException("IpPrefix with invalid address! Shouldn't happen.", unknownHostException);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByteArray(this.address);
    paramParcel.writeInt(this.prefixLength);
  }
  
  public static Comparator<IpPrefix> lengthComparator() {
    return (Comparator<IpPrefix>)new Object();
  }
  
  public static final Parcelable.Creator<IpPrefix> CREATOR = new Parcelable.Creator<IpPrefix>() {
      public IpPrefix createFromParcel(Parcel param1Parcel) {
        byte[] arrayOfByte = param1Parcel.createByteArray();
        int i = param1Parcel.readInt();
        return new IpPrefix(arrayOfByte, i);
      }
      
      public IpPrefix[] newArray(int param1Int) {
        return new IpPrefix[param1Int];
      }
    };
  
  private final byte[] address;
  
  private final int prefixLength;
}
