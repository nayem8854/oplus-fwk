package android.net.util;

import android.net.MacAddress;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public final class MacAddressUtils {
  private static final String DEFAULT_MAC_ADDRESS = "02:00:00:00:00:00";
  
  private static final int ETHER_ADDR_LEN = 6;
  
  private static final long LOCALLY_ASSIGNED_MASK;
  
  private static final long MULTICAST_MASK;
  
  private static final long NIC_MASK;
  
  private static final long OUI_MASK;
  
  private static final long VALID_LONG_MASK = 281474976710655L;
  
  static {
    byte[] arrayOfByte = MacAddress.fromString("2:0:0:0:0:0").toByteArray();
    LOCALLY_ASSIGNED_MASK = longAddrFromByteAddr(arrayOfByte);
    arrayOfByte = MacAddress.fromString("1:0:0:0:0:0").toByteArray();
    MULTICAST_MASK = longAddrFromByteAddr(arrayOfByte);
    arrayOfByte = MacAddress.fromString("ff:ff:ff:0:0:0").toByteArray();
    OUI_MASK = longAddrFromByteAddr(arrayOfByte);
    arrayOfByte = MacAddress.fromString("0:0:0:ff:ff:ff").toByteArray();
    NIC_MASK = longAddrFromByteAddr(arrayOfByte);
  }
  
  public static boolean isMulticastAddress(MacAddress paramMacAddress) {
    boolean bool;
    if ((longAddrFromByteAddr(paramMacAddress.toByteArray()) & MULTICAST_MASK) != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static MacAddress createRandomUnicastAddress() {
    return createRandomUnicastAddress(null, new SecureRandom());
  }
  
  public static MacAddress createRandomUnicastAddress(MacAddress paramMacAddress, Random paramRandom) {
    long l1;
    if (paramMacAddress == null) {
      l1 = paramRandom.nextLong() & 0xFFFFFFFFFFFFL;
    } else {
      long l4 = longAddrFromByteAddr(paramMacAddress.toByteArray());
      l1 = OUI_MASK;
      long l5 = NIC_MASK;
      l1 = l4 & l1 | l5 & paramRandom.nextLong();
    } 
    long l3 = LOCALLY_ASSIGNED_MASK;
    long l2 = MULTICAST_MASK;
    MacAddress macAddress = MacAddress.fromBytes(byteAddrFromLongAddr((l1 | l3) & (l2 ^ 0xFFFFFFFFFFFFFFFFL)));
    if (macAddress.equals("02:00:00:00:00:00"))
      return createRandomUnicastAddress(paramMacAddress, paramRandom); 
    return macAddress;
  }
  
  public static long longAddrFromByteAddr(byte[] paramArrayOfbyte) {
    Objects.requireNonNull(paramArrayOfbyte);
    if (isMacAddress(paramArrayOfbyte)) {
      long l = 0L;
      int i;
      byte b;
      for (i = paramArrayOfbyte.length, b = 0; b < i; ) {
        byte b1 = paramArrayOfbyte[b];
        l = (l << 8L) + (b1 & 0xFF);
        b++;
      } 
      return l;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(Arrays.toString(paramArrayOfbyte));
    stringBuilder.append(" was not a valid MAC address");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static byte[] byteAddrFromLongAddr(long paramLong) {
    byte[] arrayOfByte = new byte[6];
    int i = 6;
    while (true) {
      int j = i - 1;
      if (i > 0) {
        arrayOfByte[j] = (byte)(int)paramLong;
        paramLong >>= 8L;
        i = j;
        continue;
      } 
      break;
    } 
    return arrayOfByte;
  }
  
  public static boolean isMacAddress(byte[] paramArrayOfbyte) {
    boolean bool;
    if (paramArrayOfbyte != null && paramArrayOfbyte.length == 6) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
