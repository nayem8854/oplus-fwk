package android.net.util;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.UserHandle;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Slog;
import java.util.Arrays;
import java.util.List;

public class MultinetworkPolicyTracker {
  private static String TAG = MultinetworkPolicyTracker.class.getSimpleName();
  
  private volatile boolean mAvoidBadWifi = true;
  
  private int mActiveSubId = -1;
  
  private final Runnable mAvoidBadWifiCallback;
  
  private final BroadcastReceiver mBroadcastReceiver;
  
  private final Context mContext;
  
  private final Handler mHandler;
  
  private volatile int mMeteredMultipathPreference;
  
  private final ContentResolver mResolver;
  
  private final SettingObserver mSettingObserver;
  
  private final List<Uri> mSettingsUris;
  
  public MultinetworkPolicyTracker(Context paramContext, Handler paramHandler) {
    this(paramContext, paramHandler, null);
  }
  
  public MultinetworkPolicyTracker(Context paramContext, Handler paramHandler, Runnable paramRunnable) {
    this.mContext = paramContext;
    this.mHandler = paramHandler;
    this.mAvoidBadWifiCallback = paramRunnable;
    Uri uri1 = Settings.Global.getUriFor("network_avoid_bad_wifi");
    Uri uri2 = Settings.Global.getUriFor("network_metered_multipath_preference");
    this.mSettingsUris = Arrays.asList(new Uri[] { uri1, uri2 });
    this.mResolver = this.mContext.getContentResolver();
    this.mSettingObserver = new SettingObserver();
    this.mBroadcastReceiver = (BroadcastReceiver)new Object(this);
    TelephonyManager telephonyManager = (TelephonyManager)paramContext.getSystemService(TelephonyManager.class);
    Object object = new Object(this, paramHandler.getLooper());
    telephonyManager.listen((PhoneStateListener)object, 4194304);
    updateAvoidBadWifi();
    updateMeteredMultipathPreference();
  }
  
  public void start() {
    for (Uri uri : this.mSettingsUris)
      this.mResolver.registerContentObserver(uri, false, this.mSettingObserver); 
    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction("android.intent.action.CONFIGURATION_CHANGED");
    this.mContext.registerReceiverAsUser(this.mBroadcastReceiver, UserHandle.ALL, intentFilter, null, this.mHandler);
    reevaluate();
  }
  
  public void shutdown() {
    try {
      this.mResolver.unregisterContentObserver(this.mSettingObserver);
    } catch (Exception exception) {
      Slog.e(TAG, "Couldn't unregister SettingObserver: %s", exception);
    } 
    try {
      this.mContext.unregisterReceiver(this.mBroadcastReceiver);
    } catch (Exception exception) {
      Slog.e(TAG, "Couldn't unregister BroadcastReceiver: %s", exception);
    } 
  }
  
  public boolean getAvoidBadWifi() {
    return this.mAvoidBadWifi;
  }
  
  public int getMeteredMultipathPreference() {
    return this.mMeteredMultipathPreference;
  }
  
  public boolean configRestrictsAvoidBadWifi() {
    boolean bool;
    if (getResourcesForActiveSubId().getInteger(17694852) == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private Resources getResourcesForActiveSubId() {
    return SubscriptionManager.getResourcesForSubId(this.mContext, this.mActiveSubId);
  }
  
  public boolean shouldNotifyWifiUnvalidated() {
    boolean bool;
    if (configRestrictsAvoidBadWifi() && getAvoidBadWifiSetting() == null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String getAvoidBadWifiSetting() {
    return Settings.Global.getString(this.mResolver, "network_avoid_bad_wifi");
  }
  
  public void reevaluate() {
    this.mHandler.post(new _$$Lambda$MultinetworkPolicyTracker$8YMQ0fPTKk7Fw__gJjln0JT_g8E(this));
  }
  
  private void reevaluateInternal() {
    if (updateAvoidBadWifi()) {
      Runnable runnable = this.mAvoidBadWifiCallback;
      if (runnable != null)
        runnable.run(); 
    } 
    updateMeteredMultipathPreference();
  }
  
  public boolean updateAvoidBadWifi() {
    boolean bool1 = "1".equals(getAvoidBadWifiSetting());
    boolean bool2 = this.mAvoidBadWifi;
    boolean bool = false;
    if (bool1 || !configRestrictsAvoidBadWifi()) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    this.mAvoidBadWifi = bool1;
    bool1 = bool;
    if (this.mAvoidBadWifi != bool2)
      bool1 = true; 
    return bool1;
  }
  
  public int configMeteredMultipathPreference() {
    return this.mContext.getResources().getInteger(17694854);
  }
  
  public void updateMeteredMultipathPreference() {
    String str = Settings.Global.getString(this.mResolver, "network_metered_multipath_preference");
    try {
      this.mMeteredMultipathPreference = Integer.parseInt(str);
    } catch (NumberFormatException numberFormatException) {
      this.mMeteredMultipathPreference = configMeteredMultipathPreference();
    } 
  }
  
  class SettingObserver extends ContentObserver {
    final MultinetworkPolicyTracker this$0;
    
    public SettingObserver() {
      super(null);
    }
    
    public void onChange(boolean param1Boolean) {
      Slog.wtf(MultinetworkPolicyTracker.TAG, "Should never be reached.");
    }
    
    public void onChange(boolean param1Boolean, Uri param1Uri) {
      if (!MultinetworkPolicyTracker.this.mSettingsUris.contains(param1Uri)) {
        String str = MultinetworkPolicyTracker.TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unexpected settings observation: ");
        stringBuilder.append(param1Uri);
        Slog.wtf(str, stringBuilder.toString());
      } 
      MultinetworkPolicyTracker.this.reevaluate();
    }
  }
}
