package android.net.util.nsd;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class DnsSdTxtRecord implements Parcelable {
  public DnsSdTxtRecord() {
    this.mData = new byte[0];
  }
  
  public DnsSdTxtRecord(byte[] paramArrayOfbyte) {
    this.mData = (byte[])paramArrayOfbyte.clone();
  }
  
  public DnsSdTxtRecord(DnsSdTxtRecord paramDnsSdTxtRecord) {
    if (paramDnsSdTxtRecord != null) {
      byte[] arrayOfByte = paramDnsSdTxtRecord.mData;
      if (arrayOfByte != null)
        this.mData = (byte[])arrayOfByte.clone(); 
    } 
  }
  
  public void set(String paramString1, String paramString2) {
    int i;
    if (paramString2 != null) {
      byte[] arrayOfByte = paramString2.getBytes();
      i = arrayOfByte.length;
    } else {
      paramString2 = null;
      i = 0;
    } 
    try {
      byte[] arrayOfByte = paramString1.getBytes("US-ASCII");
      int j;
      for (j = 0; j < arrayOfByte.length; ) {
        if (arrayOfByte[j] != 61) {
          j++;
          continue;
        } 
        throw new IllegalArgumentException("= is not a valid character in key");
      } 
      if (arrayOfByte.length + i < 255) {
        j = remove(paramString1);
        i = j;
        if (j == -1)
          i = keyCount(); 
        insert(arrayOfByte, (byte[])paramString2, i);
        return;
      } 
      throw new IllegalArgumentException("Key and Value length cannot exceed 255 bytes");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      throw new IllegalArgumentException("key should be US-ASCII");
    } 
  }
  
  public String get(String paramString) {
    byte[] arrayOfByte = getValue(paramString);
    if (arrayOfByte != null) {
      String str = new String(arrayOfByte);
    } else {
      arrayOfByte = null;
    } 
    return (String)arrayOfByte;
  }
  
  public int remove(String paramString) {
    int i = 0;
    byte b = 0;
    while (true) {
      byte[] arrayOfByte = this.mData;
      if (i < arrayOfByte.length) {
        byte b1 = arrayOfByte[i];
        if (paramString.length() <= b1 && (
          paramString.length() == b1 || this.mData[paramString.length() + i + 1] == 61)) {
          String str = new String(this.mData, i + 1, paramString.length());
          if (paramString.compareToIgnoreCase(str) == 0) {
            byte[] arrayOfByte2 = this.mData;
            byte[] arrayOfByte1 = new byte[arrayOfByte2.length - b1 - 1];
            System.arraycopy(arrayOfByte2, 0, arrayOfByte1, 0, i);
            System.arraycopy(arrayOfByte2, i + b1 + 1, this.mData, i, arrayOfByte2.length - i - b1 - 1);
            return b;
          } 
        } 
        i += b1 + 1 & 0xFF;
        b++;
        continue;
      } 
      break;
    } 
    return -1;
  }
  
  public int keyCount() {
    byte b = 0;
    int i = 0;
    while (true) {
      byte[] arrayOfByte = this.mData;
      if (i < arrayOfByte.length) {
        i += arrayOfByte[i] + 1 & 0xFF;
        b++;
        continue;
      } 
      break;
    } 
    return b;
  }
  
  public boolean contains(String paramString) {
    byte b = 0;
    while (true) {
      String str = getKey(b);
      if (str != null) {
        if (paramString.compareToIgnoreCase(str) == 0)
          return true; 
        b++;
        continue;
      } 
      break;
    } 
    return false;
  }
  
  public int size() {
    return this.mData.length;
  }
  
  public byte[] getRawData() {
    return (byte[])this.mData.clone();
  }
  
  private void insert(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, int paramInt) {
    byte b;
    byte[] arrayOfByte1 = this.mData;
    if (paramArrayOfbyte2 != null) {
      b = paramArrayOfbyte2.length;
    } else {
      b = 0;
    } 
    int i = 0;
    int j;
    for (j = 0; j < paramInt; ) {
      byte[] arrayOfByte = this.mData;
      if (i < arrayOfByte.length) {
        i += arrayOfByte[i] + 1 & 0xFF;
        j++;
      } 
    } 
    j = paramArrayOfbyte1.length;
    if (paramArrayOfbyte2 != null) {
      paramInt = 1;
    } else {
      paramInt = 0;
    } 
    j = j + b + paramInt;
    int k = arrayOfByte1.length + j + 1;
    byte[] arrayOfByte2 = new byte[k];
    System.arraycopy(arrayOfByte1, 0, arrayOfByte2, 0, i);
    paramInt = arrayOfByte1.length - i;
    System.arraycopy(arrayOfByte1, i, this.mData, k - paramInt, paramInt);
    arrayOfByte1 = this.mData;
    arrayOfByte1[i] = (byte)j;
    System.arraycopy(paramArrayOfbyte1, 0, arrayOfByte1, i + 1, paramArrayOfbyte1.length);
    if (paramArrayOfbyte2 != null) {
      arrayOfByte1 = this.mData;
      arrayOfByte1[i + 1 + paramArrayOfbyte1.length] = 61;
      System.arraycopy(paramArrayOfbyte2, 0, arrayOfByte1, paramArrayOfbyte1.length + i + 2, b);
    } 
  }
  
  private String getKey(int paramInt) {
    int i = 0;
    byte b;
    for (b = 0; b < paramInt; ) {
      byte[] arrayOfByte1 = this.mData;
      if (i < arrayOfByte1.length) {
        i += arrayOfByte1[i] + 1;
        b++;
      } 
    } 
    byte[] arrayOfByte = this.mData;
    if (i < arrayOfByte.length) {
      b = arrayOfByte[i];
      for (paramInt = 0; paramInt < b && 
        this.mData[i + paramInt + 1] != 61; paramInt++);
      return new String(this.mData, i + 1, paramInt);
    } 
    return null;
  }
  
  private byte[] getValue(int paramInt) {
    int i = 0;
    byte[] arrayOfByte1 = null;
    byte b;
    for (b = 0; b < paramInt; ) {
      byte[] arrayOfByte = this.mData;
      if (i < arrayOfByte.length) {
        i += arrayOfByte[i] + 1;
        b++;
      } 
    } 
    byte[] arrayOfByte3 = this.mData, arrayOfByte2 = arrayOfByte1;
    if (i < arrayOfByte3.length) {
      b = arrayOfByte3[i];
      paramInt = 0;
      while (true) {
        arrayOfByte2 = arrayOfByte1;
        if (paramInt < b) {
          arrayOfByte3 = this.mData;
          if (arrayOfByte3[i + paramInt + 1] == 61) {
            arrayOfByte2 = new byte[b - paramInt - 1];
            System.arraycopy(arrayOfByte3, i + paramInt + 2, arrayOfByte2, 0, b - paramInt - 1);
            break;
          } 
          paramInt++;
          continue;
        } 
        break;
      } 
    } 
    return arrayOfByte2;
  }
  
  private String getValueAsString(int paramInt) {
    byte[] arrayOfByte = getValue(paramInt);
    if (arrayOfByte != null) {
      String str = new String(arrayOfByte);
    } else {
      arrayOfByte = null;
    } 
    return (String)arrayOfByte;
  }
  
  private byte[] getValue(String paramString) {
    byte b = 0;
    while (true) {
      String str = getKey(b);
      if (str != null) {
        if (paramString.compareToIgnoreCase(str) == 0)
          return getValue(b); 
        b++;
        continue;
      } 
      break;
    } 
    return null;
  }
  
  public String toString() {
    String str = null;
    byte b = 0;
    while (true) {
      String str1 = getKey(b);
      if (str1 != null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{");
        stringBuilder.append(str1);
        String str2 = stringBuilder.toString();
        str1 = getValueAsString(b);
        if (str1 != null) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append(str2);
          stringBuilder1.append("=");
          stringBuilder1.append(str1);
          stringBuilder1.append("}");
          str2 = stringBuilder1.toString();
        } else {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append(str2);
          stringBuilder1.append("}");
          str2 = stringBuilder1.toString();
        } 
        if (str == null) {
          str = str2;
        } else {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append(str);
          stringBuilder1.append(", ");
          stringBuilder1.append(str2);
          str = stringBuilder1.toString();
        } 
        b++;
        continue;
      } 
      break;
    } 
    if (str == null)
      str = ""; 
    return str;
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject == this)
      return true; 
    if (!(paramObject instanceof DnsSdTxtRecord))
      return false; 
    paramObject = paramObject;
    return Arrays.equals(((DnsSdTxtRecord)paramObject).mData, this.mData);
  }
  
  public int hashCode() {
    return Arrays.hashCode(this.mData);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByteArray(this.mData);
  }
  
  public static final Parcelable.Creator<DnsSdTxtRecord> CREATOR = new Parcelable.Creator<DnsSdTxtRecord>() {
      public DnsSdTxtRecord createFromParcel(Parcel param1Parcel) {
        DnsSdTxtRecord dnsSdTxtRecord = new DnsSdTxtRecord();
        param1Parcel.readByteArray(dnsSdTxtRecord.mData);
        return dnsSdTxtRecord;
      }
      
      public DnsSdTxtRecord[] newArray(int param1Int) {
        return new DnsSdTxtRecord[param1Int];
      }
    };
  
  private static final byte mSeparator = 61;
  
  private byte[] mData;
}
