package android.net.util;

import android.net.LinkAddress;
import android.net.LinkProperties;
import android.net.RouteInfo;
import android.text.TextUtils;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

public final class LinkPropertiesUtils {
  public static class CompareResult<T> {
    public final List<T> removed = new ArrayList<>();
    
    public final List<T> added = new ArrayList<>();
    
    public CompareResult() {}
    
    public CompareResult(Collection<T> param1Collection1, Collection<T> param1Collection2) {
      if (param1Collection1 != null)
        this.removed.addAll(param1Collection1); 
      if (param1Collection2 != null)
        for (Collection<T> param1Collection1 : param1Collection2) {
          if (!this.removed.remove(param1Collection1))
            this.added.add((T)param1Collection1); 
        }  
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removed=[");
      stringBuilder.append(TextUtils.join(",", this.removed));
      stringBuilder.append("] added=[");
      List<T> list = this.added;
      stringBuilder.append(TextUtils.join(",", list));
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
  }
  
  public static class CompareOrUpdateResult<K, T> {
    public final List<T> added;
    
    public final List<T> removed;
    
    public final List<T> updated;
    
    public CompareOrUpdateResult(Collection<T> param1Collection1, Collection<T> param1Collection2, Function<T, K> param1Function) {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial <init> : ()V
      //   4: aload_0
      //   5: new java/util/ArrayList
      //   8: dup
      //   9: invokespecial <init> : ()V
      //   12: putfield added : Ljava/util/List;
      //   15: aload_0
      //   16: new java/util/ArrayList
      //   19: dup
      //   20: invokespecial <init> : ()V
      //   23: putfield removed : Ljava/util/List;
      //   26: aload_0
      //   27: new java/util/ArrayList
      //   30: dup
      //   31: invokespecial <init> : ()V
      //   34: putfield updated : Ljava/util/List;
      //   37: new java/util/HashMap
      //   40: dup
      //   41: invokespecial <init> : ()V
      //   44: astore #4
      //   46: aload_1
      //   47: ifnull -> 93
      //   50: aload_1
      //   51: invokeinterface iterator : ()Ljava/util/Iterator;
      //   56: astore_1
      //   57: aload_1
      //   58: invokeinterface hasNext : ()Z
      //   63: ifeq -> 93
      //   66: aload_1
      //   67: invokeinterface next : ()Ljava/lang/Object;
      //   72: astore #5
      //   74: aload #4
      //   76: aload_3
      //   77: aload #5
      //   79: invokeinterface apply : (Ljava/lang/Object;)Ljava/lang/Object;
      //   84: aload #5
      //   86: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   89: pop
      //   90: goto -> 57
      //   93: aload_2
      //   94: ifnull -> 178
      //   97: aload_2
      //   98: invokeinterface iterator : ()Ljava/util/Iterator;
      //   103: astore_1
      //   104: aload_1
      //   105: invokeinterface hasNext : ()Z
      //   110: ifeq -> 178
      //   113: aload_1
      //   114: invokeinterface next : ()Ljava/lang/Object;
      //   119: astore #5
      //   121: aload #4
      //   123: aload_3
      //   124: aload #5
      //   126: invokeinterface apply : (Ljava/lang/Object;)Ljava/lang/Object;
      //   131: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
      //   134: astore_2
      //   135: aload_2
      //   136: ifnull -> 163
      //   139: aload_2
      //   140: aload #5
      //   142: invokevirtual equals : (Ljava/lang/Object;)Z
      //   145: ifne -> 175
      //   148: aload_0
      //   149: getfield updated : Ljava/util/List;
      //   152: aload #5
      //   154: invokeinterface add : (Ljava/lang/Object;)Z
      //   159: pop
      //   160: goto -> 175
      //   163: aload_0
      //   164: getfield added : Ljava/util/List;
      //   167: aload #5
      //   169: invokeinterface add : (Ljava/lang/Object;)Z
      //   174: pop
      //   175: goto -> 104
      //   178: aload_0
      //   179: getfield removed : Ljava/util/List;
      //   182: aload #4
      //   184: invokevirtual values : ()Ljava/util/Collection;
      //   187: invokeinterface addAll : (Ljava/util/Collection;)Z
      //   192: pop
      //   193: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #92	-> 0
      //   #81	-> 4
      //   #82	-> 15
      //   #83	-> 26
      //   #93	-> 37
      //   #95	-> 46
      //   #96	-> 50
      //   #97	-> 74
      //   #98	-> 90
      //   #101	-> 93
      //   #102	-> 97
      //   #103	-> 121
      //   #104	-> 135
      //   #105	-> 139
      //   #107	-> 148
      //   #111	-> 163
      //   #113	-> 175
      //   #116	-> 178
      //   #117	-> 193
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removed=[");
      stringBuilder.append(TextUtils.join(",", this.removed));
      stringBuilder.append("] added=[");
      List<T> list = this.added;
      stringBuilder.append(TextUtils.join(",", list));
      stringBuilder.append("] updated=[");
      list = this.updated;
      stringBuilder.append(TextUtils.join(",", list));
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
  }
  
  public static CompareResult<LinkAddress> compareAddresses(LinkProperties paramLinkProperties1, LinkProperties paramLinkProperties2) {
    List<LinkAddress> list = null;
    if (paramLinkProperties1 != null) {
      List<LinkAddress> list1 = paramLinkProperties1.getLinkAddresses();
    } else {
      paramLinkProperties1 = null;
    } 
    if (paramLinkProperties2 != null)
      list = paramLinkProperties2.getLinkAddresses(); 
    return new CompareResult<>((Collection<LinkAddress>)paramLinkProperties1, list);
  }
  
  public static boolean isIdenticalAddresses(LinkProperties paramLinkProperties1, LinkProperties paramLinkProperties2) {
    boolean bool;
    List<InetAddress> list1 = paramLinkProperties1.getAddresses();
    List<InetAddress> list2 = paramLinkProperties2.getAddresses();
    if (list1.size() == list2.size()) {
      bool = list1.containsAll(list2);
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isIdenticalDnses(LinkProperties paramLinkProperties1, LinkProperties paramLinkProperties2) {
    List<InetAddress> list1 = paramLinkProperties1.getDnsServers();
    List<InetAddress> list2 = paramLinkProperties2.getDnsServers();
    String str1 = paramLinkProperties1.getDomains();
    String str2 = paramLinkProperties2.getDomains();
    boolean bool = false;
    if (str1 == null) {
      if (str2 != null)
        return false; 
    } else if (!str1.equals(str2)) {
      return false;
    } 
    if (list1.size() == list2.size())
      bool = list1.containsAll(list2); 
    return bool;
  }
  
  public static boolean isIdenticalHttpProxy(LinkProperties paramLinkProperties1, LinkProperties paramLinkProperties2) {
    return Objects.equals(paramLinkProperties1.getHttpProxy(), paramLinkProperties2.getHttpProxy());
  }
  
  public static boolean isIdenticalInterfaceName(LinkProperties paramLinkProperties1, LinkProperties paramLinkProperties2) {
    return TextUtils.equals(paramLinkProperties1.getInterfaceName(), paramLinkProperties2.getInterfaceName());
  }
  
  public static boolean isIdenticalRoutes(LinkProperties paramLinkProperties1, LinkProperties paramLinkProperties2) {
    boolean bool;
    List<RouteInfo> list1 = paramLinkProperties1.getRoutes();
    List<RouteInfo> list2 = paramLinkProperties2.getRoutes();
    if (list1.size() == list2.size()) {
      bool = list1.containsAll(list2);
    } else {
      bool = false;
    } 
    return bool;
  }
}
