package android.net.util;

import android.content.Context;
import android.content.res.Resources;
import android.net.NetworkCapabilities;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;

public final class KeepaliveUtils {
  public static final String TAG = "KeepaliveUtils";
  
  class KeepaliveDeviceConfigurationException extends AndroidRuntimeException {
    public KeepaliveDeviceConfigurationException(KeepaliveUtils this$0) {
      super((String)this$0);
    }
  }
  
  public static int[] getSupportedKeepalives(Context paramContext) {
    Resources.NotFoundException notFoundException2 = null;
    try {
      String[] arrayOfString = paramContext.getResources().getStringArray(17236056);
    } catch (android.content.res.Resources.NotFoundException notFoundException1) {
      notFoundException1 = notFoundException2;
    } 
    if (notFoundException1 != null) {
      int[] arrayOfInt = new int[8];
      int i;
      byte b;
      for (i = notFoundException1.length, b = 0; b < i; ) {
        Resources.NotFoundException notFoundException = notFoundException1[b];
        if (!TextUtils.isEmpty((CharSequence)notFoundException)) {
          String[] arrayOfString = notFoundException.split(",");
          if (arrayOfString.length == 2)
            try {
              int j = Integer.parseInt(arrayOfString[0]);
              int k = Integer.parseInt(arrayOfString[1]);
              if (NetworkCapabilities.isValidTransport(j)) {
                if (k >= 0) {
                  arrayOfInt[j] = k;
                  b++;
                } 
                StringBuilder stringBuilder1 = new StringBuilder();
                stringBuilder1.append("Invalid supported count ");
                stringBuilder1.append(k);
                stringBuilder1.append(" for ");
                stringBuilder1.append(NetworkCapabilities.transportNameOf(j));
                throw new KeepaliveDeviceConfigurationException(stringBuilder1.toString());
              } 
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Invalid transport ");
              stringBuilder.append(j);
              throw new KeepaliveDeviceConfigurationException(stringBuilder.toString());
            } catch (NumberFormatException numberFormatException) {
              throw new KeepaliveDeviceConfigurationException("Invalid number format");
            }  
          throw new KeepaliveDeviceConfigurationException("Invalid parameter length");
        } 
        throw new KeepaliveDeviceConfigurationException("Empty string");
      } 
      return arrayOfInt;
    } 
    throw new KeepaliveDeviceConfigurationException("invalid resource");
  }
  
  public static int getSupportedKeepalivesForNetworkCapabilities(int[] paramArrayOfint, NetworkCapabilities paramNetworkCapabilities) {
    int[] arrayOfInt = paramNetworkCapabilities.getTransportTypes();
    int i = arrayOfInt.length;
    byte b = 0;
    if (i == 0)
      return 0; 
    i = paramArrayOfint[arrayOfInt[0]];
    for (int j = arrayOfInt.length; b < j; ) {
      int k = arrayOfInt[b];
      int m = i;
      if (i > paramArrayOfint[k])
        m = paramArrayOfint[k]; 
      b++;
      i = m;
    } 
    return i;
  }
}
