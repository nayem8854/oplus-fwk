package android.net.util;

import android.net.IpPrefix;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public final class IpRange {
  private static final int SIGNUM_POSITIVE = 1;
  
  private final byte[] mEndAddr;
  
  private final byte[] mStartAddr;
  
  public IpRange(InetAddress paramInetAddress1, InetAddress paramInetAddress2) {
    Objects.requireNonNull(paramInetAddress1, "startAddr must not be null");
    Objects.requireNonNull(paramInetAddress2, "endAddr must not be null");
    if (paramInetAddress1.getClass().equals(paramInetAddress2.getClass())) {
      BigInteger bigInteger1 = addrToBigInteger(paramInetAddress1.getAddress());
      BigInteger bigInteger2 = addrToBigInteger(paramInetAddress2.getAddress());
      if (bigInteger1.compareTo(bigInteger2) < 0) {
        this.mStartAddr = paramInetAddress1.getAddress();
        this.mEndAddr = paramInetAddress2.getAddress();
        return;
      } 
      throw new IllegalArgumentException("Invalid range; start address must be before end address");
    } 
    throw new IllegalArgumentException("Invalid range: Address family mismatch");
  }
  
  public IpRange(IpPrefix paramIpPrefix) {
    Objects.requireNonNull(paramIpPrefix, "prefix must not be null");
    this.mStartAddr = paramIpPrefix.getRawAddress();
    this.mEndAddr = paramIpPrefix.getRawAddress();
    int i = paramIpPrefix.getPrefixLength();
    while (true) {
      byte[] arrayOfByte = this.mEndAddr;
      if (i < arrayOfByte.length * 8) {
        int j = i / 8;
        arrayOfByte[j] = (byte)(arrayOfByte[j] | (byte)(128 >> i % 8));
        i++;
        continue;
      } 
      break;
    } 
  }
  
  private static InetAddress getAsInetAddress(byte[] paramArrayOfbyte) {
    try {
      return InetAddress.getByAddress(paramArrayOfbyte);
    } catch (UnknownHostException unknownHostException) {
      throw new IllegalArgumentException("Address is invalid");
    } 
  }
  
  public InetAddress getStartAddr() {
    return getAsInetAddress(this.mStartAddr);
  }
  
  public InetAddress getEndAddr() {
    return getAsInetAddress(this.mEndAddr);
  }
  
  public List<IpPrefix> asIpPrefixes() {
    boolean bool;
    InetAddress inetAddress;
    if (this.mStartAddr.length == 16) {
      bool = true;
    } else {
      bool = false;
    } 
    ArrayList<IpPrefix> arrayList = new ArrayList();
    LinkedList<IpPrefix> linkedList = new LinkedList();
    if (bool) {
      inetAddress = getAsInetAddress(new byte[16]);
    } else {
      inetAddress = getAsInetAddress(new byte[4]);
    } 
    IpPrefix ipPrefix = new IpPrefix(inetAddress, 0);
    linkedList.add(ipPrefix);
    while (!linkedList.isEmpty()) {
      ipPrefix = linkedList.poll();
      IpRange ipRange = new IpRange(ipPrefix);
      if (containsRange(ipRange)) {
        arrayList.add(ipPrefix);
        continue;
      } 
      if (overlapsRange(ipRange))
        linkedList.addAll(getSubsetPrefixes(ipPrefix)); 
    } 
    return arrayList;
  }
  
  private static List<IpPrefix> getSubsetPrefixes(IpPrefix paramIpPrefix) {
    ArrayList<IpPrefix> arrayList = new ArrayList();
    int i = paramIpPrefix.getPrefixLength();
    arrayList.add(new IpPrefix(paramIpPrefix.getAddress(), i + 1));
    byte[] arrayOfByte = paramIpPrefix.getRawAddress();
    arrayOfByte[i / 8] = (byte)(arrayOfByte[i / 8] ^ 128 >> i % 8);
    arrayList.add(new IpPrefix(getAsInetAddress(arrayOfByte), i + 1));
    return arrayList;
  }
  
  public boolean containsRange(IpRange paramIpRange) {
    if (addrToBigInteger(this.mStartAddr).compareTo(addrToBigInteger(paramIpRange.mStartAddr)) <= 0) {
      byte[] arrayOfByte = this.mEndAddr;
      if (addrToBigInteger(arrayOfByte).compareTo(addrToBigInteger(paramIpRange.mEndAddr)) >= 0)
        return true; 
    } 
    return false;
  }
  
  public boolean overlapsRange(IpRange paramIpRange) {
    if (addrToBigInteger(this.mStartAddr).compareTo(addrToBigInteger(paramIpRange.mEndAddr)) <= 0) {
      byte[] arrayOfByte = paramIpRange.mStartAddr;
      if (addrToBigInteger(arrayOfByte).compareTo(addrToBigInteger(this.mEndAddr)) <= 0)
        return true; 
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mStartAddr, this.mEndAddr });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof IpRange;
    boolean bool1 = false;
    if (!bool)
      return false; 
    IpRange ipRange = (IpRange)paramObject;
    if (Arrays.equals(this.mStartAddr, ipRange.mStartAddr)) {
      paramObject = this.mEndAddr;
      byte[] arrayOfByte = ipRange.mEndAddr;
      if (Arrays.equals((byte[])paramObject, arrayOfByte))
        bool1 = true; 
    } 
    return bool1;
  }
  
  private static BigInteger addrToBigInteger(byte[] paramArrayOfbyte) {
    return new BigInteger(1, paramArrayOfbyte);
  }
}
