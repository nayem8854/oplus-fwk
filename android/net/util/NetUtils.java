package android.net.util;

import android.net.RouteInfo;
import java.net.InetAddress;
import java.util.Collection;

public final class NetUtils {
  public static boolean addressTypeMatches(InetAddress paramInetAddress1, InetAddress paramInetAddress2) {
    boolean bool;
    if ((paramInetAddress1 instanceof java.net.Inet4Address && paramInetAddress2 instanceof java.net.Inet4Address) || (paramInetAddress1 instanceof java.net.Inet6Address && paramInetAddress2 instanceof java.net.Inet6Address)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static RouteInfo selectBestRoute(Collection<RouteInfo> paramCollection, InetAddress paramInetAddress) {
    RouteInfo routeInfo;
    if (paramCollection == null || paramInetAddress == null)
      return null; 
    Collection<RouteInfo> collection = null;
    for (RouteInfo routeInfo2 : paramCollection) {
      RouteInfo routeInfo1;
      paramCollection = collection;
      if (addressTypeMatches(routeInfo2.getDestination().getAddress(), paramInetAddress)) {
        if (collection != null) {
          int i = collection.getDestination().getPrefixLength();
          if (i >= routeInfo2.getDestination().getPrefixLength())
            continue; 
        } 
        paramCollection = collection;
        if (routeInfo2.matches(paramInetAddress))
          routeInfo1 = routeInfo2; 
      } 
      routeInfo = routeInfo1;
    } 
    return routeInfo;
  }
}
