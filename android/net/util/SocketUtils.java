package android.net.util;

import android.annotation.SystemApi;
import android.net.NetworkUtils;
import android.system.ErrnoException;
import android.system.NetlinkSocketAddress;
import android.system.Os;
import android.system.OsConstants;
import android.system.PacketSocketAddress;
import java.io.FileDescriptor;
import java.io.IOException;
import java.net.SocketAddress;
import libcore.io.IoBridge;

@SystemApi
public final class SocketUtils {
  public static void bindSocketToInterface(FileDescriptor paramFileDescriptor, String paramString) throws ErrnoException {
    Os.setsockoptIfreq(paramFileDescriptor, OsConstants.SOL_SOCKET, OsConstants.SO_BINDTODEVICE, paramString);
    NetworkUtils.protectFromVpn(paramFileDescriptor);
  }
  
  public static SocketAddress makeNetlinkSocketAddress(int paramInt1, int paramInt2) {
    return (SocketAddress)new NetlinkSocketAddress(paramInt1, paramInt2);
  }
  
  public static SocketAddress makePacketSocketAddress(int paramInt1, int paramInt2) {
    return (SocketAddress)new PacketSocketAddress(paramInt1, paramInt2, null);
  }
  
  @Deprecated
  public static SocketAddress makePacketSocketAddress(int paramInt, byte[] paramArrayOfbyte) {
    return (SocketAddress)new PacketSocketAddress(0, paramInt, paramArrayOfbyte);
  }
  
  public static SocketAddress makePacketSocketAddress(int paramInt1, int paramInt2, byte[] paramArrayOfbyte) {
    return (SocketAddress)new PacketSocketAddress(paramInt1, paramInt2, paramArrayOfbyte);
  }
  
  public static void closeSocket(FileDescriptor paramFileDescriptor) throws IOException {
    IoBridge.closeAndSignalBlockedThreads(paramFileDescriptor);
  }
}
