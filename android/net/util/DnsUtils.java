package android.net.util;

import android.net.InetAddresses;
import android.net.Network;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import com.android.internal.util.BitUtils;
import java.io.FileDescriptor;
import java.io.IOException;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import libcore.io.IoUtils;

public class DnsUtils {
  private static final int CHAR_BIT = 8;
  
  public static final int IPV6_ADDR_SCOPE_GLOBAL = 14;
  
  public static final int IPV6_ADDR_SCOPE_LINKLOCAL = 2;
  
  public static final int IPV6_ADDR_SCOPE_NODELOCAL = 1;
  
  public static final int IPV6_ADDR_SCOPE_SITELOCAL = 5;
  
  private static final String TAG = "DnsUtils";
  
  private static final Comparator<SortableAddress> sRfc6724Comparator = new Rfc6724Comparator();
  
  public static class Rfc6724Comparator implements Comparator<SortableAddress> {
    public int compare(DnsUtils.SortableAddress param1SortableAddress1, DnsUtils.SortableAddress param1SortableAddress2) {
      if (param1SortableAddress1.hasSrcAddr != param1SortableAddress2.hasSrcAddr)
        return param1SortableAddress2.hasSrcAddr - param1SortableAddress1.hasSrcAddr; 
      if (param1SortableAddress1.scopeMatch != param1SortableAddress2.scopeMatch)
        return param1SortableAddress2.scopeMatch - param1SortableAddress1.scopeMatch; 
      if (param1SortableAddress1.labelMatch != param1SortableAddress2.labelMatch)
        return param1SortableAddress2.labelMatch - param1SortableAddress1.labelMatch; 
      if (param1SortableAddress1.precedence != param1SortableAddress2.precedence)
        return param1SortableAddress2.precedence - param1SortableAddress1.precedence; 
      if (param1SortableAddress1.scope != param1SortableAddress2.scope)
        return param1SortableAddress1.scope - param1SortableAddress2.scope; 
      if (param1SortableAddress1.prefixMatchLen != param1SortableAddress2.prefixMatchLen)
        return param1SortableAddress2.prefixMatchLen - param1SortableAddress1.prefixMatchLen; 
      return 0;
    }
  }
  
  public static class SortableAddress {
    public final InetAddress address;
    
    public final int hasSrcAddr;
    
    public final int label;
    
    public final int labelMatch;
    
    public final int precedence;
    
    public final int prefixMatchLen;
    
    public final int scope;
    
    public final int scopeMatch;
    
    public SortableAddress(InetAddress param1InetAddress1, InetAddress param1InetAddress2) {
      boolean bool2;
      this.address = param1InetAddress1;
      boolean bool1 = true;
      if (param1InetAddress2 != null) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.hasSrcAddr = bool2;
      this.label = DnsUtils.findLabel(param1InetAddress1);
      this.scope = DnsUtils.findScope(param1InetAddress1);
      this.precedence = DnsUtils.findPrecedence(param1InetAddress1);
      if (param1InetAddress2 != null && this.label == DnsUtils.findLabel(param1InetAddress2)) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.labelMatch = bool2;
      if (param1InetAddress2 != null && this.scope == DnsUtils.findScope(param1InetAddress2)) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      this.scopeMatch = bool2;
      if (DnsUtils.isIpv6Address(param1InetAddress1) && DnsUtils.isIpv6Address(param1InetAddress2)) {
        this.prefixMatchLen = DnsUtils.compareIpv6PrefixMatchLen(param1InetAddress2, param1InetAddress1);
      } else {
        this.prefixMatchLen = 0;
      } 
    }
  }
  
  public static List<InetAddress> rfc6724Sort(Network paramNetwork, List<InetAddress> paramList) {
    ArrayList<SortableAddress> arrayList1 = new ArrayList();
    for (InetAddress inetAddress : paramList)
      arrayList1.add(new SortableAddress(inetAddress, findSrcAddress(paramNetwork, inetAddress))); 
    Collections.sort(arrayList1, sRfc6724Comparator);
    ArrayList<InetAddress> arrayList = new ArrayList();
    for (SortableAddress sortableAddress : arrayList1)
      arrayList.add(sortableAddress.address); 
    return arrayList;
  }
  
  private static InetAddress findSrcAddress(Network paramNetwork, InetAddress paramInetAddress) {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic isIpv4Address : (Ljava/net/InetAddress;)Z
    //   4: ifeq -> 14
    //   7: getstatic android/system/OsConstants.AF_INET : I
    //   10: istore_2
    //   11: goto -> 25
    //   14: aload_1
    //   15: invokestatic isIpv6Address : (Ljava/net/InetAddress;)Z
    //   18: ifeq -> 128
    //   21: getstatic android/system/OsConstants.AF_INET6 : I
    //   24: istore_2
    //   25: iload_2
    //   26: getstatic android/system/OsConstants.SOCK_DGRAM : I
    //   29: getstatic android/system/OsConstants.IPPROTO_UDP : I
    //   32: invokestatic socket : (III)Ljava/io/FileDescriptor;
    //   35: astore_3
    //   36: aload_0
    //   37: ifnull -> 45
    //   40: aload_0
    //   41: aload_3
    //   42: invokevirtual bindSocket : (Ljava/io/FileDescriptor;)V
    //   45: new java/net/InetSocketAddress
    //   48: astore_0
    //   49: aload_0
    //   50: aload_1
    //   51: iconst_0
    //   52: invokespecial <init> : (Ljava/net/InetAddress;I)V
    //   55: aload_3
    //   56: aload_0
    //   57: invokestatic connect : (Ljava/io/FileDescriptor;Ljava/net/SocketAddress;)V
    //   60: aload_3
    //   61: invokestatic getsockname : (Ljava/io/FileDescriptor;)Ljava/net/SocketAddress;
    //   64: checkcast java/net/InetSocketAddress
    //   67: invokevirtual getAddress : ()Ljava/net/InetAddress;
    //   70: astore_0
    //   71: aload_3
    //   72: invokestatic closeQuietly : (Ljava/io/FileDescriptor;)V
    //   75: aload_0
    //   76: areturn
    //   77: astore_0
    //   78: aload_3
    //   79: invokestatic closeQuietly : (Ljava/io/FileDescriptor;)V
    //   82: aload_0
    //   83: athrow
    //   84: astore_0
    //   85: aload_3
    //   86: invokestatic closeQuietly : (Ljava/io/FileDescriptor;)V
    //   89: aconst_null
    //   90: areturn
    //   91: astore_1
    //   92: new java/lang/StringBuilder
    //   95: dup
    //   96: invokespecial <init> : ()V
    //   99: astore_0
    //   100: aload_0
    //   101: ldc 'findSrcAddress:'
    //   103: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   106: pop
    //   107: aload_0
    //   108: aload_1
    //   109: invokevirtual toString : ()Ljava/lang/String;
    //   112: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   115: pop
    //   116: ldc 'DnsUtils'
    //   118: aload_0
    //   119: invokevirtual toString : ()Ljava/lang/String;
    //   122: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   125: pop
    //   126: aconst_null
    //   127: areturn
    //   128: aconst_null
    //   129: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #162	-> 0
    //   #163	-> 7
    //   #164	-> 14
    //   #165	-> 21
    //   #171	-> 25
    //   #175	-> 36
    //   #177	-> 36
    //   #178	-> 45
    //   #179	-> 60
    //   #183	-> 71
    //   #179	-> 75
    //   #183	-> 77
    //   #184	-> 82
    //   #180	-> 84
    //   #181	-> 85
    //   #183	-> 85
    //   #181	-> 89
    //   #172	-> 91
    //   #173	-> 92
    //   #174	-> 126
    //   #167	-> 128
    // Exception table:
    //   from	to	target	type
    //   25	36	91	android/system/ErrnoException
    //   40	45	84	java/io/IOException
    //   40	45	84	android/system/ErrnoException
    //   40	45	77	finally
    //   45	60	84	java/io/IOException
    //   45	60	84	android/system/ErrnoException
    //   45	60	77	finally
    //   60	71	84	java/io/IOException
    //   60	71	84	android/system/ErrnoException
    //   60	71	77	finally
  }
  
  private static int findLabel(InetAddress paramInetAddress) {
    if (isIpv4Address(paramInetAddress))
      return 4; 
    if (isIpv6Address(paramInetAddress)) {
      if (paramInetAddress.isLoopbackAddress())
        return 0; 
      if (isIpv6Address6To4(paramInetAddress))
        return 2; 
      if (isIpv6AddressTeredo(paramInetAddress))
        return 5; 
      if (isIpv6AddressULA(paramInetAddress))
        return 13; 
      if (((Inet6Address)paramInetAddress).isIPv4CompatibleAddress())
        return 3; 
      if (paramInetAddress.isSiteLocalAddress())
        return 11; 
      if (isIpv6Address6Bone(paramInetAddress))
        return 12; 
      return 1;
    } 
    return 1;
  }
  
  private static boolean isIpv6Address(InetAddress paramInetAddress) {
    return paramInetAddress instanceof Inet6Address;
  }
  
  private static boolean isIpv4Address(InetAddress paramInetAddress) {
    return paramInetAddress instanceof java.net.Inet4Address;
  }
  
  private static boolean isIpv6Address6To4(InetAddress paramInetAddress) {
    boolean bool = isIpv6Address(paramInetAddress);
    boolean bool1 = false;
    if (!bool)
      return false; 
    byte[] arrayOfByte = paramInetAddress.getAddress();
    bool = bool1;
    if (arrayOfByte[0] == 32) {
      bool = bool1;
      if (arrayOfByte[1] == 2)
        bool = true; 
    } 
    return bool;
  }
  
  private static boolean isIpv6AddressTeredo(InetAddress paramInetAddress) {
    boolean bool = isIpv6Address(paramInetAddress);
    boolean bool1 = false;
    if (!bool)
      return false; 
    byte[] arrayOfByte = paramInetAddress.getAddress();
    bool = bool1;
    if (arrayOfByte[0] == 32) {
      bool = bool1;
      if (arrayOfByte[1] == 1) {
        bool = bool1;
        if (arrayOfByte[2] == 0) {
          bool = bool1;
          if (arrayOfByte[3] == 0)
            bool = true; 
        } 
      } 
    } 
    return bool;
  }
  
  private static boolean isIpv6AddressULA(InetAddress paramInetAddress) {
    boolean bool = isIpv6Address(paramInetAddress);
    boolean bool1 = false, bool2 = bool1;
    if (bool) {
      bool2 = bool1;
      if ((paramInetAddress.getAddress()[0] & 0xFE) == 252)
        bool2 = true; 
    } 
    return bool2;
  }
  
  private static boolean isIpv6Address6Bone(InetAddress paramInetAddress) {
    boolean bool = isIpv6Address(paramInetAddress);
    boolean bool1 = false;
    if (!bool)
      return false; 
    byte[] arrayOfByte = paramInetAddress.getAddress();
    bool = bool1;
    if (arrayOfByte[0] == 63) {
      bool = bool1;
      if (arrayOfByte[1] == -2)
        bool = true; 
    } 
    return bool;
  }
  
  private static int getIpv6MulticastScope(InetAddress paramInetAddress) {
    int i;
    if (!isIpv6Address(paramInetAddress)) {
      i = 0;
    } else {
      i = paramInetAddress.getAddress()[1] & 0xF;
    } 
    return i;
  }
  
  private static int findScope(InetAddress paramInetAddress) {
    if (isIpv6Address(paramInetAddress)) {
      if (paramInetAddress.isMulticastAddress())
        return getIpv6MulticastScope(paramInetAddress); 
      if (paramInetAddress.isLoopbackAddress() || paramInetAddress.isLinkLocalAddress())
        return 2; 
      if (paramInetAddress.isSiteLocalAddress())
        return 5; 
      return 14;
    } 
    if (isIpv4Address(paramInetAddress)) {
      if (paramInetAddress.isLoopbackAddress() || paramInetAddress.isLinkLocalAddress())
        return 2; 
      return 14;
    } 
    return 1;
  }
  
  private static int findPrecedence(InetAddress paramInetAddress) {
    if (isIpv4Address(paramInetAddress))
      return 35; 
    if (isIpv6Address(paramInetAddress)) {
      if (paramInetAddress.isLoopbackAddress())
        return 50; 
      if (isIpv6Address6To4(paramInetAddress))
        return 30; 
      if (isIpv6AddressTeredo(paramInetAddress))
        return 5; 
      if (isIpv6AddressULA(paramInetAddress))
        return 3; 
      if (((Inet6Address)paramInetAddress).isIPv4CompatibleAddress() || paramInetAddress.isSiteLocalAddress() || 
        isIpv6Address6Bone(paramInetAddress))
        return 1; 
      return 40;
    } 
    return 1;
  }
  
  private static int compareIpv6PrefixMatchLen(InetAddress paramInetAddress1, InetAddress paramInetAddress2) {
    byte[] arrayOfByte1 = paramInetAddress1.getAddress();
    byte[] arrayOfByte2 = paramInetAddress2.getAddress();
    if (arrayOfByte1.length != arrayOfByte2.length)
      return 0; 
    for (byte b = 0; b < arrayOfByte2.length; ) {
      if (arrayOfByte1[b] == arrayOfByte2[b]) {
        b++;
        continue;
      } 
      int i = BitUtils.uint8(arrayOfByte1[b]), j = BitUtils.uint8(arrayOfByte2[b]);
      return b * 8 + Integer.numberOfLeadingZeros(i ^ j) - 24;
    } 
    return arrayOfByte2.length * 8;
  }
  
  public static boolean haveIpv4(Network paramNetwork) {
    InetSocketAddress inetSocketAddress = new InetSocketAddress(InetAddresses.parseNumericAddress("8.8.8.8"), 0);
    return checkConnectivity(paramNetwork, OsConstants.AF_INET, inetSocketAddress);
  }
  
  public static boolean haveIpv6(Network paramNetwork) {
    InetSocketAddress inetSocketAddress = new InetSocketAddress(InetAddresses.parseNumericAddress("2000::"), 0);
    return checkConnectivity(paramNetwork, OsConstants.AF_INET6, inetSocketAddress);
  }
  
  private static boolean checkConnectivity(Network paramNetwork, int paramInt, SocketAddress paramSocketAddress) {
    try {
      FileDescriptor fileDescriptor = Os.socket(paramInt, OsConstants.SOCK_DGRAM, OsConstants.IPPROTO_UDP);
      if (paramNetwork != null)
        try {
          paramNetwork.bindSocket(fileDescriptor);
          Os.connect(fileDescriptor, paramSocketAddress);
          return true;
        } catch (IOException|ErrnoException iOException) {
          return false;
        } finally {
          IoUtils.closeQuietly(fileDescriptor);
        }  
      Os.connect(fileDescriptor, paramSocketAddress);
      IoUtils.closeQuietly(fileDescriptor);
      return true;
    } catch (ErrnoException errnoException) {
      return false;
    } 
  }
}
