package android.net;

import android.net.wifi.WifiInfo;
import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.BackupUtils;
import android.util.Log;
import com.android.internal.util.ArrayUtils;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

public class NetworkTemplate implements Parcelable {
  private static final int BACKUP_VERSION = 1;
  
  public static final Parcelable.Creator<NetworkTemplate> CREATOR;
  
  public static final int MATCH_BLUETOOTH = 8;
  
  public static final int MATCH_ETHERNET = 5;
  
  public static final int MATCH_MOBILE = 1;
  
  public static final int MATCH_MOBILE_WILDCARD = 6;
  
  public static final int MATCH_PROXY = 9;
  
  public static final int MATCH_WIFI = 4;
  
  public static final int MATCH_WIFI_WILDCARD = 7;
  
  public static final int NETWORK_TYPE_5G_NSA = -2;
  
  public static final int NETWORK_TYPE_ALL = -1;
  
  private static final String TAG = "NetworkTemplate";
  
  private static boolean isKnownMatchRule(int paramInt) {
    if (paramInt != 1)
      switch (paramInt) {
        default:
          return false;
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
          break;
      }  
    return true;
  }
  
  private static boolean sForceAllNetworkTypes = false;
  
  private final int mDefaultNetwork;
  
  private final int mMatchRule;
  
  private final String[] mMatchSubscriberIds;
  
  private final int mMetered;
  
  private final String mNetworkId;
  
  private final int mRoaming;
  
  private final int mSubType;
  
  private final String mSubscriberId;
  
  public static void forceAllNetworkTypes() {
    sForceAllNetworkTypes = true;
  }
  
  public static void resetForceAllNetworkTypes() {
    sForceAllNetworkTypes = false;
  }
  
  public static NetworkTemplate buildTemplateMobileAll(String paramString) {
    return new NetworkTemplate(1, paramString, null);
  }
  
  public static NetworkTemplate buildTemplateMobileWithRatType(String paramString, int paramInt) {
    if (TextUtils.isEmpty(paramString))
      return new NetworkTemplate(6, null, null, null, -1, -1, -1, paramInt); 
    return new NetworkTemplate(1, paramString, new String[] { paramString }, null, -1, -1, -1, paramInt);
  }
  
  public static NetworkTemplate buildTemplateMobileWildcard() {
    return new NetworkTemplate(6, null, null);
  }
  
  public static NetworkTemplate buildTemplateWifiWildcard() {
    return new NetworkTemplate(7, null, null);
  }
  
  @Deprecated
  public static NetworkTemplate buildTemplateWifi() {
    return buildTemplateWifiWildcard();
  }
  
  public static NetworkTemplate buildTemplateWifi(String paramString) {
    return new NetworkTemplate(4, null, paramString);
  }
  
  public static NetworkTemplate buildTemplateEthernet() {
    return new NetworkTemplate(5, null, null);
  }
  
  public static NetworkTemplate buildTemplateBluetooth() {
    return new NetworkTemplate(8, null, null);
  }
  
  public static NetworkTemplate buildTemplateProxy() {
    return new NetworkTemplate(9, null, null);
  }
  
  public NetworkTemplate(int paramInt, String paramString1, String paramString2) {
    this(paramInt, paramString1, new String[] { paramString1 }, paramString2);
  }
  
  public NetworkTemplate(int paramInt, String paramString1, String[] paramArrayOfString, String paramString2) {
    this(paramInt, paramString1, paramArrayOfString, paramString2, -1, -1, -1, -1);
  }
  
  public NetworkTemplate(int paramInt1, String paramString1, String[] paramArrayOfString, String paramString2, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this.mMatchRule = paramInt1;
    this.mSubscriberId = paramString1;
    this.mMatchSubscriberIds = paramArrayOfString;
    this.mNetworkId = paramString2;
    this.mMetered = paramInt2;
    this.mRoaming = paramInt3;
    this.mDefaultNetwork = paramInt4;
    this.mSubType = paramInt5;
    if (!isKnownMatchRule(paramInt1)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown network template rule ");
      stringBuilder.append(paramInt1);
      stringBuilder.append(" will not match any identity.");
      Log.e("NetworkTemplate", stringBuilder.toString());
    } 
  }
  
  private NetworkTemplate(Parcel paramParcel) {
    this.mMatchRule = paramParcel.readInt();
    this.mSubscriberId = paramParcel.readString();
    this.mMatchSubscriberIds = paramParcel.createStringArray();
    this.mNetworkId = paramParcel.readString();
    this.mMetered = paramParcel.readInt();
    this.mRoaming = paramParcel.readInt();
    this.mDefaultNetwork = paramParcel.readInt();
    this.mSubType = paramParcel.readInt();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mMatchRule);
    paramParcel.writeString(this.mSubscriberId);
    paramParcel.writeStringArray(this.mMatchSubscriberIds);
    paramParcel.writeString(this.mNetworkId);
    paramParcel.writeInt(this.mMetered);
    paramParcel.writeInt(this.mRoaming);
    paramParcel.writeInt(this.mDefaultNetwork);
    paramParcel.writeInt(this.mSubType);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("NetworkTemplate: ");
    stringBuilder.append("matchRule=");
    stringBuilder.append(getMatchRuleName(this.mMatchRule));
    if (this.mSubscriberId != null) {
      stringBuilder.append(", subscriberId=");
      String str = this.mSubscriberId;
      str = NetworkIdentity.scrubSubscriberId(str);
      stringBuilder.append(str);
    } 
    if (this.mMatchSubscriberIds != null) {
      stringBuilder.append(", matchSubscriberIds=");
      String[] arrayOfString = this.mMatchSubscriberIds;
      String str = Arrays.toString((Object[])NetworkIdentity.scrubSubscriberId(arrayOfString));
      stringBuilder.append(str);
    } 
    if (this.mNetworkId != null) {
      stringBuilder.append(", networkId=");
      stringBuilder.append(this.mNetworkId);
    } 
    if (this.mMetered != -1) {
      stringBuilder.append(", metered=");
      stringBuilder.append(NetworkStats.meteredToString(this.mMetered));
    } 
    if (this.mRoaming != -1) {
      stringBuilder.append(", roaming=");
      stringBuilder.append(NetworkStats.roamingToString(this.mRoaming));
    } 
    if (this.mDefaultNetwork != -1) {
      stringBuilder.append(", defaultNetwork=");
      stringBuilder.append(NetworkStats.defaultNetworkToString(this.mDefaultNetwork));
    } 
    if (this.mSubType != -1) {
      stringBuilder.append(", subType=");
      stringBuilder.append(this.mSubType);
    } 
    return stringBuilder.toString();
  }
  
  public int hashCode() {
    int i = this.mMatchRule;
    String str1 = this.mSubscriberId, str2 = this.mNetworkId;
    int j = this.mMetered, k = this.mRoaming, m = this.mDefaultNetwork;
    int n = this.mSubType;
    return Objects.hash(new Object[] { Integer.valueOf(i), str1, str2, Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof NetworkTemplate;
    boolean bool1 = false;
    if (bool) {
      paramObject = paramObject;
      if (this.mMatchRule == ((NetworkTemplate)paramObject).mMatchRule) {
        String str1 = this.mSubscriberId, str2 = ((NetworkTemplate)paramObject).mSubscriberId;
        if (Objects.equals(str1, str2)) {
          str1 = this.mNetworkId;
          str2 = ((NetworkTemplate)paramObject).mNetworkId;
          if (Objects.equals(str1, str2) && this.mMetered == ((NetworkTemplate)paramObject).mMetered && this.mRoaming == ((NetworkTemplate)paramObject).mRoaming && this.mDefaultNetwork == ((NetworkTemplate)paramObject).mDefaultNetwork && this.mSubType == ((NetworkTemplate)paramObject).mSubType)
            bool1 = true; 
        } 
      } 
      return bool1;
    } 
    return false;
  }
  
  public boolean isMatchRuleMobile() {
    int i = this.mMatchRule;
    if (i != 1 && i != 6)
      return false; 
    return true;
  }
  
  public boolean isPersistable() {
    int i = this.mMatchRule;
    if (i != 6 && i != 7)
      return true; 
    return false;
  }
  
  public int getMatchRule() {
    return this.mMatchRule;
  }
  
  public String getSubscriberId() {
    return this.mSubscriberId;
  }
  
  public String getNetworkId() {
    return this.mNetworkId;
  }
  
  public boolean matches(NetworkIdentity paramNetworkIdentity) {
    if (!matchesMetered(paramNetworkIdentity))
      return false; 
    if (!matchesRoaming(paramNetworkIdentity))
      return false; 
    if (!matchesDefaultNetwork(paramNetworkIdentity))
      return false; 
    int i = this.mMatchRule;
    if (i != 1) {
      switch (i) {
        default:
          return false;
        case 9:
          return matchesProxy(paramNetworkIdentity);
        case 8:
          return matchesBluetooth(paramNetworkIdentity);
        case 7:
          return matchesWifiWildcard(paramNetworkIdentity);
        case 6:
          return matchesMobileWildcard(paramNetworkIdentity);
        case 5:
          return matchesEthernet(paramNetworkIdentity);
        case 4:
          break;
      } 
      return matchesWifi(paramNetworkIdentity);
    } 
    return matchesMobile(paramNetworkIdentity);
  }
  
  private boolean matchesMetered(NetworkIdentity paramNetworkIdentity) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mMetered : I
    //   4: istore_2
    //   5: iconst_1
    //   6: istore_3
    //   7: iload_3
    //   8: istore #4
    //   10: iload_2
    //   11: iconst_m1
    //   12: if_icmpeq -> 53
    //   15: iload_2
    //   16: iconst_1
    //   17: if_icmpne -> 30
    //   20: iload_3
    //   21: istore #4
    //   23: aload_1
    //   24: getfield mMetered : Z
    //   27: ifne -> 53
    //   30: aload_0
    //   31: getfield mMetered : I
    //   34: ifne -> 50
    //   37: aload_1
    //   38: getfield mMetered : Z
    //   41: ifne -> 50
    //   44: iload_3
    //   45: istore #4
    //   47: goto -> 53
    //   50: iconst_0
    //   51: istore #4
    //   53: iload #4
    //   55: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #409	-> 0
  }
  
  private boolean matchesRoaming(NetworkIdentity paramNetworkIdentity) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mRoaming : I
    //   4: istore_2
    //   5: iconst_1
    //   6: istore_3
    //   7: iload_3
    //   8: istore #4
    //   10: iload_2
    //   11: iconst_m1
    //   12: if_icmpeq -> 53
    //   15: iload_2
    //   16: iconst_1
    //   17: if_icmpne -> 30
    //   20: iload_3
    //   21: istore #4
    //   23: aload_1
    //   24: getfield mRoaming : Z
    //   27: ifne -> 53
    //   30: aload_0
    //   31: getfield mRoaming : I
    //   34: ifne -> 50
    //   37: aload_1
    //   38: getfield mRoaming : Z
    //   41: ifne -> 50
    //   44: iload_3
    //   45: istore #4
    //   47: goto -> 53
    //   50: iconst_0
    //   51: istore #4
    //   53: iload #4
    //   55: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #415	-> 0
  }
  
  private boolean matchesDefaultNetwork(NetworkIdentity paramNetworkIdentity) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mDefaultNetwork : I
    //   4: istore_2
    //   5: iconst_1
    //   6: istore_3
    //   7: iload_3
    //   8: istore #4
    //   10: iload_2
    //   11: iconst_m1
    //   12: if_icmpeq -> 53
    //   15: iload_2
    //   16: iconst_1
    //   17: if_icmpne -> 30
    //   20: iload_3
    //   21: istore #4
    //   23: aload_1
    //   24: getfield mDefaultNetwork : Z
    //   27: ifne -> 53
    //   30: aload_0
    //   31: getfield mDefaultNetwork : I
    //   34: ifne -> 50
    //   37: aload_1
    //   38: getfield mDefaultNetwork : Z
    //   41: ifne -> 50
    //   44: iload_3
    //   45: istore #4
    //   47: goto -> 53
    //   50: iconst_0
    //   51: istore #4
    //   53: iload #4
    //   55: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #421	-> 0
  }
  
  private boolean matchesCollapsedRatType(NetworkIdentity paramNetworkIdentity) {
    int i = this.mSubType;
    return (i == -1 || 
      getCollapsedRatType(i) == getCollapsedRatType(paramNetworkIdentity.mSubType));
  }
  
  public boolean matchesSubscriberId(String paramString) {
    return ArrayUtils.contains((Object[])this.mMatchSubscriberIds, paramString);
  }
  
  private boolean matchesMobile(NetworkIdentity paramNetworkIdentity) {
    int i = paramNetworkIdentity.mType;
    null = true;
    if (i == 6)
      return true; 
    if (sForceAllNetworkTypes || (paramNetworkIdentity.mType == 0 && paramNetworkIdentity.mMetered)) {
      String[] arrayOfString = this.mMatchSubscriberIds;
      if (!ArrayUtils.isEmpty((Object[])arrayOfString)) {
        String arrayOfString1[] = this.mMatchSubscriberIds, str = paramNetworkIdentity.mSubscriberId;
        if (ArrayUtils.contains((Object[])arrayOfString1, str) && 
          matchesCollapsedRatType(paramNetworkIdentity))
          return null; 
      } 
    } 
    return false;
  }
  
  public static int getCollapsedRatType(int paramInt) {
    if (paramInt != -2) {
      if (paramInt != 20) {
        switch (paramInt) {
          default:
            return 0;
          case 13:
          case 18:
            return 13;
          case 3:
          case 5:
          case 6:
          case 8:
          case 9:
          case 10:
          case 12:
          case 14:
          case 15:
          case 17:
            return 3;
          case 1:
          case 2:
          case 4:
          case 7:
          case 11:
          case 16:
            break;
        } 
        return 16;
      } 
      return 20;
    } 
    return -2;
  }
  
  public static final int[] getAllCollapsedRatTypes() {
    int[] arrayOfInt = TelephonyManager.getAllNetworkTypes();
    HashSet<Integer> hashSet = new HashSet();
    int i;
    byte b;
    for (i = arrayOfInt.length, b = 0; b < i; ) {
      int j = arrayOfInt[b];
      hashSet.add(Integer.valueOf(getCollapsedRatType(j)));
      b++;
    } 
    hashSet.add(Integer.valueOf(0));
    return toIntArray(hashSet);
  }
  
  private static int[] toIntArray(Collection<Integer> paramCollection) {
    int[] arrayOfInt = new int[paramCollection.size()];
    byte b = 0;
    for (Integer integer : paramCollection) {
      arrayOfInt[b] = integer.intValue();
      b++;
    } 
    return arrayOfInt;
  }
  
  private boolean matchesWifi(NetworkIdentity paramNetworkIdentity) {
    if (paramNetworkIdentity.mType != 1)
      return false; 
    String str2 = this.mNetworkId;
    str2 = WifiInfo.sanitizeSsid(str2);
    String str1 = WifiInfo.sanitizeSsid(paramNetworkIdentity.mNetworkId);
    return Objects.equals(str2, str1);
  }
  
  private boolean matchesEthernet(NetworkIdentity paramNetworkIdentity) {
    if (paramNetworkIdentity.mType == 9)
      return true; 
    return false;
  }
  
  private boolean matchesMobileWildcard(NetworkIdentity paramNetworkIdentity) {
    int i = paramNetworkIdentity.mType;
    boolean bool = true;
    if (i == 6)
      return true; 
    if ((!sForceAllNetworkTypes && (paramNetworkIdentity.mType != 0 || !paramNetworkIdentity.mMetered)) || 
      !matchesCollapsedRatType(paramNetworkIdentity))
      bool = false; 
    return bool;
  }
  
  private boolean matchesWifiWildcard(NetworkIdentity paramNetworkIdentity) {
    int i = paramNetworkIdentity.mType;
    if (i != 1 && i != 13)
      return false; 
    return true;
  }
  
  private boolean matchesBluetooth(NetworkIdentity paramNetworkIdentity) {
    if (paramNetworkIdentity.mType == 7)
      return true; 
    return false;
  }
  
  private boolean matchesProxy(NetworkIdentity paramNetworkIdentity) {
    boolean bool;
    if (paramNetworkIdentity.mType == 16) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static String getMatchRuleName(int paramInt) {
    if (paramInt != 1) {
      StringBuilder stringBuilder;
      switch (paramInt) {
        default:
          stringBuilder = new StringBuilder();
          stringBuilder.append("UNKNOWN(");
          stringBuilder.append(paramInt);
          stringBuilder.append(")");
          return stringBuilder.toString();
        case 9:
          return "PROXY";
        case 8:
          return "BLUETOOTH";
        case 7:
          return "WIFI_WILDCARD";
        case 6:
          return "MOBILE_WILDCARD";
        case 5:
          return "ETHERNET";
        case 4:
          break;
      } 
      return "WIFI";
    } 
    return "MOBILE";
  }
  
  public static NetworkTemplate normalize(NetworkTemplate paramNetworkTemplate, String[] paramArrayOfString) {
    return normalize(paramNetworkTemplate, (List)Arrays.asList(new String[][] { paramArrayOfString }));
  }
  
  public static NetworkTemplate normalize(NetworkTemplate paramNetworkTemplate, List<String[]> paramList) {
    if (!paramNetworkTemplate.isMatchRuleMobile())
      return paramNetworkTemplate; 
    for (String[] arrayOfString : paramList) {
      if (ArrayUtils.contains((Object[])arrayOfString, paramNetworkTemplate.mSubscriberId))
        return new NetworkTemplate(paramNetworkTemplate.mMatchRule, arrayOfString[0], arrayOfString, paramNetworkTemplate.mNetworkId); 
    } 
    return paramNetworkTemplate;
  }
  
  static {
    CREATOR = new Parcelable.Creator<NetworkTemplate>() {
        public NetworkTemplate createFromParcel(Parcel param1Parcel) {
          return new NetworkTemplate(param1Parcel);
        }
        
        public NetworkTemplate[] newArray(int param1Int) {
          return new NetworkTemplate[param1Int];
        }
      };
  }
  
  public byte[] getBytesForBackup() throws IOException {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
    dataOutputStream.writeInt(1);
    dataOutputStream.writeInt(this.mMatchRule);
    BackupUtils.writeString(dataOutputStream, this.mSubscriberId);
    BackupUtils.writeString(dataOutputStream, this.mNetworkId);
    return byteArrayOutputStream.toByteArray();
  }
  
  public static NetworkTemplate getNetworkTemplateFromBackup(DataInputStream paramDataInputStream) throws IOException, BackupUtils.BadVersionException {
    int i = paramDataInputStream.readInt();
    if (i >= 1 && i <= 1) {
      i = paramDataInputStream.readInt();
      String str2 = BackupUtils.readString(paramDataInputStream);
      String str1 = BackupUtils.readString(paramDataInputStream);
      if (isKnownMatchRule(i))
        return new NetworkTemplate(i, str2, str1); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Restored network template contains unknown match rule ");
      stringBuilder.append(i);
      throw new BackupUtils.BadVersionException(stringBuilder.toString());
    } 
    throw new BackupUtils.BadVersionException("Unknown Backup Serialization Version");
  }
}
