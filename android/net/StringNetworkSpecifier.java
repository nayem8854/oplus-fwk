package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.android.internal.util.Preconditions;
import java.util.Objects;

public final class StringNetworkSpecifier extends NetworkSpecifier implements Parcelable {
  public StringNetworkSpecifier(String paramString) {
    Preconditions.checkStringNotEmpty(paramString);
    this.specifier = paramString;
  }
  
  public boolean canBeSatisfiedBy(NetworkSpecifier paramNetworkSpecifier) {
    return equals(paramNetworkSpecifier);
  }
  
  public boolean equals(Object paramObject) {
    if (!(paramObject instanceof StringNetworkSpecifier))
      return false; 
    return TextUtils.equals(this.specifier, ((StringNetworkSpecifier)paramObject).specifier);
  }
  
  public int hashCode() {
    return Objects.hashCode(this.specifier);
  }
  
  public String toString() {
    return this.specifier;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.specifier);
  }
  
  public static final Parcelable.Creator<StringNetworkSpecifier> CREATOR = new Parcelable.Creator<StringNetworkSpecifier>() {
      public StringNetworkSpecifier createFromParcel(Parcel param1Parcel) {
        return new StringNetworkSpecifier(param1Parcel.readString());
      }
      
      public StringNetworkSpecifier[] newArray(int param1Int) {
        return new StringNetworkSpecifier[param1Int];
      }
    };
  
  public final String specifier;
}
