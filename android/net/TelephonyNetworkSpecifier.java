package android.net;

import android.os.Parcel;
import android.os.Parcelable;

public final class TelephonyNetworkSpecifier extends NetworkSpecifier implements Parcelable {
  public int getSubscriptionId() {
    return this.mSubId;
  }
  
  public TelephonyNetworkSpecifier(int paramInt) {
    this.mSubId = paramInt;
  }
  
  public static final Parcelable.Creator<TelephonyNetworkSpecifier> CREATOR = new Parcelable.Creator<TelephonyNetworkSpecifier>() {
      public TelephonyNetworkSpecifier createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        return new TelephonyNetworkSpecifier(i);
      }
      
      public TelephonyNetworkSpecifier[] newArray(int param1Int) {
        return new TelephonyNetworkSpecifier[param1Int];
      }
    };
  
  private final int mSubId;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mSubId);
  }
  
  public int hashCode() {
    return this.mSubId;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof TelephonyNetworkSpecifier))
      return false; 
    paramObject = paramObject;
    if (this.mSubId != ((TelephonyNetworkSpecifier)paramObject).mSubId)
      bool = false; 
    return bool;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("TelephonyNetworkSpecifier [");
    stringBuilder.append("mSubId = ");
    stringBuilder.append(this.mSubId);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public boolean canBeSatisfiedBy(NetworkSpecifier paramNetworkSpecifier) {
    return (equals(paramNetworkSpecifier) || paramNetworkSpecifier instanceof MatchAllNetworkSpecifier);
  }
  
  class Builder {
    private static final int SENTINEL_SUB_ID = -2147483648;
    
    private int mSubId;
    
    public Builder() {
      this.mSubId = Integer.MIN_VALUE;
    }
    
    public Builder setSubscriptionId(int param1Int) {
      this.mSubId = param1Int;
      return this;
    }
    
    public TelephonyNetworkSpecifier build() {
      if (this.mSubId != Integer.MIN_VALUE)
        return new TelephonyNetworkSpecifier(this.mSubId); 
      throw new IllegalArgumentException("Subscription Id is not provided.");
    }
  }
}
