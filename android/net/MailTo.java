package android.net;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MailTo {
  private static final String BODY = "body";
  
  private static final String CC = "cc";
  
  public static final String MAILTO_SCHEME = "mailto:";
  
  private static final String SUBJECT = "subject";
  
  private static final String TO = "to";
  
  public static boolean isMailTo(String paramString) {
    if (paramString != null && paramString.startsWith("mailto:"))
      return true; 
    return false;
  }
  
  public static MailTo parse(String paramString) throws ParseException {
    if (paramString != null) {
      if (isMailTo(paramString)) {
        paramString = paramString.substring("mailto:".length());
        Uri uri = Uri.parse(paramString);
        MailTo mailTo = new MailTo();
        paramString = uri.getQuery();
        if (paramString != null)
          for (String paramString : paramString.split("&")) {
            String[] arrayOfString = paramString.split("=");
            if (arrayOfString.length != 0) {
              HashMap<String, String> hashMap = mailTo.mHeaders;
              String str1 = Uri.decode(arrayOfString[0]).toLowerCase(Locale.ROOT);
              if (arrayOfString.length > 1) {
                String str2 = Uri.decode(arrayOfString[1]);
              } else {
                arrayOfString = null;
              } 
              hashMap.put(str1, (String)arrayOfString);
            } 
          }  
        String str = uri.getPath();
        if (str != null) {
          String str1, str2 = mailTo.getTo();
          paramString = str;
          if (str2 != null) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(str);
            stringBuilder.append(", ");
            stringBuilder.append(str2);
            str1 = stringBuilder.toString();
          } 
          mailTo.mHeaders.put("to", str1);
        } 
        return mailTo;
      } 
      throw new ParseException("Not a mailto scheme");
    } 
    throw null;
  }
  
  public String getTo() {
    return this.mHeaders.get("to");
  }
  
  public String getCc() {
    return this.mHeaders.get("cc");
  }
  
  public String getSubject() {
    return this.mHeaders.get("subject");
  }
  
  public String getBody() {
    return this.mHeaders.get("body");
  }
  
  public Map<String, String> getHeaders() {
    return this.mHeaders;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("mailto:");
    stringBuilder.append('?');
    for (Map.Entry<String, String> entry : this.mHeaders.entrySet()) {
      stringBuilder.append(Uri.encode((String)entry.getKey()));
      stringBuilder.append('=');
      stringBuilder.append(Uri.encode((String)entry.getValue()));
      stringBuilder.append('&');
    } 
    return stringBuilder.toString();
  }
  
  private HashMap<String, String> mHeaders = new HashMap<>();
}
