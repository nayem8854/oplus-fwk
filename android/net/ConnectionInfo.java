package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

public final class ConnectionInfo implements Parcelable {
  public int describeContents() {
    return 0;
  }
  
  public ConnectionInfo(int paramInt, InetSocketAddress paramInetSocketAddress1, InetSocketAddress paramInetSocketAddress2) {
    this.protocol = paramInt;
    this.local = paramInetSocketAddress1;
    this.remote = paramInetSocketAddress2;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.protocol);
    paramParcel.writeByteArray(this.local.getAddress().getAddress());
    paramParcel.writeInt(this.local.getPort());
    paramParcel.writeByteArray(this.remote.getAddress().getAddress());
    paramParcel.writeInt(this.remote.getPort());
  }
  
  public static final Parcelable.Creator<ConnectionInfo> CREATOR = new Parcelable.Creator<ConnectionInfo>() {
      public ConnectionInfo createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        try {
          InetAddress inetAddress = InetAddress.getByAddress(param1Parcel.createByteArray());
          int j = param1Parcel.readInt();
          try {
            InetAddress inetAddress1 = InetAddress.getByAddress(param1Parcel.createByteArray());
            int k = param1Parcel.readInt();
            InetSocketAddress inetSocketAddress1 = new InetSocketAddress(inetAddress, j);
            InetSocketAddress inetSocketAddress2 = new InetSocketAddress(inetAddress1, k);
            return new ConnectionInfo(i, inetSocketAddress1, inetSocketAddress2);
          } catch (UnknownHostException unknownHostException) {
            throw new IllegalArgumentException("Invalid InetAddress");
          } 
        } catch (UnknownHostException unknownHostException) {
          throw new IllegalArgumentException("Invalid InetAddress");
        } 
      }
      
      public ConnectionInfo[] newArray(int param1Int) {
        return new ConnectionInfo[param1Int];
      }
    };
  
  public final InetSocketAddress local;
  
  public final int protocol;
  
  public final InetSocketAddress remote;
}
