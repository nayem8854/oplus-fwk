package android.net;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import java.io.FileDescriptor;
import java.io.IOException;

public final class IpSecUdpEncapResponse implements Parcelable {
  public int describeContents() {
    boolean bool;
    if (this.fileDescriptor != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.status);
    paramParcel.writeInt(this.resourceId);
    paramParcel.writeInt(this.port);
    paramParcel.writeParcelable(this.fileDescriptor, 1);
  }
  
  public IpSecUdpEncapResponse(int paramInt) {
    if (paramInt != 0) {
      this.status = paramInt;
      this.resourceId = -1;
      this.port = -1;
      this.fileDescriptor = null;
      return;
    } 
    throw new IllegalArgumentException("Valid status implies other args must be provided");
  }
  
  public IpSecUdpEncapResponse(int paramInt1, int paramInt2, int paramInt3, FileDescriptor paramFileDescriptor) throws IOException {
    if (paramInt1 != 0 || paramFileDescriptor != null) {
      this.status = paramInt1;
      this.resourceId = paramInt2;
      this.port = paramInt3;
      if (paramInt1 == 0) {
        ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.dup(paramFileDescriptor);
      } else {
        paramFileDescriptor = null;
      } 
      this.fileDescriptor = (ParcelFileDescriptor)paramFileDescriptor;
      return;
    } 
    throw new IllegalArgumentException("Valid status implies FD must be non-null");
  }
  
  private IpSecUdpEncapResponse(Parcel paramParcel) {
    this.status = paramParcel.readInt();
    this.resourceId = paramParcel.readInt();
    this.port = paramParcel.readInt();
    this.fileDescriptor = paramParcel.<ParcelFileDescriptor>readParcelable(ParcelFileDescriptor.class.getClassLoader());
  }
  
  public static final Parcelable.Creator<IpSecUdpEncapResponse> CREATOR = new Parcelable.Creator<IpSecUdpEncapResponse>() {
      public IpSecUdpEncapResponse createFromParcel(Parcel param1Parcel) {
        return new IpSecUdpEncapResponse(param1Parcel);
      }
      
      public IpSecUdpEncapResponse[] newArray(int param1Int) {
        return new IpSecUdpEncapResponse[param1Int];
      }
    };
  
  private static final String TAG = "IpSecUdpEncapResponse";
  
  public final ParcelFileDescriptor fileDescriptor;
  
  public final int port;
  
  public final int resourceId;
  
  public final int status;
}
