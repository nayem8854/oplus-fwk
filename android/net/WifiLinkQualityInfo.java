package android.net;

import android.os.Parcel;

public class WifiLinkQualityInfo extends LinkQualityInfo {
  private int mType = Integer.MAX_VALUE;
  
  private int mRssi = Integer.MAX_VALUE;
  
  private long mTxGood = Long.MAX_VALUE;
  
  private long mTxBad = Long.MAX_VALUE;
  
  private String mBssid;
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcel(paramParcel, paramInt, 2);
    paramParcel.writeInt(this.mType);
    paramParcel.writeInt(this.mRssi);
    paramParcel.writeLong(this.mTxGood);
    paramParcel.writeLong(this.mTxBad);
    paramParcel.writeString(this.mBssid);
  }
  
  public static WifiLinkQualityInfo createFromParcelBody(Parcel paramParcel) {
    WifiLinkQualityInfo wifiLinkQualityInfo = new WifiLinkQualityInfo();
    wifiLinkQualityInfo.initializeFromParcel(paramParcel);
    wifiLinkQualityInfo.mType = paramParcel.readInt();
    wifiLinkQualityInfo.mRssi = paramParcel.readInt();
    wifiLinkQualityInfo.mTxGood = paramParcel.readLong();
    wifiLinkQualityInfo.mTxBad = paramParcel.readLong();
    wifiLinkQualityInfo.mBssid = paramParcel.readString();
    return wifiLinkQualityInfo;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public void setType(int paramInt) {
    this.mType = paramInt;
  }
  
  public String getBssid() {
    return this.mBssid;
  }
  
  public void setBssid(String paramString) {
    this.mBssid = paramString;
  }
  
  public int getRssi() {
    return this.mRssi;
  }
  
  public void setRssi(int paramInt) {
    this.mRssi = paramInt;
  }
  
  public long getTxGood() {
    return this.mTxGood;
  }
  
  public void setTxGood(long paramLong) {
    this.mTxGood = paramLong;
  }
  
  public long getTxBad() {
    return this.mTxBad;
  }
  
  public void setTxBad(long paramLong) {
    this.mTxBad = paramLong;
  }
}
