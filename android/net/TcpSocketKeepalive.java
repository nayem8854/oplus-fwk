package android.net;

import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.util.Log;
import java.io.FileDescriptor;
import java.util.concurrent.Executor;

final class TcpSocketKeepalive extends SocketKeepalive {
  TcpSocketKeepalive(IConnectivityManager paramIConnectivityManager, Network paramNetwork, ParcelFileDescriptor paramParcelFileDescriptor, Executor paramExecutor, SocketKeepalive.Callback paramCallback) {
    super(paramIConnectivityManager, paramNetwork, paramParcelFileDescriptor, paramExecutor, paramCallback);
  }
  
  void startImpl(int paramInt) {
    this.mExecutor.execute(new _$$Lambda$TcpSocketKeepalive$E1MP45uBTM6jPfrxAAqXFllEmAA(this, paramInt));
  }
  
  void stopImpl() {
    this.mExecutor.execute(new _$$Lambda$TcpSocketKeepalive$XcFd1FxqMQjF6WWgzFIVR4hV2xk(this));
  }
}
