package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IConnectivityDiagnosticsCallback extends IInterface {
  void onConnectivityReportAvailable(ConnectivityDiagnosticsManager.ConnectivityReport paramConnectivityReport) throws RemoteException;
  
  void onDataStallSuspected(ConnectivityDiagnosticsManager.DataStallReport paramDataStallReport) throws RemoteException;
  
  void onNetworkConnectivityReported(Network paramNetwork, boolean paramBoolean) throws RemoteException;
  
  class Default implements IConnectivityDiagnosticsCallback {
    public void onConnectivityReportAvailable(ConnectivityDiagnosticsManager.ConnectivityReport param1ConnectivityReport) throws RemoteException {}
    
    public void onDataStallSuspected(ConnectivityDiagnosticsManager.DataStallReport param1DataStallReport) throws RemoteException {}
    
    public void onNetworkConnectivityReported(Network param1Network, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IConnectivityDiagnosticsCallback {
    private static final String DESCRIPTOR = "android.net.IConnectivityDiagnosticsCallback";
    
    static final int TRANSACTION_onConnectivityReportAvailable = 1;
    
    static final int TRANSACTION_onDataStallSuspected = 2;
    
    static final int TRANSACTION_onNetworkConnectivityReported = 3;
    
    public Stub() {
      attachInterface(this, "android.net.IConnectivityDiagnosticsCallback");
    }
    
    public static IConnectivityDiagnosticsCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.IConnectivityDiagnosticsCallback");
      if (iInterface != null && iInterface instanceof IConnectivityDiagnosticsCallback)
        return (IConnectivityDiagnosticsCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onNetworkConnectivityReported";
        } 
        return "onDataStallSuspected";
      } 
      return "onConnectivityReportAvailable";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          boolean bool;
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.net.IConnectivityDiagnosticsCallback");
            return true;
          } 
          param1Parcel1.enforceInterface("android.net.IConnectivityDiagnosticsCallback");
          if (param1Parcel1.readInt() != 0) {
            Network network = Network.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel2 = null;
          } 
          if (param1Parcel1.readInt() != 0) {
            bool = true;
          } else {
            bool = false;
          } 
          onNetworkConnectivityReported((Network)param1Parcel2, bool);
          return true;
        } 
        param1Parcel1.enforceInterface("android.net.IConnectivityDiagnosticsCallback");
        if (param1Parcel1.readInt() != 0) {
          ConnectivityDiagnosticsManager.DataStallReport dataStallReport = ConnectivityDiagnosticsManager.DataStallReport.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onDataStallSuspected((ConnectivityDiagnosticsManager.DataStallReport)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.net.IConnectivityDiagnosticsCallback");
      if (param1Parcel1.readInt() != 0) {
        ConnectivityDiagnosticsManager.ConnectivityReport connectivityReport = ConnectivityDiagnosticsManager.ConnectivityReport.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onConnectivityReportAvailable((ConnectivityDiagnosticsManager.ConnectivityReport)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IConnectivityDiagnosticsCallback {
      public static IConnectivityDiagnosticsCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.IConnectivityDiagnosticsCallback";
      }
      
      public void onConnectivityReportAvailable(ConnectivityDiagnosticsManager.ConnectivityReport param2ConnectivityReport) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.IConnectivityDiagnosticsCallback");
          if (param2ConnectivityReport != null) {
            parcel.writeInt(1);
            param2ConnectivityReport.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IConnectivityDiagnosticsCallback.Stub.getDefaultImpl() != null) {
            IConnectivityDiagnosticsCallback.Stub.getDefaultImpl().onConnectivityReportAvailable(param2ConnectivityReport);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDataStallSuspected(ConnectivityDiagnosticsManager.DataStallReport param2DataStallReport) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.IConnectivityDiagnosticsCallback");
          if (param2DataStallReport != null) {
            parcel.writeInt(1);
            param2DataStallReport.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IConnectivityDiagnosticsCallback.Stub.getDefaultImpl() != null) {
            IConnectivityDiagnosticsCallback.Stub.getDefaultImpl().onDataStallSuspected(param2DataStallReport);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onNetworkConnectivityReported(Network param2Network, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.IConnectivityDiagnosticsCallback");
          boolean bool = false;
          if (param2Network != null) {
            parcel.writeInt(1);
            param2Network.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool1 && IConnectivityDiagnosticsCallback.Stub.getDefaultImpl() != null) {
            IConnectivityDiagnosticsCallback.Stub.getDefaultImpl().onNetworkConnectivityReported(param2Network, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IConnectivityDiagnosticsCallback param1IConnectivityDiagnosticsCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IConnectivityDiagnosticsCallback != null) {
          Proxy.sDefaultImpl = param1IConnectivityDiagnosticsCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IConnectivityDiagnosticsCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
