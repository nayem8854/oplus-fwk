package android.net;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.Objects;

@SystemApi
public class RssiCurve implements Parcelable {
  public RssiCurve(int paramInt1, int paramInt2, byte[] paramArrayOfbyte) {
    this(paramInt1, paramInt2, paramArrayOfbyte, 25);
  }
  
  public RssiCurve(int paramInt1, int paramInt2, byte[] paramArrayOfbyte, int paramInt3) {
    this.start = paramInt1;
    this.bucketWidth = paramInt2;
    if (paramArrayOfbyte != null && paramArrayOfbyte.length != 0) {
      this.rssiBuckets = paramArrayOfbyte;
      this.activeNetworkRssiBoost = paramInt3;
      return;
    } 
    throw new IllegalArgumentException("rssiBuckets must be at least one element large.");
  }
  
  private RssiCurve(Parcel paramParcel) {
    this.start = paramParcel.readInt();
    this.bucketWidth = paramParcel.readInt();
    int i = paramParcel.readInt();
    byte[] arrayOfByte = new byte[i];
    paramParcel.readByteArray(arrayOfByte);
    this.activeNetworkRssiBoost = paramParcel.readInt();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.start);
    paramParcel.writeInt(this.bucketWidth);
    paramParcel.writeInt(this.rssiBuckets.length);
    paramParcel.writeByteArray(this.rssiBuckets);
    paramParcel.writeInt(this.activeNetworkRssiBoost);
  }
  
  public byte lookupScore(int paramInt) {
    return lookupScore(paramInt, false);
  }
  
  public byte lookupScore(int paramInt, boolean paramBoolean) {
    int i = paramInt;
    if (paramBoolean)
      i = paramInt + this.activeNetworkRssiBoost; 
    i = (i - this.start) / this.bucketWidth;
    if (i < 0) {
      paramInt = 0;
    } else {
      byte[] arrayOfByte = this.rssiBuckets;
      paramInt = i;
      if (i > arrayOfByte.length - 1)
        paramInt = arrayOfByte.length - 1; 
    } 
    return this.rssiBuckets[paramInt];
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    RssiCurve rssiCurve = (RssiCurve)paramObject;
    if (this.start == rssiCurve.start && this.bucketWidth == rssiCurve.bucketWidth) {
      byte[] arrayOfByte = this.rssiBuckets;
      paramObject = rssiCurve.rssiBuckets;
      if (Arrays.equals(arrayOfByte, (byte[])paramObject) && this.activeNetworkRssiBoost == rssiCurve.activeNetworkRssiBoost)
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.start), Integer.valueOf(this.bucketWidth), Integer.valueOf(this.activeNetworkRssiBoost) }) ^ Arrays.hashCode(this.rssiBuckets);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("RssiCurve[start=");
    int i = this.start;
    stringBuilder.append(i);
    stringBuilder.append(",bucketWidth=");
    i = this.bucketWidth;
    stringBuilder.append(i);
    stringBuilder.append(",activeNetworkRssiBoost=");
    i = this.activeNetworkRssiBoost;
    stringBuilder.append(i);
    stringBuilder.append(",buckets=");
    i = 0;
    while (true) {
      byte[] arrayOfByte = this.rssiBuckets;
      if (i < arrayOfByte.length) {
        stringBuilder.append(arrayOfByte[i]);
        if (i < this.rssiBuckets.length - 1)
          stringBuilder.append(","); 
        i++;
        continue;
      } 
      break;
    } 
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<RssiCurve> CREATOR = new Parcelable.Creator<RssiCurve>() {
      public RssiCurve createFromParcel(Parcel param1Parcel) {
        return new RssiCurve(param1Parcel);
      }
      
      public RssiCurve[] newArray(int param1Int) {
        return new RssiCurve[param1Int];
      }
    };
  
  private static final int DEFAULT_ACTIVE_NETWORK_RSSI_BOOST = 25;
  
  public final int activeNetworkRssiBoost;
  
  public final int bucketWidth;
  
  public final byte[] rssiBuckets;
  
  public final int start;
}
