package android.net;

import android.annotation.SystemApi;
import android.content.Context;
import android.os.Binder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Executor;

@SystemApi
public class NetworkScoreManager {
  @Deprecated
  public static final String ACTION_CHANGE_ACTIVE = "android.net.scoring.CHANGE_ACTIVE";
  
  public static final String ACTION_CUSTOM_ENABLE = "android.net.scoring.CUSTOM_ENABLE";
  
  public static final String ACTION_RECOMMEND_NETWORKS = "android.net.action.RECOMMEND_NETWORKS";
  
  public static final String ACTION_SCORER_CHANGED = "android.net.scoring.SCORER_CHANGED";
  
  @Deprecated
  public static final String ACTION_SCORE_NETWORKS = "android.net.scoring.SCORE_NETWORKS";
  
  @Deprecated
  public static final String EXTRA_NETWORKS_TO_SCORE = "networksToScore";
  
  public static final String EXTRA_NEW_SCORER = "newScorer";
  
  @Deprecated
  public static final String EXTRA_PACKAGE_NAME = "packageName";
  
  public static final String NETWORK_AVAILABLE_NOTIFICATION_CHANNEL_ID_META_DATA = "android.net.wifi.notification_channel_id_network_available";
  
  public static final int RECOMMENDATIONS_ENABLED_FORCED_OFF = -1;
  
  public static final int RECOMMENDATIONS_ENABLED_OFF = 0;
  
  public static final int RECOMMENDATIONS_ENABLED_ON = 1;
  
  public static final String RECOMMENDATION_SERVICE_LABEL_META_DATA = "android.net.scoring.recommendation_service_label";
  
  public static final int SCORE_FILTER_CURRENT_NETWORK = 1;
  
  public static final int SCORE_FILTER_NONE = 0;
  
  public static final int SCORE_FILTER_SCAN_RESULTS = 2;
  
  private static final String TAG = "NetworkScoreManager";
  
  public static final String USE_OPEN_WIFI_PACKAGE_META_DATA = "android.net.wifi.use_open_wifi_package";
  
  private final Context mContext;
  
  private final INetworkScoreService mService;
  
  public NetworkScoreManager(Context paramContext) throws ServiceManager.ServiceNotFoundException {
    this.mContext = paramContext;
    this.mService = INetworkScoreService.Stub.asInterface(ServiceManager.getServiceOrThrow("network_score"));
  }
  
  public String getActiveScorerPackage() {
    try {
      return this.mService.getActiveScorerPackage();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public NetworkScorerAppData getActiveScorer() {
    try {
      return this.mService.getActiveScorer();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<NetworkScorerAppData> getAllValidScorers() {
    try {
      return this.mService.getAllValidScorers();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean updateScores(ScoredNetwork[] paramArrayOfScoredNetwork) throws SecurityException {
    try {
      return this.mService.updateScores(paramArrayOfScoredNetwork);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean clearScores() throws SecurityException {
    try {
      return this.mService.clearScores();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean setActiveScorer(String paramString) throws SecurityException {
    try {
      return this.mService.setActiveScorer(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void disableScoring() throws SecurityException {
    try {
      this.mService.disableScoring();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean requestScores(NetworkKey[] paramArrayOfNetworkKey) throws SecurityException {
    try {
      return this.mService.requestScores(paramArrayOfNetworkKey);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean requestScores(Collection<NetworkKey> paramCollection) throws SecurityException {
    return requestScores(paramCollection.<NetworkKey>toArray(new NetworkKey[0]));
  }
  
  @Deprecated
  public void registerNetworkScoreCache(int paramInt, INetworkScoreCache paramINetworkScoreCache) {
    registerNetworkScoreCache(paramInt, paramINetworkScoreCache, 0);
  }
  
  public void registerNetworkScoreCache(int paramInt1, INetworkScoreCache paramINetworkScoreCache, int paramInt2) {
    try {
      this.mService.registerNetworkScoreCache(paramInt1, paramINetworkScoreCache, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void unregisterNetworkScoreCache(int paramInt, INetworkScoreCache paramINetworkScoreCache) {
    try {
      this.mService.unregisterNetworkScoreCache(paramInt, paramINetworkScoreCache);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public static abstract class NetworkScoreCallback {
    public abstract void onScoresInvalidated();
    
    public abstract void onScoresUpdated(Collection<ScoredNetwork> param1Collection);
  }
  
  class NetworkScoreCallbackProxy extends INetworkScoreCache.Stub {
    private final NetworkScoreManager.NetworkScoreCallback mCallback;
    
    private final Executor mExecutor;
    
    final NetworkScoreManager this$0;
    
    NetworkScoreCallbackProxy(Executor param1Executor, NetworkScoreManager.NetworkScoreCallback param1NetworkScoreCallback) {
      this.mExecutor = param1Executor;
      this.mCallback = param1NetworkScoreCallback;
    }
    
    public void updateScores(List<ScoredNetwork> param1List) {
      Binder.clearCallingIdentity();
      this.mExecutor.execute(new _$$Lambda$NetworkScoreManager$NetworkScoreCallbackProxy$TEOhIiY2C9y8yDWwRR6zm_12TGY(this, param1List));
    }
    
    public void clearScores() {
      Binder.clearCallingIdentity();
      this.mExecutor.execute(new _$$Lambda$NetworkScoreManager$NetworkScoreCallbackProxy$PGkg1UrNyisY0wAts4zoVuYRgkw(this));
    }
  }
  
  @SystemApi
  public void registerNetworkScoreCallback(int paramInt1, int paramInt2, Executor paramExecutor, NetworkScoreCallback paramNetworkScoreCallback) throws SecurityException {
    if (paramNetworkScoreCallback != null && paramExecutor != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("registerNetworkScoreCallback: callback=");
      stringBuilder.append(paramNetworkScoreCallback);
      stringBuilder.append(", executor=");
      stringBuilder.append(paramExecutor);
      Log.v("NetworkScoreManager", stringBuilder.toString());
      registerNetworkScoreCache(paramInt1, new NetworkScoreCallbackProxy(paramExecutor, paramNetworkScoreCallback), paramInt2);
      return;
    } 
    throw new IllegalArgumentException("callback / executor cannot be null");
  }
  
  public boolean isCallerActiveScorer(int paramInt) {
    try {
      return this.mService.isCallerActiveScorer(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface RecommendationsEnabledSetting {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ScoreUpdateFilter {}
}
