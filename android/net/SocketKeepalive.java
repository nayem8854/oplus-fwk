package android.net;

import android.annotation.SystemApi;
import android.os.ParcelFileDescriptor;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.Executor;

public abstract class SocketKeepalive implements AutoCloseable {
  public static final int BINDER_DIED = -10;
  
  public static final int DATA_RECEIVED = -2;
  
  public static final int ERROR_HARDWARE_ERROR = -31;
  
  public static final int ERROR_HARDWARE_UNSUPPORTED = -30;
  
  public static final int ERROR_INSUFFICIENT_RESOURCES = -32;
  
  public static final int ERROR_INVALID_INTERVAL = -24;
  
  public static final int ERROR_INVALID_IP_ADDRESS = -21;
  
  public static final int ERROR_INVALID_LENGTH = -23;
  
  public static final int ERROR_INVALID_NETWORK = -20;
  
  public static final int ERROR_INVALID_PORT = -22;
  
  public static final int ERROR_INVALID_SOCKET = -25;
  
  public static final int ERROR_SOCKET_NOT_IDLE = -26;
  
  public static final int ERROR_UNSUPPORTED = -30;
  
  public static final int MAX_INTERVAL_SEC = 3600;
  
  public static final int MIN_INTERVAL_SEC = 10;
  
  public static final int NO_KEEPALIVE = -1;
  
  @SystemApi
  public static final int SUCCESS = 0;
  
  static final String TAG = "SocketKeepalive";
  
  final ISocketKeepaliveCallback mCallback;
  
  final Executor mExecutor;
  
  final Network mNetwork;
  
  final ParcelFileDescriptor mPfd;
  
  final IConnectivityManager mService;
  
  Integer mSlot;
  
  public static class ErrorCodeException extends Exception {
    public final int error;
    
    public ErrorCodeException(int param1Int, Throwable param1Throwable) {
      super(param1Throwable);
      this.error = param1Int;
    }
    
    public ErrorCodeException(int param1Int) {
      this.error = param1Int;
    }
  }
  
  class InvalidSocketException extends ErrorCodeException {
    public InvalidSocketException(SocketKeepalive this$0, Throwable param1Throwable) {
      super(this$0, param1Throwable);
    }
    
    public InvalidSocketException(SocketKeepalive this$0) {
      super(this$0);
    }
  }
  
  SocketKeepalive(IConnectivityManager paramIConnectivityManager, Network paramNetwork, ParcelFileDescriptor paramParcelFileDescriptor, Executor paramExecutor, Callback paramCallback) {
    this.mService = paramIConnectivityManager;
    this.mNetwork = paramNetwork;
    this.mPfd = paramParcelFileDescriptor;
    this.mExecutor = paramExecutor;
    this.mCallback = (ISocketKeepaliveCallback)new Object(this, paramCallback, paramExecutor);
  }
  
  public final void start(int paramInt) {
    startImpl(paramInt);
  }
  
  public final void stop() {
    stopImpl();
  }
  
  public final void close() {
    stop();
    try {
      this.mPfd.close();
    } catch (IOException iOException) {}
  }
  
  abstract void startImpl(int paramInt);
  
  abstract void stopImpl();
  
  public static class Callback {
    public void onStarted() {}
    
    public void onStopped() {}
    
    public void onError(int param1Int) {}
    
    public void onDataReceived() {}
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ErrorCode {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface KeepaliveEvent {}
}
