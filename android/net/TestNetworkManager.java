package android.net;

import android.os.IBinder;
import android.os.RemoteException;
import com.android.internal.util.Preconditions;

public class TestNetworkManager {
  private static final String TAG = TestNetworkManager.class.getSimpleName();
  
  public static final String TEST_TAP_PREFIX = "testtap";
  
  public static final String TEST_TUN_PREFIX = "testtun";
  
  private final ITestNetworkManager mService;
  
  public TestNetworkManager(ITestNetworkManager paramITestNetworkManager) {
    this.mService = (ITestNetworkManager)Preconditions.checkNotNull(paramITestNetworkManager, "missing ITestNetworkManager");
  }
  
  public void teardownTestNetwork(Network paramNetwork) {
    try {
      this.mService.teardownTestNetwork(paramNetwork.netId);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private void setupTestNetwork(String paramString, LinkProperties paramLinkProperties, boolean paramBoolean, int[] paramArrayOfint, IBinder paramIBinder) {
    try {
      this.mService.setupTestNetwork(paramString, paramLinkProperties, paramBoolean, paramArrayOfint, paramIBinder);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setupTestNetwork(LinkProperties paramLinkProperties, boolean paramBoolean, IBinder paramIBinder) {
    Preconditions.checkNotNull(paramLinkProperties, "Invalid LinkProperties");
    setupTestNetwork(paramLinkProperties.getInterfaceName(), paramLinkProperties, paramBoolean, new int[0], paramIBinder);
  }
  
  public void setupTestNetwork(String paramString, IBinder paramIBinder) {
    setupTestNetwork(paramString, null, true, new int[0], paramIBinder);
  }
  
  public void setupTestNetwork(String paramString, int[] paramArrayOfint, IBinder paramIBinder) {
    setupTestNetwork(paramString, null, true, paramArrayOfint, paramIBinder);
  }
  
  public TestNetworkInterface createTunInterface(LinkAddress[] paramArrayOfLinkAddress) {
    try {
      return this.mService.createTunInterface(paramArrayOfLinkAddress);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public TestNetworkInterface createTapInterface() {
    try {
      return this.mService.createTapInterface();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
}
