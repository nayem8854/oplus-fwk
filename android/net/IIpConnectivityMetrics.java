package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IIpConnectivityMetrics extends IInterface {
  boolean addNetdEventCallback(int paramInt, INetdEventCallback paramINetdEventCallback) throws RemoteException;
  
  int logEvent(ConnectivityMetricsEvent paramConnectivityMetricsEvent) throws RemoteException;
  
  boolean removeNetdEventCallback(int paramInt) throws RemoteException;
  
  class Default implements IIpConnectivityMetrics {
    public int logEvent(ConnectivityMetricsEvent param1ConnectivityMetricsEvent) throws RemoteException {
      return 0;
    }
    
    public boolean addNetdEventCallback(int param1Int, INetdEventCallback param1INetdEventCallback) throws RemoteException {
      return false;
    }
    
    public boolean removeNetdEventCallback(int param1Int) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IIpConnectivityMetrics {
    private static final String DESCRIPTOR = "android.net.IIpConnectivityMetrics";
    
    static final int TRANSACTION_addNetdEventCallback = 2;
    
    static final int TRANSACTION_logEvent = 1;
    
    static final int TRANSACTION_removeNetdEventCallback = 3;
    
    public Stub() {
      attachInterface(this, "android.net.IIpConnectivityMetrics");
    }
    
    public static IIpConnectivityMetrics asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.IIpConnectivityMetrics");
      if (iInterface != null && iInterface instanceof IIpConnectivityMetrics)
        return (IIpConnectivityMetrics)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "removeNetdEventCallback";
        } 
        return "addNetdEventCallback";
      } 
      return "logEvent";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      INetdEventCallback iNetdEventCallback;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.net.IIpConnectivityMetrics");
            return true;
          } 
          param1Parcel1.enforceInterface("android.net.IIpConnectivityMetrics");
          param1Int1 = param1Parcel1.readInt();
          boolean bool1 = removeNetdEventCallback(param1Int1);
          param1Parcel2.writeNoException();
          param1Parcel2.writeInt(bool1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.net.IIpConnectivityMetrics");
        param1Int1 = param1Parcel1.readInt();
        iNetdEventCallback = INetdEventCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
        boolean bool = addNetdEventCallback(param1Int1, iNetdEventCallback);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool);
        return true;
      } 
      iNetdEventCallback.enforceInterface("android.net.IIpConnectivityMetrics");
      if (iNetdEventCallback.readInt() != 0) {
        ConnectivityMetricsEvent connectivityMetricsEvent = ConnectivityMetricsEvent.CREATOR.createFromParcel((Parcel)iNetdEventCallback);
      } else {
        iNetdEventCallback = null;
      } 
      param1Int1 = logEvent((ConnectivityMetricsEvent)iNetdEventCallback);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(param1Int1);
      return true;
    }
    
    private static class Proxy implements IIpConnectivityMetrics {
      public static IIpConnectivityMetrics sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.IIpConnectivityMetrics";
      }
      
      public int logEvent(ConnectivityMetricsEvent param2ConnectivityMetricsEvent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IIpConnectivityMetrics");
          if (param2ConnectivityMetricsEvent != null) {
            parcel1.writeInt(1);
            param2ConnectivityMetricsEvent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IIpConnectivityMetrics.Stub.getDefaultImpl() != null)
            return IIpConnectivityMetrics.Stub.getDefaultImpl().logEvent(param2ConnectivityMetricsEvent); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean addNetdEventCallback(int param2Int, INetdEventCallback param2INetdEventCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IIpConnectivityMetrics");
          parcel1.writeInt(param2Int);
          if (param2INetdEventCallback != null) {
            iBinder = param2INetdEventCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IIpConnectivityMetrics.Stub.getDefaultImpl() != null) {
            bool1 = IIpConnectivityMetrics.Stub.getDefaultImpl().addNetdEventCallback(param2Int, param2INetdEventCallback);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeNetdEventCallback(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IIpConnectivityMetrics");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IIpConnectivityMetrics.Stub.getDefaultImpl() != null) {
            bool1 = IIpConnectivityMetrics.Stub.getDefaultImpl().removeNetdEventCallback(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IIpConnectivityMetrics param1IIpConnectivityMetrics) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IIpConnectivityMetrics != null) {
          Proxy.sDefaultImpl = param1IIpConnectivityMetrics;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IIpConnectivityMetrics getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
