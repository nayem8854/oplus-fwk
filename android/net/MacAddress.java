package android.net;

import android.net.util.MacAddressUtils;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.Inet6Address;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.util.Arrays;

public final class MacAddress implements Parcelable {
  public static final MacAddress ALL_ZEROS_ADDRESS;
  
  private static final MacAddress BASE_GOOGLE_MAC;
  
  public static final MacAddress BROADCAST_ADDRESS;
  
  public static final Parcelable.Creator<MacAddress> CREATOR;
  
  private static final MacAddress DEFAULT_MAC_ADDRESS;
  
  private static final byte[] ETHER_ADDR_BROADCAST;
  
  private static final int ETHER_ADDR_LEN = 6;
  
  private static final long LOCALLY_ASSIGNED_MASK;
  
  private static final long MULTICAST_MASK;
  
  private static final long NIC_MASK;
  
  private static final long OUI_MASK;
  
  public static final int TYPE_BROADCAST = 3;
  
  public static final int TYPE_MULTICAST = 2;
  
  public static final int TYPE_UNICAST = 1;
  
  public static final int TYPE_UNKNOWN = 0;
  
  private static final long VALID_LONG_MASK = 281474976710655L;
  
  private final long mAddr;
  
  static {
    byte[] arrayOfByte = addr(new int[] { 255, 255, 255, 255, 255, 255 });
    BROADCAST_ADDRESS = fromBytes(arrayOfByte);
    ALL_ZEROS_ADDRESS = new MacAddress(0L);
    LOCALLY_ASSIGNED_MASK = (fromString("2:0:0:0:0:0")).mAddr;
    MULTICAST_MASK = (fromString("1:0:0:0:0:0")).mAddr;
    OUI_MASK = (fromString("ff:ff:ff:0:0:0")).mAddr;
    NIC_MASK = (fromString("0:0:0:ff:ff:ff")).mAddr;
    BASE_GOOGLE_MAC = fromString("da:a1:19:0:0:0");
    DEFAULT_MAC_ADDRESS = fromString("02:00:00:00:00:00");
    CREATOR = new Parcelable.Creator<MacAddress>() {
        public MacAddress createFromParcel(Parcel param1Parcel) {
          return new MacAddress(param1Parcel.readLong());
        }
        
        public MacAddress[] newArray(int param1Int) {
          return new MacAddress[param1Int];
        }
      };
  }
  
  private MacAddress(long paramLong) {
    this.mAddr = 0xFFFFFFFFFFFFL & paramLong;
  }
  
  public int getAddressType() {
    if (equals(BROADCAST_ADDRESS))
      return 3; 
    if ((this.mAddr & MULTICAST_MASK) != 0L)
      return 2; 
    return 1;
  }
  
  public boolean isLocallyAssigned() {
    boolean bool;
    if ((this.mAddr & LOCALLY_ASSIGNED_MASK) != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public byte[] toByteArray() {
    return byteAddrFromLongAddr(this.mAddr);
  }
  
  public String toString() {
    return stringAddrFromLongAddr(this.mAddr);
  }
  
  public String toOuiString() {
    long l1 = this.mAddr;
    long l2 = this.mAddr, l3 = this.mAddr;
    return String.format("%02x:%02x:%02x", new Object[] { Long.valueOf(l1 >> 40L & 0xFFL), Long.valueOf(l2 >> 32L & 0xFFL), Long.valueOf(l3 >> 24L & 0xFFL) });
  }
  
  public int hashCode() {
    long l = this.mAddr;
    return (int)(l ^ l >> 32L);
  }
  
  public boolean equals(Object paramObject) {
    boolean bool;
    if (paramObject instanceof MacAddress && ((MacAddress)paramObject).mAddr == this.mAddr) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mAddr);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static boolean isMacAddress(byte[] paramArrayOfbyte) {
    return MacAddressUtils.isMacAddress(paramArrayOfbyte);
  }
  
  public static int macAddressType(byte[] paramArrayOfbyte) {
    if (!isMacAddress(paramArrayOfbyte))
      return 0; 
    return fromBytes(paramArrayOfbyte).getAddressType();
  }
  
  public static byte[] byteAddrFromStringAddr(String paramString) {
    Preconditions.checkNotNull(paramString);
    String[] arrayOfString = paramString.split(":");
    if (arrayOfString.length == 6) {
      byte[] arrayOfByte = new byte[6];
      for (byte b = 0; b < 6; ) {
        int i = Integer.valueOf(arrayOfString[b], 16).intValue();
        if (i >= 0 && 255 >= i) {
          arrayOfByte[b] = (byte)i;
          b++;
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(paramString);
        stringBuilder1.append("was not a valid MAC address");
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      return arrayOfByte;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(" was not a valid MAC address");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static String stringAddrFromByteAddr(byte[] paramArrayOfbyte) {
    if (!isMacAddress(paramArrayOfbyte))
      return null; 
    byte b1 = paramArrayOfbyte[0];
    byte b2 = paramArrayOfbyte[1], b3 = paramArrayOfbyte[2], b4 = paramArrayOfbyte[3], b5 = paramArrayOfbyte[4], b6 = paramArrayOfbyte[5];
    return String.format("%02x:%02x:%02x:%02x:%02x:%02x", new Object[] { Byte.valueOf(b1), Byte.valueOf(b2), Byte.valueOf(b3), Byte.valueOf(b4), Byte.valueOf(b5), Byte.valueOf(b6) });
  }
  
  private static byte[] byteAddrFromLongAddr(long paramLong) {
    return MacAddressUtils.byteAddrFromLongAddr(paramLong);
  }
  
  private static long longAddrFromByteAddr(byte[] paramArrayOfbyte) {
    return MacAddressUtils.longAddrFromByteAddr(paramArrayOfbyte);
  }
  
  private static long longAddrFromStringAddr(String paramString) {
    Preconditions.checkNotNull(paramString);
    String[] arrayOfString = paramString.split(":");
    if (arrayOfString.length == 6) {
      long l = 0L;
      for (byte b = 0; b < arrayOfString.length; ) {
        int i = Integer.valueOf(arrayOfString[b], 16).intValue();
        if (i >= 0 && 255 >= i) {
          l = i + (l << 8L);
          b++;
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(paramString);
        stringBuilder1.append("was not a valid MAC address");
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      return l;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(" was not a valid MAC address");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private static String stringAddrFromLongAddr(long paramLong) {
    return String.format("%02x:%02x:%02x:%02x:%02x:%02x", new Object[] { Long.valueOf(paramLong >> 40L & 0xFFL), Long.valueOf(paramLong >> 32L & 0xFFL), Long.valueOf(paramLong >> 24L & 0xFFL), Long.valueOf(paramLong >> 16L & 0xFFL), Long.valueOf(paramLong >> 8L & 0xFFL), Long.valueOf(paramLong & 0xFFL) });
  }
  
  public static MacAddress fromString(String paramString) {
    return new MacAddress(longAddrFromStringAddr(paramString));
  }
  
  public static MacAddress fromBytes(byte[] paramArrayOfbyte) {
    return new MacAddress(longAddrFromByteAddr(paramArrayOfbyte));
  }
  
  public static MacAddress createRandomUnicastAddressWithGoogleBase() {
    return MacAddressUtils.createRandomUnicastAddress(BASE_GOOGLE_MAC, new SecureRandom());
  }
  
  private static byte[] addr(int... paramVarArgs) {
    if (paramVarArgs.length == 6) {
      byte[] arrayOfByte = new byte[6];
      for (byte b = 0; b < 6; b++)
        arrayOfByte[b] = (byte)paramVarArgs[b]; 
      return arrayOfByte;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(Arrays.toString(paramVarArgs));
    stringBuilder.append(" was not an array with length equal to ");
    stringBuilder.append(6);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public boolean matches(MacAddress paramMacAddress1, MacAddress paramMacAddress2) {
    boolean bool;
    Preconditions.checkNotNull(paramMacAddress1);
    Preconditions.checkNotNull(paramMacAddress2);
    long l1 = this.mAddr, l2 = paramMacAddress2.mAddr;
    if ((l1 & l2) == (l2 & paramMacAddress1.mAddr)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public Inet6Address getLinkLocalIpv6FromEui48Mac() {
    byte[] arrayOfByte1 = toByteArray();
    byte[] arrayOfByte2 = new byte[16];
    arrayOfByte2[0] = -2;
    arrayOfByte2[1] = Byte.MIN_VALUE;
    arrayOfByte2[8] = (byte)(arrayOfByte1[0] ^ 0x2);
    arrayOfByte2[9] = arrayOfByte1[1];
    arrayOfByte2[10] = arrayOfByte1[2];
    arrayOfByte2[11] = -1;
    arrayOfByte2[12] = -2;
    arrayOfByte2[13] = arrayOfByte1[3];
    arrayOfByte2[14] = arrayOfByte1[4];
    arrayOfByte2[15] = arrayOfByte1[5];
    try {
      return Inet6Address.getByAddress((String)null, arrayOfByte2, 0);
    } catch (UnknownHostException unknownHostException) {
      return null;
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class MacAddressType implements Annotation {}
}
