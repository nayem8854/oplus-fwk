package android.net;

import com.android.internal.net.VpnProfile;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.security.GeneralSecurityException;

public abstract class PlatformVpnProfile {
  public static final int MAX_MTU_DEFAULT = 1360;
  
  public static final int TYPE_IKEV2_IPSEC_PSK = 7;
  
  public static final int TYPE_IKEV2_IPSEC_RSA = 8;
  
  public static final int TYPE_IKEV2_IPSEC_USER_PASS = 6;
  
  protected final int mType;
  
  PlatformVpnProfile(int paramInt) {
    this.mType = paramInt;
  }
  
  public final int getType() {
    return this.mType;
  }
  
  public final String getTypeString() {
    int i = this.mType;
    if (i != 6) {
      if (i != 7) {
        if (i != 8)
          return "Unknown VPN profile type"; 
        return "IKEv2/IPsec RSA Digital Signature";
      } 
      return "IKEv2/IPsec Preshared key";
    } 
    return "IKEv2/IPsec Username/Password";
  }
  
  public static PlatformVpnProfile fromVpnProfile(VpnProfile paramVpnProfile) throws IOException, GeneralSecurityException {
    int i = paramVpnProfile.type;
    if (i == 6 || i == 7 || i == 8)
      return Ikev2VpnProfile.fromVpnProfile(paramVpnProfile); 
    throw new IllegalArgumentException("Unknown VPN Profile type");
  }
  
  public abstract VpnProfile toVpnProfile() throws IOException, GeneralSecurityException;
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface PlatformVpnType {}
}
