package android.net;

import android.os.Parcel;
import android.os.Parcelable;

@Deprecated
public class NetworkQuotaInfo implements Parcelable {
  public NetworkQuotaInfo() {}
  
  public NetworkQuotaInfo(Parcel paramParcel) {}
  
  public long getEstimatedBytes() {
    return 0L;
  }
  
  public long getSoftLimitBytes() {
    return -1L;
  }
  
  public long getHardLimitBytes() {
    return -1L;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {}
  
  public static final Parcelable.Creator<NetworkQuotaInfo> CREATOR = new Parcelable.Creator<NetworkQuotaInfo>() {
      public NetworkQuotaInfo createFromParcel(Parcel param1Parcel) {
        return new NetworkQuotaInfo(param1Parcel);
      }
      
      public NetworkQuotaInfo[] newArray(int param1Int) {
        return new NetworkQuotaInfo[param1Int];
      }
    };
  
  public static final long NO_LIMIT = -1L;
}
