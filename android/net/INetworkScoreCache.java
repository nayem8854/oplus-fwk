package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface INetworkScoreCache extends IInterface {
  void clearScores() throws RemoteException;
  
  void updateScores(List<ScoredNetwork> paramList) throws RemoteException;
  
  class Default implements INetworkScoreCache {
    public void updateScores(List<ScoredNetwork> param1List) throws RemoteException {}
    
    public void clearScores() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INetworkScoreCache {
    private static final String DESCRIPTOR = "android.net.INetworkScoreCache";
    
    static final int TRANSACTION_clearScores = 2;
    
    static final int TRANSACTION_updateScores = 1;
    
    public Stub() {
      attachInterface(this, "android.net.INetworkScoreCache");
    }
    
    public static INetworkScoreCache asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.INetworkScoreCache");
      if (iInterface != null && iInterface instanceof INetworkScoreCache)
        return (INetworkScoreCache)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "clearScores";
      } 
      return "updateScores";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.net.INetworkScoreCache");
          return true;
        } 
        param1Parcel1.enforceInterface("android.net.INetworkScoreCache");
        clearScores();
        return true;
      } 
      param1Parcel1.enforceInterface("android.net.INetworkScoreCache");
      ArrayList<ScoredNetwork> arrayList = param1Parcel1.createTypedArrayList(ScoredNetwork.CREATOR);
      updateScores(arrayList);
      return true;
    }
    
    private static class Proxy implements INetworkScoreCache {
      public static INetworkScoreCache sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.INetworkScoreCache";
      }
      
      public void updateScores(List<ScoredNetwork> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.INetworkScoreCache");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && INetworkScoreCache.Stub.getDefaultImpl() != null) {
            INetworkScoreCache.Stub.getDefaultImpl().updateScores(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void clearScores() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.INetworkScoreCache");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && INetworkScoreCache.Stub.getDefaultImpl() != null) {
            INetworkScoreCache.Stub.getDefaultImpl().clearScores();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INetworkScoreCache param1INetworkScoreCache) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INetworkScoreCache != null) {
          Proxy.sDefaultImpl = param1INetworkScoreCache;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INetworkScoreCache getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
