package android.net;

import android.annotation.SystemApi;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.util.concurrent.Executor;

@SystemApi
public abstract class NetworkRecommendationProvider {
  private static final String TAG = "NetworkRecProvider";
  
  private static final boolean VERBOSE;
  
  private final IBinder mService;
  
  static {
    boolean bool;
    if (Build.IS_DEBUGGABLE && Log.isLoggable("NetworkRecProvider", 2)) {
      bool = true;
    } else {
      bool = false;
    } 
    VERBOSE = bool;
  }
  
  public NetworkRecommendationProvider(Context paramContext, Executor paramExecutor) {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramExecutor);
    this.mService = new ServiceWrapper(paramContext, paramExecutor);
  }
  
  public final IBinder getBinder() {
    return this.mService;
  }
  
  public abstract void onRequestScores(NetworkKey[] paramArrayOfNetworkKey);
  
  class ServiceWrapper extends INetworkRecommendationProvider.Stub {
    private final Context mContext;
    
    private final Executor mExecutor;
    
    private final Handler mHandler;
    
    final NetworkRecommendationProvider this$0;
    
    ServiceWrapper(Context param1Context, Executor param1Executor) {
      this.mContext = param1Context;
      this.mExecutor = param1Executor;
      this.mHandler = null;
    }
    
    public void requestScores(final NetworkKey[] networks) throws RemoteException {
      enforceCallingPermission();
      if (networks != null && networks.length > 0)
        execute(new Runnable() {
              final NetworkRecommendationProvider.ServiceWrapper this$1;
              
              final NetworkKey[] val$networks;
              
              public void run() {
                NetworkRecommendationProvider.this.onRequestScores(networks);
              }
            }); 
    }
    
    private void execute(Runnable param1Runnable) {
      Executor executor = this.mExecutor;
      if (executor != null) {
        executor.execute(param1Runnable);
      } else {
        this.mHandler.post(param1Runnable);
      } 
    }
    
    private void enforceCallingPermission() {
      Context context = this.mContext;
      if (context != null)
        context.enforceCallingOrSelfPermission("android.permission.REQUEST_NETWORK_SCORES", "Permission denied."); 
    }
  }
  
  class null implements Runnable {
    final NetworkRecommendationProvider.ServiceWrapper this$1;
    
    final NetworkKey[] val$networks;
    
    public void run() {
      NetworkRecommendationProvider.this.onRequestScores(networks);
    }
  }
}
