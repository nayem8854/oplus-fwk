package android.net;

import android.annotation.SystemApi;
import android.content.Intent;
import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.StrictMode;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.AbstractList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.RandomAccess;
import java.util.Set;

public abstract class Uri implements Parcelable, Comparable<Uri> {
  public static final Parcelable.Creator<Uri> CREATOR;
  
  private static final String DEFAULT_ENCODING = "UTF-8";
  
  public static final Uri EMPTY;
  
  private static final char[] HEX_DIGITS;
  
  private static final String LOG = Uri.class.getSimpleName();
  
  private static final String NOT_CACHED = new String("NOT CACHED");
  
  private static final int NOT_CALCULATED = -2;
  
  private static final int NOT_FOUND = -1;
  
  private static final String NOT_HIERARCHICAL = "This isn't a hierarchical URI.";
  
  private static final int NULL_TYPE_ID = 0;
  
  static {
    EMPTY = new HierarchicalUri(Part.NULL, PathPart.EMPTY, Part.NULL, Part.NULL);
    CREATOR = new Parcelable.Creator<Uri>() {
        public Uri createFromParcel(Parcel param1Parcel) {
          int i = param1Parcel.readInt();
          if (i != 0) {
            StringBuilder stringBuilder;
            if (i != 1) {
              if (i != 2) {
                if (i == 3)
                  return Uri.HierarchicalUri.readFrom(param1Parcel); 
                stringBuilder = new StringBuilder();
                stringBuilder.append("Unknown URI type: ");
                stringBuilder.append(i);
                throw new IllegalArgumentException(stringBuilder.toString());
              } 
              return Uri.OpaqueUri.readFrom((Parcel)stringBuilder);
            } 
            return Uri.StringUri.readFrom((Parcel)stringBuilder);
          } 
          return null;
        }
        
        public Uri[] newArray(int param1Int) {
          return new Uri[param1Int];
        }
      };
    HEX_DIGITS = "0123456789ABCDEF".toCharArray();
  }
  
  private Uri() {}
  
  public boolean isOpaque() {
    return isHierarchical() ^ true;
  }
  
  public boolean isAbsolute() {
    return isRelative() ^ true;
  }
  
  public boolean equals(Object paramObject) {
    if (!(paramObject instanceof Uri))
      return false; 
    paramObject = paramObject;
    return toString().equals(paramObject.toString());
  }
  
  public int hashCode() {
    return toString().hashCode();
  }
  
  public int compareTo(Uri paramUri) {
    return toString().compareTo(paramUri.toString());
  }
  
  @SystemApi
  public String toSafeString() {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getScheme : ()Ljava/lang/String;
    //   4: astore_1
    //   5: aload_0
    //   6: invokevirtual getSchemeSpecificPart : ()Ljava/lang/String;
    //   9: astore_2
    //   10: aload_2
    //   11: astore_3
    //   12: aload_1
    //   13: ifnull -> 335
    //   16: aload_1
    //   17: ldc_w 'tel'
    //   20: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   23: ifne -> 236
    //   26: aload_1
    //   27: ldc_w 'sip'
    //   30: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   33: ifne -> 236
    //   36: aload_1
    //   37: ldc_w 'sms'
    //   40: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   43: ifne -> 236
    //   46: aload_1
    //   47: ldc_w 'smsto'
    //   50: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   53: ifne -> 236
    //   56: aload_1
    //   57: ldc_w 'mailto'
    //   60: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   63: ifne -> 236
    //   66: aload_1
    //   67: ldc_w 'nfc'
    //   70: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   73: ifeq -> 79
    //   76: goto -> 236
    //   79: aload_1
    //   80: ldc_w 'http'
    //   83: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   86: ifne -> 121
    //   89: aload_1
    //   90: ldc_w 'https'
    //   93: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   96: ifne -> 121
    //   99: aload_1
    //   100: ldc_w 'ftp'
    //   103: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   106: ifne -> 121
    //   109: aload_2
    //   110: astore_3
    //   111: aload_1
    //   112: ldc_w 'rtsp'
    //   115: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   118: ifeq -> 335
    //   121: new java/lang/StringBuilder
    //   124: dup
    //   125: invokespecial <init> : ()V
    //   128: astore #4
    //   130: aload #4
    //   132: ldc_w '//'
    //   135: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   138: pop
    //   139: aload_0
    //   140: invokevirtual getHost : ()Ljava/lang/String;
    //   143: astore_3
    //   144: ldc_w ''
    //   147: astore_2
    //   148: aload_3
    //   149: ifnull -> 160
    //   152: aload_0
    //   153: invokevirtual getHost : ()Ljava/lang/String;
    //   156: astore_3
    //   157: goto -> 164
    //   160: ldc_w ''
    //   163: astore_3
    //   164: aload #4
    //   166: aload_3
    //   167: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   170: pop
    //   171: aload_2
    //   172: astore_3
    //   173: aload_0
    //   174: invokevirtual getPort : ()I
    //   177: iconst_m1
    //   178: if_icmpeq -> 211
    //   181: new java/lang/StringBuilder
    //   184: dup
    //   185: invokespecial <init> : ()V
    //   188: astore_3
    //   189: aload_3
    //   190: ldc_w ':'
    //   193: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   196: pop
    //   197: aload_3
    //   198: aload_0
    //   199: invokevirtual getPort : ()I
    //   202: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   205: pop
    //   206: aload_3
    //   207: invokevirtual toString : ()Ljava/lang/String;
    //   210: astore_3
    //   211: aload #4
    //   213: aload_3
    //   214: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   217: pop
    //   218: aload #4
    //   220: ldc_w '/...'
    //   223: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   226: pop
    //   227: aload #4
    //   229: invokevirtual toString : ()Ljava/lang/String;
    //   232: astore_3
    //   233: goto -> 335
    //   236: new java/lang/StringBuilder
    //   239: dup
    //   240: bipush #64
    //   242: invokespecial <init> : (I)V
    //   245: astore_3
    //   246: aload_3
    //   247: aload_1
    //   248: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   251: pop
    //   252: aload_3
    //   253: bipush #58
    //   255: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   258: pop
    //   259: aload_2
    //   260: ifnull -> 330
    //   263: iconst_0
    //   264: istore #5
    //   266: iload #5
    //   268: aload_2
    //   269: invokevirtual length : ()I
    //   272: if_icmpge -> 330
    //   275: aload_2
    //   276: iload #5
    //   278: invokevirtual charAt : (I)C
    //   281: istore #6
    //   283: iload #6
    //   285: bipush #45
    //   287: if_icmpeq -> 317
    //   290: iload #6
    //   292: bipush #64
    //   294: if_icmpeq -> 317
    //   297: iload #6
    //   299: bipush #46
    //   301: if_icmpne -> 307
    //   304: goto -> 317
    //   307: aload_3
    //   308: bipush #120
    //   310: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   313: pop
    //   314: goto -> 324
    //   317: aload_3
    //   318: iload #6
    //   320: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   323: pop
    //   324: iinc #5, 1
    //   327: goto -> 266
    //   330: aload_3
    //   331: invokevirtual toString : ()Ljava/lang/String;
    //   334: areturn
    //   335: new java/lang/StringBuilder
    //   338: dup
    //   339: bipush #64
    //   341: invokespecial <init> : (I)V
    //   344: astore_2
    //   345: aload_1
    //   346: ifnull -> 362
    //   349: aload_2
    //   350: aload_1
    //   351: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   354: pop
    //   355: aload_2
    //   356: bipush #58
    //   358: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   361: pop
    //   362: aload_3
    //   363: ifnull -> 372
    //   366: aload_2
    //   367: aload_3
    //   368: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   371: pop
    //   372: aload_2
    //   373: invokevirtual toString : ()Ljava/lang/String;
    //   376: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #388	-> 0
    //   #389	-> 5
    //   #390	-> 10
    //   #391	-> 16
    //   #392	-> 36
    //   #393	-> 56
    //   #408	-> 79
    //   #409	-> 99
    //   #410	-> 121
    //   #411	-> 171
    //   #394	-> 236
    //   #395	-> 246
    //   #396	-> 252
    //   #397	-> 259
    //   #398	-> 263
    //   #399	-> 275
    //   #400	-> 283
    //   #403	-> 307
    //   #401	-> 317
    //   #398	-> 324
    //   #407	-> 330
    //   #418	-> 335
    //   #419	-> 345
    //   #420	-> 349
    //   #421	-> 355
    //   #423	-> 362
    //   #424	-> 366
    //   #426	-> 372
  }
  
  public static Uri parse(String paramString) {
    return new StringUri();
  }
  
  public static Uri fromFile(File paramFile) {
    if (paramFile != null) {
      PathPart pathPart = PathPart.fromDecoded(paramFile.getAbsolutePath());
      return new HierarchicalUri(Part.EMPTY, pathPart, Part.NULL, Part.NULL);
    } 
    throw new NullPointerException("file");
  }
  
  class StringUri extends AbstractHierarchicalUri {
    static final int TYPE_ID = 1;
    
    private Uri.Part authority;
    
    private volatile int cachedFsi;
    
    private volatile int cachedSsi;
    
    private Uri.Part fragment;
    
    private Uri.PathPart path;
    
    private Uri.Part query;
    
    private volatile String scheme;
    
    private Uri.Part ssp;
    
    private final String uriString;
    
    private StringUri(Uri this$0) {
      this.cachedSsi = -2;
      this.cachedFsi = -2;
      this.scheme = Uri.NOT_CACHED;
      if (this$0 != null) {
        this.uriString = (String)this$0;
        return;
      } 
      throw new NullPointerException("uriString");
    }
    
    static Uri readFrom(Parcel param1Parcel) {
      return new StringUri();
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(1);
      param1Parcel.writeString8(this.uriString);
    }
    
    private int findSchemeSeparator() {
      int i;
      if (this.cachedSsi == -2) {
        this.cachedSsi = i = this.uriString.indexOf(':');
      } else {
        i = this.cachedSsi;
      } 
      return i;
    }
    
    private int findFragmentSeparator() {
      int i;
      if (this.cachedFsi == -2) {
        this.cachedFsi = i = this.uriString.indexOf('#', findSchemeSeparator());
      } else {
        i = this.cachedFsi;
      } 
      return i;
    }
    
    public boolean isHierarchical() {
      int i = findSchemeSeparator();
      boolean bool = true;
      if (i == -1)
        return true; 
      if (this.uriString.length() == i + 1)
        return false; 
      if (this.uriString.charAt(i + 1) != '/')
        bool = false; 
      return bool;
    }
    
    public boolean isRelative() {
      boolean bool;
      if (findSchemeSeparator() == -1) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public String getScheme() {
      boolean bool;
      String str;
      if (this.scheme != Uri.NOT_CACHED) {
        bool = true;
      } else {
        bool = false;
      } 
      if (bool) {
        str = this.scheme;
      } else {
        this.scheme = str = parseScheme();
      } 
      return str;
    }
    
    private String parseScheme() {
      String str;
      int i = findSchemeSeparator();
      if (i == -1) {
        str = null;
      } else {
        str = this.uriString.substring(0, i);
      } 
      return str;
    }
    
    private Uri.Part getSsp() {
      Uri.Part part1 = this.ssp, part2 = part1;
      if (part1 == null)
        this.ssp = part2 = Uri.Part.fromEncoded(parseSsp()); 
      return part2;
    }
    
    public String getEncodedSchemeSpecificPart() {
      return getSsp().getEncoded();
    }
    
    public String getSchemeSpecificPart() {
      return getSsp().getDecoded();
    }
    
    private String parseSsp() {
      String str;
      int i = findSchemeSeparator();
      int j = findFragmentSeparator();
      if (j == -1) {
        str = this.uriString.substring(i + 1);
      } else {
        str = this.uriString.substring(i + 1, j);
      } 
      return str;
    }
    
    private Uri.Part getAuthorityPart() {
      Uri.Part part = this.authority;
      if (part == null) {
        String str = this.uriString;
        str = parseAuthority(str, findSchemeSeparator());
        this.authority = part = Uri.Part.fromEncoded(str);
        return part;
      } 
      return part;
    }
    
    public String getEncodedAuthority() {
      return getAuthorityPart().getEncoded();
    }
    
    public String getAuthority() {
      return getAuthorityPart().getDecoded();
    }
    
    private Uri.PathPart getPathPart() {
      Uri.PathPart pathPart = this.path;
      if (pathPart == null)
        this.path = pathPart = Uri.PathPart.fromEncoded(parsePath()); 
      return pathPart;
    }
    
    public String getPath() {
      return getPathPart().getDecoded();
    }
    
    public String getEncodedPath() {
      return getPathPart().getEncoded();
    }
    
    public List<String> getPathSegments() {
      return getPathPart().getPathSegments();
    }
    
    private String parsePath() {
      String str = this.uriString;
      int i = findSchemeSeparator();
      if (i > -1) {
        boolean bool;
        if (i + 1 == str.length()) {
          bool = true;
        } else {
          bool = false;
        } 
        if (bool)
          return null; 
        if (str.charAt(i + 1) != '/')
          return null; 
      } 
      return parsePath(str, i);
    }
    
    private Uri.Part getQueryPart() {
      Uri.Part part = this.query;
      if (part == null)
        this.query = part = Uri.Part.fromEncoded(parseQuery()); 
      return part;
    }
    
    public String getEncodedQuery() {
      return getQueryPart().getEncoded();
    }
    
    private String parseQuery() {
      int i = this.uriString.indexOf('?', findSchemeSeparator());
      if (i == -1)
        return null; 
      int j = findFragmentSeparator();
      if (j == -1)
        return this.uriString.substring(i + 1); 
      if (j < i)
        return null; 
      return this.uriString.substring(i + 1, j);
    }
    
    public String getQuery() {
      return getQueryPart().getDecoded();
    }
    
    private Uri.Part getFragmentPart() {
      Uri.Part part = this.fragment;
      if (part == null)
        this.fragment = part = Uri.Part.fromEncoded(parseFragment()); 
      return part;
    }
    
    public String getEncodedFragment() {
      return getFragmentPart().getEncoded();
    }
    
    private String parseFragment() {
      String str;
      int i = findFragmentSeparator();
      if (i == -1) {
        str = null;
      } else {
        str = this.uriString.substring(i + 1);
      } 
      return str;
    }
    
    public String getFragment() {
      return getFragmentPart().getDecoded();
    }
    
    public String toString() {
      return this.uriString;
    }
    
    static String parseAuthority(String param1String, int param1Int) {
      int i = param1String.length();
      if (i > param1Int + 2 && param1String.charAt(param1Int + 1) == '/' && param1String.charAt(param1Int + 2) == '/') {
        int j = param1Int + 3;
        while (j < i) {
          char c = param1String.charAt(j);
          if (c != '#' && c != '/' && c != '?' && c != '\\')
            j++; 
        } 
        return param1String.substring(param1Int + 3, j);
      } 
      return null;
    }
    
    static String parsePath(String param1String, int param1Int) {
      int i = param1String.length();
      if (i > param1Int + 2 && param1String.charAt(param1Int + 1) == '/' && param1String.charAt(param1Int + 2) == '/') {
        int k = param1Int + 3;
        while (true) {
          param1Int = k;
          if (k < i) {
            param1Int = param1String.charAt(k);
            if (param1Int != 35) {
              if (param1Int != 47)
                if (param1Int != 63) {
                  if (param1Int != 92) {
                    k++;
                    continue;
                  } 
                } else {
                  return "";
                }  
              param1Int = k;
              break;
            } 
            return "";
          } 
          break;
        } 
      } else {
        param1Int++;
      } 
      int j = param1Int;
      while (j < i) {
        char c = param1String.charAt(j);
        if (c != '#' && c != '?')
          j++; 
      } 
      return param1String.substring(param1Int, j);
    }
    
    public Uri.Builder buildUpon() {
      if (isHierarchical()) {
        null = new Uri.Builder();
        null = null.scheme(getScheme());
        null = null.authority(getAuthorityPart());
        null = null.path(getPathPart());
        null = null.query(getQueryPart());
        return null.fragment(getFragmentPart());
      } 
      null = new Uri.Builder();
      null = null.scheme(getScheme());
      null = null.opaquePart(getSsp());
      return null.fragment(getFragmentPart());
    }
  }
  
  public static Uri fromParts(String paramString1, String paramString2, String paramString3) {
    if (paramString1 != null) {
      if (paramString2 != null) {
        Part part = Part.fromDecoded(paramString2);
        return new OpaqueUri(part, Part.fromDecoded(paramString3));
      } 
      throw new NullPointerException("ssp");
    } 
    throw new NullPointerException("scheme");
  }
  
  class OpaqueUri extends Uri {
    static final int TYPE_ID = 2;
    
    private volatile String cachedString;
    
    private final Uri.Part fragment;
    
    private final String scheme;
    
    private final Uri.Part ssp;
    
    private OpaqueUri(Uri this$0, Uri.Part param1Part1, Uri.Part param1Part2) {
      this.cachedString = Uri.NOT_CACHED;
      this.scheme = (String)this$0;
      this.ssp = param1Part1;
      if (param1Part2 == null)
        param1Part2 = Uri.Part.NULL; 
      this.fragment = param1Part2;
    }
    
    static Uri readFrom(Parcel param1Parcel) {
      String str = param1Parcel.readString8();
      Uri.Part part = Uri.Part.readFrom(param1Parcel);
      return new OpaqueUri(part, Uri.Part.readFrom(param1Parcel));
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(2);
      param1Parcel.writeString8(this.scheme);
      this.ssp.writeTo(param1Parcel);
      this.fragment.writeTo(param1Parcel);
    }
    
    public boolean isHierarchical() {
      return false;
    }
    
    public boolean isRelative() {
      boolean bool;
      if (this.scheme == null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public String getScheme() {
      return this.scheme;
    }
    
    public String getEncodedSchemeSpecificPart() {
      return this.ssp.getEncoded();
    }
    
    public String getSchemeSpecificPart() {
      return this.ssp.getDecoded();
    }
    
    public String getAuthority() {
      return null;
    }
    
    public String getEncodedAuthority() {
      return null;
    }
    
    public String getPath() {
      return null;
    }
    
    public String getEncodedPath() {
      return null;
    }
    
    public String getQuery() {
      return null;
    }
    
    public String getEncodedQuery() {
      return null;
    }
    
    public String getFragment() {
      return this.fragment.getDecoded();
    }
    
    public String getEncodedFragment() {
      return this.fragment.getEncoded();
    }
    
    public List<String> getPathSegments() {
      return Collections.emptyList();
    }
    
    public String getLastPathSegment() {
      return null;
    }
    
    public String getUserInfo() {
      return null;
    }
    
    public String getEncodedUserInfo() {
      return null;
    }
    
    public String getHost() {
      return null;
    }
    
    public int getPort() {
      return -1;
    }
    
    public String toString() {
      boolean bool;
      if (this.cachedString != Uri.NOT_CACHED) {
        bool = true;
      } else {
        bool = false;
      } 
      if (bool)
        return this.cachedString; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this.scheme);
      stringBuilder.append(':');
      stringBuilder.append(getEncodedSchemeSpecificPart());
      if (!this.fragment.isEmpty()) {
        stringBuilder.append('#');
        stringBuilder.append(this.fragment.getEncoded());
      } 
      String str = stringBuilder.toString();
      return str;
    }
    
    public Uri.Builder buildUpon() {
      Uri.Builder builder2 = new Uri.Builder();
      String str = this.scheme;
      Uri.Builder builder3 = builder2.scheme(str);
      Uri.Part part1 = this.ssp;
      Uri.Builder builder1 = builder3.opaquePart(part1);
      Uri.Part part2 = this.fragment;
      return builder1.fragment(part2);
    }
  }
  
  class PathSegments extends AbstractList<String> implements RandomAccess {
    static final PathSegments EMPTY = new PathSegments(null, 0);
    
    final String[] segments;
    
    final int size;
    
    PathSegments(Uri this$0, int param1Int) {
      this.segments = (String[])this$0;
      this.size = param1Int;
    }
    
    public String get(int param1Int) {
      if (param1Int < this.size)
        return this.segments[param1Int]; 
      throw new IndexOutOfBoundsException();
    }
    
    public int size() {
      return this.size;
    }
  }
  
  class PathSegmentsBuilder {
    String[] segments;
    
    int size;
    
    PathSegmentsBuilder() {
      this.size = 0;
    }
    
    void add(String param1String) {
      String[] arrayOfString1 = this.segments;
      if (arrayOfString1 == null) {
        this.segments = new String[4];
      } else if (this.size + 1 == arrayOfString1.length) {
        String[] arrayOfString = new String[arrayOfString1.length * 2];
        System.arraycopy(arrayOfString1, 0, arrayOfString, 0, arrayOfString1.length);
        this.segments = arrayOfString;
      } 
      String[] arrayOfString2 = this.segments;
      int i = this.size;
      this.size = i + 1;
      arrayOfString2[i] = param1String;
    }
    
    Uri.PathSegments build() {
      if (this.segments == null)
        return Uri.PathSegments.EMPTY; 
      try {
        return new Uri.PathSegments(this.segments, this.size);
      } finally {
        this.segments = null;
      } 
    }
  }
  
  class AbstractHierarchicalUri extends Uri {
    private volatile String host;
    
    private volatile int port;
    
    private Uri.Part userInfo;
    
    private AbstractHierarchicalUri() {
      this.host = Uri.NOT_CACHED;
      this.port = -2;
    }
    
    public String getLastPathSegment() {
      List<String> list = getPathSegments();
      int i = list.size();
      if (i == 0)
        return null; 
      return list.get(i - 1);
    }
    
    private Uri.Part getUserInfoPart() {
      Uri.Part part = this.userInfo;
      if (part == null)
        this.userInfo = part = Uri.Part.fromEncoded(parseUserInfo()); 
      return part;
    }
    
    public final String getEncodedUserInfo() {
      return getUserInfoPart().getEncoded();
    }
    
    private String parseUserInfo() {
      String str1 = getEncodedAuthority();
      String str2 = null;
      if (str1 == null)
        return null; 
      int i = str1.lastIndexOf('@');
      if (i != -1)
        str2 = str1.substring(0, i); 
      return str2;
    }
    
    public String getUserInfo() {
      return getUserInfoPart().getDecoded();
    }
    
    public String getHost() {
      boolean bool;
      String str;
      if (this.host != Uri.NOT_CACHED) {
        bool = true;
      } else {
        bool = false;
      } 
      if (bool) {
        str = this.host;
      } else {
        this.host = str = parseHost();
      } 
      return str;
    }
    
    private String parseHost() {
      String str = getEncodedAuthority();
      if (str == null)
        return null; 
      int i = str.lastIndexOf('@');
      int j = findPortSeparator(str);
      if (j == -1) {
        str = str.substring(i + 1);
      } else {
        str = str.substring(i + 1, j);
      } 
      return decode(str);
    }
    
    public int getPort() {
      int i;
      if (this.port == -2) {
        this.port = i = parsePort();
      } else {
        i = this.port;
      } 
      return i;
    }
    
    private int parsePort() {
      String str = getEncodedAuthority();
      int i = findPortSeparator(str);
      if (i == -1)
        return -1; 
      str = decode(str.substring(i + 1));
      try {
        return Integer.parseInt(str);
      } catch (NumberFormatException numberFormatException) {
        Log.w(Uri.LOG, "Error parsing port string.", numberFormatException);
        return -1;
      } 
    }
    
    private int findPortSeparator(String param1String) {
      if (param1String == null)
        return -1; 
      for (int i = param1String.length() - 1; i >= 0; i--) {
        char c = param1String.charAt(i);
        if (':' == c)
          return i; 
        if (c < '0' || c > '9')
          return -1; 
      } 
      return -1;
    }
  }
  
  class HierarchicalUri extends AbstractHierarchicalUri {
    static final int TYPE_ID = 3;
    
    private final Uri.Part authority;
    
    private final Uri.Part fragment;
    
    private final Uri.PathPart path;
    
    private final Uri.Part query;
    
    private final String scheme;
    
    private Uri.Part ssp;
    
    private volatile String uriString;
    
    private HierarchicalUri(Uri this$0, Uri.Part param1Part1, Uri.PathPart param1PathPart, Uri.Part param1Part2, Uri.Part param1Part3) {
      Uri.PathPart pathPart;
      this.uriString = Uri.NOT_CACHED;
      this.scheme = (String)this$0;
      this.authority = Uri.Part.nonNull(param1Part1);
      if (param1PathPart == null) {
        pathPart = Uri.PathPart.NULL;
      } else {
        pathPart = param1PathPart;
      } 
      this.path = pathPart;
      this.query = Uri.Part.nonNull(param1Part2);
      this.fragment = Uri.Part.nonNull(param1Part3);
    }
    
    static Uri readFrom(Parcel param1Parcel) {
      String str = param1Parcel.readString8();
      Uri.Part part1 = Uri.Part.readFrom(param1Parcel);
      Uri.PathPart pathPart = Uri.PathPart.readFrom(param1Parcel);
      Uri.Part part2 = Uri.Part.readFrom(param1Parcel);
      return new HierarchicalUri(part1, pathPart, part2, Uri.Part.readFrom(param1Parcel));
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(3);
      param1Parcel.writeString8(this.scheme);
      this.authority.writeTo(param1Parcel);
      this.path.writeTo(param1Parcel);
      this.query.writeTo(param1Parcel);
      this.fragment.writeTo(param1Parcel);
    }
    
    public boolean isHierarchical() {
      return true;
    }
    
    public boolean isRelative() {
      boolean bool;
      if (this.scheme == null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public String getScheme() {
      return this.scheme;
    }
    
    private Uri.Part getSsp() {
      Uri.Part part = this.ssp;
      if (part == null)
        this.ssp = part = Uri.Part.fromEncoded(makeSchemeSpecificPart()); 
      return part;
    }
    
    public String getEncodedSchemeSpecificPart() {
      return getSsp().getEncoded();
    }
    
    public String getSchemeSpecificPart() {
      return getSsp().getDecoded();
    }
    
    private String makeSchemeSpecificPart() {
      StringBuilder stringBuilder = new StringBuilder();
      appendSspTo(stringBuilder);
      return stringBuilder.toString();
    }
    
    private void appendSspTo(StringBuilder param1StringBuilder) {
      String str = this.authority.getEncoded();
      if (str != null) {
        param1StringBuilder.append("//");
        param1StringBuilder.append(str);
      } 
      str = this.path.getEncoded();
      if (str != null)
        param1StringBuilder.append(str); 
      if (!this.query.isEmpty()) {
        param1StringBuilder.append('?');
        param1StringBuilder.append(this.query.getEncoded());
      } 
    }
    
    public String getAuthority() {
      return this.authority.getDecoded();
    }
    
    public String getEncodedAuthority() {
      return this.authority.getEncoded();
    }
    
    public String getEncodedPath() {
      return this.path.getEncoded();
    }
    
    public String getPath() {
      return this.path.getDecoded();
    }
    
    public String getQuery() {
      return this.query.getDecoded();
    }
    
    public String getEncodedQuery() {
      return this.query.getEncoded();
    }
    
    public String getFragment() {
      return this.fragment.getDecoded();
    }
    
    public String getEncodedFragment() {
      return this.fragment.getEncoded();
    }
    
    public List<String> getPathSegments() {
      return this.path.getPathSegments();
    }
    
    public String toString() {
      boolean bool;
      String str;
      if (this.uriString != Uri.NOT_CACHED) {
        bool = true;
      } else {
        bool = false;
      } 
      if (bool) {
        str = this.uriString;
      } else {
        this.uriString = str = makeUriString();
      } 
      return str;
    }
    
    private String makeUriString() {
      StringBuilder stringBuilder = new StringBuilder();
      String str = this.scheme;
      if (str != null) {
        stringBuilder.append(str);
        stringBuilder.append(':');
      } 
      appendSspTo(stringBuilder);
      if (!this.fragment.isEmpty()) {
        stringBuilder.append('#');
        stringBuilder.append(this.fragment.getEncoded());
      } 
      return stringBuilder.toString();
    }
    
    public Uri.Builder buildUpon() {
      Uri.Builder builder1 = new Uri.Builder();
      String str = this.scheme;
      Uri.Builder builder2 = builder1.scheme(str);
      Uri.Part part1 = this.authority;
      builder2 = builder2.authority(part1);
      Uri.PathPart pathPart = this.path;
      null = builder2.path(pathPart);
      Uri.Part part2 = this.query;
      null = null.query(part2);
      part2 = this.fragment;
      return null.fragment(part2);
    }
  }
  
  class Builder {
    private Uri.Part authority;
    
    private Uri.Part fragment;
    
    private Uri.Part opaquePart;
    
    private Uri.PathPart path;
    
    private Uri.Part query;
    
    private String scheme;
    
    public Builder scheme(String param1String) {
      this.scheme = param1String;
      return this;
    }
    
    Builder opaquePart(Uri.Part param1Part) {
      this.opaquePart = param1Part;
      return this;
    }
    
    public Builder opaquePart(String param1String) {
      return opaquePart(Uri.Part.fromDecoded(param1String));
    }
    
    public Builder encodedOpaquePart(String param1String) {
      return opaquePart(Uri.Part.fromEncoded(param1String));
    }
    
    Builder authority(Uri.Part param1Part) {
      this.opaquePart = null;
      this.authority = param1Part;
      return this;
    }
    
    public Builder authority(String param1String) {
      return authority(Uri.Part.fromDecoded(param1String));
    }
    
    public Builder encodedAuthority(String param1String) {
      return authority(Uri.Part.fromEncoded(param1String));
    }
    
    Builder path(Uri.PathPart param1PathPart) {
      this.opaquePart = null;
      this.path = param1PathPart;
      return this;
    }
    
    public Builder path(String param1String) {
      return path(Uri.PathPart.fromDecoded(param1String));
    }
    
    public Builder encodedPath(String param1String) {
      return path(Uri.PathPart.fromEncoded(param1String));
    }
    
    public Builder appendPath(String param1String) {
      return path(Uri.PathPart.appendDecodedSegment(this.path, param1String));
    }
    
    public Builder appendEncodedPath(String param1String) {
      return path(Uri.PathPart.appendEncodedSegment(this.path, param1String));
    }
    
    Builder query(Uri.Part param1Part) {
      this.opaquePart = null;
      this.query = param1Part;
      return this;
    }
    
    public Builder query(String param1String) {
      return query(Uri.Part.fromDecoded(param1String));
    }
    
    public Builder encodedQuery(String param1String) {
      return query(Uri.Part.fromEncoded(param1String));
    }
    
    Builder fragment(Uri.Part param1Part) {
      this.fragment = param1Part;
      return this;
    }
    
    public Builder fragment(String param1String) {
      return fragment(Uri.Part.fromDecoded(param1String));
    }
    
    public Builder encodedFragment(String param1String) {
      return fragment(Uri.Part.fromEncoded(param1String));
    }
    
    public Builder appendQueryParameter(String param1String1, String param1String2) {
      this.opaquePart = null;
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append(Uri.encode(param1String1, null));
      stringBuilder2.append("=");
      stringBuilder2.append(Uri.encode(param1String2, null));
      param1String1 = stringBuilder2.toString();
      Uri.Part part = this.query;
      if (part == null) {
        this.query = Uri.Part.fromEncoded(param1String1);
        return this;
      } 
      String str = part.getEncoded();
      if (str == null || str.length() == 0) {
        this.query = Uri.Part.fromEncoded(param1String1);
        return this;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(str);
      stringBuilder1.append("&");
      stringBuilder1.append(param1String1);
      this.query = Uri.Part.fromEncoded(stringBuilder1.toString());
      return this;
    }
    
    public Builder clearQuery() {
      return query((Uri.Part)null);
    }
    
    public Uri build() {
      if (this.opaquePart != null) {
        if (this.scheme != null)
          return new Uri.OpaqueUri(this.opaquePart, this.fragment); 
        throw new UnsupportedOperationException("An opaque URI must have a scheme.");
      } 
      Uri.PathPart pathPart1 = this.path;
      if (pathPart1 == null || pathPart1 == Uri.PathPart.NULL) {
        Uri.PathPart pathPart = Uri.PathPart.EMPTY;
        return new Uri.HierarchicalUri(this.authority, pathPart, this.query, this.fragment);
      } 
      Uri.PathPart pathPart2 = pathPart1;
      if (hasSchemeOrAuthority())
        pathPart2 = Uri.PathPart.makeAbsolute(pathPart1); 
      return new Uri.HierarchicalUri(this.authority, pathPart2, this.query, this.fragment);
    }
    
    private boolean hasSchemeOrAuthority() {
      if (this.scheme == null) {
        Uri.Part part = this.authority;
        return (part != null && part != Uri.Part.NULL);
      } 
      return true;
    }
    
    public String toString() {
      return build().toString();
    }
  }
  
  public Set<String> getQueryParameterNames() {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual isOpaque : ()Z
    //   4: ifne -> 125
    //   7: aload_0
    //   8: invokevirtual getEncodedQuery : ()Ljava/lang/String;
    //   11: astore_1
    //   12: aload_1
    //   13: ifnonnull -> 20
    //   16: invokestatic emptySet : ()Ljava/util/Set;
    //   19: areturn
    //   20: new java/util/LinkedHashSet
    //   23: dup
    //   24: invokespecial <init> : ()V
    //   27: astore_2
    //   28: iconst_0
    //   29: istore_3
    //   30: aload_1
    //   31: bipush #38
    //   33: iload_3
    //   34: invokevirtual indexOf : (II)I
    //   37: istore #4
    //   39: iload #4
    //   41: iconst_m1
    //   42: if_icmpne -> 54
    //   45: aload_1
    //   46: invokevirtual length : ()I
    //   49: istore #4
    //   51: goto -> 54
    //   54: aload_1
    //   55: bipush #61
    //   57: iload_3
    //   58: invokevirtual indexOf : (II)I
    //   61: istore #5
    //   63: iload #5
    //   65: iload #4
    //   67: if_icmpgt -> 80
    //   70: iload #5
    //   72: istore #6
    //   74: iload #5
    //   76: iconst_m1
    //   77: if_icmpne -> 84
    //   80: iload #4
    //   82: istore #6
    //   84: aload_1
    //   85: iload_3
    //   86: iload #6
    //   88: invokevirtual substring : (II)Ljava/lang/String;
    //   91: astore #7
    //   93: aload_2
    //   94: aload #7
    //   96: invokestatic decode : (Ljava/lang/String;)Ljava/lang/String;
    //   99: invokeinterface add : (Ljava/lang/Object;)Z
    //   104: pop
    //   105: iinc #4, 1
    //   108: iload #4
    //   110: istore_3
    //   111: iload #4
    //   113: aload_1
    //   114: invokevirtual length : ()I
    //   117: if_icmplt -> 30
    //   120: aload_2
    //   121: invokestatic unmodifiableSet : (Ljava/util/Set;)Ljava/util/Set;
    //   124: areturn
    //   125: new java/lang/UnsupportedOperationException
    //   128: dup
    //   129: ldc 'This isn't a hierarchical URI.'
    //   131: invokespecial <init> : (Ljava/lang/String;)V
    //   134: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1604	-> 0
    //   #1608	-> 7
    //   #1609	-> 12
    //   #1610	-> 16
    //   #1613	-> 20
    //   #1614	-> 28
    //   #1616	-> 30
    //   #1617	-> 39
    //   #1619	-> 54
    //   #1620	-> 63
    //   #1621	-> 80
    //   #1624	-> 84
    //   #1625	-> 93
    //   #1628	-> 105
    //   #1629	-> 108
    //   #1631	-> 120
    //   #1605	-> 125
  }
  
  public List<String> getQueryParameters(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual isOpaque : ()Z
    //   4: ifne -> 206
    //   7: aload_1
    //   8: ifnull -> 195
    //   11: aload_0
    //   12: invokevirtual getEncodedQuery : ()Ljava/lang/String;
    //   15: astore_2
    //   16: aload_2
    //   17: ifnonnull -> 24
    //   20: invokestatic emptyList : ()Ljava/util/List;
    //   23: areturn
    //   24: aload_1
    //   25: ldc 'UTF-8'
    //   27: invokestatic encode : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   30: astore_1
    //   31: new java/util/ArrayList
    //   34: dup
    //   35: invokespecial <init> : ()V
    //   38: astore_3
    //   39: iconst_0
    //   40: istore #4
    //   42: aload_2
    //   43: bipush #38
    //   45: iload #4
    //   47: invokevirtual indexOf : (II)I
    //   50: istore #5
    //   52: iload #5
    //   54: iconst_m1
    //   55: if_icmpeq -> 65
    //   58: iload #5
    //   60: istore #6
    //   62: goto -> 71
    //   65: aload_2
    //   66: invokevirtual length : ()I
    //   69: istore #6
    //   71: aload_2
    //   72: bipush #61
    //   74: iload #4
    //   76: invokevirtual indexOf : (II)I
    //   79: istore #7
    //   81: iload #7
    //   83: iload #6
    //   85: if_icmpgt -> 98
    //   88: iload #7
    //   90: istore #8
    //   92: iload #7
    //   94: iconst_m1
    //   95: if_icmpne -> 102
    //   98: iload #6
    //   100: istore #8
    //   102: iload #8
    //   104: iload #4
    //   106: isub
    //   107: aload_1
    //   108: invokevirtual length : ()I
    //   111: if_icmpne -> 165
    //   114: aload_2
    //   115: iload #4
    //   117: aload_1
    //   118: iconst_0
    //   119: aload_1
    //   120: invokevirtual length : ()I
    //   123: invokevirtual regionMatches : (ILjava/lang/String;II)Z
    //   126: ifeq -> 165
    //   129: iload #8
    //   131: iload #6
    //   133: if_icmpne -> 147
    //   136: aload_3
    //   137: ldc_w ''
    //   140: invokevirtual add : (Ljava/lang/Object;)Z
    //   143: pop
    //   144: goto -> 165
    //   147: aload_3
    //   148: aload_2
    //   149: iload #8
    //   151: iconst_1
    //   152: iadd
    //   153: iload #6
    //   155: invokevirtual substring : (II)Ljava/lang/String;
    //   158: invokestatic decode : (Ljava/lang/String;)Ljava/lang/String;
    //   161: invokevirtual add : (Ljava/lang/Object;)Z
    //   164: pop
    //   165: iload #5
    //   167: iconst_m1
    //   168: if_icmpeq -> 180
    //   171: iload #5
    //   173: iconst_1
    //   174: iadd
    //   175: istore #4
    //   177: goto -> 42
    //   180: aload_3
    //   181: invokestatic unmodifiableList : (Ljava/util/List;)Ljava/util/List;
    //   184: areturn
    //   185: astore_1
    //   186: new java/lang/AssertionError
    //   189: dup
    //   190: aload_1
    //   191: invokespecial <init> : (Ljava/lang/Object;)V
    //   194: athrow
    //   195: new java/lang/NullPointerException
    //   198: dup
    //   199: ldc_w 'key'
    //   202: invokespecial <init> : (Ljava/lang/String;)V
    //   205: athrow
    //   206: new java/lang/UnsupportedOperationException
    //   209: dup
    //   210: ldc 'This isn't a hierarchical URI.'
    //   212: invokespecial <init> : (Ljava/lang/String;)V
    //   215: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1644	-> 0
    //   #1647	-> 7
    //   #1651	-> 11
    //   #1652	-> 16
    //   #1653	-> 20
    //   #1658	-> 24
    //   #1661	-> 31
    //   #1663	-> 31
    //   #1665	-> 39
    //   #1667	-> 42
    //   #1668	-> 52
    //   #1670	-> 71
    //   #1671	-> 81
    //   #1672	-> 98
    //   #1675	-> 102
    //   #1676	-> 114
    //   #1677	-> 129
    //   #1678	-> 136
    //   #1680	-> 147
    //   #1685	-> 165
    //   #1686	-> 171
    //   #1690	-> 177
    //   #1692	-> 180
    //   #1659	-> 185
    //   #1660	-> 186
    //   #1648	-> 195
    //   #1645	-> 206
    // Exception table:
    //   from	to	target	type
    //   24	31	185	java/io/UnsupportedEncodingException
  }
  
  public String getQueryParameter(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual isOpaque : ()Z
    //   4: ifne -> 180
    //   7: aload_1
    //   8: ifnull -> 169
    //   11: aload_0
    //   12: invokevirtual getEncodedQuery : ()Ljava/lang/String;
    //   15: astore_2
    //   16: aload_2
    //   17: ifnonnull -> 22
    //   20: aconst_null
    //   21: areturn
    //   22: aload_1
    //   23: aconst_null
    //   24: invokestatic encode : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   27: astore_1
    //   28: aload_2
    //   29: invokevirtual length : ()I
    //   32: istore_3
    //   33: iconst_0
    //   34: istore #4
    //   36: aload_2
    //   37: bipush #38
    //   39: iload #4
    //   41: invokevirtual indexOf : (II)I
    //   44: istore #5
    //   46: iload #5
    //   48: iconst_m1
    //   49: if_icmpeq -> 59
    //   52: iload #5
    //   54: istore #6
    //   56: goto -> 62
    //   59: iload_3
    //   60: istore #6
    //   62: aload_2
    //   63: bipush #61
    //   65: iload #4
    //   67: invokevirtual indexOf : (II)I
    //   70: istore #7
    //   72: iload #7
    //   74: iload #6
    //   76: if_icmpgt -> 89
    //   79: iload #7
    //   81: istore #8
    //   83: iload #7
    //   85: iconst_m1
    //   86: if_icmpne -> 93
    //   89: iload #6
    //   91: istore #8
    //   93: iload #8
    //   95: iload #4
    //   97: isub
    //   98: aload_1
    //   99: invokevirtual length : ()I
    //   102: if_icmpne -> 152
    //   105: aload_2
    //   106: iload #4
    //   108: aload_1
    //   109: iconst_0
    //   110: aload_1
    //   111: invokevirtual length : ()I
    //   114: invokevirtual regionMatches : (ILjava/lang/String;II)Z
    //   117: ifeq -> 152
    //   120: iload #8
    //   122: iload #6
    //   124: if_icmpne -> 131
    //   127: ldc_w ''
    //   130: areturn
    //   131: aload_2
    //   132: iload #8
    //   134: iconst_1
    //   135: iadd
    //   136: iload #6
    //   138: invokevirtual substring : (II)Ljava/lang/String;
    //   141: astore_1
    //   142: aload_1
    //   143: iconst_1
    //   144: getstatic java/nio/charset/StandardCharsets.UTF_8 : Ljava/nio/charset/Charset;
    //   147: iconst_0
    //   148: invokestatic decode : (Ljava/lang/String;ZLjava/nio/charset/Charset;Z)Ljava/lang/String;
    //   151: areturn
    //   152: iload #5
    //   154: iconst_m1
    //   155: if_icmpeq -> 167
    //   158: iload #5
    //   160: iconst_1
    //   161: iadd
    //   162: istore #4
    //   164: goto -> 36
    //   167: aconst_null
    //   168: areturn
    //   169: new java/lang/NullPointerException
    //   172: dup
    //   173: ldc_w 'key'
    //   176: invokespecial <init> : (Ljava/lang/String;)V
    //   179: athrow
    //   180: new java/lang/UnsupportedOperationException
    //   183: dup
    //   184: ldc 'This isn't a hierarchical URI.'
    //   186: invokespecial <init> : (Ljava/lang/String;)V
    //   189: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1708	-> 0
    //   #1711	-> 7
    //   #1715	-> 11
    //   #1716	-> 16
    //   #1717	-> 20
    //   #1720	-> 22
    //   #1721	-> 28
    //   #1722	-> 33
    //   #1724	-> 36
    //   #1725	-> 46
    //   #1727	-> 62
    //   #1728	-> 72
    //   #1729	-> 89
    //   #1732	-> 93
    //   #1733	-> 105
    //   #1734	-> 120
    //   #1735	-> 127
    //   #1737	-> 131
    //   #1738	-> 142
    //   #1743	-> 152
    //   #1744	-> 158
    //   #1748	-> 164
    //   #1749	-> 167
    //   #1712	-> 169
    //   #1709	-> 180
  }
  
  public boolean getBooleanQueryParameter(String paramString, boolean paramBoolean) {
    paramString = getQueryParameter(paramString);
    if (paramString == null)
      return paramBoolean; 
    paramString = paramString.toLowerCase(Locale.ROOT);
    if (!"false".equals(paramString) && !"0".equals(paramString)) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    } 
    return paramBoolean;
  }
  
  public Uri normalizeScheme() {
    String str1 = getScheme();
    if (str1 == null)
      return this; 
    String str2 = str1.toLowerCase(Locale.ROOT);
    if (str1.equals(str2))
      return this; 
    return buildUpon().scheme(str2).build();
  }
  
  public static void writeToParcel(Parcel paramParcel, Uri paramUri) {
    if (paramUri == null) {
      paramParcel.writeInt(0);
    } else {
      paramUri.writeToParcel(paramParcel, 0);
    } 
  }
  
  public static String encode(String paramString) {
    return encode(paramString, null);
  }
  
  public static String encode(String paramString1, String paramString2) {
    if (paramString1 == null)
      return null; 
    StringBuilder stringBuilder = null;
    int i = paramString1.length();
    int j = 0;
    while (j < i) {
      int k = j;
      while (k < i && 
        isAllowed(paramString1.charAt(k), paramString2))
        k++; 
      if (k == i) {
        if (j == 0)
          return paramString1; 
        stringBuilder.append(paramString1, j, i);
        return stringBuilder.toString();
      } 
      StringBuilder stringBuilder1 = stringBuilder;
      if (stringBuilder == null)
        stringBuilder1 = new StringBuilder(); 
      if (k > j)
        stringBuilder1.append(paramString1, j, k); 
      j = k + 1;
      while (j < i && 
        !isAllowed(paramString1.charAt(j), paramString2))
        j++; 
      String str = paramString1.substring(k, j);
      try {
        byte[] arrayOfByte = str.getBytes("UTF-8");
        int m = arrayOfByte.length;
        for (k = 0; k < m; k++) {
          stringBuilder1.append('%');
          stringBuilder1.append(HEX_DIGITS[(arrayOfByte[k] & 0xF0) >> 4]);
          stringBuilder1.append(HEX_DIGITS[arrayOfByte[k] & 0xF]);
        } 
        stringBuilder = stringBuilder1;
      } catch (UnsupportedEncodingException unsupportedEncodingException) {
        throw new AssertionError(unsupportedEncodingException);
      } 
    } 
    if (stringBuilder != null)
      paramString1 = stringBuilder.toString(); 
    return paramString1;
  }
  
  private static boolean isAllowed(char paramChar, String paramString) {
    return ((paramChar < 'A' || paramChar > 'Z') && (paramChar < 'a' || paramChar > 'z') && (paramChar < '0' || paramChar > '9')) ? (

      
      ("_-!.~'()*".indexOf(paramChar) != -1 || (paramString != null && 
      paramString.indexOf(paramChar) != -1))) : true;
  }
  
  public static String decode(String paramString) {
    if (paramString == null)
      return null; 
    return UriCodec.decode(paramString, false, StandardCharsets.UTF_8, false);
  }
  
  class AbstractPart {
    static final int REPRESENTATION_DECODED = 2;
    
    static final int REPRESENTATION_ENCODED = 1;
    
    volatile String decoded;
    
    volatile String encoded;
    
    private final int mCanonicalRepresentation;
    
    AbstractPart(Uri this$0, String param1String1) {
      if (this$0 != Uri.NOT_CACHED) {
        this.mCanonicalRepresentation = 1;
        this.encoded = (String)this$0;
        this.decoded = Uri.NOT_CACHED;
      } else {
        if (param1String1 != Uri.NOT_CACHED) {
          this.mCanonicalRepresentation = 2;
          this.encoded = Uri.NOT_CACHED;
          this.decoded = param1String1;
          return;
        } 
        throw new IllegalArgumentException("Neither encoded nor decoded");
      } 
    }
    
    final String getDecoded() {
      boolean bool;
      String str;
      if (this.decoded != Uri.NOT_CACHED) {
        bool = true;
      } else {
        bool = false;
      } 
      if (bool) {
        str = this.decoded;
      } else {
        this.decoded = str = Uri.decode(this.encoded);
      } 
      return str;
    }
    
    abstract String getEncoded();
    
    final void writeTo(Parcel param1Parcel) {
      String str;
      int i = this.mCanonicalRepresentation;
      if (i == 1) {
        str = this.encoded;
      } else if (i == 2) {
        str = this.decoded;
      } else {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown representation: ");
        stringBuilder.append(this.mCanonicalRepresentation);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      if (str != Uri.NOT_CACHED) {
        stringBuilder.writeInt(this.mCanonicalRepresentation);
        stringBuilder.writeString8(str);
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Canonical value not cached (");
      stringBuilder.append(this.mCanonicalRepresentation);
      stringBuilder.append(")");
      throw new AssertionError(stringBuilder.toString());
    }
  }
  
  static class Part extends AbstractPart {
    static final Part EMPTY = new EmptyPart("");
    
    static final Part NULL = new EmptyPart(null);
    
    static {
    
    }
    
    private Part(String param1String1, String param1String2) {
      super(param1String1, param1String2);
    }
    
    boolean isEmpty() {
      return false;
    }
    
    String getEncoded() {
      boolean bool;
      String str;
      if (this.encoded != Uri.NOT_CACHED) {
        bool = true;
      } else {
        bool = false;
      } 
      if (bool) {
        str = this.encoded;
      } else {
        this.encoded = str = Uri.encode(this.decoded);
      } 
      return str;
    }
    
    static Part readFrom(Parcel param1Parcel) {
      StringBuilder stringBuilder;
      int i = param1Parcel.readInt();
      String str = param1Parcel.readString8();
      if (i != 1) {
        if (i == 2)
          return fromDecoded(str); 
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown representation: ");
        stringBuilder.append(i);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      return fromEncoded((String)stringBuilder);
    }
    
    static Part nonNull(Part param1Part) {
      if (param1Part == null)
        param1Part = NULL; 
      return param1Part;
    }
    
    static Part fromEncoded(String param1String) {
      return from(param1String, Uri.NOT_CACHED);
    }
    
    static Part fromDecoded(String param1String) {
      return from(Uri.NOT_CACHED, param1String);
    }
    
    static Part from(String param1String1, String param1String2) {
      if (param1String1 == null)
        return NULL; 
      if (param1String1.length() == 0)
        return EMPTY; 
      if (param1String2 == null)
        return NULL; 
      if (param1String2.length() == 0)
        return EMPTY; 
      return new Part(param1String1, param1String2);
    }
    
    class EmptyPart extends Part {
      public EmptyPart(Uri.Part this$0) {
        super((String)this$0, (String)this$0);
        if (this$0 == null || this$0.isEmpty()) {
          this.decoded = (String)this$0;
          this.encoded = (String)this$0;
          return;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Expected empty value, got: ");
        stringBuilder.append((String)this$0);
        throw new IllegalArgumentException(stringBuilder.toString());
      }
      
      boolean isEmpty() {
        return true;
      }
    }
  }
  
  class EmptyPart extends Part {
    public EmptyPart(Uri this$0) {
      super((String)this$0, (String)this$0);
      if (this$0 == null || this$0.isEmpty()) {
        this.decoded = (String)this$0;
        this.encoded = (String)this$0;
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Expected empty value, got: ");
      stringBuilder.append((String)this$0);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    boolean isEmpty() {
      return true;
    }
  }
  
  static class PathPart extends AbstractPart {
    static final PathPart EMPTY = new PathPart("", "");
    
    static final PathPart NULL = new PathPart(null, null);
    
    private Uri.PathSegments pathSegments;
    
    static {
    
    }
    
    private PathPart(String param1String1, String param1String2) {
      super(param1String1, param1String2);
    }
    
    String getEncoded() {
      boolean bool;
      String str;
      if (this.encoded != Uri.NOT_CACHED) {
        bool = true;
      } else {
        bool = false;
      } 
      if (bool) {
        str = this.encoded;
      } else {
        this.encoded = str = Uri.encode(this.decoded, "/");
      } 
      return str;
    }
    
    Uri.PathSegments getPathSegments() {
      Uri.PathSegments pathSegments2 = this.pathSegments;
      if (pathSegments2 != null)
        return pathSegments2; 
      String str = getEncoded();
      if (str == null) {
        this.pathSegments = pathSegments1 = Uri.PathSegments.EMPTY;
        return pathSegments1;
      } 
      Uri.PathSegmentsBuilder pathSegmentsBuilder = new Uri.PathSegmentsBuilder();
      int i = 0;
      while (true) {
        int j = pathSegments1.indexOf('/', i);
        if (j > -1) {
          if (i < j) {
            String str1 = Uri.decode(pathSegments1.substring(i, j));
            pathSegmentsBuilder.add(str1);
          } 
          i = j + 1;
          continue;
        } 
        break;
      } 
      if (i < pathSegments1.length())
        pathSegmentsBuilder.add(Uri.decode(pathSegments1.substring(i))); 
      Uri.PathSegments pathSegments1 = pathSegmentsBuilder.build();
      return pathSegments1;
    }
    
    static PathPart appendEncodedSegment(PathPart param1PathPart, String param1String) {
      StringBuilder stringBuilder;
      if (param1PathPart == null) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("/");
        stringBuilder.append(param1String);
        return fromEncoded(stringBuilder.toString());
      } 
      String str2 = stringBuilder.getEncoded();
      String str1 = str2;
      if (str2 == null)
        str1 = ""; 
      int i = str1.length();
      if (i == 0) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("/");
        stringBuilder1.append(param1String);
        str1 = stringBuilder1.toString();
      } else if (str1.charAt(i - 1) == '/') {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(str1);
        stringBuilder1.append(param1String);
        str1 = stringBuilder1.toString();
      } else {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(str1);
        stringBuilder1.append("/");
        stringBuilder1.append(param1String);
        str1 = stringBuilder1.toString();
      } 
      return fromEncoded(str1);
    }
    
    static PathPart appendDecodedSegment(PathPart param1PathPart, String param1String) {
      param1String = Uri.encode(param1String);
      return appendEncodedSegment(param1PathPart, param1String);
    }
    
    static PathPart readFrom(Parcel param1Parcel) {
      StringBuilder stringBuilder;
      int i = param1Parcel.readInt();
      if (i != 1) {
        if (i == 2)
          return fromDecoded(param1Parcel.readString8()); 
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown representation: ");
        stringBuilder.append(i);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      return fromEncoded(stringBuilder.readString8());
    }
    
    static PathPart fromEncoded(String param1String) {
      return from(param1String, Uri.NOT_CACHED);
    }
    
    static PathPart fromDecoded(String param1String) {
      return from(Uri.NOT_CACHED, param1String);
    }
    
    static PathPart from(String param1String1, String param1String2) {
      if (param1String1 == null)
        return NULL; 
      if (param1String1.length() == 0)
        return EMPTY; 
      return new PathPart(param1String1, param1String2);
    }
    
    static PathPart makeAbsolute(PathPart param1PathPart) {
      String str1;
      boolean bool2;
      String str2 = param1PathPart.encoded, str3 = Uri.NOT_CACHED;
      boolean bool1 = true;
      if (str2 != str3) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (bool2) {
        str2 = param1PathPart.encoded;
      } else {
        str2 = param1PathPart.decoded;
      } 
      if (str2 == null || str2.length() == 0 || 
        str2.startsWith("/"))
        return param1PathPart; 
      if (bool2) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("/");
        stringBuilder.append(param1PathPart.encoded);
        String str = stringBuilder.toString();
      } else {
        str2 = Uri.NOT_CACHED;
      } 
      if (param1PathPart.decoded != Uri.NOT_CACHED) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      if (bool2) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("/");
        stringBuilder.append(param1PathPart.decoded);
        str1 = stringBuilder.toString();
      } else {
        str1 = Uri.NOT_CACHED;
      } 
      return new PathPart(str2, str1);
    }
  }
  
  public static Uri withAppendedPath(Uri paramUri, String paramString) {
    Builder builder = paramUri.buildUpon();
    builder = builder.appendEncodedPath(paramString);
    return builder.build();
  }
  
  public Uri getCanonicalUri() {
    if ("file".equals(getScheme()))
      try {
        File file1, file2 = new File();
        this(getPath());
        String str = file2.getCanonicalPath();
        if (Environment.isExternalStorageEmulated()) {
          File file = Environment.getLegacyExternalStorageDirectory();
          String str1 = file.toString();
          if (str.startsWith(str1)) {
            String str2 = Environment.getExternalStorageDirectory().toString();
            file1 = new File(str2, str.substring(str1.length() + 1));
            return fromFile(file1);
          } 
        } 
        return fromFile(new File((String)file1));
      } catch (IOException iOException) {
        return this;
      }  
    return this;
  }
  
  public void checkFileUriExposed(String paramString) {
    if ("file".equals(getScheme()) && 
      getPath() != null && !getPath().startsWith("/system/"))
      StrictMode.onFileUriExposed(this, paramString); 
  }
  
  public void checkContentUriWithoutPermission(String paramString, int paramInt) {
    if ("content".equals(getScheme()) && !Intent.isAccessUriMode(paramInt))
      StrictMode.onContentUriWithoutPermission(this, paramString); 
  }
  
  public boolean isPathPrefixMatch(Uri paramUri) {
    if (!Objects.equals(getScheme(), paramUri.getScheme()))
      return false; 
    if (!Objects.equals(getAuthority(), paramUri.getAuthority()))
      return false; 
    List<String> list2 = getPathSegments();
    List<String> list1 = paramUri.getPathSegments();
    int i = list1.size();
    if (list2.size() < i)
      return false; 
    for (byte b = 0; b < i; b++) {
      if (!Objects.equals(list2.get(b), list1.get(b)))
        return false; 
    } 
    return true;
  }
  
  public abstract Builder buildUpon();
  
  public abstract String getAuthority();
  
  public abstract String getEncodedAuthority();
  
  public abstract String getEncodedFragment();
  
  public abstract String getEncodedPath();
  
  public abstract String getEncodedQuery();
  
  public abstract String getEncodedSchemeSpecificPart();
  
  public abstract String getEncodedUserInfo();
  
  public abstract String getFragment();
  
  public abstract String getHost();
  
  public abstract String getLastPathSegment();
  
  public abstract String getPath();
  
  public abstract List<String> getPathSegments();
  
  public abstract int getPort();
  
  public abstract String getQuery();
  
  public abstract String getScheme();
  
  public abstract String getSchemeSpecificPart();
  
  public abstract String getUserInfo();
  
  public abstract boolean isHierarchical();
  
  public abstract boolean isRelative();
  
  public abstract String toString();
}
