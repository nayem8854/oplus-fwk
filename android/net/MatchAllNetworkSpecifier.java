package android.net;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class MatchAllNetworkSpecifier extends NetworkSpecifier implements Parcelable {
  public static void checkNotMatchAllNetworkSpecifier(NetworkSpecifier paramNetworkSpecifier) {
    if (!(paramNetworkSpecifier instanceof MatchAllNetworkSpecifier))
      return; 
    throw new IllegalArgumentException("A MatchAllNetworkSpecifier is not permitted");
  }
  
  public boolean satisfiedBy(NetworkSpecifier paramNetworkSpecifier) {
    throw new IllegalStateException("MatchAllNetworkSpecifier must not be used in NetworkRequests");
  }
  
  public boolean equals(Object paramObject) {
    return paramObject instanceof MatchAllNetworkSpecifier;
  }
  
  public int hashCode() {
    return 0;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {}
  
  public static final Parcelable.Creator<MatchAllNetworkSpecifier> CREATOR = new Parcelable.Creator<MatchAllNetworkSpecifier>() {
      public MatchAllNetworkSpecifier createFromParcel(Parcel param1Parcel) {
        return new MatchAllNetworkSpecifier();
      }
      
      public MatchAllNetworkSpecifier[] newArray(int param1Int) {
        return new MatchAllNetworkSpecifier[param1Int];
      }
    };
}
