package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Collection;

public final class UidRange implements Parcelable {
  public UidRange(int paramInt1, int paramInt2) {
    if (paramInt1 >= 0) {
      if (paramInt2 >= 0) {
        if (paramInt1 <= paramInt2) {
          this.start = paramInt1;
          this.stop = paramInt2;
          return;
        } 
        throw new IllegalArgumentException("Invalid UID range.");
      } 
      throw new IllegalArgumentException("Invalid stop UID.");
    } 
    throw new IllegalArgumentException("Invalid start UID.");
  }
  
  public static UidRange createForUser(int paramInt) {
    return new UidRange(paramInt * 100000, (paramInt + 1) * 100000 - 1);
  }
  
  public int getStartUser() {
    return this.start / 100000;
  }
  
  public int getEndUser() {
    return this.stop / 100000;
  }
  
  public boolean contains(int paramInt) {
    boolean bool;
    if (this.start <= paramInt && paramInt <= this.stop) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int count() {
    return this.stop + 1 - this.start;
  }
  
  public boolean containsRange(UidRange paramUidRange) {
    boolean bool;
    if (this.start <= paramUidRange.start && paramUidRange.stop <= this.stop) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int hashCode() {
    int i = this.start;
    int j = this.stop;
    return (17 * 31 + i) * 31 + j;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject instanceof UidRange) {
      paramObject = paramObject;
      if (this.start != ((UidRange)paramObject).start || this.stop != ((UidRange)paramObject).stop)
        bool = false; 
      return bool;
    } 
    return false;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.start);
    stringBuilder.append("-");
    stringBuilder.append(this.stop);
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.start);
    paramParcel.writeInt(this.stop);
  }
  
  public static final Parcelable.Creator<UidRange> CREATOR = new Parcelable.Creator<UidRange>() {
      public UidRange createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        return new UidRange(i, j);
      }
      
      public UidRange[] newArray(int param1Int) {
        return new UidRange[param1Int];
      }
    };
  
  public final int start;
  
  public final int stop;
  
  public static boolean containsUid(Collection<UidRange> paramCollection, int paramInt) {
    if (paramCollection == null)
      return false; 
    for (UidRange uidRange : paramCollection) {
      if (uidRange.contains(paramInt))
        return true; 
    } 
    return false;
  }
}
