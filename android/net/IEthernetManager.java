package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IEthernetManager extends IInterface {
  void addListener(IEthernetServiceListener paramIEthernetServiceListener) throws RemoteException;
  
  String[] getAvailableInterfaces() throws RemoteException;
  
  IpConfiguration getConfiguration(String paramString) throws RemoteException;
  
  boolean isAvailable(String paramString) throws RemoteException;
  
  void releaseTetheredInterface(ITetheredInterfaceCallback paramITetheredInterfaceCallback) throws RemoteException;
  
  void removeListener(IEthernetServiceListener paramIEthernetServiceListener) throws RemoteException;
  
  void requestTetheredInterface(ITetheredInterfaceCallback paramITetheredInterfaceCallback) throws RemoteException;
  
  void setConfiguration(String paramString, IpConfiguration paramIpConfiguration) throws RemoteException;
  
  void setIncludeTestInterfaces(boolean paramBoolean) throws RemoteException;
  
  class Default implements IEthernetManager {
    public String[] getAvailableInterfaces() throws RemoteException {
      return null;
    }
    
    public IpConfiguration getConfiguration(String param1String) throws RemoteException {
      return null;
    }
    
    public void setConfiguration(String param1String, IpConfiguration param1IpConfiguration) throws RemoteException {}
    
    public boolean isAvailable(String param1String) throws RemoteException {
      return false;
    }
    
    public void addListener(IEthernetServiceListener param1IEthernetServiceListener) throws RemoteException {}
    
    public void removeListener(IEthernetServiceListener param1IEthernetServiceListener) throws RemoteException {}
    
    public void setIncludeTestInterfaces(boolean param1Boolean) throws RemoteException {}
    
    public void requestTetheredInterface(ITetheredInterfaceCallback param1ITetheredInterfaceCallback) throws RemoteException {}
    
    public void releaseTetheredInterface(ITetheredInterfaceCallback param1ITetheredInterfaceCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IEthernetManager {
    private static final String DESCRIPTOR = "android.net.IEthernetManager";
    
    static final int TRANSACTION_addListener = 5;
    
    static final int TRANSACTION_getAvailableInterfaces = 1;
    
    static final int TRANSACTION_getConfiguration = 2;
    
    static final int TRANSACTION_isAvailable = 4;
    
    static final int TRANSACTION_releaseTetheredInterface = 9;
    
    static final int TRANSACTION_removeListener = 6;
    
    static final int TRANSACTION_requestTetheredInterface = 8;
    
    static final int TRANSACTION_setConfiguration = 3;
    
    static final int TRANSACTION_setIncludeTestInterfaces = 7;
    
    public Stub() {
      attachInterface(this, "android.net.IEthernetManager");
    }
    
    public static IEthernetManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.IEthernetManager");
      if (iInterface != null && iInterface instanceof IEthernetManager)
        return (IEthernetManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 9:
          return "releaseTetheredInterface";
        case 8:
          return "requestTetheredInterface";
        case 7:
          return "setIncludeTestInterfaces";
        case 6:
          return "removeListener";
        case 5:
          return "addListener";
        case 4:
          return "isAvailable";
        case 3:
          return "setConfiguration";
        case 2:
          return "getConfiguration";
        case 1:
          break;
      } 
      return "getAvailableInterfaces";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        ITetheredInterfaceCallback iTetheredInterfaceCallback;
        IEthernetServiceListener iEthernetServiceListener;
        String str1;
        IpConfiguration ipConfiguration;
        String str2;
        boolean bool1 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 9:
            param1Parcel1.enforceInterface("android.net.IEthernetManager");
            iTetheredInterfaceCallback = ITetheredInterfaceCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            releaseTetheredInterface(iTetheredInterfaceCallback);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            iTetheredInterfaceCallback.enforceInterface("android.net.IEthernetManager");
            iTetheredInterfaceCallback = ITetheredInterfaceCallback.Stub.asInterface(iTetheredInterfaceCallback.readStrongBinder());
            requestTetheredInterface(iTetheredInterfaceCallback);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            iTetheredInterfaceCallback.enforceInterface("android.net.IEthernetManager");
            if (iTetheredInterfaceCallback.readInt() != 0)
              bool1 = true; 
            setIncludeTestInterfaces(bool1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            iTetheredInterfaceCallback.enforceInterface("android.net.IEthernetManager");
            iEthernetServiceListener = IEthernetServiceListener.Stub.asInterface(iTetheredInterfaceCallback.readStrongBinder());
            removeListener(iEthernetServiceListener);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            iEthernetServiceListener.enforceInterface("android.net.IEthernetManager");
            iEthernetServiceListener = IEthernetServiceListener.Stub.asInterface(iEthernetServiceListener.readStrongBinder());
            addListener(iEthernetServiceListener);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            iEthernetServiceListener.enforceInterface("android.net.IEthernetManager");
            str1 = iEthernetServiceListener.readString();
            bool = isAvailable(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 3:
            str1.enforceInterface("android.net.IEthernetManager");
            str2 = str1.readString();
            if (str1.readInt() != 0) {
              IpConfiguration ipConfiguration1 = IpConfiguration.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            setConfiguration(str2, (IpConfiguration)str1);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str1.enforceInterface("android.net.IEthernetManager");
            str1 = str1.readString();
            ipConfiguration = getConfiguration(str1);
            param1Parcel2.writeNoException();
            if (ipConfiguration != null) {
              param1Parcel2.writeInt(1);
              ipConfiguration.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        ipConfiguration.enforceInterface("android.net.IEthernetManager");
        String[] arrayOfString = getAvailableInterfaces();
        param1Parcel2.writeNoException();
        param1Parcel2.writeStringArray(arrayOfString);
        return true;
      } 
      param1Parcel2.writeString("android.net.IEthernetManager");
      return true;
    }
    
    private static class Proxy implements IEthernetManager {
      public static IEthernetManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.IEthernetManager";
      }
      
      public String[] getAvailableInterfaces() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IEthernetManager");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IEthernetManager.Stub.getDefaultImpl() != null)
            return IEthernetManager.Stub.getDefaultImpl().getAvailableInterfaces(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IpConfiguration getConfiguration(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IEthernetManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IEthernetManager.Stub.getDefaultImpl() != null)
            return IEthernetManager.Stub.getDefaultImpl().getConfiguration(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            IpConfiguration ipConfiguration = IpConfiguration.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (IpConfiguration)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setConfiguration(String param2String, IpConfiguration param2IpConfiguration) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IEthernetManager");
          parcel1.writeString(param2String);
          if (param2IpConfiguration != null) {
            parcel1.writeInt(1);
            param2IpConfiguration.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IEthernetManager.Stub.getDefaultImpl() != null) {
            IEthernetManager.Stub.getDefaultImpl().setConfiguration(param2String, param2IpConfiguration);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAvailable(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IEthernetManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IEthernetManager.Stub.getDefaultImpl() != null) {
            bool1 = IEthernetManager.Stub.getDefaultImpl().isAvailable(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addListener(IEthernetServiceListener param2IEthernetServiceListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.net.IEthernetManager");
          if (param2IEthernetServiceListener != null) {
            iBinder = param2IEthernetServiceListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IEthernetManager.Stub.getDefaultImpl() != null) {
            IEthernetManager.Stub.getDefaultImpl().addListener(param2IEthernetServiceListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeListener(IEthernetServiceListener param2IEthernetServiceListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.net.IEthernetManager");
          if (param2IEthernetServiceListener != null) {
            iBinder = param2IEthernetServiceListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IEthernetManager.Stub.getDefaultImpl() != null) {
            IEthernetManager.Stub.getDefaultImpl().removeListener(param2IEthernetServiceListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setIncludeTestInterfaces(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.net.IEthernetManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool1 && IEthernetManager.Stub.getDefaultImpl() != null) {
            IEthernetManager.Stub.getDefaultImpl().setIncludeTestInterfaces(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestTetheredInterface(ITetheredInterfaceCallback param2ITetheredInterfaceCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.net.IEthernetManager");
          if (param2ITetheredInterfaceCallback != null) {
            iBinder = param2ITetheredInterfaceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IEthernetManager.Stub.getDefaultImpl() != null) {
            IEthernetManager.Stub.getDefaultImpl().requestTetheredInterface(param2ITetheredInterfaceCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void releaseTetheredInterface(ITetheredInterfaceCallback param2ITetheredInterfaceCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.net.IEthernetManager");
          if (param2ITetheredInterfaceCallback != null) {
            iBinder = param2ITetheredInterfaceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IEthernetManager.Stub.getDefaultImpl() != null) {
            IEthernetManager.Stub.getDefaultImpl().releaseTetheredInterface(param2ITetheredInterfaceCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IEthernetManager param1IEthernetManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IEthernetManager != null) {
          Proxy.sDefaultImpl = param1IEthernetManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IEthernetManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
