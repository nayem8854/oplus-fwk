package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IEthernetServiceListener extends IInterface {
  void onAvailabilityChanged(String paramString, boolean paramBoolean) throws RemoteException;
  
  class Default implements IEthernetServiceListener {
    public void onAvailabilityChanged(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IEthernetServiceListener {
    private static final String DESCRIPTOR = "android.net.IEthernetServiceListener";
    
    static final int TRANSACTION_onAvailabilityChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.net.IEthernetServiceListener");
    }
    
    public static IEthernetServiceListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.IEthernetServiceListener");
      if (iInterface != null && iInterface instanceof IEthernetServiceListener)
        return (IEthernetServiceListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onAvailabilityChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.net.IEthernetServiceListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.net.IEthernetServiceListener");
      String str = param1Parcel1.readString();
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      onAvailabilityChanged(str, bool);
      return true;
    }
    
    private static class Proxy implements IEthernetServiceListener {
      public static IEthernetServiceListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.IEthernetServiceListener";
      }
      
      public void onAvailabilityChanged(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.net.IEthernetServiceListener");
          parcel.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool1 && IEthernetServiceListener.Stub.getDefaultImpl() != null) {
            IEthernetServiceListener.Stub.getDefaultImpl().onAvailabilityChanged(param2String, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IEthernetServiceListener param1IEthernetServiceListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IEthernetServiceListener != null) {
          Proxy.sDefaultImpl = param1IEthernetServiceListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IEthernetServiceListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
