package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashSet;

public class InterfaceConfiguration implements Parcelable {
  private String mHwAddr;
  
  private HashSet<String> mFlags = new HashSet<>();
  
  private LinkAddress mAddr;
  
  private static final String FLAG_UP = "up";
  
  private static final String FLAG_DOWN = "down";
  
  private static final String[] EMPTY_STRING_ARRAY = new String[0];
  
  public static final Parcelable.Creator<InterfaceConfiguration> CREATOR;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("mHwAddr=");
    stringBuilder.append(this.mHwAddr);
    stringBuilder.append(" mAddr=");
    stringBuilder.append(String.valueOf(this.mAddr));
    stringBuilder.append(" mFlags=");
    stringBuilder.append(getFlags());
    return stringBuilder.toString();
  }
  
  public Iterable<String> getFlags() {
    return this.mFlags;
  }
  
  public boolean hasFlag(String paramString) {
    validateFlag(paramString);
    return this.mFlags.contains(paramString);
  }
  
  public void clearFlag(String paramString) {
    validateFlag(paramString);
    this.mFlags.remove(paramString);
  }
  
  public void setFlag(String paramString) {
    validateFlag(paramString);
    this.mFlags.add(paramString);
  }
  
  public void setInterfaceUp() {
    this.mFlags.remove("down");
    this.mFlags.add("up");
  }
  
  public void setInterfaceDown() {
    this.mFlags.remove("up");
    this.mFlags.add("down");
  }
  
  public void ignoreInterfaceUpDownStatus() {
    this.mFlags.remove("up");
    this.mFlags.remove("down");
  }
  
  public LinkAddress getLinkAddress() {
    return this.mAddr;
  }
  
  public void setLinkAddress(LinkAddress paramLinkAddress) {
    this.mAddr = paramLinkAddress;
  }
  
  public String getHardwareAddress() {
    return this.mHwAddr;
  }
  
  public void setHardwareAddress(String paramString) {
    this.mHwAddr = paramString;
  }
  
  public boolean isActive() {
    try {
      if (isUp())
        for (byte b : this.mAddr.getAddress().getAddress()) {
          if (b != 0)
            return true; 
        }  
      return false;
    } catch (NullPointerException nullPointerException) {
      return false;
    } 
  }
  
  public boolean isUp() {
    return hasFlag("up");
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mHwAddr);
    if (this.mAddr != null) {
      paramParcel.writeByte((byte)1);
      paramParcel.writeParcelable(this.mAddr, paramInt);
    } else {
      paramParcel.writeByte((byte)0);
    } 
    paramParcel.writeInt(this.mFlags.size());
    for (String str : this.mFlags)
      paramParcel.writeString(str); 
  }
  
  static {
    CREATOR = new Parcelable.Creator<InterfaceConfiguration>() {
        public InterfaceConfiguration createFromParcel(Parcel param1Parcel) {
          InterfaceConfiguration interfaceConfiguration = new InterfaceConfiguration();
          InterfaceConfiguration.access$002(interfaceConfiguration, param1Parcel.readString());
          if (param1Parcel.readByte() == 1)
            InterfaceConfiguration.access$102(interfaceConfiguration, param1Parcel.<LinkAddress>readParcelable(null)); 
          int i = param1Parcel.readInt();
          for (byte b = 0; b < i; b++)
            interfaceConfiguration.mFlags.add(param1Parcel.readString()); 
          return interfaceConfiguration;
        }
        
        public InterfaceConfiguration[] newArray(int param1Int) {
          return new InterfaceConfiguration[param1Int];
        }
      };
  }
  
  private static void validateFlag(String paramString) {
    if (paramString.indexOf(' ') < 0)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("flag contains space: ");
    stringBuilder.append(paramString);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
}
