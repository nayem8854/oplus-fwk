package android.net.http;

import java.security.cert.X509Certificate;

public class SslError {
  static final boolean $assertionsDisabled = false;
  
  public static final int SSL_DATE_INVALID = 4;
  
  public static final int SSL_EXPIRED = 1;
  
  public static final int SSL_IDMISMATCH = 2;
  
  public static final int SSL_INVALID = 5;
  
  @Deprecated
  public static final int SSL_MAX_ERROR = 6;
  
  public static final int SSL_NOTYETVALID = 0;
  
  public static final int SSL_UNTRUSTED = 3;
  
  final SslCertificate mCertificate;
  
  int mErrors;
  
  final String mUrl;
  
  @Deprecated
  public SslError(int paramInt, SslCertificate paramSslCertificate) {
    this(paramInt, paramSslCertificate, "");
  }
  
  @Deprecated
  public SslError(int paramInt, X509Certificate paramX509Certificate) {
    this(paramInt, paramX509Certificate, "");
  }
  
  public SslError(int paramInt, SslCertificate paramSslCertificate, String paramString) {
    addError(paramInt);
    this.mCertificate = paramSslCertificate;
    this.mUrl = paramString;
  }
  
  public SslError(int paramInt, X509Certificate paramX509Certificate, String paramString) {
    this(paramInt, new SslCertificate(paramX509Certificate), paramString);
  }
  
  public static SslError SslErrorFromChromiumErrorCode(int paramInt, SslCertificate paramSslCertificate, String paramString) {
    if (paramInt == -200)
      return new SslError(2, paramSslCertificate, paramString); 
    if (paramInt == -201)
      return new SslError(4, paramSslCertificate, paramString); 
    if (paramInt == -202)
      return new SslError(3, paramSslCertificate, paramString); 
    return new SslError(5, paramSslCertificate, paramString);
  }
  
  public SslCertificate getCertificate() {
    return this.mCertificate;
  }
  
  public String getUrl() {
    return this.mUrl;
  }
  
  public boolean addError(int paramInt) {
    boolean bool;
    if (paramInt >= 0 && paramInt < 6) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool)
      this.mErrors = 1 << paramInt | this.mErrors; 
    return bool;
  }
  
  public boolean hasError(int paramInt) {
    boolean bool2, bool1 = false;
    if (paramInt >= 0 && paramInt < 6) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    boolean bool3 = bool2;
    if (bool2) {
      bool2 = bool1;
      if ((this.mErrors & 1 << paramInt) != 0)
        bool2 = true; 
      bool3 = bool2;
    } 
    return bool3;
  }
  
  public int getPrimaryError() {
    if (this.mErrors != 0)
      for (byte b = 5; b >= 0; b--) {
        if ((this.mErrors & 1 << b) != 0)
          return b; 
      }  
    return -1;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("primary error: ");
    stringBuilder.append(getPrimaryError());
    stringBuilder.append(" certificate: ");
    stringBuilder.append(getCertificate());
    stringBuilder.append(" on URL: ");
    stringBuilder.append(getUrl());
    return stringBuilder.toString();
  }
}
