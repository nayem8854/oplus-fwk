package android.net.http;

import android.security.net.config.UserCertificateSource;
import com.android.org.conscrypt.TrustManagerImpl;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.net.ssl.X509TrustManager;

public class X509TrustManagerExtensions {
  private final Method mCheckServerTrusted;
  
  private final TrustManagerImpl mDelegate;
  
  private final Method mIsSameTrustConfiguration;
  
  private final X509TrustManager mTrustManager;
  
  public X509TrustManagerExtensions(X509TrustManager paramX509TrustManager) throws IllegalArgumentException {
    if (paramX509TrustManager instanceof TrustManagerImpl) {
      this.mDelegate = (TrustManagerImpl)paramX509TrustManager;
      this.mTrustManager = null;
      this.mCheckServerTrusted = null;
      this.mIsSameTrustConfiguration = null;
      return;
    } 
    this.mDelegate = null;
    this.mTrustManager = paramX509TrustManager;
    try {
      this.mCheckServerTrusted = paramX509TrustManager.getClass().getMethod("checkServerTrusted", new Class[] { X509Certificate[].class, String.class, String.class });
      ReflectiveOperationException reflectiveOperationException2 = null;
      try {
        Method method = paramX509TrustManager.getClass().getMethod("isSameTrustConfiguration", new Class[] { String.class, String.class });
      } catch (ReflectiveOperationException reflectiveOperationException1) {
        reflectiveOperationException1 = reflectiveOperationException2;
      } 
      this.mIsSameTrustConfiguration = (Method)reflectiveOperationException1;
      return;
    } catch (NoSuchMethodException noSuchMethodException) {
      throw new IllegalArgumentException("Required method checkServerTrusted(X509Certificate[], String, String, String) missing");
    } 
  }
  
  public List<X509Certificate> checkServerTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString1, String paramString2) throws CertificateException {
    TrustManagerImpl trustManagerImpl = this.mDelegate;
    if (trustManagerImpl != null)
      return trustManagerImpl.checkServerTrusted(paramArrayOfX509Certificate, paramString1, paramString2); 
    try {
      return (List)this.mCheckServerTrusted.invoke(this.mTrustManager, new Object[] { paramArrayOfX509Certificate, paramString1, paramString2 });
    } catch (IllegalAccessException illegalAccessException) {
      throw new CertificateException("Failed to call checkServerTrusted", illegalAccessException);
    } catch (InvocationTargetException invocationTargetException) {
      if (!(invocationTargetException.getCause() instanceof CertificateException)) {
        if (invocationTargetException.getCause() instanceof RuntimeException)
          throw (RuntimeException)invocationTargetException.getCause(); 
        throw new CertificateException("checkServerTrusted failed", invocationTargetException.getCause());
      } 
      throw (CertificateException)invocationTargetException.getCause();
    } 
  }
  
  public boolean isUserAddedCertificate(X509Certificate paramX509Certificate) {
    boolean bool;
    if (UserCertificateSource.getInstance().findBySubjectAndPublicKey(paramX509Certificate) != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isSameTrustConfiguration(String paramString1, String paramString2) {
    Method method = this.mIsSameTrustConfiguration;
    if (method == null)
      return true; 
    try {
      return ((Boolean)method.invoke(this.mTrustManager, new Object[] { paramString1, paramString2 })).booleanValue();
    } catch (IllegalAccessException illegalAccessException) {
      throw new RuntimeException("Failed to call isSameTrustConfiguration", illegalAccessException);
    } catch (InvocationTargetException invocationTargetException) {
      if (invocationTargetException.getCause() instanceof RuntimeException)
        throw (RuntimeException)invocationTargetException.getCause(); 
      throw new RuntimeException("isSameTrustConfiguration failed", invocationTargetException.getCause());
    } 
  }
}
