package android.net.http;

import com.android.okhttp.internalandroidapi.AndroidResponseCacheAdapter;
import com.android.okhttp.internalandroidapi.HasCacheHolder;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.net.CacheRequest;
import java.net.CacheResponse;
import java.net.ResponseCache;
import java.net.URI;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

public final class HttpResponseCache extends ResponseCache implements HasCacheHolder, Closeable {
  private final AndroidResponseCacheAdapter mDelegate;
  
  private HttpResponseCache(AndroidResponseCacheAdapter paramAndroidResponseCacheAdapter) {
    this.mDelegate = paramAndroidResponseCacheAdapter;
  }
  
  public static HttpResponseCache getInstalled() {
    ResponseCache responseCache = ResponseCache.getDefault();
    if (responseCache instanceof HttpResponseCache)
      return (HttpResponseCache)responseCache; 
    return null;
  }
  
  public static HttpResponseCache install(File paramFile, long paramLong) throws IOException {
    // Byte code:
    //   0: ldc android/net/http/HttpResponseCache
    //   2: monitorenter
    //   3: invokestatic getDefault : ()Ljava/net/ResponseCache;
    //   6: astore_3
    //   7: aload_3
    //   8: instanceof android/net/http/HttpResponseCache
    //   11: ifeq -> 48
    //   14: aload_3
    //   15: checkcast android/net/http/HttpResponseCache
    //   18: astore_3
    //   19: aload_3
    //   20: invokevirtual getCacheHolder : ()Lcom/android/okhttp/internalandroidapi/HasCacheHolder$CacheHolder;
    //   23: astore #4
    //   25: aload #4
    //   27: aload_0
    //   28: lload_1
    //   29: invokevirtual isEquivalent : (Ljava/io/File;J)Z
    //   32: istore #5
    //   34: iload #5
    //   36: ifeq -> 44
    //   39: ldc android/net/http/HttpResponseCache
    //   41: monitorexit
    //   42: aload_3
    //   43: areturn
    //   44: aload_3
    //   45: invokevirtual close : ()V
    //   48: aload_0
    //   49: lload_1
    //   50: invokestatic create : (Ljava/io/File;J)Lcom/android/okhttp/internalandroidapi/HasCacheHolder$CacheHolder;
    //   53: astore_3
    //   54: new com/android/okhttp/internalandroidapi/AndroidResponseCacheAdapter
    //   57: astore_0
    //   58: aload_0
    //   59: aload_3
    //   60: invokespecial <init> : (Lcom/android/okhttp/internalandroidapi/HasCacheHolder$CacheHolder;)V
    //   63: new android/net/http/HttpResponseCache
    //   66: astore_3
    //   67: aload_3
    //   68: aload_0
    //   69: invokespecial <init> : (Lcom/android/okhttp/internalandroidapi/AndroidResponseCacheAdapter;)V
    //   72: aload_3
    //   73: invokestatic setDefault : (Ljava/net/ResponseCache;)V
    //   76: ldc android/net/http/HttpResponseCache
    //   78: monitorexit
    //   79: aload_3
    //   80: areturn
    //   81: astore_0
    //   82: ldc android/net/http/HttpResponseCache
    //   84: monitorexit
    //   85: aload_0
    //   86: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #183	-> 3
    //   #184	-> 7
    //   #185	-> 14
    //   #186	-> 19
    //   #188	-> 25
    //   #189	-> 39
    //   #192	-> 44
    //   #196	-> 48
    //   #197	-> 54
    //   #199	-> 63
    //   #200	-> 72
    //   #201	-> 76
    //   #182	-> 81
    // Exception table:
    //   from	to	target	type
    //   3	7	81	finally
    //   7	14	81	finally
    //   14	19	81	finally
    //   19	25	81	finally
    //   25	34	81	finally
    //   44	48	81	finally
    //   48	54	81	finally
    //   54	63	81	finally
    //   63	72	81	finally
    //   72	76	81	finally
  }
  
  public CacheResponse get(URI paramURI, String paramString, Map<String, List<String>> paramMap) throws IOException {
    return this.mDelegate.get(paramURI, paramString, paramMap);
  }
  
  public CacheRequest put(URI paramURI, URLConnection paramURLConnection) throws IOException {
    return this.mDelegate.put(paramURI, paramURLConnection);
  }
  
  public long size() {
    try {
      return this.mDelegate.getSize();
    } catch (IOException iOException) {
      return -1L;
    } 
  }
  
  public long maxSize() {
    return this.mDelegate.getMaxSize();
  }
  
  public void flush() {
    try {
      this.mDelegate.flush();
    } catch (IOException iOException) {}
  }
  
  public int getNetworkCount() {
    return this.mDelegate.getNetworkCount();
  }
  
  public int getHitCount() {
    return this.mDelegate.getHitCount();
  }
  
  public int getRequestCount() {
    return this.mDelegate.getRequestCount();
  }
  
  public void close() throws IOException {
    if (ResponseCache.getDefault() == this)
      ResponseCache.setDefault(null); 
    this.mDelegate.close();
  }
  
  public void delete() throws IOException {
    if (ResponseCache.getDefault() == this)
      ResponseCache.setDefault(null); 
    this.mDelegate.delete();
  }
  
  public HasCacheHolder.CacheHolder getCacheHolder() {
    return this.mDelegate.getCacheHolder();
  }
}
