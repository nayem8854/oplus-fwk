package android.net.http;

import android.content.Context;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.android.internal.util.HexDump;
import com.android.org.bouncycastle.asn1.x509.X509Name;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

public class SslCertificate {
  private static String ISO_8601_DATE_FORMAT = "yyyy-MM-dd HH:mm:ssZ";
  
  private static final String ISSUED_BY = "issued-by";
  
  private static final String ISSUED_TO = "issued-to";
  
  private static final String VALID_NOT_AFTER = "valid-not-after";
  
  private static final String VALID_NOT_BEFORE = "valid-not-before";
  
  private static final String X509_CERTIFICATE = "x509-certificate";
  
  private final DName mIssuedBy;
  
  private final DName mIssuedTo;
  
  private final Date mValidNotAfter;
  
  private final Date mValidNotBefore;
  
  private final X509Certificate mX509Certificate;
  
  public static Bundle saveState(SslCertificate paramSslCertificate) {
    if (paramSslCertificate == null)
      return null; 
    Bundle bundle = new Bundle();
    bundle.putString("issued-to", paramSslCertificate.getIssuedTo().getDName());
    bundle.putString("issued-by", paramSslCertificate.getIssuedBy().getDName());
    bundle.putString("valid-not-before", paramSslCertificate.getValidNotBefore());
    bundle.putString("valid-not-after", paramSslCertificate.getValidNotAfter());
    X509Certificate x509Certificate = paramSslCertificate.mX509Certificate;
    if (x509Certificate != null)
      try {
        bundle.putByteArray("x509-certificate", x509Certificate.getEncoded());
      } catch (CertificateEncodingException certificateEncodingException) {} 
    return bundle;
  }
  
  public static SslCertificate restoreState(Bundle paramBundle) {
    if (paramBundle == null)
      return null; 
    byte[] arrayOfByte = paramBundle.getByteArray("x509-certificate");
    if (arrayOfByte == null) {
      arrayOfByte = null;
    } else {
      try {
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream();
        this(arrayOfByte);
        Certificate certificate = certificateFactory.generateCertificate(byteArrayInputStream);
        certificate = certificate;
      } catch (CertificateException certificateException) {
        certificateException = null;
      } 
    } 
    String str1 = paramBundle.getString("issued-to");
    String str2 = paramBundle.getString("issued-by");
    Date date = parseDate(paramBundle.getString("valid-not-before"));
    return new SslCertificate(str1, str2, date, parseDate(paramBundle.getString("valid-not-after")), (X509Certificate)certificateException);
  }
  
  @Deprecated
  public SslCertificate(String paramString1, String paramString2, String paramString3, String paramString4) {
    this(paramString1, paramString2, parseDate(paramString3), parseDate(paramString4), null);
  }
  
  @Deprecated
  public SslCertificate(String paramString1, String paramString2, Date paramDate1, Date paramDate2) {
    this(paramString1, paramString2, paramDate1, paramDate2, null);
  }
  
  public SslCertificate(X509Certificate paramX509Certificate) {
    this(str1, str2, date1, date2, paramX509Certificate);
  }
  
  private SslCertificate(String paramString1, String paramString2, Date paramDate1, Date paramDate2, X509Certificate paramX509Certificate) {
    this.mIssuedTo = new DName(paramString1);
    this.mIssuedBy = new DName(paramString2);
    this.mValidNotBefore = cloneDate(paramDate1);
    this.mValidNotAfter = cloneDate(paramDate2);
    this.mX509Certificate = paramX509Certificate;
  }
  
  public Date getValidNotBeforeDate() {
    return cloneDate(this.mValidNotBefore);
  }
  
  @Deprecated
  public String getValidNotBefore() {
    return formatDate(this.mValidNotBefore);
  }
  
  public Date getValidNotAfterDate() {
    return cloneDate(this.mValidNotAfter);
  }
  
  @Deprecated
  public String getValidNotAfter() {
    return formatDate(this.mValidNotAfter);
  }
  
  public DName getIssuedTo() {
    return this.mIssuedTo;
  }
  
  public DName getIssuedBy() {
    return this.mIssuedBy;
  }
  
  public X509Certificate getX509Certificate() {
    return this.mX509Certificate;
  }
  
  private static String getSerialNumber(X509Certificate paramX509Certificate) {
    if (paramX509Certificate == null)
      return ""; 
    BigInteger bigInteger = paramX509Certificate.getSerialNumber();
    if (bigInteger == null)
      return ""; 
    return fingerprint(bigInteger.toByteArray());
  }
  
  private static String getDigest(X509Certificate paramX509Certificate, String paramString) {
    if (paramX509Certificate == null)
      return ""; 
    try {
      byte[] arrayOfByte = paramX509Certificate.getEncoded();
      MessageDigest messageDigest = MessageDigest.getInstance(paramString);
      arrayOfByte = messageDigest.digest(arrayOfByte);
      return fingerprint(arrayOfByte);
    } catch (CertificateEncodingException certificateEncodingException) {
      return "";
    } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
      return "";
    } 
  }
  
  private static final String fingerprint(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte == null)
      return ""; 
    StringBuilder stringBuilder = new StringBuilder();
    for (byte b = 0; b < paramArrayOfbyte.length; b++) {
      byte b1 = paramArrayOfbyte[b];
      HexDump.appendByteAsHex(stringBuilder, b1, true);
      if (b + 1 != paramArrayOfbyte.length)
        stringBuilder.append(':'); 
    } 
    return stringBuilder.toString();
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Issued to: ");
    stringBuilder.append(this.mIssuedTo.getDName());
    stringBuilder.append(";\nIssued by: ");
    DName dName = this.mIssuedBy;
    stringBuilder.append(dName.getDName());
    stringBuilder.append(";\n");
    return stringBuilder.toString();
  }
  
  private static Date parseDate(String paramString) {
    try {
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
      this(ISO_8601_DATE_FORMAT);
      return simpleDateFormat.parse(paramString);
    } catch (ParseException parseException) {
      return null;
    } 
  }
  
  private static String formatDate(Date paramDate) {
    if (paramDate == null)
      return ""; 
    return (new SimpleDateFormat(ISO_8601_DATE_FORMAT)).format(paramDate);
  }
  
  private static Date cloneDate(Date paramDate) {
    if (paramDate == null)
      return null; 
    return (Date)paramDate.clone();
  }
  
  public class DName {
    private String mCName;
    
    private String mDName;
    
    private String mOName;
    
    private String mUName;
    
    final SslCertificate this$0;
    
    public DName(String param1String) {
      if (param1String != null) {
        this.mDName = param1String;
        try {
          X509Name x509Name = new X509Name();
          this(param1String);
          Vector<String> vector = x509Name.getValues();
          Vector<E> vector1 = x509Name.getOIDs();
          for (byte b = 0; b < vector1.size(); b++) {
            if (vector1.elementAt(b).equals(X509Name.CN)) {
              if (this.mCName == null)
                this.mCName = vector.elementAt(b); 
            } else if (vector1.elementAt(b).equals(X509Name.O) && 
              this.mOName == null) {
              this.mOName = vector.elementAt(b);
            } else if (vector1.elementAt(b).equals(X509Name.OU) && 
              this.mUName == null) {
              this.mUName = vector.elementAt(b);
            } 
          } 
        } catch (IllegalArgumentException illegalArgumentException) {}
      } 
    }
    
    public String getDName() {
      String str = this.mDName;
      if (str == null)
        str = ""; 
      return str;
    }
    
    public String getCName() {
      String str = this.mCName;
      if (str == null)
        str = ""; 
      return str;
    }
    
    public String getOName() {
      String str = this.mOName;
      if (str == null)
        str = ""; 
      return str;
    }
    
    public String getUName() {
      String str = this.mUName;
      if (str == null)
        str = ""; 
      return str;
    }
  }
  
  public View inflateCertificateView(Context paramContext) {
    LayoutInflater layoutInflater = LayoutInflater.from(paramContext);
    View view = layoutInflater.inflate(17367314, null);
    DName dName2 = getIssuedTo();
    if (dName2 != null) {
      TextView textView = (TextView)view.findViewById(16909549);
      textView.setText(dName2.getCName());
      textView = (TextView)view.findViewById(16909551);
      textView.setText(dName2.getOName());
      textView = (TextView)view.findViewById(16909553);
      textView.setText(dName2.getUName());
    } 
    TextView textView2 = (TextView)view.findViewById(16909415);
    X509Certificate x509Certificate2 = this.mX509Certificate;
    textView2.setText(getSerialNumber(x509Certificate2));
    DName dName1 = getIssuedBy();
    if (dName1 != null) {
      textView2 = (TextView)view.findViewById(16908815);
      textView2.setText(dName1.getCName());
      textView2 = (TextView)view.findViewById(16908817);
      textView2.setText(dName1.getOName());
      textView2 = (TextView)view.findViewById(16908819);
      textView2.setText(dName1.getUName());
    } 
    String str2 = formatCertificateDate(paramContext, getValidNotBeforeDate());
    TextView textView1 = (TextView)view.findViewById(16909099);
    textView1.setText(str2);
    String str1 = formatCertificateDate(paramContext, getValidNotAfterDate());
    textView1 = (TextView)view.findViewById(16908955);
    textView1.setText(str1);
    textView1 = (TextView)view.findViewById(16909422);
    X509Certificate x509Certificate1 = this.mX509Certificate;
    textView1.setText(getDigest(x509Certificate1, "SHA256"));
    textView1 = (TextView)view.findViewById(16909420);
    x509Certificate1 = this.mX509Certificate;
    textView1.setText(getDigest(x509Certificate1, "SHA1"));
    return view;
  }
  
  private String formatCertificateDate(Context paramContext, Date paramDate) {
    if (paramDate == null)
      return ""; 
    return DateFormat.getMediumDateFormat(paramContext).format(paramDate);
  }
}
