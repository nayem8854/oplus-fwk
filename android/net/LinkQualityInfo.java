package android.net;

import android.os.Parcel;
import android.os.Parcelable;

public class LinkQualityInfo implements Parcelable {
  private int mNetworkType = -1;
  
  private int mNormalizedSignalStrength = Integer.MAX_VALUE;
  
  private long mPacketCount = Long.MAX_VALUE;
  
  private long mPacketErrorCount = Long.MAX_VALUE;
  
  private int mTheoreticalTxBandwidth = Integer.MAX_VALUE;
  
  private int mTheoreticalRxBandwidth = Integer.MAX_VALUE;
  
  private int mTheoreticalLatency = Integer.MAX_VALUE;
  
  private long mLastDataSampleTime = Long.MAX_VALUE;
  
  private int mDataSampleDuration = Integer.MAX_VALUE;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcel(paramParcel, paramInt, 1);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt1, int paramInt2) {
    paramParcel.writeInt(paramInt2);
    paramParcel.writeInt(this.mNetworkType);
    paramParcel.writeInt(this.mNormalizedSignalStrength);
    paramParcel.writeLong(this.mPacketCount);
    paramParcel.writeLong(this.mPacketErrorCount);
    paramParcel.writeInt(this.mTheoreticalTxBandwidth);
    paramParcel.writeInt(this.mTheoreticalRxBandwidth);
    paramParcel.writeInt(this.mTheoreticalLatency);
    paramParcel.writeLong(this.mLastDataSampleTime);
    paramParcel.writeInt(this.mDataSampleDuration);
  }
  
  public static final Parcelable.Creator<LinkQualityInfo> CREATOR = new Parcelable.Creator<LinkQualityInfo>() {
      public LinkQualityInfo createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        if (i == 1) {
          LinkQualityInfo linkQualityInfo = new LinkQualityInfo();
          linkQualityInfo.initializeFromParcel(param1Parcel);
          return linkQualityInfo;
        } 
        if (i == 2)
          return WifiLinkQualityInfo.createFromParcelBody(param1Parcel); 
        if (i == 3)
          return MobileLinkQualityInfo.createFromParcelBody(param1Parcel); 
        return null;
      }
      
      public LinkQualityInfo[] newArray(int param1Int) {
        return new LinkQualityInfo[param1Int];
      }
    };
  
  public static final int NORMALIZED_MAX_SIGNAL_STRENGTH = 99;
  
  public static final int NORMALIZED_MIN_SIGNAL_STRENGTH = 0;
  
  public static final int NORMALIZED_SIGNAL_STRENGTH_RANGE = 100;
  
  protected static final int OBJECT_TYPE_LINK_QUALITY_INFO = 1;
  
  protected static final int OBJECT_TYPE_MOBILE_LINK_QUALITY_INFO = 3;
  
  protected static final int OBJECT_TYPE_WIFI_LINK_QUALITY_INFO = 2;
  
  public static final int UNKNOWN_INT = 2147483647;
  
  public static final long UNKNOWN_LONG = 9223372036854775807L;
  
  protected void initializeFromParcel(Parcel paramParcel) {
    this.mNetworkType = paramParcel.readInt();
    this.mNormalizedSignalStrength = paramParcel.readInt();
    this.mPacketCount = paramParcel.readLong();
    this.mPacketErrorCount = paramParcel.readLong();
    this.mTheoreticalTxBandwidth = paramParcel.readInt();
    this.mTheoreticalRxBandwidth = paramParcel.readInt();
    this.mTheoreticalLatency = paramParcel.readInt();
    this.mLastDataSampleTime = paramParcel.readLong();
    this.mDataSampleDuration = paramParcel.readInt();
  }
  
  public int getNetworkType() {
    return this.mNetworkType;
  }
  
  public void setNetworkType(int paramInt) {
    this.mNetworkType = paramInt;
  }
  
  public int getNormalizedSignalStrength() {
    return this.mNormalizedSignalStrength;
  }
  
  public void setNormalizedSignalStrength(int paramInt) {
    this.mNormalizedSignalStrength = paramInt;
  }
  
  public long getPacketCount() {
    return this.mPacketCount;
  }
  
  public void setPacketCount(long paramLong) {
    this.mPacketCount = paramLong;
  }
  
  public long getPacketErrorCount() {
    return this.mPacketErrorCount;
  }
  
  public void setPacketErrorCount(long paramLong) {
    this.mPacketErrorCount = paramLong;
  }
  
  public int getTheoreticalTxBandwidth() {
    return this.mTheoreticalTxBandwidth;
  }
  
  public void setTheoreticalTxBandwidth(int paramInt) {
    this.mTheoreticalTxBandwidth = paramInt;
  }
  
  public int getTheoreticalRxBandwidth() {
    return this.mTheoreticalRxBandwidth;
  }
  
  public void setTheoreticalRxBandwidth(int paramInt) {
    this.mTheoreticalRxBandwidth = paramInt;
  }
  
  public int getTheoreticalLatency() {
    return this.mTheoreticalLatency;
  }
  
  public void setTheoreticalLatency(int paramInt) {
    this.mTheoreticalLatency = paramInt;
  }
  
  public long getLastDataSampleTime() {
    return this.mLastDataSampleTime;
  }
  
  public void setLastDataSampleTime(long paramLong) {
    this.mLastDataSampleTime = paramLong;
  }
  
  public int getDataSampleDuration() {
    return this.mDataSampleDuration;
  }
  
  public void setDataSampleDuration(int paramInt) {
    this.mDataSampleDuration = paramInt;
  }
}
