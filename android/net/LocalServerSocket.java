package android.net;

import java.io.Closeable;
import java.io.FileDescriptor;
import java.io.IOException;

public class LocalServerSocket implements Closeable {
  private static final int LISTEN_BACKLOG = 50;
  
  private final LocalSocketImpl impl;
  
  private final LocalSocketAddress localAddress;
  
  public LocalServerSocket(String paramString) throws IOException {
    LocalSocketImpl localSocketImpl = new LocalSocketImpl();
    localSocketImpl.create(2);
    LocalSocketAddress localSocketAddress = new LocalSocketAddress(paramString);
    this.impl.bind(localSocketAddress);
    this.impl.listen(50);
  }
  
  public LocalServerSocket(FileDescriptor paramFileDescriptor) throws IOException {
    LocalSocketImpl localSocketImpl = new LocalSocketImpl(paramFileDescriptor);
    localSocketImpl.listen(50);
    this.localAddress = this.impl.getSockAddress();
  }
  
  public LocalSocketAddress getLocalSocketAddress() {
    return this.localAddress;
  }
  
  public LocalSocket accept() throws IOException {
    LocalSocketImpl localSocketImpl = new LocalSocketImpl();
    this.impl.accept(localSocketImpl);
    return LocalSocket.createLocalSocketForAccept(localSocketImpl);
  }
  
  public FileDescriptor getFileDescriptor() {
    return this.impl.getFileDescriptor();
  }
  
  public void close() throws IOException {
    this.impl.close();
  }
}
