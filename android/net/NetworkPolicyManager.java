package android.net;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.os.RemoteException;
import android.os.UserHandle;
import android.telephony.SubscriptionPlan;
import android.util.DebugUtils;
import android.util.Pair;
import android.util.Range;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.time.ZonedDateTime;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class NetworkPolicyManager {
  private static final boolean ALLOW_PLATFORM_APP_POLICY = true;
  
  public static final String EXTRA_NETWORK_TEMPLATE = "android.net.NETWORK_TEMPLATE";
  
  public static final String FIREWALL_CHAIN_NAME_DOZABLE = "dozable";
  
  public static final String FIREWALL_CHAIN_NAME_NONE = "none";
  
  public static final String FIREWALL_CHAIN_NAME_POWERSAVE = "powersave";
  
  public static final String FIREWALL_CHAIN_NAME_STANDBY = "standby";
  
  public static final int FIREWALL_RULE_DEFAULT = 0;
  
  public static final int FOREGROUND_THRESHOLD_STATE = 5;
  
  public static final int MASK_ALL_NETWORKS = 240;
  
  public static final int MASK_METERED_NETWORKS = 15;
  
  public static final int POLICY_ALLOW_METERED_BACKGROUND = 4;
  
  public static final int POLICY_NONE = 0;
  
  public static final int POLICY_REJECT_METERED_BACKGROUND = 1;
  
  public static final int RULE_ALLOW_ALL = 32;
  
  public static final int RULE_ALLOW_METERED = 1;
  
  public static final int RULE_NONE = 0;
  
  public static final int RULE_REJECT_ALL = 64;
  
  public static final int RULE_REJECT_METERED = 4;
  
  public static final int RULE_TEMPORARY_ALLOW_METERED = 2;
  
  public static final int SUBSCRIPTION_OVERRIDE_CONGESTED = 2;
  
  public static final int SUBSCRIPTION_OVERRIDE_UNMETERED = 1;
  
  private final Map<SubscriptionCallback, SubscriptionCallbackProxy> mCallbackMap = new ConcurrentHashMap<>();
  
  private final Context mContext;
  
  private INetworkPolicyManager mService;
  
  public NetworkPolicyManager(Context paramContext, INetworkPolicyManager paramINetworkPolicyManager) {
    if (paramINetworkPolicyManager != null) {
      this.mContext = paramContext;
      this.mService = paramINetworkPolicyManager;
      return;
    } 
    throw new IllegalArgumentException("missing INetworkPolicyManager");
  }
  
  public static NetworkPolicyManager from(Context paramContext) {
    return (NetworkPolicyManager)paramContext.getSystemService("netpolicy");
  }
  
  public void setUidPolicy(int paramInt1, int paramInt2) {
    try {
      this.mService.setUidPolicy(paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void addUidPolicy(int paramInt1, int paramInt2) {
    try {
      this.mService.addUidPolicy(paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void removeUidPolicy(int paramInt1, int paramInt2) {
    try {
      this.mService.removeUidPolicy(paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getUidPolicy(int paramInt) {
    try {
      return this.mService.getUidPolicy(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int[] getUidsWithPolicy(int paramInt) {
    try {
      return this.mService.getUidsWithPolicy(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void registerListener(INetworkPolicyListener paramINetworkPolicyListener) {
    try {
      this.mService.registerListener(paramINetworkPolicyListener);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void unregisterListener(INetworkPolicyListener paramINetworkPolicyListener) {
    try {
      this.mService.unregisterListener(paramINetworkPolicyListener);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void registerSubscriptionCallback(SubscriptionCallback paramSubscriptionCallback) {
    if (paramSubscriptionCallback != null) {
      SubscriptionCallbackProxy subscriptionCallbackProxy = new SubscriptionCallbackProxy(paramSubscriptionCallback);
      if (this.mCallbackMap.putIfAbsent(paramSubscriptionCallback, subscriptionCallbackProxy) == null) {
        registerListener(subscriptionCallbackProxy);
        return;
      } 
      throw new IllegalArgumentException("Callback is already registered.");
    } 
    throw new NullPointerException("Callback cannot be null.");
  }
  
  public void unregisterSubscriptionCallback(SubscriptionCallback paramSubscriptionCallback) {
    if (paramSubscriptionCallback != null) {
      SubscriptionCallbackProxy subscriptionCallbackProxy = this.mCallbackMap.remove(paramSubscriptionCallback);
      if (subscriptionCallbackProxy == null)
        return; 
      unregisterListener(subscriptionCallbackProxy);
      return;
    } 
    throw new NullPointerException("Callback cannot be null.");
  }
  
  public void setNetworkPolicies(NetworkPolicy[] paramArrayOfNetworkPolicy) {
    try {
      this.mService.setNetworkPolicies(paramArrayOfNetworkPolicy);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public NetworkPolicy[] getNetworkPolicies() {
    try {
      return this.mService.getNetworkPolicies(this.mContext.getOpPackageName());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setRestrictBackground(boolean paramBoolean) {
    try {
      this.mService.setRestrictBackground(paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean getRestrictBackground() {
    try {
      return this.mService.getRestrictBackground();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setSubscriptionOverride(int paramInt1, int paramInt2, int paramInt3, long paramLong, String paramString) {
    try {
      this.mService.setSubscriptionOverride(paramInt1, paramInt2, paramInt3, paramLong, paramString);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setSubscriptionPlans(int paramInt, SubscriptionPlan[] paramArrayOfSubscriptionPlan, String paramString) {
    try {
      this.mService.setSubscriptionPlans(paramInt, paramArrayOfSubscriptionPlan, paramString);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public SubscriptionPlan[] getSubscriptionPlans(int paramInt, String paramString) {
    try {
      return this.mService.getSubscriptionPlans(paramInt, paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void factoryReset(String paramString) {
    try {
      this.mService.factoryReset(paramString);
      OplusNetworkingControlManager oplusNetworkingControlManager = (OplusNetworkingControlManager)this.mContext.getSystemService("networking_control");
      if (oplusNetworkingControlManager != null)
        oplusNetworkingControlManager.factoryReset(); 
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public static Iterator<Pair<ZonedDateTime, ZonedDateTime>> cycleIterator(NetworkPolicy paramNetworkPolicy) {
    final Iterator<Range<ZonedDateTime>> it = paramNetworkPolicy.cycleIterator();
    return new Iterator<Pair<ZonedDateTime, ZonedDateTime>>() {
        final Iterator val$it;
        
        public boolean hasNext() {
          return it.hasNext();
        }
        
        public Pair<ZonedDateTime, ZonedDateTime> next() {
          if (hasNext()) {
            Range range = it.next();
            return Pair.create(range.getLower(), range.getUpper());
          } 
          return Pair.create(null, null);
        }
      };
  }
  
  @Deprecated
  public static boolean isUidValidForPolicy(Context paramContext, int paramInt) {
    if (!UserHandle.isApp(paramInt))
      return false; 
    return true;
  }
  
  public static String uidRulesToString(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramInt);
    stringBuilder = stringBuilder.append(" (");
    if (paramInt == 0) {
      stringBuilder.append("NONE");
    } else {
      stringBuilder.append(DebugUtils.flagsToString(NetworkPolicyManager.class, "RULE_", paramInt));
    } 
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
  
  public static String uidPoliciesToString(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramInt);
    stringBuilder = stringBuilder.append(" (");
    if (paramInt == 0) {
      stringBuilder.append("NONE");
    } else {
      stringBuilder.append(DebugUtils.flagsToString(NetworkPolicyManager.class, "POLICY_", paramInt));
    } 
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
  
  public static boolean isProcStateAllowedWhileIdleOrPowerSaveMode(int paramInt) {
    boolean bool;
    if (paramInt <= 5) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isProcStateAllowedWhileOnRestrictBackground(int paramInt) {
    boolean bool;
    if (paramInt <= 5) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static String resolveNetworkId(WifiConfiguration paramWifiConfiguration) {
    String str;
    if (paramWifiConfiguration.isPasspoint()) {
      str = paramWifiConfiguration.providerFriendlyName;
    } else {
      str = ((WifiConfiguration)str).SSID;
    } 
    return WifiInfo.sanitizeSsid(str);
  }
  
  public static String resolveNetworkId(String paramString) {
    return WifiInfo.sanitizeSsid(paramString);
  }
  
  public static class SubscriptionCallback {
    public void onSubscriptionOverride(int param1Int1, int param1Int2, int param1Int3) {}
    
    public void onSubscriptionPlansChanged(int param1Int, SubscriptionPlan[] param1ArrayOfSubscriptionPlan) {}
  }
  
  class SubscriptionCallbackProxy extends Listener {
    private final NetworkPolicyManager.SubscriptionCallback mCallback;
    
    final NetworkPolicyManager this$0;
    
    SubscriptionCallbackProxy(NetworkPolicyManager.SubscriptionCallback param1SubscriptionCallback) {
      this.mCallback = param1SubscriptionCallback;
    }
    
    public void onSubscriptionOverride(int param1Int1, int param1Int2, int param1Int3) {
      this.mCallback.onSubscriptionOverride(param1Int1, param1Int2, param1Int3);
    }
    
    public void onSubscriptionPlansChanged(int param1Int, SubscriptionPlan[] param1ArrayOfSubscriptionPlan) {
      this.mCallback.onSubscriptionPlansChanged(param1Int, param1ArrayOfSubscriptionPlan);
    }
  }
  
  class Listener extends INetworkPolicyListener.Stub {
    public void onUidRulesChanged(int param1Int1, int param1Int2) {}
    
    public void onMeteredIfacesChanged(String[] param1ArrayOfString) {}
    
    public void onRestrictBackgroundChanged(boolean param1Boolean) {}
    
    public void onUidPoliciesChanged(int param1Int1, int param1Int2) {}
    
    public void onSubscriptionOverride(int param1Int1, int param1Int2, int param1Int3) {}
    
    public void onSubscriptionPlansChanged(int param1Int, SubscriptionPlan[] param1ArrayOfSubscriptionPlan) {}
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SubscriptionOverrideMask {}
}
