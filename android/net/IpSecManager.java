package android.net;

import android.annotation.SystemApi;
import android.content.Context;
import android.os.Binder;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.os.ServiceSpecificException;
import android.system.ErrnoException;
import android.system.OsConstants;
import android.util.AndroidException;
import android.util.Log;
import com.android.internal.util.Preconditions;
import dalvik.system.CloseGuard;
import java.io.FileDescriptor;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;

public final class IpSecManager {
  public static final int DIRECTION_IN = 0;
  
  public static final int DIRECTION_OUT = 1;
  
  public static final int INVALID_RESOURCE_ID = -1;
  
  public static final int INVALID_SECURITY_PARAMETER_INDEX = 0;
  
  private static final String TAG = "IpSecManager";
  
  private final Context mContext;
  
  private final IIpSecService mService;
  
  class SpiUnavailableException extends AndroidException {
    private final int mSpi;
    
    SpiUnavailableException(IpSecManager this$0, int param1Int) {
      super(stringBuilder.toString());
      this.mSpi = param1Int;
    }
    
    public int getSpi() {
      return this.mSpi;
    }
  }
  
  class ResourceUnavailableException extends AndroidException {
    ResourceUnavailableException(IpSecManager this$0) {
      super((String)this$0);
    }
  }
  
  public static final class SecurityParameterIndex implements AutoCloseable {
    private final CloseGuard mCloseGuard = CloseGuard.get();
    
    private int mSpi = 0;
    
    private int mResourceId = -1;
    
    private final InetAddress mDestinationAddress;
    
    private final IIpSecService mService;
    
    public int getSpi() {
      return this.mSpi;
    }
    
    public void close() {
      Exception exception;
      try {
        this.mService.releaseSecurityParameterIndex(this.mResourceId);
        this.mResourceId = -1;
        this.mCloseGuard.close();
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } catch (Exception null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Failed to close ");
        stringBuilder.append(this);
        stringBuilder.append(", Exception=");
        stringBuilder.append(exception);
        Log.e("IpSecManager", stringBuilder.toString());
        this.mResourceId = -1;
        this.mCloseGuard.close();
        return;
      } finally {}
      this.mResourceId = -1;
      this.mCloseGuard.close();
      throw exception;
    }
    
    protected void finalize() throws Throwable {
      CloseGuard closeGuard = this.mCloseGuard;
      if (closeGuard != null)
        closeGuard.warnIfOpen(); 
      close();
    }
    
    private SecurityParameterIndex(IIpSecService param1IIpSecService, InetAddress param1InetAddress, int param1Int) throws IpSecManager.ResourceUnavailableException, IpSecManager.SpiUnavailableException {
      this.mService = param1IIpSecService;
      this.mDestinationAddress = param1InetAddress;
      try {
        String str = param1InetAddress.getHostAddress();
        Binder binder = new Binder();
        this();
        IpSecSpiResponse ipSecSpiResponse = param1IIpSecService.allocateSecurityParameterIndex(str, param1Int, binder);
        if (ipSecSpiResponse != null) {
          IpSecManager.ResourceUnavailableException resourceUnavailableException;
          int i = ipSecSpiResponse.status;
          if (i != 0) {
            if (i != 1) {
              if (i != 2) {
                RuntimeException runtimeException1 = new RuntimeException();
                StringBuilder stringBuilder1 = new StringBuilder();
                this();
                stringBuilder1.append("Unknown status returned by IpSecService: ");
                stringBuilder1.append(i);
                this(stringBuilder1.toString());
                throw runtimeException1;
              } 
              IpSecManager.SpiUnavailableException spiUnavailableException = new IpSecManager.SpiUnavailableException();
              this("Requested SPI is unavailable", param1Int);
              throw spiUnavailableException;
            } 
            resourceUnavailableException = new IpSecManager.ResourceUnavailableException();
            this("No more SPIs may be allocated by this requester.");
            throw resourceUnavailableException;
          } 
          this.mSpi = ((IpSecSpiResponse)resourceUnavailableException).spi;
          this.mResourceId = param1Int = ((IpSecSpiResponse)resourceUnavailableException).resourceId;
          int j = this.mSpi;
          if (j != 0) {
            if (param1Int != -1) {
              this.mCloseGuard.open("open");
              return;
            } 
            RuntimeException runtimeException1 = new RuntimeException();
            StringBuilder stringBuilder1 = new StringBuilder();
            this();
            stringBuilder1.append("Invalid Resource ID returned by IpSecService: ");
            stringBuilder1.append(i);
            this(stringBuilder1.toString());
            throw runtimeException1;
          } 
          RuntimeException runtimeException = new RuntimeException();
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Invalid SPI returned by IpSecService: ");
          stringBuilder.append(i);
          this(stringBuilder.toString());
          throw runtimeException;
        } 
        NullPointerException nullPointerException = new NullPointerException();
        this("Received null response from IpSecService");
        throw nullPointerException;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    public int getResourceId() {
      return this.mResourceId;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("SecurityParameterIndex{spi=");
      int i = this.mSpi;
      stringBuilder.append(i);
      stringBuilder.append(",resourceId=");
      i = this.mResourceId;
      stringBuilder.append(i);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  public SecurityParameterIndex allocateSecurityParameterIndex(InetAddress paramInetAddress) throws ResourceUnavailableException {
    try {
      return new SecurityParameterIndex(this.mService, paramInetAddress, 0);
    } catch (ServiceSpecificException serviceSpecificException) {
      throw rethrowUncheckedExceptionFromServiceSpecificException(serviceSpecificException);
    } catch (SpiUnavailableException spiUnavailableException) {
      throw new ResourceUnavailableException("No SPIs available");
    } 
  }
  
  public SecurityParameterIndex allocateSecurityParameterIndex(InetAddress paramInetAddress, int paramInt) throws SpiUnavailableException, ResourceUnavailableException {
    if (paramInt != 0)
      try {
        return new SecurityParameterIndex(this.mService, paramInetAddress, paramInt);
      } catch (ServiceSpecificException serviceSpecificException) {
        throw rethrowUncheckedExceptionFromServiceSpecificException(serviceSpecificException);
      }  
    throw new IllegalArgumentException("Requested SPI must be a valid (non-zero) SPI");
  }
  
  public void applyTransportModeTransform(Socket paramSocket, int paramInt, IpSecTransform paramIpSecTransform) throws IOException {
    paramSocket.getSoLinger();
    applyTransportModeTransform(paramSocket.getFileDescriptor$(), paramInt, paramIpSecTransform);
  }
  
  public void applyTransportModeTransform(DatagramSocket paramDatagramSocket, int paramInt, IpSecTransform paramIpSecTransform) throws IOException {
    applyTransportModeTransform(paramDatagramSocket.getFileDescriptor$(), paramInt, paramIpSecTransform);
  }
  
  public void applyTransportModeTransform(FileDescriptor paramFileDescriptor, int paramInt, IpSecTransform paramIpSecTransform) throws IOException {
    try {
      ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.dup(paramFileDescriptor);
      try {
        this.mService.applyTransportModeTransform(parcelFileDescriptor, paramInt, paramIpSecTransform.getResourceId());
        return;
      } finally {
        if (parcelFileDescriptor != null)
          try {
            parcelFileDescriptor.close();
          } finally {
            parcelFileDescriptor = null;
          }  
      } 
    } catch (ServiceSpecificException serviceSpecificException) {
      throw rethrowCheckedExceptionFromServiceSpecificException(serviceSpecificException);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void removeTransportModeTransforms(Socket paramSocket) throws IOException {
    paramSocket.getSoLinger();
    removeTransportModeTransforms(paramSocket.getFileDescriptor$());
  }
  
  public void removeTransportModeTransforms(DatagramSocket paramDatagramSocket) throws IOException {
    removeTransportModeTransforms(paramDatagramSocket.getFileDescriptor$());
  }
  
  public void removeTransportModeTransforms(FileDescriptor paramFileDescriptor) throws IOException {
    try {
      ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.dup(paramFileDescriptor);
      try {
        this.mService.removeTransportModeTransforms(parcelFileDescriptor);
        return;
      } finally {
        if (parcelFileDescriptor != null)
          try {
            parcelFileDescriptor.close();
          } finally {
            parcelFileDescriptor = null;
          }  
      } 
    } catch (ServiceSpecificException serviceSpecificException) {
      throw rethrowCheckedExceptionFromServiceSpecificException(serviceSpecificException);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void removeTunnelModeTransform(Network paramNetwork, IpSecTransform paramIpSecTransform) {}
  
  public static final class UdpEncapsulationSocket implements AutoCloseable {
    private final IIpSecService mService;
    
    private int mResourceId = -1;
    
    private final int mPort;
    
    private final ParcelFileDescriptor mPfd;
    
    private final CloseGuard mCloseGuard = CloseGuard.get();
    
    private UdpEncapsulationSocket(IIpSecService param1IIpSecService, int param1Int) throws IpSecManager.ResourceUnavailableException, IOException {
      this.mService = param1IIpSecService;
      try {
        Binder binder = new Binder();
        this();
        IpSecUdpEncapResponse ipSecUdpEncapResponse = param1IIpSecService.openUdpEncapsulationSocket(param1Int, binder);
        param1Int = ipSecUdpEncapResponse.status;
        if (param1Int != 0) {
          if (param1Int != 1) {
            RuntimeException runtimeException = new RuntimeException();
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Unknown status returned by IpSecService: ");
            stringBuilder.append(ipSecUdpEncapResponse.status);
            this(stringBuilder.toString());
            throw runtimeException;
          } 
          IpSecManager.ResourceUnavailableException resourceUnavailableException = new IpSecManager.ResourceUnavailableException();
          this("No more Sockets may be allocated by this requester.");
          throw resourceUnavailableException;
        } 
        this.mResourceId = ipSecUdpEncapResponse.resourceId;
        this.mPort = ipSecUdpEncapResponse.port;
        this.mPfd = ipSecUdpEncapResponse.fileDescriptor;
        this.mCloseGuard.open("constructor");
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    public FileDescriptor getFileDescriptor() {
      ParcelFileDescriptor parcelFileDescriptor = this.mPfd;
      if (parcelFileDescriptor == null)
        return null; 
      return parcelFileDescriptor.getFileDescriptor();
    }
    
    public int getPort() {
      return this.mPort;
    }
    
    public void close() throws IOException {
      // Byte code:
      //   0: aload_0
      //   1: getfield mService : Landroid/net/IIpSecService;
      //   4: aload_0
      //   5: getfield mResourceId : I
      //   8: invokeinterface closeUdpEncapsulationSocket : (I)V
      //   13: aload_0
      //   14: iconst_m1
      //   15: putfield mResourceId : I
      //   18: aload_0
      //   19: iconst_m1
      //   20: putfield mResourceId : I
      //   23: aload_0
      //   24: getfield mCloseGuard : Ldalvik/system/CloseGuard;
      //   27: invokevirtual close : ()V
      //   30: goto -> 85
      //   33: astore_1
      //   34: goto -> 136
      //   37: astore_1
      //   38: new java/lang/StringBuilder
      //   41: astore_2
      //   42: aload_2
      //   43: invokespecial <init> : ()V
      //   46: aload_2
      //   47: ldc 'Failed to close '
      //   49: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   52: pop
      //   53: aload_2
      //   54: aload_0
      //   55: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   58: pop
      //   59: aload_2
      //   60: ldc ', Exception='
      //   62: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   65: pop
      //   66: aload_2
      //   67: aload_1
      //   68: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   71: pop
      //   72: ldc 'IpSecManager'
      //   74: aload_2
      //   75: invokevirtual toString : ()Ljava/lang/String;
      //   78: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   81: pop
      //   82: goto -> 18
      //   85: aload_0
      //   86: getfield mPfd : Landroid/os/ParcelFileDescriptor;
      //   89: invokevirtual close : ()V
      //   92: return
      //   93: astore_2
      //   94: new java/lang/StringBuilder
      //   97: dup
      //   98: invokespecial <init> : ()V
      //   101: astore_1
      //   102: aload_1
      //   103: ldc 'Failed to close UDP Encapsulation Socket with Port= '
      //   105: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   108: pop
      //   109: aload_1
      //   110: aload_0
      //   111: getfield mPort : I
      //   114: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   117: pop
      //   118: ldc 'IpSecManager'
      //   120: aload_1
      //   121: invokevirtual toString : ()Ljava/lang/String;
      //   124: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   127: pop
      //   128: aload_2
      //   129: athrow
      //   130: astore_1
      //   131: aload_1
      //   132: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
      //   135: athrow
      //   136: aload_0
      //   137: iconst_m1
      //   138: putfield mResourceId : I
      //   141: aload_0
      //   142: getfield mCloseGuard : Ldalvik/system/CloseGuard;
      //   145: invokevirtual close : ()V
      //   148: aload_1
      //   149: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #600	-> 0
      //   #601	-> 13
      //   #609	-> 18
      //   #610	-> 23
      //   #611	-> 30
      //   #609	-> 33
      //   #604	-> 37
      //   #607	-> 38
      //   #609	-> 82
      //   #614	-> 85
      //   #618	-> 92
      //   #619	-> 92
      //   #615	-> 93
      //   #616	-> 94
      //   #617	-> 128
      //   #602	-> 130
      //   #603	-> 131
      //   #609	-> 136
      //   #610	-> 141
      //   #611	-> 148
      // Exception table:
      //   from	to	target	type
      //   0	13	130	android/os/RemoteException
      //   0	13	37	java/lang/Exception
      //   0	13	33	finally
      //   13	18	130	android/os/RemoteException
      //   13	18	37	java/lang/Exception
      //   13	18	33	finally
      //   38	82	33	finally
      //   85	92	93	java/io/IOException
      //   131	136	33	finally
    }
    
    protected void finalize() throws Throwable {
      CloseGuard closeGuard = this.mCloseGuard;
      if (closeGuard != null)
        closeGuard.warnIfOpen(); 
      close();
    }
    
    public int getResourceId() {
      return this.mResourceId;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("UdpEncapsulationSocket{port=");
      int i = this.mPort;
      stringBuilder.append(i);
      stringBuilder.append(",resourceId=");
      i = this.mResourceId;
      stringBuilder.append(i);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  public UdpEncapsulationSocket openUdpEncapsulationSocket(int paramInt) throws IOException, ResourceUnavailableException {
    if (paramInt != 0)
      try {
        return new UdpEncapsulationSocket(this.mService, paramInt);
      } catch (ServiceSpecificException serviceSpecificException) {
        throw rethrowCheckedExceptionFromServiceSpecificException(serviceSpecificException);
      }  
    throw new IllegalArgumentException("Specified port must be a valid port number!");
  }
  
  public UdpEncapsulationSocket openUdpEncapsulationSocket() throws IOException, ResourceUnavailableException {
    try {
      return new UdpEncapsulationSocket(this.mService, 0);
    } catch (ServiceSpecificException serviceSpecificException) {
      throw rethrowCheckedExceptionFromServiceSpecificException(serviceSpecificException);
    } 
  }
  
  @SystemApi
  public static final class IpSecTunnelInterface implements AutoCloseable {
    private final CloseGuard mCloseGuard = CloseGuard.get();
    
    private String mInterfaceName;
    
    private final InetAddress mLocalAddress;
    
    private final String mOpPackageName;
    
    private final InetAddress mRemoteAddress;
    
    private int mResourceId = -1;
    
    private final IIpSecService mService;
    
    private final Network mUnderlyingNetwork;
    
    public String getInterfaceName() {
      return this.mInterfaceName;
    }
    
    @SystemApi
    public void addAddress(InetAddress param1InetAddress, int param1Int) throws IOException {
      try {
        IIpSecService iIpSecService = this.mService;
        int i = this.mResourceId;
        LinkAddress linkAddress = new LinkAddress();
        this(param1InetAddress, param1Int);
        iIpSecService.addAddressToTunnelInterface(i, linkAddress, this.mOpPackageName);
        return;
      } catch (ServiceSpecificException serviceSpecificException) {
        throw IpSecManager.rethrowCheckedExceptionFromServiceSpecificException(serviceSpecificException);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    @SystemApi
    public void removeAddress(InetAddress param1InetAddress, int param1Int) throws IOException {
      try {
        IIpSecService iIpSecService = this.mService;
        int i = this.mResourceId;
        LinkAddress linkAddress = new LinkAddress();
        this(param1InetAddress, param1Int);
        iIpSecService.removeAddressFromTunnelInterface(i, linkAddress, this.mOpPackageName);
        return;
      } catch (ServiceSpecificException serviceSpecificException) {
        throw IpSecManager.rethrowCheckedExceptionFromServiceSpecificException(serviceSpecificException);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    private IpSecTunnelInterface(Context param1Context, IIpSecService param1IIpSecService, InetAddress param1InetAddress1, InetAddress param1InetAddress2, Network param1Network) throws IpSecManager.ResourceUnavailableException, IOException {
      this.mOpPackageName = param1Context.getOpPackageName();
      this.mService = param1IIpSecService;
      this.mLocalAddress = param1InetAddress1;
      this.mRemoteAddress = param1InetAddress2;
      this.mUnderlyingNetwork = param1Network;
      try {
        String str1 = param1InetAddress1.getHostAddress();
        String str2 = param1InetAddress2.getHostAddress();
        Binder binder = new Binder();
        this();
        String str3 = this.mOpPackageName;
        IpSecTunnelInterfaceResponse ipSecTunnelInterfaceResponse = param1IIpSecService.createTunnelInterface(str1, str2, param1Network, binder, str3);
        int i = ipSecTunnelInterfaceResponse.status;
        if (i != 0) {
          if (i != 1) {
            RuntimeException runtimeException = new RuntimeException();
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Unknown status returned by IpSecService: ");
            stringBuilder.append(ipSecTunnelInterfaceResponse.status);
            this(stringBuilder.toString());
            throw runtimeException;
          } 
          IpSecManager.ResourceUnavailableException resourceUnavailableException = new IpSecManager.ResourceUnavailableException();
          this("No more tunnel interfaces may be allocated by this requester.");
          throw resourceUnavailableException;
        } 
        this.mResourceId = ipSecTunnelInterfaceResponse.resourceId;
        this.mInterfaceName = ipSecTunnelInterfaceResponse.interfaceName;
        this.mCloseGuard.open("constructor");
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    public void close() {
      Exception exception;
      try {
        this.mService.deleteTunnelInterface(this.mResourceId, this.mOpPackageName);
        this.mResourceId = -1;
        this.mCloseGuard.close();
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } catch (Exception null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Failed to close ");
        stringBuilder.append(this);
        stringBuilder.append(", Exception=");
        stringBuilder.append(exception);
        Log.e("IpSecManager", stringBuilder.toString());
        this.mResourceId = -1;
        this.mCloseGuard.close();
        return;
      } finally {}
      this.mResourceId = -1;
      this.mCloseGuard.close();
      throw exception;
    }
    
    protected void finalize() throws Throwable {
      CloseGuard closeGuard = this.mCloseGuard;
      if (closeGuard != null)
        closeGuard.warnIfOpen(); 
      close();
    }
    
    public int getResourceId() {
      return this.mResourceId;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("IpSecTunnelInterface{ifname=");
      String str = this.mInterfaceName;
      stringBuilder.append(str);
      stringBuilder.append(",resourceId=");
      int i = this.mResourceId;
      stringBuilder.append(i);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  @SystemApi
  public IpSecTunnelInterface createIpSecTunnelInterface(InetAddress paramInetAddress1, InetAddress paramInetAddress2, Network paramNetwork) throws ResourceUnavailableException, IOException {
    try {
      return new IpSecTunnelInterface(this.mContext, this.mService, paramInetAddress1, paramInetAddress2, paramNetwork);
    } catch (ServiceSpecificException serviceSpecificException) {
      throw rethrowCheckedExceptionFromServiceSpecificException(serviceSpecificException);
    } 
  }
  
  @SystemApi
  public void applyTunnelModeTransform(IpSecTunnelInterface paramIpSecTunnelInterface, int paramInt, IpSecTransform paramIpSecTransform) throws IOException {
    try {
      IIpSecService iIpSecService = this.mService;
      int i = paramIpSecTunnelInterface.getResourceId();
      int j = paramIpSecTransform.getResourceId();
      String str = this.mContext.getOpPackageName();
      iIpSecService.applyTunnelModeTransform(i, paramInt, j, str);
      return;
    } catch (ServiceSpecificException serviceSpecificException) {
      throw rethrowCheckedExceptionFromServiceSpecificException(serviceSpecificException);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public IpSecManager(Context paramContext, IIpSecService paramIIpSecService) {
    this.mContext = paramContext;
    this.mService = (IIpSecService)Preconditions.checkNotNull(paramIIpSecService, "missing service");
  }
  
  private static void maybeHandleServiceSpecificException(ServiceSpecificException paramServiceSpecificException) {
    if (paramServiceSpecificException.errorCode != OsConstants.EINVAL) {
      if (paramServiceSpecificException.errorCode != OsConstants.EAGAIN) {
        if (paramServiceSpecificException.errorCode != OsConstants.EOPNOTSUPP && paramServiceSpecificException.errorCode != OsConstants.EPROTONOSUPPORT)
          return; 
        throw new UnsupportedOperationException(paramServiceSpecificException);
      } 
      throw new IllegalStateException(paramServiceSpecificException);
    } 
    throw new IllegalArgumentException(paramServiceSpecificException);
  }
  
  static RuntimeException rethrowUncheckedExceptionFromServiceSpecificException(ServiceSpecificException paramServiceSpecificException) {
    maybeHandleServiceSpecificException(paramServiceSpecificException);
    throw new RuntimeException(paramServiceSpecificException);
  }
  
  static IOException rethrowCheckedExceptionFromServiceSpecificException(ServiceSpecificException paramServiceSpecificException) throws IOException {
    maybeHandleServiceSpecificException(paramServiceSpecificException);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("IpSec encountered errno=");
    stringBuilder.append(paramServiceSpecificException.errorCode);
    ErrnoException errnoException = new ErrnoException(stringBuilder.toString(), paramServiceSpecificException.errorCode);
    throw errnoException.rethrowAsIOException();
  }
  
  public static interface Status {
    public static final int OK = 0;
    
    public static final int RESOURCE_UNAVAILABLE = 1;
    
    public static final int SPI_UNAVAILABLE = 2;
  }
}
