package android.net.metrics;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import com.android.internal.util.MessageUtils;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public final class IpManagerEvent implements IpConnectivityLog.Event {
  public static final int COMPLETE_LIFECYCLE = 3;
  
  public IpManagerEvent(int paramInt, long paramLong) {
    this.eventType = paramInt;
    this.durationMs = paramLong;
  }
  
  private IpManagerEvent(Parcel paramParcel) {
    this.eventType = paramParcel.readInt();
    this.durationMs = paramParcel.readLong();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.eventType);
    paramParcel.writeLong(this.durationMs);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<IpManagerEvent> CREATOR = (Parcelable.Creator<IpManagerEvent>)new Object();
  
  public static final int ERROR_INTERFACE_NOT_FOUND = 8;
  
  public static final int ERROR_INVALID_PROVISIONING = 7;
  
  public static final int ERROR_STARTING_IPREACHABILITYMONITOR = 6;
  
  public static final int ERROR_STARTING_IPV4 = 4;
  
  public static final int ERROR_STARTING_IPV6 = 5;
  
  public static final int PROVISIONING_FAIL = 2;
  
  public static final int PROVISIONING_OK = 1;
  
  public final long durationMs;
  
  public final int eventType;
  
  public String toString() {
    SparseArray<String> sparseArray = Decoder.constants;
    int i = this.eventType;
    Object object = sparseArray.get(i);
    long l = this.durationMs;
    return String.format("IpManagerEvent(%s, %dms)", new Object[] { object, Long.valueOf(l) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool1 = false;
    if (paramObject == null || !paramObject.getClass().equals(IpManagerEvent.class))
      return false; 
    paramObject = paramObject;
    boolean bool2 = bool1;
    if (this.eventType == ((IpManagerEvent)paramObject).eventType) {
      bool2 = bool1;
      if (this.durationMs == ((IpManagerEvent)paramObject).durationMs)
        bool2 = true; 
    } 
    return bool2;
  }
  
  class Decoder {
    static final SparseArray<String> constants = MessageUtils.findMessageNames(new Class[] { IpManagerEvent.class }, new String[] { "PROVISIONING_", "COMPLETE_", "ERROR_" });
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class EventType implements Annotation {}
}
