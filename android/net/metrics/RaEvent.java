package android.net.metrics;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class RaEvent implements IpConnectivityLog.Event {
  public RaEvent(long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5, long paramLong6) {
    this.routerLifetime = paramLong1;
    this.prefixValidLifetime = paramLong2;
    this.prefixPreferredLifetime = paramLong3;
    this.routeInfoLifetime = paramLong4;
    this.rdnssLifetime = paramLong5;
    this.dnsslLifetime = paramLong6;
  }
  
  private RaEvent(Parcel paramParcel) {
    this.routerLifetime = paramParcel.readLong();
    this.prefixValidLifetime = paramParcel.readLong();
    this.prefixPreferredLifetime = paramParcel.readLong();
    this.routeInfoLifetime = paramParcel.readLong();
    this.rdnssLifetime = paramParcel.readLong();
    this.dnsslLifetime = paramParcel.readLong();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.routerLifetime);
    paramParcel.writeLong(this.prefixValidLifetime);
    paramParcel.writeLong(this.prefixPreferredLifetime);
    paramParcel.writeLong(this.routeInfoLifetime);
    paramParcel.writeLong(this.rdnssLifetime);
    paramParcel.writeLong(this.dnsslLifetime);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("RaEvent(lifetimes: ");
    long l = this.routerLifetime;
    stringBuilder.append(String.format("router=%ds, ", new Object[] { Long.valueOf(l) }));
    l = this.prefixValidLifetime;
    stringBuilder.append(String.format("prefix_valid=%ds, ", new Object[] { Long.valueOf(l) }));
    l = this.prefixPreferredLifetime;
    stringBuilder.append(String.format("prefix_preferred=%ds, ", new Object[] { Long.valueOf(l) }));
    l = this.routeInfoLifetime;
    stringBuilder.append(String.format("route_info=%ds, ", new Object[] { Long.valueOf(l) }));
    l = this.rdnssLifetime;
    stringBuilder.append(String.format("rdnss=%ds, ", new Object[] { Long.valueOf(l) }));
    l = this.dnsslLifetime;
    stringBuilder.append(String.format("dnssl=%ds)", new Object[] { Long.valueOf(l) }));
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool1 = false;
    if (paramObject == null || !paramObject.getClass().equals(RaEvent.class))
      return false; 
    paramObject = paramObject;
    boolean bool2 = bool1;
    if (this.routerLifetime == ((RaEvent)paramObject).routerLifetime) {
      bool2 = bool1;
      if (this.prefixValidLifetime == ((RaEvent)paramObject).prefixValidLifetime) {
        bool2 = bool1;
        if (this.prefixPreferredLifetime == ((RaEvent)paramObject).prefixPreferredLifetime) {
          bool2 = bool1;
          if (this.routeInfoLifetime == ((RaEvent)paramObject).routeInfoLifetime) {
            bool2 = bool1;
            if (this.rdnssLifetime == ((RaEvent)paramObject).rdnssLifetime) {
              bool2 = bool1;
              if (this.dnsslLifetime == ((RaEvent)paramObject).dnsslLifetime)
                bool2 = true; 
            } 
          } 
        } 
      } 
    } 
    return bool2;
  }
  
  public static final Parcelable.Creator<RaEvent> CREATOR = (Parcelable.Creator<RaEvent>)new Object();
  
  private static final long NO_LIFETIME = -1L;
  
  public final long dnsslLifetime;
  
  public final long prefixPreferredLifetime;
  
  public final long prefixValidLifetime;
  
  public final long rdnssLifetime;
  
  public final long routeInfoLifetime;
  
  public final long routerLifetime;
  
  class Builder {
    long routerLifetime = -1L;
    
    long prefixValidLifetime = -1L;
    
    long prefixPreferredLifetime = -1L;
    
    long routeInfoLifetime = -1L;
    
    long rdnssLifetime = -1L;
    
    long dnsslLifetime = -1L;
    
    public RaEvent build() {
      return new RaEvent(this.routerLifetime, this.prefixValidLifetime, this.prefixPreferredLifetime, this.routeInfoLifetime, this.rdnssLifetime, this.dnsslLifetime);
    }
    
    public Builder updateRouterLifetime(long param1Long) {
      this.routerLifetime = updateLifetime(this.routerLifetime, param1Long);
      return this;
    }
    
    public Builder updatePrefixValidLifetime(long param1Long) {
      this.prefixValidLifetime = updateLifetime(this.prefixValidLifetime, param1Long);
      return this;
    }
    
    public Builder updatePrefixPreferredLifetime(long param1Long) {
      this.prefixPreferredLifetime = updateLifetime(this.prefixPreferredLifetime, param1Long);
      return this;
    }
    
    public Builder updateRouteInfoLifetime(long param1Long) {
      this.routeInfoLifetime = updateLifetime(this.routeInfoLifetime, param1Long);
      return this;
    }
    
    public Builder updateRdnssLifetime(long param1Long) {
      this.rdnssLifetime = updateLifetime(this.rdnssLifetime, param1Long);
      return this;
    }
    
    public Builder updateDnsslLifetime(long param1Long) {
      this.dnsslLifetime = updateLifetime(this.dnsslLifetime, param1Long);
      return this;
    }
    
    private long updateLifetime(long param1Long1, long param1Long2) {
      if (param1Long1 == -1L)
        return param1Long2; 
      return Math.min(param1Long1, param1Long2);
    }
  }
}
