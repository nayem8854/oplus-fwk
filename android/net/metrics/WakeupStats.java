package android.net.metrics;

import android.os.SystemClock;
import android.util.SparseIntArray;
import java.util.StringJoiner;

public class WakeupStats {
  public final long creationTimeMs = SystemClock.elapsedRealtime();
  
  public long totalWakeups = 0L;
  
  public long rootWakeups = 0L;
  
  public long systemWakeups = 0L;
  
  public long nonApplicationWakeups = 0L;
  
  public long applicationWakeups = 0L;
  
  public long noUidWakeups = 0L;
  
  public long durationSec = 0L;
  
  public long l2UnicastCount = 0L;
  
  public long l2MulticastCount = 0L;
  
  public long l2BroadcastCount = 0L;
  
  public final SparseIntArray ethertypes = new SparseIntArray();
  
  public final SparseIntArray ipNextHeaders = new SparseIntArray();
  
  private static final int NO_UID = -1;
  
  public final String iface;
  
  public WakeupStats(String paramString) {
    this.iface = paramString;
  }
  
  public void updateDuration() {
    this.durationSec = (SystemClock.elapsedRealtime() - this.creationTimeMs) / 1000L;
  }
  
  public void countEvent(WakeupEvent paramWakeupEvent) {
    this.totalWakeups++;
    int i = paramWakeupEvent.uid;
    if (i != -1) {
      if (i != 0) {
        if (i != 1000) {
          if (paramWakeupEvent.uid >= 10000) {
            this.applicationWakeups++;
          } else {
            this.nonApplicationWakeups++;
          } 
        } else {
          this.systemWakeups++;
        } 
      } else {
        this.rootWakeups++;
      } 
    } else {
      this.noUidWakeups++;
    } 
    i = paramWakeupEvent.dstHwAddr.getAddressType();
    if (i != 1) {
      if (i != 2) {
        if (i == 3)
          this.l2BroadcastCount++; 
      } else {
        this.l2MulticastCount++;
      } 
    } else {
      this.l2UnicastCount++;
    } 
    increment(this.ethertypes, paramWakeupEvent.ethertype);
    if (paramWakeupEvent.ipNextHeader >= 0)
      increment(this.ipNextHeaders, paramWakeupEvent.ipNextHeader); 
  }
  
  public String toString() {
    updateDuration();
    StringJoiner stringJoiner = new StringJoiner(", ", "WakeupStats(", ")");
    stringJoiner.add(this.iface);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("");
    stringBuilder.append(this.durationSec);
    stringBuilder.append("s");
    stringJoiner.add(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("total: ");
    stringBuilder.append(this.totalWakeups);
    stringJoiner.add(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("root: ");
    stringBuilder.append(this.rootWakeups);
    stringJoiner.add(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("system: ");
    stringBuilder.append(this.systemWakeups);
    stringJoiner.add(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("apps: ");
    stringBuilder.append(this.applicationWakeups);
    stringJoiner.add(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("non-apps: ");
    stringBuilder.append(this.nonApplicationWakeups);
    stringJoiner.add(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("no uid: ");
    stringBuilder.append(this.noUidWakeups);
    stringJoiner.add(stringBuilder.toString());
    long l1 = this.l2UnicastCount;
    long l2 = this.l2MulticastCount, l3 = this.l2BroadcastCount;
    stringJoiner.add(String.format("l2 unicast/multicast/broadcast: %d/%d/%d", new Object[] { Long.valueOf(l1), Long.valueOf(l2), Long.valueOf(l3) }));
    byte b;
    for (b = 0; b < this.ethertypes.size(); b++) {
      int i = this.ethertypes.keyAt(b);
      int j = this.ethertypes.valueAt(b);
      stringJoiner.add(String.format("ethertype 0x%x: %d", new Object[] { Integer.valueOf(i), Integer.valueOf(j) }));
    } 
    for (b = 0; b < this.ipNextHeaders.size(); b++) {
      int i = this.ipNextHeaders.keyAt(b);
      int j = this.ipNextHeaders.valueAt(b);
      stringJoiner.add(String.format("ipNxtHdr %d: %d", new Object[] { Integer.valueOf(i), Integer.valueOf(j) }));
    } 
    return stringJoiner.toString();
  }
  
  private static void increment(SparseIntArray paramSparseIntArray, int paramInt) {
    int i = paramSparseIntArray.get(paramInt, 0);
    paramSparseIntArray.put(paramInt, i + 1);
  }
}
