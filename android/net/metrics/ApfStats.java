package android.net.metrics;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class ApfStats implements IpConnectivityLog.Event {
  private ApfStats(Parcel paramParcel) {
    this.durationMs = paramParcel.readLong();
    this.receivedRas = paramParcel.readInt();
    this.matchingRas = paramParcel.readInt();
    this.droppedRas = paramParcel.readInt();
    this.zeroLifetimeRas = paramParcel.readInt();
    this.parseErrors = paramParcel.readInt();
    this.programUpdates = paramParcel.readInt();
    this.programUpdatesAll = paramParcel.readInt();
    this.programUpdatesAllowingMulticast = paramParcel.readInt();
    this.maxProgramSize = paramParcel.readInt();
  }
  
  private ApfStats(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9) {
    this.durationMs = paramLong;
    this.receivedRas = paramInt1;
    this.matchingRas = paramInt2;
    this.droppedRas = paramInt3;
    this.zeroLifetimeRas = paramInt4;
    this.parseErrors = paramInt5;
    this.programUpdates = paramInt6;
    this.programUpdatesAll = paramInt7;
    this.programUpdatesAllowingMulticast = paramInt8;
    this.maxProgramSize = paramInt9;
  }
  
  @SystemApi
  class Builder {
    private int mDroppedRas;
    
    private long mDurationMs;
    
    private int mMatchingRas;
    
    private int mMaxProgramSize;
    
    private int mParseErrors;
    
    private int mProgramUpdates;
    
    private int mProgramUpdatesAll;
    
    private int mProgramUpdatesAllowingMulticast;
    
    private int mReceivedRas;
    
    private int mZeroLifetimeRas;
    
    public Builder setDurationMs(long param1Long) {
      this.mDurationMs = param1Long;
      return this;
    }
    
    public Builder setReceivedRas(int param1Int) {
      this.mReceivedRas = param1Int;
      return this;
    }
    
    public Builder setMatchingRas(int param1Int) {
      this.mMatchingRas = param1Int;
      return this;
    }
    
    public Builder setDroppedRas(int param1Int) {
      this.mDroppedRas = param1Int;
      return this;
    }
    
    public Builder setZeroLifetimeRas(int param1Int) {
      this.mZeroLifetimeRas = param1Int;
      return this;
    }
    
    public Builder setParseErrors(int param1Int) {
      this.mParseErrors = param1Int;
      return this;
    }
    
    public Builder setProgramUpdates(int param1Int) {
      this.mProgramUpdates = param1Int;
      return this;
    }
    
    public Builder setProgramUpdatesAll(int param1Int) {
      this.mProgramUpdatesAll = param1Int;
      return this;
    }
    
    public Builder setProgramUpdatesAllowingMulticast(int param1Int) {
      this.mProgramUpdatesAllowingMulticast = param1Int;
      return this;
    }
    
    public Builder setMaxProgramSize(int param1Int) {
      this.mMaxProgramSize = param1Int;
      return this;
    }
    
    public ApfStats build() {
      return new ApfStats(this.mDurationMs, this.mReceivedRas, this.mMatchingRas, this.mDroppedRas, this.mZeroLifetimeRas, this.mParseErrors, this.mProgramUpdates, this.mProgramUpdatesAll, this.mProgramUpdatesAllowingMulticast, this.mMaxProgramSize);
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.durationMs);
    paramParcel.writeInt(this.receivedRas);
    paramParcel.writeInt(this.matchingRas);
    paramParcel.writeInt(this.droppedRas);
    paramParcel.writeInt(this.zeroLifetimeRas);
    paramParcel.writeInt(this.parseErrors);
    paramParcel.writeInt(this.programUpdates);
    paramParcel.writeInt(this.programUpdatesAll);
    paramParcel.writeInt(this.programUpdatesAllowingMulticast);
    paramParcel.writeInt(this.maxProgramSize);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("ApfStats(");
    long l = this.durationMs;
    stringBuilder.append(String.format("%dms ", new Object[] { Long.valueOf(l) }));
    int i = this.maxProgramSize;
    stringBuilder.append(String.format("%dB RA: {", new Object[] { Integer.valueOf(i) }));
    i = this.receivedRas;
    stringBuilder.append(String.format("%d received, ", new Object[] { Integer.valueOf(i) }));
    i = this.matchingRas;
    stringBuilder.append(String.format("%d matching, ", new Object[] { Integer.valueOf(i) }));
    i = this.droppedRas;
    stringBuilder.append(String.format("%d dropped, ", new Object[] { Integer.valueOf(i) }));
    i = this.zeroLifetimeRas;
    stringBuilder.append(String.format("%d zero lifetime, ", new Object[] { Integer.valueOf(i) }));
    i = this.parseErrors;
    stringBuilder.append(String.format("%d parse errors}, ", new Object[] { Integer.valueOf(i) }));
    int j = this.programUpdatesAll;
    int k = this.programUpdates;
    i = this.programUpdatesAllowingMulticast;
    stringBuilder.append(String.format("updates: {all: %d, RAs: %d, allow multicast: %d})", new Object[] { Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(i) }));
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool1 = false;
    if (paramObject == null || !paramObject.getClass().equals(ApfStats.class))
      return false; 
    paramObject = paramObject;
    boolean bool2 = bool1;
    if (this.durationMs == ((ApfStats)paramObject).durationMs) {
      bool2 = bool1;
      if (this.receivedRas == ((ApfStats)paramObject).receivedRas) {
        bool2 = bool1;
        if (this.matchingRas == ((ApfStats)paramObject).matchingRas) {
          bool2 = bool1;
          if (this.droppedRas == ((ApfStats)paramObject).droppedRas) {
            bool2 = bool1;
            if (this.zeroLifetimeRas == ((ApfStats)paramObject).zeroLifetimeRas) {
              bool2 = bool1;
              if (this.parseErrors == ((ApfStats)paramObject).parseErrors) {
                bool2 = bool1;
                if (this.programUpdates == ((ApfStats)paramObject).programUpdates) {
                  bool2 = bool1;
                  if (this.programUpdatesAll == ((ApfStats)paramObject).programUpdatesAll) {
                    bool2 = bool1;
                    if (this.programUpdatesAllowingMulticast == ((ApfStats)paramObject).programUpdatesAllowingMulticast) {
                      bool2 = bool1;
                      if (this.maxProgramSize == ((ApfStats)paramObject).maxProgramSize)
                        bool2 = true; 
                    } 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
    } 
    return bool2;
  }
  
  public static final Parcelable.Creator<ApfStats> CREATOR = (Parcelable.Creator<ApfStats>)new Object();
  
  public final int droppedRas;
  
  public final long durationMs;
  
  public final int matchingRas;
  
  public final int maxProgramSize;
  
  public final int parseErrors;
  
  public final int programUpdates;
  
  public final int programUpdatesAll;
  
  public final int programUpdatesAllowingMulticast;
  
  public final int receivedRas;
  
  public final int zeroLifetimeRas;
}
