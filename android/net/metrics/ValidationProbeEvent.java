package android.net.metrics;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import com.android.internal.util.MessageUtils;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public final class ValidationProbeEvent implements IpConnectivityLog.Event {
  private ValidationProbeEvent(long paramLong, int paramInt1, int paramInt2) {
    this.durationMs = paramLong;
    this.probeType = paramInt1;
    this.returnCode = paramInt2;
  }
  
  private ValidationProbeEvent(Parcel paramParcel) {
    this.durationMs = paramParcel.readLong();
    this.probeType = paramParcel.readInt();
    this.returnCode = paramParcel.readInt();
  }
  
  class Builder {
    private long mDurationMs;
    
    private int mProbeType;
    
    private int mReturnCode;
    
    public Builder setDurationMs(long param1Long) {
      this.mDurationMs = param1Long;
      return this;
    }
    
    public Builder setProbeType(int param1Int, boolean param1Boolean) {
      this.mProbeType = ValidationProbeEvent.makeProbeType(param1Int, param1Boolean);
      return this;
    }
    
    public Builder setReturnCode(int param1Int) {
      this.mReturnCode = param1Int;
      return this;
    }
    
    public ValidationProbeEvent build() {
      return new ValidationProbeEvent(this.mDurationMs, this.mProbeType, this.mReturnCode);
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.durationMs);
    paramParcel.writeInt(this.probeType);
    paramParcel.writeInt(this.returnCode);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<ValidationProbeEvent> CREATOR = (Parcelable.Creator<ValidationProbeEvent>)new Object();
  
  public static final int DNS_FAILURE = 0;
  
  public static final int DNS_SUCCESS = 1;
  
  private static final int FIRST_VALIDATION = 256;
  
  public static final int PROBE_DNS = 0;
  
  public static final int PROBE_FALLBACK = 4;
  
  public static final int PROBE_HTTP = 1;
  
  public static final int PROBE_HTTPS = 2;
  
  public static final int PROBE_PAC = 3;
  
  public static final int PROBE_PRIVDNS = 5;
  
  private static final int REVALIDATION = 512;
  
  public final long durationMs;
  
  public final int probeType;
  
  public final int returnCode;
  
  private static int makeProbeType(int paramInt, boolean paramBoolean) {
    char c;
    if (paramBoolean) {
      c = 'Ā';
    } else {
      c = 'Ȁ';
    } 
    return paramInt & 0xFF | c;
  }
  
  public static String getProbeName(int paramInt) {
    return (String)Decoder.constants.get(paramInt & 0xFF, "PROBE_???");
  }
  
  private static String getValidationStage(int paramInt) {
    return (String)Decoder.constants.get(0xFF00 & paramInt, "UNKNOWN");
  }
  
  public String toString() {
    int i = this.probeType;
    String str1 = getProbeName(i);
    i = this.returnCode;
    String str2 = getValidationStage(this.probeType);
    long l = this.durationMs;
    return String.format("ValidationProbeEvent(%s:%d %s, %dms)", new Object[] { str1, Integer.valueOf(i), str2, Long.valueOf(l) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool1 = false;
    if (paramObject == null || !paramObject.getClass().equals(ValidationProbeEvent.class))
      return false; 
    paramObject = paramObject;
    boolean bool2 = bool1;
    if (this.durationMs == ((ValidationProbeEvent)paramObject).durationMs) {
      bool2 = bool1;
      if (this.probeType == ((ValidationProbeEvent)paramObject).probeType) {
        bool2 = bool1;
        if (this.returnCode == ((ValidationProbeEvent)paramObject).returnCode)
          bool2 = true; 
      } 
    } 
    return bool2;
  }
  
  class Decoder {
    static final SparseArray<String> constants = MessageUtils.findMessageNames(new Class[] { ValidationProbeEvent.class }, new String[] { "PROBE_", "FIRST_", "REVALIDATION" });
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ReturnCode implements Annotation {}
}
