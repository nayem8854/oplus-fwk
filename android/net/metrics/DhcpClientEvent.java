package android.net.metrics;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

@SystemApi
public final class DhcpClientEvent implements IpConnectivityLog.Event {
  private DhcpClientEvent(String paramString, int paramInt) {
    this.msg = paramString;
    this.durationMs = paramInt;
  }
  
  private DhcpClientEvent(Parcel paramParcel) {
    this.msg = paramParcel.readString();
    this.durationMs = paramParcel.readInt();
  }
  
  class Builder {
    private int mDurationMs;
    
    private String mMsg;
    
    public Builder setMsg(String param1String) {
      this.mMsg = param1String;
      return this;
    }
    
    public Builder setDurationMs(int param1Int) {
      this.mDurationMs = param1Int;
      return this;
    }
    
    public DhcpClientEvent build() {
      return new DhcpClientEvent(this.mMsg, this.mDurationMs);
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.msg);
    paramParcel.writeInt(this.durationMs);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    return String.format("DhcpClientEvent(%s, %dms)", new Object[] { this.msg, Integer.valueOf(this.durationMs) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool1 = false;
    if (paramObject == null || !paramObject.getClass().equals(DhcpClientEvent.class))
      return false; 
    paramObject = paramObject;
    boolean bool2 = bool1;
    if (TextUtils.equals(this.msg, ((DhcpClientEvent)paramObject).msg)) {
      bool2 = bool1;
      if (this.durationMs == ((DhcpClientEvent)paramObject).durationMs)
        bool2 = true; 
    } 
    return bool2;
  }
  
  public static final Parcelable.Creator<DhcpClientEvent> CREATOR = (Parcelable.Creator<DhcpClientEvent>)new Object();
  
  public final int durationMs;
  
  public final String msg;
}
