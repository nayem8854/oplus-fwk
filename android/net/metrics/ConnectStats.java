package android.net.metrics;

import android.net.NetworkCapabilities;
import android.system.OsConstants;
import android.util.IntArray;
import android.util.SparseIntArray;
import com.android.internal.util.BitUtils;
import com.android.internal.util.TokenBucket;

public class ConnectStats {
  private static final int EALREADY = OsConstants.EALREADY;
  
  private static final int EINPROGRESS = OsConstants.EINPROGRESS;
  
  public final SparseIntArray errnos = new SparseIntArray();
  
  public final IntArray latencies = new IntArray();
  
  public int eventCount = 0;
  
  public int connectCount = 0;
  
  public int connectBlockingCount = 0;
  
  public int ipv6ConnectCount = 0;
  
  public final TokenBucket mLatencyTb;
  
  public final int mMaxLatencyRecords;
  
  public final int netId;
  
  public final long transports;
  
  public ConnectStats(int paramInt1, long paramLong, TokenBucket paramTokenBucket, int paramInt2) {
    this.netId = paramInt1;
    this.transports = paramLong;
    this.mLatencyTb = paramTokenBucket;
    this.mMaxLatencyRecords = paramInt2;
  }
  
  boolean addEvent(int paramInt1, int paramInt2, String paramString) {
    this.eventCount++;
    if (isSuccess(paramInt1)) {
      countConnect(paramInt1, paramString);
      countLatency(paramInt1, paramInt2);
      return true;
    } 
    countError(paramInt1);
    return false;
  }
  
  private void countConnect(int paramInt, String paramString) {
    this.connectCount++;
    if (!isNonBlocking(paramInt))
      this.connectBlockingCount++; 
    if (isIPv6(paramString))
      this.ipv6ConnectCount++; 
  }
  
  private void countLatency(int paramInt1, int paramInt2) {
    if (isNonBlocking(paramInt1))
      return; 
    if (!this.mLatencyTb.get())
      return; 
    if (this.latencies.size() >= this.mMaxLatencyRecords)
      return; 
    this.latencies.add(paramInt2);
  }
  
  private void countError(int paramInt) {
    int i = this.errnos.get(paramInt, 0);
    this.errnos.put(paramInt, i + 1);
  }
  
  private static boolean isSuccess(int paramInt) {
    return (paramInt == 0 || isNonBlocking(paramInt));
  }
  
  static boolean isNonBlocking(int paramInt) {
    return (paramInt == EINPROGRESS || paramInt == EALREADY);
  }
  
  private static boolean isIPv6(String paramString) {
    return paramString.contains(":");
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("ConnectStats(");
    stringBuilder.append("netId=");
    stringBuilder.append(this.netId);
    stringBuilder = stringBuilder.append(", ");
    for (int i : BitUtils.unpackBits(this.transports)) {
      stringBuilder.append(NetworkCapabilities.transportNameOf(i));
      stringBuilder.append(", ");
    } 
    stringBuilder.append(String.format("%d events, ", new Object[] { Integer.valueOf(this.eventCount) }));
    stringBuilder.append(String.format("%d success, ", new Object[] { Integer.valueOf(this.connectCount) }));
    stringBuilder.append(String.format("%d blocking, ", new Object[] { Integer.valueOf(this.connectBlockingCount) }));
    stringBuilder.append(String.format("%d IPv6 dst", new Object[] { Integer.valueOf(this.ipv6ConnectCount) }));
    for (byte b = 0; b < this.errnos.size(); b++) {
      String str = OsConstants.errnoName(this.errnos.keyAt(b));
      int i = this.errnos.valueAt(b);
      stringBuilder.append(String.format(", %s: %d", new Object[] { str, Integer.valueOf(i) }));
    } 
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
}
