package android.net.metrics;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import com.android.internal.util.MessageUtils;

@SystemApi
public final class IpReachabilityEvent implements IpConnectivityLog.Event {
  public IpReachabilityEvent(int paramInt) {
    this.eventType = paramInt;
  }
  
  private IpReachabilityEvent(Parcel paramParcel) {
    this.eventType = paramParcel.readInt();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.eventType);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<IpReachabilityEvent> CREATOR = (Parcelable.Creator<IpReachabilityEvent>)new Object();
  
  public static final int NUD_FAILED = 512;
  
  public static final int NUD_FAILED_ORGANIC = 1024;
  
  public static final int PROBE = 256;
  
  public static final int PROVISIONING_LOST = 768;
  
  public static final int PROVISIONING_LOST_ORGANIC = 1280;
  
  public final int eventType;
  
  public String toString() {
    int i = this.eventType;
    String str = (String)Decoder.constants.get(0xFF00 & i);
    return String.format("IpReachabilityEvent(%s:%02x)", new Object[] { str, Integer.valueOf(i & 0xFF) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    if (paramObject == null || !paramObject.getClass().equals(IpReachabilityEvent.class))
      return false; 
    paramObject = paramObject;
    if (this.eventType == ((IpReachabilityEvent)paramObject).eventType)
      bool = true; 
    return bool;
  }
  
  class Decoder {
    static final SparseArray<String> constants = MessageUtils.findMessageNames(new Class[] { IpReachabilityEvent.class }, new String[] { "PROBE", "PROVISIONING_", "NUD_" });
  }
}
