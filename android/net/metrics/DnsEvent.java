package android.net.metrics;

import android.net.NetworkCapabilities;
import com.android.internal.util.BitUtils;
import java.util.Arrays;

public final class DnsEvent {
  private static final int SIZE_LIMIT = 20000;
  
  public int eventCount;
  
  public byte[] eventTypes;
  
  public int[] latenciesMs;
  
  public final int netId;
  
  public byte[] returnCodes;
  
  public int successCount;
  
  public final long transports;
  
  public DnsEvent(int paramInt1, long paramLong, int paramInt2) {
    this.netId = paramInt1;
    this.transports = paramLong;
    this.eventTypes = new byte[paramInt2];
    this.returnCodes = new byte[paramInt2];
    this.latenciesMs = new int[paramInt2];
  }
  
  boolean addResult(byte paramByte1, byte paramByte2, int paramInt) {
    boolean bool;
    if (paramByte2 == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    int i = this.eventCount;
    if (i >= 20000)
      return bool; 
    if (i == this.eventTypes.length)
      resize((int)(i * 1.4D)); 
    byte[] arrayOfByte = this.eventTypes;
    i = this.eventCount;
    arrayOfByte[i] = paramByte1;
    this.returnCodes[i] = paramByte2;
    this.latenciesMs[i] = paramInt;
    this.eventCount = i + 1;
    if (bool)
      this.successCount++; 
    return bool;
  }
  
  public void resize(int paramInt) {
    this.eventTypes = Arrays.copyOf(this.eventTypes, paramInt);
    this.returnCodes = Arrays.copyOf(this.returnCodes, paramInt);
    this.latenciesMs = Arrays.copyOf(this.latenciesMs, paramInt);
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder("DnsEvent(");
    stringBuilder1.append("netId=");
    stringBuilder1.append(this.netId);
    StringBuilder stringBuilder2 = stringBuilder1.append(", ");
    for (int i : BitUtils.unpackBits(this.transports)) {
      stringBuilder2.append(NetworkCapabilities.transportNameOf(i));
      stringBuilder2.append(", ");
    } 
    stringBuilder2.append(String.format("%d events, ", new Object[] { Integer.valueOf(this.eventCount) }));
    stringBuilder2.append(String.format("%d success)", new Object[] { Integer.valueOf(this.successCount) }));
    return stringBuilder2.toString();
  }
}
