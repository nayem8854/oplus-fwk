package android.net.metrics;

import android.annotation.SystemApi;
import android.net.ConnectivityMetricsEvent;
import android.net.IIpConnectivityMetrics;
import android.net.Network;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import com.android.internal.util.BitUtils;

@SystemApi
public class IpConnectivityLog {
  private static final boolean DBG = false;
  
  public static final String SERVICE_NAME = "connmetrics";
  
  private static final String TAG = IpConnectivityLog.class.getSimpleName();
  
  private IIpConnectivityMetrics mService;
  
  @SystemApi
  public IpConnectivityLog() {}
  
  public IpConnectivityLog(IIpConnectivityMetrics paramIIpConnectivityMetrics) {
    this.mService = paramIIpConnectivityMetrics;
  }
  
  private boolean checkLoggerService() {
    if (this.mService != null)
      return true; 
    IIpConnectivityMetrics iIpConnectivityMetrics = IIpConnectivityMetrics.Stub.asInterface(ServiceManager.getService("connmetrics"));
    if (iIpConnectivityMetrics == null)
      return false; 
    this.mService = iIpConnectivityMetrics;
    return true;
  }
  
  public boolean log(ConnectivityMetricsEvent paramConnectivityMetricsEvent) {
    boolean bool = checkLoggerService();
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (paramConnectivityMetricsEvent.timestamp == 0L)
      paramConnectivityMetricsEvent.timestamp = System.currentTimeMillis(); 
    try {
      int i = this.mService.logEvent(paramConnectivityMetricsEvent);
      if (i >= 0)
        bool1 = true; 
      return bool1;
    } catch (RemoteException remoteException) {
      Log.e(TAG, "Error logging event", (Throwable)remoteException);
      return false;
    } 
  }
  
  public boolean log(long paramLong, Event paramEvent) {
    ConnectivityMetricsEvent connectivityMetricsEvent = makeEv(paramEvent);
    connectivityMetricsEvent.timestamp = paramLong;
    return log(connectivityMetricsEvent);
  }
  
  public boolean log(String paramString, Event paramEvent) {
    ConnectivityMetricsEvent connectivityMetricsEvent = makeEv(paramEvent);
    connectivityMetricsEvent.ifname = paramString;
    return log(connectivityMetricsEvent);
  }
  
  public boolean log(Network paramNetwork, int[] paramArrayOfint, Event paramEvent) {
    return log(paramNetwork.netId, paramArrayOfint, paramEvent);
  }
  
  public boolean log(int paramInt, int[] paramArrayOfint, Event paramEvent) {
    ConnectivityMetricsEvent connectivityMetricsEvent = makeEv(paramEvent);
    connectivityMetricsEvent.netId = paramInt;
    connectivityMetricsEvent.transports = BitUtils.packBits(paramArrayOfint);
    return log(connectivityMetricsEvent);
  }
  
  public boolean log(Event paramEvent) {
    return log(makeEv(paramEvent));
  }
  
  private static ConnectivityMetricsEvent makeEv(Event paramEvent) {
    ConnectivityMetricsEvent connectivityMetricsEvent = new ConnectivityMetricsEvent();
    connectivityMetricsEvent.data = paramEvent;
    return connectivityMetricsEvent;
  }
  
  class Event implements Parcelable {}
}
