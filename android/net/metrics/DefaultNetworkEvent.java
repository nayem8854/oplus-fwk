package android.net.metrics;

import android.net.NetworkCapabilities;
import com.android.internal.util.BitUtils;
import java.util.StringJoiner;

public class DefaultNetworkEvent {
  public final long creationTimeMs;
  
  public long durationMs;
  
  public int finalScore;
  
  public int initialScore;
  
  public boolean ipv4;
  
  public boolean ipv6;
  
  public int netId = 0;
  
  public int previousTransports;
  
  public int transports;
  
  public long validatedMs;
  
  public DefaultNetworkEvent(long paramLong) {
    this.creationTimeMs = paramLong;
  }
  
  public void updateDuration(long paramLong) {
    this.durationMs = paramLong - this.creationTimeMs;
  }
  
  public String toString() {
    StringJoiner stringJoiner = new StringJoiner(", ", "DefaultNetworkEvent(", ")");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("netId=");
    stringBuilder.append(this.netId);
    stringJoiner.add(stringBuilder.toString());
    for (int i : BitUtils.unpackBits(this.transports))
      stringJoiner.add(NetworkCapabilities.transportNameOf(i)); 
    stringBuilder = new StringBuilder();
    stringBuilder.append("ip=");
    stringBuilder.append(ipSupport());
    stringJoiner.add(stringBuilder.toString());
    if (this.initialScore > 0) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("initial_score=");
      stringBuilder.append(this.initialScore);
      stringJoiner.add(stringBuilder.toString());
    } 
    if (this.finalScore > 0) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("final_score=");
      stringBuilder.append(this.finalScore);
      stringJoiner.add(stringBuilder.toString());
    } 
    stringJoiner.add(String.format("duration=%.0fs", new Object[] { Double.valueOf(this.durationMs / 1000.0D) }));
    stringJoiner.add(String.format("validation=%04.1f%%", new Object[] { Double.valueOf(this.validatedMs * 100.0D / this.durationMs) }));
    return stringJoiner.toString();
  }
  
  private String ipSupport() {
    if (this.ipv4 && this.ipv6)
      return "IPv4v6"; 
    if (this.ipv6)
      return "IPv6"; 
    if (this.ipv4)
      return "IPv4"; 
    return "NONE";
  }
}
