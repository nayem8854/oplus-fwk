package android.net.metrics;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import com.android.internal.util.MessageUtils;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public final class NetworkEvent implements IpConnectivityLog.Event {
  public NetworkEvent(int paramInt, long paramLong) {
    this.eventType = paramInt;
    this.durationMs = paramLong;
  }
  
  public NetworkEvent(int paramInt) {
    this(paramInt, 0L);
  }
  
  private NetworkEvent(Parcel paramParcel) {
    this.eventType = paramParcel.readInt();
    this.durationMs = paramParcel.readLong();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.eventType);
    paramParcel.writeLong(this.durationMs);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<NetworkEvent> CREATOR = (Parcelable.Creator<NetworkEvent>)new Object();
  
  public static final int NETWORK_CAPTIVE_PORTAL_FOUND = 4;
  
  public static final int NETWORK_CONNECTED = 1;
  
  public static final int NETWORK_CONSECUTIVE_DNS_TIMEOUT_FOUND = 12;
  
  public static final int NETWORK_DISCONNECTED = 7;
  
  public static final int NETWORK_FIRST_VALIDATION_PORTAL_FOUND = 10;
  
  public static final int NETWORK_FIRST_VALIDATION_SUCCESS = 8;
  
  public static final int NETWORK_LINGER = 5;
  
  public static final int NETWORK_PARTIAL_CONNECTIVITY = 13;
  
  public static final int NETWORK_REVALIDATION_PORTAL_FOUND = 11;
  
  public static final int NETWORK_REVALIDATION_SUCCESS = 9;
  
  public static final int NETWORK_UNLINGER = 6;
  
  public static final int NETWORK_VALIDATED = 2;
  
  public static final int NETWORK_VALIDATION_FAILED = 3;
  
  public final long durationMs;
  
  public final int eventType;
  
  public String toString() {
    SparseArray<String> sparseArray = Decoder.constants;
    int i = this.eventType;
    Object object = sparseArray.get(i);
    long l = this.durationMs;
    return String.format("NetworkEvent(%s, %dms)", new Object[] { object, Long.valueOf(l) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool1 = false;
    if (paramObject == null || !paramObject.getClass().equals(NetworkEvent.class))
      return false; 
    paramObject = paramObject;
    boolean bool2 = bool1;
    if (this.eventType == ((NetworkEvent)paramObject).eventType) {
      bool2 = bool1;
      if (this.durationMs == ((NetworkEvent)paramObject).durationMs)
        bool2 = true; 
    } 
    return bool2;
  }
  
  class Decoder {
    static final SparseArray<String> constants = MessageUtils.findMessageNames(new Class[] { NetworkEvent.class }, new String[] { "NETWORK_" });
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class EventType implements Annotation {}
}
