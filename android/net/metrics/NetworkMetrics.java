package android.net.metrics;

import android.net.NetworkCapabilities;
import com.android.internal.util.BitUtils;
import com.android.internal.util.TokenBucket;
import java.util.StringJoiner;

public class NetworkMetrics {
  private static final int CONNECT_LATENCY_MAXIMUM_RECORDS = 20000;
  
  private static final int INITIAL_DNS_BATCH_SIZE = 100;
  
  public final ConnectStats connectMetrics;
  
  public final DnsEvent dnsMetrics;
  
  public final int netId;
  
  public Summary pendingSummary;
  
  public final Summary summary;
  
  public final long transports;
  
  public NetworkMetrics(int paramInt, long paramLong, TokenBucket paramTokenBucket) {
    this.netId = paramInt;
    this.transports = paramLong;
    this.connectMetrics = new ConnectStats(paramInt, paramLong, paramTokenBucket, 20000);
    this.dnsMetrics = new DnsEvent(paramInt, paramLong, 100);
    this.summary = new Summary(paramInt, paramLong);
  }
  
  public Summary getPendingStats() {
    Summary summary = this.pendingSummary;
    this.pendingSummary = null;
    if (summary != null)
      this.summary.merge(summary); 
    return summary;
  }
  
  public void addDnsResult(int paramInt1, int paramInt2, int paramInt3) {
    double d;
    if (this.pendingSummary == null)
      this.pendingSummary = new Summary(this.netId, this.transports); 
    boolean bool = this.dnsMetrics.addResult((byte)paramInt1, (byte)paramInt2, paramInt3);
    this.pendingSummary.dnsLatencies.count(paramInt3);
    Metrics metrics = this.pendingSummary.dnsErrorRate;
    if (bool) {
      d = 0.0D;
    } else {
      d = 1.0D;
    } 
    metrics.count(d);
  }
  
  public void addConnectResult(int paramInt1, int paramInt2, String paramString) {
    double d;
    if (this.pendingSummary == null)
      this.pendingSummary = new Summary(this.netId, this.transports); 
    boolean bool = this.connectMetrics.addEvent(paramInt1, paramInt2, paramString);
    Metrics metrics = this.pendingSummary.connectErrorRate;
    if (bool) {
      d = 0.0D;
    } else {
      d = 1.0D;
    } 
    metrics.count(d);
    if (ConnectStats.isNonBlocking(paramInt1))
      this.pendingSummary.connectLatencies.count(paramInt2); 
  }
  
  public void addTcpStatsResult(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (this.pendingSummary == null)
      this.pendingSummary = new Summary(this.netId, this.transports); 
    this.pendingSummary.tcpLossRate.count(paramInt2, paramInt1);
    this.pendingSummary.roundTripTimeUs.count(paramInt3);
    this.pendingSummary.sentAckTimeDiffenceMs.count(paramInt4);
  }
  
  public static class Summary {
    public final NetworkMetrics.Metrics dnsLatencies = new NetworkMetrics.Metrics();
    
    public final NetworkMetrics.Metrics dnsErrorRate = new NetworkMetrics.Metrics();
    
    public final NetworkMetrics.Metrics connectLatencies = new NetworkMetrics.Metrics();
    
    public final NetworkMetrics.Metrics connectErrorRate = new NetworkMetrics.Metrics();
    
    public final NetworkMetrics.Metrics tcpLossRate = new NetworkMetrics.Metrics();
    
    public final NetworkMetrics.Metrics roundTripTimeUs = new NetworkMetrics.Metrics();
    
    public final NetworkMetrics.Metrics sentAckTimeDiffenceMs = new NetworkMetrics.Metrics();
    
    public final int netId;
    
    public final long transports;
    
    public Summary(int param1Int, long param1Long) {
      this.netId = param1Int;
      this.transports = param1Long;
    }
    
    void merge(Summary param1Summary) {
      this.dnsLatencies.merge(param1Summary.dnsLatencies);
      this.dnsErrorRate.merge(param1Summary.dnsErrorRate);
      this.connectLatencies.merge(param1Summary.connectLatencies);
      this.connectErrorRate.merge(param1Summary.connectErrorRate);
      this.tcpLossRate.merge(param1Summary.tcpLossRate);
    }
    
    public String toString() {
      StringJoiner stringJoiner = new StringJoiner(", ", "{", "}");
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("netId=");
      stringBuilder.append(this.netId);
      stringJoiner.add(stringBuilder.toString());
      for (int m : BitUtils.unpackBits(this.transports))
        stringJoiner.add(NetworkCapabilities.transportNameOf(m)); 
      NetworkMetrics.Metrics metrics = this.dnsLatencies;
      int k = (int)metrics.average(), i = (int)this.dnsLatencies.max;
      metrics = this.dnsErrorRate;
      double d = metrics.average();
      int j = this.dnsErrorRate.count;
      stringJoiner.add(String.format("dns avg=%dms max=%dms err=%.1f%% tot=%d", new Object[] { Integer.valueOf(k), Integer.valueOf(i), Double.valueOf(d * 100.0D), Integer.valueOf(j) }));
      metrics = this.connectLatencies;
      i = (int)metrics.average();
      j = (int)this.connectLatencies.max;
      metrics = this.connectErrorRate;
      d = metrics.average();
      k = this.connectErrorRate.count;
      stringJoiner.add(String.format("connect avg=%dms max=%dms err=%.1f%% tot=%d", new Object[] { Integer.valueOf(i), Integer.valueOf(j), Double.valueOf(d * 100.0D), Integer.valueOf(k) }));
      metrics = this.tcpLossRate;
      d = metrics.average();
      i = this.tcpLossRate.count;
      j = (int)this.tcpLossRate.sum;
      stringJoiner.add(String.format("tcp avg_loss=%.1f%% total_sent=%d total_lost=%d", new Object[] { Double.valueOf(d * 100.0D), Integer.valueOf(i), Integer.valueOf(j) }));
      stringJoiner.add(String.format("tcp rtt=%dms", new Object[] { Integer.valueOf((int)(this.roundTripTimeUs.average() / 1000.0D)) }));
      stringJoiner.add(String.format("tcp sent-ack_diff=%dms", new Object[] { Integer.valueOf((int)this.sentAckTimeDiffenceMs.average()) }));
      return stringJoiner.toString();
    }
  }
  
  static class Metrics {
    public int count;
    
    public double max = Double.MIN_VALUE;
    
    public double sum;
    
    void merge(Metrics param1Metrics) {
      this.count += param1Metrics.count;
      this.sum += param1Metrics.sum;
      this.max = Math.max(this.max, param1Metrics.max);
    }
    
    void count(double param1Double) {
      count(param1Double, 1);
    }
    
    void count(double param1Double, int param1Int) {
      this.count += param1Int;
      this.sum += param1Double;
      this.max = Math.max(this.max, param1Double);
    }
    
    double average() {
      double d1 = this.sum / this.count;
      double d2 = d1;
      if (Double.isNaN(d1))
        d2 = 0.0D; 
      return d2;
    }
  }
}
