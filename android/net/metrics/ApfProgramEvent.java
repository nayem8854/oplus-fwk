package android.net.metrics;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.SparseArray;
import com.android.internal.util.MessageUtils;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.BitSet;

@SystemApi
public final class ApfProgramEvent implements IpConnectivityLog.Event {
  private ApfProgramEvent(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.lifetime = paramLong1;
    this.actualLifetime = paramLong2;
    this.filteredRas = paramInt1;
    this.currentRas = paramInt2;
    this.programLength = paramInt3;
    this.flags = paramInt4;
  }
  
  private ApfProgramEvent(Parcel paramParcel) {
    this.lifetime = paramParcel.readLong();
    this.actualLifetime = paramParcel.readLong();
    this.filteredRas = paramParcel.readInt();
    this.currentRas = paramParcel.readInt();
    this.programLength = paramParcel.readInt();
    this.flags = paramParcel.readInt();
  }
  
  class Builder {
    private long mActualLifetime;
    
    private int mCurrentRas;
    
    private int mFilteredRas;
    
    private int mFlags;
    
    private long mLifetime;
    
    private int mProgramLength;
    
    public Builder setLifetime(long param1Long) {
      this.mLifetime = param1Long;
      return this;
    }
    
    public Builder setActualLifetime(long param1Long) {
      this.mActualLifetime = param1Long;
      return this;
    }
    
    public Builder setFilteredRas(int param1Int) {
      this.mFilteredRas = param1Int;
      return this;
    }
    
    public Builder setCurrentRas(int param1Int) {
      this.mCurrentRas = param1Int;
      return this;
    }
    
    public Builder setProgramLength(int param1Int) {
      this.mProgramLength = param1Int;
      return this;
    }
    
    public Builder setFlags(boolean param1Boolean1, boolean param1Boolean2) {
      this.mFlags = ApfProgramEvent.flagsFor(param1Boolean1, param1Boolean2);
      return this;
    }
    
    public ApfProgramEvent build() {
      return new ApfProgramEvent(this.mLifetime, this.mActualLifetime, this.mFilteredRas, this.mCurrentRas, this.mProgramLength, this.mFlags);
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.lifetime);
    paramParcel.writeLong(this.actualLifetime);
    paramParcel.writeInt(this.filteredRas);
    paramParcel.writeInt(this.currentRas);
    paramParcel.writeInt(this.programLength);
    paramParcel.writeInt(this.flags);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    String str1;
    if (this.lifetime < Long.MAX_VALUE) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this.lifetime);
      stringBuilder.append("s");
      str1 = stringBuilder.toString();
    } else {
      str1 = "forever";
    } 
    int i = this.filteredRas, j = this.currentRas, k = this.programLength;
    long l = this.actualLifetime;
    String str2 = namesOf(this.flags);
    return String.format("ApfProgramEvent(%d/%d RAs %dB %ds/%s %s)", new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Long.valueOf(l), str1, str2 });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool1 = false;
    if (paramObject == null || !paramObject.getClass().equals(ApfProgramEvent.class))
      return false; 
    paramObject = paramObject;
    boolean bool2 = bool1;
    if (this.lifetime == ((ApfProgramEvent)paramObject).lifetime) {
      bool2 = bool1;
      if (this.actualLifetime == ((ApfProgramEvent)paramObject).actualLifetime) {
        bool2 = bool1;
        if (this.filteredRas == ((ApfProgramEvent)paramObject).filteredRas) {
          bool2 = bool1;
          if (this.currentRas == ((ApfProgramEvent)paramObject).currentRas) {
            bool2 = bool1;
            if (this.programLength == ((ApfProgramEvent)paramObject).programLength) {
              bool2 = bool1;
              if (this.flags == ((ApfProgramEvent)paramObject).flags)
                bool2 = true; 
            } 
          } 
        } 
      } 
    } 
    return bool2;
  }
  
  public static final Parcelable.Creator<ApfProgramEvent> CREATOR = (Parcelable.Creator<ApfProgramEvent>)new Object();
  
  public static final int FLAG_HAS_IPV4_ADDRESS = 1;
  
  public static final int FLAG_MULTICAST_FILTER_ON = 0;
  
  public final long actualLifetime;
  
  public final int currentRas;
  
  public final int filteredRas;
  
  public final int flags;
  
  public final long lifetime;
  
  public final int programLength;
  
  public static int flagsFor(boolean paramBoolean1, boolean paramBoolean2) {
    int i = 0;
    if (paramBoolean1)
      i = 0x0 | 0x2; 
    int j = i;
    if (paramBoolean2)
      j = i | 0x1; 
    return j;
  }
  
  private static String namesOf(int paramInt) {
    ArrayList<String> arrayList = new ArrayList(Integer.bitCount(paramInt));
    BitSet bitSet = BitSet.valueOf(new long[] { (Integer.MAX_VALUE & paramInt) });
    for (paramInt = bitSet.nextSetBit(0); paramInt >= 0; paramInt = bitSet.nextSetBit(paramInt + 1))
      arrayList.add((String)Decoder.constants.get(paramInt)); 
    return TextUtils.join("|", arrayList);
  }
  
  class Decoder {
    static final SparseArray<String> constants = MessageUtils.findMessageNames(new Class[] { ApfProgramEvent.class }, new String[] { "FLAG_" });
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Flags implements Annotation {}
}
