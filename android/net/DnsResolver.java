package android.net;

import android.net.util.DnsUtils;
import android.os.CancellationSignal;
import android.os.Looper;
import android.os.MessageQueue;
import android.system.ErrnoException;
import android.system.OsConstants;
import com.android.net.module.util.DnsPacket;
import java.io.FileDescriptor;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

public final class DnsResolver {
  public static final int CLASS_IN = 1;
  
  public static final int ERROR_PARSE = 0;
  
  public static final int ERROR_SYSTEM = 1;
  
  private static final int FD_EVENTS = 5;
  
  public static final int FLAG_EMPTY = 0;
  
  public static final int FLAG_NO_CACHE_LOOKUP = 4;
  
  public static final int FLAG_NO_CACHE_STORE = 2;
  
  public static final int FLAG_NO_RETRY = 1;
  
  private static final int MAXPACKET = 8192;
  
  private static final int NETID_UNSET = 0;
  
  private static final int SLEEP_TIME_MS = 2;
  
  private static final String TAG = "DnsResolver";
  
  public static final int TYPE_A = 1;
  
  public static final int TYPE_AAAA = 28;
  
  private static final DnsResolver sInstance = new DnsResolver();
  
  public static DnsResolver getInstance() {
    return sInstance;
  }
  
  public static class DnsException extends Exception {
    public final int code;
    
    DnsException(int param1Int, Throwable param1Throwable) {
      super(param1Throwable);
      this.code = param1Int;
    }
  }
  
  public void rawQuery(Network paramNetwork, byte[] paramArrayOfbyte, int paramInt, Executor paramExecutor, CancellationSignal paramCancellationSignal, Callback<? super byte[]> paramCallback) {
    boolean bool;
    if (paramCancellationSignal != null && paramCancellationSignal.isCanceled())
      return; 
    Object object = new Object();
    if (paramNetwork != null) {
      try {
        bool = paramNetwork.getNetIdForResolv();
      } catch (ErrnoException errnoException) {}
    } else {
      bool = false;
    } 
    int i = paramArrayOfbyte.length;
    synchronized (NetworkUtils.resNetworkSend(bool, paramArrayOfbyte, i, paramInt)) {
      registerFDListener(paramExecutor, null, paramCallback, paramCancellationSignal, object);
      if (paramCancellationSignal == null) {
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        return;
      } 
      addCancellationSignal(paramCancellationSignal, null, object);
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      return;
    } 
  }
  
  public void rawQuery(Network paramNetwork, String paramString, int paramInt1, int paramInt2, int paramInt3, Executor paramExecutor, CancellationSignal paramCancellationSignal, Callback<? super byte[]> paramCallback) {
    boolean bool;
    if (paramCancellationSignal != null && paramCancellationSignal.isCanceled())
      return; 
    Object object = new Object();
    if (paramNetwork != null) {
      try {
        bool = paramNetwork.getNetIdForResolv();
      } catch (ErrnoException null) {}
    } else {
      bool = false;
    } 
    try {
      FileDescriptor fileDescriptor = NetworkUtils.resNetworkQuery(bool, paramString, paramInt1, paramInt2, paramInt3);
      /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      try {
        registerFDListener(paramExecutor, fileDescriptor, paramCallback, paramCancellationSignal, object);
        if (paramCancellationSignal == null) {
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          return;
        } 
        try {
          addCancellationSignal(paramCancellationSignal, fileDescriptor, object);
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          return;
        } finally {}
      } finally {}
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      throw fileDescriptor;
    } catch (ErrnoException errnoException) {}
    paramExecutor.execute(new _$$Lambda$DnsResolver$GTAgQiExADAzbCx0WiV_97W72_g(paramCallback, errnoException));
  }
  
  class InetAddressAnswerAccumulator implements Callback<byte[]> {
    private final List<InetAddress> mAllAnswers;
    
    private DnsResolver.DnsException mDnsException;
    
    private final Network mNetwork;
    
    private int mRcode;
    
    private int mReceivedAnswerCount = 0;
    
    private final int mTargetAnswerCount;
    
    private final DnsResolver.Callback<? super List<InetAddress>> mUserCallback;
    
    final DnsResolver this$0;
    
    InetAddressAnswerAccumulator(Network param1Network, int param1Int, DnsResolver.Callback<? super List<InetAddress>> param1Callback) {
      this.mNetwork = param1Network;
      this.mTargetAnswerCount = param1Int;
      this.mAllAnswers = new ArrayList<>();
      this.mUserCallback = param1Callback;
    }
    
    private boolean maybeReportError() {
      int i = this.mRcode;
      if (i != 0) {
        this.mUserCallback.onAnswer(this.mAllAnswers, i);
        return true;
      } 
      DnsResolver.DnsException dnsException = this.mDnsException;
      if (dnsException != null) {
        this.mUserCallback.onError(dnsException);
        return true;
      } 
      return false;
    }
    
    private void maybeReportAnswer() {
      int i = this.mReceivedAnswerCount + 1;
      if (i != this.mTargetAnswerCount)
        return; 
      if (this.mAllAnswers.isEmpty() && maybeReportError())
        return; 
      this.mUserCallback.onAnswer(DnsUtils.rfc6724Sort(this.mNetwork, this.mAllAnswers), this.mRcode);
    }
    
    public void onAnswer(byte[] param1ArrayOfbyte, int param1Int) {
      if (this.mReceivedAnswerCount == 0 || param1Int == 0)
        this.mRcode = param1Int; 
      try {
        List<InetAddress> list = this.mAllAnswers;
        DnsResolver.DnsAddressAnswer dnsAddressAnswer = new DnsResolver.DnsAddressAnswer();
        this(param1ArrayOfbyte);
        list.addAll(dnsAddressAnswer.getAddresses());
      } catch (com.android.net.module.util.DnsPacket.ParseException parseException) {
        ParseException parseException1 = new ParseException(parseException.reason, parseException.getCause());
        parseException1.setStackTrace(parseException.getStackTrace());
        this.mDnsException = new DnsResolver.DnsException(0, parseException1);
      } 
      maybeReportAnswer();
    }
    
    public void onError(DnsResolver.DnsException param1DnsException) {
      this.mDnsException = param1DnsException;
      maybeReportAnswer();
    }
  }
  
  public void query(Network paramNetwork, String paramString, int paramInt, Executor paramExecutor, CancellationSignal paramCancellationSignal, Callback<? super List<InetAddress>> paramCallback) {
    // Byte code:
    //   0: aload #5
    //   2: ifnull -> 14
    //   5: aload #5
    //   7: invokevirtual isCanceled : ()Z
    //   10: ifeq -> 14
    //   13: return
    //   14: new java/lang/Object
    //   17: dup
    //   18: invokespecial <init> : ()V
    //   21: astore #7
    //   23: aload_1
    //   24: ifnull -> 33
    //   27: aload_1
    //   28: astore #8
    //   30: goto -> 38
    //   33: invokestatic getDnsNetwork : ()Landroid/net/Network;
    //   36: astore #8
    //   38: aload #8
    //   40: invokestatic haveIpv6 : (Landroid/net/Network;)Z
    //   43: istore #9
    //   45: aload #8
    //   47: invokestatic haveIpv4 : (Landroid/net/Network;)Z
    //   50: istore #10
    //   52: iload #9
    //   54: ifne -> 79
    //   57: iload #10
    //   59: ifne -> 79
    //   62: aload #4
    //   64: new android/net/_$$Lambda$DnsResolver$kjq9c3feWPGKUPV3AzJBFi1GUvw
    //   67: dup
    //   68: aload #6
    //   70: invokespecial <init> : (Landroid/net/DnsResolver$Callback;)V
    //   73: invokeinterface execute : (Ljava/lang/Runnable;)V
    //   78: return
    //   79: iconst_0
    //   80: istore #11
    //   82: iload #9
    //   84: ifeq -> 128
    //   87: aload #8
    //   89: invokevirtual getNetIdForResolv : ()I
    //   92: aload_2
    //   93: iconst_1
    //   94: bipush #28
    //   96: iload_3
    //   97: invokestatic resNetworkQuery : (ILjava/lang/String;III)Ljava/io/FileDescriptor;
    //   100: astore_1
    //   101: iconst_0
    //   102: iconst_1
    //   103: iadd
    //   104: istore #11
    //   106: goto -> 130
    //   109: astore_1
    //   110: aload #4
    //   112: new android/net/_$$Lambda$DnsResolver$uxb9gSgrd6Qyj9SLhCAtOvpxa3I
    //   115: dup
    //   116: aload #6
    //   118: aload_1
    //   119: invokespecial <init> : (Landroid/net/DnsResolver$Callback;Landroid/system/ErrnoException;)V
    //   122: invokeinterface execute : (Ljava/lang/Runnable;)V
    //   127: return
    //   128: aconst_null
    //   129: astore_1
    //   130: ldc2_w 2
    //   133: invokestatic sleep : (J)V
    //   136: goto -> 147
    //   139: astore #12
    //   141: invokestatic currentThread : ()Ljava/lang/Thread;
    //   144: invokevirtual interrupt : ()V
    //   147: iload #10
    //   149: ifeq -> 201
    //   152: aload #8
    //   154: invokevirtual getNetIdForResolv : ()I
    //   157: aload_2
    //   158: iconst_1
    //   159: iconst_1
    //   160: iload_3
    //   161: invokestatic resNetworkQuery : (ILjava/lang/String;III)Ljava/io/FileDescriptor;
    //   164: astore_2
    //   165: iload #11
    //   167: iconst_1
    //   168: iadd
    //   169: istore_3
    //   170: goto -> 206
    //   173: astore_2
    //   174: iload #9
    //   176: ifeq -> 183
    //   179: aload_1
    //   180: invokestatic resNetworkCancel : (Ljava/io/FileDescriptor;)V
    //   183: aload #4
    //   185: new android/net/_$$Lambda$DnsResolver$t5xp_fS_zTQ564hG_PIaWJdBP8c
    //   188: dup
    //   189: aload #6
    //   191: aload_2
    //   192: invokespecial <init> : (Landroid/net/DnsResolver$Callback;Landroid/system/ErrnoException;)V
    //   195: invokeinterface execute : (Ljava/lang/Runnable;)V
    //   200: return
    //   201: aconst_null
    //   202: astore_2
    //   203: iload #11
    //   205: istore_3
    //   206: new android/net/DnsResolver$InetAddressAnswerAccumulator
    //   209: dup
    //   210: aload_0
    //   211: aload #8
    //   213: iload_3
    //   214: aload #6
    //   216: invokespecial <init> : (Landroid/net/DnsResolver;Landroid/net/Network;ILandroid/net/DnsResolver$Callback;)V
    //   219: astore #6
    //   221: aload #7
    //   223: monitorenter
    //   224: iload #9
    //   226: ifeq -> 249
    //   229: aload_0
    //   230: aload #4
    //   232: aload_1
    //   233: aload #6
    //   235: aload #5
    //   237: aload #7
    //   239: invokespecial registerFDListener : (Ljava/util/concurrent/Executor;Ljava/io/FileDescriptor;Landroid/net/DnsResolver$Callback;Landroid/os/CancellationSignal;Ljava/lang/Object;)V
    //   242: goto -> 249
    //   245: astore_1
    //   246: goto -> 313
    //   249: iload #10
    //   251: ifeq -> 274
    //   254: aload_0
    //   255: aload #4
    //   257: aload_2
    //   258: aload #6
    //   260: aload #5
    //   262: aload #7
    //   264: invokespecial registerFDListener : (Ljava/util/concurrent/Executor;Ljava/io/FileDescriptor;Landroid/net/DnsResolver$Callback;Landroid/os/CancellationSignal;Ljava/lang/Object;)V
    //   267: goto -> 274
    //   270: astore_1
    //   271: goto -> 313
    //   274: aload #5
    //   276: ifnonnull -> 283
    //   279: aload #7
    //   281: monitorexit
    //   282: return
    //   283: new android/net/_$$Lambda$DnsResolver$DW9jYL2ZOH6BjebIVPhZIrrhoD8
    //   286: astore #4
    //   288: aload #4
    //   290: aload_0
    //   291: aload #7
    //   293: iload #10
    //   295: aload_2
    //   296: iload #9
    //   298: aload_1
    //   299: invokespecial <init> : (Landroid/net/DnsResolver;Ljava/lang/Object;ZLjava/io/FileDescriptor;ZLjava/io/FileDescriptor;)V
    //   302: aload #5
    //   304: aload #4
    //   306: invokevirtual setOnCancelListener : (Landroid/os/CancellationSignal$OnCancelListener;)V
    //   309: aload #7
    //   311: monitorexit
    //   312: return
    //   313: aload #7
    //   315: monitorexit
    //   316: aload_1
    //   317: athrow
    //   318: astore_1
    //   319: aload #4
    //   321: new android/net/_$$Lambda$DnsResolver$vvKhya16sREGcN1Gxnqgw_LBoV4
    //   324: dup
    //   325: aload #6
    //   327: aload_1
    //   328: invokespecial <init> : (Landroid/net/DnsResolver$Callback;Landroid/system/ErrnoException;)V
    //   331: invokeinterface execute : (Ljava/lang/Runnable;)V
    //   336: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #340	-> 0
    //   #341	-> 13
    //   #343	-> 14
    //   #346	-> 23
    //   #350	-> 38
    //   #351	-> 38
    //   #352	-> 45
    //   #356	-> 52
    //   #357	-> 62
    //   #359	-> 78
    //   #365	-> 79
    //   #367	-> 82
    //   #369	-> 87
    //   #374	-> 101
    //   #375	-> 101
    //   #371	-> 109
    //   #372	-> 110
    //   #373	-> 127
    //   #376	-> 128
    //   #380	-> 130
    //   #383	-> 136
    //   #381	-> 139
    //   #382	-> 141
    //   #385	-> 147
    //   #387	-> 152
    //   #393	-> 165
    //   #394	-> 165
    //   #389	-> 173
    //   #390	-> 174
    //   #391	-> 183
    //   #392	-> 200
    //   #395	-> 201
    //   #397	-> 206
    //   #400	-> 221
    //   #401	-> 224
    //   #402	-> 229
    //   #414	-> 245
    //   #404	-> 249
    //   #405	-> 254
    //   #414	-> 270
    //   #404	-> 274
    //   #407	-> 274
    //   #408	-> 283
    //   #414	-> 309
    //   #415	-> 312
    //   #414	-> 313
    //   #347	-> 318
    //   #348	-> 319
    //   #349	-> 336
    // Exception table:
    //   from	to	target	type
    //   33	38	318	android/system/ErrnoException
    //   87	101	109	android/system/ErrnoException
    //   130	136	139	java/lang/InterruptedException
    //   152	165	173	android/system/ErrnoException
    //   229	242	245	finally
    //   254	267	270	finally
    //   279	282	270	finally
    //   283	309	270	finally
    //   309	312	270	finally
    //   313	316	270	finally
  }
  
  public void query(Network paramNetwork, String paramString, int paramInt1, int paramInt2, Executor paramExecutor, CancellationSignal paramCancellationSignal, Callback<? super List<InetAddress>> paramCallback) {
    // Byte code:
    //   0: aload #6
    //   2: ifnull -> 14
    //   5: aload #6
    //   7: invokevirtual isCanceled : ()Z
    //   10: ifeq -> 14
    //   13: return
    //   14: new java/lang/Object
    //   17: dup
    //   18: invokespecial <init> : ()V
    //   21: astore #8
    //   23: aload_1
    //   24: ifnull -> 30
    //   27: goto -> 34
    //   30: invokestatic getDnsNetwork : ()Landroid/net/Network;
    //   33: astore_1
    //   34: aload_1
    //   35: invokevirtual getNetIdForResolv : ()I
    //   38: istore #9
    //   40: iload #9
    //   42: aload_2
    //   43: iconst_1
    //   44: iload_3
    //   45: iload #4
    //   47: invokestatic resNetworkQuery : (ILjava/lang/String;III)Ljava/io/FileDescriptor;
    //   50: astore_2
    //   51: new android/net/DnsResolver$InetAddressAnswerAccumulator
    //   54: dup
    //   55: aload_0
    //   56: aload_1
    //   57: iconst_1
    //   58: aload #7
    //   60: invokespecial <init> : (Landroid/net/DnsResolver;Landroid/net/Network;ILandroid/net/DnsResolver$Callback;)V
    //   63: astore_1
    //   64: aload #8
    //   66: monitorenter
    //   67: aload_0
    //   68: aload #5
    //   70: aload_2
    //   71: aload_1
    //   72: aload #6
    //   74: aload #8
    //   76: invokespecial registerFDListener : (Ljava/util/concurrent/Executor;Ljava/io/FileDescriptor;Landroid/net/DnsResolver$Callback;Landroid/os/CancellationSignal;Ljava/lang/Object;)V
    //   79: aload #6
    //   81: ifnonnull -> 88
    //   84: aload #8
    //   86: monitorexit
    //   87: return
    //   88: aload_0
    //   89: aload #6
    //   91: aload_2
    //   92: aload #8
    //   94: invokespecial addCancellationSignal : (Landroid/os/CancellationSignal;Ljava/io/FileDescriptor;Ljava/lang/Object;)V
    //   97: aload #8
    //   99: monitorexit
    //   100: return
    //   101: astore_1
    //   102: aload #8
    //   104: monitorexit
    //   105: aload_1
    //   106: athrow
    //   107: astore_1
    //   108: goto -> 112
    //   111: astore_1
    //   112: aload #5
    //   114: new android/net/_$$Lambda$DnsResolver$wc3_cnx2aezlAHvMEbQVFaTPAcE
    //   117: dup
    //   118: aload #7
    //   120: aload_1
    //   121: invokespecial <init> : (Landroid/net/DnsResolver$Callback;Landroid/system/ErrnoException;)V
    //   124: invokeinterface execute : (Ljava/lang/Runnable;)V
    //   129: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #439	-> 0
    //   #440	-> 13
    //   #442	-> 14
    //   #446	-> 23
    //   #447	-> 34
    //   #452	-> 51
    //   #453	-> 51
    //   #455	-> 64
    //   #456	-> 67
    //   #457	-> 79
    //   #458	-> 88
    //   #459	-> 97
    //   #460	-> 100
    //   #459	-> 101
    //   #449	-> 107
    //   #450	-> 112
    //   #451	-> 129
    // Exception table:
    //   from	to	target	type
    //   30	34	111	android/system/ErrnoException
    //   34	40	111	android/system/ErrnoException
    //   40	51	107	android/system/ErrnoException
    //   67	79	101	finally
    //   84	87	101	finally
    //   88	97	101	finally
    //   97	100	101	finally
    //   102	105	101	finally
  }
  
  public static final class DnsResponse {
    public final byte[] answerbuf;
    
    public final int rcode;
    
    public DnsResponse(byte[] param1ArrayOfbyte, int param1Int) {
      this.answerbuf = param1ArrayOfbyte;
      this.rcode = param1Int;
    }
  }
  
  private void registerFDListener(Executor paramExecutor, FileDescriptor paramFileDescriptor, Callback<? super byte[]> paramCallback, CancellationSignal paramCancellationSignal, Object paramObject) {
    MessageQueue messageQueue = Looper.getMainLooper().getQueue();
    messageQueue.addOnFileDescriptorEventListener(paramFileDescriptor, 5, new _$$Lambda$DnsResolver$kxKi6qjPYeR_SIipxW4tYpxyM50(messageQueue, paramExecutor, paramObject, paramCancellationSignal, paramCallback));
  }
  
  private void cancelQuery(FileDescriptor paramFileDescriptor) {
    if (!paramFileDescriptor.valid())
      return; 
    Looper.getMainLooper().getQueue().removeOnFileDescriptorEventListener(paramFileDescriptor);
    NetworkUtils.resNetworkCancel(paramFileDescriptor);
  }
  
  private void addCancellationSignal(CancellationSignal paramCancellationSignal, FileDescriptor paramFileDescriptor, Object paramObject) {
    paramCancellationSignal.setOnCancelListener(new _$$Lambda$DnsResolver$05nTktlCCI7FQsULCMbgIrjmrGc(this, paramObject, paramFileDescriptor));
  }
  
  class DnsAddressAnswer extends DnsPacket {
    private static final boolean DBG = false;
    
    private static final String TAG = "DnsResolver.DnsAddressAnswer";
    
    private final int mQueryType;
    
    DnsAddressAnswer(DnsResolver this$0) throws DnsPacket.ParseException {
      super((byte[])this$0);
      if ((this.mHeader.flags & 0x8000) != 0) {
        if (this.mHeader.getRecordCount(0) != 0) {
          this.mQueryType = ((DnsPacket.DnsRecord)this.mRecords[0].get(0)).nsType;
          return;
        } 
        throw new DnsPacket.ParseException("No question found");
      } 
      throw new DnsPacket.ParseException("Not an answer packet");
    }
    
    public List<InetAddress> getAddresses() {
      ArrayList<InetAddress> arrayList = new ArrayList();
      if (this.mHeader.getRecordCount(1) == 0)
        return arrayList; 
      for (DnsPacket.DnsRecord dnsRecord : this.mRecords[1]) {
        int i = dnsRecord.nsType;
        if (i != this.mQueryType || (i != 1 && i != 28))
          continue; 
        try {
          arrayList.add(InetAddress.getByAddress(dnsRecord.getRR()));
        } catch (UnknownHostException unknownHostException) {}
      } 
      return arrayList;
    }
  }
  
  public static interface Callback<T> {
    void onAnswer(T param1T, int param1Int);
    
    void onError(DnsResolver.DnsException param1DnsException);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  static @interface DnsError {}
  
  @Retention(RetentionPolicy.SOURCE)
  static @interface QueryClass {}
  
  @Retention(RetentionPolicy.SOURCE)
  static @interface QueryFlag {}
  
  @Retention(RetentionPolicy.SOURCE)
  static @interface QueryType {}
}
