package android.net;

import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;

public final class UriCodec {
  private static final char INVALID_INPUT_CHARACTER = '�';
  
  private static int hexCharToValue(char paramChar) {
    if ('0' <= paramChar && paramChar <= '9')
      return paramChar - 48; 
    if ('a' <= paramChar && paramChar <= 'f')
      return paramChar + 10 - 97; 
    if ('A' <= paramChar && paramChar <= 'F')
      return paramChar + 10 - 65; 
    return -1;
  }
  
  private static URISyntaxException unexpectedCharacterException(String paramString1, String paramString2, char paramChar, int paramInt) {
    if (paramString2 == null) {
      paramString2 = "";
    } else {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(" in [");
      stringBuilder1.append(paramString2);
      stringBuilder1.append("]");
      paramString2 = stringBuilder1.toString();
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unexpected character");
    stringBuilder.append(paramString2);
    stringBuilder.append(": ");
    stringBuilder.append(paramChar);
    return new URISyntaxException(paramString1, stringBuilder.toString(), paramInt);
  }
  
  private static char getNextCharacter(String paramString1, int paramInt1, int paramInt2, String paramString2) throws URISyntaxException {
    if (paramInt1 >= paramInt2) {
      if (paramString2 == null) {
        paramString2 = "";
      } else {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(" in [");
        stringBuilder1.append(paramString2);
        stringBuilder1.append("]");
        paramString2 = stringBuilder1.toString();
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected end of string");
      stringBuilder.append(paramString2);
      throw new URISyntaxException(paramString1, stringBuilder.toString(), paramInt1);
    } 
    return paramString1.charAt(paramInt1);
  }
  
  public static String decode(String paramString, boolean paramBoolean1, Charset paramCharset, boolean paramBoolean2) {
    StringBuilder stringBuilder = new StringBuilder(paramString.length());
    appendDecoded(stringBuilder, paramString, paramBoolean1, paramCharset, paramBoolean2);
    return stringBuilder.toString();
  }
  
  private static void appendDecoded(StringBuilder paramStringBuilder, String paramString, boolean paramBoolean1, Charset paramCharset, boolean paramBoolean2) {
    CharsetDecoder charsetDecoder2 = paramCharset.newDecoder();
    CodingErrorAction codingErrorAction2 = CodingErrorAction.REPLACE;
    charsetDecoder2 = charsetDecoder2.onMalformedInput(codingErrorAction2);
    CharsetDecoder charsetDecoder3 = charsetDecoder2.replaceWith("�");
    CodingErrorAction codingErrorAction1 = CodingErrorAction.REPORT;
    CharsetDecoder charsetDecoder1 = charsetDecoder3.onUnmappableCharacter(codingErrorAction1);
    ByteBuffer byteBuffer = ByteBuffer.allocate(paramString.length());
    int i = 0;
    while (i < paramString.length()) {
      char c = paramString.charAt(i);
      int j = i + 1;
      if (c != '%') {
        byte b2 = 43;
        if (c != '+') {
          flushDecodingByteAccumulator(paramStringBuilder, charsetDecoder1, byteBuffer, paramBoolean2);
          paramStringBuilder.append(c);
          i = j;
          continue;
        } 
        flushDecodingByteAccumulator(paramStringBuilder, charsetDecoder1, byteBuffer, paramBoolean2);
        if (paramBoolean1)
          b2 = 32; 
        paramStringBuilder.append(b2);
        i = j;
        continue;
      } 
      byte b = 0;
      byte b1 = 0;
      while (true) {
        i = j;
        if (b1 < 2) {
          try {
            char c1 = getNextCharacter(paramString, j, paramString.length(), null);
            j++;
            i = hexCharToValue(c1);
            if (i < 0) {
              if (!paramBoolean2) {
                flushDecodingByteAccumulator(paramStringBuilder, charsetDecoder1, byteBuffer, paramBoolean2);
                paramStringBuilder.append('�');
                i = j;
                break;
              } 
              throw new IllegalArgumentException(unexpectedCharacterException(paramString, null, c1, j - 1));
            } 
            b = (byte)(b * 16 + i);
            b1++;
          } catch (URISyntaxException uRISyntaxException) {
            if (!paramBoolean2) {
              flushDecodingByteAccumulator(paramStringBuilder, charsetDecoder1, byteBuffer, paramBoolean2);
              paramStringBuilder.append('�');
              return;
            } 
            throw new IllegalArgumentException(uRISyntaxException);
          } 
          continue;
        } 
        break;
      } 
      byteBuffer.put(b);
    } 
    flushDecodingByteAccumulator(paramStringBuilder, charsetDecoder1, byteBuffer, paramBoolean2);
  }
  
  private static void flushDecodingByteAccumulator(StringBuilder paramStringBuilder, CharsetDecoder paramCharsetDecoder, ByteBuffer paramByteBuffer, boolean paramBoolean) {
    if (paramByteBuffer.position() == 0)
      return; 
    paramByteBuffer.flip();
    try {
      paramStringBuilder.append(paramCharsetDecoder.decode(paramByteBuffer));
      paramByteBuffer.flip();
      paramByteBuffer.limit(paramByteBuffer.capacity());
    } catch (CharacterCodingException characterCodingException) {
      if (!paramBoolean) {
        paramStringBuilder.append('�');
      } else {
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        this(characterCodingException);
        throw illegalArgumentException;
      } 
      paramByteBuffer.flip();
      paramByteBuffer.limit(paramByteBuffer.capacity());
    } finally {}
  }
}
