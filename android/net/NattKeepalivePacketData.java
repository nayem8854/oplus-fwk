package android.net;

import android.annotation.SystemApi;
import android.net.util.IpUtils;
import android.os.Parcel;
import android.os.Parcelable;
import android.system.OsConstants;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Objects;

@SystemApi
public final class NattKeepalivePacketData extends KeepalivePacketData implements Parcelable {
  public NattKeepalivePacketData(InetAddress paramInetAddress1, int paramInt1, InetAddress paramInetAddress2, int paramInt2, byte[] paramArrayOfbyte) throws InvalidPacketException {
    super(paramInetAddress1, paramInt1, paramInetAddress2, paramInt2, paramArrayOfbyte);
  }
  
  public static NattKeepalivePacketData nattKeepalivePacket(InetAddress paramInetAddress1, int paramInt1, InetAddress paramInetAddress2, int paramInt2) throws InvalidPacketException {
    if (paramInetAddress1 instanceof java.net.Inet4Address && paramInetAddress2 instanceof java.net.Inet4Address) {
      if (paramInt2 == 4500) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(29);
        byteBuffer.order(ByteOrder.BIG_ENDIAN);
        byteBuffer.putShort((short)17664);
        byteBuffer.putShort((short)29);
        byteBuffer.putInt(0);
        byteBuffer.put((byte)64);
        byteBuffer.put((byte)OsConstants.IPPROTO_UDP);
        int i = byteBuffer.position();
        byteBuffer.putShort((short)0);
        byteBuffer.put(paramInetAddress1.getAddress());
        byteBuffer.put(paramInetAddress2.getAddress());
        byteBuffer.putShort((short)paramInt1);
        byteBuffer.putShort((short)paramInt2);
        byteBuffer.putShort((short)(29 - 20));
        int j = byteBuffer.position();
        byteBuffer.putShort((short)0);
        byteBuffer.put((byte)-1);
        byteBuffer.putShort(i, IpUtils.ipChecksum(byteBuffer, 0));
        byteBuffer.putShort(j, IpUtils.udpChecksum(byteBuffer, 0, 20));
        return new NattKeepalivePacketData(paramInetAddress1, paramInt1, paramInetAddress2, paramInt2, byteBuffer.array());
      } 
      throw new InvalidPacketException(-22);
    } 
    throw new InvalidPacketException(-21);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(getSrcAddress().getHostAddress());
    paramParcel.writeString(getDstAddress().getHostAddress());
    paramParcel.writeInt(getSrcPort());
    paramParcel.writeInt(getDstPort());
  }
  
  public static final Parcelable.Creator<NattKeepalivePacketData> CREATOR = new Parcelable.Creator<NattKeepalivePacketData>() {
      public NattKeepalivePacketData createFromParcel(Parcel param1Parcel) {
        InetAddress inetAddress1 = InetAddresses.parseNumericAddress(param1Parcel.readString());
        InetAddress inetAddress2 = InetAddresses.parseNumericAddress(param1Parcel.readString());
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        try {
          return NattKeepalivePacketData.nattKeepalivePacket(inetAddress1, i, inetAddress2, j);
        } catch (InvalidPacketException invalidPacketException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Invalid NAT-T keepalive data: ");
          stringBuilder.append(invalidPacketException.getError());
          throw new IllegalArgumentException(stringBuilder.toString());
        } 
      }
      
      public NattKeepalivePacketData[] newArray(int param1Int) {
        return new NattKeepalivePacketData[param1Int];
      }
    };
  
  private static final int IPV4_HEADER_LENGTH = 20;
  
  private static final int UDP_HEADER_LENGTH = 8;
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof NattKeepalivePacketData;
    boolean bool1 = false;
    if (!bool)
      return false; 
    NattKeepalivePacketData nattKeepalivePacketData = (NattKeepalivePacketData)paramObject;
    paramObject = getSrcAddress();
    InetAddress inetAddress = getDstAddress();
    if (paramObject.equals(nattKeepalivePacketData.getSrcAddress()) && 
      inetAddress.equals(nattKeepalivePacketData.getDstAddress()) && 
      getSrcPort() == nattKeepalivePacketData.getSrcPort() && 
      getDstPort() == nattKeepalivePacketData.getDstPort())
      bool1 = true; 
    return bool1;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { getSrcAddress(), getDstAddress(), Integer.valueOf(getSrcPort()), Integer.valueOf(getDstPort()) });
  }
}
