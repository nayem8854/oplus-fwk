package android.net;

import android.os.SystemProperties;
import android.util.Log;
import com.android.internal.os.RoSystemProperties;
import com.android.org.conscrypt.ClientSessionContext;
import com.android.org.conscrypt.OpenSSLSocketImpl;
import com.android.org.conscrypt.SSLClientSessionCache;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.security.KeyManagementException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import javax.net.SocketFactory;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.http.conn.ssl.SSLSocketFactory;

@Deprecated
public class SSLCertificateSocketFactory extends SSLSocketFactory {
  private static final TrustManager[] INSECURE_TRUST_MANAGER = new TrustManager[] { new X509TrustManager() {
        public X509Certificate[] getAcceptedIssuers() {
          return null;
        }
        
        public void checkClientTrusted(X509Certificate[] param1ArrayOfX509Certificate, String param1String) {}
        
        public void checkServerTrusted(X509Certificate[] param1ArrayOfX509Certificate, String param1String) {}
      } };
  
  private static final String TAG = "SSLCertificateSocketFactory";
  
  private byte[] mAlpnProtocols;
  
  private PrivateKey mChannelIdPrivateKey;
  
  private final int mHandshakeTimeoutMillis;
  
  private SSLSocketFactory mInsecureFactory;
  
  private KeyManager[] mKeyManagers;
  
  private byte[] mNpnProtocols;
  
  private final boolean mSecure;
  
  private SSLSocketFactory mSecureFactory;
  
  private final SSLClientSessionCache mSessionCache;
  
  private TrustManager[] mTrustManagers;
  
  @Deprecated
  public SSLCertificateSocketFactory(int paramInt) {
    this(paramInt, null, true);
  }
  
  private SSLCertificateSocketFactory(int paramInt, SSLSessionCache paramSSLSessionCache, boolean paramBoolean) {
    SSLClientSessionCache sSLClientSessionCache;
    SSLSessionCache sSLSessionCache = null;
    this.mInsecureFactory = null;
    this.mSecureFactory = null;
    this.mTrustManagers = null;
    this.mKeyManagers = null;
    this.mNpnProtocols = null;
    this.mAlpnProtocols = null;
    this.mChannelIdPrivateKey = null;
    this.mHandshakeTimeoutMillis = paramInt;
    if (paramSSLSessionCache == null) {
      paramSSLSessionCache = sSLSessionCache;
    } else {
      sSLClientSessionCache = paramSSLSessionCache.mSessionCache;
    } 
    this.mSessionCache = sSLClientSessionCache;
    this.mSecure = paramBoolean;
  }
  
  public static SocketFactory getDefault(int paramInt) {
    return new SSLCertificateSocketFactory(paramInt, null, true);
  }
  
  public static SSLSocketFactory getDefault(int paramInt, SSLSessionCache paramSSLSessionCache) {
    return new SSLCertificateSocketFactory(paramInt, paramSSLSessionCache, true);
  }
  
  public static SSLSocketFactory getInsecure(int paramInt, SSLSessionCache paramSSLSessionCache) {
    return new SSLCertificateSocketFactory(paramInt, paramSSLSessionCache, false);
  }
  
  @Deprecated
  public static SSLSocketFactory getHttpSocketFactory(int paramInt, SSLSessionCache paramSSLSessionCache) {
    return new SSLSocketFactory(new SSLCertificateSocketFactory(paramInt, paramSSLSessionCache, true));
  }
  
  public static void verifyHostname(Socket paramSocket, String paramString) throws IOException {
    if (paramSocket instanceof javax.net.ssl.SSLSocket) {
      if (!isSslCheckRelaxed()) {
        paramSocket = paramSocket;
        paramSocket.startHandshake();
        SSLSession sSLSession = paramSocket.getSession();
        if (sSLSession != null) {
          if (!HttpsURLConnection.getDefaultHostnameVerifier().verify(paramString, sSLSession)) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Cannot verify hostname: ");
            stringBuilder.append(paramString);
            throw new SSLPeerUnverifiedException(stringBuilder.toString());
          } 
        } else {
          throw new SSLException("Cannot verify SSL socket without session");
        } 
      } 
      return;
    } 
    throw new IllegalArgumentException("Attempt to verify non-SSL socket");
  }
  
  private SSLSocketFactory makeSocketFactory(KeyManager[] paramArrayOfKeyManager, TrustManager[] paramArrayOfTrustManager) {
    try {
      SSLContext sSLContext = SSLContext.getInstance("TLS", "AndroidOpenSSL");
      sSLContext.init(paramArrayOfKeyManager, paramArrayOfTrustManager, null);
      ClientSessionContext clientSessionContext = (ClientSessionContext)sSLContext.getClientSessionContext();
      SSLClientSessionCache sSLClientSessionCache = this.mSessionCache;
      clientSessionContext.setPersistentCache(sSLClientSessionCache);
      return sSLContext.getSocketFactory();
    } catch (KeyManagementException|java.security.NoSuchAlgorithmException|java.security.NoSuchProviderException keyManagementException) {
      Log.wtf("SSLCertificateSocketFactory", keyManagementException);
      return (SSLSocketFactory)SSLSocketFactory.getDefault();
    } 
  }
  
  private static boolean isSslCheckRelaxed() {
    boolean bool = RoSystemProperties.DEBUGGABLE;
    boolean bool1 = false;
    if (bool && 
      SystemProperties.getBoolean("socket.relaxsslcheck", false))
      bool1 = true; 
    return bool1;
  }
  
  private SSLSocketFactory getDelegate() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mSecure : Z
    //   6: ifeq -> 50
    //   9: invokestatic isSslCheckRelaxed : ()Z
    //   12: ifeq -> 18
    //   15: goto -> 50
    //   18: aload_0
    //   19: getfield mSecureFactory : Ljavax/net/ssl/SSLSocketFactory;
    //   22: ifnonnull -> 41
    //   25: aload_0
    //   26: aload_0
    //   27: aload_0
    //   28: getfield mKeyManagers : [Ljavax/net/ssl/KeyManager;
    //   31: aload_0
    //   32: getfield mTrustManagers : [Ljavax/net/ssl/TrustManager;
    //   35: invokespecial makeSocketFactory : ([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;)Ljavax/net/ssl/SSLSocketFactory;
    //   38: putfield mSecureFactory : Ljavax/net/ssl/SSLSocketFactory;
    //   41: aload_0
    //   42: getfield mSecureFactory : Ljavax/net/ssl/SSLSocketFactory;
    //   45: astore_1
    //   46: aload_0
    //   47: monitorexit
    //   48: aload_1
    //   49: areturn
    //   50: aload_0
    //   51: getfield mInsecureFactory : Ljavax/net/ssl/SSLSocketFactory;
    //   54: ifnonnull -> 98
    //   57: aload_0
    //   58: getfield mSecure : Z
    //   61: ifeq -> 75
    //   64: ldc 'SSLCertificateSocketFactory'
    //   66: ldc '*** BYPASSING SSL SECURITY CHECKS (socket.relaxsslcheck=yes) ***'
    //   68: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   71: pop
    //   72: goto -> 83
    //   75: ldc 'SSLCertificateSocketFactory'
    //   77: ldc 'Bypassing SSL security checks at caller's request'
    //   79: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   82: pop
    //   83: aload_0
    //   84: aload_0
    //   85: aload_0
    //   86: getfield mKeyManagers : [Ljavax/net/ssl/KeyManager;
    //   89: getstatic android/net/SSLCertificateSocketFactory.INSECURE_TRUST_MANAGER : [Ljavax/net/ssl/TrustManager;
    //   92: invokespecial makeSocketFactory : ([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;)Ljavax/net/ssl/SSLSocketFactory;
    //   95: putfield mInsecureFactory : Ljavax/net/ssl/SSLSocketFactory;
    //   98: aload_0
    //   99: getfield mInsecureFactory : Ljavax/net/ssl/SSLSocketFactory;
    //   102: astore_1
    //   103: aload_0
    //   104: monitorexit
    //   105: aload_1
    //   106: areturn
    //   107: astore_1
    //   108: aload_0
    //   109: monitorexit
    //   110: aload_1
    //   111: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #276	-> 2
    //   #287	-> 18
    //   #288	-> 25
    //   #290	-> 41
    //   #277	-> 50
    //   #278	-> 57
    //   #279	-> 64
    //   #281	-> 75
    //   #283	-> 83
    //   #285	-> 98
    //   #275	-> 107
    // Exception table:
    //   from	to	target	type
    //   2	15	107	finally
    //   18	25	107	finally
    //   25	41	107	finally
    //   41	46	107	finally
    //   50	57	107	finally
    //   57	64	107	finally
    //   64	72	107	finally
    //   75	83	107	finally
    //   83	98	107	finally
    //   98	103	107	finally
  }
  
  public void setTrustManagers(TrustManager[] paramArrayOfTrustManager) {
    this.mTrustManagers = paramArrayOfTrustManager;
    this.mSecureFactory = null;
  }
  
  public void setNpnProtocols(byte[][] paramArrayOfbyte) {
    this.mNpnProtocols = toLengthPrefixedList(paramArrayOfbyte);
  }
  
  public void setAlpnProtocols(byte[][] paramArrayOfbyte) {
    this.mAlpnProtocols = toLengthPrefixedList(paramArrayOfbyte);
  }
  
  public static byte[] toLengthPrefixedList(byte[]... paramVarArgs) {
    if (paramVarArgs.length != 0) {
      StringBuilder stringBuilder;
      int i = 0;
      int j, k;
      for (j = paramVarArgs.length, k = 0; k < j; ) {
        byte[] arrayOfByte1 = paramVarArgs[k];
        if (arrayOfByte1.length != 0 && arrayOfByte1.length <= 255) {
          i += arrayOfByte1.length + 1;
          k++;
        } 
        stringBuilder = new StringBuilder();
        stringBuilder.append("s.length == 0 || s.length > 255: ");
        stringBuilder.append(arrayOfByte1.length);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      byte[] arrayOfByte = new byte[i];
      k = 0;
      for (int m = stringBuilder.length; i < m; ) {
        StringBuilder stringBuilder1 = stringBuilder[i];
        int n = k + 1;
        arrayOfByte[k] = (byte)stringBuilder1.length;
        for (int i1 = stringBuilder1.length; j < i1; ) {
          StringBuilder stringBuilder2 = stringBuilder1[j];
          arrayOfByte[k] = stringBuilder2;
          j++;
          k++;
        } 
        i++;
      } 
      return arrayOfByte;
    } 
    throw new IllegalArgumentException("items.length == 0");
  }
  
  public byte[] getNpnSelectedProtocol(Socket paramSocket) {
    return castToOpenSSLSocket(paramSocket).getNpnSelectedProtocol();
  }
  
  public byte[] getAlpnSelectedProtocol(Socket paramSocket) {
    return castToOpenSSLSocket(paramSocket).getAlpnSelectedProtocol();
  }
  
  public void setKeyManagers(KeyManager[] paramArrayOfKeyManager) {
    this.mKeyManagers = paramArrayOfKeyManager;
    this.mSecureFactory = null;
    this.mInsecureFactory = null;
  }
  
  public void setChannelIdPrivateKey(PrivateKey paramPrivateKey) {
    this.mChannelIdPrivateKey = paramPrivateKey;
  }
  
  public void setUseSessionTickets(Socket paramSocket, boolean paramBoolean) {
    castToOpenSSLSocket(paramSocket).setUseSessionTickets(paramBoolean);
  }
  
  public void setHostname(Socket paramSocket, String paramString) {
    castToOpenSSLSocket(paramSocket).setHostname(paramString);
  }
  
  public void setSoWriteTimeout(Socket paramSocket, int paramInt) throws SocketException {
    castToOpenSSLSocket(paramSocket).setSoWriteTimeout(paramInt);
  }
  
  private static OpenSSLSocketImpl castToOpenSSLSocket(Socket paramSocket) {
    if (paramSocket instanceof OpenSSLSocketImpl)
      return (OpenSSLSocketImpl)paramSocket; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Socket not created by this factory: ");
    stringBuilder.append(paramSocket);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public Socket createSocket(Socket paramSocket, String paramString, int paramInt, boolean paramBoolean) throws IOException {
    OpenSSLSocketImpl openSSLSocketImpl = (OpenSSLSocketImpl)getDelegate().createSocket(paramSocket, paramString, paramInt, paramBoolean);
    openSSLSocketImpl.setNpnProtocols(this.mNpnProtocols);
    openSSLSocketImpl.setAlpnProtocols(this.mAlpnProtocols);
    openSSLSocketImpl.setHandshakeTimeout(this.mHandshakeTimeoutMillis);
    openSSLSocketImpl.setChannelIdPrivateKey(this.mChannelIdPrivateKey);
    if (this.mSecure)
      verifyHostname((Socket)openSSLSocketImpl, paramString); 
    return (Socket)openSSLSocketImpl;
  }
  
  public Socket createSocket() throws IOException {
    OpenSSLSocketImpl openSSLSocketImpl = (OpenSSLSocketImpl)getDelegate().createSocket();
    openSSLSocketImpl.setNpnProtocols(this.mNpnProtocols);
    openSSLSocketImpl.setAlpnProtocols(this.mAlpnProtocols);
    openSSLSocketImpl.setHandshakeTimeout(this.mHandshakeTimeoutMillis);
    openSSLSocketImpl.setChannelIdPrivateKey(this.mChannelIdPrivateKey);
    return (Socket)openSSLSocketImpl;
  }
  
  public Socket createSocket(InetAddress paramInetAddress1, int paramInt1, InetAddress paramInetAddress2, int paramInt2) throws IOException {
    OpenSSLSocketImpl openSSLSocketImpl = (OpenSSLSocketImpl)getDelegate().createSocket(paramInetAddress1, paramInt1, paramInetAddress2, paramInt2);
    openSSLSocketImpl.setNpnProtocols(this.mNpnProtocols);
    openSSLSocketImpl.setAlpnProtocols(this.mAlpnProtocols);
    openSSLSocketImpl.setHandshakeTimeout(this.mHandshakeTimeoutMillis);
    openSSLSocketImpl.setChannelIdPrivateKey(this.mChannelIdPrivateKey);
    return (Socket)openSSLSocketImpl;
  }
  
  public Socket createSocket(InetAddress paramInetAddress, int paramInt) throws IOException {
    OpenSSLSocketImpl openSSLSocketImpl = (OpenSSLSocketImpl)getDelegate().createSocket(paramInetAddress, paramInt);
    openSSLSocketImpl.setNpnProtocols(this.mNpnProtocols);
    openSSLSocketImpl.setAlpnProtocols(this.mAlpnProtocols);
    openSSLSocketImpl.setHandshakeTimeout(this.mHandshakeTimeoutMillis);
    openSSLSocketImpl.setChannelIdPrivateKey(this.mChannelIdPrivateKey);
    return (Socket)openSSLSocketImpl;
  }
  
  public Socket createSocket(String paramString, int paramInt1, InetAddress paramInetAddress, int paramInt2) throws IOException {
    OpenSSLSocketImpl openSSLSocketImpl = (OpenSSLSocketImpl)getDelegate().createSocket(paramString, paramInt1, paramInetAddress, paramInt2);
    openSSLSocketImpl.setNpnProtocols(this.mNpnProtocols);
    openSSLSocketImpl.setAlpnProtocols(this.mAlpnProtocols);
    openSSLSocketImpl.setHandshakeTimeout(this.mHandshakeTimeoutMillis);
    openSSLSocketImpl.setChannelIdPrivateKey(this.mChannelIdPrivateKey);
    if (this.mSecure)
      verifyHostname((Socket)openSSLSocketImpl, paramString); 
    return (Socket)openSSLSocketImpl;
  }
  
  public Socket createSocket(String paramString, int paramInt) throws IOException {
    OpenSSLSocketImpl openSSLSocketImpl = (OpenSSLSocketImpl)getDelegate().createSocket(paramString, paramInt);
    openSSLSocketImpl.setNpnProtocols(this.mNpnProtocols);
    openSSLSocketImpl.setAlpnProtocols(this.mAlpnProtocols);
    openSSLSocketImpl.setHandshakeTimeout(this.mHandshakeTimeoutMillis);
    openSSLSocketImpl.setChannelIdPrivateKey(this.mChannelIdPrivateKey);
    if (this.mSecure)
      verifyHostname((Socket)openSSLSocketImpl, paramString); 
    return (Socket)openSSLSocketImpl;
  }
  
  public String[] getDefaultCipherSuites() {
    return getDelegate().getDefaultCipherSuites();
  }
  
  public String[] getSupportedCipherSuites() {
    return getDelegate().getSupportedCipherSuites();
  }
}
