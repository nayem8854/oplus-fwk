package android.net;

import android.annotation.SystemApi;
import android.net.util.IpUtils;
import android.util.Log;
import java.net.InetAddress;

@SystemApi
public class KeepalivePacketData {
  private static final String TAG = "KeepalivePacketData";
  
  private final InetAddress mDstAddress;
  
  private final int mDstPort;
  
  private final byte[] mPacket;
  
  private final InetAddress mSrcAddress;
  
  private final int mSrcPort;
  
  protected KeepalivePacketData(InetAddress paramInetAddress1, int paramInt1, InetAddress paramInetAddress2, int paramInt2, byte[] paramArrayOfbyte) throws InvalidPacketException {
    this.mSrcAddress = paramInetAddress1;
    this.mDstAddress = paramInetAddress2;
    this.mSrcPort = paramInt1;
    this.mDstPort = paramInt2;
    this.mPacket = paramArrayOfbyte;
    if (paramInetAddress1 != null && paramInetAddress2 != null) {
      String str = paramInetAddress1.getClass().getName();
      if (str.equals(paramInetAddress2.getClass().getName())) {
        if (IpUtils.isValidUdpOrTcpPort(paramInt1) && IpUtils.isValidUdpOrTcpPort(paramInt2))
          return; 
        Log.e("KeepalivePacketData", "Invalid ports in KeepalivePacketData");
        throw new InvalidPacketException(-22);
      } 
    } 
    Log.e("KeepalivePacketData", "Invalid or mismatched InetAddresses in KeepalivePacketData");
    throw new InvalidPacketException(-21);
  }
  
  public InetAddress getSrcAddress() {
    return this.mSrcAddress;
  }
  
  public InetAddress getDstAddress() {
    return this.mDstAddress;
  }
  
  public int getSrcPort() {
    return this.mSrcPort;
  }
  
  public int getDstPort() {
    return this.mDstPort;
  }
  
  public byte[] getPacket() {
    return (byte[])this.mPacket.clone();
  }
}
