package android.net;

import android.system.ErrnoException;
import android.system.Int32Ref;
import android.system.Os;
import android.system.OsConstants;
import android.system.StructLinger;
import android.system.StructTimeval;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

class LocalSocketImpl {
  private FileDescriptor fd;
  
  private SocketInputStream fis;
  
  private SocketOutputStream fos;
  
  FileDescriptor[] inboundFileDescriptors;
  
  private boolean mFdCreatedInternally;
  
  FileDescriptor[] outboundFileDescriptors;
  
  private Object readMonitor = new Object();
  
  private Object writeMonitor = new Object();
  
  class SocketInputStream extends InputStream {
    final LocalSocketImpl this$0;
    
    public int available() throws IOException {
      FileDescriptor fileDescriptor = LocalSocketImpl.this.fd;
      if (fileDescriptor != null) {
        Int32Ref int32Ref = new Int32Ref(0);
        try {
          Os.ioctlInt(fileDescriptor, OsConstants.FIONREAD, int32Ref);
          return int32Ref.value;
        } catch (ErrnoException errnoException) {
          throw errnoException.rethrowAsIOException();
        } 
      } 
      throw new IOException("socket closed");
    }
    
    public void close() throws IOException {
      LocalSocketImpl.this.close();
    }
    
    public int read() throws IOException {
      synchronized (LocalSocketImpl.this.readMonitor) {
        FileDescriptor fileDescriptor = LocalSocketImpl.this.fd;
        if (fileDescriptor != null)
          return LocalSocketImpl.this.read_native(fileDescriptor); 
        IOException iOException = new IOException();
        this("socket closed");
        throw iOException;
      } 
    }
    
    public int read(byte[] param1ArrayOfbyte) throws IOException {
      return read(param1ArrayOfbyte, 0, param1ArrayOfbyte.length);
    }
    
    public int read(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      synchronized (LocalSocketImpl.this.readMonitor) {
        FileDescriptor fileDescriptor = LocalSocketImpl.this.fd;
        if (fileDescriptor != null) {
          if (param1Int1 >= 0 && param1Int2 >= 0 && param1Int1 + param1Int2 <= param1ArrayOfbyte.length) {
            param1Int1 = LocalSocketImpl.this.readba_native(param1ArrayOfbyte, param1Int1, param1Int2, fileDescriptor);
            return param1Int1;
          } 
          ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException = new ArrayIndexOutOfBoundsException();
          this();
          throw arrayIndexOutOfBoundsException;
        } 
        IOException iOException = new IOException();
        this("socket closed");
        throw iOException;
      } 
    }
  }
  
  class SocketOutputStream extends OutputStream {
    final LocalSocketImpl this$0;
    
    public void close() throws IOException {
      LocalSocketImpl.this.close();
    }
    
    public void write(byte[] param1ArrayOfbyte) throws IOException {
      write(param1ArrayOfbyte, 0, param1ArrayOfbyte.length);
    }
    
    public void write(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      synchronized (LocalSocketImpl.this.writeMonitor) {
        FileDescriptor fileDescriptor = LocalSocketImpl.this.fd;
        if (fileDescriptor != null) {
          if (param1Int1 >= 0 && param1Int2 >= 0 && param1Int1 + param1Int2 <= param1ArrayOfbyte.length) {
            LocalSocketImpl.this.writeba_native(param1ArrayOfbyte, param1Int1, param1Int2, fileDescriptor);
            return;
          } 
          ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException = new ArrayIndexOutOfBoundsException();
          this();
          throw arrayIndexOutOfBoundsException;
        } 
        IOException iOException = new IOException();
        this("socket closed");
        throw iOException;
      } 
    }
    
    public void write(int param1Int) throws IOException {
      synchronized (LocalSocketImpl.this.writeMonitor) {
        FileDescriptor fileDescriptor = LocalSocketImpl.this.fd;
        if (fileDescriptor != null) {
          LocalSocketImpl.this.write_native(param1Int, fileDescriptor);
          return;
        } 
        IOException iOException = new IOException();
        this("socket closed");
        throw iOException;
      } 
    }
  }
  
  LocalSocketImpl(FileDescriptor paramFileDescriptor) {
    this.fd = paramFileDescriptor;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(super.toString());
    stringBuilder.append(" fd:");
    stringBuilder.append(this.fd);
    return stringBuilder.toString();
  }
  
  public void create(int paramInt) throws IOException {
    if (this.fd == null) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt == 3) {
            paramInt = OsConstants.SOCK_SEQPACKET;
          } else {
            throw new IllegalStateException("unknown sockType");
          } 
        } else {
          paramInt = OsConstants.SOCK_STREAM;
        } 
      } else {
        paramInt = OsConstants.SOCK_DGRAM;
      } 
      try {
        this.fd = Os.socket(OsConstants.AF_UNIX, paramInt, 0);
        this.mFdCreatedInternally = true;
      } catch (ErrnoException errnoException) {
        errnoException.rethrowAsIOException();
      } 
      return;
    } 
    throw new IOException("LocalSocketImpl already has an fd");
  }
  
  public void close() throws IOException {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield fd : Ljava/io/FileDescriptor;
    //   6: ifnull -> 45
    //   9: aload_0
    //   10: getfield mFdCreatedInternally : Z
    //   13: istore_1
    //   14: iload_1
    //   15: ifne -> 21
    //   18: goto -> 45
    //   21: aload_0
    //   22: getfield fd : Ljava/io/FileDescriptor;
    //   25: invokestatic close : (Ljava/io/FileDescriptor;)V
    //   28: goto -> 37
    //   31: astore_2
    //   32: aload_2
    //   33: invokevirtual rethrowAsIOException : ()Ljava/io/IOException;
    //   36: pop
    //   37: aload_0
    //   38: aconst_null
    //   39: putfield fd : Ljava/io/FileDescriptor;
    //   42: aload_0
    //   43: monitorexit
    //   44: return
    //   45: aload_0
    //   46: aconst_null
    //   47: putfield fd : Ljava/io/FileDescriptor;
    //   50: aload_0
    //   51: monitorexit
    //   52: return
    //   53: astore_2
    //   54: aload_0
    //   55: monitorexit
    //   56: aload_2
    //   57: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #240	-> 0
    //   #241	-> 2
    //   #246	-> 21
    //   #249	-> 28
    //   #247	-> 31
    //   #248	-> 32
    //   #250	-> 37
    //   #251	-> 42
    //   #252	-> 44
    //   #242	-> 45
    //   #243	-> 50
    //   #251	-> 53
    // Exception table:
    //   from	to	target	type
    //   2	14	53	finally
    //   21	28	31	android/system/ErrnoException
    //   21	28	53	finally
    //   32	37	53	finally
    //   37	42	53	finally
    //   42	44	53	finally
    //   45	50	53	finally
    //   50	52	53	finally
    //   54	56	53	finally
  }
  
  protected void connect(LocalSocketAddress paramLocalSocketAddress, int paramInt) throws IOException {
    FileDescriptor fileDescriptor = this.fd;
    if (fileDescriptor != null) {
      connectLocal(fileDescriptor, paramLocalSocketAddress.getName(), paramLocalSocketAddress.getNamespace().getId());
      return;
    } 
    throw new IOException("socket not created");
  }
  
  public void bind(LocalSocketAddress paramLocalSocketAddress) throws IOException {
    FileDescriptor fileDescriptor = this.fd;
    if (fileDescriptor != null) {
      bindLocal(fileDescriptor, paramLocalSocketAddress.getName(), paramLocalSocketAddress.getNamespace().getId());
      return;
    } 
    throw new IOException("socket not created");
  }
  
  protected void listen(int paramInt) throws IOException {
    FileDescriptor fileDescriptor = this.fd;
    if (fileDescriptor != null)
      try {
        Os.listen(fileDescriptor, paramInt);
        return;
      } catch (ErrnoException errnoException) {
        throw errnoException.rethrowAsIOException();
      }  
    throw new IOException("socket not created");
  }
  
  protected void accept(LocalSocketImpl paramLocalSocketImpl) throws IOException {
    FileDescriptor fileDescriptor = this.fd;
    if (fileDescriptor != null)
      try {
        paramLocalSocketImpl.fd = Os.accept(fileDescriptor, null);
        paramLocalSocketImpl.mFdCreatedInternally = true;
        return;
      } catch (ErrnoException errnoException) {
        throw errnoException.rethrowAsIOException();
      }  
    throw new IOException("socket not created");
  }
  
  protected InputStream getInputStream() throws IOException {
    // Byte code:
    //   0: aload_0
    //   1: getfield fd : Ljava/io/FileDescriptor;
    //   4: ifnull -> 44
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield fis : Landroid/net/LocalSocketImpl$SocketInputStream;
    //   13: ifnonnull -> 30
    //   16: new android/net/LocalSocketImpl$SocketInputStream
    //   19: astore_1
    //   20: aload_1
    //   21: aload_0
    //   22: invokespecial <init> : (Landroid/net/LocalSocketImpl;)V
    //   25: aload_0
    //   26: aload_1
    //   27: putfield fis : Landroid/net/LocalSocketImpl$SocketInputStream;
    //   30: aload_0
    //   31: getfield fis : Landroid/net/LocalSocketImpl$SocketInputStream;
    //   34: astore_1
    //   35: aload_0
    //   36: monitorexit
    //   37: aload_1
    //   38: areturn
    //   39: astore_1
    //   40: aload_0
    //   41: monitorexit
    //   42: aload_1
    //   43: athrow
    //   44: new java/io/IOException
    //   47: dup
    //   48: ldc 'socket not created'
    //   50: invokespecial <init> : (Ljava/lang/String;)V
    //   53: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #321	-> 0
    //   #325	-> 7
    //   #326	-> 9
    //   #327	-> 16
    //   #330	-> 30
    //   #331	-> 39
    //   #322	-> 44
    // Exception table:
    //   from	to	target	type
    //   9	16	39	finally
    //   16	30	39	finally
    //   30	37	39	finally
    //   40	42	39	finally
  }
  
  protected OutputStream getOutputStream() throws IOException {
    // Byte code:
    //   0: aload_0
    //   1: getfield fd : Ljava/io/FileDescriptor;
    //   4: ifnull -> 44
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield fos : Landroid/net/LocalSocketImpl$SocketOutputStream;
    //   13: ifnonnull -> 30
    //   16: new android/net/LocalSocketImpl$SocketOutputStream
    //   19: astore_1
    //   20: aload_1
    //   21: aload_0
    //   22: invokespecial <init> : (Landroid/net/LocalSocketImpl;)V
    //   25: aload_0
    //   26: aload_1
    //   27: putfield fos : Landroid/net/LocalSocketImpl$SocketOutputStream;
    //   30: aload_0
    //   31: getfield fos : Landroid/net/LocalSocketImpl$SocketOutputStream;
    //   34: astore_1
    //   35: aload_0
    //   36: monitorexit
    //   37: aload_1
    //   38: areturn
    //   39: astore_1
    //   40: aload_0
    //   41: monitorexit
    //   42: aload_1
    //   43: athrow
    //   44: new java/io/IOException
    //   47: dup
    //   48: ldc 'socket not created'
    //   50: invokespecial <init> : (Ljava/lang/String;)V
    //   53: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #342	-> 0
    //   #346	-> 7
    //   #347	-> 9
    //   #348	-> 16
    //   #351	-> 30
    //   #352	-> 39
    //   #343	-> 44
    // Exception table:
    //   from	to	target	type
    //   9	16	39	finally
    //   16	30	39	finally
    //   30	37	39	finally
    //   40	42	39	finally
  }
  
  protected int available() throws IOException {
    return getInputStream().available();
  }
  
  protected void shutdownInput() throws IOException {
    FileDescriptor fileDescriptor = this.fd;
    if (fileDescriptor != null)
      try {
        Os.shutdown(fileDescriptor, OsConstants.SHUT_RD);
        return;
      } catch (ErrnoException errnoException) {
        throw errnoException.rethrowAsIOException();
      }  
    throw new IOException("socket not created");
  }
  
  protected void shutdownOutput() throws IOException {
    FileDescriptor fileDescriptor = this.fd;
    if (fileDescriptor != null)
      try {
        Os.shutdown(fileDescriptor, OsConstants.SHUT_WR);
        return;
      } catch (ErrnoException errnoException) {
        throw errnoException.rethrowAsIOException();
      }  
    throw new IOException("socket not created");
  }
  
  protected FileDescriptor getFileDescriptor() {
    return this.fd;
  }
  
  protected boolean supportsUrgentData() {
    return false;
  }
  
  protected void sendUrgentData(int paramInt) throws IOException {
    throw new RuntimeException("not impled");
  }
  
  public Object getOption(int paramInt) throws IOException {
    FileDescriptor fileDescriptor = this.fd;
    if (fileDescriptor != null) {
      Integer integer;
      if (paramInt != 1) {
        if (paramInt != 4) {
          Integer integer1;
          if (paramInt != 128) {
            if (paramInt != 4102) {
              if (paramInt != 4097 && paramInt != 4098)
                try {
                  IOException iOException = new IOException();
                  StringBuilder stringBuilder = new StringBuilder();
                  this();
                  stringBuilder.append("Unknown option: ");
                  stringBuilder.append(paramInt);
                  this(stringBuilder.toString());
                  throw iOException;
                } catch (ErrnoException errnoException) {
                  throw errnoException.rethrowAsIOException();
                }  
            } else {
              StructTimeval structTimeval = Os.getsockoptTimeval((FileDescriptor)errnoException, OsConstants.SOL_SOCKET, OsConstants.SO_SNDTIMEO);
              integer1 = Integer.valueOf((int)structTimeval.toMillis());
              return integer1;
            } 
          } else {
            Integer integer2;
            paramInt = OsConstants.SOL_SOCKET;
            int i = OsConstants.SO_LINGER;
            StructLinger structLinger = Os.getsockoptLinger((FileDescriptor)integer1, paramInt, i);
            if (!structLinger.isOn()) {
              integer2 = Integer.valueOf(-1);
            } else {
              integer2 = Integer.valueOf(((StructLinger)integer2).l_linger);
            } 
            return integer2;
          } 
        } 
        paramInt = javaSoToOsOpt(paramInt);
        integer = Integer.valueOf(Os.getsockoptInt(this.fd, OsConstants.SOL_SOCKET, paramInt));
      } else {
        paramInt = Os.getsockoptInt((FileDescriptor)integer, OsConstants.IPPROTO_TCP, OsConstants.TCP_NODELAY);
        integer = Integer.valueOf(paramInt);
      } 
      return integer;
    } 
    throw new IOException("socket not created");
  }
  
  public void setOption(int paramInt, Object paramObject) throws IOException {
    if (this.fd != null) {
      boolean bool;
      byte b = -1;
      int i = 0;
      if (paramObject instanceof Integer) {
        i = ((Integer)paramObject).intValue();
      } else if (paramObject instanceof Boolean) {
        bool = ((Boolean)paramObject).booleanValue();
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("bad value: ");
        stringBuilder.append(paramObject);
        throw new IOException(stringBuilder.toString());
      } 
      if (paramInt != 1) {
        if (paramInt != 4)
          if (paramInt != 128) {
            if (paramInt != 4102) {
              if (paramInt != 4097 && paramInt != 4098)
                try {
                  IOException iOException = new IOException();
                  paramObject = new StringBuilder();
                  super();
                  paramObject.append("Unknown option: ");
                  paramObject.append(paramInt);
                  this(paramObject.toString());
                  throw iOException;
                } catch (ErrnoException errnoException) {
                  throw errnoException.rethrowAsIOException();
                }  
            } else {
              paramObject = StructTimeval.fromMillis(i);
              Os.setsockoptTimeval(this.fd, OsConstants.SOL_SOCKET, OsConstants.SO_RCVTIMEO, (StructTimeval)paramObject);
              Os.setsockoptTimeval(this.fd, OsConstants.SOL_SOCKET, OsConstants.SO_SNDTIMEO, (StructTimeval)paramObject);
              return;
            } 
          } else {
            paramObject = new StructLinger();
            super(bool, i);
            Os.setsockoptLinger(this.fd, OsConstants.SOL_SOCKET, OsConstants.SO_LINGER, (StructLinger)paramObject);
            return;
          }  
        paramInt = javaSoToOsOpt(paramInt);
        Os.setsockoptInt(this.fd, OsConstants.SOL_SOCKET, paramInt, i);
      } else {
        Os.setsockoptInt(this.fd, OsConstants.IPPROTO_TCP, OsConstants.TCP_NODELAY, i);
      } 
      return;
    } 
    throw new IOException("socket not created");
  }
  
  public void setFileDescriptorsForSend(FileDescriptor[] paramArrayOfFileDescriptor) {
    synchronized (this.writeMonitor) {
      this.outboundFileDescriptors = paramArrayOfFileDescriptor;
      return;
    } 
  }
  
  public FileDescriptor[] getAncillaryFileDescriptors() throws IOException {
    synchronized (this.readMonitor) {
      FileDescriptor[] arrayOfFileDescriptor = this.inboundFileDescriptors;
      this.inboundFileDescriptors = null;
      return arrayOfFileDescriptor;
    } 
  }
  
  public Credentials getPeerCredentials() throws IOException {
    return getPeerCredentials_native(this.fd);
  }
  
  public LocalSocketAddress getSockAddress() throws IOException {
    return null;
  }
  
  protected void finalize() throws IOException {
    close();
  }
  
  private static int javaSoToOsOpt(int paramInt) {
    if (paramInt != 4) {
      if (paramInt != 4097) {
        if (paramInt == 4098)
          return OsConstants.SO_RCVBUF; 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown option: ");
        stringBuilder.append(paramInt);
        throw new UnsupportedOperationException(stringBuilder.toString());
      } 
      return OsConstants.SO_SNDBUF;
    } 
    return OsConstants.SO_REUSEADDR;
  }
  
  LocalSocketImpl() {}
  
  private native void bindLocal(FileDescriptor paramFileDescriptor, String paramString, int paramInt) throws IOException;
  
  private native void connectLocal(FileDescriptor paramFileDescriptor, String paramString, int paramInt) throws IOException;
  
  private native Credentials getPeerCredentials_native(FileDescriptor paramFileDescriptor) throws IOException;
  
  private native int read_native(FileDescriptor paramFileDescriptor) throws IOException;
  
  private native int readba_native(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, FileDescriptor paramFileDescriptor) throws IOException;
  
  private native void write_native(int paramInt, FileDescriptor paramFileDescriptor) throws IOException;
  
  private native void writeba_native(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, FileDescriptor paramFileDescriptor) throws IOException;
}
