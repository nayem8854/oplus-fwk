package android.net;

import android.annotation.SystemApi;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import java.lang.annotation.Annotation;

public class CaptivePortal implements Parcelable {
  private static final int APP_REQUEST_BASE = 100;
  
  @SystemApi
  public static final int APP_REQUEST_REEVALUATION_REQUIRED = 100;
  
  @SystemApi
  public static final int APP_RETURN_DISMISSED = 0;
  
  @SystemApi
  public static final int APP_RETURN_UNWANTED = 1;
  
  @SystemApi
  public static final int APP_RETURN_WANTED_AS_IS = 2;
  
  public CaptivePortal(IBinder paramIBinder) {
    this.mBinder = paramIBinder;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeStrongBinder(this.mBinder);
  }
  
  public static final Parcelable.Creator<CaptivePortal> CREATOR = new Parcelable.Creator<CaptivePortal>() {
      public CaptivePortal createFromParcel(Parcel param1Parcel) {
        return new CaptivePortal(param1Parcel.readStrongBinder());
      }
      
      public CaptivePortal[] newArray(int param1Int) {
        return new CaptivePortal[param1Int];
      }
    };
  
  private final IBinder mBinder;
  
  public void reportCaptivePortalDismissed() {
    try {
      ICaptivePortal.Stub.asInterface(this.mBinder).appResponse(0);
    } catch (RemoteException remoteException) {}
  }
  
  public void ignoreNetwork() {
    try {
      ICaptivePortal.Stub.asInterface(this.mBinder).appResponse(1);
    } catch (RemoteException remoteException) {}
  }
  
  @SystemApi
  public void useNetwork() {
    try {
      ICaptivePortal.Stub.asInterface(this.mBinder).appResponse(2);
    } catch (RemoteException remoteException) {}
  }
  
  @SystemApi
  public void reevaluateNetwork() {
    try {
      ICaptivePortal.Stub.asInterface(this.mBinder).appRequest(100);
    } catch (RemoteException remoteException) {}
  }
  
  @SystemApi
  public void logEvent(int paramInt, String paramString) {
    try {
      ICaptivePortal.Stub.asInterface(this.mBinder).logEvent(paramInt, paramString);
    } catch (RemoteException remoteException) {}
  }
  
  class EventId implements Annotation {}
}
