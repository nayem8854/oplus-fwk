package android.net;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Deprecated
public class NetworkBadging {
  public static final int BADGING_4K = 30;
  
  public static final int BADGING_HD = 20;
  
  public static final int BADGING_NONE = 0;
  
  public static final int BADGING_SD = 10;
  
  public static Drawable getWifiIcon(int paramInt1, int paramInt2, Resources.Theme paramTheme) {
    return Resources.getSystem().getDrawable(getWifiSignalResource(paramInt1), paramTheme);
  }
  
  private static int getWifiSignalResource(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt == 4)
              return 17302890; 
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Invalid signal level: ");
            stringBuilder.append(paramInt);
            throw new IllegalArgumentException(stringBuilder.toString());
          } 
          return 17302889;
        } 
        return 17302888;
      } 
      return 17302887;
    } 
    return 17302886;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Badging {}
}
