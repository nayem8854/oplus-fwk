package android.net;

import android.annotation.SystemApi;
import android.content.Context;
import android.os.Process;
import android.os.RemoteException;
import com.android.server.NetworkManagementSocketTagger;
import dalvik.system.SocketTagger;
import java.io.FileDescriptor;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.Socket;
import java.net.SocketException;

public class TrafficStats {
  @Deprecated
  public static final long GB_IN_BYTES = 1073741824L;
  
  @Deprecated
  public static final long KB_IN_BYTES = 1024L;
  
  private static final String LOOPBACK_IFACE = "lo";
  
  @Deprecated
  public static final long MB_IN_BYTES = 1048576L;
  
  @Deprecated
  public static final long PB_IN_BYTES = 1125899906842624L;
  
  @SystemApi
  public static final int TAG_NETWORK_STACK_IMPERSONATION_RANGE_END = -113;
  
  @SystemApi
  public static final int TAG_NETWORK_STACK_IMPERSONATION_RANGE_START = -128;
  
  @SystemApi
  public static final int TAG_NETWORK_STACK_RANGE_END = -257;
  
  @SystemApi
  public static final int TAG_NETWORK_STACK_RANGE_START = -768;
  
  public static final int TAG_SYSTEM_APP = -251;
  
  public static final int TAG_SYSTEM_BACKUP = -253;
  
  public static final int TAG_SYSTEM_DOWNLOAD = -255;
  
  @SystemApi
  public static final int TAG_SYSTEM_IMPERSONATION_RANGE_END = -241;
  
  @SystemApi
  public static final int TAG_SYSTEM_IMPERSONATION_RANGE_START = -256;
  
  public static final int TAG_SYSTEM_MEDIA = -254;
  
  public static final int TAG_SYSTEM_PROBE = -190;
  
  public static final int TAG_SYSTEM_RESTORE = -252;
  
  @Deprecated
  public static final long TB_IN_BYTES = 1099511627776L;
  
  public static final int TYPE_RX_BYTES = 0;
  
  public static final int TYPE_RX_PACKETS = 1;
  
  public static final int TYPE_TCP_RX_PACKETS = 4;
  
  public static final int TYPE_TCP_TX_PACKETS = 5;
  
  public static final int TYPE_TX_BYTES = 2;
  
  public static final int TYPE_TX_PACKETS = 3;
  
  public static final int UID_REMOVED = -4;
  
  public static final int UID_TETHERING = -5;
  
  public static final int UNSUPPORTED = -1;
  
  private static NetworkStats sActiveProfilingStart;
  
  private static INetworkStatsService getStatsService() {
    // Byte code:
    //   0: ldc android/net/TrafficStats
    //   2: monitorenter
    //   3: getstatic android/net/TrafficStats.sStatsService : Landroid/net/INetworkStatsService;
    //   6: ifnonnull -> 22
    //   9: ldc 'netstats'
    //   11: invokestatic getService : (Ljava/lang/String;)Landroid/os/IBinder;
    //   14: astore_0
    //   15: aload_0
    //   16: invokestatic asInterface : (Landroid/os/IBinder;)Landroid/net/INetworkStatsService;
    //   19: putstatic android/net/TrafficStats.sStatsService : Landroid/net/INetworkStatsService;
    //   22: getstatic android/net/TrafficStats.sStatsService : Landroid/net/INetworkStatsService;
    //   25: astore_0
    //   26: ldc android/net/TrafficStats
    //   28: monitorexit
    //   29: aload_0
    //   30: areturn
    //   31: astore_0
    //   32: ldc android/net/TrafficStats
    //   34: monitorexit
    //   35: aload_0
    //   36: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #176	-> 3
    //   #177	-> 9
    //   #178	-> 9
    //   #177	-> 15
    //   #180	-> 22
    //   #175	-> 31
    // Exception table:
    //   from	to	target	type
    //   3	9	31	finally
    //   9	15	31	finally
    //   15	22	31	finally
    //   22	26	31	finally
  }
  
  private static Object sProfilingLock = new Object();
  
  private static INetworkStatsService sStatsService;
  
  public static void setThreadStatsTag(int paramInt) {
    NetworkManagementSocketTagger.setThreadSocketStatsTag(paramInt);
  }
  
  public static int getAndSetThreadStatsTag(int paramInt) {
    return NetworkManagementSocketTagger.setThreadSocketStatsTag(paramInt);
  }
  
  @SystemApi
  public static void setThreadStatsTagBackup() {
    setThreadStatsTag(-253);
  }
  
  @SystemApi
  public static void setThreadStatsTagRestore() {
    setThreadStatsTag(-252);
  }
  
  @SystemApi
  public static void setThreadStatsTagApp() {
    setThreadStatsTag(-251);
  }
  
  public static int getThreadStatsTag() {
    return NetworkManagementSocketTagger.getThreadSocketStatsTag();
  }
  
  public static void clearThreadStatsTag() {
    NetworkManagementSocketTagger.setThreadSocketStatsTag(-1);
  }
  
  public static void setThreadStatsUid(int paramInt) {
    NetworkManagementSocketTagger.setThreadSocketStatsUid(paramInt);
  }
  
  public static int getThreadStatsUid() {
    return NetworkManagementSocketTagger.getThreadSocketStatsUid();
  }
  
  @Deprecated
  public static void setThreadStatsUidSelf() {
    setThreadStatsUid(Process.myUid());
  }
  
  public static void clearThreadStatsUid() {
    NetworkManagementSocketTagger.setThreadSocketStatsUid(-1);
  }
  
  public static void tagSocket(Socket paramSocket) throws SocketException {
    SocketTagger.get().tag(paramSocket);
  }
  
  public static void untagSocket(Socket paramSocket) throws SocketException {
    SocketTagger.get().untag(paramSocket);
  }
  
  public static void tagDatagramSocket(DatagramSocket paramDatagramSocket) throws SocketException {
    SocketTagger.get().tag(paramDatagramSocket);
  }
  
  public static void untagDatagramSocket(DatagramSocket paramDatagramSocket) throws SocketException {
    SocketTagger.get().untag(paramDatagramSocket);
  }
  
  public static void tagFileDescriptor(FileDescriptor paramFileDescriptor) throws IOException {
    SocketTagger.get().tag(paramFileDescriptor);
  }
  
  public static void untagFileDescriptor(FileDescriptor paramFileDescriptor) throws IOException {
    SocketTagger.get().untag(paramFileDescriptor);
  }
  
  public static void startDataProfiling(Context paramContext) {
    synchronized (sProfilingLock) {
      if (sActiveProfilingStart == null) {
        sActiveProfilingStart = getDataLayerSnapshotForUid(paramContext);
        return;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("already profiling data");
      throw illegalStateException;
    } 
  }
  
  public static NetworkStats stopDataProfiling(Context paramContext) {
    synchronized (sProfilingLock) {
      if (sActiveProfilingStart != null) {
        NetworkStats networkStats = getDataLayerSnapshotForUid(paramContext);
        networkStats = NetworkStats.subtract(networkStats, sActiveProfilingStart, null, null);
        sActiveProfilingStart = null;
        return networkStats;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("not profiling data");
      throw illegalStateException;
    } 
  }
  
  public static void incrementOperationCount(int paramInt) {
    int i = getThreadStatsTag();
    incrementOperationCount(i, paramInt);
  }
  
  public static void incrementOperationCount(int paramInt1, int paramInt2) {
    int i = Process.myUid();
    try {
      getStatsService().incrementOperationCount(i, paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static void closeQuietly(INetworkStatsSession paramINetworkStatsSession) {
    if (paramINetworkStatsSession != null)
      try {
        paramINetworkStatsSession.close();
      } catch (RuntimeException runtimeException) {
        throw runtimeException;
      } catch (Exception exception) {} 
  }
  
  private static long addIfSupported(long paramLong) {
    if (paramLong == -1L)
      paramLong = 0L; 
    return paramLong;
  }
  
  public static long getMobileTxPackets() {
    long l = 0L;
    for (String str : getMobileIfaces())
      l += addIfSupported(getTxPackets(str)); 
    return l;
  }
  
  public static long getMobileRxPackets() {
    long l = 0L;
    for (String str : getMobileIfaces())
      l += addIfSupported(getRxPackets(str)); 
    return l;
  }
  
  public static long getMobileTxBytes() {
    long l = 0L;
    for (String str : getMobileIfaces())
      l += addIfSupported(getTxBytes(str)); 
    return l;
  }
  
  public static long getMobileRxBytes() {
    long l = 0L;
    for (String str : getMobileIfaces())
      l += addIfSupported(getRxBytes(str)); 
    return l;
  }
  
  public static long getMobileTcpRxPackets() {
    long l = 0L;
    String[] arrayOfString;
    int i;
    byte b;
    for (arrayOfString = getMobileIfaces(), i = arrayOfString.length, b = 0; b < i; ) {
      String str = arrayOfString[b];
      try {
        long l1 = getStatsService().getIfaceStats(str, 4);
        l += addIfSupported(l1);
        b++;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
    return l;
  }
  
  public static long getMobileTcpTxPackets() {
    long l = 0L;
    String[] arrayOfString;
    int i;
    byte b;
    for (arrayOfString = getMobileIfaces(), i = arrayOfString.length, b = 0; b < i; ) {
      String str = arrayOfString[b];
      try {
        long l1 = getStatsService().getIfaceStats(str, 5);
        l += addIfSupported(l1);
        b++;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
    return l;
  }
  
  public static long getTxPackets(String paramString) {
    try {
      return getStatsService().getIfaceStats(paramString, 3);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static long getRxPackets(String paramString) {
    try {
      return getStatsService().getIfaceStats(paramString, 1);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static long getTcpTxPackets(String paramString) {
    try {
      return getStatsService().getIfaceStats(paramString, 5);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static long getTcpRxPackets(String paramString) {
    try {
      return getStatsService().getIfaceStats(paramString, 4);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static long getTxBytes(String paramString) {
    try {
      return getStatsService().getIfaceStats(paramString, 2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static long getRxBytes(String paramString) {
    try {
      return getStatsService().getIfaceStats(paramString, 0);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static long getLoopbackTxPackets() {
    try {
      return getStatsService().getIfaceStats("lo", 3);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static long getLoopbackRxPackets() {
    try {
      return getStatsService().getIfaceStats("lo", 1);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static long getLoopbackTxBytes() {
    try {
      return getStatsService().getIfaceStats("lo", 2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static long getLoopbackRxBytes() {
    try {
      return getStatsService().getIfaceStats("lo", 0);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static long getTotalTxPackets() {
    try {
      return getStatsService().getTotalStats(3);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static long getTotalRxPackets() {
    try {
      return getStatsService().getTotalStats(1);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static long getTotalTxBytes() {
    try {
      return getStatsService().getTotalStats(2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static long getTotalRxBytes() {
    try {
      return getStatsService().getTotalStats(0);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static long getUidTxBytes(int paramInt) {
    try {
      return getStatsService().getUidStats(paramInt, 2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static long getUidRxBytes(int paramInt) {
    try {
      return getStatsService().getUidStats(paramInt, 0);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static long getUidTxPackets(int paramInt) {
    try {
      return getStatsService().getUidStats(paramInt, 3);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static long getUidRxPackets(int paramInt) {
    try {
      return getStatsService().getUidStats(paramInt, 1);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public static long getUidTcpTxBytes(int paramInt) {
    return -1L;
  }
  
  @Deprecated
  public static long getUidTcpRxBytes(int paramInt) {
    return -1L;
  }
  
  @Deprecated
  public static long getUidUdpTxBytes(int paramInt) {
    return -1L;
  }
  
  @Deprecated
  public static long getUidUdpRxBytes(int paramInt) {
    return -1L;
  }
  
  @Deprecated
  public static long getUidTcpTxSegments(int paramInt) {
    return -1L;
  }
  
  @Deprecated
  public static long getUidTcpRxSegments(int paramInt) {
    return -1L;
  }
  
  @Deprecated
  public static long getUidUdpTxPackets(int paramInt) {
    return -1L;
  }
  
  @Deprecated
  public static long getUidUdpRxPackets(int paramInt) {
    return -1L;
  }
  
  private static NetworkStats getDataLayerSnapshotForUid(Context paramContext) {
    int i = Process.myUid();
    try {
      return getStatsService().getDataLayerSnapshotForUid(i);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private static String[] getMobileIfaces() {
    try {
      return getStatsService().getMobileIfaces();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
}
