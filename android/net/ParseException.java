package android.net;

public class ParseException extends RuntimeException {
  public String response;
  
  ParseException(String paramString) {
    super(paramString);
    this.response = paramString;
  }
  
  ParseException(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
    this.response = paramString;
  }
}
