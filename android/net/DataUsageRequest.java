package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public final class DataUsageRequest implements Parcelable {
  public DataUsageRequest(int paramInt, NetworkTemplate paramNetworkTemplate, long paramLong) {
    this.requestId = paramInt;
    this.template = paramNetworkTemplate;
    this.thresholdInBytes = paramLong;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.requestId);
    paramParcel.writeParcelable(this.template, paramInt);
    paramParcel.writeLong(this.thresholdInBytes);
  }
  
  public static final Parcelable.Creator<DataUsageRequest> CREATOR = new Parcelable.Creator<DataUsageRequest>() {
      public DataUsageRequest createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        NetworkTemplate networkTemplate = param1Parcel.<NetworkTemplate>readParcelable(null);
        long l = param1Parcel.readLong();
        return new DataUsageRequest(i, networkTemplate, l);
      }
      
      public DataUsageRequest[] newArray(int param1Int) {
        return new DataUsageRequest[param1Int];
      }
    };
  
  public static final String PARCELABLE_KEY = "DataUsageRequest";
  
  public static final int REQUEST_ID_UNSET = 0;
  
  public final int requestId;
  
  public final NetworkTemplate template;
  
  public final long thresholdInBytes;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("DataUsageRequest [ requestId=");
    stringBuilder.append(this.requestId);
    stringBuilder.append(", networkTemplate=");
    stringBuilder.append(this.template);
    stringBuilder.append(", thresholdInBytes=");
    stringBuilder.append(this.thresholdInBytes);
    stringBuilder.append(" ]");
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof DataUsageRequest;
    boolean bool1 = false;
    if (!bool)
      return false; 
    DataUsageRequest dataUsageRequest = (DataUsageRequest)paramObject;
    if (dataUsageRequest.requestId == this.requestId) {
      NetworkTemplate networkTemplate = dataUsageRequest.template;
      paramObject = this.template;
      if (Objects.equals(networkTemplate, paramObject) && dataUsageRequest.thresholdInBytes == this.thresholdInBytes)
        bool1 = true; 
    } 
    return bool1;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.requestId), this.template, Long.valueOf(this.thresholdInBytes) });
  }
}
