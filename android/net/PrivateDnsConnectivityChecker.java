package android.net;

import android.util.Log;
import java.io.IOException;
import java.net.InetSocketAddress;
import javax.net.SocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class PrivateDnsConnectivityChecker {
  private static final int CONNECTION_TIMEOUT_MS = 5000;
  
  private static final int PRIVATE_DNS_PORT = 853;
  
  private static final String TAG = "NetworkUtils";
  
  public static boolean canConnectToPrivateDnsServer(String paramString) {
    SocketFactory socketFactory = SSLSocketFactory.getDefault();
    TrafficStats.setThreadStatsTag(-251);
    try {
      SSLSocket sSLSocket = (SSLSocket)socketFactory.createSocket();
      try {
        sSLSocket.setSoTimeout(5000);
        InetSocketAddress inetSocketAddress = new InetSocketAddress();
        this(paramString, 853);
        sSLSocket.connect(inetSocketAddress);
        if (!sSLSocket.isConnected()) {
          Log.w("NetworkUtils", String.format("Connection to %s failed.", new Object[] { paramString }));
          return false;
        } 
        sSLSocket.startHandshake();
        Log.w("NetworkUtils", String.format("TLS handshake to %s succeeded.", new Object[] { paramString }));
        return true;
      } finally {
        if (sSLSocket != null)
          try {
            sSLSocket.close();
          } finally {
            sSLSocket = null;
          }  
      } 
    } catch (IOException iOException) {
      Log.w("NetworkUtils", String.format("TLS handshake to %s failed.", new Object[] { paramString }), iOException);
      return false;
    } 
  }
}
