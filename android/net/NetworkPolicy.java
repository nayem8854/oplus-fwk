package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.BackupUtils;
import android.util.Range;
import android.util.RecurrenceRule;
import com.android.internal.util.Preconditions;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Iterator;
import java.util.Objects;

public class NetworkPolicy implements Parcelable, Comparable<NetworkPolicy> {
  public static RecurrenceRule buildRule(int paramInt, ZoneId paramZoneId) {
    if (paramInt != -1)
      return RecurrenceRule.buildRecurringMonthly(paramInt, paramZoneId); 
    return RecurrenceRule.buildNever();
  }
  
  @Deprecated
  public NetworkPolicy(NetworkTemplate paramNetworkTemplate, int paramInt, String paramString, long paramLong1, long paramLong2, boolean paramBoolean) {
    this(paramNetworkTemplate, paramInt, paramString, paramLong1, paramLong2, -1L, -1L, paramBoolean, false);
  }
  
  @Deprecated
  public NetworkPolicy(NetworkTemplate paramNetworkTemplate, int paramInt, String paramString, long paramLong1, long paramLong2, long paramLong3, long paramLong4, boolean paramBoolean1, boolean paramBoolean2) {
    this(paramNetworkTemplate, buildRule(paramInt, ZoneId.of(paramString)), paramLong1, paramLong2, paramLong3, paramLong4, paramBoolean1, paramBoolean2);
  }
  
  @Deprecated
  public NetworkPolicy(NetworkTemplate paramNetworkTemplate, RecurrenceRule paramRecurrenceRule, long paramLong1, long paramLong2, long paramLong3, long paramLong4, boolean paramBoolean1, boolean paramBoolean2) {
    this(paramNetworkTemplate, paramRecurrenceRule, paramLong1, paramLong2, paramLong3, paramLong4, -1L, paramBoolean1, paramBoolean2);
  }
  
  public NetworkPolicy(NetworkTemplate paramNetworkTemplate, RecurrenceRule paramRecurrenceRule, long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5, boolean paramBoolean1, boolean paramBoolean2) {
    this.warningBytes = -1L;
    this.limitBytes = -1L;
    this.lastWarningSnooze = -1L;
    this.lastLimitSnooze = -1L;
    this.lastRapidSnooze = -1L;
    this.metered = true;
    this.inferred = false;
    this.template = (NetworkTemplate)Preconditions.checkNotNull(paramNetworkTemplate, "missing NetworkTemplate");
    this.cycleRule = (RecurrenceRule)Preconditions.checkNotNull(paramRecurrenceRule, "missing RecurrenceRule");
    this.warningBytes = paramLong1;
    this.limitBytes = paramLong2;
    this.lastWarningSnooze = paramLong3;
    this.lastLimitSnooze = paramLong4;
    this.lastRapidSnooze = paramLong5;
    this.metered = paramBoolean1;
    this.inferred = paramBoolean2;
  }
  
  private NetworkPolicy(Parcel paramParcel) {
    boolean bool2;
    this.warningBytes = -1L;
    this.limitBytes = -1L;
    this.lastWarningSnooze = -1L;
    this.lastLimitSnooze = -1L;
    this.lastRapidSnooze = -1L;
    boolean bool1 = true;
    this.metered = true;
    this.inferred = false;
    this.template = paramParcel.<NetworkTemplate>readParcelable(null);
    this.cycleRule = paramParcel.<RecurrenceRule>readParcelable(null);
    this.warningBytes = paramParcel.readLong();
    this.limitBytes = paramParcel.readLong();
    this.lastWarningSnooze = paramParcel.readLong();
    this.lastLimitSnooze = paramParcel.readLong();
    this.lastRapidSnooze = paramParcel.readLong();
    if (paramParcel.readInt() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.metered = bool2;
    if (paramParcel.readInt() != 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.inferred = bool2;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.template, paramInt);
    paramParcel.writeParcelable((Parcelable)this.cycleRule, paramInt);
    paramParcel.writeLong(this.warningBytes);
    paramParcel.writeLong(this.limitBytes);
    paramParcel.writeLong(this.lastWarningSnooze);
    paramParcel.writeLong(this.lastLimitSnooze);
    paramParcel.writeLong(this.lastRapidSnooze);
    paramParcel.writeInt(this.metered);
    paramParcel.writeInt(this.inferred);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public Iterator<Range<ZonedDateTime>> cycleIterator() {
    return this.cycleRule.cycleIterator();
  }
  
  public boolean isOverWarning(long paramLong) {
    boolean bool;
    long l = this.warningBytes;
    if (l != -1L && paramLong >= l) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isOverLimit(long paramLong) {
    boolean bool;
    long l = this.limitBytes;
    if (l != -1L && paramLong + 3000L >= l) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void clearSnooze() {
    this.lastWarningSnooze = -1L;
    this.lastLimitSnooze = -1L;
    this.lastRapidSnooze = -1L;
  }
  
  public boolean hasCycle() {
    return this.cycleRule.cycleIterator().hasNext();
  }
  
  public int compareTo(NetworkPolicy paramNetworkPolicy) {
    if (paramNetworkPolicy != null) {
      long l = paramNetworkPolicy.limitBytes;
      if (l != -1L) {
        long l1 = this.limitBytes;
        if (l1 == -1L || l < l1)
          return 1; 
        return 0;
      } 
    } 
    return -1;
  }
  
  public int hashCode() {
    NetworkTemplate networkTemplate = this.template;
    RecurrenceRule recurrenceRule = this.cycleRule;
    long l1 = this.warningBytes, l2 = this.limitBytes, l3 = this.lastWarningSnooze;
    long l4 = this.lastLimitSnooze, l5 = this.lastRapidSnooze;
    boolean bool1 = this.metered, bool2 = this.inferred;
    return Objects.hash(new Object[] { networkTemplate, recurrenceRule, Long.valueOf(l1), Long.valueOf(l2), Long.valueOf(l3), Long.valueOf(l4), Long.valueOf(l5), Boolean.valueOf(bool1), Boolean.valueOf(bool2) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof NetworkPolicy;
    boolean bool1 = false;
    if (bool) {
      paramObject = paramObject;
      if (this.warningBytes == ((NetworkPolicy)paramObject).warningBytes && this.limitBytes == ((NetworkPolicy)paramObject).limitBytes && this.lastWarningSnooze == ((NetworkPolicy)paramObject).lastWarningSnooze && this.lastLimitSnooze == ((NetworkPolicy)paramObject).lastLimitSnooze && this.lastRapidSnooze == ((NetworkPolicy)paramObject).lastRapidSnooze && this.metered == ((NetworkPolicy)paramObject).metered && this.inferred == ((NetworkPolicy)paramObject).inferred) {
        NetworkTemplate networkTemplate1 = this.template, networkTemplate2 = ((NetworkPolicy)paramObject).template;
        if (Objects.equals(networkTemplate1, networkTemplate2)) {
          RecurrenceRule recurrenceRule = this.cycleRule;
          paramObject = ((NetworkPolicy)paramObject).cycleRule;
          if (Objects.equals(recurrenceRule, paramObject))
            bool1 = true; 
        } 
      } 
      return bool1;
    } 
    return false;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("NetworkPolicy{");
    stringBuilder.append("template=");
    stringBuilder.append(this.template);
    stringBuilder.append(" cycleRule=");
    stringBuilder.append(this.cycleRule);
    stringBuilder.append(" warningBytes=");
    stringBuilder.append(this.warningBytes);
    stringBuilder.append(" limitBytes=");
    stringBuilder.append(this.limitBytes);
    stringBuilder.append(" lastWarningSnooze=");
    stringBuilder.append(this.lastWarningSnooze);
    stringBuilder.append(" lastLimitSnooze=");
    stringBuilder.append(this.lastLimitSnooze);
    stringBuilder.append(" lastRapidSnooze=");
    stringBuilder.append(this.lastRapidSnooze);
    stringBuilder.append(" metered=");
    stringBuilder.append(this.metered);
    stringBuilder.append(" inferred=");
    stringBuilder.append(this.inferred);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<NetworkPolicy> CREATOR = new Parcelable.Creator<NetworkPolicy>() {
      public NetworkPolicy createFromParcel(Parcel param1Parcel) {
        return new NetworkPolicy(param1Parcel);
      }
      
      public NetworkPolicy[] newArray(int param1Int) {
        return new NetworkPolicy[param1Int];
      }
    };
  
  public static final int CYCLE_NONE = -1;
  
  private static final long DEFAULT_MTU = 1500L;
  
  public static final long LIMIT_DISABLED = -1L;
  
  public static final long SNOOZE_NEVER = -1L;
  
  private static final int VERSION_INIT = 1;
  
  private static final int VERSION_RAPID = 3;
  
  private static final int VERSION_RULE = 2;
  
  public static final long WARNING_DISABLED = -1L;
  
  public RecurrenceRule cycleRule;
  
  public boolean inferred;
  
  public long lastLimitSnooze;
  
  public long lastRapidSnooze;
  
  public long lastWarningSnooze;
  
  public long limitBytes;
  
  @Deprecated
  public boolean metered;
  
  public NetworkTemplate template;
  
  public long warningBytes;
  
  public byte[] getBytesForBackup() throws IOException {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
    dataOutputStream.writeInt(3);
    dataOutputStream.write(this.template.getBytesForBackup());
    this.cycleRule.writeToStream(dataOutputStream);
    dataOutputStream.writeLong(this.warningBytes);
    dataOutputStream.writeLong(this.limitBytes);
    dataOutputStream.writeLong(this.lastWarningSnooze);
    dataOutputStream.writeLong(this.lastLimitSnooze);
    dataOutputStream.writeLong(this.lastRapidSnooze);
    dataOutputStream.writeInt(this.metered);
    dataOutputStream.writeInt(this.inferred);
    return byteArrayOutputStream.toByteArray();
  }
  
  public static NetworkPolicy getNetworkPolicyFromBackup(DataInputStream paramDataInputStream) throws IOException, BackupUtils.BadVersionException {
    int i = paramDataInputStream.readInt();
    if (i >= 1 && i <= 3) {
      RecurrenceRule recurrenceRule;
      long l5;
      boolean bool1, bool2;
      NetworkTemplate networkTemplate = NetworkTemplate.getNetworkTemplateFromBackup(paramDataInputStream);
      if (i >= 2) {
        recurrenceRule = new RecurrenceRule(paramDataInputStream);
      } else {
        int j = paramDataInputStream.readInt();
        String str = BackupUtils.readString(paramDataInputStream);
        recurrenceRule = buildRule(j, ZoneId.of(str));
      } 
      long l1 = paramDataInputStream.readLong();
      long l2 = paramDataInputStream.readLong();
      long l3 = paramDataInputStream.readLong();
      long l4 = paramDataInputStream.readLong();
      if (i >= 3) {
        l5 = paramDataInputStream.readLong();
      } else {
        l5 = -1L;
      } 
      if (paramDataInputStream.readInt() == 1) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      if (paramDataInputStream.readInt() == 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      return new NetworkPolicy(networkTemplate, recurrenceRule, l1, l2, l3, l4, l5, bool1, bool2);
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unknown backup version: ");
    stringBuilder.append(i);
    throw new BackupUtils.BadVersionException(stringBuilder.toString());
  }
}
