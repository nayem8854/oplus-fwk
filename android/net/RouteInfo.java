package android.net;

import android.annotation.SystemApi;
import android.net.util.NetUtils;
import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Objects;

public final class RouteInfo implements Parcelable {
  @SystemApi
  public RouteInfo(IpPrefix paramIpPrefix, InetAddress paramInetAddress, String paramString, int paramInt) {
    this(paramIpPrefix, paramInetAddress, paramString, paramInt, 0);
  }
  
  @SystemApi
  public RouteInfo(IpPrefix paramIpPrefix, InetAddress paramInetAddress, String paramString, int paramInt1, int paramInt2) {
    if (paramInt1 == 1 || paramInt1 == 7 || paramInt1 == 9) {
      StringBuilder stringBuilder1;
      IpPrefix ipPrefix = paramIpPrefix;
      if (paramIpPrefix == null)
        if (paramInetAddress != null) {
          if (paramInetAddress instanceof Inet4Address) {
            ipPrefix = new IpPrefix(Inet4Address.ANY, 0);
          } else {
            ipPrefix = new IpPrefix(Inet6Address.ANY, 0);
          } 
        } else {
          stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Invalid arguments passed in: ");
          stringBuilder1.append(paramInetAddress);
          stringBuilder1.append(",");
          stringBuilder1.append(paramIpPrefix);
          throw new IllegalArgumentException(stringBuilder1.toString());
        }  
      InetAddress inetAddress = paramInetAddress;
      if (paramInetAddress == null)
        if (ipPrefix.getAddress() instanceof Inet4Address) {
          inetAddress = Inet4Address.ANY;
        } else {
          inetAddress = Inet6Address.ANY;
        }  
      this.mHasGateway = true ^ inetAddress.isAnyLocalAddress();
      if (!(ipPrefix.getAddress() instanceof Inet4Address) || inetAddress instanceof Inet4Address)
        if (!(ipPrefix.getAddress() instanceof Inet6Address) || inetAddress instanceof Inet6Address) {
          this.mDestination = ipPrefix;
          this.mGateway = inetAddress;
          this.mInterface = (String)stringBuilder1;
          this.mType = paramInt1;
          this.mIsHost = isHost();
          this.mMtu = paramInt2;
          return;
        }  
      throw new IllegalArgumentException("address family mismatch in RouteInfo constructor");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unknown route type ");
    stringBuilder.append(paramInt1);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public RouteInfo(IpPrefix paramIpPrefix, InetAddress paramInetAddress, String paramString) {
    this(paramIpPrefix, paramInetAddress, paramString, 1);
  }
  
  public RouteInfo(LinkAddress paramLinkAddress, InetAddress paramInetAddress, String paramString) {
    this(ipPrefix, paramInetAddress, paramString);
  }
  
  public RouteInfo(IpPrefix paramIpPrefix, InetAddress paramInetAddress) {
    this(paramIpPrefix, paramInetAddress, (String)null);
  }
  
  public RouteInfo(LinkAddress paramLinkAddress, InetAddress paramInetAddress) {
    this(paramLinkAddress, paramInetAddress, (String)null);
  }
  
  public RouteInfo(InetAddress paramInetAddress) {
    this((IpPrefix)null, paramInetAddress, (String)null);
  }
  
  public RouteInfo(IpPrefix paramIpPrefix) {
    this(paramIpPrefix, (InetAddress)null, (String)null);
  }
  
  public RouteInfo(LinkAddress paramLinkAddress) {
    this(paramLinkAddress, (InetAddress)null, (String)null);
  }
  
  public RouteInfo(IpPrefix paramIpPrefix, int paramInt) {
    this(paramIpPrefix, null, null, paramInt);
  }
  
  public static RouteInfo makeHostRoute(InetAddress paramInetAddress, String paramString) {
    return makeHostRoute(paramInetAddress, null, paramString);
  }
  
  public static RouteInfo makeHostRoute(InetAddress paramInetAddress1, InetAddress paramInetAddress2, String paramString) {
    if (paramInetAddress1 == null)
      return null; 
    if (paramInetAddress1 instanceof Inet4Address)
      return new RouteInfo(new IpPrefix(paramInetAddress1, 32), paramInetAddress2, paramString); 
    return new RouteInfo(new IpPrefix(paramInetAddress1, 128), paramInetAddress2, paramString);
  }
  
  private boolean isHost() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mDestination : Landroid/net/IpPrefix;
    //   4: invokevirtual getAddress : ()Ljava/net/InetAddress;
    //   7: instanceof java/net/Inet4Address
    //   10: ifeq -> 27
    //   13: aload_0
    //   14: getfield mDestination : Landroid/net/IpPrefix;
    //   17: astore_1
    //   18: aload_1
    //   19: invokevirtual getPrefixLength : ()I
    //   22: bipush #32
    //   24: if_icmpeq -> 57
    //   27: aload_0
    //   28: getfield mDestination : Landroid/net/IpPrefix;
    //   31: astore_1
    //   32: aload_1
    //   33: invokevirtual getAddress : ()Ljava/net/InetAddress;
    //   36: instanceof java/net/Inet6Address
    //   39: ifeq -> 62
    //   42: aload_0
    //   43: getfield mDestination : Landroid/net/IpPrefix;
    //   46: astore_1
    //   47: aload_1
    //   48: invokevirtual getPrefixLength : ()I
    //   51: sipush #128
    //   54: if_icmpne -> 62
    //   57: iconst_1
    //   58: istore_2
    //   59: goto -> 64
    //   62: iconst_0
    //   63: istore_2
    //   64: iload_2
    //   65: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #347	-> 0
    //   #348	-> 18
    //   #349	-> 32
    //   #350	-> 47
    //   #347	-> 64
  }
  
  public IpPrefix getDestination() {
    return this.mDestination;
  }
  
  public LinkAddress getDestinationLinkAddress() {
    return new LinkAddress(this.mDestination.getAddress(), this.mDestination.getPrefixLength());
  }
  
  public InetAddress getGateway() {
    return this.mGateway;
  }
  
  public String getInterface() {
    return this.mInterface;
  }
  
  @SystemApi
  public int getType() {
    return this.mType;
  }
  
  @SystemApi
  public int getMtu() {
    return this.mMtu;
  }
  
  public boolean isDefaultRoute() {
    int i = this.mType;
    boolean bool = true;
    if (i != 1 || this.mDestination.getPrefixLength() != 0)
      bool = false; 
    return bool;
  }
  
  private boolean isUnreachableDefaultRoute() {
    boolean bool;
    if (this.mType == 7 && this.mDestination.getPrefixLength() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isIPv4Default() {
    boolean bool;
    if (isDefaultRoute() && this.mDestination.getAddress() instanceof Inet4Address) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isIPv4UnreachableDefault() {
    boolean bool;
    if (isUnreachableDefaultRoute() && this.mDestination.getAddress() instanceof Inet4Address) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isIPv6Default() {
    boolean bool;
    if (isDefaultRoute() && this.mDestination.getAddress() instanceof Inet6Address) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isIPv6UnreachableDefault() {
    boolean bool;
    if (isUnreachableDefaultRoute() && this.mDestination.getAddress() instanceof Inet6Address) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isHostRoute() {
    return this.mIsHost;
  }
  
  public boolean hasGateway() {
    return this.mHasGateway;
  }
  
  public boolean matches(InetAddress paramInetAddress) {
    return this.mDestination.contains(paramInetAddress);
  }
  
  public static RouteInfo selectBestRoute(Collection<RouteInfo> paramCollection, InetAddress paramInetAddress) {
    return NetUtils.selectBestRoute(paramCollection, paramInetAddress);
  }
  
  public String toString() {
    String str = "";
    IpPrefix ipPrefix = this.mDestination;
    if (ipPrefix != null)
      str = ipPrefix.toString(); 
    int i = this.mType;
    if (i == 7) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(str);
      stringBuilder1.append(" unreachable");
      str = stringBuilder1.toString();
    } else if (i == 9) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(str);
      stringBuilder1.append(" throw");
      str = stringBuilder1.toString();
    } else {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(str);
      stringBuilder1.append(" ->");
      String str2 = stringBuilder1.toString();
      str = str2;
      if (this.mGateway != null) {
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append(str2);
        stringBuilder2.append(" ");
        stringBuilder2.append(this.mGateway.getHostAddress());
        str1 = stringBuilder2.toString();
      } 
      str2 = str1;
      if (this.mInterface != null) {
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append(str1);
        stringBuilder2.append(" ");
        stringBuilder2.append(this.mInterface);
        str2 = stringBuilder2.toString();
      } 
      String str1 = str2;
      if (this.mType != 1) {
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append(str2);
        stringBuilder2.append(" unknown type ");
        stringBuilder2.append(this.mType);
        str = stringBuilder2.toString();
      } 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str);
    stringBuilder.append(" mtu ");
    stringBuilder.append(this.mMtu);
    str = stringBuilder.toString();
    return str;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof RouteInfo))
      return false; 
    paramObject = paramObject;
    if (Objects.equals(this.mDestination, paramObject.getDestination())) {
      InetAddress inetAddress = this.mGateway;
      if (Objects.equals(inetAddress, paramObject.getGateway())) {
        String str = this.mInterface;
        if (Objects.equals(str, paramObject.getInterface())) {
          int i = this.mType;
          if (i == paramObject.getType() && this.mMtu == paramObject.getMtu())
            return null; 
        } 
      } 
    } 
    return false;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class RouteType implements Annotation {}
  
  class RouteKey {
    private final IpPrefix mDestination;
    
    private final InetAddress mGateway;
    
    private final String mInterface;
    
    RouteKey(RouteInfo this$0, InetAddress param1InetAddress, String param1String) {
      this.mDestination = (IpPrefix)this$0;
      this.mGateway = param1InetAddress;
      this.mInterface = param1String;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof RouteKey;
      boolean bool1 = false;
      if (!bool)
        return false; 
      RouteKey routeKey = (RouteKey)param1Object;
      if (Objects.equals(routeKey.mDestination, this.mDestination)) {
        param1Object = routeKey.mGateway;
        InetAddress inetAddress = this.mGateway;
        if (Objects.equals(param1Object, inetAddress)) {
          param1Object = routeKey.mInterface;
          String str = this.mInterface;
          if (Objects.equals(param1Object, str))
            bool1 = true; 
        } 
      } 
      return bool1;
    }
    
    public int hashCode() {
      return Objects.hash(new Object[] { this.mDestination, this.mGateway, this.mInterface });
    }
  }
  
  public RouteKey getRouteKey() {
    return new RouteKey(this.mDestination, this.mGateway, this.mInterface);
  }
  
  public int hashCode() {
    int k, i = this.mDestination.hashCode();
    InetAddress inetAddress = this.mGateway;
    int j = 0;
    if (inetAddress == null) {
      k = 0;
    } else {
      k = inetAddress.hashCode() * 47;
    } 
    String str = this.mInterface;
    if (str != null)
      j = str.hashCode() * 67; 
    int m = this.mType, n = this.mMtu;
    return i * 41 + k + j + m * 71 + n * 89;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    byte[] arrayOfByte;
    paramParcel.writeParcelable(this.mDestination, paramInt);
    InetAddress inetAddress = this.mGateway;
    if (inetAddress == null) {
      inetAddress = null;
    } else {
      arrayOfByte = inetAddress.getAddress();
    } 
    paramParcel.writeByteArray(arrayOfByte);
    paramParcel.writeString(this.mInterface);
    paramParcel.writeInt(this.mType);
    paramParcel.writeInt(this.mMtu);
  }
  
  public static final Parcelable.Creator<RouteInfo> CREATOR = new Parcelable.Creator<RouteInfo>() {
      public RouteInfo createFromParcel(Parcel param1Parcel) {
        IpPrefix ipPrefix = param1Parcel.<IpPrefix>readParcelable(null);
        InetAddress inetAddress = null;
        byte[] arrayOfByte = param1Parcel.createByteArray();
        try {
          InetAddress inetAddress1 = InetAddress.getByAddress(arrayOfByte);
        } catch (UnknownHostException unknownHostException) {}
        String str = param1Parcel.readString();
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        return new RouteInfo(ipPrefix, inetAddress, str, i, j);
      }
      
      public RouteInfo[] newArray(int param1Int) {
        return new RouteInfo[param1Int];
      }
    };
  
  @SystemApi
  public static final int RTN_THROW = 9;
  
  @SystemApi
  public static final int RTN_UNICAST = 1;
  
  @SystemApi
  public static final int RTN_UNREACHABLE = 7;
  
  private final IpPrefix mDestination;
  
  private final InetAddress mGateway;
  
  private final boolean mHasGateway;
  
  private final String mInterface;
  
  private final boolean mIsHost;
  
  private final int mMtu;
  
  private final int mType;
}
