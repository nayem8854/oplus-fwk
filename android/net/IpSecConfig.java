package android.net;

import android.os.Parcel;
import android.os.Parcelable;

public final class IpSecConfig implements Parcelable {
  private int mMode = 0;
  
  private String mSourceAddress = "";
  
  private String mDestinationAddress = "";
  
  private int mSpiResourceId = -1;
  
  private int mEncapType = 0;
  
  private int mEncapSocketResourceId = -1;
  
  public void setMode(int paramInt) {
    this.mMode = paramInt;
  }
  
  public void setSourceAddress(String paramString) {
    this.mSourceAddress = paramString;
  }
  
  public void setDestinationAddress(String paramString) {
    this.mDestinationAddress = paramString;
  }
  
  public void setSpiResourceId(int paramInt) {
    this.mSpiResourceId = paramInt;
  }
  
  public void setEncryption(IpSecAlgorithm paramIpSecAlgorithm) {
    this.mEncryption = paramIpSecAlgorithm;
  }
  
  public void setAuthentication(IpSecAlgorithm paramIpSecAlgorithm) {
    this.mAuthentication = paramIpSecAlgorithm;
  }
  
  public void setAuthenticatedEncryption(IpSecAlgorithm paramIpSecAlgorithm) {
    this.mAuthenticatedEncryption = paramIpSecAlgorithm;
  }
  
  public void setNetwork(Network paramNetwork) {
    this.mNetwork = paramNetwork;
  }
  
  public void setEncapType(int paramInt) {
    this.mEncapType = paramInt;
  }
  
  public void setEncapSocketResourceId(int paramInt) {
    this.mEncapSocketResourceId = paramInt;
  }
  
  public void setEncapRemotePort(int paramInt) {
    this.mEncapRemotePort = paramInt;
  }
  
  public void setNattKeepaliveInterval(int paramInt) {
    this.mNattKeepaliveInterval = paramInt;
  }
  
  public void setMarkValue(int paramInt) {
    this.mMarkValue = paramInt;
  }
  
  public void setMarkMask(int paramInt) {
    this.mMarkMask = paramInt;
  }
  
  public void setXfrmInterfaceId(int paramInt) {
    this.mXfrmInterfaceId = paramInt;
  }
  
  public int getMode() {
    return this.mMode;
  }
  
  public String getSourceAddress() {
    return this.mSourceAddress;
  }
  
  public int getSpiResourceId() {
    return this.mSpiResourceId;
  }
  
  public String getDestinationAddress() {
    return this.mDestinationAddress;
  }
  
  public IpSecAlgorithm getEncryption() {
    return this.mEncryption;
  }
  
  public IpSecAlgorithm getAuthentication() {
    return this.mAuthentication;
  }
  
  public IpSecAlgorithm getAuthenticatedEncryption() {
    return this.mAuthenticatedEncryption;
  }
  
  public Network getNetwork() {
    return this.mNetwork;
  }
  
  public int getEncapType() {
    return this.mEncapType;
  }
  
  public int getEncapSocketResourceId() {
    return this.mEncapSocketResourceId;
  }
  
  public int getEncapRemotePort() {
    return this.mEncapRemotePort;
  }
  
  public int getNattKeepaliveInterval() {
    return this.mNattKeepaliveInterval;
  }
  
  public int getMarkValue() {
    return this.mMarkValue;
  }
  
  public int getMarkMask() {
    return this.mMarkMask;
  }
  
  public int getXfrmInterfaceId() {
    return this.mXfrmInterfaceId;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mMode);
    paramParcel.writeString(this.mSourceAddress);
    paramParcel.writeString(this.mDestinationAddress);
    paramParcel.writeParcelable(this.mNetwork, paramInt);
    paramParcel.writeInt(this.mSpiResourceId);
    paramParcel.writeParcelable(this.mEncryption, paramInt);
    paramParcel.writeParcelable(this.mAuthentication, paramInt);
    paramParcel.writeParcelable(this.mAuthenticatedEncryption, paramInt);
    paramParcel.writeInt(this.mEncapType);
    paramParcel.writeInt(this.mEncapSocketResourceId);
    paramParcel.writeInt(this.mEncapRemotePort);
    paramParcel.writeInt(this.mNattKeepaliveInterval);
    paramParcel.writeInt(this.mMarkValue);
    paramParcel.writeInt(this.mMarkMask);
    paramParcel.writeInt(this.mXfrmInterfaceId);
  }
  
  public IpSecConfig(IpSecConfig paramIpSecConfig) {
    this.mMode = paramIpSecConfig.mMode;
    this.mSourceAddress = paramIpSecConfig.mSourceAddress;
    this.mDestinationAddress = paramIpSecConfig.mDestinationAddress;
    this.mNetwork = paramIpSecConfig.mNetwork;
    this.mSpiResourceId = paramIpSecConfig.mSpiResourceId;
    this.mEncryption = paramIpSecConfig.mEncryption;
    this.mAuthentication = paramIpSecConfig.mAuthentication;
    this.mAuthenticatedEncryption = paramIpSecConfig.mAuthenticatedEncryption;
    this.mEncapType = paramIpSecConfig.mEncapType;
    this.mEncapSocketResourceId = paramIpSecConfig.mEncapSocketResourceId;
    this.mEncapRemotePort = paramIpSecConfig.mEncapRemotePort;
    this.mNattKeepaliveInterval = paramIpSecConfig.mNattKeepaliveInterval;
    this.mMarkValue = paramIpSecConfig.mMarkValue;
    this.mMarkMask = paramIpSecConfig.mMarkMask;
    this.mXfrmInterfaceId = paramIpSecConfig.mXfrmInterfaceId;
  }
  
  private IpSecConfig(Parcel paramParcel) {
    this.mMode = paramParcel.readInt();
    this.mSourceAddress = paramParcel.readString();
    this.mDestinationAddress = paramParcel.readString();
    this.mNetwork = paramParcel.<Network>readParcelable(Network.class.getClassLoader());
    this.mSpiResourceId = paramParcel.readInt();
    this.mEncryption = paramParcel.<IpSecAlgorithm>readParcelable(IpSecAlgorithm.class.getClassLoader());
    this.mAuthentication = paramParcel.<IpSecAlgorithm>readParcelable(IpSecAlgorithm.class.getClassLoader());
    this.mAuthenticatedEncryption = paramParcel.<IpSecAlgorithm>readParcelable(IpSecAlgorithm.class.getClassLoader());
    this.mEncapType = paramParcel.readInt();
    this.mEncapSocketResourceId = paramParcel.readInt();
    this.mEncapRemotePort = paramParcel.readInt();
    this.mNattKeepaliveInterval = paramParcel.readInt();
    this.mMarkValue = paramParcel.readInt();
    this.mMarkMask = paramParcel.readInt();
    this.mXfrmInterfaceId = paramParcel.readInt();
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{mMode=");
    if (this.mMode == 1) {
      str = "TUNNEL";
    } else {
      str = "TRANSPORT";
    } 
    stringBuilder.append(str);
    stringBuilder.append(", mSourceAddress=");
    String str = this.mSourceAddress;
    stringBuilder.append(str);
    stringBuilder.append(", mDestinationAddress=");
    str = this.mDestinationAddress;
    stringBuilder.append(str);
    stringBuilder.append(", mNetwork=");
    Network network = this.mNetwork;
    stringBuilder.append(network);
    stringBuilder.append(", mEncapType=");
    int i = this.mEncapType;
    stringBuilder.append(i);
    stringBuilder.append(", mEncapSocketResourceId=");
    i = this.mEncapSocketResourceId;
    stringBuilder.append(i);
    stringBuilder.append(", mEncapRemotePort=");
    i = this.mEncapRemotePort;
    stringBuilder.append(i);
    stringBuilder.append(", mNattKeepaliveInterval=");
    i = this.mNattKeepaliveInterval;
    stringBuilder.append(i);
    stringBuilder.append("{mSpiResourceId=");
    i = this.mSpiResourceId;
    stringBuilder.append(i);
    stringBuilder.append(", mEncryption=");
    IpSecAlgorithm ipSecAlgorithm = this.mEncryption;
    stringBuilder.append(ipSecAlgorithm);
    stringBuilder.append(", mAuthentication=");
    ipSecAlgorithm = this.mAuthentication;
    stringBuilder.append(ipSecAlgorithm);
    stringBuilder.append(", mAuthenticatedEncryption=");
    ipSecAlgorithm = this.mAuthenticatedEncryption;
    stringBuilder.append(ipSecAlgorithm);
    stringBuilder.append(", mMarkValue=");
    i = this.mMarkValue;
    stringBuilder.append(i);
    stringBuilder.append(", mMarkMask=");
    i = this.mMarkMask;
    stringBuilder.append(i);
    stringBuilder.append(", mXfrmInterfaceId=");
    i = this.mXfrmInterfaceId;
    stringBuilder.append(i);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<IpSecConfig> CREATOR = new Parcelable.Creator<IpSecConfig>() {
      public IpSecConfig createFromParcel(Parcel param1Parcel) {
        return new IpSecConfig(param1Parcel);
      }
      
      public IpSecConfig[] newArray(int param1Int) {
        return new IpSecConfig[param1Int];
      }
    };
  
  private static final String TAG = "IpSecConfig";
  
  private IpSecAlgorithm mAuthenticatedEncryption;
  
  private IpSecAlgorithm mAuthentication;
  
  private int mEncapRemotePort;
  
  private IpSecAlgorithm mEncryption;
  
  private int mMarkMask;
  
  private int mMarkValue;
  
  private int mNattKeepaliveInterval;
  
  private Network mNetwork;
  
  private int mXfrmInterfaceId;
  
  public boolean equals(Object paramObject) {
    // Byte code:
    //   0: aload_1
    //   1: instanceof android/net/IpSecConfig
    //   4: istore_2
    //   5: iconst_0
    //   6: istore_3
    //   7: iload_2
    //   8: ifne -> 13
    //   11: iconst_0
    //   12: ireturn
    //   13: aload_1
    //   14: checkcast android/net/IpSecConfig
    //   17: astore_1
    //   18: aload_0
    //   19: getfield mMode : I
    //   22: aload_1
    //   23: getfield mMode : I
    //   26: if_icmpne -> 270
    //   29: aload_0
    //   30: getfield mSourceAddress : Ljava/lang/String;
    //   33: astore #4
    //   35: aload_1
    //   36: getfield mSourceAddress : Ljava/lang/String;
    //   39: astore #5
    //   41: aload #4
    //   43: aload #5
    //   45: invokevirtual equals : (Ljava/lang/Object;)Z
    //   48: ifeq -> 270
    //   51: aload_0
    //   52: getfield mDestinationAddress : Ljava/lang/String;
    //   55: astore #5
    //   57: aload_1
    //   58: getfield mDestinationAddress : Ljava/lang/String;
    //   61: astore #4
    //   63: aload #5
    //   65: aload #4
    //   67: invokevirtual equals : (Ljava/lang/Object;)Z
    //   70: ifeq -> 270
    //   73: aload_0
    //   74: getfield mNetwork : Landroid/net/Network;
    //   77: astore #4
    //   79: aload #4
    //   81: ifnull -> 100
    //   84: aload_1
    //   85: getfield mNetwork : Landroid/net/Network;
    //   88: astore #5
    //   90: aload #4
    //   92: aload #5
    //   94: invokevirtual equals : (Ljava/lang/Object;)Z
    //   97: ifne -> 111
    //   100: aload_0
    //   101: getfield mNetwork : Landroid/net/Network;
    //   104: aload_1
    //   105: getfield mNetwork : Landroid/net/Network;
    //   108: if_acmpne -> 270
    //   111: aload_0
    //   112: getfield mEncapType : I
    //   115: aload_1
    //   116: getfield mEncapType : I
    //   119: if_icmpne -> 270
    //   122: aload_0
    //   123: getfield mEncapSocketResourceId : I
    //   126: aload_1
    //   127: getfield mEncapSocketResourceId : I
    //   130: if_icmpne -> 270
    //   133: aload_0
    //   134: getfield mEncapRemotePort : I
    //   137: aload_1
    //   138: getfield mEncapRemotePort : I
    //   141: if_icmpne -> 270
    //   144: aload_0
    //   145: getfield mNattKeepaliveInterval : I
    //   148: aload_1
    //   149: getfield mNattKeepaliveInterval : I
    //   152: if_icmpne -> 270
    //   155: aload_0
    //   156: getfield mSpiResourceId : I
    //   159: aload_1
    //   160: getfield mSpiResourceId : I
    //   163: if_icmpne -> 270
    //   166: aload_0
    //   167: getfield mEncryption : Landroid/net/IpSecAlgorithm;
    //   170: astore #4
    //   172: aload_1
    //   173: getfield mEncryption : Landroid/net/IpSecAlgorithm;
    //   176: astore #5
    //   178: aload #4
    //   180: aload #5
    //   182: invokestatic equals : (Landroid/net/IpSecAlgorithm;Landroid/net/IpSecAlgorithm;)Z
    //   185: ifeq -> 270
    //   188: aload_0
    //   189: getfield mAuthenticatedEncryption : Landroid/net/IpSecAlgorithm;
    //   192: astore #5
    //   194: aload_1
    //   195: getfield mAuthenticatedEncryption : Landroid/net/IpSecAlgorithm;
    //   198: astore #4
    //   200: aload #5
    //   202: aload #4
    //   204: invokestatic equals : (Landroid/net/IpSecAlgorithm;Landroid/net/IpSecAlgorithm;)Z
    //   207: ifeq -> 270
    //   210: aload_0
    //   211: getfield mAuthentication : Landroid/net/IpSecAlgorithm;
    //   214: astore #5
    //   216: aload_1
    //   217: getfield mAuthentication : Landroid/net/IpSecAlgorithm;
    //   220: astore #4
    //   222: aload #5
    //   224: aload #4
    //   226: invokestatic equals : (Landroid/net/IpSecAlgorithm;Landroid/net/IpSecAlgorithm;)Z
    //   229: ifeq -> 270
    //   232: aload_0
    //   233: getfield mMarkValue : I
    //   236: aload_1
    //   237: getfield mMarkValue : I
    //   240: if_icmpne -> 270
    //   243: aload_0
    //   244: getfield mMarkMask : I
    //   247: aload_1
    //   248: getfield mMarkMask : I
    //   251: if_icmpne -> 270
    //   254: aload_0
    //   255: getfield mXfrmInterfaceId : I
    //   258: aload_1
    //   259: getfield mXfrmInterfaceId : I
    //   262: if_icmpne -> 270
    //   265: iconst_1
    //   266: istore_3
    //   267: goto -> 270
    //   270: iload_3
    //   271: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #339	-> 0
    //   #340	-> 13
    //   #341	-> 18
    //   #342	-> 41
    //   #343	-> 63
    //   #344	-> 90
    //   #351	-> 178
    //   #352	-> 200
    //   #353	-> 222
    //   #341	-> 270
  }
  
  public IpSecConfig() {}
}
