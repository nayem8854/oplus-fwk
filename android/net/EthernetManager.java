package android.net;

import android.annotation.SystemApi;
import android.content.Context;
import android.os.Handler;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.Executor;

@SystemApi
public class EthernetManager {
  private static final int MSG_AVAILABILITY_CHANGED = 1000;
  
  private static final String TAG = "EthernetManager";
  
  private final Context mContext;
  
  private final Handler mHandler = (Handler)new Object(this, ConnectivityThread.getInstanceLooper());
  
  private final ArrayList<Listener> mListeners = new ArrayList<>();
  
  private final IEthernetManager mService;
  
  private final IEthernetServiceListener.Stub mServiceListener = (IEthernetServiceListener.Stub)new Object(this);
  
  public EthernetManager(Context paramContext, IEthernetManager paramIEthernetManager) {
    this.mContext = paramContext;
    this.mService = paramIEthernetManager;
  }
  
  public IpConfiguration getConfiguration(String paramString) {
    try {
      return this.mService.getConfiguration(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setConfiguration(String paramString, IpConfiguration paramIpConfiguration) {
    try {
      this.mService.setConfiguration(paramString, paramIpConfiguration);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isAvailable() {
    boolean bool;
    if ((getAvailableInterfaces()).length > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isAvailable(String paramString) {
    try {
      return this.mService.isAvailable(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void addListener(Listener paramListener) {
    if (paramListener != null) {
      this.mListeners.add(paramListener);
      if (this.mListeners.size() == 1)
        try {
          this.mService.addListener(this.mServiceListener);
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        }  
      return;
    } 
    throw new IllegalArgumentException("listener must not be null");
  }
  
  public String[] getAvailableInterfaces() {
    try {
      return this.mService.getAvailableInterfaces();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public void removeListener(Listener paramListener) {
    if (paramListener != null) {
      this.mListeners.remove(paramListener);
      if (this.mListeners.isEmpty())
        try {
          this.mService.removeListener(this.mServiceListener);
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        }  
      return;
    } 
    throw new IllegalArgumentException("listener must not be null");
  }
  
  public void setIncludeTestInterfaces(boolean paramBoolean) {
    try {
      this.mService.setIncludeTestInterfaces(paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  class Listener {
    public abstract void onAvailabilityChanged(String param1String, boolean param1Boolean);
  }
  
  class TetheredInterfaceCallback {
    public abstract void onAvailable(String param1String);
    
    public abstract void onUnavailable();
  }
  
  public static class TetheredInterfaceRequest {
    private final ITetheredInterfaceCallback mCb;
    
    private final IEthernetManager mService;
    
    private TetheredInterfaceRequest(IEthernetManager param1IEthernetManager, ITetheredInterfaceCallback param1ITetheredInterfaceCallback) {
      this.mService = param1IEthernetManager;
      this.mCb = param1ITetheredInterfaceCallback;
    }
    
    public void release() {
      try {
        this.mService.releaseTetheredInterface(this.mCb);
      } catch (RemoteException remoteException) {
        remoteException.rethrowFromSystemServer();
      } 
    }
  }
  
  public TetheredInterfaceRequest requestTetheredInterface(Executor paramExecutor, TetheredInterfaceCallback paramTetheredInterfaceCallback) {
    Objects.requireNonNull(paramTetheredInterfaceCallback, "Callback must be non-null");
    Objects.requireNonNull(paramExecutor, "Executor must be non-null");
    Object object = new Object(this, paramExecutor, paramTetheredInterfaceCallback);
    try {
      this.mService.requestTetheredInterface((ITetheredInterfaceCallback)object);
      return new TetheredInterfaceRequest(this.mService, (ITetheredInterfaceCallback)object);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
}
