package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.EnumMap;

@Deprecated
public class NetworkInfo implements Parcelable {
  @Deprecated
  class State extends Enum<State> {
    private static final State[] $VALUES;
    
    public static final State CONNECTED = new State("CONNECTED", 1);
    
    public static State[] values() {
      return (State[])$VALUES.clone();
    }
    
    public static State valueOf(String param1String) {
      return Enum.<State>valueOf(State.class, param1String);
    }
    
    private State(NetworkInfo this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static final State CONNECTING = new State("CONNECTING", 0);
    
    public static final State DISCONNECTED;
    
    public static final State DISCONNECTING;
    
    public static final State SUSPENDED = new State("SUSPENDED", 2);
    
    public static final State UNKNOWN;
    
    static {
      DISCONNECTING = new State("DISCONNECTING", 3);
      DISCONNECTED = new State("DISCONNECTED", 4);
      State state = new State("UNKNOWN", 5);
      $VALUES = new State[] { CONNECTING, CONNECTED, SUSPENDED, DISCONNECTING, DISCONNECTED, state };
    }
  }
  
  @Deprecated
  class DetailedState extends Enum<DetailedState> {
    private static final DetailedState[] $VALUES;
    
    public static final DetailedState AUTHENTICATING;
    
    public static final DetailedState BLOCKED;
    
    public static final DetailedState CAPTIVE_PORTAL_CHECK;
    
    public static final DetailedState CONNECTED;
    
    public static final DetailedState CONNECTING = new DetailedState("CONNECTING", 2);
    
    public static final DetailedState DISCONNECTED;
    
    public static final DetailedState DISCONNECTING;
    
    public static final DetailedState FAILED;
    
    public static DetailedState[] values() {
      return (DetailedState[])$VALUES.clone();
    }
    
    public static DetailedState valueOf(String param1String) {
      return Enum.<DetailedState>valueOf(DetailedState.class, param1String);
    }
    
    private DetailedState(NetworkInfo this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static final DetailedState IDLE = new DetailedState("IDLE", 0);
    
    public static final DetailedState OBTAINING_IPADDR;
    
    public static final DetailedState SCANNING = new DetailedState("SCANNING", 1);
    
    public static final DetailedState SUSPENDED;
    
    public static final DetailedState VERIFYING_POOR_LINK;
    
    static {
      AUTHENTICATING = new DetailedState("AUTHENTICATING", 3);
      OBTAINING_IPADDR = new DetailedState("OBTAINING_IPADDR", 4);
      CONNECTED = new DetailedState("CONNECTED", 5);
      SUSPENDED = new DetailedState("SUSPENDED", 6);
      DISCONNECTING = new DetailedState("DISCONNECTING", 7);
      DISCONNECTED = new DetailedState("DISCONNECTED", 8);
      FAILED = new DetailedState("FAILED", 9);
      BLOCKED = new DetailedState("BLOCKED", 10);
      VERIFYING_POOR_LINK = new DetailedState("VERIFYING_POOR_LINK", 11);
      DetailedState detailedState = new DetailedState("CAPTIVE_PORTAL_CHECK", 12);
      $VALUES = new DetailedState[] { 
          IDLE, SCANNING, CONNECTING, AUTHENTICATING, OBTAINING_IPADDR, CONNECTED, SUSPENDED, DISCONNECTING, DISCONNECTED, FAILED, 
          BLOCKED, VERIFYING_POOR_LINK, detailedState };
    }
  }
  
  static {
    EnumMap<DetailedState, Object> enumMap = new EnumMap<>(DetailedState.class);
    enumMap.put(DetailedState.IDLE, State.DISCONNECTED);
    stateMap.put(DetailedState.SCANNING, State.DISCONNECTED);
    stateMap.put(DetailedState.CONNECTING, State.CONNECTING);
    stateMap.put(DetailedState.AUTHENTICATING, State.CONNECTING);
    stateMap.put(DetailedState.OBTAINING_IPADDR, State.CONNECTING);
    stateMap.put(DetailedState.VERIFYING_POOR_LINK, State.CONNECTING);
    stateMap.put(DetailedState.CAPTIVE_PORTAL_CHECK, State.CONNECTED);
    stateMap.put(DetailedState.CONNECTED, State.CONNECTED);
    stateMap.put(DetailedState.SUSPENDED, State.SUSPENDED);
    stateMap.put(DetailedState.DISCONNECTING, State.DISCONNECTING);
    stateMap.put(DetailedState.DISCONNECTED, State.DISCONNECTED);
    stateMap.put(DetailedState.FAILED, State.DISCONNECTED);
    stateMap.put(DetailedState.BLOCKED, State.DISCONNECTED);
  }
  
  public NetworkInfo(int paramInt1, int paramInt2, String paramString1, String paramString2) {
    if (ConnectivityManager.isNetworkTypeValid(paramInt1) || paramInt1 == -1) {
      this.mNetworkType = paramInt1;
      this.mSubtype = paramInt2;
      this.mTypeName = paramString1;
      this.mSubtypeName = paramString2;
      setDetailedState(DetailedState.IDLE, null, null);
      this.mState = State.UNKNOWN;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid network type: ");
    stringBuilder.append(paramInt1);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public NetworkInfo(NetworkInfo paramNetworkInfo) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial <init> : ()V
    //   4: aload_1
    //   5: ifnull -> 108
    //   8: aload_1
    //   9: monitorenter
    //   10: aload_0
    //   11: aload_1
    //   12: getfield mNetworkType : I
    //   15: putfield mNetworkType : I
    //   18: aload_0
    //   19: aload_1
    //   20: getfield mSubtype : I
    //   23: putfield mSubtype : I
    //   26: aload_0
    //   27: aload_1
    //   28: getfield mTypeName : Ljava/lang/String;
    //   31: putfield mTypeName : Ljava/lang/String;
    //   34: aload_0
    //   35: aload_1
    //   36: getfield mSubtypeName : Ljava/lang/String;
    //   39: putfield mSubtypeName : Ljava/lang/String;
    //   42: aload_0
    //   43: aload_1
    //   44: getfield mState : Landroid/net/NetworkInfo$State;
    //   47: putfield mState : Landroid/net/NetworkInfo$State;
    //   50: aload_0
    //   51: aload_1
    //   52: getfield mDetailedState : Landroid/net/NetworkInfo$DetailedState;
    //   55: putfield mDetailedState : Landroid/net/NetworkInfo$DetailedState;
    //   58: aload_0
    //   59: aload_1
    //   60: getfield mReason : Ljava/lang/String;
    //   63: putfield mReason : Ljava/lang/String;
    //   66: aload_0
    //   67: aload_1
    //   68: getfield mExtraInfo : Ljava/lang/String;
    //   71: putfield mExtraInfo : Ljava/lang/String;
    //   74: aload_0
    //   75: aload_1
    //   76: getfield mIsFailover : Z
    //   79: putfield mIsFailover : Z
    //   82: aload_0
    //   83: aload_1
    //   84: getfield mIsAvailable : Z
    //   87: putfield mIsAvailable : Z
    //   90: aload_0
    //   91: aload_1
    //   92: getfield mIsRoaming : Z
    //   95: putfield mIsRoaming : Z
    //   98: aload_1
    //   99: monitorexit
    //   100: goto -> 108
    //   103: astore_2
    //   104: aload_1
    //   105: monitorexit
    //   106: aload_2
    //   107: athrow
    //   108: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #190	-> 0
    //   #191	-> 4
    //   #192	-> 8
    //   #193	-> 10
    //   #194	-> 18
    //   #195	-> 26
    //   #196	-> 34
    //   #197	-> 42
    //   #198	-> 50
    //   #199	-> 58
    //   #200	-> 66
    //   #201	-> 74
    //   #202	-> 82
    //   #203	-> 90
    //   #204	-> 98
    //   #206	-> 108
    // Exception table:
    //   from	to	target	type
    //   10	18	103	finally
    //   18	26	103	finally
    //   26	34	103	finally
    //   34	42	103	finally
    //   42	50	103	finally
    //   50	58	103	finally
    //   58	66	103	finally
    //   66	74	103	finally
    //   74	82	103	finally
    //   82	90	103	finally
    //   90	98	103	finally
    //   98	100	103	finally
    //   104	106	103	finally
  }
  
  @Deprecated
  public int getType() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mNetworkType : I
    //   6: istore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: iload_1
    //   10: ireturn
    //   11: astore_2
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_2
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #226	-> 0
    //   #227	-> 2
    //   #228	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	9	11	finally
    //   12	14	11	finally
  }
  
  @Deprecated
  public void setType(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iload_1
    //   4: putfield mNetworkType : I
    //   7: aload_0
    //   8: monitorexit
    //   9: return
    //   10: astore_2
    //   11: aload_0
    //   12: monitorexit
    //   13: aload_2
    //   14: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #237	-> 0
    //   #238	-> 2
    //   #239	-> 7
    //   #240	-> 9
    //   #239	-> 10
    // Exception table:
    //   from	to	target	type
    //   2	7	10	finally
    //   7	9	10	finally
    //   11	13	10	finally
  }
  
  @Deprecated
  public int getSubtype() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mSubtype : I
    //   6: istore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: iload_1
    //   10: ireturn
    //   11: astore_2
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_2
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #250	-> 0
    //   #251	-> 2
    //   #252	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	9	11	finally
    //   12	14	11	finally
  }
  
  public void setSubtype(int paramInt, String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iload_1
    //   4: putfield mSubtype : I
    //   7: aload_0
    //   8: aload_2
    //   9: putfield mSubtypeName : Ljava/lang/String;
    //   12: aload_0
    //   13: monitorexit
    //   14: return
    //   15: astore_2
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_2
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #260	-> 0
    //   #261	-> 2
    //   #262	-> 7
    //   #263	-> 12
    //   #264	-> 14
    //   #263	-> 15
    // Exception table:
    //   from	to	target	type
    //   2	7	15	finally
    //   7	12	15	finally
    //   12	14	15	finally
    //   16	18	15	finally
  }
  
  @Deprecated
  public String getTypeName() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mTypeName : Ljava/lang/String;
    //   6: astore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: aload_1
    //   10: areturn
    //   11: astore_1
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_1
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #281	-> 0
    //   #282	-> 2
    //   #283	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	9	11	finally
    //   12	14	11	finally
  }
  
  @Deprecated
  public String getSubtypeName() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mSubtypeName : Ljava/lang/String;
    //   6: astore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: aload_1
    //   10: areturn
    //   11: astore_1
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_1
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #293	-> 0
    //   #294	-> 2
    //   #295	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	9	11	finally
    //   12	14	11	finally
  }
  
  @Deprecated
  public boolean isConnectedOrConnecting() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mState : Landroid/net/NetworkInfo$State;
    //   6: getstatic android/net/NetworkInfo$State.CONNECTED : Landroid/net/NetworkInfo$State;
    //   9: if_acmpeq -> 30
    //   12: aload_0
    //   13: getfield mState : Landroid/net/NetworkInfo$State;
    //   16: getstatic android/net/NetworkInfo$State.CONNECTING : Landroid/net/NetworkInfo$State;
    //   19: if_acmpne -> 25
    //   22: goto -> 30
    //   25: iconst_0
    //   26: istore_1
    //   27: goto -> 32
    //   30: iconst_1
    //   31: istore_1
    //   32: aload_0
    //   33: monitorexit
    //   34: iload_1
    //   35: ireturn
    //   36: astore_2
    //   37: aload_0
    //   38: monitorexit
    //   39: aload_2
    //   40: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #316	-> 0
    //   #317	-> 2
    //   #318	-> 36
    // Exception table:
    //   from	to	target	type
    //   2	22	36	finally
    //   32	34	36	finally
    //   37	39	36	finally
  }
  
  @Deprecated
  public boolean isConnected() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mState : Landroid/net/NetworkInfo$State;
    //   6: getstatic android/net/NetworkInfo$State.CONNECTED : Landroid/net/NetworkInfo$State;
    //   9: if_acmpne -> 17
    //   12: iconst_1
    //   13: istore_1
    //   14: goto -> 19
    //   17: iconst_0
    //   18: istore_1
    //   19: aload_0
    //   20: monitorexit
    //   21: iload_1
    //   22: ireturn
    //   23: astore_2
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_2
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #336	-> 0
    //   #337	-> 2
    //   #338	-> 23
    // Exception table:
    //   from	to	target	type
    //   2	12	23	finally
    //   19	21	23	finally
    //   24	26	23	finally
  }
  
  @Deprecated
  public boolean isAvailable() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIsAvailable : Z
    //   6: istore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: iload_1
    //   10: ireturn
    //   11: astore_2
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_2
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #364	-> 0
    //   #365	-> 2
    //   #366	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	9	11	finally
    //   12	14	11	finally
  }
  
  @Deprecated
  public void setIsAvailable(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iload_1
    //   4: putfield mIsAvailable : Z
    //   7: aload_0
    //   8: monitorexit
    //   9: return
    //   10: astore_2
    //   11: aload_0
    //   12: monitorexit
    //   13: aload_2
    //   14: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #379	-> 0
    //   #380	-> 2
    //   #381	-> 7
    //   #382	-> 9
    //   #381	-> 10
    // Exception table:
    //   from	to	target	type
    //   2	7	10	finally
    //   7	9	10	finally
    //   11	13	10	finally
  }
  
  @Deprecated
  public boolean isFailover() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIsFailover : Z
    //   6: istore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: iload_1
    //   10: ireturn
    //   11: astore_2
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_2
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #395	-> 0
    //   #396	-> 2
    //   #397	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	9	11	finally
    //   12	14	11	finally
  }
  
  @Deprecated
  public void setFailover(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iload_1
    //   4: putfield mIsFailover : Z
    //   7: aload_0
    //   8: monitorexit
    //   9: return
    //   10: astore_2
    //   11: aload_0
    //   12: monitorexit
    //   13: aload_2
    //   14: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #410	-> 0
    //   #411	-> 2
    //   #412	-> 7
    //   #413	-> 9
    //   #412	-> 10
    // Exception table:
    //   from	to	target	type
    //   2	7	10	finally
    //   7	9	10	finally
    //   11	13	10	finally
  }
  
  @Deprecated
  public boolean isRoaming() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIsRoaming : Z
    //   6: istore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: iload_1
    //   10: ireturn
    //   11: astore_2
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_2
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #428	-> 0
    //   #429	-> 2
    //   #430	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	9	11	finally
    //   12	14	11	finally
  }
  
  @Deprecated
  public void setRoaming(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iload_1
    //   4: putfield mIsRoaming : Z
    //   7: aload_0
    //   8: monitorexit
    //   9: return
    //   10: astore_2
    //   11: aload_0
    //   12: monitorexit
    //   13: aload_2
    //   14: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #441	-> 0
    //   #442	-> 2
    //   #443	-> 7
    //   #444	-> 9
    //   #443	-> 10
    // Exception table:
    //   from	to	target	type
    //   2	7	10	finally
    //   7	9	10	finally
    //   11	13	10	finally
  }
  
  @Deprecated
  public State getState() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mState : Landroid/net/NetworkInfo$State;
    //   6: astore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: aload_1
    //   10: areturn
    //   11: astore_1
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_1
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #459	-> 0
    //   #460	-> 2
    //   #461	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	9	11	finally
    //   12	14	11	finally
  }
  
  @Deprecated
  public DetailedState getDetailedState() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mDetailedState : Landroid/net/NetworkInfo$DetailedState;
    //   6: astore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: aload_1
    //   10: areturn
    //   11: astore_1
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_1
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #477	-> 0
    //   #478	-> 2
    //   #479	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	9	11	finally
    //   12	14	11	finally
  }
  
  @Deprecated
  public void setDetailedState(DetailedState paramDetailedState, String paramString1, String paramString2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: putfield mDetailedState : Landroid/net/NetworkInfo$DetailedState;
    //   7: aload_0
    //   8: getstatic android/net/NetworkInfo.stateMap : Ljava/util/EnumMap;
    //   11: aload_1
    //   12: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   15: checkcast android/net/NetworkInfo$State
    //   18: putfield mState : Landroid/net/NetworkInfo$State;
    //   21: aload_0
    //   22: aload_2
    //   23: putfield mReason : Ljava/lang/String;
    //   26: aload_0
    //   27: aload_3
    //   28: putfield mExtraInfo : Ljava/lang/String;
    //   31: aload_0
    //   32: monitorexit
    //   33: return
    //   34: astore_1
    //   35: aload_0
    //   36: monitorexit
    //   37: aload_1
    //   38: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #497	-> 0
    //   #498	-> 2
    //   #499	-> 7
    //   #500	-> 21
    //   #501	-> 26
    //   #502	-> 31
    //   #503	-> 33
    //   #502	-> 34
    // Exception table:
    //   from	to	target	type
    //   2	7	34	finally
    //   7	21	34	finally
    //   21	26	34	finally
    //   26	31	34	finally
    //   31	33	34	finally
    //   35	37	34	finally
  }
  
  @Deprecated
  public void setExtraInfo(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: putfield mExtraInfo : Ljava/lang/String;
    //   7: aload_0
    //   8: monitorexit
    //   9: return
    //   10: astore_1
    //   11: aload_0
    //   12: monitorexit
    //   13: aload_1
    //   14: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #514	-> 0
    //   #515	-> 2
    //   #516	-> 7
    //   #517	-> 9
    //   #516	-> 10
    // Exception table:
    //   from	to	target	type
    //   2	7	10	finally
    //   7	9	10	finally
    //   11	13	10	finally
  }
  
  public String getReason() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mReason : Ljava/lang/String;
    //   6: astore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: aload_1
    //   10: areturn
    //   11: astore_1
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_1
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #527	-> 0
    //   #528	-> 2
    //   #529	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	9	11	finally
    //   12	14	11	finally
  }
  
  @Deprecated
  public String getExtraInfo() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mExtraInfo : Ljava/lang/String;
    //   6: astore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: aload_1
    //   10: areturn
    //   11: astore_1
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_1
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #541	-> 0
    //   #542	-> 2
    //   #543	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	9	11	finally
    //   12	14	11	finally
  }
  
  public String toString() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new java/lang/StringBuilder
    //   5: astore_1
    //   6: aload_1
    //   7: ldc '['
    //   9: invokespecial <init> : (Ljava/lang/String;)V
    //   12: aload_1
    //   13: ldc 'type: '
    //   15: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   18: pop
    //   19: aload_1
    //   20: aload_0
    //   21: invokevirtual getTypeName : ()Ljava/lang/String;
    //   24: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   27: pop
    //   28: aload_1
    //   29: ldc '['
    //   31: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   34: pop
    //   35: aload_1
    //   36: aload_0
    //   37: invokevirtual getSubtypeName : ()Ljava/lang/String;
    //   40: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   43: pop
    //   44: aload_1
    //   45: ldc '], state: '
    //   47: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   50: pop
    //   51: aload_1
    //   52: aload_0
    //   53: getfield mState : Landroid/net/NetworkInfo$State;
    //   56: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   59: pop
    //   60: aload_1
    //   61: ldc '/'
    //   63: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   66: pop
    //   67: aload_1
    //   68: aload_0
    //   69: getfield mDetailedState : Landroid/net/NetworkInfo$DetailedState;
    //   72: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   75: pop
    //   76: aload_1
    //   77: ldc ', reason: '
    //   79: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   82: pop
    //   83: aload_0
    //   84: getfield mReason : Ljava/lang/String;
    //   87: ifnonnull -> 96
    //   90: ldc '(unspecified)'
    //   92: astore_2
    //   93: goto -> 101
    //   96: aload_0
    //   97: getfield mReason : Ljava/lang/String;
    //   100: astore_2
    //   101: aload_1
    //   102: aload_2
    //   103: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   106: pop
    //   107: aload_1
    //   108: ldc ', extra: '
    //   110: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   113: pop
    //   114: aload_0
    //   115: getfield mExtraInfo : Ljava/lang/String;
    //   118: ifnonnull -> 127
    //   121: ldc '(none)'
    //   123: astore_2
    //   124: goto -> 132
    //   127: aload_0
    //   128: getfield mExtraInfo : Ljava/lang/String;
    //   131: astore_2
    //   132: aload_1
    //   133: aload_2
    //   134: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   137: pop
    //   138: aload_1
    //   139: ldc ', failover: '
    //   141: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   144: pop
    //   145: aload_1
    //   146: aload_0
    //   147: getfield mIsFailover : Z
    //   150: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   153: pop
    //   154: aload_1
    //   155: ldc ', available: '
    //   157: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   160: pop
    //   161: aload_1
    //   162: aload_0
    //   163: getfield mIsAvailable : Z
    //   166: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   169: pop
    //   170: aload_1
    //   171: ldc ', roaming: '
    //   173: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   176: pop
    //   177: aload_1
    //   178: aload_0
    //   179: getfield mIsRoaming : Z
    //   182: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   185: pop
    //   186: aload_1
    //   187: ldc ']'
    //   189: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   192: pop
    //   193: aload_1
    //   194: invokevirtual toString : ()Ljava/lang/String;
    //   197: astore_2
    //   198: aload_0
    //   199: monitorexit
    //   200: aload_2
    //   201: areturn
    //   202: astore_2
    //   203: aload_0
    //   204: monitorexit
    //   205: aload_2
    //   206: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #548	-> 0
    //   #549	-> 2
    //   #550	-> 12
    //   #551	-> 44
    //   #552	-> 76
    //   #553	-> 107
    //   #554	-> 138
    //   #555	-> 154
    //   #556	-> 170
    //   #557	-> 186
    //   #558	-> 193
    //   #559	-> 202
    // Exception table:
    //   from	to	target	type
    //   2	12	202	finally
    //   12	44	202	finally
    //   44	76	202	finally
    //   76	90	202	finally
    //   96	101	202	finally
    //   101	107	202	finally
    //   107	121	202	finally
    //   127	132	202	finally
    //   132	138	202	finally
    //   138	154	202	finally
    //   154	170	202	finally
    //   170	186	202	finally
    //   186	193	202	finally
    //   193	200	202	finally
    //   203	205	202	finally
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: aload_0
    //   4: getfield mNetworkType : I
    //   7: invokevirtual writeInt : (I)V
    //   10: aload_1
    //   11: aload_0
    //   12: getfield mSubtype : I
    //   15: invokevirtual writeInt : (I)V
    //   18: aload_1
    //   19: aload_0
    //   20: getfield mTypeName : Ljava/lang/String;
    //   23: invokevirtual writeString : (Ljava/lang/String;)V
    //   26: aload_1
    //   27: aload_0
    //   28: getfield mSubtypeName : Ljava/lang/String;
    //   31: invokevirtual writeString : (Ljava/lang/String;)V
    //   34: aload_1
    //   35: aload_0
    //   36: getfield mState : Landroid/net/NetworkInfo$State;
    //   39: invokevirtual name : ()Ljava/lang/String;
    //   42: invokevirtual writeString : (Ljava/lang/String;)V
    //   45: aload_1
    //   46: aload_0
    //   47: getfield mDetailedState : Landroid/net/NetworkInfo$DetailedState;
    //   50: invokevirtual name : ()Ljava/lang/String;
    //   53: invokevirtual writeString : (Ljava/lang/String;)V
    //   56: aload_0
    //   57: getfield mIsFailover : Z
    //   60: istore_3
    //   61: iconst_1
    //   62: istore #4
    //   64: iload_3
    //   65: ifeq -> 73
    //   68: iconst_1
    //   69: istore_2
    //   70: goto -> 75
    //   73: iconst_0
    //   74: istore_2
    //   75: aload_1
    //   76: iload_2
    //   77: invokevirtual writeInt : (I)V
    //   80: aload_0
    //   81: getfield mIsAvailable : Z
    //   84: ifeq -> 92
    //   87: iconst_1
    //   88: istore_2
    //   89: goto -> 94
    //   92: iconst_0
    //   93: istore_2
    //   94: aload_1
    //   95: iload_2
    //   96: invokevirtual writeInt : (I)V
    //   99: aload_0
    //   100: getfield mIsRoaming : Z
    //   103: ifeq -> 112
    //   106: iload #4
    //   108: istore_2
    //   109: goto -> 114
    //   112: iconst_0
    //   113: istore_2
    //   114: aload_1
    //   115: iload_2
    //   116: invokevirtual writeInt : (I)V
    //   119: aload_1
    //   120: aload_0
    //   121: getfield mReason : Ljava/lang/String;
    //   124: invokevirtual writeString : (Ljava/lang/String;)V
    //   127: aload_1
    //   128: aload_0
    //   129: getfield mExtraInfo : Ljava/lang/String;
    //   132: invokevirtual writeString : (Ljava/lang/String;)V
    //   135: aload_0
    //   136: monitorexit
    //   137: return
    //   138: astore_1
    //   139: aload_0
    //   140: monitorexit
    //   141: aload_1
    //   142: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #569	-> 0
    //   #570	-> 2
    //   #571	-> 10
    //   #572	-> 18
    //   #573	-> 26
    //   #574	-> 34
    //   #575	-> 45
    //   #576	-> 56
    //   #577	-> 80
    //   #578	-> 99
    //   #579	-> 119
    //   #580	-> 127
    //   #581	-> 135
    //   #582	-> 137
    //   #581	-> 138
    // Exception table:
    //   from	to	target	type
    //   2	10	138	finally
    //   10	18	138	finally
    //   18	26	138	finally
    //   26	34	138	finally
    //   34	45	138	finally
    //   45	56	138	finally
    //   56	61	138	finally
    //   75	80	138	finally
    //   80	87	138	finally
    //   94	99	138	finally
    //   99	106	138	finally
    //   114	119	138	finally
    //   119	127	138	finally
    //   127	135	138	finally
    //   135	137	138	finally
    //   139	141	138	finally
  }
  
  public static final Parcelable.Creator<NetworkInfo> CREATOR = new Parcelable.Creator<NetworkInfo>() {
      public NetworkInfo createFromParcel(Parcel param1Parcel) {
        boolean bool2;
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        String str1 = param1Parcel.readString();
        String str2 = param1Parcel.readString();
        NetworkInfo networkInfo = new NetworkInfo(i, j, str1, str2);
        NetworkInfo.access$002(networkInfo, NetworkInfo.State.valueOf(param1Parcel.readString()));
        NetworkInfo.access$102(networkInfo, NetworkInfo.DetailedState.valueOf(param1Parcel.readString()));
        j = param1Parcel.readInt();
        boolean bool1 = true;
        if (j != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        NetworkInfo.access$202(networkInfo, bool2);
        if (param1Parcel.readInt() != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        NetworkInfo.access$302(networkInfo, bool2);
        if (param1Parcel.readInt() != 0) {
          bool2 = bool1;
        } else {
          bool2 = false;
        } 
        NetworkInfo.access$402(networkInfo, bool2);
        NetworkInfo.access$502(networkInfo, param1Parcel.readString());
        NetworkInfo.access$602(networkInfo, param1Parcel.readString());
        return networkInfo;
      }
      
      public NetworkInfo[] newArray(int param1Int) {
        return new NetworkInfo[param1Int];
      }
    };
  
  private static final EnumMap<DetailedState, State> stateMap;
  
  private DetailedState mDetailedState;
  
  private String mExtraInfo;
  
  private boolean mIsAvailable;
  
  private boolean mIsFailover;
  
  private boolean mIsRoaming;
  
  private int mNetworkType;
  
  private String mReason;
  
  private State mState;
  
  private int mSubtype;
  
  private String mSubtypeName;
  
  private String mTypeName;
}
