package android.net;

import android.content.Context;
import android.os.Binder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.os.RemoteException;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

public class ConnectivityDiagnosticsManager {
  public static final Map<ConnectivityDiagnosticsCallback, ConnectivityDiagnosticsBinder> sCallbacks = new ConcurrentHashMap<>();
  
  private final Context mContext;
  
  private final IConnectivityManager mService;
  
  public ConnectivityDiagnosticsManager(Context paramContext, IConnectivityManager paramIConnectivityManager) {
    this.mContext = (Context)Preconditions.checkNotNull(paramContext, "missing context");
    this.mService = (IConnectivityManager)Preconditions.checkNotNull(paramIConnectivityManager, "missing IConnectivityManager");
  }
  
  public static boolean persistableBundleEquals(PersistableBundle paramPersistableBundle1, PersistableBundle paramPersistableBundle2) {
    if (paramPersistableBundle1 == paramPersistableBundle2)
      return true; 
    if (paramPersistableBundle1 == null || paramPersistableBundle2 == null)
      return false; 
    if (!Objects.equals(paramPersistableBundle1.keySet(), paramPersistableBundle2.keySet()))
      return false; 
    for (String str : paramPersistableBundle1.keySet()) {
      if (!Objects.equals(paramPersistableBundle1.get(str), paramPersistableBundle2.get(str)))
        return false; 
    } 
    return true;
  }
  
  class ConnectivityReport implements Parcelable {
    public ConnectivityReport(ConnectivityDiagnosticsManager this$0, long param1Long, LinkProperties param1LinkProperties, NetworkCapabilities param1NetworkCapabilities, PersistableBundle param1PersistableBundle) {
      this.mNetwork = (Network)this$0;
      this.mReportTimestamp = param1Long;
      this.mLinkProperties = new LinkProperties(param1LinkProperties);
      this.mNetworkCapabilities = new NetworkCapabilities(param1NetworkCapabilities);
      this.mAdditionalInfo = param1PersistableBundle;
    }
    
    public Network getNetwork() {
      return this.mNetwork;
    }
    
    public long getReportTimestamp() {
      return this.mReportTimestamp;
    }
    
    public LinkProperties getLinkProperties() {
      return new LinkProperties(this.mLinkProperties);
    }
    
    public NetworkCapabilities getNetworkCapabilities() {
      return new NetworkCapabilities(this.mNetworkCapabilities);
    }
    
    public PersistableBundle getAdditionalInfo() {
      return new PersistableBundle(this.mAdditionalInfo);
    }
    
    public boolean equals(Object param1Object) {
      null = true;
      if (this == param1Object)
        return true; 
      if (!(param1Object instanceof ConnectivityReport))
        return false; 
      param1Object = param1Object;
      if (this.mReportTimestamp == ((ConnectivityReport)param1Object).mReportTimestamp) {
        Network network1 = this.mNetwork, network2 = ((ConnectivityReport)param1Object).mNetwork;
        if (network1.equals(network2)) {
          LinkProperties linkProperties2 = this.mLinkProperties, linkProperties1 = ((ConnectivityReport)param1Object).mLinkProperties;
          if (linkProperties2.equals(linkProperties1)) {
            NetworkCapabilities networkCapabilities1 = this.mNetworkCapabilities, networkCapabilities2 = ((ConnectivityReport)param1Object).mNetworkCapabilities;
            if (networkCapabilities1.equals(networkCapabilities2)) {
              PersistableBundle persistableBundle = this.mAdditionalInfo;
              param1Object = ((ConnectivityReport)param1Object).mAdditionalInfo;
              if (ConnectivityDiagnosticsManager.persistableBundleEquals(persistableBundle, (PersistableBundle)param1Object))
                return null; 
            } 
          } 
        } 
      } 
      return false;
    }
    
    public int hashCode() {
      Network network = this.mNetwork;
      long l = this.mReportTimestamp;
      LinkProperties linkProperties = this.mLinkProperties;
      NetworkCapabilities networkCapabilities = this.mNetworkCapabilities;
      PersistableBundle persistableBundle = this.mAdditionalInfo;
      return Objects.hash(new Object[] { network, Long.valueOf(l), linkProperties, networkCapabilities, persistableBundle });
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeParcelable(this.mNetwork, param1Int);
      param1Parcel.writeLong(this.mReportTimestamp);
      param1Parcel.writeParcelable(this.mLinkProperties, param1Int);
      param1Parcel.writeParcelable(this.mNetworkCapabilities, param1Int);
      param1Parcel.writeParcelable(this.mAdditionalInfo, param1Int);
    }
    
    public static final Parcelable.Creator<ConnectivityReport> CREATOR = new Parcelable.Creator<ConnectivityReport>() {
        public ConnectivityDiagnosticsManager.ConnectivityReport createFromParcel(Parcel param2Parcel) {
          Network network = param2Parcel.<Network>readParcelable(null);
          long l = param2Parcel.readLong();
          LinkProperties linkProperties = param2Parcel.<LinkProperties>readParcelable(null);
          NetworkCapabilities networkCapabilities = param2Parcel.<NetworkCapabilities>readParcelable(null);
          return new ConnectivityDiagnosticsManager.ConnectivityReport(network, l, linkProperties, networkCapabilities, param2Parcel.<PersistableBundle>readParcelable(null));
        }
        
        public ConnectivityDiagnosticsManager.ConnectivityReport[] newArray(int param2Int) {
          return new ConnectivityDiagnosticsManager.ConnectivityReport[param2Int];
        }
      };
    
    public static final String KEY_NETWORK_PROBES_ATTEMPTED_BITMASK = "networkProbesAttempted";
    
    public static final String KEY_NETWORK_PROBES_SUCCEEDED_BITMASK = "networkProbesSucceeded";
    
    public static final String KEY_NETWORK_VALIDATION_RESULT = "networkValidationResult";
    
    public static final int NETWORK_PROBE_DNS = 4;
    
    public static final int NETWORK_PROBE_FALLBACK = 32;
    
    public static final int NETWORK_PROBE_HTTP = 8;
    
    public static final int NETWORK_PROBE_HTTPS = 16;
    
    public static final int NETWORK_PROBE_PRIVATE_DNS = 64;
    
    public static final int NETWORK_VALIDATION_RESULT_INVALID = 0;
    
    public static final int NETWORK_VALIDATION_RESULT_PARTIALLY_VALID = 2;
    
    public static final int NETWORK_VALIDATION_RESULT_SKIPPED = 3;
    
    public static final int NETWORK_VALIDATION_RESULT_VALID = 1;
    
    private final PersistableBundle mAdditionalInfo;
    
    private final LinkProperties mLinkProperties;
    
    private final Network mNetwork;
    
    private final NetworkCapabilities mNetworkCapabilities;
    
    private final long mReportTimestamp;
    
    @Retention(RetentionPolicy.SOURCE)
    class ConnectivityReportBundleKeys implements Annotation {}
    
    @Retention(RetentionPolicy.SOURCE)
    class NetworkProbe implements Annotation {}
    
    @Retention(RetentionPolicy.SOURCE)
    class NetworkValidationResult implements Annotation {}
  }
  
  class DataStallReport implements Parcelable {
    public DataStallReport(ConnectivityDiagnosticsManager this$0, long param1Long, int param1Int, LinkProperties param1LinkProperties, NetworkCapabilities param1NetworkCapabilities, PersistableBundle param1PersistableBundle) {
      this.mNetwork = (Network)this$0;
      this.mReportTimestamp = param1Long;
      this.mDetectionMethod = param1Int;
      this.mLinkProperties = new LinkProperties(param1LinkProperties);
      this.mNetworkCapabilities = new NetworkCapabilities(param1NetworkCapabilities);
      this.mStallDetails = param1PersistableBundle;
    }
    
    public Network getNetwork() {
      return this.mNetwork;
    }
    
    public long getReportTimestamp() {
      return this.mReportTimestamp;
    }
    
    public int getDetectionMethod() {
      return this.mDetectionMethod;
    }
    
    public LinkProperties getLinkProperties() {
      return new LinkProperties(this.mLinkProperties);
    }
    
    public NetworkCapabilities getNetworkCapabilities() {
      return new NetworkCapabilities(this.mNetworkCapabilities);
    }
    
    public PersistableBundle getStallDetails() {
      return new PersistableBundle(this.mStallDetails);
    }
    
    public boolean equals(Object param1Object) {
      null = true;
      if (this == param1Object)
        return true; 
      if (!(param1Object instanceof DataStallReport))
        return false; 
      param1Object = param1Object;
      if (this.mReportTimestamp == ((DataStallReport)param1Object).mReportTimestamp && this.mDetectionMethod == ((DataStallReport)param1Object).mDetectionMethod) {
        Network network1 = this.mNetwork, network2 = ((DataStallReport)param1Object).mNetwork;
        if (network1.equals(network2)) {
          LinkProperties linkProperties1 = this.mLinkProperties, linkProperties2 = ((DataStallReport)param1Object).mLinkProperties;
          if (linkProperties1.equals(linkProperties2)) {
            NetworkCapabilities networkCapabilities1 = this.mNetworkCapabilities, networkCapabilities2 = ((DataStallReport)param1Object).mNetworkCapabilities;
            if (networkCapabilities1.equals(networkCapabilities2)) {
              PersistableBundle persistableBundle = this.mStallDetails;
              param1Object = ((DataStallReport)param1Object).mStallDetails;
              if (ConnectivityDiagnosticsManager.persistableBundleEquals(persistableBundle, (PersistableBundle)param1Object))
                return null; 
            } 
          } 
        } 
      } 
      return false;
    }
    
    public int hashCode() {
      Network network = this.mNetwork;
      long l = this.mReportTimestamp;
      int i = this.mDetectionMethod;
      LinkProperties linkProperties = this.mLinkProperties;
      NetworkCapabilities networkCapabilities = this.mNetworkCapabilities;
      PersistableBundle persistableBundle = this.mStallDetails;
      return Objects.hash(new Object[] { network, Long.valueOf(l), Integer.valueOf(i), linkProperties, networkCapabilities, persistableBundle });
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeParcelable(this.mNetwork, param1Int);
      param1Parcel.writeLong(this.mReportTimestamp);
      param1Parcel.writeInt(this.mDetectionMethod);
      param1Parcel.writeParcelable(this.mLinkProperties, param1Int);
      param1Parcel.writeParcelable(this.mNetworkCapabilities, param1Int);
      param1Parcel.writeParcelable(this.mStallDetails, param1Int);
    }
    
    public static final Parcelable.Creator<DataStallReport> CREATOR = new Parcelable.Creator<DataStallReport>() {
        public ConnectivityDiagnosticsManager.DataStallReport createFromParcel(Parcel param2Parcel) {
          Network network = param2Parcel.<Network>readParcelable(null);
          long l = param2Parcel.readLong();
          int i = param2Parcel.readInt();
          LinkProperties linkProperties = param2Parcel.<LinkProperties>readParcelable(null);
          NetworkCapabilities networkCapabilities = param2Parcel.<NetworkCapabilities>readParcelable(null);
          return new ConnectivityDiagnosticsManager.DataStallReport(network, l, i, linkProperties, networkCapabilities, param2Parcel.<PersistableBundle>readParcelable(null));
        }
        
        public ConnectivityDiagnosticsManager.DataStallReport[] newArray(int param2Int) {
          return new ConnectivityDiagnosticsManager.DataStallReport[param2Int];
        }
      };
    
    public static final int DETECTION_METHOD_DNS_EVENTS = 1;
    
    public static final int DETECTION_METHOD_TCP_METRICS = 2;
    
    public static final String KEY_DNS_CONSECUTIVE_TIMEOUTS = "dnsConsecutiveTimeouts";
    
    public static final String KEY_TCP_METRICS_COLLECTION_PERIOD_MILLIS = "tcpMetricsCollectionPeriodMillis";
    
    public static final String KEY_TCP_PACKET_FAIL_RATE = "tcpPacketFailRate";
    
    private final int mDetectionMethod;
    
    private final LinkProperties mLinkProperties;
    
    private final Network mNetwork;
    
    private final NetworkCapabilities mNetworkCapabilities;
    
    private long mReportTimestamp;
    
    private final PersistableBundle mStallDetails;
    
    @Retention(RetentionPolicy.SOURCE)
    class DataStallReportBundleKeys implements Annotation {}
    
    @Retention(RetentionPolicy.SOURCE)
    class DetectionMethod implements Annotation {}
  }
  
  class ConnectivityDiagnosticsBinder extends IConnectivityDiagnosticsCallback.Stub {
    private final ConnectivityDiagnosticsManager.ConnectivityDiagnosticsCallback mCb;
    
    private final Executor mExecutor;
    
    public ConnectivityDiagnosticsBinder(ConnectivityDiagnosticsManager this$0, Executor param1Executor) {
      this.mCb = (ConnectivityDiagnosticsManager.ConnectivityDiagnosticsCallback)this$0;
      this.mExecutor = param1Executor;
    }
    
    public void onConnectivityReportAvailable(ConnectivityDiagnosticsManager.ConnectivityReport param1ConnectivityReport) {
      Binder.withCleanCallingIdentity(new _$$Lambda$ConnectivityDiagnosticsManager$ConnectivityDiagnosticsBinder$FGvmuK56MYkuwwlBPZ9M93t2UqY(this, param1ConnectivityReport));
    }
    
    public void onDataStallSuspected(ConnectivityDiagnosticsManager.DataStallReport param1DataStallReport) {
      Binder.withCleanCallingIdentity(new _$$Lambda$ConnectivityDiagnosticsManager$ConnectivityDiagnosticsBinder$oQ8vg4N13Yt69vO4YyGcJmSjA_U(this, param1DataStallReport));
    }
    
    public void onNetworkConnectivityReported(Network param1Network, boolean param1Boolean) {
      Binder.withCleanCallingIdentity(new _$$Lambda$ConnectivityDiagnosticsManager$ConnectivityDiagnosticsBinder$_i7Xc4PglBiVdW9VXLaiY3i_ufk(this, param1Network, param1Boolean));
    }
  }
  
  public static abstract class ConnectivityDiagnosticsCallback {
    public void onConnectivityReportAvailable(ConnectivityDiagnosticsManager.ConnectivityReport param1ConnectivityReport) {}
    
    public void onDataStallSuspected(ConnectivityDiagnosticsManager.DataStallReport param1DataStallReport) {}
    
    public void onNetworkConnectivityReported(Network param1Network, boolean param1Boolean) {}
  }
  
  public void registerConnectivityDiagnosticsCallback(NetworkRequest paramNetworkRequest, Executor paramExecutor, ConnectivityDiagnosticsCallback paramConnectivityDiagnosticsCallback) {
    ConnectivityDiagnosticsBinder connectivityDiagnosticsBinder = new ConnectivityDiagnosticsBinder(paramConnectivityDiagnosticsCallback, paramExecutor);
    if (sCallbacks.putIfAbsent(paramConnectivityDiagnosticsCallback, connectivityDiagnosticsBinder) == null) {
      try {
        IConnectivityManager iConnectivityManager = this.mService;
        Context context = this.mContext;
        String str = context.getOpPackageName();
        iConnectivityManager.registerConnectivityDiagnosticsCallback(connectivityDiagnosticsBinder, paramNetworkRequest, str);
      } catch (RemoteException remoteException) {
        remoteException.rethrowFromSystemServer();
      } 
      return;
    } 
    throw new IllegalArgumentException("Callback is currently registered");
  }
  
  public void unregisterConnectivityDiagnosticsCallback(ConnectivityDiagnosticsCallback paramConnectivityDiagnosticsCallback) {
    ConnectivityDiagnosticsBinder connectivityDiagnosticsBinder = sCallbacks.remove(paramConnectivityDiagnosticsCallback);
    if (connectivityDiagnosticsBinder == null)
      return; 
    try {
      this.mService.unregisterConnectivityDiagnosticsCallback(connectivityDiagnosticsBinder);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ConnectivityReportBundleKeys {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface NetworkProbe {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface NetworkValidationResult {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface DataStallReportBundleKeys {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface DetectionMethod {}
}
