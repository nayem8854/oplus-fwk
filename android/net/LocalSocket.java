package android.net;

import java.io.Closeable;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class LocalSocket implements Closeable {
  public static final int SOCKET_DGRAM = 1;
  
  public static final int SOCKET_SEQPACKET = 3;
  
  public static final int SOCKET_STREAM = 2;
  
  static final int SOCKET_UNKNOWN = 0;
  
  private final LocalSocketImpl impl;
  
  private volatile boolean implCreated;
  
  private boolean isBound;
  
  private boolean isConnected;
  
  private LocalSocketAddress localAddress;
  
  private final int sockType;
  
  public LocalSocket() {
    this(2);
  }
  
  public LocalSocket(int paramInt) {
    this(new LocalSocketImpl(), paramInt);
  }
  
  private LocalSocket(LocalSocketImpl paramLocalSocketImpl, int paramInt) {
    this.impl = paramLocalSocketImpl;
    this.sockType = paramInt;
    this.isConnected = false;
    this.isBound = false;
  }
  
  public static LocalSocket createConnectedLocalSocket(FileDescriptor paramFileDescriptor) {
    return createConnectedLocalSocket(new LocalSocketImpl(paramFileDescriptor), 0);
  }
  
  static LocalSocket createLocalSocketForAccept(LocalSocketImpl paramLocalSocketImpl) {
    return createConnectedLocalSocket(paramLocalSocketImpl, 0);
  }
  
  private static LocalSocket createConnectedLocalSocket(LocalSocketImpl paramLocalSocketImpl, int paramInt) {
    LocalSocket localSocket = new LocalSocket(paramLocalSocketImpl, paramInt);
    localSocket.isConnected = true;
    localSocket.isBound = true;
    localSocket.implCreated = true;
    return localSocket;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(super.toString());
    stringBuilder.append(" impl:");
    stringBuilder.append(this.impl);
    return stringBuilder.toString();
  }
  
  private void implCreateIfNeeded() throws IOException {
    // Byte code:
    //   0: aload_0
    //   1: getfield implCreated : Z
    //   4: ifne -> 55
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield implCreated : Z
    //   13: istore_1
    //   14: iload_1
    //   15: ifne -> 45
    //   18: aload_0
    //   19: getfield impl : Landroid/net/LocalSocketImpl;
    //   22: aload_0
    //   23: getfield sockType : I
    //   26: invokevirtual create : (I)V
    //   29: aload_0
    //   30: iconst_1
    //   31: putfield implCreated : Z
    //   34: goto -> 45
    //   37: astore_2
    //   38: aload_0
    //   39: iconst_1
    //   40: putfield implCreated : Z
    //   43: aload_2
    //   44: athrow
    //   45: aload_0
    //   46: monitorexit
    //   47: goto -> 55
    //   50: astore_2
    //   51: aload_0
    //   52: monitorexit
    //   53: aload_2
    //   54: athrow
    //   55: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #120	-> 0
    //   #121	-> 7
    //   #122	-> 9
    //   #124	-> 18
    //   #126	-> 29
    //   #127	-> 34
    //   #126	-> 37
    //   #127	-> 43
    //   #129	-> 45
    //   #131	-> 55
    // Exception table:
    //   from	to	target	type
    //   9	14	50	finally
    //   18	29	37	finally
    //   29	34	50	finally
    //   38	43	50	finally
    //   43	45	50	finally
    //   45	47	50	finally
    //   51	53	50	finally
  }
  
  public void connect(LocalSocketAddress paramLocalSocketAddress) throws IOException {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield isConnected : Z
    //   6: ifne -> 35
    //   9: aload_0
    //   10: invokespecial implCreateIfNeeded : ()V
    //   13: aload_0
    //   14: getfield impl : Landroid/net/LocalSocketImpl;
    //   17: aload_1
    //   18: iconst_0
    //   19: invokevirtual connect : (Landroid/net/LocalSocketAddress;I)V
    //   22: aload_0
    //   23: iconst_1
    //   24: putfield isConnected : Z
    //   27: aload_0
    //   28: iconst_1
    //   29: putfield isBound : Z
    //   32: aload_0
    //   33: monitorexit
    //   34: return
    //   35: new java/io/IOException
    //   38: astore_1
    //   39: aload_1
    //   40: ldc 'already connected'
    //   42: invokespecial <init> : (Ljava/lang/String;)V
    //   45: aload_1
    //   46: athrow
    //   47: astore_1
    //   48: aload_0
    //   49: monitorexit
    //   50: aload_1
    //   51: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #142	-> 0
    //   #143	-> 2
    //   #147	-> 9
    //   #148	-> 13
    //   #149	-> 22
    //   #150	-> 27
    //   #151	-> 32
    //   #152	-> 34
    //   #144	-> 35
    //   #151	-> 47
    // Exception table:
    //   from	to	target	type
    //   2	9	47	finally
    //   9	13	47	finally
    //   13	22	47	finally
    //   22	27	47	finally
    //   27	32	47	finally
    //   32	34	47	finally
    //   35	47	47	finally
    //   48	50	47	finally
  }
  
  public void bind(LocalSocketAddress paramLocalSocketAddress) throws IOException {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial implCreateIfNeeded : ()V
    //   4: aload_0
    //   5: monitorenter
    //   6: aload_0
    //   7: getfield isBound : Z
    //   10: ifne -> 34
    //   13: aload_0
    //   14: aload_1
    //   15: putfield localAddress : Landroid/net/LocalSocketAddress;
    //   18: aload_0
    //   19: getfield impl : Landroid/net/LocalSocketImpl;
    //   22: aload_1
    //   23: invokevirtual bind : (Landroid/net/LocalSocketAddress;)V
    //   26: aload_0
    //   27: iconst_1
    //   28: putfield isBound : Z
    //   31: aload_0
    //   32: monitorexit
    //   33: return
    //   34: new java/io/IOException
    //   37: astore_1
    //   38: aload_1
    //   39: ldc 'already bound'
    //   41: invokespecial <init> : (Ljava/lang/String;)V
    //   44: aload_1
    //   45: athrow
    //   46: astore_1
    //   47: aload_0
    //   48: monitorexit
    //   49: aload_1
    //   50: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #162	-> 0
    //   #164	-> 4
    //   #165	-> 6
    //   #169	-> 13
    //   #170	-> 18
    //   #171	-> 26
    //   #172	-> 31
    //   #173	-> 33
    //   #166	-> 34
    //   #172	-> 46
    // Exception table:
    //   from	to	target	type
    //   6	13	46	finally
    //   13	18	46	finally
    //   18	26	46	finally
    //   26	31	46	finally
    //   31	33	46	finally
    //   34	46	46	finally
    //   47	49	46	finally
  }
  
  public LocalSocketAddress getLocalSocketAddress() {
    return this.localAddress;
  }
  
  public InputStream getInputStream() throws IOException {
    implCreateIfNeeded();
    return this.impl.getInputStream();
  }
  
  public OutputStream getOutputStream() throws IOException {
    implCreateIfNeeded();
    return this.impl.getOutputStream();
  }
  
  public void close() throws IOException {
    implCreateIfNeeded();
    this.impl.close();
  }
  
  public void shutdownInput() throws IOException {
    implCreateIfNeeded();
    this.impl.shutdownInput();
  }
  
  public void shutdownOutput() throws IOException {
    implCreateIfNeeded();
    this.impl.shutdownOutput();
  }
  
  public void setReceiveBufferSize(int paramInt) throws IOException {
    this.impl.setOption(4098, Integer.valueOf(paramInt));
  }
  
  public int getReceiveBufferSize() throws IOException {
    return ((Integer)this.impl.getOption(4098)).intValue();
  }
  
  public void setSoTimeout(int paramInt) throws IOException {
    this.impl.setOption(4102, Integer.valueOf(paramInt));
  }
  
  public int getSoTimeout() throws IOException {
    return ((Integer)this.impl.getOption(4102)).intValue();
  }
  
  public void setSendBufferSize(int paramInt) throws IOException {
    this.impl.setOption(4097, Integer.valueOf(paramInt));
  }
  
  public int getSendBufferSize() throws IOException {
    return ((Integer)this.impl.getOption(4097)).intValue();
  }
  
  public LocalSocketAddress getRemoteSocketAddress() {
    throw new UnsupportedOperationException();
  }
  
  public boolean isConnected() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield isConnected : Z
    //   6: istore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: iload_1
    //   10: ireturn
    //   11: astore_2
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_2
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #268	-> 2
    //   #268	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	7	11	finally
  }
  
  public boolean isClosed() {
    throw new UnsupportedOperationException();
  }
  
  public boolean isBound() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield isBound : Z
    //   6: istore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: iload_1
    //   10: ireturn
    //   11: astore_2
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_2
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #278	-> 2
    //   #278	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	7	11	finally
  }
  
  public boolean isOutputShutdown() {
    throw new UnsupportedOperationException();
  }
  
  public boolean isInputShutdown() {
    throw new UnsupportedOperationException();
  }
  
  public void connect(LocalSocketAddress paramLocalSocketAddress, int paramInt) throws IOException {
    throw new UnsupportedOperationException();
  }
  
  public void setFileDescriptorsForSend(FileDescriptor[] paramArrayOfFileDescriptor) {
    this.impl.setFileDescriptorsForSend(paramArrayOfFileDescriptor);
  }
  
  public FileDescriptor[] getAncillaryFileDescriptors() throws IOException {
    return this.impl.getAncillaryFileDescriptors();
  }
  
  public Credentials getPeerCredentials() throws IOException {
    return this.impl.getPeerCredentials();
  }
  
  public FileDescriptor getFileDescriptor() {
    return this.impl.getFileDescriptor();
  }
}
