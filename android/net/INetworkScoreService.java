package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface INetworkScoreService extends IInterface {
  boolean clearScores() throws RemoteException;
  
  void disableScoring() throws RemoteException;
  
  NetworkScorerAppData getActiveScorer() throws RemoteException;
  
  String getActiveScorerPackage() throws RemoteException;
  
  List<NetworkScorerAppData> getAllValidScorers() throws RemoteException;
  
  boolean isCallerActiveScorer(int paramInt) throws RemoteException;
  
  void registerNetworkScoreCache(int paramInt1, INetworkScoreCache paramINetworkScoreCache, int paramInt2) throws RemoteException;
  
  boolean requestScores(NetworkKey[] paramArrayOfNetworkKey) throws RemoteException;
  
  boolean setActiveScorer(String paramString) throws RemoteException;
  
  void unregisterNetworkScoreCache(int paramInt, INetworkScoreCache paramINetworkScoreCache) throws RemoteException;
  
  boolean updateScores(ScoredNetwork[] paramArrayOfScoredNetwork) throws RemoteException;
  
  class Default implements INetworkScoreService {
    public boolean updateScores(ScoredNetwork[] param1ArrayOfScoredNetwork) throws RemoteException {
      return false;
    }
    
    public boolean clearScores() throws RemoteException {
      return false;
    }
    
    public boolean setActiveScorer(String param1String) throws RemoteException {
      return false;
    }
    
    public void disableScoring() throws RemoteException {}
    
    public void registerNetworkScoreCache(int param1Int1, INetworkScoreCache param1INetworkScoreCache, int param1Int2) throws RemoteException {}
    
    public void unregisterNetworkScoreCache(int param1Int, INetworkScoreCache param1INetworkScoreCache) throws RemoteException {}
    
    public boolean requestScores(NetworkKey[] param1ArrayOfNetworkKey) throws RemoteException {
      return false;
    }
    
    public boolean isCallerActiveScorer(int param1Int) throws RemoteException {
      return false;
    }
    
    public String getActiveScorerPackage() throws RemoteException {
      return null;
    }
    
    public NetworkScorerAppData getActiveScorer() throws RemoteException {
      return null;
    }
    
    public List<NetworkScorerAppData> getAllValidScorers() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INetworkScoreService {
    private static final String DESCRIPTOR = "android.net.INetworkScoreService";
    
    static final int TRANSACTION_clearScores = 2;
    
    static final int TRANSACTION_disableScoring = 4;
    
    static final int TRANSACTION_getActiveScorer = 10;
    
    static final int TRANSACTION_getActiveScorerPackage = 9;
    
    static final int TRANSACTION_getAllValidScorers = 11;
    
    static final int TRANSACTION_isCallerActiveScorer = 8;
    
    static final int TRANSACTION_registerNetworkScoreCache = 5;
    
    static final int TRANSACTION_requestScores = 7;
    
    static final int TRANSACTION_setActiveScorer = 3;
    
    static final int TRANSACTION_unregisterNetworkScoreCache = 6;
    
    static final int TRANSACTION_updateScores = 1;
    
    public Stub() {
      attachInterface(this, "android.net.INetworkScoreService");
    }
    
    public static INetworkScoreService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.INetworkScoreService");
      if (iInterface != null && iInterface instanceof INetworkScoreService)
        return (INetworkScoreService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 11:
          return "getAllValidScorers";
        case 10:
          return "getActiveScorer";
        case 9:
          return "getActiveScorerPackage";
        case 8:
          return "isCallerActiveScorer";
        case 7:
          return "requestScores";
        case 6:
          return "unregisterNetworkScoreCache";
        case 5:
          return "registerNetworkScoreCache";
        case 4:
          return "disableScoring";
        case 3:
          return "setActiveScorer";
        case 2:
          return "clearScores";
        case 1:
          break;
      } 
      return "updateScores";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int i;
        List<NetworkScorerAppData> list;
        NetworkScorerAppData networkScorerAppData;
        String str2;
        NetworkKey[] arrayOfNetworkKey;
        INetworkScoreCache iNetworkScoreCache1;
        String str1;
        INetworkScoreCache iNetworkScoreCache2;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 11:
            param1Parcel1.enforceInterface("android.net.INetworkScoreService");
            list = getAllValidScorers();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 10:
            list.enforceInterface("android.net.INetworkScoreService");
            networkScorerAppData = getActiveScorer();
            param1Parcel2.writeNoException();
            if (networkScorerAppData != null) {
              param1Parcel2.writeInt(1);
              networkScorerAppData.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 9:
            networkScorerAppData.enforceInterface("android.net.INetworkScoreService");
            str2 = getActiveScorerPackage();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 8:
            str2.enforceInterface("android.net.INetworkScoreService");
            param1Int1 = str2.readInt();
            bool2 = isCallerActiveScorer(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 7:
            str2.enforceInterface("android.net.INetworkScoreService");
            arrayOfNetworkKey = str2.<NetworkKey>createTypedArray(NetworkKey.CREATOR);
            bool2 = requestScores(arrayOfNetworkKey);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 6:
            arrayOfNetworkKey.enforceInterface("android.net.INetworkScoreService");
            i = arrayOfNetworkKey.readInt();
            iNetworkScoreCache1 = INetworkScoreCache.Stub.asInterface(arrayOfNetworkKey.readStrongBinder());
            unregisterNetworkScoreCache(i, iNetworkScoreCache1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            iNetworkScoreCache1.enforceInterface("android.net.INetworkScoreService");
            i = iNetworkScoreCache1.readInt();
            iNetworkScoreCache2 = INetworkScoreCache.Stub.asInterface(iNetworkScoreCache1.readStrongBinder());
            param1Int2 = iNetworkScoreCache1.readInt();
            registerNetworkScoreCache(i, iNetworkScoreCache2, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            iNetworkScoreCache1.enforceInterface("android.net.INetworkScoreService");
            disableScoring();
            param1Parcel2.writeNoException();
            return true;
          case 3:
            iNetworkScoreCache1.enforceInterface("android.net.INetworkScoreService");
            str1 = iNetworkScoreCache1.readString();
            bool1 = setActiveScorer(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 2:
            str1.enforceInterface("android.net.INetworkScoreService");
            bool1 = clearScores();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.net.INetworkScoreService");
        ScoredNetwork[] arrayOfScoredNetwork = str1.<ScoredNetwork>createTypedArray(ScoredNetwork.CREATOR);
        boolean bool1 = updateScores(arrayOfScoredNetwork);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("android.net.INetworkScoreService");
      return true;
    }
    
    private static class Proxy implements INetworkScoreService {
      public static INetworkScoreService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.INetworkScoreService";
      }
      
      public boolean updateScores(ScoredNetwork[] param2ArrayOfScoredNetwork) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkScoreService");
          boolean bool1 = false;
          parcel1.writeTypedArray(param2ArrayOfScoredNetwork, 0);
          boolean bool2 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool2 && INetworkScoreService.Stub.getDefaultImpl() != null) {
            bool1 = INetworkScoreService.Stub.getDefaultImpl().updateScores(param2ArrayOfScoredNetwork);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean clearScores() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkScoreService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && INetworkScoreService.Stub.getDefaultImpl() != null) {
            bool1 = INetworkScoreService.Stub.getDefaultImpl().clearScores();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setActiveScorer(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkScoreService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && INetworkScoreService.Stub.getDefaultImpl() != null) {
            bool1 = INetworkScoreService.Stub.getDefaultImpl().setActiveScorer(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disableScoring() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkScoreService");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && INetworkScoreService.Stub.getDefaultImpl() != null) {
            INetworkScoreService.Stub.getDefaultImpl().disableScoring();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerNetworkScoreCache(int param2Int1, INetworkScoreCache param2INetworkScoreCache, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.net.INetworkScoreService");
          parcel1.writeInt(param2Int1);
          if (param2INetworkScoreCache != null) {
            iBinder = param2INetworkScoreCache.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && INetworkScoreService.Stub.getDefaultImpl() != null) {
            INetworkScoreService.Stub.getDefaultImpl().registerNetworkScoreCache(param2Int1, param2INetworkScoreCache, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterNetworkScoreCache(int param2Int, INetworkScoreCache param2INetworkScoreCache) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.net.INetworkScoreService");
          parcel1.writeInt(param2Int);
          if (param2INetworkScoreCache != null) {
            iBinder = param2INetworkScoreCache.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && INetworkScoreService.Stub.getDefaultImpl() != null) {
            INetworkScoreService.Stub.getDefaultImpl().unregisterNetworkScoreCache(param2Int, param2INetworkScoreCache);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean requestScores(NetworkKey[] param2ArrayOfNetworkKey) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkScoreService");
          boolean bool1 = false;
          parcel1.writeTypedArray(param2ArrayOfNetworkKey, 0);
          boolean bool2 = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool2 && INetworkScoreService.Stub.getDefaultImpl() != null) {
            bool1 = INetworkScoreService.Stub.getDefaultImpl().requestScores(param2ArrayOfNetworkKey);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isCallerActiveScorer(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkScoreService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(8, parcel1, parcel2, 0);
          if (!bool2 && INetworkScoreService.Stub.getDefaultImpl() != null) {
            bool1 = INetworkScoreService.Stub.getDefaultImpl().isCallerActiveScorer(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getActiveScorerPackage() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkScoreService");
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && INetworkScoreService.Stub.getDefaultImpl() != null)
            return INetworkScoreService.Stub.getDefaultImpl().getActiveScorerPackage(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkScorerAppData getActiveScorer() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          NetworkScorerAppData networkScorerAppData;
          parcel1.writeInterfaceToken("android.net.INetworkScoreService");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && INetworkScoreService.Stub.getDefaultImpl() != null) {
            networkScorerAppData = INetworkScoreService.Stub.getDefaultImpl().getActiveScorer();
            return networkScorerAppData;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            networkScorerAppData = NetworkScorerAppData.CREATOR.createFromParcel(parcel2);
          } else {
            networkScorerAppData = null;
          } 
          return networkScorerAppData;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<NetworkScorerAppData> getAllValidScorers() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkScoreService");
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && INetworkScoreService.Stub.getDefaultImpl() != null)
            return INetworkScoreService.Stub.getDefaultImpl().getAllValidScorers(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(NetworkScorerAppData.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INetworkScoreService param1INetworkScoreService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INetworkScoreService != null) {
          Proxy.sDefaultImpl = param1INetworkScoreService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INetworkScoreService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
