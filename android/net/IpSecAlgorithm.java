package android.net;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.HexDump;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;

public final class IpSecAlgorithm implements Parcelable {
  public static final String AUTH_CRYPT_AES_GCM = "rfc4106(gcm(aes))";
  
  public static final String AUTH_HMAC_MD5 = "hmac(md5)";
  
  public static final String AUTH_HMAC_SHA1 = "hmac(sha1)";
  
  public static final String AUTH_HMAC_SHA256 = "hmac(sha256)";
  
  public static final String AUTH_HMAC_SHA384 = "hmac(sha384)";
  
  public static final String AUTH_HMAC_SHA512 = "hmac(sha512)";
  
  public IpSecAlgorithm(String paramString, byte[] paramArrayOfbyte) {
    this(paramString, paramArrayOfbyte, 0);
  }
  
  public IpSecAlgorithm(String paramString, byte[] paramArrayOfbyte, int paramInt) {
    this.mName = paramString;
    byte[] arrayOfByte = (byte[])paramArrayOfbyte.clone();
    this.mTruncLenBits = paramInt;
    checkValidOrThrow(this.mName, arrayOfByte.length * 8, paramInt);
  }
  
  public String getName() {
    return this.mName;
  }
  
  public byte[] getKey() {
    return (byte[])this.mKey.clone();
  }
  
  public int getTruncationLengthBits() {
    return this.mTruncLenBits;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mName);
    paramParcel.writeByteArray(this.mKey);
    paramParcel.writeInt(this.mTruncLenBits);
  }
  
  public static final Parcelable.Creator<IpSecAlgorithm> CREATOR = new Parcelable.Creator<IpSecAlgorithm>() {
      public IpSecAlgorithm createFromParcel(Parcel param1Parcel) {
        String str = param1Parcel.readString();
        byte[] arrayOfByte = param1Parcel.createByteArray();
        int i = param1Parcel.readInt();
        return new IpSecAlgorithm(str, arrayOfByte, i);
      }
      
      public IpSecAlgorithm[] newArray(int param1Int) {
        return new IpSecAlgorithm[param1Int];
      }
    };
  
  public static final String CRYPT_AES_CBC = "cbc(aes)";
  
  public static final String CRYPT_NULL = "ecb(cipher_null)";
  
  private static final String TAG = "IpSecAlgorithm";
  
  private final byte[] mKey;
  
  private final String mName;
  
  private final int mTruncLenBits;
  
  private static void checkValidOrThrow(String paramString, int paramInt1, int paramInt2) {
    StringBuilder stringBuilder2;
    int i = 1;
    int j = paramString.hashCode();
    boolean bool1 = true, bool2 = true, bool3 = true, bool4 = true, bool5 = true, bool6 = true;
    int k = 1;
    switch (j) {
      default:
        j = -1;
        break;
      case 2065384259:
        if (paramString.equals("hmac(sha1)")) {
          j = 2;
          break;
        } 
      case 759177996:
        if (paramString.equals("hmac(md5)")) {
          j = 1;
          break;
        } 
      case 559510590:
        if (paramString.equals("hmac(sha512)")) {
          j = 5;
          break;
        } 
      case 559457797:
        if (paramString.equals("hmac(sha384)")) {
          j = 4;
          break;
        } 
      case 559425185:
        if (paramString.equals("hmac(sha256)")) {
          j = 3;
          break;
        } 
      case 394796030:
        if (paramString.equals("cbc(aes)")) {
          j = 0;
          break;
        } 
      case -1137603038:
        if (paramString.equals("rfc4106(gcm(aes))")) {
          j = 6;
          break;
        } 
    } 
    switch (j) {
      default:
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append("Couldn't find an algorithm: ");
        stringBuilder2.append(paramString);
        throw new IllegalArgumentException(stringBuilder2.toString());
      case 6:
        if (paramInt1 == 160 || paramInt1 == 224 || paramInt1 == 288) {
          j = 1;
        } else {
          j = 0;
        } 
        i = j;
        j = k;
        if (paramInt2 != 64) {
          j = k;
          if (paramInt2 != 96)
            if (paramInt2 == 128) {
              j = k;
            } else {
              j = 0;
            }  
        } 
        k = j;
        j = i;
        i = k;
        break;
      case 5:
        if (paramInt1 == 512) {
          j = 1;
        } else {
          j = 0;
        } 
        i = j;
        if (paramInt2 >= 256 && paramInt2 <= 512) {
          j = bool1;
        } else {
          j = 0;
        } 
        k = j;
        j = i;
        i = k;
        break;
      case 4:
        if (paramInt1 == 384) {
          j = 1;
        } else {
          j = 0;
        } 
        i = j;
        if (paramInt2 >= 192 && paramInt2 <= 384) {
          j = bool2;
        } else {
          j = 0;
        } 
        k = j;
        j = i;
        i = k;
        break;
      case 3:
        if (paramInt1 == 256) {
          j = 1;
        } else {
          j = 0;
        } 
        i = j;
        if (paramInt2 >= 96 && paramInt2 <= 256) {
          j = bool3;
        } else {
          j = 0;
        } 
        k = j;
        j = i;
        i = k;
        break;
      case 2:
        if (paramInt1 == 160) {
          j = 1;
        } else {
          j = 0;
        } 
        i = j;
        if (paramInt2 >= 96 && paramInt2 <= 160) {
          j = bool4;
        } else {
          j = 0;
        } 
        k = j;
        j = i;
        i = k;
        break;
      case 1:
        if (paramInt1 == 128) {
          j = 1;
        } else {
          j = 0;
        } 
        i = j;
        if (paramInt2 >= 96 && paramInt2 <= 128) {
          j = bool5;
        } else {
          j = 0;
        } 
        k = j;
        j = i;
        i = k;
        break;
      case 0:
        j = bool6;
        if (paramInt1 != 128) {
          j = bool6;
          if (paramInt1 != 192) {
            if (paramInt1 == 256) {
              j = bool6;
              break;
            } 
            j = 0;
          } 
        } 
        break;
    } 
    if (j != 0) {
      if (i != 0)
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid truncation keyLength: ");
      stringBuilder.append(paramInt2);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("Invalid key material keyLength: ");
    stringBuilder1.append(paramInt1);
    throw new IllegalArgumentException(stringBuilder1.toString());
  }
  
  public boolean isAuthentication() {
    byte b;
    String str = getName();
    switch (str.hashCode()) {
      default:
        b = -1;
        break;
      case 2065384259:
        if (str.equals("hmac(sha1)")) {
          b = 1;
          break;
        } 
      case 759177996:
        if (str.equals("hmac(md5)")) {
          b = 0;
          break;
        } 
      case 559510590:
        if (str.equals("hmac(sha512)")) {
          b = 4;
          break;
        } 
      case 559457797:
        if (str.equals("hmac(sha384)")) {
          b = 3;
          break;
        } 
      case 559425185:
        if (str.equals("hmac(sha256)")) {
          b = 2;
          break;
        } 
    } 
    if (b != 0 && b != 1 && b != 2 && b != 3 && b != 4)
      return false; 
    return true;
  }
  
  public boolean isEncryption() {
    return getName().equals("cbc(aes)");
  }
  
  public boolean isAead() {
    return getName().equals("rfc4106(gcm(aes))");
  }
  
  private static boolean isUnsafeBuild() {
    boolean bool;
    if (Build.IS_DEBUGGABLE && Build.IS_ENG) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{mName=");
    null = this.mName;
    stringBuilder.append(null);
    stringBuilder.append(", mKey=");
    if (isUnsafeBuild()) {
      null = HexDump.toHexString(this.mKey);
    } else {
      null = "<hidden>";
    } 
    stringBuilder.append(null);
    stringBuilder.append(", mTruncLenBits=");
    int i = this.mTruncLenBits;
    stringBuilder.append(i);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public static boolean equals(IpSecAlgorithm paramIpSecAlgorithm1, IpSecAlgorithm paramIpSecAlgorithm2) {
    null = true;
    boolean bool = true;
    if (paramIpSecAlgorithm1 == null || paramIpSecAlgorithm2 == null) {
      if (paramIpSecAlgorithm1 != paramIpSecAlgorithm2)
        null = false; 
      return null;
    } 
    if (paramIpSecAlgorithm1.mName.equals(paramIpSecAlgorithm2.mName)) {
      byte[] arrayOfByte1 = paramIpSecAlgorithm1.mKey, arrayOfByte2 = paramIpSecAlgorithm2.mKey;
      if (Arrays.equals(arrayOfByte1, arrayOfByte2) && paramIpSecAlgorithm1.mTruncLenBits == paramIpSecAlgorithm2.mTruncLenBits)
        return bool; 
    } 
    return false;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class AlgorithmName implements Annotation {}
}
