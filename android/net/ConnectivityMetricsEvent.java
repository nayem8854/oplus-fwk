package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.BitUtils;

public final class ConnectivityMetricsEvent implements Parcelable {
  public ConnectivityMetricsEvent() {}
  
  private ConnectivityMetricsEvent(Parcel paramParcel) {
    this.timestamp = paramParcel.readLong();
    this.transports = paramParcel.readLong();
    this.netId = paramParcel.readInt();
    this.ifname = paramParcel.readString();
    this.data = paramParcel.readParcelable(null);
  }
  
  public static final Parcelable.Creator<ConnectivityMetricsEvent> CREATOR = new Parcelable.Creator<ConnectivityMetricsEvent>() {
      public ConnectivityMetricsEvent createFromParcel(Parcel param1Parcel) {
        return new ConnectivityMetricsEvent(param1Parcel);
      }
      
      public ConnectivityMetricsEvent[] newArray(int param1Int) {
        return new ConnectivityMetricsEvent[param1Int];
      }
    };
  
  public Parcelable data;
  
  public String ifname;
  
  public int netId;
  
  public long timestamp;
  
  public long transports;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.timestamp);
    paramParcel.writeLong(this.transports);
    paramParcel.writeInt(this.netId);
    paramParcel.writeString(this.ifname);
    paramParcel.writeParcelable(this.data, 0);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("ConnectivityMetricsEvent(");
    long l = this.timestamp;
    byte b = 0;
    stringBuilder.append(String.format("%tT.%tL", new Object[] { Long.valueOf(l), Long.valueOf(this.timestamp) }));
    if (this.netId != 0) {
      stringBuilder.append(", ");
      stringBuilder.append("netId=");
      stringBuilder.append(this.netId);
    } 
    if (this.ifname != null) {
      stringBuilder.append(", ");
      stringBuilder.append(this.ifname);
    } 
    for (int arrayOfInt[] = BitUtils.unpackBits(this.transports), i = arrayOfInt.length; b < i; ) {
      int j = arrayOfInt[b];
      stringBuilder.append(", ");
      stringBuilder.append(NetworkCapabilities.transportNameOf(j));
      b++;
    } 
    stringBuilder.append("): ");
    stringBuilder.append(this.data.toString());
    return stringBuilder.toString();
  }
}
