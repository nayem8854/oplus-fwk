package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ITestNetworkManager extends IInterface {
  TestNetworkInterface createTapInterface() throws RemoteException;
  
  TestNetworkInterface createTunInterface(LinkAddress[] paramArrayOfLinkAddress) throws RemoteException;
  
  void setupTestNetwork(String paramString, LinkProperties paramLinkProperties, boolean paramBoolean, int[] paramArrayOfint, IBinder paramIBinder) throws RemoteException;
  
  void teardownTestNetwork(int paramInt) throws RemoteException;
  
  class Default implements ITestNetworkManager {
    public TestNetworkInterface createTunInterface(LinkAddress[] param1ArrayOfLinkAddress) throws RemoteException {
      return null;
    }
    
    public TestNetworkInterface createTapInterface() throws RemoteException {
      return null;
    }
    
    public void setupTestNetwork(String param1String, LinkProperties param1LinkProperties, boolean param1Boolean, int[] param1ArrayOfint, IBinder param1IBinder) throws RemoteException {}
    
    public void teardownTestNetwork(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITestNetworkManager {
    private static final String DESCRIPTOR = "android.net.ITestNetworkManager";
    
    static final int TRANSACTION_createTapInterface = 2;
    
    static final int TRANSACTION_createTunInterface = 1;
    
    static final int TRANSACTION_setupTestNetwork = 3;
    
    static final int TRANSACTION_teardownTestNetwork = 4;
    
    public Stub() {
      attachInterface(this, "android.net.ITestNetworkManager");
    }
    
    public static ITestNetworkManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.ITestNetworkManager");
      if (iInterface != null && iInterface instanceof ITestNetworkManager)
        return (ITestNetworkManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "teardownTestNetwork";
          } 
          return "setupTestNetwork";
        } 
        return "createTapInterface";
      } 
      return "createTunInterface";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      TestNetworkInterface testNetworkInterface2;
      if (param1Int1 != 1) {
        IBinder iBinder;
        if (param1Int1 != 2) {
          LinkProperties linkProperties;
          boolean bool;
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.net.ITestNetworkManager");
              return true;
            } 
            param1Parcel1.enforceInterface("android.net.ITestNetworkManager");
            param1Int1 = param1Parcel1.readInt();
            teardownTestNetwork(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          } 
          param1Parcel1.enforceInterface("android.net.ITestNetworkManager");
          String str = param1Parcel1.readString();
          if (param1Parcel1.readInt() != 0) {
            linkProperties = LinkProperties.CREATOR.createFromParcel(param1Parcel1);
          } else {
            linkProperties = null;
          } 
          if (param1Parcel1.readInt() != 0) {
            bool = true;
          } else {
            bool = false;
          } 
          int[] arrayOfInt = param1Parcel1.createIntArray();
          iBinder = param1Parcel1.readStrongBinder();
          setupTestNetwork(str, linkProperties, bool, arrayOfInt, iBinder);
          param1Parcel2.writeNoException();
          return true;
        } 
        iBinder.enforceInterface("android.net.ITestNetworkManager");
        testNetworkInterface2 = createTapInterface();
        param1Parcel2.writeNoException();
        if (testNetworkInterface2 != null) {
          param1Parcel2.writeInt(1);
          testNetworkInterface2.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      testNetworkInterface2.enforceInterface("android.net.ITestNetworkManager");
      LinkAddress[] arrayOfLinkAddress = testNetworkInterface2.<LinkAddress>createTypedArray(LinkAddress.CREATOR);
      TestNetworkInterface testNetworkInterface1 = createTunInterface(arrayOfLinkAddress);
      param1Parcel2.writeNoException();
      if (testNetworkInterface1 != null) {
        param1Parcel2.writeInt(1);
        testNetworkInterface1.writeToParcel(param1Parcel2, 1);
      } else {
        param1Parcel2.writeInt(0);
      } 
      return true;
    }
    
    private static class Proxy implements ITestNetworkManager {
      public static ITestNetworkManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.ITestNetworkManager";
      }
      
      public TestNetworkInterface createTunInterface(LinkAddress[] param2ArrayOfLinkAddress) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.ITestNetworkManager");
          parcel1.writeTypedArray(param2ArrayOfLinkAddress, 0);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ITestNetworkManager.Stub.getDefaultImpl() != null)
            return ITestNetworkManager.Stub.getDefaultImpl().createTunInterface(param2ArrayOfLinkAddress); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            TestNetworkInterface testNetworkInterface = TestNetworkInterface.CREATOR.createFromParcel(parcel2);
          } else {
            param2ArrayOfLinkAddress = null;
          } 
          return (TestNetworkInterface)param2ArrayOfLinkAddress;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public TestNetworkInterface createTapInterface() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          TestNetworkInterface testNetworkInterface;
          parcel1.writeInterfaceToken("android.net.ITestNetworkManager");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ITestNetworkManager.Stub.getDefaultImpl() != null) {
            testNetworkInterface = ITestNetworkManager.Stub.getDefaultImpl().createTapInterface();
            return testNetworkInterface;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            testNetworkInterface = TestNetworkInterface.CREATOR.createFromParcel(parcel2);
          } else {
            testNetworkInterface = null;
          } 
          return testNetworkInterface;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setupTestNetwork(String param2String, LinkProperties param2LinkProperties, boolean param2Boolean, int[] param2ArrayOfint, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.ITestNetworkManager");
          parcel1.writeString(param2String);
          boolean bool = true;
          if (param2LinkProperties != null) {
            parcel1.writeInt(1);
            param2LinkProperties.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          parcel1.writeIntArray(param2ArrayOfint);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool1 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool1 && ITestNetworkManager.Stub.getDefaultImpl() != null) {
            ITestNetworkManager.Stub.getDefaultImpl().setupTestNetwork(param2String, param2LinkProperties, param2Boolean, param2ArrayOfint, param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void teardownTestNetwork(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.ITestNetworkManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ITestNetworkManager.Stub.getDefaultImpl() != null) {
            ITestNetworkManager.Stub.getDefaultImpl().teardownTestNetwork(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITestNetworkManager param1ITestNetworkManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITestNetworkManager != null) {
          Proxy.sDefaultImpl = param1ITestNetworkManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITestNetworkManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
