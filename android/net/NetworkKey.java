package android.net;

import android.annotation.SystemApi;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;

@SystemApi
public class NetworkKey implements Parcelable {
  public static NetworkKey createFromScanResult(ScanResult paramScanResult) {
    Objects.requireNonNull(paramScanResult);
    String str1 = paramScanResult.SSID;
    if (TextUtils.isEmpty(str1) || str1.equals("<unknown ssid>"))
      return null; 
    String str2 = paramScanResult.BSSID;
    if (TextUtils.isEmpty(str2))
      return null; 
    try {
      WifiKey wifiKey = new WifiKey();
      this(String.format("\"%s\"", new Object[] { str1 }), str2);
      return new NetworkKey(wifiKey);
    } catch (IllegalArgumentException illegalArgumentException) {
      Log.e("NetworkKey", "Unable to create WifiKey.", illegalArgumentException);
      return null;
    } 
  }
  
  public static NetworkKey createFromWifiInfo(WifiInfo paramWifiInfo) {
    if (paramWifiInfo != null) {
      String str2 = paramWifiInfo.getSSID();
      String str1 = paramWifiInfo.getBSSID();
      if (!TextUtils.isEmpty(str2) && !str2.equals("<unknown ssid>") && 
        !TextUtils.isEmpty(str1))
        try {
          WifiKey wifiKey = new WifiKey(str2, str1);
          return new NetworkKey(wifiKey);
        } catch (IllegalArgumentException illegalArgumentException) {
          Log.e("NetworkKey", "Unable to create WifiKey.", illegalArgumentException);
          return null;
        }  
    } 
    return null;
  }
  
  public NetworkKey(WifiKey paramWifiKey) {
    this.type = 1;
    this.wifiKey = paramWifiKey;
  }
  
  private NetworkKey(Parcel paramParcel) {
    int i = paramParcel.readInt();
    if (i == 1) {
      this.wifiKey = WifiKey.CREATOR.createFromParcel(paramParcel);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Parcel has unknown type: ");
    stringBuilder.append(this.type);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.type);
    if (this.type == 1) {
      this.wifiKey.writeToParcel(paramParcel, paramInt);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("NetworkKey has unknown type ");
    stringBuilder.append(this.type);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.type != ((NetworkKey)paramObject).type || !Objects.equals(this.wifiKey, ((NetworkKey)paramObject).wifiKey))
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.type), this.wifiKey });
  }
  
  public String toString() {
    if (this.type != 1)
      return "InvalidKey"; 
    return this.wifiKey.toString();
  }
  
  public static final Parcelable.Creator<NetworkKey> CREATOR = new Parcelable.Creator<NetworkKey>() {
      public NetworkKey createFromParcel(Parcel param1Parcel) {
        return new NetworkKey(param1Parcel);
      }
      
      public NetworkKey[] newArray(int param1Int) {
        return new NetworkKey[param1Int];
      }
    };
  
  private static final String TAG = "NetworkKey";
  
  public static final int TYPE_WIFI = 1;
  
  public final int type;
  
  public final WifiKey wifiKey;
  
  @Retention(RetentionPolicy.SOURCE)
  class NetworkType implements Annotation {}
}
