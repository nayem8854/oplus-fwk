package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INetworkRecommendationProvider extends IInterface {
  void requestScores(NetworkKey[] paramArrayOfNetworkKey) throws RemoteException;
  
  class Default implements INetworkRecommendationProvider {
    public void requestScores(NetworkKey[] param1ArrayOfNetworkKey) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INetworkRecommendationProvider {
    private static final String DESCRIPTOR = "android.net.INetworkRecommendationProvider";
    
    static final int TRANSACTION_requestScores = 1;
    
    public Stub() {
      attachInterface(this, "android.net.INetworkRecommendationProvider");
    }
    
    public static INetworkRecommendationProvider asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.INetworkRecommendationProvider");
      if (iInterface != null && iInterface instanceof INetworkRecommendationProvider)
        return (INetworkRecommendationProvider)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "requestScores";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.net.INetworkRecommendationProvider");
        return true;
      } 
      param1Parcel1.enforceInterface("android.net.INetworkRecommendationProvider");
      NetworkKey[] arrayOfNetworkKey = param1Parcel1.<NetworkKey>createTypedArray(NetworkKey.CREATOR);
      requestScores(arrayOfNetworkKey);
      return true;
    }
    
    private static class Proxy implements INetworkRecommendationProvider {
      public static INetworkRecommendationProvider sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.INetworkRecommendationProvider";
      }
      
      public void requestScores(NetworkKey[] param2ArrayOfNetworkKey) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.INetworkRecommendationProvider");
          parcel.writeTypedArray(param2ArrayOfNetworkKey, 0);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && INetworkRecommendationProvider.Stub.getDefaultImpl() != null) {
            INetworkRecommendationProvider.Stub.getDefaultImpl().requestScores(param2ArrayOfNetworkKey);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INetworkRecommendationProvider param1INetworkRecommendationProvider) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INetworkRecommendationProvider != null) {
          Proxy.sDefaultImpl = param1INetworkRecommendationProvider;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INetworkRecommendationProvider getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
