package android.net.lowpan;

import java.util.Map;

public abstract class LowpanProperty<T> {
  public void putInMap(Map<String, T> paramMap, T paramT) {
    paramMap.put(getName(), paramT);
  }
  
  public abstract Class<T> getType();
  
  public abstract String getName();
  
  public T getFromMap(Map paramMap) {
    return (T)paramMap.get(getName());
  }
}
