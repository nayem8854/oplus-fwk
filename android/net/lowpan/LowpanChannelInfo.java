package android.net.lowpan;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public class LowpanChannelInfo implements Parcelable {
  private int mIndex = 0;
  
  private String mName = null;
  
  private float mSpectrumCenterFrequency = 0.0F;
  
  private float mSpectrumBandwidth = 0.0F;
  
  private int mMaxTransmitPower = Integer.MAX_VALUE;
  
  private boolean mIsMaskedByRegulatoryDomain = false;
  
  public static LowpanChannelInfo getChannelInfoForIeee802154Page0(int paramInt) {
    LowpanChannelInfo lowpanChannelInfo = new LowpanChannelInfo();
    if (paramInt < 0) {
      lowpanChannelInfo = null;
    } else if (paramInt == 0) {
      lowpanChannelInfo.mSpectrumCenterFrequency = 8.683E8F;
      lowpanChannelInfo.mSpectrumBandwidth = 600000.0F;
    } else if (paramInt < 11) {
      lowpanChannelInfo.mSpectrumCenterFrequency = paramInt * 2000000.0F + 9.04E8F;
      lowpanChannelInfo.mSpectrumBandwidth = 0.0F;
    } else if (paramInt < 26) {
      lowpanChannelInfo.mSpectrumCenterFrequency = paramInt * 5000000.0F + 2.3499999E9F;
      lowpanChannelInfo.mSpectrumBandwidth = 2000000.0F;
    } else {
      lowpanChannelInfo = null;
    } 
    lowpanChannelInfo.mName = Integer.toString(paramInt);
    return lowpanChannelInfo;
  }
  
  private LowpanChannelInfo(int paramInt, String paramString, float paramFloat1, float paramFloat2) {
    this.mIndex = paramInt;
    this.mName = paramString;
    this.mSpectrumCenterFrequency = paramFloat1;
    this.mSpectrumBandwidth = paramFloat2;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public int getIndex() {
    return this.mIndex;
  }
  
  public int getMaxTransmitPower() {
    return this.mMaxTransmitPower;
  }
  
  public boolean isMaskedByRegulatoryDomain() {
    return this.mIsMaskedByRegulatoryDomain;
  }
  
  public float getSpectrumCenterFrequency() {
    return this.mSpectrumCenterFrequency;
  }
  
  public float getSpectrumBandwidth() {
    return this.mSpectrumBandwidth;
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append("Channel ");
    stringBuffer.append(this.mIndex);
    String str = this.mName;
    if (str != null && !str.equals(Integer.toString(this.mIndex))) {
      stringBuffer.append(" (");
      stringBuffer.append(this.mName);
      stringBuffer.append(")");
    } 
    float f = this.mSpectrumCenterFrequency;
    if (f > 0.0F)
      if (f > 1.0E9F) {
        stringBuffer.append(", SpectrumCenterFrequency: ");
        f = this.mSpectrumCenterFrequency / 1.0E9F;
        stringBuffer.append(f);
        stringBuffer.append("GHz");
      } else if (f > 1000000.0F) {
        stringBuffer.append(", SpectrumCenterFrequency: ");
        f = this.mSpectrumCenterFrequency / 1000000.0F;
        stringBuffer.append(f);
        stringBuffer.append("MHz");
      } else {
        stringBuffer.append(", SpectrumCenterFrequency: ");
        f = this.mSpectrumCenterFrequency / 1000.0F;
        stringBuffer.append(f);
        stringBuffer.append("kHz");
      }  
    f = this.mSpectrumBandwidth;
    if (f > 0.0F)
      if (f > 1.0E9F) {
        stringBuffer.append(", SpectrumBandwidth: ");
        f = this.mSpectrumBandwidth / 1.0E9F;
        stringBuffer.append(f);
        stringBuffer.append("GHz");
      } else if (f > 1000000.0F) {
        stringBuffer.append(", SpectrumBandwidth: ");
        f = this.mSpectrumBandwidth / 1000000.0F;
        stringBuffer.append(f);
        stringBuffer.append("MHz");
      } else {
        stringBuffer.append(", SpectrumBandwidth: ");
        f = this.mSpectrumBandwidth / 1000.0F;
        stringBuffer.append(f);
        stringBuffer.append("kHz");
      }  
    if (this.mMaxTransmitPower != Integer.MAX_VALUE) {
      stringBuffer.append(", MaxTransmitPower: ");
      stringBuffer.append(this.mMaxTransmitPower);
      stringBuffer.append("dBm");
    } 
    return stringBuffer.toString();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof LowpanChannelInfo;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    bool = bool1;
    if (Objects.equals(this.mName, ((LowpanChannelInfo)paramObject).mName)) {
      bool = bool1;
      if (this.mIndex == ((LowpanChannelInfo)paramObject).mIndex) {
        bool = bool1;
        if (this.mIsMaskedByRegulatoryDomain == ((LowpanChannelInfo)paramObject).mIsMaskedByRegulatoryDomain) {
          bool = bool1;
          if (this.mSpectrumCenterFrequency == ((LowpanChannelInfo)paramObject).mSpectrumCenterFrequency) {
            bool = bool1;
            if (this.mSpectrumBandwidth == ((LowpanChannelInfo)paramObject).mSpectrumBandwidth) {
              bool = bool1;
              if (this.mMaxTransmitPower == ((LowpanChannelInfo)paramObject).mMaxTransmitPower)
                bool = true; 
            } 
          } 
        } 
      } 
    } 
    return bool;
  }
  
  public int hashCode() {
    String str = this.mName;
    int i = this.mIndex;
    boolean bool = this.mIsMaskedByRegulatoryDomain;
    float f1 = this.mSpectrumCenterFrequency;
    float f2 = this.mSpectrumBandwidth;
    int j = this.mMaxTransmitPower;
    return Objects.hash(new Object[] { str, Integer.valueOf(i), Boolean.valueOf(bool), Float.valueOf(f1), Float.valueOf(f2), Integer.valueOf(j) });
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mIndex);
    paramParcel.writeString(this.mName);
    paramParcel.writeFloat(this.mSpectrumCenterFrequency);
    paramParcel.writeFloat(this.mSpectrumBandwidth);
    paramParcel.writeInt(this.mMaxTransmitPower);
    paramParcel.writeBoolean(this.mIsMaskedByRegulatoryDomain);
  }
  
  public static final Parcelable.Creator<LowpanChannelInfo> CREATOR = new Parcelable.Creator<LowpanChannelInfo>() {
      public LowpanChannelInfo createFromParcel(Parcel param1Parcel) {
        LowpanChannelInfo lowpanChannelInfo = new LowpanChannelInfo();
        LowpanChannelInfo.access$102(lowpanChannelInfo, param1Parcel.readInt());
        LowpanChannelInfo.access$202(lowpanChannelInfo, param1Parcel.readString());
        LowpanChannelInfo.access$302(lowpanChannelInfo, param1Parcel.readFloat());
        LowpanChannelInfo.access$402(lowpanChannelInfo, param1Parcel.readFloat());
        LowpanChannelInfo.access$502(lowpanChannelInfo, param1Parcel.readInt());
        LowpanChannelInfo.access$602(lowpanChannelInfo, param1Parcel.readBoolean());
        return lowpanChannelInfo;
      }
      
      public LowpanChannelInfo[] newArray(int param1Int) {
        return new LowpanChannelInfo[param1Int];
      }
    };
  
  public static final float UNKNOWN_BANDWIDTH = 0.0F;
  
  public static final float UNKNOWN_FREQUENCY = 0.0F;
  
  public static final int UNKNOWN_POWER = 2147483647;
  
  private LowpanChannelInfo() {}
}
