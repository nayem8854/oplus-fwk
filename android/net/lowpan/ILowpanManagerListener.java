package android.net.lowpan;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ILowpanManagerListener extends IInterface {
  void onInterfaceAdded(ILowpanInterface paramILowpanInterface) throws RemoteException;
  
  void onInterfaceRemoved(ILowpanInterface paramILowpanInterface) throws RemoteException;
  
  class Default implements ILowpanManagerListener {
    public void onInterfaceAdded(ILowpanInterface param1ILowpanInterface) throws RemoteException {}
    
    public void onInterfaceRemoved(ILowpanInterface param1ILowpanInterface) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ILowpanManagerListener {
    private static final String DESCRIPTOR = "android.net.lowpan.ILowpanManagerListener";
    
    static final int TRANSACTION_onInterfaceAdded = 1;
    
    static final int TRANSACTION_onInterfaceRemoved = 2;
    
    public Stub() {
      attachInterface(this, "android.net.lowpan.ILowpanManagerListener");
    }
    
    public static ILowpanManagerListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.lowpan.ILowpanManagerListener");
      if (iInterface != null && iInterface instanceof ILowpanManagerListener)
        return (ILowpanManagerListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onInterfaceRemoved";
      } 
      return "onInterfaceAdded";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.net.lowpan.ILowpanManagerListener");
          return true;
        } 
        param1Parcel1.enforceInterface("android.net.lowpan.ILowpanManagerListener");
        iLowpanInterface = ILowpanInterface.Stub.asInterface(param1Parcel1.readStrongBinder());
        onInterfaceRemoved(iLowpanInterface);
        return true;
      } 
      iLowpanInterface.enforceInterface("android.net.lowpan.ILowpanManagerListener");
      ILowpanInterface iLowpanInterface = ILowpanInterface.Stub.asInterface(iLowpanInterface.readStrongBinder());
      onInterfaceAdded(iLowpanInterface);
      return true;
    }
    
    private static class Proxy implements ILowpanManagerListener {
      public static ILowpanManagerListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.lowpan.ILowpanManagerListener";
      }
      
      public void onInterfaceAdded(ILowpanInterface param2ILowpanInterface) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanManagerListener");
          if (param2ILowpanInterface != null) {
            iBinder = param2ILowpanInterface.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ILowpanManagerListener.Stub.getDefaultImpl() != null) {
            ILowpanManagerListener.Stub.getDefaultImpl().onInterfaceAdded(param2ILowpanInterface);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onInterfaceRemoved(ILowpanInterface param2ILowpanInterface) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanManagerListener");
          if (param2ILowpanInterface != null) {
            iBinder = param2ILowpanInterface.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ILowpanManagerListener.Stub.getDefaultImpl() != null) {
            ILowpanManagerListener.Stub.getDefaultImpl().onInterfaceRemoved(param2ILowpanInterface);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ILowpanManagerListener param1ILowpanManagerListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ILowpanManagerListener != null) {
          Proxy.sDefaultImpl = param1ILowpanManagerListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ILowpanManagerListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
