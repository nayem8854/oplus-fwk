package android.net.lowpan;

public final class LowpanProperties {
  public static final LowpanProperty<int[]> KEY_CHANNEL_MASK = (LowpanProperty)new LowpanStandardProperty<>("android.net.lowpan.property.CHANNEL_MASK", (Class)int[].class);
  
  public static final LowpanProperty<Integer> KEY_MAX_TX_POWER = new LowpanStandardProperty<>("android.net.lowpan.property.MAX_TX_POWER", Integer.class);
  
  class LowpanStandardProperty<T> extends LowpanProperty<T> {
    private final String mName;
    
    private final Class<T> mType;
    
    LowpanStandardProperty(LowpanProperties this$0, Class<T> param1Class) {
      this.mName = (String)this$0;
      this.mType = param1Class;
    }
    
    public String getName() {
      return this.mName;
    }
    
    public Class<T> getType() {
      return this.mType;
    }
    
    public String toString() {
      return getName();
    }
  }
}
