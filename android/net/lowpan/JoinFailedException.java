package android.net.lowpan;

public class JoinFailedException extends LowpanException {
  public JoinFailedException() {}
  
  public JoinFailedException(String paramString) {
    super(paramString);
  }
  
  public JoinFailedException(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
  }
  
  protected JoinFailedException(Exception paramException) {
    super(paramException);
  }
}
