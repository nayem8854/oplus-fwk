package android.net.lowpan;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.HexDump;
import java.util.Arrays;
import java.util.Objects;

public class LowpanCredential implements Parcelable {
  private byte[] mMasterKey = null;
  
  private int mMasterKeyIndex = 0;
  
  private LowpanCredential(byte[] paramArrayOfbyte, int paramInt) {
    setMasterKey(paramArrayOfbyte, paramInt);
  }
  
  private LowpanCredential(byte[] paramArrayOfbyte) {
    setMasterKey(paramArrayOfbyte);
  }
  
  public static LowpanCredential createMasterKey(byte[] paramArrayOfbyte) {
    return new LowpanCredential(paramArrayOfbyte);
  }
  
  public static LowpanCredential createMasterKey(byte[] paramArrayOfbyte, int paramInt) {
    return new LowpanCredential(paramArrayOfbyte, paramInt);
  }
  
  void setMasterKey(byte[] paramArrayOfbyte) {
    byte[] arrayOfByte = paramArrayOfbyte;
    if (paramArrayOfbyte != null)
      arrayOfByte = (byte[])paramArrayOfbyte.clone(); 
    this.mMasterKey = arrayOfByte;
  }
  
  void setMasterKeyIndex(int paramInt) {
    this.mMasterKeyIndex = paramInt;
  }
  
  void setMasterKey(byte[] paramArrayOfbyte, int paramInt) {
    setMasterKey(paramArrayOfbyte);
    setMasterKeyIndex(paramInt);
  }
  
  public byte[] getMasterKey() {
    byte[] arrayOfByte = this.mMasterKey;
    if (arrayOfByte != null)
      return (byte[])arrayOfByte.clone(); 
    return null;
  }
  
  public int getMasterKeyIndex() {
    return this.mMasterKeyIndex;
  }
  
  public boolean isMasterKey() {
    boolean bool;
    if (this.mMasterKey != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String toSensitiveString() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append("<LowpanCredential");
    if (isMasterKey()) {
      stringBuffer.append(" MasterKey:");
      stringBuffer.append(HexDump.toHexString(this.mMasterKey));
      if (this.mMasterKeyIndex != 0) {
        stringBuffer.append(", Index:");
        stringBuffer.append(this.mMasterKeyIndex);
      } 
    } else {
      stringBuffer.append(" empty");
    } 
    stringBuffer.append(">");
    return stringBuffer.toString();
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append("<LowpanCredential");
    if (isMasterKey()) {
      stringBuffer.append(" MasterKey");
      if (this.mMasterKeyIndex != 0) {
        stringBuffer.append(", Index:");
        stringBuffer.append(this.mMasterKeyIndex);
      } 
    } else {
      stringBuffer.append(" empty");
    } 
    stringBuffer.append(">");
    return stringBuffer.toString();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof LowpanCredential;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    bool = bool1;
    if (Arrays.equals(this.mMasterKey, ((LowpanCredential)paramObject).mMasterKey)) {
      bool = bool1;
      if (this.mMasterKeyIndex == ((LowpanCredential)paramObject).mMasterKeyIndex)
        bool = true; 
    } 
    return bool;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(Arrays.hashCode(this.mMasterKey)), Integer.valueOf(this.mMasterKeyIndex) });
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByteArray(this.mMasterKey);
    paramParcel.writeInt(this.mMasterKeyIndex);
  }
  
  public static final Parcelable.Creator<LowpanCredential> CREATOR = new Parcelable.Creator<LowpanCredential>() {
      public LowpanCredential createFromParcel(Parcel param1Parcel) {
        LowpanCredential lowpanCredential = new LowpanCredential();
        LowpanCredential.access$002(lowpanCredential, param1Parcel.createByteArray());
        LowpanCredential.access$102(lowpanCredential, param1Parcel.readInt());
        return lowpanCredential;
      }
      
      public LowpanCredential[] newArray(int param1Int) {
        return new LowpanCredential[param1Int];
      }
    };
  
  public static final int UNSPECIFIED_KEY_INDEX = 0;
  
  LowpanCredential() {}
}
