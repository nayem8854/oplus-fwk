package android.net.lowpan;

import android.net.IpPrefix;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ILowpanInterfaceListener extends IInterface {
  void onConnectedChanged(boolean paramBoolean) throws RemoteException;
  
  void onEnabledChanged(boolean paramBoolean) throws RemoteException;
  
  void onLinkAddressAdded(String paramString) throws RemoteException;
  
  void onLinkAddressRemoved(String paramString) throws RemoteException;
  
  void onLinkNetworkAdded(IpPrefix paramIpPrefix) throws RemoteException;
  
  void onLinkNetworkRemoved(IpPrefix paramIpPrefix) throws RemoteException;
  
  void onLowpanIdentityChanged(LowpanIdentity paramLowpanIdentity) throws RemoteException;
  
  void onReceiveFromCommissioner(byte[] paramArrayOfbyte) throws RemoteException;
  
  void onRoleChanged(String paramString) throws RemoteException;
  
  void onStateChanged(String paramString) throws RemoteException;
  
  void onUpChanged(boolean paramBoolean) throws RemoteException;
  
  class Default implements ILowpanInterfaceListener {
    public void onEnabledChanged(boolean param1Boolean) throws RemoteException {}
    
    public void onConnectedChanged(boolean param1Boolean) throws RemoteException {}
    
    public void onUpChanged(boolean param1Boolean) throws RemoteException {}
    
    public void onRoleChanged(String param1String) throws RemoteException {}
    
    public void onStateChanged(String param1String) throws RemoteException {}
    
    public void onLowpanIdentityChanged(LowpanIdentity param1LowpanIdentity) throws RemoteException {}
    
    public void onLinkNetworkAdded(IpPrefix param1IpPrefix) throws RemoteException {}
    
    public void onLinkNetworkRemoved(IpPrefix param1IpPrefix) throws RemoteException {}
    
    public void onLinkAddressAdded(String param1String) throws RemoteException {}
    
    public void onLinkAddressRemoved(String param1String) throws RemoteException {}
    
    public void onReceiveFromCommissioner(byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ILowpanInterfaceListener {
    private static final String DESCRIPTOR = "android.net.lowpan.ILowpanInterfaceListener";
    
    static final int TRANSACTION_onConnectedChanged = 2;
    
    static final int TRANSACTION_onEnabledChanged = 1;
    
    static final int TRANSACTION_onLinkAddressAdded = 9;
    
    static final int TRANSACTION_onLinkAddressRemoved = 10;
    
    static final int TRANSACTION_onLinkNetworkAdded = 7;
    
    static final int TRANSACTION_onLinkNetworkRemoved = 8;
    
    static final int TRANSACTION_onLowpanIdentityChanged = 6;
    
    static final int TRANSACTION_onReceiveFromCommissioner = 11;
    
    static final int TRANSACTION_onRoleChanged = 4;
    
    static final int TRANSACTION_onStateChanged = 5;
    
    static final int TRANSACTION_onUpChanged = 3;
    
    public Stub() {
      attachInterface(this, "android.net.lowpan.ILowpanInterfaceListener");
    }
    
    public static ILowpanInterfaceListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.lowpan.ILowpanInterfaceListener");
      if (iInterface != null && iInterface instanceof ILowpanInterfaceListener)
        return (ILowpanInterfaceListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 11:
          return "onReceiveFromCommissioner";
        case 10:
          return "onLinkAddressRemoved";
        case 9:
          return "onLinkAddressAdded";
        case 8:
          return "onLinkNetworkRemoved";
        case 7:
          return "onLinkNetworkAdded";
        case 6:
          return "onLowpanIdentityChanged";
        case 5:
          return "onStateChanged";
        case 4:
          return "onRoleChanged";
        case 3:
          return "onUpChanged";
        case 2:
          return "onConnectedChanged";
        case 1:
          break;
      } 
      return "onEnabledChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        byte[] arrayOfByte;
        String str;
        boolean bool1 = false, bool2 = false, bool3 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 11:
            param1Parcel1.enforceInterface("android.net.lowpan.ILowpanInterfaceListener");
            arrayOfByte = param1Parcel1.createByteArray();
            onReceiveFromCommissioner(arrayOfByte);
            return true;
          case 10:
            arrayOfByte.enforceInterface("android.net.lowpan.ILowpanInterfaceListener");
            str = arrayOfByte.readString();
            onLinkAddressRemoved(str);
            return true;
          case 9:
            str.enforceInterface("android.net.lowpan.ILowpanInterfaceListener");
            str = str.readString();
            onLinkAddressAdded(str);
            return true;
          case 8:
            str.enforceInterface("android.net.lowpan.ILowpanInterfaceListener");
            if (str.readInt() != 0) {
              IpPrefix ipPrefix = IpPrefix.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            onLinkNetworkRemoved((IpPrefix)str);
            return true;
          case 7:
            str.enforceInterface("android.net.lowpan.ILowpanInterfaceListener");
            if (str.readInt() != 0) {
              IpPrefix ipPrefix = IpPrefix.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            onLinkNetworkAdded((IpPrefix)str);
            return true;
          case 6:
            str.enforceInterface("android.net.lowpan.ILowpanInterfaceListener");
            if (str.readInt() != 0) {
              LowpanIdentity lowpanIdentity = LowpanIdentity.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            onLowpanIdentityChanged((LowpanIdentity)str);
            return true;
          case 5:
            str.enforceInterface("android.net.lowpan.ILowpanInterfaceListener");
            str = str.readString();
            onStateChanged(str);
            return true;
          case 4:
            str.enforceInterface("android.net.lowpan.ILowpanInterfaceListener");
            str = str.readString();
            onRoleChanged(str);
            return true;
          case 3:
            str.enforceInterface("android.net.lowpan.ILowpanInterfaceListener");
            bool2 = bool3;
            if (str.readInt() != 0)
              bool2 = true; 
            onUpChanged(bool2);
            return true;
          case 2:
            str.enforceInterface("android.net.lowpan.ILowpanInterfaceListener");
            bool2 = bool1;
            if (str.readInt() != 0)
              bool2 = true; 
            onConnectedChanged(bool2);
            return true;
          case 1:
            break;
        } 
        str.enforceInterface("android.net.lowpan.ILowpanInterfaceListener");
        if (str.readInt() != 0)
          bool2 = true; 
        onEnabledChanged(bool2);
        return true;
      } 
      param1Parcel2.writeString("android.net.lowpan.ILowpanInterfaceListener");
      return true;
    }
    
    private static class Proxy implements ILowpanInterfaceListener {
      public static ILowpanInterfaceListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.lowpan.ILowpanInterfaceListener";
      }
      
      public void onEnabledChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanInterfaceListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool1 && ILowpanInterfaceListener.Stub.getDefaultImpl() != null) {
            ILowpanInterfaceListener.Stub.getDefaultImpl().onEnabledChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onConnectedChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanInterfaceListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool1 && ILowpanInterfaceListener.Stub.getDefaultImpl() != null) {
            ILowpanInterfaceListener.Stub.getDefaultImpl().onConnectedChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onUpChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanInterfaceListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool1 && ILowpanInterfaceListener.Stub.getDefaultImpl() != null) {
            ILowpanInterfaceListener.Stub.getDefaultImpl().onUpChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRoleChanged(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanInterfaceListener");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && ILowpanInterfaceListener.Stub.getDefaultImpl() != null) {
            ILowpanInterfaceListener.Stub.getDefaultImpl().onRoleChanged(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onStateChanged(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanInterfaceListener");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && ILowpanInterfaceListener.Stub.getDefaultImpl() != null) {
            ILowpanInterfaceListener.Stub.getDefaultImpl().onStateChanged(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onLowpanIdentityChanged(LowpanIdentity param2LowpanIdentity) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanInterfaceListener");
          if (param2LowpanIdentity != null) {
            parcel.writeInt(1);
            param2LowpanIdentity.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && ILowpanInterfaceListener.Stub.getDefaultImpl() != null) {
            ILowpanInterfaceListener.Stub.getDefaultImpl().onLowpanIdentityChanged(param2LowpanIdentity);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onLinkNetworkAdded(IpPrefix param2IpPrefix) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanInterfaceListener");
          if (param2IpPrefix != null) {
            parcel.writeInt(1);
            param2IpPrefix.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && ILowpanInterfaceListener.Stub.getDefaultImpl() != null) {
            ILowpanInterfaceListener.Stub.getDefaultImpl().onLinkNetworkAdded(param2IpPrefix);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onLinkNetworkRemoved(IpPrefix param2IpPrefix) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanInterfaceListener");
          if (param2IpPrefix != null) {
            parcel.writeInt(1);
            param2IpPrefix.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && ILowpanInterfaceListener.Stub.getDefaultImpl() != null) {
            ILowpanInterfaceListener.Stub.getDefaultImpl().onLinkNetworkRemoved(param2IpPrefix);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onLinkAddressAdded(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanInterfaceListener");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(9, parcel, (Parcel)null, 1);
          if (!bool && ILowpanInterfaceListener.Stub.getDefaultImpl() != null) {
            ILowpanInterfaceListener.Stub.getDefaultImpl().onLinkAddressAdded(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onLinkAddressRemoved(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanInterfaceListener");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(10, parcel, (Parcel)null, 1);
          if (!bool && ILowpanInterfaceListener.Stub.getDefaultImpl() != null) {
            ILowpanInterfaceListener.Stub.getDefaultImpl().onLinkAddressRemoved(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onReceiveFromCommissioner(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanInterfaceListener");
          parcel.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(11, parcel, (Parcel)null, 1);
          if (!bool && ILowpanInterfaceListener.Stub.getDefaultImpl() != null) {
            ILowpanInterfaceListener.Stub.getDefaultImpl().onReceiveFromCommissioner(param2ArrayOfbyte);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ILowpanInterfaceListener param1ILowpanInterfaceListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ILowpanInterfaceListener != null) {
          Proxy.sDefaultImpl = param1ILowpanInterfaceListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ILowpanInterfaceListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
