package android.net.lowpan;

import android.os.ServiceSpecificException;
import android.util.AndroidException;

public class LowpanException extends AndroidException {
  public LowpanException() {}
  
  public LowpanException(String paramString) {
    super(paramString);
  }
  
  public LowpanException(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
  }
  
  public LowpanException(Exception paramException) {
    super(paramException);
  }
  
  static LowpanException rethrowFromServiceSpecificException(ServiceSpecificException paramServiceSpecificException) throws LowpanException {
    String str;
    int i = paramServiceSpecificException.errorCode;
    if (i != 2) {
      if (i != 3) {
        if (i != 4) {
          if (i != 7) {
            switch (i) {
              default:
                throw new LowpanRuntimeException(paramServiceSpecificException);
              case 15:
                throw new NetworkAlreadyExistsException(paramServiceSpecificException);
              case 14:
                throw new JoinFailedAtAuthException(paramServiceSpecificException);
              case 13:
                throw new JoinFailedAtScanException(paramServiceSpecificException);
              case 12:
                throw new JoinFailedException(paramServiceSpecificException);
              case 11:
                if (paramServiceSpecificException.getMessage() != null) {
                  str = paramServiceSpecificException.getMessage();
                } else {
                  str = "Feature not supported";
                } 
                throw new LowpanException(str, paramServiceSpecificException);
              case 10:
                break;
            } 
            throw new OperationCanceledException(paramServiceSpecificException);
          } 
          if (paramServiceSpecificException.getMessage() != null) {
            str = paramServiceSpecificException.getMessage();
          } else {
            str = "NCP problem";
          } 
          throw new LowpanRuntimeException(str, paramServiceSpecificException);
        } 
        throw new WrongStateException(paramServiceSpecificException);
      } 
      throw new InterfaceDisabledException(paramServiceSpecificException);
    } 
    if (paramServiceSpecificException.getMessage() != null) {
      str = paramServiceSpecificException.getMessage();
    } else {
      str = "Invalid argument";
    } 
    throw new LowpanRuntimeException(str, paramServiceSpecificException);
  }
}
