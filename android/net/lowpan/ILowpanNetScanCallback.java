package android.net.lowpan;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ILowpanNetScanCallback extends IInterface {
  void onNetScanBeacon(LowpanBeaconInfo paramLowpanBeaconInfo) throws RemoteException;
  
  void onNetScanFinished() throws RemoteException;
  
  class Default implements ILowpanNetScanCallback {
    public void onNetScanBeacon(LowpanBeaconInfo param1LowpanBeaconInfo) throws RemoteException {}
    
    public void onNetScanFinished() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ILowpanNetScanCallback {
    private static final String DESCRIPTOR = "android.net.lowpan.ILowpanNetScanCallback";
    
    static final int TRANSACTION_onNetScanBeacon = 1;
    
    static final int TRANSACTION_onNetScanFinished = 2;
    
    public Stub() {
      attachInterface(this, "android.net.lowpan.ILowpanNetScanCallback");
    }
    
    public static ILowpanNetScanCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.lowpan.ILowpanNetScanCallback");
      if (iInterface != null && iInterface instanceof ILowpanNetScanCallback)
        return (ILowpanNetScanCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onNetScanFinished";
      } 
      return "onNetScanBeacon";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.net.lowpan.ILowpanNetScanCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("android.net.lowpan.ILowpanNetScanCallback");
        onNetScanFinished();
        return true;
      } 
      param1Parcel1.enforceInterface("android.net.lowpan.ILowpanNetScanCallback");
      if (param1Parcel1.readInt() != 0) {
        LowpanBeaconInfo lowpanBeaconInfo = LowpanBeaconInfo.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onNetScanBeacon((LowpanBeaconInfo)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements ILowpanNetScanCallback {
      public static ILowpanNetScanCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.lowpan.ILowpanNetScanCallback";
      }
      
      public void onNetScanBeacon(LowpanBeaconInfo param2LowpanBeaconInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanNetScanCallback");
          if (param2LowpanBeaconInfo != null) {
            parcel.writeInt(1);
            param2LowpanBeaconInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ILowpanNetScanCallback.Stub.getDefaultImpl() != null) {
            ILowpanNetScanCallback.Stub.getDefaultImpl().onNetScanBeacon(param2LowpanBeaconInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onNetScanFinished() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanNetScanCallback");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ILowpanNetScanCallback.Stub.getDefaultImpl() != null) {
            ILowpanNetScanCallback.Stub.getDefaultImpl().onNetScanFinished();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ILowpanNetScanCallback param1ILowpanNetScanCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ILowpanNetScanCallback != null) {
          Proxy.sDefaultImpl = param1ILowpanNetScanCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ILowpanNetScanCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
