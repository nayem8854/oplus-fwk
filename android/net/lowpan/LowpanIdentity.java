package android.net.lowpan;

import android.icu.text.StringPrep;
import android.icu.text.StringPrepParseException;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.android.internal.util.HexDump;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;

public class LowpanIdentity implements Parcelable {
  public static final Parcelable.Creator<LowpanIdentity> CREATOR;
  
  private static final String TAG = LowpanIdentity.class.getSimpleName();
  
  public static final int UNSPECIFIED_CHANNEL = -1;
  
  public static final int UNSPECIFIED_PANID = -1;
  
  private int mChannel;
  
  private boolean mIsNameValid;
  
  private String mName;
  
  private int mPanid;
  
  private byte[] mRawName;
  
  private String mType;
  
  private byte[] mXpanid;
  
  class Builder {
    private static final StringPrep stringPrep = StringPrep.getInstance(8);
    
    final LowpanIdentity mIdentity = new LowpanIdentity();
    
    private static String escape(byte[] param1ArrayOfbyte) {
      StringBuffer stringBuffer = new StringBuffer();
      int i;
      byte b;
      for (i = param1ArrayOfbyte.length, b = 0; b < i; ) {
        byte b1 = param1ArrayOfbyte[b];
        if (b1 >= 32 && b1 <= 126) {
          stringBuffer.append((char)b1);
        } else {
          stringBuffer.append(String.format("\\0x%02x", new Object[] { Integer.valueOf(b1 & 0xFF) }));
        } 
        b++;
      } 
      return stringBuffer.toString();
    }
    
    public Builder setLowpanIdentity(LowpanIdentity param1LowpanIdentity) {
      Objects.requireNonNull(param1LowpanIdentity);
      setRawName(param1LowpanIdentity.getRawName());
      setXpanid(param1LowpanIdentity.getXpanid());
      setPanid(param1LowpanIdentity.getPanid());
      setChannel(param1LowpanIdentity.getChannel());
      setType(param1LowpanIdentity.getType());
      return this;
    }
    
    public Builder setName(String param1String) {
      Objects.requireNonNull(param1String);
      try {
        LowpanIdentity.access$002(this.mIdentity, stringPrep.prepare(param1String, 0));
        LowpanIdentity.access$102(this.mIdentity, this.mIdentity.mName.getBytes(StandardCharsets.UTF_8));
        LowpanIdentity.access$202(this.mIdentity, true);
      } catch (StringPrepParseException stringPrepParseException) {
        Log.w(LowpanIdentity.TAG, stringPrepParseException.toString());
        setRawName(param1String.getBytes(StandardCharsets.UTF_8));
      } 
      return this;
    }
    
    public Builder setRawName(byte[] param1ArrayOfbyte) {
      Objects.requireNonNull(param1ArrayOfbyte);
      LowpanIdentity.access$102(this.mIdentity, (byte[])param1ArrayOfbyte.clone());
      LowpanIdentity.access$002(this.mIdentity, new String(param1ArrayOfbyte, StandardCharsets.UTF_8));
      try {
        String str = stringPrep.prepare(this.mIdentity.mName, 0);
        LowpanIdentity lowpanIdentity = this.mIdentity;
        Charset charset = StandardCharsets.UTF_8;
        boolean bool = Arrays.equals(str.getBytes(charset), param1ArrayOfbyte);
        LowpanIdentity.access$202(lowpanIdentity, bool);
      } catch (StringPrepParseException stringPrepParseException) {
        Log.w(LowpanIdentity.TAG, stringPrepParseException.toString());
        LowpanIdentity.access$202(this.mIdentity, false);
      } 
      if (!this.mIdentity.mIsNameValid) {
        LowpanIdentity lowpanIdentity = this.mIdentity;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("«");
        stringBuilder.append(escape(param1ArrayOfbyte));
        stringBuilder.append("»");
        LowpanIdentity.access$002(lowpanIdentity, stringBuilder.toString());
      } 
      return this;
    }
    
    public Builder setXpanid(byte[] param1ArrayOfbyte) {
      LowpanIdentity lowpanIdentity = this.mIdentity;
      if (param1ArrayOfbyte != null) {
        param1ArrayOfbyte = (byte[])param1ArrayOfbyte.clone();
      } else {
        param1ArrayOfbyte = null;
      } 
      LowpanIdentity.access$402(lowpanIdentity, param1ArrayOfbyte);
      return this;
    }
    
    public Builder setPanid(int param1Int) {
      LowpanIdentity.access$502(this.mIdentity, param1Int);
      return this;
    }
    
    public Builder setType(String param1String) {
      LowpanIdentity.access$602(this.mIdentity, param1String);
      return this;
    }
    
    public Builder setChannel(int param1Int) {
      LowpanIdentity.access$702(this.mIdentity, param1Int);
      return this;
    }
    
    public LowpanIdentity build() {
      return this.mIdentity;
    }
  }
  
  LowpanIdentity() {
    this.mName = "";
    this.mIsNameValid = true;
    this.mRawName = new byte[0];
    this.mType = "";
    this.mXpanid = new byte[0];
    this.mPanid = -1;
    this.mChannel = -1;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public boolean isNameValid() {
    return this.mIsNameValid;
  }
  
  public byte[] getRawName() {
    return (byte[])this.mRawName.clone();
  }
  
  public byte[] getXpanid() {
    return (byte[])this.mXpanid.clone();
  }
  
  public int getPanid() {
    return this.mPanid;
  }
  
  public String getType() {
    return this.mType;
  }
  
  public int getChannel() {
    return this.mChannel;
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append("Name:");
    stringBuffer.append(getName());
    if (this.mType.length() > 0) {
      stringBuffer.append(", Type:");
      stringBuffer.append(this.mType);
    } 
    if (this.mXpanid.length > 0) {
      stringBuffer.append(", XPANID:");
      stringBuffer.append(HexDump.toHexString(this.mXpanid));
    } 
    if (this.mPanid != -1) {
      stringBuffer.append(", PANID:");
      stringBuffer.append(String.format("0x%04X", new Object[] { Integer.valueOf(this.mPanid) }));
    } 
    if (this.mChannel != -1) {
      stringBuffer.append(", Channel:");
      stringBuffer.append(this.mChannel);
    } 
    return stringBuffer.toString();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof LowpanIdentity;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    if (Arrays.equals(this.mRawName, ((LowpanIdentity)paramObject).mRawName)) {
      byte[] arrayOfByte1 = this.mXpanid, arrayOfByte2 = ((LowpanIdentity)paramObject).mXpanid;
      if (Arrays.equals(arrayOfByte1, arrayOfByte2)) {
        String str1 = this.mType, str2 = ((LowpanIdentity)paramObject).mType;
        if (str1.equals(str2) && this.mPanid == ((LowpanIdentity)paramObject).mPanid && this.mChannel == ((LowpanIdentity)paramObject).mChannel)
          bool1 = true; 
      } 
    } 
    return bool1;
  }
  
  public int hashCode() {
    byte[] arrayOfByte = this.mRawName;
    int i = Arrays.hashCode(arrayOfByte);
    String str = this.mType;
    int j = Arrays.hashCode(this.mXpanid), k = this.mPanid, m = this.mChannel;
    return Objects.hash(new Object[] { Integer.valueOf(i), str, Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m) });
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByteArray(this.mRawName);
    paramParcel.writeString(this.mType);
    paramParcel.writeByteArray(this.mXpanid);
    paramParcel.writeInt(this.mPanid);
    paramParcel.writeInt(this.mChannel);
  }
  
  static {
    CREATOR = new Parcelable.Creator<LowpanIdentity>() {
        public LowpanIdentity createFromParcel(Parcel param1Parcel) {
          LowpanIdentity.Builder builder = new LowpanIdentity.Builder();
          builder.setRawName(param1Parcel.createByteArray());
          builder.setType(param1Parcel.readString());
          builder.setXpanid(param1Parcel.createByteArray());
          builder.setPanid(param1Parcel.readInt());
          builder.setChannel(param1Parcel.readInt());
          return builder.build();
        }
        
        public LowpanIdentity[] newArray(int param1Int) {
          return new LowpanIdentity[param1Int];
        }
      };
  }
}
