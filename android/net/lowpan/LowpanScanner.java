package android.net.lowpan;

import android.os.Handler;
import android.os.RemoteException;
import android.os.ServiceSpecificException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.ToIntFunction;

public class LowpanScanner {
  private static final String TAG = LowpanScanner.class.getSimpleName();
  
  public static abstract class Callback {
    public void onNetScanBeacon(LowpanBeaconInfo param1LowpanBeaconInfo) {}
    
    public void onEnergyScanResult(LowpanEnergyScanResult param1LowpanEnergyScanResult) {}
    
    public void onScanFinished() {}
  }
  
  private Callback mCallback = null;
  
  private Handler mHandler = null;
  
  private ArrayList<Integer> mChannelMask = null;
  
  private int mTxPower = Integer.MAX_VALUE;
  
  private ILowpanInterface mBinder;
  
  LowpanScanner(ILowpanInterface paramILowpanInterface) {
    this.mBinder = paramILowpanInterface;
  }
  
  public void setCallback(Callback paramCallback, Handler paramHandler) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: putfield mCallback : Landroid/net/lowpan/LowpanScanner$Callback;
    //   7: aload_0
    //   8: aload_2
    //   9: putfield mHandler : Landroid/os/Handler;
    //   12: aload_0
    //   13: monitorexit
    //   14: return
    //   15: astore_1
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_1
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #73	-> 2
    //   #74	-> 7
    //   #75	-> 12
    //   #72	-> 15
    // Exception table:
    //   from	to	target	type
    //   2	7	15	finally
    //   7	12	15	finally
  }
  
  public void setCallback(Callback paramCallback) {
    setCallback(paramCallback, null);
  }
  
  public void setChannelMask(Collection<Integer> paramCollection) {
    if (paramCollection == null) {
      this.mChannelMask = null;
    } else {
      ArrayList<Integer> arrayList = this.mChannelMask;
      if (arrayList == null) {
        this.mChannelMask = new ArrayList<>();
      } else {
        arrayList.clear();
      } 
      this.mChannelMask.addAll(paramCollection);
    } 
  }
  
  public Collection<Integer> getChannelMask() {
    return (Collection<Integer>)this.mChannelMask.clone();
  }
  
  public void addChannel(int paramInt) {
    if (this.mChannelMask == null)
      this.mChannelMask = new ArrayList<>(); 
    this.mChannelMask.add(Integer.valueOf(paramInt));
  }
  
  public void setTxPower(int paramInt) {
    this.mTxPower = paramInt;
  }
  
  public int getTxPower() {
    return this.mTxPower;
  }
  
  private Map<String, Object> createScanOptionMap() {
    HashMap<Object, Object> hashMap = new HashMap<>();
    if (this.mChannelMask != null) {
      LowpanProperty<int[]> lowpanProperty = LowpanProperties.KEY_CHANNEL_MASK;
      ArrayList<Integer> arrayList = this.mChannelMask;
      int[] arrayOfInt = arrayList.stream().mapToInt((ToIntFunction)_$$Lambda$LowpanScanner$b0nnjTe02JXonssLsm5Kp4EaFqs.INSTANCE).toArray();
      lowpanProperty.putInMap(hashMap, arrayOfInt);
    } 
    if (this.mTxPower != Integer.MAX_VALUE)
      LowpanProperties.KEY_MAX_TX_POWER.putInMap(hashMap, Integer.valueOf(this.mTxPower)); 
    return (Map)hashMap;
  }
  
  public void startNetScan() throws LowpanException {
    Map<String, Object> map = createScanOptionMap();
    Object object = new Object(this);
    try {
      this.mBinder.startNetScan(map, (ILowpanNetScanCallback)object);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw LowpanException.rethrowFromServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public void stopNetScan() {
    try {
      this.mBinder.stopNetScan();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public void startEnergyScan() throws LowpanException {
    Map<String, Object> map = createScanOptionMap();
    Object object = new Object(this);
    try {
      this.mBinder.startEnergyScan(map, (ILowpanEnergyScanCallback)object);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw LowpanException.rethrowFromServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public void stopEnergyScan() {
    try {
      this.mBinder.stopEnergyScan();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
}
