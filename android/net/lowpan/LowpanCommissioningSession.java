package android.net.lowpan;

import android.net.IpPrefix;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;

public class LowpanCommissioningSession {
  private final ILowpanInterfaceListener mInternalCallback = new InternalCallback();
  
  private Callback mCallback = null;
  
  private volatile boolean mIsClosed = false;
  
  private final LowpanBeaconInfo mBeaconInfo;
  
  private final ILowpanInterface mBinder;
  
  private Handler mHandler;
  
  private final Looper mLooper;
  
  public static abstract class Callback {
    public void onReceiveFromCommissioner(byte[] param1ArrayOfbyte) {}
    
    public void onClosed() {}
  }
  
  class InternalCallback extends ILowpanInterfaceListener.Stub {
    final LowpanCommissioningSession this$0;
    
    private InternalCallback() {}
    
    public void onStateChanged(String param1String) {
      if (!LowpanCommissioningSession.this.mIsClosed) {
        byte b = -1;
        int i = param1String.hashCode();
        if (i != -1548612125) {
          if (i == 97204770 && param1String.equals("fault"))
            b = 1; 
        } else if (param1String.equals("offline")) {
          b = 0;
        } 
        if (b == 0 || b == 1)
          synchronized (LowpanCommissioningSession.this) {
            LowpanCommissioningSession.this.lockedCleanup();
          }  
      } 
    }
    
    public void onReceiveFromCommissioner(byte[] param1ArrayOfbyte) {
      LowpanCommissioningSession.this.mHandler.post(new _$$Lambda$LowpanCommissioningSession$InternalCallback$TrrmDykqIWeXNdgrXO7t2_rqCTo(this, param1ArrayOfbyte));
    }
    
    public void onEnabledChanged(boolean param1Boolean) {}
    
    public void onConnectedChanged(boolean param1Boolean) {}
    
    public void onUpChanged(boolean param1Boolean) {}
    
    public void onRoleChanged(String param1String) {}
    
    public void onLowpanIdentityChanged(LowpanIdentity param1LowpanIdentity) {}
    
    public void onLinkNetworkAdded(IpPrefix param1IpPrefix) {}
    
    public void onLinkNetworkRemoved(IpPrefix param1IpPrefix) {}
    
    public void onLinkAddressAdded(String param1String) {}
    
    public void onLinkAddressRemoved(String param1String) {}
  }
  
  LowpanCommissioningSession(ILowpanInterface paramILowpanInterface, LowpanBeaconInfo paramLowpanBeaconInfo, Looper paramLooper) {
    this.mBinder = paramILowpanInterface;
    this.mBeaconInfo = paramLowpanBeaconInfo;
    this.mLooper = paramLooper;
    if (paramLooper != null) {
      this.mHandler = new Handler(this.mLooper);
    } else {
      this.mHandler = new Handler();
    } 
    try {
      this.mBinder.addListener(this.mInternalCallback);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  private void lockedCleanup() {
    if (!this.mIsClosed) {
      try {
        this.mBinder.removeListener(this.mInternalCallback);
      } catch (DeadObjectException deadObjectException) {
      
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      } 
      if (this.mCallback != null)
        this.mHandler.post(new _$$Lambda$LowpanCommissioningSession$jqpl_iUq_e7YuWqkG33P8PNe7Ag(this)); 
    } 
    this.mCallback = null;
    this.mIsClosed = true;
  }
  
  public LowpanBeaconInfo getBeaconInfo() {
    return this.mBeaconInfo;
  }
  
  public void sendToCommissioner(byte[] paramArrayOfbyte) {
    if (!this.mIsClosed)
      try {
        this.mBinder.sendToCommissioner(paramArrayOfbyte);
      } catch (DeadObjectException deadObjectException) {
      
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      }  
  }
  
  public void setCallback(Callback paramCallback, Handler paramHandler) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIsClosed : Z
    //   6: ifne -> 66
    //   9: aload_2
    //   10: ifnull -> 21
    //   13: aload_0
    //   14: aload_2
    //   15: putfield mHandler : Landroid/os/Handler;
    //   18: goto -> 61
    //   21: aload_0
    //   22: getfield mLooper : Landroid/os/Looper;
    //   25: ifnull -> 48
    //   28: new android/os/Handler
    //   31: astore_2
    //   32: aload_2
    //   33: aload_0
    //   34: getfield mLooper : Landroid/os/Looper;
    //   37: invokespecial <init> : (Landroid/os/Looper;)V
    //   40: aload_0
    //   41: aload_2
    //   42: putfield mHandler : Landroid/os/Handler;
    //   45: goto -> 61
    //   48: new android/os/Handler
    //   51: astore_2
    //   52: aload_2
    //   53: invokespecial <init> : ()V
    //   56: aload_0
    //   57: aload_2
    //   58: putfield mHandler : Landroid/os/Handler;
    //   61: aload_0
    //   62: aload_1
    //   63: putfield mCallback : Landroid/net/lowpan/LowpanCommissioningSession$Callback;
    //   66: aload_0
    //   67: monitorexit
    //   68: return
    //   69: astore_1
    //   70: aload_0
    //   71: monitorexit
    //   72: aload_1
    //   73: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #183	-> 2
    //   #194	-> 9
    //   #195	-> 13
    //   #196	-> 21
    //   #197	-> 28
    //   #199	-> 48
    //   #201	-> 61
    //   #203	-> 66
    //   #182	-> 69
    // Exception table:
    //   from	to	target	type
    //   2	9	69	finally
    //   13	18	69	finally
    //   21	28	69	finally
    //   28	45	69	finally
    //   48	61	69	finally
    //   61	66	69	finally
  }
  
  public void close() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIsClosed : Z
    //   6: istore_1
    //   7: iload_1
    //   8: ifne -> 34
    //   11: aload_0
    //   12: getfield mBinder : Landroid/net/lowpan/ILowpanInterface;
    //   15: invokeinterface closeCommissioningSession : ()V
    //   20: aload_0
    //   21: invokespecial lockedCleanup : ()V
    //   24: goto -> 34
    //   27: astore_2
    //   28: aload_2
    //   29: invokevirtual rethrowAsRuntimeException : ()Ljava/lang/RuntimeException;
    //   32: athrow
    //   33: astore_2
    //   34: aload_0
    //   35: monitorexit
    //   36: return
    //   37: astore_2
    //   38: aload_0
    //   39: monitorexit
    //   40: aload_2
    //   41: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #207	-> 2
    //   #209	-> 11
    //   #211	-> 20
    //   #220	-> 24
    //   #218	-> 27
    //   #219	-> 28
    //   #213	-> 33
    //   #222	-> 34
    //   #206	-> 37
    // Exception table:
    //   from	to	target	type
    //   2	7	37	finally
    //   11	20	33	android/os/DeadObjectException
    //   11	20	27	android/os/RemoteException
    //   11	20	37	finally
    //   20	24	33	android/os/DeadObjectException
    //   20	24	27	android/os/RemoteException
    //   20	24	37	finally
    //   28	33	37	finally
  }
}
