package android.net.lowpan;

import android.content.Context;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.os.ServiceManager;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

public class LowpanManager {
  private static final String TAG = LowpanManager.class.getSimpleName();
  
  public static abstract class Callback {
    public void onInterfaceAdded(LowpanInterface param1LowpanInterface) {}
    
    public void onInterfaceRemoved(LowpanInterface param1LowpanInterface) {}
  }
  
  private final Map<Integer, ILowpanManagerListener> mListenerMap = new HashMap<>();
  
  private final Map<String, LowpanInterface> mInterfaceCache = new HashMap<>();
  
  private final Map<IBinder, WeakReference<LowpanInterface>> mBinderCache = new WeakHashMap<>();
  
  private final Context mContext;
  
  private final Looper mLooper;
  
  private final ILowpanManager mService;
  
  public static LowpanManager from(Context paramContext) {
    return (LowpanManager)paramContext.getSystemService("lowpan");
  }
  
  public static LowpanManager getManager() {
    IBinder iBinder = ServiceManager.getService("lowpan");
    if (iBinder != null) {
      ILowpanManager iLowpanManager = ILowpanManager.Stub.asInterface(iBinder);
      return new LowpanManager(iLowpanManager);
    } 
    return null;
  }
  
  LowpanManager(ILowpanManager paramILowpanManager) {
    this.mService = paramILowpanManager;
    this.mContext = null;
    this.mLooper = null;
  }
  
  public LowpanManager(Context paramContext, ILowpanManager paramILowpanManager, Looper paramLooper) {
    this.mContext = paramContext;
    this.mService = paramILowpanManager;
    this.mLooper = paramLooper;
  }
  
  public LowpanInterface getInterfaceNoCreate(ILowpanInterface paramILowpanInterface) {
    LowpanInterface lowpanInterface = null;
    synchronized (this.mBinderCache) {
      if (this.mBinderCache.containsKey(paramILowpanInterface.asBinder()))
        lowpanInterface = ((WeakReference<LowpanInterface>)this.mBinderCache.get(paramILowpanInterface.asBinder())).get(); 
      return lowpanInterface;
    } 
  }
  
  public LowpanInterface getInterface(ILowpanInterface paramILowpanInterface) {
    LowpanInterface lowpanInterface = null;
    try {
      synchronized (this.mBinderCache) {
        if (this.mBinderCache.containsKey(paramILowpanInterface.asBinder()))
          lowpanInterface = ((WeakReference<LowpanInterface>)this.mBinderCache.get(paramILowpanInterface.asBinder())).get(); 
        LowpanInterface lowpanInterface1 = lowpanInterface;
        if (lowpanInterface == null) {
          String str = paramILowpanInterface.getName();
          lowpanInterface = new LowpanInterface();
          this(this.mContext, paramILowpanInterface, this.mLooper);
          synchronized (this.mInterfaceCache) {
            this.mInterfaceCache.put(lowpanInterface.getName(), lowpanInterface);
            Map<IBinder, WeakReference<LowpanInterface>> map = this.mBinderCache;
            IBinder iBinder1 = paramILowpanInterface.asBinder();
            WeakReference<LowpanInterface> weakReference = new WeakReference();
            this((T)lowpanInterface);
            map.put(iBinder1, weakReference);
            IBinder iBinder2 = paramILowpanInterface.asBinder();
            null = new Object();
            super(this, str, paramILowpanInterface);
            iBinder2.linkToDeath((IBinder.DeathRecipient)null, 0);
            lowpanInterface1 = lowpanInterface;
          } 
        } 
        return lowpanInterface1;
      } 
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public LowpanInterface getInterface(String paramString) {
    LowpanInterface lowpanInterface = null;
    try {
      synchronized (this.mInterfaceCache) {
        LowpanInterface lowpanInterface1;
        if (this.mInterfaceCache.containsKey(paramString)) {
          lowpanInterface1 = this.mInterfaceCache.get(paramString);
        } else {
          ILowpanInterface iLowpanInterface = this.mService.getInterface((String)lowpanInterface1);
          lowpanInterface1 = lowpanInterface;
          if (iLowpanInterface != null)
            lowpanInterface1 = getInterface(iLowpanInterface); 
        } 
        return lowpanInterface1;
      } 
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public LowpanInterface getInterface() {
    String[] arrayOfString = getInterfaceList();
    if (arrayOfString.length > 0)
      return getInterface(arrayOfString[0]); 
    return null;
  }
  
  public String[] getInterfaceList() {
    try {
      return this.mService.getInterfaceList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void registerCallback(Callback paramCallback, Handler paramHandler) throws LowpanException {
    Object object = new Object(this, paramHandler, paramCallback);
    try {
      this.mService.addListener((ILowpanManagerListener)object);
      synchronized (this.mListenerMap) {
        this.mListenerMap.put(Integer.valueOf(System.identityHashCode(paramCallback)), object);
        return;
      } 
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void registerCallback(Callback paramCallback) throws LowpanException {
    registerCallback(paramCallback, null);
  }
  
  public void unregisterCallback(Callback paramCallback) {
    Map<Integer, ILowpanManagerListener> map;
    Integer integer = Integer.valueOf(System.identityHashCode(paramCallback));
    synchronized (this.mListenerMap) {
      ILowpanManagerListener iLowpanManagerListener = this.mListenerMap.get(integer);
      this.mListenerMap.remove(integer);
      if (iLowpanManagerListener != null)
        try {
          this.mService.removeListener(iLowpanManagerListener);
          return;
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        }  
      throw new RuntimeException("Attempt to unregister an unknown callback");
    } 
  }
}
