package android.net.lowpan;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public class LowpanProvision implements Parcelable {
  class Builder {
    private final LowpanProvision provision = new LowpanProvision();
    
    public Builder setLowpanIdentity(LowpanIdentity param1LowpanIdentity) {
      LowpanProvision.access$102(this.provision, param1LowpanIdentity);
      return this;
    }
    
    public Builder setLowpanCredential(LowpanCredential param1LowpanCredential) {
      LowpanProvision.access$202(this.provision, param1LowpanCredential);
      return this;
    }
    
    public LowpanProvision build() {
      return this.provision;
    }
  }
  
  private LowpanProvision() {
    this.mIdentity = new LowpanIdentity();
    this.mCredential = null;
  }
  
  public LowpanIdentity getLowpanIdentity() {
    return this.mIdentity;
  }
  
  public LowpanCredential getLowpanCredential() {
    return this.mCredential;
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append("LowpanProvision { identity => ");
    stringBuffer.append(this.mIdentity.toString());
    if (this.mCredential != null) {
      stringBuffer.append(", credential => ");
      stringBuffer.append(this.mCredential.toString());
    } 
    stringBuffer.append("}");
    return stringBuffer.toString();
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mIdentity, this.mCredential });
  }
  
  public boolean equals(Object paramObject) {
    if (!(paramObject instanceof LowpanProvision))
      return false; 
    paramObject = paramObject;
    if (!this.mIdentity.equals(((LowpanProvision)paramObject).mIdentity))
      return false; 
    if (!Objects.equals(this.mCredential, ((LowpanProvision)paramObject).mCredential))
      return false; 
    return true;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.mIdentity.writeToParcel(paramParcel, paramInt);
    if (this.mCredential == null) {
      paramParcel.writeBoolean(false);
    } else {
      paramParcel.writeBoolean(true);
      this.mCredential.writeToParcel(paramParcel, paramInt);
    } 
  }
  
  public static final Parcelable.Creator<LowpanProvision> CREATOR = new Parcelable.Creator<LowpanProvision>() {
      public LowpanProvision createFromParcel(Parcel param1Parcel) {
        LowpanProvision.Builder builder = new LowpanProvision.Builder();
        builder.setLowpanIdentity(LowpanIdentity.CREATOR.createFromParcel(param1Parcel));
        if (param1Parcel.readBoolean())
          builder.setLowpanCredential(LowpanCredential.CREATOR.createFromParcel(param1Parcel)); 
        return builder.build();
      }
      
      public LowpanProvision[] newArray(int param1Int) {
        return new LowpanProvision[param1Int];
      }
    };
  
  private LowpanCredential mCredential;
  
  private LowpanIdentity mIdentity;
}
