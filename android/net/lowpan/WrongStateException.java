package android.net.lowpan;

public class WrongStateException extends LowpanException {
  public WrongStateException() {}
  
  public WrongStateException(String paramString) {
    super(paramString);
  }
  
  public WrongStateException(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
  }
  
  protected WrongStateException(Exception paramException) {
    super(paramException);
  }
}
