package android.net.lowpan;

public class InterfaceDisabledException extends LowpanException {
  public InterfaceDisabledException() {}
  
  public InterfaceDisabledException(String paramString) {
    super(paramString);
  }
  
  public InterfaceDisabledException(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
  }
  
  protected InterfaceDisabledException(Exception paramException) {
    super(paramException);
  }
}
