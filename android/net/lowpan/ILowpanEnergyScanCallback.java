package android.net.lowpan;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ILowpanEnergyScanCallback extends IInterface {
  void onEnergyScanFinished() throws RemoteException;
  
  void onEnergyScanResult(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements ILowpanEnergyScanCallback {
    public void onEnergyScanResult(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onEnergyScanFinished() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ILowpanEnergyScanCallback {
    private static final String DESCRIPTOR = "android.net.lowpan.ILowpanEnergyScanCallback";
    
    static final int TRANSACTION_onEnergyScanFinished = 2;
    
    static final int TRANSACTION_onEnergyScanResult = 1;
    
    public Stub() {
      attachInterface(this, "android.net.lowpan.ILowpanEnergyScanCallback");
    }
    
    public static ILowpanEnergyScanCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.lowpan.ILowpanEnergyScanCallback");
      if (iInterface != null && iInterface instanceof ILowpanEnergyScanCallback)
        return (ILowpanEnergyScanCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onEnergyScanFinished";
      } 
      return "onEnergyScanResult";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.net.lowpan.ILowpanEnergyScanCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("android.net.lowpan.ILowpanEnergyScanCallback");
        onEnergyScanFinished();
        return true;
      } 
      param1Parcel1.enforceInterface("android.net.lowpan.ILowpanEnergyScanCallback");
      param1Int1 = param1Parcel1.readInt();
      param1Int2 = param1Parcel1.readInt();
      onEnergyScanResult(param1Int1, param1Int2);
      return true;
    }
    
    private static class Proxy implements ILowpanEnergyScanCallback {
      public static ILowpanEnergyScanCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.lowpan.ILowpanEnergyScanCallback";
      }
      
      public void onEnergyScanResult(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanEnergyScanCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ILowpanEnergyScanCallback.Stub.getDefaultImpl() != null) {
            ILowpanEnergyScanCallback.Stub.getDefaultImpl().onEnergyScanResult(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onEnergyScanFinished() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanEnergyScanCallback");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ILowpanEnergyScanCallback.Stub.getDefaultImpl() != null) {
            ILowpanEnergyScanCallback.Stub.getDefaultImpl().onEnergyScanFinished();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ILowpanEnergyScanCallback param1ILowpanEnergyScanCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ILowpanEnergyScanCallback != null) {
          Proxy.sDefaultImpl = param1ILowpanEnergyScanCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ILowpanEnergyScanCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
