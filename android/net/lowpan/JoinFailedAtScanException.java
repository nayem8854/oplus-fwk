package android.net.lowpan;

public class JoinFailedAtScanException extends JoinFailedException {
  public JoinFailedAtScanException() {}
  
  public JoinFailedAtScanException(String paramString) {
    super(paramString);
  }
  
  public JoinFailedAtScanException(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
  }
  
  public JoinFailedAtScanException(Exception paramException) {
    super(paramException);
  }
}
