package android.net.lowpan;

public class LowpanEnergyScanResult {
  public static final int UNKNOWN = 2147483647;
  
  private int mChannel = Integer.MAX_VALUE;
  
  private int mMaxRssi = Integer.MAX_VALUE;
  
  public int getChannel() {
    return this.mChannel;
  }
  
  public int getMaxRssi() {
    return this.mMaxRssi;
  }
  
  void setChannel(int paramInt) {
    this.mChannel = paramInt;
  }
  
  void setMaxRssi(int paramInt) {
    this.mMaxRssi = paramInt;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("LowpanEnergyScanResult(channel: ");
    stringBuilder.append(this.mChannel);
    stringBuilder.append(", maxRssi:");
    stringBuilder.append(this.mMaxRssi);
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
}
