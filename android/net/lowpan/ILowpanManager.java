package android.net.lowpan;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ILowpanManager extends IInterface {
  public static final String LOWPAN_SERVICE_NAME = "lowpan";
  
  void addInterface(ILowpanInterface paramILowpanInterface) throws RemoteException;
  
  void addListener(ILowpanManagerListener paramILowpanManagerListener) throws RemoteException;
  
  ILowpanInterface getInterface(String paramString) throws RemoteException;
  
  String[] getInterfaceList() throws RemoteException;
  
  void removeInterface(ILowpanInterface paramILowpanInterface) throws RemoteException;
  
  void removeListener(ILowpanManagerListener paramILowpanManagerListener) throws RemoteException;
  
  class Default implements ILowpanManager {
    public ILowpanInterface getInterface(String param1String) throws RemoteException {
      return null;
    }
    
    public String[] getInterfaceList() throws RemoteException {
      return null;
    }
    
    public void addListener(ILowpanManagerListener param1ILowpanManagerListener) throws RemoteException {}
    
    public void removeListener(ILowpanManagerListener param1ILowpanManagerListener) throws RemoteException {}
    
    public void addInterface(ILowpanInterface param1ILowpanInterface) throws RemoteException {}
    
    public void removeInterface(ILowpanInterface param1ILowpanInterface) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ILowpanManager {
    private static final String DESCRIPTOR = "android.net.lowpan.ILowpanManager";
    
    static final int TRANSACTION_addInterface = 5;
    
    static final int TRANSACTION_addListener = 3;
    
    static final int TRANSACTION_getInterface = 1;
    
    static final int TRANSACTION_getInterfaceList = 2;
    
    static final int TRANSACTION_removeInterface = 6;
    
    static final int TRANSACTION_removeListener = 4;
    
    public Stub() {
      attachInterface(this, "android.net.lowpan.ILowpanManager");
    }
    
    public static ILowpanManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.lowpan.ILowpanManager");
      if (iInterface != null && iInterface instanceof ILowpanManager)
        return (ILowpanManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "removeInterface";
        case 5:
          return "addInterface";
        case 4:
          return "removeListener";
        case 3:
          return "addListener";
        case 2:
          return "getInterfaceList";
        case 1:
          break;
      } 
      return "getInterface";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        ILowpanInterface iLowpanInterface2;
        ILowpanManagerListener iLowpanManagerListener;
        String[] arrayOfString;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("android.net.lowpan.ILowpanManager");
            iLowpanInterface2 = ILowpanInterface.Stub.asInterface(param1Parcel1.readStrongBinder());
            removeInterface(iLowpanInterface2);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            iLowpanInterface2.enforceInterface("android.net.lowpan.ILowpanManager");
            iLowpanInterface2 = ILowpanInterface.Stub.asInterface(iLowpanInterface2.readStrongBinder());
            addInterface(iLowpanInterface2);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            iLowpanInterface2.enforceInterface("android.net.lowpan.ILowpanManager");
            iLowpanManagerListener = ILowpanManagerListener.Stub.asInterface(iLowpanInterface2.readStrongBinder());
            removeListener(iLowpanManagerListener);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            iLowpanManagerListener.enforceInterface("android.net.lowpan.ILowpanManager");
            iLowpanManagerListener = ILowpanManagerListener.Stub.asInterface(iLowpanManagerListener.readStrongBinder());
            addListener(iLowpanManagerListener);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            iLowpanManagerListener.enforceInterface("android.net.lowpan.ILowpanManager");
            arrayOfString = getInterfaceList();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString);
            return true;
          case 1:
            break;
        } 
        arrayOfString.enforceInterface("android.net.lowpan.ILowpanManager");
        String str = arrayOfString.readString();
        ILowpanInterface iLowpanInterface1 = getInterface(str);
        param1Parcel2.writeNoException();
        if (iLowpanInterface1 != null) {
          IBinder iBinder = iLowpanInterface1.asBinder();
        } else {
          iLowpanInterface1 = null;
        } 
        param1Parcel2.writeStrongBinder((IBinder)iLowpanInterface1);
        return true;
      } 
      param1Parcel2.writeString("android.net.lowpan.ILowpanManager");
      return true;
    }
    
    private static class Proxy implements ILowpanManager {
      public static ILowpanManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.lowpan.ILowpanManager";
      }
      
      public ILowpanInterface getInterface(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ILowpanManager.Stub.getDefaultImpl() != null)
            return ILowpanManager.Stub.getDefaultImpl().getInterface(param2String); 
          parcel2.readException();
          return ILowpanInterface.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getInterfaceList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanManager");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ILowpanManager.Stub.getDefaultImpl() != null)
            return ILowpanManager.Stub.getDefaultImpl().getInterfaceList(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addListener(ILowpanManagerListener param2ILowpanManagerListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanManager");
          if (param2ILowpanManagerListener != null) {
            iBinder = param2ILowpanManagerListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ILowpanManager.Stub.getDefaultImpl() != null) {
            ILowpanManager.Stub.getDefaultImpl().addListener(param2ILowpanManagerListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeListener(ILowpanManagerListener param2ILowpanManagerListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanManager");
          if (param2ILowpanManagerListener != null) {
            iBinder = param2ILowpanManagerListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ILowpanManager.Stub.getDefaultImpl() != null) {
            ILowpanManager.Stub.getDefaultImpl().removeListener(param2ILowpanManagerListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addInterface(ILowpanInterface param2ILowpanInterface) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanManager");
          if (param2ILowpanInterface != null) {
            iBinder = param2ILowpanInterface.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && ILowpanManager.Stub.getDefaultImpl() != null) {
            ILowpanManager.Stub.getDefaultImpl().addInterface(param2ILowpanInterface);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeInterface(ILowpanInterface param2ILowpanInterface) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanManager");
          if (param2ILowpanInterface != null) {
            iBinder = param2ILowpanInterface.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && ILowpanManager.Stub.getDefaultImpl() != null) {
            ILowpanManager.Stub.getDefaultImpl().removeInterface(param2ILowpanInterface);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ILowpanManager param1ILowpanManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ILowpanManager != null) {
          Proxy.sDefaultImpl = param1ILowpanManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ILowpanManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
