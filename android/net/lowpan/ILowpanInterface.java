package android.net.lowpan;

import android.net.IpPrefix;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.HashMap;
import java.util.Map;

public interface ILowpanInterface extends IInterface {
  public static final int ERROR_ALREADY = 9;
  
  public static final int ERROR_BUSY = 8;
  
  public static final int ERROR_CANCELED = 10;
  
  public static final int ERROR_DISABLED = 3;
  
  public static final int ERROR_FEATURE_NOT_SUPPORTED = 11;
  
  public static final int ERROR_FORM_FAILED_AT_SCAN = 15;
  
  public static final int ERROR_INVALID_ARGUMENT = 2;
  
  public static final int ERROR_IO_FAILURE = 6;
  
  public static final int ERROR_JOIN_FAILED_AT_AUTH = 14;
  
  public static final int ERROR_JOIN_FAILED_AT_SCAN = 13;
  
  public static final int ERROR_JOIN_FAILED_UNKNOWN = 12;
  
  public static final int ERROR_NCP_PROBLEM = 7;
  
  public static final int ERROR_TIMEOUT = 5;
  
  public static final int ERROR_UNSPECIFIED = 1;
  
  public static final int ERROR_WRONG_STATE = 4;
  
  public static final String KEY_CHANNEL_MASK = "android.net.lowpan.property.CHANNEL_MASK";
  
  public static final String KEY_MAX_TX_POWER = "android.net.lowpan.property.MAX_TX_POWER";
  
  public static final String NETWORK_TYPE_THREAD_V1 = "org.threadgroup.thread.v1";
  
  public static final String NETWORK_TYPE_UNKNOWN = "unknown";
  
  public static final String PERM_ACCESS_LOWPAN_STATE = "android.permission.ACCESS_LOWPAN_STATE";
  
  public static final String PERM_CHANGE_LOWPAN_STATE = "android.permission.CHANGE_LOWPAN_STATE";
  
  public static final String PERM_READ_LOWPAN_CREDENTIAL = "android.permission.READ_LOWPAN_CREDENTIAL";
  
  public static final String ROLE_COORDINATOR = "coordinator";
  
  public static final String ROLE_DETACHED = "detached";
  
  public static final String ROLE_END_DEVICE = "end-device";
  
  public static final String ROLE_LEADER = "leader";
  
  public static final String ROLE_ROUTER = "router";
  
  public static final String ROLE_SLEEPY_END_DEVICE = "sleepy-end-device";
  
  public static final String ROLE_SLEEPY_ROUTER = "sleepy-router";
  
  public static final String STATE_ATTACHED = "attached";
  
  public static final String STATE_ATTACHING = "attaching";
  
  public static final String STATE_COMMISSIONING = "commissioning";
  
  public static final String STATE_FAULT = "fault";
  
  public static final String STATE_OFFLINE = "offline";
  
  void addExternalRoute(IpPrefix paramIpPrefix, int paramInt) throws RemoteException;
  
  void addListener(ILowpanInterfaceListener paramILowpanInterfaceListener) throws RemoteException;
  
  void addOnMeshPrefix(IpPrefix paramIpPrefix, int paramInt) throws RemoteException;
  
  void attach(LowpanProvision paramLowpanProvision) throws RemoteException;
  
  void beginLowPower() throws RemoteException;
  
  void closeCommissioningSession() throws RemoteException;
  
  void form(LowpanProvision paramLowpanProvision) throws RemoteException;
  
  String getDriverVersion() throws RemoteException;
  
  byte[] getExtendedAddress() throws RemoteException;
  
  String[] getLinkAddresses() throws RemoteException;
  
  IpPrefix[] getLinkNetworks() throws RemoteException;
  
  LowpanCredential getLowpanCredential() throws RemoteException;
  
  LowpanIdentity getLowpanIdentity() throws RemoteException;
  
  byte[] getMacAddress() throws RemoteException;
  
  String getName() throws RemoteException;
  
  String getNcpVersion() throws RemoteException;
  
  String getPartitionId() throws RemoteException;
  
  String getRole() throws RemoteException;
  
  String getState() throws RemoteException;
  
  LowpanChannelInfo[] getSupportedChannels() throws RemoteException;
  
  String[] getSupportedNetworkTypes() throws RemoteException;
  
  boolean isCommissioned() throws RemoteException;
  
  boolean isConnected() throws RemoteException;
  
  boolean isEnabled() throws RemoteException;
  
  boolean isUp() throws RemoteException;
  
  void join(LowpanProvision paramLowpanProvision) throws RemoteException;
  
  void leave() throws RemoteException;
  
  void onHostWake() throws RemoteException;
  
  void pollForData() throws RemoteException;
  
  void removeExternalRoute(IpPrefix paramIpPrefix) throws RemoteException;
  
  void removeListener(ILowpanInterfaceListener paramILowpanInterfaceListener) throws RemoteException;
  
  void removeOnMeshPrefix(IpPrefix paramIpPrefix) throws RemoteException;
  
  void reset() throws RemoteException;
  
  void sendToCommissioner(byte[] paramArrayOfbyte) throws RemoteException;
  
  void setEnabled(boolean paramBoolean) throws RemoteException;
  
  void startCommissioningSession(LowpanBeaconInfo paramLowpanBeaconInfo) throws RemoteException;
  
  void startEnergyScan(Map paramMap, ILowpanEnergyScanCallback paramILowpanEnergyScanCallback) throws RemoteException;
  
  void startNetScan(Map paramMap, ILowpanNetScanCallback paramILowpanNetScanCallback) throws RemoteException;
  
  void stopEnergyScan() throws RemoteException;
  
  void stopNetScan() throws RemoteException;
  
  class Default implements ILowpanInterface {
    public String getName() throws RemoteException {
      return null;
    }
    
    public String getNcpVersion() throws RemoteException {
      return null;
    }
    
    public String getDriverVersion() throws RemoteException {
      return null;
    }
    
    public LowpanChannelInfo[] getSupportedChannels() throws RemoteException {
      return null;
    }
    
    public String[] getSupportedNetworkTypes() throws RemoteException {
      return null;
    }
    
    public byte[] getMacAddress() throws RemoteException {
      return null;
    }
    
    public boolean isEnabled() throws RemoteException {
      return false;
    }
    
    public void setEnabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isUp() throws RemoteException {
      return false;
    }
    
    public boolean isCommissioned() throws RemoteException {
      return false;
    }
    
    public boolean isConnected() throws RemoteException {
      return false;
    }
    
    public String getState() throws RemoteException {
      return null;
    }
    
    public String getRole() throws RemoteException {
      return null;
    }
    
    public String getPartitionId() throws RemoteException {
      return null;
    }
    
    public byte[] getExtendedAddress() throws RemoteException {
      return null;
    }
    
    public LowpanIdentity getLowpanIdentity() throws RemoteException {
      return null;
    }
    
    public LowpanCredential getLowpanCredential() throws RemoteException {
      return null;
    }
    
    public String[] getLinkAddresses() throws RemoteException {
      return null;
    }
    
    public IpPrefix[] getLinkNetworks() throws RemoteException {
      return null;
    }
    
    public void join(LowpanProvision param1LowpanProvision) throws RemoteException {}
    
    public void form(LowpanProvision param1LowpanProvision) throws RemoteException {}
    
    public void attach(LowpanProvision param1LowpanProvision) throws RemoteException {}
    
    public void leave() throws RemoteException {}
    
    public void reset() throws RemoteException {}
    
    public void startCommissioningSession(LowpanBeaconInfo param1LowpanBeaconInfo) throws RemoteException {}
    
    public void closeCommissioningSession() throws RemoteException {}
    
    public void sendToCommissioner(byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void beginLowPower() throws RemoteException {}
    
    public void pollForData() throws RemoteException {}
    
    public void onHostWake() throws RemoteException {}
    
    public void addListener(ILowpanInterfaceListener param1ILowpanInterfaceListener) throws RemoteException {}
    
    public void removeListener(ILowpanInterfaceListener param1ILowpanInterfaceListener) throws RemoteException {}
    
    public void startNetScan(Map param1Map, ILowpanNetScanCallback param1ILowpanNetScanCallback) throws RemoteException {}
    
    public void stopNetScan() throws RemoteException {}
    
    public void startEnergyScan(Map param1Map, ILowpanEnergyScanCallback param1ILowpanEnergyScanCallback) throws RemoteException {}
    
    public void stopEnergyScan() throws RemoteException {}
    
    public void addOnMeshPrefix(IpPrefix param1IpPrefix, int param1Int) throws RemoteException {}
    
    public void removeOnMeshPrefix(IpPrefix param1IpPrefix) throws RemoteException {}
    
    public void addExternalRoute(IpPrefix param1IpPrefix, int param1Int) throws RemoteException {}
    
    public void removeExternalRoute(IpPrefix param1IpPrefix) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ILowpanInterface {
    private static final String DESCRIPTOR = "android.net.lowpan.ILowpanInterface";
    
    static final int TRANSACTION_addExternalRoute = 39;
    
    static final int TRANSACTION_addListener = 31;
    
    static final int TRANSACTION_addOnMeshPrefix = 37;
    
    static final int TRANSACTION_attach = 22;
    
    static final int TRANSACTION_beginLowPower = 28;
    
    static final int TRANSACTION_closeCommissioningSession = 26;
    
    static final int TRANSACTION_form = 21;
    
    static final int TRANSACTION_getDriverVersion = 3;
    
    static final int TRANSACTION_getExtendedAddress = 15;
    
    static final int TRANSACTION_getLinkAddresses = 18;
    
    static final int TRANSACTION_getLinkNetworks = 19;
    
    static final int TRANSACTION_getLowpanCredential = 17;
    
    static final int TRANSACTION_getLowpanIdentity = 16;
    
    static final int TRANSACTION_getMacAddress = 6;
    
    static final int TRANSACTION_getName = 1;
    
    static final int TRANSACTION_getNcpVersion = 2;
    
    static final int TRANSACTION_getPartitionId = 14;
    
    static final int TRANSACTION_getRole = 13;
    
    static final int TRANSACTION_getState = 12;
    
    static final int TRANSACTION_getSupportedChannels = 4;
    
    static final int TRANSACTION_getSupportedNetworkTypes = 5;
    
    static final int TRANSACTION_isCommissioned = 10;
    
    static final int TRANSACTION_isConnected = 11;
    
    static final int TRANSACTION_isEnabled = 7;
    
    static final int TRANSACTION_isUp = 9;
    
    static final int TRANSACTION_join = 20;
    
    static final int TRANSACTION_leave = 23;
    
    static final int TRANSACTION_onHostWake = 30;
    
    static final int TRANSACTION_pollForData = 29;
    
    static final int TRANSACTION_removeExternalRoute = 40;
    
    static final int TRANSACTION_removeListener = 32;
    
    static final int TRANSACTION_removeOnMeshPrefix = 38;
    
    static final int TRANSACTION_reset = 24;
    
    static final int TRANSACTION_sendToCommissioner = 27;
    
    static final int TRANSACTION_setEnabled = 8;
    
    static final int TRANSACTION_startCommissioningSession = 25;
    
    static final int TRANSACTION_startEnergyScan = 35;
    
    static final int TRANSACTION_startNetScan = 33;
    
    static final int TRANSACTION_stopEnergyScan = 36;
    
    static final int TRANSACTION_stopNetScan = 34;
    
    public Stub() {
      attachInterface(this, "android.net.lowpan.ILowpanInterface");
    }
    
    public static ILowpanInterface asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.lowpan.ILowpanInterface");
      if (iInterface != null && iInterface instanceof ILowpanInterface)
        return (ILowpanInterface)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 40:
          return "removeExternalRoute";
        case 39:
          return "addExternalRoute";
        case 38:
          return "removeOnMeshPrefix";
        case 37:
          return "addOnMeshPrefix";
        case 36:
          return "stopEnergyScan";
        case 35:
          return "startEnergyScan";
        case 34:
          return "stopNetScan";
        case 33:
          return "startNetScan";
        case 32:
          return "removeListener";
        case 31:
          return "addListener";
        case 30:
          return "onHostWake";
        case 29:
          return "pollForData";
        case 28:
          return "beginLowPower";
        case 27:
          return "sendToCommissioner";
        case 26:
          return "closeCommissioningSession";
        case 25:
          return "startCommissioningSession";
        case 24:
          return "reset";
        case 23:
          return "leave";
        case 22:
          return "attach";
        case 21:
          return "form";
        case 20:
          return "join";
        case 19:
          return "getLinkNetworks";
        case 18:
          return "getLinkAddresses";
        case 17:
          return "getLowpanCredential";
        case 16:
          return "getLowpanIdentity";
        case 15:
          return "getExtendedAddress";
        case 14:
          return "getPartitionId";
        case 13:
          return "getRole";
        case 12:
          return "getState";
        case 11:
          return "isConnected";
        case 10:
          return "isCommissioned";
        case 9:
          return "isUp";
        case 8:
          return "setEnabled";
        case 7:
          return "isEnabled";
        case 6:
          return "getMacAddress";
        case 5:
          return "getSupportedNetworkTypes";
        case 4:
          return "getSupportedChannels";
        case 3:
          return "getDriverVersion";
        case 2:
          return "getNcpVersion";
        case 1:
          break;
      } 
      return "getName";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        ILowpanEnergyScanCallback iLowpanEnergyScanCallback;
        ILowpanNetScanCallback iLowpanNetScanCallback;
        ILowpanInterfaceListener iLowpanInterfaceListener;
        byte[] arrayOfByte3;
        IpPrefix[] arrayOfIpPrefix;
        String[] arrayOfString2;
        LowpanCredential lowpanCredential;
        LowpanIdentity lowpanIdentity;
        byte[] arrayOfByte2;
        String str2;
        byte[] arrayOfByte1;
        String[] arrayOfString1;
        LowpanChannelInfo[] arrayOfLowpanChannelInfo;
        IpPrefix ipPrefix;
        ClassLoader classLoader2;
        HashMap hashMap2;
        ClassLoader classLoader1;
        HashMap hashMap1;
        boolean bool1 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 40:
            param1Parcel1.enforceInterface("android.net.lowpan.ILowpanInterface");
            if (param1Parcel1.readInt() != 0) {
              IpPrefix ipPrefix1 = IpPrefix.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            removeExternalRoute((IpPrefix)param1Parcel1);
            return true;
          case 39:
            param1Parcel1.enforceInterface("android.net.lowpan.ILowpanInterface");
            if (param1Parcel1.readInt() != 0) {
              ipPrefix = IpPrefix.CREATOR.createFromParcel(param1Parcel1);
            } else {
              ipPrefix = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            addExternalRoute(ipPrefix, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 38:
            param1Parcel1.enforceInterface("android.net.lowpan.ILowpanInterface");
            if (param1Parcel1.readInt() != 0) {
              IpPrefix ipPrefix1 = IpPrefix.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            removeOnMeshPrefix((IpPrefix)param1Parcel1);
            return true;
          case 37:
            param1Parcel1.enforceInterface("android.net.lowpan.ILowpanInterface");
            if (param1Parcel1.readInt() != 0) {
              ipPrefix = IpPrefix.CREATOR.createFromParcel(param1Parcel1);
            } else {
              ipPrefix = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            addOnMeshPrefix(ipPrefix, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 36:
            param1Parcel1.enforceInterface("android.net.lowpan.ILowpanInterface");
            stopEnergyScan();
            return true;
          case 35:
            param1Parcel1.enforceInterface("android.net.lowpan.ILowpanInterface");
            classLoader2 = getClass().getClassLoader();
            hashMap2 = param1Parcel1.readHashMap(classLoader2);
            iLowpanEnergyScanCallback = ILowpanEnergyScanCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            startEnergyScan(hashMap2, iLowpanEnergyScanCallback);
            param1Parcel2.writeNoException();
            return true;
          case 34:
            iLowpanEnergyScanCallback.enforceInterface("android.net.lowpan.ILowpanInterface");
            stopNetScan();
            return true;
          case 33:
            iLowpanEnergyScanCallback.enforceInterface("android.net.lowpan.ILowpanInterface");
            classLoader1 = getClass().getClassLoader();
            hashMap1 = iLowpanEnergyScanCallback.readHashMap(classLoader1);
            iLowpanNetScanCallback = ILowpanNetScanCallback.Stub.asInterface(iLowpanEnergyScanCallback.readStrongBinder());
            startNetScan(hashMap1, iLowpanNetScanCallback);
            param1Parcel2.writeNoException();
            return true;
          case 32:
            iLowpanNetScanCallback.enforceInterface("android.net.lowpan.ILowpanInterface");
            iLowpanInterfaceListener = ILowpanInterfaceListener.Stub.asInterface(iLowpanNetScanCallback.readStrongBinder());
            removeListener(iLowpanInterfaceListener);
            return true;
          case 31:
            iLowpanInterfaceListener.enforceInterface("android.net.lowpan.ILowpanInterface");
            iLowpanInterfaceListener = ILowpanInterfaceListener.Stub.asInterface(iLowpanInterfaceListener.readStrongBinder());
            addListener(iLowpanInterfaceListener);
            param1Parcel2.writeNoException();
            return true;
          case 30:
            iLowpanInterfaceListener.enforceInterface("android.net.lowpan.ILowpanInterface");
            onHostWake();
            return true;
          case 29:
            iLowpanInterfaceListener.enforceInterface("android.net.lowpan.ILowpanInterface");
            pollForData();
            return true;
          case 28:
            iLowpanInterfaceListener.enforceInterface("android.net.lowpan.ILowpanInterface");
            beginLowPower();
            param1Parcel2.writeNoException();
            return true;
          case 27:
            iLowpanInterfaceListener.enforceInterface("android.net.lowpan.ILowpanInterface");
            arrayOfByte3 = iLowpanInterfaceListener.createByteArray();
            sendToCommissioner(arrayOfByte3);
            return true;
          case 26:
            arrayOfByte3.enforceInterface("android.net.lowpan.ILowpanInterface");
            closeCommissioningSession();
            param1Parcel2.writeNoException();
            return true;
          case 25:
            arrayOfByte3.enforceInterface("android.net.lowpan.ILowpanInterface");
            if (arrayOfByte3.readInt() != 0) {
              LowpanBeaconInfo lowpanBeaconInfo = LowpanBeaconInfo.CREATOR.createFromParcel((Parcel)arrayOfByte3);
            } else {
              arrayOfByte3 = null;
            } 
            startCommissioningSession((LowpanBeaconInfo)arrayOfByte3);
            param1Parcel2.writeNoException();
            return true;
          case 24:
            arrayOfByte3.enforceInterface("android.net.lowpan.ILowpanInterface");
            reset();
            param1Parcel2.writeNoException();
            return true;
          case 23:
            arrayOfByte3.enforceInterface("android.net.lowpan.ILowpanInterface");
            leave();
            param1Parcel2.writeNoException();
            return true;
          case 22:
            arrayOfByte3.enforceInterface("android.net.lowpan.ILowpanInterface");
            if (arrayOfByte3.readInt() != 0) {
              LowpanProvision lowpanProvision = LowpanProvision.CREATOR.createFromParcel((Parcel)arrayOfByte3);
            } else {
              arrayOfByte3 = null;
            } 
            attach((LowpanProvision)arrayOfByte3);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            arrayOfByte3.enforceInterface("android.net.lowpan.ILowpanInterface");
            if (arrayOfByte3.readInt() != 0) {
              LowpanProvision lowpanProvision = LowpanProvision.CREATOR.createFromParcel((Parcel)arrayOfByte3);
            } else {
              arrayOfByte3 = null;
            } 
            form((LowpanProvision)arrayOfByte3);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            arrayOfByte3.enforceInterface("android.net.lowpan.ILowpanInterface");
            if (arrayOfByte3.readInt() != 0) {
              LowpanProvision lowpanProvision = LowpanProvision.CREATOR.createFromParcel((Parcel)arrayOfByte3);
            } else {
              arrayOfByte3 = null;
            } 
            join((LowpanProvision)arrayOfByte3);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            arrayOfByte3.enforceInterface("android.net.lowpan.ILowpanInterface");
            arrayOfIpPrefix = getLinkNetworks();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray(arrayOfIpPrefix, 1);
            return true;
          case 18:
            arrayOfIpPrefix.enforceInterface("android.net.lowpan.ILowpanInterface");
            arrayOfString2 = getLinkAddresses();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString2);
            return true;
          case 17:
            arrayOfString2.enforceInterface("android.net.lowpan.ILowpanInterface");
            lowpanCredential = getLowpanCredential();
            param1Parcel2.writeNoException();
            if (lowpanCredential != null) {
              param1Parcel2.writeInt(1);
              lowpanCredential.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 16:
            lowpanCredential.enforceInterface("android.net.lowpan.ILowpanInterface");
            lowpanIdentity = getLowpanIdentity();
            param1Parcel2.writeNoException();
            if (lowpanIdentity != null) {
              param1Parcel2.writeInt(1);
              lowpanIdentity.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 15:
            lowpanIdentity.enforceInterface("android.net.lowpan.ILowpanInterface");
            arrayOfByte2 = getExtendedAddress();
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte2);
            return true;
          case 14:
            arrayOfByte2.enforceInterface("android.net.lowpan.ILowpanInterface");
            str2 = getPartitionId();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 13:
            str2.enforceInterface("android.net.lowpan.ILowpanInterface");
            str2 = getRole();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 12:
            str2.enforceInterface("android.net.lowpan.ILowpanInterface");
            str2 = getState();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 11:
            str2.enforceInterface("android.net.lowpan.ILowpanInterface");
            bool = isConnected();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 10:
            str2.enforceInterface("android.net.lowpan.ILowpanInterface");
            bool = isCommissioned();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 9:
            str2.enforceInterface("android.net.lowpan.ILowpanInterface");
            bool = isUp();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 8:
            str2.enforceInterface("android.net.lowpan.ILowpanInterface");
            if (str2.readInt() != 0)
              bool1 = true; 
            setEnabled(bool1);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            str2.enforceInterface("android.net.lowpan.ILowpanInterface");
            bool = isEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 6:
            str2.enforceInterface("android.net.lowpan.ILowpanInterface");
            arrayOfByte1 = getMacAddress();
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte1);
            return true;
          case 5:
            arrayOfByte1.enforceInterface("android.net.lowpan.ILowpanInterface");
            arrayOfString1 = getSupportedNetworkTypes();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString1);
            return true;
          case 4:
            arrayOfString1.enforceInterface("android.net.lowpan.ILowpanInterface");
            arrayOfLowpanChannelInfo = getSupportedChannels();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray(arrayOfLowpanChannelInfo, 1);
            return true;
          case 3:
            arrayOfLowpanChannelInfo.enforceInterface("android.net.lowpan.ILowpanInterface");
            str1 = getDriverVersion();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 2:
            str1.enforceInterface("android.net.lowpan.ILowpanInterface");
            str1 = getNcpVersion();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.net.lowpan.ILowpanInterface");
        String str1 = getName();
        param1Parcel2.writeNoException();
        param1Parcel2.writeString(str1);
        return true;
      } 
      param1Parcel2.writeString("android.net.lowpan.ILowpanInterface");
      return true;
    }
    
    private static class Proxy implements ILowpanInterface {
      public static ILowpanInterface sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.lowpan.ILowpanInterface";
      }
      
      public String getName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null)
            return ILowpanInterface.Stub.getDefaultImpl().getName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getNcpVersion() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null)
            return ILowpanInterface.Stub.getDefaultImpl().getNcpVersion(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDriverVersion() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null)
            return ILowpanInterface.Stub.getDefaultImpl().getDriverVersion(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public LowpanChannelInfo[] getSupportedChannels() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null)
            return ILowpanInterface.Stub.getDefaultImpl().getSupportedChannels(); 
          parcel2.readException();
          return parcel2.<LowpanChannelInfo>createTypedArray(LowpanChannelInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getSupportedNetworkTypes() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null)
            return ILowpanInterface.Stub.getDefaultImpl().getSupportedNetworkTypes(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getMacAddress() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null)
            return ILowpanInterface.Stub.getDefaultImpl().getMacAddress(); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && ILowpanInterface.Stub.getDefaultImpl() != null) {
            bool1 = ILowpanInterface.Stub.getDefaultImpl().isEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool1 && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().setEnabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUp() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && ILowpanInterface.Stub.getDefaultImpl() != null) {
            bool1 = ILowpanInterface.Stub.getDefaultImpl().isUp();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isCommissioned() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
          if (!bool2 && ILowpanInterface.Stub.getDefaultImpl() != null) {
            bool1 = ILowpanInterface.Stub.getDefaultImpl().isCommissioned();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isConnected() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(11, parcel1, parcel2, 0);
          if (!bool2 && ILowpanInterface.Stub.getDefaultImpl() != null) {
            bool1 = ILowpanInterface.Stub.getDefaultImpl().isConnected();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null)
            return ILowpanInterface.Stub.getDefaultImpl().getState(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getRole() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null)
            return ILowpanInterface.Stub.getDefaultImpl().getRole(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getPartitionId() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null)
            return ILowpanInterface.Stub.getDefaultImpl().getPartitionId(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getExtendedAddress() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null)
            return ILowpanInterface.Stub.getDefaultImpl().getExtendedAddress(); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public LowpanIdentity getLowpanIdentity() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          LowpanIdentity lowpanIdentity;
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            lowpanIdentity = ILowpanInterface.Stub.getDefaultImpl().getLowpanIdentity();
            return lowpanIdentity;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            lowpanIdentity = LowpanIdentity.CREATOR.createFromParcel(parcel2);
          } else {
            lowpanIdentity = null;
          } 
          return lowpanIdentity;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public LowpanCredential getLowpanCredential() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          LowpanCredential lowpanCredential;
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            lowpanCredential = ILowpanInterface.Stub.getDefaultImpl().getLowpanCredential();
            return lowpanCredential;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            lowpanCredential = LowpanCredential.CREATOR.createFromParcel(parcel2);
          } else {
            lowpanCredential = null;
          } 
          return lowpanCredential;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getLinkAddresses() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null)
            return ILowpanInterface.Stub.getDefaultImpl().getLinkAddresses(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IpPrefix[] getLinkNetworks() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null)
            return ILowpanInterface.Stub.getDefaultImpl().getLinkNetworks(); 
          parcel2.readException();
          return parcel2.<IpPrefix>createTypedArray(IpPrefix.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void join(LowpanProvision param2LowpanProvision) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          if (param2LowpanProvision != null) {
            parcel1.writeInt(1);
            param2LowpanProvision.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().join(param2LowpanProvision);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void form(LowpanProvision param2LowpanProvision) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          if (param2LowpanProvision != null) {
            parcel1.writeInt(1);
            param2LowpanProvision.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().form(param2LowpanProvision);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void attach(LowpanProvision param2LowpanProvision) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          if (param2LowpanProvision != null) {
            parcel1.writeInt(1);
            param2LowpanProvision.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().attach(param2LowpanProvision);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void leave() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().leave();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reset() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().reset();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startCommissioningSession(LowpanBeaconInfo param2LowpanBeaconInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          if (param2LowpanBeaconInfo != null) {
            parcel1.writeInt(1);
            param2LowpanBeaconInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().startCommissioningSession(param2LowpanBeaconInfo);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void closeCommissioningSession() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().closeCommissioningSession();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendToCommissioner(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          parcel.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(27, parcel, (Parcel)null, 1);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().sendToCommissioner(param2ArrayOfbyte);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void beginLowPower() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().beginLowPower();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void pollForData() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(29, parcel, (Parcel)null, 1);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().pollForData();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onHostWake() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(30, parcel, (Parcel)null, 1);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().onHostWake();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void addListener(ILowpanInterfaceListener param2ILowpanInterfaceListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          if (param2ILowpanInterfaceListener != null) {
            iBinder = param2ILowpanInterfaceListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().addListener(param2ILowpanInterfaceListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeListener(ILowpanInterfaceListener param2ILowpanInterfaceListener) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          if (param2ILowpanInterfaceListener != null) {
            iBinder = param2ILowpanInterfaceListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(32, parcel, (Parcel)null, 1);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().removeListener(param2ILowpanInterfaceListener);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void startNetScan(Map param2Map, ILowpanNetScanCallback param2ILowpanNetScanCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          parcel1.writeMap(param2Map);
          if (param2ILowpanNetScanCallback != null) {
            iBinder = param2ILowpanNetScanCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().startNetScan(param2Map, param2ILowpanNetScanCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopNetScan() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(34, parcel, (Parcel)null, 1);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().stopNetScan();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void startEnergyScan(Map param2Map, ILowpanEnergyScanCallback param2ILowpanEnergyScanCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          parcel1.writeMap(param2Map);
          if (param2ILowpanEnergyScanCallback != null) {
            iBinder = param2ILowpanEnergyScanCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().startEnergyScan(param2Map, param2ILowpanEnergyScanCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopEnergyScan() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          boolean bool = this.mRemote.transact(36, parcel, (Parcel)null, 1);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().stopEnergyScan();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void addOnMeshPrefix(IpPrefix param2IpPrefix, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          if (param2IpPrefix != null) {
            parcel1.writeInt(1);
            param2IpPrefix.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().addOnMeshPrefix(param2IpPrefix, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeOnMeshPrefix(IpPrefix param2IpPrefix) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          if (param2IpPrefix != null) {
            parcel.writeInt(1);
            param2IpPrefix.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(38, parcel, (Parcel)null, 1);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().removeOnMeshPrefix(param2IpPrefix);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void addExternalRoute(IpPrefix param2IpPrefix, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          if (param2IpPrefix != null) {
            parcel1.writeInt(1);
            param2IpPrefix.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().addExternalRoute(param2IpPrefix, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeExternalRoute(IpPrefix param2IpPrefix) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.lowpan.ILowpanInterface");
          if (param2IpPrefix != null) {
            parcel.writeInt(1);
            param2IpPrefix.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(40, parcel, (Parcel)null, 1);
          if (!bool && ILowpanInterface.Stub.getDefaultImpl() != null) {
            ILowpanInterface.Stub.getDefaultImpl().removeExternalRoute(param2IpPrefix);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ILowpanInterface param1ILowpanInterface) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ILowpanInterface != null) {
          Proxy.sDefaultImpl = param1ILowpanInterface;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ILowpanInterface getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
