package android.net.lowpan;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.HexDump;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.TreeSet;

public class LowpanBeaconInfo implements Parcelable {
  private int mRssi = Integer.MAX_VALUE;
  
  private int mLqi = 0;
  
  private byte[] mBeaconAddress = null;
  
  private final TreeSet<Integer> mFlags = new TreeSet<>();
  
  class Builder {
    final LowpanIdentity.Builder mIdentityBuilder = new LowpanIdentity.Builder();
    
    final LowpanBeaconInfo mBeaconInfo = new LowpanBeaconInfo();
    
    public Builder setLowpanIdentity(LowpanIdentity param1LowpanIdentity) {
      this.mIdentityBuilder.setLowpanIdentity(param1LowpanIdentity);
      return this;
    }
    
    public Builder setName(String param1String) {
      this.mIdentityBuilder.setName(param1String);
      return this;
    }
    
    public Builder setXpanid(byte[] param1ArrayOfbyte) {
      this.mIdentityBuilder.setXpanid(param1ArrayOfbyte);
      return this;
    }
    
    public Builder setPanid(int param1Int) {
      this.mIdentityBuilder.setPanid(param1Int);
      return this;
    }
    
    public Builder setChannel(int param1Int) {
      this.mIdentityBuilder.setChannel(param1Int);
      return this;
    }
    
    public Builder setType(String param1String) {
      this.mIdentityBuilder.setType(param1String);
      return this;
    }
    
    public Builder setRssi(int param1Int) {
      LowpanBeaconInfo.access$102(this.mBeaconInfo, param1Int);
      return this;
    }
    
    public Builder setLqi(int param1Int) {
      LowpanBeaconInfo.access$202(this.mBeaconInfo, param1Int);
      return this;
    }
    
    public Builder setBeaconAddress(byte[] param1ArrayOfbyte) {
      LowpanBeaconInfo lowpanBeaconInfo = this.mBeaconInfo;
      if (param1ArrayOfbyte != null) {
        param1ArrayOfbyte = (byte[])param1ArrayOfbyte.clone();
      } else {
        param1ArrayOfbyte = null;
      } 
      LowpanBeaconInfo.access$302(lowpanBeaconInfo, param1ArrayOfbyte);
      return this;
    }
    
    public Builder setFlag(int param1Int) {
      this.mBeaconInfo.mFlags.add(Integer.valueOf(param1Int));
      return this;
    }
    
    public Builder setFlags(Collection<Integer> param1Collection) {
      this.mBeaconInfo.mFlags.addAll(param1Collection);
      return this;
    }
    
    public LowpanBeaconInfo build() {
      LowpanBeaconInfo.access$502(this.mBeaconInfo, this.mIdentityBuilder.build());
      if (this.mBeaconInfo.mBeaconAddress == null)
        LowpanBeaconInfo.access$302(this.mBeaconInfo, new byte[0]); 
      return this.mBeaconInfo;
    }
  }
  
  public LowpanIdentity getLowpanIdentity() {
    return this.mIdentity;
  }
  
  public int getRssi() {
    return this.mRssi;
  }
  
  public int getLqi() {
    return this.mLqi;
  }
  
  public byte[] getBeaconAddress() {
    return (byte[])this.mBeaconAddress.clone();
  }
  
  public Collection<Integer> getFlags() {
    return (Collection<Integer>)this.mFlags.clone();
  }
  
  public boolean isFlagSet(int paramInt) {
    return this.mFlags.contains(Integer.valueOf(paramInt));
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(this.mIdentity.toString());
    if (this.mRssi != Integer.MAX_VALUE) {
      stringBuffer.append(", RSSI:");
      stringBuffer.append(this.mRssi);
      stringBuffer.append("dBm");
    } 
    if (this.mLqi != 0) {
      stringBuffer.append(", LQI:");
      stringBuffer.append(this.mLqi);
    } 
    if (this.mBeaconAddress.length > 0) {
      stringBuffer.append(", BeaconAddress:");
      stringBuffer.append(HexDump.toHexString(this.mBeaconAddress));
    } 
    for (Integer integer : this.mFlags) {
      if (integer.intValue() != 1) {
        stringBuffer.append(", FLAG_");
        stringBuffer.append(Integer.toHexString(integer.intValue()));
        continue;
      } 
      stringBuffer.append(", CAN_ASSIST");
    } 
    return stringBuffer.toString();
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mIdentity, Integer.valueOf(this.mRssi), Integer.valueOf(this.mLqi), Integer.valueOf(Arrays.hashCode(this.mBeaconAddress)), this.mFlags });
  }
  
  public boolean equals(Object<Integer> paramObject) {
    boolean bool = paramObject instanceof LowpanBeaconInfo;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    if (this.mIdentity.equals(((LowpanBeaconInfo)paramObject).mIdentity)) {
      byte[] arrayOfByte1 = this.mBeaconAddress, arrayOfByte2 = ((LowpanBeaconInfo)paramObject).mBeaconAddress;
      if (Arrays.equals(arrayOfByte1, arrayOfByte2) && this.mRssi == ((LowpanBeaconInfo)paramObject).mRssi && this.mLqi == ((LowpanBeaconInfo)paramObject).mLqi) {
        TreeSet<Integer> treeSet = this.mFlags;
        paramObject = (Object<Integer>)((LowpanBeaconInfo)paramObject).mFlags;
        if (treeSet.equals(paramObject))
          bool1 = true; 
      } 
    } 
    return bool1;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.mIdentity.writeToParcel(paramParcel, paramInt);
    paramParcel.writeInt(this.mRssi);
    paramParcel.writeInt(this.mLqi);
    paramParcel.writeByteArray(this.mBeaconAddress);
    paramParcel.writeInt(this.mFlags.size());
    for (Integer integer : this.mFlags)
      paramParcel.writeInt(integer.intValue()); 
  }
  
  public static final Parcelable.Creator<LowpanBeaconInfo> CREATOR = new Parcelable.Creator<LowpanBeaconInfo>() {
      public LowpanBeaconInfo createFromParcel(Parcel param1Parcel) {
        LowpanBeaconInfo.Builder builder = new LowpanBeaconInfo.Builder();
        builder.setLowpanIdentity(LowpanIdentity.CREATOR.createFromParcel(param1Parcel));
        builder.setRssi(param1Parcel.readInt());
        builder.setLqi(param1Parcel.readInt());
        builder.setBeaconAddress(param1Parcel.createByteArray());
        for (int i = param1Parcel.readInt(); i > 0; i--)
          builder.setFlag(param1Parcel.readInt()); 
        return builder.build();
      }
      
      public LowpanBeaconInfo[] newArray(int param1Int) {
        return new LowpanBeaconInfo[param1Int];
      }
    };
  
  public static final int FLAG_CAN_ASSIST = 1;
  
  public static final int UNKNOWN_LQI = 0;
  
  public static final int UNKNOWN_RSSI = 2147483647;
  
  private LowpanIdentity mIdentity;
  
  private LowpanBeaconInfo() {}
}
