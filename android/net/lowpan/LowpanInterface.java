package android.net.lowpan;

import android.content.Context;
import android.net.IpPrefix;
import android.net.LinkAddress;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.os.ServiceSpecificException;
import android.util.Log;
import java.util.HashMap;

public class LowpanInterface {
  public static final String EMPTY_PARTITION_ID = "";
  
  public static final String NETWORK_TYPE_THREAD_V1 = "org.threadgroup.thread.v1";
  
  public static final String ROLE_COORDINATOR = "coordinator";
  
  public static final String ROLE_DETACHED = "detached";
  
  public static final String ROLE_END_DEVICE = "end-device";
  
  public static final String ROLE_LEADER = "leader";
  
  public static final String ROLE_ROUTER = "router";
  
  public static final String ROLE_SLEEPY_END_DEVICE = "sleepy-end-device";
  
  public static final String ROLE_SLEEPY_ROUTER = "sleepy-router";
  
  public static final String STATE_ATTACHED = "attached";
  
  public static final String STATE_ATTACHING = "attaching";
  
  public static final String STATE_COMMISSIONING = "commissioning";
  
  public static final String STATE_FAULT = "fault";
  
  public static final String STATE_OFFLINE = "offline";
  
  private static final String TAG = LowpanInterface.class.getSimpleName();
  
  private final ILowpanInterface mBinder;
  
  public static abstract class Callback {
    public void onConnectedChanged(boolean param1Boolean) {}
    
    public void onEnabledChanged(boolean param1Boolean) {}
    
    public void onUpChanged(boolean param1Boolean) {}
    
    public void onRoleChanged(String param1String) {}
    
    public void onStateChanged(String param1String) {}
    
    public void onLowpanIdentityChanged(LowpanIdentity param1LowpanIdentity) {}
    
    public void onLinkNetworkAdded(IpPrefix param1IpPrefix) {}
    
    public void onLinkNetworkRemoved(IpPrefix param1IpPrefix) {}
    
    public void onLinkAddressAdded(LinkAddress param1LinkAddress) {}
    
    public void onLinkAddressRemoved(LinkAddress param1LinkAddress) {}
  }
  
  private final HashMap<Integer, ILowpanInterfaceListener> mListenerMap = new HashMap<>();
  
  private final Looper mLooper;
  
  public LowpanInterface(Context paramContext, ILowpanInterface paramILowpanInterface, Looper paramLooper) {
    this.mBinder = paramILowpanInterface;
    this.mLooper = paramLooper;
  }
  
  public ILowpanInterface getService() {
    return this.mBinder;
  }
  
  public void form(LowpanProvision paramLowpanProvision) throws LowpanException {
    try {
      this.mBinder.form(paramLowpanProvision);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw LowpanException.rethrowFromServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public void join(LowpanProvision paramLowpanProvision) throws LowpanException {
    try {
      this.mBinder.join(paramLowpanProvision);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw LowpanException.rethrowFromServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public void attach(LowpanProvision paramLowpanProvision) throws LowpanException {
    try {
      this.mBinder.attach(paramLowpanProvision);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw LowpanException.rethrowFromServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public void leave() throws LowpanException {
    try {
      this.mBinder.leave();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw LowpanException.rethrowFromServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public LowpanCommissioningSession startCommissioningSession(LowpanBeaconInfo paramLowpanBeaconInfo) throws LowpanException {
    try {
      this.mBinder.startCommissioningSession(paramLowpanBeaconInfo);
      return new LowpanCommissioningSession(this.mBinder, paramLowpanBeaconInfo, this.mLooper);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw LowpanException.rethrowFromServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public void reset() throws LowpanException {
    try {
      this.mBinder.reset();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw LowpanException.rethrowFromServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public String getName() {
    try {
      return this.mBinder.getName();
    } catch (DeadObjectException deadObjectException) {
      return "";
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public boolean isEnabled() {
    try {
      return this.mBinder.isEnabled();
    } catch (DeadObjectException deadObjectException) {
      return false;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public void setEnabled(boolean paramBoolean) throws LowpanException {
    try {
      this.mBinder.setEnabled(paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw LowpanException.rethrowFromServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public boolean isUp() {
    try {
      return this.mBinder.isUp();
    } catch (DeadObjectException deadObjectException) {
      return false;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public boolean isConnected() {
    try {
      return this.mBinder.isConnected();
    } catch (DeadObjectException deadObjectException) {
      return false;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public boolean isCommissioned() {
    try {
      return this.mBinder.isCommissioned();
    } catch (DeadObjectException deadObjectException) {
      return false;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public String getState() {
    try {
      return this.mBinder.getState();
    } catch (DeadObjectException deadObjectException) {
      return "fault";
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public String getPartitionId() {
    try {
      return this.mBinder.getPartitionId();
    } catch (DeadObjectException deadObjectException) {
      return "";
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public LowpanIdentity getLowpanIdentity() {
    try {
      return this.mBinder.getLowpanIdentity();
    } catch (DeadObjectException deadObjectException) {
      return new LowpanIdentity();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public String getRole() {
    try {
      return this.mBinder.getRole();
    } catch (DeadObjectException deadObjectException) {
      return "detached";
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public LowpanCredential getLowpanCredential() {
    try {
      return this.mBinder.getLowpanCredential();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public String[] getSupportedNetworkTypes() throws LowpanException {
    try {
      return this.mBinder.getSupportedNetworkTypes();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw LowpanException.rethrowFromServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public LowpanChannelInfo[] getSupportedChannels() throws LowpanException {
    try {
      return this.mBinder.getSupportedChannels();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw LowpanException.rethrowFromServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public void registerCallback(Callback paramCallback, Handler paramHandler) {
    Object object = new Object(this, paramHandler, paramCallback);
    try {
      this.mBinder.addListener((ILowpanInterfaceListener)object);
      synchronized (this.mListenerMap) {
        this.mListenerMap.put(Integer.valueOf(System.identityHashCode(paramCallback)), object);
        return;
      } 
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public void registerCallback(Callback paramCallback) {
    registerCallback(paramCallback, null);
  }
  
  public void unregisterCallback(Callback paramCallback) {
    int i = System.identityHashCode(paramCallback);
    synchronized (this.mListenerMap) {
      ILowpanInterfaceListener iLowpanInterfaceListener = this.mListenerMap.get(Integer.valueOf(i));
      if (iLowpanInterfaceListener != null) {
        this.mListenerMap.remove(Integer.valueOf(i));
        try {
          this.mBinder.removeListener(iLowpanInterfaceListener);
        } catch (DeadObjectException deadObjectException) {
        
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowAsRuntimeException();
        } 
      } 
      return;
    } 
  }
  
  public LowpanScanner createScanner() {
    return new LowpanScanner(this.mBinder);
  }
  
  public LinkAddress[] getLinkAddresses() throws LowpanException {
    try {
      String[] arrayOfString = this.mBinder.getLinkAddresses();
      LinkAddress[] arrayOfLinkAddress = new LinkAddress[arrayOfString.length];
      byte b1 = 0;
      int i;
      byte b2;
      for (i = arrayOfString.length, b2 = 0; b2 < i; ) {
        String str = arrayOfString[b2];
        arrayOfLinkAddress[b1] = new LinkAddress(str);
        b2++;
        b1++;
      } 
      return arrayOfLinkAddress;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw LowpanException.rethrowFromServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public IpPrefix[] getLinkNetworks() throws LowpanException {
    try {
      return this.mBinder.getLinkNetworks();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw LowpanException.rethrowFromServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public void addOnMeshPrefix(IpPrefix paramIpPrefix, int paramInt) throws LowpanException {
    try {
      this.mBinder.addOnMeshPrefix(paramIpPrefix, paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw LowpanException.rethrowFromServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public void removeOnMeshPrefix(IpPrefix paramIpPrefix) {
    try {
      this.mBinder.removeOnMeshPrefix(paramIpPrefix);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } catch (ServiceSpecificException serviceSpecificException) {
      Log.e(TAG, serviceSpecificException.toString());
      return;
    } 
  }
  
  public void addExternalRoute(IpPrefix paramIpPrefix, int paramInt) throws LowpanException {
    try {
      this.mBinder.addExternalRoute(paramIpPrefix, paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw LowpanException.rethrowFromServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public void removeExternalRoute(IpPrefix paramIpPrefix) {
    try {
      this.mBinder.removeExternalRoute(paramIpPrefix);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } catch (ServiceSpecificException serviceSpecificException) {
      Log.e(TAG, serviceSpecificException.toString());
      return;
    } 
  }
}
