package android.net;

import android.os.Parcel;
import android.os.Parcelable;

public class DhcpInfo implements Parcelable {
  public DhcpInfo() {}
  
  public DhcpInfo(DhcpInfo paramDhcpInfo) {
    if (paramDhcpInfo != null) {
      this.ipAddress = paramDhcpInfo.ipAddress;
      this.gateway = paramDhcpInfo.gateway;
      this.netmask = paramDhcpInfo.netmask;
      this.dns1 = paramDhcpInfo.dns1;
      this.dns2 = paramDhcpInfo.dns2;
      this.serverAddress = paramDhcpInfo.serverAddress;
      this.leaseDuration = paramDhcpInfo.leaseDuration;
    } 
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append("ipaddr ");
    putAddress(stringBuffer, this.ipAddress);
    stringBuffer.append(" gateway ");
    putAddress(stringBuffer, this.gateway);
    stringBuffer.append(" netmask ");
    putAddress(stringBuffer, this.netmask);
    stringBuffer.append(" dns1 ");
    putAddress(stringBuffer, this.dns1);
    stringBuffer.append(" dns2 ");
    putAddress(stringBuffer, this.dns2);
    stringBuffer.append(" DHCP server ");
    putAddress(stringBuffer, this.serverAddress);
    stringBuffer.append(" lease ");
    stringBuffer.append(this.leaseDuration);
    stringBuffer.append(" seconds");
    return stringBuffer.toString();
  }
  
  private static void putAddress(StringBuffer paramStringBuffer, int paramInt) {
    paramStringBuffer.append(NetworkUtils.intToInetAddress(paramInt).getHostAddress());
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.ipAddress);
    paramParcel.writeInt(this.gateway);
    paramParcel.writeInt(this.netmask);
    paramParcel.writeInt(this.dns1);
    paramParcel.writeInt(this.dns2);
    paramParcel.writeInt(this.serverAddress);
    paramParcel.writeInt(this.leaseDuration);
  }
  
  public static final Parcelable.Creator<DhcpInfo> CREATOR = new Parcelable.Creator<DhcpInfo>() {
      public DhcpInfo createFromParcel(Parcel param1Parcel) {
        DhcpInfo dhcpInfo = new DhcpInfo();
        dhcpInfo.ipAddress = param1Parcel.readInt();
        dhcpInfo.gateway = param1Parcel.readInt();
        dhcpInfo.netmask = param1Parcel.readInt();
        dhcpInfo.dns1 = param1Parcel.readInt();
        dhcpInfo.dns2 = param1Parcel.readInt();
        dhcpInfo.serverAddress = param1Parcel.readInt();
        dhcpInfo.leaseDuration = param1Parcel.readInt();
        return dhcpInfo;
      }
      
      public DhcpInfo[] newArray(int param1Int) {
        return new DhcpInfo[param1Int];
      }
    };
  
  public int dns1;
  
  public int dns2;
  
  public int gateway;
  
  public int ipAddress;
  
  public int leaseDuration;
  
  public int netmask;
  
  public int serverAddress;
}
