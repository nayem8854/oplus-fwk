package android.net;

import android.os.Parcel;

public class MobileLinkQualityInfo extends LinkQualityInfo {
  private int mMobileNetworkType = Integer.MAX_VALUE;
  
  private int mRssi = Integer.MAX_VALUE;
  
  private int mGsmErrorRate = Integer.MAX_VALUE;
  
  private int mCdmaDbm = Integer.MAX_VALUE;
  
  private int mCdmaEcio = Integer.MAX_VALUE;
  
  private int mEvdoDbm = Integer.MAX_VALUE;
  
  private int mEvdoEcio = Integer.MAX_VALUE;
  
  private int mEvdoSnr = Integer.MAX_VALUE;
  
  private int mLteSignalStrength = Integer.MAX_VALUE;
  
  private int mLteRsrp = Integer.MAX_VALUE;
  
  private int mLteRsrq = Integer.MAX_VALUE;
  
  private int mLteRssnr = Integer.MAX_VALUE;
  
  private int mLteCqi = Integer.MAX_VALUE;
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcel(paramParcel, paramInt, 3);
    paramParcel.writeInt(this.mMobileNetworkType);
    paramParcel.writeInt(this.mRssi);
    paramParcel.writeInt(this.mGsmErrorRate);
    paramParcel.writeInt(this.mCdmaDbm);
    paramParcel.writeInt(this.mCdmaEcio);
    paramParcel.writeInt(this.mEvdoDbm);
    paramParcel.writeInt(this.mEvdoEcio);
    paramParcel.writeInt(this.mEvdoSnr);
    paramParcel.writeInt(this.mLteSignalStrength);
    paramParcel.writeInt(this.mLteRsrp);
    paramParcel.writeInt(this.mLteRsrq);
    paramParcel.writeInt(this.mLteRssnr);
    paramParcel.writeInt(this.mLteCqi);
  }
  
  public static MobileLinkQualityInfo createFromParcelBody(Parcel paramParcel) {
    MobileLinkQualityInfo mobileLinkQualityInfo = new MobileLinkQualityInfo();
    mobileLinkQualityInfo.initializeFromParcel(paramParcel);
    mobileLinkQualityInfo.mMobileNetworkType = paramParcel.readInt();
    mobileLinkQualityInfo.mRssi = paramParcel.readInt();
    mobileLinkQualityInfo.mGsmErrorRate = paramParcel.readInt();
    mobileLinkQualityInfo.mCdmaDbm = paramParcel.readInt();
    mobileLinkQualityInfo.mCdmaEcio = paramParcel.readInt();
    mobileLinkQualityInfo.mEvdoDbm = paramParcel.readInt();
    mobileLinkQualityInfo.mEvdoEcio = paramParcel.readInt();
    mobileLinkQualityInfo.mEvdoSnr = paramParcel.readInt();
    mobileLinkQualityInfo.mLteSignalStrength = paramParcel.readInt();
    mobileLinkQualityInfo.mLteRsrp = paramParcel.readInt();
    mobileLinkQualityInfo.mLteRsrq = paramParcel.readInt();
    mobileLinkQualityInfo.mLteRssnr = paramParcel.readInt();
    mobileLinkQualityInfo.mLteCqi = paramParcel.readInt();
    return mobileLinkQualityInfo;
  }
  
  public int getMobileNetworkType() {
    return this.mMobileNetworkType;
  }
  
  public void setMobileNetworkType(int paramInt) {
    this.mMobileNetworkType = paramInt;
  }
  
  public int getRssi() {
    return this.mRssi;
  }
  
  public void setRssi(int paramInt) {
    this.mRssi = paramInt;
  }
  
  public int getGsmErrorRate() {
    return this.mGsmErrorRate;
  }
  
  public void setGsmErrorRate(int paramInt) {
    this.mGsmErrorRate = paramInt;
  }
  
  public int getCdmaDbm() {
    return this.mCdmaDbm;
  }
  
  public void setCdmaDbm(int paramInt) {
    this.mCdmaDbm = paramInt;
  }
  
  public int getCdmaEcio() {
    return this.mCdmaEcio;
  }
  
  public void setCdmaEcio(int paramInt) {
    this.mCdmaEcio = paramInt;
  }
  
  public int getEvdoDbm() {
    return this.mEvdoDbm;
  }
  
  public void setEvdoDbm(int paramInt) {
    this.mEvdoDbm = paramInt;
  }
  
  public int getEvdoEcio() {
    return this.mEvdoEcio;
  }
  
  public void setEvdoEcio(int paramInt) {
    this.mEvdoEcio = paramInt;
  }
  
  public int getEvdoSnr() {
    return this.mEvdoSnr;
  }
  
  public void setEvdoSnr(int paramInt) {
    this.mEvdoSnr = paramInt;
  }
  
  public int getLteSignalStrength() {
    return this.mLteSignalStrength;
  }
  
  public void setLteSignalStrength(int paramInt) {
    this.mLteSignalStrength = paramInt;
  }
  
  public int getLteRsrp() {
    return this.mLteRsrp;
  }
  
  public void setLteRsrp(int paramInt) {
    this.mLteRsrp = paramInt;
  }
  
  public int getLteRsrq() {
    return this.mLteRsrq;
  }
  
  public void setLteRsrq(int paramInt) {
    this.mLteRsrq = paramInt;
  }
  
  public int getLteRssnr() {
    return this.mLteRssnr;
  }
  
  public void setLteRssnr(int paramInt) {
    this.mLteRssnr = paramInt;
  }
  
  public int getLteCqi() {
    return this.mLteCqi;
  }
  
  public void setLteCqi(int paramInt) {
    this.mLteCqi = paramInt;
  }
}
