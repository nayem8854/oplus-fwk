package android.net;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.util.proto.ProtoOutputStream;
import com.android.okhttp.internalandroidapi.HttpURLConnectionFactory;
import java.io.FileDescriptor;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.SocketFactory;
import libcore.io.IoUtils;

public class Network implements Parcelable {
  public final int netId;
  
  private volatile HttpURLConnectionFactory mUrlConnectionFactory;
  
  private final transient boolean mPrivateDnsBypass;
  
  private volatile NetworkBoundSocketFactory mNetworkBoundSocketFactory = null;
  
  private final Object mLock = new Object();
  
  private static final int httpMaxConnections;
  
  private static final long httpKeepAliveDurationMs;
  
  private static final boolean httpKeepAlive;
  
  private static final int HANDLE_MAGIC_SIZE = 32;
  
  private static final long HANDLE_MAGIC = 3405697037L;
  
  public static final Parcelable.Creator<Network> CREATOR;
  
  static {
    boolean bool1;
    boolean bool = Boolean.parseBoolean(System.getProperty("http.keepAlive", "true"));
    if (bool) {
      bool1 = Integer.parseInt(System.getProperty("http.maxConnections", "5"));
    } else {
      bool1 = false;
    } 
    httpMaxConnections = bool1;
    httpKeepAliveDurationMs = Long.parseLong(System.getProperty("http.keepAliveDuration", "300000"));
    CREATOR = new Parcelable.Creator<Network>() {
        public Network createFromParcel(Parcel param1Parcel) {
          int i = param1Parcel.readInt();
          return new Network(i);
        }
        
        public Network[] newArray(int param1Int) {
          return new Network[param1Int];
        }
      };
  }
  
  public Network(int paramInt) {
    this(paramInt, false);
  }
  
  public Network(int paramInt, boolean paramBoolean) {
    this.netId = paramInt;
    this.mPrivateDnsBypass = paramBoolean;
  }
  
  @SystemApi
  public Network(Network paramNetwork) {
    this(paramNetwork.netId, paramNetwork.mPrivateDnsBypass);
  }
  
  public InetAddress[] getAllByName(String paramString) throws UnknownHostException {
    return InetAddress.getAllByNameOnNet(paramString, getNetIdForResolv());
  }
  
  public InetAddress getByName(String paramString) throws UnknownHostException {
    return InetAddress.getByNameOnNet(paramString, getNetIdForResolv());
  }
  
  @SystemApi
  public Network getPrivateDnsBypassingCopy() {
    return new Network(this.netId, true);
  }
  
  @SystemApi
  public int getNetId() {
    return this.netId;
  }
  
  public int getNetIdForResolv() {
    int i;
    if (this.mPrivateDnsBypass) {
      i = (int)(0x80000000L | this.netId);
    } else {
      i = this.netId;
    } 
    return i;
  }
  
  class NetworkBoundSocketFactory extends SocketFactory {
    final Network this$0;
    
    private NetworkBoundSocketFactory() {}
    
    private Socket connectToHost(String param1String, int param1Int, SocketAddress param1SocketAddress) throws IOException {
      // Byte code:
      //   0: aload_0
      //   1: getfield this$0 : Landroid/net/Network;
      //   4: aload_1
      //   5: invokevirtual getAllByName : (Ljava/lang/String;)[Ljava/net/InetAddress;
      //   8: astore #4
      //   10: iconst_0
      //   11: istore #5
      //   13: iload #5
      //   15: aload #4
      //   17: arraylength
      //   18: if_icmpge -> 107
      //   21: aload_0
      //   22: invokevirtual createSocket : ()Ljava/net/Socket;
      //   25: astore #6
      //   27: aload_3
      //   28: ifnull -> 37
      //   31: aload #6
      //   33: aload_3
      //   34: invokevirtual bind : (Ljava/net/SocketAddress;)V
      //   37: new java/net/InetSocketAddress
      //   40: astore #7
      //   42: aload #7
      //   44: aload #4
      //   46: iload #5
      //   48: aaload
      //   49: iload_2
      //   50: invokespecial <init> : (Ljava/net/InetAddress;I)V
      //   53: aload #6
      //   55: aload #7
      //   57: invokevirtual connect : (Ljava/net/SocketAddress;)V
      //   60: iconst_0
      //   61: ifeq -> 69
      //   64: aload #6
      //   66: invokestatic closeQuietly : (Ljava/net/Socket;)V
      //   69: aload #6
      //   71: areturn
      //   72: astore #7
      //   74: iconst_1
      //   75: ifeq -> 83
      //   78: aload #6
      //   80: invokestatic closeQuietly : (Ljava/net/Socket;)V
      //   83: aload #7
      //   85: athrow
      //   86: astore #6
      //   88: iload #5
      //   90: aload #4
      //   92: arraylength
      //   93: iconst_1
      //   94: isub
      //   95: if_icmpeq -> 104
      //   98: iinc #5, 1
      //   101: goto -> 13
      //   104: aload #6
      //   106: athrow
      //   107: new java/net/UnknownHostException
      //   110: dup
      //   111: aload_1
      //   112: invokespecial <init> : (Ljava/lang/String;)V
      //   115: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #204	-> 0
      //   #206	-> 10
      //   #208	-> 21
      //   #209	-> 27
      //   #211	-> 27
      //   #212	-> 37
      //   #213	-> 60
      //   #214	-> 60
      //   #216	-> 60
      //   #214	-> 69
      //   #216	-> 72
      //   #217	-> 83
      //   #218	-> 86
      //   #219	-> 88
      //   #206	-> 98
      //   #219	-> 104
      //   #222	-> 107
      // Exception table:
      //   from	to	target	type
      //   21	27	86	java/io/IOException
      //   31	37	72	finally
      //   37	60	72	finally
      //   64	69	86	java/io/IOException
      //   78	83	86	java/io/IOException
      //   83	86	86	java/io/IOException
    }
    
    public Socket createSocket(String param1String, int param1Int1, InetAddress param1InetAddress, int param1Int2) throws IOException {
      return connectToHost(param1String, param1Int1, new InetSocketAddress(param1InetAddress, param1Int2));
    }
    
    public Socket createSocket(InetAddress param1InetAddress1, int param1Int1, InetAddress param1InetAddress2, int param1Int2) throws IOException {
      Socket socket = createSocket();
      try {
        InetSocketAddress inetSocketAddress2 = new InetSocketAddress();
        this(param1InetAddress2, param1Int2);
        socket.bind(inetSocketAddress2);
        InetSocketAddress inetSocketAddress1 = new InetSocketAddress();
        this(param1InetAddress1, param1Int1);
        socket.connect(inetSocketAddress1);
        return socket;
      } finally {
        if (true)
          IoUtils.closeQuietly(socket); 
      } 
    }
    
    public Socket createSocket(InetAddress param1InetAddress, int param1Int) throws IOException {
      Socket socket = createSocket();
      try {
        InetSocketAddress inetSocketAddress = new InetSocketAddress();
        this(param1InetAddress, param1Int);
        socket.connect(inetSocketAddress);
        return socket;
      } finally {
        if (true)
          IoUtils.closeQuietly(socket); 
      } 
    }
    
    public Socket createSocket(String param1String, int param1Int) throws IOException {
      return connectToHost(param1String, param1Int, null);
    }
    
    public Socket createSocket() throws IOException {
      Socket socket = new Socket();
      try {
        Network.this.bindSocket(socket);
        return socket;
      } finally {
        if (true)
          IoUtils.closeQuietly(socket); 
      } 
    }
  }
  
  public SocketFactory getSocketFactory() {
    if (this.mNetworkBoundSocketFactory == null)
      synchronized (this.mLock) {
        if (this.mNetworkBoundSocketFactory == null) {
          NetworkBoundSocketFactory networkBoundSocketFactory = new NetworkBoundSocketFactory();
          this(this);
          this.mNetworkBoundSocketFactory = networkBoundSocketFactory;
        } 
      }  
    return this.mNetworkBoundSocketFactory;
  }
  
  private void maybeInitUrlConnectionFactory() {
    synchronized (this.mLock) {
      if (this.mUrlConnectionFactory == null) {
        _$$Lambda$Network$KD6DxaMRJIcajhj36TU1K7lJnHQ _$$Lambda$Network$KD6DxaMRJIcajhj36TU1K7lJnHQ = new _$$Lambda$Network$KD6DxaMRJIcajhj36TU1K7lJnHQ();
        this(this);
        HttpURLConnectionFactory httpURLConnectionFactory = new HttpURLConnectionFactory();
        this();
        httpURLConnectionFactory.setDns(_$$Lambda$Network$KD6DxaMRJIcajhj36TU1K7lJnHQ);
        httpURLConnectionFactory.setNewConnectionPool(httpMaxConnections, httpKeepAliveDurationMs, TimeUnit.MILLISECONDS);
        this.mUrlConnectionFactory = httpURLConnectionFactory;
      } 
      return;
    } 
  }
  
  public URLConnection openConnection(URL paramURL) throws IOException {
    ConnectivityManager connectivityManager = ConnectivityManager.getInstanceOrNull();
    if (connectivityManager != null) {
      Proxy proxy;
      ProxyInfo proxyInfo = connectivityManager.getProxyForNetwork(this);
      if (proxyInfo != null) {
        proxy = proxyInfo.makeProxy();
      } else {
        proxy = Proxy.NO_PROXY;
      } 
      return openConnection(paramURL, proxy);
    } 
    throw new IOException("No ConnectivityManager yet constructed, please construct one");
  }
  
  public URLConnection openConnection(URL paramURL, Proxy paramProxy) throws IOException {
    if (paramProxy != null) {
      maybeInitUrlConnectionFactory();
      SocketFactory socketFactory = getSocketFactory();
      return this.mUrlConnectionFactory.openConnection(paramURL, socketFactory, paramProxy);
    } 
    throw new IllegalArgumentException("proxy is null");
  }
  
  public void bindSocket(DatagramSocket paramDatagramSocket) throws IOException {
    paramDatagramSocket.getReuseAddress();
    bindSocket(paramDatagramSocket.getFileDescriptor$());
  }
  
  public void bindSocket(Socket paramSocket) throws IOException {
    paramSocket.getReuseAddress();
    bindSocket(paramSocket.getFileDescriptor$());
  }
  
  public void bindSocket(FileDescriptor paramFileDescriptor) throws IOException {
    try {
      SocketAddress socketAddress = Os.getpeername(paramFileDescriptor);
      InetAddress inetAddress = ((InetSocketAddress)socketAddress).getAddress();
      if (!inetAddress.isAnyLocalAddress()) {
        SocketException socketException = new SocketException();
        this("Socket is connected");
        throw socketException;
      } 
    } catch (ErrnoException errnoException1) {
      if (errnoException1.errno != OsConstants.ENOTCONN)
        throw errnoException1.rethrowAsSocketException(); 
    } catch (ClassCastException classCastException) {
      throw new SocketException("Only AF_INET/AF_INET6 sockets supported");
    } 
    int i = NetworkUtils.bindSocketToNetwork(classCastException.getInt$(), this.netId);
    if (i == 0)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Binding socket to network ");
    stringBuilder.append(this.netId);
    ErrnoException errnoException = new ErrnoException(stringBuilder.toString(), -i);
    throw errnoException.rethrowAsSocketException();
  }
  
  public static Network fromNetworkHandle(long paramLong) {
    if (paramLong != 0L) {
      if ((0xFFFFFFFFL & paramLong) == 3405697037L && paramLong >= 0L)
        return new Network((int)(paramLong >> 32L)); 
      throw new IllegalArgumentException("Value passed to fromNetworkHandle() is not a network handle.");
    } 
    throw new IllegalArgumentException("Network.fromNetworkHandle refusing to instantiate NETID_UNSET Network.");
  }
  
  public long getNetworkHandle() {
    int i = this.netId;
    if (i == 0)
      return 0L; 
    return i << 32L | 0xCAFED00DL;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.netId);
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof Network;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    if (this.netId == ((Network)paramObject).netId)
      bool1 = true; 
    return bool1;
  }
  
  public int hashCode() {
    return this.netId * 11;
  }
  
  public String toString() {
    return Integer.toString(this.netId);
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    paramLong = paramProtoOutputStream.start(paramLong);
    paramProtoOutputStream.write(1120986464257L, this.netId);
    paramProtoOutputStream.end(paramLong);
  }
}
