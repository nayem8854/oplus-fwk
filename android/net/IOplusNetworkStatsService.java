package android.net;

import android.net.stats.OplusSocketInfoTotal;
import android.net.stats.StatsValueTotal;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusNetworkStatsService extends IInterface {
  boolean clearSocketCookieLimit(long paramLong) throws RemoteException;
  
  boolean clearSocketUidLimit(int paramInt) throws RemoteException;
  
  long[] getAllSocketCookieLimit() throws RemoteException;
  
  int[] getAllSocketUidLimit() throws RemoteException;
  
  boolean getSocketCookieLimitStatus(long paramLong) throws RemoteException;
  
  OplusSocketInfoTotal getSocketInfoTotal() throws RemoteException;
  
  StatsValueTotal getSocketStatsTotal() throws RemoteException;
  
  boolean getSocketUidLimitStatus(int paramInt) throws RemoteException;
  
  StatsValueTotal getUidStatsTotal() throws RemoteException;
  
  boolean setSocketCookieLimit(long paramLong) throws RemoteException;
  
  boolean setSocketUidLimit(int paramInt) throws RemoteException;
  
  class Default implements IOplusNetworkStatsService {
    public StatsValueTotal getUidStatsTotal() throws RemoteException {
      return null;
    }
    
    public StatsValueTotal getSocketStatsTotal() throws RemoteException {
      return null;
    }
    
    public OplusSocketInfoTotal getSocketInfoTotal() throws RemoteException {
      return null;
    }
    
    public boolean getSocketCookieLimitStatus(long param1Long) throws RemoteException {
      return false;
    }
    
    public boolean setSocketCookieLimit(long param1Long) throws RemoteException {
      return false;
    }
    
    public boolean clearSocketCookieLimit(long param1Long) throws RemoteException {
      return false;
    }
    
    public long[] getAllSocketCookieLimit() throws RemoteException {
      return null;
    }
    
    public boolean getSocketUidLimitStatus(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean setSocketUidLimit(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean clearSocketUidLimit(int param1Int) throws RemoteException {
      return false;
    }
    
    public int[] getAllSocketUidLimit() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusNetworkStatsService {
    private static final String DESCRIPTOR = "android.net.IOplusNetworkStatsService";
    
    static final int TRANSACTION_clearSocketCookieLimit = 6;
    
    static final int TRANSACTION_clearSocketUidLimit = 10;
    
    static final int TRANSACTION_getAllSocketCookieLimit = 7;
    
    static final int TRANSACTION_getAllSocketUidLimit = 11;
    
    static final int TRANSACTION_getSocketCookieLimitStatus = 4;
    
    static final int TRANSACTION_getSocketInfoTotal = 3;
    
    static final int TRANSACTION_getSocketStatsTotal = 2;
    
    static final int TRANSACTION_getSocketUidLimitStatus = 8;
    
    static final int TRANSACTION_getUidStatsTotal = 1;
    
    static final int TRANSACTION_setSocketCookieLimit = 5;
    
    static final int TRANSACTION_setSocketUidLimit = 9;
    
    public Stub() {
      attachInterface(this, "android.net.IOplusNetworkStatsService");
    }
    
    public static IOplusNetworkStatsService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.IOplusNetworkStatsService");
      if (iInterface != null && iInterface instanceof IOplusNetworkStatsService)
        return (IOplusNetworkStatsService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 11:
          return "getAllSocketUidLimit";
        case 10:
          return "clearSocketUidLimit";
        case 9:
          return "setSocketUidLimit";
        case 8:
          return "getSocketUidLimitStatus";
        case 7:
          return "getAllSocketCookieLimit";
        case 6:
          return "clearSocketCookieLimit";
        case 5:
          return "setSocketCookieLimit";
        case 4:
          return "getSocketCookieLimitStatus";
        case 3:
          return "getSocketInfoTotal";
        case 2:
          return "getSocketStatsTotal";
        case 1:
          break;
      } 
      return "getUidStatsTotal";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool3;
        int j;
        boolean bool2;
        int i;
        boolean bool1;
        int[] arrayOfInt;
        long[] arrayOfLong;
        OplusSocketInfoTotal oplusSocketInfoTotal;
        long l;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 11:
            param1Parcel1.enforceInterface("android.net.IOplusNetworkStatsService");
            arrayOfInt = getAllSocketUidLimit();
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt);
            return true;
          case 10:
            arrayOfInt.enforceInterface("android.net.IOplusNetworkStatsService");
            param1Int1 = arrayOfInt.readInt();
            bool3 = clearSocketUidLimit(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 9:
            arrayOfInt.enforceInterface("android.net.IOplusNetworkStatsService");
            j = arrayOfInt.readInt();
            bool2 = setSocketUidLimit(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 8:
            arrayOfInt.enforceInterface("android.net.IOplusNetworkStatsService");
            i = arrayOfInt.readInt();
            bool1 = getSocketUidLimitStatus(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 7:
            arrayOfInt.enforceInterface("android.net.IOplusNetworkStatsService");
            arrayOfLong = getAllSocketCookieLimit();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLongArray(arrayOfLong);
            return true;
          case 6:
            arrayOfLong.enforceInterface("android.net.IOplusNetworkStatsService");
            l = arrayOfLong.readLong();
            bool1 = clearSocketCookieLimit(l);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 5:
            arrayOfLong.enforceInterface("android.net.IOplusNetworkStatsService");
            l = arrayOfLong.readLong();
            bool1 = setSocketCookieLimit(l);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 4:
            arrayOfLong.enforceInterface("android.net.IOplusNetworkStatsService");
            l = arrayOfLong.readLong();
            bool1 = getSocketCookieLimitStatus(l);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 3:
            arrayOfLong.enforceInterface("android.net.IOplusNetworkStatsService");
            oplusSocketInfoTotal = getSocketInfoTotal();
            param1Parcel2.writeNoException();
            if (oplusSocketInfoTotal != null) {
              param1Parcel2.writeInt(1);
              oplusSocketInfoTotal.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 2:
            oplusSocketInfoTotal.enforceInterface("android.net.IOplusNetworkStatsService");
            statsValueTotal = getSocketStatsTotal();
            param1Parcel2.writeNoException();
            if (statsValueTotal != null) {
              param1Parcel2.writeInt(1);
              statsValueTotal.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        statsValueTotal.enforceInterface("android.net.IOplusNetworkStatsService");
        StatsValueTotal statsValueTotal = getUidStatsTotal();
        param1Parcel2.writeNoException();
        if (statsValueTotal != null) {
          param1Parcel2.writeInt(1);
          statsValueTotal.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      param1Parcel2.writeString("android.net.IOplusNetworkStatsService");
      return true;
    }
    
    private static class Proxy implements IOplusNetworkStatsService {
      public static IOplusNetworkStatsService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.IOplusNetworkStatsService";
      }
      
      public StatsValueTotal getUidStatsTotal() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          StatsValueTotal statsValueTotal;
          parcel1.writeInterfaceToken("android.net.IOplusNetworkStatsService");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusNetworkStatsService.Stub.getDefaultImpl() != null) {
            statsValueTotal = IOplusNetworkStatsService.Stub.getDefaultImpl().getUidStatsTotal();
            return statsValueTotal;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            statsValueTotal = StatsValueTotal.CREATOR.createFromParcel(parcel2);
          } else {
            statsValueTotal = null;
          } 
          return statsValueTotal;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StatsValueTotal getSocketStatsTotal() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          StatsValueTotal statsValueTotal;
          parcel1.writeInterfaceToken("android.net.IOplusNetworkStatsService");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusNetworkStatsService.Stub.getDefaultImpl() != null) {
            statsValueTotal = IOplusNetworkStatsService.Stub.getDefaultImpl().getSocketStatsTotal();
            return statsValueTotal;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            statsValueTotal = StatsValueTotal.CREATOR.createFromParcel(parcel2);
          } else {
            statsValueTotal = null;
          } 
          return statsValueTotal;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public OplusSocketInfoTotal getSocketInfoTotal() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          OplusSocketInfoTotal oplusSocketInfoTotal;
          parcel1.writeInterfaceToken("android.net.IOplusNetworkStatsService");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOplusNetworkStatsService.Stub.getDefaultImpl() != null) {
            oplusSocketInfoTotal = IOplusNetworkStatsService.Stub.getDefaultImpl().getSocketInfoTotal();
            return oplusSocketInfoTotal;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            oplusSocketInfoTotal = OplusSocketInfoTotal.CREATOR.createFromParcel(parcel2);
          } else {
            oplusSocketInfoTotal = null;
          } 
          return oplusSocketInfoTotal;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getSocketCookieLimitStatus(long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IOplusNetworkStatsService");
          parcel1.writeLong(param2Long);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IOplusNetworkStatsService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusNetworkStatsService.Stub.getDefaultImpl().getSocketCookieLimitStatus(param2Long);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setSocketCookieLimit(long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IOplusNetworkStatsService");
          parcel1.writeLong(param2Long);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IOplusNetworkStatsService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusNetworkStatsService.Stub.getDefaultImpl().setSocketCookieLimit(param2Long);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean clearSocketCookieLimit(long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IOplusNetworkStatsService");
          parcel1.writeLong(param2Long);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IOplusNetworkStatsService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusNetworkStatsService.Stub.getDefaultImpl().clearSocketCookieLimit(param2Long);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long[] getAllSocketCookieLimit() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IOplusNetworkStatsService");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IOplusNetworkStatsService.Stub.getDefaultImpl() != null)
            return IOplusNetworkStatsService.Stub.getDefaultImpl().getAllSocketCookieLimit(); 
          parcel2.readException();
          return parcel2.createLongArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getSocketUidLimitStatus(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IOplusNetworkStatsService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(8, parcel1, parcel2, 0);
          if (!bool2 && IOplusNetworkStatsService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusNetworkStatsService.Stub.getDefaultImpl().getSocketUidLimitStatus(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setSocketUidLimit(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IOplusNetworkStatsService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IOplusNetworkStatsService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusNetworkStatsService.Stub.getDefaultImpl().setSocketUidLimit(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean clearSocketUidLimit(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IOplusNetworkStatsService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
          if (!bool2 && IOplusNetworkStatsService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusNetworkStatsService.Stub.getDefaultImpl().clearSocketUidLimit(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getAllSocketUidLimit() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IOplusNetworkStatsService");
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IOplusNetworkStatsService.Stub.getDefaultImpl() != null)
            return IOplusNetworkStatsService.Stub.getDefaultImpl().getAllSocketUidLimit(); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusNetworkStatsService param1IOplusNetworkStatsService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusNetworkStatsService != null) {
          Proxy.sDefaultImpl = param1IOplusNetworkStatsService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusNetworkStatsService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
