package android.net;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.RemoteException;
import com.android.internal.net.VpnProfile;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.security.GeneralSecurityException;

public class VpnManager {
  public static final int TYPE_VPN_NONE = -1;
  
  public static final int TYPE_VPN_PLATFORM = 2;
  
  public static final int TYPE_VPN_SERVICE = 1;
  
  private final Context mContext;
  
  private final IConnectivityManager mService;
  
  private static Intent getIntentForConfirmation() {
    Intent intent = new Intent();
    String str = Resources.getSystem().getString(17039948);
    ComponentName componentName = ComponentName.unflattenFromString(str);
    intent.setComponent(componentName);
    return intent;
  }
  
  public VpnManager(Context paramContext, IConnectivityManager paramIConnectivityManager) {
    this.mContext = (Context)Preconditions.checkNotNull(paramContext, "missing Context");
    this.mService = (IConnectivityManager)Preconditions.checkNotNull(paramIConnectivityManager, "missing IConnectivityManager");
  }
  
  public Intent provisionVpnProfile(PlatformVpnProfile paramPlatformVpnProfile) {
    try {
      VpnProfile vpnProfile = paramPlatformVpnProfile.toVpnProfile();
      try {
        boolean bool = this.mService.provisionVpnProfile(vpnProfile, this.mContext.getOpPackageName());
        if (bool)
          return null; 
        return getIntentForConfirmation();
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } catch (GeneralSecurityException|java.io.IOException generalSecurityException) {
      throw new IllegalArgumentException("Failed to serialize PlatformVpnProfile", generalSecurityException);
    } 
  }
  
  public void deleteProvisionedVpnProfile() {
    try {
      this.mService.deleteVpnProfile(this.mContext.getOpPackageName());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void startProvisionedVpnProfile() {
    try {
      this.mService.startVpnProfile(this.mContext.getOpPackageName());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void stopProvisionedVpnProfile() {
    try {
      this.mService.stopVpnProfile(this.mContext.getOpPackageName());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface VpnType {}
}
