package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INetworkStatsSession extends IInterface {
  void close() throws RemoteException;
  
  NetworkStats getDeviceSummaryForNetwork(NetworkTemplate paramNetworkTemplate, long paramLong1, long paramLong2) throws RemoteException;
  
  NetworkStatsHistory getHistoryForNetwork(NetworkTemplate paramNetworkTemplate, int paramInt) throws RemoteException;
  
  NetworkStatsHistory getHistoryForUid(NetworkTemplate paramNetworkTemplate, int paramInt1, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  NetworkStatsHistory getHistoryIntervalForUid(NetworkTemplate paramNetworkTemplate, int paramInt1, int paramInt2, int paramInt3, int paramInt4, long paramLong1, long paramLong2) throws RemoteException;
  
  int[] getRelevantUids() throws RemoteException;
  
  NetworkStats getSummaryForAllUid(NetworkTemplate paramNetworkTemplate, long paramLong1, long paramLong2, boolean paramBoolean) throws RemoteException;
  
  NetworkStats getSummaryForNetwork(NetworkTemplate paramNetworkTemplate, long paramLong1, long paramLong2) throws RemoteException;
  
  class Default implements INetworkStatsSession {
    public NetworkStats getDeviceSummaryForNetwork(NetworkTemplate param1NetworkTemplate, long param1Long1, long param1Long2) throws RemoteException {
      return null;
    }
    
    public NetworkStats getSummaryForNetwork(NetworkTemplate param1NetworkTemplate, long param1Long1, long param1Long2) throws RemoteException {
      return null;
    }
    
    public NetworkStatsHistory getHistoryForNetwork(NetworkTemplate param1NetworkTemplate, int param1Int) throws RemoteException {
      return null;
    }
    
    public NetworkStats getSummaryForAllUid(NetworkTemplate param1NetworkTemplate, long param1Long1, long param1Long2, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public NetworkStatsHistory getHistoryForUid(NetworkTemplate param1NetworkTemplate, int param1Int1, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {
      return null;
    }
    
    public NetworkStatsHistory getHistoryIntervalForUid(NetworkTemplate param1NetworkTemplate, int param1Int1, int param1Int2, int param1Int3, int param1Int4, long param1Long1, long param1Long2) throws RemoteException {
      return null;
    }
    
    public int[] getRelevantUids() throws RemoteException {
      return null;
    }
    
    public void close() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INetworkStatsSession {
    private static final String DESCRIPTOR = "android.net.INetworkStatsSession";
    
    static final int TRANSACTION_close = 8;
    
    static final int TRANSACTION_getDeviceSummaryForNetwork = 1;
    
    static final int TRANSACTION_getHistoryForNetwork = 3;
    
    static final int TRANSACTION_getHistoryForUid = 5;
    
    static final int TRANSACTION_getHistoryIntervalForUid = 6;
    
    static final int TRANSACTION_getRelevantUids = 7;
    
    static final int TRANSACTION_getSummaryForAllUid = 4;
    
    static final int TRANSACTION_getSummaryForNetwork = 2;
    
    public Stub() {
      attachInterface(this, "android.net.INetworkStatsSession");
    }
    
    public static INetworkStatsSession asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.INetworkStatsSession");
      if (iInterface != null && iInterface instanceof INetworkStatsSession)
        return (INetworkStatsSession)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "close";
        case 7:
          return "getRelevantUids";
        case 6:
          return "getHistoryIntervalForUid";
        case 5:
          return "getHistoryForUid";
        case 4:
          return "getSummaryForAllUid";
        case 3:
          return "getHistoryForNetwork";
        case 2:
          return "getSummaryForNetwork";
        case 1:
          break;
      } 
      return "getDeviceSummaryForNetwork";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        int[] arrayOfInt;
        NetworkStatsHistory networkStatsHistory2;
        NetworkStats networkStats2;
        NetworkStatsHistory networkStatsHistory1;
        NetworkTemplate networkTemplate;
        int i, j;
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("android.net.INetworkStatsSession");
            close();
            param1Parcel2.writeNoException();
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.net.INetworkStatsSession");
            arrayOfInt = getRelevantUids();
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt);
            return true;
          case 6:
            arrayOfInt.enforceInterface("android.net.INetworkStatsSession");
            if (arrayOfInt.readInt() != 0) {
              networkTemplate = NetworkTemplate.CREATOR.createFromParcel((Parcel)arrayOfInt);
            } else {
              networkTemplate = null;
            } 
            param1Int1 = arrayOfInt.readInt();
            i = arrayOfInt.readInt();
            j = arrayOfInt.readInt();
            param1Int2 = arrayOfInt.readInt();
            l1 = arrayOfInt.readLong();
            l2 = arrayOfInt.readLong();
            networkStatsHistory2 = getHistoryIntervalForUid(networkTemplate, param1Int1, i, j, param1Int2, l1, l2);
            param1Parcel2.writeNoException();
            if (networkStatsHistory2 != null) {
              param1Parcel2.writeInt(1);
              networkStatsHistory2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 5:
            networkStatsHistory2.enforceInterface("android.net.INetworkStatsSession");
            if (networkStatsHistory2.readInt() != 0) {
              networkTemplate = NetworkTemplate.CREATOR.createFromParcel((Parcel)networkStatsHistory2);
            } else {
              networkTemplate = null;
            } 
            param1Int1 = networkStatsHistory2.readInt();
            i = networkStatsHistory2.readInt();
            j = networkStatsHistory2.readInt();
            param1Int2 = networkStatsHistory2.readInt();
            networkStatsHistory2 = getHistoryForUid(networkTemplate, param1Int1, i, j, param1Int2);
            param1Parcel2.writeNoException();
            if (networkStatsHistory2 != null) {
              param1Parcel2.writeInt(1);
              networkStatsHistory2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 4:
            networkStatsHistory2.enforceInterface("android.net.INetworkStatsSession");
            if (networkStatsHistory2.readInt() != 0) {
              networkTemplate = NetworkTemplate.CREATOR.createFromParcel((Parcel)networkStatsHistory2);
            } else {
              networkTemplate = null;
            } 
            l2 = networkStatsHistory2.readLong();
            l1 = networkStatsHistory2.readLong();
            if (networkStatsHistory2.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            networkStats2 = getSummaryForAllUid(networkTemplate, l2, l1, bool);
            param1Parcel2.writeNoException();
            if (networkStats2 != null) {
              param1Parcel2.writeInt(1);
              networkStats2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            networkStats2.enforceInterface("android.net.INetworkStatsSession");
            if (networkStats2.readInt() != 0) {
              networkTemplate = NetworkTemplate.CREATOR.createFromParcel((Parcel)networkStats2);
            } else {
              networkTemplate = null;
            } 
            param1Int1 = networkStats2.readInt();
            networkStatsHistory1 = getHistoryForNetwork(networkTemplate, param1Int1);
            param1Parcel2.writeNoException();
            if (networkStatsHistory1 != null) {
              param1Parcel2.writeInt(1);
              networkStatsHistory1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 2:
            networkStatsHistory1.enforceInterface("android.net.INetworkStatsSession");
            if (networkStatsHistory1.readInt() != 0) {
              networkTemplate = NetworkTemplate.CREATOR.createFromParcel((Parcel)networkStatsHistory1);
            } else {
              networkTemplate = null;
            } 
            l2 = networkStatsHistory1.readLong();
            l1 = networkStatsHistory1.readLong();
            networkStats1 = getSummaryForNetwork(networkTemplate, l2, l1);
            param1Parcel2.writeNoException();
            if (networkStats1 != null) {
              param1Parcel2.writeInt(1);
              networkStats1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        networkStats1.enforceInterface("android.net.INetworkStatsSession");
        if (networkStats1.readInt() != 0) {
          networkTemplate = NetworkTemplate.CREATOR.createFromParcel((Parcel)networkStats1);
        } else {
          networkTemplate = null;
        } 
        long l1 = networkStats1.readLong();
        long l2 = networkStats1.readLong();
        NetworkStats networkStats1 = getDeviceSummaryForNetwork(networkTemplate, l1, l2);
        param1Parcel2.writeNoException();
        if (networkStats1 != null) {
          param1Parcel2.writeInt(1);
          networkStats1.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      param1Parcel2.writeString("android.net.INetworkStatsSession");
      return true;
    }
    
    private static class Proxy implements INetworkStatsSession {
      public static INetworkStatsSession sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.INetworkStatsSession";
      }
      
      public NetworkStats getDeviceSummaryForNetwork(NetworkTemplate param2NetworkTemplate, long param2Long1, long param2Long2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsSession");
          if (param2NetworkTemplate != null) {
            parcel1.writeInt(1);
            param2NetworkTemplate.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeLong(param2Long1);
          parcel1.writeLong(param2Long2);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && INetworkStatsSession.Stub.getDefaultImpl() != null)
            return INetworkStatsSession.Stub.getDefaultImpl().getDeviceSummaryForNetwork(param2NetworkTemplate, param2Long1, param2Long2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            NetworkStats networkStats = NetworkStats.CREATOR.createFromParcel(parcel2);
          } else {
            param2NetworkTemplate = null;
          } 
          return (NetworkStats)param2NetworkTemplate;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkStats getSummaryForNetwork(NetworkTemplate param2NetworkTemplate, long param2Long1, long param2Long2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsSession");
          if (param2NetworkTemplate != null) {
            parcel1.writeInt(1);
            param2NetworkTemplate.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeLong(param2Long1);
          parcel1.writeLong(param2Long2);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && INetworkStatsSession.Stub.getDefaultImpl() != null)
            return INetworkStatsSession.Stub.getDefaultImpl().getSummaryForNetwork(param2NetworkTemplate, param2Long1, param2Long2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            NetworkStats networkStats = NetworkStats.CREATOR.createFromParcel(parcel2);
          } else {
            param2NetworkTemplate = null;
          } 
          return (NetworkStats)param2NetworkTemplate;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkStatsHistory getHistoryForNetwork(NetworkTemplate param2NetworkTemplate, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsSession");
          if (param2NetworkTemplate != null) {
            parcel1.writeInt(1);
            param2NetworkTemplate.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && INetworkStatsSession.Stub.getDefaultImpl() != null)
            return INetworkStatsSession.Stub.getDefaultImpl().getHistoryForNetwork(param2NetworkTemplate, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            NetworkStatsHistory networkStatsHistory = NetworkStatsHistory.CREATOR.createFromParcel(parcel2);
          } else {
            param2NetworkTemplate = null;
          } 
          return (NetworkStatsHistory)param2NetworkTemplate;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkStats getSummaryForAllUid(NetworkTemplate param2NetworkTemplate, long param2Long1, long param2Long2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsSession");
          boolean bool = true;
          if (param2NetworkTemplate != null) {
            parcel1.writeInt(1);
            param2NetworkTemplate.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          try {
            parcel1.writeLong(param2Long1);
            try {
              parcel1.writeLong(param2Long2);
              if (!param2Boolean)
                bool = false; 
              parcel1.writeInt(bool);
              try {
                boolean bool1 = this.mRemote.transact(4, parcel1, parcel2, 0);
                if (!bool1 && INetworkStatsSession.Stub.getDefaultImpl() != null) {
                  NetworkStats networkStats = INetworkStatsSession.Stub.getDefaultImpl().getSummaryForAllUid(param2NetworkTemplate, param2Long1, param2Long2, param2Boolean);
                  parcel2.recycle();
                  parcel1.recycle();
                  return networkStats;
                } 
                parcel2.readException();
                if (parcel2.readInt() != 0) {
                  NetworkStats networkStats = NetworkStats.CREATOR.createFromParcel(parcel2);
                } else {
                  param2NetworkTemplate = null;
                } 
                parcel2.recycle();
                parcel1.recycle();
                return (NetworkStats)param2NetworkTemplate;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2NetworkTemplate;
      }
      
      public NetworkStatsHistory getHistoryForUid(NetworkTemplate param2NetworkTemplate, int param2Int1, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsSession");
          if (param2NetworkTemplate != null) {
            parcel1.writeInt(1);
            param2NetworkTemplate.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeInt(param2Int4);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && INetworkStatsSession.Stub.getDefaultImpl() != null)
            return INetworkStatsSession.Stub.getDefaultImpl().getHistoryForUid(param2NetworkTemplate, param2Int1, param2Int2, param2Int3, param2Int4); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            NetworkStatsHistory networkStatsHistory = NetworkStatsHistory.CREATOR.createFromParcel(parcel2);
          } else {
            param2NetworkTemplate = null;
          } 
          return (NetworkStatsHistory)param2NetworkTemplate;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkStatsHistory getHistoryIntervalForUid(NetworkTemplate param2NetworkTemplate, int param2Int1, int param2Int2, int param2Int3, int param2Int4, long param2Long1, long param2Long2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsSession");
          if (param2NetworkTemplate != null) {
            parcel1.writeInt(1);
            param2NetworkTemplate.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeInt(param2Int2);
              parcel1.writeInt(param2Int3);
              parcel1.writeInt(param2Int4);
              parcel1.writeLong(param2Long1);
              parcel1.writeLong(param2Long2);
              boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
              if (!bool && INetworkStatsSession.Stub.getDefaultImpl() != null) {
                NetworkStatsHistory networkStatsHistory = INetworkStatsSession.Stub.getDefaultImpl().getHistoryIntervalForUid(param2NetworkTemplate, param2Int1, param2Int2, param2Int3, param2Int4, param2Long1, param2Long2);
                parcel2.recycle();
                parcel1.recycle();
                return networkStatsHistory;
              } 
              parcel2.readException();
              if (parcel2.readInt() != 0) {
                NetworkStatsHistory networkStatsHistory = NetworkStatsHistory.CREATOR.createFromParcel(parcel2);
              } else {
                param2NetworkTemplate = null;
              } 
              parcel2.recycle();
              parcel1.recycle();
              return (NetworkStatsHistory)param2NetworkTemplate;
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2NetworkTemplate;
      }
      
      public int[] getRelevantUids() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsSession");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && INetworkStatsSession.Stub.getDefaultImpl() != null)
            return INetworkStatsSession.Stub.getDefaultImpl().getRelevantUids(); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void close() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsSession");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && INetworkStatsSession.Stub.getDefaultImpl() != null) {
            INetworkStatsSession.Stub.getDefaultImpl().close();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INetworkStatsSession param1INetworkStatsSession) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INetworkStatsSession != null) {
          Proxy.sDefaultImpl = param1INetworkStatsSession;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INetworkStatsSession getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
