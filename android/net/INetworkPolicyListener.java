package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.SubscriptionPlan;

public interface INetworkPolicyListener extends IInterface {
  void onMeteredIfacesChanged(String[] paramArrayOfString) throws RemoteException;
  
  void onRestrictBackgroundChanged(boolean paramBoolean) throws RemoteException;
  
  void onSubscriptionOverride(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void onSubscriptionPlansChanged(int paramInt, SubscriptionPlan[] paramArrayOfSubscriptionPlan) throws RemoteException;
  
  void onUidPoliciesChanged(int paramInt1, int paramInt2) throws RemoteException;
  
  void onUidRulesChanged(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements INetworkPolicyListener {
    public void onUidRulesChanged(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onMeteredIfacesChanged(String[] param1ArrayOfString) throws RemoteException {}
    
    public void onRestrictBackgroundChanged(boolean param1Boolean) throws RemoteException {}
    
    public void onUidPoliciesChanged(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onSubscriptionOverride(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void onSubscriptionPlansChanged(int param1Int, SubscriptionPlan[] param1ArrayOfSubscriptionPlan) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INetworkPolicyListener {
    private static final String DESCRIPTOR = "android.net.INetworkPolicyListener";
    
    static final int TRANSACTION_onMeteredIfacesChanged = 2;
    
    static final int TRANSACTION_onRestrictBackgroundChanged = 3;
    
    static final int TRANSACTION_onSubscriptionOverride = 5;
    
    static final int TRANSACTION_onSubscriptionPlansChanged = 6;
    
    static final int TRANSACTION_onUidPoliciesChanged = 4;
    
    static final int TRANSACTION_onUidRulesChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.net.INetworkPolicyListener");
    }
    
    public static INetworkPolicyListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.INetworkPolicyListener");
      if (iInterface != null && iInterface instanceof INetworkPolicyListener)
        return (INetworkPolicyListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "onSubscriptionPlansChanged";
        case 5:
          return "onSubscriptionOverride";
        case 4:
          return "onUidPoliciesChanged";
        case 3:
          return "onRestrictBackgroundChanged";
        case 2:
          return "onMeteredIfacesChanged";
        case 1:
          break;
      } 
      return "onUidRulesChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        SubscriptionPlan[] arrayOfSubscriptionPlan;
        String[] arrayOfString;
        int i;
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("android.net.INetworkPolicyListener");
            param1Int1 = param1Parcel1.readInt();
            arrayOfSubscriptionPlan = param1Parcel1.<SubscriptionPlan>createTypedArray(SubscriptionPlan.CREATOR);
            onSubscriptionPlansChanged(param1Int1, arrayOfSubscriptionPlan);
            return true;
          case 5:
            arrayOfSubscriptionPlan.enforceInterface("android.net.INetworkPolicyListener");
            param1Int1 = arrayOfSubscriptionPlan.readInt();
            param1Int2 = arrayOfSubscriptionPlan.readInt();
            i = arrayOfSubscriptionPlan.readInt();
            onSubscriptionOverride(param1Int1, param1Int2, i);
            return true;
          case 4:
            arrayOfSubscriptionPlan.enforceInterface("android.net.INetworkPolicyListener");
            param1Int2 = arrayOfSubscriptionPlan.readInt();
            param1Int1 = arrayOfSubscriptionPlan.readInt();
            onUidPoliciesChanged(param1Int2, param1Int1);
            return true;
          case 3:
            arrayOfSubscriptionPlan.enforceInterface("android.net.INetworkPolicyListener");
            if (arrayOfSubscriptionPlan.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            onRestrictBackgroundChanged(bool);
            return true;
          case 2:
            arrayOfSubscriptionPlan.enforceInterface("android.net.INetworkPolicyListener");
            arrayOfString = arrayOfSubscriptionPlan.createStringArray();
            onMeteredIfacesChanged(arrayOfString);
            return true;
          case 1:
            break;
        } 
        arrayOfString.enforceInterface("android.net.INetworkPolicyListener");
        param1Int1 = arrayOfString.readInt();
        param1Int2 = arrayOfString.readInt();
        onUidRulesChanged(param1Int1, param1Int2);
        return true;
      } 
      param1Parcel2.writeString("android.net.INetworkPolicyListener");
      return true;
    }
    
    private static class Proxy implements INetworkPolicyListener {
      public static INetworkPolicyListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.INetworkPolicyListener";
      }
      
      public void onUidRulesChanged(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.INetworkPolicyListener");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && INetworkPolicyListener.Stub.getDefaultImpl() != null) {
            INetworkPolicyListener.Stub.getDefaultImpl().onUidRulesChanged(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onMeteredIfacesChanged(String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.INetworkPolicyListener");
          parcel.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && INetworkPolicyListener.Stub.getDefaultImpl() != null) {
            INetworkPolicyListener.Stub.getDefaultImpl().onMeteredIfacesChanged(param2ArrayOfString);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRestrictBackgroundChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.net.INetworkPolicyListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool1 && INetworkPolicyListener.Stub.getDefaultImpl() != null) {
            INetworkPolicyListener.Stub.getDefaultImpl().onRestrictBackgroundChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onUidPoliciesChanged(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.INetworkPolicyListener");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && INetworkPolicyListener.Stub.getDefaultImpl() != null) {
            INetworkPolicyListener.Stub.getDefaultImpl().onUidPoliciesChanged(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSubscriptionOverride(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.INetworkPolicyListener");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && INetworkPolicyListener.Stub.getDefaultImpl() != null) {
            INetworkPolicyListener.Stub.getDefaultImpl().onSubscriptionOverride(param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSubscriptionPlansChanged(int param2Int, SubscriptionPlan[] param2ArrayOfSubscriptionPlan) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.INetworkPolicyListener");
          parcel.writeInt(param2Int);
          parcel.writeTypedArray(param2ArrayOfSubscriptionPlan, 0);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && INetworkPolicyListener.Stub.getDefaultImpl() != null) {
            INetworkPolicyListener.Stub.getDefaultImpl().onSubscriptionPlansChanged(param2Int, param2ArrayOfSubscriptionPlan);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INetworkPolicyListener param1INetworkPolicyListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INetworkPolicyListener != null) {
          Proxy.sDefaultImpl = param1INetworkPolicyListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INetworkPolicyListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
