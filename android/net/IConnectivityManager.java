package android.net;

import android.app.PendingIntent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Messenger;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.PersistableBundle;
import android.os.RemoteException;
import com.android.internal.net.LegacyVpnInfo;
import com.android.internal.net.VpnConfig;
import com.android.internal.net.VpnProfile;
import java.io.FileDescriptor;
import java.util.ArrayList;
import java.util.List;

public interface IConnectivityManager extends IInterface {
  boolean addVpnAddress(String paramString, int paramInt) throws RemoteException;
  
  int checkMobileProvisioning(int paramInt) throws RemoteException;
  
  void declareNetworkRequestUnfulfillable(NetworkRequest paramNetworkRequest) throws RemoteException;
  
  void deleteVpnProfile(String paramString) throws RemoteException;
  
  ParcelFileDescriptor establishVpn(VpnConfig paramVpnConfig) throws RemoteException;
  
  void factoryReset() throws RemoteException;
  
  LinkProperties getActiveLinkProperties() throws RemoteException;
  
  Network getActiveNetwork() throws RemoteException;
  
  Network getActiveNetworkForUid(int paramInt, boolean paramBoolean) throws RemoteException;
  
  NetworkInfo getActiveNetworkInfo() throws RemoteException;
  
  NetworkInfo getActiveNetworkInfoForUid(int paramInt, boolean paramBoolean) throws RemoteException;
  
  NetworkQuotaInfo getActiveNetworkQuotaInfo() throws RemoteException;
  
  NetworkInfo[] getAllNetworkInfo() throws RemoteException;
  
  NetworkState[] getAllNetworkState() throws RemoteException;
  
  Network[] getAllNetworks() throws RemoteException;
  
  String getAlwaysOnVpnPackage(int paramInt) throws RemoteException;
  
  long getCacheAge() throws RemoteException;
  
  String getCaptivePortalServerUrl() throws RemoteException;
  
  NetworkRequest getCelluarNetworkRequest() throws RemoteException;
  
  int getConnectionOwnerUid(ConnectionInfo paramConnectionInfo) throws RemoteException;
  
  long getCurrentTimeMillis() throws RemoteException;
  
  NetworkCapabilities[] getDefaultNetworkCapabilitiesForUser(int paramInt, String paramString) throws RemoteException;
  
  NetworkRequest getDefaultRequest() throws RemoteException;
  
  ProxyInfo getGlobalProxy() throws RemoteException;
  
  int getLastTetherError(String paramString) throws RemoteException;
  
  LegacyVpnInfo getLegacyVpnInfo(int paramInt) throws RemoteException;
  
  LinkProperties getLinkProperties(Network paramNetwork) throws RemoteException;
  
  LinkProperties getLinkPropertiesForType(int paramInt) throws RemoteException;
  
  String getMobileProvisioningUrl() throws RemoteException;
  
  long[] getModemTxTime() throws RemoteException;
  
  int getMultipathPreference(Network paramNetwork) throws RemoteException;
  
  NetworkCapabilities getNetworkCapabilities(Network paramNetwork, String paramString) throws RemoteException;
  
  Network getNetworkForType(int paramInt) throws RemoteException;
  
  NetworkInfo getNetworkInfo(int paramInt) throws RemoteException;
  
  NetworkInfo getNetworkInfoForUid(Network paramNetwork, int paramInt, boolean paramBoolean) throws RemoteException;
  
  byte[] getNetworkWatchlistConfigHash() throws RemoteException;
  
  ProxyInfo getProxyForNetwork(Network paramNetwork) throws RemoteException;
  
  int getRestoreDefaultNetworkDelay(int paramInt) throws RemoteException;
  
  double getTelephonyPowerLost() throws RemoteException;
  
  String getTelephonyPowerState() throws RemoteException;
  
  String[] getTetherableIfaces() throws RemoteException;
  
  String[] getTetherableUsbRegexs() throws RemoteException;
  
  String[] getTetherableWifiRegexs() throws RemoteException;
  
  String[] getTetheredIfaces() throws RemoteException;
  
  String[] getTetheringErroredIfaces() throws RemoteException;
  
  VpnConfig getVpnConfig(int paramInt) throws RemoteException;
  
  List<String> getVpnLockdownWhitelist(int paramInt) throws RemoteException;
  
  boolean hasCache() throws RemoteException;
  
  boolean isActiveNetworkMetered() throws RemoteException;
  
  boolean isAlreadyUpdated() throws RemoteException;
  
  boolean isAlwaysOnVpnPackageSupported(int paramInt, String paramString) throws RemoteException;
  
  boolean isCallerCurrentAlwaysOnVpnApp() throws RemoteException;
  
  boolean isCallerCurrentAlwaysOnVpnLockdownApp() throws RemoteException;
  
  boolean isNetworkSupported(int paramInt) throws RemoteException;
  
  boolean isVpnLockdownEnabled(int paramInt) throws RemoteException;
  
  NetworkRequest listenForNetwork(NetworkCapabilities paramNetworkCapabilities, Messenger paramMessenger, IBinder paramIBinder, String paramString) throws RemoteException;
  
  boolean measureDataState(int paramInt) throws RemoteException;
  
  boolean oppoExecuteIPtableCmd(String paramString1, String paramString2) throws RemoteException;
  
  void oppoFastappDnsConfig(String[] paramArrayOfString1, String[] paramArrayOfString2, boolean paramBoolean) throws RemoteException;
  
  String oppoGetUid(String paramString1, String paramString2) throws RemoteException;
  
  void pendingListenForNetwork(NetworkCapabilities paramNetworkCapabilities, PendingIntent paramPendingIntent, String paramString) throws RemoteException;
  
  NetworkRequest pendingRequestForNetwork(NetworkCapabilities paramNetworkCapabilities, PendingIntent paramPendingIntent, String paramString) throws RemoteException;
  
  boolean prepareVpn(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  boolean provisionVpnProfile(VpnProfile paramVpnProfile, String paramString) throws RemoteException;
  
  List<String> readArpFile() throws RemoteException;
  
  void registerConnectivityDiagnosticsCallback(IConnectivityDiagnosticsCallback paramIConnectivityDiagnosticsCallback, NetworkRequest paramNetworkRequest, String paramString) throws RemoteException;
  
  Network registerNetworkAgent(Messenger paramMessenger, NetworkInfo paramNetworkInfo, LinkProperties paramLinkProperties, NetworkCapabilities paramNetworkCapabilities, int paramInt1, NetworkAgentConfig paramNetworkAgentConfig, int paramInt2) throws RemoteException;
  
  int registerNetworkFactory(Messenger paramMessenger, String paramString) throws RemoteException;
  
  int registerNetworkProvider(Messenger paramMessenger, String paramString) throws RemoteException;
  
  void releaseNetworkRequest(NetworkRequest paramNetworkRequest) throws RemoteException;
  
  void releasePendingNetworkRequest(PendingIntent paramPendingIntent) throws RemoteException;
  
  boolean removeVpnAddress(String paramString, int paramInt) throws RemoteException;
  
  void reportInetCondition(int paramInt1, int paramInt2) throws RemoteException;
  
  void reportNetworkConnectivity(Network paramNetwork, boolean paramBoolean) throws RemoteException;
  
  boolean requestBandwidthUpdate(Network paramNetwork) throws RemoteException;
  
  NetworkRequest requestNetwork(NetworkCapabilities paramNetworkCapabilities, Messenger paramMessenger, int paramInt1, IBinder paramIBinder, int paramInt2, String paramString) throws RemoteException;
  
  boolean requestRouteToHostAddress(int paramInt, byte[] paramArrayOfbyte) throws RemoteException;
  
  void setAcceptPartialConnectivity(Network paramNetwork, boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void setAcceptUnvalidated(Network paramNetwork, boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void setAirplaneMode(boolean paramBoolean) throws RemoteException;
  
  void setAlreadyUpdated(boolean paramBoolean) throws RemoteException;
  
  boolean setAlwaysOnVpnPackage(int paramInt, String paramString, boolean paramBoolean, List<String> paramList) throws RemoteException;
  
  void setAvoidUnvalidated(Network paramNetwork) throws RemoteException;
  
  void setGlobalProxy(ProxyInfo paramProxyInfo) throws RemoteException;
  
  void setModemTxTime(long[] paramArrayOflong) throws RemoteException;
  
  void setProvisioningNotificationVisible(boolean paramBoolean, int paramInt, String paramString) throws RemoteException;
  
  void setTelephonyPowerLost(double paramDouble) throws RemoteException;
  
  void setTelephonyPowerState(String paramString) throws RemoteException;
  
  boolean setUnderlyingNetworksForVpn(Network[] paramArrayOfNetwork) throws RemoteException;
  
  void setVpnPackageAuthorization(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  boolean shouldAvoidBadWifi() throws RemoteException;
  
  boolean shouldKeepCelluarNetwork(boolean paramBoolean) throws RemoteException;
  
  void simulateDataStall(int paramInt, long paramLong, Network paramNetwork, PersistableBundle paramPersistableBundle) throws RemoteException;
  
  void startCaptivePortalApp(Network paramNetwork) throws RemoteException;
  
  void startCaptivePortalAppInternal(Network paramNetwork, Bundle paramBundle) throws RemoteException;
  
  void startLegacyVpn(VpnProfile paramVpnProfile) throws RemoteException;
  
  void startNattKeepalive(Network paramNetwork, int paramInt1, ISocketKeepaliveCallback paramISocketKeepaliveCallback, String paramString1, int paramInt2, String paramString2) throws RemoteException;
  
  void startNattKeepaliveWithFd(Network paramNetwork, FileDescriptor paramFileDescriptor, int paramInt1, int paramInt2, ISocketKeepaliveCallback paramISocketKeepaliveCallback, String paramString1, String paramString2) throws RemoteException;
  
  IBinder startOrGetTestNetworkService() throws RemoteException;
  
  void startTcpKeepalive(Network paramNetwork, FileDescriptor paramFileDescriptor, int paramInt, ISocketKeepaliveCallback paramISocketKeepaliveCallback) throws RemoteException;
  
  void startVpnProfile(String paramString) throws RemoteException;
  
  void stopKeepalive(Network paramNetwork, int paramInt) throws RemoteException;
  
  void stopVpnProfile(String paramString) throws RemoteException;
  
  void unregisterConnectivityDiagnosticsCallback(IConnectivityDiagnosticsCallback paramIConnectivityDiagnosticsCallback) throws RemoteException;
  
  void unregisterNetworkFactory(Messenger paramMessenger) throws RemoteException;
  
  void unregisterNetworkProvider(Messenger paramMessenger) throws RemoteException;
  
  void updateDataNetworkConfig(String paramString1, String paramString2) throws RemoteException;
  
  boolean updateLockdownVpn() throws RemoteException;
  
  class Default implements IConnectivityManager {
    public Network getActiveNetwork() throws RemoteException {
      return null;
    }
    
    public Network getActiveNetworkForUid(int param1Int, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public NetworkInfo getActiveNetworkInfo() throws RemoteException {
      return null;
    }
    
    public NetworkInfo getActiveNetworkInfoForUid(int param1Int, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public NetworkInfo getNetworkInfo(int param1Int) throws RemoteException {
      return null;
    }
    
    public NetworkInfo getNetworkInfoForUid(Network param1Network, int param1Int, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public NetworkInfo[] getAllNetworkInfo() throws RemoteException {
      return null;
    }
    
    public Network getNetworkForType(int param1Int) throws RemoteException {
      return null;
    }
    
    public Network[] getAllNetworks() throws RemoteException {
      return null;
    }
    
    public NetworkCapabilities[] getDefaultNetworkCapabilitiesForUser(int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public boolean isNetworkSupported(int param1Int) throws RemoteException {
      return false;
    }
    
    public LinkProperties getActiveLinkProperties() throws RemoteException {
      return null;
    }
    
    public LinkProperties getLinkPropertiesForType(int param1Int) throws RemoteException {
      return null;
    }
    
    public LinkProperties getLinkProperties(Network param1Network) throws RemoteException {
      return null;
    }
    
    public NetworkCapabilities getNetworkCapabilities(Network param1Network, String param1String) throws RemoteException {
      return null;
    }
    
    public NetworkState[] getAllNetworkState() throws RemoteException {
      return null;
    }
    
    public NetworkQuotaInfo getActiveNetworkQuotaInfo() throws RemoteException {
      return null;
    }
    
    public boolean isActiveNetworkMetered() throws RemoteException {
      return false;
    }
    
    public boolean requestRouteToHostAddress(int param1Int, byte[] param1ArrayOfbyte) throws RemoteException {
      return false;
    }
    
    public int getLastTetherError(String param1String) throws RemoteException {
      return 0;
    }
    
    public String[] getTetherableIfaces() throws RemoteException {
      return null;
    }
    
    public String[] getTetheredIfaces() throws RemoteException {
      return null;
    }
    
    public String[] getTetheringErroredIfaces() throws RemoteException {
      return null;
    }
    
    public String[] getTetherableUsbRegexs() throws RemoteException {
      return null;
    }
    
    public String[] getTetherableWifiRegexs() throws RemoteException {
      return null;
    }
    
    public void reportInetCondition(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void reportNetworkConnectivity(Network param1Network, boolean param1Boolean) throws RemoteException {}
    
    public ProxyInfo getGlobalProxy() throws RemoteException {
      return null;
    }
    
    public void setGlobalProxy(ProxyInfo param1ProxyInfo) throws RemoteException {}
    
    public ProxyInfo getProxyForNetwork(Network param1Network) throws RemoteException {
      return null;
    }
    
    public boolean prepareVpn(String param1String1, String param1String2, int param1Int) throws RemoteException {
      return false;
    }
    
    public void setVpnPackageAuthorization(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public ParcelFileDescriptor establishVpn(VpnConfig param1VpnConfig) throws RemoteException {
      return null;
    }
    
    public boolean provisionVpnProfile(VpnProfile param1VpnProfile, String param1String) throws RemoteException {
      return false;
    }
    
    public void deleteVpnProfile(String param1String) throws RemoteException {}
    
    public void startVpnProfile(String param1String) throws RemoteException {}
    
    public void stopVpnProfile(String param1String) throws RemoteException {}
    
    public VpnConfig getVpnConfig(int param1Int) throws RemoteException {
      return null;
    }
    
    public void startLegacyVpn(VpnProfile param1VpnProfile) throws RemoteException {}
    
    public LegacyVpnInfo getLegacyVpnInfo(int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean updateLockdownVpn() throws RemoteException {
      return false;
    }
    
    public boolean isAlwaysOnVpnPackageSupported(int param1Int, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean setAlwaysOnVpnPackage(int param1Int, String param1String, boolean param1Boolean, List<String> param1List) throws RemoteException {
      return false;
    }
    
    public String getAlwaysOnVpnPackage(int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean isVpnLockdownEnabled(int param1Int) throws RemoteException {
      return false;
    }
    
    public List<String> getVpnLockdownWhitelist(int param1Int) throws RemoteException {
      return null;
    }
    
    public int checkMobileProvisioning(int param1Int) throws RemoteException {
      return 0;
    }
    
    public String getMobileProvisioningUrl() throws RemoteException {
      return null;
    }
    
    public void setProvisioningNotificationVisible(boolean param1Boolean, int param1Int, String param1String) throws RemoteException {}
    
    public void setAirplaneMode(boolean param1Boolean) throws RemoteException {}
    
    public boolean requestBandwidthUpdate(Network param1Network) throws RemoteException {
      return false;
    }
    
    public int registerNetworkFactory(Messenger param1Messenger, String param1String) throws RemoteException {
      return 0;
    }
    
    public void unregisterNetworkFactory(Messenger param1Messenger) throws RemoteException {}
    
    public int registerNetworkProvider(Messenger param1Messenger, String param1String) throws RemoteException {
      return 0;
    }
    
    public void unregisterNetworkProvider(Messenger param1Messenger) throws RemoteException {}
    
    public void declareNetworkRequestUnfulfillable(NetworkRequest param1NetworkRequest) throws RemoteException {}
    
    public Network registerNetworkAgent(Messenger param1Messenger, NetworkInfo param1NetworkInfo, LinkProperties param1LinkProperties, NetworkCapabilities param1NetworkCapabilities, int param1Int1, NetworkAgentConfig param1NetworkAgentConfig, int param1Int2) throws RemoteException {
      return null;
    }
    
    public NetworkRequest requestNetwork(NetworkCapabilities param1NetworkCapabilities, Messenger param1Messenger, int param1Int1, IBinder param1IBinder, int param1Int2, String param1String) throws RemoteException {
      return null;
    }
    
    public NetworkRequest pendingRequestForNetwork(NetworkCapabilities param1NetworkCapabilities, PendingIntent param1PendingIntent, String param1String) throws RemoteException {
      return null;
    }
    
    public void releasePendingNetworkRequest(PendingIntent param1PendingIntent) throws RemoteException {}
    
    public NetworkRequest listenForNetwork(NetworkCapabilities param1NetworkCapabilities, Messenger param1Messenger, IBinder param1IBinder, String param1String) throws RemoteException {
      return null;
    }
    
    public void pendingListenForNetwork(NetworkCapabilities param1NetworkCapabilities, PendingIntent param1PendingIntent, String param1String) throws RemoteException {}
    
    public void releaseNetworkRequest(NetworkRequest param1NetworkRequest) throws RemoteException {}
    
    public void setAcceptUnvalidated(Network param1Network, boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public void setAcceptPartialConnectivity(Network param1Network, boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public void setAvoidUnvalidated(Network param1Network) throws RemoteException {}
    
    public void startCaptivePortalApp(Network param1Network) throws RemoteException {}
    
    public void startCaptivePortalAppInternal(Network param1Network, Bundle param1Bundle) throws RemoteException {}
    
    public boolean shouldAvoidBadWifi() throws RemoteException {
      return false;
    }
    
    public int getMultipathPreference(Network param1Network) throws RemoteException {
      return 0;
    }
    
    public NetworkRequest getDefaultRequest() throws RemoteException {
      return null;
    }
    
    public int getRestoreDefaultNetworkDelay(int param1Int) throws RemoteException {
      return 0;
    }
    
    public boolean addVpnAddress(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean removeVpnAddress(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean setUnderlyingNetworksForVpn(Network[] param1ArrayOfNetwork) throws RemoteException {
      return false;
    }
    
    public void factoryReset() throws RemoteException {}
    
    public void startNattKeepalive(Network param1Network, int param1Int1, ISocketKeepaliveCallback param1ISocketKeepaliveCallback, String param1String1, int param1Int2, String param1String2) throws RemoteException {}
    
    public void startNattKeepaliveWithFd(Network param1Network, FileDescriptor param1FileDescriptor, int param1Int1, int param1Int2, ISocketKeepaliveCallback param1ISocketKeepaliveCallback, String param1String1, String param1String2) throws RemoteException {}
    
    public void startTcpKeepalive(Network param1Network, FileDescriptor param1FileDescriptor, int param1Int, ISocketKeepaliveCallback param1ISocketKeepaliveCallback) throws RemoteException {}
    
    public void stopKeepalive(Network param1Network, int param1Int) throws RemoteException {}
    
    public String getCaptivePortalServerUrl() throws RemoteException {
      return null;
    }
    
    public byte[] getNetworkWatchlistConfigHash() throws RemoteException {
      return null;
    }
    
    public int getConnectionOwnerUid(ConnectionInfo param1ConnectionInfo) throws RemoteException {
      return 0;
    }
    
    public boolean isCallerCurrentAlwaysOnVpnApp() throws RemoteException {
      return false;
    }
    
    public boolean isCallerCurrentAlwaysOnVpnLockdownApp() throws RemoteException {
      return false;
    }
    
    public void registerConnectivityDiagnosticsCallback(IConnectivityDiagnosticsCallback param1IConnectivityDiagnosticsCallback, NetworkRequest param1NetworkRequest, String param1String) throws RemoteException {}
    
    public void unregisterConnectivityDiagnosticsCallback(IConnectivityDiagnosticsCallback param1IConnectivityDiagnosticsCallback) throws RemoteException {}
    
    public IBinder startOrGetTestNetworkService() throws RemoteException {
      return null;
    }
    
    public void simulateDataStall(int param1Int, long param1Long, Network param1Network, PersistableBundle param1PersistableBundle) throws RemoteException {}
    
    public boolean measureDataState(int param1Int) throws RemoteException {
      return false;
    }
    
    public NetworkRequest getCelluarNetworkRequest() throws RemoteException {
      return null;
    }
    
    public boolean shouldKeepCelluarNetwork(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public void updateDataNetworkConfig(String param1String1, String param1String2) throws RemoteException {}
    
    public String getTelephonyPowerState() throws RemoteException {
      return null;
    }
    
    public boolean isAlreadyUpdated() throws RemoteException {
      return false;
    }
    
    public double getTelephonyPowerLost() throws RemoteException {
      return 0.0D;
    }
    
    public void setTelephonyPowerState(String param1String) throws RemoteException {}
    
    public void setAlreadyUpdated(boolean param1Boolean) throws RemoteException {}
    
    public void setTelephonyPowerLost(double param1Double) throws RemoteException {}
    
    public void setModemTxTime(long[] param1ArrayOflong) throws RemoteException {}
    
    public long[] getModemTxTime() throws RemoteException {
      return null;
    }
    
    public boolean hasCache() throws RemoteException {
      return false;
    }
    
    public long getCacheAge() throws RemoteException {
      return 0L;
    }
    
    public long getCurrentTimeMillis() throws RemoteException {
      return 0L;
    }
    
    public boolean oppoExecuteIPtableCmd(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public List<String> readArpFile() throws RemoteException {
      return null;
    }
    
    public String oppoGetUid(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public void oppoFastappDnsConfig(String[] param1ArrayOfString1, String[] param1ArrayOfString2, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IConnectivityManager {
    private static final String DESCRIPTOR = "android.net.IConnectivityManager";
    
    static final int TRANSACTION_addVpnAddress = 73;
    
    static final int TRANSACTION_checkMobileProvisioning = 47;
    
    static final int TRANSACTION_declareNetworkRequestUnfulfillable = 56;
    
    static final int TRANSACTION_deleteVpnProfile = 35;
    
    static final int TRANSACTION_establishVpn = 33;
    
    static final int TRANSACTION_factoryReset = 76;
    
    static final int TRANSACTION_getActiveLinkProperties = 12;
    
    static final int TRANSACTION_getActiveNetwork = 1;
    
    static final int TRANSACTION_getActiveNetworkForUid = 2;
    
    static final int TRANSACTION_getActiveNetworkInfo = 3;
    
    static final int TRANSACTION_getActiveNetworkInfoForUid = 4;
    
    static final int TRANSACTION_getActiveNetworkQuotaInfo = 17;
    
    static final int TRANSACTION_getAllNetworkInfo = 7;
    
    static final int TRANSACTION_getAllNetworkState = 16;
    
    static final int TRANSACTION_getAllNetworks = 9;
    
    static final int TRANSACTION_getAlwaysOnVpnPackage = 44;
    
    static final int TRANSACTION_getCacheAge = 103;
    
    static final int TRANSACTION_getCaptivePortalServerUrl = 81;
    
    static final int TRANSACTION_getCelluarNetworkRequest = 91;
    
    static final int TRANSACTION_getConnectionOwnerUid = 83;
    
    static final int TRANSACTION_getCurrentTimeMillis = 104;
    
    static final int TRANSACTION_getDefaultNetworkCapabilitiesForUser = 10;
    
    static final int TRANSACTION_getDefaultRequest = 71;
    
    static final int TRANSACTION_getGlobalProxy = 28;
    
    static final int TRANSACTION_getLastTetherError = 20;
    
    static final int TRANSACTION_getLegacyVpnInfo = 40;
    
    static final int TRANSACTION_getLinkProperties = 14;
    
    static final int TRANSACTION_getLinkPropertiesForType = 13;
    
    static final int TRANSACTION_getMobileProvisioningUrl = 48;
    
    static final int TRANSACTION_getModemTxTime = 101;
    
    static final int TRANSACTION_getMultipathPreference = 70;
    
    static final int TRANSACTION_getNetworkCapabilities = 15;
    
    static final int TRANSACTION_getNetworkForType = 8;
    
    static final int TRANSACTION_getNetworkInfo = 5;
    
    static final int TRANSACTION_getNetworkInfoForUid = 6;
    
    static final int TRANSACTION_getNetworkWatchlistConfigHash = 82;
    
    static final int TRANSACTION_getProxyForNetwork = 30;
    
    static final int TRANSACTION_getRestoreDefaultNetworkDelay = 72;
    
    static final int TRANSACTION_getTelephonyPowerLost = 96;
    
    static final int TRANSACTION_getTelephonyPowerState = 94;
    
    static final int TRANSACTION_getTetherableIfaces = 21;
    
    static final int TRANSACTION_getTetherableUsbRegexs = 24;
    
    static final int TRANSACTION_getTetherableWifiRegexs = 25;
    
    static final int TRANSACTION_getTetheredIfaces = 22;
    
    static final int TRANSACTION_getTetheringErroredIfaces = 23;
    
    static final int TRANSACTION_getVpnConfig = 38;
    
    static final int TRANSACTION_getVpnLockdownWhitelist = 46;
    
    static final int TRANSACTION_hasCache = 102;
    
    static final int TRANSACTION_isActiveNetworkMetered = 18;
    
    static final int TRANSACTION_isAlreadyUpdated = 95;
    
    static final int TRANSACTION_isAlwaysOnVpnPackageSupported = 42;
    
    static final int TRANSACTION_isCallerCurrentAlwaysOnVpnApp = 84;
    
    static final int TRANSACTION_isCallerCurrentAlwaysOnVpnLockdownApp = 85;
    
    static final int TRANSACTION_isNetworkSupported = 11;
    
    static final int TRANSACTION_isVpnLockdownEnabled = 45;
    
    static final int TRANSACTION_listenForNetwork = 61;
    
    static final int TRANSACTION_measureDataState = 90;
    
    static final int TRANSACTION_oppoExecuteIPtableCmd = 105;
    
    static final int TRANSACTION_oppoFastappDnsConfig = 108;
    
    static final int TRANSACTION_oppoGetUid = 107;
    
    static final int TRANSACTION_pendingListenForNetwork = 62;
    
    static final int TRANSACTION_pendingRequestForNetwork = 59;
    
    static final int TRANSACTION_prepareVpn = 31;
    
    static final int TRANSACTION_provisionVpnProfile = 34;
    
    static final int TRANSACTION_readArpFile = 106;
    
    static final int TRANSACTION_registerConnectivityDiagnosticsCallback = 86;
    
    static final int TRANSACTION_registerNetworkAgent = 57;
    
    static final int TRANSACTION_registerNetworkFactory = 52;
    
    static final int TRANSACTION_registerNetworkProvider = 54;
    
    static final int TRANSACTION_releaseNetworkRequest = 63;
    
    static final int TRANSACTION_releasePendingNetworkRequest = 60;
    
    static final int TRANSACTION_removeVpnAddress = 74;
    
    static final int TRANSACTION_reportInetCondition = 26;
    
    static final int TRANSACTION_reportNetworkConnectivity = 27;
    
    static final int TRANSACTION_requestBandwidthUpdate = 51;
    
    static final int TRANSACTION_requestNetwork = 58;
    
    static final int TRANSACTION_requestRouteToHostAddress = 19;
    
    static final int TRANSACTION_setAcceptPartialConnectivity = 65;
    
    static final int TRANSACTION_setAcceptUnvalidated = 64;
    
    static final int TRANSACTION_setAirplaneMode = 50;
    
    static final int TRANSACTION_setAlreadyUpdated = 98;
    
    static final int TRANSACTION_setAlwaysOnVpnPackage = 43;
    
    static final int TRANSACTION_setAvoidUnvalidated = 66;
    
    static final int TRANSACTION_setGlobalProxy = 29;
    
    static final int TRANSACTION_setModemTxTime = 100;
    
    static final int TRANSACTION_setProvisioningNotificationVisible = 49;
    
    static final int TRANSACTION_setTelephonyPowerLost = 99;
    
    static final int TRANSACTION_setTelephonyPowerState = 97;
    
    static final int TRANSACTION_setUnderlyingNetworksForVpn = 75;
    
    static final int TRANSACTION_setVpnPackageAuthorization = 32;
    
    static final int TRANSACTION_shouldAvoidBadWifi = 69;
    
    static final int TRANSACTION_shouldKeepCelluarNetwork = 92;
    
    static final int TRANSACTION_simulateDataStall = 89;
    
    static final int TRANSACTION_startCaptivePortalApp = 67;
    
    static final int TRANSACTION_startCaptivePortalAppInternal = 68;
    
    static final int TRANSACTION_startLegacyVpn = 39;
    
    static final int TRANSACTION_startNattKeepalive = 77;
    
    static final int TRANSACTION_startNattKeepaliveWithFd = 78;
    
    static final int TRANSACTION_startOrGetTestNetworkService = 88;
    
    static final int TRANSACTION_startTcpKeepalive = 79;
    
    static final int TRANSACTION_startVpnProfile = 36;
    
    static final int TRANSACTION_stopKeepalive = 80;
    
    static final int TRANSACTION_stopVpnProfile = 37;
    
    static final int TRANSACTION_unregisterConnectivityDiagnosticsCallback = 87;
    
    static final int TRANSACTION_unregisterNetworkFactory = 53;
    
    static final int TRANSACTION_unregisterNetworkProvider = 55;
    
    static final int TRANSACTION_updateDataNetworkConfig = 93;
    
    static final int TRANSACTION_updateLockdownVpn = 41;
    
    public Stub() {
      attachInterface(this, "android.net.IConnectivityManager");
    }
    
    public static IConnectivityManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.IConnectivityManager");
      if (iInterface != null && iInterface instanceof IConnectivityManager)
        return (IConnectivityManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 108:
          return "oppoFastappDnsConfig";
        case 107:
          return "oppoGetUid";
        case 106:
          return "readArpFile";
        case 105:
          return "oppoExecuteIPtableCmd";
        case 104:
          return "getCurrentTimeMillis";
        case 103:
          return "getCacheAge";
        case 102:
          return "hasCache";
        case 101:
          return "getModemTxTime";
        case 100:
          return "setModemTxTime";
        case 99:
          return "setTelephonyPowerLost";
        case 98:
          return "setAlreadyUpdated";
        case 97:
          return "setTelephonyPowerState";
        case 96:
          return "getTelephonyPowerLost";
        case 95:
          return "isAlreadyUpdated";
        case 94:
          return "getTelephonyPowerState";
        case 93:
          return "updateDataNetworkConfig";
        case 92:
          return "shouldKeepCelluarNetwork";
        case 91:
          return "getCelluarNetworkRequest";
        case 90:
          return "measureDataState";
        case 89:
          return "simulateDataStall";
        case 88:
          return "startOrGetTestNetworkService";
        case 87:
          return "unregisterConnectivityDiagnosticsCallback";
        case 86:
          return "registerConnectivityDiagnosticsCallback";
        case 85:
          return "isCallerCurrentAlwaysOnVpnLockdownApp";
        case 84:
          return "isCallerCurrentAlwaysOnVpnApp";
        case 83:
          return "getConnectionOwnerUid";
        case 82:
          return "getNetworkWatchlistConfigHash";
        case 81:
          return "getCaptivePortalServerUrl";
        case 80:
          return "stopKeepalive";
        case 79:
          return "startTcpKeepalive";
        case 78:
          return "startNattKeepaliveWithFd";
        case 77:
          return "startNattKeepalive";
        case 76:
          return "factoryReset";
        case 75:
          return "setUnderlyingNetworksForVpn";
        case 74:
          return "removeVpnAddress";
        case 73:
          return "addVpnAddress";
        case 72:
          return "getRestoreDefaultNetworkDelay";
        case 71:
          return "getDefaultRequest";
        case 70:
          return "getMultipathPreference";
        case 69:
          return "shouldAvoidBadWifi";
        case 68:
          return "startCaptivePortalAppInternal";
        case 67:
          return "startCaptivePortalApp";
        case 66:
          return "setAvoidUnvalidated";
        case 65:
          return "setAcceptPartialConnectivity";
        case 64:
          return "setAcceptUnvalidated";
        case 63:
          return "releaseNetworkRequest";
        case 62:
          return "pendingListenForNetwork";
        case 61:
          return "listenForNetwork";
        case 60:
          return "releasePendingNetworkRequest";
        case 59:
          return "pendingRequestForNetwork";
        case 58:
          return "requestNetwork";
        case 57:
          return "registerNetworkAgent";
        case 56:
          return "declareNetworkRequestUnfulfillable";
        case 55:
          return "unregisterNetworkProvider";
        case 54:
          return "registerNetworkProvider";
        case 53:
          return "unregisterNetworkFactory";
        case 52:
          return "registerNetworkFactory";
        case 51:
          return "requestBandwidthUpdate";
        case 50:
          return "setAirplaneMode";
        case 49:
          return "setProvisioningNotificationVisible";
        case 48:
          return "getMobileProvisioningUrl";
        case 47:
          return "checkMobileProvisioning";
        case 46:
          return "getVpnLockdownWhitelist";
        case 45:
          return "isVpnLockdownEnabled";
        case 44:
          return "getAlwaysOnVpnPackage";
        case 43:
          return "setAlwaysOnVpnPackage";
        case 42:
          return "isAlwaysOnVpnPackageSupported";
        case 41:
          return "updateLockdownVpn";
        case 40:
          return "getLegacyVpnInfo";
        case 39:
          return "startLegacyVpn";
        case 38:
          return "getVpnConfig";
        case 37:
          return "stopVpnProfile";
        case 36:
          return "startVpnProfile";
        case 35:
          return "deleteVpnProfile";
        case 34:
          return "provisionVpnProfile";
        case 33:
          return "establishVpn";
        case 32:
          return "setVpnPackageAuthorization";
        case 31:
          return "prepareVpn";
        case 30:
          return "getProxyForNetwork";
        case 29:
          return "setGlobalProxy";
        case 28:
          return "getGlobalProxy";
        case 27:
          return "reportNetworkConnectivity";
        case 26:
          return "reportInetCondition";
        case 25:
          return "getTetherableWifiRegexs";
        case 24:
          return "getTetherableUsbRegexs";
        case 23:
          return "getTetheringErroredIfaces";
        case 22:
          return "getTetheredIfaces";
        case 21:
          return "getTetherableIfaces";
        case 20:
          return "getLastTetherError";
        case 19:
          return "requestRouteToHostAddress";
        case 18:
          return "isActiveNetworkMetered";
        case 17:
          return "getActiveNetworkQuotaInfo";
        case 16:
          return "getAllNetworkState";
        case 15:
          return "getNetworkCapabilities";
        case 14:
          return "getLinkProperties";
        case 13:
          return "getLinkPropertiesForType";
        case 12:
          return "getActiveLinkProperties";
        case 11:
          return "isNetworkSupported";
        case 10:
          return "getDefaultNetworkCapabilitiesForUser";
        case 9:
          return "getAllNetworks";
        case 8:
          return "getNetworkForType";
        case 7:
          return "getAllNetworkInfo";
        case 6:
          return "getNetworkInfoForUid";
        case 5:
          return "getNetworkInfo";
        case 4:
          return "getActiveNetworkInfoForUid";
        case 3:
          return "getActiveNetworkInfo";
        case 2:
          return "getActiveNetworkForUid";
        case 1:
          break;
      } 
      return "getActiveNetwork";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool15;
        int i10;
        boolean bool14;
        int i9;
        boolean bool13;
        int i8;
        boolean bool12;
        int i7;
        boolean bool11;
        int i6;
        boolean bool10;
        int i5;
        boolean bool9;
        int i4;
        boolean bool8;
        int i3;
        boolean bool7;
        int i2;
        boolean bool6;
        int i1;
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        int i;
        String str16;
        List<String> list2;
        String str15;
        long[] arrayOfLong;
        String str14;
        NetworkRequest networkRequest5;
        IBinder iBinder1;
        IConnectivityDiagnosticsCallback iConnectivityDiagnosticsCallback1;
        String str13;
        byte[] arrayOfByte2;
        String str12;
        ISocketKeepaliveCallback iSocketKeepaliveCallback1;
        String str11;
        Network[] arrayOfNetwork2;
        NetworkRequest networkRequest4;
        String str10;
        NetworkRequest networkRequest3;
        String str9;
        NetworkRequest networkRequest2;
        String str8;
        NetworkRequest networkRequest1;
        Network network3;
        String str7;
        List<String> list1;
        String str6;
        ArrayList<String> arrayList;
        String str5;
        LegacyVpnInfo legacyVpnInfo;
        VpnConfig vpnConfig;
        String str4;
        ParcelFileDescriptor parcelFileDescriptor;
        ProxyInfo proxyInfo;
        String arrayOfString1[], str3;
        byte[] arrayOfByte1;
        NetworkQuotaInfo networkQuotaInfo;
        NetworkState[] arrayOfNetworkState;
        String str2;
        NetworkCapabilities networkCapabilities;
        LinkProperties linkProperties;
        String str1;
        NetworkCapabilities[] arrayOfNetworkCapabilities;
        Network arrayOfNetwork1[], network2;
        NetworkInfo arrayOfNetworkInfo[], networkInfo;
        String arrayOfString2[], str17, arrayOfString3[];
        IConnectivityDiagnosticsCallback iConnectivityDiagnosticsCallback2;
        FileDescriptor fileDescriptor1;
        ISocketKeepaliveCallback iSocketKeepaliveCallback2;
        String str18;
        long l;
        double d;
        FileDescriptor fileDescriptor2;
        String str19;
        IBinder iBinder2;
        String str20;
        NetworkAgentConfig networkAgentConfig;
        boolean bool16 = false, bool17 = false, bool18 = false, bool19 = false, bool20 = false, bool21 = false, bool22 = false, bool23 = false, bool24 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 108:
            param1Parcel1.enforceInterface("android.net.IConnectivityManager");
            arrayOfString2 = param1Parcel1.createStringArray();
            arrayOfString3 = param1Parcel1.createStringArray();
            bool17 = bool24;
            if (param1Parcel1.readInt() != 0)
              bool17 = true; 
            oppoFastappDnsConfig(arrayOfString2, arrayOfString3, bool17);
            param1Parcel2.writeNoException();
            return true;
          case 107:
            param1Parcel1.enforceInterface("android.net.IConnectivityManager");
            str17 = param1Parcel1.readString();
            str16 = param1Parcel1.readString();
            str16 = oppoGetUid(str17, str16);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str16);
            return true;
          case 106:
            str16.enforceInterface("android.net.IConnectivityManager");
            list2 = readArpFile();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list2);
            return true;
          case 105:
            list2.enforceInterface("android.net.IConnectivityManager");
            str17 = list2.readString();
            str15 = list2.readString();
            bool15 = oppoExecuteIPtableCmd(str17, str15);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 104:
            str15.enforceInterface("android.net.IConnectivityManager");
            l = getCurrentTimeMillis();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 103:
            str15.enforceInterface("android.net.IConnectivityManager");
            l = getCacheAge();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 102:
            str15.enforceInterface("android.net.IConnectivityManager");
            bool15 = hasCache();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 101:
            str15.enforceInterface("android.net.IConnectivityManager");
            arrayOfLong = getModemTxTime();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLongArray(arrayOfLong);
            return true;
          case 100:
            arrayOfLong.enforceInterface("android.net.IConnectivityManager");
            arrayOfLong = arrayOfLong.createLongArray();
            setModemTxTime(arrayOfLong);
            param1Parcel2.writeNoException();
            return true;
          case 99:
            arrayOfLong.enforceInterface("android.net.IConnectivityManager");
            d = arrayOfLong.readDouble();
            setTelephonyPowerLost(d);
            param1Parcel2.writeNoException();
            return true;
          case 98:
            arrayOfLong.enforceInterface("android.net.IConnectivityManager");
            bool17 = bool16;
            if (arrayOfLong.readInt() != 0)
              bool17 = true; 
            setAlreadyUpdated(bool17);
            param1Parcel2.writeNoException();
            return true;
          case 97:
            arrayOfLong.enforceInterface("android.net.IConnectivityManager");
            str14 = arrayOfLong.readString();
            setTelephonyPowerState(str14);
            param1Parcel2.writeNoException();
            return true;
          case 96:
            str14.enforceInterface("android.net.IConnectivityManager");
            d = getTelephonyPowerLost();
            param1Parcel2.writeNoException();
            param1Parcel2.writeDouble(d);
            return true;
          case 95:
            str14.enforceInterface("android.net.IConnectivityManager");
            bool15 = isAlreadyUpdated();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 94:
            str14.enforceInterface("android.net.IConnectivityManager");
            str14 = getTelephonyPowerState();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str14);
            return true;
          case 93:
            str14.enforceInterface("android.net.IConnectivityManager");
            str17 = str14.readString();
            str14 = str14.readString();
            updateDataNetworkConfig(str17, str14);
            param1Parcel2.writeNoException();
            return true;
          case 92:
            str14.enforceInterface("android.net.IConnectivityManager");
            if (str14.readInt() != 0)
              bool17 = true; 
            bool15 = shouldKeepCelluarNetwork(bool17);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 91:
            str14.enforceInterface("android.net.IConnectivityManager");
            networkRequest5 = getCelluarNetworkRequest();
            param1Parcel2.writeNoException();
            if (networkRequest5 != null) {
              param1Parcel2.writeInt(1);
              networkRequest5.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 90:
            networkRequest5.enforceInterface("android.net.IConnectivityManager");
            i10 = networkRequest5.readInt();
            bool14 = measureDataState(i10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool14);
            return true;
          case 89:
            networkRequest5.enforceInterface("android.net.IConnectivityManager");
            i9 = networkRequest5.readInt();
            l = networkRequest5.readLong();
            if (networkRequest5.readInt() != 0) {
              Network network = Network.CREATOR.createFromParcel((Parcel)networkRequest5);
            } else {
              str17 = null;
            } 
            if (networkRequest5.readInt() != 0) {
              PersistableBundle persistableBundle = PersistableBundle.CREATOR.createFromParcel((Parcel)networkRequest5);
            } else {
              networkRequest5 = null;
            } 
            simulateDataStall(i9, l, (Network)str17, (PersistableBundle)networkRequest5);
            param1Parcel2.writeNoException();
            return true;
          case 88:
            networkRequest5.enforceInterface("android.net.IConnectivityManager");
            iBinder1 = startOrGetTestNetworkService();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStrongBinder(iBinder1);
            return true;
          case 87:
            iBinder1.enforceInterface("android.net.IConnectivityManager");
            iConnectivityDiagnosticsCallback1 = IConnectivityDiagnosticsCallback.Stub.asInterface(iBinder1.readStrongBinder());
            unregisterConnectivityDiagnosticsCallback(iConnectivityDiagnosticsCallback1);
            param1Parcel2.writeNoException();
            return true;
          case 86:
            iConnectivityDiagnosticsCallback1.enforceInterface("android.net.IConnectivityManager");
            iConnectivityDiagnosticsCallback2 = IConnectivityDiagnosticsCallback.Stub.asInterface(iConnectivityDiagnosticsCallback1.readStrongBinder());
            if (iConnectivityDiagnosticsCallback1.readInt() != 0) {
              NetworkRequest networkRequest = NetworkRequest.CREATOR.createFromParcel((Parcel)iConnectivityDiagnosticsCallback1);
            } else {
              str17 = null;
            } 
            str13 = iConnectivityDiagnosticsCallback1.readString();
            registerConnectivityDiagnosticsCallback(iConnectivityDiagnosticsCallback2, (NetworkRequest)str17, str13);
            param1Parcel2.writeNoException();
            return true;
          case 85:
            str13.enforceInterface("android.net.IConnectivityManager");
            bool13 = isCallerCurrentAlwaysOnVpnLockdownApp();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool13);
            return true;
          case 84:
            str13.enforceInterface("android.net.IConnectivityManager");
            bool13 = isCallerCurrentAlwaysOnVpnApp();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool13);
            return true;
          case 83:
            str13.enforceInterface("android.net.IConnectivityManager");
            if (str13.readInt() != 0) {
              ConnectionInfo connectionInfo = ConnectionInfo.CREATOR.createFromParcel((Parcel)str13);
            } else {
              str13 = null;
            } 
            i8 = getConnectionOwnerUid((ConnectionInfo)str13);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i8);
            return true;
          case 82:
            str13.enforceInterface("android.net.IConnectivityManager");
            arrayOfByte2 = getNetworkWatchlistConfigHash();
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte2);
            return true;
          case 81:
            arrayOfByte2.enforceInterface("android.net.IConnectivityManager");
            str12 = getCaptivePortalServerUrl();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str12);
            return true;
          case 80:
            str12.enforceInterface("android.net.IConnectivityManager");
            if (str12.readInt() != 0) {
              Network network = Network.CREATOR.createFromParcel((Parcel)str12);
            } else {
              str17 = null;
            } 
            i8 = str12.readInt();
            stopKeepalive((Network)str17, i8);
            param1Parcel2.writeNoException();
            return true;
          case 79:
            str12.enforceInterface("android.net.IConnectivityManager");
            if (str12.readInt() != 0) {
              Network network = Network.CREATOR.createFromParcel((Parcel)str12);
            } else {
              str17 = null;
            } 
            fileDescriptor1 = str12.readRawFileDescriptor();
            i8 = str12.readInt();
            iSocketKeepaliveCallback1 = ISocketKeepaliveCallback.Stub.asInterface(str12.readStrongBinder());
            startTcpKeepalive((Network)str17, fileDescriptor1, i8, iSocketKeepaliveCallback1);
            param1Parcel2.writeNoException();
            return true;
          case 78:
            iSocketKeepaliveCallback1.enforceInterface("android.net.IConnectivityManager");
            if (iSocketKeepaliveCallback1.readInt() != 0) {
              Network network = Network.CREATOR.createFromParcel((Parcel)iSocketKeepaliveCallback1);
            } else {
              str17 = null;
            } 
            fileDescriptor2 = iSocketKeepaliveCallback1.readRawFileDescriptor();
            i8 = iSocketKeepaliveCallback1.readInt();
            param1Int2 = iSocketKeepaliveCallback1.readInt();
            iSocketKeepaliveCallback2 = ISocketKeepaliveCallback.Stub.asInterface(iSocketKeepaliveCallback1.readStrongBinder());
            str20 = iSocketKeepaliveCallback1.readString();
            str11 = iSocketKeepaliveCallback1.readString();
            startNattKeepaliveWithFd((Network)str17, fileDescriptor2, i8, param1Int2, iSocketKeepaliveCallback2, str20, str11);
            param1Parcel2.writeNoException();
            return true;
          case 77:
            str11.enforceInterface("android.net.IConnectivityManager");
            if (str11.readInt() != 0) {
              Network network = Network.CREATOR.createFromParcel((Parcel)str11);
            } else {
              str17 = null;
            } 
            param1Int2 = str11.readInt();
            iSocketKeepaliveCallback2 = ISocketKeepaliveCallback.Stub.asInterface(str11.readStrongBinder());
            str19 = str11.readString();
            i8 = str11.readInt();
            str11 = str11.readString();
            startNattKeepalive((Network)str17, param1Int2, iSocketKeepaliveCallback2, str19, i8, str11);
            param1Parcel2.writeNoException();
            return true;
          case 76:
            str11.enforceInterface("android.net.IConnectivityManager");
            factoryReset();
            param1Parcel2.writeNoException();
            return true;
          case 75:
            str11.enforceInterface("android.net.IConnectivityManager");
            arrayOfNetwork2 = str11.<Network>createTypedArray(Network.CREATOR);
            bool12 = setUnderlyingNetworksForVpn(arrayOfNetwork2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool12);
            return true;
          case 74:
            arrayOfNetwork2.enforceInterface("android.net.IConnectivityManager");
            str17 = arrayOfNetwork2.readString();
            i7 = arrayOfNetwork2.readInt();
            bool11 = removeVpnAddress(str17, i7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool11);
            return true;
          case 73:
            arrayOfNetwork2.enforceInterface("android.net.IConnectivityManager");
            str17 = arrayOfNetwork2.readString();
            i6 = arrayOfNetwork2.readInt();
            bool10 = addVpnAddress(str17, i6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool10);
            return true;
          case 72:
            arrayOfNetwork2.enforceInterface("android.net.IConnectivityManager");
            i5 = arrayOfNetwork2.readInt();
            i5 = getRestoreDefaultNetworkDelay(i5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i5);
            return true;
          case 71:
            arrayOfNetwork2.enforceInterface("android.net.IConnectivityManager");
            networkRequest4 = getDefaultRequest();
            param1Parcel2.writeNoException();
            if (networkRequest4 != null) {
              param1Parcel2.writeInt(1);
              networkRequest4.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 70:
            networkRequest4.enforceInterface("android.net.IConnectivityManager");
            if (networkRequest4.readInt() != 0) {
              Network network = Network.CREATOR.createFromParcel((Parcel)networkRequest4);
            } else {
              networkRequest4 = null;
            } 
            i5 = getMultipathPreference((Network)networkRequest4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i5);
            return true;
          case 69:
            networkRequest4.enforceInterface("android.net.IConnectivityManager");
            bool9 = shouldAvoidBadWifi();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 68:
            networkRequest4.enforceInterface("android.net.IConnectivityManager");
            if (networkRequest4.readInt() != 0) {
              Network network = Network.CREATOR.createFromParcel((Parcel)networkRequest4);
            } else {
              str17 = null;
            } 
            if (networkRequest4.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)networkRequest4);
            } else {
              networkRequest4 = null;
            } 
            startCaptivePortalAppInternal((Network)str17, (Bundle)networkRequest4);
            param1Parcel2.writeNoException();
            return true;
          case 67:
            networkRequest4.enforceInterface("android.net.IConnectivityManager");
            if (networkRequest4.readInt() != 0) {
              Network network = Network.CREATOR.createFromParcel((Parcel)networkRequest4);
            } else {
              networkRequest4 = null;
            } 
            startCaptivePortalApp((Network)networkRequest4);
            param1Parcel2.writeNoException();
            return true;
          case 66:
            networkRequest4.enforceInterface("android.net.IConnectivityManager");
            if (networkRequest4.readInt() != 0) {
              Network network = Network.CREATOR.createFromParcel((Parcel)networkRequest4);
            } else {
              networkRequest4 = null;
            } 
            setAvoidUnvalidated((Network)networkRequest4);
            param1Parcel2.writeNoException();
            return true;
          case 65:
            networkRequest4.enforceInterface("android.net.IConnectivityManager");
            if (networkRequest4.readInt() != 0) {
              Network network = Network.CREATOR.createFromParcel((Parcel)networkRequest4);
            } else {
              str17 = null;
            } 
            if (networkRequest4.readInt() != 0) {
              bool17 = true;
            } else {
              bool17 = false;
            } 
            bool19 = bool18;
            if (networkRequest4.readInt() != 0)
              bool19 = true; 
            setAcceptPartialConnectivity((Network)str17, bool17, bool19);
            param1Parcel2.writeNoException();
            return true;
          case 64:
            networkRequest4.enforceInterface("android.net.IConnectivityManager");
            if (networkRequest4.readInt() != 0) {
              Network network = Network.CREATOR.createFromParcel((Parcel)networkRequest4);
            } else {
              str17 = null;
            } 
            if (networkRequest4.readInt() != 0) {
              bool17 = true;
            } else {
              bool17 = false;
            } 
            if (networkRequest4.readInt() != 0)
              bool19 = true; 
            setAcceptUnvalidated((Network)str17, bool17, bool19);
            param1Parcel2.writeNoException();
            return true;
          case 63:
            networkRequest4.enforceInterface("android.net.IConnectivityManager");
            if (networkRequest4.readInt() != 0) {
              networkRequest4 = NetworkRequest.CREATOR.createFromParcel((Parcel)networkRequest4);
            } else {
              networkRequest4 = null;
            } 
            releaseNetworkRequest(networkRequest4);
            param1Parcel2.writeNoException();
            return true;
          case 62:
            networkRequest4.enforceInterface("android.net.IConnectivityManager");
            if (networkRequest4.readInt() != 0) {
              NetworkCapabilities networkCapabilities1 = NetworkCapabilities.CREATOR.createFromParcel((Parcel)networkRequest4);
            } else {
              str17 = null;
            } 
            if (networkRequest4.readInt() != 0) {
              PendingIntent pendingIntent = PendingIntent.CREATOR.createFromParcel((Parcel)networkRequest4);
            } else {
              iSocketKeepaliveCallback2 = null;
            } 
            str10 = networkRequest4.readString();
            pendingListenForNetwork((NetworkCapabilities)str17, (PendingIntent)iSocketKeepaliveCallback2, str10);
            param1Parcel2.writeNoException();
            return true;
          case 61:
            str10.enforceInterface("android.net.IConnectivityManager");
            if (str10.readInt() != 0) {
              NetworkCapabilities networkCapabilities1 = NetworkCapabilities.CREATOR.createFromParcel((Parcel)str10);
            } else {
              str17 = null;
            } 
            if (str10.readInt() != 0) {
              Messenger messenger = Messenger.CREATOR.createFromParcel((Parcel)str10);
            } else {
              iSocketKeepaliveCallback2 = null;
            } 
            iBinder2 = str10.readStrongBinder();
            str10 = str10.readString();
            networkRequest3 = listenForNetwork((NetworkCapabilities)str17, (Messenger)iSocketKeepaliveCallback2, iBinder2, str10);
            param1Parcel2.writeNoException();
            if (networkRequest3 != null) {
              param1Parcel2.writeInt(1);
              networkRequest3.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 60:
            networkRequest3.enforceInterface("android.net.IConnectivityManager");
            if (networkRequest3.readInt() != 0) {
              PendingIntent pendingIntent = PendingIntent.CREATOR.createFromParcel((Parcel)networkRequest3);
            } else {
              networkRequest3 = null;
            } 
            releasePendingNetworkRequest((PendingIntent)networkRequest3);
            param1Parcel2.writeNoException();
            return true;
          case 59:
            networkRequest3.enforceInterface("android.net.IConnectivityManager");
            if (networkRequest3.readInt() != 0) {
              NetworkCapabilities networkCapabilities1 = NetworkCapabilities.CREATOR.createFromParcel((Parcel)networkRequest3);
            } else {
              str17 = null;
            } 
            if (networkRequest3.readInt() != 0) {
              PendingIntent pendingIntent = PendingIntent.CREATOR.createFromParcel((Parcel)networkRequest3);
            } else {
              iSocketKeepaliveCallback2 = null;
            } 
            str9 = networkRequest3.readString();
            networkRequest2 = pendingRequestForNetwork((NetworkCapabilities)str17, (PendingIntent)iSocketKeepaliveCallback2, str9);
            param1Parcel2.writeNoException();
            if (networkRequest2 != null) {
              param1Parcel2.writeInt(1);
              networkRequest2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 58:
            networkRequest2.enforceInterface("android.net.IConnectivityManager");
            if (networkRequest2.readInt() != 0) {
              NetworkCapabilities networkCapabilities1 = NetworkCapabilities.CREATOR.createFromParcel((Parcel)networkRequest2);
            } else {
              str17 = null;
            } 
            if (networkRequest2.readInt() != 0) {
              Messenger messenger = Messenger.CREATOR.createFromParcel((Parcel)networkRequest2);
            } else {
              iSocketKeepaliveCallback2 = null;
            } 
            i4 = networkRequest2.readInt();
            iBinder2 = networkRequest2.readStrongBinder();
            param1Int2 = networkRequest2.readInt();
            str8 = networkRequest2.readString();
            networkRequest1 = requestNetwork((NetworkCapabilities)str17, (Messenger)iSocketKeepaliveCallback2, i4, iBinder2, param1Int2, str8);
            param1Parcel2.writeNoException();
            if (networkRequest1 != null) {
              param1Parcel2.writeInt(1);
              networkRequest1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 57:
            networkRequest1.enforceInterface("android.net.IConnectivityManager");
            if (networkRequest1.readInt() != 0) {
              Messenger messenger = Messenger.CREATOR.createFromParcel((Parcel)networkRequest1);
            } else {
              str17 = null;
            } 
            if (networkRequest1.readInt() != 0) {
              NetworkInfo networkInfo1 = NetworkInfo.CREATOR.createFromParcel((Parcel)networkRequest1);
            } else {
              iSocketKeepaliveCallback2 = null;
            } 
            if (networkRequest1.readInt() != 0) {
              LinkProperties linkProperties1 = LinkProperties.CREATOR.createFromParcel((Parcel)networkRequest1);
            } else {
              iBinder2 = null;
            } 
            if (networkRequest1.readInt() != 0) {
              NetworkCapabilities networkCapabilities1 = NetworkCapabilities.CREATOR.createFromParcel((Parcel)networkRequest1);
            } else {
              str20 = null;
            } 
            param1Int2 = networkRequest1.readInt();
            if (networkRequest1.readInt() != 0) {
              networkAgentConfig = NetworkAgentConfig.CREATOR.createFromParcel((Parcel)networkRequest1);
            } else {
              networkAgentConfig = null;
            } 
            i4 = networkRequest1.readInt();
            network3 = registerNetworkAgent((Messenger)str17, (NetworkInfo)iSocketKeepaliveCallback2, (LinkProperties)iBinder2, (NetworkCapabilities)str20, param1Int2, networkAgentConfig, i4);
            param1Parcel2.writeNoException();
            if (network3 != null) {
              param1Parcel2.writeInt(1);
              network3.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 56:
            network3.enforceInterface("android.net.IConnectivityManager");
            if (network3.readInt() != 0) {
              NetworkRequest networkRequest = NetworkRequest.CREATOR.createFromParcel((Parcel)network3);
            } else {
              network3 = null;
            } 
            declareNetworkRequestUnfulfillable((NetworkRequest)network3);
            param1Parcel2.writeNoException();
            return true;
          case 55:
            network3.enforceInterface("android.net.IConnectivityManager");
            if (network3.readInt() != 0) {
              Messenger messenger = Messenger.CREATOR.createFromParcel((Parcel)network3);
            } else {
              network3 = null;
            } 
            unregisterNetworkProvider((Messenger)network3);
            param1Parcel2.writeNoException();
            return true;
          case 54:
            network3.enforceInterface("android.net.IConnectivityManager");
            if (network3.readInt() != 0) {
              Messenger messenger = Messenger.CREATOR.createFromParcel((Parcel)network3);
            } else {
              str17 = null;
            } 
            str7 = network3.readString();
            i4 = registerNetworkProvider((Messenger)str17, str7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i4);
            return true;
          case 53:
            str7.enforceInterface("android.net.IConnectivityManager");
            if (str7.readInt() != 0) {
              Messenger messenger = Messenger.CREATOR.createFromParcel((Parcel)str7);
            } else {
              str7 = null;
            } 
            unregisterNetworkFactory((Messenger)str7);
            param1Parcel2.writeNoException();
            return true;
          case 52:
            str7.enforceInterface("android.net.IConnectivityManager");
            if (str7.readInt() != 0) {
              Messenger messenger = Messenger.CREATOR.createFromParcel((Parcel)str7);
            } else {
              str17 = null;
            } 
            str7 = str7.readString();
            i4 = registerNetworkFactory((Messenger)str17, str7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i4);
            return true;
          case 51:
            str7.enforceInterface("android.net.IConnectivityManager");
            if (str7.readInt() != 0) {
              Network network = Network.CREATOR.createFromParcel((Parcel)str7);
            } else {
              str7 = null;
            } 
            bool8 = requestBandwidthUpdate((Network)str7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 50:
            str7.enforceInterface("android.net.IConnectivityManager");
            bool17 = bool20;
            if (str7.readInt() != 0)
              bool17 = true; 
            setAirplaneMode(bool17);
            param1Parcel2.writeNoException();
            return true;
          case 49:
            str7.enforceInterface("android.net.IConnectivityManager");
            bool17 = bool21;
            if (str7.readInt() != 0)
              bool17 = true; 
            i3 = str7.readInt();
            str7 = str7.readString();
            setProvisioningNotificationVisible(bool17, i3, str7);
            param1Parcel2.writeNoException();
            return true;
          case 48:
            str7.enforceInterface("android.net.IConnectivityManager");
            str7 = getMobileProvisioningUrl();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str7);
            return true;
          case 47:
            str7.enforceInterface("android.net.IConnectivityManager");
            i3 = str7.readInt();
            i3 = checkMobileProvisioning(i3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i3);
            return true;
          case 46:
            str7.enforceInterface("android.net.IConnectivityManager");
            i3 = str7.readInt();
            list1 = getVpnLockdownWhitelist(i3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 45:
            list1.enforceInterface("android.net.IConnectivityManager");
            i3 = list1.readInt();
            bool7 = isVpnLockdownEnabled(i3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 44:
            list1.enforceInterface("android.net.IConnectivityManager");
            i2 = list1.readInt();
            str6 = getAlwaysOnVpnPackage(i2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str6);
            return true;
          case 43:
            str6.enforceInterface("android.net.IConnectivityManager");
            i2 = str6.readInt();
            str17 = str6.readString();
            bool17 = bool22;
            if (str6.readInt() != 0)
              bool17 = true; 
            arrayList = str6.createStringArrayList();
            bool6 = setAlwaysOnVpnPackage(i2, str17, bool17, arrayList);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 42:
            arrayList.enforceInterface("android.net.IConnectivityManager");
            i1 = arrayList.readInt();
            str5 = arrayList.readString();
            bool5 = isAlwaysOnVpnPackageSupported(i1, str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 41:
            str5.enforceInterface("android.net.IConnectivityManager");
            bool5 = updateLockdownVpn();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 40:
            str5.enforceInterface("android.net.IConnectivityManager");
            n = str5.readInt();
            legacyVpnInfo = getLegacyVpnInfo(n);
            param1Parcel2.writeNoException();
            if (legacyVpnInfo != null) {
              param1Parcel2.writeInt(1);
              legacyVpnInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 39:
            legacyVpnInfo.enforceInterface("android.net.IConnectivityManager");
            if (legacyVpnInfo.readInt() != 0) {
              VpnProfile vpnProfile = VpnProfile.CREATOR.createFromParcel((Parcel)legacyVpnInfo);
            } else {
              legacyVpnInfo = null;
            } 
            startLegacyVpn((VpnProfile)legacyVpnInfo);
            param1Parcel2.writeNoException();
            return true;
          case 38:
            legacyVpnInfo.enforceInterface("android.net.IConnectivityManager");
            n = legacyVpnInfo.readInt();
            vpnConfig = getVpnConfig(n);
            param1Parcel2.writeNoException();
            if (vpnConfig != null) {
              param1Parcel2.writeInt(1);
              vpnConfig.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 37:
            vpnConfig.enforceInterface("android.net.IConnectivityManager");
            str4 = vpnConfig.readString();
            stopVpnProfile(str4);
            param1Parcel2.writeNoException();
            return true;
          case 36:
            str4.enforceInterface("android.net.IConnectivityManager");
            str4 = str4.readString();
            startVpnProfile(str4);
            param1Parcel2.writeNoException();
            return true;
          case 35:
            str4.enforceInterface("android.net.IConnectivityManager");
            str4 = str4.readString();
            deleteVpnProfile(str4);
            param1Parcel2.writeNoException();
            return true;
          case 34:
            str4.enforceInterface("android.net.IConnectivityManager");
            if (str4.readInt() != 0) {
              VpnProfile vpnProfile = VpnProfile.CREATOR.createFromParcel((Parcel)str4);
            } else {
              str17 = null;
            } 
            str4 = str4.readString();
            bool4 = provisionVpnProfile((VpnProfile)str17, str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 33:
            str4.enforceInterface("android.net.IConnectivityManager");
            if (str4.readInt() != 0) {
              VpnConfig vpnConfig1 = VpnConfig.CREATOR.createFromParcel((Parcel)str4);
            } else {
              str4 = null;
            } 
            parcelFileDescriptor = establishVpn((VpnConfig)str4);
            param1Parcel2.writeNoException();
            if (parcelFileDescriptor != null) {
              param1Parcel2.writeInt(1);
              parcelFileDescriptor.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 32:
            parcelFileDescriptor.enforceInterface("android.net.IConnectivityManager");
            str17 = parcelFileDescriptor.readString();
            m = parcelFileDescriptor.readInt();
            param1Int2 = parcelFileDescriptor.readInt();
            setVpnPackageAuthorization(str17, m, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 31:
            parcelFileDescriptor.enforceInterface("android.net.IConnectivityManager");
            str17 = parcelFileDescriptor.readString();
            str18 = parcelFileDescriptor.readString();
            m = parcelFileDescriptor.readInt();
            bool3 = prepareVpn(str17, str18, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 30:
            parcelFileDescriptor.enforceInterface("android.net.IConnectivityManager");
            if (parcelFileDescriptor.readInt() != 0) {
              Network network = Network.CREATOR.createFromParcel((Parcel)parcelFileDescriptor);
            } else {
              parcelFileDescriptor = null;
            } 
            proxyInfo = getProxyForNetwork((Network)parcelFileDescriptor);
            param1Parcel2.writeNoException();
            if (proxyInfo != null) {
              param1Parcel2.writeInt(1);
              proxyInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 29:
            proxyInfo.enforceInterface("android.net.IConnectivityManager");
            if (proxyInfo.readInt() != 0) {
              proxyInfo = ProxyInfo.CREATOR.createFromParcel((Parcel)proxyInfo);
            } else {
              proxyInfo = null;
            } 
            setGlobalProxy(proxyInfo);
            param1Parcel2.writeNoException();
            return true;
          case 28:
            proxyInfo.enforceInterface("android.net.IConnectivityManager");
            proxyInfo = getGlobalProxy();
            param1Parcel2.writeNoException();
            if (proxyInfo != null) {
              param1Parcel2.writeInt(1);
              proxyInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 27:
            proxyInfo.enforceInterface("android.net.IConnectivityManager");
            if (proxyInfo.readInt() != 0) {
              Network network = Network.CREATOR.createFromParcel((Parcel)proxyInfo);
            } else {
              str17 = null;
            } 
            bool17 = bool23;
            if (proxyInfo.readInt() != 0)
              bool17 = true; 
            reportNetworkConnectivity((Network)str17, bool17);
            param1Parcel2.writeNoException();
            return true;
          case 26:
            proxyInfo.enforceInterface("android.net.IConnectivityManager");
            param1Int2 = proxyInfo.readInt();
            k = proxyInfo.readInt();
            reportInetCondition(param1Int2, k);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            proxyInfo.enforceInterface("android.net.IConnectivityManager");
            arrayOfString1 = getTetherableWifiRegexs();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString1);
            return true;
          case 24:
            arrayOfString1.enforceInterface("android.net.IConnectivityManager");
            arrayOfString1 = getTetherableUsbRegexs();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString1);
            return true;
          case 23:
            arrayOfString1.enforceInterface("android.net.IConnectivityManager");
            arrayOfString1 = getTetheringErroredIfaces();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString1);
            return true;
          case 22:
            arrayOfString1.enforceInterface("android.net.IConnectivityManager");
            arrayOfString1 = getTetheredIfaces();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString1);
            return true;
          case 21:
            arrayOfString1.enforceInterface("android.net.IConnectivityManager");
            arrayOfString1 = getTetherableIfaces();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString1);
            return true;
          case 20:
            arrayOfString1.enforceInterface("android.net.IConnectivityManager");
            str3 = arrayOfString1.readString();
            k = getLastTetherError(str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 19:
            str3.enforceInterface("android.net.IConnectivityManager");
            k = str3.readInt();
            arrayOfByte1 = str3.createByteArray();
            bool2 = requestRouteToHostAddress(k, arrayOfByte1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 18:
            arrayOfByte1.enforceInterface("android.net.IConnectivityManager");
            bool2 = isActiveNetworkMetered();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 17:
            arrayOfByte1.enforceInterface("android.net.IConnectivityManager");
            networkQuotaInfo = getActiveNetworkQuotaInfo();
            param1Parcel2.writeNoException();
            if (networkQuotaInfo != null) {
              param1Parcel2.writeInt(1);
              networkQuotaInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 16:
            networkQuotaInfo.enforceInterface("android.net.IConnectivityManager");
            arrayOfNetworkState = getAllNetworkState();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray(arrayOfNetworkState, 1);
            return true;
          case 15:
            arrayOfNetworkState.enforceInterface("android.net.IConnectivityManager");
            if (arrayOfNetworkState.readInt() != 0) {
              Network network = Network.CREATOR.createFromParcel((Parcel)arrayOfNetworkState);
            } else {
              str17 = null;
            } 
            str2 = arrayOfNetworkState.readString();
            networkCapabilities = getNetworkCapabilities((Network)str17, str2);
            param1Parcel2.writeNoException();
            if (networkCapabilities != null) {
              param1Parcel2.writeInt(1);
              networkCapabilities.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 14:
            networkCapabilities.enforceInterface("android.net.IConnectivityManager");
            if (networkCapabilities.readInt() != 0) {
              Network network = Network.CREATOR.createFromParcel((Parcel)networkCapabilities);
            } else {
              networkCapabilities = null;
            } 
            linkProperties = getLinkProperties((Network)networkCapabilities);
            param1Parcel2.writeNoException();
            if (linkProperties != null) {
              param1Parcel2.writeInt(1);
              linkProperties.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 13:
            linkProperties.enforceInterface("android.net.IConnectivityManager");
            j = linkProperties.readInt();
            linkProperties = getLinkPropertiesForType(j);
            param1Parcel2.writeNoException();
            if (linkProperties != null) {
              param1Parcel2.writeInt(1);
              linkProperties.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 12:
            linkProperties.enforceInterface("android.net.IConnectivityManager");
            linkProperties = getActiveLinkProperties();
            param1Parcel2.writeNoException();
            if (linkProperties != null) {
              param1Parcel2.writeInt(1);
              linkProperties.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 11:
            linkProperties.enforceInterface("android.net.IConnectivityManager");
            j = linkProperties.readInt();
            bool1 = isNetworkSupported(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 10:
            linkProperties.enforceInterface("android.net.IConnectivityManager");
            i = linkProperties.readInt();
            str1 = linkProperties.readString();
            arrayOfNetworkCapabilities = getDefaultNetworkCapabilitiesForUser(i, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray(arrayOfNetworkCapabilities, 1);
            return true;
          case 9:
            arrayOfNetworkCapabilities.enforceInterface("android.net.IConnectivityManager");
            arrayOfNetwork1 = getAllNetworks();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray(arrayOfNetwork1, 1);
            return true;
          case 8:
            arrayOfNetwork1.enforceInterface("android.net.IConnectivityManager");
            i = arrayOfNetwork1.readInt();
            network2 = getNetworkForType(i);
            param1Parcel2.writeNoException();
            if (network2 != null) {
              param1Parcel2.writeInt(1);
              network2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 7:
            network2.enforceInterface("android.net.IConnectivityManager");
            arrayOfNetworkInfo = getAllNetworkInfo();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray(arrayOfNetworkInfo, 1);
            return true;
          case 6:
            arrayOfNetworkInfo.enforceInterface("android.net.IConnectivityManager");
            if (arrayOfNetworkInfo.readInt() != 0) {
              Network network = Network.CREATOR.createFromParcel((Parcel)arrayOfNetworkInfo);
            } else {
              str17 = null;
            } 
            i = arrayOfNetworkInfo.readInt();
            if (arrayOfNetworkInfo.readInt() != 0) {
              bool17 = true;
            } else {
              bool17 = false;
            } 
            networkInfo = getNetworkInfoForUid((Network)str17, i, bool17);
            param1Parcel2.writeNoException();
            if (networkInfo != null) {
              param1Parcel2.writeInt(1);
              networkInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 5:
            networkInfo.enforceInterface("android.net.IConnectivityManager");
            i = networkInfo.readInt();
            networkInfo = getNetworkInfo(i);
            param1Parcel2.writeNoException();
            if (networkInfo != null) {
              param1Parcel2.writeInt(1);
              networkInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 4:
            networkInfo.enforceInterface("android.net.IConnectivityManager");
            i = networkInfo.readInt();
            if (networkInfo.readInt() != 0) {
              bool17 = true;
            } else {
              bool17 = false;
            } 
            networkInfo = getActiveNetworkInfoForUid(i, bool17);
            param1Parcel2.writeNoException();
            if (networkInfo != null) {
              param1Parcel2.writeInt(1);
              networkInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            networkInfo.enforceInterface("android.net.IConnectivityManager");
            networkInfo = getActiveNetworkInfo();
            param1Parcel2.writeNoException();
            if (networkInfo != null) {
              param1Parcel2.writeInt(1);
              networkInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 2:
            networkInfo.enforceInterface("android.net.IConnectivityManager");
            i = networkInfo.readInt();
            if (networkInfo.readInt() != 0) {
              bool17 = true;
            } else {
              bool17 = false;
            } 
            network1 = getActiveNetworkForUid(i, bool17);
            param1Parcel2.writeNoException();
            if (network1 != null) {
              param1Parcel2.writeInt(1);
              network1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        network1.enforceInterface("android.net.IConnectivityManager");
        Network network1 = getActiveNetwork();
        param1Parcel2.writeNoException();
        if (network1 != null) {
          param1Parcel2.writeInt(1);
          network1.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      param1Parcel2.writeString("android.net.IConnectivityManager");
      return true;
    }
    
    private static class Proxy implements IConnectivityManager {
      public static IConnectivityManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.IConnectivityManager";
      }
      
      public Network getActiveNetwork() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Network network;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            network = IConnectivityManager.Stub.getDefaultImpl().getActiveNetwork();
            return network;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            network = Network.CREATOR.createFromParcel(parcel2);
          } else {
            network = null;
          } 
          return network;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Network getActiveNetworkForUid(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          Network network;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool1 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            network = IConnectivityManager.Stub.getDefaultImpl().getActiveNetworkForUid(param2Int, param2Boolean);
            return network;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            network = Network.CREATOR.createFromParcel(parcel2);
          } else {
            network = null;
          } 
          return network;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkInfo getActiveNetworkInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          NetworkInfo networkInfo;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            networkInfo = IConnectivityManager.Stub.getDefaultImpl().getActiveNetworkInfo();
            return networkInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            networkInfo = NetworkInfo.CREATOR.createFromParcel(parcel2);
          } else {
            networkInfo = null;
          } 
          return networkInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkInfo getActiveNetworkInfoForUid(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          NetworkInfo networkInfo;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool1 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            networkInfo = IConnectivityManager.Stub.getDefaultImpl().getActiveNetworkInfoForUid(param2Int, param2Boolean);
            return networkInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            networkInfo = NetworkInfo.CREATOR.createFromParcel(parcel2);
          } else {
            networkInfo = null;
          } 
          return networkInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkInfo getNetworkInfo(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          NetworkInfo networkInfo;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            networkInfo = IConnectivityManager.Stub.getDefaultImpl().getNetworkInfo(param2Int);
            return networkInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            networkInfo = NetworkInfo.CREATOR.createFromParcel(parcel2);
          } else {
            networkInfo = null;
          } 
          return networkInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkInfo getNetworkInfoForUid(Network param2Network, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = true;
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool1 && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getNetworkInfoForUid(param2Network, param2Int, param2Boolean); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            NetworkInfo networkInfo = NetworkInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2Network = null;
          } 
          return (NetworkInfo)param2Network;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkInfo[] getAllNetworkInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getAllNetworkInfo(); 
          parcel2.readException();
          return parcel2.<NetworkInfo>createTypedArray(NetworkInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Network getNetworkForType(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Network network;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            network = IConnectivityManager.Stub.getDefaultImpl().getNetworkForType(param2Int);
            return network;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            network = Network.CREATOR.createFromParcel(parcel2);
          } else {
            network = null;
          } 
          return network;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Network[] getAllNetworks() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getAllNetworks(); 
          parcel2.readException();
          return parcel2.<Network>createTypedArray(Network.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkCapabilities[] getDefaultNetworkCapabilitiesForUser(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getDefaultNetworkCapabilitiesForUser(param2Int, param2String); 
          parcel2.readException();
          return parcel2.<NetworkCapabilities>createTypedArray(NetworkCapabilities.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isNetworkSupported(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(11, parcel1, parcel2, 0);
          if (!bool2 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IConnectivityManager.Stub.getDefaultImpl().isNetworkSupported(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public LinkProperties getActiveLinkProperties() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          LinkProperties linkProperties;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            linkProperties = IConnectivityManager.Stub.getDefaultImpl().getActiveLinkProperties();
            return linkProperties;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            linkProperties = LinkProperties.CREATOR.createFromParcel(parcel2);
          } else {
            linkProperties = null;
          } 
          return linkProperties;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public LinkProperties getLinkPropertiesForType(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          LinkProperties linkProperties;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            linkProperties = IConnectivityManager.Stub.getDefaultImpl().getLinkPropertiesForType(param2Int);
            return linkProperties;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            linkProperties = LinkProperties.CREATOR.createFromParcel(parcel2);
          } else {
            linkProperties = null;
          } 
          return linkProperties;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public LinkProperties getLinkProperties(Network param2Network) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getLinkProperties(param2Network); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            LinkProperties linkProperties = LinkProperties.CREATOR.createFromParcel(parcel2);
          } else {
            param2Network = null;
          } 
          return (LinkProperties)param2Network;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkCapabilities getNetworkCapabilities(Network param2Network, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getNetworkCapabilities(param2Network, param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            NetworkCapabilities networkCapabilities = NetworkCapabilities.CREATOR.createFromParcel(parcel2);
          } else {
            param2Network = null;
          } 
          return (NetworkCapabilities)param2Network;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkState[] getAllNetworkState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getAllNetworkState(); 
          parcel2.readException();
          return parcel2.<NetworkState>createTypedArray(NetworkState.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkQuotaInfo getActiveNetworkQuotaInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          NetworkQuotaInfo networkQuotaInfo;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            networkQuotaInfo = IConnectivityManager.Stub.getDefaultImpl().getActiveNetworkQuotaInfo();
            return networkQuotaInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            networkQuotaInfo = NetworkQuotaInfo.CREATOR.createFromParcel(parcel2);
          } else {
            networkQuotaInfo = null;
          } 
          return networkQuotaInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isActiveNetworkMetered() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(18, parcel1, parcel2, 0);
          if (!bool2 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IConnectivityManager.Stub.getDefaultImpl().isActiveNetworkMetered();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean requestRouteToHostAddress(int param2Int, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeInt(param2Int);
          parcel1.writeByteArray(param2ArrayOfbyte);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(19, parcel1, parcel2, 0);
          if (!bool2 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IConnectivityManager.Stub.getDefaultImpl().requestRouteToHostAddress(param2Int, param2ArrayOfbyte);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getLastTetherError(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getLastTetherError(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getTetherableIfaces() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getTetherableIfaces(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getTetheredIfaces() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getTetheredIfaces(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getTetheringErroredIfaces() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getTetheringErroredIfaces(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getTetherableUsbRegexs() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getTetherableUsbRegexs(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getTetherableWifiRegexs() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getTetherableWifiRegexs(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportInetCondition(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().reportInetCondition(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportNetworkConnectivity(Network param2Network, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = true;
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool1 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().reportNetworkConnectivity(param2Network, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ProxyInfo getGlobalProxy() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ProxyInfo proxyInfo;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            proxyInfo = IConnectivityManager.Stub.getDefaultImpl().getGlobalProxy();
            return proxyInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            proxyInfo = ProxyInfo.CREATOR.createFromParcel(parcel2);
          } else {
            proxyInfo = null;
          } 
          return proxyInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setGlobalProxy(ProxyInfo param2ProxyInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2ProxyInfo != null) {
            parcel1.writeInt(1);
            param2ProxyInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().setGlobalProxy(param2ProxyInfo);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ProxyInfo getProxyForNetwork(Network param2Network) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getProxyForNetwork(param2Network); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ProxyInfo proxyInfo = ProxyInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2Network = null;
          } 
          return (ProxyInfo)param2Network;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean prepareVpn(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(31, parcel1, parcel2, 0);
          if (!bool2 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IConnectivityManager.Stub.getDefaultImpl().prepareVpn(param2String1, param2String2, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVpnPackageAuthorization(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().setVpnPackageAuthorization(param2String, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParcelFileDescriptor establishVpn(VpnConfig param2VpnConfig) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2VpnConfig != null) {
            parcel1.writeInt(1);
            param2VpnConfig.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().establishVpn(param2VpnConfig); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.CREATOR.createFromParcel(parcel2);
          } else {
            param2VpnConfig = null;
          } 
          return (ParcelFileDescriptor)param2VpnConfig;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean provisionVpnProfile(VpnProfile param2VpnProfile, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool1 = true;
          if (param2VpnProfile != null) {
            parcel1.writeInt(1);
            param2VpnProfile.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool2 = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool2 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IConnectivityManager.Stub.getDefaultImpl().provisionVpnProfile(param2VpnProfile, param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteVpnProfile(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().deleteVpnProfile(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startVpnProfile(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().startVpnProfile(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopVpnProfile(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().stopVpnProfile(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public VpnConfig getVpnConfig(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          VpnConfig vpnConfig;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            vpnConfig = IConnectivityManager.Stub.getDefaultImpl().getVpnConfig(param2Int);
            return vpnConfig;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            vpnConfig = VpnConfig.CREATOR.createFromParcel(parcel2);
          } else {
            vpnConfig = null;
          } 
          return vpnConfig;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startLegacyVpn(VpnProfile param2VpnProfile) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2VpnProfile != null) {
            parcel1.writeInt(1);
            param2VpnProfile.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().startLegacyVpn(param2VpnProfile);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public LegacyVpnInfo getLegacyVpnInfo(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          LegacyVpnInfo legacyVpnInfo;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            legacyVpnInfo = IConnectivityManager.Stub.getDefaultImpl().getLegacyVpnInfo(param2Int);
            return legacyVpnInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            legacyVpnInfo = LegacyVpnInfo.CREATOR.createFromParcel(parcel2);
          } else {
            legacyVpnInfo = null;
          } 
          return legacyVpnInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean updateLockdownVpn() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(41, parcel1, parcel2, 0);
          if (!bool2 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IConnectivityManager.Stub.getDefaultImpl().updateLockdownVpn();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAlwaysOnVpnPackageSupported(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(42, parcel1, parcel2, 0);
          if (!bool2 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IConnectivityManager.Stub.getDefaultImpl().isAlwaysOnVpnPackageSupported(param2Int, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setAlwaysOnVpnPackage(int param2Int, String param2String, boolean param2Boolean, List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IConnectivityManager.Stub.getDefaultImpl().setAlwaysOnVpnPackage(param2Int, param2String, param2Boolean, param2List);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getAlwaysOnVpnPackage(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getAlwaysOnVpnPackage(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isVpnLockdownEnabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(45, parcel1, parcel2, 0);
          if (!bool2 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IConnectivityManager.Stub.getDefaultImpl().isVpnLockdownEnabled(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getVpnLockdownWhitelist(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getVpnLockdownWhitelist(param2Int); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int checkMobileProvisioning(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            param2Int = IConnectivityManager.Stub.getDefaultImpl().checkMobileProvisioning(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getMobileProvisioningUrl() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(48, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getMobileProvisioningUrl(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setProvisioningNotificationVisible(boolean param2Boolean, int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool1 = this.mRemote.transact(49, parcel1, parcel2, 0);
          if (!bool1 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().setProvisioningNotificationVisible(param2Boolean, param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAirplaneMode(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(50, parcel1, parcel2, 0);
          if (!bool1 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().setAirplaneMode(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean requestBandwidthUpdate(Network param2Network) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool1 = true;
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(51, parcel1, parcel2, 0);
          if (!bool2 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IConnectivityManager.Stub.getDefaultImpl().requestBandwidthUpdate(param2Network);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int registerNetworkFactory(Messenger param2Messenger, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2Messenger != null) {
            parcel1.writeInt(1);
            param2Messenger.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(52, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().registerNetworkFactory(param2Messenger, param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterNetworkFactory(Messenger param2Messenger) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2Messenger != null) {
            parcel1.writeInt(1);
            param2Messenger.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(53, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().unregisterNetworkFactory(param2Messenger);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int registerNetworkProvider(Messenger param2Messenger, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2Messenger != null) {
            parcel1.writeInt(1);
            param2Messenger.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(54, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().registerNetworkProvider(param2Messenger, param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterNetworkProvider(Messenger param2Messenger) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2Messenger != null) {
            parcel1.writeInt(1);
            param2Messenger.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(55, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().unregisterNetworkProvider(param2Messenger);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void declareNetworkRequestUnfulfillable(NetworkRequest param2NetworkRequest) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2NetworkRequest != null) {
            parcel1.writeInt(1);
            param2NetworkRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(56, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().declareNetworkRequestUnfulfillable(param2NetworkRequest);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Network registerNetworkAgent(Messenger param2Messenger, NetworkInfo param2NetworkInfo, LinkProperties param2LinkProperties, NetworkCapabilities param2NetworkCapabilities, int param2Int1, NetworkAgentConfig param2NetworkAgentConfig, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2Messenger != null) {
            parcel1.writeInt(1);
            param2Messenger.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2NetworkInfo != null) {
            parcel1.writeInt(1);
            param2NetworkInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2LinkProperties != null) {
            parcel1.writeInt(1);
            param2LinkProperties.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2NetworkCapabilities != null) {
            parcel1.writeInt(1);
            param2NetworkCapabilities.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          if (param2NetworkAgentConfig != null) {
            parcel1.writeInt(1);
            param2NetworkAgentConfig.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(57, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().registerNetworkAgent(param2Messenger, param2NetworkInfo, param2LinkProperties, param2NetworkCapabilities, param2Int1, param2NetworkAgentConfig, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Network network = Network.CREATOR.createFromParcel(parcel2);
          } else {
            param2Messenger = null;
          } 
          return (Network)param2Messenger;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkRequest requestNetwork(NetworkCapabilities param2NetworkCapabilities, Messenger param2Messenger, int param2Int1, IBinder param2IBinder, int param2Int2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2NetworkCapabilities != null) {
            parcel1.writeInt(1);
            param2NetworkCapabilities.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Messenger != null) {
            parcel1.writeInt(1);
            param2Messenger.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeStrongBinder(param2IBinder);
              try {
                parcel1.writeInt(param2Int2);
                try {
                  parcel1.writeString(param2String);
                  boolean bool = this.mRemote.transact(58, parcel1, parcel2, 0);
                  if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
                    NetworkRequest networkRequest = IConnectivityManager.Stub.getDefaultImpl().requestNetwork(param2NetworkCapabilities, param2Messenger, param2Int1, param2IBinder, param2Int2, param2String);
                    parcel2.recycle();
                    parcel1.recycle();
                    return networkRequest;
                  } 
                  parcel2.readException();
                  if (parcel2.readInt() != 0) {
                    NetworkRequest networkRequest = NetworkRequest.CREATOR.createFromParcel(parcel2);
                  } else {
                    param2NetworkCapabilities = null;
                  } 
                  parcel2.recycle();
                  parcel1.recycle();
                  return (NetworkRequest)param2NetworkCapabilities;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2NetworkCapabilities;
      }
      
      public NetworkRequest pendingRequestForNetwork(NetworkCapabilities param2NetworkCapabilities, PendingIntent param2PendingIntent, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2NetworkCapabilities != null) {
            parcel1.writeInt(1);
            param2NetworkCapabilities.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(59, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().pendingRequestForNetwork(param2NetworkCapabilities, param2PendingIntent, param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            NetworkRequest networkRequest = NetworkRequest.CREATOR.createFromParcel(parcel2);
          } else {
            param2NetworkCapabilities = null;
          } 
          return (NetworkRequest)param2NetworkCapabilities;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void releasePendingNetworkRequest(PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(60, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().releasePendingNetworkRequest(param2PendingIntent);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkRequest listenForNetwork(NetworkCapabilities param2NetworkCapabilities, Messenger param2Messenger, IBinder param2IBinder, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2NetworkCapabilities != null) {
            parcel1.writeInt(1);
            param2NetworkCapabilities.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Messenger != null) {
            parcel1.writeInt(1);
            param2Messenger.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(61, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().listenForNetwork(param2NetworkCapabilities, param2Messenger, param2IBinder, param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            NetworkRequest networkRequest = NetworkRequest.CREATOR.createFromParcel(parcel2);
          } else {
            param2NetworkCapabilities = null;
          } 
          return (NetworkRequest)param2NetworkCapabilities;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void pendingListenForNetwork(NetworkCapabilities param2NetworkCapabilities, PendingIntent param2PendingIntent, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2NetworkCapabilities != null) {
            parcel1.writeInt(1);
            param2NetworkCapabilities.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(62, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().pendingListenForNetwork(param2NetworkCapabilities, param2PendingIntent, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void releaseNetworkRequest(NetworkRequest param2NetworkRequest) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2NetworkRequest != null) {
            parcel1.writeInt(1);
            param2NetworkRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(63, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().releaseNetworkRequest(param2NetworkRequest);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAcceptUnvalidated(Network param2Network, boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool1 = true;
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(64, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().setAcceptUnvalidated(param2Network, param2Boolean1, param2Boolean2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAcceptPartialConnectivity(Network param2Network, boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool1 = true;
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(65, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().setAcceptPartialConnectivity(param2Network, param2Boolean1, param2Boolean2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAvoidUnvalidated(Network param2Network) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(66, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().setAvoidUnvalidated(param2Network);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startCaptivePortalApp(Network param2Network) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(67, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().startCaptivePortalApp(param2Network);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startCaptivePortalAppInternal(Network param2Network, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(68, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().startCaptivePortalAppInternal(param2Network, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean shouldAvoidBadWifi() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(69, parcel1, parcel2, 0);
          if (!bool2 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IConnectivityManager.Stub.getDefaultImpl().shouldAvoidBadWifi();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getMultipathPreference(Network param2Network) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(70, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getMultipathPreference(param2Network); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkRequest getDefaultRequest() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          NetworkRequest networkRequest;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(71, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            networkRequest = IConnectivityManager.Stub.getDefaultImpl().getDefaultRequest();
            return networkRequest;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            networkRequest = NetworkRequest.CREATOR.createFromParcel(parcel2);
          } else {
            networkRequest = null;
          } 
          return networkRequest;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getRestoreDefaultNetworkDelay(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(72, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            param2Int = IConnectivityManager.Stub.getDefaultImpl().getRestoreDefaultNetworkDelay(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean addVpnAddress(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(73, parcel1, parcel2, 0);
          if (!bool2 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IConnectivityManager.Stub.getDefaultImpl().addVpnAddress(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeVpnAddress(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(74, parcel1, parcel2, 0);
          if (!bool2 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IConnectivityManager.Stub.getDefaultImpl().removeVpnAddress(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setUnderlyingNetworksForVpn(Network[] param2ArrayOfNetwork) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool1 = false;
          parcel1.writeTypedArray(param2ArrayOfNetwork, 0);
          boolean bool2 = this.mRemote.transact(75, parcel1, parcel2, 0);
          if (!bool2 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IConnectivityManager.Stub.getDefaultImpl().setUnderlyingNetworksForVpn(param2ArrayOfNetwork);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void factoryReset() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(76, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().factoryReset();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startNattKeepalive(Network param2Network, int param2Int1, ISocketKeepaliveCallback param2ISocketKeepaliveCallback, String param2String1, int param2Int2, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          try {
            IBinder iBinder;
            parcel1.writeInt(param2Int1);
            if (param2ISocketKeepaliveCallback != null) {
              iBinder = param2ISocketKeepaliveCallback.asBinder();
            } else {
              iBinder = null;
            } 
            parcel1.writeStrongBinder(iBinder);
            try {
              parcel1.writeString(param2String1);
              try {
                parcel1.writeInt(param2Int2);
                try {
                  parcel1.writeString(param2String2);
                  try {
                    boolean bool = this.mRemote.transact(77, parcel1, parcel2, 0);
                    if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
                      IConnectivityManager.Stub.getDefaultImpl().startNattKeepalive(param2Network, param2Int1, param2ISocketKeepaliveCallback, param2String1, param2Int2, param2String2);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2Network;
      }
      
      public void startNattKeepaliveWithFd(Network param2Network, FileDescriptor param2FileDescriptor, int param2Int1, int param2Int2, ISocketKeepaliveCallback param2ISocketKeepaliveCallback, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          try {
            parcel1.writeRawFileDescriptor(param2FileDescriptor);
            try {
              parcel1.writeInt(param2Int1);
              try {
                IBinder iBinder;
                parcel1.writeInt(param2Int2);
                if (param2ISocketKeepaliveCallback != null) {
                  iBinder = param2ISocketKeepaliveCallback.asBinder();
                } else {
                  iBinder = null;
                } 
                parcel1.writeStrongBinder(iBinder);
                try {
                  parcel1.writeString(param2String1);
                  parcel1.writeString(param2String2);
                  boolean bool = this.mRemote.transact(78, parcel1, parcel2, 0);
                  if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
                    IConnectivityManager.Stub.getDefaultImpl().startNattKeepaliveWithFd(param2Network, param2FileDescriptor, param2Int1, param2Int2, param2ISocketKeepaliveCallback, param2String1, param2String2);
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } 
                  parcel2.readException();
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2Network;
      }
      
      public void startTcpKeepalive(Network param2Network, FileDescriptor param2FileDescriptor, int param2Int, ISocketKeepaliveCallback param2ISocketKeepaliveCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeRawFileDescriptor(param2FileDescriptor);
          parcel1.writeInt(param2Int);
          if (param2ISocketKeepaliveCallback != null) {
            iBinder = param2ISocketKeepaliveCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(79, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().startTcpKeepalive(param2Network, param2FileDescriptor, param2Int, param2ISocketKeepaliveCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopKeepalive(Network param2Network, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(80, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().stopKeepalive(param2Network, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getCaptivePortalServerUrl() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(81, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getCaptivePortalServerUrl(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getNetworkWatchlistConfigHash() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(82, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getNetworkWatchlistConfigHash(); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getConnectionOwnerUid(ConnectionInfo param2ConnectionInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2ConnectionInfo != null) {
            parcel1.writeInt(1);
            param2ConnectionInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(83, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getConnectionOwnerUid(param2ConnectionInfo); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isCallerCurrentAlwaysOnVpnApp() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(84, parcel1, parcel2, 0);
          if (!bool2 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IConnectivityManager.Stub.getDefaultImpl().isCallerCurrentAlwaysOnVpnApp();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isCallerCurrentAlwaysOnVpnLockdownApp() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(85, parcel1, parcel2, 0);
          if (!bool2 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IConnectivityManager.Stub.getDefaultImpl().isCallerCurrentAlwaysOnVpnLockdownApp();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerConnectivityDiagnosticsCallback(IConnectivityDiagnosticsCallback param2IConnectivityDiagnosticsCallback, NetworkRequest param2NetworkRequest, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2IConnectivityDiagnosticsCallback != null) {
            iBinder = param2IConnectivityDiagnosticsCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2NetworkRequest != null) {
            parcel1.writeInt(1);
            param2NetworkRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(86, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().registerConnectivityDiagnosticsCallback(param2IConnectivityDiagnosticsCallback, param2NetworkRequest, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterConnectivityDiagnosticsCallback(IConnectivityDiagnosticsCallback param2IConnectivityDiagnosticsCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2IConnectivityDiagnosticsCallback != null) {
            iBinder = param2IConnectivityDiagnosticsCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(87, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().unregisterConnectivityDiagnosticsCallback(param2IConnectivityDiagnosticsCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IBinder startOrGetTestNetworkService() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(88, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().startOrGetTestNetworkService(); 
          parcel2.readException();
          return parcel2.readStrongBinder();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void simulateDataStall(int param2Int, long param2Long, Network param2Network, PersistableBundle param2PersistableBundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeInt(param2Int);
          parcel1.writeLong(param2Long);
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2PersistableBundle != null) {
            parcel1.writeInt(1);
            param2PersistableBundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(89, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().simulateDataStall(param2Int, param2Long, param2Network, param2PersistableBundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean measureDataState(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(90, parcel1, parcel2, 0);
          if (!bool2 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IConnectivityManager.Stub.getDefaultImpl().measureDataState(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkRequest getCelluarNetworkRequest() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          NetworkRequest networkRequest;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(91, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            networkRequest = IConnectivityManager.Stub.getDefaultImpl().getCelluarNetworkRequest();
            return networkRequest;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            networkRequest = NetworkRequest.CREATOR.createFromParcel(parcel2);
          } else {
            networkRequest = null;
          } 
          return networkRequest;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean shouldKeepCelluarNetwork(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(92, parcel1, parcel2, 0);
          if (!bool1 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IConnectivityManager.Stub.getDefaultImpl().shouldKeepCelluarNetwork(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateDataNetworkConfig(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(93, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().updateDataNetworkConfig(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getTelephonyPowerState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(94, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getTelephonyPowerState(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAlreadyUpdated() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(95, parcel1, parcel2, 0);
          if (!bool2 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IConnectivityManager.Stub.getDefaultImpl().isAlreadyUpdated();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public double getTelephonyPowerLost() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(96, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getTelephonyPowerLost(); 
          parcel2.readException();
          return parcel2.readDouble();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTelephonyPowerState(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(97, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().setTelephonyPowerState(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAlreadyUpdated(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(98, parcel1, parcel2, 0);
          if (!bool1 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().setAlreadyUpdated(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTelephonyPowerLost(double param2Double) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeDouble(param2Double);
          boolean bool = this.mRemote.transact(99, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().setTelephonyPowerLost(param2Double);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setModemTxTime(long[] param2ArrayOflong) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeLongArray(param2ArrayOflong);
          boolean bool = this.mRemote.transact(100, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().setModemTxTime(param2ArrayOflong);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long[] getModemTxTime() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(101, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getModemTxTime(); 
          parcel2.readException();
          return parcel2.createLongArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasCache() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(102, parcel1, parcel2, 0);
          if (!bool2 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IConnectivityManager.Stub.getDefaultImpl().hasCache();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getCacheAge() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(103, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getCacheAge(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getCurrentTimeMillis() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(104, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().getCurrentTimeMillis(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean oppoExecuteIPtableCmd(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(105, parcel1, parcel2, 0);
          if (!bool2 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IConnectivityManager.Stub.getDefaultImpl().oppoExecuteIPtableCmd(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> readArpFile() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          boolean bool = this.mRemote.transact(106, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null)
            return IConnectivityManager.Stub.getDefaultImpl().readArpFile(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String oppoGetUid(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(107, parcel1, parcel2, 0);
          if (!bool && IConnectivityManager.Stub.getDefaultImpl() != null) {
            param2String1 = IConnectivityManager.Stub.getDefaultImpl().oppoGetUid(param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void oppoFastappDnsConfig(String[] param2ArrayOfString1, String[] param2ArrayOfString2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.net.IConnectivityManager");
          parcel1.writeStringArray(param2ArrayOfString1);
          parcel1.writeStringArray(param2ArrayOfString2);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(108, parcel1, parcel2, 0);
          if (!bool1 && IConnectivityManager.Stub.getDefaultImpl() != null) {
            IConnectivityManager.Stub.getDefaultImpl().oppoFastappDnsConfig(param2ArrayOfString1, param2ArrayOfString2, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IConnectivityManager param1IConnectivityManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IConnectivityManager != null) {
          Proxy.sDefaultImpl = param1IConnectivityManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IConnectivityManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
