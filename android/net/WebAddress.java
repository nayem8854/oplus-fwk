package android.net;

import android.annotation.SystemApi;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SystemApi
public class WebAddress {
  static final int MATCH_GROUP_AUTHORITY = 2;
  
  static final int MATCH_GROUP_HOST = 3;
  
  static final int MATCH_GROUP_PATH = 5;
  
  static final int MATCH_GROUP_PORT = 4;
  
  static final int MATCH_GROUP_SCHEME = 1;
  
  static Pattern sAddressPattern = Pattern.compile("(?:(http|https|file)\\:\\/\\/)?(?:([-A-Za-z0-9$_.+!*'(),;?&=]+(?:\\:[-A-Za-z0-9$_.+!*'(),;?&=]+)?)@)?([a-zA-Z0-9 -퟿豈-﷏ﷰ-￯%_-][a-zA-Z0-9 -퟿豈-﷏ﷰ-￯%_\\.-]*|\\[[0-9a-fA-F:\\.]+\\])?(?:\\:([0-9]*))?(\\/?[^#]*)?.*", 2);
  
  private String mAuthInfo;
  
  private String mHost;
  
  private String mPath;
  
  private int mPort;
  
  private String mScheme;
  
  public WebAddress(String paramString) throws ParseException {
    if (paramString != null) {
      this.mScheme = "";
      this.mHost = "";
      this.mPort = -1;
      this.mPath = "/";
      this.mAuthInfo = "";
      Matcher matcher = sAddressPattern.matcher(paramString);
      if (matcher.matches()) {
        String str = matcher.group(1);
        if (str != null)
          this.mScheme = str.toLowerCase(Locale.ROOT); 
        str = matcher.group(2);
        if (str != null)
          this.mAuthInfo = str; 
        str = matcher.group(3);
        if (str != null)
          this.mHost = str; 
        str = matcher.group(4);
        if (str != null && str.length() > 0)
          try {
            this.mPort = Integer.parseInt(str);
          } catch (NumberFormatException numberFormatException) {
            throw new ParseException("Bad port");
          }  
        str = numberFormatException.group(5);
        if (str != null && str.length() > 0)
          if (str.charAt(0) == '/') {
            this.mPath = str;
          } else {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("/");
            stringBuilder.append(str);
            this.mPath = stringBuilder.toString();
          }  
        if (this.mPort == 443 && this.mScheme.equals("")) {
          this.mScheme = "https";
        } else if (this.mPort == -1) {
          if (this.mScheme.equals("https")) {
            this.mPort = 443;
          } else {
            this.mPort = 80;
          } 
        } 
        if (this.mScheme.equals(""))
          this.mScheme = "http"; 
        return;
      } 
      throw new ParseException("Bad address");
    } 
    throw null;
  }
  
  public String toString() {
    // Byte code:
    //   0: ldc ''
    //   2: astore_1
    //   3: aload_0
    //   4: getfield mPort : I
    //   7: sipush #443
    //   10: if_icmpeq -> 25
    //   13: aload_0
    //   14: getfield mScheme : Ljava/lang/String;
    //   17: ldc 'https'
    //   19: invokevirtual equals : (Ljava/lang/Object;)Z
    //   22: ifne -> 52
    //   25: aload_1
    //   26: astore_2
    //   27: aload_0
    //   28: getfield mPort : I
    //   31: bipush #80
    //   33: if_icmpeq -> 84
    //   36: aload_0
    //   37: getfield mScheme : Ljava/lang/String;
    //   40: astore_3
    //   41: aload_1
    //   42: astore_2
    //   43: aload_3
    //   44: ldc 'http'
    //   46: invokevirtual equals : (Ljava/lang/Object;)Z
    //   49: ifeq -> 84
    //   52: new java/lang/StringBuilder
    //   55: dup
    //   56: invokespecial <init> : ()V
    //   59: astore_2
    //   60: aload_2
    //   61: ldc ':'
    //   63: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   66: pop
    //   67: aload_2
    //   68: aload_0
    //   69: getfield mPort : I
    //   72: invokestatic toString : (I)Ljava/lang/String;
    //   75: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   78: pop
    //   79: aload_2
    //   80: invokevirtual toString : ()Ljava/lang/String;
    //   83: astore_2
    //   84: ldc ''
    //   86: astore_1
    //   87: aload_0
    //   88: getfield mAuthInfo : Ljava/lang/String;
    //   91: invokevirtual length : ()I
    //   94: ifle -> 126
    //   97: new java/lang/StringBuilder
    //   100: dup
    //   101: invokespecial <init> : ()V
    //   104: astore_1
    //   105: aload_1
    //   106: aload_0
    //   107: getfield mAuthInfo : Ljava/lang/String;
    //   110: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   113: pop
    //   114: aload_1
    //   115: ldc '@'
    //   117: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   120: pop
    //   121: aload_1
    //   122: invokevirtual toString : ()Ljava/lang/String;
    //   125: astore_1
    //   126: new java/lang/StringBuilder
    //   129: dup
    //   130: invokespecial <init> : ()V
    //   133: astore_3
    //   134: aload_3
    //   135: aload_0
    //   136: getfield mScheme : Ljava/lang/String;
    //   139: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   142: pop
    //   143: aload_3
    //   144: ldc '://'
    //   146: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   149: pop
    //   150: aload_3
    //   151: aload_1
    //   152: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   155: pop
    //   156: aload_3
    //   157: aload_0
    //   158: getfield mHost : Ljava/lang/String;
    //   161: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   164: pop
    //   165: aload_3
    //   166: aload_2
    //   167: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   170: pop
    //   171: aload_3
    //   172: aload_0
    //   173: getfield mPath : Ljava/lang/String;
    //   176: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   179: pop
    //   180: aload_3
    //   181: invokevirtual toString : ()Ljava/lang/String;
    //   184: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #139	-> 0
    //   #140	-> 3
    //   #141	-> 41
    //   #142	-> 52
    //   #144	-> 84
    //   #145	-> 87
    //   #146	-> 97
    //   #149	-> 126
  }
  
  public void setScheme(String paramString) {
    this.mScheme = paramString;
  }
  
  public String getScheme() {
    return this.mScheme;
  }
  
  public void setHost(String paramString) {
    this.mHost = paramString;
  }
  
  public String getHost() {
    return this.mHost;
  }
  
  public void setPort(int paramInt) {
    this.mPort = paramInt;
  }
  
  public int getPort() {
    return this.mPort;
  }
  
  public void setPath(String paramString) {
    this.mPath = paramString;
  }
  
  public String getPath() {
    return this.mPath;
  }
  
  public void setAuthInfo(String paramString) {
    this.mAuthInfo = paramString;
  }
  
  public String getAuthInfo() {
    return this.mAuthInfo;
  }
}
