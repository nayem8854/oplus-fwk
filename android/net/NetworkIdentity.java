package android.net;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.Slog;
import android.util.proto.ProtoOutputStream;
import java.util.Objects;

public class NetworkIdentity implements Comparable<NetworkIdentity> {
  public static final int SUBTYPE_COMBINED = -1;
  
  private static final String TAG = "NetworkIdentity";
  
  final boolean mDefaultNetwork;
  
  final boolean mMetered;
  
  final String mNetworkId;
  
  final boolean mRoaming;
  
  final int mSubType;
  
  final String mSubscriberId;
  
  final int mType;
  
  public NetworkIdentity(int paramInt1, int paramInt2, String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    this.mType = paramInt1;
    this.mSubType = paramInt2;
    this.mSubscriberId = paramString1;
    this.mNetworkId = paramString2;
    this.mRoaming = paramBoolean1;
    this.mMetered = paramBoolean2;
    this.mDefaultNetwork = paramBoolean3;
  }
  
  public int hashCode() {
    int i = this.mType, j = this.mSubType;
    String str1 = this.mSubscriberId, str2 = this.mNetworkId;
    boolean bool1 = this.mRoaming, bool2 = this.mMetered, bool3 = this.mDefaultNetwork;
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), str1, str2, Boolean.valueOf(bool1), Boolean.valueOf(bool2), Boolean.valueOf(bool3) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof NetworkIdentity;
    boolean bool1 = false;
    if (bool) {
      paramObject = paramObject;
      if (this.mType == ((NetworkIdentity)paramObject).mType && this.mSubType == ((NetworkIdentity)paramObject).mSubType && this.mRoaming == ((NetworkIdentity)paramObject).mRoaming) {
        String str1 = this.mSubscriberId, str2 = ((NetworkIdentity)paramObject).mSubscriberId;
        if (Objects.equals(str1, str2)) {
          str2 = this.mNetworkId;
          str1 = ((NetworkIdentity)paramObject).mNetworkId;
          if (Objects.equals(str2, str1) && this.mMetered == ((NetworkIdentity)paramObject).mMetered && this.mDefaultNetwork == ((NetworkIdentity)paramObject).mDefaultNetwork)
            bool1 = true; 
        } 
      } 
      return bool1;
    } 
    return false;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("{");
    stringBuilder.append("type=");
    stringBuilder.append(ConnectivityManager.getNetworkTypeName(this.mType));
    stringBuilder.append(", subType=");
    int i = this.mSubType;
    if (i == -1) {
      stringBuilder.append("COMBINED");
    } else {
      stringBuilder.append(i);
    } 
    if (this.mSubscriberId != null) {
      stringBuilder.append(", subscriberId=");
      stringBuilder.append(scrubSubscriberId(this.mSubscriberId));
    } 
    if (this.mNetworkId != null) {
      stringBuilder.append(", networkId=");
      stringBuilder.append(this.mNetworkId);
    } 
    if (this.mRoaming)
      stringBuilder.append(", ROAMING"); 
    stringBuilder.append(", metered=");
    stringBuilder.append(this.mMetered);
    stringBuilder.append(", defaultNetwork=");
    stringBuilder.append(this.mDefaultNetwork);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    paramLong = paramProtoOutputStream.start(paramLong);
    paramProtoOutputStream.write(1120986464257L, this.mType);
    String str = this.mSubscriberId;
    if (str != null)
      paramProtoOutputStream.write(1138166333442L, scrubSubscriberId(str)); 
    paramProtoOutputStream.write(1138166333443L, this.mNetworkId);
    paramProtoOutputStream.write(1133871366148L, this.mRoaming);
    paramProtoOutputStream.write(1133871366149L, this.mMetered);
    paramProtoOutputStream.write(1133871366150L, this.mDefaultNetwork);
    paramProtoOutputStream.end(paramLong);
  }
  
  public int getType() {
    return this.mType;
  }
  
  public int getSubType() {
    return this.mSubType;
  }
  
  public String getSubscriberId() {
    return this.mSubscriberId;
  }
  
  public String getNetworkId() {
    return this.mNetworkId;
  }
  
  public boolean getRoaming() {
    return this.mRoaming;
  }
  
  public boolean getMetered() {
    return this.mMetered;
  }
  
  public boolean getDefaultNetwork() {
    return this.mDefaultNetwork;
  }
  
  public static String scrubSubscriberId(String paramString) {
    if (Build.IS_ENG)
      return paramString; 
    if (paramString != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString.substring(0, Math.min(6, paramString.length())));
      stringBuilder.append("...");
      return stringBuilder.toString();
    } 
    return "null";
  }
  
  public static String[] scrubSubscriberId(String[] paramArrayOfString) {
    if (paramArrayOfString == null)
      return null; 
    String[] arrayOfString = new String[paramArrayOfString.length];
    for (byte b = 0; b < arrayOfString.length; b++)
      arrayOfString[b] = scrubSubscriberId(paramArrayOfString[b]); 
    return arrayOfString;
  }
  
  public static NetworkIdentity buildNetworkIdentity(Context paramContext, NetworkState paramNetworkState, boolean paramBoolean, int paramInt) {
    int i = paramNetworkState.networkInfo.getType();
    boolean bool1 = paramNetworkState.networkCapabilities.hasCapability(18);
    boolean bool2 = paramNetworkState.networkCapabilities.hasCapability(11);
    if (ConnectivityManager.isNetworkTypeMobile(i)) {
      if (paramNetworkState.subscriberId == null && 
        paramNetworkState.networkInfo.getState() != NetworkInfo.State.DISCONNECTED) {
        NetworkInfo networkInfo = paramNetworkState.networkInfo;
        if (networkInfo.getState() != NetworkInfo.State.UNKNOWN) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Active mobile network without subscriber! ni = ");
          stringBuilder.append(paramNetworkState.networkInfo);
          Slog.w("NetworkIdentity", stringBuilder.toString());
        } 
      } 
      String str = paramNetworkState.subscriberId;
      paramNetworkState = null;
    } else if (i == 1) {
      if (paramNetworkState.networkId != null) {
        String str = paramNetworkState.networkId;
        paramContext = null;
      } else {
        WifiManager wifiManager = (WifiManager)paramContext.getSystemService("wifi");
        WifiInfo wifiInfo1 = wifiManager.getConnectionInfo();
        if (wifiInfo1 != null) {
          String str = wifiInfo1.getSSID();
        } else {
          wifiInfo1 = null;
        } 
        WifiInfo wifiInfo3 = null, wifiInfo2 = wifiInfo1;
        wifiInfo1 = wifiInfo3;
      } 
    } else {
      paramContext = null;
      paramNetworkState = null;
    } 
    return new NetworkIdentity(i, paramInt, (String)paramContext, (String)paramNetworkState, bool1 ^ true, bool2 ^ true, paramBoolean);
  }
  
  public int compareTo(NetworkIdentity paramNetworkIdentity) {
    int i = Integer.compare(this.mType, paramNetworkIdentity.mType);
    int j = i;
    if (i == 0)
      j = Integer.compare(this.mSubType, paramNetworkIdentity.mSubType); 
    i = j;
    if (j == 0) {
      String str = this.mSubscriberId;
      i = j;
      if (str != null) {
        String str1 = paramNetworkIdentity.mSubscriberId;
        i = j;
        if (str1 != null)
          i = str.compareTo(str1); 
      } 
    } 
    j = i;
    if (i == 0) {
      String str = this.mNetworkId;
      j = i;
      if (str != null) {
        String str1 = paramNetworkIdentity.mNetworkId;
        j = i;
        if (str1 != null)
          j = str.compareTo(str1); 
      } 
    } 
    i = j;
    if (j == 0)
      i = Boolean.compare(this.mRoaming, paramNetworkIdentity.mRoaming); 
    j = i;
    if (i == 0)
      j = Boolean.compare(this.mMetered, paramNetworkIdentity.mMetered); 
    i = j;
    if (j == 0)
      i = Boolean.compare(this.mDefaultNetwork, paramNetworkIdentity.mDefaultNetwork); 
    return i;
  }
}
