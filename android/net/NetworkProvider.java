package android.net;

import android.annotation.SystemApi;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Messenger;

@SystemApi
public class NetworkProvider {
  public static final int CMD_CANCEL_REQUEST = 2;
  
  public static final int CMD_REQUEST_NETWORK = 1;
  
  public static final int FIRST_PROVIDER_ID = 1;
  
  public static final int ID_NONE = -1;
  
  public static final int ID_VPN = -2;
  
  private final ConnectivityManager mCm;
  
  private final Messenger mMessenger;
  
  private final String mName;
  
  private int mProviderId = -1;
  
  @SystemApi
  public NetworkProvider(Context paramContext, Looper paramLooper, String paramString) {
    this.mCm = ConnectivityManager.from(paramContext);
    Object object = new Object(this, paramLooper);
    this.mMessenger = new Messenger((Handler)object);
    this.mName = paramString;
  }
  
  public Messenger getMessenger() {
    return this.mMessenger;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public int getProviderId() {
    return this.mProviderId;
  }
  
  public void setProviderId(int paramInt) {
    this.mProviderId = paramInt;
  }
  
  @SystemApi
  public void onNetworkRequested(NetworkRequest paramNetworkRequest, int paramInt1, int paramInt2) {}
  
  @SystemApi
  public void onNetworkRequestWithdrawn(NetworkRequest paramNetworkRequest) {}
  
  @SystemApi
  public void declareNetworkRequestUnfulfillable(NetworkRequest paramNetworkRequest) {
    this.mCm.declareNetworkRequestUnfulfillable(paramNetworkRequest);
  }
}
