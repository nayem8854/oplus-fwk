package android.net;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

@SystemApi
public final class CaptivePortalData implements Parcelable {
  private CaptivePortalData(long paramLong1, Uri paramUri1, Uri paramUri2, boolean paramBoolean1, long paramLong2, long paramLong3, boolean paramBoolean2) {
    this.mRefreshTimeMillis = paramLong1;
    this.mUserPortalUrl = paramUri1;
    this.mVenueInfoUrl = paramUri2;
    this.mIsSessionExtendable = paramBoolean1;
    this.mByteLimit = paramLong2;
    this.mExpiryTimeMillis = paramLong3;
    this.mCaptive = paramBoolean2;
  }
  
  private CaptivePortalData(Parcel paramParcel) {
    this(l1, uri1, uri2, bool1, l2, l3, bool2);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mRefreshTimeMillis);
    paramParcel.writeParcelable(this.mUserPortalUrl, 0);
    paramParcel.writeParcelable(this.mVenueInfoUrl, 0);
    paramParcel.writeBoolean(this.mIsSessionExtendable);
    paramParcel.writeLong(this.mByteLimit);
    paramParcel.writeLong(this.mExpiryTimeMillis);
    paramParcel.writeBoolean(this.mCaptive);
  }
  
  class Builder {
    private long mBytesRemaining = -1L;
    
    private boolean mCaptive;
    
    private long mExpiryTime = -1L;
    
    private boolean mIsSessionExtendable;
    
    private long mRefreshTime;
    
    private Uri mUserPortalUrl;
    
    private Uri mVenueInfoUrl;
    
    public Builder(CaptivePortalData this$0) {
      if (this$0 == null)
        return; 
      Builder builder = setRefreshTime(this$0.mRefreshTimeMillis);
      builder = builder.setUserPortalUrl(this$0.mUserPortalUrl);
      builder = builder.setVenueInfoUrl(this$0.mVenueInfoUrl);
      builder = builder.setSessionExtendable(this$0.mIsSessionExtendable);
      builder = builder.setBytesRemaining(this$0.mByteLimit);
      builder = builder.setExpiryTime(this$0.mExpiryTimeMillis);
      builder.setCaptive(this$0.mCaptive);
    }
    
    public Builder setRefreshTime(long param1Long) {
      this.mRefreshTime = param1Long;
      return this;
    }
    
    public Builder setUserPortalUrl(Uri param1Uri) {
      this.mUserPortalUrl = param1Uri;
      return this;
    }
    
    public Builder setVenueInfoUrl(Uri param1Uri) {
      this.mVenueInfoUrl = param1Uri;
      return this;
    }
    
    public Builder setSessionExtendable(boolean param1Boolean) {
      this.mIsSessionExtendable = param1Boolean;
      return this;
    }
    
    public Builder setBytesRemaining(long param1Long) {
      this.mBytesRemaining = param1Long;
      return this;
    }
    
    public Builder setExpiryTime(long param1Long) {
      this.mExpiryTime = param1Long;
      return this;
    }
    
    public Builder setCaptive(boolean param1Boolean) {
      this.mCaptive = param1Boolean;
      return this;
    }
    
    public CaptivePortalData build() {
      return new CaptivePortalData(this.mRefreshTime, this.mUserPortalUrl, this.mVenueInfoUrl, this.mIsSessionExtendable, this.mBytesRemaining, this.mExpiryTime, this.mCaptive);
    }
    
    public Builder() {}
  }
  
  public long getRefreshTimeMillis() {
    return this.mRefreshTimeMillis;
  }
  
  public Uri getUserPortalUrl() {
    return this.mUserPortalUrl;
  }
  
  public Uri getVenueInfoUrl() {
    return this.mVenueInfoUrl;
  }
  
  public boolean isSessionExtendable() {
    return this.mIsSessionExtendable;
  }
  
  public long getByteLimit() {
    return this.mByteLimit;
  }
  
  public long getExpiryTimeMillis() {
    return this.mExpiryTimeMillis;
  }
  
  public boolean isCaptive() {
    return this.mCaptive;
  }
  
  public static final Parcelable.Creator<CaptivePortalData> CREATOR = new Parcelable.Creator<CaptivePortalData>() {
      public CaptivePortalData createFromParcel(Parcel param1Parcel) {
        return new CaptivePortalData(param1Parcel);
      }
      
      public CaptivePortalData[] newArray(int param1Int) {
        return new CaptivePortalData[param1Int];
      }
    };
  
  private final long mByteLimit;
  
  private final boolean mCaptive;
  
  private final long mExpiryTimeMillis;
  
  private final boolean mIsSessionExtendable;
  
  private final long mRefreshTimeMillis;
  
  private final Uri mUserPortalUrl;
  
  private final Uri mVenueInfoUrl;
  
  public int hashCode() {
    long l1 = this.mRefreshTimeMillis;
    Uri uri1 = this.mUserPortalUrl, uri2 = this.mVenueInfoUrl;
    boolean bool1 = this.mIsSessionExtendable;
    long l2 = this.mByteLimit, l3 = this.mExpiryTimeMillis;
    boolean bool2 = this.mCaptive;
    return Objects.hash(new Object[] { Long.valueOf(l1), uri1, uri2, Boolean.valueOf(bool1), Long.valueOf(l2), Long.valueOf(l3), Boolean.valueOf(bool2) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof CaptivePortalData;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    if (this.mRefreshTimeMillis == ((CaptivePortalData)paramObject).mRefreshTimeMillis) {
      Uri uri1 = this.mUserPortalUrl, uri2 = ((CaptivePortalData)paramObject).mUserPortalUrl;
      if (Objects.equals(uri1, uri2)) {
        uri1 = this.mVenueInfoUrl;
        uri2 = ((CaptivePortalData)paramObject).mVenueInfoUrl;
        if (Objects.equals(uri1, uri2) && this.mIsSessionExtendable == ((CaptivePortalData)paramObject).mIsSessionExtendable && this.mByteLimit == ((CaptivePortalData)paramObject).mByteLimit && this.mExpiryTimeMillis == ((CaptivePortalData)paramObject).mExpiryTimeMillis && this.mCaptive == ((CaptivePortalData)paramObject).mCaptive)
          bool1 = true; 
      } 
    } 
    return bool1;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CaptivePortalData {refreshTime: ");
    stringBuilder.append(this.mRefreshTimeMillis);
    stringBuilder.append(", userPortalUrl: ");
    stringBuilder.append(this.mUserPortalUrl);
    stringBuilder.append(", venueInfoUrl: ");
    stringBuilder.append(this.mVenueInfoUrl);
    stringBuilder.append(", isSessionExtendable: ");
    stringBuilder.append(this.mIsSessionExtendable);
    stringBuilder.append(", byteLimit: ");
    stringBuilder.append(this.mByteLimit);
    stringBuilder.append(", expiryTime: ");
    stringBuilder.append(this.mExpiryTimeMillis);
    stringBuilder.append(", captive: ");
    stringBuilder.append(this.mCaptive);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
}
