package android.net;

import android.os.SystemClock;
import android.util.Log;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Arrays;

public class SntpClient {
  private static final boolean DBG = true;
  
  private static final int NTP_LEAP_NOSYNC = 3;
  
  private static final int NTP_MODE_BROADCAST = 5;
  
  private static final int NTP_MODE_CLIENT = 3;
  
  private static final int NTP_MODE_SERVER = 4;
  
  private static final int NTP_PACKET_SIZE = 48;
  
  private static final int NTP_PORT = 123;
  
  private static final int NTP_STRATUM_DEATH = 0;
  
  private static final int NTP_STRATUM_MAX = 15;
  
  private static final int NTP_VERSION = 3;
  
  private static final long OFFSET_1900_TO_1970 = 2208988800L;
  
  private static final int ORIGINATE_TIME_OFFSET = 24;
  
  private static final int RECEIVE_TIME_OFFSET = 32;
  
  private static final int REFERENCE_TIME_OFFSET = 16;
  
  private static final String TAG = "SntpClient";
  
  private static final int TRANSMIT_TIME_OFFSET = 40;
  
  private long mNtpTime;
  
  private long mNtpTimeReference;
  
  private long mRoundTripTime;
  
  private static class InvalidServerReplyException extends Exception {
    public InvalidServerReplyException(String param1String) {
      super(param1String);
    }
  }
  
  public boolean requestTime(String paramString, int paramInt, Network paramNetwork) {
    paramNetwork = paramNetwork.getPrivateDnsBypassingCopy();
    try {
      InetAddress inetAddress = paramNetwork.getByName(paramString);
      return requestTime(inetAddress, 123, paramInt, paramNetwork);
    } catch (Exception exception) {
      EventLogTags.writeNtpFailure(paramString, exception.toString());
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("request time failed: ");
      stringBuilder.append(exception);
      Log.d("SntpClient", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean requestTime(InetAddress paramInetAddress, int paramInt1, int paramInt2, Network paramNetwork) {
    DatagramSocket datagramSocket1 = null, datagramSocket2 = null;
    int i = TrafficStats.getAndSetThreadStatsTag(-191);
    DatagramSocket datagramSocket3 = datagramSocket2, datagramSocket4 = datagramSocket1;
    try {
      DatagramSocket datagramSocket = new DatagramSocket();
      datagramSocket3 = datagramSocket2;
      datagramSocket4 = datagramSocket1;
      this();
      datagramSocket1 = datagramSocket;
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      paramNetwork.bindSocket(datagramSocket1);
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      datagramSocket1.setSoTimeout(paramInt2);
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      byte[] arrayOfByte = new byte[48];
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      DatagramPacket datagramPacket = new DatagramPacket();
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      this(arrayOfByte, arrayOfByte.length, paramInetAddress, paramInt1);
      arrayOfByte[0] = 27;
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      long l1 = System.currentTimeMillis();
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      long l2 = SystemClock.elapsedRealtime();
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      writeTimeStamp(arrayOfByte, 40, l1);
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      datagramSocket1.send(datagramPacket);
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      datagramPacket = new DatagramPacket();
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      this(arrayOfByte, arrayOfByte.length);
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      datagramSocket1.receive(datagramPacket);
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      long l3 = SystemClock.elapsedRealtime();
      l1 += l3 - l2;
      byte b1 = (byte)(arrayOfByte[0] >> 6 & 0x3);
      byte b2 = (byte)(arrayOfByte[0] & 0x7);
      paramInt1 = arrayOfByte[1];
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      long l4 = readTimeStamp(arrayOfByte, 24);
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      long l5 = readTimeStamp(arrayOfByte, 32);
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      long l6 = readTimeStamp(arrayOfByte, 40);
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      checkValidServerReply(b1, b2, paramInt1 & 0xFF, l6);
      l2 = l3 - l2 - l6 - l5;
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      l4 = (l5 - l4 + l6 - l1) / 2L;
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      EventLogTags.writeNtpSuccess(paramInetAddress.toString(), l2, l4);
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      StringBuilder stringBuilder = new StringBuilder();
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      this();
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      stringBuilder.append("round trip: ");
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      stringBuilder.append(l2);
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      stringBuilder.append("ms, clock offset: ");
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      stringBuilder.append(l4);
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      stringBuilder.append("ms");
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      Log.d("SntpClient", stringBuilder.toString());
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      this.mNtpTime = l1 + l4;
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      this.mNtpTimeReference = l3;
      datagramSocket3 = datagramSocket1;
      datagramSocket4 = datagramSocket1;
      this.mRoundTripTime = l2;
      datagramSocket1.close();
      TrafficStats.setThreadStatsTag(i);
      return true;
    } catch (Exception exception) {
      datagramSocket3 = datagramSocket4;
      EventLogTags.writeNtpFailure(paramInetAddress.toString(), exception.toString());
      datagramSocket3 = datagramSocket4;
      StringBuilder stringBuilder = new StringBuilder();
      datagramSocket3 = datagramSocket4;
      this();
      datagramSocket3 = datagramSocket4;
      stringBuilder.append("request time failed: ");
      datagramSocket3 = datagramSocket4;
      stringBuilder.append(exception);
      datagramSocket3 = datagramSocket4;
      Log.d("SntpClient", stringBuilder.toString());
      if (datagramSocket4 != null)
        datagramSocket4.close(); 
      TrafficStats.setThreadStatsTag(i);
      return false;
    } finally {}
    if (datagramSocket3 != null)
      datagramSocket3.close(); 
    TrafficStats.setThreadStatsTag(i);
    throw paramInetAddress;
  }
  
  @Deprecated
  public boolean requestTime(String paramString, int paramInt) {
    Log.w("SntpClient", "Shame on you for calling the hidden API requestTime()!");
    return false;
  }
  
  public long getNtpTime() {
    return this.mNtpTime;
  }
  
  public long getNtpTimeReference() {
    return this.mNtpTimeReference;
  }
  
  public long getRoundTripTime() {
    return this.mRoundTripTime;
  }
  
  private static void checkValidServerReply(byte paramByte1, byte paramByte2, int paramInt, long paramLong) throws InvalidServerReplyException {
    if (paramByte1 != 3) {
      if (paramByte2 == 4 || paramByte2 == 5) {
        if (paramInt != 0 && paramInt <= 15) {
          if (paramLong != 0L)
            return; 
          throw new InvalidServerReplyException("zero transmitTime");
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("untrusted stratum: ");
        stringBuilder1.append(paramInt);
        throw new InvalidServerReplyException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("untrusted mode: ");
      stringBuilder.append(paramByte2);
      throw new InvalidServerReplyException(stringBuilder.toString());
    } 
    throw new InvalidServerReplyException("unsynchronized server");
  }
  
  private long read32(byte[] paramArrayOfbyte, int paramInt) {
    byte b = paramArrayOfbyte[paramInt];
    int i = paramArrayOfbyte[paramInt + 1];
    int j = paramArrayOfbyte[paramInt + 2];
    int k = paramArrayOfbyte[paramInt + 3];
    if ((b & 0x80) == 128) {
      paramInt = (b & Byte.MAX_VALUE) + 128;
    } else {
      paramInt = b;
    } 
    if ((i & 0x80) == 128)
      i = (i & 0x7F) + 128; 
    if ((j & 0x80) == 128)
      j = (j & 0x7F) + 128; 
    if ((k & 0x80) == 128)
      k = (k & 0x7F) + 128; 
    return (paramInt << 24L) + (i << 16L) + (j << 8L) + k;
  }
  
  private long readTimeStamp(byte[] paramArrayOfbyte, int paramInt) {
    long l1 = read32(paramArrayOfbyte, paramInt);
    long l2 = read32(paramArrayOfbyte, paramInt + 4);
    if (l1 == 0L && l2 == 0L)
      return 0L; 
    return (l1 - 2208988800L) * 1000L + 1000L * l2 / 4294967296L;
  }
  
  private void writeTimeStamp(byte[] paramArrayOfbyte, int paramInt, long paramLong) {
    if (paramLong == 0L) {
      Arrays.fill(paramArrayOfbyte, paramInt, paramInt + 8, (byte)0);
      return;
    } 
    long l1 = paramLong / 1000L;
    long l2 = l1 + 2208988800L;
    int i = paramInt + 1;
    paramArrayOfbyte[paramInt] = (byte)(int)(l2 >> 24L);
    paramInt = i + 1;
    paramArrayOfbyte[i] = (byte)(int)(l2 >> 16L);
    i = paramInt + 1;
    paramArrayOfbyte[paramInt] = (byte)(int)(l2 >> 8L);
    paramInt = i + 1;
    paramArrayOfbyte[i] = (byte)(int)(l2 >> 0L);
    paramLong = 4294967296L * (paramLong - l1 * 1000L) / 1000L;
    i = paramInt + 1;
    paramArrayOfbyte[paramInt] = (byte)(int)(paramLong >> 24L);
    paramInt = i + 1;
    paramArrayOfbyte[i] = (byte)(int)(paramLong >> 16L);
    i = paramInt + 1;
    paramArrayOfbyte[paramInt] = (byte)(int)(paramLong >> 8L);
    paramArrayOfbyte[i] = (byte)(int)(Math.random() * 255.0D);
  }
}
