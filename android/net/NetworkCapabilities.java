package android.net;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.ArraySet;
import android.util.proto.ProtoOutputStream;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.BitUtils;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;

public final class NetworkCapabilities implements Parcelable {
  private static final long CONNECTIVITY_MANAGED_CAPABILITIES = 17498112L;
  
  public static final Parcelable.Creator<NetworkCapabilities> CREATOR;
  
  private static final long DEFAULT_CAPABILITIES = 57344L;
  
  private static final long FORCE_RESTRICTED_CAPABILITIES = 4194304L;
  
  public static final int LINK_BANDWIDTH_UNSPECIFIED = 0;
  
  private static final int MAX_NET_CAPABILITY = 30;
  
  public static final int MAX_TRANSPORT = 7;
  
  private static final int MIN_NET_CAPABILITY = 0;
  
  public static final int MIN_TRANSPORT = 0;
  
  private static final long MUTABLE_CAPABILITIES = 54476800L;
  
  public static final int NET_CAPABILITY_CAPTIVE_PORTAL = 17;
  
  public static final int NET_CAPABILITY_CBS = 5;
  
  public static final int NET_CAPABILITY_DUALWIFI = 30;
  
  public static final int NET_CAPABILITY_DUN = 2;
  
  public static final int NET_CAPABILITY_EIMS = 10;
  
  public static final int NET_CAPABILITY_FOREGROUND = 19;
  
  public static final int NET_CAPABILITY_FOTA = 3;
  
  public static final int NET_CAPABILITY_IA = 7;
  
  public static final int NET_CAPABILITY_IMS = 4;
  
  public static final int NET_CAPABILITY_INTERNET = 12;
  
  public static final int NET_CAPABILITY_MCX = 23;
  
  public static final int NET_CAPABILITY_MMS = 0;
  
  public static final int NET_CAPABILITY_NOT_CONGESTED = 20;
  
  public static final int NET_CAPABILITY_NOT_METERED = 11;
  
  public static final int NET_CAPABILITY_NOT_RESTRICTED = 13;
  
  public static final int NET_CAPABILITY_NOT_ROAMING = 18;
  
  public static final int NET_CAPABILITY_NOT_SUSPENDED = 21;
  
  public static final int NET_CAPABILITY_NOT_VPN = 15;
  
  @SystemApi
  public static final int NET_CAPABILITY_OEM_PAID = 22;
  
  @SystemApi
  public static final int NET_CAPABILITY_PARTIAL_CONNECTIVITY = 24;
  
  public static final int NET_CAPABILITY_RCS = 8;
  
  public static final int NET_CAPABILITY_SUPL = 1;
  
  public static final int NET_CAPABILITY_TEMPORARILY_NOT_METERED = 25;
  
  public static final int NET_CAPABILITY_TRUSTED = 14;
  
  public static final int NET_CAPABILITY_VALIDATED = 16;
  
  public static final int NET_CAPABILITY_WIFI_P2P = 6;
  
  public static final int NET_CAPABILITY_XCAP = 9;
  
  private static final long NON_REQUESTABLE_CAPABILITIES = 54460416L;
  
  static final long RESTRICTED_CAPABILITIES = 8390588L;
  
  public static final int SIGNAL_STRENGTH_UNSPECIFIED = -2147483648;
  
  private static final String TAG = "NetworkCapabilities";
  
  private static final long TEST_NETWORKS_ALLOWED_CAPABILITIES = 37005312L;
  
  private static final int TEST_NETWORKS_ALLOWED_TRANSPORTS = 136;
  
  public static final int TRANSPORT_BLUETOOTH = 2;
  
  public static final int TRANSPORT_CELLULAR = 0;
  
  public static final int TRANSPORT_ETHERNET = 3;
  
  public static final int TRANSPORT_LOWPAN = 6;
  
  public NetworkCapabilities() {
    this.mOwnerUid = -1;
    this.mAdministratorUids = new int[0];
    this.mLinkUpBandwidthKbps = 0;
    this.mLinkDownBandwidthKbps = 0;
    this.mNetworkSpecifier = null;
    this.mTransportInfo = null;
    this.mSignalStrength = Integer.MIN_VALUE;
    this.mUids = null;
    clearAll();
    this.mNetworkCapabilities = 57344L;
  }
  
  public NetworkCapabilities(NetworkCapabilities paramNetworkCapabilities) {
    this.mOwnerUid = -1;
    this.mAdministratorUids = new int[0];
    this.mLinkUpBandwidthKbps = 0;
    this.mLinkDownBandwidthKbps = 0;
    this.mNetworkSpecifier = null;
    this.mTransportInfo = null;
    this.mSignalStrength = Integer.MIN_VALUE;
    this.mUids = null;
    if (paramNetworkCapabilities != null)
      set(paramNetworkCapabilities); 
  }
  
  public void clearAll() {
    this.mUnwantedNetworkCapabilities = 0L;
    this.mTransportTypes = 0L;
    this.mNetworkCapabilities = 0L;
    this.mLinkDownBandwidthKbps = 0;
    this.mLinkUpBandwidthKbps = 0;
    this.mNetworkSpecifier = null;
    this.mTransportInfo = null;
    this.mSignalStrength = Integer.MIN_VALUE;
    this.mUids = null;
    this.mAdministratorUids = new int[0];
    this.mOwnerUid = -1;
    this.mSSID = null;
    this.mPrivateDnsBroken = false;
    this.mRequestorUid = -1;
    this.mRequestorPackageName = null;
  }
  
  public void set(NetworkCapabilities paramNetworkCapabilities) {
    this.mNetworkCapabilities = paramNetworkCapabilities.mNetworkCapabilities;
    this.mTransportTypes = paramNetworkCapabilities.mTransportTypes;
    this.mLinkUpBandwidthKbps = paramNetworkCapabilities.mLinkUpBandwidthKbps;
    this.mLinkDownBandwidthKbps = paramNetworkCapabilities.mLinkDownBandwidthKbps;
    this.mNetworkSpecifier = paramNetworkCapabilities.mNetworkSpecifier;
    this.mTransportInfo = paramNetworkCapabilities.mTransportInfo;
    this.mSignalStrength = paramNetworkCapabilities.mSignalStrength;
    setUids((Set<UidRange>)paramNetworkCapabilities.mUids);
    setAdministratorUids(paramNetworkCapabilities.getAdministratorUids());
    this.mOwnerUid = paramNetworkCapabilities.mOwnerUid;
    this.mUnwantedNetworkCapabilities = paramNetworkCapabilities.mUnwantedNetworkCapabilities;
    this.mSSID = paramNetworkCapabilities.mSSID;
    this.mPrivateDnsBroken = paramNetworkCapabilities.mPrivateDnsBroken;
    this.mRequestorUid = paramNetworkCapabilities.mRequestorUid;
    this.mRequestorPackageName = paramNetworkCapabilities.mRequestorPackageName;
  }
  
  public NetworkCapabilities addCapability(int paramInt) {
    checkValidCapability(paramInt);
    this.mNetworkCapabilities |= (1 << paramInt);
    this.mUnwantedNetworkCapabilities &= (1 << paramInt ^ 0xFFFFFFFF);
    return this;
  }
  
  public void addUnwantedCapability(int paramInt) {
    checkValidCapability(paramInt);
    this.mUnwantedNetworkCapabilities |= (1 << paramInt);
    this.mNetworkCapabilities &= (1 << paramInt ^ 0xFFFFFFFF);
  }
  
  public NetworkCapabilities removeCapability(int paramInt) {
    checkValidCapability(paramInt);
    long l = (1 << paramInt ^ 0xFFFFFFFF);
    this.mNetworkCapabilities &= l;
    this.mUnwantedNetworkCapabilities &= l;
    return this;
  }
  
  public NetworkCapabilities setCapability(int paramInt, boolean paramBoolean) {
    if (paramBoolean) {
      addCapability(paramInt);
    } else {
      removeCapability(paramInt);
    } 
    return this;
  }
  
  public int[] getCapabilities() {
    return BitUtils.unpackBits(this.mNetworkCapabilities);
  }
  
  public int[] getUnwantedCapabilities() {
    return BitUtils.unpackBits(this.mUnwantedNetworkCapabilities);
  }
  
  public void setCapabilities(int[] paramArrayOfint1, int[] paramArrayOfint2) {
    this.mNetworkCapabilities = BitUtils.packBits(paramArrayOfint1);
    this.mUnwantedNetworkCapabilities = BitUtils.packBits(paramArrayOfint2);
  }
  
  @Deprecated
  public void setCapabilities(int[] paramArrayOfint) {
    setCapabilities(paramArrayOfint, new int[0]);
  }
  
  public boolean hasCapability(int paramInt) {
    boolean bool = isValidCapability(paramInt);
    boolean bool1 = true;
    if (!bool || (this.mNetworkCapabilities & (1 << paramInt)) == 0L)
      bool1 = false; 
    return bool1;
  }
  
  public boolean hasUnwantedCapability(int paramInt) {
    boolean bool = isValidCapability(paramInt);
    boolean bool1 = true;
    if (!bool || (this.mUnwantedNetworkCapabilities & (1 << paramInt)) == 0L)
      bool1 = false; 
    return bool1;
  }
  
  public boolean hasConnectivityManagedCapability() {
    boolean bool;
    if ((this.mNetworkCapabilities & 0x10B0000L) != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void combineNetCapabilities(NetworkCapabilities paramNetworkCapabilities) {
    this.mNetworkCapabilities |= paramNetworkCapabilities.mNetworkCapabilities;
    this.mUnwantedNetworkCapabilities |= paramNetworkCapabilities.mUnwantedNetworkCapabilities;
  }
  
  public String describeFirstNonRequestableCapability() {
    long l = (this.mNetworkCapabilities | this.mUnwantedNetworkCapabilities) & 0x33F0000L;
    if (l != 0L)
      return capabilityNameOf(BitUtils.unpackBits(l)[0]); 
    if (this.mLinkUpBandwidthKbps != 0 || this.mLinkDownBandwidthKbps != 0)
      return "link bandwidth"; 
    if (hasSignalStrength())
      return "signalStrength"; 
    if (isPrivateDnsBroken())
      return "privateDnsBroken"; 
    return null;
  }
  
  private boolean satisfiedByNetCapabilities(NetworkCapabilities paramNetworkCapabilities, boolean paramBoolean) {
    long l1 = this.mNetworkCapabilities;
    long l2 = this.mUnwantedNetworkCapabilities;
    long l3 = paramNetworkCapabilities.mNetworkCapabilities;
    long l4 = l1, l5 = l2;
    if (paramBoolean) {
      l4 = l1 & 0xFFFFFFFFFCC0BFFFL;
      l5 = l2 & 0xFFFFFFFFFCC0BFFFL;
    } 
    if ((l3 & l4) == l4 && (l5 & l3) == 0L) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    } 
    return paramBoolean;
  }
  
  public boolean equalsNetCapabilities(NetworkCapabilities paramNetworkCapabilities) {
    boolean bool;
    if (paramNetworkCapabilities.mNetworkCapabilities == this.mNetworkCapabilities && paramNetworkCapabilities.mUnwantedNetworkCapabilities == this.mUnwantedNetworkCapabilities) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean equalsNetCapabilitiesRequestable(NetworkCapabilities paramNetworkCapabilities) {
    boolean bool;
    if ((this.mNetworkCapabilities & 0xFFFFFFFFFCC0FFFFL) == (paramNetworkCapabilities.mNetworkCapabilities & 0xFFFFFFFFFCC0FFFFL) && (this.mUnwantedNetworkCapabilities & 0xFFFFFFFFFCC0FFFFL) == (0xFFFFFFFFFCC0FFFFL & paramNetworkCapabilities.mUnwantedNetworkCapabilities)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean deduceRestrictedCapability() {
    boolean bool2, bool3, bool4;
    long l = this.mNetworkCapabilities;
    boolean bool1 = true;
    if ((l & 0x400000L) != 0L) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if ((this.mNetworkCapabilities & 0x1043L) != 0L) {
      bool3 = true;
    } else {
      bool3 = false;
    } 
    if ((this.mNetworkCapabilities & 0x8007BCL) != 0L) {
      bool4 = true;
    } else {
      bool4 = false;
    } 
    boolean bool5 = bool1;
    if (!bool2)
      if (bool4 && !bool3) {
        bool5 = bool1;
      } else {
        bool5 = false;
      }  
    return bool5;
  }
  
  public void maybeMarkCapabilitiesRestricted() {
    if (deduceRestrictedCapability())
      removeCapability(13); 
  }
  
  public void restrictCapabilitesForTestNetwork(int paramInt) {
    long l1 = this.mNetworkCapabilities;
    long l2 = this.mTransportTypes;
    NetworkSpecifier networkSpecifier = this.mNetworkSpecifier;
    int i = this.mSignalStrength;
    int j = getOwnerUid();
    int[] arrayOfInt = getAdministratorUids();
    clearAll();
    this.mTransportTypes = 0x88L & l2 | 0x80L;
    this.mNetworkCapabilities = 0x234A800L & l1;
    this.mNetworkSpecifier = networkSpecifier;
    this.mSignalStrength = i;
    if (j == paramInt)
      setOwnerUid(paramInt); 
    if (ArrayUtils.contains(arrayOfInt, paramInt))
      setAdministratorUids(new int[] { paramInt }); 
  }
  
  public static boolean isValidTransport(int paramInt) {
    boolean bool;
    if (paramInt >= 0 && paramInt <= 7) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static final String[] TRANSPORT_NAMES = new String[] { "CELLULAR", "WIFI", "BLUETOOTH", "ETHERNET", "VPN", "WIFI_AWARE", "LOWPAN", "TEST" };
  
  public static final int TRANSPORT_TEST = 7;
  
  public static final int TRANSPORT_VPN = 4;
  
  public static final int TRANSPORT_WIFI = 1;
  
  public static final int TRANSPORT_WIFI_AWARE = 5;
  
  static final long UNRESTRICTED_CAPABILITIES = 4163L;
  
  private int[] mAdministratorUids;
  
  private int mLinkDownBandwidthKbps;
  
  private int mLinkUpBandwidthKbps;
  
  private long mNetworkCapabilities;
  
  private NetworkSpecifier mNetworkSpecifier;
  
  private int mOwnerUid;
  
  private boolean mPrivateDnsBroken;
  
  private String mRequestorPackageName;
  
  private int mRequestorUid;
  
  private String mSSID;
  
  private int mSignalStrength;
  
  private TransportInfo mTransportInfo;
  
  private long mTransportTypes;
  
  private ArraySet<UidRange> mUids;
  
  private long mUnwantedNetworkCapabilities;
  
  public NetworkCapabilities addTransportType(int paramInt) {
    checkValidTransportType(paramInt);
    this.mTransportTypes |= (1 << paramInt);
    setNetworkSpecifier(this.mNetworkSpecifier);
    return this;
  }
  
  public NetworkCapabilities removeTransportType(int paramInt) {
    checkValidTransportType(paramInt);
    this.mTransportTypes &= (1 << paramInt ^ 0xFFFFFFFF);
    setNetworkSpecifier(this.mNetworkSpecifier);
    return this;
  }
  
  public NetworkCapabilities setTransportType(int paramInt, boolean paramBoolean) {
    if (paramBoolean) {
      addTransportType(paramInt);
    } else {
      removeTransportType(paramInt);
    } 
    return this;
  }
  
  @SystemApi
  public int[] getTransportTypes() {
    return BitUtils.unpackBits(this.mTransportTypes);
  }
  
  public void setTransportTypes(int[] paramArrayOfint) {
    this.mTransportTypes = BitUtils.packBits(paramArrayOfint);
  }
  
  public boolean hasTransport(int paramInt) {
    boolean bool = isValidTransport(paramInt);
    boolean bool1 = true;
    if (!bool || (this.mTransportTypes & (1 << paramInt)) == 0L)
      bool1 = false; 
    return bool1;
  }
  
  private void combineTransportTypes(NetworkCapabilities paramNetworkCapabilities) {
    this.mTransportTypes |= paramNetworkCapabilities.mTransportTypes;
  }
  
  private boolean satisfiedByTransportTypes(NetworkCapabilities paramNetworkCapabilities) {
    long l = this.mTransportTypes;
    return (l == 0L || (l & paramNetworkCapabilities.mTransportTypes) != 0L);
  }
  
  public boolean equalsTransportTypes(NetworkCapabilities paramNetworkCapabilities) {
    boolean bool;
    if (paramNetworkCapabilities.mTransportTypes == this.mTransportTypes) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public NetworkCapabilities setOwnerUid(int paramInt) {
    this.mOwnerUid = paramInt;
    return this;
  }
  
  public int getOwnerUid() {
    return this.mOwnerUid;
  }
  
  public NetworkCapabilities setAdministratorUids(int[] paramArrayOfint) {
    this.mAdministratorUids = paramArrayOfint = Arrays.copyOf(paramArrayOfint, paramArrayOfint.length);
    Arrays.sort(paramArrayOfint);
    byte b = 0;
    while (true) {
      paramArrayOfint = this.mAdministratorUids;
      if (b < paramArrayOfint.length - 1) {
        if (paramArrayOfint[b] < paramArrayOfint[b + 1]) {
          b++;
          continue;
        } 
        throw new IllegalArgumentException("All administrator UIDs must be unique");
      } 
      break;
    } 
    return this;
  }
  
  @SystemApi
  public int[] getAdministratorUids() {
    int[] arrayOfInt = this.mAdministratorUids;
    return Arrays.copyOf(arrayOfInt, arrayOfInt.length);
  }
  
  public boolean equalsAdministratorUids(NetworkCapabilities paramNetworkCapabilities) {
    return Arrays.equals(this.mAdministratorUids, paramNetworkCapabilities.mAdministratorUids);
  }
  
  private void combineAdministratorUids(NetworkCapabilities paramNetworkCapabilities) {
    int[] arrayOfInt = paramNetworkCapabilities.mAdministratorUids;
    if (arrayOfInt.length == 0)
      return; 
    if (this.mAdministratorUids.length == 0) {
      this.mAdministratorUids = Arrays.copyOf(arrayOfInt, arrayOfInt.length);
      return;
    } 
    if (equalsAdministratorUids(paramNetworkCapabilities))
      return; 
    throw new IllegalStateException("Can't combine two different administrator UID lists");
  }
  
  public NetworkCapabilities setLinkUpstreamBandwidthKbps(int paramInt) {
    this.mLinkUpBandwidthKbps = paramInt;
    return this;
  }
  
  public int getLinkUpstreamBandwidthKbps() {
    return this.mLinkUpBandwidthKbps;
  }
  
  public NetworkCapabilities setLinkDownstreamBandwidthKbps(int paramInt) {
    this.mLinkDownBandwidthKbps = paramInt;
    return this;
  }
  
  public int getLinkDownstreamBandwidthKbps() {
    return this.mLinkDownBandwidthKbps;
  }
  
  private void combineLinkBandwidths(NetworkCapabilities paramNetworkCapabilities) {
    int i = this.mLinkUpBandwidthKbps, j = paramNetworkCapabilities.mLinkUpBandwidthKbps;
    this.mLinkUpBandwidthKbps = Math.max(i, j);
    j = this.mLinkDownBandwidthKbps;
    i = paramNetworkCapabilities.mLinkDownBandwidthKbps;
    this.mLinkDownBandwidthKbps = Math.max(j, i);
  }
  
  private boolean satisfiedByLinkBandwidths(NetworkCapabilities paramNetworkCapabilities) {
    boolean bool;
    if (this.mLinkUpBandwidthKbps <= paramNetworkCapabilities.mLinkUpBandwidthKbps && this.mLinkDownBandwidthKbps <= paramNetworkCapabilities.mLinkDownBandwidthKbps) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean equalsLinkBandwidths(NetworkCapabilities paramNetworkCapabilities) {
    boolean bool;
    if (this.mLinkUpBandwidthKbps == paramNetworkCapabilities.mLinkUpBandwidthKbps && this.mLinkDownBandwidthKbps == paramNetworkCapabilities.mLinkDownBandwidthKbps) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static int minBandwidth(int paramInt1, int paramInt2) {
    if (paramInt1 == 0)
      return paramInt2; 
    if (paramInt2 == 0)
      return paramInt1; 
    return Math.min(paramInt1, paramInt2);
  }
  
  public static int maxBandwidth(int paramInt1, int paramInt2) {
    return Math.max(paramInt1, paramInt2);
  }
  
  public NetworkCapabilities setNetworkSpecifier(NetworkSpecifier paramNetworkSpecifier) {
    if (paramNetworkSpecifier == null || Long.bitCount(this.mTransportTypes) == 1) {
      this.mNetworkSpecifier = paramNetworkSpecifier;
      return this;
    } 
    throw new IllegalStateException("Must have a single transport specified to use setNetworkSpecifier");
  }
  
  public NetworkCapabilities setTransportInfo(TransportInfo paramTransportInfo) {
    this.mTransportInfo = paramTransportInfo;
    return this;
  }
  
  public NetworkSpecifier getNetworkSpecifier() {
    return this.mNetworkSpecifier;
  }
  
  public TransportInfo getTransportInfo() {
    return this.mTransportInfo;
  }
  
  private void combineSpecifiers(NetworkCapabilities paramNetworkCapabilities) {
    NetworkSpecifier networkSpecifier = this.mNetworkSpecifier;
    if (networkSpecifier == null || networkSpecifier.equals(paramNetworkCapabilities.mNetworkSpecifier)) {
      setNetworkSpecifier(paramNetworkCapabilities.mNetworkSpecifier);
      return;
    } 
    throw new IllegalStateException("Can't combine two networkSpecifiers");
  }
  
  private boolean satisfiedBySpecifier(NetworkCapabilities paramNetworkCapabilities) {
    NetworkSpecifier networkSpecifier = this.mNetworkSpecifier;
    return (networkSpecifier == null || networkSpecifier.canBeSatisfiedBy(paramNetworkCapabilities.mNetworkSpecifier) || paramNetworkCapabilities.mNetworkSpecifier instanceof MatchAllNetworkSpecifier);
  }
  
  private boolean equalsSpecifier(NetworkCapabilities paramNetworkCapabilities) {
    return Objects.equals(this.mNetworkSpecifier, paramNetworkCapabilities.mNetworkSpecifier);
  }
  
  private void combineTransportInfos(NetworkCapabilities paramNetworkCapabilities) {
    TransportInfo transportInfo = this.mTransportInfo;
    if (transportInfo == null || transportInfo.equals(paramNetworkCapabilities.mTransportInfo)) {
      setTransportInfo(paramNetworkCapabilities.mTransportInfo);
      return;
    } 
    throw new IllegalStateException("Can't combine two TransportInfos");
  }
  
  private boolean equalsTransportInfo(NetworkCapabilities paramNetworkCapabilities) {
    return Objects.equals(this.mTransportInfo, paramNetworkCapabilities.mTransportInfo);
  }
  
  public NetworkCapabilities setSignalStrength(int paramInt) {
    this.mSignalStrength = paramInt;
    return this;
  }
  
  public boolean hasSignalStrength() {
    boolean bool;
    if (this.mSignalStrength > Integer.MIN_VALUE) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getSignalStrength() {
    return this.mSignalStrength;
  }
  
  private void combineSignalStrength(NetworkCapabilities paramNetworkCapabilities) {
    this.mSignalStrength = Math.max(this.mSignalStrength, paramNetworkCapabilities.mSignalStrength);
  }
  
  private boolean satisfiedBySignalStrength(NetworkCapabilities paramNetworkCapabilities) {
    boolean bool;
    if (this.mSignalStrength <= paramNetworkCapabilities.mSignalStrength) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean equalsSignalStrength(NetworkCapabilities paramNetworkCapabilities) {
    boolean bool;
    if (this.mSignalStrength == paramNetworkCapabilities.mSignalStrength) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public NetworkCapabilities setSingleUid(int paramInt) {
    ArraySet arraySet = new ArraySet(1);
    arraySet.add(new UidRange(paramInt, paramInt));
    setUids((Set<UidRange>)arraySet);
    return this;
  }
  
  public NetworkCapabilities setUids(Set<UidRange> paramSet) {
    if (paramSet == null) {
      this.mUids = null;
    } else {
      this.mUids = new ArraySet(paramSet);
    } 
    return this;
  }
  
  public Set<UidRange> getUids() {
    ArraySet arraySet;
    if (this.mUids == null) {
      arraySet = null;
    } else {
      arraySet = new ArraySet(this.mUids);
    } 
    return (Set<UidRange>)arraySet;
  }
  
  public boolean appliesToUid(int paramInt) {
    ArraySet<UidRange> arraySet = this.mUids;
    if (arraySet == null)
      return true; 
    for (UidRange uidRange : arraySet) {
      if (uidRange.contains(paramInt))
        return true; 
    } 
    return false;
  }
  
  public boolean equalsUids(NetworkCapabilities paramNetworkCapabilities) {
    ArraySet<UidRange> arraySet1 = paramNetworkCapabilities.mUids;
    boolean bool = false;
    if (arraySet1 == null) {
      if (this.mUids == null)
        bool = true; 
      return bool;
    } 
    if (this.mUids == null)
      return false; 
    ArraySet arraySet = new ArraySet(this.mUids);
    for (UidRange uidRange : arraySet1) {
      if (!arraySet.contains(uidRange))
        return false; 
      arraySet.remove(uidRange);
    } 
    return arraySet.isEmpty();
  }
  
  public boolean satisfiedByUids(NetworkCapabilities paramNetworkCapabilities) {
    if (paramNetworkCapabilities.mUids != null) {
      ArraySet<UidRange> arraySet = this.mUids;
      if (arraySet != null) {
        for (UidRange uidRange : arraySet) {
          if (uidRange.contains(paramNetworkCapabilities.mOwnerUid))
            return true; 
          if (!paramNetworkCapabilities.appliesToUidRange(uidRange))
            return false; 
        } 
        return true;
      } 
    } 
    return true;
  }
  
  public boolean appliesToUidRange(UidRange paramUidRange) {
    ArraySet<UidRange> arraySet = this.mUids;
    if (arraySet == null)
      return true; 
    for (UidRange uidRange : arraySet) {
      if (uidRange.containsRange(paramUidRange))
        return true; 
    } 
    return false;
  }
  
  private void combineUids(NetworkCapabilities paramNetworkCapabilities) {
    ArraySet<UidRange> arraySet = paramNetworkCapabilities.mUids;
    if (arraySet != null) {
      ArraySet<UidRange> arraySet1 = this.mUids;
      if (arraySet1 != null) {
        arraySet1.addAll(arraySet);
        return;
      } 
    } 
    this.mUids = null;
  }
  
  public NetworkCapabilities setSSID(String paramString) {
    this.mSSID = paramString;
    return this;
  }
  
  @SystemApi
  public String getSsid() {
    return this.mSSID;
  }
  
  public boolean equalsSSID(NetworkCapabilities paramNetworkCapabilities) {
    return Objects.equals(this.mSSID, paramNetworkCapabilities.mSSID);
  }
  
  public boolean satisfiedBySSID(NetworkCapabilities paramNetworkCapabilities) {
    String str = this.mSSID;
    return (str == null || str.equals(paramNetworkCapabilities.mSSID));
  }
  
  private void combineSSIDs(NetworkCapabilities paramNetworkCapabilities) {
    String str = this.mSSID;
    if (str == null || str.equals(paramNetworkCapabilities.mSSID)) {
      setSSID(paramNetworkCapabilities.mSSID);
      return;
    } 
    throw new IllegalStateException("Can't combine two SSIDs");
  }
  
  public void combineCapabilities(NetworkCapabilities paramNetworkCapabilities) {
    combineNetCapabilities(paramNetworkCapabilities);
    combineTransportTypes(paramNetworkCapabilities);
    combineLinkBandwidths(paramNetworkCapabilities);
    combineSpecifiers(paramNetworkCapabilities);
    combineTransportInfos(paramNetworkCapabilities);
    combineSignalStrength(paramNetworkCapabilities);
    combineUids(paramNetworkCapabilities);
    combineSSIDs(paramNetworkCapabilities);
    combineRequestor(paramNetworkCapabilities);
    combineAdministratorUids(paramNetworkCapabilities);
  }
  
  private boolean satisfiedByNetworkCapabilities(NetworkCapabilities paramNetworkCapabilities, boolean paramBoolean) {
    if (paramNetworkCapabilities != null && 
      satisfiedByNetCapabilities(paramNetworkCapabilities, paramBoolean) && 
      satisfiedByTransportTypes(paramNetworkCapabilities) && (paramBoolean || 
      satisfiedByLinkBandwidths(paramNetworkCapabilities)) && 
      satisfiedBySpecifier(paramNetworkCapabilities) && (paramBoolean || 
      satisfiedBySignalStrength(paramNetworkCapabilities)) && (paramBoolean || 
      satisfiedByUids(paramNetworkCapabilities)) && (paramBoolean || 
      satisfiedBySSID(paramNetworkCapabilities)) && (paramBoolean || 
      satisfiedByRequestor(paramNetworkCapabilities))) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    } 
    return paramBoolean;
  }
  
  @SystemApi
  public boolean satisfiedByNetworkCapabilities(NetworkCapabilities paramNetworkCapabilities) {
    return satisfiedByNetworkCapabilities(paramNetworkCapabilities, false);
  }
  
  public boolean satisfiedByImmutableNetworkCapabilities(NetworkCapabilities paramNetworkCapabilities) {
    return satisfiedByNetworkCapabilities(paramNetworkCapabilities, true);
  }
  
  public String describeImmutableDifferences(NetworkCapabilities paramNetworkCapabilities) {
    if (paramNetworkCapabilities == null)
      return "other NetworkCapabilities was null"; 
    StringJoiner stringJoiner = new StringJoiner(", ");
    long l1 = this.mNetworkCapabilities & 0xFFFFFFFFFCC0B7FFL;
    long l2 = 0xFFFFFFFFFCC0B7FFL & paramNetworkCapabilities.mNetworkCapabilities;
    if (l1 != l2) {
      String str1 = capabilityNamesOf(BitUtils.unpackBits(l1));
      String str2 = capabilityNamesOf(BitUtils.unpackBits(l2));
      stringJoiner.add(String.format("immutable capabilities changed: %s -> %s", new Object[] { str1, str2 }));
    } 
    if (!equalsSpecifier(paramNetworkCapabilities)) {
      NetworkSpecifier networkSpecifier1 = getNetworkSpecifier();
      NetworkSpecifier networkSpecifier2 = paramNetworkCapabilities.getNetworkSpecifier();
      stringJoiner.add(String.format("specifier changed: %s -> %s", new Object[] { networkSpecifier1, networkSpecifier2 }));
    } 
    if (!equalsTransportTypes(paramNetworkCapabilities)) {
      String str2 = transportNamesOf(getTransportTypes());
      String str1 = transportNamesOf(paramNetworkCapabilities.getTransportTypes());
      stringJoiner.add(String.format("transports changed: %s -> %s", new Object[] { str2, str1 }));
    } 
    return stringJoiner.toString();
  }
  
  public boolean equalRequestableCapabilities(NetworkCapabilities paramNetworkCapabilities) {
    boolean bool = false;
    if (paramNetworkCapabilities == null)
      return false; 
    if (equalsNetCapabilitiesRequestable(paramNetworkCapabilities) && 
      equalsTransportTypes(paramNetworkCapabilities) && 
      equalsSpecifier(paramNetworkCapabilities))
      bool = true; 
    return bool;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    if (paramObject == null || !(paramObject instanceof NetworkCapabilities))
      return false; 
    paramObject = paramObject;
    if (equalsNetCapabilities((NetworkCapabilities)paramObject) && 
      equalsTransportTypes((NetworkCapabilities)paramObject) && 
      equalsLinkBandwidths((NetworkCapabilities)paramObject) && 
      equalsSignalStrength((NetworkCapabilities)paramObject) && 
      equalsSpecifier((NetworkCapabilities)paramObject) && 
      equalsTransportInfo((NetworkCapabilities)paramObject) && 
      equalsUids((NetworkCapabilities)paramObject) && 
      equalsSSID((NetworkCapabilities)paramObject) && 
      equalsPrivateDnsBroken((NetworkCapabilities)paramObject) && 
      equalsRequestor((NetworkCapabilities)paramObject) && 
      equalsAdministratorUids((NetworkCapabilities)paramObject))
      bool = true; 
    return bool;
  }
  
  public int hashCode() {
    long l = this.mNetworkCapabilities;
    int i = (int)(l & 0xFFFFFFFFFFFFFFFFL), j = (int)(l >> 32L);
    l = this.mUnwantedNetworkCapabilities;
    int k = (int)(l & 0xFFFFFFFFFFFFFFFFL), m = (int)(l >> 32L);
    l = this.mTransportTypes;
    int n = (int)(0xFFFFFFFFFFFFFFFFL & l), i1 = (int)(l >> 32L), i2 = this.mLinkUpBandwidthKbps, i3 = this.mLinkDownBandwidthKbps;
    NetworkSpecifier networkSpecifier = this.mNetworkSpecifier;
    int i4 = Objects.hashCode(networkSpecifier), i5 = this.mSignalStrength;
    ArraySet<UidRange> arraySet = this.mUids;
    int i6 = Objects.hashCode(arraySet);
    String str2 = this.mSSID;
    int i7 = Objects.hashCode(str2);
    TransportInfo transportInfo = this.mTransportInfo;
    int i8 = Objects.hashCode(transportInfo);
    boolean bool = this.mPrivateDnsBroken;
    int i9 = Objects.hashCode(Boolean.valueOf(bool)), i10 = this.mRequestorUid;
    int i11 = Objects.hashCode(Integer.valueOf(i10));
    String str1 = this.mRequestorPackageName;
    i10 = Objects.hashCode(str1);
    int[] arrayOfInt = this.mAdministratorUids;
    int i12 = Arrays.hashCode(arrayOfInt);
    return i + j * 3 + k * 5 + m * 7 + n * 11 + i1 * 13 + i2 * 17 + i3 * 19 + i4 * 23 + i5 * 29 + i6 * 31 + i7 * 37 + i8 * 41 + i9 * 43 + i11 * 47 + i10 * 53 + i12 * 59;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mNetworkCapabilities);
    paramParcel.writeLong(this.mUnwantedNetworkCapabilities);
    paramParcel.writeLong(this.mTransportTypes);
    paramParcel.writeInt(this.mLinkUpBandwidthKbps);
    paramParcel.writeInt(this.mLinkDownBandwidthKbps);
    paramParcel.writeParcelable((Parcelable)this.mNetworkSpecifier, paramInt);
    paramParcel.writeParcelable((Parcelable)this.mTransportInfo, paramInt);
    paramParcel.writeInt(this.mSignalStrength);
    paramParcel.writeArraySet((ArraySet)this.mUids);
    paramParcel.writeString(this.mSSID);
    paramParcel.writeBoolean(this.mPrivateDnsBroken);
    paramParcel.writeIntArray(getAdministratorUids());
    paramParcel.writeInt(this.mOwnerUid);
    paramParcel.writeInt(this.mRequestorUid);
    paramParcel.writeString(this.mRequestorPackageName);
  }
  
  static {
    CREATOR = new Parcelable.Creator<NetworkCapabilities>() {
        public NetworkCapabilities createFromParcel(Parcel param1Parcel) {
          NetworkCapabilities networkCapabilities = new NetworkCapabilities();
          NetworkCapabilities.access$002(networkCapabilities, param1Parcel.readLong());
          NetworkCapabilities.access$102(networkCapabilities, param1Parcel.readLong());
          NetworkCapabilities.access$202(networkCapabilities, param1Parcel.readLong());
          NetworkCapabilities.access$302(networkCapabilities, param1Parcel.readInt());
          NetworkCapabilities.access$402(networkCapabilities, param1Parcel.readInt());
          NetworkCapabilities.access$502(networkCapabilities, param1Parcel.<NetworkSpecifier>readParcelable(null));
          NetworkCapabilities.access$602(networkCapabilities, param1Parcel.<TransportInfo>readParcelable(null));
          NetworkCapabilities.access$702(networkCapabilities, param1Parcel.readInt());
          NetworkCapabilities.access$802(networkCapabilities, param1Parcel.readArraySet(null));
          NetworkCapabilities.access$902(networkCapabilities, param1Parcel.readString());
          NetworkCapabilities.access$1002(networkCapabilities, param1Parcel.readBoolean());
          networkCapabilities.setAdministratorUids(param1Parcel.createIntArray());
          NetworkCapabilities.access$1102(networkCapabilities, param1Parcel.readInt());
          NetworkCapabilities.access$1202(networkCapabilities, param1Parcel.readInt());
          NetworkCapabilities.access$1302(networkCapabilities, param1Parcel.readString());
          return networkCapabilities;
        }
        
        public NetworkCapabilities[] newArray(int param1Int) {
          return new NetworkCapabilities[param1Int];
        }
      };
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("[");
    if (0L != this.mTransportTypes) {
      stringBuilder.append(" Transports: ");
      appendStringRepresentationOfBitMaskToStringBuilder(stringBuilder, this.mTransportTypes, (NameOf)_$$Lambda$FpGXkd3pLxeXY58eJ_84mi1PLWQ.INSTANCE, "|");
    } 
    if (0L != this.mNetworkCapabilities) {
      stringBuilder.append(" Capabilities: ");
      appendStringRepresentationOfBitMaskToStringBuilder(stringBuilder, this.mNetworkCapabilities, (NameOf)_$$Lambda$p1_56lwnt1xBuY1muPblbN1Dtkw.INSTANCE, "&");
    } 
    if (0L != this.mUnwantedNetworkCapabilities) {
      stringBuilder.append(" Unwanted: ");
      appendStringRepresentationOfBitMaskToStringBuilder(stringBuilder, this.mUnwantedNetworkCapabilities, (NameOf)_$$Lambda$p1_56lwnt1xBuY1muPblbN1Dtkw.INSTANCE, "&");
    } 
    if (this.mLinkUpBandwidthKbps > 0) {
      stringBuilder.append(" LinkUpBandwidth>=");
      stringBuilder.append(this.mLinkUpBandwidthKbps);
      stringBuilder.append("Kbps");
    } 
    if (this.mLinkDownBandwidthKbps > 0) {
      stringBuilder.append(" LinkDnBandwidth>=");
      stringBuilder.append(this.mLinkDownBandwidthKbps);
      stringBuilder.append("Kbps");
    } 
    if (this.mNetworkSpecifier != null) {
      stringBuilder.append(" Specifier: <");
      stringBuilder.append(this.mNetworkSpecifier);
      stringBuilder.append(">");
    } 
    if (this.mTransportInfo != null) {
      stringBuilder.append(" TransportInfo: <");
      stringBuilder.append(this.mTransportInfo);
      stringBuilder.append(">");
    } 
    if (hasSignalStrength()) {
      stringBuilder.append(" SignalStrength: ");
      stringBuilder.append(this.mSignalStrength);
    } 
    ArraySet<UidRange> arraySet = this.mUids;
    if (arraySet != null)
      if (1 == arraySet.size() && ((UidRange)this.mUids.valueAt(0)).count() == 1) {
        stringBuilder.append(" Uid: ");
        stringBuilder.append(((UidRange)this.mUids.valueAt(0)).start);
      } else {
        stringBuilder.append(" Uids: <");
        stringBuilder.append(this.mUids);
        stringBuilder.append(">");
      }  
    if (this.mOwnerUid != -1) {
      stringBuilder.append(" OwnerUid: ");
      stringBuilder.append(this.mOwnerUid);
    } 
    if (this.mAdministratorUids.length == 0) {
      stringBuilder.append(" AdministratorUids: ");
      stringBuilder.append(Arrays.toString(this.mAdministratorUids));
    } 
    if (this.mSSID != null) {
      stringBuilder.append(" SSID: ");
      stringBuilder.append(this.mSSID);
    } 
    if (this.mPrivateDnsBroken)
      stringBuilder.append(" Private DNS is broken"); 
    stringBuilder.append(" RequestorUid: ");
    stringBuilder.append(this.mRequestorUid);
    stringBuilder.append(" RequestorPackageName: ");
    stringBuilder.append(this.mRequestorPackageName);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public static void appendStringRepresentationOfBitMaskToStringBuilder(StringBuilder paramStringBuilder, long paramLong, NameOf paramNameOf, String paramString) {
    byte b = 0;
    boolean bool = false;
    while (paramLong != 0L) {
      boolean bool1 = bool;
      if ((0x1L & paramLong) != 0L) {
        if (bool) {
          paramStringBuilder.append(paramString);
        } else {
          bool = true;
        } 
        paramStringBuilder.append(paramNameOf.nameOf(b));
        bool1 = bool;
      } 
      paramLong >>= 1L;
      b++;
      bool = bool1;
    } 
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    paramLong = paramProtoOutputStream.start(paramLong);
    int arrayOfInt[], i, j, k;
    for (arrayOfInt = getTransportTypes(), i = arrayOfInt.length, j = 0, k = 0; k < i; ) {
      int m = arrayOfInt[k];
      paramProtoOutputStream.write(2259152797697L, m);
      k++;
    } 
    for (arrayOfInt = getCapabilities(), i = arrayOfInt.length, k = j; k < i; ) {
      j = arrayOfInt[k];
      paramProtoOutputStream.write(2259152797698L, j);
      k++;
    } 
    paramProtoOutputStream.write(1120986464259L, this.mLinkUpBandwidthKbps);
    paramProtoOutputStream.write(1120986464260L, this.mLinkDownBandwidthKbps);
    NetworkSpecifier networkSpecifier = this.mNetworkSpecifier;
    if (networkSpecifier != null)
      paramProtoOutputStream.write(1138166333445L, networkSpecifier.toString()); 
    paramProtoOutputStream.write(1133871366150L, hasSignalStrength());
    paramProtoOutputStream.write(1172526071815L, this.mSignalStrength);
    paramProtoOutputStream.end(paramLong);
  }
  
  public static String capabilityNamesOf(int[] paramArrayOfint) {
    StringJoiner stringJoiner = new StringJoiner("|");
    if (paramArrayOfint != null) {
      int i;
      byte b;
      for (i = paramArrayOfint.length, b = 0; b < i; ) {
        int j = paramArrayOfint[b];
        stringJoiner.add(capabilityNameOf(j));
        b++;
      } 
    } 
    return stringJoiner.toString();
  }
  
  public static String capabilityNameOf(int paramInt) {
    if (paramInt != 30) {
      switch (paramInt) {
        default:
          return Integer.toString(paramInt);
        case 25:
          return "TEMPORARILY_NOT_METERED";
        case 24:
          return "PARTIAL_CONNECTIVITY";
        case 23:
          return "MCX";
        case 22:
          return "OEM_PAID";
        case 21:
          return "NOT_SUSPENDED";
        case 20:
          return "NOT_CONGESTED";
        case 19:
          return "FOREGROUND";
        case 18:
          return "NOT_ROAMING";
        case 17:
          return "CAPTIVE_PORTAL";
        case 16:
          return "VALIDATED";
        case 15:
          return "NOT_VPN";
        case 14:
          return "TRUSTED";
        case 13:
          return "NOT_RESTRICTED";
        case 12:
          return "INTERNET";
        case 11:
          return "NOT_METERED";
        case 10:
          return "EIMS";
        case 9:
          return "XCAP";
        case 8:
          return "RCS";
        case 7:
          return "IA";
        case 6:
          return "WIFI_P2P";
        case 5:
          return "CBS";
        case 4:
          return "IMS";
        case 3:
          return "FOTA";
        case 2:
          return "DUN";
        case 1:
          return "SUPL";
        case 0:
          break;
      } 
      return "MMS";
    } 
    return "DUAL_WIFI";
  }
  
  public static String transportNamesOf(int[] paramArrayOfint) {
    StringJoiner stringJoiner = new StringJoiner("|");
    if (paramArrayOfint != null) {
      int i;
      byte b;
      for (i = paramArrayOfint.length, b = 0; b < i; ) {
        int j = paramArrayOfint[b];
        stringJoiner.add(transportNameOf(j));
        b++;
      } 
    } 
    return stringJoiner.toString();
  }
  
  public static String transportNameOf(int paramInt) {
    if (!isValidTransport(paramInt))
      return "UNKNOWN"; 
    return TRANSPORT_NAMES[paramInt];
  }
  
  private static void checkValidTransportType(int paramInt) {
    boolean bool = isValidTransport(paramInt);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid TransportType ");
    stringBuilder.append(paramInt);
    String str = stringBuilder.toString();
    Preconditions.checkArgument(bool, str);
  }
  
  private static boolean isValidCapability(int paramInt) {
    boolean bool;
    if (paramInt >= 0 && paramInt <= 30) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static void checkValidCapability(int paramInt) {
    boolean bool = isValidCapability(paramInt);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("NetworkCapability ");
    stringBuilder.append(paramInt);
    stringBuilder.append("out of range");
    Preconditions.checkArgument(bool, stringBuilder.toString());
  }
  
  public boolean isMetered() {
    return hasCapability(11) ^ true;
  }
  
  public boolean isPrivateDnsBroken() {
    return this.mPrivateDnsBroken;
  }
  
  public void setPrivateDnsBroken(boolean paramBoolean) {
    this.mPrivateDnsBroken = paramBoolean;
  }
  
  private boolean equalsPrivateDnsBroken(NetworkCapabilities paramNetworkCapabilities) {
    boolean bool;
    if (this.mPrivateDnsBroken == paramNetworkCapabilities.mPrivateDnsBroken) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public NetworkCapabilities setRequestorUid(int paramInt) {
    this.mRequestorUid = paramInt;
    return this;
  }
  
  public int getRequestorUid() {
    return this.mRequestorUid;
  }
  
  public NetworkCapabilities setRequestorPackageName(String paramString) {
    this.mRequestorPackageName = paramString;
    return this;
  }
  
  public String getRequestorPackageName() {
    return this.mRequestorPackageName;
  }
  
  public NetworkCapabilities setRequestorUidAndPackageName(int paramInt, String paramString) {
    return setRequestorUid(paramInt).setRequestorPackageName(paramString);
  }
  
  private boolean satisfiedByRequestor(NetworkCapabilities paramNetworkCapabilities) {
    int i = this.mRequestorUid;
    if (i != -1) {
      int j = paramNetworkCapabilities.mRequestorUid;
      if (j != -1) {
        if (i != j)
          return false; 
        String str = paramNetworkCapabilities.mRequestorPackageName;
        if (str != null) {
          String str1 = this.mRequestorPackageName;
          if (str1 != null)
            return TextUtils.equals(str1, str); 
        } 
        return true;
      } 
    } 
    return true;
  }
  
  private void combineRequestor(NetworkCapabilities paramNetworkCapabilities) {
    int i = this.mRequestorUid;
    if (i == -1 || i == paramNetworkCapabilities.mOwnerUid) {
      String str = this.mRequestorPackageName;
      if (str != null) {
        String str1 = paramNetworkCapabilities.mRequestorPackageName;
        if (!str.equals(str1))
          throw new IllegalStateException("Can't combine two package names"); 
      } 
      setRequestorUid(paramNetworkCapabilities.mRequestorUid);
      setRequestorPackageName(paramNetworkCapabilities.mRequestorPackageName);
      return;
    } 
    throw new IllegalStateException("Can't combine two uids");
  }
  
  private boolean equalsRequestor(NetworkCapabilities paramNetworkCapabilities) {
    if (this.mRequestorUid == paramNetworkCapabilities.mRequestorUid) {
      String str2 = this.mRequestorPackageName, str1 = paramNetworkCapabilities.mRequestorPackageName;
      if (TextUtils.equals(str2, str1))
        return true; 
    } 
    return false;
  }
  
  @SystemApi
  class Builder {
    private final NetworkCapabilities mCaps;
    
    public Builder() {
      this.mCaps = new NetworkCapabilities();
    }
    
    public Builder(NetworkCapabilities this$0) {
      Objects.requireNonNull(this$0);
      this.mCaps = new NetworkCapabilities(this$0);
    }
    
    public Builder addTransportType(int param1Int) {
      NetworkCapabilities.checkValidTransportType(param1Int);
      this.mCaps.addTransportType(param1Int);
      return this;
    }
    
    public Builder removeTransportType(int param1Int) {
      NetworkCapabilities.checkValidTransportType(param1Int);
      this.mCaps.removeTransportType(param1Int);
      return this;
    }
    
    public Builder addCapability(int param1Int) {
      this.mCaps.setCapability(param1Int, true);
      return this;
    }
    
    public Builder removeCapability(int param1Int) {
      this.mCaps.setCapability(param1Int, false);
      return this;
    }
    
    public Builder setOwnerUid(int param1Int) {
      this.mCaps.setOwnerUid(param1Int);
      return this;
    }
    
    public Builder setAdministratorUids(int[] param1ArrayOfint) {
      Objects.requireNonNull(param1ArrayOfint);
      this.mCaps.setAdministratorUids(param1ArrayOfint);
      return this;
    }
    
    public Builder setLinkUpstreamBandwidthKbps(int param1Int) {
      this.mCaps.setLinkUpstreamBandwidthKbps(param1Int);
      return this;
    }
    
    public Builder setLinkDownstreamBandwidthKbps(int param1Int) {
      this.mCaps.setLinkDownstreamBandwidthKbps(param1Int);
      return this;
    }
    
    public Builder setNetworkSpecifier(NetworkSpecifier param1NetworkSpecifier) {
      this.mCaps.setNetworkSpecifier(param1NetworkSpecifier);
      return this;
    }
    
    public Builder setTransportInfo(TransportInfo param1TransportInfo) {
      this.mCaps.setTransportInfo(param1TransportInfo);
      return this;
    }
    
    public Builder setSignalStrength(int param1Int) {
      this.mCaps.setSignalStrength(param1Int);
      return this;
    }
    
    public Builder setSsid(String param1String) {
      this.mCaps.setSSID(param1String);
      return this;
    }
    
    public Builder setRequestorUid(int param1Int) {
      this.mCaps.setRequestorUid(param1Int);
      return this;
    }
    
    public Builder setRequestorPackageName(String param1String) {
      this.mCaps.setRequestorPackageName(param1String);
      return this;
    }
    
    public NetworkCapabilities build() {
      if (this.mCaps.getOwnerUid() == -1 || 
        ArrayUtils.contains(this.mCaps.getAdministratorUids(), this.mCaps.getOwnerUid()))
        return new NetworkCapabilities(this.mCaps); 
      throw new IllegalStateException("The owner UID must be included in  administrator UIDs.");
    }
  }
  
  class NameOf {
    public abstract String nameOf(int param1Int);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class NetCapability implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Transport implements Annotation {}
}
