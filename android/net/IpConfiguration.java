package android.net;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

@SystemApi
public final class IpConfiguration implements Parcelable {
  class IpAssignment extends Enum<IpAssignment> {
    private static final IpAssignment[] $VALUES;
    
    public static final IpAssignment DHCP = new IpAssignment("DHCP", 1);
    
    public static IpAssignment[] values() {
      return (IpAssignment[])$VALUES.clone();
    }
    
    public static IpAssignment valueOf(String param1String) {
      return Enum.<IpAssignment>valueOf(IpAssignment.class, param1String);
    }
    
    private IpAssignment(IpConfiguration this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static final IpAssignment STATIC = new IpAssignment("STATIC", 0);
    
    public static final IpAssignment UNASSIGNED;
    
    static {
      IpAssignment ipAssignment = new IpAssignment("UNASSIGNED", 2);
      $VALUES = new IpAssignment[] { STATIC, DHCP, ipAssignment };
    }
  }
  
  class ProxySettings extends Enum<ProxySettings> {
    private static final ProxySettings[] $VALUES;
    
    public static ProxySettings[] values() {
      return (ProxySettings[])$VALUES.clone();
    }
    
    public static ProxySettings valueOf(String param1String) {
      return Enum.<ProxySettings>valueOf(ProxySettings.class, param1String);
    }
    
    private ProxySettings(IpConfiguration this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static final ProxySettings NONE = new ProxySettings("NONE", 0);
    
    public static final ProxySettings PAC;
    
    public static final ProxySettings STATIC = new ProxySettings("STATIC", 1);
    
    public static final ProxySettings UNASSIGNED = new ProxySettings("UNASSIGNED", 2);
    
    static {
      ProxySettings proxySettings = new ProxySettings("PAC", 3);
      $VALUES = new ProxySettings[] { NONE, STATIC, UNASSIGNED, proxySettings };
    }
  }
  
  private void init(IpAssignment paramIpAssignment, ProxySettings paramProxySettings, StaticIpConfiguration paramStaticIpConfiguration, ProxyInfo paramProxyInfo) {
    StaticIpConfiguration staticIpConfiguration;
    ProxyInfo proxyInfo;
    this.ipAssignment = paramIpAssignment;
    this.proxySettings = paramProxySettings;
    paramProxySettings = null;
    if (paramStaticIpConfiguration == null) {
      paramIpAssignment = null;
    } else {
      staticIpConfiguration = new StaticIpConfiguration(paramStaticIpConfiguration);
    } 
    this.staticIpConfiguration = staticIpConfiguration;
    if (paramProxyInfo == null) {
      ProxySettings proxySettings = paramProxySettings;
    } else {
      proxyInfo = new ProxyInfo(paramProxyInfo);
    } 
    this.httpProxy = proxyInfo;
  }
  
  public IpConfiguration() {
    init(IpAssignment.UNASSIGNED, ProxySettings.UNASSIGNED, null, null);
  }
  
  public IpConfiguration(IpAssignment paramIpAssignment, ProxySettings paramProxySettings, StaticIpConfiguration paramStaticIpConfiguration, ProxyInfo paramProxyInfo) {
    init(paramIpAssignment, paramProxySettings, paramStaticIpConfiguration, paramProxyInfo);
  }
  
  public IpConfiguration(IpConfiguration paramIpConfiguration) {
    this();
    if (paramIpConfiguration != null)
      init(paramIpConfiguration.ipAssignment, paramIpConfiguration.proxySettings, paramIpConfiguration.staticIpConfiguration, paramIpConfiguration.httpProxy); 
  }
  
  public IpAssignment getIpAssignment() {
    return this.ipAssignment;
  }
  
  public void setIpAssignment(IpAssignment paramIpAssignment) {
    this.ipAssignment = paramIpAssignment;
  }
  
  public StaticIpConfiguration getStaticIpConfiguration() {
    return this.staticIpConfiguration;
  }
  
  public void setStaticIpConfiguration(StaticIpConfiguration paramStaticIpConfiguration) {
    this.staticIpConfiguration = paramStaticIpConfiguration;
  }
  
  public ProxySettings getProxySettings() {
    return this.proxySettings;
  }
  
  public void setProxySettings(ProxySettings paramProxySettings) {
    this.proxySettings = paramProxySettings;
  }
  
  public ProxyInfo getHttpProxy() {
    return this.httpProxy;
  }
  
  public void setHttpProxy(ProxyInfo paramProxyInfo) {
    this.httpProxy = paramProxyInfo;
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("IP assignment: ");
    stringBuilder2.append(this.ipAssignment.toString());
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append("\n");
    if (this.staticIpConfiguration != null) {
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append("Static configuration: ");
      stringBuilder2.append(this.staticIpConfiguration.toString());
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder1.append("\n");
    } 
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("Proxy settings: ");
    stringBuilder2.append(this.proxySettings.toString());
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append("\n");
    if (this.httpProxy != null) {
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append("HTTP proxy: ");
      stringBuilder2.append(this.httpProxy.toString());
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder1.append("\n");
    } 
    return stringBuilder1.toString();
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (paramObject == this)
      return true; 
    if (!(paramObject instanceof IpConfiguration))
      return false; 
    paramObject = paramObject;
    if (this.ipAssignment == ((IpConfiguration)paramObject).ipAssignment && this.proxySettings == ((IpConfiguration)paramObject).proxySettings) {
      StaticIpConfiguration staticIpConfiguration1 = this.staticIpConfiguration, staticIpConfiguration2 = ((IpConfiguration)paramObject).staticIpConfiguration;
      if (Objects.equals(staticIpConfiguration1, staticIpConfiguration2)) {
        ProxyInfo proxyInfo = this.httpProxy;
        paramObject = ((IpConfiguration)paramObject).httpProxy;
        if (Objects.equals(proxyInfo, paramObject))
          return null; 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    byte b;
    StaticIpConfiguration staticIpConfiguration = this.staticIpConfiguration;
    if (staticIpConfiguration != null) {
      b = staticIpConfiguration.hashCode();
    } else {
      b = 0;
    } 
    IpAssignment ipAssignment = this.ipAssignment;
    int i = ipAssignment.ordinal();
    ProxySettings proxySettings = this.proxySettings;
    int j = proxySettings.ordinal();
    ProxyInfo proxyInfo = this.httpProxy;
    int k = proxyInfo.hashCode();
    return b + 13 + i * 17 + j * 47 + k * 83;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.ipAssignment.name());
    paramParcel.writeString(this.proxySettings.name());
    paramParcel.writeParcelable(this.staticIpConfiguration, paramInt);
    paramParcel.writeParcelable(this.httpProxy, paramInt);
  }
  
  public static final Parcelable.Creator<IpConfiguration> CREATOR = new Parcelable.Creator<IpConfiguration>() {
      public IpConfiguration createFromParcel(Parcel param1Parcel) {
        IpConfiguration ipConfiguration = new IpConfiguration();
        ipConfiguration.ipAssignment = IpConfiguration.IpAssignment.valueOf(param1Parcel.readString());
        ipConfiguration.proxySettings = IpConfiguration.ProxySettings.valueOf(param1Parcel.readString());
        ipConfiguration.staticIpConfiguration = param1Parcel.<StaticIpConfiguration>readParcelable(null);
        ipConfiguration.httpProxy = param1Parcel.<ProxyInfo>readParcelable(null);
        return ipConfiguration;
      }
      
      public IpConfiguration[] newArray(int param1Int) {
        return new IpConfiguration[param1Int];
      }
    };
  
  private static final String TAG = "IpConfiguration";
  
  public ProxyInfo httpProxy;
  
  public IpAssignment ipAssignment;
  
  public ProxySettings proxySettings;
  
  public StaticIpConfiguration staticIpConfiguration;
}
