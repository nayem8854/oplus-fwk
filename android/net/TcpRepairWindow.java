package android.net;

public final class TcpRepairWindow {
  public final int maxWindow;
  
  public final int rcvWnd;
  
  public final int rcvWndScale;
  
  public final int rcvWup;
  
  public final int sndWl1;
  
  public final int sndWnd;
  
  public TcpRepairWindow(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    this.sndWl1 = paramInt1;
    this.sndWnd = paramInt2;
    this.maxWindow = paramInt3;
    this.rcvWnd = paramInt4;
    this.rcvWup = paramInt5;
    this.rcvWndScale = paramInt6;
  }
}
