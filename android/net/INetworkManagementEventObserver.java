package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INetworkManagementEventObserver extends IInterface {
  void addressRemoved(String paramString, LinkAddress paramLinkAddress) throws RemoteException;
  
  void addressUpdated(String paramString, LinkAddress paramLinkAddress) throws RemoteException;
  
  void interfaceAdded(String paramString) throws RemoteException;
  
  void interfaceClassDataActivityChanged(String paramString, boolean paramBoolean, long paramLong) throws RemoteException;
  
  void interfaceDnsServerInfo(String paramString, long paramLong, String[] paramArrayOfString) throws RemoteException;
  
  void interfaceLinkStateChanged(String paramString, boolean paramBoolean) throws RemoteException;
  
  void interfaceRemoved(String paramString) throws RemoteException;
  
  void interfaceStatusChanged(String paramString, boolean paramBoolean) throws RemoteException;
  
  void limitReached(String paramString1, String paramString2) throws RemoteException;
  
  void routeRemoved(RouteInfo paramRouteInfo) throws RemoteException;
  
  void routeUpdated(RouteInfo paramRouteInfo) throws RemoteException;
  
  class Default implements INetworkManagementEventObserver {
    public void interfaceStatusChanged(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void interfaceLinkStateChanged(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void interfaceAdded(String param1String) throws RemoteException {}
    
    public void interfaceRemoved(String param1String) throws RemoteException {}
    
    public void addressUpdated(String param1String, LinkAddress param1LinkAddress) throws RemoteException {}
    
    public void addressRemoved(String param1String, LinkAddress param1LinkAddress) throws RemoteException {}
    
    public void limitReached(String param1String1, String param1String2) throws RemoteException {}
    
    public void interfaceClassDataActivityChanged(String param1String, boolean param1Boolean, long param1Long) throws RemoteException {}
    
    public void interfaceDnsServerInfo(String param1String, long param1Long, String[] param1ArrayOfString) throws RemoteException {}
    
    public void routeUpdated(RouteInfo param1RouteInfo) throws RemoteException {}
    
    public void routeRemoved(RouteInfo param1RouteInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INetworkManagementEventObserver {
    private static final String DESCRIPTOR = "android.net.INetworkManagementEventObserver";
    
    static final int TRANSACTION_addressRemoved = 6;
    
    static final int TRANSACTION_addressUpdated = 5;
    
    static final int TRANSACTION_interfaceAdded = 3;
    
    static final int TRANSACTION_interfaceClassDataActivityChanged = 8;
    
    static final int TRANSACTION_interfaceDnsServerInfo = 9;
    
    static final int TRANSACTION_interfaceLinkStateChanged = 2;
    
    static final int TRANSACTION_interfaceRemoved = 4;
    
    static final int TRANSACTION_interfaceStatusChanged = 1;
    
    static final int TRANSACTION_limitReached = 7;
    
    static final int TRANSACTION_routeRemoved = 11;
    
    static final int TRANSACTION_routeUpdated = 10;
    
    public Stub() {
      attachInterface(this, "android.net.INetworkManagementEventObserver");
    }
    
    public static INetworkManagementEventObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.INetworkManagementEventObserver");
      if (iInterface != null && iInterface instanceof INetworkManagementEventObserver)
        return (INetworkManagementEventObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 11:
          return "routeRemoved";
        case 10:
          return "routeUpdated";
        case 9:
          return "interfaceDnsServerInfo";
        case 8:
          return "interfaceClassDataActivityChanged";
        case 7:
          return "limitReached";
        case 6:
          return "addressRemoved";
        case 5:
          return "addressUpdated";
        case 4:
          return "interfaceRemoved";
        case 3:
          return "interfaceAdded";
        case 2:
          return "interfaceLinkStateChanged";
        case 1:
          break;
      } 
      return "interfaceStatusChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        String arrayOfString[], str1;
        long l;
        boolean bool1 = false, bool2 = false, bool3 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 11:
            param1Parcel1.enforceInterface("android.net.INetworkManagementEventObserver");
            if (param1Parcel1.readInt() != 0) {
              RouteInfo routeInfo = RouteInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            routeRemoved((RouteInfo)param1Parcel1);
            return true;
          case 10:
            param1Parcel1.enforceInterface("android.net.INetworkManagementEventObserver");
            if (param1Parcel1.readInt() != 0) {
              RouteInfo routeInfo = RouteInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            routeUpdated((RouteInfo)param1Parcel1);
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.net.INetworkManagementEventObserver");
            str = param1Parcel1.readString();
            l = param1Parcel1.readLong();
            arrayOfString = param1Parcel1.createStringArray();
            interfaceDnsServerInfo(str, l, arrayOfString);
            return true;
          case 8:
            arrayOfString.enforceInterface("android.net.INetworkManagementEventObserver");
            str = arrayOfString.readString();
            bool1 = bool3;
            if (arrayOfString.readInt() != 0)
              bool1 = true; 
            l = arrayOfString.readLong();
            interfaceClassDataActivityChanged(str, bool1, l);
            return true;
          case 7:
            arrayOfString.enforceInterface("android.net.INetworkManagementEventObserver");
            str = arrayOfString.readString();
            str1 = arrayOfString.readString();
            limitReached(str, str1);
            return true;
          case 6:
            str1.enforceInterface("android.net.INetworkManagementEventObserver");
            str = str1.readString();
            if (str1.readInt() != 0) {
              LinkAddress linkAddress = LinkAddress.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            addressRemoved(str, (LinkAddress)str1);
            return true;
          case 5:
            str1.enforceInterface("android.net.INetworkManagementEventObserver");
            str = str1.readString();
            if (str1.readInt() != 0) {
              LinkAddress linkAddress = LinkAddress.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            addressUpdated(str, (LinkAddress)str1);
            return true;
          case 4:
            str1.enforceInterface("android.net.INetworkManagementEventObserver");
            str1 = str1.readString();
            interfaceRemoved(str1);
            return true;
          case 3:
            str1.enforceInterface("android.net.INetworkManagementEventObserver");
            str1 = str1.readString();
            interfaceAdded(str1);
            return true;
          case 2:
            str1.enforceInterface("android.net.INetworkManagementEventObserver");
            str = str1.readString();
            if (str1.readInt() != 0)
              bool1 = true; 
            interfaceLinkStateChanged(str, bool1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.net.INetworkManagementEventObserver");
        str = str1.readString();
        bool1 = bool2;
        if (str1.readInt() != 0)
          bool1 = true; 
        interfaceStatusChanged(str, bool1);
        return true;
      } 
      str.writeString("android.net.INetworkManagementEventObserver");
      return true;
    }
    
    private static class Proxy implements INetworkManagementEventObserver {
      public static INetworkManagementEventObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.INetworkManagementEventObserver";
      }
      
      public void interfaceStatusChanged(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.net.INetworkManagementEventObserver");
          parcel.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool1 && INetworkManagementEventObserver.Stub.getDefaultImpl() != null) {
            INetworkManagementEventObserver.Stub.getDefaultImpl().interfaceStatusChanged(param2String, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void interfaceLinkStateChanged(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.net.INetworkManagementEventObserver");
          parcel.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool1 && INetworkManagementEventObserver.Stub.getDefaultImpl() != null) {
            INetworkManagementEventObserver.Stub.getDefaultImpl().interfaceLinkStateChanged(param2String, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void interfaceAdded(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.INetworkManagementEventObserver");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && INetworkManagementEventObserver.Stub.getDefaultImpl() != null) {
            INetworkManagementEventObserver.Stub.getDefaultImpl().interfaceAdded(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void interfaceRemoved(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.INetworkManagementEventObserver");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && INetworkManagementEventObserver.Stub.getDefaultImpl() != null) {
            INetworkManagementEventObserver.Stub.getDefaultImpl().interfaceRemoved(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void addressUpdated(String param2String, LinkAddress param2LinkAddress) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.INetworkManagementEventObserver");
          parcel.writeString(param2String);
          if (param2LinkAddress != null) {
            parcel.writeInt(1);
            param2LinkAddress.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && INetworkManagementEventObserver.Stub.getDefaultImpl() != null) {
            INetworkManagementEventObserver.Stub.getDefaultImpl().addressUpdated(param2String, param2LinkAddress);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void addressRemoved(String param2String, LinkAddress param2LinkAddress) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.INetworkManagementEventObserver");
          parcel.writeString(param2String);
          if (param2LinkAddress != null) {
            parcel.writeInt(1);
            param2LinkAddress.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && INetworkManagementEventObserver.Stub.getDefaultImpl() != null) {
            INetworkManagementEventObserver.Stub.getDefaultImpl().addressRemoved(param2String, param2LinkAddress);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void limitReached(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.INetworkManagementEventObserver");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && INetworkManagementEventObserver.Stub.getDefaultImpl() != null) {
            INetworkManagementEventObserver.Stub.getDefaultImpl().limitReached(param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void interfaceClassDataActivityChanged(String param2String, boolean param2Boolean, long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.net.INetworkManagementEventObserver");
          parcel.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeLong(param2Long);
          boolean bool1 = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool1 && INetworkManagementEventObserver.Stub.getDefaultImpl() != null) {
            INetworkManagementEventObserver.Stub.getDefaultImpl().interfaceClassDataActivityChanged(param2String, param2Boolean, param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void interfaceDnsServerInfo(String param2String, long param2Long, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.INetworkManagementEventObserver");
          parcel.writeString(param2String);
          parcel.writeLong(param2Long);
          parcel.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(9, parcel, (Parcel)null, 1);
          if (!bool && INetworkManagementEventObserver.Stub.getDefaultImpl() != null) {
            INetworkManagementEventObserver.Stub.getDefaultImpl().interfaceDnsServerInfo(param2String, param2Long, param2ArrayOfString);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void routeUpdated(RouteInfo param2RouteInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.INetworkManagementEventObserver");
          if (param2RouteInfo != null) {
            parcel.writeInt(1);
            param2RouteInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel, (Parcel)null, 1);
          if (!bool && INetworkManagementEventObserver.Stub.getDefaultImpl() != null) {
            INetworkManagementEventObserver.Stub.getDefaultImpl().routeUpdated(param2RouteInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void routeRemoved(RouteInfo param2RouteInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.INetworkManagementEventObserver");
          if (param2RouteInfo != null) {
            parcel.writeInt(1);
            param2RouteInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel, (Parcel)null, 1);
          if (!bool && INetworkManagementEventObserver.Stub.getDefaultImpl() != null) {
            INetworkManagementEventObserver.Stub.getDefaultImpl().routeRemoved(param2RouteInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INetworkManagementEventObserver param1INetworkManagementEventObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INetworkManagementEventObserver != null) {
          Proxy.sDefaultImpl = param1INetworkManagementEventObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INetworkManagementEventObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
