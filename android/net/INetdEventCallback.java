package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INetdEventCallback extends IInterface {
  public static final int CALLBACK_CALLER_CONNECTIVITY_SERVICE = 0;
  
  public static final int CALLBACK_CALLER_DEVICE_POLICY = 1;
  
  public static final int CALLBACK_CALLER_NETWORK_WATCHLIST = 2;
  
  void onConnectEvent(String paramString, int paramInt1, long paramLong, int paramInt2) throws RemoteException;
  
  void onDnsEvent(int paramInt1, int paramInt2, int paramInt3, String paramString, String[] paramArrayOfString, int paramInt4, long paramLong, int paramInt5) throws RemoteException;
  
  void onNat64PrefixEvent(int paramInt1, boolean paramBoolean, String paramString, int paramInt2) throws RemoteException;
  
  void onPrivateDnsValidationEvent(int paramInt, String paramString1, String paramString2, boolean paramBoolean) throws RemoteException;
  
  class Default implements INetdEventCallback {
    public void onDnsEvent(int param1Int1, int param1Int2, int param1Int3, String param1String, String[] param1ArrayOfString, int param1Int4, long param1Long, int param1Int5) throws RemoteException {}
    
    public void onNat64PrefixEvent(int param1Int1, boolean param1Boolean, String param1String, int param1Int2) throws RemoteException {}
    
    public void onPrivateDnsValidationEvent(int param1Int, String param1String1, String param1String2, boolean param1Boolean) throws RemoteException {}
    
    public void onConnectEvent(String param1String, int param1Int1, long param1Long, int param1Int2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INetdEventCallback {
    private static final String DESCRIPTOR = "android.net.INetdEventCallback";
    
    static final int TRANSACTION_onConnectEvent = 4;
    
    static final int TRANSACTION_onDnsEvent = 1;
    
    static final int TRANSACTION_onNat64PrefixEvent = 2;
    
    static final int TRANSACTION_onPrivateDnsValidationEvent = 3;
    
    public Stub() {
      attachInterface(this, "android.net.INetdEventCallback");
    }
    
    public static INetdEventCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.INetdEventCallback");
      if (iInterface != null && iInterface instanceof INetdEventCallback)
        return (INetdEventCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onConnectEvent";
          } 
          return "onPrivateDnsValidationEvent";
        } 
        return "onNat64PrefixEvent";
      } 
      return "onDnsEvent";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        boolean bool1 = false, bool2 = false;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.net.INetdEventCallback");
              return true;
            } 
            param1Parcel1.enforceInterface("android.net.INetdEventCallback");
            String str4 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            long l1 = param1Parcel1.readLong();
            param1Int2 = param1Parcel1.readInt();
            onConnectEvent(str4, param1Int1, l1, param1Int2);
            return true;
          } 
          param1Parcel1.enforceInterface("android.net.INetdEventCallback");
          param1Int1 = param1Parcel1.readInt();
          String str3 = param1Parcel1.readString();
          String str2 = param1Parcel1.readString();
          if (param1Parcel1.readInt() != 0)
            bool2 = true; 
          onPrivateDnsValidationEvent(param1Int1, str3, str2, bool2);
          return true;
        } 
        param1Parcel1.enforceInterface("android.net.INetdEventCallback");
        param1Int2 = param1Parcel1.readInt();
        bool2 = bool1;
        if (param1Parcel1.readInt() != 0)
          bool2 = true; 
        String str1 = param1Parcel1.readString();
        param1Int1 = param1Parcel1.readInt();
        onNat64PrefixEvent(param1Int2, bool2, str1, param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.net.INetdEventCallback");
      int i = param1Parcel1.readInt();
      int j = param1Parcel1.readInt();
      param1Int2 = param1Parcel1.readInt();
      String str = param1Parcel1.readString();
      String[] arrayOfString = param1Parcel1.createStringArray();
      param1Int1 = param1Parcel1.readInt();
      long l = param1Parcel1.readLong();
      int k = param1Parcel1.readInt();
      onDnsEvent(i, j, param1Int2, str, arrayOfString, param1Int1, l, k);
      return true;
    }
    
    private static class Proxy implements INetdEventCallback {
      public static INetdEventCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.INetdEventCallback";
      }
      
      public void onDnsEvent(int param2Int1, int param2Int2, int param2Int3, String param2String, String[] param2ArrayOfString, int param2Int4, long param2Long, int param2Int5) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.INetdEventCallback");
          try {
            parcel.writeInt(param2Int1);
            try {
              parcel.writeInt(param2Int2);
              try {
                parcel.writeInt(param2Int3);
                try {
                  parcel.writeString(param2String);
                  parcel.writeStringArray(param2ArrayOfString);
                  parcel.writeInt(param2Int4);
                  parcel.writeLong(param2Long);
                  parcel.writeInt(param2Int5);
                  boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
                  if (!bool && INetdEventCallback.Stub.getDefaultImpl() != null) {
                    INetdEventCallback.Stub.getDefaultImpl().onDnsEvent(param2Int1, param2Int2, param2Int3, param2String, param2ArrayOfString, param2Int4, param2Long, param2Int5);
                    parcel.recycle();
                    return;
                  } 
                  parcel.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2String;
      }
      
      public void onNat64PrefixEvent(int param2Int1, boolean param2Boolean, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.net.INetdEventCallback");
          parcel.writeInt(param2Int1);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeString(param2String);
          parcel.writeInt(param2Int2);
          boolean bool1 = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool1 && INetdEventCallback.Stub.getDefaultImpl() != null) {
            INetdEventCallback.Stub.getDefaultImpl().onNat64PrefixEvent(param2Int1, param2Boolean, param2String, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPrivateDnsValidationEvent(int param2Int, String param2String1, String param2String2, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.net.INetdEventCallback");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool1 && INetdEventCallback.Stub.getDefaultImpl() != null) {
            INetdEventCallback.Stub.getDefaultImpl().onPrivateDnsValidationEvent(param2Int, param2String1, param2String2, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onConnectEvent(String param2String, int param2Int1, long param2Long, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.INetdEventCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeLong(param2Long);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && INetdEventCallback.Stub.getDefaultImpl() != null) {
            INetdEventCallback.Stub.getDefaultImpl().onConnectEvent(param2String, param2Int1, param2Long, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INetdEventCallback param1INetdEventCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INetdEventCallback != null) {
          Proxy.sDefaultImpl = param1INetdEventCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INetdEventCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
