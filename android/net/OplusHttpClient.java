package android.net;

import android.content.Context;
import android.os.SystemProperties;
import android.util.Log;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class OplusHttpClient {
  private static final long AVERAGE_RECEIVE_TIME = 832L;
  
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static final long GMT_BEIJING_OFFSET = 28800000L;
  
  private static final String TAG = "OplusHttpClient";
  
  private static final long VALID_LAST_TIME_THRESHOLD = 1500L;
  
  private static long mLastGotSuccessLocaltime = 0L;
  
  private long mHttpTime;
  
  private long mHttpTimeReference;
  
  private long mRoundTripTime;
  
  private class MyX509TrustManager implements X509TrustManager {
    final OplusHttpClient this$0;
    
    private MyX509TrustManager() {}
    
    public void checkClientTrusted(X509Certificate[] param1ArrayOfX509Certificate, String param1String) throws CertificateException {}
    
    public void checkServerTrusted(X509Certificate[] param1ArrayOfX509Certificate, String param1String) throws CertificateException {}
    
    public X509Certificate[] getAcceptedIssuers() {
      return null;
    }
  }
  
  public boolean requestTime(Context paramContext, int paramInt1, int paramInt2) {
    return forceRefreshTimeFromOppoServer(paramContext, paramInt1, paramInt2);
  }
  
  private boolean forceRefreshTimeFromOppoServer(Context paramContext, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: ldc 'OplusHttpClient'
    //   2: ldc 'Enter forceRefreshTimeFromOppoServer run'
    //   4: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   7: pop
    //   8: aload_1
    //   9: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   12: ldc 201589022
    //   14: invokevirtual getString : (I)Ljava/lang/String;
    //   17: astore #4
    //   19: iload_2
    //   20: ifle -> 41
    //   23: aload_1
    //   24: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   27: ldc 201589023
    //   29: invokevirtual getString : (I)Ljava/lang/String;
    //   32: astore #4
    //   34: goto -> 41
    //   37: astore_1
    //   38: goto -> 1792
    //   41: getstatic android/os/OplusPropertyList.OPLUS_REGIONMARK : Ljava/lang/String;
    //   44: invokevirtual toLowerCase : ()Ljava/lang/String;
    //   47: ldc 'euex'
    //   49: invokevirtual equals : (Ljava/lang/Object;)Z
    //   52: istore #5
    //   54: iload #5
    //   56: ifeq -> 113
    //   59: aload #4
    //   61: astore #6
    //   63: getstatic android/net/OplusHttpClient.DEBUG : Z
    //   66: ifeq -> 227
    //   69: new java/lang/StringBuilder
    //   72: astore #6
    //   74: aload #6
    //   76: invokespecial <init> : ()V
    //   79: aload #6
    //   81: ldc 'eu http request:'
    //   83: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   86: pop
    //   87: aload #6
    //   89: aload #4
    //   91: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   94: pop
    //   95: ldc 'OplusHttpClient'
    //   97: aload #6
    //   99: invokevirtual toString : ()Ljava/lang/String;
    //   102: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   105: pop
    //   106: aload #4
    //   108: astore #6
    //   110: goto -> 227
    //   113: getstatic android/os/OplusPropertyList.OPLUS_REGIONMARK : Ljava/lang/String;
    //   116: invokevirtual toLowerCase : ()Ljava/lang/String;
    //   119: ldc 'in'
    //   121: invokevirtual equals : (Ljava/lang/Object;)Z
    //   124: istore #5
    //   126: iload #5
    //   128: ifeq -> 145
    //   131: aload #4
    //   133: ldc 'eu'
    //   135: ldc 'in'
    //   137: invokevirtual replace : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   140: astore #6
    //   142: goto -> 227
    //   145: getstatic android/os/OplusPropertyList.OPLUS_REGIONMARK : Ljava/lang/String;
    //   148: invokevirtual toLowerCase : ()Ljava/lang/String;
    //   151: ldc 'cn'
    //   153: invokevirtual equals : (Ljava/lang/Object;)Z
    //   156: istore #5
    //   158: iload #5
    //   160: ifne -> 198
    //   163: ldc 'ro.oplus.image.my_region.type'
    //   165: ldc ''
    //   167: invokestatic get : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   170: invokevirtual toLowerCase : ()Ljava/lang/String;
    //   173: ldc 'cn'
    //   175: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   178: ifeq -> 184
    //   181: goto -> 198
    //   184: aload #4
    //   186: ldc 'eu'
    //   188: ldc 'sg'
    //   190: invokevirtual replace : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   193: astore #6
    //   195: goto -> 227
    //   198: iload_2
    //   199: ifle -> 216
    //   202: aload #4
    //   204: ldc 'fourier-newds02-eu'
    //   206: ldc 'newds02'
    //   208: invokevirtual replace : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   211: astore #6
    //   213: goto -> 227
    //   216: aload #4
    //   218: ldc 'fourier-newds01-eu'
    //   220: ldc 'newds01'
    //   222: invokevirtual replace : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   225: astore #6
    //   227: new java/lang/StringBuilder
    //   230: astore #4
    //   232: aload #4
    //   234: invokespecial <init> : ()V
    //   237: aload #4
    //   239: aload #6
    //   241: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   244: pop
    //   245: aload #4
    //   247: invokestatic currentTimeMillis : ()J
    //   250: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   253: pop
    //   254: aload #4
    //   256: invokevirtual toString : ()Ljava/lang/String;
    //   259: astore #7
    //   261: ldc 'SSL'
    //   263: invokestatic getInstance : (Ljava/lang/String;)Ljavax/net/ssl/SSLContext;
    //   266: astore #4
    //   268: new android/net/OplusHttpClient$MyX509TrustManager
    //   271: astore #6
    //   273: aload #6
    //   275: aload_0
    //   276: aconst_null
    //   277: invokespecial <init> : (Landroid/net/OplusHttpClient;Landroid/net/OplusHttpClient$1;)V
    //   280: new java/security/SecureRandom
    //   283: astore #8
    //   285: aload #8
    //   287: invokespecial <init> : ()V
    //   290: aload #4
    //   292: aconst_null
    //   293: iconst_1
    //   294: anewarray javax/net/ssl/TrustManager
    //   297: dup
    //   298: iconst_0
    //   299: aload #6
    //   301: aastore
    //   302: aload #8
    //   304: invokevirtual init : ([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V
    //   307: new java/net/URL
    //   310: astore #6
    //   312: aload #6
    //   314: aload #7
    //   316: invokespecial <init> : (Ljava/lang/String;)V
    //   319: getstatic android/net/OplusHttpClient.DEBUG : Z
    //   322: istore #5
    //   324: iload #5
    //   326: ifeq -> 366
    //   329: new java/lang/StringBuilder
    //   332: astore #8
    //   334: aload #8
    //   336: invokespecial <init> : ()V
    //   339: aload #8
    //   341: ldc 'Cur http request:'
    //   343: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   346: pop
    //   347: aload #8
    //   349: aload #7
    //   351: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   354: pop
    //   355: ldc 'OplusHttpClient'
    //   357: aload #8
    //   359: invokevirtual toString : ()Ljava/lang/String;
    //   362: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   365: pop
    //   366: new android/net/OplusHttpClient$1
    //   369: astore #9
    //   371: aload #9
    //   373: aload_0
    //   374: invokespecial <init> : (Landroid/net/OplusHttpClient;)V
    //   377: invokestatic getDefaultHost : ()Ljava/lang/String;
    //   380: astore #8
    //   382: invokestatic getDefaultPort : ()I
    //   385: istore #10
    //   387: aload #9
    //   389: invokestatic setDefaultHostnameVerifier : (Ljavax/net/ssl/HostnameVerifier;)V
    //   392: aload #4
    //   394: invokevirtual getSocketFactory : ()Ljavax/net/ssl/SSLSocketFactory;
    //   397: invokestatic setDefaultSSLSocketFactory : (Ljavax/net/ssl/SSLSocketFactory;)V
    //   400: getstatic android/net/OplusHttpClient.DEBUG : Z
    //   403: istore #5
    //   405: iload #5
    //   407: ifeq -> 463
    //   410: new java/lang/StringBuilder
    //   413: astore #4
    //   415: aload #4
    //   417: invokespecial <init> : ()V
    //   420: aload #4
    //   422: ldc 'OppoServer proxyHost = '
    //   424: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   427: pop
    //   428: aload #4
    //   430: aload #8
    //   432: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   435: pop
    //   436: aload #4
    //   438: ldc ' proxyPort = '
    //   440: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   443: pop
    //   444: aload #4
    //   446: iload #10
    //   448: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   451: pop
    //   452: ldc 'OplusHttpClient'
    //   454: aload #4
    //   456: invokevirtual toString : ()Ljava/lang/String;
    //   459: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   462: pop
    //   463: aload_0
    //   464: aload_1
    //   465: invokespecial getNetType : (Landroid/content/Context;)Z
    //   468: istore #5
    //   470: iload #5
    //   472: ifeq -> 504
    //   475: ldc 'OplusHttpClient'
    //   477: ldc 'Get network type success!'
    //   479: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   482: pop
    //   483: aload #6
    //   485: invokevirtual openConnection : ()Ljava/net/URLConnection;
    //   488: checkcast javax/net/ssl/HttpsURLConnection
    //   491: astore #4
    //   493: ldc 'OplusHttpClient'
    //   495: ldc 'HttpURLConnection open openConnection success!'
    //   497: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   500: pop
    //   501: goto -> 555
    //   504: ldc 'OplusHttpClient'
    //   506: ldc 'Use http proxy!'
    //   508: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   511: pop
    //   512: new java/net/Proxy
    //   515: astore #9
    //   517: getstatic java/net/Proxy$Type.HTTP : Ljava/net/Proxy$Type;
    //   520: astore_1
    //   521: new java/net/InetSocketAddress
    //   524: astore #4
    //   526: aload #4
    //   528: aload #8
    //   530: iload #10
    //   532: invokespecial <init> : (Ljava/lang/String;I)V
    //   535: aload #9
    //   537: aload_1
    //   538: aload #4
    //   540: invokespecial <init> : (Ljava/net/Proxy$Type;Ljava/net/SocketAddress;)V
    //   543: aload #6
    //   545: aload #9
    //   547: invokevirtual openConnection : (Ljava/net/Proxy;)Ljava/net/URLConnection;
    //   550: checkcast javax/net/ssl/HttpsURLConnection
    //   553: astore #4
    //   555: aload #4
    //   557: ldc 'GET'
    //   559: invokevirtual setRequestMethod : (Ljava/lang/String;)V
    //   562: aload #4
    //   564: iconst_1
    //   565: invokevirtual setDoInput : (Z)V
    //   568: aload #4
    //   570: iconst_0
    //   571: invokevirtual setUseCaches : (Z)V
    //   574: aload #4
    //   576: iconst_0
    //   577: invokevirtual setInstanceFollowRedirects : (Z)V
    //   580: aload #4
    //   582: ldc_w 'Accept-Charset'
    //   585: ldc_w 'UTF-8'
    //   588: invokevirtual setRequestProperty : (Ljava/lang/String;Ljava/lang/String;)V
    //   591: iload_2
    //   592: ifle -> 602
    //   595: iload_3
    //   596: iconst_3
    //   597: imul
    //   598: istore_3
    //   599: goto -> 602
    //   602: new java/lang/StringBuilder
    //   605: astore_1
    //   606: aload_1
    //   607: invokespecial <init> : ()V
    //   610: aload_1
    //   611: ldc_w 'timeout:'
    //   614: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   617: pop
    //   618: aload_1
    //   619: iload_3
    //   620: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   623: pop
    //   624: ldc 'OplusHttpClient'
    //   626: aload_1
    //   627: invokevirtual toString : ()Ljava/lang/String;
    //   630: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   633: pop
    //   634: aload #4
    //   636: iload_3
    //   637: invokevirtual setConnectTimeout : (I)V
    //   640: aload #4
    //   642: iload_3
    //   643: invokevirtual setReadTimeout : (I)V
    //   646: invokestatic elapsedRealtime : ()J
    //   649: lstore #11
    //   651: getstatic android/net/OplusHttpClient.DEBUG : Z
    //   654: istore #5
    //   656: iload #5
    //   658: ifeq -> 677
    //   661: ldc 'OplusHttpClient'
    //   663: ldc_w 'Strart to connect http server!'
    //   666: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   669: pop
    //   670: goto -> 677
    //   673: astore_1
    //   674: goto -> 1792
    //   677: aload #4
    //   679: invokevirtual connect : ()V
    //   682: getstatic android/net/OplusHttpClient.DEBUG : Z
    //   685: istore #5
    //   687: iload #5
    //   689: ifeq -> 701
    //   692: ldc 'OplusHttpClient'
    //   694: ldc_w 'Connect http server success!'
    //   697: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   700: pop
    //   701: aconst_null
    //   702: astore #13
    //   704: aconst_null
    //   705: astore #14
    //   707: new java/lang/StringBuffer
    //   710: astore #9
    //   712: aload #9
    //   714: invokespecial <init> : ()V
    //   717: lconst_0
    //   718: lstore #15
    //   720: aload_0
    //   721: lconst_0
    //   722: putfield mHttpTimeReference : J
    //   725: aload #4
    //   727: invokevirtual getResponseCode : ()I
    //   730: istore_2
    //   731: new java/lang/StringBuilder
    //   734: astore_1
    //   735: aload_1
    //   736: invokespecial <init> : ()V
    //   739: aload_1
    //   740: ldc_w 'Http responseCode:'
    //   743: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   746: pop
    //   747: aload #6
    //   749: astore #17
    //   751: aload_1
    //   752: iload_2
    //   753: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   756: pop
    //   757: aload #6
    //   759: astore #17
    //   761: ldc 'OplusHttpClient'
    //   763: aload_1
    //   764: invokevirtual toString : ()Ljava/lang/String;
    //   767: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   770: pop
    //   771: iload_2
    //   772: sipush #200
    //   775: if_icmpne -> 1020
    //   778: aload #6
    //   780: astore #17
    //   782: invokestatic currentTimeMillis : ()J
    //   785: lstore #15
    //   787: aload #6
    //   789: astore #17
    //   791: new java/io/InputStreamReader
    //   794: astore #13
    //   796: aload #6
    //   798: astore #17
    //   800: aload #13
    //   802: aload #4
    //   804: invokevirtual getInputStream : ()Ljava/io/InputStream;
    //   807: ldc_w 'utf-8'
    //   810: invokespecial <init> : (Ljava/io/InputStream;Ljava/lang/String;)V
    //   813: aload #6
    //   815: astore #17
    //   817: new java/io/BufferedReader
    //   820: astore #14
    //   822: aload #6
    //   824: astore #17
    //   826: aload #14
    //   828: aload #13
    //   830: invokespecial <init> : (Ljava/io/Reader;)V
    //   833: ldc ''
    //   835: astore #8
    //   837: aload #6
    //   839: astore_1
    //   840: aload_1
    //   841: astore #17
    //   843: aload #14
    //   845: invokevirtual readLine : ()Ljava/lang/String;
    //   848: astore #18
    //   850: aload #18
    //   852: ifnull -> 952
    //   855: aload #18
    //   857: astore #8
    //   859: aload_1
    //   860: astore #17
    //   862: aload #9
    //   864: aload #18
    //   866: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   869: pop
    //   870: aload_1
    //   871: astore #6
    //   873: new java/lang/StringBuilder
    //   876: astore #17
    //   878: aload_1
    //   879: astore #6
    //   881: aload #17
    //   883: invokespecial <init> : ()V
    //   886: aload_1
    //   887: astore #6
    //   889: aload #17
    //   891: ldc_w 'Read response, lineString='
    //   894: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   897: pop
    //   898: aload_1
    //   899: astore #6
    //   901: aload #17
    //   903: aload #18
    //   905: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   908: pop
    //   909: aload_1
    //   910: astore #6
    //   912: aload #17
    //   914: ldc_w ',sb='
    //   917: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   920: pop
    //   921: aload_1
    //   922: astore #6
    //   924: aload #17
    //   926: aload #9
    //   928: invokevirtual toString : ()Ljava/lang/String;
    //   931: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   934: pop
    //   935: aload_1
    //   936: astore #6
    //   938: ldc 'OplusHttpClient'
    //   940: aload #17
    //   942: invokevirtual toString : ()Ljava/lang/String;
    //   945: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   948: pop
    //   949: goto -> 840
    //   952: aload #9
    //   954: astore #17
    //   956: aload_1
    //   957: astore #6
    //   959: new java/lang/StringBuilder
    //   962: astore #9
    //   964: aload_1
    //   965: astore #6
    //   967: aload #9
    //   969: invokespecial <init> : ()V
    //   972: aload_1
    //   973: astore #6
    //   975: aload #9
    //   977: ldc_w 'Read response data success! mDateTimeXmlString='
    //   980: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   983: pop
    //   984: aload_1
    //   985: astore #6
    //   987: aload #9
    //   989: aload #8
    //   991: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   994: pop
    //   995: aload_1
    //   996: astore #6
    //   998: ldc 'OplusHttpClient'
    //   1000: aload #9
    //   1002: invokevirtual toString : ()Ljava/lang/String;
    //   1005: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   1008: pop
    //   1009: aload #8
    //   1011: astore #9
    //   1013: aload #17
    //   1015: astore #6
    //   1017: goto -> 1035
    //   1020: aload #6
    //   1022: astore_1
    //   1023: ldc ''
    //   1025: astore #8
    //   1027: aload #9
    //   1029: astore #6
    //   1031: aload #8
    //   1033: astore #9
    //   1035: aload_1
    //   1036: astore #6
    //   1038: invokestatic elapsedRealtime : ()J
    //   1041: lstore #19
    //   1043: aload_1
    //   1044: astore #6
    //   1046: aload_0
    //   1047: invokestatic elapsedRealtime : ()J
    //   1050: putfield mHttpTimeReference : J
    //   1053: aload #14
    //   1055: ifnull -> 1066
    //   1058: aload_1
    //   1059: astore #6
    //   1061: aload #14
    //   1063: invokevirtual close : ()V
    //   1066: aload #13
    //   1068: ifnull -> 1079
    //   1071: aload_1
    //   1072: astore #6
    //   1074: aload #13
    //   1076: invokevirtual close : ()V
    //   1079: aload_1
    //   1080: astore #6
    //   1082: aload #4
    //   1084: invokevirtual disconnect : ()V
    //   1087: aload_1
    //   1088: astore #6
    //   1090: getstatic android/net/OplusHttpClient.DEBUG : Z
    //   1093: ifeq -> 1108
    //   1096: aload_1
    //   1097: astore #6
    //   1099: ldc 'OplusHttpClient'
    //   1101: ldc_w 'Start to parser http response data!'
    //   1104: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   1107: pop
    //   1108: aload_1
    //   1109: astore #6
    //   1111: invokestatic newInstance : ()Ljavax/xml/parsers/SAXParserFactory;
    //   1114: astore #8
    //   1116: aload_1
    //   1117: astore #6
    //   1119: aload #8
    //   1121: invokevirtual newSAXParser : ()Ljavax/xml/parsers/SAXParser;
    //   1124: astore #8
    //   1126: aload_1
    //   1127: astore #6
    //   1129: aload #8
    //   1131: invokevirtual getXMLReader : ()Lorg/xml/sax/XMLReader;
    //   1134: astore #13
    //   1136: aload_1
    //   1137: astore #6
    //   1139: new android/net/OplusHttpClient$DateTimeXmlParseHandler
    //   1142: astore #8
    //   1144: aload_1
    //   1145: astore #6
    //   1147: aload #8
    //   1149: aload_0
    //   1150: invokespecial <init> : (Landroid/net/OplusHttpClient;)V
    //   1153: aload_1
    //   1154: astore #6
    //   1156: aload #13
    //   1158: aload #8
    //   1160: invokeinterface setContentHandler : (Lorg/xml/sax/ContentHandler;)V
    //   1165: aload_1
    //   1166: astore #6
    //   1168: new org/xml/sax/InputSource
    //   1171: astore #14
    //   1173: aload_1
    //   1174: astore #6
    //   1176: new java/io/StringReader
    //   1179: astore #17
    //   1181: aload_1
    //   1182: astore #6
    //   1184: aload #17
    //   1186: aload #9
    //   1188: invokespecial <init> : (Ljava/lang/String;)V
    //   1191: aload_1
    //   1192: astore #6
    //   1194: aload #14
    //   1196: aload #17
    //   1198: invokespecial <init> : (Ljava/io/Reader;)V
    //   1201: aload_1
    //   1202: astore #6
    //   1204: aload #13
    //   1206: aload #14
    //   1208: invokeinterface parse : (Lorg/xml/sax/InputSource;)V
    //   1213: aload_1
    //   1214: astore #6
    //   1216: aload #8
    //   1218: invokevirtual getDate : ()Ljava/lang/String;
    //   1221: astore #13
    //   1223: aload_1
    //   1224: astore #6
    //   1226: aload #13
    //   1228: ldc_w '-'
    //   1231: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   1234: astore #9
    //   1236: aload_1
    //   1237: astore #6
    //   1239: iconst_3
    //   1240: newarray int
    //   1242: astore #14
    //   1244: iconst_0
    //   1245: istore_2
    //   1246: aload #9
    //   1248: astore #6
    //   1250: aload #4
    //   1252: astore #9
    //   1254: aload #6
    //   1256: astore #4
    //   1258: aload_1
    //   1259: astore #6
    //   1261: iload_2
    //   1262: aload #4
    //   1264: arraylength
    //   1265: if_icmpge -> 1292
    //   1268: aload_1
    //   1269: astore #6
    //   1271: aload #14
    //   1273: iload_2
    //   1274: aload #4
    //   1276: iload_2
    //   1277: aaload
    //   1278: invokestatic parseInt : (Ljava/lang/String;)I
    //   1281: iastore
    //   1282: iinc #2, 1
    //   1285: aload #4
    //   1287: astore #6
    //   1289: goto -> 1254
    //   1292: aload_1
    //   1293: astore #6
    //   1295: aload #8
    //   1297: invokevirtual getTime : ()Ljava/lang/String;
    //   1300: astore #9
    //   1302: aload_1
    //   1303: astore #6
    //   1305: aload #9
    //   1307: ldc_w ':'
    //   1310: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   1313: astore #9
    //   1315: aload_1
    //   1316: astore #6
    //   1318: iconst_3
    //   1319: newarray int
    //   1321: astore #13
    //   1323: iconst_0
    //   1324: istore_2
    //   1325: aload_1
    //   1326: astore #6
    //   1328: iload_2
    //   1329: aload #9
    //   1331: arraylength
    //   1332: if_icmpge -> 1355
    //   1335: aload_1
    //   1336: astore #6
    //   1338: aload #13
    //   1340: iload_2
    //   1341: aload #9
    //   1343: iload_2
    //   1344: aaload
    //   1345: invokestatic parseInt : (Ljava/lang/String;)I
    //   1348: iastore
    //   1349: iinc #2, 1
    //   1352: goto -> 1325
    //   1355: aload_1
    //   1356: astore #6
    //   1358: new android/text/format/Time
    //   1361: astore #8
    //   1363: aload_1
    //   1364: astore #6
    //   1366: aload #8
    //   1368: invokespecial <init> : ()V
    //   1371: aload_1
    //   1372: astore #6
    //   1374: new java/lang/StringBuilder
    //   1377: astore #4
    //   1379: aload_1
    //   1380: astore #6
    //   1382: aload #4
    //   1384: invokespecial <init> : ()V
    //   1387: aload_1
    //   1388: astore #6
    //   1390: aload #4
    //   1392: ldc_w 'Parser time success, hour= '
    //   1395: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1398: pop
    //   1399: aload_1
    //   1400: astore #6
    //   1402: aload #4
    //   1404: aload #13
    //   1406: iconst_0
    //   1407: iaload
    //   1408: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1411: pop
    //   1412: aload_1
    //   1413: astore #6
    //   1415: aload #4
    //   1417: ldc_w ' minute = '
    //   1420: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1423: pop
    //   1424: aload_1
    //   1425: astore #6
    //   1427: aload #4
    //   1429: aload #13
    //   1431: iconst_1
    //   1432: iaload
    //   1433: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1436: pop
    //   1437: aload_1
    //   1438: astore #6
    //   1440: aload #4
    //   1442: ldc_w 'seconds ='
    //   1445: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1448: pop
    //   1449: aload_1
    //   1450: astore #6
    //   1452: aload #4
    //   1454: aload #13
    //   1456: iconst_2
    //   1457: iaload
    //   1458: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1461: pop
    //   1462: aload_1
    //   1463: astore #6
    //   1465: ldc 'OplusHttpClient'
    //   1467: aload #4
    //   1469: invokevirtual toString : ()Ljava/lang/String;
    //   1472: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   1475: pop
    //   1476: aload_1
    //   1477: astore #6
    //   1479: aload #8
    //   1481: aload #13
    //   1483: iconst_2
    //   1484: iaload
    //   1485: aload #13
    //   1487: iconst_1
    //   1488: iaload
    //   1489: aload #13
    //   1491: iconst_0
    //   1492: iaload
    //   1493: aload #14
    //   1495: iconst_2
    //   1496: iaload
    //   1497: aload #14
    //   1499: iconst_1
    //   1500: iaload
    //   1501: iconst_1
    //   1502: isub
    //   1503: aload #14
    //   1505: iconst_0
    //   1506: iaload
    //   1507: invokevirtual set : (IIIIII)V
    //   1510: aload_1
    //   1511: astore #6
    //   1513: aload #8
    //   1515: iconst_1
    //   1516: invokevirtual toMillis : (Z)J
    //   1519: ldc2_w 28800000
    //   1522: lsub
    //   1523: lstore #21
    //   1525: aload_1
    //   1526: astore #6
    //   1528: invokestatic currentTimeMillis : ()J
    //   1531: lstore #23
    //   1533: aload_1
    //   1534: astore #6
    //   1536: invokestatic getDefault : ()Ljava/util/TimeZone;
    //   1539: invokevirtual getRawOffset : ()I
    //   1542: i2l
    //   1543: lload #21
    //   1545: ladd
    //   1546: lload #23
    //   1548: lload #15
    //   1550: lsub
    //   1551: ladd
    //   1552: ldc2_w 832
    //   1555: ladd
    //   1556: lstore #15
    //   1558: aload_1
    //   1559: astore #6
    //   1561: invokestatic getDefault : ()Ljava/util/TimeZone;
    //   1564: lload #15
    //   1566: invokevirtual getOffset : (J)I
    //   1569: istore_2
    //   1570: aload_1
    //   1571: astore #6
    //   1573: invokestatic getDefault : ()Ljava/util/TimeZone;
    //   1576: invokevirtual getRawOffset : ()I
    //   1579: istore_3
    //   1580: aload_1
    //   1581: astore #6
    //   1583: aload_0
    //   1584: iload_2
    //   1585: iload_3
    //   1586: isub
    //   1587: i2l
    //   1588: lload #15
    //   1590: ladd
    //   1591: putfield mHttpTime : J
    //   1594: aload_1
    //   1595: astore #6
    //   1597: aload_0
    //   1598: lload #19
    //   1600: lload #11
    //   1602: lsub
    //   1603: putfield mRoundTripTime : J
    //   1606: iconst_1
    //   1607: istore #25
    //   1609: iconst_1
    //   1610: istore #5
    //   1612: ldc android/net/OplusHttpClient
    //   1614: monitorenter
    //   1615: invokestatic elapsedRealtime : ()J
    //   1618: getstatic android/net/OplusHttpClient.mLastGotSuccessLocaltime : J
    //   1621: lsub
    //   1622: ldc2_w 1500
    //   1625: lcmp
    //   1626: ifle -> 1718
    //   1629: iload #25
    //   1631: istore #26
    //   1633: ldc_w 'persist.sys.lasttime'
    //   1636: lconst_0
    //   1637: invokestatic getLong : (Ljava/lang/String;J)J
    //   1640: lload #21
    //   1642: lcmp
    //   1643: iflt -> 1718
    //   1646: iload #25
    //   1648: istore #26
    //   1650: ldc 'OplusHttpClient'
    //   1652: ldc_w 'Cached by carrieroperator or others, Need Ntp algin time!'
    //   1655: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   1658: pop
    //   1659: iload #25
    //   1661: istore #26
    //   1663: new java/lang/StringBuilder
    //   1666: astore_1
    //   1667: iload #25
    //   1669: istore #26
    //   1671: aload_1
    //   1672: invokespecial <init> : ()V
    //   1675: iload #25
    //   1677: istore #26
    //   1679: aload_1
    //   1680: ldc_w 'mGMTTime:'
    //   1683: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1686: pop
    //   1687: iload #25
    //   1689: istore #26
    //   1691: aload_1
    //   1692: lload #21
    //   1694: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   1697: pop
    //   1698: iload #25
    //   1700: istore #26
    //   1702: ldc 'OplusHttpClient'
    //   1704: aload_1
    //   1705: invokevirtual toString : ()Ljava/lang/String;
    //   1708: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   1711: pop
    //   1712: iconst_0
    //   1713: istore #5
    //   1715: goto -> 1743
    //   1718: iload #25
    //   1720: istore #26
    //   1722: ldc_w 'persist.sys.lasttime'
    //   1725: lload #21
    //   1727: invokestatic toString : (J)Ljava/lang/String;
    //   1730: invokestatic set : (Ljava/lang/String;Ljava/lang/String;)V
    //   1733: iload #25
    //   1735: istore #26
    //   1737: invokestatic elapsedRealtime : ()J
    //   1740: putstatic android/net/OplusHttpClient.mLastGotSuccessLocaltime : J
    //   1743: iload #5
    //   1745: istore #26
    //   1747: ldc android/net/OplusHttpClient
    //   1749: monitorexit
    //   1750: goto -> 1831
    //   1753: astore_1
    //   1754: goto -> 1758
    //   1757: astore_1
    //   1758: ldc android/net/OplusHttpClient
    //   1760: monitorexit
    //   1761: aload_1
    //   1762: athrow
    //   1763: astore_1
    //   1764: goto -> 1792
    //   1767: astore_1
    //   1768: goto -> 1758
    //   1771: astore_1
    //   1772: goto -> 1792
    //   1775: astore_1
    //   1776: goto -> 1792
    //   1779: astore_1
    //   1780: goto -> 1792
    //   1783: astore_1
    //   1784: goto -> 1792
    //   1787: astore_1
    //   1788: goto -> 1792
    //   1791: astore_1
    //   1792: new java/lang/StringBuilder
    //   1795: dup
    //   1796: invokespecial <init> : ()V
    //   1799: astore #6
    //   1801: aload #6
    //   1803: ldc_w 'oppoServer exception: '
    //   1806: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1809: pop
    //   1810: aload #6
    //   1812: aload_1
    //   1813: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1816: pop
    //   1817: ldc 'OplusHttpClient'
    //   1819: aload #6
    //   1821: invokevirtual toString : ()Ljava/lang/String;
    //   1824: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1827: pop
    //   1828: iconst_0
    //   1829: istore #5
    //   1831: iload #5
    //   1833: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #98	-> 0
    //   #99	-> 0
    //   #100	-> 0
    //   #102	-> 8
    //   #103	-> 19
    //   #104	-> 23
    //   #290	-> 37
    //   #106	-> 41
    //   #107	-> 59
    //   #108	-> 69
    //   #110	-> 113
    //   #111	-> 131
    //   #112	-> 145
    //   #119	-> 184
    //   #113	-> 198
    //   #114	-> 202
    //   #116	-> 216
    //   #121	-> 227
    //   #122	-> 261
    //   #123	-> 268
    //   #124	-> 307
    //   #125	-> 319
    //   #126	-> 329
    //   #128	-> 366
    //   #135	-> 377
    //   #136	-> 377
    //   #137	-> 382
    //   #138	-> 387
    //   #139	-> 392
    //   #140	-> 400
    //   #141	-> 410
    //   #144	-> 463
    //   #145	-> 475
    //   #146	-> 483
    //   #147	-> 493
    //   #149	-> 504
    //   #150	-> 512
    //   #152	-> 543
    //   #154	-> 555
    //   #155	-> 562
    //   #157	-> 568
    //   #158	-> 574
    //   #160	-> 580
    //   #161	-> 591
    //   #162	-> 595
    //   #161	-> 602
    //   #164	-> 602
    //   #165	-> 634
    //   #166	-> 640
    //   #167	-> 646
    //   #168	-> 651
    //   #169	-> 661
    //   #290	-> 673
    //   #171	-> 677
    //   #172	-> 682
    //   #173	-> 692
    //   #183	-> 701
    //   #184	-> 704
    //   #185	-> 707
    //   #187	-> 707
    //   #188	-> 717
    //   #189	-> 720
    //   #190	-> 720
    //   #192	-> 725
    //   #193	-> 731
    //   #194	-> 771
    //   #196	-> 778
    //   #198	-> 787
    //   #199	-> 813
    //   #200	-> 833
    //   #201	-> 840
    //   #202	-> 855
    //   #204	-> 859
    //   #205	-> 870
    //   #208	-> 952
    //   #194	-> 1020
    //   #210	-> 1035
    //   #211	-> 1043
    //   #214	-> 1053
    //   #215	-> 1058
    //   #217	-> 1066
    //   #218	-> 1071
    //   #220	-> 1079
    //   #221	-> 1087
    //   #222	-> 1096
    //   #224	-> 1108
    //   #225	-> 1116
    //   #226	-> 1126
    //   #227	-> 1136
    //   #228	-> 1153
    //   #229	-> 1165
    //   #231	-> 1213
    //   #232	-> 1223
    //   #233	-> 1236
    //   #234	-> 1244
    //   #235	-> 1268
    //   #234	-> 1282
    //   #238	-> 1292
    //   #239	-> 1302
    //   #240	-> 1315
    //   #241	-> 1323
    //   #242	-> 1335
    //   #241	-> 1349
    //   #245	-> 1355
    //   #246	-> 1371
    //   #248	-> 1476
    //   #250	-> 1510
    //   #252	-> 1525
    //   #253	-> 1533
    //   #256	-> 1558
    //   #258	-> 1580
    //   #260	-> 1594
    //   #261	-> 1606
    //   #277	-> 1612
    //   #278	-> 1615
    //   #279	-> 1629
    //   #280	-> 1646
    //   #281	-> 1659
    //   #282	-> 1712
    //   #278	-> 1718
    //   #284	-> 1718
    //   #285	-> 1733
    //   #287	-> 1743
    //   #293	-> 1750
    //   #287	-> 1753
    //   #290	-> 1763
    //   #287	-> 1767
    //   #290	-> 1771
    //   #291	-> 1792
    //   #292	-> 1828
    //   #295	-> 1831
    // Exception table:
    //   from	to	target	type
    //   8	19	1791	java/lang/Exception
    //   23	34	37	java/lang/Exception
    //   41	54	1791	java/lang/Exception
    //   63	69	37	java/lang/Exception
    //   69	106	37	java/lang/Exception
    //   113	126	1791	java/lang/Exception
    //   131	142	37	java/lang/Exception
    //   145	158	1791	java/lang/Exception
    //   163	181	37	java/lang/Exception
    //   184	195	37	java/lang/Exception
    //   202	213	37	java/lang/Exception
    //   216	227	1791	java/lang/Exception
    //   227	261	1791	java/lang/Exception
    //   261	268	1791	java/lang/Exception
    //   268	307	1791	java/lang/Exception
    //   307	319	1791	java/lang/Exception
    //   319	324	1787	java/lang/Exception
    //   329	366	37	java/lang/Exception
    //   366	377	1787	java/lang/Exception
    //   377	382	1787	java/lang/Exception
    //   382	387	1787	java/lang/Exception
    //   387	392	1787	java/lang/Exception
    //   392	400	1787	java/lang/Exception
    //   400	405	1787	java/lang/Exception
    //   410	463	37	java/lang/Exception
    //   463	470	1787	java/lang/Exception
    //   475	483	37	java/lang/Exception
    //   483	493	37	java/lang/Exception
    //   493	501	37	java/lang/Exception
    //   504	512	1787	java/lang/Exception
    //   512	543	1787	java/lang/Exception
    //   543	555	1787	java/lang/Exception
    //   555	562	1787	java/lang/Exception
    //   562	568	1787	java/lang/Exception
    //   568	574	1787	java/lang/Exception
    //   574	580	1787	java/lang/Exception
    //   580	591	1787	java/lang/Exception
    //   602	634	1783	java/lang/Exception
    //   634	640	1783	java/lang/Exception
    //   640	646	1783	java/lang/Exception
    //   646	651	1783	java/lang/Exception
    //   651	656	1783	java/lang/Exception
    //   661	670	673	java/lang/Exception
    //   677	682	1783	java/lang/Exception
    //   682	687	1783	java/lang/Exception
    //   692	701	673	java/lang/Exception
    //   707	717	1783	java/lang/Exception
    //   720	725	1783	java/lang/Exception
    //   725	731	1783	java/lang/Exception
    //   731	747	1783	java/lang/Exception
    //   751	757	1779	java/lang/Exception
    //   761	771	1779	java/lang/Exception
    //   782	787	1779	java/lang/Exception
    //   791	796	1779	java/lang/Exception
    //   800	813	1779	java/lang/Exception
    //   817	822	1779	java/lang/Exception
    //   826	833	1779	java/lang/Exception
    //   843	850	1779	java/lang/Exception
    //   862	870	1779	java/lang/Exception
    //   873	878	1775	java/lang/Exception
    //   881	886	1775	java/lang/Exception
    //   889	898	1775	java/lang/Exception
    //   901	909	1775	java/lang/Exception
    //   912	921	1775	java/lang/Exception
    //   924	935	1775	java/lang/Exception
    //   938	949	1775	java/lang/Exception
    //   959	964	1775	java/lang/Exception
    //   967	972	1775	java/lang/Exception
    //   975	984	1775	java/lang/Exception
    //   987	995	1775	java/lang/Exception
    //   998	1009	1775	java/lang/Exception
    //   1038	1043	1775	java/lang/Exception
    //   1046	1053	1775	java/lang/Exception
    //   1061	1066	1775	java/lang/Exception
    //   1074	1079	1775	java/lang/Exception
    //   1082	1087	1775	java/lang/Exception
    //   1090	1096	1775	java/lang/Exception
    //   1099	1108	1775	java/lang/Exception
    //   1111	1116	1775	java/lang/Exception
    //   1119	1126	1775	java/lang/Exception
    //   1129	1136	1775	java/lang/Exception
    //   1139	1144	1775	java/lang/Exception
    //   1147	1153	1775	java/lang/Exception
    //   1156	1165	1775	java/lang/Exception
    //   1168	1173	1775	java/lang/Exception
    //   1176	1181	1775	java/lang/Exception
    //   1184	1191	1775	java/lang/Exception
    //   1194	1201	1775	java/lang/Exception
    //   1204	1213	1775	java/lang/Exception
    //   1216	1223	1775	java/lang/Exception
    //   1226	1236	1775	java/lang/Exception
    //   1239	1244	1775	java/lang/Exception
    //   1261	1268	1775	java/lang/Exception
    //   1271	1282	1775	java/lang/Exception
    //   1295	1302	1775	java/lang/Exception
    //   1305	1315	1775	java/lang/Exception
    //   1318	1323	1775	java/lang/Exception
    //   1328	1335	1775	java/lang/Exception
    //   1338	1349	1775	java/lang/Exception
    //   1358	1363	1775	java/lang/Exception
    //   1366	1371	1775	java/lang/Exception
    //   1374	1379	1775	java/lang/Exception
    //   1382	1387	1775	java/lang/Exception
    //   1390	1399	1775	java/lang/Exception
    //   1402	1412	1775	java/lang/Exception
    //   1415	1424	1775	java/lang/Exception
    //   1427	1437	1775	java/lang/Exception
    //   1440	1449	1775	java/lang/Exception
    //   1452	1462	1775	java/lang/Exception
    //   1465	1476	1775	java/lang/Exception
    //   1479	1510	1775	java/lang/Exception
    //   1513	1525	1775	java/lang/Exception
    //   1528	1533	1775	java/lang/Exception
    //   1536	1558	1775	java/lang/Exception
    //   1561	1570	1775	java/lang/Exception
    //   1573	1580	1775	java/lang/Exception
    //   1583	1594	1775	java/lang/Exception
    //   1597	1606	1775	java/lang/Exception
    //   1612	1615	1771	java/lang/Exception
    //   1615	1629	1757	finally
    //   1633	1646	1753	finally
    //   1650	1659	1753	finally
    //   1663	1667	1753	finally
    //   1671	1675	1753	finally
    //   1679	1687	1753	finally
    //   1691	1698	1753	finally
    //   1702	1712	1753	finally
    //   1722	1733	1753	finally
    //   1737	1743	1753	finally
    //   1747	1750	1753	finally
    //   1758	1761	1767	finally
    //   1761	1763	1763	java/lang/Exception
  }
  
  private boolean getNetType(Context paramContext) {
    ConnectivityManager connectivityManager = (ConnectivityManager)paramContext.getSystemService("connectivity");
    if (connectivityManager == null)
      return false; 
    NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
    if (networkInfo == null)
      return false; 
    String str = networkInfo.getTypeName();
    if (str.equalsIgnoreCase("WIFI"))
      return true; 
    if (str.equalsIgnoreCase("MOBILE") || str.equalsIgnoreCase("GPRS")) {
      String str1 = networkInfo.getExtraInfo();
      if (str1 != null && str1.equalsIgnoreCase("cmwap"))
        return false; 
      return true;
    } 
    return true;
  }
  
  public class DateTimeXmlParseHandler extends DefaultHandler {
    private String mDateString;
    
    private boolean mIsDateFlag;
    
    private boolean mIsTimeFlag;
    
    private boolean mIsTimeZoneFlag;
    
    private String mTimeString;
    
    private String mTimeZoneString;
    
    final OplusHttpClient this$0;
    
    public DateTimeXmlParseHandler() {
      this.mIsTimeZoneFlag = false;
      this.mIsDateFlag = false;
      this.mIsTimeFlag = false;
      this.mTimeZoneString = "";
      this.mDateString = "";
      this.mTimeString = "";
    }
    
    public void characters(char[] param1ArrayOfchar, int param1Int1, int param1Int2) throws SAXException {
      super.characters(param1ArrayOfchar, param1Int1, param1Int2);
      if (this.mIsTimeZoneFlag) {
        this.mTimeZoneString = new String(param1ArrayOfchar, param1Int1, param1Int2);
      } else if (this.mIsDateFlag) {
        this.mDateString = new String(param1ArrayOfchar, param1Int1, param1Int2);
      } else if (this.mIsTimeFlag) {
        this.mTimeString = new String(param1ArrayOfchar, param1Int1, param1Int2);
      } 
    }
    
    public void endDocument() throws SAXException {
      super.endDocument();
    }
    
    public void endElement(String param1String1, String param1String2, String param1String3) throws SAXException {
      super.endElement(param1String1, param1String2, param1String3);
      if (param1String2.equals("TimeZone")) {
        this.mIsTimeZoneFlag = false;
      } else if (param1String2.equals("Date")) {
        this.mIsDateFlag = false;
      } else if (param1String2.equals("Time")) {
        this.mIsTimeFlag = false;
      } 
    }
    
    public void startDocument() throws SAXException {
      super.startDocument();
    }
    
    public void startElement(String param1String1, String param1String2, String param1String3, Attributes param1Attributes) throws SAXException {
      super.startElement(param1String1, param1String2, param1String3, param1Attributes);
      if (param1String2.equals("TimeZone")) {
        this.mIsTimeZoneFlag = true;
      } else if (param1String2.equals("Date")) {
        this.mIsDateFlag = true;
      } else if (param1String2.equals("Time")) {
        this.mIsTimeFlag = true;
      } 
    }
    
    public String getTimeZone() {
      return this.mTimeZoneString;
    }
    
    public String getDate() {
      return this.mDateString;
    }
    
    public String getTime() {
      return this.mTimeString;
    }
  }
  
  public long getHttpTime() {
    return this.mHttpTime;
  }
  
  public long getHttpTimeReference() {
    return this.mHttpTimeReference;
  }
  
  public long getRoundTripTime() {
    return this.mRoundTripTime;
  }
}
