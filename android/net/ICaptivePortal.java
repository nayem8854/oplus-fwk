package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ICaptivePortal extends IInterface {
  void appRequest(int paramInt) throws RemoteException;
  
  void appResponse(int paramInt) throws RemoteException;
  
  void logEvent(int paramInt, String paramString) throws RemoteException;
  
  class Default implements ICaptivePortal {
    public void appRequest(int param1Int) throws RemoteException {}
    
    public void appResponse(int param1Int) throws RemoteException {}
    
    public void logEvent(int param1Int, String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICaptivePortal {
    private static final String DESCRIPTOR = "android.net.ICaptivePortal";
    
    static final int TRANSACTION_appRequest = 1;
    
    static final int TRANSACTION_appResponse = 2;
    
    static final int TRANSACTION_logEvent = 3;
    
    public Stub() {
      attachInterface(this, "android.net.ICaptivePortal");
    }
    
    public static ICaptivePortal asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.ICaptivePortal");
      if (iInterface != null && iInterface instanceof ICaptivePortal)
        return (ICaptivePortal)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "logEvent";
        } 
        return "appResponse";
      } 
      return "appRequest";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.net.ICaptivePortal");
            return true;
          } 
          param1Parcel1.enforceInterface("android.net.ICaptivePortal");
          param1Int1 = param1Parcel1.readInt();
          str = param1Parcel1.readString();
          logEvent(param1Int1, str);
          return true;
        } 
        str.enforceInterface("android.net.ICaptivePortal");
        param1Int1 = str.readInt();
        appResponse(param1Int1);
        return true;
      } 
      str.enforceInterface("android.net.ICaptivePortal");
      param1Int1 = str.readInt();
      appRequest(param1Int1);
      return true;
    }
    
    private static class Proxy implements ICaptivePortal {
      public static ICaptivePortal sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.ICaptivePortal";
      }
      
      public void appRequest(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.ICaptivePortal");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ICaptivePortal.Stub.getDefaultImpl() != null) {
            ICaptivePortal.Stub.getDefaultImpl().appRequest(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void appResponse(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.ICaptivePortal");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ICaptivePortal.Stub.getDefaultImpl() != null) {
            ICaptivePortal.Stub.getDefaultImpl().appResponse(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void logEvent(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.ICaptivePortal");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && ICaptivePortal.Stub.getDefaultImpl() != null) {
            ICaptivePortal.Stub.getDefaultImpl().logEvent(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICaptivePortal param1ICaptivePortal) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICaptivePortal != null) {
          Proxy.sDefaultImpl = param1ICaptivePortal;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICaptivePortal getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
