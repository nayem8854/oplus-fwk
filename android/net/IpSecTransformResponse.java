package android.net;

import android.os.Parcel;
import android.os.Parcelable;

public final class IpSecTransformResponse implements Parcelable {
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.status);
    paramParcel.writeInt(this.resourceId);
  }
  
  public IpSecTransformResponse(int paramInt) {
    if (paramInt != 0) {
      this.status = paramInt;
      this.resourceId = -1;
      return;
    } 
    throw new IllegalArgumentException("Valid status implies other args must be provided");
  }
  
  public IpSecTransformResponse(int paramInt1, int paramInt2) {
    this.status = paramInt1;
    this.resourceId = paramInt2;
  }
  
  private IpSecTransformResponse(Parcel paramParcel) {
    this.status = paramParcel.readInt();
    this.resourceId = paramParcel.readInt();
  }
  
  public static final Parcelable.Creator<IpSecTransformResponse> CREATOR = new Parcelable.Creator<IpSecTransformResponse>() {
      public IpSecTransformResponse createFromParcel(Parcel param1Parcel) {
        return new IpSecTransformResponse(param1Parcel);
      }
      
      public IpSecTransformResponse[] newArray(int param1Int) {
        return new IpSecTransformResponse[param1Int];
      }
    };
  
  private static final String TAG = "IpSecTransformResponse";
  
  public final int resourceId;
  
  public final int status;
}
