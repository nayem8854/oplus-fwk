package android.net;

import android.os.HandlerThread;
import android.os.Looper;

public final class ConnectivityThread extends HandlerThread {
  class Singleton {
    private static final ConnectivityThread INSTANCE = ConnectivityThread.createInstance();
  }
  
  private ConnectivityThread() {
    super("ConnectivityThread");
  }
  
  private static ConnectivityThread createInstance() {
    ConnectivityThread connectivityThread = new ConnectivityThread();
    connectivityThread.start();
    return connectivityThread;
  }
  
  public static ConnectivityThread get() {
    return Singleton.INSTANCE;
  }
  
  public static Looper getInstanceLooper() {
    return Singleton.INSTANCE.getLooper();
  }
}
