package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAutoConCaptivePortalControlCallbacks extends IInterface {
  void notifyNetworkTested(int paramInt) throws RemoteException;
  
  class Default implements IAutoConCaptivePortalControlCallbacks {
    public void notifyNetworkTested(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAutoConCaptivePortalControlCallbacks {
    private static final String DESCRIPTOR = "android.net.IAutoConCaptivePortalControlCallbacks";
    
    static final int TRANSACTION_notifyNetworkTested = 1;
    
    public Stub() {
      attachInterface(this, "android.net.IAutoConCaptivePortalControlCallbacks");
    }
    
    public static IAutoConCaptivePortalControlCallbacks asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.IAutoConCaptivePortalControlCallbacks");
      if (iInterface != null && iInterface instanceof IAutoConCaptivePortalControlCallbacks)
        return (IAutoConCaptivePortalControlCallbacks)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "notifyNetworkTested";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.net.IAutoConCaptivePortalControlCallbacks");
        return true;
      } 
      param1Parcel1.enforceInterface("android.net.IAutoConCaptivePortalControlCallbacks");
      param1Int1 = param1Parcel1.readInt();
      notifyNetworkTested(param1Int1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IAutoConCaptivePortalControlCallbacks {
      public static IAutoConCaptivePortalControlCallbacks sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.IAutoConCaptivePortalControlCallbacks";
      }
      
      public void notifyNetworkTested(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IAutoConCaptivePortalControlCallbacks");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IAutoConCaptivePortalControlCallbacks.Stub.getDefaultImpl() != null) {
            IAutoConCaptivePortalControlCallbacks.Stub.getDefaultImpl().notifyNetworkTested(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAutoConCaptivePortalControlCallbacks param1IAutoConCaptivePortalControlCallbacks) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAutoConCaptivePortalControlCallbacks != null) {
          Proxy.sDefaultImpl = param1IAutoConCaptivePortalControlCallbacks;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAutoConCaptivePortalControlCallbacks getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
