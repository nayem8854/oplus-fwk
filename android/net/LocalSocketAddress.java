package android.net;

public class LocalSocketAddress {
  private final String name;
  
  private final Namespace namespace;
  
  public enum Namespace {
    ABSTRACT(0),
    FILESYSTEM(0),
    RESERVED(1);
    
    private static final Namespace[] $VALUES;
    
    private int id;
    
    static {
      Namespace namespace = new Namespace("FILESYSTEM", 2, 2);
      $VALUES = new Namespace[] { ABSTRACT, RESERVED, namespace };
    }
    
    Namespace(int param1Int1) {
      this.id = param1Int1;
    }
    
    int getId() {
      return this.id;
    }
  }
  
  public LocalSocketAddress(String paramString, Namespace paramNamespace) {
    this.name = paramString;
    this.namespace = paramNamespace;
  }
  
  public LocalSocketAddress(String paramString) {
    this(paramString, Namespace.ABSTRACT);
  }
  
  public String getName() {
    return this.name;
  }
  
  public Namespace getNamespace() {
    return this.namespace;
  }
}
