package android.net;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.system.OsConstants;
import android.util.Pair;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.UnknownHostException;
import java.util.Objects;

public class LinkAddress implements Parcelable {
  private static int scopeForUnicastAddress(InetAddress paramInetAddress) {
    if (paramInetAddress.isAnyLocalAddress())
      return OsConstants.RT_SCOPE_HOST; 
    if (paramInetAddress.isLoopbackAddress() || paramInetAddress.isLinkLocalAddress())
      return OsConstants.RT_SCOPE_LINK; 
    if (!(paramInetAddress instanceof java.net.Inet4Address) && paramInetAddress.isSiteLocalAddress())
      return OsConstants.RT_SCOPE_SITE; 
    return OsConstants.RT_SCOPE_UNIVERSE;
  }
  
  private boolean isIpv6ULA() {
    boolean bool = isIpv6();
    boolean bool1 = false;
    if (bool) {
      byte[] arrayOfByte = this.address.getAddress();
      if ((arrayOfByte[0] & 0xFFFFFFFE) == -4)
        bool1 = true; 
      return bool1;
    } 
    return false;
  }
  
  @SystemApi
  public boolean isIpv6() {
    return this.address instanceof java.net.Inet6Address;
  }
  
  public boolean isIPv6() {
    return isIpv6();
  }
  
  @SystemApi
  public boolean isIpv4() {
    return this.address instanceof java.net.Inet4Address;
  }
  
  private void init(InetAddress paramInetAddress, int paramInt1, int paramInt2, int paramInt3, long paramLong1, long paramLong2) {
    StringBuilder stringBuilder1;
    if (paramInetAddress != null && 
      !paramInetAddress.isMulticastAddress() && paramInt1 >= 0 && (!(paramInetAddress instanceof java.net.Inet4Address) || paramInt1 <= 32) && paramInt1 <= 128) {
      boolean bool2, bool1 = true;
      if (paramLong1 == -1L) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (paramLong2 != -1L)
        bool1 = false; 
      if (bool2 == bool1) {
        if (paramLong1 == -1L || paramLong1 >= 0L) {
          if (paramLong2 == -1L || paramLong2 >= 0L) {
            if (paramLong1 == -1L || paramLong2 == -1L || paramLong2 >= paramLong1) {
              this.address = paramInetAddress;
              this.prefixLength = paramInt1;
              this.flags = paramInt2;
              this.scope = paramInt3;
              this.deprecationTime = paramLong1;
              this.expirationTime = paramLong2;
              return;
            } 
            StringBuilder stringBuilder3 = new StringBuilder();
            stringBuilder3.append("expiration earlier than deprecation (");
            stringBuilder3.append(paramLong1);
            stringBuilder3.append(", ");
            stringBuilder3.append(paramLong2);
            stringBuilder3.append(")");
            throw new IllegalArgumentException(stringBuilder3.toString());
          } 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("invalid expiration time ");
          stringBuilder.append(paramLong2);
          throw new IllegalArgumentException(stringBuilder.toString());
        } 
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("invalid deprecation time ");
        stringBuilder1.append(paramLong1);
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      throw new IllegalArgumentException("Must not specify only one of deprecation time and expiration time");
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("Bad LinkAddress params ");
    stringBuilder2.append(stringBuilder1);
    stringBuilder2.append("/");
    stringBuilder2.append(paramInt1);
    throw new IllegalArgumentException(stringBuilder2.toString());
  }
  
  @SystemApi
  public LinkAddress(InetAddress paramInetAddress, int paramInt1, int paramInt2, int paramInt3) {
    init(paramInetAddress, paramInt1, paramInt2, paramInt3, -1L, -1L);
  }
  
  @SystemApi
  public LinkAddress(InetAddress paramInetAddress, int paramInt1, int paramInt2, int paramInt3, long paramLong1, long paramLong2) {
    init(paramInetAddress, paramInt1, paramInt2, paramInt3, paramLong1, paramLong2);
  }
  
  @SystemApi
  public LinkAddress(InetAddress paramInetAddress, int paramInt) {
    this(paramInetAddress, paramInt, 0, 0);
    this.scope = scopeForUnicastAddress(paramInetAddress);
  }
  
  public LinkAddress(InterfaceAddress paramInterfaceAddress) {
    this(inetAddress, s);
  }
  
  @SystemApi
  public LinkAddress(String paramString) {
    this(paramString, 0, 0);
    this.scope = scopeForUnicastAddress(this.address);
  }
  
  @SystemApi
  public LinkAddress(String paramString, int paramInt1, int paramInt2) {
    Pair<InetAddress, Integer> pair = NetworkUtils.parseIpAndMask(paramString);
    init((InetAddress)pair.first, ((Integer)pair.second).intValue(), paramInt1, paramInt2, -1L, -1L);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.address.getHostAddress());
    stringBuilder.append("/");
    stringBuilder.append(this.prefixLength);
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof LinkAddress;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    bool = bool1;
    if (this.address.equals(((LinkAddress)paramObject).address)) {
      bool = bool1;
      if (this.prefixLength == ((LinkAddress)paramObject).prefixLength) {
        bool = bool1;
        if (this.flags == ((LinkAddress)paramObject).flags) {
          bool = bool1;
          if (this.scope == ((LinkAddress)paramObject).scope) {
            bool = bool1;
            if (this.deprecationTime == ((LinkAddress)paramObject).deprecationTime) {
              bool = bool1;
              if (this.expirationTime == ((LinkAddress)paramObject).expirationTime)
                bool = true; 
            } 
          } 
        } 
      } 
    } 
    return bool;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.address, Integer.valueOf(this.prefixLength), Integer.valueOf(this.flags), Integer.valueOf(this.scope), Long.valueOf(this.deprecationTime), Long.valueOf(this.expirationTime) });
  }
  
  @SystemApi
  public boolean isSameAddressAs(LinkAddress paramLinkAddress) {
    boolean bool1 = false;
    if (paramLinkAddress == null)
      return false; 
    boolean bool2 = bool1;
    if (this.address.equals(paramLinkAddress.address)) {
      bool2 = bool1;
      if (this.prefixLength == paramLinkAddress.prefixLength)
        bool2 = true; 
    } 
    return bool2;
  }
  
  public InetAddress getAddress() {
    return this.address;
  }
  
  public int getPrefixLength() {
    return this.prefixLength;
  }
  
  public int getNetworkPrefixLength() {
    return getPrefixLength();
  }
  
  public int getFlags() {
    int i = this.flags;
    int j = i;
    if (this.deprecationTime != -1L)
      if (SystemClock.elapsedRealtime() >= this.deprecationTime) {
        j = i | OsConstants.IFA_F_DEPRECATED;
      } else {
        j = i & (OsConstants.IFA_F_DEPRECATED ^ 0xFFFFFFFF);
      }  
    long l = this.expirationTime;
    if (l == Long.MAX_VALUE) {
      i = j | OsConstants.IFA_F_PERMANENT;
    } else {
      i = j;
      if (l != -1L)
        i = j & (OsConstants.IFA_F_PERMANENT ^ 0xFFFFFFFF); 
    } 
    return i;
  }
  
  public int getScope() {
    return this.scope;
  }
  
  @SystemApi
  public long getDeprecationTime() {
    return this.deprecationTime;
  }
  
  @SystemApi
  public long getExpirationTime() {
    return this.expirationTime;
  }
  
  @SystemApi
  public boolean isGlobalPreferred() {
    boolean bool;
    int i = getFlags();
    if (this.scope == OsConstants.RT_SCOPE_UNIVERSE && 
      !isIpv6ULA() && ((OsConstants.IFA_F_DADFAILED | OsConstants.IFA_F_DEPRECATED) & i) == 0L && ((OsConstants.IFA_F_TENTATIVE & i) == 0L || (OsConstants.IFA_F_OPTIMISTIC & i) != 0L)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByteArray(this.address.getAddress());
    paramParcel.writeInt(this.prefixLength);
    paramParcel.writeInt(this.flags);
    paramParcel.writeInt(this.scope);
    paramParcel.writeLong(this.deprecationTime);
    paramParcel.writeLong(this.expirationTime);
  }
  
  public static final Parcelable.Creator<LinkAddress> CREATOR = new Parcelable.Creator<LinkAddress>() {
      public LinkAddress createFromParcel(Parcel param1Parcel) {
        InetAddress inetAddress = null;
        try {
          InetAddress inetAddress1 = InetAddress.getByAddress(param1Parcel.createByteArray());
        } catch (UnknownHostException unknownHostException) {}
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        int k = param1Parcel.readInt();
        long l1 = param1Parcel.readLong();
        long l2 = param1Parcel.readLong();
        return new LinkAddress(inetAddress, i, j, k, l1, l2);
      }
      
      public LinkAddress[] newArray(int param1Int) {
        return new LinkAddress[param1Int];
      }
    };
  
  @SystemApi
  public static final long LIFETIME_PERMANENT = 9223372036854775807L;
  
  @SystemApi
  public static final long LIFETIME_UNKNOWN = -1L;
  
  private InetAddress address;
  
  private long deprecationTime;
  
  private long expirationTime;
  
  private int flags;
  
  private int prefixLength;
  
  private int scope;
}
