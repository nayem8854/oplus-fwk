package android.net;

import android.annotation.SystemApi;
import android.content.Context;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.ServiceSpecificException;
import android.util.Log;
import com.android.internal.util.Preconditions;
import dalvik.system.CloseGuard;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.InetAddress;

public final class IpSecTransform implements AutoCloseable {
  public static final int ENCAP_ESPINUDP = 2;
  
  public static final int ENCAP_ESPINUDP_NON_IKE = 1;
  
  public static final int ENCAP_NONE = 0;
  
  public static final int MODE_TRANSPORT = 0;
  
  public static final int MODE_TUNNEL = 1;
  
  private static final String TAG = "IpSecTransform";
  
  private Handler mCallbackHandler;
  
  private final CloseGuard mCloseGuard;
  
  private final IpSecConfig mConfig;
  
  private final Context mContext;
  
  private ConnectivityManager.PacketKeepalive mKeepalive;
  
  private final ConnectivityManager.PacketKeepaliveCallback mKeepaliveCallback;
  
  private int mResourceId;
  
  private NattKeepaliveCallback mUserKeepaliveCallback;
  
  public IpSecTransform(Context paramContext, IpSecConfig paramIpSecConfig) {
    this.mCloseGuard = CloseGuard.get();
    this.mKeepaliveCallback = (ConnectivityManager.PacketKeepaliveCallback)new Object(this);
    this.mContext = paramContext;
    this.mConfig = new IpSecConfig(paramIpSecConfig);
    this.mResourceId = -1;
  }
  
  private IIpSecService getIpSecService() {
    IBinder iBinder = ServiceManager.getService("ipsec");
    if (iBinder != null)
      return IIpSecService.Stub.asInterface(iBinder); 
    RemoteException remoteException = new RemoteException("Failed to connect to IpSecService");
    throw remoteException.rethrowAsRuntimeException();
  }
  
  private void checkResultStatus(int paramInt) throws IOException, IpSecManager.ResourceUnavailableException, IpSecManager.SpiUnavailableException {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt == 2)
          Log.wtf("IpSecTransform", "Attempting to use an SPI that was somehow not reserved"); 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to Create a Transform with status code ");
        stringBuilder.append(paramInt);
        throw new IllegalStateException(stringBuilder.toString());
      } 
      throw new IpSecManager.ResourceUnavailableException("Failed to allocate a new IpSecTransform");
    } 
  }
  
  private IpSecTransform activate() throws IOException, IpSecManager.ResourceUnavailableException, IpSecManager.SpiUnavailableException {
    Exception exception;
    /* monitor enter ThisExpression{ObjectType{android/net/IpSecTransform}} */
    try {
      IIpSecService iIpSecService = getIpSecService();
      IpSecConfig ipSecConfig = this.mConfig;
      Binder binder = new Binder();
      this();
      Context context = this.mContext;
      String str = context.getOpPackageName();
      IpSecTransformResponse ipSecTransformResponse = iIpSecService.createTransform(ipSecConfig, binder, str);
      int i = ipSecTransformResponse.status;
      checkResultStatus(i);
      this.mResourceId = ipSecTransformResponse.resourceId;
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Added Transform with Id ");
      stringBuilder.append(this.mResourceId);
      Log.d("IpSecTransform", stringBuilder.toString());
      this.mCloseGuard.open("build");
      /* monitor exit ThisExpression{ObjectType{android/net/IpSecTransform}} */
      return this;
    } catch (ServiceSpecificException null) {
      throw IpSecManager.rethrowUncheckedExceptionFromServiceSpecificException(exception);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } finally {}
    /* monitor exit ThisExpression{ObjectType{android/net/IpSecTransform}} */
    throw exception;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof IpSecTransform))
      return false; 
    paramObject = paramObject;
    if (!getConfig().equals(paramObject.getConfig()) || this.mResourceId != ((IpSecTransform)paramObject).mResourceId)
      bool = false; 
    return bool;
  }
  
  public void close() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Removing Transform with Id ");
    stringBuilder.append(this.mResourceId);
    Log.d("IpSecTransform", stringBuilder.toString());
    if (this.mResourceId == -1) {
      this.mCloseGuard.close();
      return;
    } 
    try {
      IIpSecService iIpSecService = getIpSecService();
      iIpSecService.deleteTransform(this.mResourceId);
      stopNattKeepalive();
      this.mResourceId = -1;
      this.mCloseGuard.close();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } catch (Exception exception) {
      StringBuilder stringBuilder1 = new StringBuilder();
      this();
      stringBuilder1.append("Failed to close ");
      stringBuilder1.append(this);
      stringBuilder1.append(", Exception=");
      stringBuilder1.append(exception);
      Log.e("IpSecTransform", stringBuilder1.toString());
      this.mResourceId = -1;
      this.mCloseGuard.close();
      return;
    } finally {}
    this.mResourceId = -1;
    this.mCloseGuard.close();
    throw stringBuilder;
  }
  
  protected void finalize() throws Throwable {
    CloseGuard closeGuard = this.mCloseGuard;
    if (closeGuard != null)
      closeGuard.warnIfOpen(); 
    close();
  }
  
  IpSecConfig getConfig() {
    return this.mConfig;
  }
  
  public int getResourceId() {
    return this.mResourceId;
  }
  
  public static class NattKeepaliveCallback {
    public static final int ERROR_HARDWARE_ERROR = 3;
    
    public static final int ERROR_HARDWARE_UNSUPPORTED = 2;
    
    public static final int ERROR_INVALID_NETWORK = 1;
    
    public void onStarted() {}
    
    public void onStopped() {}
    
    public void onError(int param1Int) {}
  }
  
  public void startNattKeepalive(NattKeepaliveCallback paramNattKeepaliveCallback, int paramInt, Handler paramHandler) throws IOException {
    Preconditions.checkNotNull(paramNattKeepaliveCallback);
    if (paramInt >= 20 && paramInt <= 3600) {
      Preconditions.checkNotNull(paramHandler);
      if (this.mResourceId != -1)
        synchronized (this.mKeepaliveCallback) {
          if (this.mKeepaliveCallback == null) {
            this.mUserKeepaliveCallback = paramNattKeepaliveCallback;
            ConnectivityManager connectivityManager = (ConnectivityManager)this.mContext.getSystemService("connectivity");
            IpSecConfig ipSecConfig1 = this.mConfig;
            Network network = ipSecConfig1.getNetwork();
            ConnectivityManager.PacketKeepaliveCallback packetKeepaliveCallback = this.mKeepaliveCallback;
            IpSecConfig ipSecConfig2 = this.mConfig;
            InetAddress inetAddress1 = NetworkUtils.numericToInetAddress(ipSecConfig2.getSourceAddress());
            IpSecConfig ipSecConfig3 = this.mConfig;
            InetAddress inetAddress2 = NetworkUtils.numericToInetAddress(ipSecConfig3.getDestinationAddress());
            this.mKeepalive = connectivityManager.startNattKeepalive(network, paramInt, packetKeepaliveCallback, inetAddress1, 4500, inetAddress2);
            this.mCallbackHandler = paramHandler;
            return;
          } 
          IllegalStateException illegalStateException = new IllegalStateException();
          this("Keepalive already active");
          throw illegalStateException;
        }  
      throw new IllegalStateException("Packet keepalive cannot be started for an inactive transform");
    } 
    throw new IllegalArgumentException("Invalid NAT-T keepalive interval");
  }
  
  public void stopNattKeepalive() {
    synchronized (this.mKeepaliveCallback) {
      if (this.mKeepalive == null) {
        Log.e("IpSecTransform", "No active keepalive to stop");
        return;
      } 
      this.mKeepalive.stop();
      return;
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface EncapType {}
  
  public static class Builder {
    private IpSecConfig mConfig;
    
    private Context mContext;
    
    public Builder setEncryption(IpSecAlgorithm param1IpSecAlgorithm) {
      Preconditions.checkNotNull(param1IpSecAlgorithm);
      this.mConfig.setEncryption(param1IpSecAlgorithm);
      return this;
    }
    
    public Builder setAuthentication(IpSecAlgorithm param1IpSecAlgorithm) {
      Preconditions.checkNotNull(param1IpSecAlgorithm);
      this.mConfig.setAuthentication(param1IpSecAlgorithm);
      return this;
    }
    
    public Builder setAuthenticatedEncryption(IpSecAlgorithm param1IpSecAlgorithm) {
      Preconditions.checkNotNull(param1IpSecAlgorithm);
      this.mConfig.setAuthenticatedEncryption(param1IpSecAlgorithm);
      return this;
    }
    
    public Builder setIpv4Encapsulation(IpSecManager.UdpEncapsulationSocket param1UdpEncapsulationSocket, int param1Int) {
      Preconditions.checkNotNull(param1UdpEncapsulationSocket);
      this.mConfig.setEncapType(2);
      if (param1UdpEncapsulationSocket.getResourceId() != -1) {
        this.mConfig.setEncapSocketResourceId(param1UdpEncapsulationSocket.getResourceId());
        this.mConfig.setEncapRemotePort(param1Int);
        return this;
      } 
      throw new IllegalArgumentException("Invalid UdpEncapsulationSocket");
    }
    
    public IpSecTransform buildTransportModeTransform(InetAddress param1InetAddress, IpSecManager.SecurityParameterIndex param1SecurityParameterIndex) throws IpSecManager.ResourceUnavailableException, IpSecManager.SpiUnavailableException, IOException {
      Preconditions.checkNotNull(param1InetAddress);
      Preconditions.checkNotNull(param1SecurityParameterIndex);
      if (param1SecurityParameterIndex.getResourceId() != -1) {
        this.mConfig.setMode(0);
        this.mConfig.setSourceAddress(param1InetAddress.getHostAddress());
        this.mConfig.setSpiResourceId(param1SecurityParameterIndex.getResourceId());
        return (new IpSecTransform(this.mContext, this.mConfig)).activate();
      } 
      throw new IllegalArgumentException("Invalid SecurityParameterIndex");
    }
    
    @SystemApi
    public IpSecTransform buildTunnelModeTransform(InetAddress param1InetAddress, IpSecManager.SecurityParameterIndex param1SecurityParameterIndex) throws IpSecManager.ResourceUnavailableException, IpSecManager.SpiUnavailableException, IOException {
      Preconditions.checkNotNull(param1InetAddress);
      Preconditions.checkNotNull(param1SecurityParameterIndex);
      if (param1SecurityParameterIndex.getResourceId() != -1) {
        this.mConfig.setMode(1);
        this.mConfig.setSourceAddress(param1InetAddress.getHostAddress());
        this.mConfig.setSpiResourceId(param1SecurityParameterIndex.getResourceId());
        return (new IpSecTransform(this.mContext, this.mConfig)).activate();
      } 
      throw new IllegalArgumentException("Invalid SecurityParameterIndex");
    }
    
    public Builder(Context param1Context) {
      Preconditions.checkNotNull(param1Context);
      this.mContext = param1Context;
      this.mConfig = new IpSecConfig();
    }
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("IpSecTransform{resourceId=");
    int i = this.mResourceId;
    stringBuilder.append(i);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
}
