package android.net;

import android.annotation.SystemApi;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;
import java.util.Set;

@SystemApi
public class ScoredNetwork implements Parcelable {
  public static final String ATTRIBUTES_KEY_BADGING_CURVE = "android.net.attributes.key.BADGING_CURVE";
  
  public static final String ATTRIBUTES_KEY_HAS_CAPTIVE_PORTAL = "android.net.attributes.key.HAS_CAPTIVE_PORTAL";
  
  public static final String ATTRIBUTES_KEY_RANKING_SCORE_OFFSET = "android.net.attributes.key.RANKING_SCORE_OFFSET";
  
  public ScoredNetwork(NetworkKey paramNetworkKey, RssiCurve paramRssiCurve) {
    this(paramNetworkKey, paramRssiCurve, false);
  }
  
  public ScoredNetwork(NetworkKey paramNetworkKey, RssiCurve paramRssiCurve, boolean paramBoolean) {
    this(paramNetworkKey, paramRssiCurve, paramBoolean, null);
  }
  
  public ScoredNetwork(NetworkKey paramNetworkKey, RssiCurve paramRssiCurve, boolean paramBoolean, Bundle paramBundle) {
    this.networkKey = paramNetworkKey;
    this.rssiCurve = paramRssiCurve;
    this.meteredHint = paramBoolean;
    this.attributes = paramBundle;
  }
  
  private ScoredNetwork(Parcel paramParcel) {
    this.networkKey = NetworkKey.CREATOR.createFromParcel(paramParcel);
    byte b = paramParcel.readByte();
    boolean bool = true;
    if (b == 1) {
      this.rssiCurve = RssiCurve.CREATOR.createFromParcel(paramParcel);
    } else {
      this.rssiCurve = null;
    } 
    if (paramParcel.readByte() != 1)
      bool = false; 
    this.meteredHint = bool;
    this.attributes = paramParcel.readBundle();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.networkKey.writeToParcel(paramParcel, paramInt);
    if (this.rssiCurve != null) {
      paramParcel.writeByte((byte)1);
      this.rssiCurve.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeByte((byte)0);
    } 
    paramParcel.writeByte((byte)this.meteredHint);
    paramParcel.writeBundle(this.attributes);
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (Objects.equals(this.networkKey, ((ScoredNetwork)paramObject).networkKey)) {
      RssiCurve rssiCurve1 = this.rssiCurve, rssiCurve2 = ((ScoredNetwork)paramObject).rssiCurve;
      if (Objects.equals(rssiCurve1, rssiCurve2)) {
        boolean bool = this.meteredHint;
        if (Objects.equals(Boolean.valueOf(bool), Boolean.valueOf(((ScoredNetwork)paramObject).meteredHint))) {
          Bundle bundle = this.attributes;
          paramObject = ((ScoredNetwork)paramObject).attributes;
          if (bundleEquals(bundle, (Bundle)paramObject))
            return null; 
        } 
      } 
    } 
    return false;
  }
  
  private boolean bundleEquals(Bundle paramBundle1, Bundle paramBundle2) {
    if (paramBundle1 == paramBundle2)
      return true; 
    if (paramBundle1 == null || paramBundle2 == null)
      return false; 
    if (paramBundle1.size() != paramBundle2.size())
      return false; 
    Set<String> set = paramBundle1.keySet();
    for (String str : set) {
      Object object2 = paramBundle1.get(str);
      Object object1 = paramBundle2.get(str);
      if (!Objects.equals(object2, object1))
        return false; 
    } 
    return true;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.networkKey, this.rssiCurve, Boolean.valueOf(this.meteredHint), this.attributes });
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ScoredNetwork{networkKey=");
    stringBuilder.append(this.networkKey);
    stringBuilder.append(", rssiCurve=");
    stringBuilder.append(this.rssiCurve);
    stringBuilder.append(", meteredHint=");
    stringBuilder.append(this.meteredHint);
    stringBuilder = new StringBuilder(stringBuilder.toString());
    Bundle bundle = this.attributes;
    if (bundle != null && !bundle.isEmpty()) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(", attributes=");
      stringBuilder1.append(this.attributes);
      stringBuilder.append(stringBuilder1.toString());
    } 
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public boolean hasRankingScore() {
    if (this.rssiCurve == null) {
      Bundle bundle = this.attributes;
      if (bundle != null)
        if (bundle.containsKey("android.net.attributes.key.RANKING_SCORE_OFFSET"))
          return true;  
      return false;
    } 
    return true;
  }
  
  public int calculateRankingScore(int paramInt) throws UnsupportedOperationException {
    if (hasRankingScore()) {
      int i = 0;
      Bundle bundle = this.attributes;
      boolean bool = false;
      if (bundle != null)
        i = 0 + bundle.getInt("android.net.attributes.key.RANKING_SCORE_OFFSET", 0); 
      RssiCurve rssiCurve = this.rssiCurve;
      if (rssiCurve == null) {
        paramInt = bool;
      } else {
        paramInt = rssiCurve.lookupScore(paramInt) << 8;
      } 
      try {
        return Math.addExact(paramInt, i);
      } catch (ArithmeticException arithmeticException) {
        if (paramInt < 0) {
          paramInt = Integer.MIN_VALUE;
        } else {
          paramInt = Integer.MAX_VALUE;
        } 
        return paramInt;
      } 
    } 
    throw new UnsupportedOperationException("Either rssiCurve or rankingScoreOffset is required to calculate the ranking score");
  }
  
  public int calculateBadge(int paramInt) {
    Bundle bundle = this.attributes;
    if (bundle != null && bundle.containsKey("android.net.attributes.key.BADGING_CURVE")) {
      bundle = this.attributes;
      RssiCurve rssiCurve = bundle.<RssiCurve>getParcelable("android.net.attributes.key.BADGING_CURVE");
      return rssiCurve.lookupScore(paramInt);
    } 
    return 0;
  }
  
  public static final Parcelable.Creator<ScoredNetwork> CREATOR = new Parcelable.Creator<ScoredNetwork>() {
      public ScoredNetwork createFromParcel(Parcel param1Parcel) {
        return new ScoredNetwork(param1Parcel);
      }
      
      public ScoredNetwork[] newArray(int param1Int) {
        return new ScoredNetwork[param1Int];
      }
    };
  
  public final Bundle attributes;
  
  public final boolean meteredHint;
  
  public final NetworkKey networkKey;
  
  public final RssiCurve rssiCurve;
}
