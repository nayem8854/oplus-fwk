package android.net;

import android.net.netstats.provider.INetworkStatsProvider;
import android.net.netstats.provider.INetworkStatsProviderCallback;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Messenger;
import android.os.Parcel;
import android.os.RemoteException;
import com.android.internal.net.VpnInfo;

public interface INetworkStatsService extends IInterface {
  void forceUpdate() throws RemoteException;
  
  void forceUpdateIfaces(Network[] paramArrayOfNetwork, NetworkState[] paramArrayOfNetworkState, String paramString, VpnInfo[] paramArrayOfVpnInfo) throws RemoteException;
  
  NetworkStats getDataLayerSnapshotForUid(int paramInt) throws RemoteException;
  
  NetworkStats getDetailedUidStats(String[] paramArrayOfString) throws RemoteException;
  
  long getIfaceStats(String paramString, int paramInt) throws RemoteException;
  
  String[] getMobileIfaces() throws RemoteException;
  
  long getTotalStats(int paramInt) throws RemoteException;
  
  long getUidStats(int paramInt1, int paramInt2) throws RemoteException;
  
  long getUidStatsWithoutCheckUid(int paramInt1, int paramInt2) throws RemoteException;
  
  void incrementOperationCount(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  INetworkStatsSession openSession() throws RemoteException;
  
  INetworkStatsSession openSessionForUsageStats(int paramInt, String paramString) throws RemoteException;
  
  INetworkStatsProviderCallback registerNetworkStatsProvider(String paramString, INetworkStatsProvider paramINetworkStatsProvider) throws RemoteException;
  
  DataUsageRequest registerUsageCallback(String paramString, DataUsageRequest paramDataUsageRequest, Messenger paramMessenger, IBinder paramIBinder) throws RemoteException;
  
  void unregisterUsageRequest(DataUsageRequest paramDataUsageRequest) throws RemoteException;
  
  class Default implements INetworkStatsService {
    public INetworkStatsSession openSession() throws RemoteException {
      return null;
    }
    
    public INetworkStatsSession openSessionForUsageStats(int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public NetworkStats getDataLayerSnapshotForUid(int param1Int) throws RemoteException {
      return null;
    }
    
    public NetworkStats getDetailedUidStats(String[] param1ArrayOfString) throws RemoteException {
      return null;
    }
    
    public String[] getMobileIfaces() throws RemoteException {
      return null;
    }
    
    public void incrementOperationCount(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void forceUpdateIfaces(Network[] param1ArrayOfNetwork, NetworkState[] param1ArrayOfNetworkState, String param1String, VpnInfo[] param1ArrayOfVpnInfo) throws RemoteException {}
    
    public void forceUpdate() throws RemoteException {}
    
    public DataUsageRequest registerUsageCallback(String param1String, DataUsageRequest param1DataUsageRequest, Messenger param1Messenger, IBinder param1IBinder) throws RemoteException {
      return null;
    }
    
    public void unregisterUsageRequest(DataUsageRequest param1DataUsageRequest) throws RemoteException {}
    
    public long getUidStats(int param1Int1, int param1Int2) throws RemoteException {
      return 0L;
    }
    
    public long getUidStatsWithoutCheckUid(int param1Int1, int param1Int2) throws RemoteException {
      return 0L;
    }
    
    public long getIfaceStats(String param1String, int param1Int) throws RemoteException {
      return 0L;
    }
    
    public long getTotalStats(int param1Int) throws RemoteException {
      return 0L;
    }
    
    public INetworkStatsProviderCallback registerNetworkStatsProvider(String param1String, INetworkStatsProvider param1INetworkStatsProvider) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INetworkStatsService {
    private static final String DESCRIPTOR = "android.net.INetworkStatsService";
    
    static final int TRANSACTION_forceUpdate = 8;
    
    static final int TRANSACTION_forceUpdateIfaces = 7;
    
    static final int TRANSACTION_getDataLayerSnapshotForUid = 3;
    
    static final int TRANSACTION_getDetailedUidStats = 4;
    
    static final int TRANSACTION_getIfaceStats = 13;
    
    static final int TRANSACTION_getMobileIfaces = 5;
    
    static final int TRANSACTION_getTotalStats = 14;
    
    static final int TRANSACTION_getUidStats = 11;
    
    static final int TRANSACTION_getUidStatsWithoutCheckUid = 12;
    
    static final int TRANSACTION_incrementOperationCount = 6;
    
    static final int TRANSACTION_openSession = 1;
    
    static final int TRANSACTION_openSessionForUsageStats = 2;
    
    static final int TRANSACTION_registerNetworkStatsProvider = 15;
    
    static final int TRANSACTION_registerUsageCallback = 9;
    
    static final int TRANSACTION_unregisterUsageRequest = 10;
    
    public Stub() {
      attachInterface(this, "android.net.INetworkStatsService");
    }
    
    public static INetworkStatsService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.INetworkStatsService");
      if (iInterface != null && iInterface instanceof INetworkStatsService)
        return (INetworkStatsService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 15:
          return "registerNetworkStatsProvider";
        case 14:
          return "getTotalStats";
        case 13:
          return "getIfaceStats";
        case 12:
          return "getUidStatsWithoutCheckUid";
        case 11:
          return "getUidStats";
        case 10:
          return "unregisterUsageRequest";
        case 9:
          return "registerUsageCallback";
        case 8:
          return "forceUpdate";
        case 7:
          return "forceUpdateIfaces";
        case 6:
          return "incrementOperationCount";
        case 5:
          return "getMobileIfaces";
        case 4:
          return "getDetailedUidStats";
        case 3:
          return "getDataLayerSnapshotForUid";
        case 2:
          return "openSessionForUsageStats";
        case 1:
          break;
      } 
      return "openSession";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        INetworkStatsProvider iNetworkStatsProvider1;
        IBinder iBinder3;
        DataUsageRequest dataUsageRequest;
        VpnInfo[] arrayOfVpnInfo;
        String[] arrayOfString;
        NetworkStats networkStats;
        String str1;
        IBinder iBinder2, iBinder1;
        INetworkStatsProviderCallback iNetworkStatsProviderCallback;
        String str2;
        NetworkState[] arrayOfNetworkState2;
        String str5;
        Network[] arrayOfNetwork;
        long l;
        int i;
        String str3 = null, str4 = null;
        INetworkStatsProvider iNetworkStatsProvider2 = null;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 15:
            param1Parcel1.enforceInterface("android.net.INetworkStatsService");
            str3 = param1Parcel1.readString();
            iNetworkStatsProvider1 = INetworkStatsProvider.Stub.asInterface(param1Parcel1.readStrongBinder());
            iNetworkStatsProviderCallback = registerNetworkStatsProvider(str3, iNetworkStatsProvider1);
            param1Parcel2.writeNoException();
            iNetworkStatsProvider1 = iNetworkStatsProvider2;
            if (iNetworkStatsProviderCallback != null)
              iBinder3 = iNetworkStatsProviderCallback.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder3);
            return true;
          case 14:
            iBinder3.enforceInterface("android.net.INetworkStatsService");
            param1Int1 = iBinder3.readInt();
            l = getTotalStats(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 13:
            iBinder3.enforceInterface("android.net.INetworkStatsService");
            str5 = iBinder3.readString();
            param1Int1 = iBinder3.readInt();
            l = getIfaceStats(str5, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 12:
            iBinder3.enforceInterface("android.net.INetworkStatsService");
            param1Int2 = iBinder3.readInt();
            param1Int1 = iBinder3.readInt();
            l = getUidStatsWithoutCheckUid(param1Int2, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 11:
            iBinder3.enforceInterface("android.net.INetworkStatsService");
            param1Int1 = iBinder3.readInt();
            param1Int2 = iBinder3.readInt();
            l = getUidStats(param1Int1, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 10:
            iBinder3.enforceInterface("android.net.INetworkStatsService");
            if (iBinder3.readInt() != 0) {
              DataUsageRequest dataUsageRequest1 = DataUsageRequest.CREATOR.createFromParcel((Parcel)iBinder3);
            } else {
              iBinder3 = null;
            } 
            unregisterUsageRequest((DataUsageRequest)iBinder3);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            iBinder3.enforceInterface("android.net.INetworkStatsService");
            str4 = iBinder3.readString();
            if (iBinder3.readInt() != 0) {
              DataUsageRequest dataUsageRequest1 = DataUsageRequest.CREATOR.createFromParcel((Parcel)iBinder3);
            } else {
              str5 = null;
            } 
            if (iBinder3.readInt() != 0) {
              Messenger messenger = Messenger.CREATOR.createFromParcel((Parcel)iBinder3);
            } else {
              iNetworkStatsProviderCallback = null;
            } 
            iBinder3 = iBinder3.readStrongBinder();
            dataUsageRequest = registerUsageCallback(str4, (DataUsageRequest)str5, (Messenger)iNetworkStatsProviderCallback, iBinder3);
            param1Parcel2.writeNoException();
            if (dataUsageRequest != null) {
              param1Parcel2.writeInt(1);
              dataUsageRequest.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 8:
            dataUsageRequest.enforceInterface("android.net.INetworkStatsService");
            forceUpdate();
            param1Parcel2.writeNoException();
            return true;
          case 7:
            dataUsageRequest.enforceInterface("android.net.INetworkStatsService");
            arrayOfNetwork = dataUsageRequest.<Network>createTypedArray(Network.CREATOR);
            arrayOfNetworkState2 = dataUsageRequest.<NetworkState>createTypedArray(NetworkState.CREATOR);
            str2 = dataUsageRequest.readString();
            arrayOfVpnInfo = dataUsageRequest.<VpnInfo>createTypedArray(VpnInfo.CREATOR);
            forceUpdateIfaces(arrayOfNetwork, arrayOfNetworkState2, str2, arrayOfVpnInfo);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            arrayOfVpnInfo.enforceInterface("android.net.INetworkStatsService");
            param1Int1 = arrayOfVpnInfo.readInt();
            param1Int2 = arrayOfVpnInfo.readInt();
            i = arrayOfVpnInfo.readInt();
            incrementOperationCount(param1Int1, param1Int2, i);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            arrayOfVpnInfo.enforceInterface("android.net.INetworkStatsService");
            arrayOfString = getMobileIfaces();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString);
            return true;
          case 4:
            arrayOfString.enforceInterface("android.net.INetworkStatsService");
            arrayOfString = arrayOfString.createStringArray();
            networkStats = getDetailedUidStats(arrayOfString);
            param1Parcel2.writeNoException();
            if (networkStats != null) {
              param1Parcel2.writeInt(1);
              networkStats.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            networkStats.enforceInterface("android.net.INetworkStatsService");
            param1Int1 = networkStats.readInt();
            networkStats = getDataLayerSnapshotForUid(param1Int1);
            param1Parcel2.writeNoException();
            if (networkStats != null) {
              param1Parcel2.writeInt(1);
              networkStats.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 2:
            networkStats.enforceInterface("android.net.INetworkStatsService");
            param1Int1 = networkStats.readInt();
            str1 = networkStats.readString();
            iNetworkStatsSession = openSessionForUsageStats(param1Int1, str1);
            param1Parcel2.writeNoException();
            str1 = str2;
            if (iNetworkStatsSession != null)
              iBinder2 = iNetworkStatsSession.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder2);
            return true;
          case 1:
            break;
        } 
        iBinder2.enforceInterface("android.net.INetworkStatsService");
        INetworkStatsSession iNetworkStatsSession = openSession();
        param1Parcel2.writeNoException();
        NetworkState[] arrayOfNetworkState1 = arrayOfNetworkState2;
        if (iNetworkStatsSession != null)
          iBinder1 = iNetworkStatsSession.asBinder(); 
        param1Parcel2.writeStrongBinder(iBinder1);
        return true;
      } 
      param1Parcel2.writeString("android.net.INetworkStatsService");
      return true;
    }
    
    private static class Proxy implements INetworkStatsService {
      public static INetworkStatsService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.INetworkStatsService";
      }
      
      public INetworkStatsSession openSession() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsService");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && INetworkStatsService.Stub.getDefaultImpl() != null)
            return INetworkStatsService.Stub.getDefaultImpl().openSession(); 
          parcel2.readException();
          return INetworkStatsSession.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public INetworkStatsSession openSessionForUsageStats(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && INetworkStatsService.Stub.getDefaultImpl() != null)
            return INetworkStatsService.Stub.getDefaultImpl().openSessionForUsageStats(param2Int, param2String); 
          parcel2.readException();
          return INetworkStatsSession.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkStats getDataLayerSnapshotForUid(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          NetworkStats networkStats;
          parcel1.writeInterfaceToken("android.net.INetworkStatsService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && INetworkStatsService.Stub.getDefaultImpl() != null) {
            networkStats = INetworkStatsService.Stub.getDefaultImpl().getDataLayerSnapshotForUid(param2Int);
            return networkStats;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            networkStats = NetworkStats.CREATOR.createFromParcel(parcel2);
          } else {
            networkStats = null;
          } 
          return networkStats;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkStats getDetailedUidStats(String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsService");
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && INetworkStatsService.Stub.getDefaultImpl() != null)
            return INetworkStatsService.Stub.getDefaultImpl().getDetailedUidStats(param2ArrayOfString); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            NetworkStats networkStats = NetworkStats.CREATOR.createFromParcel(parcel2);
          } else {
            param2ArrayOfString = null;
          } 
          return (NetworkStats)param2ArrayOfString;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getMobileIfaces() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsService");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && INetworkStatsService.Stub.getDefaultImpl() != null)
            return INetworkStatsService.Stub.getDefaultImpl().getMobileIfaces(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void incrementOperationCount(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && INetworkStatsService.Stub.getDefaultImpl() != null) {
            INetworkStatsService.Stub.getDefaultImpl().incrementOperationCount(param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void forceUpdateIfaces(Network[] param2ArrayOfNetwork, NetworkState[] param2ArrayOfNetworkState, String param2String, VpnInfo[] param2ArrayOfVpnInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsService");
          parcel1.writeTypedArray(param2ArrayOfNetwork, 0);
          parcel1.writeTypedArray(param2ArrayOfNetworkState, 0);
          parcel1.writeString(param2String);
          parcel1.writeTypedArray(param2ArrayOfVpnInfo, 0);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && INetworkStatsService.Stub.getDefaultImpl() != null) {
            INetworkStatsService.Stub.getDefaultImpl().forceUpdateIfaces(param2ArrayOfNetwork, param2ArrayOfNetworkState, param2String, param2ArrayOfVpnInfo);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void forceUpdate() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsService");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && INetworkStatsService.Stub.getDefaultImpl() != null) {
            INetworkStatsService.Stub.getDefaultImpl().forceUpdate();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public DataUsageRequest registerUsageCallback(String param2String, DataUsageRequest param2DataUsageRequest, Messenger param2Messenger, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsService");
          parcel1.writeString(param2String);
          if (param2DataUsageRequest != null) {
            parcel1.writeInt(1);
            param2DataUsageRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Messenger != null) {
            parcel1.writeInt(1);
            param2Messenger.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && INetworkStatsService.Stub.getDefaultImpl() != null)
            return INetworkStatsService.Stub.getDefaultImpl().registerUsageCallback(param2String, param2DataUsageRequest, param2Messenger, param2IBinder); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            DataUsageRequest dataUsageRequest = DataUsageRequest.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (DataUsageRequest)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterUsageRequest(DataUsageRequest param2DataUsageRequest) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsService");
          if (param2DataUsageRequest != null) {
            parcel1.writeInt(1);
            param2DataUsageRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && INetworkStatsService.Stub.getDefaultImpl() != null) {
            INetworkStatsService.Stub.getDefaultImpl().unregisterUsageRequest(param2DataUsageRequest);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getUidStats(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && INetworkStatsService.Stub.getDefaultImpl() != null)
            return INetworkStatsService.Stub.getDefaultImpl().getUidStats(param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getUidStatsWithoutCheckUid(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && INetworkStatsService.Stub.getDefaultImpl() != null)
            return INetworkStatsService.Stub.getDefaultImpl().getUidStatsWithoutCheckUid(param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getIfaceStats(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && INetworkStatsService.Stub.getDefaultImpl() != null)
            return INetworkStatsService.Stub.getDefaultImpl().getIfaceStats(param2String, param2Int); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getTotalStats(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkStatsService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && INetworkStatsService.Stub.getDefaultImpl() != null)
            return INetworkStatsService.Stub.getDefaultImpl().getTotalStats(param2Int); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public INetworkStatsProviderCallback registerNetworkStatsProvider(String param2String, INetworkStatsProvider param2INetworkStatsProvider) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.net.INetworkStatsService");
          parcel1.writeString(param2String);
          if (param2INetworkStatsProvider != null) {
            iBinder = param2INetworkStatsProvider.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && INetworkStatsService.Stub.getDefaultImpl() != null)
            return INetworkStatsService.Stub.getDefaultImpl().registerNetworkStatsProvider(param2String, param2INetworkStatsProvider); 
          parcel2.readException();
          return INetworkStatsProviderCallback.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INetworkStatsService param1INetworkStatsService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INetworkStatsService != null) {
          Proxy.sDefaultImpl = param1INetworkStatsService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INetworkStatsService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
