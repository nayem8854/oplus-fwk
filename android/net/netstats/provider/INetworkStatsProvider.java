package android.net.netstats.provider;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INetworkStatsProvider extends IInterface {
  void onRequestStatsUpdate(int paramInt) throws RemoteException;
  
  void onSetAlert(long paramLong) throws RemoteException;
  
  void onSetLimit(String paramString, long paramLong) throws RemoteException;
  
  class Default implements INetworkStatsProvider {
    public void onRequestStatsUpdate(int param1Int) throws RemoteException {}
    
    public void onSetLimit(String param1String, long param1Long) throws RemoteException {}
    
    public void onSetAlert(long param1Long) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INetworkStatsProvider {
    private static final String DESCRIPTOR = "android.net.netstats.provider.INetworkStatsProvider";
    
    static final int TRANSACTION_onRequestStatsUpdate = 1;
    
    static final int TRANSACTION_onSetAlert = 3;
    
    static final int TRANSACTION_onSetLimit = 2;
    
    public Stub() {
      attachInterface(this, "android.net.netstats.provider.INetworkStatsProvider");
    }
    
    public static INetworkStatsProvider asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.netstats.provider.INetworkStatsProvider");
      if (iInterface != null && iInterface instanceof INetworkStatsProvider)
        return (INetworkStatsProvider)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onSetAlert";
        } 
        return "onSetLimit";
      } 
      return "onRequestStatsUpdate";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.net.netstats.provider.INetworkStatsProvider");
            return true;
          } 
          param1Parcel1.enforceInterface("android.net.netstats.provider.INetworkStatsProvider");
          long l1 = param1Parcel1.readLong();
          onSetAlert(l1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.net.netstats.provider.INetworkStatsProvider");
        String str = param1Parcel1.readString();
        long l = param1Parcel1.readLong();
        onSetLimit(str, l);
        return true;
      } 
      param1Parcel1.enforceInterface("android.net.netstats.provider.INetworkStatsProvider");
      param1Int1 = param1Parcel1.readInt();
      onRequestStatsUpdate(param1Int1);
      return true;
    }
    
    private static class Proxy implements INetworkStatsProvider {
      public static INetworkStatsProvider sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.netstats.provider.INetworkStatsProvider";
      }
      
      public void onRequestStatsUpdate(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.netstats.provider.INetworkStatsProvider");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && INetworkStatsProvider.Stub.getDefaultImpl() != null) {
            INetworkStatsProvider.Stub.getDefaultImpl().onRequestStatsUpdate(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSetLimit(String param2String, long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.netstats.provider.INetworkStatsProvider");
          parcel.writeString(param2String);
          parcel.writeLong(param2Long);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && INetworkStatsProvider.Stub.getDefaultImpl() != null) {
            INetworkStatsProvider.Stub.getDefaultImpl().onSetLimit(param2String, param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSetAlert(long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.netstats.provider.INetworkStatsProvider");
          parcel.writeLong(param2Long);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && INetworkStatsProvider.Stub.getDefaultImpl() != null) {
            INetworkStatsProvider.Stub.getDefaultImpl().onSetAlert(param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INetworkStatsProvider param1INetworkStatsProvider) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INetworkStatsProvider != null) {
          Proxy.sDefaultImpl = param1INetworkStatsProvider;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INetworkStatsProvider getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
