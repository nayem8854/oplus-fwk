package android.net.netstats.provider;

import android.net.NetworkStats;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INetworkStatsProviderCallback extends IInterface {
  void notifyAlertReached() throws RemoteException;
  
  void notifyLimitReached() throws RemoteException;
  
  void notifyStatsUpdated(int paramInt, NetworkStats paramNetworkStats1, NetworkStats paramNetworkStats2) throws RemoteException;
  
  void unregister() throws RemoteException;
  
  class Default implements INetworkStatsProviderCallback {
    public void notifyStatsUpdated(int param1Int, NetworkStats param1NetworkStats1, NetworkStats param1NetworkStats2) throws RemoteException {}
    
    public void notifyAlertReached() throws RemoteException {}
    
    public void notifyLimitReached() throws RemoteException {}
    
    public void unregister() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INetworkStatsProviderCallback {
    private static final String DESCRIPTOR = "android.net.netstats.provider.INetworkStatsProviderCallback";
    
    static final int TRANSACTION_notifyAlertReached = 2;
    
    static final int TRANSACTION_notifyLimitReached = 3;
    
    static final int TRANSACTION_notifyStatsUpdated = 1;
    
    static final int TRANSACTION_unregister = 4;
    
    public Stub() {
      attachInterface(this, "android.net.netstats.provider.INetworkStatsProviderCallback");
    }
    
    public static INetworkStatsProviderCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.netstats.provider.INetworkStatsProviderCallback");
      if (iInterface != null && iInterface instanceof INetworkStatsProviderCallback)
        return (INetworkStatsProviderCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "unregister";
          } 
          return "notifyLimitReached";
        } 
        return "notifyAlertReached";
      } 
      return "notifyStatsUpdated";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.net.netstats.provider.INetworkStatsProviderCallback");
              return true;
            } 
            param1Parcel1.enforceInterface("android.net.netstats.provider.INetworkStatsProviderCallback");
            unregister();
            return true;
          } 
          param1Parcel1.enforceInterface("android.net.netstats.provider.INetworkStatsProviderCallback");
          notifyLimitReached();
          return true;
        } 
        param1Parcel1.enforceInterface("android.net.netstats.provider.INetworkStatsProviderCallback");
        notifyAlertReached();
        return true;
      } 
      param1Parcel1.enforceInterface("android.net.netstats.provider.INetworkStatsProviderCallback");
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        NetworkStats networkStats = NetworkStats.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        NetworkStats networkStats = NetworkStats.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      notifyStatsUpdated(param1Int1, (NetworkStats)param1Parcel2, (NetworkStats)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements INetworkStatsProviderCallback {
      public static INetworkStatsProviderCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.netstats.provider.INetworkStatsProviderCallback";
      }
      
      public void notifyStatsUpdated(int param2Int, NetworkStats param2NetworkStats1, NetworkStats param2NetworkStats2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.netstats.provider.INetworkStatsProviderCallback");
          parcel.writeInt(param2Int);
          if (param2NetworkStats1 != null) {
            parcel.writeInt(1);
            param2NetworkStats1.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2NetworkStats2 != null) {
            parcel.writeInt(1);
            param2NetworkStats2.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && INetworkStatsProviderCallback.Stub.getDefaultImpl() != null) {
            INetworkStatsProviderCallback.Stub.getDefaultImpl().notifyStatsUpdated(param2Int, param2NetworkStats1, param2NetworkStats2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyAlertReached() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.netstats.provider.INetworkStatsProviderCallback");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && INetworkStatsProviderCallback.Stub.getDefaultImpl() != null) {
            INetworkStatsProviderCallback.Stub.getDefaultImpl().notifyAlertReached();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyLimitReached() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.netstats.provider.INetworkStatsProviderCallback");
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && INetworkStatsProviderCallback.Stub.getDefaultImpl() != null) {
            INetworkStatsProviderCallback.Stub.getDefaultImpl().notifyLimitReached();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void unregister() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.netstats.provider.INetworkStatsProviderCallback");
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && INetworkStatsProviderCallback.Stub.getDefaultImpl() != null) {
            INetworkStatsProviderCallback.Stub.getDefaultImpl().unregister();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INetworkStatsProviderCallback param1INetworkStatsProviderCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INetworkStatsProviderCallback != null) {
          Proxy.sDefaultImpl = param1INetworkStatsProviderCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INetworkStatsProviderCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
