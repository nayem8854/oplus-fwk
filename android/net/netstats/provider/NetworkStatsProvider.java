package android.net.netstats.provider;

import android.annotation.SystemApi;
import android.net.NetworkStats;
import android.os.RemoteException;

@SystemApi
public abstract class NetworkStatsProvider {
  public static final int QUOTA_UNLIMITED = -1;
  
  private final INetworkStatsProvider mProviderBinder = (INetworkStatsProvider)new Object(this);
  
  private INetworkStatsProviderCallback mProviderCbBinder;
  
  public INetworkStatsProvider getProviderBinder() {
    return this.mProviderBinder;
  }
  
  public void setProviderCallbackBinder(INetworkStatsProviderCallback paramINetworkStatsProviderCallback) {
    if (this.mProviderCbBinder == null) {
      this.mProviderCbBinder = paramINetworkStatsProviderCallback;
      return;
    } 
    throw new IllegalArgumentException("provider is already registered");
  }
  
  public INetworkStatsProviderCallback getProviderCallbackBinder() {
    return this.mProviderCbBinder;
  }
  
  public INetworkStatsProviderCallback getProviderCallbackBinderOrThrow() {
    INetworkStatsProviderCallback iNetworkStatsProviderCallback = this.mProviderCbBinder;
    if (iNetworkStatsProviderCallback != null)
      return iNetworkStatsProviderCallback; 
    throw new IllegalStateException("the provider is not registered");
  }
  
  public void notifyStatsUpdated(int paramInt, NetworkStats paramNetworkStats1, NetworkStats paramNetworkStats2) {
    try {
      getProviderCallbackBinderOrThrow().notifyStatsUpdated(paramInt, paramNetworkStats1, paramNetworkStats2);
    } catch (RemoteException remoteException) {
      remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public void notifyAlertReached() {
    try {
      getProviderCallbackBinderOrThrow().notifyAlertReached();
    } catch (RemoteException remoteException) {
      remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public void notifyLimitReached() {
    try {
      getProviderCallbackBinderOrThrow().notifyLimitReached();
    } catch (RemoteException remoteException) {
      remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public abstract void onRequestStatsUpdate(int paramInt);
  
  public abstract void onSetAlert(long paramLong);
  
  public abstract void onSetLimit(String paramString, long paramLong);
}
