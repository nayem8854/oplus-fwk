package android.net.wifi;

import com.oplus.reflect.RefClass;
import com.oplus.reflect.RefInt;
import com.oplus.reflect.RefObject;

public class OplusMirrorWifiConfiguration {
  public static Class<?> TYPE = RefClass.load(OplusMirrorWifiConfiguration.class, WifiConfiguration.class);
  
  public static RefObject<String> wapiCertSel;
  
  public static RefInt wapiCertSelMode;
  
  public static RefObject<String> wapiPsk;
  
  public static RefInt wapiPskType;
}
