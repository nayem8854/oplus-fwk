package android.net.wifi;

import java.util.List;
import oplus.net.wifi.HotspotClient;

public interface IOplusWifiManagerEx {
  boolean blockClient(HotspotClient paramHotspotClient);
  
  String[] getAllSlaAcceleratedApps();
  
  String[] getAllSlaAppsAndStates();
  
  List<HotspotClient> getBlockedHotspotClients();
  
  List<HotspotClient> getHotspotClients();
  
  boolean getSlaAppState(String paramString);
  
  WifiConfiguration getWifiSharingConfiguration();
  
  boolean isSlaSupported();
  
  boolean setSlaAppState(String paramString, boolean paramBoolean);
  
  boolean setWifiSharingConfiguration(WifiConfiguration paramWifiConfiguration);
  
  boolean unblockClient(HotspotClient paramHotspotClient);
}
