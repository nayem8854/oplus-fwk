package android.net.wifi;

public class OplusBaseWifiConfiguration {
  protected WapiWrapper mWrapper = new WapiWrapper();
  
  public static class WapiWrapper {
    public String wapiCertSel;
    
    public int wapiCertSelMode;
    
    public String wapiPsk;
    
    public int wapiPskType;
    
    public WapiWrapper() {
      this.wapiPskType = -1;
      this.wapiPsk = null;
      this.wapiCertSelMode = -1;
      this.wapiCertSel = null;
    }
    
    public WapiWrapper(int param1Int1, String param1String1, int param1Int2, String param1String2) {
      this.wapiPskType = param1Int1;
      this.wapiPsk = param1String1;
      this.wapiCertSelMode = param1Int2;
      this.wapiCertSel = param1String2;
    }
  }
}
