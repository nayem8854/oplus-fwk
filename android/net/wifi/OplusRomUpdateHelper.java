package android.net.wifi;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.SystemProperties;
import android.util.Log;
import android.util.Xml;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class OplusRomUpdateHelper {
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  static {
    CONTENT_URI_WHITE_LIST = Uri.parse("content://com.nearme.romupdate.provider.db/update_list");
  }
  
  private boolean which2use = true;
  
  private String mFilterName = "";
  
  private String mSystemFilePath = "";
  
  private String mDataFilePath = "";
  
  public Context mContext = null;
  
  public static final String BROADCAST_ACTION_ROM_UPDATE_CONFIG_SUCCES = "oppo.intent.action.ROM_UPDATE_CONFIG_SUCCESS";
  
  private static final String COLUMN_NAME_1 = "version";
  
  private static final String COLUMN_NAME_2 = "xml";
  
  private static final Uri CONTENT_URI_WHITE_LIST;
  
  private static final String OPPO_COMPONENT_SAFE_PERMISSION = "oppo.permission.OPPO_COMPONENT_SAFE";
  
  public static final String ROM_UPDATE_CONFIG_LIST = "ROM_UPDATE_CONFIG_LIST";
  
  private static final String TAG = "OplusRomUpdateHelper";
  
  private UpdateInfo mUpdateInfo1;
  
  private UpdateInfo mUpdateInfo2;
  
  protected class UpdateInfo {
    protected long mVersion = -1L;
    
    final OplusRomUpdateHelper this$0;
    
    public void parseContentFromXML(String param1String) {}
    
    public boolean clone(UpdateInfo param1UpdateInfo) {
      return false;
    }
    
    public boolean insert(int param1Int, String param1String) {
      return false;
    }
    
    public void clear() {}
    
    public void dump() {}
    
    public long getVersion() {
      return this.mVersion;
    }
    
    public boolean updateToLowerVersion(String param1String) {
      return false;
    }
  }
  
  public OplusRomUpdateHelper(Context paramContext, String paramString1, String paramString2, String paramString3) {
    this.mContext = paramContext;
    this.mFilterName = paramString1;
    this.mSystemFilePath = paramString2;
    this.mDataFilePath = paramString3;
  }
  
  public void init() {
    if (this.mDataFilePath == null || this.mSystemFilePath == null)
      return; 
    new File(this.mDataFilePath);
    File file1 = new File(this.mDataFilePath);
    File file2 = new File(this.mSystemFilePath);
    if (!file1.exists() && !file2.exists()) {
      Log.d("OplusRomUpdateHelper", "Fail to open data file and system file.");
      return;
    } 
    long l1 = getVersion(readFromFile(file1));
    long l2 = getVersion(readFromFile(file2));
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("dataVersion=");
    stringBuilder.append(l1);
    stringBuilder.append(", systemVersion=");
    stringBuilder.append(l2);
    Log.d("OplusRomUpdateHelper", stringBuilder.toString());
    if (l1 <= l2)
      file1 = file2; 
    parseContentFromXML(readFromFile(file1));
  }
  
  protected void setUpdateInfo(UpdateInfo paramUpdateInfo1, UpdateInfo paramUpdateInfo2) {
    this.mUpdateInfo1 = paramUpdateInfo1;
    this.mUpdateInfo2 = paramUpdateInfo2;
  }
  
  public void initUpdateBroadcastReceiver() {
    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction("oppo.intent.action.ROM_UPDATE_CONFIG_SUCCESS");
    this.mContext.registerReceiver(new BroadcastReceiver() {
          final OplusRomUpdateHelper this$0;
          
          public void onReceive(Context param1Context, Intent param1Intent) {
            if (OplusRomUpdateHelper.DEBUG) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append(this);
              stringBuilder.append(", ");
              stringBuilder.append(OplusRomUpdateHelper.this.getFilterName());
              stringBuilder.append(", onReceive intent = ");
              stringBuilder.append(param1Intent);
              Log.d("OplusRomUpdateHelper", stringBuilder.toString());
            } 
            if (param1Intent != null) {
              ArrayList arrayList = param1Intent.getStringArrayListExtra("ROM_UPDATE_CONFIG_LIST");
              if (arrayList != null && arrayList.contains(OplusRomUpdateHelper.this.mFilterName))
                OplusRomUpdateHelper.this.getUpdateFromProvider(); 
            } 
          }
        }intentFilter, "oppo.permission.OPPO_COMPONENT_SAFE", null);
  }
  
  protected UpdateInfo getUpdateInfo(boolean paramBoolean) {
    UpdateInfo updateInfo;
    if (paramBoolean) {
      if (this.which2use) {
        updateInfo = this.mUpdateInfo1;
      } else {
        updateInfo = this.mUpdateInfo2;
      } 
      return updateInfo;
    } 
    if (this.which2use) {
      updateInfo = this.mUpdateInfo2;
    } else {
      updateInfo = this.mUpdateInfo1;
    } 
    return updateInfo;
  }
  
  private void setFlip() {
    this.which2use ^= 0x1;
  }
  
  private boolean saveToFile(String paramString1, String paramString2) {
    try {
      File file = new File();
      this(paramString2);
      FileOutputStream fileOutputStream = new FileOutputStream();
      this(file);
      fileOutputStream.write(paramString1.getBytes());
      fileOutputStream.close();
      return true;
    } catch (Exception exception) {
      exception.printStackTrace();
      return false;
    } 
  }
  
  public String getFilterName() {
    return this.mFilterName;
  }
  
  private String getDataFromProvider() {
    Cursor cursor1 = null, cursor2 = null;
    String str1 = null;
    Cursor cursor3 = null;
    Cursor cursor4 = cursor2, cursor5 = cursor1;
    String str2 = str1;
    try {
      String str3;
      Context context = this.mContext;
      if (context == null) {
        if (false)
          throw new NullPointerException(); 
        return null;
      } 
      cursor4 = cursor2;
      cursor5 = cursor1;
      str2 = str1;
      ContentResolver contentResolver = this.mContext.getContentResolver();
      cursor4 = cursor2;
      cursor5 = cursor1;
      str2 = str1;
      Uri uri = CONTENT_URI_WHITE_LIST;
      cursor4 = cursor2;
      cursor5 = cursor1;
      str2 = str1;
      StringBuilder stringBuilder = new StringBuilder();
      cursor4 = cursor2;
      cursor5 = cursor1;
      str2 = str1;
      this();
      cursor4 = cursor2;
      cursor5 = cursor1;
      str2 = str1;
      stringBuilder.append("filtername=\"");
      cursor4 = cursor2;
      cursor5 = cursor1;
      str2 = str1;
      stringBuilder.append(this.mFilterName);
      cursor4 = cursor2;
      cursor5 = cursor1;
      str2 = str1;
      stringBuilder.append("\"");
      cursor4 = cursor2;
      cursor5 = cursor1;
      str2 = str1;
      String str5 = stringBuilder.toString();
      cursor4 = cursor2;
      cursor5 = cursor1;
      str2 = str1;
      cursor2 = contentResolver.query(uri, new String[] { "version", "xml" }, str5, null, null);
      cursor1 = cursor3;
      if (cursor2 != null) {
        cursor1 = cursor3;
        cursor4 = cursor2;
        cursor5 = cursor2;
        str2 = str1;
        if (cursor2.getCount() > 0) {
          cursor4 = cursor2;
          cursor5 = cursor2;
          str2 = str1;
          int i = cursor2.getColumnIndex("version");
          cursor4 = cursor2;
          cursor5 = cursor2;
          str2 = str1;
          int j = cursor2.getColumnIndex("xml");
          cursor4 = cursor2;
          cursor5 = cursor2;
          str2 = str1;
          cursor2.moveToNext();
          cursor4 = cursor2;
          cursor5 = cursor2;
          str2 = str1;
          i = cursor2.getInt(i);
          cursor4 = cursor2;
          cursor5 = cursor2;
          str2 = str1;
          str3 = cursor2.getString(j);
          cursor4 = cursor2;
          cursor5 = cursor2;
          str2 = str3;
          StringBuilder stringBuilder1 = new StringBuilder();
          cursor4 = cursor2;
          cursor5 = cursor2;
          str2 = str3;
          this();
          cursor4 = cursor2;
          cursor5 = cursor2;
          str2 = str3;
          stringBuilder1.append("White List updated, version = ");
          cursor4 = cursor2;
          cursor5 = cursor2;
          str2 = str3;
          stringBuilder1.append(i);
          cursor4 = cursor2;
          cursor5 = cursor2;
          str2 = str3;
          Log.d("OplusRomUpdateHelper", stringBuilder1.toString());
        } 
      } 
      String str4 = str3;
      if (cursor2 != null) {
        cursor5 = cursor2;
        str2 = str3;
      } else {
        return str4;
      } 
    } catch (Exception exception) {
      cursor4 = cursor5;
      StringBuilder stringBuilder = new StringBuilder();
      cursor4 = cursor5;
      this();
      cursor4 = cursor5;
      stringBuilder.append("We can not get white list data from provider, because of ");
      cursor4 = cursor5;
      stringBuilder.append(exception);
      cursor4 = cursor5;
      Log.w("OplusRomUpdateHelper", stringBuilder.toString());
      String str = str2;
      if (cursor5 != null) {
        cursor5.close();
        str = str2;
        return str;
      } 
    } finally {}
    cursor5.close();
    return str2;
  }
  
  public String readFromFile(File paramFile) {
    if (paramFile == null)
      return ""; 
    if (!paramFile.exists())
      return ""; 
    FileInputStream fileInputStream1 = null, fileInputStream2 = null, fileInputStream3 = null;
    FileInputStream fileInputStream4 = fileInputStream3, fileInputStream5 = fileInputStream1, fileInputStream6 = fileInputStream2;
    try {
      FileInputStream fileInputStream8 = new FileInputStream();
      fileInputStream4 = fileInputStream3;
      fileInputStream5 = fileInputStream1;
      fileInputStream6 = fileInputStream2;
      this(paramFile);
      FileInputStream fileInputStream7 = fileInputStream8;
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      BufferedReader bufferedReader = new BufferedReader();
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      InputStreamReader inputStreamReader = new InputStreamReader();
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      this(fileInputStream7);
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      this(inputStreamReader);
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      StringBuffer stringBuffer = new StringBuffer();
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      this();
      while (true) {
        fileInputStream4 = fileInputStream7;
        fileInputStream5 = fileInputStream7;
        fileInputStream6 = fileInputStream7;
        String str1 = bufferedReader.readLine();
        if (str1 != null) {
          fileInputStream4 = fileInputStream7;
          fileInputStream5 = fileInputStream7;
          fileInputStream6 = fileInputStream7;
          stringBuffer.append(str1);
          continue;
        } 
        break;
      } 
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      String str = stringBuffer.toString();
      try {
        fileInputStream7.close();
      } catch (IOException iOException) {
        iOException.printStackTrace();
      } 
      return str;
    } catch (FileNotFoundException fileNotFoundException) {
      fileInputStream4 = fileInputStream6;
      fileNotFoundException.printStackTrace();
      if (fileInputStream6 != null)
        fileInputStream6.close(); 
    } catch (IOException iOException) {
      fileInputStream4 = fileInputStream5;
      iOException.printStackTrace();
      if (fileInputStream5 != null)
        try {
          fileInputStream5.close();
        } catch (IOException iOException1) {
          iOException1.printStackTrace();
        }  
    } finally {}
    return null;
  }
  
  private long getVersion(String paramString) {
    if (paramString == null) {
      Log.d("OplusRomUpdateHelper", "\tcontent is null");
      return -1L;
    } 
    StringReader stringReader1 = null, stringReader2 = null, stringReader3 = null;
    StringReader stringReader4 = stringReader3, stringReader5 = stringReader1, stringReader6 = stringReader2;
    try {
      XmlPullParser xmlPullParser = Xml.newPullParser();
      stringReader4 = stringReader3;
      stringReader5 = stringReader1;
      stringReader6 = stringReader2;
      StringReader stringReader8 = new StringReader();
      stringReader4 = stringReader3;
      stringReader5 = stringReader1;
      stringReader6 = stringReader2;
      this(paramString);
      StringReader stringReader7 = stringReader8;
      stringReader4 = stringReader7;
      stringReader5 = stringReader7;
      stringReader6 = stringReader7;
      xmlPullParser.setInput(stringReader7);
      stringReader4 = stringReader7;
      stringReader5 = stringReader7;
      stringReader6 = stringReader7;
      int i = xmlPullParser.getEventType();
      while (i != 1) {
        if (i == 2) {
          stringReader4 = stringReader7;
          stringReader5 = stringReader7;
          stringReader6 = stringReader7;
          String str1 = xmlPullParser.getName();
          stringReader4 = stringReader7;
          stringReader5 = stringReader7;
          stringReader6 = stringReader7;
          xmlPullParser.next();
          stringReader4 = stringReader7;
          stringReader5 = stringReader7;
          stringReader6 = stringReader7;
          String str2 = xmlPullParser.getText();
          stringReader4 = stringReader7;
          stringReader5 = stringReader7;
          stringReader6 = stringReader7;
          if ("version".equals(str1))
            if (str2 != null) {
              stringReader4 = stringReader7;
              stringReader5 = stringReader7;
              stringReader6 = stringReader7;
              stringReader7.close();
              stringReader4 = stringReader7;
              stringReader5 = stringReader7;
              stringReader6 = stringReader7;
              StringBuilder stringBuilder = new StringBuilder();
              stringReader4 = stringReader7;
              stringReader5 = stringReader7;
              stringReader6 = stringReader7;
              this();
              stringReader4 = stringReader7;
              stringReader5 = stringReader7;
              stringReader6 = stringReader7;
              stringBuilder.append("fileVersion = ");
              stringReader4 = stringReader7;
              stringReader5 = stringReader7;
              stringReader6 = stringReader7;
              stringBuilder.append(str2);
              stringReader4 = stringReader7;
              stringReader5 = stringReader7;
              stringReader6 = stringReader7;
              Log.d("OplusRomUpdateHelper", stringBuilder.toString());
              stringReader4 = stringReader7;
              stringReader5 = stringReader7;
              stringReader6 = stringReader7;
              i = Integer.parseInt(str2);
              long l = i;
              stringReader7.close();
              return l;
            }  
        } 
        stringReader4 = stringReader7;
        stringReader5 = stringReader7;
        stringReader6 = stringReader7;
        i = xmlPullParser.next();
      } 
      stringReader7.close();
    } catch (XmlPullParserException xmlPullParserException) {
      StringReader stringReader;
      stringReader4 = stringReader6;
      log("Got execption parsing permissions.", (Exception)xmlPullParserException);
      if (stringReader6 != null) {
        stringReader = stringReader6;
      } else {
        return -1L;
      } 
      stringReader.close();
    } catch (IOException iOException) {
      StringReader stringReader;
      stringReader4 = stringReader5;
      log("Got execption parsing permissions.", iOException);
      if (stringReader5 != null) {
        stringReader = stringReader5;
      } else {
        return -1L;
      } 
      stringReader.close();
    } finally {}
    return -1L;
  }
  
  public void parseContentFromXML(String paramString) {
    if (getUpdateInfo(true) != null)
      getUpdateInfo(true).parseContentFromXML(paramString); 
  }
  
  public void getUpdateFromProvider() {
    try {
      String str = getDataFromProvider();
      File file1 = new File();
      this(this.mDataFilePath);
      File file2 = new File();
      this(this.mSystemFilePath);
      if (str == null)
        return; 
      if (updateToLowerVersion(str))
        return; 
      long l = getVersion(str);
      if (l <= getVersion(readFromFile(file1)) || 
        l <= getVersion(readFromFile(file2))) {
        Log.d("OplusRomUpdateHelper", "getUpdateFromProvider version is older than current file verison");
        return;
      } 
      saveToFile(str, this.mDataFilePath);
      if (getUpdateInfo(false) == null)
        return; 
      getUpdateInfo(false).parseContentFromXML(str);
      setFlip();
      getUpdateInfo(false).clear();
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  private boolean updateToLowerVersion(String paramString) {
    UpdateInfo updateInfo = getUpdateInfo(true);
    if (updateInfo != null && updateInfo.updateToLowerVersion(paramString)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("updateToLowerVersion true, ");
      stringBuilder.append(updateInfo.hashCode());
      Log.d("OplusRomUpdateHelper", stringBuilder.toString());
      return true;
    } 
    return false;
  }
  
  protected boolean insertValueInList(int paramInt, String paramString) {
    if (getUpdateInfo(false).clone(getUpdateInfo(true)) && 
      getUpdateInfo(false).insert(paramInt, paramString)) {
      setFlip();
      getUpdateInfo(false).clear();
      return true;
    } 
    log("Failed to insert!");
    return false;
  }
  
  public void dump() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("which2use = ");
    stringBuilder.append(this.which2use);
    log(stringBuilder.toString());
    this.mUpdateInfo1.dump();
    this.mUpdateInfo2.dump();
  }
  
  public void log(String paramString) {
    if (!DEBUG)
      return; 
    Log.d("OplusRomUpdateHelper", paramString);
  }
  
  public void log(String paramString, Exception paramException) {
    if (!DEBUG)
      return; 
    Log.d("OplusRomUpdateHelper", paramString);
  }
}
