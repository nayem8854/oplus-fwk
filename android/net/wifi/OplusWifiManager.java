package android.net.wifi;

import android.content.Context;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import android.util.SparseArray;
import com.android.internal.util.AsyncChannel;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import oplus.net.wifi.HotspotClient;

public class OplusWifiManager implements IOplusWifiManager {
  private int mListenerKey = 1;
  
  private final SparseArray mListenerMap = new SparseArray();
  
  private final Object mListenerMapLock = new Object();
  
  private static final Object sServiceHandlerDispatchLock = new Object();
  
  private List<ActionListener> mListeners = new ArrayList<>();
  
  private static final int BASE = 151552;
  
  public static final int CANCEL_WPS = 151566;
  
  public static final int CANCEL_WPS_FAILED = 151567;
  
  public static final int CANCEL_WPS_SUCCEDED = 151568;
  
  public static final String EXTRA_HOTSPOT_CLIENTS_NUM = "HotspotClientNum";
  
  private static final int INVALID_KEY = 0;
  
  public static final long OPLUS_WIFI_FEATURE_AUTORECONNECT = 128L;
  
  public static final long OPLUS_WIFI_FEATURE_DBS = 16L;
  
  public static final long OPLUS_WIFI_FEATURE_DUALSTA = 8L;
  
  public static final long OPLUS_WIFI_FEATURE_INFRA = 1L;
  
  public static final long OPLUS_WIFI_FEATURE_LIMITSPEED = 64L;
  
  public static final long OPLUS_WIFI_FEATURE_SLA = 4L;
  
  public static final long OPLUS_WIFI_FEATURE_WIFI6 = 32L;
  
  public static final int START_WPS = 151562;
  
  public static final int START_WPS_SUCCEEDED = 151563;
  
  private static final String TAG = "OplusWifiManager";
  
  public static final String WIFI_HOTSPOT_CLIENTS_CHANGED_ACTION = "android.net.wifi.WIFI_HOTSPOT_CLIENTS_CHANGED";
  
  public static final int WPS_COMPLETED = 151565;
  
  public static final int WPS_FAILED = 151564;
  
  private AsyncChannel mAsyncChannel;
  
  private CountDownLatch mConnected;
  
  private Context mContext;
  
  private IBinder mService;
  
  public OplusWifiManager(Context paramContext) {
    this.mContext = paramContext;
    this.mService = ServiceManager.getService("wifi");
  }
  
  public long getOplusSupportedFeatures() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10002, parcel1, parcel2, 0);
      parcel2.readException();
      return Long.valueOf(parcel2.readLong()).longValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean isFeatureSupported(long paramLong) throws RemoteException {
    boolean bool;
    if ((getOplusSupportedFeatures() & paramLong) == paramLong) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean startWifiSharing(WifiConfiguration paramWifiConfiguration) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      paramWifiConfiguration.writeToParcel(parcel1, 0);
      this.mService.transact(10003, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean stopWifiSharing() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10004, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean setWifiClosedByUser(boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10043, parcel1, parcel2, 0);
      parcel2.readException();
      paramBoolean = Boolean.valueOf(parcel2.readString()).booleanValue();
      return paramBoolean;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public WifiConfiguration getWifiSharingConfiguration() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10005, parcel1, parcel2, 0);
      parcel2.readException();
      Parcelable.Creator<WifiConfiguration> creator = (Parcelable.Creator)getStaticFieldValue(WifiConfiguration.class, "CREATOR");
      return creator.createFromParcel(parcel2);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean setWifiSharingConfiguration(WifiConfiguration paramWifiConfiguration) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      paramWifiConfiguration.writeToParcel(parcel1, 0);
      this.mService.transact(10006, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean setSlaAppState(String paramString, boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(paramString);
      parcel1.writeString(String.valueOf(paramBoolean));
      this.mService.transact(10007, parcel1, parcel2, 0);
      parcel2.readException();
      paramBoolean = Boolean.valueOf(parcel2.readString()).booleanValue();
      return paramBoolean;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean getSlaAppState(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(paramString);
      this.mService.transact(10008, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public String[] getAllSlaAppsAndStates() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10009, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      String[] arrayOfString = new String[i];
      parcel2.readStringArray(arrayOfString);
      return arrayOfString;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public String[] getAllSlaAcceleratedApps() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10010, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      String[] arrayOfString = new String[i];
      parcel2.readStringArray(arrayOfString);
      return arrayOfString;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public String[] getAllDualStaApps() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10011, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      String[] arrayOfString = new String[i];
      parcel2.readStringArray(arrayOfString);
      return arrayOfString;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int enableDualSta() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10012, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readInt();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int enableDualStaByForce(boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(String.valueOf(paramBoolean));
      this.mService.transact(10013, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readInt();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void disableDualSta() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10014, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  private static Object getStaticFieldValue(Class<?> paramClass, String paramString) {
    NoSuchFieldException noSuchFieldException = null;
    try {
      Field field = paramClass.getField(paramString);
      field.setAccessible(true);
      object = field.get(null);
    } catch (NoSuchFieldException|IllegalAccessException object) {
      object.printStackTrace();
      object = noSuchFieldException;
    } 
    return object;
  }
  
  public WifiInfo getOplusSta2ConnectionInfo() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(this.mContext.getOpPackageName());
      this.mService.transact(10015, parcel1, parcel2, 0);
      parcel2.readException();
      Parcelable.Creator<WifiInfo> creator = (Parcelable.Creator)getStaticFieldValue(WifiInfo.class, "CREATOR");
      return creator.createFromParcel(parcel2);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public String getOplusSta2CurConfigKey() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10016, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readString();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean isDualStaSupported() throws RemoteException {
    try {
      isFeatureSupported(8L);
      return false;
    } finally {}
  }
  
  public boolean getDualStaSwitchDefault() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10082, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void clearWifiOCloudData(boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(String.valueOf(paramBoolean));
      this.mService.transact(10017, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<String> getWifiOCloudData(boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(String.valueOf(paramBoolean));
      this.mService.transact(10018, parcel1, parcel2, 0);
      parcel2.readException();
      ArrayList<String> arrayList = new ArrayList();
      this();
      parcel2.readStringList(arrayList);
      return arrayList;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void updateGlobalId(int paramInt, String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeInt(paramInt);
      parcel1.writeString(paramString);
      this.mService.transact(10019, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void removeNetworkByGlobalId(String paramString1, String paramString2, boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      parcel1.writeString(String.valueOf(paramBoolean));
      this.mService.transact(10020, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setDirtyFlag(String paramString, boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(paramString);
      parcel1.writeString(String.valueOf(paramBoolean));
      this.mService.transact(10021, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean hasOCloudDirtyData() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10022, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void dealWithCloudBackupResult(List<String> paramList, String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeStringList(paramList);
      parcel1.writeString(paramString);
      this.mService.transact(10051, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void dealWithCloudRecoveryData(List<String> paramList, String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeStringList(paramList);
      parcel1.writeString(paramString);
      this.mService.transact(10052, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean checkPasspointCAExist(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(paramString);
      this.mService.transact(10031, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean checkPasspointXMLCAExpired(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(paramString);
      this.mService.transact(10032, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<ScanResult> passpointANQPScanResults(List<ScanResult> paramList) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeTypedList(paramList);
      this.mService.transact(10035, parcel1, parcel2, 0);
      parcel2.readException();
      paramList = new ArrayList<>();
      super();
      parcel2.readTypedList(paramList, ScanResult.CREATOR);
      return paramList;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean checkInternalPasspointPresetProvider(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(paramString);
      this.mService.transact(10036, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean checkFWKSupportPasspoint() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10037, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean setPasspointCertifiedState(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(paramString);
      this.mService.transact(10038, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public String getPasspointUserName(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(paramString);
      this.mService.transact(10039, parcel1, parcel2, 0);
      parcel2.readException();
      paramString = parcel2.readString();
      return paramString;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean startRxSensTest(WifiConfiguration paramWifiConfiguration, String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      paramWifiConfiguration.writeToParcel(parcel1, 0);
      parcel1.writeString(paramString);
      this.mService.transact(10041, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void stopRxSensTest() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10042, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setNetworkCaptiveState(boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(String.valueOf(paramBoolean));
      this.mService.transact(10023, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int getPortalLoginType() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10024, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readInt();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void startWps(WpsInfo paramWpsInfo, WifiManager.WpsCallback paramWpsCallback) throws RemoteException {
    if (paramWpsInfo != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("startWps, enter, config setup is ");
      stringBuilder.append(paramWpsInfo.setup);
      Log.e("OplusWifiManager", stringBuilder.toString());
      getChannel().sendMessage(151562, 0, putListener(paramWpsCallback), paramWpsInfo);
      return;
    } 
    throw new IllegalArgumentException("config cannot be null");
  }
  
  public void cancelWps(WifiManager.WpsCallback paramWpsCallback) throws RemoteException {
    Log.e("OplusWifiManager", "cancelWps");
    getChannel().sendMessage(151566, 0, putListener(paramWpsCallback));
  }
  
  private Messenger getOplusWifiTransactionHelperMessenger() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(String.valueOf(this.mContext.getOpPackageName()));
      this.mService.transact(10044, parcel1, parcel2, 0);
      parcel2.readException();
      Parcelable.Creator<Messenger> creator = (Parcelable.Creator)getStaticFieldValue(Messenger.class, "CREATOR");
      return creator.createFromParcel(parcel2);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  class ActionListener {
    public abstract void onFailure(int param1Int);
    
    public abstract void onSuccess();
  }
  
  private class ServiceHandler extends Handler {
    final OplusWifiManager this$0;
    
    ServiceHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      synchronized (OplusWifiManager.sServiceHandlerDispatchLock) {
        dispatchMessageToListeners(param1Message);
        return;
      } 
    }
    
    private void dispatchMessageToListeners(Message param1Message) {
      Object object = OplusWifiManager.this.removeListener(param1Message.arg2);
      switch (param1Message.what) {
        default:
          return;
        case 151568:
          if (object != null)
            ((WifiManager.WpsCallback)object).onSucceeded(); 
        case 151567:
          if (object != null)
            ((WifiManager.WpsCallback)object).onFailed(param1Message.arg1); 
        case 151565:
          if (object != null)
            ((WifiManager.WpsCallback)object).onSucceeded(); 
        case 151564:
          if (object != null)
            ((WifiManager.WpsCallback)object).onFailed(param1Message.arg1); 
        case 151563:
          if (object != null) {
            WpsResult wpsResult = (WpsResult)param1Message.obj;
            ((WifiManager.WpsCallback)object).onStarted(wpsResult.pin);
            synchronized (OplusWifiManager.this.mListenerMapLock) {
              OplusWifiManager.this.mListenerMap.put(param1Message.arg2, object);
            } 
          } 
        case 69636:
          Log.e("OplusWifiManager", "Channel connection lost");
          OplusWifiManager.access$202(OplusWifiManager.this, null);
          getLooper().quit();
        case 69632:
          break;
      } 
      if (param1Message.arg1 == 0) {
        OplusWifiManager.this.mAsyncChannel.sendMessage(69633);
      } else {
        Log.e("OplusWifiManager", "Failed to set up channel connection");
        OplusWifiManager.access$202(OplusWifiManager.this, null);
      } 
      OplusWifiManager.this.mConnected.countDown();
    }
  }
  
  private int putListener(Object paramObject) {
    if (paramObject == null)
      return 0; 
    synchronized (this.mListenerMapLock) {
      while (true) {
        int i = this.mListenerKey;
        this.mListenerKey = i + 1;
        if (i != 0) {
          this.mListenerMap.put(i, paramObject);
          return i;
        } 
      } 
    } 
  }
  
  private Object removeListener(int paramInt) {
    if (paramInt == 0)
      return null; 
    synchronized (this.mListenerMapLock) {
      Object object = this.mListenerMap.get(paramInt);
      this.mListenerMap.remove(paramInt);
      return object;
    } 
  }
  
  private AsyncChannel getChannel() throws RemoteException {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mAsyncChannel : Lcom/android/internal/util/AsyncChannel;
    //   6: ifnonnull -> 104
    //   9: aload_0
    //   10: invokespecial getOplusWifiTransactionHelperMessenger : ()Landroid/os/Messenger;
    //   13: astore_1
    //   14: aload_1
    //   15: ifnull -> 92
    //   18: new com/android/internal/util/AsyncChannel
    //   21: astore_2
    //   22: aload_2
    //   23: invokespecial <init> : ()V
    //   26: aload_0
    //   27: aload_2
    //   28: putfield mAsyncChannel : Lcom/android/internal/util/AsyncChannel;
    //   31: new java/util/concurrent/CountDownLatch
    //   34: astore_2
    //   35: aload_2
    //   36: iconst_1
    //   37: invokespecial <init> : (I)V
    //   40: aload_0
    //   41: aload_2
    //   42: putfield mConnected : Ljava/util/concurrent/CountDownLatch;
    //   45: new android/net/wifi/OplusWifiManager$ServiceHandler
    //   48: astore_2
    //   49: aload_2
    //   50: aload_0
    //   51: invokestatic getInstanceLooper : ()Landroid/os/Looper;
    //   54: invokespecial <init> : (Landroid/net/wifi/OplusWifiManager;Landroid/os/Looper;)V
    //   57: aload_0
    //   58: getfield mAsyncChannel : Lcom/android/internal/util/AsyncChannel;
    //   61: aload_0
    //   62: getfield mContext : Landroid/content/Context;
    //   65: aload_2
    //   66: aload_1
    //   67: invokevirtual connect : (Landroid/content/Context;Landroid/os/Handler;Landroid/os/Messenger;)V
    //   70: aload_0
    //   71: getfield mConnected : Ljava/util/concurrent/CountDownLatch;
    //   74: invokevirtual await : ()V
    //   77: goto -> 104
    //   80: astore_1
    //   81: ldc 'OplusWifiManager'
    //   83: ldc 'interrupted wait at init'
    //   85: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   88: pop
    //   89: goto -> 104
    //   92: new java/lang/IllegalStateException
    //   95: astore_1
    //   96: aload_1
    //   97: ldc 'getOplusWifiTransactionHelperMessenger() returned null! This is invalid.'
    //   99: invokespecial <init> : (Ljava/lang/String;)V
    //   102: aload_1
    //   103: athrow
    //   104: aload_0
    //   105: getfield mAsyncChannel : Lcom/android/internal/util/AsyncChannel;
    //   108: astore_1
    //   109: aload_0
    //   110: monitorexit
    //   111: aload_1
    //   112: areturn
    //   113: astore_1
    //   114: aload_0
    //   115: monitorexit
    //   116: aload_1
    //   117: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #957	-> 2
    //   #958	-> 9
    //   #959	-> 14
    //   #964	-> 18
    //   #965	-> 31
    //   #967	-> 45
    //   #968	-> 57
    //   #970	-> 70
    //   #973	-> 77
    //   #971	-> 80
    //   #972	-> 81
    //   #960	-> 92
    //   #975	-> 104
    //   #956	-> 113
    // Exception table:
    //   from	to	target	type
    //   2	9	113	finally
    //   9	14	113	finally
    //   18	31	113	finally
    //   31	45	113	finally
    //   45	57	113	finally
    //   57	70	113	finally
    //   70	77	80	java/lang/InterruptedException
    //   70	77	113	finally
    //   81	89	113	finally
    //   92	104	113	finally
    //   104	109	113	finally
  }
  
  public boolean isP2p5GSupported() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10045, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean isSoftap5GSupported() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10046, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setBlockedHotspotClients(List<HotspotClient> paramList) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeTypedList(paramList);
      this.mService.transact(10053, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setAllowedHotspotClients(List<HotspotClient> paramList) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeTypedList(paramList);
      this.mService.transact(10054, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<HotspotClient> getBlockedHotspotClients() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10055, parcel1, parcel2, 0);
      parcel2.readException();
      ArrayList<?> arrayList = new ArrayList();
      this();
      parcel2.readTypedList(arrayList, HotspotClient.CREATOR);
      return (List)arrayList;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<HotspotClient> getAllowedHotspotClients() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10056, parcel1, parcel2, 0);
      parcel2.readException();
      ArrayList<?> arrayList = new ArrayList();
      this();
      parcel2.readTypedList(arrayList, HotspotClient.CREATOR);
      return (List)arrayList;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<HotspotClient> getConnectedHotspotClients() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10057, parcel1, parcel2, 0);
      parcel2.readException();
      ArrayList<?> arrayList = new ArrayList();
      this();
      parcel2.readTypedList(arrayList, HotspotClient.CREATOR);
      return (List)arrayList;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void notifyGameModeState(boolean paramBoolean, String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(String.valueOf(paramBoolean));
      parcel1.writeString(paramString);
      this.mService.transact(10058, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void notifyGameLatency(String paramString1, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      this.mService.transact(10077, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void notifyGameHighTemperature(int paramInt, String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeInt(paramInt);
      parcel1.writeString(paramString);
      this.mService.transact(10083, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean setWifiRestrictionList(String paramString1, List<String> paramList, String paramString2) {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(paramString1);
      parcel1.writeStringList(paramList);
      parcel1.writeString(paramString2);
      this.mService.transact(10059, parcel1, parcel2, 0);
      parcel2.readException();
      boolean bool = Boolean.valueOf(parcel2.readString()).booleanValue();
      parcel1.recycle();
      parcel2.recycle();
      return bool;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } finally {}
    parcel1.recycle();
    parcel2.recycle();
    throw paramString1;
  }
  
  public List<String> getWifiRestrictionList(String paramString1, String paramString2) {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      this.mService.transact(10060, parcel1, parcel2, 0);
      parcel2.readException();
      ArrayList<String> arrayList = new ArrayList();
      this();
      parcel2.readStringList(arrayList);
      parcel1.recycle();
      parcel2.recycle();
      return arrayList;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } finally {}
    parcel1.recycle();
    parcel2.recycle();
    throw paramString1;
  }
  
  public boolean removeFromRestrictionList(String paramString1, List<String> paramList, String paramString2) {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(paramString1);
      parcel1.writeStringList(paramList);
      parcel1.writeString(paramString2);
      this.mService.transact(10061, parcel1, parcel2, 0);
      parcel2.readException();
      boolean bool = Boolean.valueOf(parcel2.readString()).booleanValue();
      parcel1.recycle();
      parcel2.recycle();
      return bool;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } finally {}
    parcel1.recycle();
    parcel2.recycle();
    throw paramString1;
  }
  
  public boolean setWifiAutoConnectionDisabled(boolean paramBoolean) {
    Exception exception;
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(String.valueOf(paramBoolean));
      this.mService.transact(10062, parcel1, parcel2, 0);
      parcel2.readException();
      parcel1.recycle();
      parcel2.recycle();
      return true;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } finally {}
    parcel1.recycle();
    parcel2.recycle();
    throw exception;
  }
  
  public boolean isWifiAutoConnectionDisabled() {
    Exception exception;
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10063, parcel1, parcel2, 0);
      parcel2.readException();
      boolean bool = Boolean.valueOf(parcel2.readString()).booleanValue();
      parcel1.recycle();
      parcel2.recycle();
      return bool;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } finally {}
    parcel1.recycle();
    parcel2.recycle();
    throw exception;
  }
  
  public int startSnifferMode(int paramInt1, int paramInt2, int paramInt3, int paramInt4) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      parcel1.writeInt(paramInt3);
      parcel1.writeInt(paramInt4);
      this.mService.transact(10064, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt1 = parcel2.readInt();
      return paramInt1;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int stopSnifferMode() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10065, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readInt();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int getSnifferState() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10066, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readInt();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int startFTMMode() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10067, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readInt();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int stopFTMMode() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10068, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readInt();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int sendFTMCommand(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(paramString);
      this.mService.transact(10068, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readInt();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int getFTMState() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10069, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readInt();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void removeHeIeFromProbeRequest(boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeBoolean(paramBoolean);
      this.mService.transact(10071, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setP2pPowerSave(boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeBoolean(paramBoolean);
      this.mService.transact(10072, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int getDBSCapacity() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    Log.d("OplusWifiManager", "getDBSCapacity");
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10073, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("getDBSCapacity cap = ");
      stringBuilder.append(i);
      Log.d("OplusWifiManager", stringBuilder.toString());
      return i;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int getPHYCapacity(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeInt(paramInt);
      this.mService.transact(10074, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      return paramInt;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int[] getSupportedChannels(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeInt(paramInt);
      this.mService.transact(10075, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.createIntArray();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int[] getAvoidChannels() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      this.mService.transact(10076, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.createIntArray();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void addActionListener(ActionListener paramActionListener) {
    this.mListeners.add(paramActionListener);
  }
  
  public List<ActionListener> getListener() {
    return this.mListeners;
  }
  
  private void removeActionListener(int paramInt) {
    this.mListeners.remove(paramInt);
  }
  
  public boolean isDualStaSupportedForAPP(String paramString) throws RemoteException {
    boolean bool = false;
    if (paramString != null) {
      bool = isDualStaSupported();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Ocs or ThirdAPP get isSupportedForAPP:");
      stringBuilder.append(bool);
      stringBuilder.append(" for ");
      stringBuilder.append(paramString);
      Log.d("OplusWifiManager", stringBuilder.toString());
    } 
    return bool;
  }
  
  public boolean getDualStaReadyStateForAPP(String paramString) throws RemoteException {
    Log.d("OplusWifiManager", "yangjiang WiFi Manager begin");
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(paramString);
      this.mService.transact(10078, parcel1, parcel2, 0);
      parcel2.readException();
      boolean bool = Boolean.valueOf(parcel2.readString()).booleanValue();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("OCS or ThirdAPP get DualSta state:");
      stringBuilder.append(bool);
      Log.d("OplusWifiManager", stringBuilder.toString());
      return bool;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int requestToEnableSta2ByAPP(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeString(paramString);
      this.mService.transact(10079, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("OCS or ThirdAPP requestToEnableSta2ByAPP:");
      stringBuilder.append(i);
      Log.d("OplusWifiManager", stringBuilder.toString());
      return i;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean requestToReleaseSta2ByAPP(int paramInt1, int paramInt2, String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      Log.d("OplusWifiManager", "yangjiang WiFi Manager In requestToReleaseSta2ByAPP");
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      parcel1.writeString(paramString);
      this.mService.transact(10081, parcel1, parcel2, 0);
      parcel2.readException();
      boolean bool = Boolean.valueOf(parcel2.readString()).booleanValue();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("yangjiang requestToReleaseSta2ByAPP result in OplusWifiManager:");
      stringBuilder.append(bool);
      Log.d("OplusWifiManager", stringBuilder.toString());
      return bool;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void addAuthResultInfo(int paramInt1, int paramInt2, int paramInt3, String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiManager");
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      parcel1.writeInt(paramInt3);
      parcel1.writeString(paramString);
      this.mService.transact(10080, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
}
