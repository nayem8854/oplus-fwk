package android.net.wifi.nl80211;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IWifiScannerImpl extends IInterface {
  public static final int SCAN_TYPE_DEFAULT = -1;
  
  public static final int SCAN_TYPE_HIGH_ACCURACY = 2;
  
  public static final int SCAN_TYPE_LOW_POWER = 1;
  
  public static final int SCAN_TYPE_LOW_SPAN = 0;
  
  void abortScan() throws RemoteException;
  
  NativeScanResult[] getPnoScanResults() throws RemoteException;
  
  NativeScanResult[] getScanResults() throws RemoteException;
  
  boolean scan(SingleScanSettings paramSingleScanSettings) throws RemoteException;
  
  boolean startPnoScan(PnoSettings paramPnoSettings) throws RemoteException;
  
  boolean stopPnoScan() throws RemoteException;
  
  void subscribePnoScanEvents(IPnoScanEvent paramIPnoScanEvent) throws RemoteException;
  
  void subscribeScanEvents(IScanEvent paramIScanEvent) throws RemoteException;
  
  void unsubscribePnoScanEvents() throws RemoteException;
  
  void unsubscribeScanEvents() throws RemoteException;
  
  class Default implements IWifiScannerImpl {
    public NativeScanResult[] getScanResults() throws RemoteException {
      return null;
    }
    
    public NativeScanResult[] getPnoScanResults() throws RemoteException {
      return null;
    }
    
    public boolean scan(SingleScanSettings param1SingleScanSettings) throws RemoteException {
      return false;
    }
    
    public void subscribeScanEvents(IScanEvent param1IScanEvent) throws RemoteException {}
    
    public void unsubscribeScanEvents() throws RemoteException {}
    
    public void subscribePnoScanEvents(IPnoScanEvent param1IPnoScanEvent) throws RemoteException {}
    
    public void unsubscribePnoScanEvents() throws RemoteException {}
    
    public boolean startPnoScan(PnoSettings param1PnoSettings) throws RemoteException {
      return false;
    }
    
    public boolean stopPnoScan() throws RemoteException {
      return false;
    }
    
    public void abortScan() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWifiScannerImpl {
    private static final String DESCRIPTOR = "android.net.wifi.nl80211.IWifiScannerImpl";
    
    static final int TRANSACTION_abortScan = 10;
    
    static final int TRANSACTION_getPnoScanResults = 2;
    
    static final int TRANSACTION_getScanResults = 1;
    
    static final int TRANSACTION_scan = 3;
    
    static final int TRANSACTION_startPnoScan = 8;
    
    static final int TRANSACTION_stopPnoScan = 9;
    
    static final int TRANSACTION_subscribePnoScanEvents = 6;
    
    static final int TRANSACTION_subscribeScanEvents = 4;
    
    static final int TRANSACTION_unsubscribePnoScanEvents = 7;
    
    static final int TRANSACTION_unsubscribeScanEvents = 5;
    
    public Stub() {
      attachInterface(this, "android.net.wifi.nl80211.IWifiScannerImpl");
    }
    
    public static IWifiScannerImpl asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.wifi.nl80211.IWifiScannerImpl");
      if (iInterface != null && iInterface instanceof IWifiScannerImpl)
        return (IWifiScannerImpl)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 10:
          return "abortScan";
        case 9:
          return "stopPnoScan";
        case 8:
          return "startPnoScan";
        case 7:
          return "unsubscribePnoScanEvents";
        case 6:
          return "subscribePnoScanEvents";
        case 5:
          return "unsubscribeScanEvents";
        case 4:
          return "subscribeScanEvents";
        case 3:
          return "scan";
        case 2:
          return "getPnoScanResults";
        case 1:
          break;
      } 
      return "getScanResults";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        IPnoScanEvent iPnoScanEvent;
        IScanEvent iScanEvent;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 10:
            param1Parcel1.enforceInterface("android.net.wifi.nl80211.IWifiScannerImpl");
            abortScan();
            param1Parcel2.writeNoException();
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.net.wifi.nl80211.IWifiScannerImpl");
            bool = stopPnoScan();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.net.wifi.nl80211.IWifiScannerImpl");
            if (param1Parcel1.readInt() != 0) {
              PnoSettings pnoSettings = PnoSettings.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            bool = startPnoScan((PnoSettings)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.net.wifi.nl80211.IWifiScannerImpl");
            unsubscribePnoScanEvents();
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.net.wifi.nl80211.IWifiScannerImpl");
            iPnoScanEvent = IPnoScanEvent.Stub.asInterface(param1Parcel1.readStrongBinder());
            subscribePnoScanEvents(iPnoScanEvent);
            return true;
          case 5:
            iPnoScanEvent.enforceInterface("android.net.wifi.nl80211.IWifiScannerImpl");
            unsubscribeScanEvents();
            return true;
          case 4:
            iPnoScanEvent.enforceInterface("android.net.wifi.nl80211.IWifiScannerImpl");
            iScanEvent = IScanEvent.Stub.asInterface(iPnoScanEvent.readStrongBinder());
            subscribeScanEvents(iScanEvent);
            return true;
          case 3:
            iScanEvent.enforceInterface("android.net.wifi.nl80211.IWifiScannerImpl");
            if (iScanEvent.readInt() != 0) {
              SingleScanSettings singleScanSettings = SingleScanSettings.CREATOR.createFromParcel((Parcel)iScanEvent);
            } else {
              iScanEvent = null;
            } 
            bool = scan((SingleScanSettings)iScanEvent);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 2:
            iScanEvent.enforceInterface("android.net.wifi.nl80211.IWifiScannerImpl");
            arrayOfNativeScanResult = getPnoScanResults();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray(arrayOfNativeScanResult, 1);
            return true;
          case 1:
            break;
        } 
        arrayOfNativeScanResult.enforceInterface("android.net.wifi.nl80211.IWifiScannerImpl");
        NativeScanResult[] arrayOfNativeScanResult = getScanResults();
        param1Parcel2.writeNoException();
        param1Parcel2.writeTypedArray(arrayOfNativeScanResult, 1);
        return true;
      } 
      param1Parcel2.writeString("android.net.wifi.nl80211.IWifiScannerImpl");
      return true;
    }
    
    private static class Proxy implements IWifiScannerImpl {
      public static IWifiScannerImpl sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.wifi.nl80211.IWifiScannerImpl";
      }
      
      public NativeScanResult[] getScanResults() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IWifiScannerImpl");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IWifiScannerImpl.Stub.getDefaultImpl() != null)
            return IWifiScannerImpl.Stub.getDefaultImpl().getScanResults(); 
          parcel2.readException();
          return parcel2.<NativeScanResult>createTypedArray(NativeScanResult.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NativeScanResult[] getPnoScanResults() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IWifiScannerImpl");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IWifiScannerImpl.Stub.getDefaultImpl() != null)
            return IWifiScannerImpl.Stub.getDefaultImpl().getPnoScanResults(); 
          parcel2.readException();
          return parcel2.<NativeScanResult>createTypedArray(NativeScanResult.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean scan(SingleScanSettings param2SingleScanSettings) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IWifiScannerImpl");
          boolean bool1 = true;
          if (param2SingleScanSettings != null) {
            parcel1.writeInt(1);
            param2SingleScanSettings.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IWifiScannerImpl.Stub.getDefaultImpl() != null) {
            bool1 = IWifiScannerImpl.Stub.getDefaultImpl().scan(param2SingleScanSettings);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void subscribeScanEvents(IScanEvent param2IScanEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.net.wifi.nl80211.IWifiScannerImpl");
          if (param2IScanEvent != null) {
            iBinder = param2IScanEvent.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IWifiScannerImpl.Stub.getDefaultImpl() != null) {
            IWifiScannerImpl.Stub.getDefaultImpl().subscribeScanEvents(param2IScanEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void unsubscribeScanEvents() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.wifi.nl80211.IWifiScannerImpl");
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IWifiScannerImpl.Stub.getDefaultImpl() != null) {
            IWifiScannerImpl.Stub.getDefaultImpl().unsubscribeScanEvents();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void subscribePnoScanEvents(IPnoScanEvent param2IPnoScanEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.net.wifi.nl80211.IWifiScannerImpl");
          if (param2IPnoScanEvent != null) {
            iBinder = param2IPnoScanEvent.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IWifiScannerImpl.Stub.getDefaultImpl() != null) {
            IWifiScannerImpl.Stub.getDefaultImpl().subscribePnoScanEvents(param2IPnoScanEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void unsubscribePnoScanEvents() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.wifi.nl80211.IWifiScannerImpl");
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && IWifiScannerImpl.Stub.getDefaultImpl() != null) {
            IWifiScannerImpl.Stub.getDefaultImpl().unsubscribePnoScanEvents();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean startPnoScan(PnoSettings param2PnoSettings) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IWifiScannerImpl");
          boolean bool1 = true;
          if (param2PnoSettings != null) {
            parcel1.writeInt(1);
            param2PnoSettings.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool2 && IWifiScannerImpl.Stub.getDefaultImpl() != null) {
            bool1 = IWifiScannerImpl.Stub.getDefaultImpl().startPnoScan(param2PnoSettings);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean stopPnoScan() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IWifiScannerImpl");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IWifiScannerImpl.Stub.getDefaultImpl() != null) {
            bool1 = IWifiScannerImpl.Stub.getDefaultImpl().stopPnoScan();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void abortScan() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IWifiScannerImpl");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IWifiScannerImpl.Stub.getDefaultImpl() != null) {
            IWifiScannerImpl.Stub.getDefaultImpl().abortScan();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IWifiScannerImpl param1IWifiScannerImpl) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWifiScannerImpl != null) {
          Proxy.sDefaultImpl = param1IWifiScannerImpl;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWifiScannerImpl getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
