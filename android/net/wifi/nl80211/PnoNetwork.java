package android.net.wifi.nl80211;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.Objects;

@SystemApi
public final class PnoNetwork implements Parcelable {
  public boolean isHidden() {
    return this.mIsHidden;
  }
  
  public void setHidden(boolean paramBoolean) {
    this.mIsHidden = paramBoolean;
  }
  
  public byte[] getSsid() {
    return this.mSsid;
  }
  
  public void setSsid(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null) {
      this.mSsid = paramArrayOfbyte;
      return;
    } 
    throw new IllegalArgumentException("null argument");
  }
  
  public int[] getFrequenciesMhz() {
    return this.mFrequencies;
  }
  
  public void setFrequenciesMhz(int[] paramArrayOfint) {
    if (paramArrayOfint != null) {
      this.mFrequencies = paramArrayOfint;
      return;
    } 
    throw new IllegalArgumentException("null argument");
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof PnoNetwork))
      return false; 
    paramObject = paramObject;
    if (Arrays.equals(this.mSsid, ((PnoNetwork)paramObject).mSsid)) {
      int[] arrayOfInt1 = this.mFrequencies, arrayOfInt2 = ((PnoNetwork)paramObject).mFrequencies;
      if (Arrays.equals(arrayOfInt1, arrayOfInt2) && this.mIsHidden == ((PnoNetwork)paramObject).mIsHidden)
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    boolean bool = this.mIsHidden;
    byte[] arrayOfByte = this.mSsid;
    int i = Arrays.hashCode(arrayOfByte), arrayOfInt[] = this.mFrequencies;
    int j = Arrays.hashCode(arrayOfInt);
    return Objects.hash(new Object[] { Boolean.valueOf(bool), Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mIsHidden);
    paramParcel.writeByteArray(this.mSsid);
    paramParcel.writeIntArray(this.mFrequencies);
  }
  
  public static final Parcelable.Creator<PnoNetwork> CREATOR = new Parcelable.Creator<PnoNetwork>() {
      public PnoNetwork createFromParcel(Parcel param1Parcel) {
        boolean bool;
        PnoNetwork pnoNetwork = new PnoNetwork();
        if (param1Parcel.readInt() != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        PnoNetwork.access$002(pnoNetwork, bool);
        PnoNetwork.access$102(pnoNetwork, param1Parcel.createByteArray());
        if (pnoNetwork.mSsid == null)
          PnoNetwork.access$102(pnoNetwork, new byte[0]); 
        PnoNetwork.access$202(pnoNetwork, param1Parcel.createIntArray());
        if (pnoNetwork.mFrequencies == null)
          PnoNetwork.access$202(pnoNetwork, new int[0]); 
        return pnoNetwork;
      }
      
      public PnoNetwork[] newArray(int param1Int) {
        return new PnoNetwork[param1Int];
      }
    };
  
  private int[] mFrequencies;
  
  private boolean mIsHidden;
  
  private byte[] mSsid;
}
