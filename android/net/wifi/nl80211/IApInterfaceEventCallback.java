package android.net.wifi.nl80211;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IApInterfaceEventCallback extends IInterface {
  public static final int BANDWIDTH_160 = 6;
  
  public static final int BANDWIDTH_20 = 2;
  
  public static final int BANDWIDTH_20_NOHT = 1;
  
  public static final int BANDWIDTH_40 = 3;
  
  public static final int BANDWIDTH_80 = 4;
  
  public static final int BANDWIDTH_80P80 = 5;
  
  public static final int BANDWIDTH_INVALID = 0;
  
  void onConnectedClientsChanged(NativeWifiClient paramNativeWifiClient, boolean paramBoolean) throws RemoteException;
  
  void onSoftApChannelSwitched(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements IApInterfaceEventCallback {
    public void onConnectedClientsChanged(NativeWifiClient param1NativeWifiClient, boolean param1Boolean) throws RemoteException {}
    
    public void onSoftApChannelSwitched(int param1Int1, int param1Int2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IApInterfaceEventCallback {
    private static final String DESCRIPTOR = "android.net.wifi.nl80211.IApInterfaceEventCallback";
    
    static final int TRANSACTION_onConnectedClientsChanged = 1;
    
    static final int TRANSACTION_onSoftApChannelSwitched = 2;
    
    public Stub() {
      attachInterface(this, "android.net.wifi.nl80211.IApInterfaceEventCallback");
    }
    
    public static IApInterfaceEventCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.wifi.nl80211.IApInterfaceEventCallback");
      if (iInterface != null && iInterface instanceof IApInterfaceEventCallback)
        return (IApInterfaceEventCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onSoftApChannelSwitched";
      } 
      return "onConnectedClientsChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.net.wifi.nl80211.IApInterfaceEventCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("android.net.wifi.nl80211.IApInterfaceEventCallback");
        param1Int2 = param1Parcel1.readInt();
        param1Int1 = param1Parcel1.readInt();
        onSoftApChannelSwitched(param1Int2, param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.net.wifi.nl80211.IApInterfaceEventCallback");
      if (param1Parcel1.readInt() != 0) {
        NativeWifiClient nativeWifiClient = NativeWifiClient.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      onConnectedClientsChanged((NativeWifiClient)param1Parcel2, bool);
      return true;
    }
    
    private static class Proxy implements IApInterfaceEventCallback {
      public static IApInterfaceEventCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.wifi.nl80211.IApInterfaceEventCallback";
      }
      
      public void onConnectedClientsChanged(NativeWifiClient param2NativeWifiClient, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.wifi.nl80211.IApInterfaceEventCallback");
          boolean bool = false;
          if (param2NativeWifiClient != null) {
            parcel.writeInt(1);
            param2NativeWifiClient.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool1 && IApInterfaceEventCallback.Stub.getDefaultImpl() != null) {
            IApInterfaceEventCallback.Stub.getDefaultImpl().onConnectedClientsChanged(param2NativeWifiClient, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSoftApChannelSwitched(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.wifi.nl80211.IApInterfaceEventCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IApInterfaceEventCallback.Stub.getDefaultImpl() != null) {
            IApInterfaceEventCallback.Stub.getDefaultImpl().onSoftApChannelSwitched(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IApInterfaceEventCallback param1IApInterfaceEventCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IApInterfaceEventCallback != null) {
          Proxy.sDefaultImpl = param1IApInterfaceEventCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IApInterfaceEventCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
