package android.net.wifi.nl80211;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IScanEvent extends IInterface {
  void OnScanFailed() throws RemoteException;
  
  void OnScanResultReady() throws RemoteException;
  
  class Default implements IScanEvent {
    public void OnScanResultReady() throws RemoteException {}
    
    public void OnScanFailed() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IScanEvent {
    private static final String DESCRIPTOR = "android.net.wifi.nl80211.IScanEvent";
    
    static final int TRANSACTION_OnScanFailed = 2;
    
    static final int TRANSACTION_OnScanResultReady = 1;
    
    public Stub() {
      attachInterface(this, "android.net.wifi.nl80211.IScanEvent");
    }
    
    public static IScanEvent asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.wifi.nl80211.IScanEvent");
      if (iInterface != null && iInterface instanceof IScanEvent)
        return (IScanEvent)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "OnScanFailed";
      } 
      return "OnScanResultReady";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.net.wifi.nl80211.IScanEvent");
          return true;
        } 
        param1Parcel1.enforceInterface("android.net.wifi.nl80211.IScanEvent");
        OnScanFailed();
        return true;
      } 
      param1Parcel1.enforceInterface("android.net.wifi.nl80211.IScanEvent");
      OnScanResultReady();
      return true;
    }
    
    private static class Proxy implements IScanEvent {
      public static IScanEvent sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.wifi.nl80211.IScanEvent";
      }
      
      public void OnScanResultReady() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.wifi.nl80211.IScanEvent");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IScanEvent.Stub.getDefaultImpl() != null) {
            IScanEvent.Stub.getDefaultImpl().OnScanResultReady();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void OnScanFailed() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.wifi.nl80211.IScanEvent");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IScanEvent.Stub.getDefaultImpl() != null) {
            IScanEvent.Stub.getDefaultImpl().OnScanFailed();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IScanEvent param1IScanEvent) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IScanEvent != null) {
          Proxy.sDefaultImpl = param1IScanEvent;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IScanEvent getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
