package android.net.wifi.nl80211;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.util.Objects;

@SystemApi
public final class DeviceWiphyCapabilities implements Parcelable {
  private boolean m80211nSupported = false;
  
  private boolean m80211acSupported = false;
  
  private boolean m80211axSupported = false;
  
  private boolean mChannelWidth160MhzSupported = false;
  
  private boolean mChannelWidth80p80MhzSupported = false;
  
  private int mMaxNumberTxSpatialStreams = 1;
  
  private int mMaxNumberRxSpatialStreams = 1;
  
  public boolean isWifiStandardSupported(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 4) {
        if (paramInt != 5) {
          if (paramInt != 6) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("isWifiStandardSupported called with invalid standard: ");
            stringBuilder.append(paramInt);
            Log.e("DeviceWiphyCapabilities", stringBuilder.toString());
            return false;
          } 
          return this.m80211axSupported;
        } 
        return this.m80211acSupported;
      } 
      return this.m80211nSupported;
    } 
    return true;
  }
  
  public void setWifiStandardSupport(int paramInt, boolean paramBoolean) {
    if (paramInt != 4) {
      if (paramInt != 5) {
        if (paramInt != 6) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("setWifiStandardSupport called with invalid standard: ");
          stringBuilder.append(paramInt);
          Log.e("DeviceWiphyCapabilities", stringBuilder.toString());
        } else {
          this.m80211axSupported = paramBoolean;
        } 
      } else {
        this.m80211acSupported = paramBoolean;
      } 
    } else {
      this.m80211nSupported = paramBoolean;
    } 
  }
  
  public boolean isChannelWidthSupported(int paramInt) {
    boolean bool1 = true, bool2 = true;
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt != 4) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("isChannelWidthSupported called with invalid channel width: ");
              stringBuilder.append(paramInt);
              Log.e("DeviceWiphyCapabilities", stringBuilder.toString());
              return false;
            } 
            return this.mChannelWidth80p80MhzSupported;
          } 
          return this.mChannelWidth160MhzSupported;
        } 
        boolean bool3 = bool2;
        if (!this.m80211acSupported)
          if (this.m80211axSupported) {
            bool3 = bool2;
          } else {
            bool3 = false;
          }  
        return bool3;
      } 
      boolean bool = bool1;
      if (!this.m80211nSupported) {
        bool = bool1;
        if (!this.m80211acSupported)
          if (this.m80211axSupported) {
            bool = bool1;
          } else {
            bool = false;
          }  
      } 
      return bool;
    } 
    return true;
  }
  
  public void setChannelWidthSupported(int paramInt, boolean paramBoolean) {
    if (paramInt != 3) {
      if (paramInt != 4) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("setChannelWidthSupported called with Invalid channel width: ");
        stringBuilder.append(paramInt);
        Log.e("DeviceWiphyCapabilities", stringBuilder.toString());
      } else {
        this.mChannelWidth80p80MhzSupported = paramBoolean;
      } 
    } else {
      this.mChannelWidth160MhzSupported = paramBoolean;
    } 
  }
  
  public int getMaxNumberTxSpatialStreams() {
    return this.mMaxNumberTxSpatialStreams;
  }
  
  public void setMaxNumberTxSpatialStreams(int paramInt) {
    this.mMaxNumberTxSpatialStreams = paramInt;
  }
  
  public int getMaxNumberRxSpatialStreams() {
    return this.mMaxNumberRxSpatialStreams;
  }
  
  public void setMaxNumberRxSpatialStreams(int paramInt) {
    this.mMaxNumberRxSpatialStreams = paramInt;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof DeviceWiphyCapabilities))
      return false; 
    paramObject = paramObject;
    if (this.m80211nSupported != ((DeviceWiphyCapabilities)paramObject).m80211nSupported || this.m80211acSupported != ((DeviceWiphyCapabilities)paramObject).m80211acSupported || this.m80211axSupported != ((DeviceWiphyCapabilities)paramObject).m80211axSupported || this.mChannelWidth160MhzSupported != ((DeviceWiphyCapabilities)paramObject).mChannelWidth160MhzSupported || this.mChannelWidth80p80MhzSupported != ((DeviceWiphyCapabilities)paramObject).mChannelWidth80p80MhzSupported || this.mMaxNumberTxSpatialStreams != ((DeviceWiphyCapabilities)paramObject).mMaxNumberTxSpatialStreams || this.mMaxNumberRxSpatialStreams != ((DeviceWiphyCapabilities)paramObject).mMaxNumberRxSpatialStreams)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    boolean bool1 = this.m80211nSupported, bool2 = this.m80211acSupported, bool3 = this.m80211axSupported, bool4 = this.mChannelWidth160MhzSupported;
    boolean bool5 = this.mChannelWidth80p80MhzSupported;
    int i = this.mMaxNumberTxSpatialStreams;
    int j = this.mMaxNumberRxSpatialStreams;
    return Objects.hash(new Object[] { Boolean.valueOf(bool1), Boolean.valueOf(bool2), Boolean.valueOf(bool3), Boolean.valueOf(bool4), Boolean.valueOf(bool5), Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeBoolean(this.m80211nSupported);
    paramParcel.writeBoolean(this.m80211acSupported);
    paramParcel.writeBoolean(this.m80211axSupported);
    paramParcel.writeBoolean(this.mChannelWidth160MhzSupported);
    paramParcel.writeBoolean(this.mChannelWidth80p80MhzSupported);
    paramParcel.writeInt(this.mMaxNumberTxSpatialStreams);
    paramParcel.writeInt(this.mMaxNumberRxSpatialStreams);
  }
  
  public String toString() {
    String str2;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("m80211nSupported:");
    boolean bool = this.m80211nSupported;
    String str1 = "Yes";
    if (bool) {
      str2 = "Yes";
    } else {
      str2 = "No";
    } 
    stringBuilder.append(str2);
    stringBuilder.append("m80211acSupported:");
    if (this.m80211acSupported) {
      str2 = "Yes";
    } else {
      str2 = "No";
    } 
    stringBuilder.append(str2);
    stringBuilder.append("m80211axSupported:");
    if (this.m80211axSupported) {
      str2 = "Yes";
    } else {
      str2 = "No";
    } 
    stringBuilder.append(str2);
    stringBuilder.append("mChannelWidth160MhzSupported: ");
    if (this.mChannelWidth160MhzSupported) {
      str2 = "Yes";
    } else {
      str2 = "No";
    } 
    stringBuilder.append(str2);
    stringBuilder.append("mChannelWidth80p80MhzSupported: ");
    if (this.mChannelWidth80p80MhzSupported) {
      str2 = str1;
    } else {
      str2 = "No";
    } 
    stringBuilder.append(str2);
    stringBuilder.append("mMaxNumberTxSpatialStreams: ");
    stringBuilder.append(this.mMaxNumberTxSpatialStreams);
    stringBuilder.append("mMaxNumberRxSpatialStreams: ");
    stringBuilder.append(this.mMaxNumberRxSpatialStreams);
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<DeviceWiphyCapabilities> CREATOR = new Parcelable.Creator<DeviceWiphyCapabilities>() {
      public DeviceWiphyCapabilities createFromParcel(Parcel param1Parcel) {
        DeviceWiphyCapabilities deviceWiphyCapabilities = new DeviceWiphyCapabilities();
        DeviceWiphyCapabilities.access$002(deviceWiphyCapabilities, param1Parcel.readBoolean());
        DeviceWiphyCapabilities.access$102(deviceWiphyCapabilities, param1Parcel.readBoolean());
        DeviceWiphyCapabilities.access$202(deviceWiphyCapabilities, param1Parcel.readBoolean());
        DeviceWiphyCapabilities.access$302(deviceWiphyCapabilities, param1Parcel.readBoolean());
        DeviceWiphyCapabilities.access$402(deviceWiphyCapabilities, param1Parcel.readBoolean());
        DeviceWiphyCapabilities.access$502(deviceWiphyCapabilities, param1Parcel.readInt());
        DeviceWiphyCapabilities.access$602(deviceWiphyCapabilities, param1Parcel.readInt());
        return deviceWiphyCapabilities;
      }
      
      public DeviceWiphyCapabilities[] newArray(int param1Int) {
        return new DeviceWiphyCapabilities[param1Int];
      }
    };
  
  private static final String TAG = "DeviceWiphyCapabilities";
}
