package android.net.wifi.nl80211;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.util.Objects;

public class ChannelSettings implements Parcelable {
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof ChannelSettings))
      return false; 
    paramObject = paramObject;
    if (paramObject == null)
      return false; 
    if (this.frequency != ((ChannelSettings)paramObject).frequency)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.frequency) });
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.frequency);
  }
  
  public static final Parcelable.Creator<ChannelSettings> CREATOR = new Parcelable.Creator<ChannelSettings>() {
      public ChannelSettings createFromParcel(Parcel param1Parcel) {
        ChannelSettings channelSettings = new ChannelSettings();
        channelSettings.frequency = param1Parcel.readInt();
        if (param1Parcel.dataAvail() != 0)
          Log.e("ChannelSettings", "Found trailing data after parcel parsing."); 
        return channelSettings;
      }
      
      public ChannelSettings[] newArray(int param1Int) {
        return new ChannelSettings[param1Int];
      }
    };
  
  private static final String TAG = "ChannelSettings";
  
  public int frequency;
}
