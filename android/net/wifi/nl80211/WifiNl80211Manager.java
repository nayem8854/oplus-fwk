package android.net.wifi.nl80211;

import android.annotation.SystemApi;
import android.app.AlarmManager;
import android.content.Context;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.util.Log;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

@SystemApi
public class WifiNl80211Manager {
  private boolean mVerboseLoggingEnabled = false;
  
  private HashMap<String, IClientInterface> mClientInterfaces = new HashMap<>();
  
  private HashMap<String, IApInterface> mApInterfaces = new HashMap<>();
  
  private HashMap<String, IWifiScannerImpl> mWificondScanners = new HashMap<>();
  
  private HashMap<String, IScanEvent> mScanEventHandlers = new HashMap<>();
  
  private HashMap<String, IPnoScanEvent> mPnoScanEventHandlers = new HashMap<>();
  
  private HashMap<String, IApInterfaceEventCallback> mApInterfaceListeners = new HashMap<>();
  
  private AtomicBoolean mSendMgmtFrameInProgress = new AtomicBoolean(false);
  
  private static final int MAX_SSID_LEN = 32;
  
  public static final int SCAN_TYPE_PNO_SCAN = 1;
  
  public static final int SCAN_TYPE_SINGLE_SCAN = 0;
  
  public static final int SEND_MGMT_FRAME_ERROR_ALREADY_STARTED = 5;
  
  public static final int SEND_MGMT_FRAME_ERROR_MCS_UNSUPPORTED = 2;
  
  public static final int SEND_MGMT_FRAME_ERROR_NO_ACK = 3;
  
  public static final int SEND_MGMT_FRAME_ERROR_TIMEOUT = 4;
  
  public static final int SEND_MGMT_FRAME_ERROR_UNKNOWN = 1;
  
  private static final int SEND_MGMT_FRAME_TIMEOUT_MS = 1000;
  
  private static final String TAG = "WifiNl80211Manager";
  
  private static final String TIMEOUT_ALARM_TAG = "WifiNl80211Manager Send Management Frame Timeout";
  
  private AlarmManager mAlarmManager;
  
  private Runnable mDeathEventHandler;
  
  private Handler mEventHandler;
  
  private IWificond mWificond;
  
  class ScanEventHandler extends IScanEvent.Stub {
    private WifiNl80211Manager.ScanEventCallback mCallback;
    
    private Executor mExecutor;
    
    final WifiNl80211Manager this$0;
    
    ScanEventHandler(Executor param1Executor, WifiNl80211Manager.ScanEventCallback param1ScanEventCallback) {
      this.mExecutor = param1Executor;
      this.mCallback = param1ScanEventCallback;
    }
    
    public void OnScanResultReady() {
      Log.d("WifiNl80211Manager", "Scan result ready event");
      Binder.clearCallingIdentity();
      this.mExecutor.execute(new _$$Lambda$WifiNl80211Manager$ScanEventHandler$XjI5kFSu0f9cphx_qo_5NAvCh5I(this));
    }
    
    public void OnScanFailed() {
      Log.d("WifiNl80211Manager", "Scan failed event");
      Binder.clearCallingIdentity();
      this.mExecutor.execute(new _$$Lambda$WifiNl80211Manager$ScanEventHandler$JhvqssvPGZY7hdAZwipalm6BXM0(this));
    }
  }
  
  public static class SignalPollResult {
    public final int associationFrequencyMHz;
    
    public final int currentRssiDbm;
    
    public final int rxBitrateMbps;
    
    public final int txBitrateMbps;
    
    public SignalPollResult(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      this.currentRssiDbm = param1Int1;
      this.txBitrateMbps = param1Int2;
      this.rxBitrateMbps = param1Int3;
      this.associationFrequencyMHz = param1Int4;
    }
  }
  
  public static class TxPacketCounters {
    public final int txPacketFailed;
    
    public final int txPacketSucceeded;
    
    public TxPacketCounters(int param1Int1, int param1Int2) {
      this.txPacketSucceeded = param1Int1;
      this.txPacketFailed = param1Int2;
    }
  }
  
  public WifiNl80211Manager(Context paramContext) {
    this.mAlarmManager = (AlarmManager)paramContext.getSystemService(AlarmManager.class);
    this.mEventHandler = new Handler(paramContext.getMainLooper());
  }
  
  public WifiNl80211Manager(Context paramContext, IWificond paramIWificond) {
    this(paramContext);
    this.mWificond = paramIWificond;
  }
  
  class PnoScanEventHandler extends IPnoScanEvent.Stub {
    private WifiNl80211Manager.ScanEventCallback mCallback;
    
    private Executor mExecutor;
    
    final WifiNl80211Manager this$0;
    
    PnoScanEventHandler(Executor param1Executor, WifiNl80211Manager.ScanEventCallback param1ScanEventCallback) {
      this.mExecutor = param1Executor;
      this.mCallback = param1ScanEventCallback;
    }
    
    public void OnPnoNetworkFound() {
      Log.d("WifiNl80211Manager", "Pno scan result event");
      Binder.clearCallingIdentity();
      this.mExecutor.execute(new _$$Lambda$WifiNl80211Manager$PnoScanEventHandler$yYSsPsPATilbeLB7fwmasf4d9II(this));
    }
    
    public void OnPnoScanFailed() {
      Log.d("WifiNl80211Manager", "Pno Scan failed event");
      Binder.clearCallingIdentity();
      this.mExecutor.execute(new _$$Lambda$WifiNl80211Manager$PnoScanEventHandler$oC8WJxgsPPFjKdQbqbGyPpsRuIQ(this));
    }
  }
  
  class ApInterfaceEventCallback extends IApInterfaceEventCallback.Stub {
    private Executor mExecutor;
    
    private WifiNl80211Manager.SoftApCallback mSoftApListener;
    
    final WifiNl80211Manager this$0;
    
    ApInterfaceEventCallback(Executor param1Executor, WifiNl80211Manager.SoftApCallback param1SoftApCallback) {
      this.mExecutor = param1Executor;
      this.mSoftApListener = param1SoftApCallback;
    }
    
    public void onConnectedClientsChanged(NativeWifiClient param1NativeWifiClient, boolean param1Boolean) {}
    
    public void onSoftApChannelSwitched(int param1Int1, int param1Int2) {
      Binder.clearCallingIdentity();
      this.mExecutor.execute(new _$$Lambda$WifiNl80211Manager$ApInterfaceEventCallback$ZvZs60kvmnA6Nxq2WubUsXx3Vq8(this, param1Int1, param1Int2));
    }
    
    private int toFrameworkBandwidth(int param1Int) {
      switch (param1Int) {
        default:
          return 0;
        case 6:
          return 6;
        case 5:
          return 5;
        case 4:
          return 4;
        case 3:
          return 3;
        case 2:
          return 2;
        case 1:
          break;
      } 
      return 1;
    }
  }
  
  class SendMgmtFrameEvent extends ISendMgmtFrameEvent.Stub {
    private WifiNl80211Manager.SendMgmtFrameCallback mCallback;
    
    private Executor mExecutor;
    
    private AlarmManager.OnAlarmListener mTimeoutCallback;
    
    private boolean mWasCalled;
    
    final WifiNl80211Manager this$0;
    
    private void runIfFirstCall(Runnable param1Runnable) {
      if (this.mWasCalled)
        return; 
      this.mWasCalled = true;
      WifiNl80211Manager.this.mSendMgmtFrameInProgress.set(false);
      param1Runnable.run();
    }
    
    SendMgmtFrameEvent(Executor param1Executor, WifiNl80211Manager.SendMgmtFrameCallback param1SendMgmtFrameCallback) {
      this.mExecutor = param1Executor;
      this.mCallback = param1SendMgmtFrameCallback;
      this.mTimeoutCallback = new _$$Lambda$WifiNl80211Manager$SendMgmtFrameEvent$om0NftZUGrqsz3A_5FK5rjlHcxQ(this);
      this.mWasCalled = false;
      AlarmManager alarmManager = WifiNl80211Manager.this.mAlarmManager;
      long l = SystemClock.elapsedRealtime();
      AlarmManager.OnAlarmListener onAlarmListener = this.mTimeoutCallback;
      Handler handler = WifiNl80211Manager.this.mEventHandler;
      alarmManager.set(2, l + 1000L, "WifiNl80211Manager Send Management Frame Timeout", onAlarmListener, handler);
    }
    
    public void OnAck(int param1Int) {
      WifiNl80211Manager.this.mEventHandler.post(new _$$Lambda$WifiNl80211Manager$SendMgmtFrameEvent$lqXW7dvgP4SiZa9axpAVyijTCkM(this, param1Int));
    }
    
    public void OnFailure(int param1Int) {
      WifiNl80211Manager.this.mEventHandler.post(new _$$Lambda$WifiNl80211Manager$SendMgmtFrameEvent$9KLACn0Uhup4kuzEujGfoeBXDdM(this, param1Int));
    }
  }
  
  public void binderDied() {
    this.mEventHandler.post(new _$$Lambda$WifiNl80211Manager$ShMFa3boc_GR969SdMRhe9INA5A(this));
  }
  
  public void enableVerboseLogging(boolean paramBoolean) {
    this.mVerboseLoggingEnabled = paramBoolean;
  }
  
  public void setOnServiceDeadCallback(Runnable paramRunnable) {
    if (this.mDeathEventHandler != null)
      Log.e("WifiNl80211Manager", "Death handler already present"); 
    this.mDeathEventHandler = paramRunnable;
  }
  
  private boolean retrieveWificondAndRegisterForDeath() {
    if (this.mWificond != null) {
      if (this.mVerboseLoggingEnabled)
        Log.d("WifiNl80211Manager", "Wificond handle already retrieved"); 
      return true;
    } 
    IBinder iBinder = ServiceManager.getService("wifinl80211");
    IWificond iWificond = IWificond.Stub.asInterface(iBinder);
    if (iWificond == null) {
      Log.e("WifiNl80211Manager", "Failed to get reference to wificond");
      return false;
    } 
    try {
      IBinder iBinder1 = iWificond.asBinder();
      _$$Lambda$WifiNl80211Manager$gj_UWkRFMZyZJomZvcvMZmTFj_E _$$Lambda$WifiNl80211Manager$gj_UWkRFMZyZJomZvcvMZmTFj_E = new _$$Lambda$WifiNl80211Manager$gj_UWkRFMZyZJomZvcvMZmTFj_E();
      this(this);
      iBinder1.linkToDeath(_$$Lambda$WifiNl80211Manager$gj_UWkRFMZyZJomZvcvMZmTFj_E, 0);
      return true;
    } catch (RemoteException remoteException) {
      Log.e("WifiNl80211Manager", "Failed to register death notification for wificond");
      return false;
    } 
  }
  
  public boolean setupInterfaceForClientMode(String paramString, Executor paramExecutor, ScanEventCallback paramScanEventCallback1, ScanEventCallback paramScanEventCallback2) {
    Log.d("WifiNl80211Manager", "Setting up interface for client mode");
    if (!retrieveWificondAndRegisterForDeath())
      return false; 
    if (paramScanEventCallback1 == null || paramScanEventCallback2 == null || paramExecutor == null) {
      Log.e("WifiNl80211Manager", "setupInterfaceForClientMode invoked with null callbacks");
      return false;
    } 
    try {
      IClientInterface iClientInterface = this.mWificond.createClientInterface(paramString);
      if (iClientInterface == null) {
        Log.e("WifiNl80211Manager", "Could not get IClientInterface instance from wificond");
        return false;
      } 
      Binder.allowBlocking(iClientInterface.asBinder());
      this.mClientInterfaces.put(paramString, iClientInterface);
      try {
        IWifiScannerImpl iWifiScannerImpl = iClientInterface.getWifiScannerImpl();
        if (iWifiScannerImpl == null) {
          Log.e("WifiNl80211Manager", "Failed to get WificondScannerImpl");
          return false;
        } 
        this.mWificondScanners.put(paramString, iWifiScannerImpl);
        Binder.allowBlocking(iWifiScannerImpl.asBinder());
        ScanEventHandler scanEventHandler = new ScanEventHandler();
        this(this, paramExecutor, paramScanEventCallback1);
        this.mScanEventHandlers.put(paramString, scanEventHandler);
        iWifiScannerImpl.subscribeScanEvents(scanEventHandler);
        PnoScanEventHandler pnoScanEventHandler = new PnoScanEventHandler();
        this(this, paramExecutor, paramScanEventCallback2);
        this.mPnoScanEventHandlers.put(paramString, pnoScanEventHandler);
        iWifiScannerImpl.subscribePnoScanEvents(pnoScanEventHandler);
      } catch (RemoteException remoteException) {
        Log.e("WifiNl80211Manager", "Failed to refresh wificond scanner due to remote exception");
      } 
      return true;
    } catch (RemoteException remoteException) {
      Log.e("WifiNl80211Manager", "Failed to get IClientInterface due to remote exception");
      return false;
    } 
  }
  
  public boolean tearDownClientInterface(String paramString) {
    if (getClientInterface(paramString) == null) {
      Log.e("WifiNl80211Manager", "No valid wificond client interface handler");
      return false;
    } 
    try {
      IWifiScannerImpl iWifiScannerImpl = this.mWificondScanners.get(paramString);
      if (iWifiScannerImpl != null) {
        iWifiScannerImpl.unsubscribeScanEvents();
        iWifiScannerImpl.unsubscribePnoScanEvents();
      } 
      IWificond iWificond = this.mWificond;
      if (iWificond == null) {
        Log.e("WifiNl80211Manager", "Reference to wifiCond is null");
        return false;
      } 
      try {
        boolean bool = iWificond.tearDownClientInterface(paramString);
        if (!bool) {
          Log.e("WifiNl80211Manager", "Failed to teardown client interface");
          return false;
        } 
        this.mClientInterfaces.remove(paramString);
        this.mWificondScanners.remove(paramString);
        this.mScanEventHandlers.remove(paramString);
        this.mPnoScanEventHandlers.remove(paramString);
        return true;
      } catch (RemoteException remoteException) {
        Log.e("WifiNl80211Manager", "Failed to teardown client interface due to remote exception");
        return false;
      } 
    } catch (RemoteException remoteException) {
      Log.e("WifiNl80211Manager", "Failed to unsubscribe wificond scanner due to remote exception");
      return false;
    } 
  }
  
  public boolean setupInterfaceForSoftApMode(String paramString) {
    Log.d("WifiNl80211Manager", "Setting up interface for soft ap mode");
    if (!retrieveWificondAndRegisterForDeath())
      return false; 
    try {
      IApInterface iApInterface = this.mWificond.createApInterface(paramString);
      if (iApInterface == null) {
        Log.e("WifiNl80211Manager", "Could not get IApInterface instance from wificond");
        return false;
      } 
      Binder.allowBlocking(iApInterface.asBinder());
      this.mApInterfaces.put(paramString, iApInterface);
      return true;
    } catch (RemoteException remoteException) {
      Log.e("WifiNl80211Manager", "Failed to get IApInterface due to remote exception");
      return false;
    } 
  }
  
  public boolean tearDownSoftApInterface(String paramString) {
    if (getApInterface(paramString) == null) {
      Log.e("WifiNl80211Manager", "No valid wificond ap interface handler");
      return false;
    } 
    IWificond iWificond = this.mWificond;
    if (iWificond == null) {
      Log.e("WifiNl80211Manager", "Reference to wifiCond is null");
      return false;
    } 
    try {
      boolean bool = iWificond.tearDownApInterface(paramString);
      if (!bool) {
        Log.e("WifiNl80211Manager", "Failed to teardown AP interface");
        return false;
      } 
      this.mApInterfaces.remove(paramString);
      this.mApInterfaceListeners.remove(paramString);
      return true;
    } catch (RemoteException remoteException) {
      Log.e("WifiNl80211Manager", "Failed to teardown AP interface due to remote exception");
      return false;
    } 
  }
  
  public boolean tearDownInterfaces() {
    Log.d("WifiNl80211Manager", "tearing down interfaces in wificond");
    if (!retrieveWificondAndRegisterForDeath())
      return false; 
    try {
      for (Map.Entry<String, IWifiScannerImpl> entry : this.mWificondScanners.entrySet()) {
        ((IWifiScannerImpl)entry.getValue()).unsubscribeScanEvents();
        ((IWifiScannerImpl)entry.getValue()).unsubscribePnoScanEvents();
      } 
      this.mWificond.tearDownInterfaces();
      clearState();
      return true;
    } catch (RemoteException remoteException) {
      Log.e("WifiNl80211Manager", "Failed to tear down interfaces due to remote exception");
      return false;
    } 
  }
  
  private IClientInterface getClientInterface(String paramString) {
    return this.mClientInterfaces.get(paramString);
  }
  
  public SignalPollResult signalPoll(String paramString) {
    IClientInterface iClientInterface = getClientInterface(paramString);
    if (iClientInterface == null) {
      Log.e("WifiNl80211Manager", "No valid wificond client interface handler");
      return null;
    } 
    try {
      int[] arrayOfInt = iClientInterface.signalPoll();
      if (arrayOfInt != null) {
        int i = arrayOfInt.length;
        if (i == 4)
          return new SignalPollResult(arrayOfInt[0], arrayOfInt[1], arrayOfInt[3], arrayOfInt[2]); 
      } 
      Log.e("WifiNl80211Manager", "Invalid signal poll result from wificond");
      return null;
    } catch (RemoteException remoteException) {
      Log.e("WifiNl80211Manager", "Failed to do signal polling due to remote exception");
      return null;
    } 
  }
  
  public TxPacketCounters getTxPacketCounters(String paramString) {
    IClientInterface iClientInterface = getClientInterface(paramString);
    if (iClientInterface == null) {
      Log.e("WifiNl80211Manager", "No valid wificond client interface handler");
      return null;
    } 
    try {
      int[] arrayOfInt = iClientInterface.getPacketCounters();
      if (arrayOfInt != null) {
        int i = arrayOfInt.length;
        if (i == 2)
          return new TxPacketCounters(arrayOfInt[0], arrayOfInt[1]); 
      } 
      Log.e("WifiNl80211Manager", "Invalid signal poll result from wificond");
      return null;
    } catch (RemoteException remoteException) {
      Log.e("WifiNl80211Manager", "Failed to do signal polling due to remote exception");
      return null;
    } 
  }
  
  private IWifiScannerImpl getScannerImpl(String paramString) {
    return this.mWificondScanners.get(paramString);
  }
  
  public List<NativeScanResult> getScanResults(String paramString, int paramInt) {
    List<NativeScanResult> list1;
    IWifiScannerImpl iWifiScannerImpl = getScannerImpl(paramString);
    if (iWifiScannerImpl == null) {
      Log.e("WifiNl80211Manager", "No valid wificond scanner interface handler");
      return new ArrayList<>();
    } 
    paramString = null;
    if (paramInt == 0) {
      try {
        List<NativeScanResult> list = Arrays.asList(iWifiScannerImpl.getScanResults());
      } catch (RemoteException remoteException) {
        Log.e("WifiNl80211Manager", "Failed to create ScanDetail ArrayList");
      } 
    } else {
      List<NativeScanResult> list = Arrays.asList(remoteException.getPnoScanResults());
    } 
    List<NativeScanResult> list2 = list1;
    if (list1 == null)
      list2 = new ArrayList<>(); 
    if (this.mVerboseLoggingEnabled) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("get ");
      stringBuilder.append(list2.size());
      stringBuilder.append(" scan results from wificond");
      Log.d("WifiNl80211Manager", stringBuilder.toString());
    } 
    return list2;
  }
  
  private static int getScanType(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt == 2)
          return 2; 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid scan type ");
        stringBuilder.append(paramInt);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      return 1;
    } 
    return 0;
  }
  
  public boolean startScan(String paramString, int paramInt, Set<Integer> paramSet, List<byte[]> paramList) {
    IWifiScannerImpl iWifiScannerImpl = getScannerImpl(paramString);
    if (iWifiScannerImpl == null) {
      Log.e("WifiNl80211Manager", "No valid wificond scanner interface handler");
      return false;
    } 
    SingleScanSettings singleScanSettings = new SingleScanSettings();
    try {
      singleScanSettings.scanType = getScanType(paramInt);
      singleScanSettings.channelSettings = new ArrayList<>();
      singleScanSettings.hiddenNetworks = new ArrayList<>();
      if (paramSet != null)
        for (Integer integer : paramSet) {
          ChannelSettings channelSettings = new ChannelSettings();
          channelSettings.frequency = integer.intValue();
          singleScanSettings.channelSettings.add(channelSettings);
        }  
      if (paramList != null)
        for (byte[] arrayOfByte : paramList) {
          HiddenNetwork hiddenNetwork = new HiddenNetwork();
          hiddenNetwork.ssid = arrayOfByte;
          if (hiddenNetwork.ssid.length > 32) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("SSID is too long after conversion, skipping this ssid! SSID = ");
            stringBuilder.append(hiddenNetwork.ssid);
            stringBuilder.append(" , network.ssid.size = ");
            stringBuilder.append(hiddenNetwork.ssid.length);
            Log.e("WifiNl80211Manager", stringBuilder.toString());
            continue;
          } 
          if (!singleScanSettings.hiddenNetworks.contains(hiddenNetwork))
            singleScanSettings.hiddenNetworks.add(hiddenNetwork); 
        }  
      try {
        return iWifiScannerImpl.scan(singleScanSettings);
      } catch (RemoteException remoteException) {
        Log.e("WifiNl80211Manager", "Failed to request scan due to remote exception");
        return false;
      } 
    } catch (IllegalArgumentException illegalArgumentException) {
      Log.e("WifiNl80211Manager", "Invalid scan type ", illegalArgumentException);
      return false;
    } 
  }
  
  public boolean startPnoScan(String paramString, PnoSettings paramPnoSettings, Executor paramExecutor, PnoScanRequestCallback paramPnoScanRequestCallback) {
    IWifiScannerImpl iWifiScannerImpl = getScannerImpl(paramString);
    if (iWifiScannerImpl == null) {
      Log.e("WifiNl80211Manager", "No valid wificond scanner interface handler");
      return false;
    } 
    if (paramPnoScanRequestCallback == null || paramExecutor == null) {
      Log.e("WifiNl80211Manager", "startPnoScan called with a null callback");
      return false;
    } 
    try {
      boolean bool = iWifiScannerImpl.startPnoScan(paramPnoSettings);
      if (bool) {
        Objects.requireNonNull(paramPnoScanRequestCallback);
        _$$Lambda$ZG2hKx497SbMnsFFcQwIH8UGwrM _$$Lambda$ZG2hKx497SbMnsFFcQwIH8UGwrM = new _$$Lambda$ZG2hKx497SbMnsFFcQwIH8UGwrM();
        this(paramPnoScanRequestCallback);
        paramExecutor.execute(_$$Lambda$ZG2hKx497SbMnsFFcQwIH8UGwrM);
      } else {
        Objects.requireNonNull(paramPnoScanRequestCallback);
        _$$Lambda$eTziUIMwBgXfmQ_0cHdT_qNhkIU _$$Lambda$eTziUIMwBgXfmQ_0cHdT_qNhkIU = new _$$Lambda$eTziUIMwBgXfmQ_0cHdT_qNhkIU();
        this(paramPnoScanRequestCallback);
        paramExecutor.execute(_$$Lambda$eTziUIMwBgXfmQ_0cHdT_qNhkIU);
      } 
      return bool;
    } catch (RemoteException remoteException) {
      Log.e("WifiNl80211Manager", "Failed to start pno scan due to remote exception");
      return false;
    } 
  }
  
  public boolean stopPnoScan(String paramString) {
    IWifiScannerImpl iWifiScannerImpl = getScannerImpl(paramString);
    if (iWifiScannerImpl == null) {
      Log.e("WifiNl80211Manager", "No valid wificond scanner interface handler");
      return false;
    } 
    try {
      return iWifiScannerImpl.stopPnoScan();
    } catch (RemoteException remoteException) {
      Log.e("WifiNl80211Manager", "Failed to stop pno scan due to remote exception");
      return false;
    } 
  }
  
  public void abortScan(String paramString) {
    IWifiScannerImpl iWifiScannerImpl = getScannerImpl(paramString);
    if (iWifiScannerImpl == null) {
      Log.e("WifiNl80211Manager", "No valid wificond scanner interface handler");
      return;
    } 
    try {
      iWifiScannerImpl.abortScan();
    } catch (RemoteException remoteException) {
      Log.e("WifiNl80211Manager", "Failed to request abortScan due to remote exception");
    } 
  }
  
  public int[] getChannelsMhzForBand(int paramInt) {
    IWificond iWificond = this.mWificond;
    if (iWificond == null) {
      Log.e("WifiNl80211Manager", "No valid wificond scanner interface handler");
      return new int[0];
    } 
    int[] arrayOfInt2 = null;
    if (paramInt != 1) {
      if (paramInt != 2) {
        StringBuilder stringBuilder;
        if (paramInt != 4) {
          if (paramInt == 8) {
            try {
              arrayOfInt1 = iWificond.getAvailable6gChannels();
              arrayOfInt2 = arrayOfInt1;
            } catch (RemoteException remoteException) {
              Log.e("WifiNl80211Manager", "Failed to request getChannelsForBand due to remote exception");
            } 
          } else {
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
            stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("unsupported band ");
            stringBuilder.append(paramInt);
            this(stringBuilder.toString());
            throw illegalArgumentException;
          } 
        } else {
          arrayOfInt1 = stringBuilder.getAvailableDFSChannels();
          arrayOfInt2 = arrayOfInt1;
        } 
      } else {
        arrayOfInt1 = arrayOfInt1.getAvailable5gNonDFSChannels();
        arrayOfInt2 = arrayOfInt1;
      } 
    } else {
      arrayOfInt1 = arrayOfInt1.getAvailable2gChannels();
      arrayOfInt2 = arrayOfInt1;
    } 
    int[] arrayOfInt1 = arrayOfInt2;
    if (arrayOfInt2 == null)
      arrayOfInt1 = new int[0]; 
    return arrayOfInt1;
  }
  
  private IApInterface getApInterface(String paramString) {
    return this.mApInterfaces.get(paramString);
  }
  
  public DeviceWiphyCapabilities getDeviceWiphyCapabilities(String paramString) {
    IWificond iWificond = this.mWificond;
    if (iWificond == null) {
      Log.e("WifiNl80211Manager", "Can not query for device wiphy capabilities at this time");
      return null;
    } 
    try {
      return iWificond.getDeviceWiphyCapabilities(paramString);
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public boolean registerApCallback(String paramString, Executor paramExecutor, SoftApCallback paramSoftApCallback) {
    IApInterface iApInterface = getApInterface(paramString);
    if (iApInterface == null) {
      Log.e("WifiNl80211Manager", "No valid ap interface handler");
      return false;
    } 
    if (paramSoftApCallback == null || paramExecutor == null) {
      Log.e("WifiNl80211Manager", "registerApCallback called with a null callback");
      return false;
    } 
    try {
      ApInterfaceEventCallback apInterfaceEventCallback = new ApInterfaceEventCallback();
      this(this, paramExecutor, paramSoftApCallback);
      this.mApInterfaceListeners.put(paramString, apInterfaceEventCallback);
      boolean bool = iApInterface.registerCallback(apInterfaceEventCallback);
      if (!bool) {
        Log.e("WifiNl80211Manager", "Failed to register ap callback.");
        return false;
      } 
      return true;
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Exception in registering AP callback: ");
      stringBuilder.append(remoteException);
      Log.e("WifiNl80211Manager", stringBuilder.toString());
      return false;
    } 
  }
  
  public void sendMgmtFrame(String paramString, byte[] paramArrayOfbyte, int paramInt, Executor paramExecutor, SendMgmtFrameCallback paramSendMgmtFrameCallback) {
    if (paramSendMgmtFrameCallback == null || paramExecutor == null) {
      Log.e("WifiNl80211Manager", "callback cannot be null!");
      return;
    } 
    if (paramArrayOfbyte == null) {
      Log.e("WifiNl80211Manager", "frame cannot be null!");
      paramExecutor.execute(new _$$Lambda$WifiNl80211Manager$px5c9lsbhVJXt3KKfBEFiQ_k20E(paramSendMgmtFrameCallback));
      return;
    } 
    IClientInterface iClientInterface = getClientInterface(paramString);
    if (iClientInterface == null) {
      Log.e("WifiNl80211Manager", "No valid wificond client interface handler");
      paramExecutor.execute(new _$$Lambda$WifiNl80211Manager$jQofYBr8nr0eWIuyBBzw2O7pcw8(paramSendMgmtFrameCallback));
      return;
    } 
    if (!this.mSendMgmtFrameInProgress.compareAndSet(false, true)) {
      Log.e("WifiNl80211Manager", "An existing management frame transmission is in progress!");
      paramExecutor.execute(new _$$Lambda$WifiNl80211Manager$_84sQMp93x3MR5HbAeuP427nm0g(paramSendMgmtFrameCallback));
      return;
    } 
    SendMgmtFrameEvent sendMgmtFrameEvent = new SendMgmtFrameEvent(paramExecutor, paramSendMgmtFrameCallback);
    try {
      iClientInterface.SendMgmtFrame(paramArrayOfbyte, sendMgmtFrameEvent, paramInt);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Exception while starting link probe: ");
      stringBuilder.append(remoteException);
      Log.e("WifiNl80211Manager", stringBuilder.toString());
      sendMgmtFrameEvent.OnFailure(1);
    } 
  }
  
  public boolean blockClient(String paramString1, String paramString2, boolean paramBoolean) {
    StringBuilder stringBuilder;
    IApInterface iApInterface = getApInterface(paramString1);
    if (iApInterface == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("No valid ap interface handler ifaceName=");
      stringBuilder.append(paramString1);
      Log.e("WifiNl80211Manager", stringBuilder.toString());
      return false;
    } 
    try {
      paramBoolean = iApInterface.blockClient((String)stringBuilder, paramBoolean);
      if (!paramBoolean) {
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Failed to blockClient at wificond ifaceName=");
        stringBuilder.append(paramString1);
        Log.e("WifiNl80211Manager", stringBuilder.toString());
        return false;
      } 
      return true;
    } catch (RemoteException remoteException) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Exception in blockClient at wificond, ifaceName= ");
      stringBuilder.append(paramString1);
      stringBuilder.append(" e:");
      stringBuilder.append(remoteException);
      Log.e("WifiNl80211Manager", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean setMaxClient(String paramString, int paramInt) {
    StringBuilder stringBuilder;
    IApInterface iApInterface = getApInterface(paramString);
    if (iApInterface == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("No valid ap interface handler ifaceName=");
      stringBuilder.append(paramString);
      Log.e("WifiNl80211Manager", stringBuilder.toString());
      return false;
    } 
    try {
      boolean bool = stringBuilder.setMaxClient(paramInt);
      if (!bool) {
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Failed to setMaxClient at wificond ifaceName=");
        stringBuilder.append(paramString);
        Log.e("WifiNl80211Manager", stringBuilder.toString());
        return false;
      } 
      return true;
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Exception in setMaxClient at wificond, ifaceName= ");
      stringBuilder1.append(paramString);
      stringBuilder1.append(" e:");
      stringBuilder1.append(remoteException);
      Log.e("WifiNl80211Manager", stringBuilder1.toString());
      return false;
    } 
  }
  
  boolean blockSavedClients(String paramString, String[] paramArrayOfString) {
    StringBuilder stringBuilder;
    IApInterface iApInterface = getApInterface(paramString);
    if (iApInterface == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("No valid ap interface handler ifaceName=");
      stringBuilder.append(paramString);
      Log.e("WifiNl80211Manager", stringBuilder.toString());
      return false;
    } 
    try {
      boolean bool = iApInterface.blockSavedClients((String[])stringBuilder);
      if (!bool) {
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Failed to blockSavedClients at wificond ifaceName=");
        stringBuilder.append(paramString);
        Log.e("WifiNl80211Manager", stringBuilder.toString());
        return false;
      } 
      return true;
    } catch (RemoteException remoteException) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Exception in blockSavedClients at wificond, ifaceName= ");
      stringBuilder.append(paramString);
      stringBuilder.append(" e:");
      stringBuilder.append(remoteException);
      Log.e("WifiNl80211Manager", stringBuilder.toString());
      return false;
    } 
  }
  
  private void clearState() {
    this.mClientInterfaces.clear();
    this.mWificondScanners.clear();
    this.mPnoScanEventHandlers.clear();
    this.mScanEventHandlers.clear();
    this.mApInterfaces.clear();
    this.mApInterfaceListeners.clear();
    this.mSendMgmtFrameInProgress.set(false);
  }
  
  public static class OemSecurityType {
    public final int groupCipher;
    
    public final List<Integer> keyManagement;
    
    public final List<Integer> pairwiseCipher;
    
    public final int protocol;
    
    public OemSecurityType(int param1Int1, List<Integer> param1List1, List<Integer> param1List2, int param1Int2) {
      this.protocol = param1Int1;
      if (param1List1 == null)
        param1List1 = new ArrayList<>(); 
      this.keyManagement = param1List1;
      if (param1List2 != null) {
        param1List1 = param1List2;
      } else {
        param1List1 = new ArrayList<>();
      } 
      this.pairwiseCipher = param1List1;
      this.groupCipher = param1Int2;
    }
  }
  
  public static OemSecurityType parseOemSecurityTypeElement(int paramInt1, int paramInt2, byte[] paramArrayOfbyte) {
    return null;
  }
  
  public static interface PnoScanRequestCallback {
    void onPnoRequestFailed();
    
    void onPnoRequestSucceeded();
  }
  
  public static interface ScanEventCallback {
    void onScanFailed();
    
    void onScanResultReady();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ScanResultType {}
  
  public static interface SendMgmtFrameCallback {
    void onAck(int param1Int);
    
    void onFailure(int param1Int);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SendMgmtFrameError {}
  
  public static interface SoftApCallback {
    void onConnectedClientsChanged(NativeWifiClient param1NativeWifiClient, boolean param1Boolean);
    
    void onFailure();
    
    void onSoftApChannelSwitched(int param1Int1, int param1Int2);
  }
}
