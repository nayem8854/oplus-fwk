package android.net.wifi.nl80211;

import android.annotation.SystemApi;
import android.net.MacAddress;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

@SystemApi
public final class NativeWifiClient implements Parcelable {
  public MacAddress getMacAddress() {
    return this.mMacAddress;
  }
  
  public NativeWifiClient(MacAddress paramMacAddress) {
    this.mMacAddress = paramMacAddress;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof NativeWifiClient))
      return false; 
    paramObject = paramObject;
    return Objects.equals(this.mMacAddress, ((NativeWifiClient)paramObject).mMacAddress);
  }
  
  public int hashCode() {
    return this.mMacAddress.hashCode();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByteArray(this.mMacAddress.toByteArray());
  }
  
  public static final Parcelable.Creator<NativeWifiClient> CREATOR = new Parcelable.Creator<NativeWifiClient>() {
      public NativeWifiClient createFromParcel(Parcel param1Parcel) {
        try {
          MacAddress macAddress = MacAddress.fromBytes(param1Parcel.createByteArray());
        } catch (IllegalArgumentException illegalArgumentException) {
          illegalArgumentException = null;
        } 
        return new NativeWifiClient((MacAddress)illegalArgumentException);
      }
      
      public NativeWifiClient[] newArray(int param1Int) {
        return new NativeWifiClient[param1Int];
      }
    };
  
  private final MacAddress mMacAddress;
}
