package android.net.wifi.nl80211;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IWificond extends IInterface {
  List<IBinder> GetApInterfaces() throws RemoteException;
  
  List<IBinder> GetClientInterfaces() throws RemoteException;
  
  void RegisterCallback(IInterfaceEventCallback paramIInterfaceEventCallback) throws RemoteException;
  
  void UnregisterCallback(IInterfaceEventCallback paramIInterfaceEventCallback) throws RemoteException;
  
  IApInterface createApInterface(String paramString) throws RemoteException;
  
  IClientInterface createClientInterface(String paramString) throws RemoteException;
  
  int[] getAvailable2gChannels() throws RemoteException;
  
  int[] getAvailable5gNonDFSChannels() throws RemoteException;
  
  int[] getAvailable6gChannels() throws RemoteException;
  
  int[] getAvailableDFSChannels() throws RemoteException;
  
  DeviceWiphyCapabilities getDeviceWiphyCapabilities(String paramString) throws RemoteException;
  
  boolean tearDownApInterface(String paramString) throws RemoteException;
  
  boolean tearDownClientInterface(String paramString) throws RemoteException;
  
  void tearDownInterfaces() throws RemoteException;
  
  class Default implements IWificond {
    public IApInterface createApInterface(String param1String) throws RemoteException {
      return null;
    }
    
    public IClientInterface createClientInterface(String param1String) throws RemoteException {
      return null;
    }
    
    public boolean tearDownApInterface(String param1String) throws RemoteException {
      return false;
    }
    
    public boolean tearDownClientInterface(String param1String) throws RemoteException {
      return false;
    }
    
    public void tearDownInterfaces() throws RemoteException {}
    
    public List<IBinder> GetClientInterfaces() throws RemoteException {
      return null;
    }
    
    public List<IBinder> GetApInterfaces() throws RemoteException {
      return null;
    }
    
    public int[] getAvailable2gChannels() throws RemoteException {
      return null;
    }
    
    public int[] getAvailable5gNonDFSChannels() throws RemoteException {
      return null;
    }
    
    public int[] getAvailableDFSChannels() throws RemoteException {
      return null;
    }
    
    public int[] getAvailable6gChannels() throws RemoteException {
      return null;
    }
    
    public void RegisterCallback(IInterfaceEventCallback param1IInterfaceEventCallback) throws RemoteException {}
    
    public void UnregisterCallback(IInterfaceEventCallback param1IInterfaceEventCallback) throws RemoteException {}
    
    public DeviceWiphyCapabilities getDeviceWiphyCapabilities(String param1String) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWificond {
    private static final String DESCRIPTOR = "android.net.wifi.nl80211.IWificond";
    
    static final int TRANSACTION_GetApInterfaces = 7;
    
    static final int TRANSACTION_GetClientInterfaces = 6;
    
    static final int TRANSACTION_RegisterCallback = 12;
    
    static final int TRANSACTION_UnregisterCallback = 13;
    
    static final int TRANSACTION_createApInterface = 1;
    
    static final int TRANSACTION_createClientInterface = 2;
    
    static final int TRANSACTION_getAvailable2gChannels = 8;
    
    static final int TRANSACTION_getAvailable5gNonDFSChannels = 9;
    
    static final int TRANSACTION_getAvailable6gChannels = 11;
    
    static final int TRANSACTION_getAvailableDFSChannels = 10;
    
    static final int TRANSACTION_getDeviceWiphyCapabilities = 14;
    
    static final int TRANSACTION_tearDownApInterface = 3;
    
    static final int TRANSACTION_tearDownClientInterface = 4;
    
    static final int TRANSACTION_tearDownInterfaces = 5;
    
    public Stub() {
      attachInterface(this, "android.net.wifi.nl80211.IWificond");
    }
    
    public static IWificond asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.wifi.nl80211.IWificond");
      if (iInterface != null && iInterface instanceof IWificond)
        return (IWificond)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 14:
          return "getDeviceWiphyCapabilities";
        case 13:
          return "UnregisterCallback";
        case 12:
          return "RegisterCallback";
        case 11:
          return "getAvailable6gChannels";
        case 10:
          return "getAvailableDFSChannels";
        case 9:
          return "getAvailable5gNonDFSChannels";
        case 8:
          return "getAvailable2gChannels";
        case 7:
          return "GetApInterfaces";
        case 6:
          return "GetClientInterfaces";
        case 5:
          return "tearDownInterfaces";
        case 4:
          return "tearDownClientInterface";
        case 3:
          return "tearDownApInterface";
        case 2:
          return "createClientInterface";
        case 1:
          break;
      } 
      return "createApInterface";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        String str3;
        DeviceWiphyCapabilities deviceWiphyCapabilities;
        IInterfaceEventCallback iInterfaceEventCallback;
        int[] arrayOfInt;
        List<IBinder> list;
        String str2;
        IBinder iBinder2, iBinder1;
        IClientInterface iClientInterface2 = null;
        String str4 = null;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 14:
            param1Parcel1.enforceInterface("android.net.wifi.nl80211.IWificond");
            str3 = param1Parcel1.readString();
            deviceWiphyCapabilities = getDeviceWiphyCapabilities(str3);
            param1Parcel2.writeNoException();
            if (deviceWiphyCapabilities != null) {
              param1Parcel2.writeInt(1);
              deviceWiphyCapabilities.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 13:
            deviceWiphyCapabilities.enforceInterface("android.net.wifi.nl80211.IWificond");
            iInterfaceEventCallback = IInterfaceEventCallback.Stub.asInterface(deviceWiphyCapabilities.readStrongBinder());
            UnregisterCallback(iInterfaceEventCallback);
            return true;
          case 12:
            iInterfaceEventCallback.enforceInterface("android.net.wifi.nl80211.IWificond");
            iInterfaceEventCallback = IInterfaceEventCallback.Stub.asInterface(iInterfaceEventCallback.readStrongBinder());
            RegisterCallback(iInterfaceEventCallback);
            return true;
          case 11:
            iInterfaceEventCallback.enforceInterface("android.net.wifi.nl80211.IWificond");
            arrayOfInt = getAvailable6gChannels();
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt);
            return true;
          case 10:
            arrayOfInt.enforceInterface("android.net.wifi.nl80211.IWificond");
            arrayOfInt = getAvailableDFSChannels();
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt);
            return true;
          case 9:
            arrayOfInt.enforceInterface("android.net.wifi.nl80211.IWificond");
            arrayOfInt = getAvailable5gNonDFSChannels();
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt);
            return true;
          case 8:
            arrayOfInt.enforceInterface("android.net.wifi.nl80211.IWificond");
            arrayOfInt = getAvailable2gChannels();
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt);
            return true;
          case 7:
            arrayOfInt.enforceInterface("android.net.wifi.nl80211.IWificond");
            list = GetApInterfaces();
            param1Parcel2.writeNoException();
            param1Parcel2.writeBinderList(list);
            return true;
          case 6:
            list.enforceInterface("android.net.wifi.nl80211.IWificond");
            list = GetClientInterfaces();
            param1Parcel2.writeNoException();
            param1Parcel2.writeBinderList(list);
            return true;
          case 5:
            list.enforceInterface("android.net.wifi.nl80211.IWificond");
            tearDownInterfaces();
            param1Parcel2.writeNoException();
            return true;
          case 4:
            list.enforceInterface("android.net.wifi.nl80211.IWificond");
            str2 = list.readString();
            bool = tearDownClientInterface(str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 3:
            str2.enforceInterface("android.net.wifi.nl80211.IWificond");
            str2 = str2.readString();
            bool = tearDownApInterface(str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 2:
            str2.enforceInterface("android.net.wifi.nl80211.IWificond");
            str2 = str2.readString();
            iClientInterface2 = createClientInterface(str2);
            param1Parcel2.writeNoException();
            str2 = str4;
            if (iClientInterface2 != null)
              iBinder2 = iClientInterface2.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder2);
            return true;
          case 1:
            break;
        } 
        iBinder2.enforceInterface("android.net.wifi.nl80211.IWificond");
        String str1 = iBinder2.readString();
        IApInterface iApInterface = createApInterface(str1);
        param1Parcel2.writeNoException();
        IClientInterface iClientInterface1 = iClientInterface2;
        if (iApInterface != null)
          iBinder1 = iApInterface.asBinder(); 
        param1Parcel2.writeStrongBinder(iBinder1);
        return true;
      } 
      param1Parcel2.writeString("android.net.wifi.nl80211.IWificond");
      return true;
    }
    
    private static class Proxy implements IWificond {
      public static IWificond sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.wifi.nl80211.IWificond";
      }
      
      public IApInterface createApInterface(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IWificond");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IWificond.Stub.getDefaultImpl() != null)
            return IWificond.Stub.getDefaultImpl().createApInterface(param2String); 
          parcel2.readException();
          return IApInterface.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IClientInterface createClientInterface(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IWificond");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IWificond.Stub.getDefaultImpl() != null)
            return IWificond.Stub.getDefaultImpl().createClientInterface(param2String); 
          parcel2.readException();
          return IClientInterface.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean tearDownApInterface(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IWificond");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IWificond.Stub.getDefaultImpl() != null) {
            bool1 = IWificond.Stub.getDefaultImpl().tearDownApInterface(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean tearDownClientInterface(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IWificond");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IWificond.Stub.getDefaultImpl() != null) {
            bool1 = IWificond.Stub.getDefaultImpl().tearDownClientInterface(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void tearDownInterfaces() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IWificond");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IWificond.Stub.getDefaultImpl() != null) {
            IWificond.Stub.getDefaultImpl().tearDownInterfaces();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<IBinder> GetClientInterfaces() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IWificond");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IWificond.Stub.getDefaultImpl() != null)
            return IWificond.Stub.getDefaultImpl().GetClientInterfaces(); 
          parcel2.readException();
          return parcel2.createBinderArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<IBinder> GetApInterfaces() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IWificond");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IWificond.Stub.getDefaultImpl() != null)
            return IWificond.Stub.getDefaultImpl().GetApInterfaces(); 
          parcel2.readException();
          return parcel2.createBinderArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getAvailable2gChannels() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IWificond");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IWificond.Stub.getDefaultImpl() != null)
            return IWificond.Stub.getDefaultImpl().getAvailable2gChannels(); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getAvailable5gNonDFSChannels() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IWificond");
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IWificond.Stub.getDefaultImpl() != null)
            return IWificond.Stub.getDefaultImpl().getAvailable5gNonDFSChannels(); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getAvailableDFSChannels() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IWificond");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IWificond.Stub.getDefaultImpl() != null)
            return IWificond.Stub.getDefaultImpl().getAvailableDFSChannels(); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getAvailable6gChannels() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IWificond");
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IWificond.Stub.getDefaultImpl() != null)
            return IWificond.Stub.getDefaultImpl().getAvailable6gChannels(); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void RegisterCallback(IInterfaceEventCallback param2IInterfaceEventCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.net.wifi.nl80211.IWificond");
          if (param2IInterfaceEventCallback != null) {
            iBinder = param2IInterfaceEventCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(12, parcel, (Parcel)null, 1);
          if (!bool && IWificond.Stub.getDefaultImpl() != null) {
            IWificond.Stub.getDefaultImpl().RegisterCallback(param2IInterfaceEventCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void UnregisterCallback(IInterfaceEventCallback param2IInterfaceEventCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.net.wifi.nl80211.IWificond");
          if (param2IInterfaceEventCallback != null) {
            iBinder = param2IInterfaceEventCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(13, parcel, (Parcel)null, 1);
          if (!bool && IWificond.Stub.getDefaultImpl() != null) {
            IWificond.Stub.getDefaultImpl().UnregisterCallback(param2IInterfaceEventCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public DeviceWiphyCapabilities getDeviceWiphyCapabilities(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IWificond");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IWificond.Stub.getDefaultImpl() != null)
            return IWificond.Stub.getDefaultImpl().getDeviceWiphyCapabilities(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            DeviceWiphyCapabilities deviceWiphyCapabilities = DeviceWiphyCapabilities.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (DeviceWiphyCapabilities)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IWificond param1IWificond) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWificond != null) {
          Proxy.sDefaultImpl = param1IWificond;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWificond getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
