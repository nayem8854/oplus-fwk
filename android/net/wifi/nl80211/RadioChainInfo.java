package android.net.wifi.nl80211;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

@SystemApi
public final class RadioChainInfo implements Parcelable {
  public int getChainId() {
    return this.chainId;
  }
  
  public int getLevelDbm() {
    return this.level;
  }
  
  public RadioChainInfo(int paramInt1, int paramInt2) {
    this.chainId = paramInt1;
    this.level = paramInt2;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof RadioChainInfo))
      return false; 
    paramObject = paramObject;
    if (paramObject == null)
      return false; 
    if (this.chainId != ((RadioChainInfo)paramObject).chainId || this.level != ((RadioChainInfo)paramObject).level)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.chainId), Integer.valueOf(this.level) });
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.chainId);
    paramParcel.writeInt(this.level);
  }
  
  public static final Parcelable.Creator<RadioChainInfo> CREATOR = new Parcelable.Creator<RadioChainInfo>() {
      public RadioChainInfo createFromParcel(Parcel param1Parcel) {
        return new RadioChainInfo(param1Parcel.readInt(), param1Parcel.readInt());
      }
      
      public RadioChainInfo[] newArray(int param1Int) {
        return new RadioChainInfo[param1Int];
      }
    };
  
  private static final String TAG = "RadioChainInfo";
  
  public int chainId;
  
  public int level;
}
