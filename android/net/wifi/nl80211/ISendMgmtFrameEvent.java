package android.net.wifi.nl80211;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISendMgmtFrameEvent extends IInterface {
  public static final int SEND_MGMT_FRAME_ERROR_ALREADY_STARTED = 5;
  
  public static final int SEND_MGMT_FRAME_ERROR_MCS_UNSUPPORTED = 2;
  
  public static final int SEND_MGMT_FRAME_ERROR_NO_ACK = 3;
  
  public static final int SEND_MGMT_FRAME_ERROR_TIMEOUT = 4;
  
  public static final int SEND_MGMT_FRAME_ERROR_UNKNOWN = 1;
  
  void OnAck(int paramInt) throws RemoteException;
  
  void OnFailure(int paramInt) throws RemoteException;
  
  class Default implements ISendMgmtFrameEvent {
    public void OnAck(int param1Int) throws RemoteException {}
    
    public void OnFailure(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISendMgmtFrameEvent {
    private static final String DESCRIPTOR = "android.net.wifi.nl80211.ISendMgmtFrameEvent";
    
    static final int TRANSACTION_OnAck = 1;
    
    static final int TRANSACTION_OnFailure = 2;
    
    public Stub() {
      attachInterface(this, "android.net.wifi.nl80211.ISendMgmtFrameEvent");
    }
    
    public static ISendMgmtFrameEvent asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.wifi.nl80211.ISendMgmtFrameEvent");
      if (iInterface != null && iInterface instanceof ISendMgmtFrameEvent)
        return (ISendMgmtFrameEvent)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "OnFailure";
      } 
      return "OnAck";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.net.wifi.nl80211.ISendMgmtFrameEvent");
          return true;
        } 
        param1Parcel1.enforceInterface("android.net.wifi.nl80211.ISendMgmtFrameEvent");
        param1Int1 = param1Parcel1.readInt();
        OnFailure(param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.net.wifi.nl80211.ISendMgmtFrameEvent");
      param1Int1 = param1Parcel1.readInt();
      OnAck(param1Int1);
      return true;
    }
    
    private static class Proxy implements ISendMgmtFrameEvent {
      public static ISendMgmtFrameEvent sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.wifi.nl80211.ISendMgmtFrameEvent";
      }
      
      public void OnAck(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.wifi.nl80211.ISendMgmtFrameEvent");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ISendMgmtFrameEvent.Stub.getDefaultImpl() != null) {
            ISendMgmtFrameEvent.Stub.getDefaultImpl().OnAck(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void OnFailure(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.wifi.nl80211.ISendMgmtFrameEvent");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ISendMgmtFrameEvent.Stub.getDefaultImpl() != null) {
            ISendMgmtFrameEvent.Stub.getDefaultImpl().OnFailure(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISendMgmtFrameEvent param1ISendMgmtFrameEvent) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISendMgmtFrameEvent != null) {
          Proxy.sDefaultImpl = param1ISendMgmtFrameEvent;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISendMgmtFrameEvent getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
