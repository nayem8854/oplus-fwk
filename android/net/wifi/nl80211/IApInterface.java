package android.net.wifi.nl80211;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IApInterface extends IInterface {
  public static final int ENCRYPTION_TYPE_NONE = 0;
  
  public static final int ENCRYPTION_TYPE_WPA = 1;
  
  public static final int ENCRYPTION_TYPE_WPA2 = 2;
  
  boolean blockClient(String paramString, boolean paramBoolean) throws RemoteException;
  
  boolean blockSavedClients(String[] paramArrayOfString) throws RemoteException;
  
  String getInterfaceName() throws RemoteException;
  
  boolean registerCallback(IApInterfaceEventCallback paramIApInterfaceEventCallback) throws RemoteException;
  
  boolean setMaxClient(int paramInt) throws RemoteException;
  
  class Default implements IApInterface {
    public boolean registerCallback(IApInterfaceEventCallback param1IApInterfaceEventCallback) throws RemoteException {
      return false;
    }
    
    public String getInterfaceName() throws RemoteException {
      return null;
    }
    
    public boolean setMaxClient(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean blockClient(String param1String, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean blockSavedClients(String[] param1ArrayOfString) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IApInterface {
    private static final String DESCRIPTOR = "android.net.wifi.nl80211.IApInterface";
    
    static final int TRANSACTION_blockClient = 4;
    
    static final int TRANSACTION_blockSavedClients = 5;
    
    static final int TRANSACTION_getInterfaceName = 2;
    
    static final int TRANSACTION_registerCallback = 1;
    
    static final int TRANSACTION_setMaxClient = 3;
    
    public Stub() {
      attachInterface(this, "android.net.wifi.nl80211.IApInterface");
    }
    
    public static IApInterface asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.wifi.nl80211.IApInterface");
      if (iInterface != null && iInterface instanceof IApInterface)
        return (IApInterface)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "blockSavedClients";
            } 
            return "blockClient";
          } 
          return "setMaxClient";
        } 
        return "getInterfaceName";
      } 
      return "registerCallback";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1) {
        String[] arrayOfString;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            boolean bool3;
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.net.wifi.nl80211.IApInterface");
                return true;
              } 
              param1Parcel1.enforceInterface("android.net.wifi.nl80211.IApInterface");
              arrayOfString = param1Parcel1.createStringArray();
              boolean bool4 = blockSavedClients(arrayOfString);
              param1Parcel2.writeNoException();
              param1Parcel2.writeInt(bool4);
              return true;
            } 
            arrayOfString.enforceInterface("android.net.wifi.nl80211.IApInterface");
            String str1 = arrayOfString.readString();
            if (arrayOfString.readInt() != 0) {
              bool3 = true;
            } else {
              bool3 = false;
            } 
            boolean bool2 = blockClient(str1, bool3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          } 
          arrayOfString.enforceInterface("android.net.wifi.nl80211.IApInterface");
          param1Int1 = arrayOfString.readInt();
          boolean bool1 = setMaxClient(param1Int1);
          param1Parcel2.writeNoException();
          param1Parcel2.writeInt(bool1);
          return true;
        } 
        arrayOfString.enforceInterface("android.net.wifi.nl80211.IApInterface");
        str = getInterfaceName();
        param1Parcel2.writeNoException();
        param1Parcel2.writeString(str);
        return true;
      } 
      str.enforceInterface("android.net.wifi.nl80211.IApInterface");
      IApInterfaceEventCallback iApInterfaceEventCallback = IApInterfaceEventCallback.Stub.asInterface(str.readStrongBinder());
      boolean bool = registerCallback(iApInterfaceEventCallback);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements IApInterface {
      public static IApInterface sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.wifi.nl80211.IApInterface";
      }
      
      public boolean registerCallback(IApInterfaceEventCallback param2IApInterfaceEventCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IApInterface");
          if (param2IApInterfaceEventCallback != null) {
            iBinder = param2IApInterfaceEventCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IApInterface.Stub.getDefaultImpl() != null) {
            bool1 = IApInterface.Stub.getDefaultImpl().registerCallback(param2IApInterfaceEventCallback);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getInterfaceName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IApInterface");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IApInterface.Stub.getDefaultImpl() != null)
            return IApInterface.Stub.getDefaultImpl().getInterfaceName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setMaxClient(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IApInterface");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IApInterface.Stub.getDefaultImpl() != null) {
            bool1 = IApInterface.Stub.getDefaultImpl().setMaxClient(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean blockClient(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IApInterface");
          parcel1.writeString(param2String);
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool1 && IApInterface.Stub.getDefaultImpl() != null) {
            param2Boolean = IApInterface.Stub.getDefaultImpl().blockClient(param2String, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean blockSavedClients(String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IApInterface");
          parcel1.writeStringArray(param2ArrayOfString);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IApInterface.Stub.getDefaultImpl() != null) {
            bool1 = IApInterface.Stub.getDefaultImpl().blockSavedClients(param2ArrayOfString);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IApInterface param1IApInterface) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IApInterface != null) {
          Proxy.sDefaultImpl = param1IApInterface;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IApInterface getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
