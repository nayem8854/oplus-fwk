package android.net.wifi.nl80211;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IInterfaceEventCallback extends IInterface {
  void OnApInterfaceReady(IApInterface paramIApInterface) throws RemoteException;
  
  void OnApTorndownEvent(IApInterface paramIApInterface) throws RemoteException;
  
  void OnClientInterfaceReady(IClientInterface paramIClientInterface) throws RemoteException;
  
  void OnClientTorndownEvent(IClientInterface paramIClientInterface) throws RemoteException;
  
  class Default implements IInterfaceEventCallback {
    public void OnClientInterfaceReady(IClientInterface param1IClientInterface) throws RemoteException {}
    
    public void OnApInterfaceReady(IApInterface param1IApInterface) throws RemoteException {}
    
    public void OnClientTorndownEvent(IClientInterface param1IClientInterface) throws RemoteException {}
    
    public void OnApTorndownEvent(IApInterface param1IApInterface) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInterfaceEventCallback {
    private static final String DESCRIPTOR = "android.net.wifi.nl80211.IInterfaceEventCallback";
    
    static final int TRANSACTION_OnApInterfaceReady = 2;
    
    static final int TRANSACTION_OnApTorndownEvent = 4;
    
    static final int TRANSACTION_OnClientInterfaceReady = 1;
    
    static final int TRANSACTION_OnClientTorndownEvent = 3;
    
    public Stub() {
      attachInterface(this, "android.net.wifi.nl80211.IInterfaceEventCallback");
    }
    
    public static IInterfaceEventCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.wifi.nl80211.IInterfaceEventCallback");
      if (iInterface != null && iInterface instanceof IInterfaceEventCallback)
        return (IInterfaceEventCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "OnApTorndownEvent";
          } 
          return "OnClientTorndownEvent";
        } 
        return "OnApInterfaceReady";
      } 
      return "OnClientInterfaceReady";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IApInterface iApInterface;
      if (param1Int1 != 1) {
        IClientInterface iClientInterface1;
        if (param1Int1 != 2) {
          IApInterface iApInterface1;
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.net.wifi.nl80211.IInterfaceEventCallback");
              return true;
            } 
            param1Parcel1.enforceInterface("android.net.wifi.nl80211.IInterfaceEventCallback");
            iApInterface1 = IApInterface.Stub.asInterface(param1Parcel1.readStrongBinder());
            OnApTorndownEvent(iApInterface1);
            return true;
          } 
          iApInterface1.enforceInterface("android.net.wifi.nl80211.IInterfaceEventCallback");
          iClientInterface1 = IClientInterface.Stub.asInterface(iApInterface1.readStrongBinder());
          OnClientTorndownEvent(iClientInterface1);
          return true;
        } 
        iClientInterface1.enforceInterface("android.net.wifi.nl80211.IInterfaceEventCallback");
        iApInterface = IApInterface.Stub.asInterface(iClientInterface1.readStrongBinder());
        OnApInterfaceReady(iApInterface);
        return true;
      } 
      iApInterface.enforceInterface("android.net.wifi.nl80211.IInterfaceEventCallback");
      IClientInterface iClientInterface = IClientInterface.Stub.asInterface(iApInterface.readStrongBinder());
      OnClientInterfaceReady(iClientInterface);
      return true;
    }
    
    private static class Proxy implements IInterfaceEventCallback {
      public static IInterfaceEventCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.wifi.nl80211.IInterfaceEventCallback";
      }
      
      public void OnClientInterfaceReady(IClientInterface param2IClientInterface) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.net.wifi.nl80211.IInterfaceEventCallback");
          if (param2IClientInterface != null) {
            iBinder = param2IClientInterface.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IInterfaceEventCallback.Stub.getDefaultImpl() != null) {
            IInterfaceEventCallback.Stub.getDefaultImpl().OnClientInterfaceReady(param2IClientInterface);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void OnApInterfaceReady(IApInterface param2IApInterface) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.net.wifi.nl80211.IInterfaceEventCallback");
          if (param2IApInterface != null) {
            iBinder = param2IApInterface.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IInterfaceEventCallback.Stub.getDefaultImpl() != null) {
            IInterfaceEventCallback.Stub.getDefaultImpl().OnApInterfaceReady(param2IApInterface);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void OnClientTorndownEvent(IClientInterface param2IClientInterface) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.net.wifi.nl80211.IInterfaceEventCallback");
          if (param2IClientInterface != null) {
            iBinder = param2IClientInterface.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IInterfaceEventCallback.Stub.getDefaultImpl() != null) {
            IInterfaceEventCallback.Stub.getDefaultImpl().OnClientTorndownEvent(param2IClientInterface);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void OnApTorndownEvent(IApInterface param2IApInterface) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.net.wifi.nl80211.IInterfaceEventCallback");
          if (param2IApInterface != null) {
            iBinder = param2IApInterface.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IInterfaceEventCallback.Stub.getDefaultImpl() != null) {
            IInterfaceEventCallback.Stub.getDefaultImpl().OnApTorndownEvent(param2IApInterface);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInterfaceEventCallback param1IInterfaceEventCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInterfaceEventCallback != null) {
          Proxy.sDefaultImpl = param1IInterfaceEventCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInterfaceEventCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
