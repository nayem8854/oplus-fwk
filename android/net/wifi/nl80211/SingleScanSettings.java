package android.net.wifi.nl80211;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.util.ArrayList;
import java.util.Objects;

public class SingleScanSettings implements Parcelable {
  public boolean equals(Object<HiddenNetwork> paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof SingleScanSettings))
      return false; 
    paramObject = paramObject;
    if (paramObject == null)
      return false; 
    if (this.scanType == ((SingleScanSettings)paramObject).scanType) {
      ArrayList<ChannelSettings> arrayList1 = this.channelSettings, arrayList2 = ((SingleScanSettings)paramObject).channelSettings;
      if (arrayList1.equals(arrayList2)) {
        ArrayList<HiddenNetwork> arrayList = this.hiddenNetworks;
        paramObject = (Object<HiddenNetwork>)((SingleScanSettings)paramObject).hiddenNetworks;
        if (arrayList.equals(paramObject))
          return null; 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.scanType), this.channelSettings, this.hiddenNetworks });
  }
  
  public int describeContents() {
    return 0;
  }
  
  private static boolean isValidScanType(int paramInt) {
    boolean bool1 = true, bool2 = bool1;
    if (paramInt != 0) {
      bool2 = bool1;
      if (paramInt != 1)
        if (paramInt == 2) {
          bool2 = bool1;
        } else {
          bool2 = false;
        }  
    } 
    return bool2;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (!isValidScanType(this.scanType)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid scan type ");
      stringBuilder.append(this.scanType);
      Log.wtf("SingleScanSettings", stringBuilder.toString());
    } 
    paramParcel.writeInt(this.scanType);
    paramParcel.writeTypedList(this.channelSettings);
    paramParcel.writeTypedList(this.hiddenNetworks);
  }
  
  public static final Parcelable.Creator<SingleScanSettings> CREATOR = new Parcelable.Creator<SingleScanSettings>() {
      public SingleScanSettings createFromParcel(Parcel param1Parcel) {
        SingleScanSettings singleScanSettings = new SingleScanSettings();
        singleScanSettings.scanType = param1Parcel.readInt();
        if (!SingleScanSettings.isValidScanType(singleScanSettings.scanType)) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Invalid scan type ");
          stringBuilder.append(singleScanSettings.scanType);
          Log.wtf("SingleScanSettings", stringBuilder.toString());
        } 
        singleScanSettings.channelSettings = new ArrayList<>();
        param1Parcel.readTypedList(singleScanSettings.channelSettings, ChannelSettings.CREATOR);
        singleScanSettings.hiddenNetworks = new ArrayList<>();
        param1Parcel.readTypedList(singleScanSettings.hiddenNetworks, HiddenNetwork.CREATOR);
        if (param1Parcel.dataAvail() != 0)
          Log.e("SingleScanSettings", "Found trailing data after parcel parsing."); 
        return singleScanSettings;
      }
      
      public SingleScanSettings[] newArray(int param1Int) {
        return new SingleScanSettings[param1Int];
      }
    };
  
  private static final String TAG = "SingleScanSettings";
  
  public ArrayList<ChannelSettings> channelSettings;
  
  public ArrayList<HiddenNetwork> hiddenNetworks;
  
  public int scanType;
}
