package android.net.wifi.nl80211;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

public class HiddenNetwork implements Parcelable {
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof HiddenNetwork))
      return false; 
    paramObject = paramObject;
    return Arrays.equals(this.ssid, ((HiddenNetwork)paramObject).ssid);
  }
  
  public int hashCode() {
    return Arrays.hashCode(this.ssid);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByteArray(this.ssid);
  }
  
  public static final Parcelable.Creator<HiddenNetwork> CREATOR = new Parcelable.Creator<HiddenNetwork>() {
      public HiddenNetwork createFromParcel(Parcel param1Parcel) {
        HiddenNetwork hiddenNetwork = new HiddenNetwork();
        hiddenNetwork.ssid = param1Parcel.createByteArray();
        return hiddenNetwork;
      }
      
      public HiddenNetwork[] newArray(int param1Int) {
        return new HiddenNetwork[param1Int];
      }
    };
  
  private static final String TAG = "HiddenNetwork";
  
  public byte[] ssid;
}
