package android.net.wifi.nl80211;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPnoScanEvent extends IInterface {
  void OnPnoNetworkFound() throws RemoteException;
  
  void OnPnoScanFailed() throws RemoteException;
  
  class Default implements IPnoScanEvent {
    public void OnPnoNetworkFound() throws RemoteException {}
    
    public void OnPnoScanFailed() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPnoScanEvent {
    private static final String DESCRIPTOR = "android.net.wifi.nl80211.IPnoScanEvent";
    
    static final int TRANSACTION_OnPnoNetworkFound = 1;
    
    static final int TRANSACTION_OnPnoScanFailed = 2;
    
    public Stub() {
      attachInterface(this, "android.net.wifi.nl80211.IPnoScanEvent");
    }
    
    public static IPnoScanEvent asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.wifi.nl80211.IPnoScanEvent");
      if (iInterface != null && iInterface instanceof IPnoScanEvent)
        return (IPnoScanEvent)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "OnPnoScanFailed";
      } 
      return "OnPnoNetworkFound";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.net.wifi.nl80211.IPnoScanEvent");
          return true;
        } 
        param1Parcel1.enforceInterface("android.net.wifi.nl80211.IPnoScanEvent");
        OnPnoScanFailed();
        return true;
      } 
      param1Parcel1.enforceInterface("android.net.wifi.nl80211.IPnoScanEvent");
      OnPnoNetworkFound();
      return true;
    }
    
    private static class Proxy implements IPnoScanEvent {
      public static IPnoScanEvent sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.wifi.nl80211.IPnoScanEvent";
      }
      
      public void OnPnoNetworkFound() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.wifi.nl80211.IPnoScanEvent");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IPnoScanEvent.Stub.getDefaultImpl() != null) {
            IPnoScanEvent.Stub.getDefaultImpl().OnPnoNetworkFound();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void OnPnoScanFailed() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.net.wifi.nl80211.IPnoScanEvent");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IPnoScanEvent.Stub.getDefaultImpl() != null) {
            IPnoScanEvent.Stub.getDefaultImpl().OnPnoScanFailed();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPnoScanEvent param1IPnoScanEvent) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPnoScanEvent != null) {
          Proxy.sDefaultImpl = param1IPnoScanEvent;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPnoScanEvent getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
