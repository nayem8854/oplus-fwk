package android.net.wifi.nl80211;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@SystemApi
public final class PnoSettings implements Parcelable {
  public long getIntervalMillis() {
    return this.mIntervalMs;
  }
  
  public void setIntervalMillis(long paramLong) {
    this.mIntervalMs = paramLong;
  }
  
  public int getMin2gRssiDbm() {
    return this.mMin2gRssi;
  }
  
  public void setMin2gRssiDbm(int paramInt) {
    this.mMin2gRssi = paramInt;
  }
  
  public int getMin5gRssiDbm() {
    return this.mMin5gRssi;
  }
  
  public void setMin5gRssiDbm(int paramInt) {
    this.mMin5gRssi = paramInt;
  }
  
  public int getMin6gRssiDbm() {
    return this.mMin6gRssi;
  }
  
  public void setMin6gRssiDbm(int paramInt) {
    this.mMin6gRssi = paramInt;
  }
  
  public List<PnoNetwork> getPnoNetworks() {
    return this.mPnoNetworks;
  }
  
  public void setPnoNetworks(List<PnoNetwork> paramList) {
    this.mPnoNetworks = paramList;
  }
  
  public boolean equals(Object<PnoNetwork> paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof PnoSettings))
      return false; 
    PnoSettings pnoSettings = (PnoSettings)paramObject;
    if (pnoSettings == null)
      return false; 
    if (this.mIntervalMs == pnoSettings.mIntervalMs && this.mMin2gRssi == pnoSettings.mMin2gRssi && this.mMin5gRssi == pnoSettings.mMin5gRssi && this.mMin6gRssi == pnoSettings.mMin6gRssi) {
      paramObject = (Object<PnoNetwork>)this.mPnoNetworks;
      List<PnoNetwork> list = pnoSettings.mPnoNetworks;
      if (paramObject.equals(list))
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Long.valueOf(this.mIntervalMs), Integer.valueOf(this.mMin2gRssi), Integer.valueOf(this.mMin5gRssi), Integer.valueOf(this.mMin6gRssi), this.mPnoNetworks });
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mIntervalMs);
    paramParcel.writeInt(this.mMin2gRssi);
    paramParcel.writeInt(this.mMin5gRssi);
    paramParcel.writeInt(this.mMin6gRssi);
    paramParcel.writeTypedList(this.mPnoNetworks);
  }
  
  public static final Parcelable.Creator<PnoSettings> CREATOR = new Parcelable.Creator<PnoSettings>() {
      public PnoSettings createFromParcel(Parcel param1Parcel) {
        PnoSettings pnoSettings = new PnoSettings();
        PnoSettings.access$002(pnoSettings, param1Parcel.readLong());
        PnoSettings.access$102(pnoSettings, param1Parcel.readInt());
        PnoSettings.access$202(pnoSettings, param1Parcel.readInt());
        PnoSettings.access$302(pnoSettings, param1Parcel.readInt());
        PnoSettings.access$402(pnoSettings, new ArrayList());
        param1Parcel.readTypedList(pnoSettings.mPnoNetworks, PnoNetwork.CREATOR);
        return pnoSettings;
      }
      
      public PnoSettings[] newArray(int param1Int) {
        return new PnoSettings[param1Int];
      }
    };
  
  private long mIntervalMs;
  
  private int mMin2gRssi;
  
  private int mMin5gRssi;
  
  private int mMin6gRssi;
  
  private List<PnoNetwork> mPnoNetworks;
}
