package android.net.wifi.nl80211;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IClientInterface extends IInterface {
  void SendMgmtFrame(byte[] paramArrayOfbyte, ISendMgmtFrameEvent paramISendMgmtFrameEvent, int paramInt) throws RemoteException;
  
  String getInterfaceName() throws RemoteException;
  
  byte[] getMacAddress() throws RemoteException;
  
  int[] getPacketCounters() throws RemoteException;
  
  IWifiScannerImpl getWifiScannerImpl() throws RemoteException;
  
  int[] signalPoll() throws RemoteException;
  
  class Default implements IClientInterface {
    public int[] getPacketCounters() throws RemoteException {
      return null;
    }
    
    public int[] signalPoll() throws RemoteException {
      return null;
    }
    
    public byte[] getMacAddress() throws RemoteException {
      return null;
    }
    
    public String getInterfaceName() throws RemoteException {
      return null;
    }
    
    public IWifiScannerImpl getWifiScannerImpl() throws RemoteException {
      return null;
    }
    
    public void SendMgmtFrame(byte[] param1ArrayOfbyte, ISendMgmtFrameEvent param1ISendMgmtFrameEvent, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IClientInterface {
    private static final String DESCRIPTOR = "android.net.wifi.nl80211.IClientInterface";
    
    static final int TRANSACTION_SendMgmtFrame = 6;
    
    static final int TRANSACTION_getInterfaceName = 4;
    
    static final int TRANSACTION_getMacAddress = 3;
    
    static final int TRANSACTION_getPacketCounters = 1;
    
    static final int TRANSACTION_getWifiScannerImpl = 5;
    
    static final int TRANSACTION_signalPoll = 2;
    
    public Stub() {
      attachInterface(this, "android.net.wifi.nl80211.IClientInterface");
    }
    
    public static IClientInterface asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.wifi.nl80211.IClientInterface");
      if (iInterface != null && iInterface instanceof IClientInterface)
        return (IClientInterface)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "SendMgmtFrame";
        case 5:
          return "getWifiScannerImpl";
        case 4:
          return "getInterfaceName";
        case 3:
          return "getMacAddress";
        case 2:
          return "signalPoll";
        case 1:
          break;
      } 
      return "getPacketCounters";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      byte[] arrayOfByte;
      if (param1Int1 != 1598968902) {
        IWifiScannerImpl iWifiScannerImpl;
        String str;
        byte[] arrayOfByte1;
        ISendMgmtFrameEvent iSendMgmtFrameEvent;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("android.net.wifi.nl80211.IClientInterface");
            arrayOfByte = param1Parcel1.createByteArray();
            iSendMgmtFrameEvent = ISendMgmtFrameEvent.Stub.asInterface(param1Parcel1.readStrongBinder());
            param1Int1 = param1Parcel1.readInt();
            SendMgmtFrame(arrayOfByte, iSendMgmtFrameEvent, param1Int1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.net.wifi.nl80211.IClientInterface");
            iWifiScannerImpl = getWifiScannerImpl();
            arrayOfByte.writeNoException();
            if (iWifiScannerImpl != null) {
              IBinder iBinder = iWifiScannerImpl.asBinder();
            } else {
              iWifiScannerImpl = null;
            } 
            arrayOfByte.writeStrongBinder((IBinder)iWifiScannerImpl);
            return true;
          case 4:
            iWifiScannerImpl.enforceInterface("android.net.wifi.nl80211.IClientInterface");
            str = getInterfaceName();
            arrayOfByte.writeNoException();
            arrayOfByte.writeString(str);
            return true;
          case 3:
            str.enforceInterface("android.net.wifi.nl80211.IClientInterface");
            arrayOfByte1 = getMacAddress();
            arrayOfByte.writeNoException();
            arrayOfByte.writeByteArray(arrayOfByte1);
            return true;
          case 2:
            arrayOfByte1.enforceInterface("android.net.wifi.nl80211.IClientInterface");
            arrayOfInt = signalPoll();
            arrayOfByte.writeNoException();
            arrayOfByte.writeIntArray(arrayOfInt);
            return true;
          case 1:
            break;
        } 
        arrayOfInt.enforceInterface("android.net.wifi.nl80211.IClientInterface");
        int[] arrayOfInt = getPacketCounters();
        arrayOfByte.writeNoException();
        arrayOfByte.writeIntArray(arrayOfInt);
        return true;
      } 
      arrayOfByte.writeString("android.net.wifi.nl80211.IClientInterface");
      return true;
    }
    
    private static class Proxy implements IClientInterface {
      public static IClientInterface sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.wifi.nl80211.IClientInterface";
      }
      
      public int[] getPacketCounters() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IClientInterface");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IClientInterface.Stub.getDefaultImpl() != null)
            return IClientInterface.Stub.getDefaultImpl().getPacketCounters(); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] signalPoll() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IClientInterface");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IClientInterface.Stub.getDefaultImpl() != null)
            return IClientInterface.Stub.getDefaultImpl().signalPoll(); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getMacAddress() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IClientInterface");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IClientInterface.Stub.getDefaultImpl() != null)
            return IClientInterface.Stub.getDefaultImpl().getMacAddress(); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getInterfaceName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IClientInterface");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IClientInterface.Stub.getDefaultImpl() != null)
            return IClientInterface.Stub.getDefaultImpl().getInterfaceName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IWifiScannerImpl getWifiScannerImpl() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.wifi.nl80211.IClientInterface");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IClientInterface.Stub.getDefaultImpl() != null)
            return IClientInterface.Stub.getDefaultImpl().getWifiScannerImpl(); 
          parcel2.readException();
          return IWifiScannerImpl.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void SendMgmtFrame(byte[] param2ArrayOfbyte, ISendMgmtFrameEvent param2ISendMgmtFrameEvent, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.net.wifi.nl80211.IClientInterface");
          parcel.writeByteArray(param2ArrayOfbyte);
          if (param2ISendMgmtFrameEvent != null) {
            iBinder = param2ISendMgmtFrameEvent.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IClientInterface.Stub.getDefaultImpl() != null) {
            IClientInterface.Stub.getDefaultImpl().SendMgmtFrame(param2ArrayOfbyte, param2ISendMgmtFrameEvent, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IClientInterface param1IClientInterface) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IClientInterface != null) {
          Proxy.sDefaultImpl = param1IClientInterface;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IClientInterface getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
