package android.net.wifi.nl80211;

import android.annotation.SystemApi;
import android.net.MacAddress;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SystemApi
public final class NativeScanResult implements Parcelable {
  public static final int BSS_CAPABILITY_APSD = 2048;
  
  public static final int BSS_CAPABILITY_CF_POLLABLE = 4;
  
  public static final int BSS_CAPABILITY_CF_POLL_REQUEST = 8;
  
  public static final int BSS_CAPABILITY_CHANNEL_AGILITY = 128;
  
  public static final int BSS_CAPABILITY_DELAYED_BLOCK_ACK = 16384;
  
  public static final int BSS_CAPABILITY_DSSS_OFDM = 8192;
  
  public static final int BSS_CAPABILITY_ESS = 1;
  
  public static final int BSS_CAPABILITY_IBSS = 2;
  
  public static final int BSS_CAPABILITY_IMMEDIATE_BLOCK_ACK = 32768;
  
  public static final int BSS_CAPABILITY_PBCC = 64;
  
  public static final int BSS_CAPABILITY_PRIVACY = 16;
  
  public static final int BSS_CAPABILITY_QOS = 512;
  
  public static final int BSS_CAPABILITY_RADIO_MANAGEMENT = 4096;
  
  public static final int BSS_CAPABILITY_SHORT_PREAMBLE = 32;
  
  public static final int BSS_CAPABILITY_SHORT_SLOT_TIME = 1024;
  
  public static final int BSS_CAPABILITY_SPECTRUM_MANAGEMENT = 256;
  
  public byte[] getSsid() {
    return this.ssid;
  }
  
  public MacAddress getBssid() {
    try {
      return MacAddress.fromBytes(this.bssid);
    } catch (IllegalArgumentException illegalArgumentException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Illegal argument ");
      stringBuilder.append(Arrays.toString(this.bssid));
      Log.e("NativeScanResult", stringBuilder.toString(), illegalArgumentException);
      return null;
    } 
  }
  
  public byte[] getInformationElements() {
    return this.infoElement;
  }
  
  public int getFrequencyMhz() {
    return this.frequency;
  }
  
  public int getSignalMbm() {
    return this.signalMbm;
  }
  
  public long getTsf() {
    return this.tsf;
  }
  
  public boolean isAssociated() {
    return this.associated;
  }
  
  public int getCapabilities() {
    return this.capability;
  }
  
  public List<RadioChainInfo> getRadioChainInfos() {
    return this.radioChainInfos;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByteArray(this.ssid);
    paramParcel.writeByteArray(this.bssid);
    paramParcel.writeByteArray(this.infoElement);
    paramParcel.writeInt(this.frequency);
    paramParcel.writeInt(this.signalMbm);
    paramParcel.writeLong(this.tsf);
    paramParcel.writeInt(this.capability);
    paramParcel.writeInt(this.associated);
    paramParcel.writeTypedList(this.radioChainInfos);
  }
  
  public static final Parcelable.Creator<NativeScanResult> CREATOR = new Parcelable.Creator<NativeScanResult>() {
      public NativeScanResult createFromParcel(Parcel param1Parcel) {
        NativeScanResult nativeScanResult = new NativeScanResult();
        nativeScanResult.ssid = param1Parcel.createByteArray();
        byte[] arrayOfByte = nativeScanResult.ssid;
        boolean bool = false;
        if (arrayOfByte == null)
          nativeScanResult.ssid = new byte[0]; 
        nativeScanResult.bssid = param1Parcel.createByteArray();
        if (nativeScanResult.bssid == null)
          nativeScanResult.bssid = new byte[0]; 
        nativeScanResult.infoElement = param1Parcel.createByteArray();
        if (nativeScanResult.infoElement == null)
          nativeScanResult.infoElement = new byte[0]; 
        nativeScanResult.frequency = param1Parcel.readInt();
        nativeScanResult.signalMbm = param1Parcel.readInt();
        nativeScanResult.tsf = param1Parcel.readLong();
        nativeScanResult.capability = param1Parcel.readInt();
        if (param1Parcel.readInt() != 0)
          bool = true; 
        nativeScanResult.associated = bool;
        nativeScanResult.radioChainInfos = new ArrayList<>();
        param1Parcel.readTypedList(nativeScanResult.radioChainInfos, RadioChainInfo.CREATOR);
        return nativeScanResult;
      }
      
      public NativeScanResult[] newArray(int param1Int) {
        return new NativeScanResult[param1Int];
      }
    };
  
  private static final String TAG = "NativeScanResult";
  
  public boolean associated;
  
  public byte[] bssid;
  
  public int capability;
  
  public int frequency;
  
  public byte[] infoElement;
  
  public List<RadioChainInfo> radioChainInfos;
  
  public int signalMbm;
  
  public byte[] ssid;
  
  public long tsf;
  
  @Retention(RetentionPolicy.SOURCE)
  class BssCapabilityBits implements Annotation {}
}
