package android.net.wifi;

import android.os.Parcel;
import android.os.Parcelable;

public class WpsResult implements Parcelable {
  class Status extends Enum<Status> {
    private static final Status[] $VALUES;
    
    public static final Status FAILURE = new Status("FAILURE", 1);
    
    public static final Status IN_PROGRESS;
    
    public static Status[] values() {
      return (Status[])$VALUES.clone();
    }
    
    public static Status valueOf(String param1String) {
      return Enum.<Status>valueOf(Status.class, param1String);
    }
    
    private Status(WpsResult this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static final Status SUCCESS = new Status("SUCCESS", 0);
    
    static {
      Status status = new Status("IN_PROGRESS", 2);
      $VALUES = new Status[] { SUCCESS, FAILURE, status };
    }
  }
  
  public WpsResult() {
    this.status = Status.FAILURE;
    this.pin = null;
  }
  
  public WpsResult(Status paramStatus) {
    this.status = paramStatus;
    this.pin = null;
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(" status: ");
    stringBuffer.append(this.status.toString());
    stringBuffer.append('\n');
    stringBuffer.append(" pin: ");
    stringBuffer.append(this.pin);
    stringBuffer.append("\n");
    return stringBuffer.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public WpsResult(WpsResult paramWpsResult) {
    if (paramWpsResult != null) {
      this.status = paramWpsResult.status;
      this.pin = paramWpsResult.pin;
    } 
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.status.name());
    paramParcel.writeString(this.pin);
  }
  
  public static final Parcelable.Creator<WpsResult> CREATOR = new Parcelable.Creator<WpsResult>() {
      public WpsResult createFromParcel(Parcel param1Parcel) {
        WpsResult wpsResult = new WpsResult();
        wpsResult.status = WpsResult.Status.valueOf(param1Parcel.readString());
        wpsResult.pin = param1Parcel.readString();
        return wpsResult;
      }
      
      public WpsResult[] newArray(int param1Int) {
        return new WpsResult[param1Int];
      }
    };
  
  public String pin;
  
  public Status status;
}
