package android.net.wifi;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemProperties;
import android.provider.Settings;
import android.util.Log;
import java.util.HashMap;
import java.util.Iterator;

public class OplusWifiRomUpdateHelper extends OplusRomUpdateHelper implements IWifiRomUpdateHelper {
  private static final boolean sIsQcomPlatform = SystemProperties.get("ro.boot.hardware", "oppo").toLowerCase().startsWith("qcom");
  
  static {
    sIsMTKPlatform = SystemProperties.get("ro.boot.hardware", "oppo").toLowerCase().startsWith("mt");
  }
  
  private boolean DEBUG = false;
  
  private HashMap<String, String> mKeyValuePair = new HashMap<>();
  
  private String[] mDualStaDisableMcc = null;
  
  public static final String BROADCAST_ACTION_ROM_UPDATE_CONFIG_SUCCES = "oppo.intent.action.ROM_UPDATE_CONFIG_SUCCESS";
  
  private static final String DATA_FILE_PATH = "/data/misc/wifi/sys_wifi_par_config_list.xml";
  
  private static final String FILE_NAME = "sys_wifi_par_config_list";
  
  private static final String MTK_SMART_BW_PARAMS = "MTK_SMART_BW_PARAMS";
  
  private static final String OPPO_COMPONENT_SAFE_PERMISSION = "oppo.permission.OPPO_COMPONENT_SAFE";
  
  public static final String ROM_UPDATE_CONFIG_LIST = "ROM_UPDATE_CONFIG_LIST";
  
  private static final String SMART_BW_PARAMS = "SMART_BW_PARAMS";
  
  private static final String SYS_FILE_PATH = "/system_ext/etc/sys_wifi_par_config_list.xml";
  
  private static final String TAG = "OplusWifiRomUpdateHelper";
  
  private static final boolean WIFI_ASSISTANT_DISABLE = false;
  
  private static final int WIFI_ASSISTANT_DISABLE_VALUE = 0;
  
  private static final boolean WIFI_ASSISTANT_ENABLE = true;
  
  private static final int WIFI_ASSISTANT_ENABLE_VALUE = 1;
  
  private static final String WIFI_ASSISTANT_ROMUPDATE = "rom.update.wifi.assistant";
  
  private static OplusWifiRomUpdateHelper sInstance;
  
  private static final boolean sIsMTKPlatform;
  
  private String[] mDownloadApps;
  
  private String[] mDualStaApps;
  
  private String[] mDualStaAppsExp;
  
  private String[] mDualStaBlackList;
  
  private String[] mDualStaCapHostBlackList;
  
  private String[] mLmParams;
  
  private int[] mMtkSmartBWParams;
  
  private String[] mMtuServer;
  
  private String[] mRouterBoostDupPktGameBlackList;
  
  private String[] mRouterBoostDupPktGameWhiteList;
  
  private String[] mSkipDestroySocketApps;
  
  private String[] mSlaApps;
  
  private String[] mSlaAppsExp;
  
  private String[] mSlaBlackList;
  
  private String[] mSlaEnabledMCC;
  
  private String[] mSlaGameApps;
  
  private String[] mSlaGameAppsExp;
  
  private String[] mSlaGameParams;
  
  private String[] mSlaParams;
  
  private int[] mSmartBWParams;
  
  private int[] mSpeedRttParams;
  
  private String[] mVideoApps;
  
  public class WifiRomUpdateInfo extends OplusRomUpdateHelper.UpdateInfo {
    final OplusWifiRomUpdateHelper this$0;
    
    public void parseContentFromXML(String param1String) {
      // Byte code:
      //   0: aload_0
      //   1: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   4: invokestatic access$000 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;)Z
      //   7: ifeq -> 18
      //   10: ldc 'OplusWifiRomUpdateHelper'
      //   12: ldc 'parseContentFromXML'
      //   14: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   17: pop
      //   18: aload_1
      //   19: ifnonnull -> 31
      //   22: ldc 'OplusWifiRomUpdateHelper'
      //   24: ldc '\\tcontent is null'
      //   26: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   29: pop
      //   30: return
      //   31: aconst_null
      //   32: astore_2
      //   33: aconst_null
      //   34: astore_3
      //   35: aconst_null
      //   36: astore #4
      //   38: aload #4
      //   40: astore #5
      //   42: aload_2
      //   43: astore #6
      //   45: aload_3
      //   46: astore #7
      //   48: invokestatic newPullParser : ()Lorg/xmlpull/v1/XmlPullParser;
      //   51: astore #8
      //   53: aload #4
      //   55: astore #5
      //   57: aload_2
      //   58: astore #6
      //   60: aload_3
      //   61: astore #7
      //   63: new java/io/StringReader
      //   66: astore #9
      //   68: aload #4
      //   70: astore #5
      //   72: aload_2
      //   73: astore #6
      //   75: aload_3
      //   76: astore #7
      //   78: aload #9
      //   80: aload_1
      //   81: invokespecial <init> : (Ljava/lang/String;)V
      //   84: aload #9
      //   86: astore_1
      //   87: aload_1
      //   88: astore #5
      //   90: aload_1
      //   91: astore #6
      //   93: aload_1
      //   94: astore #7
      //   96: aload #8
      //   98: aload_1
      //   99: invokeinterface setInput : (Ljava/io/Reader;)V
      //   104: aload_1
      //   105: astore #5
      //   107: aload_1
      //   108: astore #6
      //   110: aload_1
      //   111: astore #7
      //   113: aload #8
      //   115: invokeinterface getEventType : ()I
      //   120: istore #10
      //   122: iload #10
      //   124: iconst_1
      //   125: if_icmpeq -> 1441
      //   128: iload #10
      //   130: iconst_2
      //   131: if_icmpeq -> 137
      //   134: goto -> 1420
      //   137: aload_1
      //   138: astore #5
      //   140: aload_1
      //   141: astore #6
      //   143: aload_1
      //   144: astore #7
      //   146: aload #8
      //   148: invokeinterface getName : ()Ljava/lang/String;
      //   153: astore #4
      //   155: aload_1
      //   156: astore #5
      //   158: aload_1
      //   159: astore #6
      //   161: aload_1
      //   162: astore #7
      //   164: aload #8
      //   166: invokeinterface next : ()I
      //   171: pop
      //   172: aload_1
      //   173: astore #5
      //   175: aload_1
      //   176: astore #6
      //   178: aload_1
      //   179: astore #7
      //   181: aload #8
      //   183: invokeinterface getText : ()Ljava/lang/String;
      //   188: astore #9
      //   190: aload_1
      //   191: astore #5
      //   193: aload_1
      //   194: astore #6
      //   196: aload_1
      //   197: astore #7
      //   199: aload_0
      //   200: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   203: invokestatic access$000 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;)Z
      //   206: ifeq -> 318
      //   209: aload_1
      //   210: astore #5
      //   212: aload_1
      //   213: astore #6
      //   215: aload_1
      //   216: astore #7
      //   218: new java/lang/StringBuilder
      //   221: astore_2
      //   222: aload_1
      //   223: astore #5
      //   225: aload_1
      //   226: astore #6
      //   228: aload_1
      //   229: astore #7
      //   231: aload_2
      //   232: invokespecial <init> : ()V
      //   235: aload_1
      //   236: astore #5
      //   238: aload_1
      //   239: astore #6
      //   241: aload_1
      //   242: astore #7
      //   244: aload_2
      //   245: ldc '\\t'
      //   247: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   250: pop
      //   251: aload_1
      //   252: astore #5
      //   254: aload_1
      //   255: astore #6
      //   257: aload_1
      //   258: astore #7
      //   260: aload_2
      //   261: aload #4
      //   263: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   266: pop
      //   267: aload_1
      //   268: astore #5
      //   270: aload_1
      //   271: astore #6
      //   273: aload_1
      //   274: astore #7
      //   276: aload_2
      //   277: ldc ':'
      //   279: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   282: pop
      //   283: aload_1
      //   284: astore #5
      //   286: aload_1
      //   287: astore #6
      //   289: aload_1
      //   290: astore #7
      //   292: aload_2
      //   293: aload #9
      //   295: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   298: pop
      //   299: aload_1
      //   300: astore #5
      //   302: aload_1
      //   303: astore #6
      //   305: aload_1
      //   306: astore #7
      //   308: ldc 'OplusWifiRomUpdateHelper'
      //   310: aload_2
      //   311: invokevirtual toString : ()Ljava/lang/String;
      //   314: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   317: pop
      //   318: aload_1
      //   319: astore #5
      //   321: aload_1
      //   322: astore #6
      //   324: aload_1
      //   325: astore #7
      //   327: ldc 'NETWORK_SLA_APPS'
      //   329: aload #4
      //   331: invokevirtual equals : (Ljava/lang/Object;)Z
      //   334: istore #11
      //   336: iload #11
      //   338: ifeq -> 368
      //   341: aload_1
      //   342: astore #5
      //   344: aload_1
      //   345: astore #6
      //   347: aload_1
      //   348: astore #7
      //   350: aload_0
      //   351: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   354: aload #9
      //   356: ldc ','
      //   358: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
      //   361: invokestatic access$102 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;[Ljava/lang/String;)[Ljava/lang/String;
      //   364: pop
      //   365: goto -> 1420
      //   368: aload_1
      //   369: astore #5
      //   371: aload_1
      //   372: astore #6
      //   374: aload_1
      //   375: astore #7
      //   377: ldc 'NETWORK_SLA_APPS_EXP'
      //   379: aload #4
      //   381: invokevirtual equals : (Ljava/lang/Object;)Z
      //   384: ifeq -> 414
      //   387: aload_1
      //   388: astore #5
      //   390: aload_1
      //   391: astore #6
      //   393: aload_1
      //   394: astore #7
      //   396: aload_0
      //   397: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   400: aload #9
      //   402: ldc ','
      //   404: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
      //   407: invokestatic access$202 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;[Ljava/lang/String;)[Ljava/lang/String;
      //   410: pop
      //   411: goto -> 1420
      //   414: aload_1
      //   415: astore #5
      //   417: aload_1
      //   418: astore #6
      //   420: aload_1
      //   421: astore #7
      //   423: ldc 'NETWORK_SLA_BLACK_LIST'
      //   425: aload #4
      //   427: invokevirtual equals : (Ljava/lang/Object;)Z
      //   430: ifeq -> 460
      //   433: aload_1
      //   434: astore #5
      //   436: aload_1
      //   437: astore #6
      //   439: aload_1
      //   440: astore #7
      //   442: aload_0
      //   443: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   446: aload #9
      //   448: ldc ','
      //   450: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
      //   453: invokestatic access$302 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;[Ljava/lang/String;)[Ljava/lang/String;
      //   456: pop
      //   457: goto -> 1420
      //   460: aload_1
      //   461: astore #5
      //   463: aload_1
      //   464: astore #6
      //   466: aload_1
      //   467: astore #7
      //   469: ldc 'NETWORK_SLA_GAME_APPS'
      //   471: aload #4
      //   473: invokevirtual equals : (Ljava/lang/Object;)Z
      //   476: ifeq -> 506
      //   479: aload_1
      //   480: astore #5
      //   482: aload_1
      //   483: astore #6
      //   485: aload_1
      //   486: astore #7
      //   488: aload_0
      //   489: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   492: aload #9
      //   494: ldc ','
      //   496: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
      //   499: invokestatic access$402 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;[Ljava/lang/String;)[Ljava/lang/String;
      //   502: pop
      //   503: goto -> 1420
      //   506: aload_1
      //   507: astore #5
      //   509: aload_1
      //   510: astore #6
      //   512: aload_1
      //   513: astore #7
      //   515: ldc 'NETWORK_SLA_GAME_APPS_EXP'
      //   517: aload #4
      //   519: invokevirtual equals : (Ljava/lang/Object;)Z
      //   522: ifeq -> 552
      //   525: aload_1
      //   526: astore #5
      //   528: aload_1
      //   529: astore #6
      //   531: aload_1
      //   532: astore #7
      //   534: aload_0
      //   535: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   538: aload #9
      //   540: ldc ','
      //   542: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
      //   545: invokestatic access$502 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;[Ljava/lang/String;)[Ljava/lang/String;
      //   548: pop
      //   549: goto -> 1420
      //   552: aload_1
      //   553: astore #5
      //   555: aload_1
      //   556: astore #6
      //   558: aload_1
      //   559: astore #7
      //   561: ldc 'NETWORK_SLA_ENABLED_MCC'
      //   563: aload #4
      //   565: invokevirtual equals : (Ljava/lang/Object;)Z
      //   568: ifeq -> 598
      //   571: aload_1
      //   572: astore #5
      //   574: aload_1
      //   575: astore #6
      //   577: aload_1
      //   578: astore #7
      //   580: aload_0
      //   581: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   584: aload #9
      //   586: ldc ','
      //   588: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
      //   591: invokestatic access$602 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;[Ljava/lang/String;)[Ljava/lang/String;
      //   594: pop
      //   595: goto -> 1420
      //   598: aload_1
      //   599: astore #5
      //   601: aload_1
      //   602: astore #6
      //   604: aload_1
      //   605: astore #7
      //   607: ldc 'OPLUS_DUAL_STA_DISABLED_MCC'
      //   609: aload #4
      //   611: invokevirtual equals : (Ljava/lang/Object;)Z
      //   614: ifeq -> 644
      //   617: aload_1
      //   618: astore #5
      //   620: aload_1
      //   621: astore #6
      //   623: aload_1
      //   624: astore #7
      //   626: aload_0
      //   627: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   630: aload #9
      //   632: ldc ','
      //   634: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
      //   637: invokestatic access$702 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;[Ljava/lang/String;)[Ljava/lang/String;
      //   640: pop
      //   641: goto -> 1420
      //   644: aload_1
      //   645: astore #5
      //   647: aload_1
      //   648: astore #6
      //   650: aload_1
      //   651: astore #7
      //   653: ldc 'NETWORK_SLA_PARAMS'
      //   655: aload #4
      //   657: invokevirtual equals : (Ljava/lang/Object;)Z
      //   660: ifeq -> 690
      //   663: aload_1
      //   664: astore #5
      //   666: aload_1
      //   667: astore #6
      //   669: aload_1
      //   670: astore #7
      //   672: aload_0
      //   673: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   676: aload #9
      //   678: ldc ','
      //   680: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
      //   683: invokestatic access$802 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;[Ljava/lang/String;)[Ljava/lang/String;
      //   686: pop
      //   687: goto -> 1420
      //   690: aload_1
      //   691: astore #5
      //   693: aload_1
      //   694: astore #6
      //   696: aload_1
      //   697: astore #7
      //   699: ldc 'NETWORK_SPEED_RTT_PARAMS'
      //   701: aload #4
      //   703: invokevirtual equals : (Ljava/lang/Object;)Z
      //   706: ifeq -> 730
      //   709: aload_1
      //   710: astore #5
      //   712: aload_1
      //   713: astore #6
      //   715: aload_1
      //   716: astore #7
      //   718: aload_0
      //   719: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   722: aload #9
      //   724: invokestatic access$900 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;Ljava/lang/String;)V
      //   727: goto -> 1420
      //   730: aload_1
      //   731: astore #5
      //   733: aload_1
      //   734: astore #6
      //   736: aload_1
      //   737: astore #7
      //   739: ldc 'NETWORK_SLA_GAME_PARAMS'
      //   741: aload #4
      //   743: invokevirtual equals : (Ljava/lang/Object;)Z
      //   746: ifeq -> 776
      //   749: aload_1
      //   750: astore #5
      //   752: aload_1
      //   753: astore #6
      //   755: aload_1
      //   756: astore #7
      //   758: aload_0
      //   759: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   762: aload #9
      //   764: ldc ','
      //   766: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
      //   769: invokestatic access$1002 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;[Ljava/lang/String;)[Ljava/lang/String;
      //   772: pop
      //   773: goto -> 1420
      //   776: aload_1
      //   777: astore #5
      //   779: aload_1
      //   780: astore #6
      //   782: aload_1
      //   783: astore #7
      //   785: ldc 'NETWORK_DUAL_STA_APPS'
      //   787: aload #4
      //   789: invokevirtual equals : (Ljava/lang/Object;)Z
      //   792: ifeq -> 822
      //   795: aload_1
      //   796: astore #5
      //   798: aload_1
      //   799: astore #6
      //   801: aload_1
      //   802: astore #7
      //   804: aload_0
      //   805: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   808: aload #9
      //   810: ldc ','
      //   812: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
      //   815: invokestatic access$1102 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;[Ljava/lang/String;)[Ljava/lang/String;
      //   818: pop
      //   819: goto -> 1420
      //   822: aload_1
      //   823: astore #5
      //   825: aload_1
      //   826: astore #6
      //   828: aload_1
      //   829: astore #7
      //   831: ldc 'NETWORK_DUAL_STA_APPS_EXP'
      //   833: aload #4
      //   835: invokevirtual equals : (Ljava/lang/Object;)Z
      //   838: ifeq -> 868
      //   841: aload_1
      //   842: astore #5
      //   844: aload_1
      //   845: astore #6
      //   847: aload_1
      //   848: astore #7
      //   850: aload_0
      //   851: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   854: aload #9
      //   856: ldc ','
      //   858: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
      //   861: invokestatic access$1202 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;[Ljava/lang/String;)[Ljava/lang/String;
      //   864: pop
      //   865: goto -> 1420
      //   868: aload_1
      //   869: astore #5
      //   871: aload_1
      //   872: astore #6
      //   874: aload_1
      //   875: astore #7
      //   877: ldc 'NETWORK_DUAL_STA_BLACK_LIST'
      //   879: aload #4
      //   881: invokevirtual equals : (Ljava/lang/Object;)Z
      //   884: ifeq -> 914
      //   887: aload_1
      //   888: astore #5
      //   890: aload_1
      //   891: astore #6
      //   893: aload_1
      //   894: astore #7
      //   896: aload_0
      //   897: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   900: aload #9
      //   902: ldc ','
      //   904: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
      //   907: invokestatic access$1302 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;[Ljava/lang/String;)[Ljava/lang/String;
      //   910: pop
      //   911: goto -> 1420
      //   914: aload_1
      //   915: astore #5
      //   917: aload_1
      //   918: astore #6
      //   920: aload_1
      //   921: astore #7
      //   923: ldc 'NETWORK_WECHAT_LM_PARAMS'
      //   925: aload #4
      //   927: invokevirtual equals : (Ljava/lang/Object;)Z
      //   930: ifeq -> 960
      //   933: aload_1
      //   934: astore #5
      //   936: aload_1
      //   937: astore #6
      //   939: aload_1
      //   940: astore #7
      //   942: aload_0
      //   943: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   946: aload #9
      //   948: ldc ','
      //   950: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
      //   953: invokestatic access$1402 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;[Ljava/lang/String;)[Ljava/lang/String;
      //   956: pop
      //   957: goto -> 1420
      //   960: aload_1
      //   961: astore #5
      //   963: aload_1
      //   964: astore #6
      //   966: aload_1
      //   967: astore #7
      //   969: ldc 'NETWORK_VIDEO_APPS'
      //   971: aload #4
      //   973: invokevirtual equals : (Ljava/lang/Object;)Z
      //   976: ifeq -> 1006
      //   979: aload_1
      //   980: astore #5
      //   982: aload_1
      //   983: astore #6
      //   985: aload_1
      //   986: astore #7
      //   988: aload_0
      //   989: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   992: aload #9
      //   994: ldc ','
      //   996: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
      //   999: invokestatic access$1502 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;[Ljava/lang/String;)[Ljava/lang/String;
      //   1002: pop
      //   1003: goto -> 1420
      //   1006: aload_1
      //   1007: astore #5
      //   1009: aload_1
      //   1010: astore #6
      //   1012: aload_1
      //   1013: astore #7
      //   1015: ldc 'NETWORK_DOWNLOAD_APPS'
      //   1017: aload #4
      //   1019: invokevirtual equals : (Ljava/lang/Object;)Z
      //   1022: ifeq -> 1052
      //   1025: aload_1
      //   1026: astore #5
      //   1028: aload_1
      //   1029: astore #6
      //   1031: aload_1
      //   1032: astore #7
      //   1034: aload_0
      //   1035: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   1038: aload #9
      //   1040: ldc ','
      //   1042: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
      //   1045: invokestatic access$1602 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;[Ljava/lang/String;)[Ljava/lang/String;
      //   1048: pop
      //   1049: goto -> 1420
      //   1052: aload_1
      //   1053: astore #5
      //   1055: aload_1
      //   1056: astore #6
      //   1058: aload_1
      //   1059: astore #7
      //   1061: ldc 'NETWORK_DUAL_STA_CAP_HOST_BLACK_LIST'
      //   1063: aload #4
      //   1065: invokevirtual equals : (Ljava/lang/Object;)Z
      //   1068: ifeq -> 1098
      //   1071: aload_1
      //   1072: astore #5
      //   1074: aload_1
      //   1075: astore #6
      //   1077: aload_1
      //   1078: astore #7
      //   1080: aload_0
      //   1081: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   1084: aload #9
      //   1086: ldc ','
      //   1088: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
      //   1091: invokestatic access$1702 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;[Ljava/lang/String;)[Ljava/lang/String;
      //   1094: pop
      //   1095: goto -> 1420
      //   1098: aload_1
      //   1099: astore #5
      //   1101: aload_1
      //   1102: astore #6
      //   1104: aload_1
      //   1105: astore #7
      //   1107: ldc 'NETWORK_SKIP_DESTROY_SOCKET_APPS'
      //   1109: aload #4
      //   1111: invokevirtual equals : (Ljava/lang/Object;)Z
      //   1114: ifeq -> 1144
      //   1117: aload_1
      //   1118: astore #5
      //   1120: aload_1
      //   1121: astore #6
      //   1123: aload_1
      //   1124: astore #7
      //   1126: aload_0
      //   1127: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   1130: aload #9
      //   1132: ldc ','
      //   1134: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
      //   1137: invokestatic access$1802 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;[Ljava/lang/String;)[Ljava/lang/String;
      //   1140: pop
      //   1141: goto -> 1420
      //   1144: aload_1
      //   1145: astore #5
      //   1147: aload_1
      //   1148: astore #6
      //   1150: aload_1
      //   1151: astore #7
      //   1153: ldc 'ROUTERBOOST_DUPPKT_GAME_WHITE_LIST'
      //   1155: aload #4
      //   1157: invokevirtual equals : (Ljava/lang/Object;)Z
      //   1160: ifeq -> 1190
      //   1163: aload_1
      //   1164: astore #5
      //   1166: aload_1
      //   1167: astore #6
      //   1169: aload_1
      //   1170: astore #7
      //   1172: aload_0
      //   1173: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   1176: aload #9
      //   1178: ldc ','
      //   1180: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
      //   1183: invokestatic access$1902 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;[Ljava/lang/String;)[Ljava/lang/String;
      //   1186: pop
      //   1187: goto -> 1420
      //   1190: aload_1
      //   1191: astore #5
      //   1193: aload_1
      //   1194: astore #6
      //   1196: aload_1
      //   1197: astore #7
      //   1199: ldc 'ROUTERBOOST_DUPPKT_GAME_BLACK_LIST'
      //   1201: aload #4
      //   1203: invokevirtual equals : (Ljava/lang/Object;)Z
      //   1206: ifeq -> 1236
      //   1209: aload_1
      //   1210: astore #5
      //   1212: aload_1
      //   1213: astore #6
      //   1215: aload_1
      //   1216: astore #7
      //   1218: aload_0
      //   1219: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   1222: aload #9
      //   1224: ldc ','
      //   1226: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
      //   1229: invokestatic access$2002 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;[Ljava/lang/String;)[Ljava/lang/String;
      //   1232: pop
      //   1233: goto -> 1420
      //   1236: aload_1
      //   1237: astore #5
      //   1239: aload_1
      //   1240: astore #6
      //   1242: aload_1
      //   1243: astore #7
      //   1245: ldc 'DEFAULT_MAC_RANDOMIZATION_SETTING'
      //   1247: aload #4
      //   1249: invokevirtual equals : (Ljava/lang/Object;)Z
      //   1252: ifeq -> 1276
      //   1255: aload_1
      //   1256: astore #5
      //   1258: aload_1
      //   1259: astore #6
      //   1261: aload_1
      //   1262: astore #7
      //   1264: aload_0
      //   1265: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   1268: aload #9
      //   1270: invokestatic access$2100 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;Ljava/lang/String;)V
      //   1273: goto -> 1420
      //   1276: aload_1
      //   1277: astore #5
      //   1279: aload_1
      //   1280: astore #6
      //   1282: aload_1
      //   1283: astore #7
      //   1285: ldc 'OPLUS_WIFI_ASSISTANT_FEATURE'
      //   1287: aload #4
      //   1289: invokevirtual equals : (Ljava/lang/Object;)Z
      //   1292: ifeq -> 1316
      //   1295: aload_1
      //   1296: astore #5
      //   1298: aload_1
      //   1299: astore #6
      //   1301: aload_1
      //   1302: astore #7
      //   1304: aload_0
      //   1305: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   1308: aload #9
      //   1310: invokestatic access$2200 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;Ljava/lang/String;)V
      //   1313: goto -> 1420
      //   1316: aload_1
      //   1317: astore #5
      //   1319: aload_1
      //   1320: astore #6
      //   1322: aload_1
      //   1323: astore #7
      //   1325: ldc 'SMART_BW_PARAMS'
      //   1327: aload #4
      //   1329: invokevirtual equals : (Ljava/lang/Object;)Z
      //   1332: ifeq -> 1356
      //   1335: aload_1
      //   1336: astore #5
      //   1338: aload_1
      //   1339: astore #6
      //   1341: aload_1
      //   1342: astore #7
      //   1344: aload_0
      //   1345: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   1348: aload #9
      //   1350: invokestatic access$2300 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;Ljava/lang/String;)V
      //   1353: goto -> 1420
      //   1356: aload_1
      //   1357: astore #5
      //   1359: aload_1
      //   1360: astore #6
      //   1362: aload_1
      //   1363: astore #7
      //   1365: ldc 'MTK_SMART_BW_PARAMS'
      //   1367: aload #4
      //   1369: invokevirtual equals : (Ljava/lang/Object;)Z
      //   1372: ifeq -> 1396
      //   1375: aload_1
      //   1376: astore #5
      //   1378: aload_1
      //   1379: astore #6
      //   1381: aload_1
      //   1382: astore #7
      //   1384: aload_0
      //   1385: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   1388: aload #9
      //   1390: invokestatic access$2400 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;Ljava/lang/String;)V
      //   1393: goto -> 1420
      //   1396: aload_1
      //   1397: astore #5
      //   1399: aload_1
      //   1400: astore #6
      //   1402: aload_1
      //   1403: astore #7
      //   1405: aload_0
      //   1406: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   1409: invokestatic access$2500 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;)Ljava/util/HashMap;
      //   1412: aload #4
      //   1414: aload #9
      //   1416: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   1419: pop
      //   1420: aload_1
      //   1421: astore #5
      //   1423: aload_1
      //   1424: astore #6
      //   1426: aload_1
      //   1427: astore #7
      //   1429: aload #8
      //   1431: invokeinterface next : ()I
      //   1436: istore #10
      //   1438: goto -> 122
      //   1441: aload_1
      //   1442: invokevirtual close : ()V
      //   1445: goto -> 1504
      //   1448: astore_1
      //   1449: goto -> 1531
      //   1452: astore_1
      //   1453: aload #6
      //   1455: astore #5
      //   1457: aload_0
      //   1458: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   1461: ldc 'Got execption parsing permissions.'
      //   1463: aload_1
      //   1464: invokevirtual log : (Ljava/lang/String;Ljava/lang/Exception;)V
      //   1467: aload #6
      //   1469: ifnull -> 1504
      //   1472: aload #6
      //   1474: astore_1
      //   1475: goto -> 1441
      //   1478: astore_1
      //   1479: aload #7
      //   1481: astore #5
      //   1483: aload_0
      //   1484: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   1487: ldc 'Got execption parsing permissions.'
      //   1489: aload_1
      //   1490: invokevirtual log : (Ljava/lang/String;Ljava/lang/Exception;)V
      //   1493: aload #7
      //   1495: ifnull -> 1504
      //   1498: aload #7
      //   1500: astore_1
      //   1501: goto -> 1441
      //   1504: aload_0
      //   1505: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   1508: invokestatic access$2600 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;)V
      //   1511: aload_0
      //   1512: getfield this$0 : Landroid/net/wifi/OplusWifiRomUpdateHelper;
      //   1515: invokestatic access$000 : (Landroid/net/wifi/OplusWifiRomUpdateHelper;)Z
      //   1518: ifeq -> 1530
      //   1521: ldc 'OplusWifiRomUpdateHelper'
      //   1523: ldc_w '\\txml file parse end!'
      //   1526: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   1529: pop
      //   1530: return
      //   1531: aload #5
      //   1533: ifnull -> 1541
      //   1536: aload #5
      //   1538: invokevirtual close : ()V
      //   1541: aload_1
      //   1542: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #83	-> 0
      //   #84	-> 18
      //   #85	-> 22
      //   #86	-> 30
      //   #88	-> 31
      //   #89	-> 31
      //   #90	-> 31
      //   #92	-> 38
      //   #93	-> 53
      //   #94	-> 87
      //   #95	-> 104
      //   #96	-> 122
      //   #97	-> 128
      //   #101	-> 137
      //   #102	-> 155
      //   #103	-> 172
      //   #104	-> 190
      //   #105	-> 318
      //   #106	-> 341
      //   #107	-> 368
      //   #108	-> 387
      //   #109	-> 414
      //   #110	-> 433
      //   #111	-> 460
      //   #112	-> 479
      //   #113	-> 506
      //   #114	-> 525
      //   #115	-> 552
      //   #116	-> 571
      //   #117	-> 598
      //   #118	-> 617
      //   #119	-> 644
      //   #120	-> 663
      //   #121	-> 690
      //   #122	-> 709
      //   #123	-> 730
      //   #124	-> 749
      //   #125	-> 776
      //   #126	-> 795
      //   #127	-> 822
      //   #128	-> 841
      //   #129	-> 868
      //   #130	-> 887
      //   #131	-> 914
      //   #132	-> 933
      //   #133	-> 960
      //   #134	-> 979
      //   #135	-> 1006
      //   #136	-> 1025
      //   #137	-> 1052
      //   #138	-> 1071
      //   #139	-> 1098
      //   #140	-> 1117
      //   #141	-> 1144
      //   #142	-> 1163
      //   #143	-> 1190
      //   #144	-> 1209
      //   #145	-> 1236
      //   #146	-> 1255
      //   #147	-> 1276
      //   #148	-> 1295
      //   #149	-> 1316
      //   #150	-> 1335
      //   #151	-> 1356
      //   #152	-> 1375
      //   #154	-> 1396
      //   #156	-> 1420
      //   #160	-> 1420
      //   #167	-> 1441
      //   #168	-> 1441
      //   #167	-> 1448
      //   #164	-> 1452
      //   #165	-> 1453
      //   #167	-> 1467
      //   #168	-> 1472
      //   #162	-> 1478
      //   #163	-> 1479
      //   #167	-> 1493
      //   #168	-> 1498
      //   #173	-> 1504
      //   #175	-> 1511
      //   #176	-> 1530
      //   #167	-> 1531
      //   #168	-> 1536
      //   #170	-> 1541
      // Exception table:
      //   from	to	target	type
      //   48	53	1478	org/xmlpull/v1/XmlPullParserException
      //   48	53	1452	java/io/IOException
      //   48	53	1448	finally
      //   63	68	1478	org/xmlpull/v1/XmlPullParserException
      //   63	68	1452	java/io/IOException
      //   63	68	1448	finally
      //   78	84	1478	org/xmlpull/v1/XmlPullParserException
      //   78	84	1452	java/io/IOException
      //   78	84	1448	finally
      //   96	104	1478	org/xmlpull/v1/XmlPullParserException
      //   96	104	1452	java/io/IOException
      //   96	104	1448	finally
      //   113	122	1478	org/xmlpull/v1/XmlPullParserException
      //   113	122	1452	java/io/IOException
      //   113	122	1448	finally
      //   146	155	1478	org/xmlpull/v1/XmlPullParserException
      //   146	155	1452	java/io/IOException
      //   146	155	1448	finally
      //   164	172	1478	org/xmlpull/v1/XmlPullParserException
      //   164	172	1452	java/io/IOException
      //   164	172	1448	finally
      //   181	190	1478	org/xmlpull/v1/XmlPullParserException
      //   181	190	1452	java/io/IOException
      //   181	190	1448	finally
      //   199	209	1478	org/xmlpull/v1/XmlPullParserException
      //   199	209	1452	java/io/IOException
      //   199	209	1448	finally
      //   218	222	1478	org/xmlpull/v1/XmlPullParserException
      //   218	222	1452	java/io/IOException
      //   218	222	1448	finally
      //   231	235	1478	org/xmlpull/v1/XmlPullParserException
      //   231	235	1452	java/io/IOException
      //   231	235	1448	finally
      //   244	251	1478	org/xmlpull/v1/XmlPullParserException
      //   244	251	1452	java/io/IOException
      //   244	251	1448	finally
      //   260	267	1478	org/xmlpull/v1/XmlPullParserException
      //   260	267	1452	java/io/IOException
      //   260	267	1448	finally
      //   276	283	1478	org/xmlpull/v1/XmlPullParserException
      //   276	283	1452	java/io/IOException
      //   276	283	1448	finally
      //   292	299	1478	org/xmlpull/v1/XmlPullParserException
      //   292	299	1452	java/io/IOException
      //   292	299	1448	finally
      //   308	318	1478	org/xmlpull/v1/XmlPullParserException
      //   308	318	1452	java/io/IOException
      //   308	318	1448	finally
      //   327	336	1478	org/xmlpull/v1/XmlPullParserException
      //   327	336	1452	java/io/IOException
      //   327	336	1448	finally
      //   350	365	1478	org/xmlpull/v1/XmlPullParserException
      //   350	365	1452	java/io/IOException
      //   350	365	1448	finally
      //   377	387	1478	org/xmlpull/v1/XmlPullParserException
      //   377	387	1452	java/io/IOException
      //   377	387	1448	finally
      //   396	411	1478	org/xmlpull/v1/XmlPullParserException
      //   396	411	1452	java/io/IOException
      //   396	411	1448	finally
      //   423	433	1478	org/xmlpull/v1/XmlPullParserException
      //   423	433	1452	java/io/IOException
      //   423	433	1448	finally
      //   442	457	1478	org/xmlpull/v1/XmlPullParserException
      //   442	457	1452	java/io/IOException
      //   442	457	1448	finally
      //   469	479	1478	org/xmlpull/v1/XmlPullParserException
      //   469	479	1452	java/io/IOException
      //   469	479	1448	finally
      //   488	503	1478	org/xmlpull/v1/XmlPullParserException
      //   488	503	1452	java/io/IOException
      //   488	503	1448	finally
      //   515	525	1478	org/xmlpull/v1/XmlPullParserException
      //   515	525	1452	java/io/IOException
      //   515	525	1448	finally
      //   534	549	1478	org/xmlpull/v1/XmlPullParserException
      //   534	549	1452	java/io/IOException
      //   534	549	1448	finally
      //   561	571	1478	org/xmlpull/v1/XmlPullParserException
      //   561	571	1452	java/io/IOException
      //   561	571	1448	finally
      //   580	595	1478	org/xmlpull/v1/XmlPullParserException
      //   580	595	1452	java/io/IOException
      //   580	595	1448	finally
      //   607	617	1478	org/xmlpull/v1/XmlPullParserException
      //   607	617	1452	java/io/IOException
      //   607	617	1448	finally
      //   626	641	1478	org/xmlpull/v1/XmlPullParserException
      //   626	641	1452	java/io/IOException
      //   626	641	1448	finally
      //   653	663	1478	org/xmlpull/v1/XmlPullParserException
      //   653	663	1452	java/io/IOException
      //   653	663	1448	finally
      //   672	687	1478	org/xmlpull/v1/XmlPullParserException
      //   672	687	1452	java/io/IOException
      //   672	687	1448	finally
      //   699	709	1478	org/xmlpull/v1/XmlPullParserException
      //   699	709	1452	java/io/IOException
      //   699	709	1448	finally
      //   718	727	1478	org/xmlpull/v1/XmlPullParserException
      //   718	727	1452	java/io/IOException
      //   718	727	1448	finally
      //   739	749	1478	org/xmlpull/v1/XmlPullParserException
      //   739	749	1452	java/io/IOException
      //   739	749	1448	finally
      //   758	773	1478	org/xmlpull/v1/XmlPullParserException
      //   758	773	1452	java/io/IOException
      //   758	773	1448	finally
      //   785	795	1478	org/xmlpull/v1/XmlPullParserException
      //   785	795	1452	java/io/IOException
      //   785	795	1448	finally
      //   804	819	1478	org/xmlpull/v1/XmlPullParserException
      //   804	819	1452	java/io/IOException
      //   804	819	1448	finally
      //   831	841	1478	org/xmlpull/v1/XmlPullParserException
      //   831	841	1452	java/io/IOException
      //   831	841	1448	finally
      //   850	865	1478	org/xmlpull/v1/XmlPullParserException
      //   850	865	1452	java/io/IOException
      //   850	865	1448	finally
      //   877	887	1478	org/xmlpull/v1/XmlPullParserException
      //   877	887	1452	java/io/IOException
      //   877	887	1448	finally
      //   896	911	1478	org/xmlpull/v1/XmlPullParserException
      //   896	911	1452	java/io/IOException
      //   896	911	1448	finally
      //   923	933	1478	org/xmlpull/v1/XmlPullParserException
      //   923	933	1452	java/io/IOException
      //   923	933	1448	finally
      //   942	957	1478	org/xmlpull/v1/XmlPullParserException
      //   942	957	1452	java/io/IOException
      //   942	957	1448	finally
      //   969	979	1478	org/xmlpull/v1/XmlPullParserException
      //   969	979	1452	java/io/IOException
      //   969	979	1448	finally
      //   988	1003	1478	org/xmlpull/v1/XmlPullParserException
      //   988	1003	1452	java/io/IOException
      //   988	1003	1448	finally
      //   1015	1025	1478	org/xmlpull/v1/XmlPullParserException
      //   1015	1025	1452	java/io/IOException
      //   1015	1025	1448	finally
      //   1034	1049	1478	org/xmlpull/v1/XmlPullParserException
      //   1034	1049	1452	java/io/IOException
      //   1034	1049	1448	finally
      //   1061	1071	1478	org/xmlpull/v1/XmlPullParserException
      //   1061	1071	1452	java/io/IOException
      //   1061	1071	1448	finally
      //   1080	1095	1478	org/xmlpull/v1/XmlPullParserException
      //   1080	1095	1452	java/io/IOException
      //   1080	1095	1448	finally
      //   1107	1117	1478	org/xmlpull/v1/XmlPullParserException
      //   1107	1117	1452	java/io/IOException
      //   1107	1117	1448	finally
      //   1126	1141	1478	org/xmlpull/v1/XmlPullParserException
      //   1126	1141	1452	java/io/IOException
      //   1126	1141	1448	finally
      //   1153	1163	1478	org/xmlpull/v1/XmlPullParserException
      //   1153	1163	1452	java/io/IOException
      //   1153	1163	1448	finally
      //   1172	1187	1478	org/xmlpull/v1/XmlPullParserException
      //   1172	1187	1452	java/io/IOException
      //   1172	1187	1448	finally
      //   1199	1209	1478	org/xmlpull/v1/XmlPullParserException
      //   1199	1209	1452	java/io/IOException
      //   1199	1209	1448	finally
      //   1218	1233	1478	org/xmlpull/v1/XmlPullParserException
      //   1218	1233	1452	java/io/IOException
      //   1218	1233	1448	finally
      //   1245	1255	1478	org/xmlpull/v1/XmlPullParserException
      //   1245	1255	1452	java/io/IOException
      //   1245	1255	1448	finally
      //   1264	1273	1478	org/xmlpull/v1/XmlPullParserException
      //   1264	1273	1452	java/io/IOException
      //   1264	1273	1448	finally
      //   1285	1295	1478	org/xmlpull/v1/XmlPullParserException
      //   1285	1295	1452	java/io/IOException
      //   1285	1295	1448	finally
      //   1304	1313	1478	org/xmlpull/v1/XmlPullParserException
      //   1304	1313	1452	java/io/IOException
      //   1304	1313	1448	finally
      //   1325	1335	1478	org/xmlpull/v1/XmlPullParserException
      //   1325	1335	1452	java/io/IOException
      //   1325	1335	1448	finally
      //   1344	1353	1478	org/xmlpull/v1/XmlPullParserException
      //   1344	1353	1452	java/io/IOException
      //   1344	1353	1448	finally
      //   1365	1375	1478	org/xmlpull/v1/XmlPullParserException
      //   1365	1375	1452	java/io/IOException
      //   1365	1375	1448	finally
      //   1384	1393	1478	org/xmlpull/v1/XmlPullParserException
      //   1384	1393	1452	java/io/IOException
      //   1384	1393	1448	finally
      //   1405	1420	1478	org/xmlpull/v1/XmlPullParserException
      //   1405	1420	1452	java/io/IOException
      //   1405	1420	1448	finally
      //   1429	1438	1478	org/xmlpull/v1/XmlPullParserException
      //   1429	1438	1452	java/io/IOException
      //   1429	1438	1448	finally
      //   1457	1467	1448	finally
      //   1483	1493	1448	finally
    }
  }
  
  private OplusWifiRomUpdateHelper(Context paramContext) {
    super(paramContext, "sys_wifi_par_config_list", "/system_ext/etc/sys_wifi_par_config_list.xml", "/data/misc/wifi/sys_wifi_par_config_list.xml");
    this.mMtuServer = new String[] { "conn3.coloros.com", "conn4.coloros.com", "conn5.coloros.com" };
    this.mSlaApps = new String[] { 
        "com.heytap.browser", "com.android.browser", "com.coloros.browser", "com.UCMobile", "com.tencent.mm", "com.tencent.mobileqq", "com.sina.weibo", "com.netease.newsreader.activity", "com.ss.android.article.news", "com.jingdong.app.mall", 
        "com.taobao.taobao", "com.tmall.wireless", "com.achievo.vipshop", "com.xunmeng.pinduoduo", "com.baidu.tieba", "com.qzone", "com.zhihu.android", "com.xingin.xhs", "com.baidu.browser.apps", "com.tencent.mtt", 
        "com.eg.android.AlipayGphone", "me.ele", "com.sankuai.meituan", "com.sankuai.meituan.takeoutnew", "com.dianping.v1", "com.moji.mjweather", "ctrip.android.view", "com.Qunar", "com.tencent.news", "com.tencent.reading" };
    this.mSlaAppsExp = new String[] { 
        "com.whatsapp", "in.mohalla.sharechat", "app.buzz.share", "com.facebook.orca", "com.UCMobile.intl", "com.mcent.browser", "com.redefine.welike", "com.instagram.android", "com.heytap.browser", "com.android.browser", 
        "com.coloros.browser", "com.android.chrome", "com.facebook.katana", "org.mozilla.firefox", "com.opera.browser", "com.heytap.browser" };
    this.mSlaGameApps = new String[] { "not.defined", "com.tencent.tmgp.sgame", "com.tencent.tmgp.pubgmhd" };
    this.mSlaGameAppsExp = new String[] { "not.defined", "not.defined", "com.tencent.ig" };
    this.mSlaBlackList = null;
    this.mSlaEnabledMCC = new String[] { "460", "404-405-406" };
    this.mSlaParams = new String[] { 
        "200", "500", "1000", "500", "230", "200", "220", "55", "75", "2000", 
        "2000", "200", "55" };
    this.mSpeedRttParams = new int[] { 
        150, 100, 250, 200, 150, 5, 10, 15, 10, 5, 
        5 };
    this.mSlaGameParams = new String[] { "4#8#0000000100010003", "4#8#000003e900040003", "5#5#0864100118", "5#5#0865100018" };
    this.mDualStaApps = new String[] { 
        "com.heytap.browser", "com.android.browser", "com.coloros.browser", "com.UCMobile", "com.tencent.mm", "com.tencent.mobileqq", "com.sina.weibo", "com.netease.newsreader.activity", "com.ss.android.article.news", "com.jingdong.app.mall", 
        "com.taobao.taobao", "com.tmall.wireless", "com.achievo.vipshop", "com.xunmeng.pinduoduo", "com.baidu.tieba", "com.qzone", "com.zhihu.android", "com.xingin.xhs", "com.baidu.browser.apps", "com.tencent.mtt", 
        "com.eg.android.AlipayGphone", "me.ele", "com.sankuai.meituan", "com.sankuai.meituan.takeoutnew", "com.dianping.v1", "com.moji.mjweather", "ctrip.android.view", "com.Qunar", "com.tencent.news", "com.tencent.reading", 
        "com.tencent.qqlive", "com.youku.phone", "com.qiyi.video", "com.sohu.sohuvideo", "com.tencent.android.qqdownloader", "com.oppo.market", "com.nearme.gamecenter", "com.xunlei.downloadprovider", "tv.danmaku.bili", "com.ss.android.ugc.aweme", 
        "com.smile.gifmaker", "air.tv.douyu.android", "com.ss.android.ugc.live", "com.hunantv.imgo.activity", "com.ss.android.article.video", "com.duowan.kiwi", "com.netease.cloudmusic", "com.kugou.android", "com.tencent.qqmusic" };
    this.mDualStaAppsExp = new String[] { 
        "com.whatsapp", "in.mohalla.sharechat", "app.buzz.share", "com.facebook.orca", "com.UCMobile.intl", "com.mcent.browser", "com.redefine.welike", "com.instagram.android", "com.heytap.browser", "com.android.browser", 
        "com.coloros.browser", "com.android.chrome", "com.facebook.katana", "org.mozilla.firefox", "com.opera.browser" };
    this.mDualStaBlackList = null;
    this.mVideoApps = null;
    this.mDownloadApps = null;
    this.mDualStaCapHostBlackList = null;
    this.mSkipDestroySocketApps = new String[] { "com.tencent.mm", "com.tencent.mobileqq", "com.alibaba.android.rimet" };
    this.mLmParams = new String[] { "1350#1200#0#4#17f10304", "1400#1250#0#4#17f10305", "1800#1250#0#4#17f10306", "1800#1250#0#4#17f10307" };
    this.mMtkSmartBWParams = new int[] { 
        1, 0, 1, 60, 30, 25, -75, 10, 15, 5, 
        3, 5, 20, 6, 135, 20, 15, 200 };
    this.mSmartBWParams = new int[] { 1, 2, 85, 35100, 8, 100, 2, 5, 2 };
    this.mRouterBoostDupPktGameWhiteList = null;
    this.mRouterBoostDupPktGameBlackList = null;
    setUpdateInfo(new WifiRomUpdateInfo(), new WifiRomUpdateInfo());
    try {
      init();
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public static OplusWifiRomUpdateHelper getInstance(Context paramContext) {
    // Byte code:
    //   0: ldc android/net/wifi/OplusWifiRomUpdateHelper
    //   2: monitorenter
    //   3: getstatic android/net/wifi/OplusWifiRomUpdateHelper.sInstance : Landroid/net/wifi/OplusWifiRomUpdateHelper;
    //   6: ifnonnull -> 43
    //   9: ldc android/net/wifi/OplusWifiRomUpdateHelper
    //   11: monitorenter
    //   12: getstatic android/net/wifi/OplusWifiRomUpdateHelper.sInstance : Landroid/net/wifi/OplusWifiRomUpdateHelper;
    //   15: ifnonnull -> 31
    //   18: new android/net/wifi/OplusWifiRomUpdateHelper
    //   21: astore_1
    //   22: aload_1
    //   23: aload_0
    //   24: invokespecial <init> : (Landroid/content/Context;)V
    //   27: aload_1
    //   28: putstatic android/net/wifi/OplusWifiRomUpdateHelper.sInstance : Landroid/net/wifi/OplusWifiRomUpdateHelper;
    //   31: ldc android/net/wifi/OplusWifiRomUpdateHelper
    //   33: monitorexit
    //   34: goto -> 43
    //   37: astore_0
    //   38: ldc android/net/wifi/OplusWifiRomUpdateHelper
    //   40: monitorexit
    //   41: aload_0
    //   42: athrow
    //   43: getstatic android/net/wifi/OplusWifiRomUpdateHelper.sInstance : Landroid/net/wifi/OplusWifiRomUpdateHelper;
    //   46: astore_0
    //   47: ldc android/net/wifi/OplusWifiRomUpdateHelper
    //   49: monitorexit
    //   50: aload_0
    //   51: areturn
    //   52: astore_0
    //   53: ldc android/net/wifi/OplusWifiRomUpdateHelper
    //   55: monitorexit
    //   56: aload_0
    //   57: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #190	-> 3
    //   #191	-> 9
    //   #192	-> 12
    //   #193	-> 18
    //   #195	-> 31
    //   #197	-> 43
    //   #189	-> 52
    // Exception table:
    //   from	to	target	type
    //   3	9	52	finally
    //   9	12	52	finally
    //   12	18	37	finally
    //   18	31	37	finally
    //   31	34	37	finally
    //   38	41	37	finally
    //   41	43	52	finally
    //   43	47	52	finally
  }
  
  public String getValue(String paramString1, String paramString2) {
    paramString1 = this.mKeyValuePair.get(paramString1);
    if (paramString1 == null)
      return paramString2; 
    return paramString1;
  }
  
  public Integer getIntegerValue(String paramString, Integer paramInteger) {
    paramString = this.mKeyValuePair.get(paramString);
    if (paramString == null)
      return paramInteger; 
    try {
      int i = Integer.parseInt(paramString);
      paramInteger = Integer.valueOf(i);
    } catch (NumberFormatException numberFormatException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("parse exception:");
      stringBuilder.append(numberFormatException);
      Log.d("OplusWifiRomUpdateHelper", stringBuilder.toString());
    } 
    return paramInteger;
  }
  
  public boolean getBooleanValue(String paramString, boolean paramBoolean) {
    Boolean bool;
    paramString = this.mKeyValuePair.get(paramString);
    if (paramString == null)
      return paramBoolean; 
    try {
      boolean bool1 = Boolean.parseBoolean(paramString);
      bool = Boolean.valueOf(bool1);
    } catch (NumberFormatException numberFormatException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("parse exception:");
      stringBuilder.append(numberFormatException);
      Log.d("OplusWifiRomUpdateHelper", stringBuilder.toString());
      bool = Boolean.valueOf(paramBoolean);
    } 
    return bool.booleanValue();
  }
  
  public Double getFloatValue(String paramString, Double paramDouble) {
    paramString = this.mKeyValuePair.get(paramString);
    if (paramString == null)
      return paramDouble; 
    try {
      double d = Double.parseDouble(paramString);
      paramDouble = Double.valueOf(d);
    } catch (NumberFormatException numberFormatException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("parse exception:");
      stringBuilder.append(numberFormatException);
      Log.d("OplusWifiRomUpdateHelper", stringBuilder.toString());
    } 
    return paramDouble;
  }
  
  public Long getLongValue(String paramString, Long paramLong) {
    paramString = this.mKeyValuePair.get(paramString);
    if (paramString == null)
      return paramLong; 
    try {
      long l = Long.parseLong(paramString);
      paramLong = Long.valueOf(l);
    } catch (NumberFormatException numberFormatException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("parse exception:");
      stringBuilder.append(numberFormatException);
      Log.d("OplusWifiRomUpdateHelper", stringBuilder.toString());
    } 
    return paramLong;
  }
  
  public void enableVerboseLogging(int paramInt) {
    boolean bool;
    if (paramInt > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.DEBUG = bool;
  }
  
  public void dump() {
    if (this.DEBUG)
      Log.d("OplusWifiRomUpdateHelper", "dump:"); 
    Iterator<String> iterator = this.mKeyValuePair.keySet().iterator();
    while (iterator.hasNext()) {
      String str1 = iterator.next();
      String str2 = this.mKeyValuePair.get(str1);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("\t");
      stringBuilder.append(str1);
      stringBuilder.append(":");
      stringBuilder.append(str2);
      Log.d("OplusWifiRomUpdateHelper", stringBuilder.toString());
    } 
  }
  
  public String[] getMtuServer() {
    return this.mMtuServer;
  }
  
  public String[] getSlaWhiteListApps() {
    return this.mSlaApps;
  }
  
  public String[] getSlaWhiteListAppsExp() {
    return this.mSlaAppsExp;
  }
  
  public String[] getSlaBlackListApps() {
    return this.mSlaBlackList;
  }
  
  public String[] getSlaGameApps() {
    return this.mSlaGameApps;
  }
  
  public String[] getSlaGameAppsExp() {
    return this.mSlaGameAppsExp;
  }
  
  public String[] getSlaEnabledMcc() {
    return this.mSlaEnabledMCC;
  }
  
  public String[] getSlaParams() {
    return this.mSlaParams;
  }
  
  public int[] getSpeedRttParams() {
    return this.mSpeedRttParams;
  }
  
  private void setSpeedRttParams(String paramString) {
    if (paramString != null) {
      String[] arrayOfString = paramString.split(",");
      if (arrayOfString != null && arrayOfString.length == 11)
        try {
          int i = Integer.parseInt(arrayOfString[0]);
          int j = Integer.parseInt(arrayOfString[1]);
          int k = Integer.parseInt(arrayOfString[2]);
          int m = Integer.parseInt(arrayOfString[3]);
          int n = Integer.parseInt(arrayOfString[4]);
          int i1 = Integer.parseInt(arrayOfString[5]);
          int i2 = Integer.parseInt(arrayOfString[6]);
          int i3 = Integer.parseInt(arrayOfString[7]);
          int i4 = Integer.parseInt(arrayOfString[8]);
          int i5 = Integer.parseInt(arrayOfString[9]);
          int i6 = Integer.parseInt(arrayOfString[10]);
          this.mSpeedRttParams = new int[] { 
              i, j, k, m, n, i1, i2, i3, i4, i5, 
              i6 };
        } catch (Exception exception) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("setSpeedRttParams failed to parse params:");
          stringBuilder.append(paramString);
          Log.e("OplusWifiRomUpdateHelper", stringBuilder.toString());
          this.mSpeedRttParams = null;
        }  
    } 
  }
  
  public String[] getSlaGameParams() {
    return this.mSlaGameParams;
  }
  
  public String[] getDualStaWhiteListApps() {
    return this.mDualStaApps;
  }
  
  public String[] getDualStaWhiteListAppsExp() {
    return this.mDualStaAppsExp;
  }
  
  public String[] getDualStaBlackListApps() {
    return this.mDualStaBlackList;
  }
  
  public String[] getDualStaBlackListCapHosts() {
    return this.mDualStaCapHostBlackList;
  }
  
  public String[] getDualStaDisabledMcc() {
    return this.mDualStaDisableMcc;
  }
  
  public String[] getAllVideoApps() {
    return this.mVideoApps;
  }
  
  public String[] getDownloadApps() {
    return this.mDownloadApps;
  }
  
  public String[] getSkipDestroySocketApps() {
    return this.mSkipDestroySocketApps;
  }
  
  public String[] getWechatLmParams() {
    return this.mLmParams;
  }
  
  private void sendWifiRomUpdateChangedBroadcast() {
    Intent intent = new Intent("oppo.intent.action.WIFI_ROM_UPDATE_CHANGED");
    intent.addFlags(67108864);
    if (this.mContext != null)
      this.mContext.sendBroadcast(intent, "oppo.permission.OPPO_COMPONENT_SAFE"); 
  }
  
  private void setDefaultMacRandomizationSetting(String paramString) {
    int i = 0;
    try {
      int j = Integer.parseInt(paramString);
    } catch (NumberFormatException numberFormatException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("parse int exception:");
      stringBuilder.append(numberFormatException);
      Log.d("OplusWifiRomUpdateHelper", stringBuilder.toString());
    } 
    if (i == 0 || i == 1) {
      SystemProperties.set("persist.sys.oplus.wifi.mac_randomization", Integer.toString(i));
      return;
    } 
    Log.d("OplusWifiRomUpdateHelper", "random mac value invalid!");
    SystemProperties.set("persist.sys.oplus.wifi.mac_randomization", Integer.toString(0));
  }
  
  private void setMtkSmartBWParams(String paramString) {
    if (paramString != null) {
      String[] arrayOfString = paramString.split(",");
      if (arrayOfString != null && arrayOfString.length == this.mMtkSmartBWParams.length)
        try {
          int i = Integer.parseInt(arrayOfString[0]);
          int j = Integer.parseInt(arrayOfString[1]);
          int k = Integer.parseInt(arrayOfString[2]);
          int m = Integer.parseInt(arrayOfString[3]);
          int n = Integer.parseInt(arrayOfString[4]);
          int i1 = Integer.parseInt(arrayOfString[5]);
          int i2 = Integer.parseInt(arrayOfString[6]);
          int i3 = Integer.parseInt(arrayOfString[7]);
          int i4 = Integer.parseInt(arrayOfString[8]);
          Integer.parseInt(arrayOfString[9]);
          int i5 = Integer.parseInt(arrayOfString[10]);
          int i6 = Integer.parseInt(arrayOfString[11]);
          int i7 = Integer.parseInt(arrayOfString[12]);
          int i8 = Integer.parseInt(arrayOfString[13]);
          int i9 = Integer.parseInt(arrayOfString[14]);
          int i10 = Integer.parseInt(arrayOfString[15]);
          int i11 = Integer.parseInt(arrayOfString[16]);
          int i12 = Integer.parseInt(arrayOfString[17]);
          this.mMtkSmartBWParams = new int[] { 
              i, j, k, m, n, i1, i2, i3, i4, 5, 
              i5, i6, i7, i8, i9, i10, i11, i12 };
          if (k > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("setMtkSmartBWParams parse params:");
            stringBuilder.append(paramString);
            Log.d("OplusWifiRomUpdateHelper", stringBuilder.toString());
          } 
        } catch (Exception exception) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("setMtkSmartBWParams failed to parse params:");
          stringBuilder.append(paramString);
          Log.e("OplusWifiRomUpdateHelper", stringBuilder.toString());
          this.mMtkSmartBWParams = null;
        }  
    } 
  }
  
  private void setSmartBWParams(String paramString) {
    if (paramString != null) {
      String[] arrayOfString = paramString.split(",");
      if (arrayOfString != null && arrayOfString.length == this.mSmartBWParams.length)
        try {
          int i = Integer.parseInt(arrayOfString[0]);
          int j = Integer.parseInt(arrayOfString[1]);
          int k = Integer.parseInt(arrayOfString[2]);
          int m = Integer.parseInt(arrayOfString[3]);
          int n = Integer.parseInt(arrayOfString[4]);
          int i1 = Integer.parseInt(arrayOfString[5]);
          int i2 = Integer.parseInt(arrayOfString[6]);
          int i3 = Integer.parseInt(arrayOfString[7]);
          int i4 = Integer.parseInt(arrayOfString[8]);
          this.mSmartBWParams = new int[] { i, j, k, m, n, i1, i2, i3, i4 };
        } catch (Exception exception) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("setSmartBWParams failed to parse params:");
          stringBuilder.append(paramString);
          Log.e("OplusWifiRomUpdateHelper", stringBuilder.toString());
          this.mSmartBWParams = null;
        }  
    } 
  }
  
  public int[] getSmartBWParams() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("sIsQcomPlatform = ");
    stringBuilder.append(sIsQcomPlatform);
    stringBuilder.append(" sIsMTKPlatform = ");
    stringBuilder.append(sIsMTKPlatform);
    Log.d("OplusWifiRomUpdateHelper", stringBuilder.toString());
    if (sIsQcomPlatform)
      return this.mSmartBWParams; 
    if (sIsMTKPlatform)
      return this.mMtkSmartBWParams; 
    return null;
  }
  
  public String[] getRouterBoostDupPktGameWhiteList() {
    return this.mRouterBoostDupPktGameWhiteList;
  }
  
  public String[] getRouterBoostDupPktGameBlackList() {
    return this.mRouterBoostDupPktGameBlackList;
  }
  
  private void setWifiAssistantFeatureState(String paramString) {
    boolean bool1 = Boolean.parseBoolean(paramString);
    ContentResolver contentResolver = this.mContext.getContentResolver();
    boolean bool2 = true;
    if (Settings.Global.getInt(contentResolver, "rom.update.wifi.assistant", 1) != 1)
      bool2 = false; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("setWifiAssistantFeatureState exp:");
    stringBuilder.append(bool1);
    stringBuilder.append(" cur:");
    stringBuilder.append(bool2);
    Log.d("OplusWifiRomUpdateHelper", stringBuilder.toString());
    if (bool1 != bool2) {
      ContentResolver contentResolver1 = this.mContext.getContentResolver();
      Settings.Global.putInt(contentResolver1, "rom.update.wifi.assistant", bool1);
    } 
  }
}
