package android.net.wifi;

import android.content.Context;
import android.os.SystemProperties;
import android.text.TextUtils;
import com.oplus.content.OplusFeatureConfigManager;

public class OplusWifiGlobalMethod implements IOplusWifiGlobalMethod {
  private static final String TAG = "OplusWifiGlobalMethod";
  
  private static OplusWifiGlobalMethod sInstance;
  
  private Context mContext;
  
  public static OplusWifiGlobalMethod getInstance() {
    // Byte code:
    //   0: ldc android/net/wifi/OplusWifiGlobalMethod
    //   2: monitorenter
    //   3: getstatic android/net/wifi/OplusWifiGlobalMethod.sInstance : Landroid/net/wifi/OplusWifiGlobalMethod;
    //   6: ifnonnull -> 42
    //   9: ldc android/net/wifi/OplusWifiGlobalMethod
    //   11: monitorenter
    //   12: getstatic android/net/wifi/OplusWifiGlobalMethod.sInstance : Landroid/net/wifi/OplusWifiGlobalMethod;
    //   15: ifnonnull -> 30
    //   18: new android/net/wifi/OplusWifiGlobalMethod
    //   21: astore_0
    //   22: aload_0
    //   23: invokespecial <init> : ()V
    //   26: aload_0
    //   27: putstatic android/net/wifi/OplusWifiGlobalMethod.sInstance : Landroid/net/wifi/OplusWifiGlobalMethod;
    //   30: ldc android/net/wifi/OplusWifiGlobalMethod
    //   32: monitorexit
    //   33: goto -> 42
    //   36: astore_0
    //   37: ldc android/net/wifi/OplusWifiGlobalMethod
    //   39: monitorexit
    //   40: aload_0
    //   41: athrow
    //   42: getstatic android/net/wifi/OplusWifiGlobalMethod.sInstance : Landroid/net/wifi/OplusWifiGlobalMethod;
    //   45: astore_0
    //   46: ldc android/net/wifi/OplusWifiGlobalMethod
    //   48: monitorexit
    //   49: aload_0
    //   50: areturn
    //   51: astore_0
    //   52: ldc android/net/wifi/OplusWifiGlobalMethod
    //   54: monitorexit
    //   55: aload_0
    //   56: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #42	-> 3
    //   #43	-> 9
    //   #44	-> 12
    //   #45	-> 18
    //   #47	-> 30
    //   #49	-> 42
    //   #41	-> 51
    // Exception table:
    //   from	to	target	type
    //   3	9	51	finally
    //   9	12	51	finally
    //   12	18	36	finally
    //   18	30	36	finally
    //   30	33	36	finally
    //   37	40	36	finally
    //   40	42	51	finally
    //   42	46	51	finally
  }
  
  public boolean isNotChineseOperator() {
    String str1 = SystemProperties.get("android.telephony.mcc_change", "");
    String str2 = SystemProperties.get("android.telephony.mcc_change2", "");
    boolean bool = TextUtils.isEmpty(str1);
    boolean bool1 = true;
    if (bool && TextUtils.isEmpty(str2))
      return OplusFeatureConfigManager.getInstance().hasFeature("oplus.software.connectivity.region_CN") ^ true; 
    if ("460".equals(str1) || "460".equals(str2))
      bool1 = false; 
    return bool1;
  }
}
