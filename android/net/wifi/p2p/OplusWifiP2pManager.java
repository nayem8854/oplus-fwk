package android.net.wifi.p2p;

import android.content.Context;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;

public class OplusWifiP2pManager implements IOplusWifiP2pManager {
  private static final String TAG = "OplusWifiP2pManager";
  
  private Context mContext;
  
  private IBinder mService;
  
  public OplusWifiP2pManager(Context paramContext) {
    this.mContext = paramContext;
    this.mService = ServiceManager.getService("wifip2p");
  }
  
  public boolean setNfcTriggered(boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiP2pManager");
      parcel1.writeBoolean(paramBoolean);
      this.mService.transact(11003, parcel1, parcel2, 0);
      parcel2.readException();
      paramBoolean = parcel2.readBoolean();
      return paramBoolean;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void saveExternalPeerAddress(String paramString) throws RemoteException {
    if (paramString == null) {
      Log.d("OplusWifiP2pManager", "saveExternalPeerAddress the address is null");
      return;
    } 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiP2pManager");
      parcel1.writeString(paramString);
      this.mService.transact(11004, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean setPcAutonomousGo(boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.net.wifi.IOplusWifiP2pManager");
      parcel1.writeBoolean(paramBoolean);
      this.mService.transact(11005, parcel1, parcel2, 0);
      parcel2.readException();
      paramBoolean = parcel2.readBoolean();
      return paramBoolean;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
}
