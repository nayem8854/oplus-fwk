package android.net.wifi.p2p;

import android.os.RemoteException;

public interface IOplusWifiP2pManager {
  public static final String DESCRIPTOR = "android.net.wifi.IOplusWifiP2pManager";
  
  public static final int OPLUS_CALL_TRANSACTION_INDEX = 11000;
  
  public static final int OPLUS_FIRST_CALL_TRANSACTION = 11001;
  
  public static final int TRANSACTION_saveExternalPeerAddress = 11004;
  
  public static final int TRANSACTION_setNfcTriggered = 11003;
  
  public static final int TRANSACTION_setPcAutonomousGo = 11005;
  
  void saveExternalPeerAddress(String paramString) throws RemoteException;
  
  boolean setNfcTriggered(boolean paramBoolean) throws RemoteException;
  
  boolean setPcAutonomousGo(boolean paramBoolean) throws RemoteException;
}
