package android.net.wifi;

import com.oplus.reflect.MethodParams;
import com.oplus.reflect.RefClass;
import com.oplus.reflect.RefMethod;
import java.util.List;
import oplus.net.wifi.HotspotClient;

public class OplusMirrorWifiManager {
  public static Class<?> TYPE = RefClass.load(OplusMirrorWifiManager.class, WifiManager.class);
  
  @MethodParams({HotspotClient.class})
  public static RefMethod<Boolean> blockClient;
  
  public static RefMethod<String[]> getAllSlaAcceleratedApps;
  
  public static RefMethod<String[]> getAllSlaAppsAndStates;
  
  public static RefMethod<List<HotspotClient>> getBlockedHotspotClients;
  
  public static RefMethod<List<HotspotClient>> getHotspotClients;
  
  public static RefMethod<WifiConfiguration> getWifiSharingConfiguration;
  
  public static RefMethod<Boolean> isSlaSupported;
  
  @MethodParams({WifiConfiguration.class})
  public static RefMethod<Boolean> setWifiSharingConfiguration;
  
  @MethodParams({HotspotClient.class})
  public static RefMethod<Boolean> unblockClient;
}
