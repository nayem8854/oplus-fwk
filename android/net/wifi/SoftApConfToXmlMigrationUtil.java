package android.net.wifi;

import android.app.ActivityThread;
import android.app.ContextImpl;
import android.net.MacAddress;
import android.os.Environment;
import android.os.RemoteException;
import android.provider.Settings;
import android.util.Log;
import com.android.internal.util.FastXmlSerializer;
import com.android.internal.util.XmlUtils;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import libcore.io.IoUtils;
import org.xmlpull.v1.XmlSerializer;

public final class SoftApConfToXmlMigrationUtil {
  private static final int CONFIG_STORE_DATA_VERSION = 3;
  
  private static final String FILE_HOSTAPD_DENY = "/data/misc/wifi/hostapd.deny";
  
  private static final String LEGACY_AP_CONFIG_FILE = "softap.conf";
  
  private static final String LEGACY_WIFI_STORE_DIRECTORY_NAME = "wifi";
  
  private static final String MAC_PATTERN_STR = "([A-Fa-f0-9]{2}:){5}[A-Fa-f0-9]{2}";
  
  private static final int MAX_CLIENT_DEFAULT = 10;
  
  private static final int MIN_CLIENT_DEFAULT = 1;
  
  private static final String NAME_TAG = "#name-";
  
  private static final String TAG = "SoftApConfToXmlMigrationUtil";
  
  private static final int WIFICONFIG_AP_BAND_2GHZ = 0;
  
  private static final int WIFICONFIG_AP_BAND_5GHZ = 1;
  
  private static final int WIFICONFIG_AP_BAND_ANY = -1;
  
  private static final String WIFI_HOTSPOT_MAX_CLIENT_NUM = "oppo_wifi_ap_max_devices_connect";
  
  private static final String XML_TAG_ALLOWED_CLIENT_LIST = "AllowedClientList";
  
  private static final String XML_TAG_AP_BAND = "ApBand";
  
  private static final String XML_TAG_AUTO_SHUTDOWN_ENABLED = "AutoShutdownEnabled";
  
  private static final String XML_TAG_BLOCKED_CLIENT_LIST = "BlockedClientList";
  
  private static final String XML_TAG_BSSID = "Bssid";
  
  private static final String XML_TAG_CHANNEL = "Channel";
  
  private static final String XML_TAG_CLIENT_CONTROL_BY_USER = "ClientControlByUser";
  
  public static final String XML_TAG_CLIENT_MACADDRESS = "ClientMacAddress";
  
  private static final String XML_TAG_DOCUMENT_HEADER = "WifiConfigStoreData";
  
  private static final String XML_TAG_HIDDEN_SSID = "HiddenSSID";
  
  private static final String XML_TAG_MAX_NUMBER_OF_CLIENTS = "MaxNumberOfClients";
  
  private static final String XML_TAG_PASSPHRASE = "Passphrase";
  
  private static final String XML_TAG_SECTION_HEADER_SOFTAP = "SoftAp";
  
  private static final String XML_TAG_SECURITY_TYPE = "SecurityType";
  
  private static final String XML_TAG_SHUTDOWN_TIMEOUT_MILLIS = "ShutdownTimeoutMillis";
  
  private static final String XML_TAG_SSID = "SSID";
  
  private static final String XML_TAG_VERSION = "Version";
  
  private static File getLegacyWifiSharedDirectory() {
    return new File(Environment.getDataMiscDirectory(), "wifi");
  }
  
  public static int convertWifiConfigBandToSoftApConfigBand(int paramInt) {
    if (paramInt != -1) {
      if (paramInt != 1)
        return 1; 
      return 2;
    } 
    return 3;
  }
  
  private static SoftApConfiguration loadFromLegacyFile(InputStream paramInputStream) {
    SoftApConfiguration softApConfiguration;
    InputStream inputStream1 = null, inputStream2 = null, inputStream3 = null;
    InputStream inputStream4 = inputStream3, inputStream5 = inputStream1, inputStream6 = inputStream2;
    try {
      SoftApConfiguration.Builder builder = new SoftApConfiguration.Builder();
      inputStream4 = inputStream3;
      inputStream5 = inputStream1;
      inputStream6 = inputStream2;
      this();
      inputStream4 = inputStream3;
      inputStream5 = inputStream1;
      inputStream6 = inputStream2;
      DataInputStream dataInputStream = new DataInputStream();
      inputStream4 = inputStream3;
      inputStream5 = inputStream1;
      inputStream6 = inputStream2;
      BufferedInputStream bufferedInputStream = new BufferedInputStream();
      inputStream4 = inputStream3;
      inputStream5 = inputStream1;
      inputStream6 = inputStream2;
      this(paramInputStream);
      inputStream4 = inputStream3;
      inputStream5 = inputStream1;
      inputStream6 = inputStream2;
      this(bufferedInputStream);
      paramInputStream = dataInputStream;
      inputStream4 = paramInputStream;
      inputStream5 = paramInputStream;
      inputStream6 = paramInputStream;
      int i = paramInputStream.readInt();
      boolean bool = true;
      if (i < 1 || i > 3) {
        inputStream4 = paramInputStream;
        inputStream5 = paramInputStream;
        inputStream6 = paramInputStream;
        Log.e("SoftApConfToXmlMigrationUtil", "Bad version on hotspot configuration file");
        try {
          paramInputStream.close();
        } catch (IOException iOException1) {
          Log.e("SoftApConfToXmlMigrationUtil", "Error closing hotspot configuration during read", iOException1);
        } 
        return null;
      } 
      IOException iOException2 = iOException1, iOException3 = iOException1, iOException4 = iOException1;
      builder.setSsid(iOException1.readUTF());
      if (i >= 2) {
        iOException2 = iOException1;
        iOException3 = iOException1;
        iOException4 = iOException1;
        int j = iOException1.readInt();
        iOException2 = iOException1;
        iOException3 = iOException1;
        iOException4 = iOException1;
        i = iOException1.readInt();
        if (i == 0) {
          iOException2 = iOException1;
          iOException3 = iOException1;
          iOException4 = iOException1;
          i = convertWifiConfigBandToSoftApConfigBand(j);
          iOException2 = iOException1;
          iOException3 = iOException1;
          iOException4 = iOException1;
          builder.setBand(i);
        } else {
          iOException2 = iOException1;
          iOException3 = iOException1;
          iOException4 = iOException1;
          j = convertWifiConfigBandToSoftApConfigBand(j);
          iOException2 = iOException1;
          iOException3 = iOException1;
          iOException4 = iOException1;
          builder.setChannel(i, j);
        } 
      } 
      iOException2 = iOException1;
      iOException3 = iOException1;
      iOException4 = iOException1;
      i = iOException1.readInt();
      if (i == 1 || i == 4) {
        iOException2 = iOException1;
        iOException3 = iOException1;
        iOException4 = iOException1;
        builder.setPassphrase(iOException1.readUTF(), 1);
      } 
      iOException2 = iOException1;
      iOException3 = iOException1;
      try {
        builder.setHiddenSsid(iOException1.readBoolean());
      } catch (IOException iOException) {
        iOException2 = iOException1;
        iOException3 = iOException1;
        iOException4 = iOException1;
        StringBuilder stringBuilder = new StringBuilder();
        iOException2 = iOException1;
        iOException3 = iOException1;
        iOException4 = iOException1;
        this();
        iOException2 = iOException1;
        iOException3 = iOException1;
        iOException4 = iOException1;
        stringBuilder.append("hiddenSSID error ");
        iOException2 = iOException1;
        iOException3 = iOException1;
        iOException4 = iOException1;
        stringBuilder.append(iOException);
        iOException2 = iOException1;
        iOException3 = iOException1;
        iOException4 = iOException1;
        Log.e("SoftApConfToXmlMigrationUtil", stringBuilder.toString());
        iOException2 = iOException1;
        iOException3 = iOException1;
        iOException4 = iOException1;
        builder.setHiddenSsid(false);
      } 
      iOException2 = iOException1;
      iOException3 = iOException1;
      iOException4 = iOException1;
      ContextImpl contextImpl = ActivityThread.currentActivityThread().getSystemContext();
      iOException2 = iOException1;
      iOException3 = iOException1;
      iOException4 = iOException1;
      i = Settings.System.getInt(contextImpl.getContentResolver(), "oppo_wifi_ap_max_devices_connect", 10);
      iOException2 = iOException1;
      iOException3 = iOException1;
      iOException4 = iOException1;
      StringBuilder stringBuilder1 = new StringBuilder();
      iOException2 = iOException1;
      iOException3 = iOException1;
      iOException4 = iOException1;
      this();
      iOException2 = iOException1;
      iOException3 = iOException1;
      iOException4 = iOException1;
      stringBuilder1.append("in addOEMToConfig, maxNumSta = ");
      iOException2 = iOException1;
      iOException3 = iOException1;
      iOException4 = iOException1;
      stringBuilder1.append(i);
      iOException2 = iOException1;
      iOException3 = iOException1;
      iOException4 = iOException1;
      Log.d("SoftApConfToXmlMigrationUtil", stringBuilder1.toString());
      if (i >= 1 && i <= 10) {
        iOException2 = iOException1;
        iOException3 = iOException1;
        iOException4 = iOException1;
        builder.setMaxNumberOfClients(i);
      } 
      iOException2 = iOException1;
      iOException3 = iOException1;
      iOException4 = iOException1;
      if (Settings.Global.getInt(contextImpl.getContentResolver(), "soft_ap_timeout_enabled", 1) == 0)
        bool = false; 
      iOException2 = iOException1;
      iOException3 = iOException1;
      iOException4 = iOException1;
      StringBuilder stringBuilder2 = new StringBuilder();
      iOException2 = iOException1;
      iOException3 = iOException1;
      iOException4 = iOException1;
      this();
      iOException2 = iOException1;
      iOException3 = iOException1;
      iOException4 = iOException1;
      stringBuilder2.append("in addOEMToConfig, isSoftApTimeOutEnabled = ");
      iOException2 = iOException1;
      iOException3 = iOException1;
      iOException4 = iOException1;
      stringBuilder2.append(bool);
      iOException2 = iOException1;
      iOException3 = iOException1;
      iOException4 = iOException1;
      Log.d("SoftApConfToXmlMigrationUtil", stringBuilder2.toString());
      iOException2 = iOException1;
      iOException3 = iOException1;
      iOException4 = iOException1;
      builder.setAutoShutdownEnabled(bool);
      iOException2 = iOException1;
      iOException3 = iOException1;
      iOException4 = iOException1;
      try {
        List<String> list = convertHostApdDenyData("/data/misc/wifi/hostapd.deny");
        iOException2 = iOException1;
        iOException3 = iOException1;
        iOException4 = iOException1;
        ArrayList<MacAddress> arrayList = new ArrayList();
        iOException2 = iOException1;
        iOException3 = iOException1;
        iOException4 = iOException1;
        this();
        iOException2 = iOException1;
        iOException3 = iOException1;
        iOException4 = iOException1;
        if (list.size() == 0) {
          iOException2 = iOException1;
          iOException3 = iOException1;
          iOException4 = iOException1;
          Log.d("SoftApConfToXmlMigrationUtil", "restoreHostApdDenyData deniedClients.size is zero.");
        } 
        iOException2 = iOException1;
        iOException3 = iOException1;
        iOException4 = iOException1;
        Iterator<String> iterator = list.iterator();
        while (true) {
          iOException2 = iOException1;
          iOException3 = iOException1;
          iOException4 = iOException1;
          if (iterator.hasNext()) {
            iOException2 = iOException1;
            iOException3 = iOException1;
            iOException4 = iOException1;
            String str = iterator.next();
            iOException2 = iOException1;
            iOException3 = iOException1;
            iOException4 = iOException1;
            arrayList.add(MacAddress.fromString(str));
            continue;
          } 
          break;
        } 
        iOException2 = iOException1;
        iOException3 = iOException1;
        iOException4 = iOException1;
        builder.setBlockedClientList(arrayList);
      } catch (RemoteException remoteException) {
        IOException iOException = iOException1;
        iOException3 = iOException1;
        iOException4 = iOException1;
        Log.e("SoftApConfToXmlMigrationUtil", "convertHostApdDenyData failed.");
      } 
      iOException2 = iOException1;
      iOException3 = iOException1;
      iOException4 = iOException1;
      SoftApConfiguration softApConfiguration2 = builder.build();
      SoftApConfiguration softApConfiguration1 = softApConfiguration;
      try {
        iOException1.close();
        SoftApConfiguration softApConfiguration3 = softApConfiguration;
      } catch (IOException iOException) {
        Log.e("SoftApConfToXmlMigrationUtil", "Error closing hotspot configuration during read", iOException);
        SoftApConfiguration softApConfiguration3 = softApConfiguration1;
      } 
    } catch (IOException iOException) {
      SoftApConfiguration softApConfiguration1 = softApConfiguration;
      Log.e("SoftApConfToXmlMigrationUtil", "Error reading hotspot configuration ", iOException);
      softApConfiguration1 = null;
      iOException = null;
      inputStream5 = null;
      if (softApConfiguration != null) {
        softApConfiguration.close();
        InputStream inputStream = inputStream5;
      } 
    } catch (IllegalArgumentException illegalArgumentException) {
      inputStream4 = inputStream5;
      Log.e("SoftApConfToXmlMigrationUtil", "Invalid hotspot configuration ", illegalArgumentException);
      inputStream4 = null;
      illegalArgumentException = null;
      softApConfiguration = null;
      if (inputStream5 != null) {
        inputStream5.close();
        SoftApConfiguration softApConfiguration1 = softApConfiguration;
      } 
    } finally {}
    return (SoftApConfiguration)paramInputStream;
  }
  
  private static byte[] convertConfToXml(SoftApConfiguration paramSoftApConfiguration) {
    try {
      FastXmlSerializer fastXmlSerializer = new FastXmlSerializer();
      this();
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      this();
      fastXmlSerializer.setOutput(byteArrayOutputStream, StandardCharsets.UTF_8.name());
      fastXmlSerializer.startDocument(null, Boolean.valueOf(true));
      fastXmlSerializer.startTag(null, "WifiConfigStoreData");
      XmlUtils.writeValueXml(Integer.valueOf(3), "Version", (XmlSerializer)fastXmlSerializer);
      fastXmlSerializer.startTag(null, "SoftAp");
      XmlUtils.writeValueXml(paramSoftApConfiguration.getSsid(), "SSID", (XmlSerializer)fastXmlSerializer);
      if (paramSoftApConfiguration.getBssid() != null)
        XmlUtils.writeValueXml(paramSoftApConfiguration.getBssid().toString(), "Bssid", (XmlSerializer)fastXmlSerializer); 
      XmlUtils.writeValueXml(Integer.valueOf(paramSoftApConfiguration.getBand()), "ApBand", (XmlSerializer)fastXmlSerializer);
      XmlUtils.writeValueXml(Integer.valueOf(paramSoftApConfiguration.getChannel()), "Channel", (XmlSerializer)fastXmlSerializer);
      XmlUtils.writeValueXml(Boolean.valueOf(paramSoftApConfiguration.isHiddenSsid()), "HiddenSSID", (XmlSerializer)fastXmlSerializer);
      XmlUtils.writeValueXml(Integer.valueOf(paramSoftApConfiguration.getSecurityType()), "SecurityType", (XmlSerializer)fastXmlSerializer);
      if (paramSoftApConfiguration.getSecurityType() != 0)
        XmlUtils.writeValueXml(paramSoftApConfiguration.getPassphrase(), "Passphrase", (XmlSerializer)fastXmlSerializer); 
      XmlUtils.writeValueXml(Integer.valueOf(paramSoftApConfiguration.getMaxNumberOfClients()), "MaxNumberOfClients", (XmlSerializer)fastXmlSerializer);
      XmlUtils.writeValueXml(Boolean.valueOf(paramSoftApConfiguration.isClientControlByUserEnabled()), "ClientControlByUser", (XmlSerializer)fastXmlSerializer);
      XmlUtils.writeValueXml(Boolean.valueOf(paramSoftApConfiguration.isAutoShutdownEnabled()), "AutoShutdownEnabled", (XmlSerializer)fastXmlSerializer);
      XmlUtils.writeValueXml(Long.valueOf(paramSoftApConfiguration.getShutdownTimeoutMillis()), "ShutdownTimeoutMillis", (XmlSerializer)fastXmlSerializer);
      fastXmlSerializer.startTag(null, "BlockedClientList");
      Iterator<MacAddress> iterator = paramSoftApConfiguration.getBlockedClientList().iterator();
      while (true) {
        boolean bool = iterator.hasNext();
        if (bool) {
          MacAddress macAddress = iterator.next();
          XmlUtils.writeValueXml(macAddress.toString(), "ClientMacAddress", (XmlSerializer)fastXmlSerializer);
          continue;
        } 
        break;
      } 
      fastXmlSerializer.endTag(null, "BlockedClientList");
      fastXmlSerializer.startTag(null, "AllowedClientList");
      for (MacAddress macAddress : paramSoftApConfiguration.getAllowedClientList())
        XmlUtils.writeValueXml(macAddress.toString(), "ClientMacAddress", (XmlSerializer)fastXmlSerializer); 
      fastXmlSerializer.endTag(null, "AllowedClientList");
      fastXmlSerializer.endTag(null, "SoftAp");
      fastXmlSerializer.endTag(null, "WifiConfigStoreData");
      fastXmlSerializer.endDocument();
      return byteArrayOutputStream.toByteArray();
    } catch (IOException|org.xmlpull.v1.XmlPullParserException iOException) {
      Log.e("SoftApConfToXmlMigrationUtil", "Failed to convert softap conf to XML", iOException);
      return null;
    } 
  }
  
  public static InputStream convert(InputStream paramInputStream) {
    SoftApConfiguration softApConfiguration = loadFromLegacyFile(paramInputStream);
    if (softApConfiguration == null)
      return null; 
    byte[] arrayOfByte = convertConfToXml(softApConfiguration);
    if (arrayOfByte == null)
      return null; 
    return new ByteArrayInputStream(arrayOfByte);
  }
  
  public static InputStream convert() {
    File file = new File(getLegacyWifiSharedDirectory(), "softap.conf");
    try {
      FileInputStream fileInputStream = new FileInputStream(file);
      return convert(fileInputStream);
    } catch (FileNotFoundException fileNotFoundException) {
      return null;
    } 
  }
  
  public static void remove() {
    File file = new File(getLegacyWifiSharedDirectory(), "softap.conf");
    file.delete();
  }
  
  private static List<String> convertHostApdDenyData(String paramString) throws RemoteException {
    ArrayList<String> arrayList = new ArrayList();
    BufferedReader bufferedReader1 = null, bufferedReader2 = null;
    String str = "";
    BufferedReader bufferedReader3 = bufferedReader2, bufferedReader4 = bufferedReader1;
    try {
      String str1;
      BufferedReader bufferedReader6 = new BufferedReader();
      bufferedReader3 = bufferedReader2;
      bufferedReader4 = bufferedReader1;
      FileReader fileReader = new FileReader();
      bufferedReader3 = bufferedReader2;
      bufferedReader4 = bufferedReader1;
      this(paramString);
      bufferedReader3 = bufferedReader2;
      bufferedReader4 = bufferedReader1;
      this(fileReader);
      BufferedReader bufferedReader5 = bufferedReader6;
      do {
        bufferedReader3 = bufferedReader5;
        bufferedReader4 = bufferedReader5;
        str1 = bufferedReader5.readLine();
        bufferedReader3 = bufferedReader5;
        bufferedReader4 = bufferedReader5;
        StringBuilder stringBuilder = new StringBuilder();
        bufferedReader3 = bufferedReader5;
        bufferedReader4 = bufferedReader5;
        this();
        bufferedReader3 = bufferedReader5;
        bufferedReader4 = bufferedReader5;
        stringBuilder.append("Reach hostapd.deny! line = ");
        bufferedReader3 = bufferedReader5;
        bufferedReader4 = bufferedReader5;
        stringBuilder.append(str1);
        bufferedReader3 = bufferedReader5;
        bufferedReader4 = bufferedReader5;
        Log.d("SoftApConfToXmlMigrationUtil", stringBuilder.toString());
        if (str1 == null) {
          bufferedReader3 = bufferedReader5;
          bufferedReader4 = bufferedReader5;
          Log.d("SoftApConfToXmlMigrationUtil", "Reach end of hostapd.deny!");
          break;
        } 
        bufferedReader3 = bufferedReader5;
        bufferedReader4 = bufferedReader5;
        if (str1.startsWith("#")) {
          bufferedReader3 = bufferedReader5;
          bufferedReader4 = bufferedReader5;
          if (str1.startsWith("#name-")) {
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            str = str1.substring("#name-".length());
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            stringBuilder = new StringBuilder();
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            this();
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            stringBuilder.append("Device name: ");
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            stringBuilder.append(str);
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            Log.d("SoftApConfToXmlMigrationUtil", stringBuilder.toString());
          } else {
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            stringBuilder = new StringBuilder();
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            this();
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            stringBuilder.append("Skip comment: ");
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            stringBuilder.append(str1);
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            Log.d("SoftApConfToXmlMigrationUtil", stringBuilder.toString());
          } 
        } else {
          bufferedReader3 = bufferedReader5;
          bufferedReader4 = bufferedReader5;
          if (!str1.matches("([A-Fa-f0-9]{2}:){5}[A-Fa-f0-9]{2}")) {
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            stringBuilder = new StringBuilder();
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            this();
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            stringBuilder.append("Invalid dev mac: ");
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            stringBuilder.append(str1);
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            Log.d("SoftApConfToXmlMigrationUtil", stringBuilder.toString());
          } else {
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            stringBuilder = new StringBuilder();
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            this();
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            stringBuilder.append("convertHostApdDenyData deviceName = ");
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            stringBuilder.append(str);
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            stringBuilder.append("; mac = ");
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            stringBuilder.append(str1);
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            Log.d("SoftApConfToXmlMigrationUtil", stringBuilder.toString());
            bufferedReader3 = bufferedReader5;
            bufferedReader4 = bufferedReader5;
            arrayList.add(str1);
          } 
        } 
      } while (str1 != null);
    } catch (IOException iOException) {
      bufferedReader3 = bufferedReader4;
      iOException.printStackTrace();
      BufferedReader bufferedReader = bufferedReader4;
    } finally {}
    IoUtils.closeQuietly((AutoCloseable)paramString);
    return arrayList;
  }
}
