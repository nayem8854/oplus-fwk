package android.net.wifi;

import android.content.Context;
import android.net.INetworkScoreCache;
import android.net.NetworkKey;
import android.net.ScoredNetwork;
import android.os.Handler;
import android.os.Process;
import android.util.Log;
import android.util.LruCache;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.List;
import java.util.Objects;

public class WifiNetworkScoreCache extends INetworkScoreCache.Stub {
  private static final boolean DBG = Log.isLoggable("WifiNetworkScoreCache", 3);
  
  private static final int DEFAULT_MAX_CACHE_SIZE = 100;
  
  public static final int INVALID_NETWORK_SCORE = -128;
  
  private static final String TAG = "WifiNetworkScoreCache";
  
  private final LruCache<String, ScoredNetwork> mCache;
  
  private final Context mContext;
  
  private CacheListener mListener;
  
  private final Object mLock = new Object();
  
  public WifiNetworkScoreCache(Context paramContext) {
    this(paramContext, null);
  }
  
  public WifiNetworkScoreCache(Context paramContext, CacheListener paramCacheListener) {
    this(paramContext, paramCacheListener, 100);
  }
  
  public WifiNetworkScoreCache(Context paramContext, CacheListener paramCacheListener, int paramInt) {
    this.mContext = paramContext.getApplicationContext();
    this.mListener = paramCacheListener;
    this.mCache = new LruCache(paramInt);
  }
  
  public final void updateScores(List<ScoredNetwork> paramList) {
    if (paramList == null || paramList.isEmpty())
      return; 
    if (DBG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("updateScores list size=");
      stringBuilder.append(paramList.size());
      Log.d("WifiNetworkScoreCache", stringBuilder.toString());
    } 
    boolean bool = false;
    synchronized (this.mLock) {
      for (ScoredNetwork scoredNetwork : paramList) {
        StringBuilder stringBuilder;
        String str = buildNetworkKey(scoredNetwork);
        if (str == null) {
          if (DBG) {
            stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Failed to build network key for ScoredNetwork");
            stringBuilder.append(scoredNetwork);
            Log.d("WifiNetworkScoreCache", stringBuilder.toString());
          } 
          continue;
        } 
        this.mCache.put(stringBuilder, scoredNetwork);
        bool = true;
      } 
      if (this.mListener != null && bool)
        this.mListener.post(paramList); 
      return;
    } 
  }
  
  public final void clearScores() {
    synchronized (this.mLock) {
      this.mCache.evictAll();
      return;
    } 
  }
  
  public boolean isScoredNetwork(ScanResult paramScanResult) {
    boolean bool;
    if (getScoredNetwork(paramScanResult) != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean hasScoreCurve(ScanResult paramScanResult) {
    boolean bool;
    ScoredNetwork scoredNetwork = getScoredNetwork(paramScanResult);
    if (scoredNetwork != null && scoredNetwork.rssiCurve != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getNetworkScore(ScanResult paramScanResult) {
    byte b1 = -128;
    ScoredNetwork scoredNetwork = getScoredNetwork(paramScanResult);
    byte b2 = b1;
    if (scoredNetwork != null) {
      b2 = b1;
      if (scoredNetwork.rssiCurve != null) {
        b1 = scoredNetwork.rssiCurve.lookupScore(paramScanResult.level);
        b2 = b1;
        if (DBG) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("getNetworkScore found scored network ");
          stringBuilder.append(scoredNetwork.networkKey);
          stringBuilder.append(" score ");
          stringBuilder.append(Integer.toString(b1));
          stringBuilder.append(" RSSI ");
          stringBuilder.append(paramScanResult.level);
          String str = stringBuilder.toString();
          Log.d("WifiNetworkScoreCache", str);
          b2 = b1;
        } 
      } 
    } 
    return b2;
  }
  
  public boolean getMeteredHint(ScanResult paramScanResult) {
    boolean bool;
    ScoredNetwork scoredNetwork = getScoredNetwork(paramScanResult);
    if (scoredNetwork != null && scoredNetwork.meteredHint) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getNetworkScore(ScanResult paramScanResult, boolean paramBoolean) {
    byte b1 = -128;
    ScoredNetwork scoredNetwork = getScoredNetwork(paramScanResult);
    byte b2 = b1;
    if (scoredNetwork != null) {
      b2 = b1;
      if (scoredNetwork.rssiCurve != null) {
        b1 = scoredNetwork.rssiCurve.lookupScore(paramScanResult.level, paramBoolean);
        b2 = b1;
        if (DBG) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("getNetworkScore found scored network ");
          stringBuilder.append(scoredNetwork.networkKey);
          stringBuilder.append(" score ");
          stringBuilder.append(Integer.toString(b1));
          stringBuilder.append(" RSSI ");
          stringBuilder.append(paramScanResult.level);
          stringBuilder.append(" isActiveNetwork ");
          stringBuilder.append(paramBoolean);
          String str = stringBuilder.toString();
          Log.d("WifiNetworkScoreCache", str);
          b2 = b1;
        } 
      } 
    } 
    return b2;
  }
  
  public ScoredNetwork getScoredNetwork(ScanResult paramScanResult) {
    null = buildNetworkKey(paramScanResult);
    if (null == null)
      return null; 
    synchronized (this.mLock) {
      return (ScoredNetwork)this.mCache.get(null);
    } 
  }
  
  public ScoredNetwork getScoredNetwork(NetworkKey paramNetworkKey) {
    String str = buildNetworkKey(paramNetworkKey);
    if (str == null) {
      if (DBG) {
        null = new StringBuilder();
        null.append("Could not build key string for Network Key: ");
        null.append(paramNetworkKey);
        Log.d("WifiNetworkScoreCache", null.toString());
      } 
      return null;
    } 
    synchronized (this.mLock) {
      return (ScoredNetwork)this.mCache.get(null);
    } 
  }
  
  private String buildNetworkKey(ScoredNetwork paramScoredNetwork) {
    if (paramScoredNetwork == null)
      return null; 
    return buildNetworkKey(paramScoredNetwork.networkKey);
  }
  
  private String buildNetworkKey(NetworkKey paramNetworkKey) {
    if (paramNetworkKey == null)
      return null; 
    if (paramNetworkKey.wifiKey == null)
      return null; 
    if (paramNetworkKey.type == 1) {
      String str1 = paramNetworkKey.wifiKey.ssid;
      if (str1 == null)
        return null; 
      String str2 = str1;
      if (paramNetworkKey.wifiKey.bssid != null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str1);
        stringBuilder.append(paramNetworkKey.wifiKey.bssid);
        str2 = stringBuilder.toString();
      } 
      return str2;
    } 
    return null;
  }
  
  private String buildNetworkKey(ScanResult paramScanResult) {
    if (paramScanResult == null || paramScanResult.SSID == null)
      return null; 
    StringBuilder stringBuilder = new StringBuilder("\"");
    stringBuilder.append(paramScanResult.SSID);
    stringBuilder.append("\"");
    if (paramScanResult.BSSID != null)
      stringBuilder.append(paramScanResult.BSSID); 
    return stringBuilder.toString();
  }
  
  protected final void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    this.mContext.enforceCallingOrSelfPermission("android.permission.DUMP", "WifiNetworkScoreCache");
    Context context = this.mContext;
    String str = context.getPackageName();
    int i = Process.myUid();
    str = String.format("WifiNetworkScoreCache (%s/%d)", new Object[] { str, Integer.valueOf(i) });
    paramPrintWriter.println(str);
    paramPrintWriter.println("  All score curves:");
    synchronized (this.mLock) {
      for (ScoredNetwork scoredNetwork : this.mCache.snapshot().values()) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("    ");
        stringBuilder.append(scoredNetwork);
        paramPrintWriter.println(stringBuilder.toString());
      } 
      paramPrintWriter.println("  Network scores for latest ScanResults:");
      WifiManager wifiManager = (WifiManager)this.mContext.getSystemService("wifi");
      for (ScanResult scanResult : wifiManager.getScanResults()) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("    ");
        stringBuilder.append(buildNetworkKey(scanResult));
        stringBuilder.append(": ");
        stringBuilder.append(getNetworkScore(scanResult));
        String str1 = stringBuilder.toString();
        paramPrintWriter.println(str1);
      } 
      return;
    } 
  }
  
  public void registerListener(CacheListener paramCacheListener) {
    synchronized (this.mLock) {
      this.mListener = paramCacheListener;
      return;
    } 
  }
  
  public void unregisterListener() {
    synchronized (this.mLock) {
      this.mListener = null;
      return;
    } 
  }
  
  class CacheListener {
    private Handler mHandler;
    
    public CacheListener(WifiNetworkScoreCache this$0) {
      Objects.requireNonNull(this$0);
      this.mHandler = (Handler)this$0;
    }
    
    public abstract void networkCacheUpdated(List<ScoredNetwork> param1List);
    
    void post(final List<ScoredNetwork> updatedNetworks) {
      this.mHandler.post(new Runnable() {
            final WifiNetworkScoreCache.CacheListener this$0;
            
            final List val$updatedNetworks;
            
            public void run() {
              WifiNetworkScoreCache.CacheListener.this.networkCacheUpdated(updatedNetworks);
            }
          });
    }
  }
}
