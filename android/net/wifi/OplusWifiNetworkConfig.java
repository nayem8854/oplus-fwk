package android.net.wifi;

import android.common.IOplusCommonFeature;
import android.common.OplusFrameworkFactory;
import android.content.Context;
import android.os.OplusBaseEnvironment;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class OplusWifiNetworkConfig implements IOplusWifiNetworkConfig {
  static {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("");
    stringBuilder.append(OplusBaseEnvironment.getMyProductDirectory().getAbsolutePath());
    stringBuilder.append("/etc/wifi_network_config.xml");
    MY_PRODUCT_FILE_DIR = stringBuilder.toString();
    stringBuilder = new StringBuilder();
    stringBuilder.append("");
    stringBuilder.append(OplusBaseEnvironment.getMyRegionDirectory().getAbsolutePath());
    stringBuilder.append("/etc/wifi_network_config.xml");
    MY_COUNTRY_FILE_DIR = stringBuilder.toString();
    DEBUG = false;
  }
  
  private String[] mInternalServers = new String[] { "conn4.coloros.com,conn5.coloros.com" };
  
  private String[] mPublicHttpsServers = new String[] { "https://conn5.coloros.com/generate204", "https://conn4.coloros.com/generate204" };
  
  private String[] mFallbackHttpServers = new String[] { "http://conn3.coloros.com/generate204", "http://connectivitycheck.gstatic.com/generate_204" };
  
  private HashMap<String, String> mKeyValuePair = new HashMap<>();
  
  public static OplusWifiNetworkConfig getInstance(Context paramContext) {
    // Byte code:
    //   0: ldc android/net/wifi/OplusWifiNetworkConfig
    //   2: monitorenter
    //   3: getstatic android/net/wifi/OplusWifiNetworkConfig.sInstance : Landroid/net/wifi/OplusWifiNetworkConfig;
    //   6: ifnonnull -> 22
    //   9: new android/net/wifi/OplusWifiNetworkConfig
    //   12: astore_1
    //   13: aload_1
    //   14: aload_0
    //   15: invokespecial <init> : (Landroid/content/Context;)V
    //   18: aload_1
    //   19: putstatic android/net/wifi/OplusWifiNetworkConfig.sInstance : Landroid/net/wifi/OplusWifiNetworkConfig;
    //   22: getstatic android/net/wifi/OplusWifiNetworkConfig.sInstance : Landroid/net/wifi/OplusWifiNetworkConfig;
    //   25: astore_0
    //   26: ldc android/net/wifi/OplusWifiNetworkConfig
    //   28: monitorexit
    //   29: aload_0
    //   30: areturn
    //   31: astore_0
    //   32: ldc android/net/wifi/OplusWifiNetworkConfig
    //   34: monitorexit
    //   35: aload_0
    //   36: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #70	-> 0
    //   #71	-> 3
    //   #72	-> 9
    //   #74	-> 22
    //   #75	-> 31
    // Exception table:
    //   from	to	target	type
    //   3	9	31	finally
    //   9	22	31	finally
    //   22	29	31	finally
    //   32	35	31	finally
  }
  
  private IWifiRomUpdateHelper mWifiRomUpdateHelper = null;
  
  private static boolean DEBUG = false;
  
  private static final String DEFAULT_DNS = "8.8.8.8";
  
  private static final String DEFAULT_HTTPS_URL_EXP = "https://www.google.com/generate_204";
  
  private static final String DEFAULT_HTTP_URL_EXP = "http://connectivitycheck.gstatic.com/generate_204";
  
  private static final String DEFAULT_NETWORK_BACKUP_DNS_EXP = "8.8.8.8,8.8.4.4";
  
  private static final String DEFAULT_SPECIAL_URL = "unknow";
  
  private static final String MY_COUNTRY_FILE_DIR;
  
  private static final String MY_PRODUCT_FILE_DIR;
  
  private static final String TAG = "OplusNetworkConfig";
  
  private static OplusWifiNetworkConfig sInstance;
  
  private Context mContext;
  
  public String getRomUpdateValue(String paramString1, String paramString2) {
    IWifiRomUpdateHelper iWifiRomUpdateHelper = this.mWifiRomUpdateHelper;
    if (iWifiRomUpdateHelper != null)
      return iWifiRomUpdateHelper.getValue(paramString1, paramString2); 
    return paramString2;
  }
  
  private OplusWifiNetworkConfig(Context paramContext) {
    this.mContext = paramContext;
    this.mWifiRomUpdateHelper = (IWifiRomUpdateHelper)OplusFrameworkFactory.getInstance().getFeature((IOplusCommonFeature)IWifiRomUpdateHelper.DEFAULT, new Object[] { this.mContext });
    init();
    dump();
  }
  
  private String readFromFile(File paramFile) {
    if (paramFile == null)
      return ""; 
    if (!paramFile.exists())
      return ""; 
    FileInputStream fileInputStream1 = null, fileInputStream2 = null, fileInputStream3 = null;
    FileInputStream fileInputStream4 = fileInputStream3, fileInputStream5 = fileInputStream1, fileInputStream6 = fileInputStream2;
    try {
      FileInputStream fileInputStream8 = new FileInputStream();
      fileInputStream4 = fileInputStream3;
      fileInputStream5 = fileInputStream1;
      fileInputStream6 = fileInputStream2;
      this(paramFile);
      FileInputStream fileInputStream7 = fileInputStream8;
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      BufferedReader bufferedReader = new BufferedReader();
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      InputStreamReader inputStreamReader = new InputStreamReader();
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      this(fileInputStream7);
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      this(inputStreamReader);
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      StringBuffer stringBuffer = new StringBuffer();
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      this();
      while (true) {
        fileInputStream4 = fileInputStream7;
        fileInputStream5 = fileInputStream7;
        fileInputStream6 = fileInputStream7;
        String str1 = bufferedReader.readLine();
        if (str1 != null) {
          fileInputStream4 = fileInputStream7;
          fileInputStream5 = fileInputStream7;
          fileInputStream6 = fileInputStream7;
          stringBuffer.append(str1);
          continue;
        } 
        break;
      } 
      fileInputStream4 = fileInputStream7;
      fileInputStream5 = fileInputStream7;
      fileInputStream6 = fileInputStream7;
      String str = stringBuffer.toString();
      try {
        fileInputStream7.close();
      } catch (IOException iOException) {
        iOException.printStackTrace();
      } 
      return str;
    } catch (FileNotFoundException fileNotFoundException) {
      fileInputStream4 = fileInputStream6;
      fileNotFoundException.printStackTrace();
      if (fileInputStream6 != null)
        fileInputStream6.close(); 
    } catch (IOException iOException) {
      fileInputStream4 = fileInputStream5;
      iOException.printStackTrace();
      if (fileInputStream5 != null)
        try {
          fileInputStream5.close();
        } catch (IOException iOException1) {
          iOException1.printStackTrace();
        }  
    } finally {}
    return null;
  }
  
  private void init() {
    Log.d("OplusNetworkConfig", "OplusWifiNetworkConfig init...");
    File file1 = new File(MY_PRODUCT_FILE_DIR);
    File file2 = new File(MY_COUNTRY_FILE_DIR);
    if (file1.exists()) {
      parseContentFromXML(readFromFile(file1));
    } else {
      parseContentFromXML(readFromFile(file2));
    } 
  }
  
  private void parseContentFromXML(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: ldc 'parseContentFromXML'
    //   3: invokespecial logD : (Ljava/lang/String;)V
    //   6: aload_1
    //   7: ifnonnull -> 19
    //   10: ldc 'OplusNetworkConfig'
    //   12: ldc '\\tcontent is null'
    //   14: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   17: pop
    //   18: return
    //   19: aconst_null
    //   20: astore_2
    //   21: aconst_null
    //   22: astore_3
    //   23: aconst_null
    //   24: astore #4
    //   26: aload #4
    //   28: astore #5
    //   30: aload_2
    //   31: astore #6
    //   33: aload_3
    //   34: astore #7
    //   36: invokestatic newPullParser : ()Lorg/xmlpull/v1/XmlPullParser;
    //   39: astore #8
    //   41: aload #4
    //   43: astore #5
    //   45: aload_2
    //   46: astore #6
    //   48: aload_3
    //   49: astore #7
    //   51: new java/io/StringReader
    //   54: astore #9
    //   56: aload #4
    //   58: astore #5
    //   60: aload_2
    //   61: astore #6
    //   63: aload_3
    //   64: astore #7
    //   66: aload #9
    //   68: aload_1
    //   69: invokespecial <init> : (Ljava/lang/String;)V
    //   72: aload #9
    //   74: astore_1
    //   75: aload_1
    //   76: astore #5
    //   78: aload_1
    //   79: astore #6
    //   81: aload_1
    //   82: astore #7
    //   84: aload #8
    //   86: aload_1
    //   87: invokeinterface setInput : (Ljava/io/Reader;)V
    //   92: aload_1
    //   93: astore #5
    //   95: aload_1
    //   96: astore #6
    //   98: aload_1
    //   99: astore #7
    //   101: aload #8
    //   103: invokeinterface getEventType : ()I
    //   108: istore #10
    //   110: iload #10
    //   112: iconst_1
    //   113: if_icmpeq -> 220
    //   116: iload #10
    //   118: iconst_2
    //   119: if_icmpeq -> 125
    //   122: goto -> 199
    //   125: aload_1
    //   126: astore #5
    //   128: aload_1
    //   129: astore #6
    //   131: aload_1
    //   132: astore #7
    //   134: aload #8
    //   136: invokeinterface getName : ()Ljava/lang/String;
    //   141: astore #9
    //   143: aload_1
    //   144: astore #5
    //   146: aload_1
    //   147: astore #6
    //   149: aload_1
    //   150: astore #7
    //   152: aload #8
    //   154: invokeinterface next : ()I
    //   159: pop
    //   160: aload_1
    //   161: astore #5
    //   163: aload_1
    //   164: astore #6
    //   166: aload_1
    //   167: astore #7
    //   169: aload #8
    //   171: invokeinterface getText : ()Ljava/lang/String;
    //   176: astore #4
    //   178: aload_1
    //   179: astore #5
    //   181: aload_1
    //   182: astore #6
    //   184: aload_1
    //   185: astore #7
    //   187: aload_0
    //   188: getfield mKeyValuePair : Ljava/util/HashMap;
    //   191: aload #9
    //   193: aload #4
    //   195: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   198: pop
    //   199: aload_1
    //   200: astore #5
    //   202: aload_1
    //   203: astore #6
    //   205: aload_1
    //   206: astore #7
    //   208: aload #8
    //   210: invokeinterface next : ()I
    //   215: istore #10
    //   217: goto -> 110
    //   220: aload_1
    //   221: invokevirtual close : ()V
    //   224: goto -> 277
    //   227: astore_1
    //   228: goto -> 284
    //   231: astore_1
    //   232: aload #6
    //   234: astore #5
    //   236: aload_0
    //   237: ldc 'Got execption parsing permissions.'
    //   239: aload_1
    //   240: invokespecial logD : (Ljava/lang/String;Ljava/lang/Exception;)V
    //   243: aload #6
    //   245: ifnull -> 277
    //   248: aload #6
    //   250: astore_1
    //   251: goto -> 220
    //   254: astore_1
    //   255: aload #7
    //   257: astore #5
    //   259: aload_0
    //   260: ldc 'Got execption parsing permissions.'
    //   262: aload_1
    //   263: invokespecial logD : (Ljava/lang/String;Ljava/lang/Exception;)V
    //   266: aload #7
    //   268: ifnull -> 277
    //   271: aload #7
    //   273: astore_1
    //   274: goto -> 220
    //   277: aload_0
    //   278: ldc '\\txml file parse end!'
    //   280: invokespecial logD : (Ljava/lang/String;)V
    //   283: return
    //   284: aload #5
    //   286: ifnull -> 294
    //   289: aload #5
    //   291: invokevirtual close : ()V
    //   294: aload_1
    //   295: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #139	-> 0
    //   #140	-> 6
    //   #141	-> 10
    //   #142	-> 18
    //   #144	-> 19
    //   #145	-> 19
    //   #146	-> 19
    //   #148	-> 26
    //   #149	-> 41
    //   #150	-> 75
    //   #151	-> 92
    //   #152	-> 110
    //   #153	-> 116
    //   #157	-> 125
    //   #158	-> 143
    //   #159	-> 160
    //   #164	-> 178
    //   #166	-> 199
    //   #170	-> 199
    //   #177	-> 220
    //   #178	-> 220
    //   #177	-> 227
    //   #174	-> 231
    //   #175	-> 232
    //   #177	-> 243
    //   #178	-> 248
    //   #172	-> 254
    //   #173	-> 255
    //   #177	-> 266
    //   #178	-> 271
    //   #181	-> 277
    //   #182	-> 283
    //   #177	-> 284
    //   #178	-> 289
    //   #180	-> 294
    // Exception table:
    //   from	to	target	type
    //   36	41	254	org/xmlpull/v1/XmlPullParserException
    //   36	41	231	java/io/IOException
    //   36	41	227	finally
    //   51	56	254	org/xmlpull/v1/XmlPullParserException
    //   51	56	231	java/io/IOException
    //   51	56	227	finally
    //   66	72	254	org/xmlpull/v1/XmlPullParserException
    //   66	72	231	java/io/IOException
    //   66	72	227	finally
    //   84	92	254	org/xmlpull/v1/XmlPullParserException
    //   84	92	231	java/io/IOException
    //   84	92	227	finally
    //   101	110	254	org/xmlpull/v1/XmlPullParserException
    //   101	110	231	java/io/IOException
    //   101	110	227	finally
    //   134	143	254	org/xmlpull/v1/XmlPullParserException
    //   134	143	231	java/io/IOException
    //   134	143	227	finally
    //   152	160	254	org/xmlpull/v1/XmlPullParserException
    //   152	160	231	java/io/IOException
    //   152	160	227	finally
    //   169	178	254	org/xmlpull/v1/XmlPullParserException
    //   169	178	231	java/io/IOException
    //   169	178	227	finally
    //   187	199	254	org/xmlpull/v1/XmlPullParserException
    //   187	199	231	java/io/IOException
    //   187	199	227	finally
    //   208	217	254	org/xmlpull/v1/XmlPullParserException
    //   208	217	231	java/io/IOException
    //   208	217	227	finally
    //   236	243	227	finally
    //   259	266	227	finally
  }
  
  private String getValue(String paramString1, String paramString2) {
    paramString1 = this.mKeyValuePair.get(paramString1);
    if (paramString1 == null)
      return paramString2; 
    return paramString1;
  }
  
  private void dump() {
    logD("dump:");
    Iterator<String> iterator = this.mKeyValuePair.keySet().iterator();
    while (iterator.hasNext()) {
      String str1 = iterator.next();
      String str2 = this.mKeyValuePair.get(str1);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("\t");
      stringBuilder.append(str1);
      stringBuilder.append(":");
      stringBuilder.append(str2);
      logD(stringBuilder.toString());
    } 
  }
  
  public boolean inSpecialUrlList(String paramString) {
    if (paramString == null) {
      logD("url is null.");
      return false;
    } 
    String str1 = getRomUpdateValue("NETWORK_SPECIAL_REDIRECT_URL", null);
    str2 = str1;
    if (str1 == null)
      str2 = getValue("NETWORK_SPECIAL_REDIRECT_URL", null); 
    if (str2 == null) {
      logD("Fail to getRomUpdateValue & defaultxml");
      return false;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("inSpecialUrlList(), url list: ");
    stringBuilder.append(str2);
    logD(stringBuilder.toString());
    for (String str2 : str2.split(",")) {
      if (paramString.contains(str2))
        return true; 
    } 
    return false;
  }
  
  public List<String> getServers(String paramString, String[] paramArrayOfString) {
    List<String> list = Arrays.asList(paramArrayOfString);
    str2 = getRomUpdateValue(paramString, null);
    String str1 = str2;
    if (str2 == null)
      str1 = getValue(paramString, null); 
    if (str1 == null) {
      logD("romupdate & defaultxml is null, using default servers!");
      return list;
    } 
    ArrayList<String> arrayList = new ArrayList();
    for (String str2 : str1.split(","))
      arrayList.add(str2); 
    if (arrayList.size() >= 2)
      return arrayList; 
    logD("updated Servers less than 2, using default servers!");
    return list;
  }
  
  public List<String> getInternalServers() {
    return getServers("OPLUS_WIFI_ASSISTANT_NETSERVER", this.mInternalServers);
  }
  
  public List<String> getPublicHttpsServers() {
    return getServers("NETWORK_PUBLIC_HTTPS_SERVERS_URL", this.mPublicHttpsServers);
  }
  
  public List<String> getFallbackServers() {
    return getServers("NETWORK_FALLBACK_HTTP_SERVERS_URL", this.mFallbackHttpServers);
  }
  
  public String getExpHttpUrl() {
    String str1 = getRomUpdateValue("NETWORK_EXP_CAPTIVE_SERVER_HTTP_URL", null);
    String str2 = str1;
    if (str1 == null)
      str2 = getValue("NETWORK_EXP_CAPTIVE_SERVER_HTTP_URL", null); 
    if (str2 == null)
      return "http://connectivitycheck.gstatic.com/generate_204"; 
    return str2;
  }
  
  public String getExpHttpsUrl() {
    String str1 = getRomUpdateValue("NETWORK_EXP_CAPTIVE_SERVER_HTTPS_URL", null);
    String str2 = str1;
    if (str1 == null)
      str2 = getValue("NETWORK_EXP_CAPTIVE_SERVER_HTTPS_URL", null); 
    if (str2 == null)
      return "https://www.google.com/generate_204"; 
    return str2;
  }
  
  public String getDefaultDns(boolean paramBoolean) {
    String str1;
    if (paramBoolean) {
      str1 = getRomUpdateValue("NETWORK_EXP_DEFAULT_DNS", null);
    } else {
      str1 = getRomUpdateValue("NETWORK_DEFAULT_DNS", null);
    } 
    String str2 = str1;
    if (str1 == null)
      if (paramBoolean) {
        str2 = getValue("NETWORK_EXP_DEFAULT_DNS", null);
      } else {
        str2 = getValue("NETWORK_DEFAULT_DNS", null);
      }  
    if (str2 == null)
      return "8.8.8.8"; 
    return str2;
  }
  
  public String[] getBackupDnsServer(boolean paramBoolean) {
    if (paramBoolean) {
      str1 = getRomUpdateValue("NETWORK_BACKUP_DNS_EXP", null);
    } else {
      str1 = getRomUpdateValue("NETWORK_BACKUP_DNS", null);
    } 
    String str2 = str1;
    if (str1 == null)
      if (paramBoolean) {
        str2 = getValue("NETWORK_BACKUP_DNS_EXP", null);
      } else {
        str2 = getValue("NETWORK_BACKUP_DNS", null);
      }  
    String str1 = str2;
    if (str2 == null) {
      logD("get from rom update null\n");
      str1 = "8.8.8.8,8.8.4.4";
    } 
    return str1.split(",");
  }
  
  private void logD(String paramString) {
    if (!DEBUG)
      return; 
    Log.d("OplusNetworkConfig", paramString);
  }
  
  private void logD(String paramString, Exception paramException) {
    if (!DEBUG)
      return; 
    Log.d("OplusNetworkConfig", paramString);
  }
}
