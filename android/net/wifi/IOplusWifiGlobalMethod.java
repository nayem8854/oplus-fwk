package android.net.wifi;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;

public interface IOplusWifiGlobalMethod extends IOplusCommonFeature {
  public static final IOplusWifiGlobalMethod DEFAULT = (IOplusWifiGlobalMethod)new Object();
  
  public static final String NAME = "IOplusWifiGlobalMethod";
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusWifiGlobalMethod;
  }
  
  default IOplusCommonFeature getDefault() {
    return DEFAULT;
  }
  
  default boolean isNotChineseOperator() {
    return false;
  }
}
