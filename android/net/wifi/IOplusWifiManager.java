package android.net.wifi;

import android.os.RemoteException;
import java.util.List;
import oplus.net.wifi.HotspotClient;

public interface IOplusWifiManager {
  public static final String ACTION_OPLUS_NETWORK_CONDITIONS_MEASURED = "android.net.conn.OPLUS_NETWORK_CONDITIONS_MEASURED";
  
  public static final int AUTOCONNECT_PORTAL_LOGIN = 2;
  
  public static final String DESCRIPTOR = "android.net.wifi.IOplusWifiManager";
  
  public static final int MANUCONNECT_PORTAL_LOGIN = 1;
  
  public static final int OPLUS_CALL_TRANSACTION_INDEX = 10000;
  
  public static final int OPLUS_FIRST_CALL_TRANSACTION = 10001;
  
  public static final int OTHER_LOGIN = 0;
  
  public static final int TRANSACTION_addAuthResult = 10080;
  
  public static final int TRANSACTION_checkExandsCAExist = 10027;
  
  public static final int TRANSACTION_checkExandsXMLCAExpired = 10028;
  
  public static final int TRANSACTION_checkExandsXMLExist = 10026;
  
  public static final int TRANSACTION_checkExandsXWifiConfigExpired = 10029;
  
  public static final int TRANSACTION_checkFWKSupportPasspoint = 10037;
  
  public static final int TRANSACTION_checkInternalPasspointPresetProvider = 10036;
  
  public static final int TRANSACTION_checkPasspointCAExist = 10031;
  
  public static final int TRANSACTION_checkPasspointProfileExpired = 10033;
  
  public static final int TRANSACTION_checkPasspointXMLCAExpired = 10032;
  
  public static final int TRANSACTION_checkPasspointXMLExist = 10030;
  
  public static final int TRANSACTION_clearWifiOCloudData = 10017;
  
  public static final int TRANSACTION_dealWithCloudBackupResult = 10051;
  
  public static final int TRANSACTION_dealWithCloudRecoveryData = 10052;
  
  public static final int TRANSACTION_disableDualSta = 10014;
  
  public static final int TRANSACTION_enableDualSta = 10012;
  
  public static final int TRANSACTION_enableDualStaByForce = 10013;
  
  public static final int TRANSACTION_getAllDualStaApps = 10011;
  
  public static final int TRANSACTION_getAllSlaAcceleratedApps = 10010;
  
  public static final int TRANSACTION_getAllSlaAppsAndStates = 10009;
  
  public static final int TRANSACTION_getAllowedHotspotClients = 10056;
  
  public static final int TRANSACTION_getAvoidChannels = 10076;
  
  public static final int TRANSACTION_getBlockedHotspotClients = 10055;
  
  public static final int TRANSACTION_getConnectedHotspotClients = 10057;
  
  public static final int TRANSACTION_getDBSCapacity = 10073;
  
  public static final int TRANSACTION_getDualStaReadyState = 10078;
  
  public static final int TRANSACTION_getDualStaSwitchDefault = 10082;
  
  public static final int TRANSACTION_getFTMState = 10069;
  
  public static final int TRANSACTION_getHSRelease = 10040;
  
  public static final int TRANSACTION_getMatchingWifiConfig = 10025;
  
  public static final int TRANSACTION_getOplusSta2ConnectionInfo = 10015;
  
  public static final int TRANSACTION_getOplusSta2CurConfigKey = 10016;
  
  public static final int TRANSACTION_getOplusSupportedFeatures = 10002;
  
  public static final int TRANSACTION_getOplusWifiTransactionHelperMessenger = 10044;
  
  public static final int TRANSACTION_getPHYCapacity = 10074;
  
  public static final int TRANSACTION_getPasspointUserName = 10039;
  
  public static final int TRANSACTION_getPortalLoginType = 10024;
  
  public static final int TRANSACTION_getSlaAppState = 10008;
  
  public static final int TRANSACTION_getSnifferState = 10066;
  
  public static final int TRANSACTION_getSupportedChannels = 10075;
  
  public static final int TRANSACTION_getWifiOCloudData = 10018;
  
  public static final int TRANSACTION_getWifiRestrictionList = 10060;
  
  public static final int TRANSACTION_getWifiSharingConfiguration = 10005;
  
  public static final int TRANSACTION_hasOCloudDirtyData = 10022;
  
  public static final int TRANSACTION_isP2p5GSupported = 10045;
  
  public static final int TRANSACTION_isSoftap5GSupported = 10046;
  
  public static final int TRANSACTION_isWifiAutoConnectionDisabled = 10063;
  
  public static final int TRANSACTION_manualForgetStatistics = 10034;
  
  public static final int TRANSACTION_notifyGameHighTemperature = 10083;
  
  public static final int TRANSACTION_notifyGameLatency = 10077;
  
  public static final int TRANSACTION_notifyGameModeState = 10058;
  
  public static final int TRANSACTION_passpointANQPScanResults = 10035;
  
  public static final int TRANSACTION_removeFromRestrictionList = 10061;
  
  public static final int TRANSACTION_removeHeIeFromProbeRequest = 10071;
  
  public static final int TRANSACTION_removeNetworkByGlobalId = 10020;
  
  public static final int TRANSACTION_requestToEnableSta2ByAPP = 10079;
  
  public static final int TRANSACTION_requestToReleaseSta2ByAPP = 10081;
  
  public static final int TRANSACTION_sendFTMCommand = 10070;
  
  public static final int TRANSACTION_setAllowedHotspotClients = 10054;
  
  public static final int TRANSACTION_setBlockedHotspotClients = 10053;
  
  public static final int TRANSACTION_setDirtyFlag = 10021;
  
  public static final int TRANSACTION_setNetworkCaptiveState = 10023;
  
  public static final int TRANSACTION_setP2pPowerSave = 10072;
  
  public static final int TRANSACTION_setPasspointCertifiedState = 10038;
  
  public static final int TRANSACTION_setSlaAppState = 10007;
  
  public static final int TRANSACTION_setWifiAutoConnectionDisabled = 10062;
  
  public static final int TRANSACTION_setWifiClosedByUser = 10043;
  
  public static final int TRANSACTION_setWifiRestrictionList = 10059;
  
  public static final int TRANSACTION_setWifiSharingConfiguration = 10006;
  
  public static final int TRANSACTION_startFTMMode = 10067;
  
  public static final int TRANSACTION_startRxSensTest = 10041;
  
  public static final int TRANSACTION_startSnifferMode = 10064;
  
  public static final int TRANSACTION_startWifiSharing = 10003;
  
  public static final int TRANSACTION_stopFTMMode = 10068;
  
  public static final int TRANSACTION_stopRxSensTest = 10042;
  
  public static final int TRANSACTION_stopSnifferMode = 10065;
  
  public static final int TRANSACTION_stopWifiSharing = 10004;
  
  public static final int TRANSACTION_updateGlobalId = 10019;
  
  void addAuthResultInfo(int paramInt1, int paramInt2, int paramInt3, String paramString) throws RemoteException;
  
  boolean checkFWKSupportPasspoint() throws RemoteException;
  
  boolean checkInternalPasspointPresetProvider(String paramString) throws RemoteException;
  
  boolean checkPasspointCAExist(String paramString) throws RemoteException;
  
  boolean checkPasspointXMLCAExpired(String paramString) throws RemoteException;
  
  void clearWifiOCloudData(boolean paramBoolean) throws RemoteException;
  
  void dealWithCloudBackupResult(List<String> paramList, String paramString) throws RemoteException;
  
  void dealWithCloudRecoveryData(List<String> paramList, String paramString) throws RemoteException;
  
  void disableDualSta() throws RemoteException;
  
  int enableDualSta() throws RemoteException;
  
  int enableDualStaByForce(boolean paramBoolean) throws RemoteException;
  
  String[] getAllDualStaApps() throws RemoteException;
  
  String[] getAllSlaAcceleratedApps() throws RemoteException;
  
  String[] getAllSlaAppsAndStates() throws RemoteException;
  
  List<HotspotClient> getAllowedHotspotClients() throws RemoteException;
  
  int[] getAvoidChannels() throws RemoteException;
  
  List<HotspotClient> getBlockedHotspotClients() throws RemoteException;
  
  List<HotspotClient> getConnectedHotspotClients() throws RemoteException;
  
  int getDBSCapacity() throws RemoteException;
  
  boolean getDualStaReadyStateForAPP(String paramString) throws RemoteException;
  
  boolean getDualStaSwitchDefault() throws RemoteException;
  
  int getFTMState() throws RemoteException;
  
  WifiInfo getOplusSta2ConnectionInfo() throws RemoteException;
  
  String getOplusSta2CurConfigKey() throws RemoteException;
  
  long getOplusSupportedFeatures() throws RemoteException;
  
  int getPHYCapacity(int paramInt) throws RemoteException;
  
  String getPasspointUserName(String paramString) throws RemoteException;
  
  int getPortalLoginType() throws RemoteException;
  
  boolean getSlaAppState(String paramString) throws RemoteException;
  
  int getSnifferState() throws RemoteException;
  
  int[] getSupportedChannels(int paramInt) throws RemoteException;
  
  List<String> getWifiOCloudData(boolean paramBoolean) throws RemoteException;
  
  WifiConfiguration getWifiSharingConfiguration() throws RemoteException;
  
  boolean hasOCloudDirtyData() throws RemoteException;
  
  boolean isDualStaSupported() throws RemoteException;
  
  boolean isDualStaSupportedForAPP(String paramString) throws RemoteException;
  
  boolean isP2p5GSupported() throws RemoteException;
  
  boolean isSoftap5GSupported() throws RemoteException;
  
  void notifyGameHighTemperature(int paramInt, String paramString) throws RemoteException;
  
  void notifyGameLatency(String paramString1, String paramString2) throws RemoteException;
  
  void notifyGameModeState(boolean paramBoolean, String paramString) throws RemoteException;
  
  List<ScanResult> passpointANQPScanResults(List<ScanResult> paramList) throws RemoteException;
  
  void removeHeIeFromProbeRequest(boolean paramBoolean) throws RemoteException;
  
  void removeNetworkByGlobalId(String paramString1, String paramString2, boolean paramBoolean) throws RemoteException;
  
  int requestToEnableSta2ByAPP(String paramString) throws RemoteException;
  
  boolean requestToReleaseSta2ByAPP(int paramInt1, int paramInt2, String paramString) throws RemoteException;
  
  int sendFTMCommand(String paramString) throws RemoteException;
  
  void setAllowedHotspotClients(List<HotspotClient> paramList) throws RemoteException;
  
  void setBlockedHotspotClients(List<HotspotClient> paramList) throws RemoteException;
  
  void setDirtyFlag(String paramString, boolean paramBoolean) throws RemoteException;
  
  void setNetworkCaptiveState(boolean paramBoolean) throws RemoteException;
  
  void setP2pPowerSave(boolean paramBoolean) throws RemoteException;
  
  boolean setPasspointCertifiedState(String paramString) throws RemoteException;
  
  boolean setSlaAppState(String paramString, boolean paramBoolean) throws RemoteException;
  
  boolean setWifiClosedByUser(boolean paramBoolean) throws RemoteException;
  
  boolean setWifiSharingConfiguration(WifiConfiguration paramWifiConfiguration) throws RemoteException;
  
  int startFTMMode() throws RemoteException;
  
  boolean startRxSensTest(WifiConfiguration paramWifiConfiguration, String paramString) throws RemoteException;
  
  int startSnifferMode(int paramInt1, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  boolean startWifiSharing(WifiConfiguration paramWifiConfiguration) throws RemoteException;
  
  int stopFTMMode() throws RemoteException;
  
  void stopRxSensTest() throws RemoteException;
  
  int stopSnifferMode() throws RemoteException;
  
  boolean stopWifiSharing() throws RemoteException;
  
  void updateGlobalId(int paramInt, String paramString) throws RemoteException;
}
