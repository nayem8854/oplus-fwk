package android.net.wifi;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import java.util.List;

public interface IOplusWifiNetworkConfig extends IOplusCommonFeature {
  public static final IOplusWifiNetworkConfig DEFAULT = (IOplusWifiNetworkConfig)new Object();
  
  public static final String NAME = "IOplusWifiNetworkConfig";
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusWifiNetworkConfig;
  }
  
  default IOplusCommonFeature getDefault() {
    return DEFAULT;
  }
  
  default boolean inSpecialUrlList(String paramString) {
    return false;
  }
  
  default List<String> getInternalServers() {
    return null;
  }
  
  default List<String> getPublicHttpsServers() {
    return null;
  }
  
  default List<String> getFallbackServers() {
    return null;
  }
  
  default String getExpHttpUrl() {
    return null;
  }
  
  default String getExpHttpsUrl() {
    return null;
  }
  
  default String getDefaultDns(boolean paramBoolean) {
    return null;
  }
  
  default String[] getBackupDnsServer(boolean paramBoolean) {
    return null;
  }
}
