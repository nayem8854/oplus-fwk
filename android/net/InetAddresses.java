package android.net;

import java.net.InetAddress;
import libcore.net.InetAddressUtils;

public class InetAddresses {
  public static boolean isNumericAddress(String paramString) {
    return InetAddressUtils.isNumericAddress(paramString);
  }
  
  public static InetAddress parseNumericAddress(String paramString) {
    return InetAddressUtils.parseNumericAddress(paramString);
  }
}
