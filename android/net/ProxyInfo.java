package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;
import java.util.Locale;

public class ProxyInfo implements Parcelable {
  public static ProxyInfo buildDirectProxy(String paramString, int paramInt) {
    return new ProxyInfo(paramString, paramInt, null);
  }
  
  public static ProxyInfo buildDirectProxy(String paramString, int paramInt, List<String> paramList) {
    String[] arrayOfString = paramList.<String>toArray(new String[paramList.size()]);
    return new ProxyInfo(paramString, paramInt, TextUtils.join(",", (Object[])arrayOfString), arrayOfString);
  }
  
  public static ProxyInfo buildPacProxy(Uri paramUri) {
    return new ProxyInfo(paramUri);
  }
  
  public static ProxyInfo buildPacProxy(Uri paramUri, int paramInt) {
    return new ProxyInfo(paramUri, paramInt);
  }
  
  public ProxyInfo(String paramString1, int paramInt, String paramString2) {
    this.mHost = paramString1;
    this.mPort = paramInt;
    this.mExclusionList = paramString2;
    this.mParsedExclusionList = parseExclusionList(paramString2);
    this.mPacFileUrl = Uri.EMPTY;
  }
  
  public ProxyInfo(Uri paramUri) {
    this.mHost = "localhost";
    this.mPort = -1;
    this.mExclusionList = "";
    this.mParsedExclusionList = parseExclusionList("");
    if (paramUri != null) {
      this.mPacFileUrl = paramUri;
      return;
    } 
    throw null;
  }
  
  public ProxyInfo(String paramString) {
    this.mHost = "localhost";
    this.mPort = -1;
    this.mExclusionList = "";
    this.mParsedExclusionList = parseExclusionList("");
    this.mPacFileUrl = Uri.parse(paramString);
  }
  
  public ProxyInfo(Uri paramUri, int paramInt) {
    this.mHost = "localhost";
    this.mPort = paramInt;
    this.mExclusionList = "";
    this.mParsedExclusionList = parseExclusionList("");
    if (paramUri != null) {
      this.mPacFileUrl = paramUri;
      return;
    } 
    throw null;
  }
  
  private static String[] parseExclusionList(String paramString) {
    if (paramString == null)
      return new String[0]; 
    return paramString.toLowerCase(Locale.ROOT).split(",");
  }
  
  private ProxyInfo(String paramString1, int paramInt, String paramString2, String[] paramArrayOfString) {
    this.mHost = paramString1;
    this.mPort = paramInt;
    this.mExclusionList = paramString2;
    this.mParsedExclusionList = paramArrayOfString;
    this.mPacFileUrl = Uri.EMPTY;
  }
  
  public ProxyInfo(ProxyInfo paramProxyInfo) {
    if (paramProxyInfo != null) {
      this.mHost = paramProxyInfo.getHost();
      this.mPort = paramProxyInfo.getPort();
      this.mPacFileUrl = paramProxyInfo.mPacFileUrl;
      this.mExclusionList = paramProxyInfo.getExclusionListAsString();
      this.mParsedExclusionList = paramProxyInfo.mParsedExclusionList;
    } else {
      this.mHost = null;
      this.mPort = 0;
      this.mExclusionList = null;
      this.mParsedExclusionList = null;
      this.mPacFileUrl = Uri.EMPTY;
    } 
  }
  
  public InetSocketAddress getSocketAddress() {
    InetSocketAddress inetSocketAddress = null;
    try {
      InetSocketAddress inetSocketAddress1 = new InetSocketAddress();
      this(this.mHost, this.mPort);
      inetSocketAddress = inetSocketAddress1;
    } catch (IllegalArgumentException illegalArgumentException) {}
    return inetSocketAddress;
  }
  
  public Uri getPacFileUrl() {
    return this.mPacFileUrl;
  }
  
  public String getHost() {
    return this.mHost;
  }
  
  public int getPort() {
    return this.mPort;
  }
  
  public String[] getExclusionList() {
    return this.mParsedExclusionList;
  }
  
  public String getExclusionListAsString() {
    return this.mExclusionList;
  }
  
  public boolean isValid() {
    boolean bool = Uri.EMPTY.equals(this.mPacFileUrl);
    boolean bool1 = true;
    if (!bool)
      return true; 
    String str1 = this.mHost, str2 = "", str3 = str1;
    if (str1 == null)
      str3 = ""; 
    int i = this.mPort;
    if (i == 0) {
      str1 = "";
    } else {
      str1 = Integer.toString(i);
    } 
    String str4 = this.mExclusionList;
    if (str4 != null)
      str2 = str4; 
    if (Proxy.validate(str3, str1, str2) != 0)
      bool1 = false; 
    return bool1;
  }
  
  public Proxy makeProxy() {
    Proxy proxy1 = Proxy.NO_PROXY;
    Proxy proxy2 = proxy1;
    if (this.mHost != null)
      try {
        InetSocketAddress inetSocketAddress = new InetSocketAddress();
        this(this.mHost, this.mPort);
        proxy2 = new Proxy();
        this(Proxy.Type.HTTP, inetSocketAddress);
      } catch (IllegalArgumentException illegalArgumentException) {
        proxy2 = proxy1;
      }  
    return proxy2;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    if (!Uri.EMPTY.equals(this.mPacFileUrl)) {
      stringBuilder.append("PAC Script: ");
      stringBuilder.append(this.mPacFileUrl);
    } 
    if (this.mHost != null) {
      stringBuilder.append("[");
      stringBuilder.append(this.mHost);
      stringBuilder.append("] ");
      stringBuilder.append(Integer.toString(this.mPort));
      if (this.mExclusionList != null) {
        stringBuilder.append(" xl=");
        stringBuilder.append(this.mExclusionList);
      } 
    } else {
      stringBuilder.append("[ProxyProperties.mHost == null]");
    } 
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof ProxyInfo;
    boolean bool1 = false;
    if (!bool)
      return false; 
    ProxyInfo proxyInfo = (ProxyInfo)paramObject;
    if (!Uri.EMPTY.equals(this.mPacFileUrl)) {
      bool = bool1;
      if (this.mPacFileUrl.equals(proxyInfo.getPacFileUrl())) {
        bool = bool1;
        if (this.mPort == proxyInfo.mPort)
          bool = true; 
      } 
      return bool;
    } 
    if (!Uri.EMPTY.equals(proxyInfo.mPacFileUrl))
      return false; 
    paramObject = this.mExclusionList;
    if (paramObject != null && !paramObject.equals(proxyInfo.getExclusionListAsString()))
      return false; 
    if (this.mHost != null && proxyInfo.getHost() != null && !this.mHost.equals(proxyInfo.getHost()))
      return false; 
    if (this.mHost != null && proxyInfo.mHost == null)
      return false; 
    if (this.mHost == null && proxyInfo.mHost != null)
      return false; 
    if (this.mPort != proxyInfo.mPort)
      return false; 
    return true;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public int hashCode() {
    int j;
    String str = this.mHost;
    int i = 0;
    if (str == null) {
      j = 0;
    } else {
      j = str.hashCode();
    } 
    str = this.mExclusionList;
    if (str != null)
      i = str.hashCode(); 
    int k = this.mPort;
    return j + i + k;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (!Uri.EMPTY.equals(this.mPacFileUrl)) {
      paramParcel.writeByte((byte)1);
      this.mPacFileUrl.writeToParcel(paramParcel, 0);
      paramParcel.writeInt(this.mPort);
      return;
    } 
    paramParcel.writeByte((byte)0);
    if (this.mHost != null) {
      paramParcel.writeByte((byte)1);
      paramParcel.writeString(this.mHost);
      paramParcel.writeInt(this.mPort);
    } else {
      paramParcel.writeByte((byte)0);
    } 
    paramParcel.writeString(this.mExclusionList);
    paramParcel.writeStringArray(this.mParsedExclusionList);
  }
  
  public static final Parcelable.Creator<ProxyInfo> CREATOR = new Parcelable.Creator<ProxyInfo>() {
      public ProxyInfo createFromParcel(Parcel param1Parcel) {
        String str1;
        Uri uri = null;
        int i = 0;
        if (param1Parcel.readByte() != 0) {
          uri = Uri.CREATOR.createFromParcel(param1Parcel);
          i = param1Parcel.readInt();
          return new ProxyInfo(uri, i);
        } 
        if (param1Parcel.readByte() != 0) {
          str1 = param1Parcel.readString();
          i = param1Parcel.readInt();
        } 
        String str2 = param1Parcel.readString();
        String[] arrayOfString = param1Parcel.readStringArray();
        return new ProxyInfo(str1, i, str2, arrayOfString);
      }
      
      public ProxyInfo[] newArray(int param1Int) {
        return new ProxyInfo[param1Int];
      }
    };
  
  public static final String LOCAL_EXCL_LIST = "";
  
  public static final String LOCAL_HOST = "localhost";
  
  public static final int LOCAL_PORT = -1;
  
  private final String mExclusionList;
  
  private final String mHost;
  
  private final Uri mPacFileUrl;
  
  private final String[] mParsedExclusionList;
  
  private final int mPort;
}
