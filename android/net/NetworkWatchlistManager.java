package android.net;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import com.android.internal.net.INetworkWatchlistManager;
import com.android.internal.util.Preconditions;

public class NetworkWatchlistManager {
  private static final String SHARED_MEMORY_TAG = "NETWORK_WATCHLIST_SHARED_MEMORY";
  
  private static final String TAG = "NetworkWatchlistManager";
  
  private final Context mContext;
  
  private final INetworkWatchlistManager mNetworkWatchlistManager;
  
  public NetworkWatchlistManager(Context paramContext, INetworkWatchlistManager paramINetworkWatchlistManager) {
    this.mContext = paramContext;
    this.mNetworkWatchlistManager = paramINetworkWatchlistManager;
  }
  
  public NetworkWatchlistManager(Context paramContext) {
    this.mContext = (Context)Preconditions.checkNotNull(paramContext, "missing context");
    IBinder iBinder = ServiceManager.getService("network_watchlist");
    this.mNetworkWatchlistManager = INetworkWatchlistManager.Stub.asInterface(iBinder);
  }
  
  public void reportWatchlistIfNecessary() {
    try {
      this.mNetworkWatchlistManager.reportWatchlistIfNecessary();
    } catch (RemoteException remoteException) {
      Log.e("NetworkWatchlistManager", "Cannot report records", (Throwable)remoteException);
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reloadWatchlist() {
    try {
      this.mNetworkWatchlistManager.reloadWatchlist();
    } catch (RemoteException remoteException) {
      Log.e("NetworkWatchlistManager", "Unable to reload watchlist");
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public byte[] getWatchlistConfigHash() {
    try {
      return this.mNetworkWatchlistManager.getWatchlistConfigHash();
    } catch (RemoteException remoteException) {
      Log.e("NetworkWatchlistManager", "Unable to get watchlist config hash");
      throw remoteException.rethrowFromSystemServer();
    } 
  }
}
