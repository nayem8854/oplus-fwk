package android.net;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;
import com.android.net.module.util.InetAddressUtils;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@SystemApi
public final class StaticIpConfiguration implements Parcelable {
  public LinkAddress ipAddress;
  
  public InetAddress gateway;
  
  public String domains;
  
  public final ArrayList<InetAddress> dnsServers = new ArrayList<>();
  
  public StaticIpConfiguration(StaticIpConfiguration paramStaticIpConfiguration) {
    this();
    if (paramStaticIpConfiguration != null) {
      this.ipAddress = paramStaticIpConfiguration.ipAddress;
      this.gateway = paramStaticIpConfiguration.gateway;
      this.dnsServers.addAll(paramStaticIpConfiguration.dnsServers);
      this.domains = paramStaticIpConfiguration.domains;
    } 
  }
  
  public void clear() {
    this.ipAddress = null;
    this.gateway = null;
    this.dnsServers.clear();
    this.domains = null;
  }
  
  public LinkAddress getIpAddress() {
    return this.ipAddress;
  }
  
  public InetAddress getGateway() {
    return this.gateway;
  }
  
  public List<InetAddress> getDnsServers() {
    return this.dnsServers;
  }
  
  public String getDomains() {
    return this.domains;
  }
  
  class Builder {
    private Iterable<InetAddress> mDnsServers;
    
    private String mDomains;
    
    private InetAddress mGateway;
    
    private LinkAddress mIpAddress;
    
    public Builder setIpAddress(LinkAddress param1LinkAddress) {
      this.mIpAddress = param1LinkAddress;
      return this;
    }
    
    public Builder setGateway(InetAddress param1InetAddress) {
      this.mGateway = param1InetAddress;
      return this;
    }
    
    public Builder setDnsServers(Iterable<InetAddress> param1Iterable) {
      Preconditions.checkNotNull(param1Iterable);
      this.mDnsServers = param1Iterable;
      return this;
    }
    
    public Builder setDomains(String param1String) {
      this.mDomains = param1String;
      return this;
    }
    
    public StaticIpConfiguration build() {
      StaticIpConfiguration staticIpConfiguration = new StaticIpConfiguration();
      staticIpConfiguration.ipAddress = this.mIpAddress;
      staticIpConfiguration.gateway = this.mGateway;
      Iterable<InetAddress> iterable = this.mDnsServers;
      if (iterable != null)
        for (InetAddress inetAddress : iterable)
          staticIpConfiguration.dnsServers.add(inetAddress);  
      staticIpConfiguration.domains = this.mDomains;
      return staticIpConfiguration;
    }
  }
  
  public void addDnsServer(InetAddress paramInetAddress) {
    this.dnsServers.add(paramInetAddress);
  }
  
  public List<RouteInfo> getRoutes(String paramString) {
    ArrayList<RouteInfo> arrayList = new ArrayList(3);
    if (this.ipAddress != null) {
      RouteInfo routeInfo = new RouteInfo(this.ipAddress, null, paramString);
      arrayList.add(routeInfo);
      InetAddress inetAddress = this.gateway;
      if (inetAddress != null && !routeInfo.matches(inetAddress))
        arrayList.add(RouteInfo.makeHostRoute(this.gateway, paramString)); 
    } 
    if (this.gateway != null)
      arrayList.add(new RouteInfo((IpPrefix)null, this.gateway, paramString)); 
    return arrayList;
  }
  
  public LinkProperties toLinkProperties(String paramString) {
    LinkProperties linkProperties = new LinkProperties();
    linkProperties.setInterfaceName(paramString);
    LinkAddress linkAddress = this.ipAddress;
    if (linkAddress != null)
      linkProperties.addLinkAddress(linkAddress); 
    for (RouteInfo routeInfo : getRoutes(paramString))
      linkProperties.addRoute(routeInfo); 
    for (InetAddress inetAddress : this.dnsServers)
      linkProperties.addDnsServer(inetAddress); 
    linkProperties.setDomains(this.domains);
    return linkProperties;
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append("IP address ");
    LinkAddress linkAddress = this.ipAddress;
    if (linkAddress != null) {
      stringBuffer.append(linkAddress);
      stringBuffer.append(" ");
    } 
    stringBuffer.append("Gateway ");
    InetAddress inetAddress = this.gateway;
    if (inetAddress != null) {
      stringBuffer.append(inetAddress.getHostAddress());
      stringBuffer.append(" ");
    } 
    stringBuffer.append(" DNS servers: [");
    for (InetAddress inetAddress1 : this.dnsServers) {
      stringBuffer.append(" ");
      stringBuffer.append(inetAddress1.getHostAddress());
    } 
    stringBuffer.append(" ] Domains ");
    String str = this.domains;
    if (str != null)
      stringBuffer.append(str); 
    return stringBuffer.toString();
  }
  
  public int hashCode() {
    int j, k;
    LinkAddress linkAddress = this.ipAddress;
    int i = 0;
    if (linkAddress == null) {
      j = 0;
    } else {
      j = linkAddress.hashCode();
    } 
    InetAddress inetAddress = this.gateway;
    if (inetAddress == null) {
      k = 0;
    } else {
      k = inetAddress.hashCode();
    } 
    String str = this.domains;
    if (str != null)
      i = str.hashCode(); 
    int m = this.dnsServers.hashCode();
    return (((13 * 47 + j) * 47 + k) * 47 + i) * 47 + m;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof StaticIpConfiguration))
      return false; 
    paramObject = paramObject;
    if (paramObject != null) {
      LinkAddress linkAddress1 = this.ipAddress, linkAddress2 = ((StaticIpConfiguration)paramObject).ipAddress;
      if (Objects.equals(linkAddress1, linkAddress2)) {
        InetAddress inetAddress2 = this.gateway, inetAddress1 = ((StaticIpConfiguration)paramObject).gateway;
        if (Objects.equals(inetAddress2, inetAddress1)) {
          ArrayList<InetAddress> arrayList1 = this.dnsServers, arrayList2 = ((StaticIpConfiguration)paramObject).dnsServers;
          if (arrayList1.equals(arrayList2)) {
            String str = this.domains;
            paramObject = ((StaticIpConfiguration)paramObject).domains;
            if (Objects.equals(str, paramObject))
              return null; 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public static final Parcelable.Creator<StaticIpConfiguration> CREATOR = new Parcelable.Creator<StaticIpConfiguration>() {
      public StaticIpConfiguration createFromParcel(Parcel param1Parcel) {
        return StaticIpConfiguration.readFromParcel(param1Parcel);
      }
      
      public StaticIpConfiguration[] newArray(int param1Int) {
        return new StaticIpConfiguration[param1Int];
      }
    };
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.ipAddress, paramInt);
    InetAddressUtils.parcelInetAddress(paramParcel, this.gateway, paramInt);
    paramParcel.writeInt(this.dnsServers.size());
    for (InetAddress inetAddress : this.dnsServers)
      InetAddressUtils.parcelInetAddress(paramParcel, inetAddress, paramInt); 
    paramParcel.writeString(this.domains);
  }
  
  public static StaticIpConfiguration readFromParcel(Parcel paramParcel) {
    StaticIpConfiguration staticIpConfiguration = new StaticIpConfiguration();
    staticIpConfiguration.ipAddress = paramParcel.<LinkAddress>readParcelable(null);
    staticIpConfiguration.gateway = InetAddressUtils.unparcelInetAddress(paramParcel);
    staticIpConfiguration.dnsServers.clear();
    int i = paramParcel.readInt();
    for (byte b = 0; b < i; b++)
      staticIpConfiguration.dnsServers.add(InetAddressUtils.unparcelInetAddress(paramParcel)); 
    staticIpConfiguration.domains = paramParcel.readString();
    return staticIpConfiguration;
  }
  
  public StaticIpConfiguration() {}
}
