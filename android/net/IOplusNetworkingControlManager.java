package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.Map;

public interface IOplusNetworkingControlManager extends IInterface {
  void factoryReset() throws RemoteException;
  
  Map getPolicyList() throws RemoteException;
  
  int getUidPolicy(int paramInt) throws RemoteException;
  
  int[] getUidsWithPolicy(int paramInt) throws RemoteException;
  
  void setUidPolicy(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements IOplusNetworkingControlManager {
    public void setUidPolicy(int param1Int1, int param1Int2) throws RemoteException {}
    
    public int getUidPolicy(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int[] getUidsWithPolicy(int param1Int) throws RemoteException {
      return null;
    }
    
    public void factoryReset() throws RemoteException {}
    
    public Map getPolicyList() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusNetworkingControlManager {
    private static final String DESCRIPTOR = "android.net.IOplusNetworkingControlManager";
    
    static final int TRANSACTION_factoryReset = 4;
    
    static final int TRANSACTION_getPolicyList = 5;
    
    static final int TRANSACTION_getUidPolicy = 2;
    
    static final int TRANSACTION_getUidsWithPolicy = 3;
    
    static final int TRANSACTION_setUidPolicy = 1;
    
    public Stub() {
      attachInterface(this, "android.net.IOplusNetworkingControlManager");
    }
    
    public static IOplusNetworkingControlManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.IOplusNetworkingControlManager");
      if (iInterface != null && iInterface instanceof IOplusNetworkingControlManager)
        return (IOplusNetworkingControlManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "getPolicyList";
            } 
            return "factoryReset";
          } 
          return "getUidsWithPolicy";
        } 
        return "getUidPolicy";
      } 
      return "setUidPolicy";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      int[] arrayOfInt;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          Map map;
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.net.IOplusNetworkingControlManager");
                return true;
              } 
              param1Parcel1.enforceInterface("android.net.IOplusNetworkingControlManager");
              map = getPolicyList();
              param1Parcel2.writeNoException();
              param1Parcel2.writeMap(map);
              return true;
            } 
            map.enforceInterface("android.net.IOplusNetworkingControlManager");
            factoryReset();
            param1Parcel2.writeNoException();
            return true;
          } 
          map.enforceInterface("android.net.IOplusNetworkingControlManager");
          param1Int1 = map.readInt();
          arrayOfInt = getUidsWithPolicy(param1Int1);
          param1Parcel2.writeNoException();
          param1Parcel2.writeIntArray(arrayOfInt);
          return true;
        } 
        arrayOfInt.enforceInterface("android.net.IOplusNetworkingControlManager");
        param1Int1 = arrayOfInt.readInt();
        param1Int1 = getUidPolicy(param1Int1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(param1Int1);
        return true;
      } 
      arrayOfInt.enforceInterface("android.net.IOplusNetworkingControlManager");
      param1Int2 = arrayOfInt.readInt();
      param1Int1 = arrayOfInt.readInt();
      setUidPolicy(param1Int2, param1Int1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IOplusNetworkingControlManager {
      public static IOplusNetworkingControlManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.IOplusNetworkingControlManager";
      }
      
      public void setUidPolicy(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IOplusNetworkingControlManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusNetworkingControlManager.Stub.getDefaultImpl() != null) {
            IOplusNetworkingControlManager.Stub.getDefaultImpl().setUidPolicy(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUidPolicy(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IOplusNetworkingControlManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusNetworkingControlManager.Stub.getDefaultImpl() != null) {
            param2Int = IOplusNetworkingControlManager.Stub.getDefaultImpl().getUidPolicy(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getUidsWithPolicy(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IOplusNetworkingControlManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOplusNetworkingControlManager.Stub.getDefaultImpl() != null)
            return IOplusNetworkingControlManager.Stub.getDefaultImpl().getUidsWithPolicy(param2Int); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void factoryReset() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IOplusNetworkingControlManager");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOplusNetworkingControlManager.Stub.getDefaultImpl() != null) {
            IOplusNetworkingControlManager.Stub.getDefaultImpl().factoryReset();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Map getPolicyList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.IOplusNetworkingControlManager");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOplusNetworkingControlManager.Stub.getDefaultImpl() != null)
            return IOplusNetworkingControlManager.Stub.getDefaultImpl().getPolicyList(); 
          parcel2.readException();
          ClassLoader classLoader = getClass().getClassLoader();
          return parcel2.readHashMap(classLoader);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusNetworkingControlManager param1IOplusNetworkingControlManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusNetworkingControlManager != null) {
          Proxy.sDefaultImpl = param1IOplusNetworkingControlManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusNetworkingControlManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
