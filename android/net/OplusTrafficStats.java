package android.net;

import android.net.stats.OplusSocketInfoTotal;
import android.net.stats.StatsValueTotal;
import android.os.RemoteException;

public class OplusTrafficStats {
  private static final String OPLUS_NETWORK_STATS_SERVICE = "oplusnetworkstats";
  
  private static final int TYPE_TRANS_RX_BYTES = 6;
  
  private static final int TYPE_TRANS_TX_BYTES = 7;
  
  private static IOplusNetworkStatsService sOplusStatsService;
  
  private static INetworkStatsService sStatsService;
  
  private static INetworkStatsService getOrigStatsService() {
    // Byte code:
    //   0: ldc android/net/OplusTrafficStats
    //   2: monitorenter
    //   3: getstatic android/net/OplusTrafficStats.sStatsService : Landroid/net/INetworkStatsService;
    //   6: ifnonnull -> 22
    //   9: ldc 'netstats'
    //   11: invokestatic getService : (Ljava/lang/String;)Landroid/os/IBinder;
    //   14: astore_0
    //   15: aload_0
    //   16: invokestatic asInterface : (Landroid/os/IBinder;)Landroid/net/INetworkStatsService;
    //   19: putstatic android/net/OplusTrafficStats.sStatsService : Landroid/net/INetworkStatsService;
    //   22: getstatic android/net/OplusTrafficStats.sStatsService : Landroid/net/INetworkStatsService;
    //   25: astore_0
    //   26: ldc android/net/OplusTrafficStats
    //   28: monitorexit
    //   29: aload_0
    //   30: areturn
    //   31: astore_0
    //   32: ldc android/net/OplusTrafficStats
    //   34: monitorexit
    //   35: aload_0
    //   36: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #18	-> 3
    //   #19	-> 9
    //   #20	-> 9
    //   #19	-> 15
    //   #22	-> 22
    //   #17	-> 31
    // Exception table:
    //   from	to	target	type
    //   3	9	31	finally
    //   9	15	31	finally
    //   15	22	31	finally
    //   22	26	31	finally
  }
  
  private static IOplusNetworkStatsService getOplusStatsService() {
    // Byte code:
    //   0: ldc android/net/OplusTrafficStats
    //   2: monitorenter
    //   3: getstatic android/net/OplusTrafficStats.sOplusStatsService : Landroid/net/IOplusNetworkStatsService;
    //   6: ifnonnull -> 22
    //   9: ldc 'oplusnetworkstats'
    //   11: invokestatic getService : (Ljava/lang/String;)Landroid/os/IBinder;
    //   14: astore_0
    //   15: aload_0
    //   16: invokestatic asInterface : (Landroid/os/IBinder;)Landroid/net/IOplusNetworkStatsService;
    //   19: putstatic android/net/OplusTrafficStats.sOplusStatsService : Landroid/net/IOplusNetworkStatsService;
    //   22: getstatic android/net/OplusTrafficStats.sOplusStatsService : Landroid/net/IOplusNetworkStatsService;
    //   25: astore_0
    //   26: ldc android/net/OplusTrafficStats
    //   28: monitorexit
    //   29: aload_0
    //   30: areturn
    //   31: astore_0
    //   32: ldc android/net/OplusTrafficStats
    //   34: monitorexit
    //   35: aload_0
    //   36: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #26	-> 3
    //   #27	-> 9
    //   #28	-> 9
    //   #27	-> 15
    //   #30	-> 22
    //   #25	-> 31
    // Exception table:
    //   from	to	target	type
    //   3	9	31	finally
    //   9	15	31	finally
    //   15	22	31	finally
    //   22	26	31	finally
  }
  
  public static long getTransRxBytes(String paramString) {
    try {
      return getOrigStatsService().getIfaceStats(paramString, 6);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static long getTransTxBytes(String paramString) {
    try {
      return getOrigStatsService().getIfaceStats(paramString, 7);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static StatsValueTotal getUidStatsTotal() {
    try {
      return getOplusStatsService().getUidStatsTotal();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static StatsValueTotal getSocketStatsTotal() {
    try {
      return getOplusStatsService().getSocketStatsTotal();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static OplusSocketInfoTotal getSocketInfoTotal() {
    try {
      return getOplusStatsService().getSocketInfoTotal();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public static boolean getSocketCookieLimitStatus(long paramLong) {
    try {
      return getOplusStatsService().getSocketCookieLimitStatus(paramLong);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public static boolean setSocketCookieLimit(long paramLong) {
    try {
      return getOplusStatsService().setSocketCookieLimit(paramLong);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public static boolean clearSocketCookieLimit(long paramLong) {
    try {
      return getOplusStatsService().clearSocketCookieLimit(paramLong);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public static long[] getAllSocketCookieLimit() {
    try {
      return getOplusStatsService().getAllSocketCookieLimit();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public static boolean getSocketUidLimitStatus(int paramInt) {
    try {
      return getOplusStatsService().getSocketUidLimitStatus(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public static boolean setSocketUidLimit(int paramInt) {
    try {
      return getOplusStatsService().setSocketUidLimit(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public static boolean clearSocketUidLimit(int paramInt) {
    try {
      return getOplusStatsService().clearSocketUidLimit(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public static int[] getAllSocetUidLimit() {
    try {
      return getOplusStatsService().getAllSocketUidLimit();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
}
