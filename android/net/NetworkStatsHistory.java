package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.MathUtils;
import android.util.proto.ProtoOutputStream;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.IndentingPrintWriter;
import java.io.CharArrayWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ProtocolException;
import java.util.Arrays;
import java.util.Random;
import libcore.util.EmptyArray;

public class NetworkStatsHistory implements Parcelable {
  class Entry {
    public static final long UNKNOWN = -1L;
    
    public long activeTime;
    
    public long bucketDuration;
    
    public long bucketStart;
    
    public long operations;
    
    public long rxBytes;
    
    public long rxPackets;
    
    public long txBytes;
    
    public long txPackets;
  }
  
  public NetworkStatsHistory(long paramLong) {
    this(paramLong, 10, -1);
  }
  
  public NetworkStatsHistory(long paramLong, int paramInt) {
    this(paramLong, paramInt, -1);
  }
  
  public NetworkStatsHistory(long paramLong, int paramInt1, int paramInt2) {
    this.bucketDuration = paramLong;
    this.bucketStart = new long[paramInt1];
    if ((paramInt2 & 0x1) != 0)
      this.activeTime = new long[paramInt1]; 
    if ((paramInt2 & 0x2) != 0)
      this.rxBytes = new long[paramInt1]; 
    if ((paramInt2 & 0x4) != 0)
      this.rxPackets = new long[paramInt1]; 
    if ((paramInt2 & 0x8) != 0)
      this.txBytes = new long[paramInt1]; 
    if ((paramInt2 & 0x10) != 0)
      this.txPackets = new long[paramInt1]; 
    if ((paramInt2 & 0x20) != 0)
      this.operations = new long[paramInt1]; 
    this.bucketCount = 0;
    this.totalBytes = 0L;
  }
  
  public NetworkStatsHistory(NetworkStatsHistory paramNetworkStatsHistory, long paramLong) {
    this(paramLong, paramNetworkStatsHistory.estimateResizeBuckets(paramLong));
    recordEntireHistory(paramNetworkStatsHistory);
  }
  
  public NetworkStatsHistory(Parcel paramParcel) {
    this.bucketDuration = paramParcel.readLong();
    this.bucketStart = ParcelUtils.readLongArray(paramParcel);
    this.activeTime = ParcelUtils.readLongArray(paramParcel);
    this.rxBytes = ParcelUtils.readLongArray(paramParcel);
    this.rxPackets = ParcelUtils.readLongArray(paramParcel);
    this.txBytes = ParcelUtils.readLongArray(paramParcel);
    this.txPackets = ParcelUtils.readLongArray(paramParcel);
    this.operations = ParcelUtils.readLongArray(paramParcel);
    this.bucketCount = this.bucketStart.length;
    this.totalBytes = paramParcel.readLong();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.bucketDuration);
    ParcelUtils.writeLongArray(paramParcel, this.bucketStart, this.bucketCount);
    ParcelUtils.writeLongArray(paramParcel, this.activeTime, this.bucketCount);
    ParcelUtils.writeLongArray(paramParcel, this.rxBytes, this.bucketCount);
    ParcelUtils.writeLongArray(paramParcel, this.rxPackets, this.bucketCount);
    ParcelUtils.writeLongArray(paramParcel, this.txBytes, this.bucketCount);
    ParcelUtils.writeLongArray(paramParcel, this.txPackets, this.bucketCount);
    ParcelUtils.writeLongArray(paramParcel, this.operations, this.bucketCount);
    paramParcel.writeLong(this.totalBytes);
  }
  
  public NetworkStatsHistory(DataInputStream paramDataInputStream) throws IOException {
    StringBuilder stringBuilder;
    int i = paramDataInputStream.readInt();
    if (i != 1) {
      if (i == 2 || i == 3) {
        this.bucketDuration = paramDataInputStream.readLong();
        long[] arrayOfLong = DataStreamUtils.readVarLongArray(paramDataInputStream);
        if (i >= 3) {
          arrayOfLong = DataStreamUtils.readVarLongArray(paramDataInputStream);
        } else {
          arrayOfLong = new long[arrayOfLong.length];
        } 
        this.activeTime = arrayOfLong;
        this.rxBytes = DataStreamUtils.readVarLongArray(paramDataInputStream);
        this.rxPackets = DataStreamUtils.readVarLongArray(paramDataInputStream);
        this.txBytes = DataStreamUtils.readVarLongArray(paramDataInputStream);
        this.txPackets = DataStreamUtils.readVarLongArray(paramDataInputStream);
        this.operations = DataStreamUtils.readVarLongArray(paramDataInputStream);
        this.bucketCount = this.bucketStart.length;
        this.totalBytes = ArrayUtils.total(this.rxBytes) + ArrayUtils.total(this.txBytes);
      } else {
        stringBuilder = new StringBuilder();
        stringBuilder.append("unexpected version: ");
        stringBuilder.append(i);
        throw new ProtocolException(stringBuilder.toString());
      } 
    } else {
      this.bucketDuration = stringBuilder.readLong();
      this.bucketStart = DataStreamUtils.readFullLongArray((DataInputStream)stringBuilder);
      this.rxBytes = DataStreamUtils.readFullLongArray((DataInputStream)stringBuilder);
      this.rxPackets = new long[this.bucketStart.length];
      this.txBytes = DataStreamUtils.readFullLongArray((DataInputStream)stringBuilder);
      long[] arrayOfLong = this.bucketStart;
      this.txPackets = new long[arrayOfLong.length];
      this.operations = new long[arrayOfLong.length];
      this.bucketCount = arrayOfLong.length;
      this.totalBytes = ArrayUtils.total(this.rxBytes) + ArrayUtils.total(this.txBytes);
    } 
    i = this.bucketStart.length;
    int j = this.bucketCount;
    if (i == j && this.rxBytes.length == j && this.rxPackets.length == j && this.txBytes.length == j && this.txPackets.length == j && this.operations.length == j)
      return; 
    throw new ProtocolException("Mismatched history lengths");
  }
  
  public void writeToStream(DataOutputStream paramDataOutputStream) throws IOException {
    paramDataOutputStream.writeInt(3);
    paramDataOutputStream.writeLong(this.bucketDuration);
    DataStreamUtils.writeVarLongArray(paramDataOutputStream, this.bucketStart, this.bucketCount);
    DataStreamUtils.writeVarLongArray(paramDataOutputStream, this.activeTime, this.bucketCount);
    DataStreamUtils.writeVarLongArray(paramDataOutputStream, this.rxBytes, this.bucketCount);
    DataStreamUtils.writeVarLongArray(paramDataOutputStream, this.rxPackets, this.bucketCount);
    DataStreamUtils.writeVarLongArray(paramDataOutputStream, this.txBytes, this.bucketCount);
    DataStreamUtils.writeVarLongArray(paramDataOutputStream, this.txPackets, this.bucketCount);
    DataStreamUtils.writeVarLongArray(paramDataOutputStream, this.operations, this.bucketCount);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public int size() {
    return this.bucketCount;
  }
  
  public long getBucketDuration() {
    return this.bucketDuration;
  }
  
  public long getStart() {
    if (this.bucketCount > 0)
      return this.bucketStart[0]; 
    return Long.MAX_VALUE;
  }
  
  public long getEnd() {
    int i = this.bucketCount;
    if (i > 0)
      return this.bucketStart[i - 1] + this.bucketDuration; 
    return Long.MIN_VALUE;
  }
  
  public long getTotalBytes() {
    return this.totalBytes;
  }
  
  public static long multiplySafe(long paramLong1, long paramLong2, long paramLong3) {
    if (paramLong3 == 0L)
      paramLong3 = 1L; 
    long l1 = paramLong1 * paramLong2;
    long l2 = Math.abs(paramLong1);
    long l3 = Math.abs(paramLong2);
    if ((l2 | l3) >>> 31L != 0L)
      if ((paramLong2 != 0L && l1 / paramLong2 != paramLong1) || (paramLong1 == Long.MIN_VALUE && paramLong2 == -1L))
        return (long)(paramLong2 / paramLong3 * paramLong1);  
    return l1 / paramLong3;
  }
  
  public int getIndexBefore(long paramLong) {
    int i = Arrays.binarySearch(this.bucketStart, 0, this.bucketCount, paramLong);
    if (i < 0) {
      i = (i ^ 0xFFFFFFFF) - 1;
    } else {
      i--;
    } 
    return MathUtils.constrain(i, 0, this.bucketCount - 1);
  }
  
  public int getIndexAfter(long paramLong) {
    int i = Arrays.binarySearch(this.bucketStart, 0, this.bucketCount, paramLong);
    if (i < 0) {
      i ^= 0xFFFFFFFF;
    } else {
      i++;
    } 
    return MathUtils.constrain(i, 0, this.bucketCount - 1);
  }
  
  public Entry getValues(int paramInt, Entry paramEntry) {
    if (paramEntry == null)
      paramEntry = new Entry(); 
    paramEntry.bucketStart = this.bucketStart[paramInt];
    paramEntry.bucketDuration = this.bucketDuration;
    paramEntry.activeTime = getLong(this.activeTime, paramInt, -1L);
    paramEntry.rxBytes = getLong(this.rxBytes, paramInt, -1L);
    paramEntry.rxPackets = getLong(this.rxPackets, paramInt, -1L);
    paramEntry.txBytes = getLong(this.txBytes, paramInt, -1L);
    paramEntry.txPackets = getLong(this.txPackets, paramInt, -1L);
    paramEntry.operations = getLong(this.operations, paramInt, -1L);
    return paramEntry;
  }
  
  public void setValues(int paramInt, Entry paramEntry) {
    long[] arrayOfLong2 = this.rxBytes;
    if (arrayOfLong2 != null)
      this.totalBytes -= arrayOfLong2[paramInt]; 
    arrayOfLong2 = this.txBytes;
    if (arrayOfLong2 != null)
      this.totalBytes -= arrayOfLong2[paramInt]; 
    this.bucketStart[paramInt] = paramEntry.bucketStart;
    setLong(this.activeTime, paramInt, paramEntry.activeTime);
    setLong(this.rxBytes, paramInt, paramEntry.rxBytes);
    setLong(this.rxPackets, paramInt, paramEntry.rxPackets);
    setLong(this.txBytes, paramInt, paramEntry.txBytes);
    setLong(this.txPackets, paramInt, paramEntry.txPackets);
    setLong(this.operations, paramInt, paramEntry.operations);
    long[] arrayOfLong1 = this.rxBytes;
    if (arrayOfLong1 != null)
      this.totalBytes += arrayOfLong1[paramInt]; 
    arrayOfLong1 = this.txBytes;
    if (arrayOfLong1 != null)
      this.totalBytes += arrayOfLong1[paramInt]; 
  }
  
  @Deprecated
  public void recordData(long paramLong1, long paramLong2, long paramLong3, long paramLong4) {
    recordData(paramLong1, paramLong2, new NetworkStats.Entry(NetworkStats.IFACE_ALL, -1, 0, 0, paramLong3, 0L, paramLong4, 0L, 0L));
  }
  
  public void recordData(long paramLong1, long paramLong2, NetworkStats.Entry paramEntry) {
    long l1 = paramEntry.rxBytes;
    long l2 = paramEntry.rxPackets;
    long l3 = paramEntry.txBytes;
    long l4 = paramEntry.txPackets;
    long l5 = paramEntry.operations;
    if (!paramEntry.isNegative()) {
      if (paramEntry.isEmpty())
        return; 
      ensureBuckets(paramLong1, paramLong2);
      int i = getIndexAfter(paramLong2);
      for (long l = paramLong2 - paramLong1; i >= 0; i--) {
        long l6 = this.bucketStart[i];
        long l7 = this.bucketDuration + l6;
        if (l7 < paramLong1)
          break; 
        if (l6 <= paramLong2) {
          l7 = Math.min(l7, paramLong2) - Math.max(l6, paramLong1);
          if (l7 > 0L) {
            long l8 = multiplySafe(l1, l7, l);
            l6 = multiplySafe(l2, l7, l);
            long l9 = multiplySafe(l3, l7, l);
            long l10 = multiplySafe(l4, l7, l);
            long l11 = multiplySafe(l5, l7, l);
            addLong(this.activeTime, i, l7);
            addLong(this.rxBytes, i, l8);
            l1 -= l8;
            addLong(this.rxPackets, i, l6);
            addLong(this.txBytes, i, l9);
            l3 -= l9;
            addLong(this.txPackets, i, l10);
            l4 -= l10;
            addLong(this.operations, i, l11);
            l5 -= l11;
            l -= l7;
            l2 -= l6;
          } 
        } 
      } 
      this.totalBytes += paramEntry.rxBytes + paramEntry.txBytes;
      return;
    } 
    throw new IllegalArgumentException("tried recording negative data");
  }
  
  public void recordEntireHistory(NetworkStatsHistory paramNetworkStatsHistory) {
    recordHistory(paramNetworkStatsHistory, Long.MIN_VALUE, Long.MAX_VALUE);
  }
  
  public void recordHistory(NetworkStatsHistory paramNetworkStatsHistory, long paramLong1, long paramLong2) {
    NetworkStats.Entry entry = new NetworkStats.Entry(NetworkStats.IFACE_ALL, -1, 0, 0, 0L, 0L, 0L, 0L, 0L);
    for (byte b = 0; b < paramNetworkStatsHistory.bucketCount; b++) {
      long l1 = paramNetworkStatsHistory.bucketStart[b];
      long l2 = paramNetworkStatsHistory.bucketDuration + l1;
      if (l1 >= paramLong1 && l2 <= paramLong2) {
        entry.rxBytes = getLong(paramNetworkStatsHistory.rxBytes, b, 0L);
        entry.rxPackets = getLong(paramNetworkStatsHistory.rxPackets, b, 0L);
        entry.txBytes = getLong(paramNetworkStatsHistory.txBytes, b, 0L);
        entry.txPackets = getLong(paramNetworkStatsHistory.txPackets, b, 0L);
        entry.operations = getLong(paramNetworkStatsHistory.operations, b, 0L);
        recordData(l1, l2, entry);
      } 
    } 
  }
  
  private void ensureBuckets(long paramLong1, long paramLong2) {
    long l = this.bucketDuration;
    for (paramLong1 -= paramLong1 % l; paramLong1 < paramLong2 + (l - paramLong2 % l) % l; paramLong1 += this.bucketDuration) {
      int i = Arrays.binarySearch(this.bucketStart, 0, this.bucketCount, paramLong1);
      if (i < 0)
        insertBucket(i ^ 0xFFFFFFFF, paramLong1); 
    } 
  }
  
  private void insertBucket(int paramInt, long paramLong) {
    int i = this.bucketCount;
    long[] arrayOfLong = this.bucketStart;
    if (i >= arrayOfLong.length) {
      i = Math.max(arrayOfLong.length, 10) * 3 / 2;
      this.bucketStart = Arrays.copyOf(this.bucketStart, i);
      arrayOfLong = this.activeTime;
      if (arrayOfLong != null)
        this.activeTime = Arrays.copyOf(arrayOfLong, i); 
      arrayOfLong = this.rxBytes;
      if (arrayOfLong != null)
        this.rxBytes = Arrays.copyOf(arrayOfLong, i); 
      arrayOfLong = this.rxPackets;
      if (arrayOfLong != null)
        this.rxPackets = Arrays.copyOf(arrayOfLong, i); 
      arrayOfLong = this.txBytes;
      if (arrayOfLong != null)
        this.txBytes = Arrays.copyOf(arrayOfLong, i); 
      arrayOfLong = this.txPackets;
      if (arrayOfLong != null)
        this.txPackets = Arrays.copyOf(arrayOfLong, i); 
      arrayOfLong = this.operations;
      if (arrayOfLong != null)
        this.operations = Arrays.copyOf(arrayOfLong, i); 
    } 
    int j = this.bucketCount;
    if (paramInt < j) {
      i = paramInt + 1;
      j -= paramInt;
      arrayOfLong = this.bucketStart;
      System.arraycopy(arrayOfLong, paramInt, arrayOfLong, i, j);
      arrayOfLong = this.activeTime;
      if (arrayOfLong != null)
        System.arraycopy(arrayOfLong, paramInt, arrayOfLong, i, j); 
      arrayOfLong = this.rxBytes;
      if (arrayOfLong != null)
        System.arraycopy(arrayOfLong, paramInt, arrayOfLong, i, j); 
      arrayOfLong = this.rxPackets;
      if (arrayOfLong != null)
        System.arraycopy(arrayOfLong, paramInt, arrayOfLong, i, j); 
      arrayOfLong = this.txBytes;
      if (arrayOfLong != null)
        System.arraycopy(arrayOfLong, paramInt, arrayOfLong, i, j); 
      arrayOfLong = this.txPackets;
      if (arrayOfLong != null)
        System.arraycopy(arrayOfLong, paramInt, arrayOfLong, i, j); 
      arrayOfLong = this.operations;
      if (arrayOfLong != null)
        System.arraycopy(arrayOfLong, paramInt, arrayOfLong, i, j); 
    } 
    this.bucketStart[paramInt] = paramLong;
    setLong(this.activeTime, paramInt, 0L);
    setLong(this.rxBytes, paramInt, 0L);
    setLong(this.rxPackets, paramInt, 0L);
    setLong(this.txBytes, paramInt, 0L);
    setLong(this.txPackets, paramInt, 0L);
    setLong(this.operations, paramInt, 0L);
    this.bucketCount++;
  }
  
  public void clear() {
    this.bucketStart = EmptyArray.LONG;
    if (this.activeTime != null)
      this.activeTime = EmptyArray.LONG; 
    if (this.rxBytes != null)
      this.rxBytes = EmptyArray.LONG; 
    if (this.rxPackets != null)
      this.rxPackets = EmptyArray.LONG; 
    if (this.txBytes != null)
      this.txBytes = EmptyArray.LONG; 
    if (this.txPackets != null)
      this.txPackets = EmptyArray.LONG; 
    if (this.operations != null)
      this.operations = EmptyArray.LONG; 
    this.bucketCount = 0;
    this.totalBytes = 0L;
  }
  
  @Deprecated
  public void removeBucketsBefore(long paramLong) {
    byte b;
    for (b = 0; b < this.bucketCount; b++) {
      long l1 = this.bucketStart[b];
      long l2 = this.bucketDuration;
      if (l2 + l1 > paramLong)
        break; 
    } 
    if (b > 0) {
      long[] arrayOfLong = this.bucketStart;
      int i = arrayOfLong.length;
      this.bucketStart = Arrays.copyOfRange(arrayOfLong, b, i);
      arrayOfLong = this.activeTime;
      if (arrayOfLong != null)
        this.activeTime = Arrays.copyOfRange(arrayOfLong, b, i); 
      arrayOfLong = this.rxBytes;
      if (arrayOfLong != null)
        this.rxBytes = Arrays.copyOfRange(arrayOfLong, b, i); 
      arrayOfLong = this.rxPackets;
      if (arrayOfLong != null)
        this.rxPackets = Arrays.copyOfRange(arrayOfLong, b, i); 
      arrayOfLong = this.txBytes;
      if (arrayOfLong != null)
        this.txBytes = Arrays.copyOfRange(arrayOfLong, b, i); 
      arrayOfLong = this.txPackets;
      if (arrayOfLong != null)
        this.txPackets = Arrays.copyOfRange(arrayOfLong, b, i); 
      arrayOfLong = this.operations;
      if (arrayOfLong != null)
        this.operations = Arrays.copyOfRange(arrayOfLong, b, i); 
      this.bucketCount -= b;
    } 
  }
  
  public Entry getValues(long paramLong1, long paramLong2, Entry paramEntry) {
    return getValues(paramLong1, paramLong2, Long.MAX_VALUE, paramEntry);
  }
  
  public Entry getValues(long paramLong1, long paramLong2, long paramLong3, Entry paramEntry) {
    if (paramEntry == null)
      paramEntry = new Entry(); 
    paramEntry.bucketDuration = paramLong2 - paramLong1;
    paramEntry.bucketStart = paramLong1;
    long arrayOfLong[] = this.activeTime, l1 = -1L;
    if (arrayOfLong != null) {
      l2 = 0L;
    } else {
      l2 = -1L;
    } 
    paramEntry.activeTime = l2;
    if (this.rxBytes != null) {
      l2 = 0L;
    } else {
      l2 = -1L;
    } 
    paramEntry.rxBytes = l2;
    if (this.rxPackets != null) {
      l2 = 0L;
    } else {
      l2 = -1L;
    } 
    paramEntry.rxPackets = l2;
    if (this.txBytes != null) {
      l2 = 0L;
    } else {
      l2 = -1L;
    } 
    paramEntry.txBytes = l2;
    if (this.txPackets != null) {
      l2 = 0L;
    } else {
      l2 = -1L;
    } 
    paramEntry.txPackets = l2;
    long l2 = l1;
    if (this.operations != null)
      l2 = 0L; 
    paramEntry.operations = l2;
    int i = getIndexAfter(paramLong2);
    for (; i >= 0; i--) {
      l1 = this.bucketStart[i];
      long l = this.bucketDuration + l1;
      if (l <= paramLong1)
        break; 
      if (l1 < paramLong2) {
        l2 = l;
        if (l > paramLong3)
          l2 = paramLong3; 
        l = l2 - l1;
        if (l > 0L) {
          if (l2 >= paramLong2)
            l2 = paramLong2; 
          if (l1 <= paramLong1)
            l1 = paramLong1; 
          l2 -= l1;
          if (l2 > 0L) {
            if (this.activeTime != null)
              paramEntry.activeTime += multiplySafe(this.activeTime[i], l2, l); 
            if (this.rxBytes != null)
              paramEntry.rxBytes += multiplySafe(this.rxBytes[i], l2, l); 
            if (this.rxPackets != null)
              paramEntry.rxPackets += multiplySafe(this.rxPackets[i], l2, l); 
            if (this.txBytes != null)
              paramEntry.txBytes += multiplySafe(this.txBytes[i], l2, l); 
            if (this.txPackets != null)
              paramEntry.txPackets += multiplySafe(this.txPackets[i], l2, l); 
            if (this.operations != null)
              paramEntry.operations += multiplySafe(this.operations[i], l2, l); 
          } 
        } 
      } 
    } 
    return paramEntry;
  }
  
  @Deprecated
  public void generateRandom(long paramLong1, long paramLong2, long paramLong3) {
    Random random = new Random();
    float f = random.nextFloat();
    long l1 = (long)((float)paramLong3 * f);
    paramLong3 = (long)((float)paramLong3 * (1.0F - f));
    long l2 = l1 / 1024L;
    long l3 = paramLong3 / 1024L;
    long l4 = l1 / 2048L;
    generateRandom(paramLong1, paramLong2, l1, l2, paramLong3, l3, l4, random);
  }
  
  @Deprecated
  public void generateRandom(long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5, long paramLong6, long paramLong7, Random paramRandom) {
    ensureBuckets(paramLong1, paramLong2);
    NetworkStats.Entry entry = new NetworkStats.Entry(NetworkStats.IFACE_ALL, -1, 0, 0, 0L, 0L, 0L, 0L, 0L);
    long l1 = paramLong3, l2 = paramLong4;
    paramLong4 = paramLong6;
    paramLong3 = paramLong7;
    paramLong6 = l2;
    while (true) {
      if (l1 > 1024L || paramLong6 > 128L || paramLong5 > 1024L || paramLong4 > 128L || paramLong3 > 32L) {
        paramLong7 = randomLong(paramRandom, paramLong1, paramLong2);
        l2 = randomLong(paramRandom, 0L, (paramLong2 - paramLong7) / 2L);
        entry.rxBytes = randomLong(paramRandom, 0L, l1);
        entry.rxPackets = randomLong(paramRandom, 0L, paramLong6);
        entry.txBytes = randomLong(paramRandom, 0L, paramLong5);
        entry.txPackets = randomLong(paramRandom, 0L, paramLong4);
        entry.operations = randomLong(paramRandom, 0L, paramLong3);
        l1 -= entry.rxBytes;
        paramLong6 -= entry.rxPackets;
        paramLong5 -= entry.txBytes;
        paramLong4 -= entry.txPackets;
        paramLong3 -= entry.operations;
        recordData(paramLong7, paramLong7 + l2, entry);
        continue;
      } 
      return;
    } 
  }
  
  public static long randomLong(Random paramRandom, long paramLong1, long paramLong2) {
    return (long)((float)paramLong1 + paramRandom.nextFloat() * (float)(paramLong2 - paramLong1));
  }
  
  public boolean intersects(long paramLong1, long paramLong2) {
    long l1 = getStart();
    long l2 = getEnd();
    if (paramLong1 >= l1 && paramLong1 <= l2)
      return true; 
    if (paramLong2 >= l1 && paramLong2 <= l2)
      return true; 
    if (l1 >= paramLong1 && l1 <= paramLong2)
      return true; 
    if (l2 >= paramLong1 && l2 <= paramLong2)
      return true; 
    return false;
  }
  
  public void dump(IndentingPrintWriter paramIndentingPrintWriter, boolean paramBoolean) {
    paramIndentingPrintWriter.print("NetworkStatsHistory: bucketDuration=");
    paramIndentingPrintWriter.println(this.bucketDuration / 1000L);
    paramIndentingPrintWriter.increaseIndent();
    int i = 0;
    if (!paramBoolean)
      i = Math.max(0, this.bucketCount - 32); 
    if (i > 0) {
      paramIndentingPrintWriter.print("(omitting ");
      paramIndentingPrintWriter.print(i);
      paramIndentingPrintWriter.println(" buckets)");
    } 
    for (; i < this.bucketCount; i++) {
      paramIndentingPrintWriter.print("st=");
      paramIndentingPrintWriter.print(this.bucketStart[i] / 1000L);
      if (this.rxBytes != null) {
        paramIndentingPrintWriter.print(" rb=");
        paramIndentingPrintWriter.print(this.rxBytes[i]);
      } 
      if (this.rxPackets != null) {
        paramIndentingPrintWriter.print(" rp=");
        paramIndentingPrintWriter.print(this.rxPackets[i]);
      } 
      if (this.txBytes != null) {
        paramIndentingPrintWriter.print(" tb=");
        paramIndentingPrintWriter.print(this.txBytes[i]);
      } 
      if (this.txPackets != null) {
        paramIndentingPrintWriter.print(" tp=");
        paramIndentingPrintWriter.print(this.txPackets[i]);
      } 
      if (this.operations != null) {
        paramIndentingPrintWriter.print(" op=");
        paramIndentingPrintWriter.print(this.operations[i]);
      } 
      paramIndentingPrintWriter.println();
    } 
    paramIndentingPrintWriter.decreaseIndent();
  }
  
  public void dumpCheckin(PrintWriter paramPrintWriter) {
    paramPrintWriter.print("d,");
    paramPrintWriter.print(this.bucketDuration / 1000L);
    paramPrintWriter.println();
    for (byte b = 0; b < this.bucketCount; b++) {
      paramPrintWriter.print("b,");
      paramPrintWriter.print(this.bucketStart[b] / 1000L);
      paramPrintWriter.print(',');
      long[] arrayOfLong = this.rxBytes;
      if (arrayOfLong != null) {
        paramPrintWriter.print(arrayOfLong[b]);
      } else {
        paramPrintWriter.print("*");
      } 
      paramPrintWriter.print(',');
      arrayOfLong = this.rxPackets;
      if (arrayOfLong != null) {
        paramPrintWriter.print(arrayOfLong[b]);
      } else {
        paramPrintWriter.print("*");
      } 
      paramPrintWriter.print(',');
      arrayOfLong = this.txBytes;
      if (arrayOfLong != null) {
        paramPrintWriter.print(arrayOfLong[b]);
      } else {
        paramPrintWriter.print("*");
      } 
      paramPrintWriter.print(',');
      arrayOfLong = this.txPackets;
      if (arrayOfLong != null) {
        paramPrintWriter.print(arrayOfLong[b]);
      } else {
        paramPrintWriter.print("*");
      } 
      paramPrintWriter.print(',');
      arrayOfLong = this.operations;
      if (arrayOfLong != null) {
        paramPrintWriter.print(arrayOfLong[b]);
      } else {
        paramPrintWriter.print("*");
      } 
      paramPrintWriter.println();
    } 
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    paramLong = paramProtoOutputStream.start(paramLong);
    paramProtoOutputStream.write(1112396529665L, this.bucketDuration);
    for (byte b = 0; b < this.bucketCount; b++) {
      long l = paramProtoOutputStream.start(2246267895810L);
      paramProtoOutputStream.write(1112396529665L, this.bucketStart[b]);
      dumpDebug(paramProtoOutputStream, 1112396529666L, this.rxBytes, b);
      dumpDebug(paramProtoOutputStream, 1112396529667L, this.rxPackets, b);
      dumpDebug(paramProtoOutputStream, 1112396529668L, this.txBytes, b);
      dumpDebug(paramProtoOutputStream, 1112396529669L, this.txPackets, b);
      dumpDebug(paramProtoOutputStream, 1112396529670L, this.operations, b);
      paramProtoOutputStream.end(l);
    } 
    paramProtoOutputStream.end(paramLong);
  }
  
  private static void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong, long[] paramArrayOflong, int paramInt) {
    if (paramArrayOflong != null)
      paramProtoOutputStream.write(paramLong, paramArrayOflong[paramInt]); 
  }
  
  public String toString() {
    CharArrayWriter charArrayWriter = new CharArrayWriter();
    dump(new IndentingPrintWriter(charArrayWriter, "  "), false);
    return charArrayWriter.toString();
  }
  
  public static final Parcelable.Creator<NetworkStatsHistory> CREATOR = new Parcelable.Creator<NetworkStatsHistory>() {
      public NetworkStatsHistory createFromParcel(Parcel param1Parcel) {
        return new NetworkStatsHistory(param1Parcel);
      }
      
      public NetworkStatsHistory[] newArray(int param1Int) {
        return new NetworkStatsHistory[param1Int];
      }
    };
  
  public static final int FIELD_ACTIVE_TIME = 1;
  
  public static final int FIELD_ALL = -1;
  
  public static final int FIELD_OPERATIONS = 32;
  
  public static final int FIELD_RX_BYTES = 2;
  
  public static final int FIELD_RX_PACKETS = 4;
  
  public static final int FIELD_TX_BYTES = 8;
  
  public static final int FIELD_TX_PACKETS = 16;
  
  private static final int VERSION_ADD_ACTIVE = 3;
  
  private static final int VERSION_ADD_PACKETS = 2;
  
  private static final int VERSION_INIT = 1;
  
  private long[] activeTime;
  
  private int bucketCount;
  
  private long bucketDuration;
  
  private long[] bucketStart;
  
  private long[] operations;
  
  private long[] rxBytes;
  
  private long[] rxPackets;
  
  private long totalBytes;
  
  private long[] txBytes;
  
  private long[] txPackets;
  
  private static long getLong(long[] paramArrayOflong, int paramInt, long paramLong) {
    if (paramArrayOflong != null)
      paramLong = paramArrayOflong[paramInt]; 
    return paramLong;
  }
  
  private static void setLong(long[] paramArrayOflong, int paramInt, long paramLong) {
    if (paramArrayOflong != null)
      paramArrayOflong[paramInt] = paramLong; 
  }
  
  private static void addLong(long[] paramArrayOflong, int paramInt, long paramLong) {
    if (paramArrayOflong != null)
      paramArrayOflong[paramInt] = paramArrayOflong[paramInt] + paramLong; 
  }
  
  public int estimateResizeBuckets(long paramLong) {
    return (int)(size() * getBucketDuration() / paramLong);
  }
  
  class DataStreamUtils {
    @Deprecated
    public static long[] readFullLongArray(DataInputStream param1DataInputStream) throws IOException {
      int i = param1DataInputStream.readInt();
      if (i >= 0) {
        long[] arrayOfLong = new long[i];
        for (i = 0; i < arrayOfLong.length; i++)
          arrayOfLong[i] = param1DataInputStream.readLong(); 
        return arrayOfLong;
      } 
      throw new ProtocolException("negative array size");
    }
    
    public static long readVarLong(DataInputStream param1DataInputStream) throws IOException {
      byte b = 0;
      long l = 0L;
      while (b < 64) {
        byte b1 = param1DataInputStream.readByte();
        l |= (b1 & Byte.MAX_VALUE) << b;
        if ((b1 & 0x80) == 0)
          return l; 
        b += 7;
      } 
      throw new ProtocolException("malformed long");
    }
    
    public static void writeVarLong(DataOutputStream param1DataOutputStream, long param1Long) throws IOException {
      while (true) {
        if ((0xFFFFFFFFFFFFFF80L & param1Long) == 0L) {
          param1DataOutputStream.writeByte((int)param1Long);
          return;
        } 
        param1DataOutputStream.writeByte((int)param1Long & 0x7F | 0x80);
        param1Long >>>= 7L;
      } 
    }
    
    public static long[] readVarLongArray(DataInputStream param1DataInputStream) throws IOException {
      int i = param1DataInputStream.readInt();
      if (i == -1)
        return null; 
      if (i >= 0) {
        long[] arrayOfLong = new long[i];
        for (i = 0; i < arrayOfLong.length; i++)
          arrayOfLong[i] = readVarLong(param1DataInputStream); 
        return arrayOfLong;
      } 
      throw new ProtocolException("negative array size");
    }
    
    public static void writeVarLongArray(DataOutputStream param1DataOutputStream, long[] param1ArrayOflong, int param1Int) throws IOException {
      if (param1ArrayOflong == null) {
        param1DataOutputStream.writeInt(-1);
        return;
      } 
      if (param1Int <= param1ArrayOflong.length) {
        param1DataOutputStream.writeInt(param1Int);
        for (byte b = 0; b < param1Int; b++)
          writeVarLong(param1DataOutputStream, param1ArrayOflong[b]); 
        return;
      } 
      throw new IllegalArgumentException("size larger than length");
    }
  }
  
  class ParcelUtils {
    public static long[] readLongArray(Parcel param1Parcel) {
      int i = param1Parcel.readInt();
      if (i == -1)
        return null; 
      long[] arrayOfLong = new long[i];
      for (i = 0; i < arrayOfLong.length; i++)
        arrayOfLong[i] = param1Parcel.readLong(); 
      return arrayOfLong;
    }
    
    public static void writeLongArray(Parcel param1Parcel, long[] param1ArrayOflong, int param1Int) {
      if (param1ArrayOflong == null) {
        param1Parcel.writeInt(-1);
        return;
      } 
      if (param1Int <= param1ArrayOflong.length) {
        param1Parcel.writeInt(param1Int);
        for (byte b = 0; b < param1Int; b++)
          param1Parcel.writeLong(param1ArrayOflong[b]); 
        return;
      } 
      throw new IllegalArgumentException("size larger than length");
    }
  }
}
