package android.net;

import android.os.Parcel;
import android.os.Parcelable;

public final class IpSecTunnelInterfaceResponse implements Parcelable {
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.status);
    paramParcel.writeInt(this.resourceId);
    paramParcel.writeString(this.interfaceName);
  }
  
  public IpSecTunnelInterfaceResponse(int paramInt) {
    if (paramInt != 0) {
      this.status = paramInt;
      this.resourceId = -1;
      this.interfaceName = "";
      return;
    } 
    throw new IllegalArgumentException("Valid status implies other args must be provided");
  }
  
  public IpSecTunnelInterfaceResponse(int paramInt1, int paramInt2, String paramString) {
    this.status = paramInt1;
    this.resourceId = paramInt2;
    this.interfaceName = paramString;
  }
  
  private IpSecTunnelInterfaceResponse(Parcel paramParcel) {
    this.status = paramParcel.readInt();
    this.resourceId = paramParcel.readInt();
    this.interfaceName = paramParcel.readString();
  }
  
  public static final Parcelable.Creator<IpSecTunnelInterfaceResponse> CREATOR = new Parcelable.Creator<IpSecTunnelInterfaceResponse>() {
      public IpSecTunnelInterfaceResponse createFromParcel(Parcel param1Parcel) {
        return new IpSecTunnelInterfaceResponse(param1Parcel);
      }
      
      public IpSecTunnelInterfaceResponse[] newArray(int param1Int) {
        return new IpSecTunnelInterfaceResponse[param1Int];
      }
    };
  
  private static final String TAG = "IpSecTunnelInterfaceResponse";
  
  public final String interfaceName;
  
  public final int resourceId;
  
  public final int status;
}
