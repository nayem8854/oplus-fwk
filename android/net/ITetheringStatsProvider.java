package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ITetheringStatsProvider extends IInterface {
  public static final int QUOTA_UNLIMITED = -1;
  
  NetworkStats getTetherStats(int paramInt) throws RemoteException;
  
  void setInterfaceQuota(String paramString, long paramLong) throws RemoteException;
  
  class Default implements ITetheringStatsProvider {
    public NetworkStats getTetherStats(int param1Int) throws RemoteException {
      return null;
    }
    
    public void setInterfaceQuota(String param1String, long param1Long) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITetheringStatsProvider {
    private static final String DESCRIPTOR = "android.net.ITetheringStatsProvider";
    
    static final int TRANSACTION_getTetherStats = 1;
    
    static final int TRANSACTION_setInterfaceQuota = 2;
    
    public Stub() {
      attachInterface(this, "android.net.ITetheringStatsProvider");
    }
    
    public static ITetheringStatsProvider asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.ITetheringStatsProvider");
      if (iInterface != null && iInterface instanceof ITetheringStatsProvider)
        return (ITetheringStatsProvider)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "setInterfaceQuota";
      } 
      return "getTetherStats";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.net.ITetheringStatsProvider");
          return true;
        } 
        param1Parcel1.enforceInterface("android.net.ITetheringStatsProvider");
        String str = param1Parcel1.readString();
        long l = param1Parcel1.readLong();
        setInterfaceQuota(str, l);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel1.enforceInterface("android.net.ITetheringStatsProvider");
      param1Int1 = param1Parcel1.readInt();
      NetworkStats networkStats = getTetherStats(param1Int1);
      param1Parcel2.writeNoException();
      if (networkStats != null) {
        param1Parcel2.writeInt(1);
        networkStats.writeToParcel(param1Parcel2, 1);
      } else {
        param1Parcel2.writeInt(0);
      } 
      return true;
    }
    
    private static class Proxy implements ITetheringStatsProvider {
      public static ITetheringStatsProvider sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.ITetheringStatsProvider";
      }
      
      public NetworkStats getTetherStats(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          NetworkStats networkStats;
          parcel1.writeInterfaceToken("android.net.ITetheringStatsProvider");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ITetheringStatsProvider.Stub.getDefaultImpl() != null) {
            networkStats = ITetheringStatsProvider.Stub.getDefaultImpl().getTetherStats(param2Int);
            return networkStats;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            networkStats = NetworkStats.CREATOR.createFromParcel(parcel2);
          } else {
            networkStats = null;
          } 
          return networkStats;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInterfaceQuota(String param2String, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.ITetheringStatsProvider");
          parcel1.writeString(param2String);
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ITetheringStatsProvider.Stub.getDefaultImpl() != null) {
            ITetheringStatsProvider.Stub.getDefaultImpl().setInterfaceQuota(param2String, param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITetheringStatsProvider param1ITetheringStatsProvider) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITetheringStatsProvider != null) {
          Proxy.sDefaultImpl = param1ITetheringStatsProvider;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITetheringStatsProvider getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
