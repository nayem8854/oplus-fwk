package android.net;

import android.content.Context;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.DebugUtils;
import java.util.Map;

public class OplusNetworkingControlManager {
  public static final String ACTION_APP_NETWORK_NOT_ALLOWED = "oppo.intent.action.APP_NETWORK_NOT_ALLOWED";
  
  public static final String ACTION_MULTI_PACKAGE_ADDED = "oppo.intent.action.MULTI_APP_PACKAGE_ADDED";
  
  public static final String ACTION_MULTI_PACKAGE_REMOVED = "oppo.intent.action.MULTI_APP_PACKAGE_REMOVED";
  
  public static final String ACTION_ROM_APP_CHAGNE = "oppo.intent.action.ROM_APP_CHANGE";
  
  public static final int ALLOW = 1;
  
  public static final int DENY = 2;
  
  public static final String EXTRA_NETWORK_TYPE = "networkType";
  
  public static final String EXTRA_PACKAGE_NAME = "packageName";
  
  public static final int INVALID_UID = -1;
  
  public static final int POLICY_AllOW_MOBILEDATA_REJECT_WIFI = 2;
  
  public static final int POLICY_NONE = 0;
  
  public static final int POLICY_REJECT_ALL = 4;
  
  public static final int POLICY_REJECT_MOBILEDATA_AllOW_WIFI = 1;
  
  public static final int REJECT_MOBILEDATA = 5;
  
  public static final int REJECT_WIFI = 6;
  
  public static final int TYPE_MOBILEDATA = 0;
  
  public static final int TYPE_MOBILEDATA_MTK = 1;
  
  public static final int TYPE_MOBILEDATA_QCOM = 2;
  
  public static final int TYPE_WIFI = 3;
  
  private static OplusNetworkingControlManager mInstance = null;
  
  private IOplusNetworkingControlManager mService;
  
  private OplusNetworkingControlManager() {
    this.mService = IOplusNetworkingControlManager.Stub.asInterface(ServiceManager.getService("networking_control"));
  }
  
  public static OplusNetworkingControlManager getOppoNetworkingControlManager() {
    if (mInstance == null)
      mInstance = new OplusNetworkingControlManager(); 
    return mInstance;
  }
  
  public OplusNetworkingControlManager(Context paramContext, IOplusNetworkingControlManager paramIOplusNetworkingControlManager) {
    if (paramIOplusNetworkingControlManager != null) {
      this.mService = paramIOplusNetworkingControlManager;
      return;
    } 
    throw new IllegalArgumentException("missing IOplusNetworkingControlManager");
  }
  
  public static OplusNetworkingControlManager from(Context paramContext) {
    return (OplusNetworkingControlManager)paramContext.getSystemService("networking_control");
  }
  
  public void setUidPolicy(int paramInt1, int paramInt2) {
    IOplusNetworkingControlManager iOplusNetworkingControlManager = this.mService;
    if (iOplusNetworkingControlManager == null)
      return; 
    try {
      iOplusNetworkingControlManager.setUidPolicy(paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getUidPolicy(int paramInt) {
    IOplusNetworkingControlManager iOplusNetworkingControlManager = this.mService;
    if (iOplusNetworkingControlManager == null)
      return 0; 
    try {
      return iOplusNetworkingControlManager.getUidPolicy(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int[] getUidsWithPolicy(int paramInt) {
    IOplusNetworkingControlManager iOplusNetworkingControlManager = this.mService;
    if (iOplusNetworkingControlManager == null)
      return null; 
    try {
      return iOplusNetworkingControlManager.getUidsWithPolicy(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Map<Integer, Integer> getPolicyList() {
    IOplusNetworkingControlManager iOplusNetworkingControlManager = this.mService;
    if (iOplusNetworkingControlManager == null)
      return null; 
    try {
      return iOplusNetworkingControlManager.getPolicyList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static String uidPoliciesToString(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramInt);
    stringBuilder = stringBuilder.append(" (");
    if (paramInt == 0) {
      stringBuilder.append("NONE");
    } else {
      stringBuilder.append(DebugUtils.flagsToString(OplusNetworkingControlManager.class, "POLICY_", paramInt));
    } 
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
  
  public void factoryReset() {
    IOplusNetworkingControlManager iOplusNetworkingControlManager = this.mService;
    if (iOplusNetworkingControlManager == null)
      return; 
    try {
      iOplusNetworkingControlManager.factoryReset();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
}
