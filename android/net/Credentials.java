package android.net;

public class Credentials {
  private final int gid;
  
  private final int pid;
  
  private final int uid;
  
  public Credentials(int paramInt1, int paramInt2, int paramInt3) {
    this.pid = paramInt1;
    this.uid = paramInt2;
    this.gid = paramInt3;
  }
  
  public int getPid() {
    return this.pid;
  }
  
  public int getUid() {
    return this.uid;
  }
  
  public int getGid() {
    return this.gid;
  }
}
