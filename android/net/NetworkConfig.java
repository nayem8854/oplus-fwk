package android.net;

import java.util.Locale;

public class NetworkConfig {
  public boolean dependencyMet;
  
  public String name;
  
  public int priority;
  
  public int radio;
  
  public int restoreTime;
  
  public int type;
  
  public NetworkConfig(String paramString) {
    String[] arrayOfString = paramString.split(",");
    this.name = arrayOfString[0].trim().toLowerCase(Locale.ROOT);
    this.type = Integer.parseInt(arrayOfString[1]);
    this.radio = Integer.parseInt(arrayOfString[2]);
    this.priority = Integer.parseInt(arrayOfString[3]);
    this.restoreTime = Integer.parseInt(arrayOfString[4]);
    this.dependencyMet = Boolean.parseBoolean(arrayOfString[5]);
  }
  
  public boolean isDefault() {
    boolean bool;
    if (this.type == this.radio) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
