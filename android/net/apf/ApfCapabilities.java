package android.net.apf;

import android.annotation.SystemApi;
import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class ApfCapabilities implements Parcelable {
  public ApfCapabilities(int paramInt1, int paramInt2, int paramInt3) {
    this.apfVersionSupported = paramInt1;
    this.maximumApfProgramSize = paramInt2;
    this.apfPacketFormat = paramInt3;
  }
  
  private ApfCapabilities(Parcel paramParcel) {
    this.apfVersionSupported = paramParcel.readInt();
    this.maximumApfProgramSize = paramParcel.readInt();
    this.apfPacketFormat = paramParcel.readInt();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.apfVersionSupported);
    paramParcel.writeInt(this.maximumApfProgramSize);
    paramParcel.writeInt(this.apfPacketFormat);
  }
  
  public static final Parcelable.Creator<ApfCapabilities> CREATOR = new Parcelable.Creator<ApfCapabilities>() {
      public ApfCapabilities createFromParcel(Parcel param1Parcel) {
        return new ApfCapabilities(param1Parcel);
      }
      
      public ApfCapabilities[] newArray(int param1Int) {
        return new ApfCapabilities[param1Int];
      }
    };
  
  public final int apfPacketFormat;
  
  public final int apfVersionSupported;
  
  public final int maximumApfProgramSize;
  
  public String toString() {
    String str = getClass().getSimpleName();
    int i = this.apfVersionSupported;
    int j = this.maximumApfProgramSize, k = this.apfPacketFormat;
    return String.format("%s{version: %d, maxSize: %d, format: %d}", new Object[] { str, Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof ApfCapabilities;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    bool = bool1;
    if (this.apfVersionSupported == ((ApfCapabilities)paramObject).apfVersionSupported) {
      bool = bool1;
      if (this.maximumApfProgramSize == ((ApfCapabilities)paramObject).maximumApfProgramSize) {
        bool = bool1;
        if (this.apfPacketFormat == ((ApfCapabilities)paramObject).apfPacketFormat)
          bool = true; 
      } 
    } 
    return bool;
  }
  
  public boolean hasDataAccess() {
    boolean bool;
    if (this.apfVersionSupported >= 4) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean getApfDrop8023Frames() {
    return Resources.getSystem().getBoolean(17891363);
  }
  
  public static int[] getApfEtherTypeBlackList() {
    return Resources.getSystem().getIntArray(17235983);
  }
}
