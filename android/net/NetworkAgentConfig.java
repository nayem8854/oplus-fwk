package android.net;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

@SystemApi
public final class NetworkAgentConfig implements Parcelable {
  public boolean isExplicitlySelected() {
    return this.explicitlySelected;
  }
  
  public boolean isUnvalidatedConnectivityAcceptable() {
    return this.acceptUnvalidated;
  }
  
  public boolean isPartialConnectivityAcceptable() {
    return this.acceptPartialConnectivity;
  }
  
  public boolean isProvisioningNotificationEnabled() {
    return this.provisioningNotificationDisabled ^ true;
  }
  
  public String getSubscriberId() {
    return this.subscriberId;
  }
  
  public boolean isNat64DetectionEnabled() {
    return this.skip464xlat ^ true;
  }
  
  public int legacyType = -1;
  
  public int getLegacyType() {
    return this.legacyType;
  }
  
  public String legacyTypeName = "";
  
  public String getLegacyTypeName() {
    return this.legacyTypeName;
  }
  
  private String mLegacyExtraInfo = "";
  
  public String getLegacyExtraInfo() {
    return this.mLegacyExtraInfo;
  }
  
  public NetworkAgentConfig(NetworkAgentConfig paramNetworkAgentConfig) {
    if (paramNetworkAgentConfig != null) {
      this.allowBypass = paramNetworkAgentConfig.allowBypass;
      this.explicitlySelected = paramNetworkAgentConfig.explicitlySelected;
      this.acceptUnvalidated = paramNetworkAgentConfig.acceptUnvalidated;
      this.acceptPartialConnectivity = paramNetworkAgentConfig.acceptPartialConnectivity;
      this.subscriberId = paramNetworkAgentConfig.subscriberId;
      this.provisioningNotificationDisabled = paramNetworkAgentConfig.provisioningNotificationDisabled;
      this.skip464xlat = paramNetworkAgentConfig.skip464xlat;
      this.legacyType = paramNetworkAgentConfig.legacyType;
      this.legacyTypeName = paramNetworkAgentConfig.legacyTypeName;
      this.mLegacyExtraInfo = paramNetworkAgentConfig.mLegacyExtraInfo;
    } 
  }
  
  class Builder {
    private final NetworkAgentConfig mConfig = new NetworkAgentConfig();
    
    public Builder setExplicitlySelected(boolean param1Boolean) {
      this.mConfig.explicitlySelected = param1Boolean;
      return this;
    }
    
    public Builder setUnvalidatedConnectivityAcceptable(boolean param1Boolean) {
      this.mConfig.acceptUnvalidated = param1Boolean;
      return this;
    }
    
    public Builder setPartialConnectivityAcceptable(boolean param1Boolean) {
      this.mConfig.acceptPartialConnectivity = param1Boolean;
      return this;
    }
    
    public Builder setSubscriberId(String param1String) {
      this.mConfig.subscriberId = param1String;
      return this;
    }
    
    public Builder disableNat64Detection() {
      this.mConfig.skip464xlat = true;
      return this;
    }
    
    public Builder disableProvisioningNotification() {
      this.mConfig.provisioningNotificationDisabled = true;
      return this;
    }
    
    public Builder setLegacyType(int param1Int) {
      this.mConfig.legacyType = param1Int;
      return this;
    }
    
    public Builder setLegacyTypeName(String param1String) {
      this.mConfig.legacyTypeName = param1String;
      return this;
    }
    
    public Builder setLegacyExtraInfo(String param1String) {
      NetworkAgentConfig.access$002(this.mConfig, param1String);
      return this;
    }
    
    public NetworkAgentConfig build() {
      return this.mConfig;
    }
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.allowBypass == ((NetworkAgentConfig)paramObject).allowBypass && this.explicitlySelected == ((NetworkAgentConfig)paramObject).explicitlySelected && this.acceptUnvalidated == ((NetworkAgentConfig)paramObject).acceptUnvalidated && this.acceptPartialConnectivity == ((NetworkAgentConfig)paramObject).acceptPartialConnectivity && this.provisioningNotificationDisabled == ((NetworkAgentConfig)paramObject).provisioningNotificationDisabled && this.skip464xlat == ((NetworkAgentConfig)paramObject).skip464xlat && this.legacyType == ((NetworkAgentConfig)paramObject).legacyType) {
      String str1 = this.subscriberId, str2 = ((NetworkAgentConfig)paramObject).subscriberId;
      if (Objects.equals(str1, str2)) {
        str2 = this.legacyTypeName;
        str1 = ((NetworkAgentConfig)paramObject).legacyTypeName;
        if (Objects.equals(str2, str1)) {
          str2 = this.mLegacyExtraInfo;
          paramObject = ((NetworkAgentConfig)paramObject).mLegacyExtraInfo;
          if (Objects.equals(str2, paramObject))
            return null; 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    boolean bool1 = this.allowBypass, bool2 = this.explicitlySelected, bool3 = this.acceptUnvalidated, bool4 = this.acceptPartialConnectivity;
    boolean bool5 = this.provisioningNotificationDisabled;
    String str1 = this.subscriberId;
    boolean bool6 = this.skip464xlat;
    int i = this.legacyType;
    String str2 = this.legacyTypeName, str3 = this.mLegacyExtraInfo;
    return Objects.hash(new Object[] { Boolean.valueOf(bool1), Boolean.valueOf(bool2), Boolean.valueOf(bool3), Boolean.valueOf(bool4), Boolean.valueOf(bool5), str1, Boolean.valueOf(bool6), Integer.valueOf(i), str2, str3 });
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("NetworkAgentConfig { allowBypass = ");
    stringBuilder.append(this.allowBypass);
    stringBuilder.append(", explicitlySelected = ");
    stringBuilder.append(this.explicitlySelected);
    stringBuilder.append(", acceptUnvalidated = ");
    stringBuilder.append(this.acceptUnvalidated);
    stringBuilder.append(", acceptPartialConnectivity = ");
    stringBuilder.append(this.acceptPartialConnectivity);
    stringBuilder.append(", provisioningNotificationDisabled = ");
    stringBuilder.append(this.provisioningNotificationDisabled);
    stringBuilder.append(", subscriberId = '");
    stringBuilder.append(this.subscriberId);
    stringBuilder.append('\'');
    stringBuilder.append(", skip464xlat = ");
    stringBuilder.append(this.skip464xlat);
    stringBuilder.append(", legacyType = ");
    stringBuilder.append(this.legacyType);
    stringBuilder.append(", hasShownBroken = ");
    stringBuilder.append(this.hasShownBroken);
    stringBuilder.append(", legacyTypeName = '");
    stringBuilder.append(this.legacyTypeName);
    stringBuilder.append('\'');
    stringBuilder.append(", legacyExtraInfo = '");
    stringBuilder.append(this.mLegacyExtraInfo);
    stringBuilder.append('\'');
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.allowBypass);
    paramParcel.writeInt(this.explicitlySelected);
    paramParcel.writeInt(this.acceptUnvalidated);
    paramParcel.writeInt(this.acceptPartialConnectivity);
    paramParcel.writeString(this.subscriberId);
    paramParcel.writeInt(this.provisioningNotificationDisabled);
    paramParcel.writeInt(this.skip464xlat);
    paramParcel.writeInt(this.legacyType);
    paramParcel.writeString(this.legacyTypeName);
    paramParcel.writeString(this.mLegacyExtraInfo);
  }
  
  public static final Parcelable.Creator<NetworkAgentConfig> CREATOR = new Parcelable.Creator<NetworkAgentConfig>() {
      public NetworkAgentConfig createFromParcel(Parcel param1Parcel) {
        boolean bool2;
        NetworkAgentConfig networkAgentConfig = new NetworkAgentConfig();
        int i = param1Parcel.readInt();
        boolean bool1 = true;
        if (i != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        networkAgentConfig.allowBypass = bool2;
        if (param1Parcel.readInt() != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        networkAgentConfig.explicitlySelected = bool2;
        if (param1Parcel.readInt() != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        networkAgentConfig.acceptUnvalidated = bool2;
        if (param1Parcel.readInt() != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        networkAgentConfig.acceptPartialConnectivity = bool2;
        networkAgentConfig.subscriberId = param1Parcel.readString();
        if (param1Parcel.readInt() != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        networkAgentConfig.provisioningNotificationDisabled = bool2;
        if (param1Parcel.readInt() != 0) {
          bool2 = bool1;
        } else {
          bool2 = false;
        } 
        networkAgentConfig.skip464xlat = bool2;
        networkAgentConfig.legacyType = param1Parcel.readInt();
        networkAgentConfig.legacyTypeName = param1Parcel.readString();
        NetworkAgentConfig.access$002(networkAgentConfig, param1Parcel.readString());
        return networkAgentConfig;
      }
      
      public NetworkAgentConfig[] newArray(int param1Int) {
        return new NetworkAgentConfig[param1Int];
      }
    };
  
  public boolean acceptPartialConnectivity;
  
  public boolean acceptUnvalidated;
  
  public boolean allowBypass;
  
  public boolean explicitlySelected;
  
  public transient boolean hasShownBroken;
  
  public boolean provisioningNotificationDisabled;
  
  public boolean skip464xlat;
  
  public String subscriberId;
  
  public NetworkAgentConfig() {}
}
