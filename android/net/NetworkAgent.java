package android.net;

import android.annotation.SystemApi;
import android.content.Context;
import android.os.Bundle;
import android.os.ConditionVariable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import com.android.internal.util.AsyncChannel;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

@SystemApi
public abstract class NetworkAgent {
  private final ArrayList<Message> mPreConnectedQueue = new ArrayList<>();
  
  private volatile long mLastBwRefreshTime = 0L;
  
  private boolean mBandwidthUpdateScheduled = false;
  
  private AtomicBoolean mBandwidthUpdatePending = new AtomicBoolean(false);
  
  private final Object mRegisterLock = new Object();
  
  public static String REDIRECT_URL_KEY = "redirect URL";
  
  private static final int BASE = 528384;
  
  private static final long BW_REFRESH_MIN_WIN_MS = 500L;
  
  public static final int CMD_ADD_KEEPALIVE_PACKET_FILTER = 528400;
  
  public static final int CMD_NOTIFY_DATA_CONNECT_STATE_RECOVERY = 528403;
  
  public static final int CMD_NOTIFY_DATA_CONNECT_STATE_SUSPEND = 528402;
  
  public static final int CMD_PREVENT_AUTOMATIC_RECONNECT = 528399;
  
  public static final int CMD_REMOVE_KEEPALIVE_PACKET_FILTER = 528401;
  
  public static final int CMD_REPORT_NETWORK_STATUS = 528391;
  
  public static final int CMD_REQUEST_BANDWIDTH_UPDATE = 528394;
  
  public static final int CMD_SAVE_ACCEPT_UNVALIDATED = 528393;
  
  public static final int CMD_SET_SIGNAL_STRENGTH_THRESHOLDS = 528398;
  
  public static final int CMD_START_SOCKET_KEEPALIVE = 528395;
  
  public static final int CMD_STOP_SOCKET_KEEPALIVE = 528396;
  
  public static final int CMD_SUSPECT_BAD = 528384;
  
  private static final boolean DBG = true;
  
  public static final int EVENT_NETWORK_CAPABILITIES_CHANGED = 528386;
  
  public static final int EVENT_NETWORK_INFO_CHANGED = 528385;
  
  public static final int EVENT_NETWORK_PROPERTIES_CHANGED = 528387;
  
  public static final int EVENT_NETWORK_SCORE_CHANGED = 528388;
  
  public static final int EVENT_SET_EXPLICITLY_SELECTED = 528392;
  
  public static final int EVENT_SOCKET_KEEPALIVE = 528397;
  
  public static final int INVALID_NETWORK = 2;
  
  public static final int VALIDATION_STATUS_NOT_VALID = 2;
  
  public static final int VALIDATION_STATUS_VALID = 1;
  
  public static final int VALID_NETWORK = 1;
  
  private static final boolean VDBG = false;
  
  public static final int WIFI_BASE_SCORE = 60;
  
  private final String LOG_TAG;
  
  private volatile AsyncChannel mAsyncChannel;
  
  private final Handler mHandler;
  
  private volatile InitialConfiguration mInitialConfiguration;
  
  private final boolean mIsLegacy;
  
  private volatile Network mNetwork;
  
  private NetworkInfo mNetworkInfo;
  
  public final int providerId;
  
  public NetworkAgent(Looper paramLooper, Context paramContext, String paramString, NetworkInfo paramNetworkInfo, NetworkCapabilities paramNetworkCapabilities, LinkProperties paramLinkProperties, int paramInt) {
    this(paramLooper, paramContext, paramString, paramNetworkInfo, paramNetworkCapabilities, paramLinkProperties, paramInt, null, -1);
  }
  
  public NetworkAgent(Looper paramLooper, Context paramContext, String paramString, NetworkInfo paramNetworkInfo, NetworkCapabilities paramNetworkCapabilities, LinkProperties paramLinkProperties, int paramInt, NetworkAgentConfig paramNetworkAgentConfig) {
    this(paramLooper, paramContext, paramString, paramNetworkInfo, paramNetworkCapabilities, paramLinkProperties, paramInt, paramNetworkAgentConfig, -1);
  }
  
  public NetworkAgent(Looper paramLooper, Context paramContext, String paramString, NetworkInfo paramNetworkInfo, NetworkCapabilities paramNetworkCapabilities, LinkProperties paramLinkProperties, int paramInt1, int paramInt2) {
    this(paramLooper, paramContext, paramString, paramNetworkInfo, paramNetworkCapabilities, paramLinkProperties, paramInt1, null, paramInt2);
  }
  
  public NetworkAgent(Looper paramLooper, Context paramContext, String paramString, NetworkInfo paramNetworkInfo, NetworkCapabilities paramNetworkCapabilities, LinkProperties paramLinkProperties, int paramInt1, NetworkAgentConfig paramNetworkAgentConfig, int paramInt2) {
    this(paramLooper, paramContext, paramString, paramNetworkCapabilities, paramLinkProperties, paramInt1, paramNetworkAgentConfig, paramInt2, paramNetworkInfo, true);
    register();
  }
  
  private static NetworkInfo getLegacyNetworkInfo(NetworkAgentConfig paramNetworkAgentConfig) {
    NetworkInfo networkInfo = new NetworkInfo(paramNetworkAgentConfig.legacyType, 0, paramNetworkAgentConfig.legacyTypeName, "");
    networkInfo.setIsAvailable(true);
    networkInfo.setExtraInfo(paramNetworkAgentConfig.getLegacyExtraInfo());
    return networkInfo;
  }
  
  public NetworkAgent(Context paramContext, Looper paramLooper, String paramString, NetworkCapabilities paramNetworkCapabilities, LinkProperties paramLinkProperties, int paramInt, NetworkAgentConfig paramNetworkAgentConfig, NetworkProvider paramNetworkProvider) {
    this(paramLooper, paramContext, paramString, paramNetworkCapabilities, paramLinkProperties, paramInt, paramNetworkAgentConfig, i, networkInfo, false);
  }
  
  private static class InitialConfiguration {
    public final NetworkCapabilities capabilities;
    
    public final NetworkAgentConfig config;
    
    public final Context context;
    
    public final NetworkInfo info;
    
    public final LinkProperties properties;
    
    public final int score;
    
    InitialConfiguration(Context param1Context, NetworkCapabilities param1NetworkCapabilities, LinkProperties param1LinkProperties, int param1Int, NetworkAgentConfig param1NetworkAgentConfig, NetworkInfo param1NetworkInfo) {
      this.context = param1Context;
      this.capabilities = param1NetworkCapabilities;
      this.properties = param1LinkProperties;
      this.score = param1Int;
      this.config = param1NetworkAgentConfig;
      this.info = param1NetworkInfo;
    }
  }
  
  private NetworkAgent(Looper paramLooper, Context paramContext, String paramString, NetworkCapabilities paramNetworkCapabilities, LinkProperties paramLinkProperties, int paramInt1, NetworkAgentConfig paramNetworkAgentConfig, int paramInt2, NetworkInfo paramNetworkInfo, boolean paramBoolean) {
    this.mHandler = new NetworkAgentHandler(paramLooper);
    this.LOG_TAG = paramString;
    this.mIsLegacy = paramBoolean;
    this.mNetworkInfo = new NetworkInfo(paramNetworkInfo);
    this.providerId = paramInt2;
    if (paramNetworkInfo != null && paramNetworkCapabilities != null && paramLinkProperties != null) {
      this.mInitialConfiguration = new InitialConfiguration(paramContext, new NetworkCapabilities(paramNetworkCapabilities), new LinkProperties(paramLinkProperties), paramInt1, paramNetworkAgentConfig, paramNetworkInfo);
      return;
    } 
    throw new IllegalArgumentException();
  }
  
  class NetworkAgentHandler extends Handler {
    final NetworkAgent this$0;
    
    NetworkAgentHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      boolean bool = false;
      int j = 0;
      if (i != 69633) {
        if (i != 528384) {
          StringBuilder stringBuilder1;
          if (i != 528391) {
            Bundle bundle;
            int[] arrayOfInt;
            KeepalivePacketData keepalivePacketData;
            ArrayList<Integer> arrayList;
            long l;
            Duration duration;
            switch (i) {
              default:
                switch (i) {
                  default:
                    switch (i) {
                      default:
                        return;
                      case 528403:
                        NetworkAgent.this.notifyDataRecovery();
                      case 528402:
                        NetworkAgent.this.notifyDataSuspend();
                      case 528401:
                        NetworkAgent.this.onRemoveKeepalivePacketFilter(param1Message.arg1);
                      case 528400:
                        NetworkAgent.this.onAddKeepalivePacketFilter(param1Message.arg1, (KeepalivePacketData)param1Message.obj);
                      case 528399:
                        NetworkAgent.this.onAutomaticReconnectDisabled();
                      case 528398:
                        break;
                    } 
                    bundle = (Bundle)param1Message.obj;
                    arrayList = bundle.getIntegerArrayList("thresholds");
                    if (arrayList != null)
                      j = arrayList.size(); 
                    arrayOfInt = new int[j];
                    for (j = 0; j < arrayOfInt.length; j++)
                      arrayOfInt[j] = ((Integer)arrayList.get(j)).intValue(); 
                    NetworkAgent.this.onSignalStrengthThresholdsUpdated(arrayOfInt);
                  case 528396:
                    NetworkAgent.this.onStopSocketKeepalive(((Message)arrayOfInt).arg1);
                  case 528395:
                    null = NetworkAgent.this;
                    j = ((Message)arrayOfInt).arg1;
                    l = ((Message)arrayOfInt).arg2;
                    duration = Duration.ofSeconds(l);
                    keepalivePacketData = (KeepalivePacketData)((Message)arrayOfInt).obj;
                    null.onStartSocketKeepalive(j, duration, keepalivePacketData);
                  case 528394:
                    l = System.currentTimeMillis();
                    if (l >= NetworkAgent.this.mLastBwRefreshTime + 500L) {
                      NetworkAgent.access$302(NetworkAgent.this, false);
                      if (!NetworkAgent.this.mBandwidthUpdatePending.getAndSet(true))
                        NetworkAgent.this.onBandwidthUpdateRequested(); 
                    } 
                    if (!NetworkAgent.this.mBandwidthUpdateScheduled) {
                      long l1 = NetworkAgent.this.mLastBwRefreshTime;
                      NetworkAgent.access$302(NetworkAgent.this, sendEmptyMessageDelayed(528394, l1 + 500L - l + 1L));
                    } 
                  case 528393:
                    break;
                } 
                null = NetworkAgent.this;
                if (((Message)keepalivePacketData).arg1 != 0)
                  bool = true; 
                null.onSaveAcceptUnvalidated(bool);
              case 69636:
                NetworkAgent.this.log("NetworkAgent channel lost");
                NetworkAgent.this.onNetworkUnwanted();
                synchronized (NetworkAgent.this.mPreConnectedQueue) {
                
                } 
              case 69635:
                break;
            } 
            if (NetworkAgent.this.mAsyncChannel != null)
              NetworkAgent.this.mAsyncChannel.disconnect(); 
          } 
          String str = ((Bundle)param1Message.obj).getString(NetworkAgent.REDIRECT_URL_KEY);
          StringBuilder stringBuilder2 = null;
          Uri uri = null;
          if (str != null)
            try {
              uri = Uri.parse(str);
            } catch (Exception exception) {
              String str1 = NetworkAgent.this.LOG_TAG;
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append("Surprising URI : ");
              stringBuilder1.append(str);
              Log.wtf(str1, stringBuilder1.toString(), exception);
              stringBuilder1 = stringBuilder2;
            }  
          NetworkAgent.this.onValidationStatus(param1Message.arg1, (Uri)stringBuilder1);
        } 
        NetworkAgent networkAgent = NetworkAgent.this;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unhandled Message ");
        stringBuilder.append(param1Message);
        networkAgent.log(stringBuilder.toString());
      } 
      if (NetworkAgent.this.mAsyncChannel != null)
        NetworkAgent.this.log("Received new connection while already connected!"); 
      null = new AsyncChannel();
      null.connected(null, this, param1Message.replyTo);
      null.replyToMessage(param1Message, 69634, 0);
      synchronized (NetworkAgent.this.mPreConnectedQueue) {
        NetworkAgent.access$002(NetworkAgent.this, null);
        for (Message message : NetworkAgent.this.mPreConnectedQueue)
          null.sendMessage(message); 
        NetworkAgent.this.mPreConnectedQueue.clear();
      } 
    }
  }
  
  public void notifyDataSuspend() {}
  
  public void notifyDataRecovery() {}
  
  public Network register() {
    synchronized (this.mRegisterLock) {
      if (this.mNetwork == null) {
        Context context = this.mInitialConfiguration.context;
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService("connectivity");
        Messenger messenger = new Messenger();
        this(this.mHandler);
        NetworkInfo networkInfo = new NetworkInfo();
        this(this.mInitialConfiguration.info);
        this.mNetwork = connectivityManager.registerNetworkAgent(messenger, networkInfo, this.mInitialConfiguration.properties, this.mInitialConfiguration.capabilities, this.mInitialConfiguration.score, this.mInitialConfiguration.config, this.providerId);
        this.mInitialConfiguration = null;
        return this.mNetwork;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Agent already registered");
      throw illegalStateException;
    } 
  }
  
  public Messenger registerForTest(Network paramNetwork) {
    log("Registering NetworkAgent for test");
    synchronized (this.mRegisterLock) {
      this.mNetwork = paramNetwork;
      this.mInitialConfiguration = null;
      return new Messenger(this.mHandler);
    } 
  }
  
  public boolean waitForIdle(long paramLong) {
    ConditionVariable conditionVariable = new ConditionVariable(false);
    Handler handler = this.mHandler;
    Objects.requireNonNull(conditionVariable);
    handler.post(new _$$Lambda$xEDVsWySjOhZCU_CTVGu6ziJ2xc(conditionVariable));
    return conditionVariable.block(paramLong);
  }
  
  public Network getNetwork() {
    return this.mNetwork;
  }
  
  private void queueOrSendMessage(int paramInt, Object paramObject) {
    queueOrSendMessage(paramInt, 0, 0, paramObject);
  }
  
  private void queueOrSendMessage(int paramInt1, int paramInt2, int paramInt3) {
    queueOrSendMessage(paramInt1, paramInt2, paramInt3, null);
  }
  
  private void queueOrSendMessage(int paramInt1, int paramInt2, int paramInt3, Object paramObject) {
    Message message = Message.obtain();
    message.what = paramInt1;
    message.arg1 = paramInt2;
    message.arg2 = paramInt3;
    message.obj = paramObject;
    queueOrSendMessage(message);
  }
  
  private void queueOrSendMessage(Message paramMessage) {
    synchronized (this.mPreConnectedQueue) {
      if (this.mAsyncChannel != null) {
        this.mAsyncChannel.sendMessage(paramMessage);
      } else {
        this.mPreConnectedQueue.add(paramMessage);
      } 
      return;
    } 
  }
  
  public final void sendLinkProperties(LinkProperties paramLinkProperties) {
    Objects.requireNonNull(paramLinkProperties);
    queueOrSendMessage(528387, new LinkProperties(paramLinkProperties));
  }
  
  public void markConnected() {
    if (!this.mIsLegacy) {
      NetworkInfo networkInfo1 = this.mNetworkInfo;
      NetworkInfo.DetailedState detailedState = NetworkInfo.DetailedState.CONNECTED;
      NetworkInfo networkInfo2 = this.mNetworkInfo;
      String str = networkInfo2.getExtraInfo();
      networkInfo1.setDetailedState(detailedState, null, str);
      queueOrSendMessage(528385, this.mNetworkInfo);
      return;
    } 
    throw new UnsupportedOperationException("Legacy agents can't call markConnected.");
  }
  
  public void unregister() {
    if (!this.mIsLegacy) {
      this.mNetworkInfo.setDetailedState(NetworkInfo.DetailedState.DISCONNECTED, null, null);
      queueOrSendMessage(528385, this.mNetworkInfo);
      return;
    } 
    throw new UnsupportedOperationException("Legacy agents can't call unregister.");
  }
  
  @Deprecated
  public void setLegacySubtype(int paramInt, String paramString) {
    if (!this.mIsLegacy) {
      this.mNetworkInfo.setSubtype(paramInt, paramString);
      queueOrSendMessage(528385, this.mNetworkInfo);
      return;
    } 
    throw new UnsupportedOperationException("Legacy agents can't call setLegacySubtype.");
  }
  
  @Deprecated
  public void setLegacyExtraInfo(String paramString) {
    if (!this.mIsLegacy) {
      this.mNetworkInfo.setExtraInfo(paramString);
      queueOrSendMessage(528385, this.mNetworkInfo);
      return;
    } 
    throw new UnsupportedOperationException("Legacy agents can't call setLegacyExtraInfo.");
  }
  
  public final void sendNetworkInfo(NetworkInfo paramNetworkInfo, NetworkInfo.DetailedState paramDetailedState) {
    String str = this.LOG_TAG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("NetworkInfo DetailedState: ");
    stringBuilder.append(paramDetailedState);
    Log.d(str, stringBuilder.toString());
    queueOrSendMessage(528385, new NetworkInfo(paramNetworkInfo));
  }
  
  public final void sendNetworkInfo(NetworkInfo paramNetworkInfo) {
    if (this.mIsLegacy) {
      queueOrSendMessage(528385, new NetworkInfo(paramNetworkInfo));
      return;
    } 
    throw new UnsupportedOperationException("Only legacy agents can call sendNetworkInfo.");
  }
  
  public final void sendNetworkCapabilities(NetworkCapabilities paramNetworkCapabilities) {
    Objects.requireNonNull(paramNetworkCapabilities);
    this.mBandwidthUpdatePending.set(false);
    this.mLastBwRefreshTime = System.currentTimeMillis();
    queueOrSendMessage(528386, new NetworkCapabilities(paramNetworkCapabilities));
  }
  
  public final void sendNetworkScore(int paramInt) {
    if (paramInt >= 0) {
      queueOrSendMessage(528388, paramInt, 0);
      return;
    } 
    throw new IllegalArgumentException("Score must be >= 0");
  }
  
  public void explicitlySelected(boolean paramBoolean) {
    explicitlySelected(true, paramBoolean);
  }
  
  public void explicitlySelected(boolean paramBoolean1, boolean paramBoolean2) {
    queueOrSendMessage(528392, paramBoolean1, paramBoolean2);
  }
  
  public void onNetworkUnwanted() {
    unwanted();
  }
  
  protected void unwanted() {}
  
  public void onBandwidthUpdateRequested() {
    pollLceData();
  }
  
  protected void pollLceData() {}
  
  public void onValidationStatus(int paramInt, Uri paramUri) {
    String str;
    if (paramUri == null) {
      str = "";
    } else {
      str = str.toString();
    } 
    networkStatus(paramInt, str);
  }
  
  protected void networkStatus(int paramInt, String paramString) {}
  
  public void onSaveAcceptUnvalidated(boolean paramBoolean) {
    saveAcceptUnvalidated(paramBoolean);
  }
  
  protected void saveAcceptUnvalidated(boolean paramBoolean) {}
  
  public void onStartSocketKeepalive(int paramInt, Duration paramDuration, KeepalivePacketData paramKeepalivePacketData) {
    long l = paramDuration.getSeconds();
    if (l >= 10L && l <= 3600L) {
      Message message = this.mHandler.obtainMessage(528395, paramInt, (int)l, paramKeepalivePacketData);
      startSocketKeepalive(message);
      message.recycle();
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Interval needs to be comprised between 10 and 3600 but was ");
    stringBuilder.append(l);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  protected void startSocketKeepalive(Message paramMessage) {
    onSocketKeepaliveEvent(paramMessage.arg1, -30);
  }
  
  public void onStopSocketKeepalive(int paramInt) {
    Message message = this.mHandler.obtainMessage(528396, paramInt, 0, null);
    stopSocketKeepalive(message);
    message.recycle();
  }
  
  protected void stopSocketKeepalive(Message paramMessage) {
    onSocketKeepaliveEvent(paramMessage.arg1, -30);
  }
  
  public final void sendSocketKeepaliveEvent(int paramInt1, int paramInt2) {
    queueOrSendMessage(528397, paramInt1, paramInt2);
  }
  
  public void onSocketKeepaliveEvent(int paramInt1, int paramInt2) {
    sendSocketKeepaliveEvent(paramInt1, paramInt2);
  }
  
  public void onAddKeepalivePacketFilter(int paramInt, KeepalivePacketData paramKeepalivePacketData) {
    Message message = this.mHandler.obtainMessage(528400, paramInt, 0, paramKeepalivePacketData);
    addKeepalivePacketFilter(message);
    message.recycle();
  }
  
  protected void addKeepalivePacketFilter(Message paramMessage) {}
  
  public void onRemoveKeepalivePacketFilter(int paramInt) {
    Message message = this.mHandler.obtainMessage(528401, paramInt, 0, null);
    removeKeepalivePacketFilter(message);
    message.recycle();
  }
  
  protected void removeKeepalivePacketFilter(Message paramMessage) {}
  
  public void onSignalStrengthThresholdsUpdated(int[] paramArrayOfint) {
    setSignalStrengthThresholds(paramArrayOfint);
  }
  
  protected void setSignalStrengthThresholds(int[] paramArrayOfint) {}
  
  public void onAutomaticReconnectDisabled() {
    preventAutomaticReconnect();
  }
  
  protected void preventAutomaticReconnect() {}
  
  protected void log(String paramString) {
    String str = this.LOG_TAG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("NetworkAgent: ");
    stringBuilder.append(paramString);
    Log.d(str, stringBuilder.toString());
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ValidationStatus {}
}
