package android.net;

import android.content.Context;
import android.util.Log;
import com.android.org.conscrypt.ClientSessionContext;
import com.android.org.conscrypt.FileClientSessionCache;
import com.android.org.conscrypt.SSLClientSessionCache;
import java.io.File;
import java.io.IOException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSessionContext;

public final class SSLSessionCache {
  private static final String TAG = "SSLSessionCache";
  
  final SSLClientSessionCache mSessionCache;
  
  public static void install(SSLSessionCache paramSSLSessionCache, SSLContext paramSSLContext) {
    ClientSessionContext clientSessionContext;
    SSLSessionContext sSLSessionContext = paramSSLContext.getClientSessionContext();
    if (sSLSessionContext instanceof ClientSessionContext) {
      SSLClientSessionCache sSLClientSessionCache;
      clientSessionContext = (ClientSessionContext)sSLSessionContext;
      if (paramSSLSessionCache == null) {
        paramSSLSessionCache = null;
      } else {
        sSLClientSessionCache = paramSSLSessionCache.mSessionCache;
      } 
      clientSessionContext.setPersistentCache(sSLClientSessionCache);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Incompatible SSLContext: ");
    stringBuilder.append(clientSessionContext);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public SSLSessionCache(Object paramObject) {
    this.mSessionCache = (SSLClientSessionCache)paramObject;
  }
  
  public SSLSessionCache(File paramFile) throws IOException {
    this.mSessionCache = FileClientSessionCache.usingDirectory(paramFile);
  }
  
  public SSLSessionCache(Context paramContext) {
    SSLClientSessionCache sSLClientSessionCache;
    File file = paramContext.getDir("sslcache", 0);
    paramContext = null;
    try {
      SSLClientSessionCache sSLClientSessionCache1 = FileClientSessionCache.usingDirectory(file);
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to create SSL session cache in ");
      stringBuilder.append(file);
      Log.w("SSLSessionCache", stringBuilder.toString(), iOException);
    } 
    this.mSessionCache = sSLClientSessionCache;
  }
}
