package android.net;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;

public final class TestNetworkInterface implements Parcelable {
  public int describeContents() {
    boolean bool;
    if (this.mFileDescriptor != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.mFileDescriptor, 1);
    paramParcel.writeString(this.mInterfaceName);
  }
  
  public TestNetworkInterface(ParcelFileDescriptor paramParcelFileDescriptor, String paramString) {
    this.mFileDescriptor = paramParcelFileDescriptor;
    this.mInterfaceName = paramString;
  }
  
  private TestNetworkInterface(Parcel paramParcel) {
    this.mFileDescriptor = paramParcel.<ParcelFileDescriptor>readParcelable(ParcelFileDescriptor.class.getClassLoader());
    this.mInterfaceName = paramParcel.readString();
  }
  
  public ParcelFileDescriptor getFileDescriptor() {
    return this.mFileDescriptor;
  }
  
  public String getInterfaceName() {
    return this.mInterfaceName;
  }
  
  public static final Parcelable.Creator<TestNetworkInterface> CREATOR = new Parcelable.Creator<TestNetworkInterface>() {
      public TestNetworkInterface createFromParcel(Parcel param1Parcel) {
        return new TestNetworkInterface(param1Parcel);
      }
      
      public TestNetworkInterface[] newArray(int param1Int) {
        return new TestNetworkInterface[param1Int];
      }
    };
  
  private final ParcelFileDescriptor mFileDescriptor;
  
  private final String mInterfaceName;
}
