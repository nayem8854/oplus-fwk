package android.net;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UrlQuerySanitizer {
  public static interface ValueSanitizer {
    String sanitize(String param1String);
  }
  
  public class ParameterValuePair {
    public String mParameter;
    
    public String mValue;
    
    final UrlQuerySanitizer this$0;
    
    public ParameterValuePair(String param1String1, String param1String2) {
      this.mParameter = param1String1;
      this.mValue = param1String2;
    }
  }
  
  private final HashMap<String, ValueSanitizer> mSanitizers = new HashMap<>();
  
  private final HashMap<String, String> mEntries = new HashMap<>();
  
  private final ArrayList<ParameterValuePair> mEntriesList = new ArrayList<>();
  
  private ValueSanitizer mUnregisteredParameterValueSanitizer = getAllIllegal();
  
  class IllegalCharacterValueSanitizer implements ValueSanitizer {
    public static final int ALL_BUT_NUL_AND_ANGLE_BRACKETS_LEGAL = 1439;
    
    public static final int ALL_BUT_NUL_LEGAL = 1535;
    
    public static final int ALL_BUT_WHITESPACE_LEGAL = 1532;
    
    public static final int ALL_ILLEGAL = 0;
    
    public static final int ALL_OK = 2047;
    
    public static final int ALL_WHITESPACE_OK = 3;
    
    public static final int AMP_AND_SPACE_LEGAL = 129;
    
    public static final int AMP_LEGAL = 128;
    
    public static final int AMP_OK = 128;
    
    public static final int DQUOTE_OK = 8;
    
    public static final int GT_OK = 64;
    
    private static final String JAVASCRIPT_PREFIX = "javascript:";
    
    public static final int LT_OK = 32;
    
    private static final int MIN_SCRIPT_PREFIX_LENGTH;
    
    public static final int NON_7_BIT_ASCII_OK = 4;
    
    public static final int NUL_OK = 512;
    
    public static final int OTHER_WHITESPACE_OK = 2;
    
    public static final int PCT_OK = 256;
    
    public static final int SCRIPT_URL_OK = 1024;
    
    public static final int SPACE_LEGAL = 1;
    
    public static final int SPACE_OK = 1;
    
    public static final int SQUOTE_OK = 16;
    
    public static final int URL_AND_SPACE_LEGAL = 405;
    
    public static final int URL_LEGAL = 404;
    
    private static final String VBSCRIPT_PREFIX = "vbscript:";
    
    private int mFlags;
    
    static {
      int i = "javascript:".length(), j = "vbscript:".length();
      MIN_SCRIPT_PREFIX_LENGTH = Math.min(i, j);
    }
    
    public IllegalCharacterValueSanitizer(UrlQuerySanitizer this$0) {
      this.mFlags = this$0;
    }
    
    public String sanitize(String param1String) {
      if (param1String == null)
        return null; 
      int i = param1String.length();
      if ((this.mFlags & 0x400) == 0 && 
        i >= MIN_SCRIPT_PREFIX_LENGTH) {
        String str1 = param1String.toLowerCase(Locale.ROOT);
        if (str1.startsWith("javascript:") || 
          str1.startsWith("vbscript:"))
          return ""; 
      } 
      String str = param1String;
      if ((this.mFlags & 0x3) == 0) {
        str = trimWhitespace(param1String);
        i = str.length();
      } 
      StringBuilder stringBuilder = new StringBuilder(i);
      for (byte b = 0; b < i; b++) {
        char c = str.charAt(b);
        char c1 = c;
        if (!characterIsLegal(c))
          if ((this.mFlags & 0x1) != 0) {
            c1 = ' ';
          } else {
            c1 = '_';
          }  
        stringBuilder.append(c1);
      } 
      return stringBuilder.toString();
    }
    
    private String trimWhitespace(String param1String) {
      int k;
      byte b = 0;
      int i = param1String.length() - 1;
      int j = i;
      while (true) {
        k = j;
        if (b <= j) {
          k = j;
          if (isWhitespace(param1String.charAt(b))) {
            b++;
            continue;
          } 
        } 
        break;
      } 
      while (k >= b && isWhitespace(param1String.charAt(k)))
        k--; 
      if (b == 0 && k == i)
        return param1String; 
      return param1String.substring(b, k + 1);
    }
    
    private boolean isWhitespace(char param1Char) {
      if (param1Char != ' ')
        switch (param1Char) {
          default:
            return false;
          case '\t':
          case '\n':
          case '\013':
          case '\f':
          case '\r':
            break;
        }  
      return true;
    }
    
    private boolean characterIsLegal(char param1Char) {
      boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false, bool5 = false, bool6 = false, bool7 = false, bool8 = false, bool9 = false, bool10 = false;
      if (param1Char != '\000') {
        if (param1Char != ' ') {
          if (param1Char != '"') {
            if (param1Char != '<') {
              if (param1Char != '>') {
                switch (param1Char) {
                  default:
                    switch (param1Char) {
                      default:
                        if (param1Char < ' ' || param1Char >= '') {
                          bool5 = bool10;
                          if (param1Char >= '') {
                            bool5 = bool10;
                            if ((this.mFlags & 0x4) != 0)
                              return true; 
                          } 
                          return bool5;
                        } 
                        return true;
                      case '\'':
                        bool5 = bool1;
                        if ((this.mFlags & 0x10) != 0)
                          bool5 = true; 
                        return bool5;
                      case '&':
                        bool5 = bool2;
                        if ((this.mFlags & 0x80) != 0)
                          bool5 = true; 
                        return bool5;
                      case '%':
                        break;
                    } 
                    bool5 = bool3;
                    if ((this.mFlags & 0x100) != 0)
                      bool5 = true; 
                    return bool5;
                  case '\t':
                  case '\n':
                  case '\013':
                  case '\f':
                  case '\r':
                    break;
                } 
                bool5 = bool4;
                if ((this.mFlags & 0x2) != 0)
                  bool5 = true; 
                return bool5;
              } 
              if ((this.mFlags & 0x40) != 0)
                bool5 = true; 
              return bool5;
            } 
            bool5 = bool6;
            if ((0x20 & this.mFlags) != 0)
              bool5 = true; 
            return bool5;
          } 
          bool5 = bool7;
          if ((this.mFlags & 0x8) != 0)
            bool5 = true; 
          return bool5;
        } 
        bool5 = bool8;
        if ((this.mFlags & 0x1) != 0)
          bool5 = true; 
        return bool5;
      } 
      bool5 = bool9;
      if ((this.mFlags & 0x200) != 0)
        bool5 = true; 
      return bool5;
    }
  }
  
  public ValueSanitizer getUnregisteredParameterValueSanitizer() {
    return this.mUnregisteredParameterValueSanitizer;
  }
  
  public void setUnregisteredParameterValueSanitizer(ValueSanitizer paramValueSanitizer) {
    this.mUnregisteredParameterValueSanitizer = paramValueSanitizer;
  }
  
  private static final ValueSanitizer sAllIllegal = new IllegalCharacterValueSanitizer(0);
  
  private static final Pattern plusOrPercent;
  
  private static final ValueSanitizer sAllButNulAndAngleBracketsLegal;
  
  private static final ValueSanitizer sAllButNulLegal;
  
  private static final ValueSanitizer sAllButWhitespaceLegal;
  
  private static final ValueSanitizer sAmpAndSpaceLegal;
  
  private static final ValueSanitizer sAmpLegal;
  
  private static final ValueSanitizer sSpaceLegal;
  
  private static final ValueSanitizer sURLLegal;
  
  private static final ValueSanitizer sUrlAndSpaceLegal;
  
  private boolean mAllowUnregisteredParamaters;
  
  private boolean mPreferFirstRepeatedParameter;
  
  static {
    sAllButNulLegal = new IllegalCharacterValueSanitizer(1535);
    sAllButWhitespaceLegal = new IllegalCharacterValueSanitizer(1532);
    sURLLegal = new IllegalCharacterValueSanitizer(404);
    sUrlAndSpaceLegal = new IllegalCharacterValueSanitizer(405);
    sAmpLegal = new IllegalCharacterValueSanitizer(128);
    sAmpAndSpaceLegal = new IllegalCharacterValueSanitizer(129);
    sSpaceLegal = new IllegalCharacterValueSanitizer(1);
    sAllButNulAndAngleBracketsLegal = new IllegalCharacterValueSanitizer(1439);
    plusOrPercent = Pattern.compile("[+%]");
  }
  
  public static final ValueSanitizer getAllIllegal() {
    return sAllIllegal;
  }
  
  public static final ValueSanitizer getAllButNulLegal() {
    return sAllButNulLegal;
  }
  
  public static final ValueSanitizer getAllButWhitespaceLegal() {
    return sAllButWhitespaceLegal;
  }
  
  public static final ValueSanitizer getUrlLegal() {
    return sURLLegal;
  }
  
  public static final ValueSanitizer getUrlAndSpaceLegal() {
    return sUrlAndSpaceLegal;
  }
  
  public static final ValueSanitizer getAmpLegal() {
    return sAmpLegal;
  }
  
  public static final ValueSanitizer getAmpAndSpaceLegal() {
    return sAmpAndSpaceLegal;
  }
  
  public static final ValueSanitizer getSpaceLegal() {
    return sSpaceLegal;
  }
  
  public static final ValueSanitizer getAllButNulAndAngleBracketsLegal() {
    return sAllButNulAndAngleBracketsLegal;
  }
  
  public UrlQuerySanitizer(String paramString) {
    setAllowUnregisteredParamaters(true);
    parseUrl(paramString);
  }
  
  public void parseUrl(String paramString) {
    int i = paramString.indexOf('?');
    if (i >= 0) {
      paramString = paramString.substring(i + 1);
    } else {
      paramString = "";
    } 
    parseQuery(paramString);
  }
  
  public void parseQuery(String paramString) {
    clear();
    StringTokenizer stringTokenizer = new StringTokenizer(paramString, "&");
    while (stringTokenizer.hasMoreElements()) {
      String str = stringTokenizer.nextToken();
      if (str.length() > 0) {
        int i = str.indexOf('=');
        if (i < 0) {
          parseEntry(str, "");
          continue;
        } 
        paramString = str.substring(0, i);
        str = str.substring(i + 1);
        parseEntry(paramString, str);
      } 
    } 
  }
  
  public Set<String> getParameterSet() {
    return this.mEntries.keySet();
  }
  
  public List<ParameterValuePair> getParameterList() {
    return this.mEntriesList;
  }
  
  public boolean hasParameter(String paramString) {
    return this.mEntries.containsKey(paramString);
  }
  
  public String getValue(String paramString) {
    return this.mEntries.get(paramString);
  }
  
  public void registerParameter(String paramString, ValueSanitizer paramValueSanitizer) {
    if (paramValueSanitizer == null)
      this.mSanitizers.remove(paramString); 
    this.mSanitizers.put(paramString, paramValueSanitizer);
  }
  
  public void registerParameters(String[] paramArrayOfString, ValueSanitizer paramValueSanitizer) {
    int i = paramArrayOfString.length;
    for (byte b = 0; b < i; b++)
      this.mSanitizers.put(paramArrayOfString[b], paramValueSanitizer); 
  }
  
  public void setAllowUnregisteredParamaters(boolean paramBoolean) {
    this.mAllowUnregisteredParamaters = paramBoolean;
  }
  
  public boolean getAllowUnregisteredParamaters() {
    return this.mAllowUnregisteredParamaters;
  }
  
  public void setPreferFirstRepeatedParameter(boolean paramBoolean) {
    this.mPreferFirstRepeatedParameter = paramBoolean;
  }
  
  public boolean getPreferFirstRepeatedParameter() {
    return this.mPreferFirstRepeatedParameter;
  }
  
  protected void parseEntry(String paramString1, String paramString2) {
    paramString1 = unescape(paramString1);
    ValueSanitizer valueSanitizer = getEffectiveValueSanitizer(paramString1);
    if (valueSanitizer == null)
      return; 
    paramString2 = unescape(paramString2);
    paramString2 = valueSanitizer.sanitize(paramString2);
    addSanitizedEntry(paramString1, paramString2);
  }
  
  protected void addSanitizedEntry(String paramString1, String paramString2) {
    this.mEntriesList.add(new ParameterValuePair(paramString1, paramString2));
    if (this.mPreferFirstRepeatedParameter && this.mEntries.containsKey(paramString1))
      return; 
    this.mEntries.put(paramString1, paramString2);
  }
  
  public ValueSanitizer getValueSanitizer(String paramString) {
    return this.mSanitizers.get(paramString);
  }
  
  public ValueSanitizer getEffectiveValueSanitizer(String paramString) {
    ValueSanitizer valueSanitizer2 = getValueSanitizer(paramString);
    ValueSanitizer valueSanitizer1 = valueSanitizer2;
    if (valueSanitizer2 == null) {
      valueSanitizer1 = valueSanitizer2;
      if (this.mAllowUnregisteredParamaters)
        valueSanitizer1 = getUnregisteredParameterValueSanitizer(); 
    } 
    return valueSanitizer1;
  }
  
  public String unescape(String paramString) {
    Matcher matcher = plusOrPercent.matcher(paramString);
    if (!matcher.find())
      return paramString; 
    int i = matcher.start();
    int j = paramString.length();
    StringBuilder stringBuilder = new StringBuilder(j);
    stringBuilder.append(paramString.substring(0, i));
    for (; i < j; i = k + 1) {
      char c1;
      int k;
      char c = paramString.charAt(i);
      if (c == '+') {
        c1 = ' ';
        k = i;
      } else {
        k = i;
        c1 = c;
        if (c == '%') {
          k = i;
          c1 = c;
          if (i + 2 < j) {
            char c2 = paramString.charAt(i + 1);
            char c3 = paramString.charAt(i + 2);
            k = i;
            c1 = c;
            if (isHexDigit(c2)) {
              k = i;
              c1 = c;
              if (isHexDigit(c3)) {
                c1 = (char)(decodeHexDigit(c2) * 16 + decodeHexDigit(c3));
                k = i + 2;
              } 
            } 
          } 
        } 
      } 
      stringBuilder.append(c1);
    } 
    return stringBuilder.toString();
  }
  
  protected boolean isHexDigit(char paramChar) {
    boolean bool;
    if (decodeHexDigit(paramChar) >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected int decodeHexDigit(char paramChar) {
    if (paramChar >= '0' && paramChar <= '9')
      return paramChar - 48; 
    if (paramChar >= 'A' && paramChar <= 'F')
      return paramChar - 65 + 10; 
    if (paramChar >= 'a' && paramChar <= 'f')
      return paramChar - 97 + 10; 
    return -1;
  }
  
  protected void clear() {
    this.mEntries.clear();
    this.mEntriesList.clear();
  }
  
  public UrlQuerySanitizer() {}
}
