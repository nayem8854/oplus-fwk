package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.SubscriptionPlan;

public interface INetworkPolicyManager extends IInterface {
  void addUidPolicy(int paramInt1, int paramInt2) throws RemoteException;
  
  void factoryReset(String paramString) throws RemoteException;
  
  boolean getGameSpaceMode() throws RemoteException;
  
  NetworkPolicy[] getNetworkPolicies(String paramString) throws RemoteException;
  
  NetworkQuotaInfo getNetworkQuotaInfo(NetworkState paramNetworkState) throws RemoteException;
  
  boolean getRestrictBackground() throws RemoteException;
  
  int getRestrictBackgroundByCaller() throws RemoteException;
  
  SubscriptionPlan[] getSubscriptionPlans(int paramInt, String paramString) throws RemoteException;
  
  String getSubscriptionPlansOwner(int paramInt) throws RemoteException;
  
  int getUidPolicy(int paramInt) throws RemoteException;
  
  int[] getUidsWithPolicy(int paramInt) throws RemoteException;
  
  boolean isUidNetworkingBlocked(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void registerListener(INetworkPolicyListener paramINetworkPolicyListener) throws RemoteException;
  
  void removeUidPolicy(int paramInt1, int paramInt2) throws RemoteException;
  
  void setDeviceIdleMode(boolean paramBoolean) throws RemoteException;
  
  void setGameSpaceMode(boolean paramBoolean) throws RemoteException;
  
  void setNetworkPolicies(NetworkPolicy[] paramArrayOfNetworkPolicy) throws RemoteException;
  
  void setRestrictBackground(boolean paramBoolean) throws RemoteException;
  
  void setSubscriptionOverride(int paramInt1, int paramInt2, int paramInt3, long paramLong, String paramString) throws RemoteException;
  
  void setSubscriptionPlans(int paramInt, SubscriptionPlan[] paramArrayOfSubscriptionPlan, String paramString) throws RemoteException;
  
  void setUidPolicy(int paramInt1, int paramInt2) throws RemoteException;
  
  void setWifiMeteredOverride(String paramString, int paramInt) throws RemoteException;
  
  void snoozeLimit(NetworkTemplate paramNetworkTemplate) throws RemoteException;
  
  void unregisterListener(INetworkPolicyListener paramINetworkPolicyListener) throws RemoteException;
  
  class Default implements INetworkPolicyManager {
    public void setUidPolicy(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void addUidPolicy(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void removeUidPolicy(int param1Int1, int param1Int2) throws RemoteException {}
    
    public int getUidPolicy(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int[] getUidsWithPolicy(int param1Int) throws RemoteException {
      return null;
    }
    
    public void registerListener(INetworkPolicyListener param1INetworkPolicyListener) throws RemoteException {}
    
    public void unregisterListener(INetworkPolicyListener param1INetworkPolicyListener) throws RemoteException {}
    
    public void setNetworkPolicies(NetworkPolicy[] param1ArrayOfNetworkPolicy) throws RemoteException {}
    
    public NetworkPolicy[] getNetworkPolicies(String param1String) throws RemoteException {
      return null;
    }
    
    public void snoozeLimit(NetworkTemplate param1NetworkTemplate) throws RemoteException {}
    
    public void setRestrictBackground(boolean param1Boolean) throws RemoteException {}
    
    public boolean getRestrictBackground() throws RemoteException {
      return false;
    }
    
    public int getRestrictBackgroundByCaller() throws RemoteException {
      return 0;
    }
    
    public void setDeviceIdleMode(boolean param1Boolean) throws RemoteException {}
    
    public void setWifiMeteredOverride(String param1String, int param1Int) throws RemoteException {}
    
    public NetworkQuotaInfo getNetworkQuotaInfo(NetworkState param1NetworkState) throws RemoteException {
      return null;
    }
    
    public SubscriptionPlan[] getSubscriptionPlans(int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public void setSubscriptionPlans(int param1Int, SubscriptionPlan[] param1ArrayOfSubscriptionPlan, String param1String) throws RemoteException {}
    
    public String getSubscriptionPlansOwner(int param1Int) throws RemoteException {
      return null;
    }
    
    public void setSubscriptionOverride(int param1Int1, int param1Int2, int param1Int3, long param1Long, String param1String) throws RemoteException {}
    
    public void factoryReset(String param1String) throws RemoteException {}
    
    public boolean isUidNetworkingBlocked(int param1Int, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean getGameSpaceMode() throws RemoteException {
      return false;
    }
    
    public void setGameSpaceMode(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INetworkPolicyManager {
    private static final String DESCRIPTOR = "android.net.INetworkPolicyManager";
    
    static final int TRANSACTION_addUidPolicy = 2;
    
    static final int TRANSACTION_factoryReset = 21;
    
    static final int TRANSACTION_getGameSpaceMode = 23;
    
    static final int TRANSACTION_getNetworkPolicies = 9;
    
    static final int TRANSACTION_getNetworkQuotaInfo = 16;
    
    static final int TRANSACTION_getRestrictBackground = 12;
    
    static final int TRANSACTION_getRestrictBackgroundByCaller = 13;
    
    static final int TRANSACTION_getSubscriptionPlans = 17;
    
    static final int TRANSACTION_getSubscriptionPlansOwner = 19;
    
    static final int TRANSACTION_getUidPolicy = 4;
    
    static final int TRANSACTION_getUidsWithPolicy = 5;
    
    static final int TRANSACTION_isUidNetworkingBlocked = 22;
    
    static final int TRANSACTION_registerListener = 6;
    
    static final int TRANSACTION_removeUidPolicy = 3;
    
    static final int TRANSACTION_setDeviceIdleMode = 14;
    
    static final int TRANSACTION_setGameSpaceMode = 24;
    
    static final int TRANSACTION_setNetworkPolicies = 8;
    
    static final int TRANSACTION_setRestrictBackground = 11;
    
    static final int TRANSACTION_setSubscriptionOverride = 20;
    
    static final int TRANSACTION_setSubscriptionPlans = 18;
    
    static final int TRANSACTION_setUidPolicy = 1;
    
    static final int TRANSACTION_setWifiMeteredOverride = 15;
    
    static final int TRANSACTION_snoozeLimit = 10;
    
    static final int TRANSACTION_unregisterListener = 7;
    
    public Stub() {
      attachInterface(this, "android.net.INetworkPolicyManager");
    }
    
    public static INetworkPolicyManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.net.INetworkPolicyManager");
      if (iInterface != null && iInterface instanceof INetworkPolicyManager)
        return (INetworkPolicyManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 24:
          return "setGameSpaceMode";
        case 23:
          return "getGameSpaceMode";
        case 22:
          return "isUidNetworkingBlocked";
        case 21:
          return "factoryReset";
        case 20:
          return "setSubscriptionOverride";
        case 19:
          return "getSubscriptionPlansOwner";
        case 18:
          return "setSubscriptionPlans";
        case 17:
          return "getSubscriptionPlans";
        case 16:
          return "getNetworkQuotaInfo";
        case 15:
          return "setWifiMeteredOverride";
        case 14:
          return "setDeviceIdleMode";
        case 13:
          return "getRestrictBackgroundByCaller";
        case 12:
          return "getRestrictBackground";
        case 11:
          return "setRestrictBackground";
        case 10:
          return "snoozeLimit";
        case 9:
          return "getNetworkPolicies";
        case 8:
          return "setNetworkPolicies";
        case 7:
          return "unregisterListener";
        case 6:
          return "registerListener";
        case 5:
          return "getUidsWithPolicy";
        case 4:
          return "getUidPolicy";
        case 3:
          return "removeUidPolicy";
        case 2:
          return "addUidPolicy";
        case 1:
          break;
      } 
      return "setUidPolicy";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        String str2;
        SubscriptionPlan[] arrayOfSubscriptionPlan1;
        NetworkQuotaInfo networkQuotaInfo;
        String str1;
        NetworkPolicy[] arrayOfNetworkPolicy;
        INetworkPolicyListener iNetworkPolicyListener;
        int arrayOfInt[], m;
        long l;
        SubscriptionPlan[] arrayOfSubscriptionPlan2;
        String str3;
        boolean bool4 = false, bool5 = false, bool6 = false, bool7 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 24:
            param1Parcel1.enforceInterface("android.net.INetworkPolicyManager");
            bool5 = bool7;
            if (param1Parcel1.readInt() != 0)
              bool5 = true; 
            setGameSpaceMode(bool5);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            param1Parcel1.enforceInterface("android.net.INetworkPolicyManager");
            bool3 = getGameSpaceMode();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 22:
            param1Parcel1.enforceInterface("android.net.INetworkPolicyManager");
            k = param1Parcel1.readInt();
            bool5 = bool4;
            if (param1Parcel1.readInt() != 0)
              bool5 = true; 
            bool2 = isUidNetworkingBlocked(k, bool5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 21:
            param1Parcel1.enforceInterface("android.net.INetworkPolicyManager");
            str2 = param1Parcel1.readString();
            factoryReset(str2);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            str2.enforceInterface("android.net.INetworkPolicyManager");
            param1Int2 = str2.readInt();
            j = str2.readInt();
            m = str2.readInt();
            l = str2.readLong();
            str2 = str2.readString();
            setSubscriptionOverride(param1Int2, j, m, l, str2);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            str2.enforceInterface("android.net.INetworkPolicyManager");
            j = str2.readInt();
            str2 = getSubscriptionPlansOwner(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 18:
            str2.enforceInterface("android.net.INetworkPolicyManager");
            j = str2.readInt();
            arrayOfSubscriptionPlan2 = str2.<SubscriptionPlan>createTypedArray(SubscriptionPlan.CREATOR);
            str2 = str2.readString();
            setSubscriptionPlans(j, arrayOfSubscriptionPlan2, str2);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            str2.enforceInterface("android.net.INetworkPolicyManager");
            j = str2.readInt();
            str2 = str2.readString();
            arrayOfSubscriptionPlan1 = getSubscriptionPlans(j, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray(arrayOfSubscriptionPlan1, 1);
            return true;
          case 16:
            arrayOfSubscriptionPlan1.enforceInterface("android.net.INetworkPolicyManager");
            if (arrayOfSubscriptionPlan1.readInt() != 0) {
              NetworkState networkState = NetworkState.CREATOR.createFromParcel((Parcel)arrayOfSubscriptionPlan1);
            } else {
              arrayOfSubscriptionPlan1 = null;
            } 
            networkQuotaInfo = getNetworkQuotaInfo((NetworkState)arrayOfSubscriptionPlan1);
            param1Parcel2.writeNoException();
            if (networkQuotaInfo != null) {
              param1Parcel2.writeInt(1);
              networkQuotaInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 15:
            networkQuotaInfo.enforceInterface("android.net.INetworkPolicyManager");
            str3 = networkQuotaInfo.readString();
            j = networkQuotaInfo.readInt();
            setWifiMeteredOverride(str3, j);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            networkQuotaInfo.enforceInterface("android.net.INetworkPolicyManager");
            if (networkQuotaInfo.readInt() != 0)
              bool5 = true; 
            setDeviceIdleMode(bool5);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            networkQuotaInfo.enforceInterface("android.net.INetworkPolicyManager");
            j = getRestrictBackgroundByCaller();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 12:
            networkQuotaInfo.enforceInterface("android.net.INetworkPolicyManager");
            bool1 = getRestrictBackground();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 11:
            networkQuotaInfo.enforceInterface("android.net.INetworkPolicyManager");
            bool5 = bool6;
            if (networkQuotaInfo.readInt() != 0)
              bool5 = true; 
            setRestrictBackground(bool5);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            networkQuotaInfo.enforceInterface("android.net.INetworkPolicyManager");
            if (networkQuotaInfo.readInt() != 0) {
              NetworkTemplate networkTemplate = NetworkTemplate.CREATOR.createFromParcel((Parcel)networkQuotaInfo);
            } else {
              networkQuotaInfo = null;
            } 
            snoozeLimit((NetworkTemplate)networkQuotaInfo);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            networkQuotaInfo.enforceInterface("android.net.INetworkPolicyManager");
            str1 = networkQuotaInfo.readString();
            arrayOfNetworkPolicy = getNetworkPolicies(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray(arrayOfNetworkPolicy, 1);
            return true;
          case 8:
            arrayOfNetworkPolicy.enforceInterface("android.net.INetworkPolicyManager");
            arrayOfNetworkPolicy = arrayOfNetworkPolicy.<NetworkPolicy>createTypedArray(NetworkPolicy.CREATOR);
            setNetworkPolicies(arrayOfNetworkPolicy);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            arrayOfNetworkPolicy.enforceInterface("android.net.INetworkPolicyManager");
            iNetworkPolicyListener = INetworkPolicyListener.Stub.asInterface(arrayOfNetworkPolicy.readStrongBinder());
            unregisterListener(iNetworkPolicyListener);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            iNetworkPolicyListener.enforceInterface("android.net.INetworkPolicyManager");
            iNetworkPolicyListener = INetworkPolicyListener.Stub.asInterface(iNetworkPolicyListener.readStrongBinder());
            registerListener(iNetworkPolicyListener);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            iNetworkPolicyListener.enforceInterface("android.net.INetworkPolicyManager");
            i = iNetworkPolicyListener.readInt();
            arrayOfInt = getUidsWithPolicy(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt);
            return true;
          case 4:
            arrayOfInt.enforceInterface("android.net.INetworkPolicyManager");
            i = arrayOfInt.readInt();
            i = getUidPolicy(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 3:
            arrayOfInt.enforceInterface("android.net.INetworkPolicyManager");
            param1Int2 = arrayOfInt.readInt();
            i = arrayOfInt.readInt();
            removeUidPolicy(param1Int2, i);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            arrayOfInt.enforceInterface("android.net.INetworkPolicyManager");
            i = arrayOfInt.readInt();
            param1Int2 = arrayOfInt.readInt();
            addUidPolicy(i, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        arrayOfInt.enforceInterface("android.net.INetworkPolicyManager");
        int i = arrayOfInt.readInt();
        param1Int2 = arrayOfInt.readInt();
        setUidPolicy(i, param1Int2);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.net.INetworkPolicyManager");
      return true;
    }
    
    private static class Proxy implements INetworkPolicyManager {
      public static INetworkPolicyManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.net.INetworkPolicyManager";
      }
      
      public void setUidPolicy(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && INetworkPolicyManager.Stub.getDefaultImpl() != null) {
            INetworkPolicyManager.Stub.getDefaultImpl().setUidPolicy(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addUidPolicy(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && INetworkPolicyManager.Stub.getDefaultImpl() != null) {
            INetworkPolicyManager.Stub.getDefaultImpl().addUidPolicy(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeUidPolicy(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && INetworkPolicyManager.Stub.getDefaultImpl() != null) {
            INetworkPolicyManager.Stub.getDefaultImpl().removeUidPolicy(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUidPolicy(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && INetworkPolicyManager.Stub.getDefaultImpl() != null) {
            param2Int = INetworkPolicyManager.Stub.getDefaultImpl().getUidPolicy(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getUidsWithPolicy(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && INetworkPolicyManager.Stub.getDefaultImpl() != null)
            return INetworkPolicyManager.Stub.getDefaultImpl().getUidsWithPolicy(param2Int); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerListener(INetworkPolicyListener param2INetworkPolicyListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          if (param2INetworkPolicyListener != null) {
            iBinder = param2INetworkPolicyListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && INetworkPolicyManager.Stub.getDefaultImpl() != null) {
            INetworkPolicyManager.Stub.getDefaultImpl().registerListener(param2INetworkPolicyListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterListener(INetworkPolicyListener param2INetworkPolicyListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          if (param2INetworkPolicyListener != null) {
            iBinder = param2INetworkPolicyListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && INetworkPolicyManager.Stub.getDefaultImpl() != null) {
            INetworkPolicyManager.Stub.getDefaultImpl().unregisterListener(param2INetworkPolicyListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setNetworkPolicies(NetworkPolicy[] param2ArrayOfNetworkPolicy) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          parcel1.writeTypedArray(param2ArrayOfNetworkPolicy, 0);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && INetworkPolicyManager.Stub.getDefaultImpl() != null) {
            INetworkPolicyManager.Stub.getDefaultImpl().setNetworkPolicies(param2ArrayOfNetworkPolicy);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkPolicy[] getNetworkPolicies(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && INetworkPolicyManager.Stub.getDefaultImpl() != null)
            return INetworkPolicyManager.Stub.getDefaultImpl().getNetworkPolicies(param2String); 
          parcel2.readException();
          return parcel2.<NetworkPolicy>createTypedArray(NetworkPolicy.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void snoozeLimit(NetworkTemplate param2NetworkTemplate) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          if (param2NetworkTemplate != null) {
            parcel1.writeInt(1);
            param2NetworkTemplate.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && INetworkPolicyManager.Stub.getDefaultImpl() != null) {
            INetworkPolicyManager.Stub.getDefaultImpl().snoozeLimit(param2NetworkTemplate);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setRestrictBackground(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool1 && INetworkPolicyManager.Stub.getDefaultImpl() != null) {
            INetworkPolicyManager.Stub.getDefaultImpl().setRestrictBackground(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getRestrictBackground() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(12, parcel1, parcel2, 0);
          if (!bool2 && INetworkPolicyManager.Stub.getDefaultImpl() != null) {
            bool1 = INetworkPolicyManager.Stub.getDefaultImpl().getRestrictBackground();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getRestrictBackgroundByCaller() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && INetworkPolicyManager.Stub.getDefaultImpl() != null)
            return INetworkPolicyManager.Stub.getDefaultImpl().getRestrictBackgroundByCaller(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDeviceIdleMode(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool1 && INetworkPolicyManager.Stub.getDefaultImpl() != null) {
            INetworkPolicyManager.Stub.getDefaultImpl().setDeviceIdleMode(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setWifiMeteredOverride(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && INetworkPolicyManager.Stub.getDefaultImpl() != null) {
            INetworkPolicyManager.Stub.getDefaultImpl().setWifiMeteredOverride(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkQuotaInfo getNetworkQuotaInfo(NetworkState param2NetworkState) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          if (param2NetworkState != null) {
            parcel1.writeInt(1);
            param2NetworkState.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && INetworkPolicyManager.Stub.getDefaultImpl() != null)
            return INetworkPolicyManager.Stub.getDefaultImpl().getNetworkQuotaInfo(param2NetworkState); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            NetworkQuotaInfo networkQuotaInfo = NetworkQuotaInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2NetworkState = null;
          } 
          return (NetworkQuotaInfo)param2NetworkState;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public SubscriptionPlan[] getSubscriptionPlans(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && INetworkPolicyManager.Stub.getDefaultImpl() != null)
            return INetworkPolicyManager.Stub.getDefaultImpl().getSubscriptionPlans(param2Int, param2String); 
          parcel2.readException();
          return parcel2.<SubscriptionPlan>createTypedArray(SubscriptionPlan.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSubscriptionPlans(int param2Int, SubscriptionPlan[] param2ArrayOfSubscriptionPlan, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          parcel1.writeInt(param2Int);
          parcel1.writeTypedArray(param2ArrayOfSubscriptionPlan, 0);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && INetworkPolicyManager.Stub.getDefaultImpl() != null) {
            INetworkPolicyManager.Stub.getDefaultImpl().setSubscriptionPlans(param2Int, param2ArrayOfSubscriptionPlan, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSubscriptionPlansOwner(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && INetworkPolicyManager.Stub.getDefaultImpl() != null)
            return INetworkPolicyManager.Stub.getDefaultImpl().getSubscriptionPlansOwner(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSubscriptionOverride(int param2Int1, int param2Int2, int param2Int3, long param2Long, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeInt(param2Int2);
              try {
                parcel1.writeInt(param2Int3);
                try {
                  parcel1.writeLong(param2Long);
                  try {
                    parcel1.writeString(param2String);
                    boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
                    if (!bool && INetworkPolicyManager.Stub.getDefaultImpl() != null) {
                      INetworkPolicyManager.Stub.getDefaultImpl().setSubscriptionOverride(param2Int1, param2Int2, param2Int3, param2Long, param2String);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public void factoryReset(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && INetworkPolicyManager.Stub.getDefaultImpl() != null) {
            INetworkPolicyManager.Stub.getDefaultImpl().factoryReset(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUidNetworkingBlocked(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && INetworkPolicyManager.Stub.getDefaultImpl() != null) {
            param2Boolean = INetworkPolicyManager.Stub.getDefaultImpl().isUidNetworkingBlocked(param2Int, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getGameSpaceMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(23, parcel1, parcel2, 0);
          if (!bool2 && INetworkPolicyManager.Stub.getDefaultImpl() != null) {
            bool1 = INetworkPolicyManager.Stub.getDefaultImpl().getGameSpaceMode();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setGameSpaceMode(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool1 && INetworkPolicyManager.Stub.getDefaultImpl() != null) {
            INetworkPolicyManager.Stub.getDefaultImpl().setGameSpaceMode(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INetworkPolicyManager param1INetworkPolicyManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INetworkPolicyManager != null) {
          Proxy.sDefaultImpl = param1INetworkPolicyManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INetworkPolicyManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
