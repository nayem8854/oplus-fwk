package android.net;

import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.util.Log;
import android.util.Pair;
import com.android.net.module.util.Inet4AddressUtils;
import java.io.FileDescriptor;
import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import java.util.TreeSet;

public class NetworkUtils {
  public static boolean protectFromVpn(FileDescriptor paramFileDescriptor) {
    return protectFromVpn(paramFileDescriptor.getInt$());
  }
  
  @Deprecated
  public static InetAddress intToInetAddress(int paramInt) {
    return Inet4AddressUtils.intToInet4AddressHTL(paramInt);
  }
  
  @Deprecated
  public static int inetAddressToInt(Inet4Address paramInet4Address) throws IllegalArgumentException {
    return Inet4AddressUtils.inet4AddressToIntHTL(paramInet4Address);
  }
  
  @Deprecated
  public static int prefixLengthToNetmaskInt(int paramInt) throws IllegalArgumentException {
    return Inet4AddressUtils.prefixLengthToV4NetmaskIntHTL(paramInt);
  }
  
  public static int netmaskIntToPrefixLength(int paramInt) {
    return Integer.bitCount(paramInt);
  }
  
  @Deprecated
  public static int netmaskToPrefixLength(Inet4Address paramInet4Address) {
    return Inet4AddressUtils.netmaskToPrefixLength(paramInet4Address);
  }
  
  @Deprecated
  public static InetAddress numericToInetAddress(String paramString) throws IllegalArgumentException {
    return InetAddress.parseNumericAddress(paramString);
  }
  
  public static void maskRawAddress(byte[] paramArrayOfbyte, int paramInt) {
    if (paramInt >= 0 && paramInt <= paramArrayOfbyte.length * 8) {
      int i = paramInt / 8;
      paramInt = (byte)(255 << 8 - paramInt % 8);
      if (i < paramArrayOfbyte.length)
        paramArrayOfbyte[i] = (byte)(paramArrayOfbyte[i] & paramInt); 
      paramInt = i + 1;
      for (; paramInt < paramArrayOfbyte.length; paramInt++)
        paramArrayOfbyte[paramInt] = 0; 
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("IP address with ");
    stringBuilder.append(paramArrayOfbyte.length);
    stringBuilder.append(" bytes has invalid prefix length ");
    stringBuilder.append(paramInt);
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public static InetAddress getNetworkPart(InetAddress paramInetAddress, int paramInt) {
    byte[] arrayOfByte = paramInetAddress.getAddress();
    maskRawAddress(arrayOfByte, paramInt);
    try {
      return InetAddress.getByAddress(arrayOfByte);
    } catch (UnknownHostException unknownHostException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getNetworkPart error - ");
      stringBuilder.append(unknownHostException.toString());
      throw new RuntimeException(stringBuilder.toString());
    } 
  }
  
  public static int getImplicitNetmask(Inet4Address paramInet4Address) {
    return Inet4AddressUtils.getImplicitNetmask(paramInet4Address);
  }
  
  public static Pair<InetAddress, Integer> parseIpAndMask(String paramString) {
    numberFormatException = null;
    InetAddress inetAddress = null;
    int i = -1;
    int j = i, k = i, m = i, n = i;
    try {
      String[] arrayOfString = paramString.split("/", 2);
      j = i;
      k = i;
      m = i;
      n = i;
      i = Integer.parseInt(arrayOfString[1]);
      j = i;
      k = i;
      m = i;
      n = i;
      InetAddress inetAddress1 = InetAddress.parseNumericAddress(arrayOfString[0]);
    } catch (NullPointerException nullPointerException) {
      i = n;
    } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
      i = m;
    } catch (NumberFormatException numberFormatException) {
      i = k;
    } catch (IllegalArgumentException illegalArgumentException) {
      illegalArgumentException = numberFormatException;
      i = j;
    } 
    if (illegalArgumentException != null && i != -1)
      return new Pair(illegalArgumentException, Integer.valueOf(i)); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid IP address and mask ");
    stringBuilder.append(paramString);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static InetAddress hexToInet6Address(String paramString) throws IllegalArgumentException {
    try {
      Locale locale = Locale.US;
      String str1 = paramString.substring(0, 4), str2 = paramString.substring(4, 8);
      String str3 = paramString.substring(8, 12), str4 = paramString.substring(12, 16);
      String str5 = paramString.substring(16, 20), str6 = paramString.substring(20, 24);
      String str7 = paramString.substring(24, 28), str8 = paramString.substring(28, 32);
      return numericToInetAddress(String.format(locale, "%s:%s:%s:%s:%s:%s:%s:%s", new Object[] { str1, str2, str3, str4, str5, str6, str7, str8 }));
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("error in hexToInet6Address(");
      stringBuilder.append(paramString);
      stringBuilder.append("): ");
      stringBuilder.append(exception);
      Log.e("NetworkUtils", stringBuilder.toString());
      throw new IllegalArgumentException(exception);
    } 
  }
  
  public static String[] makeStrings(Collection<InetAddress> paramCollection) {
    String[] arrayOfString = new String[paramCollection.size()];
    byte b = 0;
    for (InetAddress inetAddress : paramCollection) {
      arrayOfString[b] = inetAddress.getHostAddress();
      b++;
    } 
    return arrayOfString;
  }
  
  public static String trimV4AddrZeros(String paramString) {
    if (paramString == null)
      return null; 
    String[] arrayOfString = paramString.split("\\.");
    if (arrayOfString.length != 4)
      return paramString; 
    StringBuilder stringBuilder = new StringBuilder(16);
    for (byte b = 0; b < 4;) {
      try {
        if (arrayOfString[b].length() > 3)
          return paramString; 
        stringBuilder.append(Integer.parseInt(arrayOfString[b]));
        if (b < 3)
          stringBuilder.append('.'); 
        b++;
      } catch (NumberFormatException numberFormatException) {
        return paramString;
      } 
    } 
    paramString = numberFormatException.toString();
    return paramString;
  }
  
  private static TreeSet<IpPrefix> deduplicatePrefixSet(TreeSet<IpPrefix> paramTreeSet) {
    TreeSet<IpPrefix> treeSet = new TreeSet(paramTreeSet.comparator());
    label14: for (IpPrefix ipPrefix : paramTreeSet) {
      for (IpPrefix ipPrefix1 : treeSet) {
        if (ipPrefix1.containsPrefix(ipPrefix))
          continue label14; 
      } 
      treeSet.add(ipPrefix);
    } 
    return treeSet;
  }
  
  public static long routedIPv4AddressCount(TreeSet<IpPrefix> paramTreeSet) {
    long l = 0L;
    for (IpPrefix ipPrefix : deduplicatePrefixSet(paramTreeSet)) {
      if (!ipPrefix.isIPv4())
        Log.wtf("NetworkUtils", "Non-IPv4 prefix in routedIPv4AddressCount"); 
      int i = ipPrefix.getPrefixLength();
      l += 1L << 32 - i;
    } 
    return l;
  }
  
  public static BigInteger routedIPv6AddressCount(TreeSet<IpPrefix> paramTreeSet) {
    BigInteger bigInteger2 = BigInteger.ZERO;
    BigInteger bigInteger1;
    for (Iterator<IpPrefix> iterator = deduplicatePrefixSet(paramTreeSet).iterator(); iterator.hasNext(); ) {
      IpPrefix ipPrefix = iterator.next();
      if (!ipPrefix.isIPv6())
        Log.wtf("NetworkUtils", "Non-IPv6 prefix in routedIPv6AddressCount"); 
      int i = ipPrefix.getPrefixLength();
      bigInteger1 = bigInteger1.add(BigInteger.ONE.shiftLeft(128 - i));
    } 
    return bigInteger1;
  }
  
  private static final int[] ADDRESS_FAMILIES = new int[] { OsConstants.AF_INET, OsConstants.AF_INET6 };
  
  private static final String TAG = "NetworkUtils";
  
  public static boolean isWeaklyValidatedHostname(String paramString) {
    if (!paramString.matches("^[a-zA-Z0-9_.-]+$"))
      return false; 
    for (int i : ADDRESS_FAMILIES) {
      if (Os.inet_pton(i, paramString) != null)
        return false; 
    } 
    return true;
  }
  
  public static native void attachDropAllBPFFilter(FileDescriptor paramFileDescriptor) throws SocketException;
  
  public static native boolean bindProcessToNetwork(int paramInt);
  
  @Deprecated
  public static native boolean bindProcessToNetworkForHostResolution(int paramInt);
  
  public static native int bindSocketToNetwork(int paramInt1, int paramInt2);
  
  public static native void detachBPFFilter(FileDescriptor paramFileDescriptor) throws SocketException;
  
  public static native int getBoundNetworkForProcess();
  
  public static native Network getDnsNetwork() throws ErrnoException;
  
  public static native TcpRepairWindow getTcpRepairWindow(FileDescriptor paramFileDescriptor) throws ErrnoException;
  
  public static native boolean protectFromVpn(int paramInt);
  
  public static native boolean queryUserAccess(int paramInt1, int paramInt2);
  
  public static native void resNetworkCancel(FileDescriptor paramFileDescriptor);
  
  public static native FileDescriptor resNetworkQuery(int paramInt1, String paramString, int paramInt2, int paramInt3, int paramInt4) throws ErrnoException;
  
  public static native DnsResolver.DnsResponse resNetworkResult(FileDescriptor paramFileDescriptor) throws ErrnoException;
  
  public static native FileDescriptor resNetworkSend(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3) throws ErrnoException;
  
  public static native void setAllowNetworkingForProcess(boolean paramBoolean);
}
