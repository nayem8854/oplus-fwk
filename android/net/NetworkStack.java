package android.net;

import android.annotation.SystemApi;
import android.content.Context;
import android.os.IBinder;
import android.os.ServiceManager;
import java.util.ArrayList;
import java.util.Arrays;

@SystemApi
public class NetworkStack {
  @SystemApi
  public static final String PERMISSION_MAINLINE_NETWORK_STACK = "android.permission.MAINLINE_NETWORK_STACK";
  
  private static volatile IBinder sMockService;
  
  @SystemApi
  public static IBinder getService() {
    IBinder iBinder = sMockService;
    if (iBinder != null)
      return iBinder; 
    return ServiceManager.getService("network_stack");
  }
  
  public static void setServiceForTest(IBinder paramIBinder) {
    sMockService = paramIBinder;
  }
  
  public static void checkNetworkStackPermission(Context paramContext) {
    checkNetworkStackPermissionOr(paramContext, new String[0]);
  }
  
  public static void checkNetworkStackPermissionOr(Context paramContext, String... paramVarArgs) {
    ArrayList<String> arrayList = new ArrayList(Arrays.asList((Object[])paramVarArgs));
    arrayList.add("android.permission.NETWORK_STACK");
    arrayList.add("android.permission.MAINLINE_NETWORK_STACK");
    enforceAnyPermissionOf(paramContext, arrayList.<String>toArray(new String[0]));
  }
  
  private static void enforceAnyPermissionOf(Context paramContext, String... paramVarArgs) {
    if (checkAnyPermissionOf(paramContext, paramVarArgs))
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Requires one of the following permissions: ");
    stringBuilder.append(String.join(", ", (CharSequence[])paramVarArgs));
    stringBuilder.append(".");
    throw new SecurityException(stringBuilder.toString());
  }
  
  private static boolean checkAnyPermissionOf(Context paramContext, String... paramVarArgs) {
    int i;
    byte b;
    for (i = paramVarArgs.length, b = 0; b < i; ) {
      String str = paramVarArgs[b];
      if (paramContext.checkCallingOrSelfPermission(str) == 0)
        return true; 
      b++;
    } 
    return false;
  }
}
