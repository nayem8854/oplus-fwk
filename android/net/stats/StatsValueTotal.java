package android.net.stats;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashMap;
import java.util.Map;

public class StatsValueTotal implements Parcelable {
  public StatsValueTotal(HashMap<Long, StatsValue> paramHashMap) {
    this.mStatsMap = paramHashMap;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    Bundle bundle = new Bundle();
    for (Map.Entry<Long, StatsValue> entry : this.mStatsMap.entrySet())
      bundle.putParcelable(String.valueOf(entry.getKey()), (Parcelable)entry.getValue()); 
    paramParcel.writeBundle(bundle);
  }
  
  public static final Parcelable.Creator<StatsValueTotal> CREATOR = new Parcelable.Creator<StatsValueTotal>() {
      public StatsValueTotal createFromParcel(Parcel param1Parcel) {
        Bundle bundle = param1Parcel.readBundle();
        HashMap<Object, Object> hashMap = new HashMap<>();
        for (String str : bundle.keySet())
          hashMap.put(Long.valueOf(Long.parseLong(str)), bundle.<StatsValue>getParcelable(str)); 
        return new StatsValueTotal((HashMap)hashMap);
      }
      
      public StatsValueTotal[] newArray(int param1Int) {
        return new StatsValueTotal[param1Int];
      }
    };
  
  public HashMap<Long, StatsValue> mStatsMap;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("StatsValueTotal{mStatsMap=");
    stringBuilder.append(this.mStatsMap);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
}
