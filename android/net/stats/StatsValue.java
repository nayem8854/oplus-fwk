package android.net.stats;

import android.os.Parcel;
import android.os.Parcelable;

public class StatsValue implements Parcelable {
  public StatsValue(long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5, long paramLong6, long paramLong7, long paramLong8) {
    this.mRxBytes = paramLong1;
    this.mRxPackets = paramLong2;
    this.mTxBytes = paramLong3;
    this.mTxPackets = paramLong4;
    this.mTcpRxPackets = paramLong5;
    this.mTcpTxPackets = paramLong6;
    this.mTransportRxBytes = paramLong7;
    this.mTransportTxBytes = paramLong8;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mRxBytes);
    paramParcel.writeLong(this.mRxPackets);
    paramParcel.writeLong(this.mTxBytes);
    paramParcel.writeLong(this.mTxPackets);
    paramParcel.writeLong(this.mTcpRxPackets);
    paramParcel.writeLong(this.mTcpTxPackets);
    paramParcel.writeLong(this.mTransportRxBytes);
    paramParcel.writeLong(this.mTransportTxBytes);
  }
  
  public static final Parcelable.Creator<StatsValue> CREATOR = new Parcelable.Creator<StatsValue>() {
      public StatsValue createFromParcel(Parcel param1Parcel) {
        long l1 = param1Parcel.readLong();
        long l2 = param1Parcel.readLong();
        long l3 = param1Parcel.readLong();
        long l4 = param1Parcel.readLong();
        long l5 = param1Parcel.readLong();
        long l6 = param1Parcel.readLong();
        long l7 = param1Parcel.readLong();
        long l8 = param1Parcel.readLong();
        return new StatsValue(l1, l2, l3, l4, l5, l6, l7, l8);
      }
      
      public StatsValue[] newArray(int param1Int) {
        return new StatsValue[param1Int];
      }
    };
  
  public long mRxBytes;
  
  public long mRxPackets;
  
  public long mTcpRxPackets;
  
  public long mTcpTxPackets;
  
  public long mTransportRxBytes;
  
  public long mTransportTxBytes;
  
  public long mTxBytes;
  
  public long mTxPackets;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("StatsValue{mRxBytes=");
    stringBuilder.append(this.mRxBytes);
    stringBuilder.append(", mRxPackets=");
    stringBuilder.append(this.mRxPackets);
    stringBuilder.append(", mTxBytes=");
    stringBuilder.append(this.mTxBytes);
    stringBuilder.append(", mTxPackets=");
    stringBuilder.append(this.mTxPackets);
    stringBuilder.append(", mTcpRxPackets=");
    stringBuilder.append(this.mTcpRxPackets);
    stringBuilder.append(", mTcpTxPackets=");
    stringBuilder.append(this.mTcpTxPackets);
    stringBuilder.append(", mTransportRxBytes=");
    stringBuilder.append(this.mTransportRxBytes);
    stringBuilder.append(", mTransportTxBytes=");
    stringBuilder.append(this.mTransportTxBytes);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    if (paramObject == null)
      return false; 
    if (paramObject instanceof StatsValue) {
      boolean bool1 = bool;
      if (this.mRxBytes == ((StatsValue)paramObject).mRxBytes) {
        bool1 = bool;
        if (this.mTxBytes == ((StatsValue)paramObject).mTxBytes) {
          bool1 = bool;
          if (this.mRxPackets == ((StatsValue)paramObject).mRxPackets) {
            bool1 = bool;
            if (this.mTxPackets == ((StatsValue)paramObject).mTxPackets) {
              bool1 = bool;
              if (this.mTcpRxPackets == ((StatsValue)paramObject).mTcpRxPackets) {
                bool1 = bool;
                if (this.mTcpTxPackets == ((StatsValue)paramObject).mTcpTxPackets) {
                  bool1 = bool;
                  if (this.mTransportRxBytes == ((StatsValue)paramObject).mTransportRxBytes) {
                    bool1 = bool;
                    if (this.mTransportTxBytes == ((StatsValue)paramObject).mTransportTxBytes)
                      bool1 = true; 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
      return bool1;
    } 
    return false;
  }
}
