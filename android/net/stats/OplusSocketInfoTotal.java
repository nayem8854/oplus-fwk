package android.net.stats;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashMap;
import java.util.Map;

public class OplusSocketInfoTotal implements Parcelable {
  public OplusSocketInfoTotal(HashMap<Long, OplusSocketInfo> paramHashMap) {
    this.mSocketInfoMap = paramHashMap;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    Bundle bundle = new Bundle();
    for (Map.Entry<Long, OplusSocketInfo> entry : this.mSocketInfoMap.entrySet())
      bundle.putParcelable(String.valueOf(entry.getKey()), (Parcelable)entry.getValue()); 
    paramParcel.writeBundle(bundle);
  }
  
  public static final Parcelable.Creator<OplusSocketInfoTotal> CREATOR = new Parcelable.Creator<OplusSocketInfoTotal>() {
      public OplusSocketInfoTotal createFromParcel(Parcel param1Parcel) {
        Bundle bundle = param1Parcel.readBundle();
        HashMap<Object, Object> hashMap = new HashMap<>();
        for (String str : bundle.keySet())
          hashMap.put(Long.valueOf(Long.parseLong(str)), bundle.<OplusSocketInfo>getParcelable(str)); 
        return new OplusSocketInfoTotal((HashMap)hashMap);
      }
      
      public OplusSocketInfoTotal[] newArray(int param1Int) {
        return new OplusSocketInfoTotal[param1Int];
      }
    };
  
  public HashMap<Long, OplusSocketInfo> mSocketInfoMap;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("OplusSocketInfoTotal{mSocketInfoMap=");
    stringBuilder.append(this.mSocketInfoMap);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
}
