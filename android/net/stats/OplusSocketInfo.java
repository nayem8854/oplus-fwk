package android.net.stats;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

public class OplusSocketInfo implements Parcelable {
  public OplusSocketInfo(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int[] paramArrayOfint1, int[] paramArrayOfint2, int paramInt7, int paramInt8) {
    this.mUid = paramInt1;
    this.mIfIndex = paramInt2;
    this.mIsIpv4 = paramInt3;
    this.mProtocol = paramInt4;
    this.mLocalIpv4 = paramInt5;
    this.mRemoteIpv4 = paramInt6;
    this.mLocalIpv6 = paramArrayOfint1;
    this.mRemoteIpv6 = paramArrayOfint2;
    this.mLocalPort = paramInt7;
    this.mRemotePort = paramInt8;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mUid);
    paramParcel.writeInt(this.mIfIndex);
    paramParcel.writeInt(this.mIsIpv4);
    paramParcel.writeInt(this.mProtocol);
    paramParcel.writeInt(this.mLocalIpv4);
    paramParcel.writeInt(this.mRemoteIpv4);
    int[] arrayOfInt = this.mLocalIpv6;
    boolean bool = false;
    if (arrayOfInt != null) {
      paramInt = arrayOfInt.length;
    } else {
      paramInt = 0;
    } 
    paramParcel.writeInt(paramInt);
    byte b;
    for (b = 0; b < paramInt; b++)
      paramParcel.writeInt(this.mLocalIpv6[b]); 
    arrayOfInt = this.mRemoteIpv6;
    paramInt = bool;
    if (arrayOfInt != null)
      paramInt = arrayOfInt.length; 
    paramParcel.writeInt(paramInt);
    for (b = 0; b < paramInt; b++)
      paramParcel.writeInt(this.mRemoteIpv6[b]); 
    paramParcel.writeInt(this.mLocalPort);
    paramParcel.writeInt(this.mRemotePort);
  }
  
  public static final Parcelable.Creator<OplusSocketInfo> CREATOR = new Parcelable.Creator<OplusSocketInfo>() {
      public OplusSocketInfo createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        int k = param1Parcel.readInt();
        int m = param1Parcel.readInt();
        int n = param1Parcel.readInt();
        int i1 = param1Parcel.readInt();
        int i2 = param1Parcel.readInt();
        int[] arrayOfInt1 = new int[i2];
        int i3;
        for (i3 = 0; i3 < i2; i3++)
          arrayOfInt1[i3] = param1Parcel.readInt(); 
        i2 = param1Parcel.readInt();
        int[] arrayOfInt2 = new int[i2];
        for (i3 = 0; i3 < i2; i3++)
          arrayOfInt2[i3] = param1Parcel.readInt(); 
        i2 = param1Parcel.readInt();
        i3 = param1Parcel.readInt();
        return new OplusSocketInfo(i, j, k, m, n, i1, arrayOfInt1, arrayOfInt2, i2, i3);
      }
      
      public OplusSocketInfo[] newArray(int param1Int) {
        return new OplusSocketInfo[param1Int];
      }
    };
  
  public int mIfIndex;
  
  public int mIsIpv4;
  
  public int mLocalIpv4;
  
  public int[] mLocalIpv6;
  
  public int mLocalPort;
  
  public int mProtocol;
  
  public int mRemoteIpv4;
  
  public int[] mRemoteIpv6;
  
  public int mRemotePort;
  
  public int mUid;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("OplusSocketInfo{mUid=");
    stringBuilder.append(this.mUid);
    stringBuilder.append(", mIfIndex=");
    stringBuilder.append(this.mIfIndex);
    stringBuilder.append(", mIsIpv4=");
    stringBuilder.append(this.mIsIpv4);
    stringBuilder.append(", mProtocol=");
    stringBuilder.append(this.mProtocol);
    stringBuilder.append(", mLocalIpv4=");
    stringBuilder.append(this.mLocalIpv4);
    stringBuilder.append(", mRemoteIpv4=");
    stringBuilder.append(this.mRemoteIpv4);
    stringBuilder.append(", mLocalIpv6=");
    int[] arrayOfInt = this.mLocalIpv6;
    stringBuilder.append(Arrays.toString(arrayOfInt));
    stringBuilder.append(", mRemoteIpv6=");
    arrayOfInt = this.mRemoteIpv6;
    stringBuilder.append(Arrays.toString(arrayOfInt));
    stringBuilder.append(", mLocalPort=");
    stringBuilder.append(this.mLocalPort);
    stringBuilder.append(", mRemotePort=");
    stringBuilder.append(this.mRemotePort);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
}
