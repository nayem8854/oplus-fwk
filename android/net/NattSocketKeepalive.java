package android.net;

import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.util.Log;
import java.io.FileDescriptor;
import java.net.InetAddress;
import java.util.concurrent.Executor;

public final class NattSocketKeepalive extends SocketKeepalive {
  public static final int NATT_PORT = 4500;
  
  private final InetAddress mDestination;
  
  private final int mResourceId;
  
  private final InetAddress mSource;
  
  NattSocketKeepalive(IConnectivityManager paramIConnectivityManager, Network paramNetwork, ParcelFileDescriptor paramParcelFileDescriptor, int paramInt, InetAddress paramInetAddress1, InetAddress paramInetAddress2, Executor paramExecutor, SocketKeepalive.Callback paramCallback) {
    super(paramIConnectivityManager, paramNetwork, paramParcelFileDescriptor, paramExecutor, paramCallback);
    this.mSource = paramInetAddress1;
    this.mDestination = paramInetAddress2;
    this.mResourceId = paramInt;
  }
  
  void startImpl(int paramInt) {
    this.mExecutor.execute(new _$$Lambda$NattSocketKeepalive$7nsE_7bVdhw33oN4gmk8WVi_r9U(this, paramInt));
  }
  
  void stopImpl() {
    this.mExecutor.execute(new _$$Lambda$NattSocketKeepalive$60CcdfQ34rdXme76td_j4bbtPHU(this));
  }
}
