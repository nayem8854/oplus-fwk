package android.net;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Process;
import android.text.TextUtils;
import android.util.proto.ProtoOutputStream;
import java.util.Objects;
import java.util.Set;

public class NetworkRequest implements Parcelable {
  class Type extends Enum<Type> {
    private static final Type[] $VALUES;
    
    public static final Type BACKGROUND_REQUEST;
    
    public static final Type LISTEN = new Type("LISTEN", 1);
    
    public static Type[] values() {
      return (Type[])$VALUES.clone();
    }
    
    public static Type valueOf(String param1String) {
      return Enum.<Type>valueOf(Type.class, param1String);
    }
    
    private Type(NetworkRequest this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static final Type NONE = new Type("NONE", 0);
    
    public static final Type REQUEST;
    
    public static final Type TRACK_DEFAULT = new Type("TRACK_DEFAULT", 2);
    
    static {
      REQUEST = new Type("REQUEST", 3);
      Type type = new Type("BACKGROUND_REQUEST", 4);
      $VALUES = new Type[] { NONE, LISTEN, TRACK_DEFAULT, REQUEST, type };
    }
  }
  
  public NetworkRequest(NetworkCapabilities paramNetworkCapabilities, int paramInt1, int paramInt2, Type paramType) {
    if (paramNetworkCapabilities != null) {
      this.requestId = paramInt2;
      this.networkCapabilities = paramNetworkCapabilities;
      this.legacyType = paramInt1;
      this.type = paramType;
      return;
    } 
    throw null;
  }
  
  public NetworkRequest(NetworkRequest paramNetworkRequest) {
    this.networkCapabilities = new NetworkCapabilities(paramNetworkRequest.networkCapabilities);
    this.requestId = paramNetworkRequest.requestId;
    this.legacyType = paramNetworkRequest.legacyType;
    this.type = paramNetworkRequest.type;
  }
  
  class Builder {
    private final NetworkCapabilities mNetworkCapabilities;
    
    public Builder() {
      NetworkCapabilities networkCapabilities = new NetworkCapabilities();
      networkCapabilities.setSingleUid(Process.myUid());
    }
    
    public NetworkRequest build() {
      NetworkCapabilities networkCapabilities = new NetworkCapabilities(this.mNetworkCapabilities);
      networkCapabilities.maybeMarkCapabilitiesRestricted();
      return new NetworkRequest(networkCapabilities, -1, 0, NetworkRequest.Type.NONE);
    }
    
    public Builder addCapability(int param1Int) {
      this.mNetworkCapabilities.addCapability(param1Int);
      return this;
    }
    
    public Builder removeCapability(int param1Int) {
      this.mNetworkCapabilities.removeCapability(param1Int);
      return this;
    }
    
    public Builder setCapabilities(NetworkCapabilities param1NetworkCapabilities) {
      this.mNetworkCapabilities.set(param1NetworkCapabilities);
      return this;
    }
    
    public Builder setUids(Set<UidRange> param1Set) {
      this.mNetworkCapabilities.setUids(param1Set);
      return this;
    }
    
    public Builder addUnwantedCapability(int param1Int) {
      this.mNetworkCapabilities.addUnwantedCapability(param1Int);
      return this;
    }
    
    public Builder clearCapabilities() {
      this.mNetworkCapabilities.clearAll();
      return this;
    }
    
    public Builder addTransportType(int param1Int) {
      this.mNetworkCapabilities.addTransportType(param1Int);
      return this;
    }
    
    public Builder removeTransportType(int param1Int) {
      this.mNetworkCapabilities.removeTransportType(param1Int);
      return this;
    }
    
    public Builder setLinkUpstreamBandwidthKbps(int param1Int) {
      this.mNetworkCapabilities.setLinkUpstreamBandwidthKbps(param1Int);
      return this;
    }
    
    public Builder setLinkDownstreamBandwidthKbps(int param1Int) {
      this.mNetworkCapabilities.setLinkDownstreamBandwidthKbps(param1Int);
      return this;
    }
    
    @Deprecated
    public Builder setNetworkSpecifier(String param1String) {
      try {
        int i = Integer.parseInt(param1String);
        TelephonyNetworkSpecifier.Builder builder = new TelephonyNetworkSpecifier.Builder();
        this();
        TelephonyNetworkSpecifier telephonyNetworkSpecifier = builder.setSubscriptionId(i).build();
        return setNetworkSpecifier(telephonyNetworkSpecifier);
      } catch (NumberFormatException numberFormatException) {
        StringNetworkSpecifier stringNetworkSpecifier;
        if (TextUtils.isEmpty(param1String)) {
          param1String = null;
        } else {
          stringNetworkSpecifier = new StringNetworkSpecifier(param1String);
        } 
        return setNetworkSpecifier(stringNetworkSpecifier);
      } 
    }
    
    public Builder setNetworkSpecifier(NetworkSpecifier param1NetworkSpecifier) {
      MatchAllNetworkSpecifier.checkNotMatchAllNetworkSpecifier(param1NetworkSpecifier);
      this.mNetworkCapabilities.setNetworkSpecifier(param1NetworkSpecifier);
      return this;
    }
    
    @SystemApi
    public Builder setSignalStrength(int param1Int) {
      this.mNetworkCapabilities.setSignalStrength(param1Int);
      return this;
    }
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.networkCapabilities.writeToParcel(paramParcel, paramInt);
    paramParcel.writeInt(this.legacyType);
    paramParcel.writeInt(this.requestId);
    paramParcel.writeString(this.type.name());
  }
  
  public static final Parcelable.Creator<NetworkRequest> CREATOR = new Parcelable.Creator<NetworkRequest>() {
      public NetworkRequest createFromParcel(Parcel param1Parcel) {
        NetworkCapabilities networkCapabilities = NetworkCapabilities.CREATOR.createFromParcel(param1Parcel);
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        NetworkRequest.Type type = NetworkRequest.Type.valueOf(param1Parcel.readString());
        return new NetworkRequest(networkCapabilities, i, j, type);
      }
      
      public NetworkRequest[] newArray(int param1Int) {
        return new NetworkRequest[param1Int];
      }
    };
  
  public final int legacyType;
  
  public final NetworkCapabilities networkCapabilities;
  
  public final int requestId;
  
  public final Type type;
  
  public boolean isListen() {
    boolean bool;
    if (this.type == Type.LISTEN) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isRequest() {
    return (isForegroundRequest() || isBackgroundRequest());
  }
  
  public boolean isForegroundRequest() {
    return (this.type == Type.TRACK_DEFAULT || this.type == Type.REQUEST);
  }
  
  public boolean isBackgroundRequest() {
    boolean bool;
    if (this.type == Type.BACKGROUND_REQUEST) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean hasCapability(int paramInt) {
    return this.networkCapabilities.hasCapability(paramInt);
  }
  
  public boolean hasUnwantedCapability(int paramInt) {
    return this.networkCapabilities.hasUnwantedCapability(paramInt);
  }
  
  public boolean canBeSatisfiedBy(NetworkCapabilities paramNetworkCapabilities) {
    return this.networkCapabilities.satisfiedByNetworkCapabilities(paramNetworkCapabilities);
  }
  
  public boolean hasTransport(int paramInt) {
    return this.networkCapabilities.hasTransport(paramInt);
  }
  
  public NetworkSpecifier getNetworkSpecifier() {
    return this.networkCapabilities.getNetworkSpecifier();
  }
  
  @SystemApi
  public int getRequestorUid() {
    return this.networkCapabilities.getRequestorUid();
  }
  
  @SystemApi
  public String getRequestorPackageName() {
    return this.networkCapabilities.getRequestorPackageName();
  }
  
  public String toString() {
    String str;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("NetworkRequest [ ");
    stringBuilder.append(this.type);
    stringBuilder.append(" id=");
    stringBuilder.append(this.requestId);
    if (this.legacyType != -1) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(", legacyType=");
      stringBuilder1.append(this.legacyType);
      str = stringBuilder1.toString();
    } else {
      str = "";
    } 
    stringBuilder.append(str);
    stringBuilder.append(", ");
    NetworkCapabilities networkCapabilities = this.networkCapabilities;
    stringBuilder.append(networkCapabilities.toString());
    stringBuilder.append(" ]");
    return stringBuilder.toString();
  }
  
  private int typeToProtoEnum(Type paramType) {
    int i = null.$SwitchMap$android$net$NetworkRequest$Type[paramType.ordinal()];
    if (i != 1) {
      if (i != 2) {
        if (i != 3) {
          if (i != 4) {
            if (i != 5)
              return 0; 
            return 5;
          } 
          return 4;
        } 
        return 3;
      } 
      return 2;
    } 
    return 1;
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    paramLong = paramProtoOutputStream.start(paramLong);
    paramProtoOutputStream.write(1159641169921L, typeToProtoEnum(this.type));
    paramProtoOutputStream.write(1120986464258L, this.requestId);
    paramProtoOutputStream.write(1120986464259L, this.legacyType);
    this.networkCapabilities.dumpDebug(paramProtoOutputStream, 1146756268036L);
    paramProtoOutputStream.end(paramLong);
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof NetworkRequest;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    if (((NetworkRequest)paramObject).legacyType == this.legacyType && ((NetworkRequest)paramObject).requestId == this.requestId && ((NetworkRequest)paramObject).type == this.type) {
      NetworkCapabilities networkCapabilities = ((NetworkRequest)paramObject).networkCapabilities;
      paramObject = this.networkCapabilities;
      if (Objects.equals(networkCapabilities, paramObject))
        bool1 = true; 
    } 
    return bool1;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.requestId), Integer.valueOf(this.legacyType), this.networkCapabilities, this.type });
  }
}
