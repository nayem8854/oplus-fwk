package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import com.android.net.module.util.InetAddressUtils;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public final class DhcpResults implements Parcelable {
  public String vendorInfo;
  
  public String serverHostName;
  
  public Inet4Address serverAddress;
  
  public int mtu;
  
  public int leaseDuration;
  
  public LinkAddress ipAddress;
  
  public InetAddress gateway;
  
  public String domains;
  
  public final ArrayList<InetAddress> dnsServers = new ArrayList<>();
  
  public String captivePortalApiUrl;
  
  private static final String TAG = "DhcpResults";
  
  public StaticIpConfiguration toStaticIpConfiguration() {
    StaticIpConfiguration.Builder builder2 = new StaticIpConfiguration.Builder();
    LinkAddress linkAddress = this.ipAddress;
    StaticIpConfiguration.Builder builder4 = builder2.setIpAddress(linkAddress);
    InetAddress inetAddress = this.gateway;
    builder4 = builder4.setGateway(inetAddress);
    ArrayList<InetAddress> arrayList = this.dnsServers;
    StaticIpConfiguration.Builder builder1 = builder4.setDnsServers(arrayList);
    String str = this.domains;
    StaticIpConfiguration.Builder builder3 = builder1.setDomains(str);
    return builder3.build();
  }
  
  public DhcpResults(StaticIpConfiguration paramStaticIpConfiguration) {
    if (paramStaticIpConfiguration != null) {
      this.ipAddress = paramStaticIpConfiguration.getIpAddress();
      this.gateway = paramStaticIpConfiguration.getGateway();
      this.dnsServers.addAll(paramStaticIpConfiguration.getDnsServers());
      this.domains = paramStaticIpConfiguration.getDomains();
    } 
  }
  
  public DhcpResults(DhcpResults paramDhcpResults) {
    this(staticIpConfiguration);
    StaticIpConfiguration staticIpConfiguration;
    if (paramDhcpResults != null) {
      this.serverAddress = paramDhcpResults.serverAddress;
      this.vendorInfo = paramDhcpResults.vendorInfo;
      this.leaseDuration = paramDhcpResults.leaseDuration;
      this.mtu = paramDhcpResults.mtu;
      this.serverHostName = paramDhcpResults.serverHostName;
      this.captivePortalApiUrl = paramDhcpResults.captivePortalApiUrl;
    } 
  }
  
  public List<RouteInfo> getRoutes(String paramString) {
    return toStaticIpConfiguration().getRoutes(paramString);
  }
  
  public boolean hasMeteredHint() {
    String str = this.vendorInfo;
    if (str != null)
      return str.contains("ANDROID_METERED"); 
    return false;
  }
  
  public void clear() {
    this.ipAddress = null;
    this.gateway = null;
    this.dnsServers.clear();
    this.domains = null;
    this.serverAddress = null;
    this.vendorInfo = null;
    this.leaseDuration = 0;
    this.mtu = 0;
    this.serverHostName = null;
    this.captivePortalApiUrl = null;
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer(super.toString());
    stringBuffer.append(" DHCP server ");
    stringBuffer.append(this.serverAddress);
    stringBuffer.append(" Vendor info ");
    stringBuffer.append(this.vendorInfo);
    stringBuffer.append(" lease ");
    stringBuffer.append(this.leaseDuration);
    stringBuffer.append(" seconds");
    if (this.mtu != 0) {
      stringBuffer.append(" MTU ");
      stringBuffer.append(this.mtu);
    } 
    stringBuffer.append(" Servername ");
    stringBuffer.append(this.serverHostName);
    if (this.captivePortalApiUrl != null) {
      stringBuffer.append(" CaptivePortalApiUrl ");
      stringBuffer.append(this.captivePortalApiUrl);
    } 
    return stringBuffer.toString();
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof DhcpResults))
      return false; 
    paramObject = paramObject;
    if (toStaticIpConfiguration().equals(paramObject.toStaticIpConfiguration())) {
      Inet4Address inet4Address1 = this.serverAddress, inet4Address2 = ((DhcpResults)paramObject).serverAddress;
      if (Objects.equals(inet4Address1, inet4Address2)) {
        String str1 = this.vendorInfo, str2 = ((DhcpResults)paramObject).vendorInfo;
        if (Objects.equals(str1, str2)) {
          str1 = this.serverHostName;
          str2 = ((DhcpResults)paramObject).serverHostName;
          if (Objects.equals(str1, str2) && this.leaseDuration == ((DhcpResults)paramObject).leaseDuration && this.mtu == ((DhcpResults)paramObject).mtu) {
            str1 = this.captivePortalApiUrl;
            paramObject = ((DhcpResults)paramObject).captivePortalApiUrl;
            if (Objects.equals(str1, paramObject))
              return null; 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public static final Parcelable.Creator<DhcpResults> CREATOR = new Parcelable.Creator<DhcpResults>() {
      public DhcpResults createFromParcel(Parcel param1Parcel) {
        return DhcpResults.readFromParcel(param1Parcel);
      }
      
      public DhcpResults[] newArray(int param1Int) {
        return new DhcpResults[param1Int];
      }
    };
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    toStaticIpConfiguration().writeToParcel(paramParcel, paramInt);
    paramParcel.writeInt(this.leaseDuration);
    paramParcel.writeInt(this.mtu);
    InetAddressUtils.parcelInetAddress(paramParcel, this.serverAddress, paramInt);
    paramParcel.writeString(this.vendorInfo);
    paramParcel.writeString(this.serverHostName);
    paramParcel.writeString(this.captivePortalApiUrl);
  }
  
  public int describeContents() {
    return 0;
  }
  
  private static DhcpResults readFromParcel(Parcel paramParcel) {
    StaticIpConfiguration staticIpConfiguration = StaticIpConfiguration.CREATOR.createFromParcel(paramParcel);
    DhcpResults dhcpResults = new DhcpResults(staticIpConfiguration);
    dhcpResults.leaseDuration = paramParcel.readInt();
    dhcpResults.mtu = paramParcel.readInt();
    dhcpResults.serverAddress = (Inet4Address)InetAddressUtils.unparcelInetAddress(paramParcel);
    dhcpResults.vendorInfo = paramParcel.readString();
    dhcpResults.serverHostName = paramParcel.readString();
    dhcpResults.captivePortalApiUrl = paramParcel.readString();
    return dhcpResults;
  }
  
  public boolean setIpAddress(String paramString, int paramInt) {
    try {
      Inet4Address inet4Address = (Inet4Address)InetAddresses.parseNumericAddress(paramString);
      LinkAddress linkAddress = new LinkAddress();
      this(inet4Address, paramInt);
      this.ipAddress = linkAddress;
      return false;
    } catch (IllegalArgumentException|ClassCastException illegalArgumentException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setIpAddress failed with addrString ");
      stringBuilder.append(paramString);
      stringBuilder.append("/");
      stringBuilder.append(paramInt);
      Log.e("DhcpResults", stringBuilder.toString());
      return true;
    } 
  }
  
  public boolean setGateway(String paramString) {
    try {
      this.gateway = InetAddresses.parseNumericAddress(paramString);
      return false;
    } catch (IllegalArgumentException illegalArgumentException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setGateway failed with addrString ");
      stringBuilder.append(paramString);
      Log.e("DhcpResults", stringBuilder.toString());
      return true;
    } 
  }
  
  public boolean addDns(String paramString) {
    if (!TextUtils.isEmpty(paramString))
      try {
        this.dnsServers.add(InetAddresses.parseNumericAddress(paramString));
      } catch (IllegalArgumentException illegalArgumentException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("addDns failed with addrString ");
        stringBuilder.append(paramString);
        Log.e("DhcpResults", stringBuilder.toString());
        return true;
      }  
    return false;
  }
  
  public LinkAddress getIpAddress() {
    return this.ipAddress;
  }
  
  public void setIpAddress(LinkAddress paramLinkAddress) {
    this.ipAddress = paramLinkAddress;
  }
  
  public InetAddress getGateway() {
    return this.gateway;
  }
  
  public void setGateway(InetAddress paramInetAddress) {
    this.gateway = paramInetAddress;
  }
  
  public List<InetAddress> getDnsServers() {
    return this.dnsServers;
  }
  
  public void addDnsServer(InetAddress paramInetAddress) {
    this.dnsServers.add(paramInetAddress);
  }
  
  public String getDomains() {
    return this.domains;
  }
  
  public void setDomains(String paramString) {
    this.domains = paramString;
  }
  
  public Inet4Address getServerAddress() {
    return this.serverAddress;
  }
  
  public void setServerAddress(Inet4Address paramInet4Address) {
    this.serverAddress = paramInet4Address;
  }
  
  public int getLeaseDuration() {
    return this.leaseDuration;
  }
  
  public void setLeaseDuration(int paramInt) {
    this.leaseDuration = paramInt;
  }
  
  public String getVendorInfo() {
    return this.vendorInfo;
  }
  
  public void setVendorInfo(String paramString) {
    this.vendorInfo = paramString;
  }
  
  public int getMtu() {
    return this.mtu;
  }
  
  public void setMtu(int paramInt) {
    this.mtu = paramInt;
  }
  
  public String getCaptivePortalApiUrl() {
    return this.captivePortalApiUrl;
  }
  
  public void setCaptivePortalApiUrl(String paramString) {
    this.captivePortalApiUrl = paramString;
  }
  
  public DhcpResults() {}
}
