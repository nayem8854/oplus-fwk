package android.net;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;
import java.util.regex.Pattern;

@SystemApi
public class WifiKey implements Parcelable {
  private static final Pattern BSSID_PATTERN;
  
  public static final Parcelable.Creator<WifiKey> CREATOR;
  
  private static final Pattern SSID_PATTERN = Pattern.compile("(\".*\")|(0x[\\p{XDigit}]+)", 32);
  
  public final String bssid;
  
  public final String ssid;
  
  static {
    BSSID_PATTERN = Pattern.compile("([\\p{XDigit}]{2}:){5}[\\p{XDigit}]{2}");
    CREATOR = new Parcelable.Creator<WifiKey>() {
        public WifiKey createFromParcel(Parcel param1Parcel) {
          return new WifiKey(param1Parcel);
        }
        
        public WifiKey[] newArray(int param1Int) {
          return new WifiKey[param1Int];
        }
      };
  }
  
  public WifiKey(String paramString1, String paramString2) {
    StringBuilder stringBuilder1;
    if (paramString1 != null && SSID_PATTERN.matcher(paramString1).matches()) {
      if (paramString2 != null && BSSID_PATTERN.matcher(paramString2).matches()) {
        this.ssid = paramString1;
        this.bssid = paramString2;
        return;
      } 
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Invalid bssid: ");
      stringBuilder1.append(paramString2);
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("Invalid ssid: ");
    stringBuilder2.append((String)stringBuilder1);
    throw new IllegalArgumentException(stringBuilder2.toString());
  }
  
  private WifiKey(Parcel paramParcel) {
    this.ssid = paramParcel.readString();
    this.bssid = paramParcel.readString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.ssid);
    paramParcel.writeString(this.bssid);
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (!Objects.equals(this.ssid, ((WifiKey)paramObject).ssid) || !Objects.equals(this.bssid, ((WifiKey)paramObject).bssid))
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.ssid, this.bssid });
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("WifiKey[SSID=");
    stringBuilder.append(this.ssid);
    stringBuilder.append(",BSSID=");
    stringBuilder.append(this.bssid);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
}
