package android.permission;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.os.UserHandle;
import android.util.ArrayMap;
import android.util.Log;
import com.android.internal.infra.AndroidFuture;
import com.android.internal.util.CollectionUtils;
import com.android.internal.util.Preconditions;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.IntConsumer;

@SystemApi
public abstract class PermissionControllerService extends Service {
  private static final String LOG_TAG = PermissionControllerService.class.getSimpleName();
  
  public static final String SERVICE_INTERFACE = "android.permission.PermissionControllerService";
  
  @Deprecated
  public void onRestoreRuntimePermissionsBackup(UserHandle paramUserHandle, InputStream paramInputStream, Runnable paramRunnable) {}
  
  public void onStageAndApplyRuntimePermissionsBackup(UserHandle paramUserHandle, InputStream paramInputStream, Runnable paramRunnable) {
    onRestoreRuntimePermissionsBackup(paramUserHandle, paramInputStream, paramRunnable);
  }
  
  @Deprecated
  public void onRestoreDelayedRuntimePermissionsBackup(String paramString, UserHandle paramUserHandle, Consumer<Boolean> paramConsumer) {}
  
  public void onApplyStagedRuntimePermissionBackup(String paramString, UserHandle paramUserHandle, Consumer<Boolean> paramConsumer) {
    onRestoreDelayedRuntimePermissionsBackup(paramString, paramUserHandle, paramConsumer);
  }
  
  public void onUpdateUserSensitivePermissionFlags(int paramInt, Executor paramExecutor, Runnable paramRunnable) {
    throw new AbstractMethodError("Must be overridden in implementing class");
  }
  
  public void onUpdateUserSensitivePermissionFlags(int paramInt, Runnable paramRunnable) {
    onUpdateUserSensitivePermissionFlags(paramInt, getMainExecutor(), paramRunnable);
  }
  
  public void onOneTimePermissionSessionTimeout(String paramString) {
    throw new AbstractMethodError("Must be overridden in implementing class");
  }
  
  public final IBinder onBind(Intent paramIntent) {
    return new IPermissionController.Stub() {
        final PermissionControllerService this$0;
        
        public void revokeRuntimePermissions(Bundle param1Bundle, boolean param1Boolean, int param1Int, String param1String, AndroidFuture param1AndroidFuture) {
          Preconditions.checkNotNull(param1Bundle, "bundleizedRequest");
          Preconditions.checkNotNull(param1String);
          Preconditions.checkNotNull(param1AndroidFuture);
          ArrayMap<String, ArrayList<String>> arrayMap = new ArrayMap();
          for (String str : param1Bundle.keySet()) {
            Preconditions.checkNotNull(str);
            ArrayList<String> arrayList = param1Bundle.getStringArrayList(str);
            Preconditions.checkCollectionElementsNotNull(arrayList, "permissions");
            arrayMap.put(str, arrayList);
          } 
          enforceSomePermissionsGrantedToCaller(new String[] { "android.permission.REVOKE_RUNTIME_PERMISSIONS" });
          try {
            PackageManager packageManager = PermissionControllerService.this.getPackageManager();
            boolean bool = false;
            PackageInfo packageInfo = packageManager.getPackageInfo(param1String, 0);
            if (getCallingUid() == packageInfo.applicationInfo.uid)
              bool = true; 
            Preconditions.checkArgument(bool);
            PermissionControllerService.this.onRevokeRuntimePermissions((Map)arrayMap, param1Boolean, param1Int, param1String, new _$$Lambda$PermissionControllerService$1$d7FwwlMbNXDw5Sg__Gg51Tk0Dx8(param1AndroidFuture));
            return;
          } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
            throw new RuntimeException(nameNotFoundException);
          } 
        }
        
        private void enforceSomePermissionsGrantedToCaller(String... param1VarArgs) {
          int i;
          byte b;
          for (i = param1VarArgs.length, b = 0; b < i; ) {
            String str = param1VarArgs[b];
            if (PermissionControllerService.this.checkCallingPermission(str) == 0)
              return; 
            b++;
          } 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("At lest one of the following permissions is required: ");
          stringBuilder.append(Arrays.toString((Object[])param1VarArgs));
          throw new SecurityException(stringBuilder.toString());
        }
        
        public void getRuntimePermissionBackup(UserHandle param1UserHandle, ParcelFileDescriptor param1ParcelFileDescriptor) {
          Preconditions.checkNotNull(param1UserHandle);
          Preconditions.checkNotNull(param1ParcelFileDescriptor);
          enforceSomePermissionsGrantedToCaller(new String[] { "android.permission.GET_RUNTIME_PERMISSIONS" });
          try {
            ParcelFileDescriptor.AutoCloseOutputStream autoCloseOutputStream = new ParcelFileDescriptor.AutoCloseOutputStream();
            this(param1ParcelFileDescriptor);
            try {
              CountDownLatch countDownLatch = new CountDownLatch();
              this(1);
              PermissionControllerService permissionControllerService = PermissionControllerService.this;
              Objects.requireNonNull(countDownLatch);
              _$$Lambda$5k6tNlswoNAjCdgttrkQIe8VHVs _$$Lambda$5k6tNlswoNAjCdgttrkQIe8VHVs = new _$$Lambda$5k6tNlswoNAjCdgttrkQIe8VHVs();
              this(countDownLatch);
              permissionControllerService.onGetRuntimePermissionsBackup(param1UserHandle, autoCloseOutputStream, _$$Lambda$5k6tNlswoNAjCdgttrkQIe8VHVs);
              countDownLatch.await();
            } finally {
              try {
                autoCloseOutputStream.close();
              } finally {
                param1ParcelFileDescriptor = null;
              } 
            } 
          } catch (IOException iOException) {
            Log.e(PermissionControllerService.LOG_TAG, "Could not open pipe to write backup to", iOException);
          } catch (InterruptedException interruptedException) {
            Log.e(PermissionControllerService.LOG_TAG, "getRuntimePermissionBackup timed out", interruptedException);
          } 
        }
        
        public void stageAndApplyRuntimePermissionsBackup(UserHandle param1UserHandle, ParcelFileDescriptor param1ParcelFileDescriptor) {
          Preconditions.checkNotNull(param1UserHandle);
          Preconditions.checkNotNull(param1ParcelFileDescriptor);
          enforceSomePermissionsGrantedToCaller(new String[] { "android.permission.GRANT_RUNTIME_PERMISSIONS", "android.permission.RESTORE_RUNTIME_PERMISSIONS" });
          try {
            ParcelFileDescriptor.AutoCloseInputStream autoCloseInputStream = new ParcelFileDescriptor.AutoCloseInputStream();
            this(param1ParcelFileDescriptor);
            try {
              CountDownLatch countDownLatch = new CountDownLatch();
              this(1);
              PermissionControllerService permissionControllerService = PermissionControllerService.this;
              Objects.requireNonNull(countDownLatch);
              _$$Lambda$5k6tNlswoNAjCdgttrkQIe8VHVs _$$Lambda$5k6tNlswoNAjCdgttrkQIe8VHVs = new _$$Lambda$5k6tNlswoNAjCdgttrkQIe8VHVs();
              this(countDownLatch);
              permissionControllerService.onStageAndApplyRuntimePermissionsBackup(param1UserHandle, autoCloseInputStream, _$$Lambda$5k6tNlswoNAjCdgttrkQIe8VHVs);
              countDownLatch.await();
            } finally {
              try {
                autoCloseInputStream.close();
              } finally {
                param1ParcelFileDescriptor = null;
              } 
            } 
          } catch (IOException iOException) {
            Log.e(PermissionControllerService.LOG_TAG, "Could not open pipe to read backup from", iOException);
          } catch (InterruptedException interruptedException) {
            Log.e(PermissionControllerService.LOG_TAG, "restoreRuntimePermissionBackup timed out", interruptedException);
          } 
        }
        
        public void applyStagedRuntimePermissionBackup(String param1String, UserHandle param1UserHandle, AndroidFuture param1AndroidFuture) {
          Preconditions.checkNotNull(param1String);
          Preconditions.checkNotNull(param1UserHandle);
          Preconditions.checkNotNull(param1AndroidFuture);
          enforceSomePermissionsGrantedToCaller(new String[] { "android.permission.GRANT_RUNTIME_PERMISSIONS", "android.permission.RESTORE_RUNTIME_PERMISSIONS" });
          PermissionControllerService permissionControllerService = PermissionControllerService.this;
          Objects.requireNonNull(param1AndroidFuture);
          permissionControllerService.onApplyStagedRuntimePermissionBackup(param1String, param1UserHandle, new _$$Lambda$PYgLd3P_k0utHHEMLxjUyz_Fj7c(param1AndroidFuture));
        }
        
        public void getAppPermissions(String param1String, AndroidFuture param1AndroidFuture) {
          Preconditions.checkNotNull(param1String, "packageName");
          Preconditions.checkNotNull(param1AndroidFuture, "callback");
          enforceSomePermissionsGrantedToCaller(new String[] { "android.permission.GET_RUNTIME_PERMISSIONS" });
          PermissionControllerService permissionControllerService = PermissionControllerService.this;
          Objects.requireNonNull(param1AndroidFuture);
          permissionControllerService.onGetAppPermissions(param1String, new _$$Lambda$iNX3LK7OVLRPv9_Lf_TwPCbUW98(param1AndroidFuture));
        }
        
        public void revokeRuntimePermission(String param1String1, String param1String2) {
          Preconditions.checkNotNull(param1String1, "packageName");
          Preconditions.checkNotNull(param1String2, "permissionName");
          enforceSomePermissionsGrantedToCaller(new String[] { "android.permission.REVOKE_RUNTIME_PERMISSIONS" });
          CountDownLatch countDownLatch = new CountDownLatch(1);
          PermissionControllerService permissionControllerService = PermissionControllerService.this;
          Objects.requireNonNull(countDownLatch);
          _$$Lambda$5k6tNlswoNAjCdgttrkQIe8VHVs _$$Lambda$5k6tNlswoNAjCdgttrkQIe8VHVs = new _$$Lambda$5k6tNlswoNAjCdgttrkQIe8VHVs(countDownLatch);
          permissionControllerService.onRevokeRuntimePermission(param1String1, param1String2, _$$Lambda$5k6tNlswoNAjCdgttrkQIe8VHVs);
          try {
            countDownLatch.await();
          } catch (InterruptedException interruptedException) {
            Log.e(PermissionControllerService.LOG_TAG, "revokeRuntimePermission timed out", interruptedException);
          } 
        }
        
        public void countPermissionApps(List<String> param1List, int param1Int, AndroidFuture param1AndroidFuture) {
          Preconditions.checkCollectionElementsNotNull(param1List, "permissionNames");
          Preconditions.checkFlagsArgument(param1Int, 3);
          Preconditions.checkNotNull(param1AndroidFuture, "callback");
          enforceSomePermissionsGrantedToCaller(new String[] { "android.permission.GET_RUNTIME_PERMISSIONS" });
          PermissionControllerService permissionControllerService = PermissionControllerService.this;
          Objects.requireNonNull(param1AndroidFuture);
          permissionControllerService.onCountPermissionApps(param1List, param1Int, new _$$Lambda$we_s0SXLkNKfmYcwhvjS2fbsEsg(param1AndroidFuture));
        }
        
        public void getPermissionUsages(boolean param1Boolean, long param1Long, AndroidFuture param1AndroidFuture) {
          Preconditions.checkArgumentNonnegative(param1Long);
          Preconditions.checkNotNull(param1AndroidFuture, "callback");
          enforceSomePermissionsGrantedToCaller(new String[] { "android.permission.GET_RUNTIME_PERMISSIONS" });
          PermissionControllerService permissionControllerService = PermissionControllerService.this;
          Objects.requireNonNull(param1AndroidFuture);
          permissionControllerService.onGetPermissionUsages(param1Boolean, param1Long, new _$$Lambda$iNX3LK7OVLRPv9_Lf_TwPCbUW98(param1AndroidFuture));
        }
        
        public void setRuntimePermissionGrantStateByDeviceAdmin(String param1String1, String param1String2, String param1String3, int param1Int, AndroidFuture param1AndroidFuture) {
          Preconditions.checkStringNotEmpty(param1String1);
          Preconditions.checkStringNotEmpty(param1String2);
          Preconditions.checkStringNotEmpty(param1String3);
          boolean bool1 = true, bool2 = bool1;
          if (param1Int != 1) {
            bool2 = bool1;
            if (param1Int != 2)
              if (param1Int == 0) {
                bool2 = bool1;
              } else {
                bool2 = false;
              }  
          } 
          Preconditions.checkArgument(bool2);
          Preconditions.checkNotNull(param1AndroidFuture);
          if (param1Int == 2)
            enforceSomePermissionsGrantedToCaller(new String[] { "android.permission.GRANT_RUNTIME_PERMISSIONS" }); 
          if (param1Int == 2)
            enforceSomePermissionsGrantedToCaller(new String[] { "android.permission.REVOKE_RUNTIME_PERMISSIONS" }); 
          enforceSomePermissionsGrantedToCaller(new String[] { "android.permission.ADJUST_RUNTIME_PERMISSIONS_POLICY" });
          PermissionControllerService permissionControllerService = PermissionControllerService.this;
          Objects.requireNonNull(param1AndroidFuture);
          _$$Lambda$PYgLd3P_k0utHHEMLxjUyz_Fj7c _$$Lambda$PYgLd3P_k0utHHEMLxjUyz_Fj7c = new _$$Lambda$PYgLd3P_k0utHHEMLxjUyz_Fj7c(param1AndroidFuture);
          permissionControllerService.onSetRuntimePermissionGrantStateByDeviceAdmin(param1String1, param1String2, param1String3, param1Int, _$$Lambda$PYgLd3P_k0utHHEMLxjUyz_Fj7c);
        }
        
        public void grantOrUpgradeDefaultRuntimePermissions(AndroidFuture param1AndroidFuture) {
          Preconditions.checkNotNull(param1AndroidFuture, "callback");
          enforceSomePermissionsGrantedToCaller(new String[] { "android.permission.ADJUST_RUNTIME_PERMISSIONS_POLICY" });
          PermissionControllerService.this.onGrantOrUpgradeDefaultRuntimePermissions(new _$$Lambda$PermissionControllerService$1$gBmMMOm6HYl5a03WwYV9xJmRYLc(param1AndroidFuture));
        }
        
        public void updateUserSensitiveForApp(int param1Int, AndroidFuture param1AndroidFuture) {
          Preconditions.checkNotNull(param1AndroidFuture, "callback cannot be null");
          try {
            PermissionControllerService permissionControllerService = PermissionControllerService.this;
            _$$Lambda$PermissionControllerService$1$TbuFGuD1HySgLHbAMINqz_Xt8ZE _$$Lambda$PermissionControllerService$1$TbuFGuD1HySgLHbAMINqz_Xt8ZE = new _$$Lambda$PermissionControllerService$1$TbuFGuD1HySgLHbAMINqz_Xt8ZE();
            this(param1AndroidFuture);
            permissionControllerService.onUpdateUserSensitivePermissionFlags(param1Int, _$$Lambda$PermissionControllerService$1$TbuFGuD1HySgLHbAMINqz_Xt8ZE);
          } catch (Exception exception) {
            param1AndroidFuture.completeExceptionally(exception);
          } 
        }
        
        public void notifyOneTimePermissionSessionTimeout(String param1String) {
          enforceSomePermissionsGrantedToCaller(new String[] { "android.permission.REVOKE_RUNTIME_PERMISSIONS" });
          param1String = (String)Preconditions.checkNotNull(param1String, "packageName cannot be null");
          PermissionControllerService.this.onOneTimePermissionSessionTimeout(param1String);
        }
        
        protected void dump(FileDescriptor param1FileDescriptor, PrintWriter param1PrintWriter, String[] param1ArrayOfString) {
          Preconditions.checkNotNull(param1FileDescriptor, "fd");
          Preconditions.checkNotNull(param1PrintWriter, "writer");
          enforceSomePermissionsGrantedToCaller(new String[] { "android.permission.GET_RUNTIME_PERMISSIONS" });
          PermissionControllerService.this.dump(param1FileDescriptor, param1PrintWriter, param1ArrayOfString);
        }
      };
  }
  
  public abstract void onCountPermissionApps(List<String> paramList, int paramInt, IntConsumer paramIntConsumer);
  
  public abstract void onGetAppPermissions(String paramString, Consumer<List<RuntimePermissionPresentationInfo>> paramConsumer);
  
  public abstract void onGetPermissionUsages(boolean paramBoolean, long paramLong, Consumer<List<RuntimePermissionUsageInfo>> paramConsumer);
  
  public abstract void onGetRuntimePermissionsBackup(UserHandle paramUserHandle, OutputStream paramOutputStream, Runnable paramRunnable);
  
  public abstract void onGrantOrUpgradeDefaultRuntimePermissions(Runnable paramRunnable);
  
  public abstract void onRevokeRuntimePermission(String paramString1, String paramString2, Runnable paramRunnable);
  
  public abstract void onRevokeRuntimePermissions(Map<String, List<String>> paramMap, boolean paramBoolean, int paramInt, String paramString, Consumer<Map<String, List<String>>> paramConsumer);
  
  public abstract void onSetRuntimePermissionGrantStateByDeviceAdmin(String paramString1, String paramString2, String paramString3, int paramInt, Consumer<Boolean> paramConsumer);
}
