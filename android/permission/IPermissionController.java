package android.permission;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.os.UserHandle;
import com.android.internal.infra.AndroidFuture;
import java.util.ArrayList;
import java.util.List;

public interface IPermissionController extends IInterface {
  void applyStagedRuntimePermissionBackup(String paramString, UserHandle paramUserHandle, AndroidFuture paramAndroidFuture) throws RemoteException;
  
  void countPermissionApps(List<String> paramList, int paramInt, AndroidFuture paramAndroidFuture) throws RemoteException;
  
  void getAppPermissions(String paramString, AndroidFuture paramAndroidFuture) throws RemoteException;
  
  void getPermissionUsages(boolean paramBoolean, long paramLong, AndroidFuture paramAndroidFuture) throws RemoteException;
  
  void getRuntimePermissionBackup(UserHandle paramUserHandle, ParcelFileDescriptor paramParcelFileDescriptor) throws RemoteException;
  
  void grantOrUpgradeDefaultRuntimePermissions(AndroidFuture paramAndroidFuture) throws RemoteException;
  
  void notifyOneTimePermissionSessionTimeout(String paramString) throws RemoteException;
  
  void revokeRuntimePermission(String paramString1, String paramString2) throws RemoteException;
  
  void revokeRuntimePermissions(Bundle paramBundle, boolean paramBoolean, int paramInt, String paramString, AndroidFuture paramAndroidFuture) throws RemoteException;
  
  void setRuntimePermissionGrantStateByDeviceAdmin(String paramString1, String paramString2, String paramString3, int paramInt, AndroidFuture paramAndroidFuture) throws RemoteException;
  
  void stageAndApplyRuntimePermissionsBackup(UserHandle paramUserHandle, ParcelFileDescriptor paramParcelFileDescriptor) throws RemoteException;
  
  void updateUserSensitiveForApp(int paramInt, AndroidFuture paramAndroidFuture) throws RemoteException;
  
  class Default implements IPermissionController {
    public void revokeRuntimePermissions(Bundle param1Bundle, boolean param1Boolean, int param1Int, String param1String, AndroidFuture param1AndroidFuture) throws RemoteException {}
    
    public void getRuntimePermissionBackup(UserHandle param1UserHandle, ParcelFileDescriptor param1ParcelFileDescriptor) throws RemoteException {}
    
    public void stageAndApplyRuntimePermissionsBackup(UserHandle param1UserHandle, ParcelFileDescriptor param1ParcelFileDescriptor) throws RemoteException {}
    
    public void applyStagedRuntimePermissionBackup(String param1String, UserHandle param1UserHandle, AndroidFuture param1AndroidFuture) throws RemoteException {}
    
    public void getAppPermissions(String param1String, AndroidFuture param1AndroidFuture) throws RemoteException {}
    
    public void revokeRuntimePermission(String param1String1, String param1String2) throws RemoteException {}
    
    public void countPermissionApps(List<String> param1List, int param1Int, AndroidFuture param1AndroidFuture) throws RemoteException {}
    
    public void getPermissionUsages(boolean param1Boolean, long param1Long, AndroidFuture param1AndroidFuture) throws RemoteException {}
    
    public void setRuntimePermissionGrantStateByDeviceAdmin(String param1String1, String param1String2, String param1String3, int param1Int, AndroidFuture param1AndroidFuture) throws RemoteException {}
    
    public void grantOrUpgradeDefaultRuntimePermissions(AndroidFuture param1AndroidFuture) throws RemoteException {}
    
    public void notifyOneTimePermissionSessionTimeout(String param1String) throws RemoteException {}
    
    public void updateUserSensitiveForApp(int param1Int, AndroidFuture param1AndroidFuture) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPermissionController {
    private static final String DESCRIPTOR = "android.permission.IPermissionController";
    
    static final int TRANSACTION_applyStagedRuntimePermissionBackup = 4;
    
    static final int TRANSACTION_countPermissionApps = 7;
    
    static final int TRANSACTION_getAppPermissions = 5;
    
    static final int TRANSACTION_getPermissionUsages = 8;
    
    static final int TRANSACTION_getRuntimePermissionBackup = 2;
    
    static final int TRANSACTION_grantOrUpgradeDefaultRuntimePermissions = 10;
    
    static final int TRANSACTION_notifyOneTimePermissionSessionTimeout = 11;
    
    static final int TRANSACTION_revokeRuntimePermission = 6;
    
    static final int TRANSACTION_revokeRuntimePermissions = 1;
    
    static final int TRANSACTION_setRuntimePermissionGrantStateByDeviceAdmin = 9;
    
    static final int TRANSACTION_stageAndApplyRuntimePermissionsBackup = 3;
    
    static final int TRANSACTION_updateUserSensitiveForApp = 12;
    
    public Stub() {
      attachInterface(this, "android.permission.IPermissionController");
    }
    
    public static IPermissionController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.permission.IPermissionController");
      if (iInterface != null && iInterface instanceof IPermissionController)
        return (IPermissionController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 12:
          return "updateUserSensitiveForApp";
        case 11:
          return "notifyOneTimePermissionSessionTimeout";
        case 10:
          return "grantOrUpgradeDefaultRuntimePermissions";
        case 9:
          return "setRuntimePermissionGrantStateByDeviceAdmin";
        case 8:
          return "getPermissionUsages";
        case 7:
          return "countPermissionApps";
        case 6:
          return "revokeRuntimePermission";
        case 5:
          return "getAppPermissions";
        case 4:
          return "applyStagedRuntimePermissionBackup";
        case 3:
          return "stageAndApplyRuntimePermissionsBackup";
        case 2:
          return "getRuntimePermissionBackup";
        case 1:
          break;
      } 
      return "revokeRuntimePermissions";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        String str1, str2;
        ArrayList<String> arrayList;
        String str3;
        long l;
        boolean bool = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 12:
            param1Parcel1.enforceInterface("android.permission.IPermissionController");
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              AndroidFuture androidFuture = AndroidFuture.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            updateUserSensitiveForApp(param1Int1, (AndroidFuture)param1Parcel1);
            return true;
          case 11:
            param1Parcel1.enforceInterface("android.permission.IPermissionController");
            str1 = param1Parcel1.readString();
            notifyOneTimePermissionSessionTimeout(str1);
            return true;
          case 10:
            str1.enforceInterface("android.permission.IPermissionController");
            if (str1.readInt() != 0) {
              AndroidFuture androidFuture = AndroidFuture.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            grantOrUpgradeDefaultRuntimePermissions((AndroidFuture)str1);
            return true;
          case 9:
            str1.enforceInterface("android.permission.IPermissionController");
            str3 = str1.readString();
            str2 = str1.readString();
            str4 = str1.readString();
            param1Int1 = str1.readInt();
            if (str1.readInt() != 0) {
              AndroidFuture androidFuture = AndroidFuture.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            setRuntimePermissionGrantStateByDeviceAdmin(str3, str2, str4, param1Int1, (AndroidFuture)str1);
            return true;
          case 8:
            str1.enforceInterface("android.permission.IPermissionController");
            if (str1.readInt() != 0)
              bool = true; 
            l = str1.readLong();
            if (str1.readInt() != 0) {
              AndroidFuture androidFuture = AndroidFuture.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            getPermissionUsages(bool, l, (AndroidFuture)str1);
            return true;
          case 7:
            str1.enforceInterface("android.permission.IPermissionController");
            arrayList = str1.createStringArrayList();
            param1Int1 = str1.readInt();
            if (str1.readInt() != 0) {
              AndroidFuture androidFuture = AndroidFuture.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            countPermissionApps(arrayList, param1Int1, (AndroidFuture)str1);
            return true;
          case 6:
            str1.enforceInterface("android.permission.IPermissionController");
            str = str1.readString();
            str1 = str1.readString();
            revokeRuntimePermission(str, str1);
            return true;
          case 5:
            str1.enforceInterface("android.permission.IPermissionController");
            str = str1.readString();
            if (str1.readInt() != 0) {
              AndroidFuture androidFuture = AndroidFuture.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            getAppPermissions(str, (AndroidFuture)str1);
            return true;
          case 4:
            str1.enforceInterface("android.permission.IPermissionController");
            str4 = str1.readString();
            if (str1.readInt() != 0) {
              UserHandle userHandle = UserHandle.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str = null;
            } 
            if (str1.readInt() != 0) {
              AndroidFuture androidFuture = AndroidFuture.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            applyStagedRuntimePermissionBackup(str4, (UserHandle)str, (AndroidFuture)str1);
            return true;
          case 3:
            str1.enforceInterface("android.permission.IPermissionController");
            if (str1.readInt() != 0) {
              UserHandle userHandle = UserHandle.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str = null;
            } 
            if (str1.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            stageAndApplyRuntimePermissionsBackup((UserHandle)str, (ParcelFileDescriptor)str1);
            return true;
          case 2:
            str1.enforceInterface("android.permission.IPermissionController");
            if (str1.readInt() != 0) {
              UserHandle userHandle = UserHandle.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str = null;
            } 
            if (str1.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            getRuntimePermissionBackup((UserHandle)str, (ParcelFileDescriptor)str1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.permission.IPermissionController");
        if (str1.readInt() != 0) {
          Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)str1);
        } else {
          str = null;
        } 
        if (str1.readInt() != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        param1Int1 = str1.readInt();
        String str4 = str1.readString();
        if (str1.readInt() != 0) {
          AndroidFuture androidFuture = AndroidFuture.CREATOR.createFromParcel((Parcel)str1);
        } else {
          str1 = null;
        } 
        revokeRuntimePermissions((Bundle)str, bool, param1Int1, str4, (AndroidFuture)str1);
        return true;
      } 
      str.writeString("android.permission.IPermissionController");
      return true;
    }
    
    private static class Proxy implements IPermissionController {
      public static IPermissionController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.permission.IPermissionController";
      }
      
      public void revokeRuntimePermissions(Bundle param2Bundle, boolean param2Boolean, int param2Int, String param2String, AndroidFuture param2AndroidFuture) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.permission.IPermissionController");
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          if (param2AndroidFuture != null) {
            parcel.writeInt(1);
            param2AndroidFuture.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool1 = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool1 && IPermissionController.Stub.getDefaultImpl() != null) {
            IPermissionController.Stub.getDefaultImpl().revokeRuntimePermissions(param2Bundle, param2Boolean, param2Int, param2String, param2AndroidFuture);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getRuntimePermissionBackup(UserHandle param2UserHandle, ParcelFileDescriptor param2ParcelFileDescriptor) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.permission.IPermissionController");
          if (param2UserHandle != null) {
            parcel.writeInt(1);
            param2UserHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ParcelFileDescriptor != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IPermissionController.Stub.getDefaultImpl() != null) {
            IPermissionController.Stub.getDefaultImpl().getRuntimePermissionBackup(param2UserHandle, param2ParcelFileDescriptor);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void stageAndApplyRuntimePermissionsBackup(UserHandle param2UserHandle, ParcelFileDescriptor param2ParcelFileDescriptor) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.permission.IPermissionController");
          if (param2UserHandle != null) {
            parcel.writeInt(1);
            param2UserHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ParcelFileDescriptor != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IPermissionController.Stub.getDefaultImpl() != null) {
            IPermissionController.Stub.getDefaultImpl().stageAndApplyRuntimePermissionsBackup(param2UserHandle, param2ParcelFileDescriptor);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void applyStagedRuntimePermissionBackup(String param2String, UserHandle param2UserHandle, AndroidFuture param2AndroidFuture) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.permission.IPermissionController");
          parcel.writeString(param2String);
          if (param2UserHandle != null) {
            parcel.writeInt(1);
            param2UserHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2AndroidFuture != null) {
            parcel.writeInt(1);
            param2AndroidFuture.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IPermissionController.Stub.getDefaultImpl() != null) {
            IPermissionController.Stub.getDefaultImpl().applyStagedRuntimePermissionBackup(param2String, param2UserHandle, param2AndroidFuture);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getAppPermissions(String param2String, AndroidFuture param2AndroidFuture) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.permission.IPermissionController");
          parcel.writeString(param2String);
          if (param2AndroidFuture != null) {
            parcel.writeInt(1);
            param2AndroidFuture.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IPermissionController.Stub.getDefaultImpl() != null) {
            IPermissionController.Stub.getDefaultImpl().getAppPermissions(param2String, param2AndroidFuture);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void revokeRuntimePermission(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.permission.IPermissionController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IPermissionController.Stub.getDefaultImpl() != null) {
            IPermissionController.Stub.getDefaultImpl().revokeRuntimePermission(param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void countPermissionApps(List<String> param2List, int param2Int, AndroidFuture param2AndroidFuture) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.permission.IPermissionController");
          parcel.writeStringList(param2List);
          parcel.writeInt(param2Int);
          if (param2AndroidFuture != null) {
            parcel.writeInt(1);
            param2AndroidFuture.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && IPermissionController.Stub.getDefaultImpl() != null) {
            IPermissionController.Stub.getDefaultImpl().countPermissionApps(param2List, param2Int, param2AndroidFuture);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getPermissionUsages(boolean param2Boolean, long param2Long, AndroidFuture param2AndroidFuture) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.permission.IPermissionController");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeLong(param2Long);
          if (param2AndroidFuture != null) {
            parcel.writeInt(1);
            param2AndroidFuture.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool1 = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool1 && IPermissionController.Stub.getDefaultImpl() != null) {
            IPermissionController.Stub.getDefaultImpl().getPermissionUsages(param2Boolean, param2Long, param2AndroidFuture);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setRuntimePermissionGrantStateByDeviceAdmin(String param2String1, String param2String2, String param2String3, int param2Int, AndroidFuture param2AndroidFuture) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.permission.IPermissionController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeString(param2String3);
          parcel.writeInt(param2Int);
          if (param2AndroidFuture != null) {
            parcel.writeInt(1);
            param2AndroidFuture.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel, (Parcel)null, 1);
          if (!bool && IPermissionController.Stub.getDefaultImpl() != null) {
            IPermissionController.Stub.getDefaultImpl().setRuntimePermissionGrantStateByDeviceAdmin(param2String1, param2String2, param2String3, param2Int, param2AndroidFuture);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void grantOrUpgradeDefaultRuntimePermissions(AndroidFuture param2AndroidFuture) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.permission.IPermissionController");
          if (param2AndroidFuture != null) {
            parcel.writeInt(1);
            param2AndroidFuture.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel, (Parcel)null, 1);
          if (!bool && IPermissionController.Stub.getDefaultImpl() != null) {
            IPermissionController.Stub.getDefaultImpl().grantOrUpgradeDefaultRuntimePermissions(param2AndroidFuture);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyOneTimePermissionSessionTimeout(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.permission.IPermissionController");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(11, parcel, (Parcel)null, 1);
          if (!bool && IPermissionController.Stub.getDefaultImpl() != null) {
            IPermissionController.Stub.getDefaultImpl().notifyOneTimePermissionSessionTimeout(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void updateUserSensitiveForApp(int param2Int, AndroidFuture param2AndroidFuture) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.permission.IPermissionController");
          parcel.writeInt(param2Int);
          if (param2AndroidFuture != null) {
            parcel.writeInt(1);
            param2AndroidFuture.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(12, parcel, (Parcel)null, 1);
          if (!bool && IPermissionController.Stub.getDefaultImpl() != null) {
            IPermissionController.Stub.getDefaultImpl().updateUserSensitiveForApp(param2Int, param2AndroidFuture);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPermissionController param1IPermissionController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPermissionController != null) {
          Proxy.sDefaultImpl = param1IPermissionController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPermissionController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
