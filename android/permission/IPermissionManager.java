package android.permission;

import android.content.pm.ParceledListSlice;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.content.pm.permission.SplitPermissionInfoParcelable;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IPermissionManager extends IInterface {
  void addOnPermissionsChangeListener(IOnPermissionsChangeListener paramIOnPermissionsChangeListener) throws RemoteException;
  
  boolean addPermission(PermissionInfo paramPermissionInfo, boolean paramBoolean) throws RemoteException;
  
  boolean addWhitelistedRestrictedPermission(String paramString1, String paramString2, int paramInt1, int paramInt2) throws RemoteException;
  
  int checkDeviceIdentifierAccess(String paramString1, String paramString2, String paramString3, int paramInt1, int paramInt2) throws RemoteException;
  
  int checkPermission(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  int checkUidPermission(String paramString, int paramInt) throws RemoteException;
  
  ParceledListSlice getAllPermissionGroups(int paramInt) throws RemoteException;
  
  String[] getAppOpPermissionPackages(String paramString) throws RemoteException;
  
  List<String> getAutoRevokeExemptionGrantedPackages(int paramInt) throws RemoteException;
  
  List<String> getAutoRevokeExemptionRequestedPackages(int paramInt) throws RemoteException;
  
  String getDefaultBrowser(int paramInt) throws RemoteException;
  
  int getPermissionFlags(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  PermissionGroupInfo getPermissionGroupInfo(String paramString, int paramInt) throws RemoteException;
  
  PermissionInfo getPermissionInfo(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  List<SplitPermissionInfoParcelable> getSplitPermissions() throws RemoteException;
  
  List<String> getWhitelistedRestrictedPermissions(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void grantDefaultPermissionsToActiveLuiApp(String paramString, int paramInt) throws RemoteException;
  
  void grantDefaultPermissionsToEnabledCarrierApps(String[] paramArrayOfString, int paramInt) throws RemoteException;
  
  void grantDefaultPermissionsToEnabledImsServices(String[] paramArrayOfString, int paramInt) throws RemoteException;
  
  void grantDefaultPermissionsToEnabledTelephonyDataServices(String[] paramArrayOfString, int paramInt) throws RemoteException;
  
  void grantRuntimePermission(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  boolean isAutoRevokeWhitelisted(String paramString, int paramInt) throws RemoteException;
  
  boolean isPermissionEnforced(String paramString) throws RemoteException;
  
  boolean isPermissionRevokedByPolicy(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  ParceledListSlice queryPermissionsByGroup(String paramString, int paramInt) throws RemoteException;
  
  void removeOnPermissionsChangeListener(IOnPermissionsChangeListener paramIOnPermissionsChangeListener) throws RemoteException;
  
  void removePermission(String paramString) throws RemoteException;
  
  boolean removeWhitelistedRestrictedPermission(String paramString1, String paramString2, int paramInt1, int paramInt2) throws RemoteException;
  
  void resetRuntimePermissions() throws RemoteException;
  
  void revokeDefaultPermissionsFromDisabledTelephonyDataServices(String[] paramArrayOfString, int paramInt) throws RemoteException;
  
  void revokeDefaultPermissionsFromLuiApps(String[] paramArrayOfString, int paramInt) throws RemoteException;
  
  void revokeRuntimePermission(String paramString1, String paramString2, int paramInt, String paramString3) throws RemoteException;
  
  boolean setAutoRevokeWhitelisted(String paramString, boolean paramBoolean, int paramInt) throws RemoteException;
  
  boolean setDefaultBrowser(String paramString, int paramInt) throws RemoteException;
  
  void setPermissionEnforced(String paramString, boolean paramBoolean) throws RemoteException;
  
  boolean shouldShowRequestPermissionRationale(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  void startOneTimePermissionSession(String paramString, int paramInt1, long paramLong, int paramInt2, int paramInt3) throws RemoteException;
  
  void stopOneTimePermissionSession(String paramString, int paramInt) throws RemoteException;
  
  void updatePermissionFlags(String paramString1, String paramString2, int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3) throws RemoteException;
  
  void updatePermissionFlagsForAllApps(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  class Default implements IPermissionManager {
    public String[] getAppOpPermissionPackages(String param1String) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice getAllPermissionGroups(int param1Int) throws RemoteException {
      return null;
    }
    
    public PermissionGroupInfo getPermissionGroupInfo(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public PermissionInfo getPermissionInfo(String param1String1, String param1String2, int param1Int) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice queryPermissionsByGroup(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean addPermission(PermissionInfo param1PermissionInfo, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public void removePermission(String param1String) throws RemoteException {}
    
    public int getPermissionFlags(String param1String1, String param1String2, int param1Int) throws RemoteException {
      return 0;
    }
    
    public void updatePermissionFlags(String param1String1, String param1String2, int param1Int1, int param1Int2, boolean param1Boolean, int param1Int3) throws RemoteException {}
    
    public void updatePermissionFlagsForAllApps(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public int checkPermission(String param1String1, String param1String2, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int checkUidPermission(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int checkDeviceIdentifierAccess(String param1String1, String param1String2, String param1String3, int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public void addOnPermissionsChangeListener(IOnPermissionsChangeListener param1IOnPermissionsChangeListener) throws RemoteException {}
    
    public void removeOnPermissionsChangeListener(IOnPermissionsChangeListener param1IOnPermissionsChangeListener) throws RemoteException {}
    
    public List<String> getWhitelistedRestrictedPermissions(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public boolean addWhitelistedRestrictedPermission(String param1String1, String param1String2, int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public boolean removeWhitelistedRestrictedPermission(String param1String1, String param1String2, int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public void grantRuntimePermission(String param1String1, String param1String2, int param1Int) throws RemoteException {}
    
    public void revokeRuntimePermission(String param1String1, String param1String2, int param1Int, String param1String3) throws RemoteException {}
    
    public void resetRuntimePermissions() throws RemoteException {}
    
    public boolean setDefaultBrowser(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public String getDefaultBrowser(int param1Int) throws RemoteException {
      return null;
    }
    
    public void grantDefaultPermissionsToEnabledCarrierApps(String[] param1ArrayOfString, int param1Int) throws RemoteException {}
    
    public void grantDefaultPermissionsToEnabledImsServices(String[] param1ArrayOfString, int param1Int) throws RemoteException {}
    
    public void grantDefaultPermissionsToEnabledTelephonyDataServices(String[] param1ArrayOfString, int param1Int) throws RemoteException {}
    
    public void revokeDefaultPermissionsFromDisabledTelephonyDataServices(String[] param1ArrayOfString, int param1Int) throws RemoteException {}
    
    public void grantDefaultPermissionsToActiveLuiApp(String param1String, int param1Int) throws RemoteException {}
    
    public void revokeDefaultPermissionsFromLuiApps(String[] param1ArrayOfString, int param1Int) throws RemoteException {}
    
    public void setPermissionEnforced(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public boolean isPermissionEnforced(String param1String) throws RemoteException {
      return false;
    }
    
    public boolean shouldShowRequestPermissionRationale(String param1String1, String param1String2, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isPermissionRevokedByPolicy(String param1String1, String param1String2, int param1Int) throws RemoteException {
      return false;
    }
    
    public List<SplitPermissionInfoParcelable> getSplitPermissions() throws RemoteException {
      return null;
    }
    
    public void startOneTimePermissionSession(String param1String, int param1Int1, long param1Long, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void stopOneTimePermissionSession(String param1String, int param1Int) throws RemoteException {}
    
    public List<String> getAutoRevokeExemptionRequestedPackages(int param1Int) throws RemoteException {
      return null;
    }
    
    public List<String> getAutoRevokeExemptionGrantedPackages(int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean setAutoRevokeWhitelisted(String param1String, boolean param1Boolean, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isAutoRevokeWhitelisted(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPermissionManager {
    private static final String DESCRIPTOR = "android.permission.IPermissionManager";
    
    static final int TRANSACTION_addOnPermissionsChangeListener = 14;
    
    static final int TRANSACTION_addPermission = 6;
    
    static final int TRANSACTION_addWhitelistedRestrictedPermission = 17;
    
    static final int TRANSACTION_checkDeviceIdentifierAccess = 13;
    
    static final int TRANSACTION_checkPermission = 11;
    
    static final int TRANSACTION_checkUidPermission = 12;
    
    static final int TRANSACTION_getAllPermissionGroups = 2;
    
    static final int TRANSACTION_getAppOpPermissionPackages = 1;
    
    static final int TRANSACTION_getAutoRevokeExemptionGrantedPackages = 38;
    
    static final int TRANSACTION_getAutoRevokeExemptionRequestedPackages = 37;
    
    static final int TRANSACTION_getDefaultBrowser = 23;
    
    static final int TRANSACTION_getPermissionFlags = 8;
    
    static final int TRANSACTION_getPermissionGroupInfo = 3;
    
    static final int TRANSACTION_getPermissionInfo = 4;
    
    static final int TRANSACTION_getSplitPermissions = 34;
    
    static final int TRANSACTION_getWhitelistedRestrictedPermissions = 16;
    
    static final int TRANSACTION_grantDefaultPermissionsToActiveLuiApp = 28;
    
    static final int TRANSACTION_grantDefaultPermissionsToEnabledCarrierApps = 24;
    
    static final int TRANSACTION_grantDefaultPermissionsToEnabledImsServices = 25;
    
    static final int TRANSACTION_grantDefaultPermissionsToEnabledTelephonyDataServices = 26;
    
    static final int TRANSACTION_grantRuntimePermission = 19;
    
    static final int TRANSACTION_isAutoRevokeWhitelisted = 40;
    
    static final int TRANSACTION_isPermissionEnforced = 31;
    
    static final int TRANSACTION_isPermissionRevokedByPolicy = 33;
    
    static final int TRANSACTION_queryPermissionsByGroup = 5;
    
    static final int TRANSACTION_removeOnPermissionsChangeListener = 15;
    
    static final int TRANSACTION_removePermission = 7;
    
    static final int TRANSACTION_removeWhitelistedRestrictedPermission = 18;
    
    static final int TRANSACTION_resetRuntimePermissions = 21;
    
    static final int TRANSACTION_revokeDefaultPermissionsFromDisabledTelephonyDataServices = 27;
    
    static final int TRANSACTION_revokeDefaultPermissionsFromLuiApps = 29;
    
    static final int TRANSACTION_revokeRuntimePermission = 20;
    
    static final int TRANSACTION_setAutoRevokeWhitelisted = 39;
    
    static final int TRANSACTION_setDefaultBrowser = 22;
    
    static final int TRANSACTION_setPermissionEnforced = 30;
    
    static final int TRANSACTION_shouldShowRequestPermissionRationale = 32;
    
    static final int TRANSACTION_startOneTimePermissionSession = 35;
    
    static final int TRANSACTION_stopOneTimePermissionSession = 36;
    
    static final int TRANSACTION_updatePermissionFlags = 9;
    
    static final int TRANSACTION_updatePermissionFlagsForAllApps = 10;
    
    public Stub() {
      attachInterface(this, "android.permission.IPermissionManager");
    }
    
    public static IPermissionManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.permission.IPermissionManager");
      if (iInterface != null && iInterface instanceof IPermissionManager)
        return (IPermissionManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 40:
          return "isAutoRevokeWhitelisted";
        case 39:
          return "setAutoRevokeWhitelisted";
        case 38:
          return "getAutoRevokeExemptionGrantedPackages";
        case 37:
          return "getAutoRevokeExemptionRequestedPackages";
        case 36:
          return "stopOneTimePermissionSession";
        case 35:
          return "startOneTimePermissionSession";
        case 34:
          return "getSplitPermissions";
        case 33:
          return "isPermissionRevokedByPolicy";
        case 32:
          return "shouldShowRequestPermissionRationale";
        case 31:
          return "isPermissionEnforced";
        case 30:
          return "setPermissionEnforced";
        case 29:
          return "revokeDefaultPermissionsFromLuiApps";
        case 28:
          return "grantDefaultPermissionsToActiveLuiApp";
        case 27:
          return "revokeDefaultPermissionsFromDisabledTelephonyDataServices";
        case 26:
          return "grantDefaultPermissionsToEnabledTelephonyDataServices";
        case 25:
          return "grantDefaultPermissionsToEnabledImsServices";
        case 24:
          return "grantDefaultPermissionsToEnabledCarrierApps";
        case 23:
          return "getDefaultBrowser";
        case 22:
          return "setDefaultBrowser";
        case 21:
          return "resetRuntimePermissions";
        case 20:
          return "revokeRuntimePermission";
        case 19:
          return "grantRuntimePermission";
        case 18:
          return "removeWhitelistedRestrictedPermission";
        case 17:
          return "addWhitelistedRestrictedPermission";
        case 16:
          return "getWhitelistedRestrictedPermissions";
        case 15:
          return "removeOnPermissionsChangeListener";
        case 14:
          return "addOnPermissionsChangeListener";
        case 13:
          return "checkDeviceIdentifierAccess";
        case 12:
          return "checkUidPermission";
        case 11:
          return "checkPermission";
        case 10:
          return "updatePermissionFlagsForAllApps";
        case 9:
          return "updatePermissionFlags";
        case 8:
          return "getPermissionFlags";
        case 7:
          return "removePermission";
        case 6:
          return "addPermission";
        case 5:
          return "queryPermissionsByGroup";
        case 4:
          return "getPermissionInfo";
        case 3:
          return "getPermissionGroupInfo";
        case 2:
          return "getAllPermissionGroups";
        case 1:
          break;
      } 
      return "getAppOpPermissionPackages";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool8;
        int i3;
        boolean bool7;
        int i2;
        boolean bool6;
        int i1;
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        int i;
        List<String> list2;
        String str3;
        List<String> list1;
        IOnPermissionsChangeListener iOnPermissionsChangeListener;
        String str2;
        ParceledListSlice parceledListSlice2;
        PermissionInfo permissionInfo;
        PermissionGroupInfo permissionGroupInfo;
        ParceledListSlice parceledListSlice1;
        String str6, arrayOfString3[], str5, arrayOfString2[], str4;
        long l;
        int i4;
        String str7, str8;
        boolean bool9 = false, bool10 = false, bool11 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 40:
            param1Parcel1.enforceInterface("android.permission.IPermissionManager");
            str6 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            bool8 = isAutoRevokeWhitelisted(str6, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 39:
            param1Parcel1.enforceInterface("android.permission.IPermissionManager");
            str6 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0)
              bool11 = true; 
            i3 = param1Parcel1.readInt();
            bool7 = setAutoRevokeWhitelisted(str6, bool11, i3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 38:
            param1Parcel1.enforceInterface("android.permission.IPermissionManager");
            i2 = param1Parcel1.readInt();
            list2 = getAutoRevokeExemptionGrantedPackages(i2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list2);
            return true;
          case 37:
            list2.enforceInterface("android.permission.IPermissionManager");
            i2 = list2.readInt();
            list2 = getAutoRevokeExemptionRequestedPackages(i2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list2);
            return true;
          case 36:
            list2.enforceInterface("android.permission.IPermissionManager");
            str6 = list2.readString();
            i2 = list2.readInt();
            stopOneTimePermissionSession(str6, i2);
            param1Parcel2.writeNoException();
            return true;
          case 35:
            list2.enforceInterface("android.permission.IPermissionManager");
            str6 = list2.readString();
            param1Int2 = list2.readInt();
            l = list2.readLong();
            i2 = list2.readInt();
            i4 = list2.readInt();
            startOneTimePermissionSession(str6, param1Int2, l, i2, i4);
            param1Parcel2.writeNoException();
            return true;
          case 34:
            list2.enforceInterface("android.permission.IPermissionManager");
            list2 = (List)getSplitPermissions();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list2);
            return true;
          case 33:
            list2.enforceInterface("android.permission.IPermissionManager");
            str7 = list2.readString();
            str6 = list2.readString();
            i2 = list2.readInt();
            bool6 = isPermissionRevokedByPolicy(str7, str6, i2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 32:
            list2.enforceInterface("android.permission.IPermissionManager");
            str6 = list2.readString();
            str7 = list2.readString();
            i1 = list2.readInt();
            bool5 = shouldShowRequestPermissionRationale(str6, str7, i1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 31:
            list2.enforceInterface("android.permission.IPermissionManager");
            str3 = list2.readString();
            bool5 = isPermissionEnforced(str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 30:
            str3.enforceInterface("android.permission.IPermissionManager");
            str6 = str3.readString();
            bool11 = bool9;
            if (str3.readInt() != 0)
              bool11 = true; 
            setPermissionEnforced(str6, bool11);
            param1Parcel2.writeNoException();
            return true;
          case 29:
            str3.enforceInterface("android.permission.IPermissionManager");
            arrayOfString3 = str3.createStringArray();
            n = str3.readInt();
            revokeDefaultPermissionsFromLuiApps(arrayOfString3, n);
            param1Parcel2.writeNoException();
            return true;
          case 28:
            str3.enforceInterface("android.permission.IPermissionManager");
            str5 = str3.readString();
            n = str3.readInt();
            grantDefaultPermissionsToActiveLuiApp(str5, n);
            param1Parcel2.writeNoException();
            return true;
          case 27:
            str3.enforceInterface("android.permission.IPermissionManager");
            arrayOfString2 = str3.createStringArray();
            n = str3.readInt();
            revokeDefaultPermissionsFromDisabledTelephonyDataServices(arrayOfString2, n);
            param1Parcel2.writeNoException();
            return true;
          case 26:
            str3.enforceInterface("android.permission.IPermissionManager");
            arrayOfString2 = str3.createStringArray();
            n = str3.readInt();
            grantDefaultPermissionsToEnabledTelephonyDataServices(arrayOfString2, n);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            str3.enforceInterface("android.permission.IPermissionManager");
            arrayOfString2 = str3.createStringArray();
            n = str3.readInt();
            grantDefaultPermissionsToEnabledImsServices(arrayOfString2, n);
            param1Parcel2.writeNoException();
            return true;
          case 24:
            str3.enforceInterface("android.permission.IPermissionManager");
            arrayOfString2 = str3.createStringArray();
            n = str3.readInt();
            grantDefaultPermissionsToEnabledCarrierApps(arrayOfString2, n);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            str3.enforceInterface("android.permission.IPermissionManager");
            n = str3.readInt();
            str3 = getDefaultBrowser(n);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str3);
            return true;
          case 22:
            str3.enforceInterface("android.permission.IPermissionManager");
            str4 = str3.readString();
            n = str3.readInt();
            bool4 = setDefaultBrowser(str4, n);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 21:
            str3.enforceInterface("android.permission.IPermissionManager");
            resetRuntimePermissions();
            param1Parcel2.writeNoException();
            return true;
          case 20:
            str3.enforceInterface("android.permission.IPermissionManager");
            str4 = str3.readString();
            str7 = str3.readString();
            m = str3.readInt();
            str3 = str3.readString();
            revokeRuntimePermission(str4, str7, m, str3);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            str3.enforceInterface("android.permission.IPermissionManager");
            str4 = str3.readString();
            str7 = str3.readString();
            m = str3.readInt();
            grantRuntimePermission(str4, str7, m);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            str3.enforceInterface("android.permission.IPermissionManager");
            str7 = str3.readString();
            str4 = str3.readString();
            param1Int2 = str3.readInt();
            m = str3.readInt();
            bool3 = removeWhitelistedRestrictedPermission(str7, str4, param1Int2, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 17:
            str3.enforceInterface("android.permission.IPermissionManager");
            str7 = str3.readString();
            str4 = str3.readString();
            param1Int2 = str3.readInt();
            k = str3.readInt();
            bool2 = addWhitelistedRestrictedPermission(str7, str4, param1Int2, k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 16:
            str3.enforceInterface("android.permission.IPermissionManager");
            str4 = str3.readString();
            j = str3.readInt();
            param1Int2 = str3.readInt();
            list1 = getWhitelistedRestrictedPermissions(str4, j, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 15:
            list1.enforceInterface("android.permission.IPermissionManager");
            iOnPermissionsChangeListener = IOnPermissionsChangeListener.Stub.asInterface(list1.readStrongBinder());
            removeOnPermissionsChangeListener(iOnPermissionsChangeListener);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            iOnPermissionsChangeListener.enforceInterface("android.permission.IPermissionManager");
            iOnPermissionsChangeListener = IOnPermissionsChangeListener.Stub.asInterface(iOnPermissionsChangeListener.readStrongBinder());
            addOnPermissionsChangeListener(iOnPermissionsChangeListener);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            iOnPermissionsChangeListener.enforceInterface("android.permission.IPermissionManager");
            str4 = iOnPermissionsChangeListener.readString();
            str7 = iOnPermissionsChangeListener.readString();
            str8 = iOnPermissionsChangeListener.readString();
            j = iOnPermissionsChangeListener.readInt();
            param1Int2 = iOnPermissionsChangeListener.readInt();
            j = checkDeviceIdentifierAccess(str4, str7, str8, j, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 12:
            iOnPermissionsChangeListener.enforceInterface("android.permission.IPermissionManager");
            str4 = iOnPermissionsChangeListener.readString();
            j = iOnPermissionsChangeListener.readInt();
            j = checkUidPermission(str4, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 11:
            iOnPermissionsChangeListener.enforceInterface("android.permission.IPermissionManager");
            str4 = iOnPermissionsChangeListener.readString();
            str7 = iOnPermissionsChangeListener.readString();
            j = iOnPermissionsChangeListener.readInt();
            j = checkPermission(str4, str7, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 10:
            iOnPermissionsChangeListener.enforceInterface("android.permission.IPermissionManager");
            i4 = iOnPermissionsChangeListener.readInt();
            j = iOnPermissionsChangeListener.readInt();
            param1Int2 = iOnPermissionsChangeListener.readInt();
            updatePermissionFlagsForAllApps(i4, j, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            iOnPermissionsChangeListener.enforceInterface("android.permission.IPermissionManager");
            str7 = iOnPermissionsChangeListener.readString();
            str4 = iOnPermissionsChangeListener.readString();
            i4 = iOnPermissionsChangeListener.readInt();
            param1Int2 = iOnPermissionsChangeListener.readInt();
            if (iOnPermissionsChangeListener.readInt() != 0) {
              bool11 = true;
            } else {
              bool11 = false;
            } 
            j = iOnPermissionsChangeListener.readInt();
            updatePermissionFlags(str7, str4, i4, param1Int2, bool11, j);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            iOnPermissionsChangeListener.enforceInterface("android.permission.IPermissionManager");
            str4 = iOnPermissionsChangeListener.readString();
            str7 = iOnPermissionsChangeListener.readString();
            j = iOnPermissionsChangeListener.readInt();
            j = getPermissionFlags(str4, str7, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 7:
            iOnPermissionsChangeListener.enforceInterface("android.permission.IPermissionManager");
            str2 = iOnPermissionsChangeListener.readString();
            removePermission(str2);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            str2.enforceInterface("android.permission.IPermissionManager");
            if (str2.readInt() != 0) {
              PermissionInfo permissionInfo1 = PermissionInfo.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str4 = null;
            } 
            bool11 = bool10;
            if (str2.readInt() != 0)
              bool11 = true; 
            bool1 = addPermission((PermissionInfo)str4, bool11);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 5:
            str2.enforceInterface("android.permission.IPermissionManager");
            str4 = str2.readString();
            i = str2.readInt();
            parceledListSlice2 = queryPermissionsByGroup(str4, i);
            param1Parcel2.writeNoException();
            if (parceledListSlice2 != null) {
              param1Parcel2.writeInt(1);
              parceledListSlice2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 4:
            parceledListSlice2.enforceInterface("android.permission.IPermissionManager");
            str4 = parceledListSlice2.readString();
            str7 = parceledListSlice2.readString();
            i = parceledListSlice2.readInt();
            permissionInfo = getPermissionInfo(str4, str7, i);
            param1Parcel2.writeNoException();
            if (permissionInfo != null) {
              param1Parcel2.writeInt(1);
              permissionInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            permissionInfo.enforceInterface("android.permission.IPermissionManager");
            str4 = permissionInfo.readString();
            i = permissionInfo.readInt();
            permissionGroupInfo = getPermissionGroupInfo(str4, i);
            param1Parcel2.writeNoException();
            if (permissionGroupInfo != null) {
              param1Parcel2.writeInt(1);
              permissionGroupInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 2:
            permissionGroupInfo.enforceInterface("android.permission.IPermissionManager");
            i = permissionGroupInfo.readInt();
            parceledListSlice1 = getAllPermissionGroups(i);
            param1Parcel2.writeNoException();
            if (parceledListSlice1 != null) {
              param1Parcel2.writeInt(1);
              parceledListSlice1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        parceledListSlice1.enforceInterface("android.permission.IPermissionManager");
        String str1 = parceledListSlice1.readString();
        String[] arrayOfString1 = getAppOpPermissionPackages(str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeStringArray(arrayOfString1);
        return true;
      } 
      param1Parcel2.writeString("android.permission.IPermissionManager");
      return true;
    }
    
    private static class Proxy implements IPermissionManager {
      public static IPermissionManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.permission.IPermissionManager";
      }
      
      public String[] getAppOpPermissionPackages(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null)
            return IPermissionManager.Stub.getDefaultImpl().getAppOpPermissionPackages(param2String); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getAllPermissionGroups(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ParceledListSlice parceledListSlice;
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
            parceledListSlice = IPermissionManager.Stub.getDefaultImpl().getAllPermissionGroups(param2Int);
            return parceledListSlice;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            parceledListSlice = ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            parceledListSlice = null;
          } 
          return parceledListSlice;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PermissionGroupInfo getPermissionGroupInfo(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null)
            return IPermissionManager.Stub.getDefaultImpl().getPermissionGroupInfo(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            PermissionGroupInfo permissionGroupInfo = PermissionGroupInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (PermissionGroupInfo)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PermissionInfo getPermissionInfo(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null)
            return IPermissionManager.Stub.getDefaultImpl().getPermissionInfo(param2String1, param2String2, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            PermissionInfo permissionInfo = PermissionInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (PermissionInfo)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice queryPermissionsByGroup(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null)
            return IPermissionManager.Stub.getDefaultImpl().queryPermissionsByGroup(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParceledListSlice)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean addPermission(PermissionInfo param2PermissionInfo, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          boolean bool = true;
          if (param2PermissionInfo != null) {
            parcel1.writeInt(1);
            param2PermissionInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool1 && IPermissionManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IPermissionManager.Stub.getDefaultImpl().addPermission(param2PermissionInfo, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removePermission(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
            IPermissionManager.Stub.getDefaultImpl().removePermission(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPermissionFlags(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
            param2Int = IPermissionManager.Stub.getDefaultImpl().getPermissionFlags(param2String1, param2String2, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updatePermissionFlags(String param2String1, String param2String2, int param2Int1, int param2Int2, boolean param2Boolean, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  boolean bool;
                  parcel1.writeInt(param2Int2);
                  if (param2Boolean) {
                    bool = true;
                  } else {
                    bool = false;
                  } 
                  parcel1.writeInt(bool);
                  try {
                    parcel1.writeInt(param2Int3);
                    try {
                      boolean bool1 = this.mRemote.transact(9, parcel1, parcel2, 0);
                      if (!bool1 && IPermissionManager.Stub.getDefaultImpl() != null) {
                        IPermissionManager.Stub.getDefaultImpl().updatePermissionFlags(param2String1, param2String2, param2Int1, param2Int2, param2Boolean, param2Int3);
                        parcel2.recycle();
                        parcel1.recycle();
                        return;
                      } 
                      parcel2.readException();
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void updatePermissionFlagsForAllApps(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
            IPermissionManager.Stub.getDefaultImpl().updatePermissionFlagsForAllApps(param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int checkPermission(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
            param2Int = IPermissionManager.Stub.getDefaultImpl().checkPermission(param2String1, param2String2, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int checkUidPermission(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
            param2Int = IPermissionManager.Stub.getDefaultImpl().checkUidPermission(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int checkDeviceIdentifierAccess(String param2String1, String param2String2, String param2String3, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
            param2Int1 = IPermissionManager.Stub.getDefaultImpl().checkDeviceIdentifierAccess(param2String1, param2String2, param2String3, param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addOnPermissionsChangeListener(IOnPermissionsChangeListener param2IOnPermissionsChangeListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          if (param2IOnPermissionsChangeListener != null) {
            iBinder = param2IOnPermissionsChangeListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
            IPermissionManager.Stub.getDefaultImpl().addOnPermissionsChangeListener(param2IOnPermissionsChangeListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeOnPermissionsChangeListener(IOnPermissionsChangeListener param2IOnPermissionsChangeListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          if (param2IOnPermissionsChangeListener != null) {
            iBinder = param2IOnPermissionsChangeListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
            IPermissionManager.Stub.getDefaultImpl().removeOnPermissionsChangeListener(param2IOnPermissionsChangeListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getWhitelistedRestrictedPermissions(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null)
            return IPermissionManager.Stub.getDefaultImpl().getWhitelistedRestrictedPermissions(param2String, param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean addWhitelistedRestrictedPermission(String param2String1, String param2String2, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(17, parcel1, parcel2, 0);
          if (!bool2 && IPermissionManager.Stub.getDefaultImpl() != null) {
            bool1 = IPermissionManager.Stub.getDefaultImpl().addWhitelistedRestrictedPermission(param2String1, param2String2, param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeWhitelistedRestrictedPermission(String param2String1, String param2String2, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(18, parcel1, parcel2, 0);
          if (!bool2 && IPermissionManager.Stub.getDefaultImpl() != null) {
            bool1 = IPermissionManager.Stub.getDefaultImpl().removeWhitelistedRestrictedPermission(param2String1, param2String2, param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void grantRuntimePermission(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
            IPermissionManager.Stub.getDefaultImpl().grantRuntimePermission(param2String1, param2String2, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void revokeRuntimePermission(String param2String1, String param2String2, int param2Int, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
            IPermissionManager.Stub.getDefaultImpl().revokeRuntimePermission(param2String1, param2String2, param2Int, param2String3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resetRuntimePermissions() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
            IPermissionManager.Stub.getDefaultImpl().resetRuntimePermissions();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setDefaultBrowser(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(22, parcel1, parcel2, 0);
          if (!bool2 && IPermissionManager.Stub.getDefaultImpl() != null) {
            bool1 = IPermissionManager.Stub.getDefaultImpl().setDefaultBrowser(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDefaultBrowser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null)
            return IPermissionManager.Stub.getDefaultImpl().getDefaultBrowser(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void grantDefaultPermissionsToEnabledCarrierApps(String[] param2ArrayOfString, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeStringArray(param2ArrayOfString);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
            IPermissionManager.Stub.getDefaultImpl().grantDefaultPermissionsToEnabledCarrierApps(param2ArrayOfString, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void grantDefaultPermissionsToEnabledImsServices(String[] param2ArrayOfString, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeStringArray(param2ArrayOfString);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
            IPermissionManager.Stub.getDefaultImpl().grantDefaultPermissionsToEnabledImsServices(param2ArrayOfString, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void grantDefaultPermissionsToEnabledTelephonyDataServices(String[] param2ArrayOfString, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeStringArray(param2ArrayOfString);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
            IPermissionManager.Stub.getDefaultImpl().grantDefaultPermissionsToEnabledTelephonyDataServices(param2ArrayOfString, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void revokeDefaultPermissionsFromDisabledTelephonyDataServices(String[] param2ArrayOfString, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeStringArray(param2ArrayOfString);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
            IPermissionManager.Stub.getDefaultImpl().revokeDefaultPermissionsFromDisabledTelephonyDataServices(param2ArrayOfString, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void grantDefaultPermissionsToActiveLuiApp(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
            IPermissionManager.Stub.getDefaultImpl().grantDefaultPermissionsToActiveLuiApp(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void revokeDefaultPermissionsFromLuiApps(String[] param2ArrayOfString, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeStringArray(param2ArrayOfString);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
            IPermissionManager.Stub.getDefaultImpl().revokeDefaultPermissionsFromLuiApps(param2ArrayOfString, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPermissionEnforced(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool1 && IPermissionManager.Stub.getDefaultImpl() != null) {
            IPermissionManager.Stub.getDefaultImpl().setPermissionEnforced(param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isPermissionEnforced(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(31, parcel1, parcel2, 0);
          if (!bool2 && IPermissionManager.Stub.getDefaultImpl() != null) {
            bool1 = IPermissionManager.Stub.getDefaultImpl().isPermissionEnforced(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean shouldShowRequestPermissionRationale(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(32, parcel1, parcel2, 0);
          if (!bool2 && IPermissionManager.Stub.getDefaultImpl() != null) {
            bool1 = IPermissionManager.Stub.getDefaultImpl().shouldShowRequestPermissionRationale(param2String1, param2String2, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isPermissionRevokedByPolicy(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(33, parcel1, parcel2, 0);
          if (!bool2 && IPermissionManager.Stub.getDefaultImpl() != null) {
            bool1 = IPermissionManager.Stub.getDefaultImpl().isPermissionRevokedByPolicy(param2String1, param2String2, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<SplitPermissionInfoParcelable> getSplitPermissions() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null)
            return IPermissionManager.Stub.getDefaultImpl().getSplitPermissions(); 
          parcel2.readException();
          return (List)parcel2.createTypedArrayList(SplitPermissionInfoParcelable.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startOneTimePermissionSession(String param2String, int param2Int1, long param2Long, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          try {
            parcel1.writeString(param2String);
            try {
              parcel1.writeInt(param2Int1);
              try {
                parcel1.writeLong(param2Long);
                try {
                  parcel1.writeInt(param2Int2);
                  try {
                    parcel1.writeInt(param2Int3);
                    boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
                    if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
                      IPermissionManager.Stub.getDefaultImpl().startOneTimePermissionSession(param2String, param2Int1, param2Long, param2Int2, param2Int3);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public void stopOneTimePermissionSession(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
            IPermissionManager.Stub.getDefaultImpl().stopOneTimePermissionSession(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getAutoRevokeExemptionRequestedPackages(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null)
            return IPermissionManager.Stub.getDefaultImpl().getAutoRevokeExemptionRequestedPackages(param2Int); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getAutoRevokeExemptionGrantedPackages(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null)
            return IPermissionManager.Stub.getDefaultImpl().getAutoRevokeExemptionGrantedPackages(param2Int); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setAutoRevokeWhitelisted(String param2String, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && IPermissionManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IPermissionManager.Stub.getDefaultImpl().setAutoRevokeWhitelisted(param2String, param2Boolean, param2Int);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAutoRevokeWhitelisted(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.permission.IPermissionManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(40, parcel1, parcel2, 0);
          if (!bool2 && IPermissionManager.Stub.getDefaultImpl() != null) {
            bool1 = IPermissionManager.Stub.getDefaultImpl().isAutoRevokeWhitelisted(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPermissionManager param1IPermissionManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPermissionManager != null) {
          Proxy.sDefaultImpl = param1IPermissionManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPermissionManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
