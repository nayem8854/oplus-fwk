package android.permission;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;

@SystemApi
public final class RuntimePermissionPresentationInfo implements Parcelable {
  public RuntimePermissionPresentationInfo(CharSequence paramCharSequence, boolean paramBoolean1, boolean paramBoolean2) {
    Preconditions.checkNotNull(paramCharSequence);
    this.mLabel = paramCharSequence;
    int i = 0;
    if (paramBoolean1)
      i = false | true; 
    int j = i;
    if (paramBoolean2)
      j = i | 0x2; 
    this.mFlags = j;
  }
  
  public boolean isGranted() {
    int i = this.mFlags;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public boolean isStandard() {
    boolean bool;
    if ((this.mFlags & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public CharSequence getLabel() {
    return this.mLabel;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeCharSequence(this.mLabel);
    paramParcel.writeInt(this.mFlags);
  }
  
  public static final Parcelable.Creator<RuntimePermissionPresentationInfo> CREATOR = new Parcelable.Creator<RuntimePermissionPresentationInfo>() {
      public RuntimePermissionPresentationInfo createFromParcel(Parcel param1Parcel) {
        boolean bool2;
        CharSequence charSequence = param1Parcel.readCharSequence();
        int i = param1Parcel.readInt();
        boolean bool1 = false;
        if ((i & 0x1) != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        if ((i & 0x2) != 0)
          bool1 = true; 
        return new RuntimePermissionPresentationInfo(charSequence, bool2, bool1);
      }
      
      public RuntimePermissionPresentationInfo[] newArray(int param1Int) {
        return new RuntimePermissionPresentationInfo[param1Int];
      }
    };
  
  private static final int FLAG_GRANTED = 1;
  
  private static final int FLAG_STANDARD = 2;
  
  private final int mFlags;
  
  private final CharSequence mLabel;
}
