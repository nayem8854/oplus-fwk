package android.permission;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;

@SystemApi
public final class RuntimePermissionUsageInfo implements Parcelable {
  public RuntimePermissionUsageInfo(String paramString, int paramInt) {
    Preconditions.checkNotNull(paramString);
    Preconditions.checkArgumentNonnegative(paramInt);
    this.mName = paramString;
    this.mNumUsers = paramInt;
  }
  
  private RuntimePermissionUsageInfo(Parcel paramParcel) {
    this(paramParcel.readString(), paramParcel.readInt());
  }
  
  public int getAppAccessCount() {
    return this.mNumUsers;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mName);
    paramParcel.writeInt(this.mNumUsers);
  }
  
  public static final Parcelable.Creator<RuntimePermissionUsageInfo> CREATOR = new Parcelable.Creator<RuntimePermissionUsageInfo>() {
      public RuntimePermissionUsageInfo createFromParcel(Parcel param1Parcel) {
        return new RuntimePermissionUsageInfo(param1Parcel);
      }
      
      public RuntimePermissionUsageInfo[] newArray(int param1Int) {
        return new RuntimePermissionUsageInfo[param1Int];
      }
    };
  
  private final String mName;
  
  private final int mNumUsers;
}
