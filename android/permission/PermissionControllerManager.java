package android.permission;

import android.annotation.SystemApi;
import android.app.ActivityThread;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.os.UserHandle;
import android.util.ArrayMap;
import android.util.Log;
import android.util.Pair;
import com.android.internal.infra.AndroidFuture;
import com.android.internal.infra.RemoteStream;
import com.android.internal.infra.ServiceConnector;
import com.android.internal.util.CollectionUtils;
import com.android.internal.util.Preconditions;
import java.io.FileDescriptor;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import libcore.util.EmptyArray;

@SystemApi
public final class PermissionControllerManager {
  private static final int CHUNK_SIZE = 4096;
  
  public static final int COUNT_ONLY_WHEN_GRANTED = 1;
  
  public static final int COUNT_WHEN_SYSTEM = 2;
  
  public static final int REASON_INSTALLER_POLICY_VIOLATION = 2;
  
  public static final int REASON_MALWARE = 1;
  
  private static final long REQUEST_TIMEOUT_MILLIS = 60000L;
  
  private static final String TAG = PermissionControllerManager.class.getSimpleName();
  
  private static final long UNBIND_TIMEOUT_MILLIS = 10000L;
  
  private static final Object sLock = new Object();
  
  private static ArrayMap<Pair<Integer, Thread>, ServiceConnector<IPermissionController>> sRemoteServices = new ArrayMap(1);
  
  private final Context mContext;
  
  private final Handler mHandler;
  
  private final ServiceConnector<IPermissionController> mRemoteService;
  
  @Retention(RetentionPolicy.SOURCE)
  class CountPermissionAppsFlag implements Annotation {}
  
  class OnCountPermissionAppsResultCallback {
    public abstract void onCountPermissionApps(int param1Int);
  }
  
  class OnGetAppPermissionResultCallback {
    public abstract void onGetAppPermissions(List<RuntimePermissionPresentationInfo> param1List);
  }
  
  public static interface OnPermissionUsageResultCallback {
    void onPermissionUsageResult(List<RuntimePermissionUsageInfo> param1List);
  }
  
  public static abstract class OnRevokeRuntimePermissionsCallback {
    public abstract void onRevokeRuntimePermissions(Map<String, List<String>> param1Map);
  }
  
  public PermissionControllerManager(Context paramContext, Handler paramHandler) {
    Object object = sLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      Pair pair = new Pair();
      int i = paramContext.getUserId();
      this(Integer.valueOf(i), paramHandler.getLooper().getThread());
      ServiceConnector serviceConnector = (ServiceConnector)sRemoteServices.get(pair);
      Object object1 = serviceConnector;
      if (serviceConnector == null) {
        Intent intent1 = new Intent();
        this("android.permission.PermissionControllerService");
        intent1.setPackage(paramContext.getPackageManager().getPermissionControllerPackageName());
        ResolveInfo resolveInfo = paramContext.getPackageManager().resolveService(intent1, 0);
        object1 = new Object();
        Application application = ActivityThread.currentApplication();
        Intent intent2 = new Intent();
        this("android.permission.PermissionControllerService");
        intent2 = intent2.setComponent(resolveInfo.getComponentInfo().getComponentName());
        super(this, (Context)application, intent2, 0, paramContext.getUserId(), (Function)_$$Lambda$ViMr_PAGHrCLBQPYNzqdYUNU5zI.INSTANCE, paramHandler);
        sRemoteServices.put(pair, object1);
      } 
      this.mRemoteService = (ServiceConnector<IPermissionController>)object1;
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      this.mContext = paramContext;
      return;
    } finally {
      paramContext = null;
    } 
  }
  
  private void enforceSomePermissionsGrantedToSelf(String... paramVarArgs) {
    int i;
    byte b;
    for (i = paramVarArgs.length, b = 0; b < i; ) {
      String str = paramVarArgs[b];
      if (this.mContext.checkSelfPermission(str) == 0)
        return; 
      b++;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("At lest one of the following permissions is required: ");
    stringBuilder.append(Arrays.toString((Object[])paramVarArgs));
    throw new SecurityException(stringBuilder.toString());
  }
  
  public void revokeRuntimePermissions(Map<String, List<String>> paramMap, boolean paramBoolean, int paramInt, Executor paramExecutor, OnRevokeRuntimePermissionsCallback paramOnRevokeRuntimePermissionsCallback) {
    Preconditions.checkNotNull(paramExecutor);
    Preconditions.checkNotNull(paramOnRevokeRuntimePermissionsCallback);
    Preconditions.checkNotNull(paramMap);
    for (Map.Entry<String, List<String>> entry : paramMap.entrySet()) {
      Preconditions.checkNotNull(entry.getKey());
      Preconditions.checkCollectionElementsNotNull((List)entry.getValue(), "permissions");
    } 
    enforceSomePermissionsGrantedToSelf(new String[] { "android.permission.REVOKE_RUNTIME_PERMISSIONS" });
    AndroidFuture androidFuture = this.mRemoteService.postAsync(new _$$Lambda$PermissionControllerManager$_00c_YE4DXfkvjweKlMro3pl_G8(this, paramMap, paramBoolean, paramInt));
    _$$Lambda$PermissionControllerManager$EuAUoa5oQvhIEr2FodjehlRNmHY _$$Lambda$PermissionControllerManager$EuAUoa5oQvhIEr2FodjehlRNmHY = new _$$Lambda$PermissionControllerManager$EuAUoa5oQvhIEr2FodjehlRNmHY(paramOnRevokeRuntimePermissionsCallback);
    androidFuture.whenCompleteAsync(_$$Lambda$PermissionControllerManager$EuAUoa5oQvhIEr2FodjehlRNmHY, paramExecutor);
  }
  
  public void setRuntimePermissionGrantStateByDeviceAdmin(String paramString1, String paramString2, String paramString3, int paramInt, Executor paramExecutor, Consumer<Boolean> paramConsumer) {
    Preconditions.checkStringNotEmpty(paramString1);
    Preconditions.checkStringNotEmpty(paramString2);
    Preconditions.checkStringNotEmpty(paramString3);
    boolean bool1 = true, bool2 = bool1;
    if (paramInt != 1) {
      bool2 = bool1;
      if (paramInt != 2)
        if (paramInt == 0) {
          bool2 = bool1;
        } else {
          bool2 = false;
        }  
    } 
    Preconditions.checkArgument(bool2);
    Preconditions.checkNotNull(paramExecutor);
    Preconditions.checkNotNull(paramConsumer);
    AndroidFuture androidFuture = this.mRemoteService.postAsync(new _$$Lambda$PermissionControllerManager$ESMSOXHMTX1zwjBZlBwmbWKWJso(paramString1, paramString2, paramString3, paramInt));
    _$$Lambda$PermissionControllerManager$nFhNDDZWDMvY1EMapbsu7_rktxg _$$Lambda$PermissionControllerManager$nFhNDDZWDMvY1EMapbsu7_rktxg = new _$$Lambda$PermissionControllerManager$nFhNDDZWDMvY1EMapbsu7_rktxg(paramString2, paramConsumer);
    androidFuture.whenCompleteAsync(_$$Lambda$PermissionControllerManager$nFhNDDZWDMvY1EMapbsu7_rktxg, paramExecutor);
  }
  
  public void getRuntimePermissionBackup(UserHandle paramUserHandle, Executor paramExecutor, Consumer<byte[]> paramConsumer) {
    Preconditions.checkNotNull(paramUserHandle);
    Preconditions.checkNotNull(paramExecutor);
    Preconditions.checkNotNull(paramConsumer);
    enforceSomePermissionsGrantedToSelf(new String[] { "android.permission.GET_RUNTIME_PERMISSIONS" });
    AndroidFuture androidFuture = this.mRemoteService.postAsync(new _$$Lambda$PermissionControllerManager$Iy_7wiKMCV_MFSPGyIJxP_DSf8E(paramUserHandle));
    _$$Lambda$PermissionControllerManager$wPNqW0yZff7KXoWmrKVyzMgY2jc _$$Lambda$PermissionControllerManager$wPNqW0yZff7KXoWmrKVyzMgY2jc = new _$$Lambda$PermissionControllerManager$wPNqW0yZff7KXoWmrKVyzMgY2jc(paramConsumer);
    androidFuture.whenCompleteAsync(_$$Lambda$PermissionControllerManager$wPNqW0yZff7KXoWmrKVyzMgY2jc, paramExecutor);
  }
  
  public void stageAndApplyRuntimePermissionsBackup(byte[] paramArrayOfbyte, UserHandle paramUserHandle) {
    Preconditions.checkNotNull(paramArrayOfbyte);
    Preconditions.checkNotNull(paramUserHandle);
    enforceSomePermissionsGrantedToSelf(new String[] { "android.permission.GRANT_RUNTIME_PERMISSIONS", "android.permission.RESTORE_RUNTIME_PERMISSIONS" });
    AndroidFuture androidFuture = this.mRemoteService.postAsync(new _$$Lambda$PermissionControllerManager$DtZA5ILHLibh9MBqPQNGrWZ0CSw(paramUserHandle, paramArrayOfbyte));
    -$.Lambda.PermissionControllerManager.S2VSU3Pp--uq4UNaUiz9gMU65xU s2VSU3Pp--uq4UNaUiz9gMU65xU = _$$Lambda$PermissionControllerManager$S2VSU3Pp__uq4UNaUiz9gMU65xU.INSTANCE;
    androidFuture.whenComplete((BiConsumer)s2VSU3Pp--uq4UNaUiz9gMU65xU);
  }
  
  public void applyStagedRuntimePermissionBackup(String paramString, UserHandle paramUserHandle, Executor paramExecutor, Consumer<Boolean> paramConsumer) {
    Preconditions.checkNotNull(paramString);
    Preconditions.checkNotNull(paramUserHandle);
    Preconditions.checkNotNull(paramExecutor);
    Preconditions.checkNotNull(paramConsumer);
    enforceSomePermissionsGrantedToSelf(new String[] { "android.permission.GRANT_RUNTIME_PERMISSIONS", "android.permission.RESTORE_RUNTIME_PERMISSIONS" });
    AndroidFuture androidFuture = this.mRemoteService.postAsync(new _$$Lambda$PermissionControllerManager$2gyb4miANgsuR_Cn3HPTnP6sL54(paramString, paramUserHandle));
    _$$Lambda$PermissionControllerManager$vBYanTuMAWBbfOp_XdHzQXYNpXY _$$Lambda$PermissionControllerManager$vBYanTuMAWBbfOp_XdHzQXYNpXY = new _$$Lambda$PermissionControllerManager$vBYanTuMAWBbfOp_XdHzQXYNpXY(paramString, paramConsumer);
    androidFuture.whenCompleteAsync(_$$Lambda$PermissionControllerManager$vBYanTuMAWBbfOp_XdHzQXYNpXY, paramExecutor);
  }
  
  public void dump(FileDescriptor paramFileDescriptor, String[] paramArrayOfString) {
    try {
      ServiceConnector<IPermissionController> serviceConnector = this.mRemoteService;
      _$$Lambda$PermissionControllerManager$QKXRZuAcOE3JWRlH0VmPcShX4LM _$$Lambda$PermissionControllerManager$QKXRZuAcOE3JWRlH0VmPcShX4LM = new _$$Lambda$PermissionControllerManager$QKXRZuAcOE3JWRlH0VmPcShX4LM();
      this(paramFileDescriptor, paramArrayOfString);
      AndroidFuture androidFuture = serviceConnector.post(_$$Lambda$PermissionControllerManager$QKXRZuAcOE3JWRlH0VmPcShX4LM);
      TimeUnit timeUnit = TimeUnit.MILLISECONDS;
      androidFuture.get(60000L, timeUnit);
    } catch (Exception exception) {
      Log.e(TAG, "Could not get dump", exception);
    } 
  }
  
  public void getAppPermissions(String paramString, OnGetAppPermissionResultCallback paramOnGetAppPermissionResultCallback, Handler paramHandler) {
    Preconditions.checkNotNull(paramString);
    Preconditions.checkNotNull(paramOnGetAppPermissionResultCallback);
    if (paramHandler == null)
      paramHandler = this.mHandler; 
    AndroidFuture androidFuture = this.mRemoteService.postAsync(new _$$Lambda$PermissionControllerManager$qbjquKkizobHfe5rpkz_pBZGMm0(paramString));
    _$$Lambda$PermissionControllerManager$6Ag5ugR0frhyp44qzIn5jlbtZRg _$$Lambda$PermissionControllerManager$6Ag5ugR0frhyp44qzIn5jlbtZRg = new _$$Lambda$PermissionControllerManager$6Ag5ugR0frhyp44qzIn5jlbtZRg(paramHandler, paramOnGetAppPermissionResultCallback);
    androidFuture.whenComplete(_$$Lambda$PermissionControllerManager$6Ag5ugR0frhyp44qzIn5jlbtZRg);
  }
  
  public void revokeRuntimePermission(String paramString1, String paramString2) {
    Preconditions.checkNotNull(paramString1);
    Preconditions.checkNotNull(paramString2);
    this.mRemoteService.run(new _$$Lambda$PermissionControllerManager$fCp0CnfICduEKFO6T7fn3jMOSBI(paramString1, paramString2));
  }
  
  public void countPermissionApps(List<String> paramList, int paramInt, OnCountPermissionAppsResultCallback paramOnCountPermissionAppsResultCallback, Handler paramHandler) {
    Preconditions.checkCollectionElementsNotNull(paramList, "permissionNames");
    Preconditions.checkFlagsArgument(paramInt, 3);
    Preconditions.checkNotNull(paramOnCountPermissionAppsResultCallback);
    if (paramHandler == null)
      paramHandler = this.mHandler; 
    AndroidFuture androidFuture = this.mRemoteService.postAsync(new _$$Lambda$PermissionControllerManager$F3XGmg9VDZwhqc9cSceeDfZkCBo(paramList, paramInt));
    _$$Lambda$PermissionControllerManager$3DXENZY34HBEEqo36SfSnKNAHK4 _$$Lambda$PermissionControllerManager$3DXENZY34HBEEqo36SfSnKNAHK4 = new _$$Lambda$PermissionControllerManager$3DXENZY34HBEEqo36SfSnKNAHK4(paramHandler, paramOnCountPermissionAppsResultCallback);
    androidFuture.whenComplete(_$$Lambda$PermissionControllerManager$3DXENZY34HBEEqo36SfSnKNAHK4);
  }
  
  public void getPermissionUsages(boolean paramBoolean, long paramLong, Executor paramExecutor, OnPermissionUsageResultCallback paramOnPermissionUsageResultCallback) {
    Preconditions.checkArgumentNonnegative(paramLong);
    Preconditions.checkNotNull(paramExecutor);
    Preconditions.checkNotNull(paramOnPermissionUsageResultCallback);
    AndroidFuture androidFuture = this.mRemoteService.postAsync(new _$$Lambda$PermissionControllerManager$qxGLAK8X0GMfe_vQr0NFGapPRN4(paramBoolean, paramLong));
    _$$Lambda$PermissionControllerManager$bTHE_ox64GS5Ys00_8kOUGjAWhY _$$Lambda$PermissionControllerManager$bTHE_ox64GS5Ys00_8kOUGjAWhY = new _$$Lambda$PermissionControllerManager$bTHE_ox64GS5Ys00_8kOUGjAWhY(paramOnPermissionUsageResultCallback);
    androidFuture.whenCompleteAsync(_$$Lambda$PermissionControllerManager$bTHE_ox64GS5Ys00_8kOUGjAWhY, paramExecutor);
  }
  
  public void grantOrUpgradeDefaultRuntimePermissions(Executor paramExecutor, Consumer<Boolean> paramConsumer) {
    AndroidFuture androidFuture = this.mRemoteService.postAsync((ServiceConnector.Job)_$$Lambda$PermissionControllerManager$cn7Tw4pO8bRiaLbV2UGd6fgcSO4.INSTANCE);
    _$$Lambda$PermissionControllerManager$iZg46VI1PF4HbTl45C2Rv0XwOm0 _$$Lambda$PermissionControllerManager$iZg46VI1PF4HbTl45C2Rv0XwOm0 = new _$$Lambda$PermissionControllerManager$iZg46VI1PF4HbTl45C2Rv0XwOm0(paramConsumer);
    androidFuture.whenCompleteAsync(_$$Lambda$PermissionControllerManager$iZg46VI1PF4HbTl45C2Rv0XwOm0, paramExecutor);
  }
  
  public void updateUserSensitive() {
    updateUserSensitiveForApp(-1);
  }
  
  public void updateUserSensitiveForApp(int paramInt) {
    AndroidFuture androidFuture = this.mRemoteService.postAsync(new _$$Lambda$PermissionControllerManager$MqIts8mfNZAqQWR1_qc2_3bh08M(paramInt));
    _$$Lambda$PermissionControllerManager$LeLHpbJU3pY1PZAWD3x9Fi9QxHI _$$Lambda$PermissionControllerManager$LeLHpbJU3pY1PZAWD3x9Fi9QxHI = new _$$Lambda$PermissionControllerManager$LeLHpbJU3pY1PZAWD3x9Fi9QxHI(paramInt);
    androidFuture.whenComplete(_$$Lambda$PermissionControllerManager$LeLHpbJU3pY1PZAWD3x9Fi9QxHI);
  }
  
  public void notifyOneTimePermissionSessionTimeout(String paramString) {
    this.mRemoteService.run(new _$$Lambda$PermissionControllerManager$MqYq3LpSVm_2tnlerpH7tgxg0Us(paramString));
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Reason {}
}
