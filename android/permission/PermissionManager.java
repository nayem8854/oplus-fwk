package android.permission;

import android.annotation.SystemApi;
import android.app.ActivityManager;
import android.app.ActivityThread;
import android.app.IActivityManager;
import android.app.PropertyInvalidatedCache;
import android.content.Context;
import android.content.pm.IPackageManager;
import android.content.pm.permission.SplitPermissionInfoParcelable;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.util.Slog;
import com.android.internal.util.CollectionUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.function.Consumer;

@SystemApi
public final class PermissionManager {
  public static final String CACHE_KEY_PACKAGE_INFO = "cache_key.package_info";
  
  public static final String KILL_APP_REASON_GIDS_CHANGED = "permission grant or revoke changed gids";
  
  public static final String KILL_APP_REASON_PERMISSIONS_REVOKED = "permissions revoked";
  
  private static final String TAG = PermissionManager.class.getName();
  
  private static PropertyInvalidatedCache<PackageNamePermissionQuery, Integer> sPackageNamePermissionCache;
  
  public PermissionManager(Context paramContext, IPackageManager paramIPackageManager) throws ServiceManager.ServiceNotFoundException {
    this(paramContext, paramIPackageManager, IPermissionManager.Stub.asInterface(iBinder));
  }
  
  public PermissionManager(Context paramContext, IPackageManager paramIPackageManager, IPermissionManager paramIPermissionManager) {
    this.mContext = paramContext;
    this.mPackageManager = paramIPackageManager;
    this.mPermissionManager = paramIPermissionManager;
  }
  
  @SystemApi
  public int getRuntimePermissionsVersion() {
    try {
      return this.mPackageManager.getRuntimePermissionsVersion(this.mContext.getUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void setRuntimePermissionsVersion(int paramInt) {
    try {
      this.mPackageManager.setRuntimePermissionsVersion(paramInt, this.mContext.getUserId());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<SplitPermissionInfo> getSplitPermissions() {
    List<SplitPermissionInfo> list = this.mSplitPermissionInfos;
    if (list != null)
      return list; 
    try {
      list = (List)ActivityThread.getPermissionManager().getSplitPermissions();
      this.mSplitPermissionInfos = list = splitPermissionInfoListToNonParcelableList((List)list);
      return list;
    } catch (RemoteException remoteException) {
      Slog.e(TAG, "Error getting split permissions", (Throwable)remoteException);
      return Collections.emptyList();
    } 
  }
  
  public void grantDefaultPermissionsToLuiApp(String paramString, UserHandle paramUserHandle, Executor paramExecutor, Consumer<Boolean> paramConsumer) {
    try {
      IPermissionManager iPermissionManager = this.mPermissionManager;
      int i = paramUserHandle.getIdentifier();
      iPermissionManager.grantDefaultPermissionsToActiveLuiApp(paramString, i);
      _$$Lambda$PermissionManager$0l4DEq2HXCCVCBUfdxUjWk3qCIU _$$Lambda$PermissionManager$0l4DEq2HXCCVCBUfdxUjWk3qCIU = new _$$Lambda$PermissionManager$0l4DEq2HXCCVCBUfdxUjWk3qCIU();
      this(paramConsumer);
      paramExecutor.execute(_$$Lambda$PermissionManager$0l4DEq2HXCCVCBUfdxUjWk3qCIU);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void revokeDefaultPermissionsFromLuiApps(String[] paramArrayOfString, UserHandle paramUserHandle, Executor paramExecutor, Consumer<Boolean> paramConsumer) {
    try {
      IPermissionManager iPermissionManager = this.mPermissionManager;
      int i = paramUserHandle.getIdentifier();
      iPermissionManager.revokeDefaultPermissionsFromLuiApps(paramArrayOfString, i);
      _$$Lambda$PermissionManager$06sjxg82u271BU6DexEeXFtA39s _$$Lambda$PermissionManager$06sjxg82u271BU6DexEeXFtA39s = new _$$Lambda$PermissionManager$06sjxg82u271BU6DexEeXFtA39s();
      this(paramConsumer);
      paramExecutor.execute(_$$Lambda$PermissionManager$06sjxg82u271BU6DexEeXFtA39s);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void grantDefaultPermissionsToEnabledImsServices(String[] paramArrayOfString, UserHandle paramUserHandle, Executor paramExecutor, Consumer<Boolean> paramConsumer) {
    try {
      IPermissionManager iPermissionManager = this.mPermissionManager;
      int i = paramUserHandle.getIdentifier();
      iPermissionManager.grantDefaultPermissionsToEnabledImsServices(paramArrayOfString, i);
      _$$Lambda$PermissionManager$614OrkjHJQ8919GHESV4U3WEYEY _$$Lambda$PermissionManager$614OrkjHJQ8919GHESV4U3WEYEY = new _$$Lambda$PermissionManager$614OrkjHJQ8919GHESV4U3WEYEY();
      this(paramConsumer);
      paramExecutor.execute(_$$Lambda$PermissionManager$614OrkjHJQ8919GHESV4U3WEYEY);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void grantDefaultPermissionsToEnabledTelephonyDataServices(String[] paramArrayOfString, UserHandle paramUserHandle, Executor paramExecutor, Consumer<Boolean> paramConsumer) {
    try {
      IPermissionManager iPermissionManager = this.mPermissionManager;
      int i = paramUserHandle.getIdentifier();
      iPermissionManager.grantDefaultPermissionsToEnabledTelephonyDataServices(paramArrayOfString, i);
      _$$Lambda$PermissionManager$akRjCijSz__ZCVgA58pgpRUIzHU _$$Lambda$PermissionManager$akRjCijSz__ZCVgA58pgpRUIzHU = new _$$Lambda$PermissionManager$akRjCijSz__ZCVgA58pgpRUIzHU();
      this(paramConsumer);
      paramExecutor.execute(_$$Lambda$PermissionManager$akRjCijSz__ZCVgA58pgpRUIzHU);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void revokeDefaultPermissionsFromDisabledTelephonyDataServices(String[] paramArrayOfString, UserHandle paramUserHandle, Executor paramExecutor, Consumer<Boolean> paramConsumer) {
    try {
      IPermissionManager iPermissionManager = this.mPermissionManager;
      int i = paramUserHandle.getIdentifier();
      iPermissionManager.revokeDefaultPermissionsFromDisabledTelephonyDataServices(paramArrayOfString, i);
      _$$Lambda$PermissionManager$DhpssISwP0aRU4I94NcrcBz5vI0 _$$Lambda$PermissionManager$DhpssISwP0aRU4I94NcrcBz5vI0 = new _$$Lambda$PermissionManager$DhpssISwP0aRU4I94NcrcBz5vI0();
      this(paramConsumer);
      paramExecutor.execute(_$$Lambda$PermissionManager$DhpssISwP0aRU4I94NcrcBz5vI0);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void grantDefaultPermissionsToEnabledCarrierApps(String[] paramArrayOfString, UserHandle paramUserHandle, Executor paramExecutor, Consumer<Boolean> paramConsumer) {
    try {
      IPermissionManager iPermissionManager = this.mPermissionManager;
      int i = paramUserHandle.getIdentifier();
      iPermissionManager.grantDefaultPermissionsToEnabledCarrierApps(paramArrayOfString, i);
      _$$Lambda$PermissionManager$FyB4K5KfEKqwp_uJUXvNLbTkmAg _$$Lambda$PermissionManager$FyB4K5KfEKqwp_uJUXvNLbTkmAg = new _$$Lambda$PermissionManager$FyB4K5KfEKqwp_uJUXvNLbTkmAg();
      this(paramConsumer);
      paramExecutor.execute(_$$Lambda$PermissionManager$FyB4K5KfEKqwp_uJUXvNLbTkmAg);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public Set<String> getAutoRevokeExemptionRequestedPackages() {
    try {
      IPermissionManager iPermissionManager = this.mPermissionManager;
      Context context = this.mContext;
      int i = context.getUser().getIdentifier();
      return CollectionUtils.toSet(iPermissionManager.getAutoRevokeExemptionRequestedPackages(i));
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public Set<String> getAutoRevokeExemptionGrantedPackages() {
    try {
      IPermissionManager iPermissionManager = this.mPermissionManager;
      Context context = this.mContext;
      int i = context.getUser().getIdentifier();
      return CollectionUtils.toSet(iPermissionManager.getAutoRevokeExemptionGrantedPackages(i));
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private List<SplitPermissionInfo> splitPermissionInfoListToNonParcelableList(List<SplitPermissionInfoParcelable> paramList) {
    int i = paramList.size();
    ArrayList<SplitPermissionInfo> arrayList = new ArrayList(i);
    for (byte b = 0; b < i; b++)
      arrayList.add(new SplitPermissionInfo(paramList.get(b))); 
    return arrayList;
  }
  
  public static List<SplitPermissionInfoParcelable> splitPermissionInfoListToParcelableList(List<SplitPermissionInfo> paramList) {
    int i = paramList.size();
    ArrayList<SplitPermissionInfoParcelable> arrayList = new ArrayList(i);
    for (byte b = 0; b < i; b++) {
      SplitPermissionInfo splitPermissionInfo = paramList.get(b);
      SplitPermissionInfoParcelable splitPermissionInfoParcelable = new SplitPermissionInfoParcelable(splitPermissionInfo.getSplitPermission(), splitPermissionInfo.getNewPermissions(), splitPermissionInfo.getTargetSdk());
      arrayList.add(splitPermissionInfoParcelable);
    } 
    return arrayList;
  }
  
  public static final class SplitPermissionInfo {
    private final SplitPermissionInfoParcelable mSplitPermissionInfoParcelable;
    
    public boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      return this.mSplitPermissionInfoParcelable.equals(((SplitPermissionInfo)param1Object).mSplitPermissionInfoParcelable);
    }
    
    public int hashCode() {
      return this.mSplitPermissionInfoParcelable.hashCode();
    }
    
    public String getSplitPermission() {
      return this.mSplitPermissionInfoParcelable.getSplitPermission();
    }
    
    public List<String> getNewPermissions() {
      return this.mSplitPermissionInfoParcelable.getNewPermissions();
    }
    
    public int getTargetSdk() {
      return this.mSplitPermissionInfoParcelable.getTargetSdk();
    }
    
    public SplitPermissionInfo(String param1String, List<String> param1List, int param1Int) {
      this(new SplitPermissionInfoParcelable(param1String, param1List, param1Int));
    }
    
    private SplitPermissionInfo(SplitPermissionInfoParcelable param1SplitPermissionInfoParcelable) {
      this.mSplitPermissionInfoParcelable = param1SplitPermissionInfoParcelable;
    }
  }
  
  @SystemApi
  public void startOneTimePermissionSession(String paramString, long paramLong, int paramInt1, int paramInt2) {
    try {
      this.mPermissionManager.startOneTimePermissionSession(paramString, this.mContext.getUserId(), paramLong, paramInt1, paramInt2);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void stopOneTimePermissionSession(String paramString) {
    try {
      IPermissionManager iPermissionManager = this.mPermissionManager;
      Context context = this.mContext;
      int i = context.getUserId();
      iPermissionManager.stopOneTimePermissionSession(paramString, i);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public int checkDeviceIdentifierAccess(String paramString1, String paramString2, String paramString3, int paramInt1, int paramInt2) {
    try {
      return this.mPermissionManager.checkDeviceIdentifierAccess(paramString1, paramString2, paramString3, paramInt1, paramInt2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private static int checkPermissionUncached(String paramString, int paramInt1, int paramInt2) {
    String str;
    IActivityManager iActivityManager = ActivityManager.getService();
    if (iActivityManager == null) {
      paramInt1 = UserHandle.getAppId(paramInt2);
      if (paramInt1 == 0 || paramInt1 == 1000) {
        String str1 = TAG;
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Missing ActivityManager; assuming ");
        stringBuilder1.append(paramInt2);
        stringBuilder1.append(" holds ");
        stringBuilder1.append(paramString);
        Slog.w(str1, stringBuilder1.toString());
        return 0;
      } 
      str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Missing ActivityManager; assuming ");
      stringBuilder.append(paramInt2);
      stringBuilder.append(" does not hold ");
      stringBuilder.append(paramString);
      Slog.w(str, stringBuilder.toString());
      return -1;
    } 
    try {
      return str.checkPermission(paramString, paramInt1, paramInt2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private static final class PermissionQuery {
    final String permission;
    
    final int pid;
    
    final int uid;
    
    PermissionQuery(String param1String, int param1Int1, int param1Int2) {
      this.permission = param1String;
      this.pid = param1Int1;
      this.uid = param1Int2;
    }
    
    public String toString() {
      String str = this.permission;
      int i = this.pid;
      int j = this.uid;
      return String.format("PermissionQuery(permission=\"%s\", pid=%s, uid=%s)", new Object[] { str, Integer.valueOf(i), Integer.valueOf(j) });
    }
    
    public int hashCode() {
      int i = Objects.hashCode(this.permission);
      int j = Objects.hashCode(Integer.valueOf(this.uid));
      return i * 13 + j;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = false;
      if (param1Object == null)
        return false; 
      try {
        PermissionQuery permissionQuery = (PermissionQuery)param1Object;
        if (this.uid == permissionQuery.uid) {
          param1Object = this.permission;
          String str = permissionQuery.permission;
          if (Objects.equals(param1Object, str))
            bool = true; 
        } 
        return bool;
      } catch (ClassCastException classCastException) {
        return false;
      } 
    }
  }
  
  private static final PropertyInvalidatedCache<PermissionQuery, Integer> sPermissionCache = (PropertyInvalidatedCache<PermissionQuery, Integer>)new Object(16, "cache_key.package_info");
  
  private final Context mContext;
  
  private final IPackageManager mPackageManager;
  
  private final IPermissionManager mPermissionManager;
  
  private List<SplitPermissionInfo> mSplitPermissionInfos;
  
  public static int checkPermission(String paramString, int paramInt1, int paramInt2) {
    return ((Integer)sPermissionCache.query(new PermissionQuery(paramString, paramInt1, paramInt2))).intValue();
  }
  
  public static void disablePermissionCache() {
    sPermissionCache.disableLocal();
  }
  
  private static final class PackageNamePermissionQuery {
    final String permName;
    
    final String pkgName;
    
    final int uid;
    
    PackageNamePermissionQuery(String param1String1, String param1String2, int param1Int) {
      this.permName = param1String1;
      this.pkgName = param1String2;
      this.uid = param1Int;
    }
    
    public String toString() {
      String str1 = this.pkgName, str2 = this.permName;
      int i = this.uid;
      return String.format("PackageNamePermissionQuery(pkgName=\"%s\", permName=\"%s, uid=%s\")", new Object[] { str1, str2, Integer.valueOf(i) });
    }
    
    public int hashCode() {
      return Objects.hash(new Object[] { this.permName, this.pkgName, Integer.valueOf(this.uid) });
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = false;
      if (param1Object == null)
        return false; 
      try {
        param1Object = param1Object;
        if (Objects.equals(this.permName, ((PackageNamePermissionQuery)param1Object).permName)) {
          String str1 = this.pkgName, str2 = ((PackageNamePermissionQuery)param1Object).pkgName;
          if (Objects.equals(str1, str2) && this.uid == ((PackageNamePermissionQuery)param1Object).uid)
            bool = true; 
        } 
        return bool;
      } catch (ClassCastException classCastException) {
        return false;
      } 
    }
  }
  
  private static int checkPackageNamePermissionUncached(String paramString1, String paramString2, int paramInt) {
    try {
      return ActivityThread.getPermissionManager().checkPermission(paramString1, paramString2, paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  static {
    sPackageNamePermissionCache = (PropertyInvalidatedCache<PackageNamePermissionQuery, Integer>)new Object(16, "cache_key.package_info");
  }
  
  public static int checkPackageNamePermission(String paramString1, String paramString2, int paramInt) {
    return ((Integer)sPackageNamePermissionCache.query(new PackageNamePermissionQuery(paramString1, paramString2, paramInt))).intValue();
  }
  
  public static void disablePackageNamePermissionCache() {
    sPackageNamePermissionCache.disableLocal();
  }
}
