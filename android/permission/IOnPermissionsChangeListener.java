package android.permission;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOnPermissionsChangeListener extends IInterface {
  void onPermissionsChanged(int paramInt) throws RemoteException;
  
  class Default implements IOnPermissionsChangeListener {
    public void onPermissionsChanged(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOnPermissionsChangeListener {
    private static final String DESCRIPTOR = "android.permission.IOnPermissionsChangeListener";
    
    static final int TRANSACTION_onPermissionsChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.permission.IOnPermissionsChangeListener");
    }
    
    public static IOnPermissionsChangeListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.permission.IOnPermissionsChangeListener");
      if (iInterface != null && iInterface instanceof IOnPermissionsChangeListener)
        return (IOnPermissionsChangeListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onPermissionsChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.permission.IOnPermissionsChangeListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.permission.IOnPermissionsChangeListener");
      param1Int1 = param1Parcel1.readInt();
      onPermissionsChanged(param1Int1);
      return true;
    }
    
    private static class Proxy implements IOnPermissionsChangeListener {
      public static IOnPermissionsChangeListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.permission.IOnPermissionsChangeListener";
      }
      
      public void onPermissionsChanged(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.permission.IOnPermissionsChangeListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IOnPermissionsChangeListener.Stub.getDefaultImpl() != null) {
            IOnPermissionsChangeListener.Stub.getDefaultImpl().onPermissionsChanged(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOnPermissionsChangeListener param1IOnPermissionsChangeListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOnPermissionsChangeListener != null) {
          Proxy.sDefaultImpl = param1IOnPermissionsChangeListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOnPermissionsChangeListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
