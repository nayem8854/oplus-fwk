package android.permission;

import android.os.UserHandle;
import com.android.internal.util.function.TriFunction;
import java.util.function.BiFunction;

public abstract class PermissionManagerInternal {
  public abstract void addOnRuntimePermissionStateChangedListener(OnRuntimePermissionStateChangedListener paramOnRuntimePermissionStateChangedListener);
  
  public abstract byte[] backupRuntimePermissions(UserHandle paramUserHandle);
  
  public abstract void removeOnRuntimePermissionStateChangedListener(OnRuntimePermissionStateChangedListener paramOnRuntimePermissionStateChangedListener);
  
  public abstract void restoreDelayedRuntimePermissions(String paramString, UserHandle paramUserHandle);
  
  public abstract void restoreRuntimePermissions(byte[] paramArrayOfbyte, UserHandle paramUserHandle);
  
  public static interface CheckPermissionDelegate {
    int checkPermission(String param1String1, String param1String2, int param1Int, TriFunction<String, String, Integer, Integer> param1TriFunction);
    
    int checkUidPermission(String param1String, int param1Int, BiFunction<String, Integer, Integer> param1BiFunction);
  }
  
  public static interface OnRuntimePermissionStateChangedListener {
    void onRuntimePermissionStateChanged(String param1String, int param1Int);
  }
}
