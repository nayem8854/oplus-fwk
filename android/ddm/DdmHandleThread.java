package android.ddm;

import java.nio.ByteBuffer;
import org.apache.harmony.dalvik.ddmc.Chunk;
import org.apache.harmony.dalvik.ddmc.ChunkHandler;
import org.apache.harmony.dalvik.ddmc.DdmServer;
import org.apache.harmony.dalvik.ddmc.DdmVmInternal;

public class DdmHandleThread extends ChunkHandler {
  public static final int CHUNK_STKL;
  
  public static final int CHUNK_THCR;
  
  public static final int CHUNK_THDE;
  
  public static final int CHUNK_THEN = type("THEN");
  
  public static final int CHUNK_THST;
  
  private static DdmHandleThread mInstance;
  
  static {
    CHUNK_THCR = type("THCR");
    CHUNK_THDE = type("THDE");
    CHUNK_THST = type("THST");
    CHUNK_STKL = type("STKL");
    mInstance = new DdmHandleThread();
  }
  
  public static void register() {
    DdmServer.registerHandler(CHUNK_THEN, mInstance);
    DdmServer.registerHandler(CHUNK_THST, mInstance);
    DdmServer.registerHandler(CHUNK_STKL, mInstance);
  }
  
  public void connected() {}
  
  public void disconnected() {}
  
  public Chunk handleChunk(Chunk paramChunk) {
    int i = paramChunk.type;
    if (i == CHUNK_THEN)
      return handleTHEN(paramChunk); 
    if (i == CHUNK_THST)
      return handleTHST(paramChunk); 
    if (i == CHUNK_STKL)
      return handleSTKL(paramChunk); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unknown packet ");
    stringBuilder.append(ChunkHandler.name(i));
    throw new RuntimeException(stringBuilder.toString());
  }
  
  private Chunk handleTHEN(Chunk paramChunk) {
    boolean bool;
    ByteBuffer byteBuffer = wrapChunk(paramChunk);
    if (byteBuffer.get() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    DdmVmInternal.threadNotify(bool);
    return null;
  }
  
  private Chunk handleTHST(Chunk paramChunk) {
    wrapChunk(paramChunk);
    byte[] arrayOfByte = DdmVmInternal.getThreadStats();
    if (arrayOfByte != null)
      return new Chunk(CHUNK_THST, arrayOfByte, 0, arrayOfByte.length); 
    return createFailChunk(1, "Can't build THST chunk");
  }
  
  private Chunk handleSTKL(Chunk paramChunk) {
    ByteBuffer byteBuffer = wrapChunk(paramChunk);
    int i = byteBuffer.getInt();
    StackTraceElement[] arrayOfStackTraceElement = DdmVmInternal.getStackTraceById(i);
    if (arrayOfStackTraceElement == null)
      return createFailChunk(1, "Stack trace unavailable"); 
    return createStackChunk(arrayOfStackTraceElement, i);
  }
  
  private Chunk createStackChunk(StackTraceElement[] paramArrayOfStackTraceElement, int paramInt) {
    int i = 0 + 4 + 4 + 4;
    int k;
    for (int j = paramArrayOfStackTraceElement.length; k < j; ) {
      StackTraceElement stackTraceElement = paramArrayOfStackTraceElement[k];
      int m = stackTraceElement.getClassName().length();
      int n = stackTraceElement.getMethodName().length();
      n = i + m * 2 + 4 + n * 2 + 4 + 4;
      i = n;
      if (stackTraceElement.getFileName() != null)
        i = n + stackTraceElement.getFileName().length() * 2; 
      i += 4;
      k++;
    } 
    ByteBuffer byteBuffer = ByteBuffer.allocate(i);
    byteBuffer.putInt(0);
    byteBuffer.putInt(paramInt);
    byteBuffer.putInt(paramArrayOfStackTraceElement.length);
    for (k = paramArrayOfStackTraceElement.length, paramInt = 0; paramInt < k; ) {
      StackTraceElement stackTraceElement = paramArrayOfStackTraceElement[paramInt];
      byteBuffer.putInt(stackTraceElement.getClassName().length());
      putString(byteBuffer, stackTraceElement.getClassName());
      byteBuffer.putInt(stackTraceElement.getMethodName().length());
      putString(byteBuffer, stackTraceElement.getMethodName());
      if (stackTraceElement.getFileName() != null) {
        byteBuffer.putInt(stackTraceElement.getFileName().length());
        putString(byteBuffer, stackTraceElement.getFileName());
      } else {
        byteBuffer.putInt(0);
      } 
      byteBuffer.putInt(stackTraceElement.getLineNumber());
      paramInt++;
    } 
    return new Chunk(CHUNK_STKL, byteBuffer);
  }
}
