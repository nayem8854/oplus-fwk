package android.webkit;

import android.content.Context;
import android.util.Log;

class LegacyErrorStrings {
  private static final String LOGTAG = "Http";
  
  static String getString(int paramInt, Context paramContext) {
    return paramContext.getText(getResource(paramInt)).toString();
  }
  
  private static int getResource(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Using generic message for unknown error code: ");
        stringBuilder.append(paramInt);
        Log.w("Http", stringBuilder.toString());
        return 17040311;
      case 0:
        return 17040319;
      case -1:
        return 17040311;
      case -2:
        return 17040318;
      case -3:
        return 17040324;
      case -4:
        return 17040312;
      case -5:
        return 17040320;
      case -6:
        return 17040313;
      case -7:
        return 17040317;
      case -8:
        return 17040322;
      case -9:
        return 17040321;
      case -10:
        return 17039368;
      case -11:
        return 17040314;
      case -12:
        return 17039367;
      case -13:
        return 17040315;
      case -14:
        return 17040316;
      case -15:
        break;
    } 
    return 17040323;
  }
}
