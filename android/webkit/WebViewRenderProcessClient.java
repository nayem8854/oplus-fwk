package android.webkit;

public abstract class WebViewRenderProcessClient {
  public abstract void onRenderProcessResponsive(WebView paramWebView, WebViewRenderProcess paramWebViewRenderProcess);
  
  public abstract void onRenderProcessUnresponsive(WebView paramWebView, WebViewRenderProcess paramWebViewRenderProcess);
}
