package android.webkit;

import android.annotation.SystemApi;
import android.app.AppGlobals;
import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.Context;
import android.content.Intent;
import android.content.pm.OplusPackageManager;
import android.content.pm.PackageInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.http.SslCertificate;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.StrictMode;
import android.print.PrintDocumentAdapter;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.DragEvent;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup;
import android.view.ViewHierarchyEncoder;
import android.view.ViewStructure;
import android.view.ViewTreeObserver;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import android.view.autofill.AutofillValue;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.textclassifier.TextClassifier;
import android.widget.AbsoluteLayout;
import com.oplus.darkmode.IOplusDarkModeManager;
import java.io.BufferedWriter;
import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

public class WebView extends AbsoluteLayout implements ViewTreeObserver.OnGlobalFocusChangeListener, ViewGroup.OnHierarchyChangeListener, ViewDebug.HierarchyHandler {
  private static final String LOGTAG = "WebView";
  
  public static final int RENDERER_PRIORITY_BOUND = 1;
  
  public static final int RENDERER_PRIORITY_IMPORTANT = 2;
  
  public static final int RENDERER_PRIORITY_WAIVED = 0;
  
  public static final String SCHEME_GEO = "geo:0,0?q=";
  
  public static final String SCHEME_MAILTO = "mailto:";
  
  public static final String SCHEME_TEL = "tel:";
  
  private static volatile boolean sEnforceThreadChecking = false;
  
  private FindListenerDistributor mFindListener;
  
  private WebViewProvider mProvider;
  
  private final Looper mWebViewThread;
  
  class WebViewTransport {
    private WebView mWebview;
    
    final WebView this$0;
    
    public void setWebView(WebView param1WebView) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: aload_1
      //   4: putfield mWebview : Landroid/webkit/WebView;
      //   7: aload_0
      //   8: monitorexit
      //   9: return
      //   10: astore_1
      //   11: aload_0
      //   12: monitorexit
      //   13: aload_1
      //   14: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #138	-> 2
      //   #139	-> 7
      //   #137	-> 10
      // Exception table:
      //   from	to	target	type
      //   2	7	10	finally
    }
    
    public WebView getWebView() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mWebview : Landroid/webkit/WebView;
      //   6: astore_1
      //   7: aload_0
      //   8: monitorexit
      //   9: aload_1
      //   10: areturn
      //   11: astore_1
      //   12: aload_0
      //   13: monitorexit
      //   14: aload_1
      //   15: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #148	-> 2
      //   #148	-> 11
      // Exception table:
      //   from	to	target	type
      //   2	7	11	finally
    }
  }
  
  class VisualStateCallback {
    public abstract void onComplete(long param1Long);
  }
  
  class HitTestResult {
    @Deprecated
    public static final int ANCHOR_TYPE = 1;
    
    public static final int EDIT_TEXT_TYPE = 9;
    
    public static final int EMAIL_TYPE = 4;
    
    public static final int GEO_TYPE = 3;
    
    @Deprecated
    public static final int IMAGE_ANCHOR_TYPE = 6;
    
    public static final int IMAGE_TYPE = 5;
    
    public static final int PHONE_TYPE = 2;
    
    public static final int SRC_ANCHOR_TYPE = 7;
    
    public static final int SRC_IMAGE_ANCHOR_TYPE = 8;
    
    public static final int UNKNOWN_TYPE = 0;
    
    private String mExtra;
    
    private int mType = 0;
    
    @SystemApi
    public void setType(int param1Int) {
      this.mType = param1Int;
    }
    
    @SystemApi
    public void setExtra(String param1String) {
      this.mExtra = param1String;
    }
    
    public int getType() {
      return this.mType;
    }
    
    public String getExtra() {
      return this.mExtra;
    }
  }
  
  public WebView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public WebView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842885);
  }
  
  public WebView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public WebView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    this(paramContext, paramAttributeSet, paramInt1, paramInt2, (Map<String, Object>)null, false);
  }
  
  @Deprecated
  public WebView(Context paramContext, AttributeSet paramAttributeSet, int paramInt, boolean paramBoolean) {
    this(paramContext, paramAttributeSet, paramInt, 0, (Map<String, Object>)null, paramBoolean);
  }
  
  protected WebView(Context paramContext, AttributeSet paramAttributeSet, int paramInt, Map<String, Object> paramMap, boolean paramBoolean) {
    this(paramContext, paramAttributeSet, paramInt, 0, paramMap, paramBoolean);
  }
  
  protected WebView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2, Map<String, Object> paramMap, boolean paramBoolean) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    this.mWebViewThread = Looper.myLooper();
    paramInt1 = getImportantForAutofill();
    boolean bool = true;
    if (paramInt1 == 0)
      setImportantForAutofill(1); 
    if (getImportantForContentCapture() == 0)
      setImportantForContentCapture(1); 
    if (paramContext != null) {
      if (this.mWebViewThread != null) {
        if ((paramContext.getApplicationInfo()).targetSdkVersion < 18)
          bool = false; 
        sEnforceThreadChecking = bool;
        checkThread();
        ensureProviderCreated();
        this.mProvider.init(paramMap, paramBoolean);
        CookieSyncManager.setGetInstanceIsAllowed();
        return;
      } 
      throw new RuntimeException("WebView cannot be initialized on a thread that has no Looper.");
    } 
    throw new IllegalArgumentException("Invalid context argument");
  }
  
  @Deprecated
  public void setHorizontalScrollbarOverlay(boolean paramBoolean) {}
  
  @Deprecated
  public void setVerticalScrollbarOverlay(boolean paramBoolean) {}
  
  @Deprecated
  public boolean overlayHorizontalScrollbar() {
    return true;
  }
  
  @Deprecated
  public boolean overlayVerticalScrollbar() {
    return false;
  }
  
  @Deprecated
  public int getVisibleTitleHeight() {
    checkThread();
    return this.mProvider.getVisibleTitleHeight();
  }
  
  public SslCertificate getCertificate() {
    checkThread();
    return this.mProvider.getCertificate();
  }
  
  @Deprecated
  public void setCertificate(SslCertificate paramSslCertificate) {
    checkThread();
    this.mProvider.setCertificate(paramSslCertificate);
  }
  
  @Deprecated
  public void savePassword(String paramString1, String paramString2, String paramString3) {
    checkThread();
    this.mProvider.savePassword(paramString1, paramString2, paramString3);
  }
  
  @Deprecated
  public void setHttpAuthUsernamePassword(String paramString1, String paramString2, String paramString3, String paramString4) {
    checkThread();
    this.mProvider.setHttpAuthUsernamePassword(paramString1, paramString2, paramString3, paramString4);
  }
  
  @Deprecated
  public String[] getHttpAuthUsernamePassword(String paramString1, String paramString2) {
    checkThread();
    return this.mProvider.getHttpAuthUsernamePassword(paramString1, paramString2);
  }
  
  public void destroy() {
    checkThread();
    this.mProvider.destroy();
  }
  
  @Deprecated
  public static void enablePlatformNotifications() {}
  
  @Deprecated
  public static void disablePlatformNotifications() {}
  
  public static void freeMemoryForTests() {
    getFactory().getStatics().freeMemoryForTests();
  }
  
  public void setNetworkAvailable(boolean paramBoolean) {
    checkThread();
    this.mProvider.setNetworkAvailable(paramBoolean);
  }
  
  public WebBackForwardList saveState(Bundle paramBundle) {
    checkThread();
    return this.mProvider.saveState(paramBundle);
  }
  
  @Deprecated
  public boolean savePicture(Bundle paramBundle, File paramFile) {
    checkThread();
    return this.mProvider.savePicture(paramBundle, paramFile);
  }
  
  @Deprecated
  public boolean restorePicture(Bundle paramBundle, File paramFile) {
    checkThread();
    return this.mProvider.restorePicture(paramBundle, paramFile);
  }
  
  public WebBackForwardList restoreState(Bundle paramBundle) {
    checkThread();
    return this.mProvider.restoreState(paramBundle);
  }
  
  public void loadUrl(String paramString, Map<String, String> paramMap) {
    checkThread();
    this.mProvider.loadUrl(paramString, paramMap);
  }
  
  public void loadUrl(String paramString) {
    checkThread();
    this.mProvider.loadUrl(paramString);
  }
  
  public void postUrl(String paramString, byte[] paramArrayOfbyte) {
    checkThread();
    if (URLUtil.isNetworkUrl(paramString)) {
      this.mProvider.postUrl(paramString, paramArrayOfbyte);
    } else {
      this.mProvider.loadUrl(paramString);
    } 
  }
  
  public void loadData(String paramString1, String paramString2, String paramString3) {
    checkThread();
    this.mProvider.loadData(paramString1, paramString2, paramString3);
  }
  
  public void loadDataWithBaseURL(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5) {
    checkThread();
    this.mProvider.loadDataWithBaseURL(paramString1, paramString2, paramString3, paramString4, paramString5);
  }
  
  public void evaluateJavascript(String paramString, ValueCallback<String> paramValueCallback) {
    checkThread();
    this.mProvider.evaluateJavaScript(paramString, paramValueCallback);
  }
  
  public void saveWebArchive(String paramString) {
    checkThread();
    this.mProvider.saveWebArchive(paramString);
  }
  
  public void saveWebArchive(String paramString, boolean paramBoolean, ValueCallback<String> paramValueCallback) {
    checkThread();
    this.mProvider.saveWebArchive(paramString, paramBoolean, paramValueCallback);
  }
  
  public void stopLoading() {
    checkThread();
    this.mProvider.stopLoading();
  }
  
  public void reload() {
    checkThread();
    this.mProvider.reload();
  }
  
  public boolean canGoBack() {
    checkThread();
    return this.mProvider.canGoBack();
  }
  
  public void goBack() {
    checkThread();
    this.mProvider.goBack();
  }
  
  public boolean canGoForward() {
    checkThread();
    return this.mProvider.canGoForward();
  }
  
  public void goForward() {
    checkThread();
    this.mProvider.goForward();
  }
  
  public boolean canGoBackOrForward(int paramInt) {
    checkThread();
    return this.mProvider.canGoBackOrForward(paramInt);
  }
  
  public void goBackOrForward(int paramInt) {
    checkThread();
    this.mProvider.goBackOrForward(paramInt);
  }
  
  public boolean isPrivateBrowsingEnabled() {
    checkThread();
    return this.mProvider.isPrivateBrowsingEnabled();
  }
  
  public boolean pageUp(boolean paramBoolean) {
    checkThread();
    return this.mProvider.pageUp(paramBoolean);
  }
  
  public boolean pageDown(boolean paramBoolean) {
    checkThread();
    return this.mProvider.pageDown(paramBoolean);
  }
  
  public void postVisualStateCallback(long paramLong, VisualStateCallback paramVisualStateCallback) {
    checkThread();
    this.mProvider.insertVisualStateCallback(paramLong, paramVisualStateCallback);
  }
  
  @Deprecated
  public void clearView() {
    checkThread();
    this.mProvider.clearView();
  }
  
  @Deprecated
  public Picture capturePicture() {
    checkThread();
    return this.mProvider.capturePicture();
  }
  
  @Deprecated
  public PrintDocumentAdapter createPrintDocumentAdapter() {
    checkThread();
    return this.mProvider.createPrintDocumentAdapter("default");
  }
  
  public PrintDocumentAdapter createPrintDocumentAdapter(String paramString) {
    checkThread();
    return this.mProvider.createPrintDocumentAdapter(paramString);
  }
  
  @ExportedProperty(category = "webview")
  @Deprecated
  public float getScale() {
    checkThread();
    return this.mProvider.getScale();
  }
  
  public void setInitialScale(int paramInt) {
    checkThread();
    this.mProvider.setInitialScale(paramInt);
  }
  
  public void invokeZoomPicker() {
    checkThread();
    this.mProvider.invokeZoomPicker();
  }
  
  public HitTestResult getHitTestResult() {
    checkThread();
    return this.mProvider.getHitTestResult();
  }
  
  public void requestFocusNodeHref(Message paramMessage) {
    checkThread();
    this.mProvider.requestFocusNodeHref(paramMessage);
  }
  
  public void requestImageRef(Message paramMessage) {
    checkThread();
    this.mProvider.requestImageRef(paramMessage);
  }
  
  @ExportedProperty(category = "webview")
  public String getUrl() {
    checkThread();
    return this.mProvider.getUrl();
  }
  
  @ExportedProperty(category = "webview")
  public String getOriginalUrl() {
    checkThread();
    return this.mProvider.getOriginalUrl();
  }
  
  @ExportedProperty(category = "webview")
  public String getTitle() {
    checkThread();
    return this.mProvider.getTitle();
  }
  
  public Bitmap getFavicon() {
    checkThread();
    return this.mProvider.getFavicon();
  }
  
  public String getTouchIconUrl() {
    return this.mProvider.getTouchIconUrl();
  }
  
  public int getProgress() {
    checkThread();
    return this.mProvider.getProgress();
  }
  
  @ExportedProperty(category = "webview")
  public int getContentHeight() {
    checkThread();
    return this.mProvider.getContentHeight();
  }
  
  @ExportedProperty(category = "webview")
  public int getContentWidth() {
    return this.mProvider.getContentWidth();
  }
  
  public void pauseTimers() {
    checkThread();
    this.mProvider.pauseTimers();
  }
  
  public void resumeTimers() {
    checkThread();
    this.mProvider.resumeTimers();
  }
  
  public void onPause() {
    checkThread();
    this.mProvider.onPause();
  }
  
  public void onResume() {
    checkThread();
    this.mProvider.onResume();
  }
  
  public boolean isPaused() {
    return this.mProvider.isPaused();
  }
  
  @Deprecated
  public void freeMemory() {
    checkThread();
    this.mProvider.freeMemory();
  }
  
  public void clearCache(boolean paramBoolean) {
    checkThread();
    this.mProvider.clearCache(paramBoolean);
  }
  
  public void clearFormData() {
    checkThread();
    this.mProvider.clearFormData();
  }
  
  public void clearHistory() {
    checkThread();
    this.mProvider.clearHistory();
  }
  
  public void clearSslPreferences() {
    checkThread();
    this.mProvider.clearSslPreferences();
  }
  
  public static void clearClientCertPreferences(Runnable paramRunnable) {
    getFactory().getStatics().clearClientCertPreferences(paramRunnable);
  }
  
  public static void startSafeBrowsing(Context paramContext, ValueCallback<Boolean> paramValueCallback) {
    getFactory().getStatics().initSafeBrowsing(paramContext, paramValueCallback);
  }
  
  public static void setSafeBrowsingWhitelist(List<String> paramList, ValueCallback<Boolean> paramValueCallback) {
    getFactory().getStatics().setSafeBrowsingWhitelist(paramList, paramValueCallback);
  }
  
  public static Uri getSafeBrowsingPrivacyPolicyUrl() {
    return getFactory().getStatics().getSafeBrowsingPrivacyPolicyUrl();
  }
  
  public WebBackForwardList copyBackForwardList() {
    checkThread();
    return this.mProvider.copyBackForwardList();
  }
  
  public void setFindListener(FindListener paramFindListener) {
    checkThread();
    setupFindListenerIfNeeded();
    FindListenerDistributor.access$002(this.mFindListener, paramFindListener);
  }
  
  public void findNext(boolean paramBoolean) {
    checkThread();
    this.mProvider.findNext(paramBoolean);
  }
  
  @Deprecated
  public int findAll(String paramString) {
    checkThread();
    StrictMode.noteSlowCall("findAll blocks UI: prefer findAllAsync");
    return this.mProvider.findAll(paramString);
  }
  
  public void findAllAsync(String paramString) {
    checkThread();
    this.mProvider.findAllAsync(paramString);
  }
  
  @Deprecated
  public boolean showFindDialog(String paramString, boolean paramBoolean) {
    checkThread();
    return this.mProvider.showFindDialog(paramString, paramBoolean);
  }
  
  @Deprecated
  public static String findAddress(String paramString) {
    if (paramString != null)
      return FindAddress.findAddress(paramString); 
    throw new NullPointerException("addr is null");
  }
  
  public static void enableSlowWholeDocumentDraw() {
    getFactory().getStatics().enableSlowWholeDocumentDraw();
  }
  
  public void clearMatches() {
    checkThread();
    this.mProvider.clearMatches();
  }
  
  public void documentHasImages(Message paramMessage) {
    checkThread();
    this.mProvider.documentHasImages(paramMessage);
  }
  
  public void setWebViewClient(WebViewClient paramWebViewClient) {
    checkThread();
    this.mProvider.setWebViewClient(paramWebViewClient);
  }
  
  public WebViewClient getWebViewClient() {
    checkThread();
    return this.mProvider.getWebViewClient();
  }
  
  public WebViewRenderProcess getWebViewRenderProcess() {
    checkThread();
    return this.mProvider.getWebViewRenderProcess();
  }
  
  public void setWebViewRenderProcessClient(Executor paramExecutor, WebViewRenderProcessClient paramWebViewRenderProcessClient) {
    checkThread();
    this.mProvider.setWebViewRenderProcessClient(paramExecutor, paramWebViewRenderProcessClient);
  }
  
  public void setWebViewRenderProcessClient(WebViewRenderProcessClient paramWebViewRenderProcessClient) {
    checkThread();
    this.mProvider.setWebViewRenderProcessClient(null, paramWebViewRenderProcessClient);
  }
  
  public WebViewRenderProcessClient getWebViewRenderProcessClient() {
    checkThread();
    return this.mProvider.getWebViewRenderProcessClient();
  }
  
  public void setDownloadListener(DownloadListener paramDownloadListener) {
    checkThread();
    this.mProvider.setDownloadListener(paramDownloadListener);
  }
  
  public void setWebChromeClient(WebChromeClient paramWebChromeClient) {
    checkThread();
    this.mProvider.setWebChromeClient(paramWebChromeClient);
  }
  
  public WebChromeClient getWebChromeClient() {
    checkThread();
    return this.mProvider.getWebChromeClient();
  }
  
  @Deprecated
  public void setPictureListener(PictureListener paramPictureListener) {
    checkThread();
    this.mProvider.setPictureListener(paramPictureListener);
  }
  
  public void addJavascriptInterface(Object paramObject, String paramString) {
    checkThread();
    this.mProvider.addJavascriptInterface(paramObject, paramString);
  }
  
  public void removeJavascriptInterface(String paramString) {
    checkThread();
    this.mProvider.removeJavascriptInterface(paramString);
  }
  
  public WebMessagePort[] createWebMessageChannel() {
    checkThread();
    return this.mProvider.createWebMessageChannel();
  }
  
  public void postWebMessage(WebMessage paramWebMessage, Uri paramUri) {
    checkThread();
    this.mProvider.postMessageToMainFrame(paramWebMessage, paramUri);
  }
  
  public WebSettings getSettings() {
    checkThread();
    return this.mProvider.getSettings();
  }
  
  public static void setWebContentsDebuggingEnabled(boolean paramBoolean) {
    getFactory().getStatics().setWebContentsDebuggingEnabled(paramBoolean);
  }
  
  @Deprecated
  public static PluginList getPluginList() {
    // Byte code:
    //   0: ldc android/webkit/WebView
    //   2: monitorenter
    //   3: new android/webkit/PluginList
    //   6: dup
    //   7: invokespecial <init> : ()V
    //   10: astore_0
    //   11: ldc android/webkit/WebView
    //   13: monitorexit
    //   14: aload_0
    //   15: areturn
    //   16: astore_0
    //   17: ldc android/webkit/WebView
    //   19: monitorexit
    //   20: aload_0
    //   21: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2021	-> 3
    //   #2021	-> 16
    // Exception table:
    //   from	to	target	type
    //   3	11	16	finally
  }
  
  public static void setDataDirectorySuffix(String paramString) {
    WebViewFactory.setDataDirectorySuffix(paramString);
  }
  
  public static void disableWebView() {
    WebViewFactory.disableWebView();
  }
  
  @Deprecated
  public void refreshPlugins(boolean paramBoolean) {
    checkThread();
  }
  
  @Deprecated
  public void emulateShiftHeld() {
    checkThread();
  }
  
  @Deprecated
  public void onChildViewAdded(View paramView1, View paramView2) {}
  
  @Deprecated
  public void onChildViewRemoved(View paramView1, View paramView2) {}
  
  @Deprecated
  public void onGlobalFocusChanged(View paramView1, View paramView2) {}
  
  @Deprecated
  public void setMapTrackballToArrowKeys(boolean paramBoolean) {
    checkThread();
    this.mProvider.setMapTrackballToArrowKeys(paramBoolean);
  }
  
  public void flingScroll(int paramInt1, int paramInt2) {
    checkThread();
    this.mProvider.flingScroll(paramInt1, paramInt2);
  }
  
  @Deprecated
  public View getZoomControls() {
    checkThread();
    return this.mProvider.getZoomControls();
  }
  
  @Deprecated
  public boolean canZoomIn() {
    checkThread();
    return this.mProvider.canZoomIn();
  }
  
  @Deprecated
  public boolean canZoomOut() {
    checkThread();
    return this.mProvider.canZoomOut();
  }
  
  public void zoomBy(float paramFloat) {
    checkThread();
    if (paramFloat >= 0.01D) {
      if (paramFloat <= 100.0D) {
        this.mProvider.zoomBy(paramFloat);
        return;
      } 
      throw new IllegalArgumentException("zoomFactor must be less than 100.");
    } 
    throw new IllegalArgumentException("zoomFactor must be greater than 0.01.");
  }
  
  public boolean zoomIn() {
    checkThread();
    return this.mProvider.zoomIn();
  }
  
  public boolean zoomOut() {
    checkThread();
    return this.mProvider.zoomOut();
  }
  
  @Deprecated
  public void debugDump() {
    checkThread();
  }
  
  public void dumpViewHierarchyWithProperties(BufferedWriter paramBufferedWriter, int paramInt) {
    this.mProvider.dumpViewHierarchyWithProperties(paramBufferedWriter, paramInt);
  }
  
  public View findHierarchyView(String paramString, int paramInt) {
    return this.mProvider.findHierarchyView(paramString, paramInt);
  }
  
  public void setRendererPriorityPolicy(int paramInt, boolean paramBoolean) {
    this.mProvider.setRendererPriorityPolicy(paramInt, paramBoolean);
  }
  
  public int getRendererRequestedPriority() {
    return this.mProvider.getRendererRequestedPriority();
  }
  
  public boolean getRendererPriorityWaivedWhenNotVisible() {
    return this.mProvider.getRendererPriorityWaivedWhenNotVisible();
  }
  
  public void setTextClassifier(TextClassifier paramTextClassifier) {
    this.mProvider.setTextClassifier(paramTextClassifier);
  }
  
  public TextClassifier getTextClassifier() {
    return this.mProvider.getTextClassifier();
  }
  
  public static ClassLoader getWebViewClassLoader() {
    return getFactory().getWebViewClassLoader();
  }
  
  public Looper getWebViewLooper() {
    return this.mWebViewThread;
  }
  
  @SystemApi
  public WebViewProvider getWebViewProvider() {
    return this.mProvider;
  }
  
  @SystemApi
  class PrivateAccess {
    final WebView this$0;
    
    public int super_getScrollBarStyle() {
      return WebView.this.getScrollBarStyle();
    }
    
    public void super_scrollTo(int param1Int1, int param1Int2) {
      WebView.this.scrollTo(param1Int1, param1Int2);
    }
    
    public void super_computeScroll() {
      WebView.this.computeScroll();
    }
    
    public boolean super_onHoverEvent(MotionEvent param1MotionEvent) {
      return WebView.this.onHoverEvent(param1MotionEvent);
    }
    
    public boolean super_performAccessibilityAction(int param1Int, Bundle param1Bundle) {
      return WebView.this.performAccessibilityActionInternal(param1Int, param1Bundle);
    }
    
    public boolean super_performLongClick() {
      return WebView.this.performLongClick();
    }
    
    public boolean super_setFrame(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      return WebView.this.setFrame(param1Int1, param1Int2, param1Int3, param1Int4);
    }
    
    public boolean super_dispatchKeyEvent(KeyEvent param1KeyEvent) {
      return WebView.this.dispatchKeyEvent(param1KeyEvent);
    }
    
    public boolean super_onGenericMotionEvent(MotionEvent param1MotionEvent) {
      return WebView.this.onGenericMotionEvent(param1MotionEvent);
    }
    
    public boolean super_requestFocus(int param1Int, Rect param1Rect) {
      return WebView.this.requestFocus(param1Int, param1Rect);
    }
    
    public void super_setLayoutParams(ViewGroup.LayoutParams param1LayoutParams) {
      WebView.this.setLayoutParams(param1LayoutParams);
    }
    
    public void super_startActivityForResult(Intent param1Intent, int param1Int) {
      WebView.this.startActivityForResult(param1Intent, param1Int);
    }
    
    public void overScrollBy(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6, int param1Int7, int param1Int8, boolean param1Boolean) {
      WebView.this.overScrollBy(param1Int1, param1Int2, param1Int3, param1Int4, param1Int5, param1Int6, param1Int7, param1Int8, param1Boolean);
    }
    
    public void awakenScrollBars(int param1Int) {
      WebView.this.awakenScrollBars(param1Int);
    }
    
    public void awakenScrollBars(int param1Int, boolean param1Boolean) {
      WebView.this.awakenScrollBars(param1Int, param1Boolean);
    }
    
    public float getVerticalScrollFactor() {
      return WebView.this.getVerticalScrollFactor();
    }
    
    public float getHorizontalScrollFactor() {
      return WebView.this.getHorizontalScrollFactor();
    }
    
    public void setMeasuredDimension(int param1Int1, int param1Int2) {
      WebView.this.setMeasuredDimension(param1Int1, param1Int2);
    }
    
    public void onScrollChanged(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      WebView.this.onScrollChanged(param1Int1, param1Int2, param1Int3, param1Int4);
    }
    
    public int getHorizontalScrollbarHeight() {
      return WebView.this.getHorizontalScrollbarHeight();
    }
    
    public void super_onDrawVerticalScrollBar(Canvas param1Canvas, Drawable param1Drawable, int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      WebView.this.onDrawVerticalScrollBar(param1Canvas, param1Drawable, param1Int1, param1Int2, param1Int3, param1Int4);
    }
    
    public void setScrollXRaw(int param1Int) {
      WebView.access$2102(WebView.this, param1Int);
    }
    
    public void setScrollYRaw(int param1Int) {
      WebView.access$2202(WebView.this, param1Int);
    }
  }
  
  void setFindDialogFindListener(FindListener paramFindListener) {
    checkThread();
    setupFindListenerIfNeeded();
    FindListenerDistributor.access$2302(this.mFindListener, paramFindListener);
  }
  
  void notifyFindDialogDismissed() {
    checkThread();
    this.mProvider.notifyFindDialogDismissed();
  }
  
  class FindListenerDistributor implements FindListener {
    private WebView.FindListener mFindDialogFindListener;
    
    private WebView.FindListener mUserFindListener;
    
    final WebView this$0;
    
    private FindListenerDistributor() {}
    
    public void onFindResultReceived(int param1Int1, int param1Int2, boolean param1Boolean) {
      WebView.FindListener findListener = this.mFindDialogFindListener;
      if (findListener != null)
        findListener.onFindResultReceived(param1Int1, param1Int2, param1Boolean); 
      findListener = this.mUserFindListener;
      if (findListener != null)
        findListener.onFindResultReceived(param1Int1, param1Int2, param1Boolean); 
    }
  }
  
  private void setupFindListenerIfNeeded() {
    if (this.mFindListener == null) {
      FindListenerDistributor findListenerDistributor = new FindListenerDistributor();
      this.mProvider.setFindListener(findListenerDistributor);
    } 
  }
  
  private void ensureProviderCreated() {
    checkThread();
    if (this.mProvider == null)
      this.mProvider = getFactory().createWebView(this, new PrivateAccess()); 
  }
  
  private static WebViewFactoryProvider getFactory() {
    return WebViewFactory.getProvider();
  }
  
  private void checkThread() {
    if (this.mWebViewThread != null && Looper.myLooper() != this.mWebViewThread) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("A WebView method was called on thread '");
      stringBuilder.append(Thread.currentThread().getName());
      stringBuilder.append("'. All WebView methods must be called on the same thread. (Expected Looper ");
      stringBuilder.append(this.mWebViewThread);
      stringBuilder.append(" called on ");
      stringBuilder.append(Looper.myLooper());
      stringBuilder.append(", FYI main Looper is ");
      stringBuilder.append(Looper.getMainLooper());
      stringBuilder.append(")");
      Throwable throwable = new Throwable(stringBuilder.toString());
      Log.w("WebView", Log.getStackTraceString(throwable));
      StrictMode.onWebViewMethodCalledOnWrongThread(throwable);
      if (sEnforceThreadChecking) {
        if (AppGlobals.getPackageManager() != null) {
          int i = Binder.getCallingUid();
          try {
            OplusPackageManager oplusPackageManager = new OplusPackageManager();
            this();
            String str = AppGlobals.getPackageManager().getNameForUid(i);
            if (str != null && oplusPackageManager.inCptWhiteList(691, str)) {
              StringBuilder stringBuilder1 = new StringBuilder();
              this();
              stringBuilder1.append("modify for ");
              stringBuilder1.append(str);
              stringBuilder1.append(" skip WebView threadCheck");
              Log.d("WebView", stringBuilder1.toString());
              return;
            } 
          } catch (RemoteException remoteException) {
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("RemoteException e:");
            stringBuilder1.append(remoteException.getMessage());
            Log.d("WebView", stringBuilder1.toString());
          } 
        } 
        throw new RuntimeException(throwable);
      } 
    } 
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    this.mProvider.getViewDelegate().onAttachedToWindow();
  }
  
  protected void onDetachedFromWindowInternal() {
    this.mProvider.getViewDelegate().onDetachedFromWindow();
    super.onDetachedFromWindowInternal();
  }
  
  public void onMovedToDisplay(int paramInt, Configuration paramConfiguration) {
    this.mProvider.getViewDelegate().onMovedToDisplay(paramInt, paramConfiguration);
  }
  
  public void setLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    this.mProvider.getViewDelegate().setLayoutParams(paramLayoutParams);
  }
  
  public void setOverScrollMode(int paramInt) {
    super.setOverScrollMode(paramInt);
    ensureProviderCreated();
    this.mProvider.getViewDelegate().setOverScrollMode(paramInt);
  }
  
  public void setScrollBarStyle(int paramInt) {
    this.mProvider.getViewDelegate().setScrollBarStyle(paramInt);
    super.setScrollBarStyle(paramInt);
  }
  
  protected int computeHorizontalScrollRange() {
    return this.mProvider.getScrollDelegate().computeHorizontalScrollRange();
  }
  
  protected int computeHorizontalScrollOffset() {
    return this.mProvider.getScrollDelegate().computeHorizontalScrollOffset();
  }
  
  protected int computeVerticalScrollRange() {
    return this.mProvider.getScrollDelegate().computeVerticalScrollRange();
  }
  
  protected int computeVerticalScrollOffset() {
    return this.mProvider.getScrollDelegate().computeVerticalScrollOffset();
  }
  
  protected int computeVerticalScrollExtent() {
    return this.mProvider.getScrollDelegate().computeVerticalScrollExtent();
  }
  
  public void computeScroll() {
    this.mProvider.getScrollDelegate().computeScroll();
  }
  
  public boolean onHoverEvent(MotionEvent paramMotionEvent) {
    return this.mProvider.getViewDelegate().onHoverEvent(paramMotionEvent);
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    return this.mProvider.getViewDelegate().onTouchEvent(paramMotionEvent);
  }
  
  public boolean onGenericMotionEvent(MotionEvent paramMotionEvent) {
    return this.mProvider.getViewDelegate().onGenericMotionEvent(paramMotionEvent);
  }
  
  public boolean onTrackballEvent(MotionEvent paramMotionEvent) {
    return this.mProvider.getViewDelegate().onTrackballEvent(paramMotionEvent);
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    return this.mProvider.getViewDelegate().onKeyDown(paramInt, paramKeyEvent);
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    return this.mProvider.getViewDelegate().onKeyUp(paramInt, paramKeyEvent);
  }
  
  public boolean onKeyMultiple(int paramInt1, int paramInt2, KeyEvent paramKeyEvent) {
    return this.mProvider.getViewDelegate().onKeyMultiple(paramInt1, paramInt2, paramKeyEvent);
  }
  
  public AccessibilityNodeProvider getAccessibilityNodeProvider() {
    WebViewProvider webViewProvider = this.mProvider;
    AccessibilityNodeProvider accessibilityNodeProvider = webViewProvider.getViewDelegate().getAccessibilityNodeProvider();
    if (accessibilityNodeProvider == null)
      accessibilityNodeProvider = super.getAccessibilityNodeProvider(); 
    return accessibilityNodeProvider;
  }
  
  @Deprecated
  public boolean shouldDelayChildPressedState() {
    return this.mProvider.getViewDelegate().shouldDelayChildPressedState();
  }
  
  public CharSequence getAccessibilityClassName() {
    return WebView.class.getName();
  }
  
  public void onProvideVirtualStructure(ViewStructure paramViewStructure) {
    this.mProvider.getViewDelegate().onProvideVirtualStructure(paramViewStructure);
  }
  
  public void onProvideAutofillVirtualStructure(ViewStructure paramViewStructure, int paramInt) {
    this.mProvider.getViewDelegate().onProvideAutofillVirtualStructure(paramViewStructure, paramInt);
  }
  
  public void onProvideContentCaptureStructure(ViewStructure paramViewStructure, int paramInt) {
    this.mProvider.getViewDelegate().onProvideContentCaptureStructure(paramViewStructure, paramInt);
  }
  
  public void autofill(SparseArray<AutofillValue> paramSparseArray) {
    this.mProvider.getViewDelegate().autofill(paramSparseArray);
  }
  
  public boolean isVisibleToUserForAutofill(int paramInt) {
    return this.mProvider.getViewDelegate().isVisibleToUserForAutofill(paramInt);
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    this.mProvider.getViewDelegate().onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
  }
  
  public void onInitializeAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    super.onInitializeAccessibilityEventInternal(paramAccessibilityEvent);
    this.mProvider.getViewDelegate().onInitializeAccessibilityEvent(paramAccessibilityEvent);
  }
  
  public boolean performAccessibilityActionInternal(int paramInt, Bundle paramBundle) {
    return this.mProvider.getViewDelegate().performAccessibilityAction(paramInt, paramBundle);
  }
  
  protected void onDrawVerticalScrollBar(Canvas paramCanvas, Drawable paramDrawable, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mProvider.getViewDelegate().onDrawVerticalScrollBar(paramCanvas, paramDrawable, paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  protected void onOverScrolled(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2) {
    this.mProvider.getViewDelegate().onOverScrolled(paramInt1, paramInt2, paramBoolean1, paramBoolean2);
  }
  
  protected void onWindowVisibilityChanged(int paramInt) {
    super.onWindowVisibilityChanged(paramInt);
    this.mProvider.getViewDelegate().onWindowVisibilityChanged(paramInt);
  }
  
  protected void onDraw(Canvas paramCanvas) {
    this.mProvider.getViewDelegate().onDraw(paramCanvas);
  }
  
  public boolean performLongClick() {
    return this.mProvider.getViewDelegate().performLongClick();
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration) {
    this.mProvider.getViewDelegate().onConfigurationChanged(paramConfiguration);
  }
  
  public InputConnection onCreateInputConnection(EditorInfo paramEditorInfo) {
    return this.mProvider.getViewDelegate().onCreateInputConnection(paramEditorInfo);
  }
  
  public boolean onDragEvent(DragEvent paramDragEvent) {
    return this.mProvider.getViewDelegate().onDragEvent(paramDragEvent);
  }
  
  protected void onVisibilityChanged(View paramView, int paramInt) {
    super.onVisibilityChanged(paramView, paramInt);
    ensureProviderCreated();
    this.mProvider.getViewDelegate().onVisibilityChanged(paramView, paramInt);
    ((IOplusDarkModeManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusDarkModeManager.DEFAULT, new Object[0])).ensureWebSettingDarkMode(this);
  }
  
  public void onWindowFocusChanged(boolean paramBoolean) {
    this.mProvider.getViewDelegate().onWindowFocusChanged(paramBoolean);
    super.onWindowFocusChanged(paramBoolean);
  }
  
  protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect) {
    this.mProvider.getViewDelegate().onFocusChanged(paramBoolean, paramInt, paramRect);
    super.onFocusChanged(paramBoolean, paramInt, paramRect);
  }
  
  protected boolean setFrame(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    return this.mProvider.getViewDelegate().setFrame(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    this.mProvider.getViewDelegate().onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  protected void onScrollChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onScrollChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    this.mProvider.getViewDelegate().onScrollChanged(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent) {
    return this.mProvider.getViewDelegate().dispatchKeyEvent(paramKeyEvent);
  }
  
  public boolean requestFocus(int paramInt, Rect paramRect) {
    return this.mProvider.getViewDelegate().requestFocus(paramInt, paramRect);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    super.onMeasure(paramInt1, paramInt2);
    this.mProvider.getViewDelegate().onMeasure(paramInt1, paramInt2);
  }
  
  public boolean requestChildRectangleOnScreen(View paramView, Rect paramRect, boolean paramBoolean) {
    return this.mProvider.getViewDelegate().requestChildRectangleOnScreen(paramView, paramRect, paramBoolean);
  }
  
  public void setBackgroundColor(int paramInt) {
    this.mProvider.getViewDelegate().setBackgroundColor(paramInt);
  }
  
  public void setLayerType(int paramInt, Paint paramPaint) {
    super.setLayerType(paramInt, paramPaint);
    this.mProvider.getViewDelegate().setLayerType(paramInt, paramPaint);
  }
  
  protected void dispatchDraw(Canvas paramCanvas) {
    this.mProvider.getViewDelegate().preDispatchDraw(paramCanvas);
    super.dispatchDraw(paramCanvas);
  }
  
  public void onStartTemporaryDetach() {
    super.onStartTemporaryDetach();
    this.mProvider.getViewDelegate().onStartTemporaryDetach();
  }
  
  public void onFinishTemporaryDetach() {
    super.onFinishTemporaryDetach();
    this.mProvider.getViewDelegate().onFinishTemporaryDetach();
  }
  
  public Handler getHandler() {
    return this.mProvider.getViewDelegate().getHandler(super.getHandler());
  }
  
  public View findFocus() {
    return this.mProvider.getViewDelegate().findFocus(super.findFocus());
  }
  
  public static PackageInfo getCurrentWebViewPackage() {
    PackageInfo packageInfo = WebViewFactory.getLoadedPackageInfo();
    if (packageInfo != null)
      return packageInfo; 
    IWebViewUpdateService iWebViewUpdateService = WebViewFactory.getUpdateService();
    if (iWebViewUpdateService == null)
      return null; 
    try {
      return iWebViewUpdateService.getCurrentWebViewPackage();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent) {
    this.mProvider.getViewDelegate().onActivityResult(paramInt1, paramInt2, paramIntent);
  }
  
  public boolean onCheckIsTextEditor() {
    return this.mProvider.getViewDelegate().onCheckIsTextEditor();
  }
  
  protected void encodeProperties(ViewHierarchyEncoder paramViewHierarchyEncoder) {
    super.encodeProperties(paramViewHierarchyEncoder);
    checkThread();
    paramViewHierarchyEncoder.addProperty("webview:contentHeight", this.mProvider.getContentHeight());
    paramViewHierarchyEncoder.addProperty("webview:contentWidth", this.mProvider.getContentWidth());
    paramViewHierarchyEncoder.addProperty("webview:scale", this.mProvider.getScale());
    paramViewHierarchyEncoder.addProperty("webview:title", this.mProvider.getTitle());
    paramViewHierarchyEncoder.addProperty("webview:url", this.mProvider.getUrl());
    paramViewHierarchyEncoder.addProperty("webview:originalUrl", this.mProvider.getOriginalUrl());
  }
  
  class FindListener {
    public abstract void onFindResultReceived(int param1Int1, int param1Int2, boolean param1Boolean);
  }
  
  @Deprecated
  class PictureListener {
    @Deprecated
    public abstract void onNewPicture(WebView param1WebView, Picture param1Picture);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class RendererPriority implements Annotation {}
}
