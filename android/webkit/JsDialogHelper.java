package android.webkit;

import android.annotation.SystemApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import java.net.MalformedURLException;
import java.net.URL;

@SystemApi
public class JsDialogHelper {
  public static final int ALERT = 1;
  
  public static final int CONFIRM = 2;
  
  public static final int PROMPT = 3;
  
  private static final String TAG = "JsDialogHelper";
  
  public static final int UNLOAD = 4;
  
  private final String mDefaultValue;
  
  private final String mMessage;
  
  private final JsPromptResult mResult;
  
  private final int mType;
  
  private final String mUrl;
  
  public JsDialogHelper(JsPromptResult paramJsPromptResult, int paramInt, String paramString1, String paramString2, String paramString3) {
    this.mResult = paramJsPromptResult;
    this.mDefaultValue = paramString1;
    this.mMessage = paramString2;
    this.mType = paramInt;
    this.mUrl = paramString3;
  }
  
  public JsDialogHelper(JsPromptResult paramJsPromptResult, Message paramMessage) {
    this.mResult = paramJsPromptResult;
    this.mDefaultValue = paramMessage.getData().getString("default");
    this.mMessage = paramMessage.getData().getString("message");
    this.mType = paramMessage.getData().getInt("type");
    this.mUrl = paramMessage.getData().getString("url");
  }
  
  public boolean invokeCallback(WebChromeClient paramWebChromeClient, WebView paramWebView) {
    StringBuilder stringBuilder;
    int i = this.mType;
    if (i != 1) {
      if (i != 2) {
        if (i != 3) {
          if (i == 4)
            return paramWebChromeClient.onJsBeforeUnload(paramWebView, this.mUrl, this.mMessage, this.mResult); 
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unexpected type: ");
          stringBuilder.append(this.mType);
          throw new IllegalArgumentException(stringBuilder.toString());
        } 
        return stringBuilder.onJsPrompt(paramWebView, this.mUrl, this.mMessage, this.mDefaultValue, this.mResult);
      } 
      return stringBuilder.onJsConfirm(paramWebView, this.mUrl, this.mMessage, this.mResult);
    } 
    return stringBuilder.onJsAlert(paramWebView, this.mUrl, this.mMessage, this.mResult);
  }
  
  public void showDialog(Context paramContext) {
    String str1, str2;
    int i, j;
    if (!canShowAlertDialog(paramContext)) {
      Log.w("JsDialogHelper", "Cannot create a dialog, the WebView context is not an Activity");
      this.mResult.cancel();
      return;
    } 
    if (this.mType == 4) {
      str1 = paramContext.getString(17040368);
      str2 = paramContext.getString(17040365, new Object[] { this.mMessage });
      i = 17040367;
      j = 17040366;
    } else {
      str1 = getJsDialogTitle(paramContext);
      str2 = this.mMessage;
      i = 17039370;
      j = 17039360;
    } 
    AlertDialog.Builder builder = new AlertDialog.Builder(paramContext);
    builder.setTitle(str1);
    builder.setOnCancelListener(new CancelListener());
    if (this.mType != 3) {
      builder.setMessage(str2);
      builder.setPositiveButton(i, new PositiveListener(null));
    } else {
      View view = LayoutInflater.from(paramContext).inflate(17367179, (ViewGroup)null);
      EditText editText = view.<EditText>findViewById(16909613);
      editText.setText(this.mDefaultValue);
      builder.setPositiveButton(i, new PositiveListener(editText));
      ((TextView)view.<TextView>findViewById(16908299)).setText(this.mMessage);
      builder.setView(view);
    } 
    if (this.mType != 1)
      builder.setNegativeButton(j, new CancelListener()); 
    builder.show();
  }
  
  class CancelListener implements DialogInterface.OnCancelListener, DialogInterface.OnClickListener {
    final JsDialogHelper this$0;
    
    private CancelListener() {}
    
    public void onCancel(DialogInterface param1DialogInterface) {
      JsDialogHelper.this.mResult.cancel();
    }
    
    public void onClick(DialogInterface param1DialogInterface, int param1Int) {
      JsDialogHelper.this.mResult.cancel();
    }
  }
  
  class PositiveListener implements DialogInterface.OnClickListener {
    private final EditText mEdit;
    
    final JsDialogHelper this$0;
    
    public PositiveListener(EditText param1EditText) {
      this.mEdit = param1EditText;
    }
    
    public void onClick(DialogInterface param1DialogInterface, int param1Int) {
      if (this.mEdit == null) {
        JsDialogHelper.this.mResult.confirm();
      } else {
        JsDialogHelper.this.mResult.confirm(this.mEdit.getText().toString());
      } 
    }
  }
  
  private String getJsDialogTitle(Context paramContext) {
    String str1, str2 = this.mUrl;
    if (URLUtil.isDataUrl(this.mUrl)) {
      str1 = paramContext.getString(17040370);
    } else {
      try {
        URL uRL = new URL();
        this(this.mUrl);
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(uRL.getProtocol());
        stringBuilder.append("://");
        stringBuilder.append(uRL.getHost());
        String str = stringBuilder.toString();
        str1 = str1.getString(17040369, new Object[] { str });
      } catch (MalformedURLException malformedURLException) {
        str1 = str2;
      } 
    } 
    return str1;
  }
  
  private static boolean canShowAlertDialog(Context paramContext) {
    return paramContext instanceof android.app.Activity;
  }
}
