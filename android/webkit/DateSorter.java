package android.webkit;

import android.content.Context;
import android.content.res.Resources;
import java.util.Calendar;
import java.util.Locale;
import libcore.icu.LocaleData;

public class DateSorter {
  public static final int DAY_COUNT = 5;
  
  private static final String LOGTAG = "webkit";
  
  private static final int NUM_DAYS_AGO = 7;
  
  private long[] mBins = new long[4];
  
  private String[] mLabels = new String[5];
  
  public DateSorter(Context paramContext) {
    Resources resources = paramContext.getResources();
    Calendar calendar = Calendar.getInstance();
    beginningOfDay(calendar);
    this.mBins[0] = calendar.getTimeInMillis();
    calendar.add(6, -1);
    this.mBins[1] = calendar.getTimeInMillis();
    calendar.add(6, -6);
    this.mBins[2] = calendar.getTimeInMillis();
    calendar.add(6, 7);
    calendar.add(2, -1);
    this.mBins[3] = calendar.getTimeInMillis();
    Locale locale2 = (resources.getConfiguration()).locale;
    Locale locale1 = locale2;
    if (locale2 == null)
      locale1 = Locale.getDefault(); 
    LocaleData localeData = LocaleData.get(locale1);
    this.mLabels[0] = localeData.today;
    this.mLabels[1] = localeData.yesterday;
    String str = resources.getQuantityString(18153492, 7);
    this.mLabels[2] = String.format(str, new Object[] { Integer.valueOf(7) });
    this.mLabels[3] = paramContext.getString(17040447);
    this.mLabels[4] = paramContext.getString(17040753);
  }
  
  public int getIndex(long paramLong) {
    for (byte b = 0; b < 4; b++) {
      if (paramLong > this.mBins[b])
        return b; 
    } 
    return 4;
  }
  
  public String getLabel(int paramInt) {
    if (paramInt < 0 || paramInt >= 5)
      return ""; 
    return this.mLabels[paramInt];
  }
  
  public long getBoundary(int paramInt) {
    // Byte code:
    //   0: iload_1
    //   1: iflt -> 11
    //   4: iload_1
    //   5: istore_2
    //   6: iload_1
    //   7: iconst_4
    //   8: if_icmple -> 13
    //   11: iconst_0
    //   12: istore_2
    //   13: iload_2
    //   14: iconst_4
    //   15: if_icmpne -> 22
    //   18: ldc2_w -9223372036854775808
    //   21: lreturn
    //   22: aload_0
    //   23: getfield mBins : [J
    //   26: iload_2
    //   27: laload
    //   28: lreturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #113	-> 0
    //   #115	-> 0
    //   #118	-> 13
    //   #119	-> 22
  }
  
  private void beginningOfDay(Calendar paramCalendar) {
    paramCalendar.set(11, 0);
    paramCalendar.set(12, 0);
    paramCalendar.set(13, 0);
    paramCalendar.set(14, 0);
  }
}
