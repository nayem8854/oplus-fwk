package android.webkit;

import android.content.Context;

@Deprecated
public final class CookieSyncManager extends WebSyncManager {
  private static boolean sGetInstanceAllowed = false;
  
  private static final Object sLock = new Object();
  
  private static CookieSyncManager sRef;
  
  private CookieSyncManager() {
    super(null, null);
  }
  
  public static CookieSyncManager getInstance() {
    synchronized (sLock) {
      checkInstanceIsAllowed();
      if (sRef == null) {
        CookieSyncManager cookieSyncManager = new CookieSyncManager();
        this();
        sRef = cookieSyncManager;
      } 
      return sRef;
    } 
  }
  
  public static CookieSyncManager createInstance(Context paramContext) {
    // Byte code:
    //   0: getstatic android/webkit/CookieSyncManager.sLock : Ljava/lang/Object;
    //   3: astore_1
    //   4: aload_1
    //   5: monitorenter
    //   6: aload_0
    //   7: ifnull -> 25
    //   10: invokestatic setGetInstanceIsAllowed : ()V
    //   13: invokestatic getInstance : ()Landroid/webkit/CookieSyncManager;
    //   16: astore_0
    //   17: aload_1
    //   18: monitorexit
    //   19: aload_0
    //   20: areturn
    //   21: astore_0
    //   22: goto -> 37
    //   25: new java/lang/IllegalArgumentException
    //   28: astore_0
    //   29: aload_0
    //   30: ldc 'Invalid context argument'
    //   32: invokespecial <init> : (Ljava/lang/String;)V
    //   35: aload_0
    //   36: athrow
    //   37: aload_1
    //   38: monitorexit
    //   39: aload_0
    //   40: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #97	-> 0
    //   #98	-> 6
    //   #101	-> 10
    //   #102	-> 13
    //   #103	-> 21
    //   #99	-> 25
    //   #103	-> 37
    // Exception table:
    //   from	to	target	type
    //   10	13	21	finally
    //   13	19	21	finally
    //   25	37	21	finally
    //   37	39	21	finally
  }
  
  @Deprecated
  public void sync() {
    CookieManager.getInstance().flush();
  }
  
  @Deprecated
  protected void syncFromRamToFlash() {
    CookieManager.getInstance().flush();
  }
  
  @Deprecated
  public void resetSync() {}
  
  @Deprecated
  public void startSync() {}
  
  @Deprecated
  public void stopSync() {}
  
  static void setGetInstanceIsAllowed() {
    sGetInstanceAllowed = true;
  }
  
  private static void checkInstanceIsAllowed() {
    if (sGetInstanceAllowed)
      return; 
    throw new IllegalStateException("CookieSyncManager::createInstance() needs to be called before CookieSyncManager::getInstance()");
  }
}
