package android.webkit;

import android.content.pm.PackageInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public interface IWebViewUpdateService extends IInterface {
  String changeProviderAndSetting(String paramString) throws RemoteException;
  
  void enableMultiProcess(boolean paramBoolean) throws RemoteException;
  
  WebViewProviderInfo[] getAllWebViewPackages() throws RemoteException;
  
  PackageInfo getCurrentWebViewPackage() throws RemoteException;
  
  String getCurrentWebViewPackageName() throws RemoteException;
  
  WebViewProviderInfo[] getValidWebViewPackages() throws RemoteException;
  
  boolean isMultiProcessEnabled() throws RemoteException;
  
  void notifyRelroCreationCompleted() throws RemoteException;
  
  WebViewProviderResponse waitForAndGetProvider() throws RemoteException;
  
  class Default implements IWebViewUpdateService {
    public void notifyRelroCreationCompleted() throws RemoteException {}
    
    public WebViewProviderResponse waitForAndGetProvider() throws RemoteException {
      return null;
    }
    
    public String changeProviderAndSetting(String param1String) throws RemoteException {
      return null;
    }
    
    public WebViewProviderInfo[] getValidWebViewPackages() throws RemoteException {
      return null;
    }
    
    public WebViewProviderInfo[] getAllWebViewPackages() throws RemoteException {
      return null;
    }
    
    public String getCurrentWebViewPackageName() throws RemoteException {
      return null;
    }
    
    public PackageInfo getCurrentWebViewPackage() throws RemoteException {
      return null;
    }
    
    public boolean isMultiProcessEnabled() throws RemoteException {
      return false;
    }
    
    public void enableMultiProcess(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWebViewUpdateService {
    private static final String DESCRIPTOR = "android.webkit.IWebViewUpdateService";
    
    static final int TRANSACTION_changeProviderAndSetting = 3;
    
    static final int TRANSACTION_enableMultiProcess = 9;
    
    static final int TRANSACTION_getAllWebViewPackages = 5;
    
    static final int TRANSACTION_getCurrentWebViewPackage = 7;
    
    static final int TRANSACTION_getCurrentWebViewPackageName = 6;
    
    static final int TRANSACTION_getValidWebViewPackages = 4;
    
    static final int TRANSACTION_isMultiProcessEnabled = 8;
    
    static final int TRANSACTION_notifyRelroCreationCompleted = 1;
    
    static final int TRANSACTION_waitForAndGetProvider = 2;
    
    public Stub() {
      attachInterface(this, "android.webkit.IWebViewUpdateService");
    }
    
    public static IWebViewUpdateService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.webkit.IWebViewUpdateService");
      if (iInterface != null && iInterface instanceof IWebViewUpdateService)
        return (IWebViewUpdateService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 9:
          return "enableMultiProcess";
        case 8:
          return "isMultiProcessEnabled";
        case 7:
          return "getCurrentWebViewPackage";
        case 6:
          return "getCurrentWebViewPackageName";
        case 5:
          return "getAllWebViewPackages";
        case 4:
          return "getValidWebViewPackages";
        case 3:
          return "changeProviderAndSetting";
        case 2:
          return "waitForAndGetProvider";
        case 1:
          break;
      } 
      return "notifyRelroCreationCompleted";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        PackageInfo packageInfo;
        String str2;
        WebViewProviderInfo[] arrayOfWebViewProviderInfo;
        String str1;
        WebViewProviderResponse webViewProviderResponse;
        boolean bool1 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 9:
            param1Parcel1.enforceInterface("android.webkit.IWebViewUpdateService");
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            enableMultiProcess(bool1);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.webkit.IWebViewUpdateService");
            bool = isMultiProcessEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.webkit.IWebViewUpdateService");
            packageInfo = getCurrentWebViewPackage();
            param1Parcel2.writeNoException();
            if (packageInfo != null) {
              param1Parcel2.writeInt(1);
              packageInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 6:
            packageInfo.enforceInterface("android.webkit.IWebViewUpdateService");
            str2 = getCurrentWebViewPackageName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 5:
            str2.enforceInterface("android.webkit.IWebViewUpdateService");
            arrayOfWebViewProviderInfo = getAllWebViewPackages();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray((Parcelable[])arrayOfWebViewProviderInfo, 1);
            return true;
          case 4:
            arrayOfWebViewProviderInfo.enforceInterface("android.webkit.IWebViewUpdateService");
            arrayOfWebViewProviderInfo = getValidWebViewPackages();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray((Parcelable[])arrayOfWebViewProviderInfo, 1);
            return true;
          case 3:
            arrayOfWebViewProviderInfo.enforceInterface("android.webkit.IWebViewUpdateService");
            str1 = arrayOfWebViewProviderInfo.readString();
            str1 = changeProviderAndSetting(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 2:
            str1.enforceInterface("android.webkit.IWebViewUpdateService");
            webViewProviderResponse = waitForAndGetProvider();
            param1Parcel2.writeNoException();
            if (webViewProviderResponse != null) {
              param1Parcel2.writeInt(1);
              webViewProviderResponse.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        webViewProviderResponse.enforceInterface("android.webkit.IWebViewUpdateService");
        notifyRelroCreationCompleted();
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.webkit.IWebViewUpdateService");
      return true;
    }
    
    private static class Proxy implements IWebViewUpdateService {
      public static IWebViewUpdateService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.webkit.IWebViewUpdateService";
      }
      
      public void notifyRelroCreationCompleted() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.webkit.IWebViewUpdateService");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IWebViewUpdateService.Stub.getDefaultImpl() != null) {
            IWebViewUpdateService.Stub.getDefaultImpl().notifyRelroCreationCompleted();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public WebViewProviderResponse waitForAndGetProvider() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          WebViewProviderResponse webViewProviderResponse;
          parcel1.writeInterfaceToken("android.webkit.IWebViewUpdateService");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IWebViewUpdateService.Stub.getDefaultImpl() != null) {
            webViewProviderResponse = IWebViewUpdateService.Stub.getDefaultImpl().waitForAndGetProvider();
            return webViewProviderResponse;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            webViewProviderResponse = (WebViewProviderResponse)WebViewProviderResponse.CREATOR.createFromParcel(parcel2);
          } else {
            webViewProviderResponse = null;
          } 
          return webViewProviderResponse;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String changeProviderAndSetting(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.webkit.IWebViewUpdateService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IWebViewUpdateService.Stub.getDefaultImpl() != null) {
            param2String = IWebViewUpdateService.Stub.getDefaultImpl().changeProviderAndSetting(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public WebViewProviderInfo[] getValidWebViewPackages() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.webkit.IWebViewUpdateService");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IWebViewUpdateService.Stub.getDefaultImpl() != null)
            return IWebViewUpdateService.Stub.getDefaultImpl().getValidWebViewPackages(); 
          parcel2.readException();
          return (WebViewProviderInfo[])parcel2.createTypedArray(WebViewProviderInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public WebViewProviderInfo[] getAllWebViewPackages() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.webkit.IWebViewUpdateService");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IWebViewUpdateService.Stub.getDefaultImpl() != null)
            return IWebViewUpdateService.Stub.getDefaultImpl().getAllWebViewPackages(); 
          parcel2.readException();
          return (WebViewProviderInfo[])parcel2.createTypedArray(WebViewProviderInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getCurrentWebViewPackageName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.webkit.IWebViewUpdateService");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IWebViewUpdateService.Stub.getDefaultImpl() != null)
            return IWebViewUpdateService.Stub.getDefaultImpl().getCurrentWebViewPackageName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PackageInfo getCurrentWebViewPackage() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          PackageInfo packageInfo;
          parcel1.writeInterfaceToken("android.webkit.IWebViewUpdateService");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IWebViewUpdateService.Stub.getDefaultImpl() != null) {
            packageInfo = IWebViewUpdateService.Stub.getDefaultImpl().getCurrentWebViewPackage();
            return packageInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            packageInfo = (PackageInfo)PackageInfo.CREATOR.createFromParcel(parcel2);
          } else {
            packageInfo = null;
          } 
          return packageInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isMultiProcessEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.webkit.IWebViewUpdateService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(8, parcel1, parcel2, 0);
          if (!bool2 && IWebViewUpdateService.Stub.getDefaultImpl() != null) {
            bool1 = IWebViewUpdateService.Stub.getDefaultImpl().isMultiProcessEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enableMultiProcess(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.webkit.IWebViewUpdateService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool1 && IWebViewUpdateService.Stub.getDefaultImpl() != null) {
            IWebViewUpdateService.Stub.getDefaultImpl().enableMultiProcess(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IWebViewUpdateService param1IWebViewUpdateService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWebViewUpdateService != null) {
          Proxy.sDefaultImpl = param1IWebViewUpdateService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWebViewUpdateService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
