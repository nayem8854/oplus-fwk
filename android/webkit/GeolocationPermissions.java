package android.webkit;

import java.util.Set;

public class GeolocationPermissions {
  public static GeolocationPermissions getInstance() {
    return WebViewFactory.getProvider().getGeolocationPermissions();
  }
  
  public void getOrigins(ValueCallback<Set<String>> paramValueCallback) {}
  
  public void getAllowed(String paramString, ValueCallback<Boolean> paramValueCallback) {}
  
  public void clear(String paramString) {}
  
  public void allow(String paramString) {}
  
  public void clearAll() {}
  
  public static interface Callback {
    void invoke(String param1String, boolean param1Boolean1, boolean param1Boolean2);
  }
}
