package android.webkit;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public class PluginList {
  private ArrayList<Plugin> mPlugins = new ArrayList<>();
  
  @Deprecated
  public List getList() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mPlugins : Ljava/util/ArrayList;
    //   6: astore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: aload_1
    //   10: areturn
    //   11: astore_1
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_1
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #57	-> 2
    //   #57	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	7	11	finally
  }
  
  @Deprecated
  public void addPlugin(Plugin paramPlugin) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mPlugins : Ljava/util/ArrayList;
    //   6: aload_1
    //   7: invokevirtual contains : (Ljava/lang/Object;)Z
    //   10: ifne -> 22
    //   13: aload_0
    //   14: getfield mPlugins : Ljava/util/ArrayList;
    //   17: aload_1
    //   18: invokevirtual add : (Ljava/lang/Object;)Z
    //   21: pop
    //   22: aload_0
    //   23: monitorexit
    //   24: return
    //   25: astore_1
    //   26: aload_0
    //   27: monitorexit
    //   28: aload_1
    //   29: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #69	-> 2
    //   #70	-> 13
    //   #72	-> 22
    //   #68	-> 25
    // Exception table:
    //   from	to	target	type
    //   2	13	25	finally
    //   13	22	25	finally
  }
  
  @Deprecated
  public void removePlugin(Plugin paramPlugin) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mPlugins : Ljava/util/ArrayList;
    //   6: aload_1
    //   7: invokevirtual indexOf : (Ljava/lang/Object;)I
    //   10: istore_2
    //   11: iload_2
    //   12: iconst_m1
    //   13: if_icmpeq -> 25
    //   16: aload_0
    //   17: getfield mPlugins : Ljava/util/ArrayList;
    //   20: iload_2
    //   21: invokevirtual remove : (I)Ljava/lang/Object;
    //   24: pop
    //   25: aload_0
    //   26: monitorexit
    //   27: return
    //   28: astore_1
    //   29: aload_0
    //   30: monitorexit
    //   31: aload_1
    //   32: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #83	-> 2
    //   #84	-> 11
    //   #85	-> 16
    //   #87	-> 25
    //   #82	-> 28
    // Exception table:
    //   from	to	target	type
    //   2	11	28	finally
    //   16	25	28	finally
  }
  
  @Deprecated
  public void clear() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mPlugins : Ljava/util/ArrayList;
    //   6: invokevirtual clear : ()V
    //   9: aload_0
    //   10: monitorexit
    //   11: return
    //   12: astore_1
    //   13: aload_0
    //   14: monitorexit
    //   15: aload_1
    //   16: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #98	-> 2
    //   #99	-> 9
    //   #97	-> 12
    // Exception table:
    //   from	to	target	type
    //   2	9	12	finally
  }
  
  @Deprecated
  public void pluginClicked(Context paramContext, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mPlugins : Ljava/util/ArrayList;
    //   6: astore_3
    //   7: aload_3
    //   8: iload_2
    //   9: invokevirtual get : (I)Ljava/lang/Object;
    //   12: checkcast android/webkit/Plugin
    //   15: astore_3
    //   16: aload_3
    //   17: aload_1
    //   18: invokevirtual dispatchClickEvent : (Landroid/content/Context;)V
    //   21: goto -> 34
    //   24: astore_1
    //   25: goto -> 34
    //   28: astore_1
    //   29: aload_0
    //   30: monitorexit
    //   31: aload_1
    //   32: athrow
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #111	-> 2
    //   #112	-> 16
    //   #116	-> 21
    //   #113	-> 24
    //   #110	-> 28
    //   #113	-> 33
    //   #117	-> 34
    // Exception table:
    //   from	to	target	type
    //   2	7	33	java/lang/IndexOutOfBoundsException
    //   2	7	28	finally
    //   7	16	24	java/lang/IndexOutOfBoundsException
    //   7	16	28	finally
    //   16	21	24	java/lang/IndexOutOfBoundsException
    //   16	21	28	finally
  }
}
