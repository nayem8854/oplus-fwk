package android.webkit;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TracingConfig {
  public static final int CATEGORIES_ALL = 1;
  
  public static final int CATEGORIES_ANDROID_WEBVIEW = 2;
  
  public static final int CATEGORIES_FRAME_VIEWER = 64;
  
  public static final int CATEGORIES_INPUT_LATENCY = 8;
  
  public static final int CATEGORIES_JAVASCRIPT_AND_RENDERING = 32;
  
  public static final int CATEGORIES_NONE = 0;
  
  public static final int CATEGORIES_RENDERING = 16;
  
  public static final int CATEGORIES_WEB_DEVELOPER = 4;
  
  public static final int RECORD_CONTINUOUSLY = 1;
  
  public static final int RECORD_UNTIL_FULL = 0;
  
  private final List<String> mCustomIncludedCategories;
  
  private int mPredefinedCategories;
  
  private int mTracingMode;
  
  public TracingConfig(int paramInt1, List<String> paramList, int paramInt2) {
    ArrayList<String> arrayList = new ArrayList();
    this.mPredefinedCategories = paramInt1;
    arrayList.addAll(paramList);
    this.mTracingMode = paramInt2;
  }
  
  public int getPredefinedCategories() {
    return this.mPredefinedCategories;
  }
  
  public List<String> getCustomIncludedCategories() {
    return this.mCustomIncludedCategories;
  }
  
  public int getTracingMode() {
    return this.mTracingMode;
  }
  
  public static class Builder {
    private int mPredefinedCategories = 0;
    
    private final List<String> mCustomIncludedCategories = new ArrayList<>();
    
    private int mTracingMode = 1;
    
    public TracingConfig build() {
      return new TracingConfig(this.mPredefinedCategories, this.mCustomIncludedCategories, this.mTracingMode);
    }
    
    public Builder addCategories(int... param1VarArgs) {
      int i;
      byte b;
      for (i = param1VarArgs.length, b = 0; b < i; ) {
        int j = param1VarArgs[b];
        this.mPredefinedCategories |= j;
        b++;
      } 
      return this;
    }
    
    public Builder addCategories(String... param1VarArgs) {
      int i;
      byte b;
      for (i = param1VarArgs.length, b = 0; b < i; ) {
        String str = param1VarArgs[b];
        this.mCustomIncludedCategories.add(str);
        b++;
      } 
      return this;
    }
    
    public Builder addCategories(Collection<String> param1Collection) {
      this.mCustomIncludedCategories.addAll(param1Collection);
      return this;
    }
    
    public Builder setTracingMode(int param1Int) {
      this.mTracingMode = param1Int;
      return this;
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface PredefinedCategories {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface TracingMode {}
}
