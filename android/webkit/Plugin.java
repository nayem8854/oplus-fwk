package android.webkit;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

@Deprecated
public class Plugin {
  private String mDescription;
  
  private String mFileName;
  
  private PreferencesClickHandler mHandler;
  
  private String mName;
  
  private String mPath;
  
  @Deprecated
  public Plugin(String paramString1, String paramString2, String paramString3, String paramString4) {
    this.mName = paramString1;
    this.mPath = paramString2;
    this.mFileName = paramString3;
    this.mDescription = paramString4;
    this.mHandler = new DefaultClickHandler();
  }
  
  @Deprecated
  public String toString() {
    return this.mName;
  }
  
  @Deprecated
  public String getName() {
    return this.mName;
  }
  
  @Deprecated
  public String getPath() {
    return this.mPath;
  }
  
  @Deprecated
  public String getFileName() {
    return this.mFileName;
  }
  
  @Deprecated
  public String getDescription() {
    return this.mDescription;
  }
  
  @Deprecated
  public void setName(String paramString) {
    this.mName = paramString;
  }
  
  @Deprecated
  public void setPath(String paramString) {
    this.mPath = paramString;
  }
  
  @Deprecated
  public void setFileName(String paramString) {
    this.mFileName = paramString;
  }
  
  @Deprecated
  public void setDescription(String paramString) {
    this.mDescription = paramString;
  }
  
  @Deprecated
  public void setClickHandler(PreferencesClickHandler paramPreferencesClickHandler) {
    this.mHandler = paramPreferencesClickHandler;
  }
  
  @Deprecated
  public void dispatchClickEvent(Context paramContext) {
    PreferencesClickHandler preferencesClickHandler = this.mHandler;
    if (preferencesClickHandler != null)
      preferencesClickHandler.handleClickEvent(paramContext); 
  }
  
  @Deprecated
  class DefaultClickHandler implements PreferencesClickHandler, DialogInterface.OnClickListener {
    private AlertDialog mDialog;
    
    final Plugin this$0;
    
    private DefaultClickHandler() {}
    
    @Deprecated
    public void handleClickEvent(Context param1Context) {
      if (this.mDialog == null) {
        AlertDialog.Builder builder2 = new AlertDialog.Builder(param1Context);
        Plugin plugin2 = Plugin.this;
        AlertDialog.Builder builder3 = builder2.setTitle(plugin2.mName);
        Plugin plugin1 = Plugin.this;
        AlertDialog.Builder builder1 = builder3.setMessage(plugin1.mDescription);
        builder1 = builder1.setPositiveButton(17039370, this);
        builder1 = builder1.setCancelable(false);
        this.mDialog = builder1.show();
      } 
    }
    
    @Deprecated
    public void onClick(DialogInterface param1DialogInterface, int param1Int) {
      this.mDialog.dismiss();
      this.mDialog = null;
    }
  }
  
  public static interface PreferencesClickHandler {
    void handleClickEvent(Context param1Context);
  }
}
