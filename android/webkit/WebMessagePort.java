package android.webkit;

import android.os.Handler;

public abstract class WebMessagePort {
  public abstract void close();
  
  public abstract void postMessage(WebMessage paramWebMessage);
  
  public abstract void setWebMessageCallback(WebMessageCallback paramWebMessageCallback);
  
  public abstract void setWebMessageCallback(WebMessageCallback paramWebMessageCallback, Handler paramHandler);
  
  public static abstract class WebMessageCallback {
    public void onMessage(WebMessagePort param1WebMessagePort, WebMessage param1WebMessage) {}
  }
}
