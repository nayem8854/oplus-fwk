package android.webkit;

import android.annotation.SystemApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import java.util.List;

@SystemApi
public interface WebViewFactoryProvider {
  WebViewProvider createWebView(WebView paramWebView, WebView.PrivateAccess paramPrivateAccess);
  
  CookieManager getCookieManager();
  
  GeolocationPermissions getGeolocationPermissions();
  
  default PacProcessor getPacProcessor() {
    throw new UnsupportedOperationException("Not implemented");
  }
  
  ServiceWorkerController getServiceWorkerController();
  
  Statics getStatics();
  
  TokenBindingService getTokenBindingService();
  
  TracingController getTracingController();
  
  WebIconDatabase getWebIconDatabase();
  
  WebStorage getWebStorage();
  
  ClassLoader getWebViewClassLoader();
  
  WebViewDatabase getWebViewDatabase(Context paramContext);
  
  public static interface Statics {
    void clearClientCertPreferences(Runnable param1Runnable);
    
    void enableSlowWholeDocumentDraw();
    
    String findAddress(String param1String);
    
    void freeMemoryForTests();
    
    String getDefaultUserAgent(Context param1Context);
    
    Uri getSafeBrowsingPrivacyPolicyUrl();
    
    void initSafeBrowsing(Context param1Context, ValueCallback<Boolean> param1ValueCallback);
    
    Uri[] parseFileChooserResult(int param1Int, Intent param1Intent);
    
    void setSafeBrowsingWhitelist(List<String> param1List, ValueCallback<Boolean> param1ValueCallback);
    
    void setWebContentsDebuggingEnabled(boolean param1Boolean);
  }
}
