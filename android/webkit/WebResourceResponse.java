package android.webkit;

import android.annotation.SystemApi;
import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.util.Map;

public class WebResourceResponse {
  private String mEncoding;
  
  private boolean mImmutable;
  
  private InputStream mInputStream;
  
  private String mMimeType;
  
  private String mReasonPhrase;
  
  private Map<String, String> mResponseHeaders;
  
  private int mStatusCode;
  
  public WebResourceResponse(String paramString1, String paramString2, InputStream paramInputStream) {
    this.mMimeType = paramString1;
    this.mEncoding = paramString2;
    setData(paramInputStream);
  }
  
  public WebResourceResponse(String paramString1, String paramString2, int paramInt, String paramString3, Map<String, String> paramMap, InputStream paramInputStream) {
    this(paramString1, paramString2, paramInputStream);
    setStatusCodeAndReasonPhrase(paramInt, paramString3);
    setResponseHeaders(paramMap);
  }
  
  public void setMimeType(String paramString) {
    checkImmutable();
    this.mMimeType = paramString;
  }
  
  public String getMimeType() {
    return this.mMimeType;
  }
  
  public void setEncoding(String paramString) {
    checkImmutable();
    this.mEncoding = paramString;
  }
  
  public String getEncoding() {
    return this.mEncoding;
  }
  
  public void setStatusCodeAndReasonPhrase(int paramInt, String paramString) {
    checkImmutable();
    if (paramInt >= 100) {
      if (paramInt <= 599) {
        if (paramInt <= 299 || paramInt >= 400) {
          if (paramString != null) {
            if (!paramString.trim().isEmpty()) {
              for (byte b = 0; b < paramString.length(); ) {
                char c = paramString.charAt(b);
                if (c <= '') {
                  b++;
                  continue;
                } 
                throw new IllegalArgumentException("reasonPhrase can't contain non-ASCII characters.");
              } 
              this.mStatusCode = paramInt;
              this.mReasonPhrase = paramString;
              return;
            } 
            throw new IllegalArgumentException("reasonPhrase can't be empty.");
          } 
          throw new IllegalArgumentException("reasonPhrase can't be null.");
        } 
        throw new IllegalArgumentException("statusCode can't be in the [300, 399] range.");
      } 
      throw new IllegalArgumentException("statusCode can't be greater than 599.");
    } 
    throw new IllegalArgumentException("statusCode can't be less than 100.");
  }
  
  public int getStatusCode() {
    return this.mStatusCode;
  }
  
  public String getReasonPhrase() {
    return this.mReasonPhrase;
  }
  
  public void setResponseHeaders(Map<String, String> paramMap) {
    checkImmutable();
    this.mResponseHeaders = paramMap;
  }
  
  public Map<String, String> getResponseHeaders() {
    return this.mResponseHeaders;
  }
  
  public void setData(InputStream paramInputStream) {
    checkImmutable();
    if (paramInputStream == null || !StringBufferInputStream.class.isAssignableFrom(paramInputStream.getClass())) {
      this.mInputStream = paramInputStream;
      return;
    } 
    throw new IllegalArgumentException("StringBufferInputStream is deprecated and must not be passed to a WebResourceResponse");
  }
  
  public InputStream getData() {
    return this.mInputStream;
  }
  
  @SystemApi
  public WebResourceResponse(boolean paramBoolean, String paramString1, String paramString2, int paramInt, String paramString3, Map<String, String> paramMap, InputStream paramInputStream) {
    this.mImmutable = paramBoolean;
    this.mMimeType = paramString1;
    this.mEncoding = paramString2;
    this.mStatusCode = paramInt;
    this.mReasonPhrase = paramString3;
    this.mResponseHeaders = paramMap;
    this.mInputStream = paramInputStream;
  }
  
  private void checkImmutable() {
    if (!this.mImmutable)
      return; 
    throw new IllegalStateException("This WebResourceResponse instance is immutable");
  }
}
