package android.webkit;

import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.ChildZygoteProcess;
import android.os.Process;
import android.os.ZygoteProcess;
import android.text.TextUtils;
import android.util.Log;

public class WebViewZygote {
  private static final String LOGTAG = "WebViewZygote";
  
  private static final Object sLock = new Object();
  
  private static boolean sMultiprocessEnabled = false;
  
  private static PackageInfo sPackage;
  
  private static ChildZygoteProcess sZygote;
  
  public static ZygoteProcess getProcess() {
    synchronized (sLock) {
      if (sZygote != null)
        return (ZygoteProcess)sZygote; 
      connectToZygoteIfNeededLocked();
      return (ZygoteProcess)sZygote;
    } 
  }
  
  public static String getPackageName() {
    synchronized (sLock) {
      return sPackage.packageName;
    } 
  }
  
  public static boolean isMultiprocessEnabled() {
    synchronized (sLock) {
      boolean bool;
      if (sMultiprocessEnabled && sPackage != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
  }
  
  public static void setMultiprocessEnabled(boolean paramBoolean) {
    synchronized (sLock) {
      sMultiprocessEnabled = paramBoolean;
      if (!paramBoolean)
        stopZygoteLocked(); 
      return;
    } 
  }
  
  static void onWebViewProviderChanged(PackageInfo paramPackageInfo) {
    synchronized (sLock) {
      sPackage = paramPackageInfo;
      if (!sMultiprocessEnabled)
        return; 
      stopZygoteLocked();
      return;
    } 
  }
  
  private static void stopZygoteLocked() {
    ChildZygoteProcess childZygoteProcess = sZygote;
    if (childZygoteProcess != null) {
      childZygoteProcess.close();
      Process.killProcess(sZygote.getPid());
      sZygote = null;
    } 
  }
  
  private static void connectToZygoteIfNeededLocked() {
    if (sZygote != null)
      return; 
    PackageInfo packageInfo = sPackage;
    if (packageInfo == null) {
      Log.e("WebViewZygote", "Cannot connect to zygote, no package specified");
      return;
    } 
    try {
      String str1 = packageInfo.applicationInfo.primaryCpuAbi;
      ZygoteProcess zygoteProcess = Process.ZYGOTE_PROCESS;
      String[] arrayOfString = Build.SUPPORTED_ABIS;
      String str2 = TextUtils.join(",", (Object[])arrayOfString);
      ChildZygoteProcess childZygoteProcess = zygoteProcess.startChildZygote("com.android.internal.os.WebViewZygoteInit", "webview_zygote", 1053, 1053, null, 0, "webview_zygote", str1, str2, null, 99000, 2147483647);
      ZygoteProcess.waitForConnectionToZygote(childZygoteProcess.getPrimarySocketAddress());
      sZygote.preloadApp(sPackage.applicationInfo, str1);
    } catch (Exception exception) {
      Log.e("WebViewZygote", "Error connecting to webview zygote", exception);
      stopZygoteLocked();
    } 
  }
}
