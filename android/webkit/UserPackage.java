package android.webkit;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.UserInfo;
import android.os.UserManager;
import java.util.ArrayList;
import java.util.List;

public class UserPackage {
  public static final int MINIMUM_SUPPORTED_SDK = 30;
  
  private final PackageInfo mPackageInfo;
  
  private final UserInfo mUserInfo;
  
  public UserPackage(UserInfo paramUserInfo, PackageInfo paramPackageInfo) {
    this.mUserInfo = paramUserInfo;
    this.mPackageInfo = paramPackageInfo;
  }
  
  public static List<UserPackage> getPackageInfosAllUsers(Context paramContext, String paramString, int paramInt) {
    List<UserInfo> list = getAllUsers(paramContext);
    ArrayList<UserPackage> arrayList = new ArrayList(list.size());
    for (UserInfo userInfo : list) {
      PackageInfo packageInfo;
      list = null;
      try {
        PackageInfo packageInfo1 = paramContext.getPackageManager().getPackageInfoAsUser(paramString, paramInt, userInfo.id);
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {}
      arrayList.add(new UserPackage(userInfo, packageInfo));
    } 
    return arrayList;
  }
  
  public boolean isEnabledPackage() {
    PackageInfo packageInfo = this.mPackageInfo;
    if (packageInfo == null)
      return false; 
    return packageInfo.applicationInfo.enabled;
  }
  
  public boolean isInstalledPackage() {
    PackageInfo packageInfo = this.mPackageInfo;
    boolean bool1 = false;
    if (packageInfo == null)
      return false; 
    boolean bool2 = bool1;
    if ((packageInfo.applicationInfo.flags & 0x800000) != 0) {
      bool2 = bool1;
      if ((this.mPackageInfo.applicationInfo.privateFlags & 0x1) == 0)
        bool2 = true; 
    } 
    return bool2;
  }
  
  public static boolean hasCorrectTargetSdkVersion(PackageInfo paramPackageInfo) {
    boolean bool;
    if (paramPackageInfo.applicationInfo.targetSdkVersion >= 30) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public UserInfo getUserInfo() {
    return this.mUserInfo;
  }
  
  public PackageInfo getPackageInfo() {
    return this.mPackageInfo;
  }
  
  private static List<UserInfo> getAllUsers(Context paramContext) {
    UserManager userManager = (UserManager)paramContext.getSystemService("user");
    return userManager.getUsers(false);
  }
}
