package android.webkit;

import android.net.ParseException;
import android.net.Uri;
import android.net.WebAddress;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class URLUtil {
  static final String ASSET_BASE = "file:///android_asset/";
  
  static final String CONTENT_BASE = "content:";
  
  public static String guessUrl(String paramString) {
    StringBuilder stringBuilder;
    if (paramString.length() == 0)
      return paramString; 
    if (paramString.startsWith("about:"))
      return paramString; 
    if (paramString.startsWith("data:"))
      return paramString; 
    if (paramString.startsWith("file:"))
      return paramString; 
    if (paramString.startsWith("javascript:"))
      return paramString; 
    String str = paramString;
    if (paramString.endsWith(".") == true)
      str = paramString.substring(0, paramString.length() - 1); 
    try {
      WebAddress webAddress = new WebAddress(str);
      if (webAddress.getHost().indexOf('.') == -1) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("www.");
        stringBuilder.append(webAddress.getHost());
        stringBuilder.append(".com");
        webAddress.setHost(stringBuilder.toString());
      } 
      return webAddress.toString();
    } catch (ParseException parseException) {
      return (String)stringBuilder;
    } 
  }
  
  public static String composeSearchUrl(String paramString1, String paramString2, String paramString3) {
    int i = paramString2.indexOf(paramString3);
    if (i < 0)
      return null; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString2.substring(0, i));
    try {
      paramString1 = URLEncoder.encode(paramString1, "utf-8");
      stringBuilder.append(paramString1);
      int j = paramString3.length();
      stringBuilder.append(paramString2.substring(j + i));
      return stringBuilder.toString();
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      return null;
    } 
  }
  
  public static byte[] decode(byte[] paramArrayOfbyte) throws IllegalArgumentException {
    if (paramArrayOfbyte.length == 0)
      return new byte[0]; 
    byte[] arrayOfByte = new byte[paramArrayOfbyte.length];
    byte b = 0;
    for (int i = 0; i < paramArrayOfbyte.length; i = j + 1, b++) {
      byte b1 = paramArrayOfbyte[i];
      int j = i;
      byte b2 = b1;
      if (b1 == 37)
        if (paramArrayOfbyte.length - i > 2) {
          j = parseHex(paramArrayOfbyte[i + 1]);
          b2 = paramArrayOfbyte[i + 2];
          b2 = (byte)(j * 16 + parseHex(b2));
          j = i + 2;
        } else {
          throw new IllegalArgumentException("Invalid format");
        }  
      arrayOfByte[b] = b2;
    } 
    paramArrayOfbyte = new byte[b];
    System.arraycopy(arrayOfByte, 0, paramArrayOfbyte, 0, b);
    return paramArrayOfbyte;
  }
  
  static boolean verifyURLEncoding(String paramString) {
    int i = paramString.length();
    if (i == 0)
      return false; 
    int j = paramString.indexOf('%');
    while (j >= 0 && j < i) {
      if (j < i - 2) {
        j++;
        try {
          parseHex((byte)paramString.charAt(j));
          parseHex((byte)paramString.charAt(++j));
          j = paramString.indexOf('%', j + 1);
        } catch (IllegalArgumentException illegalArgumentException) {
          return false;
        } 
        continue;
      } 
      return false;
    } 
    return true;
  }
  
  private static int parseHex(byte paramByte) {
    if (paramByte >= 48 && paramByte <= 57)
      return paramByte - 48; 
    if (paramByte >= 65 && paramByte <= 70)
      return paramByte - 65 + 10; 
    if (paramByte >= 97 && paramByte <= 102)
      return paramByte - 97 + 10; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid hex char '");
    stringBuilder.append(paramByte);
    stringBuilder.append("'");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static boolean isAssetUrl(String paramString) {
    boolean bool;
    if (paramString != null && paramString.startsWith("file:///android_asset/")) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isResourceUrl(String paramString) {
    boolean bool;
    if (paramString != null && paramString.startsWith("file:///android_res/")) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @Deprecated
  public static boolean isCookielessProxyUrl(String paramString) {
    boolean bool;
    if (paramString != null && paramString.startsWith("file:///cookieless_proxy/")) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isFileUrl(String paramString) {
    boolean bool;
    if (paramString != null && paramString.startsWith("file:") && 
      !paramString.startsWith("file:///android_asset/") && 
      !paramString.startsWith("file:///cookieless_proxy/")) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isAboutUrl(String paramString) {
    boolean bool;
    if (paramString != null && paramString.startsWith("about:")) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isDataUrl(String paramString) {
    boolean bool;
    if (paramString != null && paramString.startsWith("data:")) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isJavaScriptUrl(String paramString) {
    boolean bool;
    if (paramString != null && paramString.startsWith("javascript:")) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isHttpUrl(String paramString) {
    boolean bool = false;
    if (paramString != null && 
      paramString.length() > 6 && 
      paramString.substring(0, 7).equalsIgnoreCase("http://"))
      bool = true; 
    return bool;
  }
  
  public static boolean isHttpsUrl(String paramString) {
    boolean bool = false;
    if (paramString != null && 
      paramString.length() > 7 && 
      paramString.substring(0, 8).equalsIgnoreCase("https://"))
      bool = true; 
    return bool;
  }
  
  public static boolean isNetworkUrl(String paramString) {
    boolean bool = false;
    if (paramString == null || paramString.length() == 0)
      return false; 
    if (isHttpUrl(paramString) || isHttpsUrl(paramString))
      bool = true; 
    return bool;
  }
  
  public static boolean isContentUrl(String paramString) {
    boolean bool;
    if (paramString != null && paramString.startsWith("content:")) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isValidUrl(String paramString) {
    boolean bool = false;
    if (paramString == null || paramString.length() == 0)
      return false; 
    if (isAssetUrl(paramString) || 
      isResourceUrl(paramString) || 
      isFileUrl(paramString) || 
      isAboutUrl(paramString) || 
      isHttpUrl(paramString) || 
      isHttpsUrl(paramString) || 
      isJavaScriptUrl(paramString) || 
      isContentUrl(paramString))
      bool = true; 
    return bool;
  }
  
  public static String stripAnchor(String paramString) {
    int i = paramString.indexOf('#');
    if (i != -1)
      return paramString.substring(0, i); 
    return paramString;
  }
  
  public static final String guessFileName(String paramString1, String paramString2, String paramString3) {
    String str1, str2 = null;
    String str3 = null, str4 = null;
    String str5 = str2;
    if (!false) {
      str5 = str2;
      if (paramString2 != null) {
        paramString2 = parseContentDisposition(paramString2);
        str5 = paramString2;
        if (paramString2 != null) {
          int j = paramString2.lastIndexOf('/') + 1;
          str5 = paramString2;
          if (j > 0)
            str5 = paramString2.substring(j); 
        } 
      } 
    } 
    paramString2 = str5;
    if (str5 == null) {
      str2 = Uri.decode(paramString1);
      paramString2 = str5;
      if (str2 != null) {
        int j = str2.indexOf('?');
        paramString1 = str2;
        if (j > 0)
          paramString1 = str2.substring(0, j); 
        paramString2 = str5;
        if (!paramString1.endsWith("/")) {
          j = paramString1.lastIndexOf('/') + 1;
          paramString2 = str5;
          if (j > 0)
            paramString2 = paramString1.substring(j); 
        } 
      } 
    } 
    str5 = paramString2;
    if (paramString2 == null)
      str5 = "downloadfile"; 
    int i = str5.indexOf('.');
    if (i < 0) {
      String str;
      paramString2 = str4;
      if (paramString3 != null) {
        paramString1 = MimeTypeMap.getSingleton().getExtensionFromMimeType(paramString3);
        paramString2 = paramString1;
        if (paramString1 != null) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append(".");
          stringBuilder1.append(paramString1);
          str = stringBuilder1.toString();
        } 
      } 
      str3 = str5;
      paramString1 = str;
      if (str == null)
        if (paramString3 != null && paramString3.toLowerCase(Locale.ROOT).startsWith("text/")) {
          if (paramString3.equalsIgnoreCase("text/html")) {
            paramString1 = ".html";
            str3 = str5;
          } else {
            paramString1 = ".txt";
            str3 = str5;
          } 
        } else {
          paramString1 = ".bin";
          str3 = str5;
        }  
    } else {
      paramString1 = str3;
      if (paramString3 != null) {
        int j = str5.lastIndexOf('.');
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        paramString2 = str5.substring(j + 1);
        paramString2 = mimeTypeMap.getMimeTypeFromExtension(paramString2);
        str1 = str3;
        if (paramString2 != null) {
          str1 = str3;
          if (!paramString2.equalsIgnoreCase(paramString3)) {
            paramString2 = MimeTypeMap.getSingleton().getExtensionFromMimeType(paramString3);
            str1 = paramString2;
            if (paramString2 != null) {
              StringBuilder stringBuilder1 = new StringBuilder();
              stringBuilder1.append(".");
              stringBuilder1.append(paramString2);
              str1 = stringBuilder1.toString();
            } 
          } 
        } 
      } 
      paramString2 = str1;
      if (str1 == null)
        paramString2 = str5.substring(i); 
      str3 = str5.substring(0, i);
      str1 = paramString2;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str3);
    stringBuilder.append(str1);
    return stringBuilder.toString();
  }
  
  private static final Pattern CONTENT_DISPOSITION_PATTERN = Pattern.compile("attachment;\\s*filename\\s*=\\s*(\"?)([^\"]*)\\1\\s*$", 2);
  
  static final String FILE_BASE = "file:";
  
  private static final String LOGTAG = "webkit";
  
  static final String PROXY_BASE = "file:///cookieless_proxy/";
  
  static final String RESOURCE_BASE = "file:///android_res/";
  
  private static final boolean TRACE = false;
  
  static String parseContentDisposition(String paramString) {
    try {
      Matcher matcher = CONTENT_DISPOSITION_PATTERN.matcher(paramString);
      if (matcher.find())
        return matcher.group(2); 
    } catch (IllegalStateException illegalStateException) {}
    return null;
  }
}
