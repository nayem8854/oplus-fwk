package android.webkit;

import android.annotation.SystemApi;
import android.content.pm.Signature;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;

@SystemApi
public final class WebViewProviderInfo implements Parcelable {
  public WebViewProviderInfo(String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2, String[] paramArrayOfString) {
    this.packageName = paramString1;
    this.description = paramString2;
    this.availableByDefault = paramBoolean1;
    this.isFallback = paramBoolean2;
    if (paramArrayOfString == null) {
      this.signatures = new Signature[0];
    } else {
      this.signatures = new Signature[paramArrayOfString.length];
      for (byte b = 0; b < paramArrayOfString.length; b++)
        this.signatures[b] = new Signature(Base64.decode(paramArrayOfString[b], 0)); 
    } 
  }
  
  public static final Parcelable.Creator<WebViewProviderInfo> CREATOR = new Parcelable.Creator<WebViewProviderInfo>() {
      public WebViewProviderInfo createFromParcel(Parcel param1Parcel) {
        return new WebViewProviderInfo(param1Parcel);
      }
      
      public WebViewProviderInfo[] newArray(int param1Int) {
        return new WebViewProviderInfo[param1Int];
      }
    };
  
  public final boolean availableByDefault;
  
  public final String description;
  
  public final boolean isFallback;
  
  public final String packageName;
  
  public final Signature[] signatures;
  
  private WebViewProviderInfo(Parcel paramParcel) {
    boolean bool2;
    this.packageName = paramParcel.readString();
    this.description = paramParcel.readString();
    int i = paramParcel.readInt();
    boolean bool1 = true;
    if (i > 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.availableByDefault = bool2;
    if (paramParcel.readInt() > 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.isFallback = bool2;
    this.signatures = (Signature[])paramParcel.createTypedArray(Signature.CREATOR);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.packageName);
    paramParcel.writeString(this.description);
    paramParcel.writeInt(this.availableByDefault);
    paramParcel.writeInt(this.isFallback);
    paramParcel.writeTypedArray((Parcelable[])this.signatures, 0);
  }
}
