package android.webkit;

import android.annotation.SystemApi;

public class JsResult {
  private final ResultReceiver mReceiver;
  
  private boolean mResult;
  
  public final void cancel() {
    this.mResult = false;
    wakeUp();
  }
  
  public final void confirm() {
    this.mResult = true;
    wakeUp();
  }
  
  @SystemApi
  public JsResult(ResultReceiver paramResultReceiver) {
    this.mReceiver = paramResultReceiver;
  }
  
  @SystemApi
  public final boolean getResult() {
    return this.mResult;
  }
  
  private final void wakeUp() {
    this.mReceiver.onJsResultComplete(this);
  }
  
  @SystemApi
  public static interface ResultReceiver {
    void onJsResultComplete(JsResult param1JsResult);
  }
}
