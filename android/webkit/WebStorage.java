package android.webkit;

import android.annotation.SystemApi;
import java.util.Map;

public class WebStorage {
  public static class Origin {
    private String mOrigin = null;
    
    private long mQuota = 0L;
    
    private long mUsage = 0L;
    
    @SystemApi
    protected Origin(String param1String, long param1Long1, long param1Long2) {
      this.mOrigin = param1String;
      this.mQuota = param1Long1;
      this.mUsage = param1Long2;
    }
    
    public String getOrigin() {
      return this.mOrigin;
    }
    
    public long getQuota() {
      return this.mQuota;
    }
    
    public long getUsage() {
      return this.mUsage;
    }
  }
  
  public void getOrigins(ValueCallback<Map> paramValueCallback) {}
  
  public void getUsageForOrigin(String paramString, ValueCallback<Long> paramValueCallback) {}
  
  public void getQuotaForOrigin(String paramString, ValueCallback<Long> paramValueCallback) {}
  
  @Deprecated
  public void setQuotaForOrigin(String paramString, long paramLong) {}
  
  public void deleteOrigin(String paramString) {}
  
  public void deleteAllData() {}
  
  public static WebStorage getInstance() {
    return WebViewFactory.getProvider().getWebStorage();
  }
  
  @Deprecated
  public static interface QuotaUpdater {
    void updateQuota(long param1Long);
  }
}
