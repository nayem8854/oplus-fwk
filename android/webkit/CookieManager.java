package android.webkit;

import android.annotation.SystemApi;
import android.net.WebAddress;

public abstract class CookieManager {
  protected Object clone() throws CloneNotSupportedException {
    throw new CloneNotSupportedException("doesn't implement Cloneable");
  }
  
  public static CookieManager getInstance() {
    return WebViewFactory.getProvider().getCookieManager();
  }
  
  @SystemApi
  public String getCookie(WebAddress paramWebAddress) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokevirtual toString : ()Ljava/lang/String;
    //   7: invokevirtual getCookie : (Ljava/lang/String;)Ljava/lang/String;
    //   10: astore_1
    //   11: aload_0
    //   12: monitorexit
    //   13: aload_1
    //   14: areturn
    //   15: astore_1
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_1
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #171	-> 2
    //   #171	-> 15
    // Exception table:
    //   from	to	target	type
    //   2	11	15	finally
  }
  
  public static boolean allowFileSchemeCookies() {
    return getInstance().allowFileSchemeCookiesImpl();
  }
  
  @Deprecated
  public static void setAcceptFileSchemeCookies(boolean paramBoolean) {
    getInstance().setAcceptFileSchemeCookiesImpl(paramBoolean);
  }
  
  public abstract boolean acceptCookie();
  
  public abstract boolean acceptThirdPartyCookies(WebView paramWebView);
  
  @SystemApi
  protected abstract boolean allowFileSchemeCookiesImpl();
  
  public abstract void flush();
  
  public abstract String getCookie(String paramString);
  
  @SystemApi
  public abstract String getCookie(String paramString, boolean paramBoolean);
  
  public abstract boolean hasCookies();
  
  @SystemApi
  public abstract boolean hasCookies(boolean paramBoolean);
  
  @Deprecated
  public abstract void removeAllCookie();
  
  public abstract void removeAllCookies(ValueCallback<Boolean> paramValueCallback);
  
  @Deprecated
  public abstract void removeExpiredCookie();
  
  @Deprecated
  public abstract void removeSessionCookie();
  
  public abstract void removeSessionCookies(ValueCallback<Boolean> paramValueCallback);
  
  public abstract void setAcceptCookie(boolean paramBoolean);
  
  @SystemApi
  protected abstract void setAcceptFileSchemeCookiesImpl(boolean paramBoolean);
  
  public abstract void setAcceptThirdPartyCookies(WebView paramWebView, boolean paramBoolean);
  
  public abstract void setCookie(String paramString1, String paramString2);
  
  public abstract void setCookie(String paramString1, String paramString2, ValueCallback<Boolean> paramValueCallback);
}
