package android.webkit;

import android.content.Context;
import android.view.View;

public interface PluginStub {
  View getEmbeddedView(int paramInt, Context paramContext);
  
  View getFullScreenView(int paramInt, Context paramContext);
}
