package android.webkit;

import android.app.ActivityManagerInternal;
import android.app.ActivityThread;
import android.app.LoadedApk;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.RemoteException;
import android.util.Log;
import com.android.server.LocalServices;
import dalvik.system.VMRuntime;
import java.util.Arrays;

public class WebViewLibraryLoader {
  private static final String CHROMIUM_WEBVIEW_NATIVE_RELRO_32 = "/data/misc/shared_relro/libwebviewchromium32.relro";
  
  private static final String CHROMIUM_WEBVIEW_NATIVE_RELRO_64 = "/data/misc/shared_relro/libwebviewchromium64.relro";
  
  private static final boolean DEBUG = false;
  
  private static final String LOGTAG = WebViewLibraryLoader.class.getSimpleName();
  
  private static boolean sAddressSpaceReserved = false;
  
  private static class RelroFileCreator {
    public static void main(String[] param1ArrayOfString) {
      boolean bool = VMRuntime.getRuntime().is64Bit();
      try {
        String str1;
        if (param1ArrayOfString.length != 2 || param1ArrayOfString[0] == null || param1ArrayOfString[1] == null) {
          String str = WebViewLibraryLoader.LOGTAG;
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("Invalid RelroFileCreator args: ");
          stringBuilder1.append(Arrays.toString((Object[])param1ArrayOfString));
          Log.e(str, stringBuilder1.toString());
          return;
        } 
        RemoteException remoteException2 = remoteException1[0];
        RemoteException remoteException3 = remoteException1[1];
        String str2 = WebViewLibraryLoader.LOGTAG;
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("RelroFileCreator (64bit = ");
        stringBuilder.append(bool);
        stringBuilder.append("), package: ");
        stringBuilder.append((String)remoteException2);
        stringBuilder.append(" library: ");
        stringBuilder.append((String)remoteException3);
        Log.v(str2, stringBuilder.toString());
        if (!WebViewLibraryLoader.sAddressSpaceReserved) {
          Log.e(WebViewLibraryLoader.LOGTAG, "can't create relro file; address space not reserved");
          return;
        } 
        LoadedApk loadedApk = ActivityThread.currentActivityThread().getPackageInfo((String)remoteException2, null, 3);
        if (bool) {
          str1 = "/data/misc/shared_relro/libwebviewchromium64.relro";
        } else {
          str1 = "/data/misc/shared_relro/libwebviewchromium32.relro";
        } 
        ClassLoader classLoader = loadedApk.getClassLoader();
        bool = WebViewLibraryLoader.nativeCreateRelroFile((String)remoteException3, str1, classLoader);
        return;
      } finally {
        try {
          WebViewFactory.getUpdateServiceUnchecked().notifyRelroCreationCompleted();
        } catch (RemoteException remoteException) {
          Log.e(WebViewLibraryLoader.LOGTAG, "error notifying update service", (Throwable)remoteException);
        } 
        if (!false)
          Log.e(WebViewLibraryLoader.LOGTAG, "failed to create relro file"); 
        System.exit(0);
      } 
    }
  }
  
  static void createRelroFile(boolean paramBoolean, String paramString1, String paramString2) {
    final String abi;
    if (paramBoolean) {
      str = Build.SUPPORTED_64_BIT_ABIS[0];
    } else {
      str = Build.SUPPORTED_32_BIT_ABIS[0];
    } 
    Runnable runnable = new Runnable() {
        final String val$abi;
        
        public void run() {
          try {
            String str = WebViewLibraryLoader.LOGTAG;
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("relro file creator for ");
            stringBuilder.append(abi);
            stringBuilder.append(" crashed. Proceeding without");
            Log.e(str, stringBuilder.toString());
            WebViewFactory.getUpdateService().notifyRelroCreationCompleted();
          } catch (RemoteException remoteException) {
            String str = WebViewLibraryLoader.LOGTAG;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Cannot reach WebViewUpdateService. ");
            stringBuilder.append(remoteException.getMessage());
            Log.e(str, stringBuilder.toString());
          } 
        }
      };
    try {
      ActivityManagerInternal activityManagerInternal = (ActivityManagerInternal)LocalServices.getService(ActivityManagerInternal.class);
      String str1 = RelroFileCreator.class.getName();
      StringBuilder stringBuilder = new StringBuilder();
    } finally {
      paramString2 = null;
      paramString1 = LOGTAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("error starting relro file creator for abi ");
      stringBuilder.append(str);
      Log.e(paramString1, stringBuilder.toString(), (Throwable)paramString2);
    } 
  }
  
  static int prepareNativeLibraries(PackageInfo paramPackageInfo) {
    String str = WebViewFactory.getWebViewLibrary(paramPackageInfo.applicationInfo);
    if (str == null)
      return 0; 
    return createRelros(paramPackageInfo.packageName, str);
  }
  
  private static int createRelros(String paramString1, String paramString2) {
    int i = 0;
    if (Build.SUPPORTED_32_BIT_ABIS.length > 0) {
      createRelroFile(false, paramString1, paramString2);
      i = 0 + 1;
    } 
    int j = i;
    if (Build.SUPPORTED_64_BIT_ABIS.length > 0) {
      createRelroFile(true, paramString1, paramString2);
      j = i + 1;
    } 
    return j;
  }
  
  static void reserveAddressSpaceInZygote() {
    long l;
    System.loadLibrary("webviewchromium_loader");
    boolean bool = VMRuntime.getRuntime().is64Bit();
    if (bool) {
      l = 1073741824L;
    } else {
      l = 136314880L;
    } 
    sAddressSpaceReserved = bool = nativeReserveAddressSpace(l);
    if (!bool) {
      String str = LOGTAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("reserving ");
      stringBuilder.append(l);
      stringBuilder.append(" bytes of address space failed");
      Log.e(str, stringBuilder.toString());
    } 
  }
  
  public static int loadNativeLibrary(ClassLoader paramClassLoader, String paramString) {
    String str;
    if (!sAddressSpaceReserved) {
      Log.e(LOGTAG, "can't load with relro file; address space not reserved");
      return 2;
    } 
    if (VMRuntime.getRuntime().is64Bit()) {
      str = "/data/misc/shared_relro/libwebviewchromium64.relro";
    } else {
      str = "/data/misc/shared_relro/libwebviewchromium32.relro";
    } 
    int i = nativeLoadWithRelroFile(paramString, str, paramClassLoader);
    if (i != 0)
      Log.w(LOGTAG, "failed to load with relro file, proceeding without"); 
    return i;
  }
  
  static native boolean nativeCreateRelroFile(String paramString1, String paramString2, ClassLoader paramClassLoader);
  
  static native int nativeLoadWithRelroFile(String paramString1, String paramString2, ClassLoader paramClassLoader);
  
  static native boolean nativeReserveAddressSpace(long paramLong);
}
