package android.webkit;

import android.annotation.SystemApi;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.http.SslCertificate;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.print.PrintDocumentAdapter;
import android.util.SparseArray;
import android.view.DragEvent;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStructure;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import android.view.autofill.AutofillValue;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.textclassifier.TextClassifier;
import java.io.BufferedWriter;
import java.io.File;
import java.util.Map;
import java.util.concurrent.Executor;

@SystemApi
public interface WebViewProvider {
  boolean zoomOut();
  
  boolean zoomIn();
  
  boolean zoomBy(float paramFloat);
  
  void stopLoading();
  
  boolean showFindDialog(String paramString, boolean paramBoolean);
  
  void setWebViewRenderProcessClient(Executor paramExecutor, WebViewRenderProcessClient paramWebViewRenderProcessClient);
  
  void setWebViewClient(WebViewClient paramWebViewClient);
  
  void setWebChromeClient(WebChromeClient paramWebChromeClient);
  
  void setVerticalScrollbarOverlay(boolean paramBoolean);
  
  default void setTextClassifier(TextClassifier paramTextClassifier) {}
  
  void setRendererPriorityPolicy(int paramInt, boolean paramBoolean);
  
  void setPictureListener(WebView.PictureListener paramPictureListener);
  
  void setNetworkAvailable(boolean paramBoolean);
  
  void setMapTrackballToArrowKeys(boolean paramBoolean);
  
  void setInitialScale(int paramInt);
  
  void setHttpAuthUsernamePassword(String paramString1, String paramString2, String paramString3, String paramString4);
  
  void setHorizontalScrollbarOverlay(boolean paramBoolean);
  
  void setFindListener(WebView.FindListener paramFindListener);
  
  void setDownloadListener(DownloadListener paramDownloadListener);
  
  void setCertificate(SslCertificate paramSslCertificate);
  
  void saveWebArchive(String paramString, boolean paramBoolean, ValueCallback<String> paramValueCallback);
  
  void saveWebArchive(String paramString);
  
  WebBackForwardList saveState(Bundle paramBundle);
  
  boolean savePicture(Bundle paramBundle, File paramFile);
  
  void savePassword(String paramString1, String paramString2, String paramString3);
  
  void resumeTimers();
  
  WebBackForwardList restoreState(Bundle paramBundle);
  
  boolean restorePicture(Bundle paramBundle, File paramFile);
  
  void requestImageRef(Message paramMessage);
  
  void requestFocusNodeHref(Message paramMessage);
  
  void removeJavascriptInterface(String paramString);
  
  void reload();
  
  void postUrl(String paramString, byte[] paramArrayOfbyte);
  
  void postMessageToMainFrame(WebMessage paramWebMessage, Uri paramUri);
  
  void pauseTimers();
  
  boolean pageUp(boolean paramBoolean);
  
  boolean pageDown(boolean paramBoolean);
  
  boolean overlayVerticalScrollbar();
  
  boolean overlayHorizontalScrollbar();
  
  void onResume();
  
  void onPause();
  
  void notifyFindDialogDismissed();
  
  void loadUrl(String paramString, Map<String, String> paramMap);
  
  void loadUrl(String paramString);
  
  void loadDataWithBaseURL(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5);
  
  void loadData(String paramString1, String paramString2, String paramString3);
  
  boolean isPrivateBrowsingEnabled();
  
  boolean isPaused();
  
  void invokeZoomPicker();
  
  void insertVisualStateCallback(long paramLong, WebView.VisualStateCallback paramVisualStateCallback);
  
  void init(Map<String, Object> paramMap, boolean paramBoolean);
  
  void goForward();
  
  void goBackOrForward(int paramInt);
  
  void goBack();
  
  View getZoomControls();
  
  WebViewRenderProcessClient getWebViewRenderProcessClient();
  
  WebViewRenderProcess getWebViewRenderProcess();
  
  WebViewClient getWebViewClient();
  
  WebChromeClient getWebChromeClient();
  
  int getVisibleTitleHeight();
  
  ViewDelegate getViewDelegate();
  
  String getUrl();
  
  String getTouchIconUrl();
  
  String getTitle();
  
  default TextClassifier getTextClassifier() {
    return TextClassifier.NO_OP;
  }
  
  WebSettings getSettings();
  
  ScrollDelegate getScrollDelegate();
  
  float getScale();
  
  int getRendererRequestedPriority();
  
  boolean getRendererPriorityWaivedWhenNotVisible();
  
  int getProgress();
  
  String getOriginalUrl();
  
  String[] getHttpAuthUsernamePassword(String paramString1, String paramString2);
  
  WebView.HitTestResult getHitTestResult();
  
  Bitmap getFavicon();
  
  int getContentWidth();
  
  int getContentHeight();
  
  SslCertificate getCertificate();
  
  void freeMemory();
  
  void flingScroll(int paramInt1, int paramInt2);
  
  void findNext(boolean paramBoolean);
  
  View findHierarchyView(String paramString, int paramInt);
  
  void findAllAsync(String paramString);
  
  int findAll(String paramString);
  
  void evaluateJavaScript(String paramString, ValueCallback<String> paramValueCallback);
  
  void dumpViewHierarchyWithProperties(BufferedWriter paramBufferedWriter, int paramInt);
  
  void documentHasImages(Message paramMessage);
  
  void destroy();
  
  WebMessagePort[] createWebMessageChannel();
  
  PrintDocumentAdapter createPrintDocumentAdapter(String paramString);
  
  WebBackForwardList copyBackForwardList();
  
  void clearView();
  
  void clearSslPreferences();
  
  void clearMatches();
  
  void clearHistory();
  
  void clearFormData();
  
  void clearCache(boolean paramBoolean);
  
  Picture capturePicture();
  
  boolean canZoomOut();
  
  boolean canZoomIn();
  
  boolean canGoForward();
  
  boolean canGoBackOrForward(int paramInt);
  
  boolean canGoBack();
  
  void addJavascriptInterface(Object paramObject, String paramString);
  
  public static interface ScrollDelegate {
    int computeHorizontalScrollOffset();
    
    int computeHorizontalScrollRange();
    
    void computeScroll();
    
    int computeVerticalScrollExtent();
    
    int computeVerticalScrollOffset();
    
    int computeVerticalScrollRange();
  }
  
  public static interface ViewDelegate {
    default void onProvideAutofillVirtualStructure(ViewStructure param1ViewStructure, int param1Int) {}
    
    default void autofill(SparseArray<AutofillValue> param1SparseArray) {}
    
    default boolean isVisibleToUserForAutofill(int param1Int) {
      return true;
    }
    
    default void onProvideContentCaptureStructure(ViewStructure param1ViewStructure, int param1Int) {}
    
    default void onMovedToDisplay(int param1Int, Configuration param1Configuration) {}
    
    default boolean onCheckIsTextEditor() {
      return false;
    }
    
    boolean dispatchKeyEvent(KeyEvent param1KeyEvent);
    
    View findFocus(View param1View);
    
    AccessibilityNodeProvider getAccessibilityNodeProvider();
    
    Handler getHandler(Handler param1Handler);
    
    void onActivityResult(int param1Int1, int param1Int2, Intent param1Intent);
    
    void onAttachedToWindow();
    
    void onConfigurationChanged(Configuration param1Configuration);
    
    InputConnection onCreateInputConnection(EditorInfo param1EditorInfo);
    
    void onDetachedFromWindow();
    
    boolean onDragEvent(DragEvent param1DragEvent);
    
    void onDraw(Canvas param1Canvas);
    
    void onDrawVerticalScrollBar(Canvas param1Canvas, Drawable param1Drawable, int param1Int1, int param1Int2, int param1Int3, int param1Int4);
    
    void onFinishTemporaryDetach();
    
    void onFocusChanged(boolean param1Boolean, int param1Int, Rect param1Rect);
    
    boolean onGenericMotionEvent(MotionEvent param1MotionEvent);
    
    boolean onHoverEvent(MotionEvent param1MotionEvent);
    
    void onInitializeAccessibilityEvent(AccessibilityEvent param1AccessibilityEvent);
    
    void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo param1AccessibilityNodeInfo);
    
    boolean onKeyDown(int param1Int, KeyEvent param1KeyEvent);
    
    boolean onKeyMultiple(int param1Int1, int param1Int2, KeyEvent param1KeyEvent);
    
    boolean onKeyUp(int param1Int, KeyEvent param1KeyEvent);
    
    void onMeasure(int param1Int1, int param1Int2);
    
    void onOverScrolled(int param1Int1, int param1Int2, boolean param1Boolean1, boolean param1Boolean2);
    
    void onProvideVirtualStructure(ViewStructure param1ViewStructure);
    
    void onScrollChanged(int param1Int1, int param1Int2, int param1Int3, int param1Int4);
    
    void onSizeChanged(int param1Int1, int param1Int2, int param1Int3, int param1Int4);
    
    void onStartTemporaryDetach();
    
    boolean onTouchEvent(MotionEvent param1MotionEvent);
    
    boolean onTrackballEvent(MotionEvent param1MotionEvent);
    
    void onVisibilityChanged(View param1View, int param1Int);
    
    void onWindowFocusChanged(boolean param1Boolean);
    
    void onWindowVisibilityChanged(int param1Int);
    
    boolean performAccessibilityAction(int param1Int, Bundle param1Bundle);
    
    boolean performLongClick();
    
    void preDispatchDraw(Canvas param1Canvas);
    
    boolean requestChildRectangleOnScreen(View param1View, Rect param1Rect, boolean param1Boolean);
    
    boolean requestFocus(int param1Int, Rect param1Rect);
    
    void setBackgroundColor(int param1Int);
    
    boolean setFrame(int param1Int1, int param1Int2, int param1Int3, int param1Int4);
    
    void setLayerType(int param1Int, Paint param1Paint);
    
    void setLayoutParams(ViewGroup.LayoutParams param1LayoutParams);
    
    void setOverScrollMode(int param1Int);
    
    void setScrollBarStyle(int param1Int);
    
    boolean shouldDelayChildPressedState();
  }
}
