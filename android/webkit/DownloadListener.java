package android.webkit;

public interface DownloadListener {
  void onDownloadStart(String paramString1, String paramString2, String paramString3, String paramString4, long paramLong);
}
