package android.webkit;

import android.annotation.SystemApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Rect;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

@SystemApi
public class FindActionModeCallback implements ActionMode.Callback, TextWatcher, View.OnClickListener, WebView.FindListener {
  private WebView mWebView;
  
  private Resources mResources;
  
  private int mNumberOfMatches;
  
  private boolean mMatchesFound;
  
  private TextView mMatches;
  
  private InputMethodManager mInput;
  
  public FindActionModeCallback(Context paramContext) {
    View view = LayoutInflater.from(paramContext).inflate(17367351, (ViewGroup)null);
    this.mEditText = (EditText)(view = view.<EditText>findViewById(16908291));
    view.setCustomSelectionActionModeCallback(new NoAction());
    this.mEditText.setOnClickListener(this);
    setText("");
    this.mMatches = this.mCustomView.<TextView>findViewById(16909146);
    this.mInput = (InputMethodManager)paramContext.getSystemService(InputMethodManager.class);
    this.mResources = paramContext.getResources();
  }
  
  public void finish() {
    this.mActionMode.finish();
  }
  
  public void setText(String paramString) {
    this.mEditText.setText(paramString);
    Editable editable = this.mEditText.getText();
    int i = editable.length();
    Selection.setSelection(editable, i, i);
    editable.setSpan(this, 0, i, 18);
    this.mMatchesFound = false;
  }
  
  public void setWebView(WebView paramWebView) {
    if (paramWebView != null) {
      this.mWebView = paramWebView;
      paramWebView.setFindDialogFindListener(this);
      return;
    } 
    throw new AssertionError("WebView supplied to FindActionModeCallback cannot be null");
  }
  
  public void onFindResultReceived(int paramInt1, int paramInt2, boolean paramBoolean) {
    if (paramBoolean) {
      if (paramInt2 == 0) {
        paramBoolean = true;
      } else {
        paramBoolean = false;
      } 
      updateMatchCount(paramInt1, paramInt2, paramBoolean);
    } 
  }
  
  private void findNext(boolean paramBoolean) {
    WebView webView = this.mWebView;
    if (webView != null) {
      if (!this.mMatchesFound) {
        findAll();
        return;
      } 
      if (this.mNumberOfMatches == 0)
        return; 
      webView.findNext(paramBoolean);
      updateMatchesString();
      return;
    } 
    throw new AssertionError("No WebView for FindActionModeCallback::findNext");
  }
  
  public void findAll() {
    if (this.mWebView != null) {
      Editable editable = this.mEditText.getText();
      if (editable.length() == 0) {
        this.mWebView.clearMatches();
        this.mMatches.setVisibility(8);
        this.mMatchesFound = false;
        this.mWebView.findAll((String)null);
      } else {
        this.mMatchesFound = true;
        this.mMatches.setVisibility(4);
        this.mNumberOfMatches = 0;
        this.mWebView.findAllAsync(editable.toString());
      } 
      return;
    } 
    throw new AssertionError("No WebView for FindActionModeCallback::findAll");
  }
  
  public void showSoftInput() {
    if (this.mEditText.requestFocus())
      this.mInput.showSoftInput(this.mEditText, 0); 
  }
  
  public void updateMatchCount(int paramInt1, int paramInt2, boolean paramBoolean) {
    if (!paramBoolean) {
      this.mNumberOfMatches = paramInt2;
      this.mActiveMatchIndex = paramInt1;
      updateMatchesString();
    } else {
      this.mMatches.setVisibility(8);
      this.mNumberOfMatches = 0;
    } 
  }
  
  private void updateMatchesString() {
    int i = this.mNumberOfMatches;
    if (i == 0) {
      this.mMatches.setText(17040696);
    } else {
      TextView textView = this.mMatches;
      Resources resources = this.mResources;
      int j = this.mActiveMatchIndex;
      int k = this.mNumberOfMatches;
      textView.setText(resources.getQuantityString(18153493, i, new Object[] { Integer.valueOf(j + 1), Integer.valueOf(k) }));
    } 
    this.mMatches.setVisibility(0);
  }
  
  public void onClick(View paramView) {
    findNext(true);
  }
  
  public boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu) {
    if (!paramActionMode.isUiFocusable())
      return false; 
    paramActionMode.setCustomView(this.mCustomView);
    paramActionMode.getMenuInflater().inflate(18087938, paramMenu);
    this.mActionMode = paramActionMode;
    Editable editable = this.mEditText.getText();
    Selection.setSelection(editable, editable.length());
    this.mMatches.setVisibility(8);
    this.mMatchesFound = false;
    this.mMatches.setText("0");
    this.mEditText.requestFocus();
    return true;
  }
  
  public void onDestroyActionMode(ActionMode paramActionMode) {
    this.mActionMode = null;
    this.mWebView.notifyFindDialogDismissed();
    this.mWebView.setFindDialogFindListener((WebView.FindListener)null);
    this.mInput.hideSoftInputFromWindow(this.mWebView.getWindowToken(), 0);
  }
  
  public boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu) {
    return false;
  }
  
  public boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem) {
    WebView webView = this.mWebView;
    if (webView != null) {
      this.mInput.hideSoftInputFromWindow(webView.getWindowToken(), 0);
      switch (paramMenuItem.getItemId()) {
        default:
          return false;
        case 16908974:
          findNext(false);
          return true;
        case 16908973:
          break;
      } 
      findNext(true);
      return true;
    } 
    throw new AssertionError("No WebView for FindActionModeCallback::onActionItemClicked");
  }
  
  public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
    findAll();
  }
  
  public void afterTextChanged(Editable paramEditable) {}
  
  private Rect mGlobalVisibleRect = new Rect();
  
  private Point mGlobalVisibleOffset = new Point();
  
  private EditText mEditText;
  
  private View mCustomView;
  
  private int mActiveMatchIndex;
  
  private ActionMode mActionMode;
  
  public int getActionModeGlobalBottom() {
    if (this.mActionMode == null)
      return 0; 
    View view1 = (View)this.mCustomView.getParent();
    View view2 = view1;
    if (view1 == null)
      view2 = this.mCustomView; 
    view2.getGlobalVisibleRect(this.mGlobalVisibleRect, this.mGlobalVisibleOffset);
    return this.mGlobalVisibleRect.bottom;
  }
  
  class NoAction implements ActionMode.Callback {
    public boolean onCreateActionMode(ActionMode param1ActionMode, Menu param1Menu) {
      return false;
    }
    
    public boolean onPrepareActionMode(ActionMode param1ActionMode, Menu param1Menu) {
      return false;
    }
    
    public boolean onActionItemClicked(ActionMode param1ActionMode, MenuItem param1MenuItem) {
      return false;
    }
    
    public void onDestroyActionMode(ActionMode param1ActionMode) {}
  }
}
