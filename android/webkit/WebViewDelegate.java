package android.webkit;

import android.annotation.SystemApi;
import android.app.ActivityThread;
import android.app.Application;
import android.app.ResourcesManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.RecordingCanvas;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.os.Trace;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewRootImpl;
import com.android.internal.util.ArrayUtils;

@SystemApi
public final class WebViewDelegate {
  public void setOnTraceEnabledChangeListener(final OnTraceEnabledChangeListener listener) {
    SystemProperties.addChangeCallback(new Runnable() {
          final WebViewDelegate this$0;
          
          final WebViewDelegate.OnTraceEnabledChangeListener val$listener;
          
          public void run() {
            listener.onTraceEnabledChange(WebViewDelegate.this.isTraceTagEnabled());
          }
        });
  }
  
  public boolean isTraceTagEnabled() {
    return Trace.isTagEnabled(16L);
  }
  
  @Deprecated
  public boolean canInvokeDrawGlFunctor(View paramView) {
    return true;
  }
  
  @Deprecated
  public void invokeDrawGlFunctor(View paramView, long paramLong, boolean paramBoolean) {
    ViewRootImpl.invokeFunctor(paramLong, paramBoolean);
  }
  
  @Deprecated
  public void callDrawGlFunction(Canvas paramCanvas, long paramLong) {
    if (paramCanvas instanceof RecordingCanvas) {
      ((RecordingCanvas)paramCanvas).drawGLFunctor2(paramLong, null);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramCanvas.getClass().getName());
    stringBuilder.append(" is not a DisplayList canvas");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  @Deprecated
  public void callDrawGlFunction(Canvas paramCanvas, long paramLong, Runnable paramRunnable) {
    if (paramCanvas instanceof RecordingCanvas) {
      ((RecordingCanvas)paramCanvas).drawGLFunctor2(paramLong, paramRunnable);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramCanvas.getClass().getName());
    stringBuilder.append(" is not a DisplayList canvas");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void drawWebViewFunctor(Canvas paramCanvas, int paramInt) {
    if (paramCanvas instanceof RecordingCanvas) {
      ((RecordingCanvas)paramCanvas).drawWebViewFunctor(paramInt);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramCanvas.getClass().getName());
    stringBuilder.append(" is not a RecordingCanvas canvas");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  @Deprecated
  public void detachDrawGlFunctor(View paramView, long paramLong) {
    ViewRootImpl viewRootImpl = paramView.getViewRootImpl();
    if (paramLong != 0L && viewRootImpl != null)
      viewRootImpl.detachFunctor(paramLong); 
  }
  
  public int getPackageId(Resources paramResources, String paramString) {
    SparseArray<String> sparseArray = paramResources.getAssets().getAssignedPackageIdentifiers();
    for (byte b = 0; b < sparseArray.size(); b++) {
      String str = sparseArray.valueAt(b);
      if (paramString.equals(str))
        return sparseArray.keyAt(b); 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Package not found: ");
    stringBuilder.append(paramString);
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public Application getApplication() {
    return ActivityThread.currentApplication();
  }
  
  public String getErrorString(Context paramContext, int paramInt) {
    return LegacyErrorStrings.getString(paramInt, paramContext);
  }
  
  public void addWebViewAssetPath(Context paramContext) {
    String[] arrayOfString2 = (WebViewFactory.getLoadedPackageInfo()).applicationInfo.getAllApkPaths();
    ApplicationInfo applicationInfo = paramContext.getApplicationInfo();
    String[] arrayOfString1 = applicationInfo.sharedLibraryFiles;
    int i;
    byte b;
    for (i = arrayOfString2.length, b = 0; b < i; ) {
      String str = arrayOfString2[b];
      arrayOfString1 = (String[])ArrayUtils.appendElement(String.class, (Object[])arrayOfString1, str);
      b++;
    } 
    if (arrayOfString1 != applicationInfo.sharedLibraryFiles) {
      applicationInfo.sharedLibraryFiles = arrayOfString1;
      ResourcesManager resourcesManager = ResourcesManager.getInstance();
      String str1 = applicationInfo.packageName;
      String str2 = applicationInfo.getBaseResourcePath();
      resourcesManager.appendLibAssetsForMainAssetPath(str1, str2, arrayOfString2);
    } 
  }
  
  public boolean isMultiProcessEnabled() {
    try {
      return WebViewFactory.getUpdateService().isMultiProcessEnabled();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String getDataDirectorySuffix() {
    return WebViewFactory.getDataDirectorySuffix();
  }
  
  public static interface OnTraceEnabledChangeListener {
    void onTraceEnabledChange(boolean param1Boolean);
  }
}
