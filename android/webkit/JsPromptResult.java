package android.webkit;

import android.annotation.SystemApi;

public class JsPromptResult extends JsResult {
  private String mStringResult;
  
  public void confirm(String paramString) {
    this.mStringResult = paramString;
    confirm();
  }
  
  @SystemApi
  public JsPromptResult(JsResult.ResultReceiver paramResultReceiver) {
    super(paramResultReceiver);
  }
  
  @SystemApi
  public String getStringResult() {
    return this.mStringResult;
  }
}
