package android.webkit;

import android.annotation.SystemApi;
import android.os.RemoteException;

@SystemApi
public final class WebViewUpdateService {
  public static WebViewProviderInfo[] getAllWebViewPackages() {
    IWebViewUpdateService iWebViewUpdateService = getUpdateService();
    if (iWebViewUpdateService == null)
      return new WebViewProviderInfo[0]; 
    try {
      return iWebViewUpdateService.getAllWebViewPackages();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static WebViewProviderInfo[] getValidWebViewPackages() {
    IWebViewUpdateService iWebViewUpdateService = getUpdateService();
    if (iWebViewUpdateService == null)
      return new WebViewProviderInfo[0]; 
    try {
      return iWebViewUpdateService.getValidWebViewPackages();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static String getCurrentWebViewPackageName() {
    IWebViewUpdateService iWebViewUpdateService = getUpdateService();
    if (iWebViewUpdateService == null)
      return null; 
    try {
      return iWebViewUpdateService.getCurrentWebViewPackageName();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private static IWebViewUpdateService getUpdateService() {
    return WebViewFactory.getUpdateService();
  }
}
