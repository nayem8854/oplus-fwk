package android.webkit;

import android.annotation.SystemApi;
import android.content.Context;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public abstract class WebSettings {
  public static final int FORCE_DARK_AUTO = 1;
  
  public static final int FORCE_DARK_OFF = 0;
  
  public static final int FORCE_DARK_ON = 2;
  
  public static final int LOAD_CACHE_ELSE_NETWORK = 1;
  
  public static final int LOAD_CACHE_ONLY = 3;
  
  public static final int LOAD_DEFAULT = -1;
  
  @Deprecated
  public static final int LOAD_NORMAL = 0;
  
  public static final int LOAD_NO_CACHE = 2;
  
  public static final int MENU_ITEM_NONE = 0;
  
  public static final int MENU_ITEM_PROCESS_TEXT = 4;
  
  public static final int MENU_ITEM_SHARE = 1;
  
  public static final int MENU_ITEM_WEB_SEARCH = 2;
  
  public static final int MIXED_CONTENT_ALWAYS_ALLOW = 0;
  
  public static final int MIXED_CONTENT_COMPATIBILITY_MODE = 2;
  
  public static final int MIXED_CONTENT_NEVER_ALLOW = 1;
  
  public enum LayoutAlgorithm {
    NARROW_COLUMNS, NORMAL, SINGLE_COLUMN, TEXT_AUTOSIZING;
    
    private static final LayoutAlgorithm[] $VALUES;
    
    static {
      LayoutAlgorithm layoutAlgorithm = new LayoutAlgorithm("TEXT_AUTOSIZING", 3);
      $VALUES = new LayoutAlgorithm[] { NORMAL, SINGLE_COLUMN, NARROW_COLUMNS, layoutAlgorithm };
    }
  }
  
  @Deprecated
  public enum TextSize {
    LARGER,
    LARGEST,
    NORMAL,
    SMALLER,
    SMALLEST(50);
    
    private static final TextSize[] $VALUES;
    
    int value;
    
    static {
      NORMAL = new TextSize("NORMAL", 2, 100);
      LARGER = new TextSize("LARGER", 3, 150);
      TextSize textSize = new TextSize("LARGEST", 4, 200);
      $VALUES = new TextSize[] { SMALLEST, SMALLER, NORMAL, LARGER, textSize };
    }
    
    TextSize(int param1Int1) {
      this.value = param1Int1;
    }
  }
  
  public enum ZoomDensity {
    CLOSE(50),
    FAR(150),
    MEDIUM(100);
    
    private static final ZoomDensity[] $VALUES;
    
    int value;
    
    static {
      ZoomDensity zoomDensity = new ZoomDensity("CLOSE", 2, 75);
      $VALUES = new ZoomDensity[] { FAR, MEDIUM, zoomDensity };
    }
    
    ZoomDensity(int param1Int1) {
      this.value = param1Int1;
    }
    
    public int getValue() {
      return this.value;
    }
  }
  
  public enum RenderPriority {
    NORMAL, HIGH, LOW;
    
    private static final RenderPriority[] $VALUES;
    
    static {
      RenderPriority renderPriority = new RenderPriority("LOW", 2);
      $VALUES = new RenderPriority[] { NORMAL, HIGH, renderPriority };
    }
  }
  
  public enum PluginState {
    ON, OFF, ON_DEMAND;
    
    private static final PluginState[] $VALUES;
    
    static {
      PluginState pluginState = new PluginState("OFF", 2);
      $VALUES = new PluginState[] { ON, ON_DEMAND, pluginState };
    }
  }
  
  @Deprecated
  public void setTextSize(TextSize paramTextSize) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: getfield value : I
    //   7: invokevirtual setTextZoom : (I)V
    //   10: aload_0
    //   11: monitorexit
    //   12: return
    //   13: astore_1
    //   14: aload_0
    //   15: monitorexit
    //   16: aload_1
    //   17: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #556	-> 2
    //   #557	-> 10
    //   #555	-> 13
    // Exception table:
    //   from	to	target	type
    //   2	10	13	finally
  }
  
  @Deprecated
  public TextSize getTextSize() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aconst_null
    //   3: astore_1
    //   4: ldc 2147483647
    //   6: istore_2
    //   7: aload_0
    //   8: invokevirtual getTextZoom : ()I
    //   11: istore_3
    //   12: invokestatic values : ()[Landroid/webkit/WebSettings$TextSize;
    //   15: astore #4
    //   17: aload #4
    //   19: arraylength
    //   20: istore #5
    //   22: iconst_0
    //   23: istore #6
    //   25: iload #6
    //   27: iload #5
    //   29: if_icmpge -> 86
    //   32: aload #4
    //   34: iload #6
    //   36: aaload
    //   37: astore #7
    //   39: iload_3
    //   40: aload #7
    //   42: getfield value : I
    //   45: isub
    //   46: invokestatic abs : (I)I
    //   49: istore #8
    //   51: iload #8
    //   53: ifne -> 61
    //   56: aload_0
    //   57: monitorexit
    //   58: aload #7
    //   60: areturn
    //   61: iload_2
    //   62: istore #9
    //   64: iload #8
    //   66: iload_2
    //   67: if_icmpge -> 77
    //   70: iload #8
    //   72: istore #9
    //   74: aload #7
    //   76: astore_1
    //   77: iinc #6, 1
    //   80: iload #9
    //   82: istore_2
    //   83: goto -> 25
    //   86: aload_1
    //   87: ifnull -> 93
    //   90: goto -> 97
    //   93: getstatic android/webkit/WebSettings$TextSize.NORMAL : Landroid/webkit/WebSettings$TextSize;
    //   96: astore_1
    //   97: aload_0
    //   98: monitorexit
    //   99: aload_1
    //   100: areturn
    //   101: astore_1
    //   102: aload_0
    //   103: monitorexit
    //   104: aload_1
    //   105: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #570	-> 2
    //   #571	-> 4
    //   #572	-> 7
    //   #573	-> 12
    //   #574	-> 39
    //   #575	-> 51
    //   #576	-> 56
    //   #578	-> 61
    //   #579	-> 70
    //   #580	-> 74
    //   #573	-> 77
    //   #583	-> 86
    //   #569	-> 101
    // Exception table:
    //   from	to	target	type
    //   7	12	101	finally
    //   12	22	101	finally
    //   39	51	101	finally
    //   93	97	101	finally
  }
  
  @Deprecated
  public void setUseDoubleTree(boolean paramBoolean) {}
  
  @Deprecated
  public boolean getUseDoubleTree() {
    return false;
  }
  
  @Deprecated
  public void setPluginsPath(String paramString) {}
  
  @Deprecated
  public String getPluginsPath() {
    return "";
  }
  
  public static String getDefaultUserAgent(Context paramContext) {
    return WebViewFactory.getProvider().getStatics().getDefaultUserAgent(paramContext);
  }
  
  public void setForceDark(int paramInt) {}
  
  public int getForceDark() {
    return 1;
  }
  
  @Deprecated
  public abstract boolean enableSmoothTransition();
  
  @SystemApi
  public abstract boolean getAcceptThirdPartyCookies();
  
  public abstract boolean getAllowContentAccess();
  
  public abstract boolean getAllowFileAccess();
  
  public abstract boolean getAllowFileAccessFromFileURLs();
  
  public abstract boolean getAllowUniversalAccessFromFileURLs();
  
  public abstract boolean getBlockNetworkImage();
  
  public abstract boolean getBlockNetworkLoads();
  
  public abstract boolean getBuiltInZoomControls();
  
  public abstract int getCacheMode();
  
  public abstract String getCursiveFontFamily();
  
  public abstract boolean getDatabaseEnabled();
  
  @Deprecated
  public abstract String getDatabasePath();
  
  public abstract int getDefaultFixedFontSize();
  
  public abstract int getDefaultFontSize();
  
  public abstract String getDefaultTextEncodingName();
  
  @Deprecated
  public abstract ZoomDensity getDefaultZoom();
  
  public abstract int getDisabledActionModeMenuItems();
  
  public abstract boolean getDisplayZoomControls();
  
  public abstract boolean getDomStorageEnabled();
  
  public abstract String getFantasyFontFamily();
  
  public abstract String getFixedFontFamily();
  
  public abstract boolean getJavaScriptCanOpenWindowsAutomatically();
  
  public abstract boolean getJavaScriptEnabled();
  
  public abstract LayoutAlgorithm getLayoutAlgorithm();
  
  @Deprecated
  public abstract boolean getLightTouchEnabled();
  
  public abstract boolean getLoadWithOverviewMode();
  
  public abstract boolean getLoadsImagesAutomatically();
  
  public abstract boolean getMediaPlaybackRequiresUserGesture();
  
  public abstract int getMinimumFontSize();
  
  public abstract int getMinimumLogicalFontSize();
  
  public abstract int getMixedContentMode();
  
  @SystemApi
  @Deprecated
  public abstract boolean getNavDump();
  
  public abstract boolean getOffscreenPreRaster();
  
  @Deprecated
  public abstract PluginState getPluginState();
  
  @SystemApi
  @Deprecated
  public abstract boolean getPluginsEnabled();
  
  public abstract boolean getSafeBrowsingEnabled();
  
  public abstract String getSansSerifFontFamily();
  
  @Deprecated
  public abstract boolean getSaveFormData();
  
  @Deprecated
  public abstract boolean getSavePassword();
  
  public abstract String getSerifFontFamily();
  
  public abstract String getStandardFontFamily();
  
  public abstract int getTextZoom();
  
  @SystemApi
  @Deprecated
  public abstract boolean getUseWebViewBackgroundForOverscrollBackground();
  
  public abstract boolean getUseWideViewPort();
  
  @SystemApi
  @Deprecated
  public abstract int getUserAgent();
  
  public abstract String getUserAgentString();
  
  @SystemApi
  public abstract boolean getVideoOverlayForEmbeddedEncryptedVideoEnabled();
  
  @SystemApi
  public abstract void setAcceptThirdPartyCookies(boolean paramBoolean);
  
  public abstract void setAllowContentAccess(boolean paramBoolean);
  
  public abstract void setAllowFileAccess(boolean paramBoolean);
  
  @Deprecated
  public abstract void setAllowFileAccessFromFileURLs(boolean paramBoolean);
  
  @Deprecated
  public abstract void setAllowUniversalAccessFromFileURLs(boolean paramBoolean);
  
  public abstract void setAppCacheEnabled(boolean paramBoolean);
  
  @Deprecated
  public abstract void setAppCacheMaxSize(long paramLong);
  
  public abstract void setAppCachePath(String paramString);
  
  public abstract void setBlockNetworkImage(boolean paramBoolean);
  
  public abstract void setBlockNetworkLoads(boolean paramBoolean);
  
  public abstract void setBuiltInZoomControls(boolean paramBoolean);
  
  public abstract void setCacheMode(int paramInt);
  
  public abstract void setCursiveFontFamily(String paramString);
  
  public abstract void setDatabaseEnabled(boolean paramBoolean);
  
  @Deprecated
  public abstract void setDatabasePath(String paramString);
  
  public abstract void setDefaultFixedFontSize(int paramInt);
  
  public abstract void setDefaultFontSize(int paramInt);
  
  public abstract void setDefaultTextEncodingName(String paramString);
  
  @Deprecated
  public abstract void setDefaultZoom(ZoomDensity paramZoomDensity);
  
  public abstract void setDisabledActionModeMenuItems(int paramInt);
  
  public abstract void setDisplayZoomControls(boolean paramBoolean);
  
  public abstract void setDomStorageEnabled(boolean paramBoolean);
  
  @Deprecated
  public abstract void setEnableSmoothTransition(boolean paramBoolean);
  
  public abstract void setFantasyFontFamily(String paramString);
  
  public abstract void setFixedFontFamily(String paramString);
  
  @Deprecated
  public abstract void setGeolocationDatabasePath(String paramString);
  
  public abstract void setGeolocationEnabled(boolean paramBoolean);
  
  public abstract void setJavaScriptCanOpenWindowsAutomatically(boolean paramBoolean);
  
  public abstract void setJavaScriptEnabled(boolean paramBoolean);
  
  public abstract void setLayoutAlgorithm(LayoutAlgorithm paramLayoutAlgorithm);
  
  @Deprecated
  public abstract void setLightTouchEnabled(boolean paramBoolean);
  
  public abstract void setLoadWithOverviewMode(boolean paramBoolean);
  
  public abstract void setLoadsImagesAutomatically(boolean paramBoolean);
  
  public abstract void setMediaPlaybackRequiresUserGesture(boolean paramBoolean);
  
  public abstract void setMinimumFontSize(int paramInt);
  
  public abstract void setMinimumLogicalFontSize(int paramInt);
  
  public abstract void setMixedContentMode(int paramInt);
  
  @SystemApi
  @Deprecated
  public abstract void setNavDump(boolean paramBoolean);
  
  public abstract void setNeedInitialFocus(boolean paramBoolean);
  
  public abstract void setOffscreenPreRaster(boolean paramBoolean);
  
  @Deprecated
  public abstract void setPluginState(PluginState paramPluginState);
  
  @SystemApi
  @Deprecated
  public abstract void setPluginsEnabled(boolean paramBoolean);
  
  @Deprecated
  public abstract void setRenderPriority(RenderPriority paramRenderPriority);
  
  public abstract void setSafeBrowsingEnabled(boolean paramBoolean);
  
  public abstract void setSansSerifFontFamily(String paramString);
  
  @Deprecated
  public abstract void setSaveFormData(boolean paramBoolean);
  
  @Deprecated
  public abstract void setSavePassword(boolean paramBoolean);
  
  public abstract void setSerifFontFamily(String paramString);
  
  public abstract void setStandardFontFamily(String paramString);
  
  public abstract void setSupportMultipleWindows(boolean paramBoolean);
  
  public abstract void setSupportZoom(boolean paramBoolean);
  
  public abstract void setTextZoom(int paramInt);
  
  @SystemApi
  @Deprecated
  public abstract void setUseWebViewBackgroundForOverscrollBackground(boolean paramBoolean);
  
  public abstract void setUseWideViewPort(boolean paramBoolean);
  
  @SystemApi
  @Deprecated
  public abstract void setUserAgent(int paramInt);
  
  public abstract void setUserAgentString(String paramString);
  
  @SystemApi
  public abstract void setVideoOverlayForEmbeddedEncryptedVideoEnabled(boolean paramBoolean);
  
  public abstract boolean supportMultipleWindows();
  
  public abstract boolean supportZoom();
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface CacheMode {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ForceDark {}
  
  @Retention(RetentionPolicy.SOURCE)
  @Target({ElementType.PARAMETER, ElementType.METHOD})
  private static @interface MenuItemFlags {}
}
