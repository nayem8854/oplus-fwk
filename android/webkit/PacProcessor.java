package android.webkit;

import android.annotation.SystemApi;

@SystemApi
public interface PacProcessor {
  static PacProcessor getInstance() {
    return WebViewFactory.getProvider().getPacProcessor();
  }
  
  String findProxyForUrl(String paramString);
  
  boolean setProxyScript(String paramString);
}
