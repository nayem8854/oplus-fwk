package android.webkit;

import java.util.LinkedList;
import java.util.Map;

@Deprecated
public final class UrlInterceptRegistry {
  private static final String LOGTAG = "intercept";
  
  private static boolean mDisabled = false;
  
  private static LinkedList mHandlerList;
  
  private static LinkedList getHandlers() {
    // Byte code:
    //   0: ldc android/webkit/UrlInterceptRegistry
    //   2: monitorenter
    //   3: getstatic android/webkit/UrlInterceptRegistry.mHandlerList : Ljava/util/LinkedList;
    //   6: ifnonnull -> 21
    //   9: new java/util/LinkedList
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic android/webkit/UrlInterceptRegistry.mHandlerList : Ljava/util/LinkedList;
    //   21: getstatic android/webkit/UrlInterceptRegistry.mHandlerList : Ljava/util/LinkedList;
    //   24: astore_0
    //   25: ldc android/webkit/UrlInterceptRegistry
    //   27: monitorexit
    //   28: aload_0
    //   29: areturn
    //   30: astore_0
    //   31: ldc android/webkit/UrlInterceptRegistry
    //   33: monitorexit
    //   34: aload_0
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #42	-> 3
    //   #43	-> 9
    //   #44	-> 21
    //   #41	-> 30
    // Exception table:
    //   from	to	target	type
    //   3	9	30	finally
    //   9	21	30	finally
    //   21	25	30	finally
  }
  
  @Deprecated
  public static void setUrlInterceptDisabled(boolean paramBoolean) {
    // Byte code:
    //   0: ldc android/webkit/UrlInterceptRegistry
    //   2: monitorenter
    //   3: iload_0
    //   4: putstatic android/webkit/UrlInterceptRegistry.mDisabled : Z
    //   7: ldc android/webkit/UrlInterceptRegistry
    //   9: monitorexit
    //   10: return
    //   11: astore_1
    //   12: ldc android/webkit/UrlInterceptRegistry
    //   14: monitorexit
    //   15: aload_1
    //   16: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #59	-> 3
    //   #60	-> 7
    //   #58	-> 11
    // Exception table:
    //   from	to	target	type
    //   3	7	11	finally
  }
  
  @Deprecated
  public static boolean urlInterceptDisabled() {
    // Byte code:
    //   0: ldc android/webkit/UrlInterceptRegistry
    //   2: monitorenter
    //   3: getstatic android/webkit/UrlInterceptRegistry.mDisabled : Z
    //   6: istore_0
    //   7: ldc android/webkit/UrlInterceptRegistry
    //   9: monitorexit
    //   10: iload_0
    //   11: ireturn
    //   12: astore_1
    //   13: ldc android/webkit/UrlInterceptRegistry
    //   15: monitorexit
    //   16: aload_1
    //   17: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #73	-> 3
    //   #73	-> 12
    // Exception table:
    //   from	to	target	type
    //   3	7	12	finally
  }
  
  @Deprecated
  public static boolean registerHandler(UrlInterceptHandler paramUrlInterceptHandler) {
    // Byte code:
    //   0: ldc android/webkit/UrlInterceptRegistry
    //   2: monitorenter
    //   3: invokestatic getHandlers : ()Ljava/util/LinkedList;
    //   6: aload_0
    //   7: invokevirtual contains : (Ljava/lang/Object;)Z
    //   10: ifne -> 25
    //   13: invokestatic getHandlers : ()Ljava/util/LinkedList;
    //   16: aload_0
    //   17: invokevirtual addFirst : (Ljava/lang/Object;)V
    //   20: ldc android/webkit/UrlInterceptRegistry
    //   22: monitorexit
    //   23: iconst_1
    //   24: ireturn
    //   25: ldc android/webkit/UrlInterceptRegistry
    //   27: monitorexit
    //   28: iconst_0
    //   29: ireturn
    //   30: astore_0
    //   31: ldc android/webkit/UrlInterceptRegistry
    //   33: monitorexit
    //   34: aload_0
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #91	-> 3
    //   #92	-> 13
    //   #93	-> 20
    //   #95	-> 25
    //   #90	-> 30
    // Exception table:
    //   from	to	target	type
    //   3	13	30	finally
    //   13	20	30	finally
  }
  
  @Deprecated
  public static boolean unregisterHandler(UrlInterceptHandler paramUrlInterceptHandler) {
    // Byte code:
    //   0: ldc android/webkit/UrlInterceptRegistry
    //   2: monitorenter
    //   3: invokestatic getHandlers : ()Ljava/util/LinkedList;
    //   6: aload_0
    //   7: invokevirtual remove : (Ljava/lang/Object;)Z
    //   10: istore_1
    //   11: ldc android/webkit/UrlInterceptRegistry
    //   13: monitorexit
    //   14: iload_1
    //   15: ireturn
    //   16: astore_0
    //   17: ldc android/webkit/UrlInterceptRegistry
    //   19: monitorexit
    //   20: aload_0
    //   21: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #113	-> 3
    //   #113	-> 16
    // Exception table:
    //   from	to	target	type
    //   3	11	16	finally
  }
  
  @Deprecated
  public static CacheManager.CacheResult getSurrogate(String paramString, Map<String, String> paramMap) {
    // Byte code:
    //   0: ldc android/webkit/UrlInterceptRegistry
    //   2: monitorenter
    //   3: invokestatic urlInterceptDisabled : ()Z
    //   6: istore_2
    //   7: iload_2
    //   8: ifeq -> 16
    //   11: ldc android/webkit/UrlInterceptRegistry
    //   13: monitorexit
    //   14: aconst_null
    //   15: areturn
    //   16: invokestatic getHandlers : ()Ljava/util/LinkedList;
    //   19: invokevirtual listIterator : ()Ljava/util/ListIterator;
    //   22: astore_3
    //   23: aload_3
    //   24: invokeinterface hasNext : ()Z
    //   29: ifeq -> 68
    //   32: aload_3
    //   33: invokeinterface next : ()Ljava/lang/Object;
    //   38: checkcast android/webkit/UrlInterceptHandler
    //   41: astore #4
    //   43: aload #4
    //   45: aload_0
    //   46: aload_1
    //   47: invokeinterface service : (Ljava/lang/String;Ljava/util/Map;)Landroid/webkit/CacheManager$CacheResult;
    //   52: astore #4
    //   54: aload #4
    //   56: ifnull -> 65
    //   59: ldc android/webkit/UrlInterceptRegistry
    //   61: monitorexit
    //   62: aload #4
    //   64: areturn
    //   65: goto -> 23
    //   68: ldc android/webkit/UrlInterceptRegistry
    //   70: monitorexit
    //   71: aconst_null
    //   72: areturn
    //   73: astore_0
    //   74: ldc android/webkit/UrlInterceptRegistry
    //   76: monitorexit
    //   77: aload_0
    //   78: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #130	-> 3
    //   #131	-> 11
    //   #133	-> 16
    //   #134	-> 23
    //   #135	-> 32
    //   #136	-> 43
    //   #137	-> 54
    //   #138	-> 59
    //   #140	-> 65
    //   #141	-> 68
    //   #129	-> 73
    // Exception table:
    //   from	to	target	type
    //   3	7	73	finally
    //   16	23	73	finally
    //   23	32	73	finally
    //   32	43	73	finally
    //   43	54	73	finally
  }
  
  @Deprecated
  public static PluginData getPluginData(String paramString, Map<String, String> paramMap) {
    // Byte code:
    //   0: ldc android/webkit/UrlInterceptRegistry
    //   2: monitorenter
    //   3: invokestatic urlInterceptDisabled : ()Z
    //   6: istore_2
    //   7: iload_2
    //   8: ifeq -> 16
    //   11: ldc android/webkit/UrlInterceptRegistry
    //   13: monitorexit
    //   14: aconst_null
    //   15: areturn
    //   16: invokestatic getHandlers : ()Ljava/util/LinkedList;
    //   19: invokevirtual listIterator : ()Ljava/util/ListIterator;
    //   22: astore_3
    //   23: aload_3
    //   24: invokeinterface hasNext : ()Z
    //   29: ifeq -> 68
    //   32: aload_3
    //   33: invokeinterface next : ()Ljava/lang/Object;
    //   38: checkcast android/webkit/UrlInterceptHandler
    //   41: astore #4
    //   43: aload #4
    //   45: aload_0
    //   46: aload_1
    //   47: invokeinterface getPluginData : (Ljava/lang/String;Ljava/util/Map;)Landroid/webkit/PluginData;
    //   52: astore #4
    //   54: aload #4
    //   56: ifnull -> 65
    //   59: ldc android/webkit/UrlInterceptRegistry
    //   61: monitorexit
    //   62: aload #4
    //   64: areturn
    //   65: goto -> 23
    //   68: ldc android/webkit/UrlInterceptRegistry
    //   70: monitorexit
    //   71: aconst_null
    //   72: areturn
    //   73: astore_0
    //   74: ldc android/webkit/UrlInterceptRegistry
    //   76: monitorexit
    //   77: aload_0
    //   78: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #160	-> 3
    //   #161	-> 11
    //   #163	-> 16
    //   #164	-> 23
    //   #165	-> 32
    //   #166	-> 43
    //   #167	-> 54
    //   #168	-> 59
    //   #170	-> 65
    //   #171	-> 68
    //   #159	-> 73
    // Exception table:
    //   from	to	target	type
    //   3	7	73	finally
    //   16	23	73	finally
    //   23	32	73	finally
    //   32	43	73	finally
    //   43	54	73	finally
  }
}
