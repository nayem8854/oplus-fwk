package android.webkit;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

@Deprecated
public final class CacheManager {
  static final boolean $assertionsDisabled = false;
  
  @Deprecated
  public static class CacheResult {
    long contentLength;
    
    String contentdisposition;
    
    String crossDomain;
    
    String encoding;
    
    String etag;
    
    long expires;
    
    String expiresString;
    
    int httpStatusCode;
    
    InputStream inStream;
    
    String lastModified;
    
    String localPath;
    
    String location;
    
    String mimeType;
    
    File outFile;
    
    OutputStream outStream;
    
    public int getHttpStatusCode() {
      return this.httpStatusCode;
    }
    
    public long getContentLength() {
      return this.contentLength;
    }
    
    public String getLocalPath() {
      return this.localPath;
    }
    
    public long getExpires() {
      return this.expires;
    }
    
    public String getExpiresString() {
      return this.expiresString;
    }
    
    public String getLastModified() {
      return this.lastModified;
    }
    
    public String getETag() {
      return this.etag;
    }
    
    public String getMimeType() {
      return this.mimeType;
    }
    
    public String getLocation() {
      return this.location;
    }
    
    public String getEncoding() {
      return this.encoding;
    }
    
    public String getContentDisposition() {
      return this.contentdisposition;
    }
    
    public InputStream getInputStream() {
      return this.inStream;
    }
    
    public OutputStream getOutputStream() {
      return this.outStream;
    }
    
    public void setInputStream(InputStream param1InputStream) {
      this.inStream = param1InputStream;
    }
    
    public void setEncoding(String param1String) {
      this.encoding = param1String;
    }
    
    public void setContentLength(long param1Long) {
      this.contentLength = param1Long;
    }
  }
  
  @Deprecated
  public static File getCacheFileBaseDir() {
    return null;
  }
  
  @Deprecated
  public static boolean cacheDisabled() {
    return false;
  }
  
  @Deprecated
  public static boolean startCacheTransaction() {
    return false;
  }
  
  @Deprecated
  public static boolean endCacheTransaction() {
    return false;
  }
  
  @Deprecated
  public static CacheResult getCacheFile(String paramString, Map<String, String> paramMap) {
    return null;
  }
  
  @Deprecated
  public static void saveCacheFile(String paramString, CacheResult paramCacheResult) {
    saveCacheFile(paramString, 0L, paramCacheResult);
  }
  
  static void saveCacheFile(String paramString, long paramLong, CacheResult paramCacheResult) {
    try {
      paramCacheResult.outStream.close();
      return;
    } catch (IOException iOException) {
      return;
    } 
  }
}
