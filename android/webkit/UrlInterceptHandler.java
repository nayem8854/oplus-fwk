package android.webkit;

import java.util.Map;

@Deprecated
public interface UrlInterceptHandler {
  @Deprecated
  PluginData getPluginData(String paramString, Map<String, String> paramMap);
  
  @Deprecated
  CacheManager.CacheResult service(String paramString, Map<String, String> paramMap);
}
