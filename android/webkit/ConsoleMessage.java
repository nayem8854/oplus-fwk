package android.webkit;

public class ConsoleMessage {
  private MessageLevel mLevel;
  
  private int mLineNumber;
  
  private String mMessage;
  
  private String mSourceId;
  
  public enum MessageLevel {
    DEBUG, ERROR, LOG, TIP, WARNING;
    
    private static final MessageLevel[] $VALUES;
    
    static {
      ERROR = new MessageLevel("ERROR", 3);
      MessageLevel messageLevel = new MessageLevel("DEBUG", 4);
      $VALUES = new MessageLevel[] { TIP, LOG, WARNING, ERROR, messageLevel };
    }
  }
  
  public ConsoleMessage(String paramString1, String paramString2, int paramInt, MessageLevel paramMessageLevel) {
    this.mMessage = paramString1;
    this.mSourceId = paramString2;
    this.mLineNumber = paramInt;
    this.mLevel = paramMessageLevel;
  }
  
  public MessageLevel messageLevel() {
    return this.mLevel;
  }
  
  public String message() {
    return this.mMessage;
  }
  
  public String sourceId() {
    return this.mSourceId;
  }
  
  public int lineNumber() {
    return this.mLineNumber;
  }
}
