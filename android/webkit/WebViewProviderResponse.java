package android.webkit;

import android.content.pm.PackageInfo;
import android.os.Parcel;
import android.os.Parcelable;

public final class WebViewProviderResponse implements Parcelable {
  public WebViewProviderResponse(PackageInfo paramPackageInfo, int paramInt) {
    this.packageInfo = paramPackageInfo;
    this.status = paramInt;
  }
  
  public static final Parcelable.Creator<WebViewProviderResponse> CREATOR = new Parcelable.Creator<WebViewProviderResponse>() {
      public WebViewProviderResponse createFromParcel(Parcel param1Parcel) {
        return new WebViewProviderResponse(param1Parcel);
      }
      
      public WebViewProviderResponse[] newArray(int param1Int) {
        return new WebViewProviderResponse[param1Int];
      }
    };
  
  public final PackageInfo packageInfo;
  
  public final int status;
  
  private WebViewProviderResponse(Parcel paramParcel) {
    this.packageInfo = (PackageInfo)paramParcel.readTypedObject(PackageInfo.CREATOR);
    this.status = paramParcel.readInt();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeTypedObject((Parcelable)this.packageInfo, paramInt);
    paramParcel.writeInt(this.status);
  }
}
