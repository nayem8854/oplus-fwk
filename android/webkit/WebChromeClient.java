package android.webkit;

import android.annotation.SystemApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Message;
import android.util.SeempLog;
import android.view.View;

public class WebChromeClient {
  public void onProgressChanged(WebView paramWebView, int paramInt) {}
  
  public void onReceivedTitle(WebView paramWebView, String paramString) {}
  
  public void onReceivedIcon(WebView paramWebView, Bitmap paramBitmap) {}
  
  public void onReceivedTouchIconUrl(WebView paramWebView, String paramString, boolean paramBoolean) {}
  
  public void onShowCustomView(View paramView, CustomViewCallback paramCustomViewCallback) {}
  
  @Deprecated
  public void onShowCustomView(View paramView, int paramInt, CustomViewCallback paramCustomViewCallback) {}
  
  public void onHideCustomView() {}
  
  public boolean onCreateWindow(WebView paramWebView, boolean paramBoolean1, boolean paramBoolean2, Message paramMessage) {
    return false;
  }
  
  public void onRequestFocus(WebView paramWebView) {}
  
  public void onCloseWindow(WebView paramWebView) {}
  
  public boolean onJsAlert(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult) {
    return false;
  }
  
  public boolean onJsConfirm(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult) {
    return false;
  }
  
  public boolean onJsPrompt(WebView paramWebView, String paramString1, String paramString2, String paramString3, JsPromptResult paramJsPromptResult) {
    return false;
  }
  
  public boolean onJsBeforeUnload(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult) {
    return false;
  }
  
  @Deprecated
  public void onExceededDatabaseQuota(String paramString1, String paramString2, long paramLong1, long paramLong2, long paramLong3, WebStorage.QuotaUpdater paramQuotaUpdater) {
    paramQuotaUpdater.updateQuota(paramLong1);
  }
  
  @Deprecated
  public void onReachedMaxAppCacheSize(long paramLong1, long paramLong2, WebStorage.QuotaUpdater paramQuotaUpdater) {
    paramQuotaUpdater.updateQuota(paramLong2);
  }
  
  public void onGeolocationPermissionsShowPrompt(String paramString, GeolocationPermissions.Callback paramCallback) {
    SeempLog.record(54);
  }
  
  public void onGeolocationPermissionsHidePrompt() {}
  
  public void onPermissionRequest(PermissionRequest paramPermissionRequest) {
    paramPermissionRequest.deny();
  }
  
  public void onPermissionRequestCanceled(PermissionRequest paramPermissionRequest) {}
  
  @Deprecated
  public boolean onJsTimeout() {
    return true;
  }
  
  @Deprecated
  public void onConsoleMessage(String paramString1, int paramInt, String paramString2) {}
  
  public boolean onConsoleMessage(ConsoleMessage paramConsoleMessage) {
    String str2 = paramConsoleMessage.message();
    int i = paramConsoleMessage.lineNumber();
    String str1 = paramConsoleMessage.sourceId();
    onConsoleMessage(str2, i, str1);
    return false;
  }
  
  public Bitmap getDefaultVideoPoster() {
    return null;
  }
  
  public View getVideoLoadingProgressView() {
    return null;
  }
  
  public void getVisitedHistory(ValueCallback<String[]> paramValueCallback) {}
  
  public boolean onShowFileChooser(WebView paramWebView, ValueCallback<Uri[]> paramValueCallback, FileChooserParams paramFileChooserParams) {
    return false;
  }
  
  public static interface CustomViewCallback {
    void onCustomViewHidden();
  }
  
  public static abstract class FileChooserParams {
    public static final int MODE_OPEN = 0;
    
    public static final int MODE_OPEN_FOLDER = 2;
    
    public static final int MODE_OPEN_MULTIPLE = 1;
    
    public static final int MODE_SAVE = 3;
    
    public static Uri[] parseResult(int param1Int, Intent param1Intent) {
      return WebViewFactory.getProvider().getStatics().parseFileChooserResult(param1Int, param1Intent);
    }
    
    public abstract Intent createIntent();
    
    public abstract String[] getAcceptTypes();
    
    public abstract String getFilenameHint();
    
    public abstract int getMode();
    
    public abstract CharSequence getTitle();
    
    public abstract boolean isCaptureEnabled();
  }
  
  @SystemApi
  @Deprecated
  public void openFileChooser(ValueCallback<Uri> paramValueCallback, String paramString1, String paramString2) {
    paramValueCallback.onReceiveValue(null);
  }
}
