package android.webkit;

import android.annotation.SystemApi;
import android.app.ActivityManager;
import android.app.AppGlobals;
import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.IBinder;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.Trace;
import android.util.AndroidRuntimeException;
import android.util.ArraySet;
import android.util.Log;
import java.io.File;
import java.lang.reflect.Method;

@SystemApi
public final class WebViewFactory {
  private static final String CHROMIUM_WEBVIEW_FACTORY = "com.android.webview.chromium.WebViewChromiumFactoryProviderForR";
  
  private static final String CHROMIUM_WEBVIEW_FACTORY_METHOD = "create";
  
  private static final boolean DEBUG = false;
  
  public static final int LIBLOAD_ADDRESS_SPACE_NOT_RESERVED = 2;
  
  public static final int LIBLOAD_FAILED_JNI_CALL = 7;
  
  public static final int LIBLOAD_FAILED_LISTING_WEBVIEW_PACKAGES = 4;
  
  public static final int LIBLOAD_FAILED_TO_FIND_NAMESPACE = 10;
  
  public static final int LIBLOAD_FAILED_TO_LOAD_LIBRARY = 6;
  
  public static final int LIBLOAD_FAILED_TO_OPEN_RELRO_FILE = 5;
  
  public static final int LIBLOAD_FAILED_WAITING_FOR_RELRO = 3;
  
  public static final int LIBLOAD_FAILED_WAITING_FOR_WEBVIEW_REASON_UNKNOWN = 8;
  
  public static final int LIBLOAD_SUCCESS = 0;
  
  public static final int LIBLOAD_WRONG_PACKAGE_NAME = 1;
  
  private static final String LOGTAG = "WebViewFactory";
  
  private static String WEBVIEW_UPDATE_SERVICE_NAME;
  
  private static String sDataDirectorySuffix;
  
  private static PackageInfo sPackageInfo;
  
  private static WebViewFactoryProvider sProviderInstance;
  
  private static final Object sProviderLock = new Object();
  
  private static boolean sWebViewDisabled;
  
  private static Boolean sWebViewSupported;
  
  private static String getWebViewPreparationErrorReason(int paramInt) {
    if (paramInt != 3) {
      if (paramInt != 4) {
        if (paramInt != 8)
          return "Unknown"; 
        return "Crashed for unknown reason";
      } 
      return "No WebView installed";
    } 
    return "Time out waiting for Relro files being created";
  }
  
  static class MissingWebViewPackageException extends Exception {
    public MissingWebViewPackageException(String param1String) {
      super(param1String);
    }
    
    public MissingWebViewPackageException(Exception param1Exception) {
      super(param1Exception);
    }
  }
  
  private static boolean isWebViewSupported() {
    if (sWebViewSupported == null) {
      PackageManager packageManager = AppGlobals.getInitialApplication().getPackageManager();
      boolean bool = packageManager.hasSystemFeature("android.software.webview");
      sWebViewSupported = Boolean.valueOf(bool);
    } 
    return sWebViewSupported.booleanValue();
  }
  
  static void disableWebView() {
    synchronized (sProviderLock) {
      if (sProviderInstance == null) {
        sWebViewDisabled = true;
        return;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Can't disable WebView: WebView already initialized");
      throw illegalStateException;
    } 
  }
  
  static void setDataDirectorySuffix(String paramString) {
    synchronized (sProviderLock) {
      if (sProviderInstance == null) {
        if (paramString.indexOf(File.separatorChar) < 0) {
          sDataDirectorySuffix = paramString;
          return;
        } 
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Suffix ");
        stringBuilder.append(paramString);
        stringBuilder.append(" contains a path separator");
        this(stringBuilder.toString());
        throw illegalArgumentException;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Can't set data directory suffix: WebView already initialized");
      throw illegalStateException;
    } 
  }
  
  static String getDataDirectorySuffix() {
    synchronized (sProviderLock) {
      return sDataDirectorySuffix;
    } 
  }
  
  public static String getWebViewLibrary(ApplicationInfo paramApplicationInfo) {
    if (paramApplicationInfo.metaData != null)
      return paramApplicationInfo.metaData.getString("com.android.webview.WebViewLibrary"); 
    return null;
  }
  
  public static PackageInfo getLoadedPackageInfo() {
    synchronized (sProviderLock) {
      return sPackageInfo;
    } 
  }
  
  public static Class<WebViewFactoryProvider> getWebViewProviderClass(ClassLoader paramClassLoader) throws ClassNotFoundException {
    return (Class)Class.forName("com.android.webview.chromium.WebViewChromiumFactoryProviderForR", true, paramClassLoader);
  }
  
  public static int loadWebViewNativeLibraryFromPackage(String paramString, ClassLoader paramClassLoader) {
    if (!isWebViewSupported())
      return 1; 
    try {
      WebViewProviderResponse webViewProviderResponse = getUpdateService().waitForAndGetProvider();
      if (webViewProviderResponse.status != 0 && webViewProviderResponse.status != 3)
        return webViewProviderResponse.status; 
      if (!webViewProviderResponse.packageInfo.packageName.equals(paramString))
        return 1; 
      PackageManager packageManager = AppGlobals.getInitialApplication().getPackageManager();
      try {
        PackageInfo packageInfo = packageManager.getPackageInfo(paramString, 268435584);
        String str = getWebViewLibrary(packageInfo.applicationInfo);
        int i = WebViewLibraryLoader.loadNativeLibrary(paramClassLoader, str);
        if (i == 0)
          return webViewProviderResponse.status; 
        return i;
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Couldn't find package ");
        stringBuilder.append(paramString);
        Log.e("WebViewFactory", stringBuilder.toString());
        return 1;
      } 
    } catch (RemoteException remoteException) {
      Log.e("WebViewFactory", "error waiting for relro creation", (Throwable)remoteException);
      return 8;
    } 
  }
  
  static WebViewFactoryProvider getProvider() {
    synchronized (sProviderLock) {
      if (sProviderInstance != null)
        return sProviderInstance; 
      int i = Process.myUid();
      if (i != 0 && i != 1000 && i != 1001 && i != 1027 && i != 1002) {
        if (isWebViewSupported()) {
          if (!sWebViewDisabled) {
            Trace.traceBegin(16L, "WebViewFactory.getProvider()");
            try {
              Class<WebViewFactoryProvider> clazz = getProviderClass();
              Method method = null;
              try {
                Method method1 = clazz.getMethod("create", new Class[] { WebViewDelegate.class });
              } catch (Exception exception) {}
              Trace.traceBegin(16L, "WebViewFactoryProvider invocation");
              try {
                WebViewDelegate webViewDelegate = new WebViewDelegate();
                this();
                WebViewFactoryProvider webViewFactoryProvider = (WebViewFactoryProvider)method.invoke(null, new Object[] { webViewDelegate });
                Trace.traceEnd(16L);
                return webViewFactoryProvider;
              } catch (Exception exception) {
                Log.e("WebViewFactory", "error instantiating provider", exception);
                AndroidRuntimeException androidRuntimeException = new AndroidRuntimeException();
                this(exception);
                throw androidRuntimeException;
              } finally {}
              Trace.traceEnd(16L);
              throw method;
            } finally {
              Trace.traceEnd(16L);
            } 
          } 
          IllegalStateException illegalStateException = new IllegalStateException();
          this("WebView.disableWebView() was called: WebView is disabled");
          throw illegalStateException;
        } 
        UnsupportedOperationException unsupportedOperationException1 = new UnsupportedOperationException();
        this();
        throw unsupportedOperationException1;
      } 
      UnsupportedOperationException unsupportedOperationException = new UnsupportedOperationException();
      this("For security reasons, WebView is not allowed in privileged processes");
      throw unsupportedOperationException;
    } 
  }
  
  private static boolean signaturesEquals(Signature[] paramArrayOfSignature1, Signature[] paramArrayOfSignature2) {
    boolean bool1 = false, bool2 = false;
    if (paramArrayOfSignature1 == null) {
      if (paramArrayOfSignature2 == null)
        bool2 = true; 
      return bool2;
    } 
    if (paramArrayOfSignature2 == null)
      return false; 
    ArraySet<Signature> arraySet1 = new ArraySet();
    int i;
    byte b;
    for (i = paramArrayOfSignature1.length, b = 0; b < i; ) {
      Signature signature = paramArrayOfSignature1[b];
      arraySet1.add(signature);
      b++;
    } 
    ArraySet<Signature> arraySet2 = new ArraySet();
    for (i = paramArrayOfSignature2.length, b = bool1; b < i; ) {
      Signature signature = paramArrayOfSignature2[b];
      arraySet2.add(signature);
      b++;
    } 
    return arraySet1.equals(arraySet2);
  }
  
  private static void verifyPackageInfo(PackageInfo paramPackageInfo1, PackageInfo paramPackageInfo2) throws MissingWebViewPackageException {
    StringBuilder stringBuilder1;
    if (paramPackageInfo1.packageName.equals(paramPackageInfo2.packageName)) {
      if (paramPackageInfo1.getLongVersionCode() <= paramPackageInfo2.getLongVersionCode()) {
        if (getWebViewLibrary(paramPackageInfo2.applicationInfo) != null) {
          if (signaturesEquals(paramPackageInfo1.signatures, paramPackageInfo2.signatures))
            return; 
          throw new MissingWebViewPackageException("Failed to verify WebView provider, signature mismatch");
        } 
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Tried to load an invalid WebView provider: ");
        stringBuilder1.append(paramPackageInfo2.packageName);
        throw new MissingWebViewPackageException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to verify WebView provider, version code is lower than expected: ");
      stringBuilder.append(stringBuilder1.getLongVersionCode());
      stringBuilder.append(" actual: ");
      stringBuilder.append(paramPackageInfo2.getLongVersionCode());
      throw new MissingWebViewPackageException(stringBuilder.toString());
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("Failed to verify WebView provider, packageName mismatch, expected: ");
    stringBuilder2.append(((PackageInfo)stringBuilder1).packageName);
    stringBuilder2.append(" actual: ");
    stringBuilder2.append(paramPackageInfo2.packageName);
    throw new MissingWebViewPackageException(stringBuilder2.toString());
  }
  
  private static Context getWebViewContextAndSetProvider() throws MissingWebViewPackageException {
    Application application = AppGlobals.getInitialApplication();
    try {
      Trace.traceBegin(16L, "WebViewUpdateService.waitForAndGetProvider()");
      try {
        WebViewProviderResponse webViewProviderResponse = getUpdateService().waitForAndGetProvider();
        Trace.traceEnd(16L);
        if (webViewProviderResponse.status == 0 || webViewProviderResponse.status == 3) {
          Trace.traceBegin(16L, "ActivityManager.addPackageDependency()");
          try {
            ActivityManager.getService().addPackageDependency(webViewProviderResponse.packageInfo.packageName);
            Trace.traceEnd(16L);
            PackageManager packageManager = application.getPackageManager();
            Trace.traceBegin(16L, "PackageManager.getPackageInfo()");
          } finally {
            Trace.traceEnd(16L);
          } 
        } 
        MissingWebViewPackageException missingWebViewPackageException = new MissingWebViewPackageException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Failed to load WebView provider: ");
        int i = webViewProviderResponse.status;
        stringBuilder.append(getWebViewPreparationErrorReason(i));
        this(stringBuilder.toString());
        throw missingWebViewPackageException;
      } finally {
        Trace.traceEnd(16L);
      } 
    } catch (RemoteException|android.content.pm.PackageManager.NameNotFoundException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to load WebView provider: ");
      stringBuilder.append(remoteException);
      throw new MissingWebViewPackageException(stringBuilder.toString());
    } 
  }
  
  private static Class<WebViewFactoryProvider> getProviderClass() {
    Application application = AppGlobals.getInitialApplication();
    try {
      Trace.traceBegin(16L, "WebViewFactory.getWebViewContextAndSetProvider()");
      try {
        Context context = getWebViewContextAndSetProvider();
        Trace.traceEnd(16L);
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Loading ");
        stringBuilder.append(sPackageInfo.packageName);
        stringBuilder.append(" version ");
        stringBuilder.append(sPackageInfo.versionName);
        stringBuilder.append(" (code ");
        PackageInfo packageInfo = sPackageInfo;
        stringBuilder.append(packageInfo.getLongVersionCode());
        stringBuilder.append(")");
        str = stringBuilder.toString();
        Log.i("WebViewFactory", str);
        Trace.traceBegin(16L, "WebViewFactory.getChromiumProviderClass()");
        try {
          for (String str : context.getApplicationInfo().getAllApkPaths())
            application.getAssets().addAssetPathAsSharedLibrary(str); 
          null = context.getClassLoader();
          Trace.traceBegin(16L, "WebViewFactory.loadNativeLibrary()");
          ApplicationInfo applicationInfo = sPackageInfo.applicationInfo;
          String str1 = getWebViewLibrary(applicationInfo);
          WebViewLibraryLoader.loadNativeLibrary(null, str1);
          Trace.traceEnd(16L);
          Trace.traceBegin(16L, "Class.forName()");
          try {
            return getWebViewProviderClass(null);
          } finally {
            Trace.traceEnd(16L);
          } 
        } catch (ClassNotFoundException classNotFoundException) {
          Log.e("WebViewFactory", "error loading provider", classNotFoundException);
          AndroidRuntimeException androidRuntimeException = new AndroidRuntimeException();
          this(classNotFoundException);
          throw androidRuntimeException;
        } finally {}
        Trace.traceEnd(16L);
        throw context;
      } finally {
        Trace.traceEnd(16L);
      } 
    } catch (MissingWebViewPackageException missingWebViewPackageException) {
      Log.e("WebViewFactory", "Chromium WebView package does not exist", missingWebViewPackageException);
      throw new AndroidRuntimeException(missingWebViewPackageException);
    } 
  }
  
  public static void prepareWebViewInZygote() {
    try {
      WebViewLibraryLoader.reserveAddressSpaceInZygote();
    } finally {
      Exception exception = null;
    } 
  }
  
  public static int onWebViewProviderChanged(PackageInfo paramPackageInfo) {
    int i = 0;
    try {
      int j;
    } finally {
      Exception exception = null;
    } 
    WebViewZygote.onWebViewProviderChanged(paramPackageInfo);
    return i;
  }
  
  static {
    WEBVIEW_UPDATE_SERVICE_NAME = "webviewupdate";
  }
  
  public static IWebViewUpdateService getUpdateService() {
    if (isWebViewSupported())
      return getUpdateServiceUnchecked(); 
    return null;
  }
  
  static IWebViewUpdateService getUpdateServiceUnchecked() {
    String str = WEBVIEW_UPDATE_SERVICE_NAME;
    IBinder iBinder = ServiceManager.getService(str);
    return IWebViewUpdateService.Stub.asInterface(iBinder);
  }
}
