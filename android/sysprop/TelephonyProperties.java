package android.sysprop;

import android.os.SystemProperties;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Function;

public final class TelephonyProperties {
  private static Boolean tryParseBoolean(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: getstatic java/util/Locale.US : Ljava/util/Locale;
    //   4: invokevirtual toLowerCase : (Ljava/util/Locale;)Ljava/lang/String;
    //   7: astore_0
    //   8: aload_0
    //   9: invokevirtual hashCode : ()I
    //   12: istore_1
    //   13: iload_1
    //   14: bipush #48
    //   16: if_icmpeq -> 86
    //   19: iload_1
    //   20: bipush #49
    //   22: if_icmpeq -> 72
    //   25: iload_1
    //   26: ldc_w 3569038
    //   29: if_icmpeq -> 57
    //   32: iload_1
    //   33: ldc_w 97196323
    //   36: if_icmpeq -> 42
    //   39: goto -> 100
    //   42: aload_0
    //   43: ldc_w 'false'
    //   46: invokevirtual equals : (Ljava/lang/Object;)Z
    //   49: ifeq -> 39
    //   52: iconst_3
    //   53: istore_1
    //   54: goto -> 102
    //   57: aload_0
    //   58: ldc_w 'true'
    //   61: invokevirtual equals : (Ljava/lang/Object;)Z
    //   64: ifeq -> 39
    //   67: iconst_1
    //   68: istore_1
    //   69: goto -> 102
    //   72: aload_0
    //   73: ldc '1'
    //   75: invokevirtual equals : (Ljava/lang/Object;)Z
    //   78: ifeq -> 39
    //   81: iconst_0
    //   82: istore_1
    //   83: goto -> 102
    //   86: aload_0
    //   87: ldc '0'
    //   89: invokevirtual equals : (Ljava/lang/Object;)Z
    //   92: ifeq -> 39
    //   95: iconst_2
    //   96: istore_1
    //   97: goto -> 102
    //   100: iconst_m1
    //   101: istore_1
    //   102: iload_1
    //   103: ifeq -> 127
    //   106: iload_1
    //   107: iconst_1
    //   108: if_icmpeq -> 127
    //   111: iload_1
    //   112: iconst_2
    //   113: if_icmpeq -> 123
    //   116: iload_1
    //   117: iconst_3
    //   118: if_icmpeq -> 123
    //   121: aconst_null
    //   122: areturn
    //   123: getstatic java/lang/Boolean.FALSE : Ljava/lang/Boolean;
    //   126: areturn
    //   127: getstatic java/lang/Boolean.TRUE : Ljava/lang/Boolean;
    //   130: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #20	-> 0
    //   #28	-> 121
    //   #26	-> 123
    //   #23	-> 127
  }
  
  private static Integer tryParseInteger(String paramString) {
    try {
      return Integer.valueOf(paramString);
    } catch (NumberFormatException numberFormatException) {
      return null;
    } 
  }
  
  private static Long tryParseLong(String paramString) {
    try {
      return Long.valueOf(paramString);
    } catch (NumberFormatException numberFormatException) {
      return null;
    } 
  }
  
  private static Double tryParseDouble(String paramString) {
    try {
      return Double.valueOf(paramString);
    } catch (NumberFormatException numberFormatException) {
      return null;
    } 
  }
  
  private static String tryParseString(String paramString) {
    if ("".equals(paramString))
      paramString = null; 
    return paramString;
  }
  
  private static <T extends Enum<T>> T tryParseEnum(Class<T> paramClass, String paramString) {
    try {
      return (T)Enum.valueOf((Class)paramClass, paramString.toUpperCase(Locale.US));
    } catch (IllegalArgumentException illegalArgumentException) {
      return null;
    } 
  }
  
  private static <T> List<T> tryParseList(Function<String, T> paramFunction, String paramString) {
    if ("".equals(paramString))
      return new ArrayList<>(); 
    ArrayList<T> arrayList = new ArrayList();
    int i = 0;
    while (true) {
      int j;
      StringBuilder stringBuilder = new StringBuilder();
      while (true) {
        j = i;
        if (i < paramString.length()) {
          j = i;
          if (paramString.charAt(i) != ',') {
            j = i;
            if (paramString.charAt(i) == '\\')
              j = i + 1; 
            if (j == paramString.length())
              break; 
            stringBuilder.append(paramString.charAt(j));
            i = j + 1;
            continue;
          } 
        } 
        break;
      } 
      arrayList.add(paramFunction.apply(stringBuilder.toString()));
      if (j == paramString.length())
        return arrayList; 
      i = j + 1;
    } 
  }
  
  private static <T extends Enum<T>> List<T> tryParseEnumList(Class<T> paramClass, String paramString) {
    if ("".equals(paramString))
      return new ArrayList<>(); 
    ArrayList<T> arrayList = new ArrayList();
    for (String paramString : paramString.split(","))
      arrayList.add(tryParseEnum(paramClass, paramString)); 
    return arrayList;
  }
  
  private static String escape(String paramString) {
    return paramString.replaceAll("([\\\\,])", "\\\\$1");
  }
  
  private static <T> String formatList(List<T> paramList) {
    StringJoiner stringJoiner = new StringJoiner(",");
    for (List<T> paramList : paramList) {
      String str;
      if (paramList == null) {
        str = "";
      } else {
        str = escape(str.toString());
      } 
      stringJoiner.add(str);
    } 
    return stringJoiner.toString();
  }
  
  private static <T extends Enum<T>> String formatEnumList(List<T> paramList, Function<T, String> paramFunction) {
    StringJoiner stringJoiner = new StringJoiner(",");
    for (Enum enum_ : paramList) {
      CharSequence charSequence;
      if (enum_ == null) {
        charSequence = "";
      } else {
        charSequence = paramFunction.apply((T)charSequence);
      } 
      stringJoiner.add(charSequence);
    } 
    return stringJoiner.toString();
  }
  
  public static Optional<Boolean> airplane_mode_on() {
    String str = SystemProperties.get("persist.radio.airplane_mode_on");
    return Optional.ofNullable(tryParseBoolean(str));
  }
  
  public static void airplane_mode_on(Boolean paramBoolean) {
    String str;
    if (paramBoolean == null) {
      str = "";
    } else if (str.booleanValue()) {
      str = "1";
    } else {
      str = "0";
    } 
    SystemProperties.set("persist.radio.airplane_mode_on", str);
  }
  
  public static List<String> baseband_version() {
    String str = SystemProperties.get("gsm.version.baseband");
    return tryParseList((Function<String, String>)_$$Lambda$TelephonyProperties$kemQbl44ndTqXdQVvnYppJuQboQ.INSTANCE, str);
  }
  
  public static void baseband_version(List<String> paramList) {
    String str;
    if (paramList == null) {
      str = "";
    } else {
      str = formatList((List<?>)str);
    } 
    SystemProperties.set("gsm.version.baseband", str);
  }
  
  public static Optional<String> ril_impl() {
    String str = SystemProperties.get("gsm.version.ril-impl");
    return Optional.ofNullable(tryParseString(str));
  }
  
  public static List<String> operator_alpha() {
    String str = SystemProperties.get("gsm.operator.alpha");
    return tryParseList((Function<String, String>)_$$Lambda$TelephonyProperties$BfPaTA0e9gauJmR4vGNCDkGZ3uc.INSTANCE, str);
  }
  
  public static void operator_alpha(List<String> paramList) {
    String str;
    if (paramList == null) {
      str = "";
    } else {
      str = formatList((List<?>)str);
    } 
    SystemProperties.set("gsm.operator.alpha", str);
  }
  
  public static List<String> operator_numeric() {
    String str = SystemProperties.get("gsm.operator.numeric");
    return tryParseList((Function<String, String>)_$$Lambda$TelephonyProperties$EV4LSOwY7Dsh1rJalZDLmnGJw5I.INSTANCE, str);
  }
  
  public static void operator_numeric(List<String> paramList) {
    String str;
    if (paramList == null) {
      str = "";
    } else {
      str = formatList((List<?>)str);
    } 
    SystemProperties.set("gsm.operator.numeric", str);
  }
  
  public static Optional<Boolean> operator_is_manual() {
    String str = SystemProperties.get("operator.ismanual");
    return Optional.ofNullable(tryParseBoolean(str));
  }
  
  public static List<Boolean> operator_is_roaming() {
    String str = SystemProperties.get("gsm.operator.isroaming");
    return tryParseList((Function<String, Boolean>)_$$Lambda$TelephonyProperties$UKEfAuJVPm5cKR_UnPj1L66mN34.INSTANCE, str);
  }
  
  public static void operator_is_roaming(List<Boolean> paramList) {
    String str;
    if (paramList == null) {
      str = "";
    } else {
      str = formatList((List<?>)str);
    } 
    SystemProperties.set("gsm.operator.isroaming", str);
  }
  
  public static List<String> operator_iso_country() {
    String str = SystemProperties.get("gsm.operator.iso-country");
    return tryParseList((Function<String, String>)_$$Lambda$TelephonyProperties$yK9cdPdkKXdcfM9Ey52BIE31a5M.INSTANCE, str);
  }
  
  public static void operator_iso_country(List<String> paramList) {
    String str;
    if (paramList == null) {
      str = "";
    } else {
      str = formatList((List<?>)str);
    } 
    SystemProperties.set("gsm.operator.iso-country", str);
  }
  
  public static Optional<String> lte_on_cdma_product_type() {
    String str = SystemProperties.get("telephony.lteOnCdmaProductType");
    return Optional.ofNullable(tryParseString(str));
  }
  
  public static Optional<Integer> lte_on_cdma_device() {
    String str = SystemProperties.get("telephony.lteOnCdmaDevice");
    return Optional.ofNullable(tryParseInteger(str));
  }
  
  public static List<Integer> current_active_phone() {
    String str = SystemProperties.get("gsm.current.phone-type");
    return tryParseList((Function<String, Integer>)_$$Lambda$TelephonyProperties$2V_2ZQoGHfOIfKo_A8Ss547oL_c.INSTANCE, str);
  }
  
  public static void current_active_phone(List<Integer> paramList) {
    String str;
    if (paramList == null) {
      str = "";
    } else {
      str = formatList((List<?>)str);
    } 
    SystemProperties.set("gsm.current.phone-type", str);
  }
  
  public static List<String> sim_state() {
    String str = SystemProperties.get("gsm.sim.state");
    return tryParseList((Function<String, String>)_$$Lambda$TelephonyProperties$fdR0mRnJd3OymvjDc_MI1AHnMwc.INSTANCE, str);
  }
  
  public static void sim_state(List<String> paramList) {
    String str;
    if (paramList == null) {
      str = "";
    } else {
      str = formatList((List<?>)str);
    } 
    SystemProperties.set("gsm.sim.state", str);
  }
  
  public static List<String> icc_operator_numeric() {
    String str = SystemProperties.get("gsm.sim.operator.numeric");
    return tryParseList((Function<String, String>)_$$Lambda$TelephonyProperties$dc_CgjsF3BtDxLSSKL5bQ9ullG0.INSTANCE, str);
  }
  
  public static void icc_operator_numeric(List<String> paramList) {
    String str;
    if (paramList == null) {
      str = "";
    } else {
      str = formatList((List<?>)str);
    } 
    SystemProperties.set("gsm.sim.operator.numeric", str);
  }
  
  public static List<String> icc_operator_alpha() {
    String str = SystemProperties.get("gsm.sim.operator.alpha");
    return tryParseList((Function<String, String>)_$$Lambda$TelephonyProperties$VtSZ_Uto4bMa49ncgAfdWewMFOU.INSTANCE, str);
  }
  
  public static void icc_operator_alpha(List<String> paramList) {
    String str;
    if (paramList == null) {
      str = "";
    } else {
      str = formatList((List<?>)str);
    } 
    SystemProperties.set("gsm.sim.operator.alpha", str);
  }
  
  public static List<String> icc_operator_iso_country() {
    String str = SystemProperties.get("gsm.sim.operator.iso-country");
    return tryParseList((Function<String, String>)_$$Lambda$TelephonyProperties$JNTRmlscGaFlYo_3krOr_WWd2QI.INSTANCE, str);
  }
  
  public static void icc_operator_iso_country(List<String> paramList) {
    String str;
    if (paramList == null) {
      str = "";
    } else {
      str = formatList((List<?>)str);
    } 
    SystemProperties.set("gsm.sim.operator.iso-country", str);
  }
  
  public static List<String> data_network_type() {
    String str = SystemProperties.get("gsm.network.type");
    return tryParseList((Function<String, String>)_$$Lambda$TelephonyProperties$0Zy6hglFVc_K9jiJWmuHmilIMkY.INSTANCE, str);
  }
  
  public static void data_network_type(List<String> paramList) {
    String str;
    if (paramList == null) {
      str = "";
    } else {
      str = formatList((List<?>)str);
    } 
    SystemProperties.set("gsm.network.type", str);
  }
  
  public static Optional<Boolean> in_ecm_mode() {
    String str = SystemProperties.get("ril.cdma.inecmmode");
    return Optional.ofNullable(tryParseBoolean(str));
  }
  
  public static void in_ecm_mode(Boolean paramBoolean) {
    String str;
    if (paramBoolean == null) {
      str = "";
    } else {
      str = str.toString();
    } 
    SystemProperties.set("ril.cdma.inecmmode", str);
  }
  
  public static Optional<Long> ecm_exit_timer() {
    String str = SystemProperties.get("ro.cdma.ecmexittimer");
    return Optional.ofNullable(tryParseLong(str));
  }
  
  public static Optional<String> operator_idp_string() {
    String str = SystemProperties.get("gsm.operator.idpstring");
    return Optional.ofNullable(tryParseString(str));
  }
  
  public static void operator_idp_string(String paramString) {
    if (paramString == null) {
      paramString = "";
    } else {
      paramString = paramString.toString();
    } 
    SystemProperties.set("gsm.operator.idpstring", paramString);
  }
  
  public static List<String> otasp_num_schema() {
    String str = SystemProperties.get("ro.cdma.otaspnumschema");
    return tryParseList((Function<String, String>)_$$Lambda$TelephonyProperties$kCQNtMqtfi6MMlFLqtIufNXwOS8.INSTANCE, str);
  }
  
  public static Optional<Boolean> disable_call() {
    String str = SystemProperties.get("ro.telephony.disable-call");
    return Optional.ofNullable(tryParseBoolean(str));
  }
  
  public static Optional<Boolean> ril_sends_multiple_call_ring() {
    String str = SystemProperties.get("ro.telephony.call_ring.multiple");
    return Optional.ofNullable(tryParseBoolean(str));
  }
  
  public static Optional<Integer> call_ring_delay() {
    String str = SystemProperties.get("ro.telephony.call_ring.delay");
    return Optional.ofNullable(tryParseInteger(str));
  }
  
  public static Optional<Integer> cdma_msg_id() {
    String str = SystemProperties.get("persist.radio.cdma.msgid");
    return Optional.ofNullable(tryParseInteger(str));
  }
  
  public static void cdma_msg_id(Integer paramInteger) {
    String str;
    if (paramInteger == null) {
      str = "";
    } else {
      str = str.toString();
    } 
    SystemProperties.set("persist.radio.cdma.msgid", str);
  }
  
  public static Optional<Integer> wake_lock_timeout() {
    String str = SystemProperties.get("ro.ril.wake_lock_timeout");
    return Optional.ofNullable(tryParseInteger(str));
  }
  
  public static Optional<Boolean> reset_on_radio_tech_change() {
    String str = SystemProperties.get("persist.radio.reset_on_switch");
    return Optional.ofNullable(tryParseBoolean(str));
  }
  
  public static List<Boolean> sms_receive() {
    String str = SystemProperties.get("telephony.sms.receive");
    return tryParseList((Function<String, Boolean>)_$$Lambda$TelephonyProperties$iJa3afMQmWbO1DX4jS9zkcOKZlY.INSTANCE, str);
  }
  
  public static List<Boolean> sms_send() {
    String str = SystemProperties.get("telephony.sms.send");
    return tryParseList((Function<String, Boolean>)_$$Lambda$TelephonyProperties$rKoNB08X7R8OCPq_VDCWDOm3lDM.INSTANCE, str);
  }
  
  public static Optional<Boolean> test_csim() {
    String str = SystemProperties.get("persist.radio.test-csim");
    return Optional.ofNullable(tryParseBoolean(str));
  }
  
  public static Optional<Boolean> ignore_nitz() {
    String str = SystemProperties.get("telephony.test.ignore.nitz");
    return Optional.ofNullable(tryParseBoolean(str));
  }
  
  public static Optional<String> multi_sim_config() {
    String str = SystemProperties.get("persist.radio.multisim.config");
    return Optional.ofNullable(tryParseString(str));
  }
  
  public static void multi_sim_config(String paramString) {
    if (paramString == null) {
      paramString = "";
    } else {
      paramString = paramString.toString();
    } 
    SystemProperties.set("persist.radio.multisim.config", paramString);
  }
  
  public static Optional<Boolean> reboot_on_modem_change() {
    String str = SystemProperties.get("persist.radio.reboot_on_modem_change");
    return Optional.ofNullable(tryParseBoolean(str));
  }
  
  public static Optional<Integer> videocall_audio_output() {
    String str = SystemProperties.get("persist.radio.call.audio.output");
    return Optional.ofNullable(tryParseInteger(str));
  }
  
  public static Optional<Boolean> enable_esim_ui_by_default() {
    String str = SystemProperties.get("esim.enable_esim_system_ui_by_default");
    return Optional.ofNullable(tryParseBoolean(str));
  }
  
  public static List<Integer> default_network() {
    String str = SystemProperties.get("ro.telephony.default_network");
    return tryParseList((Function<String, Integer>)_$$Lambda$TelephonyProperties$H4jN0VIBNpZQBeWYt6qS3DCe_M8.INSTANCE, str);
  }
  
  public static Optional<Boolean> data_roaming() {
    String str = SystemProperties.get("ro.com.android.dataroaming");
    return Optional.ofNullable(tryParseBoolean(str));
  }
  
  public static Optional<Boolean> mobile_data() {
    String str = SystemProperties.get("ro.com.android.mobiledata");
    return Optional.ofNullable(tryParseBoolean(str));
  }
  
  public static Optional<Integer> wps_info() {
    String str = SystemProperties.get("wifidirect.wps");
    return Optional.ofNullable(tryParseInteger(str));
  }
  
  public static Optional<Integer> max_active_modems() {
    String str = SystemProperties.get("telephony.active_modems.max_count");
    return Optional.ofNullable(tryParseInteger(str));
  }
  
  public static Optional<Integer> sim_slots_count() {
    String str = SystemProperties.get("ro.telephony.sim_slots.count");
    return Optional.ofNullable(tryParseInteger(str));
  }
}
