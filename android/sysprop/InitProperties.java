package android.sysprop;

import android.os.SystemProperties;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Function;

public final class InitProperties {
  private static Boolean tryParseBoolean(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: getstatic java/util/Locale.US : Ljava/util/Locale;
    //   4: invokevirtual toLowerCase : (Ljava/util/Locale;)Ljava/lang/String;
    //   7: astore_0
    //   8: aload_0
    //   9: invokevirtual hashCode : ()I
    //   12: istore_1
    //   13: iload_1
    //   14: bipush #48
    //   16: if_icmpeq -> 82
    //   19: iload_1
    //   20: bipush #49
    //   22: if_icmpeq -> 68
    //   25: iload_1
    //   26: ldc 3569038
    //   28: if_icmpeq -> 54
    //   31: iload_1
    //   32: ldc 97196323
    //   34: if_icmpeq -> 40
    //   37: goto -> 96
    //   40: aload_0
    //   41: ldc 'false'
    //   43: invokevirtual equals : (Ljava/lang/Object;)Z
    //   46: ifeq -> 37
    //   49: iconst_3
    //   50: istore_1
    //   51: goto -> 98
    //   54: aload_0
    //   55: ldc 'true'
    //   57: invokevirtual equals : (Ljava/lang/Object;)Z
    //   60: ifeq -> 37
    //   63: iconst_1
    //   64: istore_1
    //   65: goto -> 98
    //   68: aload_0
    //   69: ldc '1'
    //   71: invokevirtual equals : (Ljava/lang/Object;)Z
    //   74: ifeq -> 37
    //   77: iconst_0
    //   78: istore_1
    //   79: goto -> 98
    //   82: aload_0
    //   83: ldc '0'
    //   85: invokevirtual equals : (Ljava/lang/Object;)Z
    //   88: ifeq -> 37
    //   91: iconst_2
    //   92: istore_1
    //   93: goto -> 98
    //   96: iconst_m1
    //   97: istore_1
    //   98: iload_1
    //   99: ifeq -> 123
    //   102: iload_1
    //   103: iconst_1
    //   104: if_icmpeq -> 123
    //   107: iload_1
    //   108: iconst_2
    //   109: if_icmpeq -> 119
    //   112: iload_1
    //   113: iconst_3
    //   114: if_icmpeq -> 119
    //   117: aconst_null
    //   118: areturn
    //   119: getstatic java/lang/Boolean.FALSE : Ljava/lang/Boolean;
    //   122: areturn
    //   123: getstatic java/lang/Boolean.TRUE : Ljava/lang/Boolean;
    //   126: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #20	-> 0
    //   #28	-> 117
    //   #26	-> 119
    //   #23	-> 123
  }
  
  private static Integer tryParseInteger(String paramString) {
    try {
      return Integer.valueOf(paramString);
    } catch (NumberFormatException numberFormatException) {
      return null;
    } 
  }
  
  private static Long tryParseLong(String paramString) {
    try {
      return Long.valueOf(paramString);
    } catch (NumberFormatException numberFormatException) {
      return null;
    } 
  }
  
  private static Double tryParseDouble(String paramString) {
    try {
      return Double.valueOf(paramString);
    } catch (NumberFormatException numberFormatException) {
      return null;
    } 
  }
  
  private static String tryParseString(String paramString) {
    if ("".equals(paramString))
      paramString = null; 
    return paramString;
  }
  
  private static <T extends Enum<T>> T tryParseEnum(Class<T> paramClass, String paramString) {
    try {
      return (T)Enum.valueOf((Class)paramClass, paramString.toUpperCase(Locale.US));
    } catch (IllegalArgumentException illegalArgumentException) {
      return null;
    } 
  }
  
  private static <T> List<T> tryParseList(Function<String, T> paramFunction, String paramString) {
    if ("".equals(paramString))
      return new ArrayList<>(); 
    ArrayList<T> arrayList = new ArrayList();
    int i = 0;
    while (true) {
      int j;
      StringBuilder stringBuilder = new StringBuilder();
      while (true) {
        j = i;
        if (i < paramString.length()) {
          j = i;
          if (paramString.charAt(i) != ',') {
            j = i;
            if (paramString.charAt(i) == '\\')
              j = i + 1; 
            if (j == paramString.length())
              break; 
            stringBuilder.append(paramString.charAt(j));
            i = j + 1;
            continue;
          } 
        } 
        break;
      } 
      arrayList.add(paramFunction.apply(stringBuilder.toString()));
      if (j == paramString.length())
        return arrayList; 
      i = j + 1;
    } 
  }
  
  private static <T extends Enum<T>> List<T> tryParseEnumList(Class<T> paramClass, String paramString) {
    if ("".equals(paramString))
      return new ArrayList<>(); 
    ArrayList<T> arrayList = new ArrayList();
    for (String paramString : paramString.split(","))
      arrayList.add(tryParseEnum(paramClass, paramString)); 
    return arrayList;
  }
  
  private static String escape(String paramString) {
    return paramString.replaceAll("([\\\\,])", "\\\\$1");
  }
  
  private static <T> String formatList(List<T> paramList) {
    StringJoiner stringJoiner = new StringJoiner(",");
    for (List<T> paramList : paramList) {
      String str;
      if (paramList == null) {
        str = "";
      } else {
        str = escape(str.toString());
      } 
      stringJoiner.add(str);
    } 
    return stringJoiner.toString();
  }
  
  private static <T extends Enum<T>> String formatEnumList(List<T> paramList, Function<T, String> paramFunction) {
    StringJoiner stringJoiner = new StringJoiner(",");
    for (Enum enum_ : paramList) {
      CharSequence charSequence;
      if (enum_ == null) {
        charSequence = "";
      } else {
        charSequence = paramFunction.apply((T)charSequence);
      } 
      stringJoiner.add(charSequence);
    } 
    return stringJoiner.toString();
  }
  
  public static Optional<Boolean> userspace_reboot_in_progress() {
    String str = SystemProperties.get("sys.init.userspace_reboot.in_progress");
    return Optional.ofNullable(tryParseBoolean(str));
  }
  
  public static void userspace_reboot_in_progress(Boolean paramBoolean) {
    String str;
    if (paramBoolean == null) {
      str = "";
    } else if (str.booleanValue()) {
      str = "1";
    } else {
      str = "0";
    } 
    SystemProperties.set("sys.init.userspace_reboot.in_progress", str);
  }
  
  public static Optional<Boolean> is_userspace_reboot_supported() {
    String str = SystemProperties.get("init.userspace_reboot.is_supported");
    return Optional.ofNullable(tryParseBoolean(str));
  }
}
