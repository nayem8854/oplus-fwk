package android.privacy.internal.longitudinalreporting;

import android.privacy.DifferentialPrivacyConfig;
import android.privacy.DifferentialPrivacyEncoder;
import android.privacy.internal.rappor.RapporConfig;
import android.privacy.internal.rappor.RapporEncoder;

public class LongitudinalReportingEncoder implements DifferentialPrivacyEncoder {
  private static final boolean DEBUG = false;
  
  private static final String PRR1_ENCODER_ID = "prr1_encoder_id";
  
  private static final String PRR2_ENCODER_ID = "prr2_encoder_id";
  
  private static final String TAG = "LongitudinalEncoder";
  
  private final LongitudinalReportingConfig mConfig;
  
  private final Boolean mFakeValue;
  
  private final RapporEncoder mIRREncoder;
  
  private final boolean mIsSecure;
  
  public static LongitudinalReportingEncoder createEncoder(LongitudinalReportingConfig paramLongitudinalReportingConfig, byte[] paramArrayOfbyte) {
    return new LongitudinalReportingEncoder(paramLongitudinalReportingConfig, true, paramArrayOfbyte);
  }
  
  public static LongitudinalReportingEncoder createInsecureEncoderForTest(LongitudinalReportingConfig paramLongitudinalReportingConfig) {
    return new LongitudinalReportingEncoder(paramLongitudinalReportingConfig, false, null);
  }
  
  private LongitudinalReportingEncoder(LongitudinalReportingConfig paramLongitudinalReportingConfig, boolean paramBoolean, byte[] paramArrayOfbyte) {
    RapporEncoder rapporEncoder;
    this.mConfig = paramLongitudinalReportingConfig;
    this.mIsSecure = paramBoolean;
    double d = paramLongitudinalReportingConfig.getProbabilityP();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramLongitudinalReportingConfig.getEncoderId());
    stringBuilder.append("prr1_encoder_id");
    String str = stringBuilder.toString();
    boolean bool = getLongTermRandomizedResult(d, paramBoolean, paramArrayOfbyte, str);
    if (bool) {
      d = paramLongitudinalReportingConfig.getProbabilityQ();
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramLongitudinalReportingConfig.getEncoderId());
      stringBuilder1.append("prr2_encoder_id");
      String str1 = stringBuilder1.toString();
      this.mFakeValue = Boolean.valueOf(getLongTermRandomizedResult(d, paramBoolean, paramArrayOfbyte, str1));
    } else {
      this.mFakeValue = null;
    } 
    RapporConfig rapporConfig = paramLongitudinalReportingConfig.getIRRConfig();
    if (paramBoolean) {
      rapporEncoder = RapporEncoder.createEncoder(rapporConfig, paramArrayOfbyte);
    } else {
      rapporEncoder = RapporEncoder.createInsecureEncoderForTest((RapporConfig)rapporEncoder);
    } 
    this.mIRREncoder = rapporEncoder;
  }
  
  public byte[] encodeString(String paramString) {
    throw new UnsupportedOperationException();
  }
  
  public byte[] encodeBoolean(boolean paramBoolean) {
    Boolean bool = this.mFakeValue;
    if (bool != null)
      paramBoolean = bool.booleanValue(); 
    return this.mIRREncoder.encodeBoolean(paramBoolean);
  }
  
  public byte[] encodeBits(byte[] paramArrayOfbyte) {
    throw new UnsupportedOperationException();
  }
  
  public LongitudinalReportingConfig getConfig() {
    return this.mConfig;
  }
  
  public boolean isInsecureEncoderForTest() {
    return this.mIsSecure ^ true;
  }
  
  public static boolean getLongTermRandomizedResult(double paramDouble, boolean paramBoolean, byte[] paramArrayOfbyte, String paramString) {
    RapporEncoder rapporEncoder;
    double d;
    boolean bool2;
    if (paramDouble < 0.5D) {
      d = paramDouble * 2.0D;
    } else {
      d = (1.0D - paramDouble) * 2.0D;
    } 
    boolean bool1 = true;
    if (paramDouble < 0.5D) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    RapporConfig rapporConfig = new RapporConfig(paramString, 1, d, 0.0D, 1.0D, 1, 1);
    if (paramBoolean) {
      rapporEncoder = RapporEncoder.createEncoder(rapporConfig, paramArrayOfbyte);
    } else {
      rapporEncoder = RapporEncoder.createInsecureEncoderForTest(rapporConfig);
    } 
    if (rapporEncoder.encodeBoolean(bool2)[0] > 0) {
      paramBoolean = bool1;
    } else {
      paramBoolean = false;
    } 
    return paramBoolean;
  }
}
