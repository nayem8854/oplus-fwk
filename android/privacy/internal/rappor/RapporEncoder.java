package android.privacy.internal.rappor;

import android.privacy.DifferentialPrivacyConfig;
import android.privacy.DifferentialPrivacyEncoder;
import com.google.android.rappor.Encoder;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

public class RapporEncoder implements DifferentialPrivacyEncoder {
  private static final byte[] INSECURE_SECRET = new byte[] { 
      -41, 104, -103, -109, -108, 19, 83, 84, -2, -48, 
      126, 84, -2, -48, 126, 84, -41, 104, -103, -109, 
      -108, 19, 83, 84, -2, -48, 126, 84, -2, -48, 
      126, 84, -41, 104, -103, -109, -108, 19, 83, 84, 
      -2, -48, 126, 84, -2, -48, 126, 84 };
  
  private static final SecureRandom sSecureRandom = new SecureRandom();
  
  private final RapporConfig mConfig;
  
  private final Encoder mEncoder;
  
  private final boolean mIsSecure;
  
  private RapporEncoder(RapporConfig paramRapporConfig, boolean paramBoolean, byte[] paramArrayOfbyte) {
    Random random;
    byte[] arrayOfByte;
    this.mConfig = paramRapporConfig;
    this.mIsSecure = paramBoolean;
    if (paramBoolean) {
      SecureRandom secureRandom = sSecureRandom;
      arrayOfByte = paramArrayOfbyte;
      random = secureRandom;
    } else {
      random = new Random(getInsecureSeed(paramRapporConfig.mEncoderId));
      arrayOfByte = INSECURE_SECRET;
    } 
    this.mEncoder = new Encoder(random, null, null, arrayOfByte, paramRapporConfig.mEncoderId, paramRapporConfig.mNumBits, paramRapporConfig.mProbabilityF, paramRapporConfig.mProbabilityP, paramRapporConfig.mProbabilityQ, paramRapporConfig.mNumCohorts, paramRapporConfig.mNumBloomHashes);
  }
  
  private long getInsecureSeed(String paramString) {
    try {
      MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
      byte[] arrayOfByte = messageDigest.digest(paramString.getBytes(StandardCharsets.UTF_8));
      return ByteBuffer.wrap(arrayOfByte).getLong();
    } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
      throw new AssertionError("Unable generate insecure seed");
    } 
  }
  
  public static RapporEncoder createEncoder(RapporConfig paramRapporConfig, byte[] paramArrayOfbyte) {
    return new RapporEncoder(paramRapporConfig, true, paramArrayOfbyte);
  }
  
  public static RapporEncoder createInsecureEncoderForTest(RapporConfig paramRapporConfig) {
    return new RapporEncoder(paramRapporConfig, false, null);
  }
  
  public byte[] encodeString(String paramString) {
    return this.mEncoder.encodeString(paramString);
  }
  
  public byte[] encodeBoolean(boolean paramBoolean) {
    return this.mEncoder.encodeBoolean(paramBoolean);
  }
  
  public byte[] encodeBits(byte[] paramArrayOfbyte) {
    return this.mEncoder.encodeBits(paramArrayOfbyte);
  }
  
  public RapporConfig getConfig() {
    return this.mConfig;
  }
  
  public boolean isInsecureEncoderForTest() {
    return this.mIsSecure ^ true;
  }
}
