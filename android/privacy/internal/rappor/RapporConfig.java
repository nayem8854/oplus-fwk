package android.privacy.internal.rappor;

import android.privacy.DifferentialPrivacyConfig;
import android.text.TextUtils;
import com.android.internal.util.Preconditions;

public class RapporConfig implements DifferentialPrivacyConfig {
  private static final String ALGORITHM_NAME = "Rappor";
  
  final String mEncoderId;
  
  final int mNumBits;
  
  final int mNumBloomHashes;
  
  final int mNumCohorts;
  
  final double mProbabilityF;
  
  final double mProbabilityP;
  
  final double mProbabilityQ;
  
  public RapporConfig(String paramString, int paramInt1, double paramDouble1, double paramDouble2, double paramDouble3, int paramInt2, int paramInt3) {
    boolean bool;
    Preconditions.checkArgument(TextUtils.isEmpty(paramString) ^ true, "encoderId cannot be empty");
    this.mEncoderId = paramString;
    if (paramInt1 > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "numBits needs to be > 0");
    this.mNumBits = paramInt1;
    if (paramDouble1 >= 0.0D && paramDouble1 <= 1.0D) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "probabilityF must be in range [0.0, 1.0]");
    this.mProbabilityF = paramDouble1;
    if (paramDouble2 >= 0.0D && paramDouble2 <= 1.0D) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "probabilityP must be in range [0.0, 1.0]");
    this.mProbabilityP = paramDouble2;
    if (paramDouble3 >= 0.0D && paramDouble3 <= 1.0D) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "probabilityQ must be in range [0.0, 1.0]");
    this.mProbabilityQ = paramDouble3;
    if (paramInt2 > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "numCohorts needs to be > 0");
    this.mNumCohorts = paramInt2;
    if (paramInt3 > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "numBloomHashes needs to be > 0");
    this.mNumBloomHashes = paramInt3;
  }
  
  public String getAlgorithm() {
    return "Rappor";
  }
  
  public String toString() {
    String str = this.mEncoderId;
    int i = this.mNumBits;
    double d1 = this.mProbabilityF, d2 = this.mProbabilityP, d3 = this.mProbabilityQ;
    int j = this.mNumCohorts;
    int k = this.mNumBloomHashes;
    return String.format("EncoderId: %s, NumBits: %d, ProbabilityF: %.3f, ProbabilityP: %.3f, ProbabilityQ: %.3f, NumCohorts: %d, NumBloomHashes: %d", new Object[] { str, Integer.valueOf(i), Double.valueOf(d1), Double.valueOf(d2), Double.valueOf(d3), Integer.valueOf(j), Integer.valueOf(k) });
  }
}
