package android.privacy;

public interface DifferentialPrivacyEncoder {
  byte[] encodeBits(byte[] paramArrayOfbyte);
  
  byte[] encodeBoolean(boolean paramBoolean);
  
  byte[] encodeString(String paramString);
  
  DifferentialPrivacyConfig getConfig();
  
  boolean isInsecureEncoderForTest();
}
