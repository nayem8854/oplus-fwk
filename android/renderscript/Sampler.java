package android.renderscript;

public class Sampler extends BaseObj {
  float mAniso;
  
  Value mMag;
  
  Value mMin;
  
  Value mWrapR;
  
  Value mWrapS;
  
  Value mWrapT;
  
  class Value extends Enum<Value> {
    private static final Value[] $VALUES;
    
    public static final Value CLAMP;
    
    public static final Value LINEAR = new Value("LINEAR", 1, 1);
    
    public static final Value LINEAR_MIP_LINEAR = new Value("LINEAR_MIP_LINEAR", 2, 2);
    
    public static final Value LINEAR_MIP_NEAREST = new Value("LINEAR_MIP_NEAREST", 3, 5);
    
    public static final Value MIRRORED_REPEAT;
    
    public static Value valueOf(String param1String) {
      return Enum.<Value>valueOf(Value.class, param1String);
    }
    
    public static Value[] values() {
      return (Value[])$VALUES.clone();
    }
    
    public static final Value NEAREST = new Value("NEAREST", 0, 0);
    
    public static final Value WRAP = new Value("WRAP", 4, 3);
    
    int mID;
    
    static {
      CLAMP = new Value("CLAMP", 5, 4);
      Value value = new Value("MIRRORED_REPEAT", 6, 6);
      $VALUES = new Value[] { NEAREST, LINEAR, LINEAR_MIP_LINEAR, LINEAR_MIP_NEAREST, WRAP, CLAMP, value };
    }
    
    private Value(Sampler this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.mID = param1Int2;
    }
  }
  
  Sampler(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
    this.guard.open("destroy");
  }
  
  public Value getMinification() {
    return this.mMin;
  }
  
  public Value getMagnification() {
    return this.mMag;
  }
  
  public Value getWrapS() {
    return this.mWrapS;
  }
  
  public Value getWrapT() {
    return this.mWrapT;
  }
  
  public float getAnisotropy() {
    return this.mAniso;
  }
  
  public static Sampler CLAMP_NEAREST(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mSampler_CLAMP_NEAREST : Landroid/renderscript/Sampler;
    //   4: ifnonnull -> 71
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mSampler_CLAMP_NEAREST : Landroid/renderscript/Sampler;
    //   13: ifnonnull -> 61
    //   16: new android/renderscript/Sampler$Builder
    //   19: astore_1
    //   20: aload_1
    //   21: aload_0
    //   22: invokespecial <init> : (Landroid/renderscript/RenderScript;)V
    //   25: aload_1
    //   26: getstatic android/renderscript/Sampler$Value.NEAREST : Landroid/renderscript/Sampler$Value;
    //   29: invokevirtual setMinification : (Landroid/renderscript/Sampler$Value;)V
    //   32: aload_1
    //   33: getstatic android/renderscript/Sampler$Value.NEAREST : Landroid/renderscript/Sampler$Value;
    //   36: invokevirtual setMagnification : (Landroid/renderscript/Sampler$Value;)V
    //   39: aload_1
    //   40: getstatic android/renderscript/Sampler$Value.CLAMP : Landroid/renderscript/Sampler$Value;
    //   43: invokevirtual setWrapS : (Landroid/renderscript/Sampler$Value;)V
    //   46: aload_1
    //   47: getstatic android/renderscript/Sampler$Value.CLAMP : Landroid/renderscript/Sampler$Value;
    //   50: invokevirtual setWrapT : (Landroid/renderscript/Sampler$Value;)V
    //   53: aload_0
    //   54: aload_1
    //   55: invokevirtual create : ()Landroid/renderscript/Sampler;
    //   58: putfield mSampler_CLAMP_NEAREST : Landroid/renderscript/Sampler;
    //   61: aload_0
    //   62: monitorexit
    //   63: goto -> 71
    //   66: astore_1
    //   67: aload_0
    //   68: monitorexit
    //   69: aload_1
    //   70: athrow
    //   71: aload_0
    //   72: getfield mSampler_CLAMP_NEAREST : Landroid/renderscript/Sampler;
    //   75: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #101	-> 0
    //   #102	-> 7
    //   #103	-> 9
    //   #104	-> 16
    //   #105	-> 25
    //   #106	-> 32
    //   #107	-> 39
    //   #108	-> 46
    //   #109	-> 53
    //   #111	-> 61
    //   #113	-> 71
    // Exception table:
    //   from	to	target	type
    //   9	16	66	finally
    //   16	25	66	finally
    //   25	32	66	finally
    //   32	39	66	finally
    //   39	46	66	finally
    //   46	53	66	finally
    //   53	61	66	finally
    //   61	63	66	finally
    //   67	69	66	finally
  }
  
  public static Sampler CLAMP_LINEAR(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mSampler_CLAMP_LINEAR : Landroid/renderscript/Sampler;
    //   4: ifnonnull -> 71
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mSampler_CLAMP_LINEAR : Landroid/renderscript/Sampler;
    //   13: ifnonnull -> 61
    //   16: new android/renderscript/Sampler$Builder
    //   19: astore_1
    //   20: aload_1
    //   21: aload_0
    //   22: invokespecial <init> : (Landroid/renderscript/RenderScript;)V
    //   25: aload_1
    //   26: getstatic android/renderscript/Sampler$Value.LINEAR : Landroid/renderscript/Sampler$Value;
    //   29: invokevirtual setMinification : (Landroid/renderscript/Sampler$Value;)V
    //   32: aload_1
    //   33: getstatic android/renderscript/Sampler$Value.LINEAR : Landroid/renderscript/Sampler$Value;
    //   36: invokevirtual setMagnification : (Landroid/renderscript/Sampler$Value;)V
    //   39: aload_1
    //   40: getstatic android/renderscript/Sampler$Value.CLAMP : Landroid/renderscript/Sampler$Value;
    //   43: invokevirtual setWrapS : (Landroid/renderscript/Sampler$Value;)V
    //   46: aload_1
    //   47: getstatic android/renderscript/Sampler$Value.CLAMP : Landroid/renderscript/Sampler$Value;
    //   50: invokevirtual setWrapT : (Landroid/renderscript/Sampler$Value;)V
    //   53: aload_0
    //   54: aload_1
    //   55: invokevirtual create : ()Landroid/renderscript/Sampler;
    //   58: putfield mSampler_CLAMP_LINEAR : Landroid/renderscript/Sampler;
    //   61: aload_0
    //   62: monitorexit
    //   63: goto -> 71
    //   66: astore_1
    //   67: aload_0
    //   68: monitorexit
    //   69: aload_1
    //   70: athrow
    //   71: aload_0
    //   72: getfield mSampler_CLAMP_LINEAR : Landroid/renderscript/Sampler;
    //   75: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #125	-> 0
    //   #126	-> 7
    //   #127	-> 9
    //   #128	-> 16
    //   #129	-> 25
    //   #130	-> 32
    //   #131	-> 39
    //   #132	-> 46
    //   #133	-> 53
    //   #135	-> 61
    //   #137	-> 71
    // Exception table:
    //   from	to	target	type
    //   9	16	66	finally
    //   16	25	66	finally
    //   25	32	66	finally
    //   32	39	66	finally
    //   39	46	66	finally
    //   46	53	66	finally
    //   53	61	66	finally
    //   61	63	66	finally
    //   67	69	66	finally
  }
  
  public static Sampler CLAMP_LINEAR_MIP_LINEAR(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mSampler_CLAMP_LINEAR_MIP_LINEAR : Landroid/renderscript/Sampler;
    //   4: ifnonnull -> 71
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mSampler_CLAMP_LINEAR_MIP_LINEAR : Landroid/renderscript/Sampler;
    //   13: ifnonnull -> 61
    //   16: new android/renderscript/Sampler$Builder
    //   19: astore_1
    //   20: aload_1
    //   21: aload_0
    //   22: invokespecial <init> : (Landroid/renderscript/RenderScript;)V
    //   25: aload_1
    //   26: getstatic android/renderscript/Sampler$Value.LINEAR_MIP_LINEAR : Landroid/renderscript/Sampler$Value;
    //   29: invokevirtual setMinification : (Landroid/renderscript/Sampler$Value;)V
    //   32: aload_1
    //   33: getstatic android/renderscript/Sampler$Value.LINEAR : Landroid/renderscript/Sampler$Value;
    //   36: invokevirtual setMagnification : (Landroid/renderscript/Sampler$Value;)V
    //   39: aload_1
    //   40: getstatic android/renderscript/Sampler$Value.CLAMP : Landroid/renderscript/Sampler$Value;
    //   43: invokevirtual setWrapS : (Landroid/renderscript/Sampler$Value;)V
    //   46: aload_1
    //   47: getstatic android/renderscript/Sampler$Value.CLAMP : Landroid/renderscript/Sampler$Value;
    //   50: invokevirtual setWrapT : (Landroid/renderscript/Sampler$Value;)V
    //   53: aload_0
    //   54: aload_1
    //   55: invokevirtual create : ()Landroid/renderscript/Sampler;
    //   58: putfield mSampler_CLAMP_LINEAR_MIP_LINEAR : Landroid/renderscript/Sampler;
    //   61: aload_0
    //   62: monitorexit
    //   63: goto -> 71
    //   66: astore_1
    //   67: aload_0
    //   68: monitorexit
    //   69: aload_1
    //   70: athrow
    //   71: aload_0
    //   72: getfield mSampler_CLAMP_LINEAR_MIP_LINEAR : Landroid/renderscript/Sampler;
    //   75: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #149	-> 0
    //   #150	-> 7
    //   #151	-> 9
    //   #152	-> 16
    //   #153	-> 25
    //   #154	-> 32
    //   #155	-> 39
    //   #156	-> 46
    //   #157	-> 53
    //   #159	-> 61
    //   #161	-> 71
    // Exception table:
    //   from	to	target	type
    //   9	16	66	finally
    //   16	25	66	finally
    //   25	32	66	finally
    //   32	39	66	finally
    //   39	46	66	finally
    //   46	53	66	finally
    //   53	61	66	finally
    //   61	63	66	finally
    //   67	69	66	finally
  }
  
  public static Sampler WRAP_NEAREST(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mSampler_WRAP_NEAREST : Landroid/renderscript/Sampler;
    //   4: ifnonnull -> 71
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mSampler_WRAP_NEAREST : Landroid/renderscript/Sampler;
    //   13: ifnonnull -> 61
    //   16: new android/renderscript/Sampler$Builder
    //   19: astore_1
    //   20: aload_1
    //   21: aload_0
    //   22: invokespecial <init> : (Landroid/renderscript/RenderScript;)V
    //   25: aload_1
    //   26: getstatic android/renderscript/Sampler$Value.NEAREST : Landroid/renderscript/Sampler$Value;
    //   29: invokevirtual setMinification : (Landroid/renderscript/Sampler$Value;)V
    //   32: aload_1
    //   33: getstatic android/renderscript/Sampler$Value.NEAREST : Landroid/renderscript/Sampler$Value;
    //   36: invokevirtual setMagnification : (Landroid/renderscript/Sampler$Value;)V
    //   39: aload_1
    //   40: getstatic android/renderscript/Sampler$Value.WRAP : Landroid/renderscript/Sampler$Value;
    //   43: invokevirtual setWrapS : (Landroid/renderscript/Sampler$Value;)V
    //   46: aload_1
    //   47: getstatic android/renderscript/Sampler$Value.WRAP : Landroid/renderscript/Sampler$Value;
    //   50: invokevirtual setWrapT : (Landroid/renderscript/Sampler$Value;)V
    //   53: aload_0
    //   54: aload_1
    //   55: invokevirtual create : ()Landroid/renderscript/Sampler;
    //   58: putfield mSampler_WRAP_NEAREST : Landroid/renderscript/Sampler;
    //   61: aload_0
    //   62: monitorexit
    //   63: goto -> 71
    //   66: astore_1
    //   67: aload_0
    //   68: monitorexit
    //   69: aload_1
    //   70: athrow
    //   71: aload_0
    //   72: getfield mSampler_WRAP_NEAREST : Landroid/renderscript/Sampler;
    //   75: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #173	-> 0
    //   #174	-> 7
    //   #175	-> 9
    //   #176	-> 16
    //   #177	-> 25
    //   #178	-> 32
    //   #179	-> 39
    //   #180	-> 46
    //   #181	-> 53
    //   #183	-> 61
    //   #185	-> 71
    // Exception table:
    //   from	to	target	type
    //   9	16	66	finally
    //   16	25	66	finally
    //   25	32	66	finally
    //   32	39	66	finally
    //   39	46	66	finally
    //   46	53	66	finally
    //   53	61	66	finally
    //   61	63	66	finally
    //   67	69	66	finally
  }
  
  public static Sampler WRAP_LINEAR(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mSampler_WRAP_LINEAR : Landroid/renderscript/Sampler;
    //   4: ifnonnull -> 71
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mSampler_WRAP_LINEAR : Landroid/renderscript/Sampler;
    //   13: ifnonnull -> 61
    //   16: new android/renderscript/Sampler$Builder
    //   19: astore_1
    //   20: aload_1
    //   21: aload_0
    //   22: invokespecial <init> : (Landroid/renderscript/RenderScript;)V
    //   25: aload_1
    //   26: getstatic android/renderscript/Sampler$Value.LINEAR : Landroid/renderscript/Sampler$Value;
    //   29: invokevirtual setMinification : (Landroid/renderscript/Sampler$Value;)V
    //   32: aload_1
    //   33: getstatic android/renderscript/Sampler$Value.LINEAR : Landroid/renderscript/Sampler$Value;
    //   36: invokevirtual setMagnification : (Landroid/renderscript/Sampler$Value;)V
    //   39: aload_1
    //   40: getstatic android/renderscript/Sampler$Value.WRAP : Landroid/renderscript/Sampler$Value;
    //   43: invokevirtual setWrapS : (Landroid/renderscript/Sampler$Value;)V
    //   46: aload_1
    //   47: getstatic android/renderscript/Sampler$Value.WRAP : Landroid/renderscript/Sampler$Value;
    //   50: invokevirtual setWrapT : (Landroid/renderscript/Sampler$Value;)V
    //   53: aload_0
    //   54: aload_1
    //   55: invokevirtual create : ()Landroid/renderscript/Sampler;
    //   58: putfield mSampler_WRAP_LINEAR : Landroid/renderscript/Sampler;
    //   61: aload_0
    //   62: monitorexit
    //   63: goto -> 71
    //   66: astore_1
    //   67: aload_0
    //   68: monitorexit
    //   69: aload_1
    //   70: athrow
    //   71: aload_0
    //   72: getfield mSampler_WRAP_LINEAR : Landroid/renderscript/Sampler;
    //   75: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #197	-> 0
    //   #198	-> 7
    //   #199	-> 9
    //   #200	-> 16
    //   #201	-> 25
    //   #202	-> 32
    //   #203	-> 39
    //   #204	-> 46
    //   #205	-> 53
    //   #207	-> 61
    //   #209	-> 71
    // Exception table:
    //   from	to	target	type
    //   9	16	66	finally
    //   16	25	66	finally
    //   25	32	66	finally
    //   32	39	66	finally
    //   39	46	66	finally
    //   46	53	66	finally
    //   53	61	66	finally
    //   61	63	66	finally
    //   67	69	66	finally
  }
  
  public static Sampler WRAP_LINEAR_MIP_LINEAR(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mSampler_WRAP_LINEAR_MIP_LINEAR : Landroid/renderscript/Sampler;
    //   4: ifnonnull -> 71
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mSampler_WRAP_LINEAR_MIP_LINEAR : Landroid/renderscript/Sampler;
    //   13: ifnonnull -> 61
    //   16: new android/renderscript/Sampler$Builder
    //   19: astore_1
    //   20: aload_1
    //   21: aload_0
    //   22: invokespecial <init> : (Landroid/renderscript/RenderScript;)V
    //   25: aload_1
    //   26: getstatic android/renderscript/Sampler$Value.LINEAR_MIP_LINEAR : Landroid/renderscript/Sampler$Value;
    //   29: invokevirtual setMinification : (Landroid/renderscript/Sampler$Value;)V
    //   32: aload_1
    //   33: getstatic android/renderscript/Sampler$Value.LINEAR : Landroid/renderscript/Sampler$Value;
    //   36: invokevirtual setMagnification : (Landroid/renderscript/Sampler$Value;)V
    //   39: aload_1
    //   40: getstatic android/renderscript/Sampler$Value.WRAP : Landroid/renderscript/Sampler$Value;
    //   43: invokevirtual setWrapS : (Landroid/renderscript/Sampler$Value;)V
    //   46: aload_1
    //   47: getstatic android/renderscript/Sampler$Value.WRAP : Landroid/renderscript/Sampler$Value;
    //   50: invokevirtual setWrapT : (Landroid/renderscript/Sampler$Value;)V
    //   53: aload_0
    //   54: aload_1
    //   55: invokevirtual create : ()Landroid/renderscript/Sampler;
    //   58: putfield mSampler_WRAP_LINEAR_MIP_LINEAR : Landroid/renderscript/Sampler;
    //   61: aload_0
    //   62: monitorexit
    //   63: goto -> 71
    //   66: astore_1
    //   67: aload_0
    //   68: monitorexit
    //   69: aload_1
    //   70: athrow
    //   71: aload_0
    //   72: getfield mSampler_WRAP_LINEAR_MIP_LINEAR : Landroid/renderscript/Sampler;
    //   75: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #221	-> 0
    //   #222	-> 7
    //   #223	-> 9
    //   #224	-> 16
    //   #225	-> 25
    //   #226	-> 32
    //   #227	-> 39
    //   #228	-> 46
    //   #229	-> 53
    //   #231	-> 61
    //   #233	-> 71
    // Exception table:
    //   from	to	target	type
    //   9	16	66	finally
    //   16	25	66	finally
    //   25	32	66	finally
    //   32	39	66	finally
    //   39	46	66	finally
    //   46	53	66	finally
    //   53	61	66	finally
    //   61	63	66	finally
    //   67	69	66	finally
  }
  
  public static Sampler MIRRORED_REPEAT_NEAREST(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mSampler_MIRRORED_REPEAT_NEAREST : Landroid/renderscript/Sampler;
    //   4: ifnonnull -> 71
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mSampler_MIRRORED_REPEAT_NEAREST : Landroid/renderscript/Sampler;
    //   13: ifnonnull -> 61
    //   16: new android/renderscript/Sampler$Builder
    //   19: astore_1
    //   20: aload_1
    //   21: aload_0
    //   22: invokespecial <init> : (Landroid/renderscript/RenderScript;)V
    //   25: aload_1
    //   26: getstatic android/renderscript/Sampler$Value.NEAREST : Landroid/renderscript/Sampler$Value;
    //   29: invokevirtual setMinification : (Landroid/renderscript/Sampler$Value;)V
    //   32: aload_1
    //   33: getstatic android/renderscript/Sampler$Value.NEAREST : Landroid/renderscript/Sampler$Value;
    //   36: invokevirtual setMagnification : (Landroid/renderscript/Sampler$Value;)V
    //   39: aload_1
    //   40: getstatic android/renderscript/Sampler$Value.MIRRORED_REPEAT : Landroid/renderscript/Sampler$Value;
    //   43: invokevirtual setWrapS : (Landroid/renderscript/Sampler$Value;)V
    //   46: aload_1
    //   47: getstatic android/renderscript/Sampler$Value.MIRRORED_REPEAT : Landroid/renderscript/Sampler$Value;
    //   50: invokevirtual setWrapT : (Landroid/renderscript/Sampler$Value;)V
    //   53: aload_0
    //   54: aload_1
    //   55: invokevirtual create : ()Landroid/renderscript/Sampler;
    //   58: putfield mSampler_MIRRORED_REPEAT_NEAREST : Landroid/renderscript/Sampler;
    //   61: aload_0
    //   62: monitorexit
    //   63: goto -> 71
    //   66: astore_1
    //   67: aload_0
    //   68: monitorexit
    //   69: aload_1
    //   70: athrow
    //   71: aload_0
    //   72: getfield mSampler_MIRRORED_REPEAT_NEAREST : Landroid/renderscript/Sampler;
    //   75: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #245	-> 0
    //   #246	-> 7
    //   #247	-> 9
    //   #248	-> 16
    //   #249	-> 25
    //   #250	-> 32
    //   #251	-> 39
    //   #252	-> 46
    //   #253	-> 53
    //   #255	-> 61
    //   #257	-> 71
    // Exception table:
    //   from	to	target	type
    //   9	16	66	finally
    //   16	25	66	finally
    //   25	32	66	finally
    //   32	39	66	finally
    //   39	46	66	finally
    //   46	53	66	finally
    //   53	61	66	finally
    //   61	63	66	finally
    //   67	69	66	finally
  }
  
  public static Sampler MIRRORED_REPEAT_LINEAR(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mSampler_MIRRORED_REPEAT_LINEAR : Landroid/renderscript/Sampler;
    //   4: ifnonnull -> 71
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mSampler_MIRRORED_REPEAT_LINEAR : Landroid/renderscript/Sampler;
    //   13: ifnonnull -> 61
    //   16: new android/renderscript/Sampler$Builder
    //   19: astore_1
    //   20: aload_1
    //   21: aload_0
    //   22: invokespecial <init> : (Landroid/renderscript/RenderScript;)V
    //   25: aload_1
    //   26: getstatic android/renderscript/Sampler$Value.LINEAR : Landroid/renderscript/Sampler$Value;
    //   29: invokevirtual setMinification : (Landroid/renderscript/Sampler$Value;)V
    //   32: aload_1
    //   33: getstatic android/renderscript/Sampler$Value.LINEAR : Landroid/renderscript/Sampler$Value;
    //   36: invokevirtual setMagnification : (Landroid/renderscript/Sampler$Value;)V
    //   39: aload_1
    //   40: getstatic android/renderscript/Sampler$Value.MIRRORED_REPEAT : Landroid/renderscript/Sampler$Value;
    //   43: invokevirtual setWrapS : (Landroid/renderscript/Sampler$Value;)V
    //   46: aload_1
    //   47: getstatic android/renderscript/Sampler$Value.MIRRORED_REPEAT : Landroid/renderscript/Sampler$Value;
    //   50: invokevirtual setWrapT : (Landroid/renderscript/Sampler$Value;)V
    //   53: aload_0
    //   54: aload_1
    //   55: invokevirtual create : ()Landroid/renderscript/Sampler;
    //   58: putfield mSampler_MIRRORED_REPEAT_LINEAR : Landroid/renderscript/Sampler;
    //   61: aload_0
    //   62: monitorexit
    //   63: goto -> 71
    //   66: astore_1
    //   67: aload_0
    //   68: monitorexit
    //   69: aload_1
    //   70: athrow
    //   71: aload_0
    //   72: getfield mSampler_MIRRORED_REPEAT_LINEAR : Landroid/renderscript/Sampler;
    //   75: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #269	-> 0
    //   #270	-> 7
    //   #271	-> 9
    //   #272	-> 16
    //   #273	-> 25
    //   #274	-> 32
    //   #275	-> 39
    //   #276	-> 46
    //   #277	-> 53
    //   #279	-> 61
    //   #281	-> 71
    // Exception table:
    //   from	to	target	type
    //   9	16	66	finally
    //   16	25	66	finally
    //   25	32	66	finally
    //   32	39	66	finally
    //   39	46	66	finally
    //   46	53	66	finally
    //   53	61	66	finally
    //   61	63	66	finally
    //   67	69	66	finally
  }
  
  public static Sampler MIRRORED_REPEAT_LINEAR_MIP_LINEAR(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mSampler_MIRRORED_REPEAT_LINEAR_MIP_LINEAR : Landroid/renderscript/Sampler;
    //   4: ifnonnull -> 71
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mSampler_MIRRORED_REPEAT_LINEAR_MIP_LINEAR : Landroid/renderscript/Sampler;
    //   13: ifnonnull -> 61
    //   16: new android/renderscript/Sampler$Builder
    //   19: astore_1
    //   20: aload_1
    //   21: aload_0
    //   22: invokespecial <init> : (Landroid/renderscript/RenderScript;)V
    //   25: aload_1
    //   26: getstatic android/renderscript/Sampler$Value.LINEAR_MIP_LINEAR : Landroid/renderscript/Sampler$Value;
    //   29: invokevirtual setMinification : (Landroid/renderscript/Sampler$Value;)V
    //   32: aload_1
    //   33: getstatic android/renderscript/Sampler$Value.LINEAR : Landroid/renderscript/Sampler$Value;
    //   36: invokevirtual setMagnification : (Landroid/renderscript/Sampler$Value;)V
    //   39: aload_1
    //   40: getstatic android/renderscript/Sampler$Value.MIRRORED_REPEAT : Landroid/renderscript/Sampler$Value;
    //   43: invokevirtual setWrapS : (Landroid/renderscript/Sampler$Value;)V
    //   46: aload_1
    //   47: getstatic android/renderscript/Sampler$Value.MIRRORED_REPEAT : Landroid/renderscript/Sampler$Value;
    //   50: invokevirtual setWrapT : (Landroid/renderscript/Sampler$Value;)V
    //   53: aload_0
    //   54: aload_1
    //   55: invokevirtual create : ()Landroid/renderscript/Sampler;
    //   58: putfield mSampler_MIRRORED_REPEAT_LINEAR_MIP_LINEAR : Landroid/renderscript/Sampler;
    //   61: aload_0
    //   62: monitorexit
    //   63: goto -> 71
    //   66: astore_1
    //   67: aload_0
    //   68: monitorexit
    //   69: aload_1
    //   70: athrow
    //   71: aload_0
    //   72: getfield mSampler_MIRRORED_REPEAT_LINEAR_MIP_LINEAR : Landroid/renderscript/Sampler;
    //   75: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #293	-> 0
    //   #294	-> 7
    //   #295	-> 9
    //   #296	-> 16
    //   #297	-> 25
    //   #298	-> 32
    //   #299	-> 39
    //   #300	-> 46
    //   #301	-> 53
    //   #303	-> 61
    //   #305	-> 71
    // Exception table:
    //   from	to	target	type
    //   9	16	66	finally
    //   16	25	66	finally
    //   25	32	66	finally
    //   32	39	66	finally
    //   39	46	66	finally
    //   46	53	66	finally
    //   53	61	66	finally
    //   61	63	66	finally
    //   67	69	66	finally
  }
  
  class Builder {
    float mAniso;
    
    Sampler.Value mMag;
    
    Sampler.Value mMin;
    
    RenderScript mRS;
    
    Sampler.Value mWrapR;
    
    Sampler.Value mWrapS;
    
    Sampler.Value mWrapT;
    
    public Builder(Sampler this$0) {
      this.mRS = (RenderScript)this$0;
      this.mMin = Sampler.Value.NEAREST;
      this.mMag = Sampler.Value.NEAREST;
      this.mWrapS = Sampler.Value.WRAP;
      this.mWrapT = Sampler.Value.WRAP;
      this.mWrapR = Sampler.Value.WRAP;
      this.mAniso = 1.0F;
    }
    
    public void setMinification(Sampler.Value param1Value) {
      if (param1Value == Sampler.Value.NEAREST || param1Value == Sampler.Value.LINEAR || param1Value == Sampler.Value.LINEAR_MIP_LINEAR || param1Value == Sampler.Value.LINEAR_MIP_NEAREST) {
        this.mMin = param1Value;
        return;
      } 
      throw new IllegalArgumentException("Invalid value");
    }
    
    public void setMagnification(Sampler.Value param1Value) {
      if (param1Value == Sampler.Value.NEAREST || param1Value == Sampler.Value.LINEAR) {
        this.mMag = param1Value;
        return;
      } 
      throw new IllegalArgumentException("Invalid value");
    }
    
    public void setWrapS(Sampler.Value param1Value) {
      if (param1Value == Sampler.Value.WRAP || param1Value == Sampler.Value.CLAMP || param1Value == Sampler.Value.MIRRORED_REPEAT) {
        this.mWrapS = param1Value;
        return;
      } 
      throw new IllegalArgumentException("Invalid value");
    }
    
    public void setWrapT(Sampler.Value param1Value) {
      if (param1Value == Sampler.Value.WRAP || param1Value == Sampler.Value.CLAMP || param1Value == Sampler.Value.MIRRORED_REPEAT) {
        this.mWrapT = param1Value;
        return;
      } 
      throw new IllegalArgumentException("Invalid value");
    }
    
    public void setAnisotropy(float param1Float) {
      if (param1Float >= 0.0F) {
        this.mAniso = param1Float;
        return;
      } 
      throw new IllegalArgumentException("Invalid value");
    }
    
    public Sampler create() {
      this.mRS.validate();
      long l = this.mRS.nSamplerCreate(this.mMag.mID, this.mMin.mID, this.mWrapS.mID, this.mWrapT.mID, this.mWrapR.mID, this.mAniso);
      Sampler sampler = new Sampler(l, this.mRS);
      sampler.mMin = this.mMin;
      sampler.mMag = this.mMag;
      sampler.mWrapS = this.mWrapS;
      sampler.mWrapT = this.mWrapT;
      sampler.mWrapR = this.mWrapR;
      sampler.mAniso = this.mAniso;
      return sampler;
    }
  }
}
