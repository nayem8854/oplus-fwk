package android.renderscript;

import android.content.res.Resources;

public class Program extends BaseObj {
  static final int MAX_CONSTANT = 8;
  
  static final int MAX_INPUT = 8;
  
  static final int MAX_OUTPUT = 8;
  
  static final int MAX_TEXTURE = 8;
  
  Type[] mConstants;
  
  Element[] mInputs;
  
  Element[] mOutputs;
  
  String mShader;
  
  int mTextureCount;
  
  String[] mTextureNames;
  
  TextureType[] mTextures;
  
  class TextureType extends Enum<TextureType> {
    private static final TextureType[] $VALUES;
    
    public static TextureType valueOf(String param1String) {
      return Enum.<TextureType>valueOf(TextureType.class, param1String);
    }
    
    public static TextureType[] values() {
      return (TextureType[])$VALUES.clone();
    }
    
    public static final TextureType TEXTURE_2D = new TextureType("TEXTURE_2D", 0, 0);
    
    public static final TextureType TEXTURE_CUBE;
    
    int mID;
    
    static {
      TextureType textureType = new TextureType("TEXTURE_CUBE", 1, 1);
      $VALUES = new TextureType[] { TEXTURE_2D, textureType };
    }
    
    private TextureType(Program this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.mID = param1Int2;
    }
  }
  
  class ProgramParam extends Enum<ProgramParam> {
    private static final ProgramParam[] $VALUES;
    
    public static final ProgramParam CONSTANT = new ProgramParam("CONSTANT", 2, 2);
    
    public static ProgramParam valueOf(String param1String) {
      return Enum.<ProgramParam>valueOf(ProgramParam.class, param1String);
    }
    
    public static ProgramParam[] values() {
      return (ProgramParam[])$VALUES.clone();
    }
    
    public static final ProgramParam INPUT = new ProgramParam("INPUT", 0, 0);
    
    public static final ProgramParam OUTPUT = new ProgramParam("OUTPUT", 1, 1);
    
    public static final ProgramParam TEXTURE_TYPE;
    
    int mID;
    
    static {
      ProgramParam programParam = new ProgramParam("TEXTURE_TYPE", 3, 3);
      $VALUES = new ProgramParam[] { INPUT, OUTPUT, CONSTANT, programParam };
    }
    
    private ProgramParam(Program this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.mID = param1Int2;
    }
  }
  
  Program(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
    this.guard.open("destroy");
  }
  
  public int getConstantCount() {
    boolean bool;
    Type[] arrayOfType = this.mConstants;
    if (arrayOfType != null) {
      bool = arrayOfType.length;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public Type getConstant(int paramInt) {
    if (paramInt >= 0) {
      Type[] arrayOfType = this.mConstants;
      if (paramInt < arrayOfType.length)
        return arrayOfType[paramInt]; 
    } 
    throw new IllegalArgumentException("Slot ID out of range.");
  }
  
  public int getTextureCount() {
    return this.mTextureCount;
  }
  
  public TextureType getTextureType(int paramInt) {
    if (paramInt >= 0 && paramInt < this.mTextureCount)
      return this.mTextures[paramInt]; 
    throw new IllegalArgumentException("Slot ID out of range.");
  }
  
  public String getTextureName(int paramInt) {
    if (paramInt >= 0 && paramInt < this.mTextureCount)
      return this.mTextureNames[paramInt]; 
    throw new IllegalArgumentException("Slot ID out of range.");
  }
  
  public void bindConstants(Allocation paramAllocation, int paramInt) {
    if (paramInt >= 0 && paramInt < this.mConstants.length) {
      if (paramAllocation == null || 
        paramAllocation.getType().getID(this.mRS) == this.mConstants[paramInt].getID(this.mRS)) {
        long l;
        if (paramAllocation != null) {
          l = paramAllocation.getID(this.mRS);
        } else {
          l = 0L;
        } 
        this.mRS.nProgramBindConstants(getID(this.mRS), paramInt, l);
        return;
      } 
      throw new IllegalArgumentException("Allocation type does not match slot type.");
    } 
    throw new IllegalArgumentException("Slot ID out of range.");
  }
  
  public void bindTexture(Allocation paramAllocation, int paramInt) throws IllegalArgumentException {
    this.mRS.validate();
    if (paramInt >= 0 && paramInt < this.mTextureCount) {
      if (paramAllocation == null || !paramAllocation.getType().hasFaces() || this.mTextures[paramInt] == TextureType.TEXTURE_CUBE) {
        long l;
        if (paramAllocation != null) {
          l = paramAllocation.getID(this.mRS);
        } else {
          l = 0L;
        } 
        this.mRS.nProgramBindTexture(getID(this.mRS), paramInt, l);
        return;
      } 
      throw new IllegalArgumentException("Cannot bind cubemap to 2d texture slot");
    } 
    throw new IllegalArgumentException("Slot ID out of range.");
  }
  
  public void bindSampler(Sampler paramSampler, int paramInt) throws IllegalArgumentException {
    this.mRS.validate();
    if (paramInt >= 0 && paramInt < this.mTextureCount) {
      long l;
      if (paramSampler != null) {
        l = paramSampler.getID(this.mRS);
      } else {
        l = 0L;
      } 
      this.mRS.nProgramBindSampler(getID(this.mRS), paramInt, l);
      return;
    } 
    throw new IllegalArgumentException("Slot ID out of range.");
  }
  
  class BaseProgramBuilder {
    int mConstantCount;
    
    Type[] mConstants;
    
    int mInputCount;
    
    Element[] mInputs;
    
    int mOutputCount;
    
    Element[] mOutputs;
    
    RenderScript mRS;
    
    String mShader;
    
    int mTextureCount;
    
    String[] mTextureNames;
    
    Program.TextureType[] mTextureTypes;
    
    Type[] mTextures;
    
    protected BaseProgramBuilder(Program this$0) {
      this.mRS = (RenderScript)this$0;
      this.mInputs = new Element[8];
      this.mOutputs = new Element[8];
      this.mConstants = new Type[8];
      this.mInputCount = 0;
      this.mOutputCount = 0;
      this.mConstantCount = 0;
      this.mTextureCount = 0;
      this.mTextureTypes = new Program.TextureType[8];
      this.mTextureNames = new String[8];
    }
    
    public BaseProgramBuilder setShader(String param1String) {
      this.mShader = param1String;
      return this;
    }
    
    public BaseProgramBuilder setShader(Resources param1Resources, int param1Int) {
      // Byte code:
      //   0: aload_1
      //   1: iload_2
      //   2: invokevirtual openRawResource : (I)Ljava/io/InputStream;
      //   5: astore_3
      //   6: sipush #1024
      //   9: newarray byte
      //   11: astore_1
      //   12: iconst_0
      //   13: istore_2
      //   14: aload_1
      //   15: arraylength
      //   16: iload_2
      //   17: isub
      //   18: istore #4
      //   20: aload_1
      //   21: astore #5
      //   23: iload #4
      //   25: istore #6
      //   27: iload #4
      //   29: ifne -> 57
      //   32: aload_1
      //   33: arraylength
      //   34: iconst_2
      //   35: imul
      //   36: newarray byte
      //   38: astore #5
      //   40: aload_1
      //   41: iconst_0
      //   42: aload #5
      //   44: iconst_0
      //   45: aload_1
      //   46: arraylength
      //   47: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
      //   50: aload #5
      //   52: arraylength
      //   53: iload_2
      //   54: isub
      //   55: istore #6
      //   57: aload_3
      //   58: aload #5
      //   60: iload_2
      //   61: iload #6
      //   63: invokevirtual read : ([BII)I
      //   66: istore #6
      //   68: iload #6
      //   70: ifgt -> 110
      //   73: aload_3
      //   74: invokevirtual close : ()V
      //   77: new java/lang/String
      //   80: astore_1
      //   81: aload_1
      //   82: aload #5
      //   84: iconst_0
      //   85: iload_2
      //   86: ldc 'UTF-8'
      //   88: invokespecial <init> : ([BIILjava/lang/String;)V
      //   91: aload_0
      //   92: aload_1
      //   93: putfield mShader : Ljava/lang/String;
      //   96: goto -> 108
      //   99: astore_1
      //   100: ldc 'RenderScript shader creation'
      //   102: ldc 'Could not decode shader string'
      //   104: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   107: pop
      //   108: aload_0
      //   109: areturn
      //   110: iload_2
      //   111: iload #6
      //   113: iadd
      //   114: istore_2
      //   115: aload #5
      //   117: astore_1
      //   118: goto -> 14
      //   121: astore_1
      //   122: aload_3
      //   123: invokevirtual close : ()V
      //   126: aload_1
      //   127: athrow
      //   128: astore_1
      //   129: new android/content/res/Resources$NotFoundException
      //   132: dup
      //   133: invokespecial <init> : ()V
      //   136: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #263	-> 0
      //   #266	-> 6
      //   #267	-> 12
      //   #269	-> 14
      //   #270	-> 20
      //   #271	-> 32
      //   #272	-> 40
      //   #273	-> 50
      //   #274	-> 50
      //   #276	-> 57
      //   #277	-> 68
      //   #278	-> 73
      //   #283	-> 73
      //   #284	-> 77
      //   #287	-> 77
      //   #290	-> 77
      //   #293	-> 96
      //   #291	-> 99
      //   #292	-> 100
      //   #295	-> 108
      //   #280	-> 110
      //   #281	-> 115
      //   #283	-> 121
      //   #284	-> 126
      //   #285	-> 128
      //   #286	-> 129
      // Exception table:
      //   from	to	target	type
      //   6	12	121	finally
      //   14	20	121	finally
      //   32	40	121	finally
      //   40	50	121	finally
      //   50	57	121	finally
      //   57	68	121	finally
      //   73	77	128	java/io/IOException
      //   77	96	99	java/io/UnsupportedEncodingException
      //   122	126	128	java/io/IOException
      //   126	128	128	java/io/IOException
    }
    
    public int getCurrentConstantIndex() {
      return this.mConstantCount - 1;
    }
    
    public int getCurrentTextureIndex() {
      return this.mTextureCount - 1;
    }
    
    public BaseProgramBuilder addConstant(Type param1Type) throws IllegalStateException {
      if (this.mConstantCount < 8) {
        if (!param1Type.getElement().isComplex()) {
          Type[] arrayOfType = this.mConstants;
          int i = this.mConstantCount;
          arrayOfType[i] = param1Type;
          this.mConstantCount = i + 1;
          return this;
        } 
        throw new RSIllegalArgumentException("Complex elements not allowed.");
      } 
      throw new RSIllegalArgumentException("Max input count exceeded.");
    }
    
    public BaseProgramBuilder addTexture(Program.TextureType param1TextureType) throws IllegalArgumentException {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Tex");
      stringBuilder.append(this.mTextureCount);
      addTexture(param1TextureType, stringBuilder.toString());
      return this;
    }
    
    public BaseProgramBuilder addTexture(Program.TextureType param1TextureType, String param1String) throws IllegalArgumentException {
      int i = this.mTextureCount;
      if (i < 8) {
        this.mTextureTypes[i] = param1TextureType;
        this.mTextureNames[i] = param1String;
        this.mTextureCount = i + 1;
        return this;
      } 
      throw new IllegalArgumentException("Max texture count exceeded.");
    }
    
    protected void initProgram(Program param1Program) {
      param1Program.mInputs = new Element[this.mInputCount];
      System.arraycopy(this.mInputs, 0, param1Program.mInputs, 0, this.mInputCount);
      param1Program.mOutputs = new Element[this.mOutputCount];
      System.arraycopy(this.mOutputs, 0, param1Program.mOutputs, 0, this.mOutputCount);
      param1Program.mConstants = new Type[this.mConstantCount];
      System.arraycopy(this.mConstants, 0, param1Program.mConstants, 0, this.mConstantCount);
      param1Program.mTextureCount = this.mTextureCount;
      param1Program.mTextures = new Program.TextureType[this.mTextureCount];
      System.arraycopy(this.mTextureTypes, 0, param1Program.mTextures, 0, this.mTextureCount);
      param1Program.mTextureNames = new String[this.mTextureCount];
      System.arraycopy(this.mTextureNames, 0, param1Program.mTextureNames, 0, this.mTextureCount);
    }
  }
}
