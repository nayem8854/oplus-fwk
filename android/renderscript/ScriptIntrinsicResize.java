package android.renderscript;

public final class ScriptIntrinsicResize extends ScriptIntrinsic {
  private Allocation mInput;
  
  private ScriptIntrinsicResize(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
  }
  
  public static ScriptIntrinsicResize create(RenderScript paramRenderScript) {
    long l = paramRenderScript.nScriptIntrinsicCreate(12, 0L);
    return new ScriptIntrinsicResize(l, paramRenderScript);
  }
  
  public void setInput(Allocation paramAllocation) {
    Element element = paramAllocation.getElement();
    if (!element.isCompatible(Element.U8(this.mRS))) {
      RenderScript renderScript = this.mRS;
      if (!element.isCompatible(Element.U8_2(renderScript))) {
        renderScript = this.mRS;
        if (!element.isCompatible(Element.U8_3(renderScript))) {
          renderScript = this.mRS;
          if (!element.isCompatible(Element.U8_4(renderScript))) {
            renderScript = this.mRS;
            if (!element.isCompatible(Element.F32(renderScript))) {
              renderScript = this.mRS;
              if (!element.isCompatible(Element.F32_2(renderScript))) {
                renderScript = this.mRS;
                if (!element.isCompatible(Element.F32_3(renderScript))) {
                  renderScript = this.mRS;
                  if (!element.isCompatible(Element.F32_4(renderScript)))
                    throw new RSIllegalArgumentException("Unsupported element type."); 
                } 
              } 
            } 
          } 
        } 
      } 
    } 
    this.mInput = paramAllocation;
    setVar(0, paramAllocation);
  }
  
  public Script.FieldID getFieldID_Input() {
    return createFieldID(0, (Element)null);
  }
  
  public void forEach_bicubic(Allocation paramAllocation) {
    if (paramAllocation != this.mInput) {
      forEach_bicubic(paramAllocation, (Script.LaunchOptions)null);
      return;
    } 
    throw new RSIllegalArgumentException("Output cannot be same as Input.");
  }
  
  public void forEach_bicubic(Allocation paramAllocation, Script.LaunchOptions paramLaunchOptions) {
    forEach(0, (Allocation)null, paramAllocation, (FieldPacker)null, paramLaunchOptions);
  }
  
  public Script.KernelID getKernelID_bicubic() {
    return createKernelID(0, 2, (Element)null, (Element)null);
  }
}
