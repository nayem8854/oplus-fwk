package android.renderscript;

public class Byte2 {
  public byte x;
  
  public byte y;
  
  public Byte2() {}
  
  public Byte2(byte paramByte1, byte paramByte2) {
    this.x = paramByte1;
    this.y = paramByte2;
  }
  
  public Byte2(Byte2 paramByte2) {
    this.x = paramByte2.x;
    this.y = paramByte2.y;
  }
  
  public void add(Byte2 paramByte2) {
    this.x = (byte)(this.x + paramByte2.x);
    this.y = (byte)(this.y + paramByte2.y);
  }
  
  public static Byte2 add(Byte2 paramByte21, Byte2 paramByte22) {
    Byte2 byte2 = new Byte2();
    byte2.x = (byte)(paramByte21.x + paramByte22.x);
    byte2.y = (byte)(paramByte21.y + paramByte22.y);
    return byte2;
  }
  
  public void add(byte paramByte) {
    this.x = (byte)(this.x + paramByte);
    this.y = (byte)(this.y + paramByte);
  }
  
  public static Byte2 add(Byte2 paramByte2, byte paramByte) {
    Byte2 byte2 = new Byte2();
    byte2.x = (byte)(paramByte2.x + paramByte);
    byte2.y = (byte)(paramByte2.y + paramByte);
    return byte2;
  }
  
  public void sub(Byte2 paramByte2) {
    this.x = (byte)(this.x - paramByte2.x);
    this.y = (byte)(this.y - paramByte2.y);
  }
  
  public static Byte2 sub(Byte2 paramByte21, Byte2 paramByte22) {
    Byte2 byte2 = new Byte2();
    byte2.x = (byte)(paramByte21.x - paramByte22.x);
    byte2.y = (byte)(paramByte21.y - paramByte22.y);
    return byte2;
  }
  
  public void sub(byte paramByte) {
    this.x = (byte)(this.x - paramByte);
    this.y = (byte)(this.y - paramByte);
  }
  
  public static Byte2 sub(Byte2 paramByte2, byte paramByte) {
    Byte2 byte2 = new Byte2();
    byte2.x = (byte)(paramByte2.x - paramByte);
    byte2.y = (byte)(paramByte2.y - paramByte);
    return byte2;
  }
  
  public void mul(Byte2 paramByte2) {
    this.x = (byte)(this.x * paramByte2.x);
    this.y = (byte)(this.y * paramByte2.y);
  }
  
  public static Byte2 mul(Byte2 paramByte21, Byte2 paramByte22) {
    Byte2 byte2 = new Byte2();
    byte2.x = (byte)(paramByte21.x * paramByte22.x);
    byte2.y = (byte)(paramByte21.y * paramByte22.y);
    return byte2;
  }
  
  public void mul(byte paramByte) {
    this.x = (byte)(this.x * paramByte);
    this.y = (byte)(this.y * paramByte);
  }
  
  public static Byte2 mul(Byte2 paramByte2, byte paramByte) {
    Byte2 byte2 = new Byte2();
    byte2.x = (byte)(paramByte2.x * paramByte);
    byte2.y = (byte)(paramByte2.y * paramByte);
    return byte2;
  }
  
  public void div(Byte2 paramByte2) {
    this.x = (byte)(this.x / paramByte2.x);
    this.y = (byte)(this.y / paramByte2.y);
  }
  
  public static Byte2 div(Byte2 paramByte21, Byte2 paramByte22) {
    Byte2 byte2 = new Byte2();
    byte2.x = (byte)(paramByte21.x / paramByte22.x);
    byte2.y = (byte)(paramByte21.y / paramByte22.y);
    return byte2;
  }
  
  public void div(byte paramByte) {
    this.x = (byte)(this.x / paramByte);
    this.y = (byte)(this.y / paramByte);
  }
  
  public static Byte2 div(Byte2 paramByte2, byte paramByte) {
    Byte2 byte2 = new Byte2();
    byte2.x = (byte)(paramByte2.x / paramByte);
    byte2.y = (byte)(paramByte2.y / paramByte);
    return byte2;
  }
  
  public byte length() {
    return 2;
  }
  
  public void negate() {
    this.x = (byte)-this.x;
    this.y = (byte)-this.y;
  }
  
  public byte dotProduct(Byte2 paramByte2) {
    return (byte)(this.x * paramByte2.x + this.y * paramByte2.y);
  }
  
  public static byte dotProduct(Byte2 paramByte21, Byte2 paramByte22) {
    return (byte)(paramByte22.x * paramByte21.x + paramByte22.y * paramByte21.y);
  }
  
  public void addMultiple(Byte2 paramByte2, byte paramByte) {
    this.x = (byte)(this.x + paramByte2.x * paramByte);
    this.y = (byte)(this.y + paramByte2.y * paramByte);
  }
  
  public void set(Byte2 paramByte2) {
    this.x = paramByte2.x;
    this.y = paramByte2.y;
  }
  
  public void setValues(byte paramByte1, byte paramByte2) {
    this.x = paramByte1;
    this.y = paramByte2;
  }
  
  public byte elementSum() {
    return (byte)(this.x + this.y);
  }
  
  public byte get(int paramInt) {
    if (paramInt != 0) {
      if (paramInt == 1)
        return this.y; 
      throw new IndexOutOfBoundsException("Index: i");
    } 
    return this.x;
  }
  
  public void setAt(int paramInt, byte paramByte) {
    if (paramInt != 0) {
      if (paramInt == 1) {
        this.y = paramByte;
        return;
      } 
      throw new IndexOutOfBoundsException("Index: i");
    } 
    this.x = paramByte;
  }
  
  public void addAt(int paramInt, byte paramByte) {
    if (paramInt != 0) {
      if (paramInt == 1) {
        this.y = (byte)(this.y + paramByte);
        return;
      } 
      throw new IndexOutOfBoundsException("Index: i");
    } 
    this.x = (byte)(this.x + paramByte);
  }
  
  public void copyTo(byte[] paramArrayOfbyte, int paramInt) {
    paramArrayOfbyte[paramInt] = this.x;
    paramArrayOfbyte[paramInt + 1] = this.y;
  }
}
