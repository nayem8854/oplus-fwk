package android.renderscript;

public class Matrix2f {
  final float[] mMat;
  
  public Matrix2f() {
    this.mMat = new float[4];
    loadIdentity();
  }
  
  public Matrix2f(float[] paramArrayOffloat) {
    float[] arrayOfFloat = new float[4];
    System.arraycopy(paramArrayOffloat, 0, arrayOfFloat, 0, arrayOfFloat.length);
  }
  
  public float[] getArray() {
    return this.mMat;
  }
  
  public float get(int paramInt1, int paramInt2) {
    return this.mMat[paramInt1 * 2 + paramInt2];
  }
  
  public void set(int paramInt1, int paramInt2, float paramFloat) {
    this.mMat[paramInt1 * 2 + paramInt2] = paramFloat;
  }
  
  public void loadIdentity() {
    float[] arrayOfFloat = this.mMat;
    arrayOfFloat[0] = 1.0F;
    arrayOfFloat[1] = 0.0F;
    arrayOfFloat[2] = 0.0F;
    arrayOfFloat[3] = 1.0F;
  }
  
  public void load(Matrix2f paramMatrix2f) {
    float[] arrayOfFloat2 = paramMatrix2f.getArray(), arrayOfFloat1 = this.mMat;
    System.arraycopy(arrayOfFloat2, 0, arrayOfFloat1, 0, arrayOfFloat1.length);
  }
  
  public void loadRotate(float paramFloat) {
    float f = paramFloat * 0.017453292F;
    paramFloat = (float)Math.cos(f);
    f = (float)Math.sin(f);
    float[] arrayOfFloat = this.mMat;
    arrayOfFloat[0] = paramFloat;
    arrayOfFloat[1] = -f;
    arrayOfFloat[2] = f;
    arrayOfFloat[3] = paramFloat;
  }
  
  public void loadScale(float paramFloat1, float paramFloat2) {
    loadIdentity();
    float[] arrayOfFloat = this.mMat;
    arrayOfFloat[0] = paramFloat1;
    arrayOfFloat[3] = paramFloat2;
  }
  
  public void loadMultiply(Matrix2f paramMatrix2f1, Matrix2f paramMatrix2f2) {
    for (byte b = 0; b < 2; b++) {
      float f1 = 0.0F;
      float f2 = 0.0F;
      for (byte b1 = 0; b1 < 2; b1++) {
        float f = paramMatrix2f2.get(b, b1);
        f1 += paramMatrix2f1.get(b1, 0) * f;
        f2 += paramMatrix2f1.get(b1, 1) * f;
      } 
      set(b, 0, f1);
      set(b, 1, f2);
    } 
  }
  
  public void multiply(Matrix2f paramMatrix2f) {
    Matrix2f matrix2f = new Matrix2f();
    matrix2f.loadMultiply(this, paramMatrix2f);
    load(matrix2f);
  }
  
  public void rotate(float paramFloat) {
    Matrix2f matrix2f = new Matrix2f();
    matrix2f.loadRotate(paramFloat);
    multiply(matrix2f);
  }
  
  public void scale(float paramFloat1, float paramFloat2) {
    Matrix2f matrix2f = new Matrix2f();
    matrix2f.loadScale(paramFloat1, paramFloat2);
    multiply(matrix2f);
  }
  
  public void transpose() {
    float arrayOfFloat[] = this.mMat, f = arrayOfFloat[1];
    arrayOfFloat[1] = arrayOfFloat[2];
    arrayOfFloat[2] = f;
  }
}
