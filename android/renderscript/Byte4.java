package android.renderscript;

public class Byte4 {
  public byte w;
  
  public byte x;
  
  public byte y;
  
  public byte z;
  
  public Byte4() {}
  
  public Byte4(byte paramByte1, byte paramByte2, byte paramByte3, byte paramByte4) {
    this.x = paramByte1;
    this.y = paramByte2;
    this.z = paramByte3;
    this.w = paramByte4;
  }
  
  public Byte4(Byte4 paramByte4) {
    this.x = paramByte4.x;
    this.y = paramByte4.y;
    this.z = paramByte4.z;
    this.w = paramByte4.w;
  }
  
  public void add(Byte4 paramByte4) {
    this.x = (byte)(this.x + paramByte4.x);
    this.y = (byte)(this.y + paramByte4.y);
    this.z = (byte)(this.z + paramByte4.z);
    this.w = (byte)(this.w + paramByte4.w);
  }
  
  public static Byte4 add(Byte4 paramByte41, Byte4 paramByte42) {
    Byte4 byte4 = new Byte4();
    byte4.x = (byte)(paramByte41.x + paramByte42.x);
    byte4.y = (byte)(paramByte41.y + paramByte42.y);
    byte4.z = (byte)(paramByte41.z + paramByte42.z);
    byte4.w = (byte)(paramByte41.w + paramByte42.w);
    return byte4;
  }
  
  public void add(byte paramByte) {
    this.x = (byte)(this.x + paramByte);
    this.y = (byte)(this.y + paramByte);
    this.z = (byte)(this.z + paramByte);
    this.w = (byte)(this.w + paramByte);
  }
  
  public static Byte4 add(Byte4 paramByte4, byte paramByte) {
    Byte4 byte4 = new Byte4();
    byte4.x = (byte)(paramByte4.x + paramByte);
    byte4.y = (byte)(paramByte4.y + paramByte);
    byte4.z = (byte)(paramByte4.z + paramByte);
    byte4.w = (byte)(paramByte4.w + paramByte);
    return byte4;
  }
  
  public void sub(Byte4 paramByte4) {
    this.x = (byte)(this.x - paramByte4.x);
    this.y = (byte)(this.y - paramByte4.y);
    this.z = (byte)(this.z - paramByte4.z);
    this.w = (byte)(this.w - paramByte4.w);
  }
  
  public static Byte4 sub(Byte4 paramByte41, Byte4 paramByte42) {
    Byte4 byte4 = new Byte4();
    byte4.x = (byte)(paramByte41.x - paramByte42.x);
    byte4.y = (byte)(paramByte41.y - paramByte42.y);
    byte4.z = (byte)(paramByte41.z - paramByte42.z);
    byte4.w = (byte)(paramByte41.w - paramByte42.w);
    return byte4;
  }
  
  public void sub(byte paramByte) {
    this.x = (byte)(this.x - paramByte);
    this.y = (byte)(this.y - paramByte);
    this.z = (byte)(this.z - paramByte);
    this.w = (byte)(this.w - paramByte);
  }
  
  public static Byte4 sub(Byte4 paramByte4, byte paramByte) {
    Byte4 byte4 = new Byte4();
    byte4.x = (byte)(paramByte4.x - paramByte);
    byte4.y = (byte)(paramByte4.y - paramByte);
    byte4.z = (byte)(paramByte4.z - paramByte);
    byte4.w = (byte)(paramByte4.w - paramByte);
    return byte4;
  }
  
  public void mul(Byte4 paramByte4) {
    this.x = (byte)(this.x * paramByte4.x);
    this.y = (byte)(this.y * paramByte4.y);
    this.z = (byte)(this.z * paramByte4.z);
    this.w = (byte)(this.w * paramByte4.w);
  }
  
  public static Byte4 mul(Byte4 paramByte41, Byte4 paramByte42) {
    Byte4 byte4 = new Byte4();
    byte4.x = (byte)(paramByte41.x * paramByte42.x);
    byte4.y = (byte)(paramByte41.y * paramByte42.y);
    byte4.z = (byte)(paramByte41.z * paramByte42.z);
    byte4.w = (byte)(paramByte41.w * paramByte42.w);
    return byte4;
  }
  
  public void mul(byte paramByte) {
    this.x = (byte)(this.x * paramByte);
    this.y = (byte)(this.y * paramByte);
    this.z = (byte)(this.z * paramByte);
    this.w = (byte)(this.w * paramByte);
  }
  
  public static Byte4 mul(Byte4 paramByte4, byte paramByte) {
    Byte4 byte4 = new Byte4();
    byte4.x = (byte)(paramByte4.x * paramByte);
    byte4.y = (byte)(paramByte4.y * paramByte);
    byte4.z = (byte)(paramByte4.z * paramByte);
    byte4.w = (byte)(paramByte4.w * paramByte);
    return byte4;
  }
  
  public void div(Byte4 paramByte4) {
    this.x = (byte)(this.x / paramByte4.x);
    this.y = (byte)(this.y / paramByte4.y);
    this.z = (byte)(this.z / paramByte4.z);
    this.w = (byte)(this.w / paramByte4.w);
  }
  
  public static Byte4 div(Byte4 paramByte41, Byte4 paramByte42) {
    Byte4 byte4 = new Byte4();
    byte4.x = (byte)(paramByte41.x / paramByte42.x);
    byte4.y = (byte)(paramByte41.y / paramByte42.y);
    byte4.z = (byte)(paramByte41.z / paramByte42.z);
    byte4.w = (byte)(paramByte41.w / paramByte42.w);
    return byte4;
  }
  
  public void div(byte paramByte) {
    this.x = (byte)(this.x / paramByte);
    this.y = (byte)(this.y / paramByte);
    this.z = (byte)(this.z / paramByte);
    this.w = (byte)(this.w / paramByte);
  }
  
  public static Byte4 div(Byte4 paramByte4, byte paramByte) {
    Byte4 byte4 = new Byte4();
    byte4.x = (byte)(paramByte4.x / paramByte);
    byte4.y = (byte)(paramByte4.y / paramByte);
    byte4.z = (byte)(paramByte4.z / paramByte);
    byte4.w = (byte)(paramByte4.w / paramByte);
    return byte4;
  }
  
  public byte length() {
    return 4;
  }
  
  public void negate() {
    this.x = (byte)-this.x;
    this.y = (byte)-this.y;
    this.z = (byte)-this.z;
    this.w = (byte)-this.w;
  }
  
  public byte dotProduct(Byte4 paramByte4) {
    return (byte)(this.x * paramByte4.x + this.y * paramByte4.y + this.z * paramByte4.z + this.w * paramByte4.w);
  }
  
  public static byte dotProduct(Byte4 paramByte41, Byte4 paramByte42) {
    return (byte)(paramByte42.x * paramByte41.x + paramByte42.y * paramByte41.y + paramByte42.z * paramByte41.z + paramByte42.w * paramByte41.w);
  }
  
  public void addMultiple(Byte4 paramByte4, byte paramByte) {
    this.x = (byte)(this.x + paramByte4.x * paramByte);
    this.y = (byte)(this.y + paramByte4.y * paramByte);
    this.z = (byte)(this.z + paramByte4.z * paramByte);
    this.w = (byte)(this.w + paramByte4.w * paramByte);
  }
  
  public void set(Byte4 paramByte4) {
    this.x = paramByte4.x;
    this.y = paramByte4.y;
    this.z = paramByte4.z;
    this.w = paramByte4.w;
  }
  
  public void setValues(byte paramByte1, byte paramByte2, byte paramByte3, byte paramByte4) {
    this.x = paramByte1;
    this.y = paramByte2;
    this.z = paramByte3;
    this.w = paramByte4;
  }
  
  public byte elementSum() {
    return (byte)(this.x + this.y + this.z + this.w);
  }
  
  public byte get(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt == 3)
            return this.w; 
          throw new IndexOutOfBoundsException("Index: i");
        } 
        return this.z;
      } 
      return this.y;
    } 
    return this.x;
  }
  
  public void setAt(int paramInt, byte paramByte) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt == 3) {
            this.w = paramByte;
            return;
          } 
          throw new IndexOutOfBoundsException("Index: i");
        } 
        this.z = paramByte;
        return;
      } 
      this.y = paramByte;
      return;
    } 
    this.x = paramByte;
  }
  
  public void addAt(int paramInt, byte paramByte) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt == 3) {
            this.w = (byte)(this.w + paramByte);
            return;
          } 
          throw new IndexOutOfBoundsException("Index: i");
        } 
        this.z = (byte)(this.z + paramByte);
        return;
      } 
      this.y = (byte)(this.y + paramByte);
      return;
    } 
    this.x = (byte)(this.x + paramByte);
  }
  
  public void copyTo(byte[] paramArrayOfbyte, int paramInt) {
    paramArrayOfbyte[paramInt] = this.x;
    paramArrayOfbyte[paramInt + 1] = this.y;
    paramArrayOfbyte[paramInt + 2] = this.z;
    paramArrayOfbyte[paramInt + 3] = this.w;
  }
}
