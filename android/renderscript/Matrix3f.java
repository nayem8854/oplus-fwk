package android.renderscript;

public class Matrix3f {
  final float[] mMat;
  
  public Matrix3f() {
    this.mMat = new float[9];
    loadIdentity();
  }
  
  public Matrix3f(float[] paramArrayOffloat) {
    float[] arrayOfFloat = new float[9];
    System.arraycopy(paramArrayOffloat, 0, arrayOfFloat, 0, arrayOfFloat.length);
  }
  
  public float[] getArray() {
    return this.mMat;
  }
  
  public float get(int paramInt1, int paramInt2) {
    return this.mMat[paramInt1 * 3 + paramInt2];
  }
  
  public void set(int paramInt1, int paramInt2, float paramFloat) {
    this.mMat[paramInt1 * 3 + paramInt2] = paramFloat;
  }
  
  public void loadIdentity() {
    float[] arrayOfFloat = this.mMat;
    arrayOfFloat[0] = 1.0F;
    arrayOfFloat[1] = 0.0F;
    arrayOfFloat[2] = 0.0F;
    arrayOfFloat[3] = 0.0F;
    arrayOfFloat[4] = 1.0F;
    arrayOfFloat[5] = 0.0F;
    arrayOfFloat[6] = 0.0F;
    arrayOfFloat[7] = 0.0F;
    arrayOfFloat[8] = 1.0F;
  }
  
  public void load(Matrix3f paramMatrix3f) {
    float[] arrayOfFloat1 = paramMatrix3f.getArray(), arrayOfFloat2 = this.mMat;
    System.arraycopy(arrayOfFloat1, 0, arrayOfFloat2, 0, arrayOfFloat2.length);
  }
  
  public void loadRotate(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    paramFloat1 = 0.017453292F * paramFloat1;
    float f1 = (float)Math.cos(paramFloat1);
    float f2 = (float)Math.sin(paramFloat1);
    paramFloat1 = (float)Math.sqrt((paramFloat2 * paramFloat2 + paramFloat3 * paramFloat3 + paramFloat4 * paramFloat4));
    if (paramFloat1 == 1.0F) {
      float f = 1.0F / paramFloat1;
      paramFloat1 = paramFloat2 * f;
      paramFloat3 *= f;
      paramFloat2 = paramFloat4 * f;
    } else {
      paramFloat1 = paramFloat2;
      paramFloat2 = paramFloat4;
    } 
    float f4 = 1.0F - f1;
    float f3 = paramFloat1 * paramFloat3;
    float f5 = paramFloat3 * paramFloat2;
    paramFloat4 = paramFloat2 * paramFloat1;
    float f6 = paramFloat1 * f2;
    float f7 = paramFloat3 * f2;
    f2 = paramFloat2 * f2;
    float[] arrayOfFloat = this.mMat;
    arrayOfFloat[0] = paramFloat1 * paramFloat1 * f4 + f1;
    arrayOfFloat[3] = f3 * f4 - f2;
    arrayOfFloat[6] = paramFloat4 * f4 + f7;
    arrayOfFloat[1] = f3 * f4 + f2;
    arrayOfFloat[4] = paramFloat3 * paramFloat3 * f4 + f1;
    arrayOfFloat[7] = f5 * f4 - f6;
    arrayOfFloat[2] = paramFloat4 * f4 - f7;
    arrayOfFloat[5] = f5 * f4 + f6;
    arrayOfFloat[8] = paramFloat2 * paramFloat2 * f4 + f1;
  }
  
  public void loadRotate(float paramFloat) {
    loadIdentity();
    float f = paramFloat * 0.017453292F;
    paramFloat = (float)Math.cos(f);
    f = (float)Math.sin(f);
    float[] arrayOfFloat = this.mMat;
    arrayOfFloat[0] = paramFloat;
    arrayOfFloat[1] = -f;
    arrayOfFloat[3] = f;
    arrayOfFloat[4] = paramFloat;
  }
  
  public void loadScale(float paramFloat1, float paramFloat2) {
    loadIdentity();
    float[] arrayOfFloat = this.mMat;
    arrayOfFloat[0] = paramFloat1;
    arrayOfFloat[4] = paramFloat2;
  }
  
  public void loadScale(float paramFloat1, float paramFloat2, float paramFloat3) {
    loadIdentity();
    float[] arrayOfFloat = this.mMat;
    arrayOfFloat[0] = paramFloat1;
    arrayOfFloat[4] = paramFloat2;
    arrayOfFloat[8] = paramFloat3;
  }
  
  public void loadTranslate(float paramFloat1, float paramFloat2) {
    loadIdentity();
    float[] arrayOfFloat = this.mMat;
    arrayOfFloat[6] = paramFloat1;
    arrayOfFloat[7] = paramFloat2;
  }
  
  public void loadMultiply(Matrix3f paramMatrix3f1, Matrix3f paramMatrix3f2) {
    for (byte b = 0; b < 3; b++) {
      float f1 = 0.0F;
      float f2 = 0.0F;
      float f3 = 0.0F;
      for (byte b1 = 0; b1 < 3; b1++) {
        float f = paramMatrix3f2.get(b, b1);
        f1 += paramMatrix3f1.get(b1, 0) * f;
        f2 += paramMatrix3f1.get(b1, 1) * f;
        f3 += paramMatrix3f1.get(b1, 2) * f;
      } 
      set(b, 0, f1);
      set(b, 1, f2);
      set(b, 2, f3);
    } 
  }
  
  public void multiply(Matrix3f paramMatrix3f) {
    Matrix3f matrix3f = new Matrix3f();
    matrix3f.loadMultiply(this, paramMatrix3f);
    load(matrix3f);
  }
  
  public void rotate(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    Matrix3f matrix3f = new Matrix3f();
    matrix3f.loadRotate(paramFloat1, paramFloat2, paramFloat3, paramFloat4);
    multiply(matrix3f);
  }
  
  public void rotate(float paramFloat) {
    Matrix3f matrix3f = new Matrix3f();
    matrix3f.loadRotate(paramFloat);
    multiply(matrix3f);
  }
  
  public void scale(float paramFloat1, float paramFloat2) {
    Matrix3f matrix3f = new Matrix3f();
    matrix3f.loadScale(paramFloat1, paramFloat2);
    multiply(matrix3f);
  }
  
  public void scale(float paramFloat1, float paramFloat2, float paramFloat3) {
    Matrix3f matrix3f = new Matrix3f();
    matrix3f.loadScale(paramFloat1, paramFloat2, paramFloat3);
    multiply(matrix3f);
  }
  
  public void translate(float paramFloat1, float paramFloat2) {
    Matrix3f matrix3f = new Matrix3f();
    matrix3f.loadTranslate(paramFloat1, paramFloat2);
    multiply(matrix3f);
  }
  
  public void transpose() {
    for (byte b = 0; b < 2; b++) {
      for (int i = b + 1; i < 3; i++) {
        float arrayOfFloat[] = this.mMat, f = arrayOfFloat[b * 3 + i];
        arrayOfFloat[b * 3 + i] = arrayOfFloat[i * 3 + b];
        arrayOfFloat[i * 3 + b] = f;
      } 
    } 
  }
}
