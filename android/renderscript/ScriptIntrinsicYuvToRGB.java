package android.renderscript;

public final class ScriptIntrinsicYuvToRGB extends ScriptIntrinsic {
  private Allocation mInput;
  
  ScriptIntrinsicYuvToRGB(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
  }
  
  public static ScriptIntrinsicYuvToRGB create(RenderScript paramRenderScript, Element paramElement) {
    long l = paramRenderScript.nScriptIntrinsicCreate(6, paramElement.getID(paramRenderScript));
    return new ScriptIntrinsicYuvToRGB(l, paramRenderScript);
  }
  
  public void setInput(Allocation paramAllocation) {
    this.mInput = paramAllocation;
    setVar(0, paramAllocation);
  }
  
  public void forEach(Allocation paramAllocation) {
    forEach(0, (Allocation)null, paramAllocation, (FieldPacker)null);
  }
  
  public Script.KernelID getKernelID() {
    return createKernelID(0, 2, null, null);
  }
  
  public Script.FieldID getFieldID_Input() {
    return createFieldID(0, null);
  }
}
