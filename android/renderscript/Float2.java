package android.renderscript;

public class Float2 {
  public float x;
  
  public float y;
  
  public Float2() {}
  
  public Float2(Float2 paramFloat2) {
    this.x = paramFloat2.x;
    this.y = paramFloat2.y;
  }
  
  public Float2(float paramFloat1, float paramFloat2) {
    this.x = paramFloat1;
    this.y = paramFloat2;
  }
  
  public static Float2 add(Float2 paramFloat21, Float2 paramFloat22) {
    Float2 float2 = new Float2();
    paramFloat21.x += paramFloat22.x;
    paramFloat21.y += paramFloat22.y;
    return float2;
  }
  
  public void add(Float2 paramFloat2) {
    this.x += paramFloat2.x;
    this.y += paramFloat2.y;
  }
  
  public void add(float paramFloat) {
    this.x += paramFloat;
    this.y += paramFloat;
  }
  
  public static Float2 add(Float2 paramFloat2, float paramFloat) {
    Float2 float2 = new Float2();
    paramFloat2.x += paramFloat;
    paramFloat2.y += paramFloat;
    return float2;
  }
  
  public void sub(Float2 paramFloat2) {
    this.x -= paramFloat2.x;
    this.y -= paramFloat2.y;
  }
  
  public static Float2 sub(Float2 paramFloat21, Float2 paramFloat22) {
    Float2 float2 = new Float2();
    paramFloat21.x -= paramFloat22.x;
    paramFloat21.y -= paramFloat22.y;
    return float2;
  }
  
  public void sub(float paramFloat) {
    this.x -= paramFloat;
    this.y -= paramFloat;
  }
  
  public static Float2 sub(Float2 paramFloat2, float paramFloat) {
    Float2 float2 = new Float2();
    paramFloat2.x -= paramFloat;
    paramFloat2.y -= paramFloat;
    return float2;
  }
  
  public void mul(Float2 paramFloat2) {
    this.x *= paramFloat2.x;
    this.y *= paramFloat2.y;
  }
  
  public static Float2 mul(Float2 paramFloat21, Float2 paramFloat22) {
    Float2 float2 = new Float2();
    paramFloat21.x *= paramFloat22.x;
    paramFloat21.y *= paramFloat22.y;
    return float2;
  }
  
  public void mul(float paramFloat) {
    this.x *= paramFloat;
    this.y *= paramFloat;
  }
  
  public static Float2 mul(Float2 paramFloat2, float paramFloat) {
    Float2 float2 = new Float2();
    paramFloat2.x *= paramFloat;
    paramFloat2.y *= paramFloat;
    return float2;
  }
  
  public void div(Float2 paramFloat2) {
    this.x /= paramFloat2.x;
    this.y /= paramFloat2.y;
  }
  
  public static Float2 div(Float2 paramFloat21, Float2 paramFloat22) {
    Float2 float2 = new Float2();
    paramFloat21.x /= paramFloat22.x;
    paramFloat21.y /= paramFloat22.y;
    return float2;
  }
  
  public void div(float paramFloat) {
    this.x /= paramFloat;
    this.y /= paramFloat;
  }
  
  public static Float2 div(Float2 paramFloat2, float paramFloat) {
    Float2 float2 = new Float2();
    paramFloat2.x /= paramFloat;
    paramFloat2.y /= paramFloat;
    return float2;
  }
  
  public float dotProduct(Float2 paramFloat2) {
    return this.x * paramFloat2.x + this.y * paramFloat2.y;
  }
  
  public static float dotProduct(Float2 paramFloat21, Float2 paramFloat22) {
    return paramFloat22.x * paramFloat21.x + paramFloat22.y * paramFloat21.y;
  }
  
  public void addMultiple(Float2 paramFloat2, float paramFloat) {
    this.x += paramFloat2.x * paramFloat;
    this.y += paramFloat2.y * paramFloat;
  }
  
  public void set(Float2 paramFloat2) {
    this.x = paramFloat2.x;
    this.y = paramFloat2.y;
  }
  
  public void negate() {
    this.x = -this.x;
    this.y = -this.y;
  }
  
  public int length() {
    return 2;
  }
  
  public float elementSum() {
    return this.x + this.y;
  }
  
  public float get(int paramInt) {
    if (paramInt != 0) {
      if (paramInt == 1)
        return this.y; 
      throw new IndexOutOfBoundsException("Index: i");
    } 
    return this.x;
  }
  
  public void setAt(int paramInt, float paramFloat) {
    if (paramInt != 0) {
      if (paramInt == 1) {
        this.y = paramFloat;
        return;
      } 
      throw new IndexOutOfBoundsException("Index: i");
    } 
    this.x = paramFloat;
  }
  
  public void addAt(int paramInt, float paramFloat) {
    if (paramInt != 0) {
      if (paramInt == 1) {
        this.y += paramFloat;
        return;
      } 
      throw new IndexOutOfBoundsException("Index: i");
    } 
    this.x += paramFloat;
  }
  
  public void setValues(float paramFloat1, float paramFloat2) {
    this.x = paramFloat1;
    this.y = paramFloat2;
  }
  
  public void copyTo(float[] paramArrayOffloat, int paramInt) {
    paramArrayOffloat[paramInt] = this.x;
    paramArrayOffloat[paramInt + 1] = this.y;
  }
}
