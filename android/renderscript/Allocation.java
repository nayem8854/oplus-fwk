package android.renderscript;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Trace;
import android.util.Log;
import android.view.Surface;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.util.HashMap;

public class Allocation extends BaseObj {
  boolean mOwningType = false;
  
  long mTimeStamp = -1L;
  
  boolean mReadAllowed = true;
  
  boolean mWriteAllowed = true;
  
  boolean mAutoPadding = false;
  
  Type.CubemapFace mSelectedFace = Type.CubemapFace.POSITIVE_X;
  
  static HashMap<Long, Allocation> mAllocationMap = new HashMap<>();
  
  private Surface mGetSurfaceSurface = null;
  
  private ByteBuffer mByteBuffer = null;
  
  private long mByteBufferStride = -1L;
  
  private static final int MAX_NUMBER_IO_INPUT_ALLOC = 16;
  
  public static final int USAGE_GRAPHICS_CONSTANTS = 8;
  
  public static final int USAGE_GRAPHICS_RENDER_TARGET = 16;
  
  public static final int USAGE_GRAPHICS_TEXTURE = 2;
  
  public static final int USAGE_GRAPHICS_VERTEX = 4;
  
  public static final int USAGE_IO_INPUT = 32;
  
  public static final int USAGE_IO_OUTPUT = 64;
  
  public static final int USAGE_SCRIPT = 1;
  
  public static final int USAGE_SHARED = 128;
  
  static BitmapFactory.Options mBitmapOptions;
  
  Allocation mAdaptedAllocation;
  
  Bitmap mBitmap;
  
  OnBufferAvailableListener mBufferNotifier;
  
  int mCurrentCount;
  
  int mCurrentDimX;
  
  int mCurrentDimY;
  
  int mCurrentDimZ;
  
  MipmapControl mMipmapControl;
  
  int[] mSelectedArray;
  
  int mSelectedLOD;
  
  int mSelectedX;
  
  int mSelectedY;
  
  int mSelectedZ;
  
  int mSize;
  
  Type mType;
  
  int mUsage;
  
  private Element.DataType validateObjectIsPrimitiveArray(Object<?> paramObject, boolean paramBoolean) {
    paramObject = (Object<?>)paramObject.getClass();
    if (paramObject.isArray()) {
      Class<?> clazz = paramObject.getComponentType();
      if (clazz.isPrimitive()) {
        if (clazz == long.class) {
          if (paramBoolean) {
            validateIsInt64();
            return this.mType.mElement.mType;
          } 
          return Element.DataType.SIGNED_64;
        } 
        if (clazz == int.class) {
          if (paramBoolean) {
            validateIsInt32();
            return this.mType.mElement.mType;
          } 
          return Element.DataType.SIGNED_32;
        } 
        if (clazz == short.class) {
          if (paramBoolean) {
            validateIsInt16OrFloat16();
            return this.mType.mElement.mType;
          } 
          return Element.DataType.SIGNED_16;
        } 
        if (clazz == byte.class) {
          if (paramBoolean) {
            validateIsInt8();
            return this.mType.mElement.mType;
          } 
          return Element.DataType.SIGNED_8;
        } 
        if (clazz == float.class) {
          if (paramBoolean)
            validateIsFloat32(); 
          return Element.DataType.FLOAT_32;
        } 
        if (clazz == double.class) {
          if (paramBoolean)
            validateIsFloat64(); 
          return Element.DataType.FLOAT_64;
        } 
        paramObject = (Object<?>)new StringBuilder();
        paramObject.append("Parameter of type ");
        paramObject.append(clazz.getSimpleName());
        paramObject.append("[] is not compatible with data type ");
        Element.DataType dataType = this.mType.mElement.mType;
        paramObject.append(dataType.name());
        paramObject.append(" of allocation");
        throw new RSIllegalArgumentException(paramObject.toString());
      } 
      throw new RSIllegalArgumentException("Object passed is not an Array of primitives.");
    } 
    throw new RSIllegalArgumentException("Object passed is not an array of primitives.");
  }
  
  class MipmapControl extends Enum<MipmapControl> {
    private static final MipmapControl[] $VALUES;
    
    public static final MipmapControl MIPMAP_FULL = new MipmapControl("MIPMAP_FULL", 1, 1);
    
    public static MipmapControl valueOf(String param1String) {
      return Enum.<MipmapControl>valueOf(MipmapControl.class, param1String);
    }
    
    public static MipmapControl[] values() {
      return (MipmapControl[])$VALUES.clone();
    }
    
    public static final MipmapControl MIPMAP_NONE = new MipmapControl("MIPMAP_NONE", 0, 0);
    
    public static final MipmapControl MIPMAP_ON_SYNC_TO_TEXTURE;
    
    int mID;
    
    static {
      MipmapControl mipmapControl = new MipmapControl("MIPMAP_ON_SYNC_TO_TEXTURE", 2, 2);
      $VALUES = new MipmapControl[] { MIPMAP_NONE, MIPMAP_FULL, mipmapControl };
    }
    
    private MipmapControl(Allocation this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.mID = param1Int2;
    }
  }
  
  private long getIDSafe() {
    Allocation allocation = this.mAdaptedAllocation;
    if (allocation != null)
      return allocation.getID(this.mRS); 
    return getID(this.mRS);
  }
  
  public Element getElement() {
    return this.mType.getElement();
  }
  
  public int getUsage() {
    return this.mUsage;
  }
  
  public MipmapControl getMipmap() {
    return this.mMipmapControl;
  }
  
  public void setAutoPadding(boolean paramBoolean) {
    this.mAutoPadding = paramBoolean;
  }
  
  public int getBytesSize() {
    if (this.mType.mDimYuv != 0)
      return (int)Math.ceil((this.mType.getCount() * this.mType.getElement().getBytesSize()) * 1.5D); 
    return this.mType.getCount() * this.mType.getElement().getBytesSize();
  }
  
  private void updateCacheInfo(Type paramType) {
    this.mCurrentDimX = paramType.getX();
    this.mCurrentDimY = paramType.getY();
    this.mCurrentDimZ = paramType.getZ();
    int i = this.mCurrentDimX;
    int j = this.mCurrentDimY;
    if (j > 1)
      this.mCurrentCount = i * j; 
    j = this.mCurrentDimZ;
    if (j > 1)
      this.mCurrentCount *= j; 
  }
  
  private void setBitmap(Bitmap paramBitmap) {
    this.mBitmap = paramBitmap;
  }
  
  Allocation(long paramLong, RenderScript paramRenderScript, Type paramType, int paramInt) {
    super(paramLong, paramRenderScript);
    if ((paramInt & 0xFFFFFF00) == 0) {
      if ((paramInt & 0x20) != 0) {
        this.mWriteAllowed = false;
        if ((paramInt & 0xFFFFFFDC) != 0)
          throw new RSIllegalArgumentException("Invalid usage combination."); 
      } 
      this.mType = paramType;
      this.mUsage = paramInt;
      if (paramType != null) {
        this.mSize = paramType.getCount() * this.mType.getElement().getBytesSize();
        updateCacheInfo(paramType);
      } 
      try {
        RenderScript.registerNativeAllocation.invoke(RenderScript.sRuntime, new Object[] { Integer.valueOf(this.mSize) });
        this.guard.open("destroy");
        return;
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Couldn't invoke registerNativeAllocation:");
        stringBuilder.append(exception);
        Log.e("RenderScript_jni", stringBuilder.toString());
        stringBuilder = new StringBuilder();
        stringBuilder.append("Couldn't invoke registerNativeAllocation:");
        stringBuilder.append(exception);
        throw new RSRuntimeException(stringBuilder.toString());
      } 
    } 
    throw new RSIllegalArgumentException("Unknown usage specified.");
  }
  
  Allocation(long paramLong, RenderScript paramRenderScript, Type paramType, boolean paramBoolean, int paramInt, MipmapControl paramMipmapControl) {
    this(paramLong, paramRenderScript, paramType, paramInt);
    this.mOwningType = paramBoolean;
    this.mMipmapControl = paramMipmapControl;
  }
  
  protected void finalize() throws Throwable {
    RenderScript.registerNativeFree.invoke(RenderScript.sRuntime, new Object[] { Integer.valueOf(this.mSize) });
    super.finalize();
  }
  
  private void validateIsInt64() {
    if (this.mType.mElement.mType == Element.DataType.SIGNED_64 || this.mType.mElement.mType == Element.DataType.UNSIGNED_64)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("64 bit integer source does not match allocation type ");
    stringBuilder.append(this.mType.mElement.mType);
    throw new RSIllegalArgumentException(stringBuilder.toString());
  }
  
  private void validateIsInt32() {
    if (this.mType.mElement.mType == Element.DataType.SIGNED_32 || this.mType.mElement.mType == Element.DataType.UNSIGNED_32)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("32 bit integer source does not match allocation type ");
    stringBuilder.append(this.mType.mElement.mType);
    throw new RSIllegalArgumentException(stringBuilder.toString());
  }
  
  private void validateIsInt16OrFloat16() {
    if (this.mType.mElement.mType == Element.DataType.SIGNED_16 || this.mType.mElement.mType == Element.DataType.UNSIGNED_16 || this.mType.mElement.mType == Element.DataType.FLOAT_16)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("16 bit integer source does not match allocation type ");
    stringBuilder.append(this.mType.mElement.mType);
    throw new RSIllegalArgumentException(stringBuilder.toString());
  }
  
  private void validateIsInt8() {
    if (this.mType.mElement.mType == Element.DataType.SIGNED_8 || this.mType.mElement.mType == Element.DataType.UNSIGNED_8)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("8 bit integer source does not match allocation type ");
    stringBuilder.append(this.mType.mElement.mType);
    throw new RSIllegalArgumentException(stringBuilder.toString());
  }
  
  private void validateIsFloat32() {
    if (this.mType.mElement.mType == Element.DataType.FLOAT_32)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("32 bit float source does not match allocation type ");
    stringBuilder.append(this.mType.mElement.mType);
    throw new RSIllegalArgumentException(stringBuilder.toString());
  }
  
  private void validateIsFloat64() {
    if (this.mType.mElement.mType == Element.DataType.FLOAT_64)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("64 bit float source does not match allocation type ");
    stringBuilder.append(this.mType.mElement.mType);
    throw new RSIllegalArgumentException(stringBuilder.toString());
  }
  
  private void validateIsObject() {
    if (this.mType.mElement.mType == Element.DataType.RS_ELEMENT || this.mType.mElement.mType == Element.DataType.RS_TYPE || this.mType.mElement.mType == Element.DataType.RS_ALLOCATION || this.mType.mElement.mType == Element.DataType.RS_SAMPLER || this.mType.mElement.mType == Element.DataType.RS_SCRIPT || this.mType.mElement.mType == Element.DataType.RS_MESH || this.mType.mElement.mType == Element.DataType.RS_PROGRAM_FRAGMENT || this.mType.mElement.mType == Element.DataType.RS_PROGRAM_VERTEX || this.mType.mElement.mType == Element.DataType.RS_PROGRAM_RASTER || this.mType.mElement.mType == Element.DataType.RS_PROGRAM_STORE)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Object source does not match allocation type ");
    stringBuilder.append(this.mType.mElement.mType);
    throw new RSIllegalArgumentException(stringBuilder.toString());
  }
  
  void updateFromNative() {
    super.updateFromNative();
    long l = this.mRS.nAllocationGetType(getID(this.mRS));
    if (l != 0L) {
      Type type = new Type(l, this.mRS);
      type.updateFromNative();
      updateCacheInfo(this.mType);
    } 
  }
  
  public Type getType() {
    return this.mType;
  }
  
  public void syncAll(int paramInt) {
    try {
      Trace.traceBegin(32768L, "syncAll");
      if (paramInt != 1 && paramInt != 2) {
        if (paramInt != 4 && paramInt != 8)
          if (paramInt == 128) {
            if ((this.mUsage & 0x80) != 0)
              copyTo(this.mBitmap); 
          } else {
            RSIllegalArgumentException rSIllegalArgumentException = new RSIllegalArgumentException();
            this("Source must be exactly one usage type.");
            throw rSIllegalArgumentException;
          }  
      } else if ((this.mUsage & 0x80) != 0) {
        copyFrom(this.mBitmap);
      } 
      this.mRS.validate();
      this.mRS.nAllocationSyncAll(getIDSafe(), paramInt);
      return;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  public void ioSend() {
    try {
      Trace.traceBegin(32768L, "ioSend");
      if ((this.mUsage & 0x40) != 0) {
        this.mRS.validate();
        this.mRS.nAllocationIoSend(getID(this.mRS));
        return;
      } 
      RSIllegalArgumentException rSIllegalArgumentException = new RSIllegalArgumentException();
      this("Can only send buffer if IO_OUTPUT usage specified.");
      throw rSIllegalArgumentException;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  public void ioReceive() {
    try {
      Trace.traceBegin(32768L, "ioReceive");
      if ((this.mUsage & 0x20) != 0) {
        this.mRS.validate();
        this.mTimeStamp = this.mRS.nAllocationIoReceive(getID(this.mRS));
        return;
      } 
      RSIllegalArgumentException rSIllegalArgumentException = new RSIllegalArgumentException();
      this("Can only receive if IO_INPUT usage specified.");
      throw rSIllegalArgumentException;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  public void copyFrom(BaseObj[] paramArrayOfBaseObj) {
    try {
      Trace.traceBegin(32768L, "copyFrom");
      this.mRS.validate();
      validateIsObject();
      if (paramArrayOfBaseObj.length == this.mCurrentCount) {
        if (RenderScript.sPointerSize == 8) {
          long[] arrayOfLong = new long[paramArrayOfBaseObj.length * 4];
          for (byte b = 0; b < paramArrayOfBaseObj.length; b++)
            arrayOfLong[b * 4] = paramArrayOfBaseObj[b].getID(this.mRS); 
          copy1DRangeFromUnchecked(0, this.mCurrentCount, arrayOfLong);
        } else {
          int[] arrayOfInt = new int[paramArrayOfBaseObj.length];
          for (byte b = 0; b < paramArrayOfBaseObj.length; b++)
            arrayOfInt[b] = (int)paramArrayOfBaseObj[b].getID(this.mRS); 
          copy1DRangeFromUnchecked(0, this.mCurrentCount, arrayOfInt);
        } 
        return;
      } 
      RSIllegalArgumentException rSIllegalArgumentException = new RSIllegalArgumentException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Array size mismatch, allocation sizeX = ");
      stringBuilder.append(this.mCurrentCount);
      stringBuilder.append(", array length = ");
      stringBuilder.append(paramArrayOfBaseObj.length);
      this(stringBuilder.toString());
      throw rSIllegalArgumentException;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  private void validateBitmapFormat(Bitmap paramBitmap) {
    Bitmap.Config config = paramBitmap.getConfig();
    if (config != null) {
      int i = null.$SwitchMap$android$graphics$Bitmap$Config[config.ordinal()];
      if (i != 1) {
        if (i != 2) {
          if (i != 3) {
            if (i == 4) {
              if ((this.mType.getElement()).mKind == Element.DataKind.PIXEL_RGBA) {
                Type type1 = this.mType;
                if (type1.getElement().getBytesSize() == 2)
                  return; 
              } 
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Allocation kind is ");
              Type type = this.mType;
              stringBuilder.append((type.getElement()).mKind);
              stringBuilder.append(", type ");
              type = this.mType;
              stringBuilder.append((type.getElement()).mType);
              stringBuilder.append(" of ");
              type = this.mType;
              stringBuilder.append(type.getElement().getBytesSize());
              stringBuilder.append(" bytes, passed bitmap was ");
              stringBuilder.append(config);
              throw new RSIllegalArgumentException(stringBuilder.toString());
            } 
          } else {
            if ((this.mType.getElement()).mKind == Element.DataKind.PIXEL_RGB) {
              Type type1 = this.mType;
              if (type1.getElement().getBytesSize() == 2)
                return; 
            } 
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Allocation kind is ");
            Type type = this.mType;
            stringBuilder.append((type.getElement()).mKind);
            stringBuilder.append(", type ");
            type = this.mType;
            stringBuilder.append((type.getElement()).mType);
            stringBuilder.append(" of ");
            type = this.mType;
            stringBuilder.append(type.getElement().getBytesSize());
            stringBuilder.append(" bytes, passed bitmap was ");
            stringBuilder.append(config);
            throw new RSIllegalArgumentException(stringBuilder.toString());
          } 
        } else {
          if ((this.mType.getElement()).mKind == Element.DataKind.PIXEL_RGBA) {
            Type type1 = this.mType;
            if (type1.getElement().getBytesSize() == 4)
              return; 
          } 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Allocation kind is ");
          Type type = this.mType;
          stringBuilder.append((type.getElement()).mKind);
          stringBuilder.append(", type ");
          type = this.mType;
          stringBuilder.append((type.getElement()).mType);
          stringBuilder.append(" of ");
          type = this.mType;
          stringBuilder.append(type.getElement().getBytesSize());
          stringBuilder.append(" bytes, passed bitmap was ");
          stringBuilder.append(config);
          throw new RSIllegalArgumentException(stringBuilder.toString());
        } 
      } else if ((this.mType.getElement()).mKind != Element.DataKind.PIXEL_A) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Allocation kind is ");
        Type type = this.mType;
        stringBuilder.append((type.getElement()).mKind);
        stringBuilder.append(", type ");
        type = this.mType;
        stringBuilder.append((type.getElement()).mType);
        stringBuilder.append(" of ");
        type = this.mType;
        stringBuilder.append(type.getElement().getBytesSize());
        stringBuilder.append(" bytes, passed bitmap was ");
        stringBuilder.append(config);
        throw new RSIllegalArgumentException(stringBuilder.toString());
      } 
      return;
    } 
    throw new RSIllegalArgumentException("Bitmap has an unsupported format for this operation");
  }
  
  private void validateBitmapSize(Bitmap paramBitmap) {
    if (this.mCurrentDimX == paramBitmap.getWidth() && this.mCurrentDimY == paramBitmap.getHeight())
      return; 
    throw new RSIllegalArgumentException("Cannot update allocation from bitmap, sizes mismatch");
  }
  
  private void copyFromUnchecked(Object paramObject, Element.DataType paramDataType, int paramInt) {
    try {
      Trace.traceBegin(32768L, "copyFromUnchecked");
      this.mRS.validate();
      if (this.mCurrentDimZ > 0) {
        copy3DRangeFromUnchecked(0, 0, 0, this.mCurrentDimX, this.mCurrentDimY, this.mCurrentDimZ, paramObject, paramDataType, paramInt);
      } else if (this.mCurrentDimY > 0) {
        copy2DRangeFromUnchecked(0, 0, this.mCurrentDimX, this.mCurrentDimY, paramObject, paramDataType, paramInt);
      } else {
        copy1DRangeFromUnchecked(0, this.mCurrentCount, paramObject, paramDataType, paramInt);
      } 
      return;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  public void copyFromUnchecked(Object paramObject) {
    try {
      Trace.traceBegin(32768L, "copyFromUnchecked");
      Element.DataType dataType = validateObjectIsPrimitiveArray(paramObject, false);
      int i = Array.getLength(paramObject);
      copyFromUnchecked(paramObject, dataType, i);
      return;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  public void copyFromUnchecked(int[] paramArrayOfint) {
    copyFromUnchecked(paramArrayOfint, Element.DataType.SIGNED_32, paramArrayOfint.length);
  }
  
  public void copyFromUnchecked(short[] paramArrayOfshort) {
    copyFromUnchecked(paramArrayOfshort, Element.DataType.SIGNED_16, paramArrayOfshort.length);
  }
  
  public void copyFromUnchecked(byte[] paramArrayOfbyte) {
    copyFromUnchecked(paramArrayOfbyte, Element.DataType.SIGNED_8, paramArrayOfbyte.length);
  }
  
  public void copyFromUnchecked(float[] paramArrayOffloat) {
    copyFromUnchecked(paramArrayOffloat, Element.DataType.FLOAT_32, paramArrayOffloat.length);
  }
  
  public void copyFrom(Object paramObject) {
    try {
      Trace.traceBegin(32768L, "copyFrom");
      Element.DataType dataType = validateObjectIsPrimitiveArray(paramObject, true);
      int i = Array.getLength(paramObject);
      copyFromUnchecked(paramObject, dataType, i);
      return;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  public void copyFrom(int[] paramArrayOfint) {
    validateIsInt32();
    copyFromUnchecked(paramArrayOfint, Element.DataType.SIGNED_32, paramArrayOfint.length);
  }
  
  public void copyFrom(short[] paramArrayOfshort) {
    validateIsInt16OrFloat16();
    copyFromUnchecked(paramArrayOfshort, Element.DataType.SIGNED_16, paramArrayOfshort.length);
  }
  
  public void copyFrom(byte[] paramArrayOfbyte) {
    validateIsInt8();
    copyFromUnchecked(paramArrayOfbyte, Element.DataType.SIGNED_8, paramArrayOfbyte.length);
  }
  
  public void copyFrom(float[] paramArrayOffloat) {
    validateIsFloat32();
    copyFromUnchecked(paramArrayOffloat, Element.DataType.FLOAT_32, paramArrayOffloat.length);
  }
  
  public void copyFrom(Bitmap paramBitmap) {
    try {
      Trace.traceBegin(32768L, "copyFrom");
      this.mRS.validate();
      if (paramBitmap.getConfig() == null) {
        Bitmap bitmap = Bitmap.createBitmap(paramBitmap.getWidth(), paramBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas();
        this(bitmap);
        canvas.drawBitmap(paramBitmap, 0.0F, 0.0F, null);
        copyFrom(bitmap);
        return;
      } 
      validateBitmapSize(paramBitmap);
      validateBitmapFormat(paramBitmap);
      this.mRS.nAllocationCopyFromBitmap(getID(this.mRS), paramBitmap);
      return;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  public void copyFrom(Allocation paramAllocation) {
    try {
      Trace.traceBegin(32768L, "copyFrom");
      this.mRS.validate();
      if (this.mType.equals(paramAllocation.getType())) {
        copy2DRangeFrom(0, 0, this.mCurrentDimX, this.mCurrentDimY, paramAllocation, 0, 0);
        return;
      } 
      RSIllegalArgumentException rSIllegalArgumentException = new RSIllegalArgumentException();
      this("Types of allocations must match.");
      throw rSIllegalArgumentException;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  public void setFromFieldPacker(int paramInt, FieldPacker paramFieldPacker) {
    this.mRS.validate();
    int i = this.mType.mElement.getBytesSize();
    byte[] arrayOfByte = paramFieldPacker.getData();
    int j = paramFieldPacker.getPos();
    int k = j / i;
    if (i * k == j) {
      copy1DRangeFromUnchecked(paramInt, k, arrayOfByte);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Field packer length ");
    stringBuilder.append(j);
    stringBuilder.append(" not divisible by element size ");
    stringBuilder.append(i);
    stringBuilder.append(".");
    throw new RSIllegalArgumentException(stringBuilder.toString());
  }
  
  public void setFromFieldPacker(int paramInt1, int paramInt2, FieldPacker paramFieldPacker) {
    setFromFieldPacker(paramInt1, 0, 0, paramInt2, paramFieldPacker);
  }
  
  public void setFromFieldPacker(int paramInt1, int paramInt2, int paramInt3, int paramInt4, FieldPacker paramFieldPacker) {
    this.mRS.validate();
    if (paramInt4 < this.mType.mElement.mElements.length) {
      if (paramInt1 >= 0) {
        if (paramInt2 >= 0) {
          if (paramInt3 >= 0) {
            byte[] arrayOfByte = paramFieldPacker.getData();
            int i = paramFieldPacker.getPos();
            int j = this.mType.mElement.mElements[paramInt4].getBytesSize();
            j *= this.mType.mElement.mArraySizes[paramInt4];
            if (i == j) {
              this.mRS.nAllocationElementData(getIDSafe(), paramInt1, paramInt2, paramInt3, this.mSelectedLOD, paramInt4, arrayOfByte, i);
              return;
            } 
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("Field packer sizelength ");
            stringBuilder1.append(i);
            stringBuilder1.append(" does not match component size ");
            stringBuilder1.append(j);
            stringBuilder1.append(".");
            throw new RSIllegalArgumentException(stringBuilder1.toString());
          } 
          throw new RSIllegalArgumentException("Offset z must be >= 0.");
        } 
        throw new RSIllegalArgumentException("Offset y must be >= 0.");
      } 
      throw new RSIllegalArgumentException("Offset x must be >= 0.");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Component_number ");
    stringBuilder.append(paramInt4);
    stringBuilder.append(" out of range.");
    throw new RSIllegalArgumentException(stringBuilder.toString());
  }
  
  private void data1DChecks(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean) {
    this.mRS.validate();
    if (paramInt1 >= 0) {
      if (paramInt2 >= 1) {
        if (paramInt1 + paramInt2 <= this.mCurrentCount) {
          if (paramBoolean) {
            if (paramInt3 < paramInt4 / 4 * 3)
              throw new RSIllegalArgumentException("Array too small for allocation type."); 
          } else if (paramInt3 < paramInt4) {
            throw new RSIllegalArgumentException("Array too small for allocation type.");
          } 
          return;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Overflow, Available count ");
        stringBuilder.append(this.mCurrentCount);
        stringBuilder.append(", got ");
        stringBuilder.append(paramInt2);
        stringBuilder.append(" at offset ");
        stringBuilder.append(paramInt1);
        stringBuilder.append(".");
        throw new RSIllegalArgumentException(stringBuilder.toString());
      } 
      throw new RSIllegalArgumentException("Count must be >= 1.");
    } 
    throw new RSIllegalArgumentException("Offset must be >= 0.");
  }
  
  public void generateMipmaps() {
    this.mRS.nAllocationGenerateMipmaps(getID(this.mRS));
  }
  
  private void copy1DRangeFromUnchecked(int paramInt1, int paramInt2, Object paramObject, Element.DataType paramDataType, int paramInt3) {
    try {
      boolean bool;
      Trace.traceBegin(32768L, "copy1DRangeFromUnchecked");
      int i = this.mType.mElement.getBytesSize() * paramInt2;
      if (this.mAutoPadding && this.mType.getElement().getVectorSize() == 3) {
        bool = true;
      } else {
        bool = false;
      } 
      data1DChecks(paramInt1, paramInt2, paramInt3 * paramDataType.mSize, i, bool);
      this.mRS.nAllocationData1D(getIDSafe(), paramInt1, this.mSelectedLOD, paramInt2, paramObject, i, paramDataType, this.mType.mElement.mType.mSize, bool);
      return;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  public void copy1DRangeFromUnchecked(int paramInt1, int paramInt2, Object paramObject) {
    Element.DataType dataType = validateObjectIsPrimitiveArray(paramObject, false);
    int i = Array.getLength(paramObject);
    copy1DRangeFromUnchecked(paramInt1, paramInt2, paramObject, dataType, i);
  }
  
  public void copy1DRangeFromUnchecked(int paramInt1, int paramInt2, int[] paramArrayOfint) {
    copy1DRangeFromUnchecked(paramInt1, paramInt2, paramArrayOfint, Element.DataType.SIGNED_32, paramArrayOfint.length);
  }
  
  public void copy1DRangeFromUnchecked(int paramInt1, int paramInt2, short[] paramArrayOfshort) {
    copy1DRangeFromUnchecked(paramInt1, paramInt2, paramArrayOfshort, Element.DataType.SIGNED_16, paramArrayOfshort.length);
  }
  
  public void copy1DRangeFromUnchecked(int paramInt1, int paramInt2, byte[] paramArrayOfbyte) {
    copy1DRangeFromUnchecked(paramInt1, paramInt2, paramArrayOfbyte, Element.DataType.SIGNED_8, paramArrayOfbyte.length);
  }
  
  public void copy1DRangeFromUnchecked(int paramInt1, int paramInt2, float[] paramArrayOffloat) {
    copy1DRangeFromUnchecked(paramInt1, paramInt2, paramArrayOffloat, Element.DataType.FLOAT_32, paramArrayOffloat.length);
  }
  
  public void copy1DRangeFrom(int paramInt1, int paramInt2, Object paramObject) {
    Element.DataType dataType = validateObjectIsPrimitiveArray(paramObject, true);
    int i = Array.getLength(paramObject);
    copy1DRangeFromUnchecked(paramInt1, paramInt2, paramObject, dataType, i);
  }
  
  public void copy1DRangeFrom(int paramInt1, int paramInt2, int[] paramArrayOfint) {
    validateIsInt32();
    copy1DRangeFromUnchecked(paramInt1, paramInt2, paramArrayOfint, Element.DataType.SIGNED_32, paramArrayOfint.length);
  }
  
  public void copy1DRangeFrom(int paramInt1, int paramInt2, short[] paramArrayOfshort) {
    validateIsInt16OrFloat16();
    copy1DRangeFromUnchecked(paramInt1, paramInt2, paramArrayOfshort, Element.DataType.SIGNED_16, paramArrayOfshort.length);
  }
  
  public void copy1DRangeFrom(int paramInt1, int paramInt2, byte[] paramArrayOfbyte) {
    validateIsInt8();
    copy1DRangeFromUnchecked(paramInt1, paramInt2, paramArrayOfbyte, Element.DataType.SIGNED_8, paramArrayOfbyte.length);
  }
  
  public void copy1DRangeFrom(int paramInt1, int paramInt2, float[] paramArrayOffloat) {
    validateIsFloat32();
    copy1DRangeFromUnchecked(paramInt1, paramInt2, paramArrayOffloat, Element.DataType.FLOAT_32, paramArrayOffloat.length);
  }
  
  public void copy1DRangeFrom(int paramInt1, int paramInt2, Allocation paramAllocation, int paramInt3) {
    Trace.traceBegin(32768L, "copy1DRangeFrom");
    RenderScript renderScript1 = this.mRS;
    long l1 = getIDSafe();
    int i = this.mSelectedLOD, j = this.mSelectedFace.mID;
    RenderScript renderScript2 = this.mRS;
    long l2 = paramAllocation.getID(renderScript2);
    int k = paramAllocation.mSelectedLOD, m = paramAllocation.mSelectedFace.mID;
    renderScript1.nAllocationData2D(l1, paramInt1, 0, i, j, paramInt2, 1, l2, paramInt3, 0, k, m);
    Trace.traceEnd(32768L);
  }
  
  private void validate2DRange(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (this.mAdaptedAllocation == null) {
      if (paramInt1 >= 0 && paramInt2 >= 0) {
        if (paramInt4 >= 0 && paramInt3 >= 0) {
          if (paramInt1 + paramInt3 > this.mCurrentDimX || paramInt2 + paramInt4 > this.mCurrentDimY)
            throw new RSIllegalArgumentException("Updated region larger than allocation."); 
          return;
        } 
        throw new RSIllegalArgumentException("Height or width cannot be negative.");
      } 
      throw new RSIllegalArgumentException("Offset cannot be negative.");
    } 
  }
  
  void copy2DRangeFromUnchecked(int paramInt1, int paramInt2, int paramInt3, int paramInt4, Object paramObject, Element.DataType paramDataType, int paramInt5) {
    try {
      Trace.traceBegin(32768L, "copy2DRangeFromUnchecked");
      this.mRS.validate();
      validate2DRange(paramInt1, paramInt2, paramInt3, paramInt4);
      int i = this.mType.mElement.getBytesSize() * paramInt3 * paramInt4;
      paramInt5 = paramDataType.mSize * paramInt5;
      boolean bool = this.mAutoPadding;
      if (bool && this.mType.getElement().getVectorSize() == 3) {
        if (i / 4 * 3 <= paramInt5) {
          bool = true;
          paramInt5 = i;
        } else {
          paramObject = new RSIllegalArgumentException();
          super("Array too small for allocation type.");
          throw paramObject;
        } 
      } else if (i <= paramInt5) {
        bool = false;
      } else {
        paramObject = new RSIllegalArgumentException();
        super("Array too small for allocation type.");
        throw paramObject;
      } 
      this.mRS.nAllocationData2D(getIDSafe(), paramInt1, paramInt2, this.mSelectedLOD, this.mSelectedFace.mID, paramInt3, paramInt4, paramObject, paramInt5, paramDataType, this.mType.mElement.mType.mSize, bool);
      return;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  public void copy2DRangeFrom(int paramInt1, int paramInt2, int paramInt3, int paramInt4, Object paramObject) {
    try {
      Trace.traceBegin(32768L, "copy2DRangeFrom");
      try {
        Element.DataType dataType = validateObjectIsPrimitiveArray(paramObject, true);
        int i = Array.getLength(paramObject);
        copy2DRangeFromUnchecked(paramInt1, paramInt2, paramInt3, paramInt4, paramObject, dataType, i);
        Trace.traceEnd(32768L);
        return;
      } finally {}
    } finally {}
    Trace.traceEnd(32768L);
    throw paramObject;
  }
  
  public void copy2DRangeFrom(int paramInt1, int paramInt2, int paramInt3, int paramInt4, byte[] paramArrayOfbyte) {
    validateIsInt8();
    copy2DRangeFromUnchecked(paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfbyte, Element.DataType.SIGNED_8, paramArrayOfbyte.length);
  }
  
  public void copy2DRangeFrom(int paramInt1, int paramInt2, int paramInt3, int paramInt4, short[] paramArrayOfshort) {
    validateIsInt16OrFloat16();
    copy2DRangeFromUnchecked(paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfshort, Element.DataType.SIGNED_16, paramArrayOfshort.length);
  }
  
  public void copy2DRangeFrom(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfint) {
    validateIsInt32();
    copy2DRangeFromUnchecked(paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfint, Element.DataType.SIGNED_32, paramArrayOfint.length);
  }
  
  public void copy2DRangeFrom(int paramInt1, int paramInt2, int paramInt3, int paramInt4, float[] paramArrayOffloat) {
    validateIsFloat32();
    copy2DRangeFromUnchecked(paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOffloat, Element.DataType.FLOAT_32, paramArrayOffloat.length);
  }
  
  public void copy2DRangeFrom(int paramInt1, int paramInt2, int paramInt3, int paramInt4, Allocation paramAllocation, int paramInt5, int paramInt6) {
    try {
      Trace.traceBegin(32768L, "copy2DRangeFrom");
      this.mRS.validate();
      validate2DRange(paramInt1, paramInt2, paramInt3, paramInt4);
      RenderScript renderScript1 = this.mRS;
      long l1 = getIDSafe();
      int i = this.mSelectedLOD, j = this.mSelectedFace.mID;
      RenderScript renderScript2 = this.mRS;
      long l2 = paramAllocation.getID(renderScript2);
      int k = paramAllocation.mSelectedLOD, m = paramAllocation.mSelectedFace.mID;
      renderScript1.nAllocationData2D(l1, paramInt1, paramInt2, i, j, paramInt3, paramInt4, l2, paramInt5, paramInt6, k, m);
      return;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  public void copy2DRangeFrom(int paramInt1, int paramInt2, Bitmap paramBitmap) {
    try {
      Trace.traceBegin(32768L, "copy2DRangeFrom");
      this.mRS.validate();
      if (paramBitmap.getConfig() == null) {
        Bitmap bitmap = Bitmap.createBitmap(paramBitmap.getWidth(), paramBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas();
        this(bitmap);
        canvas.drawBitmap(paramBitmap, 0.0F, 0.0F, null);
        copy2DRangeFrom(paramInt1, paramInt2, bitmap);
        return;
      } 
      validateBitmapFormat(paramBitmap);
      validate2DRange(paramInt1, paramInt2, paramBitmap.getWidth(), paramBitmap.getHeight());
      this.mRS.nAllocationData2D(getIDSafe(), paramInt1, paramInt2, this.mSelectedLOD, this.mSelectedFace.mID, paramBitmap);
      return;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  private void validate3DRange(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    if (this.mAdaptedAllocation == null) {
      if (paramInt1 >= 0 && paramInt2 >= 0 && paramInt3 >= 0) {
        if (paramInt5 >= 0 && paramInt4 >= 0 && paramInt6 >= 0) {
          if (paramInt1 + paramInt4 > this.mCurrentDimX || paramInt2 + paramInt5 > this.mCurrentDimY || paramInt3 + paramInt6 > this.mCurrentDimZ)
            throw new RSIllegalArgumentException("Updated region larger than allocation."); 
          return;
        } 
        throw new RSIllegalArgumentException("Height or width cannot be negative.");
      } 
      throw new RSIllegalArgumentException("Offset cannot be negative.");
    } 
  }
  
  private void copy3DRangeFromUnchecked(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, Object paramObject, Element.DataType paramDataType, int paramInt7) {
    try {
      Trace.traceBegin(32768L, "copy3DRangeFromUnchecked");
      this.mRS.validate();
      validate3DRange(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
      int i = this.mType.mElement.getBytesSize() * paramInt4 * paramInt5 * paramInt6;
      paramInt7 = paramDataType.mSize * paramInt7;
      boolean bool = this.mAutoPadding;
      if (bool && this.mType.getElement().getVectorSize() == 3) {
        if (i / 4 * 3 <= paramInt7) {
          bool = true;
          paramInt7 = i;
        } else {
          paramObject = new RSIllegalArgumentException();
          super("Array too small for allocation type.");
          throw paramObject;
        } 
      } else if (i <= paramInt7) {
        bool = false;
      } else {
        paramObject = new RSIllegalArgumentException();
        super("Array too small for allocation type.");
        throw paramObject;
      } 
      this.mRS.nAllocationData3D(getIDSafe(), paramInt1, paramInt2, paramInt3, this.mSelectedLOD, paramInt4, paramInt5, paramInt6, paramObject, paramInt7, paramDataType, this.mType.mElement.mType.mSize, bool);
      return;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  public void copy3DRangeFrom(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, Object paramObject) {
    try {
      Trace.traceBegin(32768L, "copy3DRangeFrom");
      try {
        Element.DataType dataType = validateObjectIsPrimitiveArray(paramObject, true);
        int i = Array.getLength(paramObject);
        copy3DRangeFromUnchecked(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramObject, dataType, i);
        Trace.traceEnd(32768L);
        return;
      } finally {}
    } finally {}
    Trace.traceEnd(32768L);
    throw paramObject;
  }
  
  public void copy3DRangeFrom(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, Allocation paramAllocation, int paramInt7, int paramInt8, int paramInt9) {
    this.mRS.validate();
    validate3DRange(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
    RenderScript renderScript1 = this.mRS;
    long l1 = getIDSafe();
    int i = this.mSelectedLOD;
    RenderScript renderScript2 = this.mRS;
    long l2 = paramAllocation.getID(renderScript2);
    int j = paramAllocation.mSelectedLOD;
    renderScript1.nAllocationData3D(l1, paramInt1, paramInt2, paramInt3, i, paramInt4, paramInt5, paramInt6, l2, paramInt7, paramInt8, paramInt9, j);
  }
  
  public void copyTo(Bitmap paramBitmap) {
    try {
      Trace.traceBegin(32768L, "copyTo");
      this.mRS.validate();
      validateBitmapFormat(paramBitmap);
      validateBitmapSize(paramBitmap);
      this.mRS.nAllocationCopyToBitmap(getID(this.mRS), paramBitmap);
      return;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  private void copyTo(Object paramObject, Element.DataType paramDataType, int paramInt) {
    try {
      Trace.traceBegin(32768L, "copyTo");
      this.mRS.validate();
      boolean bool1 = false;
      boolean bool2 = bool1;
      if (this.mAutoPadding) {
        int i = this.mType.getElement().getVectorSize();
        bool2 = bool1;
        if (i == 3)
          bool2 = true; 
      } 
      if (bool2) {
        if (paramDataType.mSize * paramInt < this.mSize / 4 * 3) {
          paramObject = new RSIllegalArgumentException();
          super("Size of output array cannot be smaller than size of allocation.");
          throw paramObject;
        } 
      } else if (paramDataType.mSize * paramInt < this.mSize) {
        paramObject = new RSIllegalArgumentException();
        super("Size of output array cannot be smaller than size of allocation.");
        throw paramObject;
      } 
      this.mRS.nAllocationRead(getID(this.mRS), paramObject, paramDataType, this.mType.mElement.mType.mSize, bool2);
      return;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  public void copyTo(Object paramObject) {
    Element.DataType dataType = validateObjectIsPrimitiveArray(paramObject, true);
    int i = Array.getLength(paramObject);
    copyTo(paramObject, dataType, i);
  }
  
  public void copyTo(byte[] paramArrayOfbyte) {
    validateIsInt8();
    copyTo(paramArrayOfbyte, Element.DataType.SIGNED_8, paramArrayOfbyte.length);
  }
  
  public void copyTo(short[] paramArrayOfshort) {
    validateIsInt16OrFloat16();
    copyTo(paramArrayOfshort, Element.DataType.SIGNED_16, paramArrayOfshort.length);
  }
  
  public void copyTo(int[] paramArrayOfint) {
    validateIsInt32();
    copyTo(paramArrayOfint, Element.DataType.SIGNED_32, paramArrayOfint.length);
  }
  
  public void copyTo(float[] paramArrayOffloat) {
    validateIsFloat32();
    copyTo(paramArrayOffloat, Element.DataType.FLOAT_32, paramArrayOffloat.length);
  }
  
  public void copyToFieldPacker(int paramInt1, int paramInt2, int paramInt3, int paramInt4, FieldPacker paramFieldPacker) {
    this.mRS.validate();
    if (paramInt4 < this.mType.mElement.mElements.length) {
      if (paramInt1 >= 0) {
        if (paramInt2 >= 0) {
          if (paramInt3 >= 0) {
            byte[] arrayOfByte = paramFieldPacker.getData();
            int i = arrayOfByte.length;
            int j = this.mType.mElement.mElements[paramInt4].getBytesSize();
            j *= this.mType.mElement.mArraySizes[paramInt4];
            if (i == j) {
              this.mRS.nAllocationElementRead(getIDSafe(), paramInt1, paramInt2, paramInt3, this.mSelectedLOD, paramInt4, arrayOfByte, i);
              return;
            } 
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("Field packer sizelength ");
            stringBuilder1.append(i);
            stringBuilder1.append(" does not match component size ");
            stringBuilder1.append(j);
            stringBuilder1.append(".");
            throw new RSIllegalArgumentException(stringBuilder1.toString());
          } 
          throw new RSIllegalArgumentException("Offset z must be >= 0.");
        } 
        throw new RSIllegalArgumentException("Offset y must be >= 0.");
      } 
      throw new RSIllegalArgumentException("Offset x must be >= 0.");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Component_number ");
    stringBuilder.append(paramInt4);
    stringBuilder.append(" out of range.");
    throw new RSIllegalArgumentException(stringBuilder.toString());
  }
  
  public void resize(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mRS : Landroid/renderscript/RenderScript;
    //   6: invokevirtual getApplicationContext : ()Landroid/content/Context;
    //   9: invokevirtual getApplicationInfo : ()Landroid/content/pm/ApplicationInfo;
    //   12: getfield targetSdkVersion : I
    //   15: bipush #21
    //   17: if_icmpge -> 160
    //   20: aload_0
    //   21: getfield mType : Landroid/renderscript/Type;
    //   24: invokevirtual getY : ()I
    //   27: ifgt -> 144
    //   30: aload_0
    //   31: getfield mType : Landroid/renderscript/Type;
    //   34: invokevirtual getZ : ()I
    //   37: ifgt -> 144
    //   40: aload_0
    //   41: getfield mType : Landroid/renderscript/Type;
    //   44: invokevirtual hasFaces : ()Z
    //   47: ifne -> 144
    //   50: aload_0
    //   51: getfield mType : Landroid/renderscript/Type;
    //   54: invokevirtual hasMipmaps : ()Z
    //   57: ifne -> 144
    //   60: aload_0
    //   61: getfield mRS : Landroid/renderscript/RenderScript;
    //   64: aload_0
    //   65: aload_0
    //   66: getfield mRS : Landroid/renderscript/RenderScript;
    //   69: invokevirtual getID : (Landroid/renderscript/RenderScript;)J
    //   72: iload_1
    //   73: invokevirtual nAllocationResize1D : (JI)V
    //   76: aload_0
    //   77: getfield mRS : Landroid/renderscript/RenderScript;
    //   80: invokevirtual finish : ()V
    //   83: aload_0
    //   84: getfield mRS : Landroid/renderscript/RenderScript;
    //   87: aload_0
    //   88: aload_0
    //   89: getfield mRS : Landroid/renderscript/RenderScript;
    //   92: invokevirtual getID : (Landroid/renderscript/RenderScript;)J
    //   95: invokevirtual nAllocationGetType : (J)J
    //   98: lstore_2
    //   99: aload_0
    //   100: getfield mType : Landroid/renderscript/Type;
    //   103: lconst_0
    //   104: invokevirtual setID : (J)V
    //   107: new android/renderscript/Type
    //   110: astore #4
    //   112: aload #4
    //   114: lload_2
    //   115: aload_0
    //   116: getfield mRS : Landroid/renderscript/RenderScript;
    //   119: invokespecial <init> : (JLandroid/renderscript/RenderScript;)V
    //   122: aload_0
    //   123: aload #4
    //   125: putfield mType : Landroid/renderscript/Type;
    //   128: aload #4
    //   130: invokevirtual updateFromNative : ()V
    //   133: aload_0
    //   134: aload_0
    //   135: getfield mType : Landroid/renderscript/Type;
    //   138: invokespecial updateCacheInfo : (Landroid/renderscript/Type;)V
    //   141: aload_0
    //   142: monitorexit
    //   143: return
    //   144: new android/renderscript/RSInvalidStateException
    //   147: astore #4
    //   149: aload #4
    //   151: ldc_w 'Resize only support for 1D allocations at this time.'
    //   154: invokespecial <init> : (Ljava/lang/String;)V
    //   157: aload #4
    //   159: athrow
    //   160: new android/renderscript/RSRuntimeException
    //   163: astore #4
    //   165: aload #4
    //   167: ldc_w 'Resize is not allowed in API 21+.'
    //   170: invokespecial <init> : (Ljava/lang/String;)V
    //   173: aload #4
    //   175: athrow
    //   176: astore #4
    //   178: aload_0
    //   179: monitorexit
    //   180: aload #4
    //   182: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2073	-> 2
    //   #2076	-> 20
    //   #2079	-> 60
    //   #2080	-> 76
    //   #2082	-> 83
    //   #2085	-> 99
    //   #2086	-> 107
    //   #2087	-> 128
    //   #2088	-> 133
    //   #2089	-> 141
    //   #2077	-> 144
    //   #2074	-> 160
    //   #2072	-> 176
    // Exception table:
    //   from	to	target	type
    //   2	20	176	finally
    //   20	60	176	finally
    //   60	76	176	finally
    //   76	83	176	finally
    //   83	99	176	finally
    //   99	107	176	finally
    //   107	128	176	finally
    //   128	133	176	finally
    //   133	141	176	finally
    //   144	160	176	finally
    //   160	176	176	finally
  }
  
  private void copy1DRangeToUnchecked(int paramInt1, int paramInt2, Object paramObject, Element.DataType paramDataType, int paramInt3) {
    try {
      boolean bool;
      Trace.traceBegin(32768L, "copy1DRangeToUnchecked");
      int i = this.mType.mElement.getBytesSize() * paramInt2;
      if (this.mAutoPadding && this.mType.getElement().getVectorSize() == 3) {
        bool = true;
      } else {
        bool = false;
      } 
      data1DChecks(paramInt1, paramInt2, paramInt3 * paramDataType.mSize, i, bool);
      this.mRS.nAllocationRead1D(getIDSafe(), paramInt1, this.mSelectedLOD, paramInt2, paramObject, i, paramDataType, this.mType.mElement.mType.mSize, bool);
      return;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  public void copy1DRangeToUnchecked(int paramInt1, int paramInt2, Object paramObject) {
    Element.DataType dataType = validateObjectIsPrimitiveArray(paramObject, false);
    int i = Array.getLength(paramObject);
    copy1DRangeToUnchecked(paramInt1, paramInt2, paramObject, dataType, i);
  }
  
  public void copy1DRangeToUnchecked(int paramInt1, int paramInt2, int[] paramArrayOfint) {
    copy1DRangeToUnchecked(paramInt1, paramInt2, paramArrayOfint, Element.DataType.SIGNED_32, paramArrayOfint.length);
  }
  
  public void copy1DRangeToUnchecked(int paramInt1, int paramInt2, short[] paramArrayOfshort) {
    copy1DRangeToUnchecked(paramInt1, paramInt2, paramArrayOfshort, Element.DataType.SIGNED_16, paramArrayOfshort.length);
  }
  
  public void copy1DRangeToUnchecked(int paramInt1, int paramInt2, byte[] paramArrayOfbyte) {
    copy1DRangeToUnchecked(paramInt1, paramInt2, paramArrayOfbyte, Element.DataType.SIGNED_8, paramArrayOfbyte.length);
  }
  
  public void copy1DRangeToUnchecked(int paramInt1, int paramInt2, float[] paramArrayOffloat) {
    copy1DRangeToUnchecked(paramInt1, paramInt2, paramArrayOffloat, Element.DataType.FLOAT_32, paramArrayOffloat.length);
  }
  
  public void copy1DRangeTo(int paramInt1, int paramInt2, Object paramObject) {
    Element.DataType dataType = validateObjectIsPrimitiveArray(paramObject, true);
    int i = Array.getLength(paramObject);
    copy1DRangeToUnchecked(paramInt1, paramInt2, paramObject, dataType, i);
  }
  
  public void copy1DRangeTo(int paramInt1, int paramInt2, int[] paramArrayOfint) {
    validateIsInt32();
    copy1DRangeToUnchecked(paramInt1, paramInt2, paramArrayOfint, Element.DataType.SIGNED_32, paramArrayOfint.length);
  }
  
  public void copy1DRangeTo(int paramInt1, int paramInt2, short[] paramArrayOfshort) {
    validateIsInt16OrFloat16();
    copy1DRangeToUnchecked(paramInt1, paramInt2, paramArrayOfshort, Element.DataType.SIGNED_16, paramArrayOfshort.length);
  }
  
  public void copy1DRangeTo(int paramInt1, int paramInt2, byte[] paramArrayOfbyte) {
    validateIsInt8();
    copy1DRangeToUnchecked(paramInt1, paramInt2, paramArrayOfbyte, Element.DataType.SIGNED_8, paramArrayOfbyte.length);
  }
  
  public void copy1DRangeTo(int paramInt1, int paramInt2, float[] paramArrayOffloat) {
    validateIsFloat32();
    copy1DRangeToUnchecked(paramInt1, paramInt2, paramArrayOffloat, Element.DataType.FLOAT_32, paramArrayOffloat.length);
  }
  
  void copy2DRangeToUnchecked(int paramInt1, int paramInt2, int paramInt3, int paramInt4, Object paramObject, Element.DataType paramDataType, int paramInt5) {
    try {
      Trace.traceBegin(32768L, "copy2DRangeToUnchecked");
      this.mRS.validate();
      validate2DRange(paramInt1, paramInt2, paramInt3, paramInt4);
      int i = this.mType.mElement.getBytesSize() * paramInt3 * paramInt4;
      paramInt5 = paramDataType.mSize * paramInt5;
      boolean bool = this.mAutoPadding;
      if (bool && this.mType.getElement().getVectorSize() == 3) {
        if (i / 4 * 3 <= paramInt5) {
          bool = true;
          paramInt5 = i;
        } else {
          paramObject = new RSIllegalArgumentException();
          super("Array too small for allocation type.");
          throw paramObject;
        } 
      } else if (i <= paramInt5) {
        bool = false;
      } else {
        paramObject = new RSIllegalArgumentException();
        super("Array too small for allocation type.");
        throw paramObject;
      } 
      this.mRS.nAllocationRead2D(getIDSafe(), paramInt1, paramInt2, this.mSelectedLOD, this.mSelectedFace.mID, paramInt3, paramInt4, paramObject, paramInt5, paramDataType, this.mType.mElement.mType.mSize, bool);
      return;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  public void copy2DRangeTo(int paramInt1, int paramInt2, int paramInt3, int paramInt4, Object paramObject) {
    Element.DataType dataType = validateObjectIsPrimitiveArray(paramObject, true);
    int i = Array.getLength(paramObject);
    copy2DRangeToUnchecked(paramInt1, paramInt2, paramInt3, paramInt4, paramObject, dataType, i);
  }
  
  public void copy2DRangeTo(int paramInt1, int paramInt2, int paramInt3, int paramInt4, byte[] paramArrayOfbyte) {
    validateIsInt8();
    copy2DRangeToUnchecked(paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfbyte, Element.DataType.SIGNED_8, paramArrayOfbyte.length);
  }
  
  public void copy2DRangeTo(int paramInt1, int paramInt2, int paramInt3, int paramInt4, short[] paramArrayOfshort) {
    validateIsInt16OrFloat16();
    copy2DRangeToUnchecked(paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfshort, Element.DataType.SIGNED_16, paramArrayOfshort.length);
  }
  
  public void copy2DRangeTo(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfint) {
    validateIsInt32();
    copy2DRangeToUnchecked(paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfint, Element.DataType.SIGNED_32, paramArrayOfint.length);
  }
  
  public void copy2DRangeTo(int paramInt1, int paramInt2, int paramInt3, int paramInt4, float[] paramArrayOffloat) {
    validateIsFloat32();
    copy2DRangeToUnchecked(paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOffloat, Element.DataType.FLOAT_32, paramArrayOffloat.length);
  }
  
  private void copy3DRangeToUnchecked(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, Object paramObject, Element.DataType paramDataType, int paramInt7) {
    try {
      Trace.traceBegin(32768L, "copy3DRangeToUnchecked");
      this.mRS.validate();
      validate3DRange(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
      int i = this.mType.mElement.getBytesSize() * paramInt4 * paramInt5 * paramInt6;
      paramInt7 = paramDataType.mSize * paramInt7;
      boolean bool = this.mAutoPadding;
      if (bool && this.mType.getElement().getVectorSize() == 3) {
        if (i / 4 * 3 <= paramInt7) {
          bool = true;
          paramInt7 = i;
        } else {
          paramObject = new RSIllegalArgumentException();
          super("Array too small for allocation type.");
          throw paramObject;
        } 
      } else if (i <= paramInt7) {
        bool = false;
      } else {
        paramObject = new RSIllegalArgumentException();
        super("Array too small for allocation type.");
        throw paramObject;
      } 
      this.mRS.nAllocationRead3D(getIDSafe(), paramInt1, paramInt2, paramInt3, this.mSelectedLOD, paramInt4, paramInt5, paramInt6, paramObject, paramInt7, paramDataType, this.mType.mElement.mType.mSize, bool);
      return;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  public void copy3DRangeTo(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, Object paramObject) {
    Element.DataType dataType = validateObjectIsPrimitiveArray(paramObject, true);
    int i = Array.getLength(paramObject);
    copy3DRangeToUnchecked(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramObject, dataType, i);
  }
  
  static {
    BitmapFactory.Options options = new BitmapFactory.Options();
    options.inScaled = false;
  }
  
  public static Allocation createTyped(RenderScript paramRenderScript, Type paramType, MipmapControl paramMipmapControl, int paramInt) {
    try {
      Trace.traceBegin(32768L, "createTyped");
      paramRenderScript.validate();
      if (paramType.getID(paramRenderScript) != 0L) {
        long l = paramType.getID(paramRenderScript);
        try {
          l = paramRenderScript.nAllocationCreateTyped(l, paramMipmapControl.mID, paramInt, 0L);
          if (l != 0L) {
            Allocation allocation = new Allocation(l, paramRenderScript, paramType, false, paramInt, paramMipmapControl);
            Trace.traceEnd(32768L);
            return allocation;
          } 
          RSRuntimeException rSRuntimeException = new RSRuntimeException();
          this("Allocation creation failed.");
          throw rSRuntimeException;
        } finally {}
      } else {
        RSInvalidStateException rSInvalidStateException = new RSInvalidStateException();
        this("Bad Type");
        throw rSInvalidStateException;
      } 
    } finally {}
    Trace.traceEnd(32768L);
    throw paramRenderScript;
  }
  
  public static Allocation createTyped(RenderScript paramRenderScript, Type paramType, int paramInt) {
    return createTyped(paramRenderScript, paramType, MipmapControl.MIPMAP_NONE, paramInt);
  }
  
  public static Allocation createTyped(RenderScript paramRenderScript, Type paramType) {
    return createTyped(paramRenderScript, paramType, MipmapControl.MIPMAP_NONE, 1);
  }
  
  public static Allocation createSized(RenderScript paramRenderScript, Element paramElement, int paramInt1, int paramInt2) {
    try {
      Trace.traceBegin(32768L, "createSized");
      paramRenderScript.validate();
      Type.Builder builder = new Type.Builder();
      try {
        this(paramRenderScript, paramElement);
        try {
          builder.setX(paramInt1);
          Type type = builder.create();
          long l = paramRenderScript.nAllocationCreateTyped(type.getID(paramRenderScript), MipmapControl.MIPMAP_NONE.mID, paramInt2, 0L);
          if (l != 0L) {
            Allocation allocation = new Allocation(l, paramRenderScript, type, true, paramInt2, MipmapControl.MIPMAP_NONE);
            Trace.traceEnd(32768L);
            return allocation;
          } 
          RSRuntimeException rSRuntimeException = new RSRuntimeException();
          this("Allocation creation failed.");
          throw rSRuntimeException;
        } finally {}
      } finally {}
    } finally {}
    Trace.traceEnd(32768L);
    throw paramRenderScript;
  }
  
  public static Allocation createSized(RenderScript paramRenderScript, Element paramElement, int paramInt) {
    return createSized(paramRenderScript, paramElement, paramInt, 1);
  }
  
  static Element elementFromBitmap(RenderScript paramRenderScript, Bitmap paramBitmap) {
    Bitmap.Config config = paramBitmap.getConfig();
    if (config == Bitmap.Config.ALPHA_8)
      return Element.A_8(paramRenderScript); 
    if (config == Bitmap.Config.ARGB_4444)
      return Element.RGBA_4444(paramRenderScript); 
    if (config == Bitmap.Config.ARGB_8888)
      return Element.RGBA_8888(paramRenderScript); 
    if (config == Bitmap.Config.RGB_565)
      return Element.RGB_565(paramRenderScript); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Bad bitmap type: ");
    stringBuilder.append(config);
    throw new RSInvalidStateException(stringBuilder.toString());
  }
  
  static Type typeFromBitmap(RenderScript paramRenderScript, Bitmap paramBitmap, MipmapControl paramMipmapControl) {
    boolean bool;
    Element element = elementFromBitmap(paramRenderScript, paramBitmap);
    Type.Builder builder = new Type.Builder(paramRenderScript, element);
    builder.setX(paramBitmap.getWidth());
    builder.setY(paramBitmap.getHeight());
    if (paramMipmapControl == MipmapControl.MIPMAP_FULL) {
      bool = true;
    } else {
      bool = false;
    } 
    builder.setMipmaps(bool);
    return builder.create();
  }
  
  public static Allocation createFromBitmap(RenderScript paramRenderScript, Bitmap paramBitmap, MipmapControl paramMipmapControl, int paramInt) {
    try {
      Trace.traceBegin(32768L, "createFromBitmap");
      paramRenderScript.validate();
      if (paramBitmap.getConfig() == null) {
        if ((paramInt & 0x80) == 0) {
          Bitmap bitmap = Bitmap.createBitmap(paramBitmap.getWidth(), paramBitmap.getHeight(), Bitmap.Config.ARGB_8888);
          Canvas canvas = new Canvas();
          this(bitmap);
          canvas.drawBitmap(paramBitmap, 0.0F, 0.0F, null);
          return createFromBitmap(paramRenderScript, bitmap, paramMipmapControl, paramInt);
        } 
        rSRuntimeException = new RSIllegalArgumentException();
        this("USAGE_SHARED cannot be used with a Bitmap that has a null config.");
        throw rSRuntimeException;
      } 
      Type type = typeFromBitmap((RenderScript)rSRuntimeException, paramBitmap, paramMipmapControl);
      MipmapControl mipmapControl = MipmapControl.MIPMAP_NONE;
      if (paramMipmapControl == mipmapControl && 
        type.getElement().isCompatible(Element.RGBA_8888((RenderScript)rSRuntimeException)) && paramInt == 131) {
        long l1 = rSRuntimeException.nAllocationCreateBitmapBackedAllocation(type.getID((RenderScript)rSRuntimeException), paramMipmapControl.mID, paramBitmap, paramInt);
        if (l1 != 0L) {
          Allocation allocation = new Allocation();
          this(l1, (RenderScript)rSRuntimeException, type, true, paramInt, paramMipmapControl);
          allocation.setBitmap(paramBitmap);
          return allocation;
        } 
        rSRuntimeException = new RSRuntimeException();
        this("Load failed.");
        throw rSRuntimeException;
      } 
      long l = rSRuntimeException.nAllocationCreateFromBitmap(type.getID((RenderScript)rSRuntimeException), paramMipmapControl.mID, paramBitmap, paramInt);
      if (l != 0L)
        return new Allocation(l, (RenderScript)rSRuntimeException, type, true, paramInt, paramMipmapControl); 
      RSRuntimeException rSRuntimeException = new RSRuntimeException();
      this("Load failed.");
      throw rSRuntimeException;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  public ByteBuffer getByteBuffer() {
    if (!this.mType.hasFaces()) {
      if (this.mType.getYuv() != 17) {
        Type type = this.mType;
        if (type.getYuv() != 842094169) {
          type = this.mType;
          if (type.getYuv() != 35) {
            if (this.mByteBuffer == null || (this.mUsage & 0x20) != 0) {
              int i = this.mType.getX(), j = this.mType.getElement().getBytesSize();
              long[] arrayOfLong = new long[1];
              this.mByteBuffer = this.mRS.nAllocationGetByteBuffer(getID(this.mRS), arrayOfLong, i * j, this.mType.getY(), this.mType.getZ());
              this.mByteBufferStride = arrayOfLong[0];
            } 
            if ((this.mUsage & 0x20) != 0)
              return this.mByteBuffer.asReadOnlyBuffer(); 
            return this.mByteBuffer;
          } 
        } 
      } 
      throw new RSInvalidStateException("YUV format is not supported for getByteBuffer().");
    } 
    throw new RSInvalidStateException("Cubemap is not supported for getByteBuffer().");
  }
  
  public static Allocation[] createAllocations(RenderScript paramRenderScript, Type paramType, int paramInt1, int paramInt2) {
    try {
      Trace.traceBegin(32768L, "createAllocations");
      paramRenderScript.validate();
      if (paramType.getID(paramRenderScript) != 0L) {
        RSIllegalArgumentException rSIllegalArgumentException;
        Allocation[] arrayOfAllocation = new Allocation[paramInt2];
        arrayOfAllocation[0] = createTyped(paramRenderScript, paramType, paramInt1);
        if ((paramInt1 & 0x20) != 0)
          if (paramInt2 <= 16) {
            arrayOfAllocation[0].setupBufferQueue(paramInt2);
          } else {
            arrayOfAllocation[0].destroy();
            rSIllegalArgumentException = new RSIllegalArgumentException();
            this("Exceeds the max number of Allocations allowed: 16");
            throw rSIllegalArgumentException;
          }  
        for (paramInt1 = 1; paramInt1 < paramInt2; paramInt1++)
          arrayOfAllocation[paramInt1] = createFromAllocation((RenderScript)rSIllegalArgumentException, arrayOfAllocation[0]); 
        return arrayOfAllocation;
      } 
      RSInvalidStateException rSInvalidStateException = new RSInvalidStateException();
      this("Bad Type");
      throw rSInvalidStateException;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  static Allocation createFromAllocation(RenderScript paramRenderScript, Allocation paramAllocation) {
    try {
      Trace.traceBegin(32768L, "createFromAllcation");
      paramRenderScript.validate();
      if (paramAllocation.getID(paramRenderScript) != 0L) {
        Type type = paramAllocation.getType();
        int i = paramAllocation.getUsage();
        MipmapControl mipmapControl = paramAllocation.getMipmap();
        long l = paramRenderScript.nAllocationCreateTyped(type.getID(paramRenderScript), mipmapControl.mID, i, 0L);
        if (l != 0L) {
          Allocation allocation = new Allocation();
          this(l, paramRenderScript, type, false, i, mipmapControl);
          if ((i & 0x20) != 0)
            allocation.shareBufferQueue(paramAllocation); 
          return allocation;
        } 
        RSRuntimeException rSRuntimeException = new RSRuntimeException();
        this("Allocation creation failed.");
        throw rSRuntimeException;
      } 
      RSInvalidStateException rSInvalidStateException = new RSInvalidStateException();
      this("Bad input Allocation");
      throw rSInvalidStateException;
    } finally {
      Trace.traceEnd(32768L);
    } 
  }
  
  void setupBufferQueue(int paramInt) {
    this.mRS.validate();
    if ((this.mUsage & 0x20) != 0) {
      this.mRS.nAllocationSetupBufferQueue(getID(this.mRS), paramInt);
      return;
    } 
    throw new RSInvalidStateException("Allocation is not USAGE_IO_INPUT.");
  }
  
  void shareBufferQueue(Allocation paramAllocation) {
    this.mRS.validate();
    if ((this.mUsage & 0x20) != 0) {
      this.mGetSurfaceSurface = paramAllocation.getSurface();
      this.mRS.nAllocationShareBufferQueue(getID(this.mRS), paramAllocation.getID(this.mRS));
      return;
    } 
    throw new RSInvalidStateException("Allocation is not USAGE_IO_INPUT.");
  }
  
  public long getStride() {
    if (this.mByteBufferStride == -1L)
      getByteBuffer(); 
    return this.mByteBufferStride;
  }
  
  public long getTimeStamp() {
    return this.mTimeStamp;
  }
  
  public Surface getSurface() {
    if ((this.mUsage & 0x20) != 0) {
      if (this.mGetSurfaceSurface == null)
        this.mGetSurfaceSurface = this.mRS.nAllocationGetSurface(getID(this.mRS)); 
      return this.mGetSurfaceSurface;
    } 
    throw new RSInvalidStateException("Allocation is not a surface texture.");
  }
  
  public void setSurface(Surface paramSurface) {
    this.mRS.validate();
    if ((this.mUsage & 0x40) != 0) {
      this.mRS.nAllocationSetSurface(getID(this.mRS), paramSurface);
      return;
    } 
    throw new RSInvalidStateException("Allocation is not USAGE_IO_OUTPUT.");
  }
  
  public static Allocation createFromBitmap(RenderScript paramRenderScript, Bitmap paramBitmap) {
    if ((paramRenderScript.getApplicationContext().getApplicationInfo()).targetSdkVersion >= 18)
      return createFromBitmap(paramRenderScript, paramBitmap, MipmapControl.MIPMAP_NONE, 131); 
    return createFromBitmap(paramRenderScript, paramBitmap, MipmapControl.MIPMAP_NONE, 2);
  }
  
  public static Allocation createCubemapFromBitmap(RenderScript paramRenderScript, Bitmap paramBitmap, MipmapControl paramMipmapControl, int paramInt) {
    paramRenderScript.validate();
    int i = paramBitmap.getHeight();
    int j = paramBitmap.getWidth();
    if (j % 6 == 0) {
      if (j / 6 == i) {
        boolean bool = false;
        if ((i - 1 & i) == 0) {
          j = 1;
        } else {
          j = 0;
        } 
        if (j != 0) {
          Element element = elementFromBitmap(paramRenderScript, paramBitmap);
          Type.Builder builder = new Type.Builder(paramRenderScript, element);
          builder.setX(i);
          builder.setY(i);
          builder.setFaces(true);
          if (paramMipmapControl == MipmapControl.MIPMAP_FULL)
            bool = true; 
          builder.setMipmaps(bool);
          Type type = builder.create();
          long l = paramRenderScript.nAllocationCubeCreateFromBitmap(type.getID(paramRenderScript), paramMipmapControl.mID, paramBitmap, paramInt);
          if (l != 0L)
            return new Allocation(l, paramRenderScript, type, true, paramInt, paramMipmapControl); 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Load failed for bitmap ");
          stringBuilder.append(paramBitmap);
          stringBuilder.append(" element ");
          stringBuilder.append(element);
          throw new RSRuntimeException(stringBuilder.toString());
        } 
        throw new RSIllegalArgumentException("Only power of 2 cube faces supported");
      } 
      throw new RSIllegalArgumentException("Only square cube map faces supported");
    } 
    throw new RSIllegalArgumentException("Cubemap height must be multiple of 6");
  }
  
  public static Allocation createCubemapFromBitmap(RenderScript paramRenderScript, Bitmap paramBitmap) {
    return createCubemapFromBitmap(paramRenderScript, paramBitmap, MipmapControl.MIPMAP_NONE, 2);
  }
  
  public static Allocation createCubemapFromCubeFaces(RenderScript paramRenderScript, Bitmap paramBitmap1, Bitmap paramBitmap2, Bitmap paramBitmap3, Bitmap paramBitmap4, Bitmap paramBitmap5, Bitmap paramBitmap6, MipmapControl paramMipmapControl, int paramInt) {
    int i = paramBitmap1.getHeight();
    if (paramBitmap1.getWidth() == i && 
      paramBitmap2.getWidth() == i && paramBitmap2.getHeight() == i && 
      paramBitmap3.getWidth() == i && paramBitmap3.getHeight() == i && 
      paramBitmap4.getWidth() == i && paramBitmap4.getHeight() == i && 
      paramBitmap5.getWidth() == i && paramBitmap5.getHeight() == i && 
      paramBitmap6.getWidth() == i && paramBitmap6.getHeight() == i) {
      boolean bool2, bool1 = false;
      if ((i - 1 & i) == 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (bool2) {
        Element element = elementFromBitmap(paramRenderScript, paramBitmap1);
        Type.Builder builder = new Type.Builder(paramRenderScript, element);
        builder.setX(i);
        builder.setY(i);
        builder.setFaces(true);
        if (paramMipmapControl == MipmapControl.MIPMAP_FULL)
          bool1 = true; 
        builder.setMipmaps(bool1);
        Type type = builder.create();
        Allocation allocation = createTyped(paramRenderScript, type, paramMipmapControl, paramInt);
        AllocationAdapter allocationAdapter = AllocationAdapter.create2D(paramRenderScript, allocation);
        allocationAdapter.setFace(Type.CubemapFace.POSITIVE_X);
        allocationAdapter.copyFrom(paramBitmap1);
        allocationAdapter.setFace(Type.CubemapFace.NEGATIVE_X);
        allocationAdapter.copyFrom(paramBitmap2);
        allocationAdapter.setFace(Type.CubemapFace.POSITIVE_Y);
        allocationAdapter.copyFrom(paramBitmap3);
        allocationAdapter.setFace(Type.CubemapFace.NEGATIVE_Y);
        allocationAdapter.copyFrom(paramBitmap4);
        allocationAdapter.setFace(Type.CubemapFace.POSITIVE_Z);
        allocationAdapter.copyFrom(paramBitmap5);
        allocationAdapter.setFace(Type.CubemapFace.NEGATIVE_Z);
        allocationAdapter.copyFrom(paramBitmap6);
        return allocation;
      } 
      throw new RSIllegalArgumentException("Only power of 2 cube faces supported");
    } 
    throw new RSIllegalArgumentException("Only square cube map faces supported");
  }
  
  public static Allocation createCubemapFromCubeFaces(RenderScript paramRenderScript, Bitmap paramBitmap1, Bitmap paramBitmap2, Bitmap paramBitmap3, Bitmap paramBitmap4, Bitmap paramBitmap5, Bitmap paramBitmap6) {
    return createCubemapFromCubeFaces(paramRenderScript, paramBitmap1, paramBitmap2, paramBitmap3, paramBitmap4, paramBitmap5, paramBitmap6, MipmapControl.MIPMAP_NONE, 2);
  }
  
  public static Allocation createFromBitmapResource(RenderScript paramRenderScript, Resources paramResources, int paramInt1, MipmapControl paramMipmapControl, int paramInt2) {
    paramRenderScript.validate();
    if ((paramInt2 & 0xE0) == 0) {
      Bitmap bitmap = BitmapFactory.decodeResource(paramResources, paramInt1);
      Allocation allocation = createFromBitmap(paramRenderScript, bitmap, paramMipmapControl, paramInt2);
      bitmap.recycle();
      return allocation;
    } 
    throw new RSIllegalArgumentException("Unsupported usage specified.");
  }
  
  public static Allocation createFromBitmapResource(RenderScript paramRenderScript, Resources paramResources, int paramInt) {
    if ((paramRenderScript.getApplicationContext().getApplicationInfo()).targetSdkVersion >= 18)
      return createFromBitmapResource(paramRenderScript, paramResources, paramInt, MipmapControl.MIPMAP_NONE, 3); 
    return createFromBitmapResource(paramRenderScript, paramResources, paramInt, MipmapControl.MIPMAP_NONE, 2);
  }
  
  public static Allocation createFromString(RenderScript paramRenderScript, String paramString, int paramInt) {
    paramRenderScript.validate();
    try {
      byte[] arrayOfByte = paramString.getBytes("UTF-8");
      Allocation allocation = createSized(paramRenderScript, Element.U8(paramRenderScript), arrayOfByte.length, paramInt);
      allocation.copyFrom(arrayOfByte);
      return allocation;
    } catch (Exception exception) {
      throw new RSRuntimeException("Could not convert string to utf-8.");
    } 
  }
  
  public void setOnBufferAvailableListener(OnBufferAvailableListener paramOnBufferAvailableListener) {
    synchronized (mAllocationMap) {
      HashMap<Long, Allocation> hashMap = mAllocationMap;
      Long long_ = new Long();
      this(getID(this.mRS));
      hashMap.put(long_, this);
      this.mBufferNotifier = paramOnBufferAvailableListener;
      return;
    } 
  }
  
  static void sendBufferNotification(long paramLong) {
    synchronized (mAllocationMap) {
      HashMap<Long, Allocation> hashMap = mAllocationMap;
      Long long_ = new Long();
      this(paramLong);
      Allocation allocation = hashMap.get(long_);
      if (allocation != null && allocation.mBufferNotifier != null)
        allocation.mBufferNotifier.onBufferAvailable(allocation); 
      return;
    } 
  }
  
  public void destroy() {
    if ((this.mUsage & 0x40) != 0)
      setSurface((Surface)null); 
    Type type = this.mType;
    if (type != null && this.mOwningType)
      type.destroy(); 
    super.destroy();
  }
  
  class OnBufferAvailableListener {
    public abstract void onBufferAvailable(Allocation param1Allocation);
  }
}
