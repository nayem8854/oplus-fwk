package android.renderscript;

public class Long2 {
  public long x;
  
  public long y;
  
  public Long2() {}
  
  public Long2(long paramLong) {
    this.y = paramLong;
    this.x = paramLong;
  }
  
  public Long2(long paramLong1, long paramLong2) {
    this.x = paramLong1;
    this.y = paramLong2;
  }
  
  public Long2(Long2 paramLong2) {
    this.x = paramLong2.x;
    this.y = paramLong2.y;
  }
  
  public void add(Long2 paramLong2) {
    this.x += paramLong2.x;
    this.y += paramLong2.y;
  }
  
  public static Long2 add(Long2 paramLong21, Long2 paramLong22) {
    Long2 long2 = new Long2();
    paramLong21.x += paramLong22.x;
    paramLong21.y += paramLong22.y;
    return long2;
  }
  
  public void add(long paramLong) {
    this.x += paramLong;
    this.y += paramLong;
  }
  
  public static Long2 add(Long2 paramLong2, long paramLong) {
    Long2 long2 = new Long2();
    paramLong2.x += paramLong;
    paramLong2.y += paramLong;
    return long2;
  }
  
  public void sub(Long2 paramLong2) {
    this.x -= paramLong2.x;
    this.y -= paramLong2.y;
  }
  
  public static Long2 sub(Long2 paramLong21, Long2 paramLong22) {
    Long2 long2 = new Long2();
    paramLong21.x -= paramLong22.x;
    paramLong21.y -= paramLong22.y;
    return long2;
  }
  
  public void sub(long paramLong) {
    this.x -= paramLong;
    this.y -= paramLong;
  }
  
  public static Long2 sub(Long2 paramLong2, long paramLong) {
    Long2 long2 = new Long2();
    paramLong2.x -= paramLong;
    paramLong2.y -= paramLong;
    return long2;
  }
  
  public void mul(Long2 paramLong2) {
    this.x *= paramLong2.x;
    this.y *= paramLong2.y;
  }
  
  public static Long2 mul(Long2 paramLong21, Long2 paramLong22) {
    Long2 long2 = new Long2();
    paramLong21.x *= paramLong22.x;
    paramLong21.y *= paramLong22.y;
    return long2;
  }
  
  public void mul(long paramLong) {
    this.x *= paramLong;
    this.y *= paramLong;
  }
  
  public static Long2 mul(Long2 paramLong2, long paramLong) {
    Long2 long2 = new Long2();
    paramLong2.x *= paramLong;
    paramLong2.y *= paramLong;
    return long2;
  }
  
  public void div(Long2 paramLong2) {
    this.x /= paramLong2.x;
    this.y /= paramLong2.y;
  }
  
  public static Long2 div(Long2 paramLong21, Long2 paramLong22) {
    Long2 long2 = new Long2();
    paramLong21.x /= paramLong22.x;
    paramLong21.y /= paramLong22.y;
    return long2;
  }
  
  public void div(long paramLong) {
    this.x /= paramLong;
    this.y /= paramLong;
  }
  
  public static Long2 div(Long2 paramLong2, long paramLong) {
    Long2 long2 = new Long2();
    paramLong2.x /= paramLong;
    paramLong2.y /= paramLong;
    return long2;
  }
  
  public void mod(Long2 paramLong2) {
    this.x %= paramLong2.x;
    this.y %= paramLong2.y;
  }
  
  public static Long2 mod(Long2 paramLong21, Long2 paramLong22) {
    Long2 long2 = new Long2();
    paramLong21.x %= paramLong22.x;
    paramLong21.y %= paramLong22.y;
    return long2;
  }
  
  public void mod(long paramLong) {
    this.x %= paramLong;
    this.y %= paramLong;
  }
  
  public static Long2 mod(Long2 paramLong2, long paramLong) {
    Long2 long2 = new Long2();
    paramLong2.x %= paramLong;
    paramLong2.y %= paramLong;
    return long2;
  }
  
  public long length() {
    return 2L;
  }
  
  public void negate() {
    this.x = -this.x;
    this.y = -this.y;
  }
  
  public long dotProduct(Long2 paramLong2) {
    return this.x * paramLong2.x + this.y * paramLong2.y;
  }
  
  public static long dotProduct(Long2 paramLong21, Long2 paramLong22) {
    return paramLong22.x * paramLong21.x + paramLong22.y * paramLong21.y;
  }
  
  public void addMultiple(Long2 paramLong2, long paramLong) {
    this.x += paramLong2.x * paramLong;
    this.y += paramLong2.y * paramLong;
  }
  
  public void set(Long2 paramLong2) {
    this.x = paramLong2.x;
    this.y = paramLong2.y;
  }
  
  public void setValues(long paramLong1, long paramLong2) {
    this.x = paramLong1;
    this.y = paramLong2;
  }
  
  public long elementSum() {
    return this.x + this.y;
  }
  
  public long get(int paramInt) {
    if (paramInt != 0) {
      if (paramInt == 1)
        return this.y; 
      throw new IndexOutOfBoundsException("Index: i");
    } 
    return this.x;
  }
  
  public void setAt(int paramInt, long paramLong) {
    if (paramInt != 0) {
      if (paramInt == 1) {
        this.y = paramLong;
        return;
      } 
      throw new IndexOutOfBoundsException("Index: i");
    } 
    this.x = paramLong;
  }
  
  public void addAt(int paramInt, long paramLong) {
    if (paramInt != 0) {
      if (paramInt == 1) {
        this.y += paramLong;
        return;
      } 
      throw new IndexOutOfBoundsException("Index: i");
    } 
    this.x += paramLong;
  }
  
  public void copyTo(long[] paramArrayOflong, int paramInt) {
    paramArrayOflong[paramInt] = this.x;
    paramArrayOflong[paramInt + 1] = this.y;
  }
}
