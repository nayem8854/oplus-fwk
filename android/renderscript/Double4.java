package android.renderscript;

public class Double4 {
  public double w;
  
  public double x;
  
  public double y;
  
  public double z;
  
  public Double4() {}
  
  public Double4(Double4 paramDouble4) {
    this.x = paramDouble4.x;
    this.y = paramDouble4.y;
    this.z = paramDouble4.z;
    this.w = paramDouble4.w;
  }
  
  public Double4(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4) {
    this.x = paramDouble1;
    this.y = paramDouble2;
    this.z = paramDouble3;
    this.w = paramDouble4;
  }
  
  public static Double4 add(Double4 paramDouble41, Double4 paramDouble42) {
    Double4 double4 = new Double4();
    paramDouble41.x += paramDouble42.x;
    paramDouble41.y += paramDouble42.y;
    paramDouble41.z += paramDouble42.z;
    paramDouble41.w += paramDouble42.w;
    return double4;
  }
  
  public void add(Double4 paramDouble4) {
    this.x += paramDouble4.x;
    this.y += paramDouble4.y;
    this.z += paramDouble4.z;
    this.w += paramDouble4.w;
  }
  
  public void add(double paramDouble) {
    this.x += paramDouble;
    this.y += paramDouble;
    this.z += paramDouble;
    this.w += paramDouble;
  }
  
  public static Double4 add(Double4 paramDouble4, double paramDouble) {
    Double4 double4 = new Double4();
    paramDouble4.x += paramDouble;
    paramDouble4.y += paramDouble;
    paramDouble4.z += paramDouble;
    paramDouble4.w += paramDouble;
    return double4;
  }
  
  public void sub(Double4 paramDouble4) {
    this.x -= paramDouble4.x;
    this.y -= paramDouble4.y;
    this.z -= paramDouble4.z;
    this.w -= paramDouble4.w;
  }
  
  public void sub(double paramDouble) {
    this.x -= paramDouble;
    this.y -= paramDouble;
    this.z -= paramDouble;
    this.w -= paramDouble;
  }
  
  public static Double4 sub(Double4 paramDouble4, double paramDouble) {
    Double4 double4 = new Double4();
    paramDouble4.x -= paramDouble;
    paramDouble4.y -= paramDouble;
    paramDouble4.z -= paramDouble;
    paramDouble4.w -= paramDouble;
    return double4;
  }
  
  public static Double4 sub(Double4 paramDouble41, Double4 paramDouble42) {
    Double4 double4 = new Double4();
    paramDouble41.x -= paramDouble42.x;
    paramDouble41.y -= paramDouble42.y;
    paramDouble41.z -= paramDouble42.z;
    paramDouble41.w -= paramDouble42.w;
    return double4;
  }
  
  public void mul(Double4 paramDouble4) {
    this.x *= paramDouble4.x;
    this.y *= paramDouble4.y;
    this.z *= paramDouble4.z;
    this.w *= paramDouble4.w;
  }
  
  public void mul(double paramDouble) {
    this.x *= paramDouble;
    this.y *= paramDouble;
    this.z *= paramDouble;
    this.w *= paramDouble;
  }
  
  public static Double4 mul(Double4 paramDouble41, Double4 paramDouble42) {
    Double4 double4 = new Double4();
    paramDouble41.x *= paramDouble42.x;
    paramDouble41.y *= paramDouble42.y;
    paramDouble41.z *= paramDouble42.z;
    paramDouble41.w *= paramDouble42.w;
    return double4;
  }
  
  public static Double4 mul(Double4 paramDouble4, double paramDouble) {
    Double4 double4 = new Double4();
    paramDouble4.x *= paramDouble;
    paramDouble4.y *= paramDouble;
    paramDouble4.z *= paramDouble;
    paramDouble4.w *= paramDouble;
    return double4;
  }
  
  public void div(Double4 paramDouble4) {
    this.x /= paramDouble4.x;
    this.y /= paramDouble4.y;
    this.z /= paramDouble4.z;
    this.w /= paramDouble4.w;
  }
  
  public void div(double paramDouble) {
    this.x /= paramDouble;
    this.y /= paramDouble;
    this.z /= paramDouble;
    this.w /= paramDouble;
  }
  
  public static Double4 div(Double4 paramDouble4, double paramDouble) {
    Double4 double4 = new Double4();
    paramDouble4.x /= paramDouble;
    paramDouble4.y /= paramDouble;
    paramDouble4.z /= paramDouble;
    paramDouble4.w /= paramDouble;
    return double4;
  }
  
  public static Double4 div(Double4 paramDouble41, Double4 paramDouble42) {
    Double4 double4 = new Double4();
    paramDouble41.x /= paramDouble42.x;
    paramDouble41.y /= paramDouble42.y;
    paramDouble41.z /= paramDouble42.z;
    paramDouble41.w /= paramDouble42.w;
    return double4;
  }
  
  public double dotProduct(Double4 paramDouble4) {
    return this.x * paramDouble4.x + this.y * paramDouble4.y + this.z * paramDouble4.z + this.w * paramDouble4.w;
  }
  
  public static double dotProduct(Double4 paramDouble41, Double4 paramDouble42) {
    return paramDouble42.x * paramDouble41.x + paramDouble42.y * paramDouble41.y + paramDouble42.z * paramDouble41.z + paramDouble42.w * paramDouble41.w;
  }
  
  public void addMultiple(Double4 paramDouble4, double paramDouble) {
    this.x += paramDouble4.x * paramDouble;
    this.y += paramDouble4.y * paramDouble;
    this.z += paramDouble4.z * paramDouble;
    this.w += paramDouble4.w * paramDouble;
  }
  
  public void set(Double4 paramDouble4) {
    this.x = paramDouble4.x;
    this.y = paramDouble4.y;
    this.z = paramDouble4.z;
    this.w = paramDouble4.w;
  }
  
  public void negate() {
    this.x = -this.x;
    this.y = -this.y;
    this.z = -this.z;
    this.w = -this.w;
  }
  
  public int length() {
    return 4;
  }
  
  public double elementSum() {
    return this.x + this.y + this.z + this.w;
  }
  
  public double get(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt == 3)
            return this.w; 
          throw new IndexOutOfBoundsException("Index: i");
        } 
        return this.z;
      } 
      return this.y;
    } 
    return this.x;
  }
  
  public void setAt(int paramInt, double paramDouble) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt == 3) {
            this.w = paramDouble;
            return;
          } 
          throw new IndexOutOfBoundsException("Index: i");
        } 
        this.z = paramDouble;
        return;
      } 
      this.y = paramDouble;
      return;
    } 
    this.x = paramDouble;
  }
  
  public void addAt(int paramInt, double paramDouble) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt == 3) {
            this.w += paramDouble;
            return;
          } 
          throw new IndexOutOfBoundsException("Index: i");
        } 
        this.z += paramDouble;
        return;
      } 
      this.y += paramDouble;
      return;
    } 
    this.x += paramDouble;
  }
  
  public void setValues(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4) {
    this.x = paramDouble1;
    this.y = paramDouble2;
    this.z = paramDouble3;
    this.w = paramDouble4;
  }
  
  public void copyTo(double[] paramArrayOfdouble, int paramInt) {
    paramArrayOfdouble[paramInt] = this.x;
    paramArrayOfdouble[paramInt + 1] = this.y;
    paramArrayOfdouble[paramInt + 2] = this.z;
    paramArrayOfdouble[paramInt + 3] = this.w;
  }
}
