package android.renderscript;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.os.SystemProperties;
import android.util.Log;
import android.view.Surface;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class RenderScript {
  private static ArrayList<RenderScript> mProcessContextList = new ArrayList<>();
  
  private boolean mIsProcessContext = false;
  
  private int mContextFlags = 0;
  
  private int mContextSdkVersion = 0;
  
  static boolean sInitialized = false;
  
  static {
    if (!SystemProperties.getBoolean("config.disable_renderscript", false))
      try {
        Class<?> clazz = Class.forName("dalvik.system.VMRuntime");
        Method method = clazz.getDeclaredMethod("getRuntime", new Class[0]);
        sRuntime = method.invoke(null, new Object[0]);
        Class<long> clazz1 = long.class;
        registerNativeAllocation = clazz.getDeclaredMethod("registerNativeAllocation", new Class[] { clazz1 });
        registerNativeFree = clazz.getDeclaredMethod("registerNativeFree", new Class[] { long.class });
        try {
          System.loadLibrary("rs_jni");
          _nInit();
          sInitialized = true;
          sPointerSize = rsnSystemGetPointerSize();
        } catch (UnsatisfiedLinkError unsatisfiedLinkError) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Error loading RS jni library: ");
          stringBuilder.append(unsatisfiedLinkError);
          Log.e("RenderScript_jni", stringBuilder.toString());
          stringBuilder = new StringBuilder();
          stringBuilder.append("Error loading RS jni library: ");
          stringBuilder.append(unsatisfiedLinkError);
          throw new RSRuntimeException(stringBuilder.toString());
        } 
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Error loading GC methods: ");
        stringBuilder.append(exception);
        Log.e("RenderScript_jni", stringBuilder.toString());
        stringBuilder = new StringBuilder();
        stringBuilder.append("Error loading GC methods: ");
        stringBuilder.append(exception);
        throw new RSRuntimeException(stringBuilder.toString());
      }  
  }
  
  public static long getMinorID() {
    return 1L;
  }
  
  public static long getMinorVersion() {
    return 1L;
  }
  
  public enum ContextType {
    DEBUG,
    NORMAL(0),
    PROFILE(0);
    
    private static final ContextType[] $VALUES;
    
    int mID;
    
    static {
      ContextType contextType = new ContextType("PROFILE", 2, 2);
      $VALUES = new ContextType[] { NORMAL, DEBUG, contextType };
    }
    
    ContextType(int param1Int1) {
      this.mID = param1Int1;
    }
  }
  
  long nContextCreateGL(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10, int paramInt11, int paramInt12, float paramFloat, int paramInt13) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: lload_1
    //   4: iload_3
    //   5: iload #4
    //   7: iload #5
    //   9: iload #6
    //   11: iload #7
    //   13: iload #8
    //   15: iload #9
    //   17: iload #10
    //   19: iload #11
    //   21: iload #12
    //   23: iload #13
    //   25: iload #14
    //   27: fload #15
    //   29: iload #16
    //   31: invokevirtual rsnContextCreateGL : (JIIIIIIIIIIIIFI)J
    //   34: lstore_1
    //   35: aload_0
    //   36: monitorexit
    //   37: lload_1
    //   38: lreturn
    //   39: astore #17
    //   41: aload_0
    //   42: monitorexit
    //   43: aload #17
    //   45: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #224	-> 2
    //   #224	-> 39
    // Exception table:
    //   from	to	target	type
    //   2	35	39	finally
  }
  
  long nContextCreate(long paramLong, int paramInt1, int paramInt2, int paramInt3) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: lload_1
    //   4: iload_3
    //   5: iload #4
    //   7: iload #5
    //   9: invokevirtual rsnContextCreate : (JIII)J
    //   12: lstore_1
    //   13: aload_0
    //   14: monitorexit
    //   15: lload_1
    //   16: lreturn
    //   17: astore #6
    //   19: aload_0
    //   20: monitorexit
    //   21: aload #6
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #231	-> 2
    //   #231	-> 17
    // Exception table:
    //   from	to	target	type
    //   2	13	17	finally
  }
  
  void nContextDestroy() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: getfield mRWLock : Ljava/util/concurrent/locks/ReentrantReadWriteLock;
    //   10: invokevirtual writeLock : ()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;
    //   13: astore_1
    //   14: aload_1
    //   15: invokevirtual lock : ()V
    //   18: aload_0
    //   19: getfield mContext : J
    //   22: lstore_2
    //   23: aload_0
    //   24: lconst_0
    //   25: putfield mContext : J
    //   28: aload_1
    //   29: invokevirtual unlock : ()V
    //   32: aload_0
    //   33: lload_2
    //   34: invokevirtual rsnContextDestroy : (J)V
    //   37: aload_0
    //   38: monitorexit
    //   39: return
    //   40: astore_1
    //   41: aload_0
    //   42: monitorexit
    //   43: aload_1
    //   44: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #235	-> 2
    //   #239	-> 6
    //   #240	-> 14
    //   #242	-> 18
    //   #244	-> 23
    //   #246	-> 28
    //   #247	-> 32
    //   #248	-> 37
    //   #234	-> 40
    // Exception table:
    //   from	to	target	type
    //   2	6	40	finally
    //   6	14	40	finally
    //   14	18	40	finally
    //   18	23	40	finally
    //   23	28	40	finally
    //   28	32	40	finally
    //   32	37	40	finally
  }
  
  void nContextSetSurface(int paramInt1, int paramInt2, Surface paramSurface) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: iload_1
    //   12: iload_2
    //   13: aload_3
    //   14: invokevirtual rsnContextSetSurface : (JIILandroid/view/Surface;)V
    //   17: aload_0
    //   18: monitorexit
    //   19: return
    //   20: astore_3
    //   21: aload_0
    //   22: monitorexit
    //   23: aload_3
    //   24: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #251	-> 2
    //   #252	-> 6
    //   #253	-> 17
    //   #250	-> 20
    // Exception table:
    //   from	to	target	type
    //   2	6	20	finally
    //   6	17	20	finally
  }
  
  void nContextSetSurfaceTexture(int paramInt1, int paramInt2, SurfaceTexture paramSurfaceTexture) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: iload_1
    //   12: iload_2
    //   13: aload_3
    //   14: invokevirtual rsnContextSetSurfaceTexture : (JIILandroid/graphics/SurfaceTexture;)V
    //   17: aload_0
    //   18: monitorexit
    //   19: return
    //   20: astore_3
    //   21: aload_0
    //   22: monitorexit
    //   23: aload_3
    //   24: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #256	-> 2
    //   #257	-> 6
    //   #258	-> 17
    //   #255	-> 20
    // Exception table:
    //   from	to	target	type
    //   2	6	20	finally
    //   6	17	20	finally
  }
  
  void nContextSetPriority(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: iload_1
    //   12: invokevirtual rsnContextSetPriority : (JI)V
    //   15: aload_0
    //   16: monitorexit
    //   17: return
    //   18: astore_2
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_2
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #261	-> 2
    //   #262	-> 6
    //   #263	-> 15
    //   #260	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	15	18	finally
  }
  
  void nContextSetCacheDir(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: aload_1
    //   12: invokevirtual rsnContextSetCacheDir : (JLjava/lang/String;)V
    //   15: aload_0
    //   16: monitorexit
    //   17: return
    //   18: astore_1
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_1
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #266	-> 2
    //   #267	-> 6
    //   #268	-> 15
    //   #265	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	15	18	finally
  }
  
  void nContextDump(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: iload_1
    //   12: invokevirtual rsnContextDump : (JI)V
    //   15: aload_0
    //   16: monitorexit
    //   17: return
    //   18: astore_2
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_2
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #271	-> 2
    //   #272	-> 6
    //   #273	-> 15
    //   #270	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	15	18	finally
  }
  
  void nContextFinish() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: invokevirtual rsnContextFinish : (J)V
    //   14: aload_0
    //   15: monitorexit
    //   16: return
    //   17: astore_1
    //   18: aload_0
    //   19: monitorexit
    //   20: aload_1
    //   21: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #276	-> 2
    //   #277	-> 6
    //   #278	-> 14
    //   #275	-> 17
    // Exception table:
    //   from	to	target	type
    //   2	6	17	finally
    //   6	14	17	finally
  }
  
  void nContextSendMessage(int paramInt, int[] paramArrayOfint) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: iload_1
    //   12: aload_2
    //   13: invokevirtual rsnContextSendMessage : (JI[I)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_2
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_2
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #282	-> 2
    //   #283	-> 6
    //   #284	-> 16
    //   #281	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	6	19	finally
    //   6	16	19	finally
  }
  
  void nContextBindRootScript(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: invokevirtual rsnContextBindRootScript : (JJ)V
    //   15: aload_0
    //   16: monitorexit
    //   17: return
    //   18: astore_3
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_3
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #288	-> 2
    //   #289	-> 6
    //   #290	-> 15
    //   #287	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	15	18	finally
  }
  
  void nContextBindSampler(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: iload_1
    //   12: iload_2
    //   13: invokevirtual rsnContextBindSampler : (JII)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_3
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_3
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #293	-> 2
    //   #294	-> 6
    //   #295	-> 16
    //   #292	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	6	19	finally
    //   6	16	19	finally
  }
  
  void nContextBindProgramStore(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: invokevirtual rsnContextBindProgramStore : (JJ)V
    //   15: aload_0
    //   16: monitorexit
    //   17: return
    //   18: astore_3
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_3
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #298	-> 2
    //   #299	-> 6
    //   #300	-> 15
    //   #297	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	15	18	finally
  }
  
  void nContextBindProgramFragment(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: invokevirtual rsnContextBindProgramFragment : (JJ)V
    //   15: aload_0
    //   16: monitorexit
    //   17: return
    //   18: astore_3
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_3
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #303	-> 2
    //   #304	-> 6
    //   #305	-> 15
    //   #302	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	15	18	finally
  }
  
  void nContextBindProgramVertex(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: invokevirtual rsnContextBindProgramVertex : (JJ)V
    //   15: aload_0
    //   16: monitorexit
    //   17: return
    //   18: astore_3
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_3
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #308	-> 2
    //   #309	-> 6
    //   #310	-> 15
    //   #307	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	15	18	finally
  }
  
  void nContextBindProgramRaster(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: invokevirtual rsnContextBindProgramRaster : (JJ)V
    //   15: aload_0
    //   16: monitorexit
    //   17: return
    //   18: astore_3
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_3
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #313	-> 2
    //   #314	-> 6
    //   #315	-> 15
    //   #312	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	15	18	finally
  }
  
  void nContextPause() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: invokevirtual rsnContextPause : (J)V
    //   14: aload_0
    //   15: monitorexit
    //   16: return
    //   17: astore_1
    //   18: aload_0
    //   19: monitorexit
    //   20: aload_1
    //   21: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #318	-> 2
    //   #319	-> 6
    //   #320	-> 14
    //   #317	-> 17
    // Exception table:
    //   from	to	target	type
    //   2	6	17	finally
    //   6	14	17	finally
  }
  
  void nContextResume() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: invokevirtual rsnContextResume : (J)V
    //   14: aload_0
    //   15: monitorexit
    //   16: return
    //   17: astore_1
    //   18: aload_0
    //   19: monitorexit
    //   20: aload_1
    //   21: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #323	-> 2
    //   #324	-> 6
    //   #325	-> 14
    //   #322	-> 17
    // Exception table:
    //   from	to	target	type
    //   2	6	17	finally
    //   6	14	17	finally
  }
  
  long nClosureCreate(long paramLong1, long paramLong2, long[] paramArrayOflong1, long[] paramArrayOflong2, int[] paramArrayOfint, long[] paramArrayOflong3, long[] paramArrayOflong4) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: lload_3
    //   13: aload #5
    //   15: aload #6
    //   17: aload #7
    //   19: aload #8
    //   21: aload #9
    //   23: invokevirtual rsnClosureCreate : (JJJ[J[J[I[J[J)J
    //   26: lstore_1
    //   27: lload_1
    //   28: lconst_0
    //   29: lcmp
    //   30: ifeq -> 37
    //   33: aload_0
    //   34: monitorexit
    //   35: lload_1
    //   36: lreturn
    //   37: new android/renderscript/RSRuntimeException
    //   40: astore #5
    //   42: aload #5
    //   44: ldc_w 'Failed creating closure.'
    //   47: invokespecial <init> : (Ljava/lang/String;)V
    //   50: aload #5
    //   52: athrow
    //   53: astore #5
    //   55: aload_0
    //   56: monitorexit
    //   57: aload #5
    //   59: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #333	-> 2
    //   #334	-> 6
    //   #336	-> 27
    //   #339	-> 33
    //   #337	-> 37
    //   #332	-> 53
    // Exception table:
    //   from	to	target	type
    //   2	6	53	finally
    //   6	27	53	finally
    //   37	53	53	finally
  }
  
  long nInvokeClosureCreate(long paramLong, byte[] paramArrayOfbyte, long[] paramArrayOflong1, long[] paramArrayOflong2, int[] paramArrayOfint) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: aload_3
    //   13: aload #4
    //   15: aload #5
    //   17: aload #6
    //   19: invokevirtual rsnInvokeClosureCreate : (JJ[B[J[J[I)J
    //   22: lstore_1
    //   23: lload_1
    //   24: lconst_0
    //   25: lcmp
    //   26: ifeq -> 33
    //   29: aload_0
    //   30: monitorexit
    //   31: lload_1
    //   32: lreturn
    //   33: new android/renderscript/RSRuntimeException
    //   36: astore_3
    //   37: aload_3
    //   38: ldc_w 'Failed creating closure.'
    //   41: invokespecial <init> : (Ljava/lang/String;)V
    //   44: aload_3
    //   45: athrow
    //   46: astore_3
    //   47: aload_0
    //   48: monitorexit
    //   49: aload_3
    //   50: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #346	-> 2
    //   #347	-> 6
    //   #349	-> 23
    //   #352	-> 29
    //   #350	-> 33
    //   #345	-> 46
    // Exception table:
    //   from	to	target	type
    //   2	6	46	finally
    //   6	23	46	finally
    //   33	46	46	finally
  }
  
  void nClosureSetArg(long paramLong1, int paramInt1, long paramLong2, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: lload #4
    //   15: iload #6
    //   17: invokevirtual rsnClosureSetArg : (JJIJI)V
    //   20: aload_0
    //   21: monitorexit
    //   22: return
    //   23: astore #7
    //   25: aload_0
    //   26: monitorexit
    //   27: aload #7
    //   29: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #359	-> 2
    //   #360	-> 6
    //   #361	-> 20
    //   #358	-> 23
    // Exception table:
    //   from	to	target	type
    //   2	6	23	finally
    //   6	20	23	finally
  }
  
  void nClosureSetGlobal(long paramLong1, long paramLong2, long paramLong3, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: lload_3
    //   13: lload #5
    //   15: iload #7
    //   17: invokevirtual rsnClosureSetGlobal : (JJJJI)V
    //   20: aload_0
    //   21: monitorexit
    //   22: return
    //   23: astore #8
    //   25: aload_0
    //   26: monitorexit
    //   27: aload #8
    //   29: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #368	-> 2
    //   #369	-> 6
    //   #370	-> 20
    //   #367	-> 23
    // Exception table:
    //   from	to	target	type
    //   2	6	23	finally
    //   6	20	23	finally
  }
  
  long nScriptGroup2Create(String paramString1, String paramString2, long[] paramArrayOflong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: aload_1
    //   12: aload_2
    //   13: aload_3
    //   14: invokevirtual rsnScriptGroup2Create : (JLjava/lang/String;Ljava/lang/String;[J)J
    //   17: lstore #4
    //   19: lload #4
    //   21: lconst_0
    //   22: lcmp
    //   23: ifeq -> 31
    //   26: aload_0
    //   27: monitorexit
    //   28: lload #4
    //   30: lreturn
    //   31: new android/renderscript/RSRuntimeException
    //   34: astore_1
    //   35: aload_1
    //   36: ldc_w 'Failed creating script group.'
    //   39: invokespecial <init> : (Ljava/lang/String;)V
    //   42: aload_1
    //   43: athrow
    //   44: astore_1
    //   45: aload_0
    //   46: monitorexit
    //   47: aload_1
    //   48: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #376	-> 2
    //   #377	-> 6
    //   #378	-> 19
    //   #381	-> 26
    //   #379	-> 31
    //   #375	-> 44
    // Exception table:
    //   from	to	target	type
    //   2	6	44	finally
    //   6	19	44	finally
    //   31	44	44	finally
  }
  
  void nScriptGroup2Execute(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: invokevirtual rsnScriptGroup2Execute : (JJ)V
    //   15: aload_0
    //   16: monitorexit
    //   17: return
    //   18: astore_3
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_3
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #386	-> 2
    //   #387	-> 6
    //   #388	-> 15
    //   #385	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	15	18	finally
  }
  
  void nAssignName(long paramLong, byte[] paramArrayOfbyte) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: aload_3
    //   13: invokevirtual rsnAssignName : (JJ[B)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_3
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_3
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #392	-> 2
    //   #393	-> 6
    //   #394	-> 16
    //   #391	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	6	19	finally
    //   6	16	19	finally
  }
  
  String nGetName(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: invokevirtual rsnGetName : (JJ)Ljava/lang/String;
    //   15: astore_3
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_3
    //   19: areturn
    //   20: astore_3
    //   21: aload_0
    //   22: monitorexit
    //   23: aload_3
    //   24: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #397	-> 2
    //   #398	-> 6
    //   #396	-> 20
    // Exception table:
    //   from	to	target	type
    //   2	6	20	finally
    //   6	16	20	finally
  }
  
  void nObjDestroy(long paramLong) {
    long l = this.mContext;
    if (l != 0L)
      rsnObjDestroy(l, paramLong); 
  }
  
  long nElementCreate(long paramLong, int paramInt1, boolean paramBoolean, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: iload #5
    //   17: invokevirtual rsnElementCreate : (JJIZI)J
    //   20: lstore_1
    //   21: aload_0
    //   22: monitorexit
    //   23: lload_1
    //   24: lreturn
    //   25: astore #6
    //   27: aload_0
    //   28: monitorexit
    //   29: aload #6
    //   31: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #414	-> 2
    //   #415	-> 6
    //   #413	-> 25
    // Exception table:
    //   from	to	target	type
    //   2	6	25	finally
    //   6	21	25	finally
  }
  
  long nElementCreate2(long[] paramArrayOflong, String[] paramArrayOfString, int[] paramArrayOfint) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: aload_1
    //   12: aload_2
    //   13: aload_3
    //   14: invokevirtual rsnElementCreate2 : (J[J[Ljava/lang/String;[I)J
    //   17: lstore #4
    //   19: aload_0
    //   20: monitorexit
    //   21: lload #4
    //   23: lreturn
    //   24: astore_1
    //   25: aload_0
    //   26: monitorexit
    //   27: aload_1
    //   28: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #419	-> 2
    //   #420	-> 6
    //   #418	-> 24
    // Exception table:
    //   from	to	target	type
    //   2	6	24	finally
    //   6	19	24	finally
  }
  
  void nElementGetNativeData(long paramLong, int[] paramArrayOfint) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: aload_3
    //   13: invokevirtual rsnElementGetNativeData : (JJ[I)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_3
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_3
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #424	-> 2
    //   #425	-> 6
    //   #426	-> 16
    //   #423	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	6	19	finally
    //   6	16	19	finally
  }
  
  void nElementGetSubElements(long paramLong, long[] paramArrayOflong, String[] paramArrayOfString, int[] paramArrayOfint) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: aload_3
    //   13: aload #4
    //   15: aload #5
    //   17: invokevirtual rsnElementGetSubElements : (JJ[J[Ljava/lang/String;[I)V
    //   20: aload_0
    //   21: monitorexit
    //   22: return
    //   23: astore_3
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_3
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #430	-> 2
    //   #431	-> 6
    //   #432	-> 20
    //   #429	-> 23
    // Exception table:
    //   from	to	target	type
    //   2	6	23	finally
    //   6	20	23	finally
  }
  
  long nTypeCreate(long paramLong, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, boolean paramBoolean2, int paramInt4) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: iload #5
    //   17: iload #6
    //   19: iload #7
    //   21: iload #8
    //   23: invokevirtual rsnTypeCreate : (JJIIIZZI)J
    //   26: lstore_1
    //   27: aload_0
    //   28: monitorexit
    //   29: lload_1
    //   30: lreturn
    //   31: astore #9
    //   33: aload_0
    //   34: monitorexit
    //   35: aload #9
    //   37: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #436	-> 2
    //   #437	-> 6
    //   #435	-> 31
    // Exception table:
    //   from	to	target	type
    //   2	6	31	finally
    //   6	27	31	finally
  }
  
  void nTypeGetNativeData(long paramLong, long[] paramArrayOflong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: aload_3
    //   13: invokevirtual rsnTypeGetNativeData : (JJ[J)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_3
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_3
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #441	-> 2
    //   #442	-> 6
    //   #443	-> 16
    //   #440	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	6	19	finally
    //   6	16	19	finally
  }
  
  long nAllocationCreateTyped(long paramLong1, int paramInt1, int paramInt2, long paramLong2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: lload #5
    //   17: invokevirtual rsnAllocationCreateTyped : (JJIIJ)J
    //   20: lstore_1
    //   21: aload_0
    //   22: monitorexit
    //   23: lload_1
    //   24: lreturn
    //   25: astore #7
    //   27: aload_0
    //   28: monitorexit
    //   29: aload #7
    //   31: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #447	-> 2
    //   #448	-> 6
    //   #446	-> 25
    // Exception table:
    //   from	to	target	type
    //   2	6	25	finally
    //   6	21	25	finally
  }
  
  long nAllocationCreateFromBitmap(long paramLong, int paramInt1, Bitmap paramBitmap, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: aload #4
    //   15: iload #5
    //   17: invokevirtual rsnAllocationCreateFromBitmap : (JJILandroid/graphics/Bitmap;I)J
    //   20: lstore_1
    //   21: aload_0
    //   22: monitorexit
    //   23: lload_1
    //   24: lreturn
    //   25: astore #4
    //   27: aload_0
    //   28: monitorexit
    //   29: aload #4
    //   31: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #454	-> 2
    //   #455	-> 6
    //   #453	-> 25
    // Exception table:
    //   from	to	target	type
    //   2	6	25	finally
    //   6	21	25	finally
  }
  
  long nAllocationCreateBitmapBackedAllocation(long paramLong, int paramInt1, Bitmap paramBitmap, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: aload #4
    //   15: iload #5
    //   17: invokevirtual rsnAllocationCreateBitmapBackedAllocation : (JJILandroid/graphics/Bitmap;I)J
    //   20: lstore_1
    //   21: aload_0
    //   22: monitorexit
    //   23: lload_1
    //   24: lreturn
    //   25: astore #4
    //   27: aload_0
    //   28: monitorexit
    //   29: aload #4
    //   31: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #462	-> 2
    //   #463	-> 6
    //   #461	-> 25
    // Exception table:
    //   from	to	target	type
    //   2	6	25	finally
    //   6	21	25	finally
  }
  
  long nAllocationCubeCreateFromBitmap(long paramLong, int paramInt1, Bitmap paramBitmap, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: aload #4
    //   15: iload #5
    //   17: invokevirtual rsnAllocationCubeCreateFromBitmap : (JJILandroid/graphics/Bitmap;I)J
    //   20: lstore_1
    //   21: aload_0
    //   22: monitorexit
    //   23: lload_1
    //   24: lreturn
    //   25: astore #4
    //   27: aload_0
    //   28: monitorexit
    //   29: aload #4
    //   31: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #469	-> 2
    //   #470	-> 6
    //   #468	-> 25
    // Exception table:
    //   from	to	target	type
    //   2	6	25	finally
    //   6	21	25	finally
  }
  
  void nAllocationCopyToBitmap(long paramLong, Bitmap paramBitmap) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: aload_3
    //   13: invokevirtual rsnAllocationCopyToBitmap : (JJLandroid/graphics/Bitmap;)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_3
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_3
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #475	-> 2
    //   #476	-> 6
    //   #477	-> 16
    //   #474	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	6	19	finally
    //   6	16	19	finally
  }
  
  void nAllocationSyncAll(long paramLong, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: invokevirtual rsnAllocationSyncAll : (JJI)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore #4
    //   21: aload_0
    //   22: monitorexit
    //   23: aload #4
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #481	-> 2
    //   #482	-> 6
    //   #483	-> 16
    //   #480	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	6	19	finally
    //   6	16	19	finally
  }
  
  ByteBuffer nAllocationGetByteBuffer(long paramLong, long[] paramArrayOflong, int paramInt1, int paramInt2, int paramInt3) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: aload_3
    //   13: iload #4
    //   15: iload #5
    //   17: iload #6
    //   19: invokevirtual rsnAllocationGetByteBuffer : (JJ[JIII)Ljava/nio/ByteBuffer;
    //   22: astore_3
    //   23: aload_0
    //   24: monitorexit
    //   25: aload_3
    //   26: areturn
    //   27: astore_3
    //   28: aload_0
    //   29: monitorexit
    //   30: aload_3
    //   31: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #489	-> 2
    //   #490	-> 6
    //   #488	-> 27
    // Exception table:
    //   from	to	target	type
    //   2	6	27	finally
    //   6	23	27	finally
  }
  
  void nAllocationSetupBufferQueue(long paramLong, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: invokevirtual rsnAllocationSetupBufferQueue : (JJI)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore #4
    //   21: aload_0
    //   22: monitorexit
    //   23: aload #4
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #495	-> 2
    //   #496	-> 6
    //   #497	-> 16
    //   #494	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	6	19	finally
    //   6	16	19	finally
  }
  
  void nAllocationShareBufferQueue(long paramLong1, long paramLong2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: lload_3
    //   13: invokevirtual rsnAllocationShareBufferQueue : (JJJ)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore #5
    //   21: aload_0
    //   22: monitorexit
    //   23: aload #5
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #500	-> 2
    //   #501	-> 6
    //   #502	-> 16
    //   #499	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	6	19	finally
    //   6	16	19	finally
  }
  
  Surface nAllocationGetSurface(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: invokevirtual rsnAllocationGetSurface : (JJ)Landroid/view/Surface;
    //   15: astore_3
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_3
    //   19: areturn
    //   20: astore_3
    //   21: aload_0
    //   22: monitorexit
    //   23: aload_3
    //   24: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #505	-> 2
    //   #506	-> 6
    //   #504	-> 20
    // Exception table:
    //   from	to	target	type
    //   2	6	20	finally
    //   6	16	20	finally
  }
  
  void nAllocationSetSurface(long paramLong, Surface paramSurface) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: aload_3
    //   13: invokevirtual rsnAllocationSetSurface : (JJLandroid/view/Surface;)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_3
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_3
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #510	-> 2
    //   #511	-> 6
    //   #512	-> 16
    //   #509	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	6	19	finally
    //   6	16	19	finally
  }
  
  void nAllocationIoSend(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: invokevirtual rsnAllocationIoSend : (JJ)V
    //   15: aload_0
    //   16: monitorexit
    //   17: return
    //   18: astore_3
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_3
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #515	-> 2
    //   #516	-> 6
    //   #517	-> 15
    //   #514	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	15	18	finally
  }
  
  long nAllocationIoReceive(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: invokevirtual rsnAllocationIoReceive : (JJ)J
    //   15: lstore_1
    //   16: aload_0
    //   17: monitorexit
    //   18: lload_1
    //   19: lreturn
    //   20: astore_3
    //   21: aload_0
    //   22: monitorexit
    //   23: aload_3
    //   24: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #520	-> 2
    //   #521	-> 6
    //   #519	-> 20
    // Exception table:
    //   from	to	target	type
    //   2	6	20	finally
    //   6	16	20	finally
  }
  
  void nAllocationGenerateMipmaps(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: invokevirtual rsnAllocationGenerateMipmaps : (JJ)V
    //   15: aload_0
    //   16: monitorexit
    //   17: return
    //   18: astore_3
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_3
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #526	-> 2
    //   #527	-> 6
    //   #528	-> 15
    //   #525	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	15	18	finally
  }
  
  void nAllocationCopyFromBitmap(long paramLong, Bitmap paramBitmap) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: aload_3
    //   13: invokevirtual rsnAllocationCopyFromBitmap : (JJLandroid/graphics/Bitmap;)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_3
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_3
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #531	-> 2
    //   #532	-> 6
    //   #533	-> 16
    //   #530	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	6	19	finally
    //   6	16	19	finally
  }
  
  void nAllocationData1D(long paramLong, int paramInt1, int paramInt2, int paramInt3, Object paramObject, int paramInt4, Element.DataType paramDataType, int paramInt5, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: iload #5
    //   17: aload #6
    //   19: iload #7
    //   21: aload #8
    //   23: getfield mID : I
    //   26: iload #9
    //   28: iload #10
    //   30: invokevirtual rsnAllocationData1D : (JJIIILjava/lang/Object;IIIZ)V
    //   33: aload_0
    //   34: monitorexit
    //   35: return
    //   36: astore #6
    //   38: aload_0
    //   39: monitorexit
    //   40: aload #6
    //   42: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #540	-> 2
    //   #541	-> 6
    //   #542	-> 33
    //   #539	-> 36
    // Exception table:
    //   from	to	target	type
    //   2	6	36	finally
    //   6	33	36	finally
  }
  
  void nAllocationElementData(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, byte[] paramArrayOfbyte, int paramInt6) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: iload #5
    //   17: iload #6
    //   19: iload #7
    //   21: aload #8
    //   23: iload #9
    //   25: invokevirtual rsnAllocationElementData : (JJIIIII[BI)V
    //   28: aload_0
    //   29: monitorexit
    //   30: return
    //   31: astore #8
    //   33: aload_0
    //   34: monitorexit
    //   35: aload #8
    //   37: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #546	-> 2
    //   #547	-> 6
    //   #548	-> 28
    //   #545	-> 31
    // Exception table:
    //   from	to	target	type
    //   2	6	31	finally
    //   6	28	31	finally
  }
  
  void nAllocationData2D(long paramLong1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, long paramLong2, int paramInt7, int paramInt8, int paramInt9, int paramInt10) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: iload #5
    //   17: iload #6
    //   19: iload #7
    //   21: iload #8
    //   23: lload #9
    //   25: iload #11
    //   27: iload #12
    //   29: iload #13
    //   31: iload #14
    //   33: invokevirtual rsnAllocationData2D : (JJIIIIIIJIIII)V
    //   36: aload_0
    //   37: monitorexit
    //   38: return
    //   39: astore #15
    //   41: aload_0
    //   42: monitorexit
    //   43: aload #15
    //   45: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #561	-> 2
    //   #562	-> 6
    //   #568	-> 36
    //   #560	-> 39
    // Exception table:
    //   from	to	target	type
    //   2	6	39	finally
    //   6	36	39	finally
  }
  
  void nAllocationData2D(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, Object paramObject, int paramInt7, Element.DataType paramDataType, int paramInt8, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: iload #5
    //   17: iload #6
    //   19: iload #7
    //   21: iload #8
    //   23: aload #9
    //   25: iload #10
    //   27: aload #11
    //   29: getfield mID : I
    //   32: iload #12
    //   34: iload #13
    //   36: invokevirtual rsnAllocationData2D : (JJIIIIIILjava/lang/Object;IIIZ)V
    //   39: aload_0
    //   40: monitorexit
    //   41: return
    //   42: astore #9
    //   44: aload_0
    //   45: monitorexit
    //   46: aload #9
    //   48: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #576	-> 2
    //   #577	-> 6
    //   #578	-> 39
    //   #575	-> 42
    // Exception table:
    //   from	to	target	type
    //   2	6	42	finally
    //   6	39	42	finally
  }
  
  void nAllocationData2D(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Bitmap paramBitmap) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: iload #5
    //   17: iload #6
    //   19: aload #7
    //   21: invokevirtual rsnAllocationData2D : (JJIIIILandroid/graphics/Bitmap;)V
    //   24: aload_0
    //   25: monitorexit
    //   26: return
    //   27: astore #7
    //   29: aload_0
    //   30: monitorexit
    //   31: aload #7
    //   33: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #582	-> 2
    //   #583	-> 6
    //   #584	-> 24
    //   #581	-> 27
    // Exception table:
    //   from	to	target	type
    //   2	6	27	finally
    //   6	24	27	finally
  }
  
  void nAllocationData3D(long paramLong1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, long paramLong2, int paramInt8, int paramInt9, int paramInt10, int paramInt11) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: iload #5
    //   17: iload #6
    //   19: iload #7
    //   21: iload #8
    //   23: iload #9
    //   25: lload #10
    //   27: iload #12
    //   29: iload #13
    //   31: iload #14
    //   33: iload #15
    //   35: invokevirtual rsnAllocationData3D : (JJIIIIIIIJIIII)V
    //   38: aload_0
    //   39: monitorexit
    //   40: return
    //   41: astore #16
    //   43: aload_0
    //   44: monitorexit
    //   45: aload #16
    //   47: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #597	-> 2
    //   #598	-> 6
    //   #602	-> 38
    //   #596	-> 41
    // Exception table:
    //   from	to	target	type
    //   2	6	41	finally
    //   6	38	41	finally
  }
  
  void nAllocationData3D(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, Object paramObject, int paramInt8, Element.DataType paramDataType, int paramInt9, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: iload #5
    //   17: iload #6
    //   19: iload #7
    //   21: iload #8
    //   23: iload #9
    //   25: aload #10
    //   27: iload #11
    //   29: aload #12
    //   31: getfield mID : I
    //   34: iload #13
    //   36: iload #14
    //   38: invokevirtual rsnAllocationData3D : (JJIIIIIIILjava/lang/Object;IIIZ)V
    //   41: aload_0
    //   42: monitorexit
    //   43: return
    //   44: astore #10
    //   46: aload_0
    //   47: monitorexit
    //   48: aload #10
    //   50: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #610	-> 2
    //   #611	-> 6
    //   #613	-> 41
    //   #609	-> 44
    // Exception table:
    //   from	to	target	type
    //   2	6	44	finally
    //   6	41	44	finally
  }
  
  void nAllocationRead(long paramLong, Object paramObject, Element.DataType paramDataType, int paramInt, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: aload_3
    //   13: aload #4
    //   15: getfield mID : I
    //   18: iload #5
    //   20: iload #6
    //   22: invokevirtual rsnAllocationRead : (JJLjava/lang/Object;IIZ)V
    //   25: aload_0
    //   26: monitorexit
    //   27: return
    //   28: astore_3
    //   29: aload_0
    //   30: monitorexit
    //   31: aload_3
    //   32: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #617	-> 2
    //   #618	-> 6
    //   #619	-> 25
    //   #616	-> 28
    // Exception table:
    //   from	to	target	type
    //   2	6	28	finally
    //   6	25	28	finally
  }
  
  void nAllocationRead1D(long paramLong, int paramInt1, int paramInt2, int paramInt3, Object paramObject, int paramInt4, Element.DataType paramDataType, int paramInt5, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: iload #5
    //   17: aload #6
    //   19: iload #7
    //   21: aload #8
    //   23: getfield mID : I
    //   26: iload #9
    //   28: iload #10
    //   30: invokevirtual rsnAllocationRead1D : (JJIIILjava/lang/Object;IIIZ)V
    //   33: aload_0
    //   34: monitorexit
    //   35: return
    //   36: astore #6
    //   38: aload_0
    //   39: monitorexit
    //   40: aload #6
    //   42: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #625	-> 2
    //   #626	-> 6
    //   #627	-> 33
    //   #624	-> 36
    // Exception table:
    //   from	to	target	type
    //   2	6	36	finally
    //   6	33	36	finally
  }
  
  void nAllocationElementRead(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, byte[] paramArrayOfbyte, int paramInt6) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: iload #5
    //   17: iload #6
    //   19: iload #7
    //   21: aload #8
    //   23: iload #9
    //   25: invokevirtual rsnAllocationElementRead : (JJIIIII[BI)V
    //   28: aload_0
    //   29: monitorexit
    //   30: return
    //   31: astore #8
    //   33: aload_0
    //   34: monitorexit
    //   35: aload #8
    //   37: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #633	-> 2
    //   #634	-> 6
    //   #635	-> 28
    //   #632	-> 31
    // Exception table:
    //   from	to	target	type
    //   2	6	31	finally
    //   6	28	31	finally
  }
  
  void nAllocationRead2D(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, Object paramObject, int paramInt7, Element.DataType paramDataType, int paramInt8, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: iload #5
    //   17: iload #6
    //   19: iload #7
    //   21: iload #8
    //   23: aload #9
    //   25: iload #10
    //   27: aload #11
    //   29: getfield mID : I
    //   32: iload #12
    //   34: iload #13
    //   36: invokevirtual rsnAllocationRead2D : (JJIIIIIILjava/lang/Object;IIIZ)V
    //   39: aload_0
    //   40: monitorexit
    //   41: return
    //   42: astore #9
    //   44: aload_0
    //   45: monitorexit
    //   46: aload #9
    //   48: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #643	-> 2
    //   #644	-> 6
    //   #645	-> 39
    //   #642	-> 42
    // Exception table:
    //   from	to	target	type
    //   2	6	42	finally
    //   6	39	42	finally
  }
  
  void nAllocationRead3D(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, Object paramObject, int paramInt8, Element.DataType paramDataType, int paramInt9, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: iload #5
    //   17: iload #6
    //   19: iload #7
    //   21: iload #8
    //   23: iload #9
    //   25: aload #10
    //   27: iload #11
    //   29: aload #12
    //   31: getfield mID : I
    //   34: iload #13
    //   36: iload #14
    //   38: invokevirtual rsnAllocationRead3D : (JJIIIIIIILjava/lang/Object;IIIZ)V
    //   41: aload_0
    //   42: monitorexit
    //   43: return
    //   44: astore #10
    //   46: aload_0
    //   47: monitorexit
    //   48: aload #10
    //   50: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #653	-> 2
    //   #654	-> 6
    //   #655	-> 41
    //   #652	-> 44
    // Exception table:
    //   from	to	target	type
    //   2	6	44	finally
    //   6	41	44	finally
  }
  
  long nAllocationGetType(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: invokevirtual rsnAllocationGetType : (JJ)J
    //   15: lstore_1
    //   16: aload_0
    //   17: monitorexit
    //   18: lload_1
    //   19: lreturn
    //   20: astore_3
    //   21: aload_0
    //   22: monitorexit
    //   23: aload_3
    //   24: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #659	-> 2
    //   #660	-> 6
    //   #658	-> 20
    // Exception table:
    //   from	to	target	type
    //   2	6	20	finally
    //   6	16	20	finally
  }
  
  void nAllocationResize1D(long paramLong, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: invokevirtual rsnAllocationResize1D : (JJI)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore #4
    //   21: aload_0
    //   22: monitorexit
    //   23: aload #4
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #665	-> 2
    //   #666	-> 6
    //   #667	-> 16
    //   #664	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	6	19	finally
    //   6	16	19	finally
  }
  
  long nAllocationAdapterCreate(long paramLong1, long paramLong2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: lload_3
    //   13: invokevirtual rsnAllocationAdapterCreate : (JJJ)J
    //   16: lstore_1
    //   17: aload_0
    //   18: monitorexit
    //   19: lload_1
    //   20: lreturn
    //   21: astore #5
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #5
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #671	-> 2
    //   #672	-> 6
    //   #670	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	17	21	finally
  }
  
  void nAllocationAdapterOffset(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: iload #5
    //   17: iload #6
    //   19: iload #7
    //   21: iload #8
    //   23: iload #9
    //   25: iload #10
    //   27: iload #11
    //   29: invokevirtual rsnAllocationAdapterOffset : (JJIIIIIIIII)V
    //   32: aload_0
    //   33: monitorexit
    //   34: return
    //   35: astore #12
    //   37: aload_0
    //   38: monitorexit
    //   39: aload #12
    //   41: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #679	-> 2
    //   #680	-> 6
    //   #681	-> 32
    //   #678	-> 35
    // Exception table:
    //   from	to	target	type
    //   2	6	35	finally
    //   6	32	35	finally
  }
  
  long nFileA3DCreateFromAssetStream(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: invokevirtual rsnFileA3DCreateFromAssetStream : (JJ)J
    //   15: lstore_1
    //   16: aload_0
    //   17: monitorexit
    //   18: lload_1
    //   19: lreturn
    //   20: astore_3
    //   21: aload_0
    //   22: monitorexit
    //   23: aload_3
    //   24: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #685	-> 2
    //   #686	-> 6
    //   #684	-> 20
    // Exception table:
    //   from	to	target	type
    //   2	6	20	finally
    //   6	16	20	finally
  }
  
  long nFileA3DCreateFromFile(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: aload_1
    //   12: invokevirtual rsnFileA3DCreateFromFile : (JLjava/lang/String;)J
    //   15: lstore_2
    //   16: aload_0
    //   17: monitorexit
    //   18: lload_2
    //   19: lreturn
    //   20: astore_1
    //   21: aload_0
    //   22: monitorexit
    //   23: aload_1
    //   24: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #690	-> 2
    //   #691	-> 6
    //   #689	-> 20
    // Exception table:
    //   from	to	target	type
    //   2	6	20	finally
    //   6	16	20	finally
  }
  
  long nFileA3DCreateFromAsset(AssetManager paramAssetManager, String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: aload_1
    //   12: aload_2
    //   13: invokevirtual rsnFileA3DCreateFromAsset : (JLandroid/content/res/AssetManager;Ljava/lang/String;)J
    //   16: lstore_3
    //   17: aload_0
    //   18: monitorexit
    //   19: lload_3
    //   20: lreturn
    //   21: astore_1
    //   22: aload_0
    //   23: monitorexit
    //   24: aload_1
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #695	-> 2
    //   #696	-> 6
    //   #694	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	17	21	finally
  }
  
  int nFileA3DGetNumIndexEntries(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: invokevirtual rsnFileA3DGetNumIndexEntries : (JJ)I
    //   15: istore_3
    //   16: aload_0
    //   17: monitorexit
    //   18: iload_3
    //   19: ireturn
    //   20: astore #4
    //   22: aload_0
    //   23: monitorexit
    //   24: aload #4
    //   26: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #700	-> 2
    //   #701	-> 6
    //   #699	-> 20
    // Exception table:
    //   from	to	target	type
    //   2	6	20	finally
    //   6	16	20	finally
  }
  
  void nFileA3DGetIndexEntries(long paramLong, int paramInt, int[] paramArrayOfint, String[] paramArrayOfString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: aload #4
    //   15: aload #5
    //   17: invokevirtual rsnFileA3DGetIndexEntries : (JJI[I[Ljava/lang/String;)V
    //   20: aload_0
    //   21: monitorexit
    //   22: return
    //   23: astore #4
    //   25: aload_0
    //   26: monitorexit
    //   27: aload #4
    //   29: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #705	-> 2
    //   #706	-> 6
    //   #707	-> 20
    //   #704	-> 23
    // Exception table:
    //   from	to	target	type
    //   2	6	23	finally
    //   6	20	23	finally
  }
  
  long nFileA3DGetEntryByIndex(long paramLong, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: invokevirtual rsnFileA3DGetEntryByIndex : (JJI)J
    //   16: lstore_1
    //   17: aload_0
    //   18: monitorexit
    //   19: lload_1
    //   20: lreturn
    //   21: astore #4
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #4
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #710	-> 2
    //   #711	-> 6
    //   #709	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	17	21	finally
  }
  
  long nFontCreateFromFile(String paramString, float paramFloat, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: aload_1
    //   12: fload_2
    //   13: iload_3
    //   14: invokevirtual rsnFontCreateFromFile : (JLjava/lang/String;FI)J
    //   17: lstore #4
    //   19: aload_0
    //   20: monitorexit
    //   21: lload #4
    //   23: lreturn
    //   24: astore_1
    //   25: aload_0
    //   26: monitorexit
    //   27: aload_1
    //   28: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #716	-> 2
    //   #717	-> 6
    //   #715	-> 24
    // Exception table:
    //   from	to	target	type
    //   2	6	24	finally
    //   6	19	24	finally
  }
  
  long nFontCreateFromAssetStream(String paramString, float paramFloat, int paramInt, long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: aload_1
    //   12: fload_2
    //   13: iload_3
    //   14: lload #4
    //   16: invokevirtual rsnFontCreateFromAssetStream : (JLjava/lang/String;FIJ)J
    //   19: lstore #4
    //   21: aload_0
    //   22: monitorexit
    //   23: lload #4
    //   25: lreturn
    //   26: astore_1
    //   27: aload_0
    //   28: monitorexit
    //   29: aload_1
    //   30: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #721	-> 2
    //   #722	-> 6
    //   #720	-> 26
    // Exception table:
    //   from	to	target	type
    //   2	6	26	finally
    //   6	21	26	finally
  }
  
  long nFontCreateFromAsset(AssetManager paramAssetManager, String paramString, float paramFloat, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: aload_1
    //   12: aload_2
    //   13: fload_3
    //   14: iload #4
    //   16: invokevirtual rsnFontCreateFromAsset : (JLandroid/content/res/AssetManager;Ljava/lang/String;FI)J
    //   19: lstore #5
    //   21: aload_0
    //   22: monitorexit
    //   23: lload #5
    //   25: lreturn
    //   26: astore_1
    //   27: aload_0
    //   28: monitorexit
    //   29: aload_1
    //   30: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #726	-> 2
    //   #727	-> 6
    //   #725	-> 26
    // Exception table:
    //   from	to	target	type
    //   2	6	26	finally
    //   6	21	26	finally
  }
  
  void nScriptBindAllocation(long paramLong1, long paramLong2, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: lload_3
    //   13: iload #5
    //   15: invokevirtual rsnScriptBindAllocation : (JJJI)V
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: astore #6
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #6
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #733	-> 2
    //   #734	-> 6
    //   #735	-> 18
    //   #732	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	18	21	finally
  }
  
  void nScriptSetTimeZone(long paramLong, byte[] paramArrayOfbyte) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: aload_3
    //   13: invokevirtual rsnScriptSetTimeZone : (JJ[B)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_3
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_3
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #738	-> 2
    //   #739	-> 6
    //   #740	-> 16
    //   #737	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	6	19	finally
    //   6	16	19	finally
  }
  
  void nScriptInvoke(long paramLong, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: invokevirtual rsnScriptInvoke : (JJI)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore #4
    //   21: aload_0
    //   22: monitorexit
    //   23: aload #4
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #743	-> 2
    //   #744	-> 6
    //   #745	-> 16
    //   #742	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	6	19	finally
    //   6	16	19	finally
  }
  
  void nScriptForEach(long paramLong1, int paramInt, long[] paramArrayOflong, long paramLong2, byte[] paramArrayOfbyte, int[] paramArrayOfint) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: aload #4
    //   15: lload #5
    //   17: aload #7
    //   19: aload #8
    //   21: invokevirtual rsnScriptForEach : (JJI[JJ[B[I)V
    //   24: aload_0
    //   25: monitorexit
    //   26: return
    //   27: astore #4
    //   29: aload_0
    //   30: monitorexit
    //   31: aload #4
    //   33: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #752	-> 2
    //   #753	-> 6
    //   #754	-> 24
    //   #751	-> 27
    // Exception table:
    //   from	to	target	type
    //   2	6	27	finally
    //   6	24	27	finally
  }
  
  void nScriptReduce(long paramLong1, int paramInt, long[] paramArrayOflong, long paramLong2, int[] paramArrayOfint) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: aload #4
    //   15: lload #5
    //   17: aload #7
    //   19: invokevirtual rsnScriptReduce : (JJI[JJ[I)V
    //   22: aload_0
    //   23: monitorexit
    //   24: return
    //   25: astore #4
    //   27: aload_0
    //   28: monitorexit
    //   29: aload #4
    //   31: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #760	-> 2
    //   #761	-> 6
    //   #762	-> 22
    //   #759	-> 25
    // Exception table:
    //   from	to	target	type
    //   2	6	25	finally
    //   6	22	25	finally
  }
  
  void nScriptInvokeV(long paramLong, int paramInt, byte[] paramArrayOfbyte) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: aload #4
    //   15: invokevirtual rsnScriptInvokeV : (JJI[B)V
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: astore #4
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #4
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #766	-> 2
    //   #767	-> 6
    //   #768	-> 18
    //   #765	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	18	21	finally
  }
  
  void nScriptSetVarI(long paramLong, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: invokevirtual rsnScriptSetVarI : (JJII)V
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: astore #5
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #5
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #772	-> 2
    //   #773	-> 6
    //   #774	-> 18
    //   #771	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	18	21	finally
  }
  
  int nScriptGetVarI(long paramLong, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: invokevirtual rsnScriptGetVarI : (JJI)I
    //   16: istore_3
    //   17: aload_0
    //   18: monitorexit
    //   19: iload_3
    //   20: ireturn
    //   21: astore #4
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #4
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #777	-> 2
    //   #778	-> 6
    //   #776	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	17	21	finally
  }
  
  void nScriptSetVarJ(long paramLong1, int paramInt, long paramLong2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: lload #4
    //   15: invokevirtual rsnScriptSetVarJ : (JJIJ)V
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: astore #6
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #6
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #783	-> 2
    //   #784	-> 6
    //   #785	-> 18
    //   #782	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	18	21	finally
  }
  
  long nScriptGetVarJ(long paramLong, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: invokevirtual rsnScriptGetVarJ : (JJI)J
    //   16: lstore_1
    //   17: aload_0
    //   18: monitorexit
    //   19: lload_1
    //   20: lreturn
    //   21: astore #4
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #4
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #788	-> 2
    //   #789	-> 6
    //   #787	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	17	21	finally
  }
  
  void nScriptSetVarF(long paramLong, int paramInt, float paramFloat) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: fload #4
    //   15: invokevirtual rsnScriptSetVarF : (JJIF)V
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: astore #5
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #5
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #794	-> 2
    //   #795	-> 6
    //   #796	-> 18
    //   #793	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	18	21	finally
  }
  
  float nScriptGetVarF(long paramLong, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: invokevirtual rsnScriptGetVarF : (JJI)F
    //   16: fstore #4
    //   18: aload_0
    //   19: monitorexit
    //   20: fload #4
    //   22: freturn
    //   23: astore #5
    //   25: aload_0
    //   26: monitorexit
    //   27: aload #5
    //   29: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #799	-> 2
    //   #800	-> 6
    //   #798	-> 23
    // Exception table:
    //   from	to	target	type
    //   2	6	23	finally
    //   6	18	23	finally
  }
  
  void nScriptSetVarD(long paramLong, int paramInt, double paramDouble) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: dload #4
    //   15: invokevirtual rsnScriptSetVarD : (JJID)V
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: astore #6
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #6
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #804	-> 2
    //   #805	-> 6
    //   #806	-> 18
    //   #803	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	18	21	finally
  }
  
  double nScriptGetVarD(long paramLong, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: invokevirtual rsnScriptGetVarD : (JJI)D
    //   16: dstore #4
    //   18: aload_0
    //   19: monitorexit
    //   20: dload #4
    //   22: dreturn
    //   23: astore #6
    //   25: aload_0
    //   26: monitorexit
    //   27: aload #6
    //   29: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #809	-> 2
    //   #810	-> 6
    //   #808	-> 23
    // Exception table:
    //   from	to	target	type
    //   2	6	23	finally
    //   6	18	23	finally
  }
  
  void nScriptSetVarV(long paramLong, int paramInt, byte[] paramArrayOfbyte) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: aload #4
    //   15: invokevirtual rsnScriptSetVarV : (JJI[B)V
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: astore #4
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #4
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #814	-> 2
    //   #815	-> 6
    //   #816	-> 18
    //   #813	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	18	21	finally
  }
  
  void nScriptGetVarV(long paramLong, int paramInt, byte[] paramArrayOfbyte) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: aload #4
    //   15: invokevirtual rsnScriptGetVarV : (JJI[B)V
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: astore #4
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #4
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #819	-> 2
    //   #820	-> 6
    //   #821	-> 18
    //   #818	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	18	21	finally
  }
  
  void nScriptSetVarVE(long paramLong1, int paramInt, byte[] paramArrayOfbyte, long paramLong2, int[] paramArrayOfint) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: aload #4
    //   15: lload #5
    //   17: aload #7
    //   19: invokevirtual rsnScriptSetVarVE : (JJI[BJ[I)V
    //   22: aload_0
    //   23: monitorexit
    //   24: return
    //   25: astore #4
    //   27: aload_0
    //   28: monitorexit
    //   29: aload #4
    //   31: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #826	-> 2
    //   #827	-> 6
    //   #828	-> 22
    //   #825	-> 25
    // Exception table:
    //   from	to	target	type
    //   2	6	25	finally
    //   6	22	25	finally
  }
  
  void nScriptSetVarObj(long paramLong1, int paramInt, long paramLong2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: lload #4
    //   15: invokevirtual rsnScriptSetVarObj : (JJIJ)V
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: astore #6
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #6
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #831	-> 2
    //   #832	-> 6
    //   #833	-> 18
    //   #830	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	18	21	finally
  }
  
  long nScriptCCreate(String paramString1, String paramString2, byte[] paramArrayOfbyte, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: aload_1
    //   12: aload_2
    //   13: aload_3
    //   14: iload #4
    //   16: invokevirtual rsnScriptCCreate : (JLjava/lang/String;Ljava/lang/String;[BI)J
    //   19: lstore #5
    //   21: aload_0
    //   22: monitorexit
    //   23: lload #5
    //   25: lreturn
    //   26: astore_1
    //   27: aload_0
    //   28: monitorexit
    //   29: aload_1
    //   30: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #839	-> 2
    //   #840	-> 6
    //   #838	-> 26
    // Exception table:
    //   from	to	target	type
    //   2	6	26	finally
    //   6	21	26	finally
  }
  
  long nScriptIntrinsicCreate(int paramInt, long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: iload_1
    //   12: lload_2
    //   13: invokevirtual rsnScriptIntrinsicCreate : (JIJ)J
    //   16: lstore_2
    //   17: aload_0
    //   18: monitorexit
    //   19: lload_2
    //   20: lreturn
    //   21: astore #4
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #4
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #845	-> 2
    //   #846	-> 6
    //   #844	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	17	21	finally
  }
  
  long nScriptKernelIDCreate(long paramLong, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: invokevirtual rsnScriptKernelIDCreate : (JJII)J
    //   18: lstore_1
    //   19: aload_0
    //   20: monitorexit
    //   21: lload_1
    //   22: lreturn
    //   23: astore #5
    //   25: aload_0
    //   26: monitorexit
    //   27: aload #5
    //   29: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #851	-> 2
    //   #852	-> 6
    //   #850	-> 23
    // Exception table:
    //   from	to	target	type
    //   2	6	23	finally
    //   6	19	23	finally
  }
  
  long nScriptInvokeIDCreate(long paramLong, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: invokevirtual rsnScriptInvokeIDCreate : (JJI)J
    //   16: lstore_1
    //   17: aload_0
    //   18: monitorexit
    //   19: lload_1
    //   20: lreturn
    //   21: astore #4
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #4
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #857	-> 2
    //   #858	-> 6
    //   #856	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	17	21	finally
  }
  
  long nScriptFieldIDCreate(long paramLong, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: invokevirtual rsnScriptFieldIDCreate : (JJI)J
    //   16: lstore_1
    //   17: aload_0
    //   18: monitorexit
    //   19: lload_1
    //   20: lreturn
    //   21: astore #4
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #4
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #863	-> 2
    //   #864	-> 6
    //   #862	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	17	21	finally
  }
  
  long nScriptGroupCreate(long[] paramArrayOflong1, long[] paramArrayOflong2, long[] paramArrayOflong3, long[] paramArrayOflong4, long[] paramArrayOflong5) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: aload_1
    //   12: aload_2
    //   13: aload_3
    //   14: aload #4
    //   16: aload #5
    //   18: invokevirtual rsnScriptGroupCreate : (J[J[J[J[J[J)J
    //   21: lstore #6
    //   23: aload_0
    //   24: monitorexit
    //   25: lload #6
    //   27: lreturn
    //   28: astore_1
    //   29: aload_0
    //   30: monitorexit
    //   31: aload_1
    //   32: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #869	-> 2
    //   #870	-> 6
    //   #868	-> 28
    // Exception table:
    //   from	to	target	type
    //   2	6	28	finally
    //   6	23	28	finally
  }
  
  void nScriptGroupSetInput(long paramLong1, long paramLong2, long paramLong3) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: lload_3
    //   13: lload #5
    //   15: invokevirtual rsnScriptGroupSetInput : (JJJJ)V
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: astore #7
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #7
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #875	-> 2
    //   #876	-> 6
    //   #877	-> 18
    //   #874	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	18	21	finally
  }
  
  void nScriptGroupSetOutput(long paramLong1, long paramLong2, long paramLong3) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: lload_3
    //   13: lload #5
    //   15: invokevirtual rsnScriptGroupSetOutput : (JJJJ)V
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: astore #7
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #7
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #881	-> 2
    //   #882	-> 6
    //   #883	-> 18
    //   #880	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	18	21	finally
  }
  
  void nScriptGroupExecute(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: invokevirtual rsnScriptGroupExecute : (JJ)V
    //   15: aload_0
    //   16: monitorexit
    //   17: return
    //   18: astore_3
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_3
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #887	-> 2
    //   #888	-> 6
    //   #889	-> 15
    //   #886	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	15	18	finally
  }
  
  long nSamplerCreate(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float paramFloat) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: iload_1
    //   12: iload_2
    //   13: iload_3
    //   14: iload #4
    //   16: iload #5
    //   18: fload #6
    //   20: invokevirtual rsnSamplerCreate : (JIIIIIF)J
    //   23: lstore #7
    //   25: aload_0
    //   26: monitorexit
    //   27: lload #7
    //   29: lreturn
    //   30: astore #9
    //   32: aload_0
    //   33: monitorexit
    //   34: aload #9
    //   36: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #895	-> 2
    //   #896	-> 6
    //   #894	-> 30
    // Exception table:
    //   from	to	target	type
    //   2	6	30	finally
    //   6	25	30	finally
  }
  
  long nProgramStoreCreate(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, int paramInt1, int paramInt2, int paramInt3) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: iload_1
    //   12: iload_2
    //   13: iload_3
    //   14: iload #4
    //   16: iload #5
    //   18: iload #6
    //   20: iload #7
    //   22: iload #8
    //   24: iload #9
    //   26: invokevirtual rsnProgramStoreCreate : (JZZZZZZIII)J
    //   29: lstore #10
    //   31: aload_0
    //   32: monitorexit
    //   33: lload #10
    //   35: lreturn
    //   36: astore #12
    //   38: aload_0
    //   39: monitorexit
    //   40: aload #12
    //   42: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #905	-> 2
    //   #906	-> 6
    //   #904	-> 36
    // Exception table:
    //   from	to	target	type
    //   2	6	36	finally
    //   6	31	36	finally
  }
  
  long nProgramRasterCreate(boolean paramBoolean, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: iload_1
    //   12: iload_2
    //   13: invokevirtual rsnProgramRasterCreate : (JZI)J
    //   16: lstore_3
    //   17: aload_0
    //   18: monitorexit
    //   19: lload_3
    //   20: lreturn
    //   21: astore #5
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #5
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #912	-> 2
    //   #913	-> 6
    //   #911	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	17	21	finally
  }
  
  void nProgramBindConstants(long paramLong1, int paramInt, long paramLong2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: lload #4
    //   15: invokevirtual rsnProgramBindConstants : (JJIJ)V
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: astore #6
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #6
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #918	-> 2
    //   #919	-> 6
    //   #920	-> 18
    //   #917	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	18	21	finally
  }
  
  void nProgramBindTexture(long paramLong1, int paramInt, long paramLong2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: lload #4
    //   15: invokevirtual rsnProgramBindTexture : (JJIJ)V
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: astore #6
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #6
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #923	-> 2
    //   #924	-> 6
    //   #925	-> 18
    //   #922	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	18	21	finally
  }
  
  void nProgramBindSampler(long paramLong1, int paramInt, long paramLong2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: lload #4
    //   15: invokevirtual rsnProgramBindSampler : (JJIJ)V
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: astore #6
    //   23: aload_0
    //   24: monitorexit
    //   25: aload #6
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #928	-> 2
    //   #929	-> 6
    //   #930	-> 18
    //   #927	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	18	21	finally
  }
  
  long nProgramFragmentCreate(String paramString, String[] paramArrayOfString, long[] paramArrayOflong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: aload_1
    //   12: aload_2
    //   13: aload_3
    //   14: invokevirtual rsnProgramFragmentCreate : (JLjava/lang/String;[Ljava/lang/String;[J)J
    //   17: lstore #4
    //   19: aload_0
    //   20: monitorexit
    //   21: lload #4
    //   23: lreturn
    //   24: astore_1
    //   25: aload_0
    //   26: monitorexit
    //   27: aload_1
    //   28: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #933	-> 2
    //   #934	-> 6
    //   #932	-> 24
    // Exception table:
    //   from	to	target	type
    //   2	6	24	finally
    //   6	19	24	finally
  }
  
  long nProgramVertexCreate(String paramString, String[] paramArrayOfString, long[] paramArrayOflong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: aload_1
    //   12: aload_2
    //   13: aload_3
    //   14: invokevirtual rsnProgramVertexCreate : (JLjava/lang/String;[Ljava/lang/String;[J)J
    //   17: lstore #4
    //   19: aload_0
    //   20: monitorexit
    //   21: lload #4
    //   23: lreturn
    //   24: astore_1
    //   25: aload_0
    //   26: monitorexit
    //   27: aload_1
    //   28: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #938	-> 2
    //   #939	-> 6
    //   #937	-> 24
    // Exception table:
    //   from	to	target	type
    //   2	6	24	finally
    //   6	19	24	finally
  }
  
  long nMeshCreate(long[] paramArrayOflong1, long[] paramArrayOflong2, int[] paramArrayOfint) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: aload_1
    //   12: aload_2
    //   13: aload_3
    //   14: invokevirtual rsnMeshCreate : (J[J[J[I)J
    //   17: lstore #4
    //   19: aload_0
    //   20: monitorexit
    //   21: lload #4
    //   23: lreturn
    //   24: astore_1
    //   25: aload_0
    //   26: monitorexit
    //   27: aload_1
    //   28: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #944	-> 2
    //   #945	-> 6
    //   #943	-> 24
    // Exception table:
    //   from	to	target	type
    //   2	6	24	finally
    //   6	19	24	finally
  }
  
  int nMeshGetVertexBufferCount(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: invokevirtual rsnMeshGetVertexBufferCount : (JJ)I
    //   15: istore_3
    //   16: aload_0
    //   17: monitorexit
    //   18: iload_3
    //   19: ireturn
    //   20: astore #4
    //   22: aload_0
    //   23: monitorexit
    //   24: aload #4
    //   26: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #949	-> 2
    //   #950	-> 6
    //   #948	-> 20
    // Exception table:
    //   from	to	target	type
    //   2	6	20	finally
    //   6	16	20	finally
  }
  
  int nMeshGetIndexCount(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: invokevirtual rsnMeshGetIndexCount : (JJ)I
    //   15: istore_3
    //   16: aload_0
    //   17: monitorexit
    //   18: iload_3
    //   19: ireturn
    //   20: astore #4
    //   22: aload_0
    //   23: monitorexit
    //   24: aload #4
    //   26: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #954	-> 2
    //   #955	-> 6
    //   #953	-> 20
    // Exception table:
    //   from	to	target	type
    //   2	6	20	finally
    //   6	16	20	finally
  }
  
  void nMeshGetVertices(long paramLong, long[] paramArrayOflong, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: aload_3
    //   13: iload #4
    //   15: invokevirtual rsnMeshGetVertices : (JJ[JI)V
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: astore_3
    //   22: aload_0
    //   23: monitorexit
    //   24: aload_3
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #959	-> 2
    //   #960	-> 6
    //   #961	-> 18
    //   #958	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	18	21	finally
  }
  
  void nMeshGetIndices(long paramLong, long[] paramArrayOflong, int[] paramArrayOfint, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: aload_3
    //   13: aload #4
    //   15: iload #5
    //   17: invokevirtual rsnMeshGetIndices : (JJ[J[II)V
    //   20: aload_0
    //   21: monitorexit
    //   22: return
    //   23: astore_3
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_3
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #964	-> 2
    //   #965	-> 6
    //   #966	-> 20
    //   #963	-> 23
    // Exception table:
    //   from	to	target	type
    //   2	6	23	finally
    //   6	20	23	finally
  }
  
  void nScriptIntrinsicBLAS_Single(long paramLong1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, float paramFloat1, long paramLong2, long paramLong3, float paramFloat2, long paramLong4, int paramInt10, int paramInt11, int paramInt12, int paramInt13) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: iload #5
    //   17: iload #6
    //   19: iload #7
    //   21: iload #8
    //   23: iload #9
    //   25: iload #10
    //   27: iload #11
    //   29: fload #12
    //   31: lload #13
    //   33: lload #15
    //   35: fload #17
    //   37: lload #18
    //   39: iload #20
    //   41: iload #21
    //   43: iload #22
    //   45: iload #23
    //   47: invokevirtual rsnScriptIntrinsicBLAS_Single : (JJIIIIIIIIIFJJFJIIII)V
    //   50: aload_0
    //   51: monitorexit
    //   52: return
    //   53: astore #24
    //   55: aload_0
    //   56: monitorexit
    //   57: aload #24
    //   59: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #976	-> 2
    //   #977	-> 6
    //   #978	-> 50
    //   #975	-> 53
    // Exception table:
    //   from	to	target	type
    //   2	6	53	finally
    //   6	50	53	finally
  }
  
  void nScriptIntrinsicBLAS_Double(long paramLong1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, double paramDouble1, long paramLong2, long paramLong3, double paramDouble2, long paramLong4, int paramInt10, int paramInt11, int paramInt12, int paramInt13) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: iload #5
    //   17: iload #6
    //   19: iload #7
    //   21: iload #8
    //   23: iload #9
    //   25: iload #10
    //   27: iload #11
    //   29: dload #12
    //   31: lload #14
    //   33: lload #16
    //   35: dload #18
    //   37: lload #20
    //   39: iload #22
    //   41: iload #23
    //   43: iload #24
    //   45: iload #25
    //   47: invokevirtual rsnScriptIntrinsicBLAS_Double : (JJIIIIIIIIIDJJDJIIII)V
    //   50: aload_0
    //   51: monitorexit
    //   52: return
    //   53: astore #26
    //   55: aload_0
    //   56: monitorexit
    //   57: aload #26
    //   59: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #988	-> 2
    //   #989	-> 6
    //   #990	-> 50
    //   #987	-> 53
    // Exception table:
    //   from	to	target	type
    //   2	6	53	finally
    //   6	50	53	finally
  }
  
  void nScriptIntrinsicBLAS_Complex(long paramLong1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, float paramFloat1, float paramFloat2, long paramLong2, long paramLong3, float paramFloat3, float paramFloat4, long paramLong4, int paramInt10, int paramInt11, int paramInt12, int paramInt13) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: iload #5
    //   17: iload #6
    //   19: iload #7
    //   21: iload #8
    //   23: iload #9
    //   25: iload #10
    //   27: iload #11
    //   29: fload #12
    //   31: fload #13
    //   33: lload #14
    //   35: lload #16
    //   37: fload #18
    //   39: fload #19
    //   41: lload #20
    //   43: iload #22
    //   45: iload #23
    //   47: iload #24
    //   49: iload #25
    //   51: invokevirtual rsnScriptIntrinsicBLAS_Complex : (JJIIIIIIIIIFFJJFFJIIII)V
    //   54: aload_0
    //   55: monitorexit
    //   56: return
    //   57: astore #26
    //   59: aload_0
    //   60: monitorexit
    //   61: aload #26
    //   63: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1000	-> 2
    //   #1001	-> 6
    //   #1002	-> 54
    //   #999	-> 57
    // Exception table:
    //   from	to	target	type
    //   2	6	57	finally
    //   6	54	57	finally
  }
  
  void nScriptIntrinsicBLAS_Z(long paramLong1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, double paramDouble1, double paramDouble2, long paramLong2, long paramLong3, double paramDouble3, double paramDouble4, long paramLong4, int paramInt10, int paramInt11, int paramInt12, int paramInt13) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: iload #5
    //   17: iload #6
    //   19: iload #7
    //   21: iload #8
    //   23: iload #9
    //   25: iload #10
    //   27: iload #11
    //   29: dload #12
    //   31: dload #14
    //   33: lload #16
    //   35: lload #18
    //   37: dload #20
    //   39: dload #22
    //   41: lload #24
    //   43: iload #26
    //   45: iload #27
    //   47: iload #28
    //   49: iload #29
    //   51: invokevirtual rsnScriptIntrinsicBLAS_Z : (JJIIIIIIIIIDDJJDDJIIII)V
    //   54: aload_0
    //   55: monitorexit
    //   56: return
    //   57: astore #30
    //   59: aload_0
    //   60: monitorexit
    //   61: aload #30
    //   63: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1012	-> 2
    //   #1013	-> 6
    //   #1014	-> 54
    //   #1011	-> 57
    // Exception table:
    //   from	to	target	type
    //   2	6	57	finally
    //   6	54	57	finally
  }
  
  void nScriptIntrinsicBLAS_BNNM(long paramLong1, int paramInt1, int paramInt2, int paramInt3, long paramLong2, int paramInt4, long paramLong3, int paramInt5, long paramLong4, int paramInt6, int paramInt7) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual validate : ()V
    //   6: aload_0
    //   7: aload_0
    //   8: getfield mContext : J
    //   11: lload_1
    //   12: iload_3
    //   13: iload #4
    //   15: iload #5
    //   17: lload #6
    //   19: iload #8
    //   21: lload #9
    //   23: iload #11
    //   25: lload #12
    //   27: iload #14
    //   29: iload #15
    //   31: invokevirtual rsnScriptIntrinsicBLAS_BNNM : (JJIIIJIJIJII)V
    //   34: aload_0
    //   35: monitorexit
    //   36: return
    //   37: astore #16
    //   39: aload_0
    //   40: monitorexit
    //   41: aload #16
    //   43: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1022	-> 2
    //   #1023	-> 6
    //   #1024	-> 34
    //   #1021	-> 37
    // Exception table:
    //   from	to	target	type
    //   2	6	37	finally
    //   6	34	37	finally
  }
  
  private boolean mDestroyed = false;
  
  public static class RSMessageHandler implements Runnable {
    protected int[] mData;
    
    protected int mID;
    
    protected int mLength;
    
    public void run() {}
  }
  
  RSMessageHandler mMessageCallback = null;
  
  public void setMessageHandler(RSMessageHandler paramRSMessageHandler) {
    this.mMessageCallback = paramRSMessageHandler;
  }
  
  public RSMessageHandler getMessageHandler() {
    return this.mMessageCallback;
  }
  
  public void sendMessage(int paramInt, int[] paramArrayOfint) {
    nContextSendMessage(paramInt, paramArrayOfint);
  }
  
  public static class RSErrorHandler implements Runnable {
    protected String mErrorMessage;
    
    protected int mErrorNum;
    
    public void run() {}
  }
  
  RSErrorHandler mErrorCallback = null;
  
  public static final int CREATE_FLAG_LOW_LATENCY = 2;
  
  public static final int CREATE_FLAG_LOW_POWER = 4;
  
  public static final int CREATE_FLAG_NONE = 0;
  
  public static final int CREATE_FLAG_WAIT_FOR_ATTACH = 8;
  
  static final boolean DEBUG = false;
  
  static final boolean LOG_ENABLED = false;
  
  static final String LOG_TAG = "RenderScript_jni";
  
  static final long TRACE_TAG = 32768L;
  
  private static String mCachePath;
  
  static Method registerNativeAllocation;
  
  static Method registerNativeFree;
  
  static final long sMinorVersion = 1L;
  
  static int sPointerSize;
  
  static Object sRuntime;
  
  private Context mApplicationContext;
  
  long mContext;
  
  ContextType mContextType;
  
  volatile Element mElement_ALLOCATION;
  
  volatile Element mElement_A_8;
  
  volatile Element mElement_BOOLEAN;
  
  volatile Element mElement_CHAR_2;
  
  volatile Element mElement_CHAR_3;
  
  volatile Element mElement_CHAR_4;
  
  volatile Element mElement_DOUBLE_2;
  
  volatile Element mElement_DOUBLE_3;
  
  volatile Element mElement_DOUBLE_4;
  
  volatile Element mElement_ELEMENT;
  
  volatile Element mElement_F16;
  
  volatile Element mElement_F32;
  
  volatile Element mElement_F64;
  
  volatile Element mElement_FLOAT_2;
  
  volatile Element mElement_FLOAT_3;
  
  volatile Element mElement_FLOAT_4;
  
  volatile Element mElement_FONT;
  
  volatile Element mElement_HALF_2;
  
  volatile Element mElement_HALF_3;
  
  volatile Element mElement_HALF_4;
  
  volatile Element mElement_I16;
  
  volatile Element mElement_I32;
  
  volatile Element mElement_I64;
  
  volatile Element mElement_I8;
  
  volatile Element mElement_INT_2;
  
  volatile Element mElement_INT_3;
  
  volatile Element mElement_INT_4;
  
  volatile Element mElement_LONG_2;
  
  volatile Element mElement_LONG_3;
  
  volatile Element mElement_LONG_4;
  
  volatile Element mElement_MATRIX_2X2;
  
  volatile Element mElement_MATRIX_3X3;
  
  volatile Element mElement_MATRIX_4X4;
  
  volatile Element mElement_MESH;
  
  volatile Element mElement_PROGRAM_FRAGMENT;
  
  volatile Element mElement_PROGRAM_RASTER;
  
  volatile Element mElement_PROGRAM_STORE;
  
  volatile Element mElement_PROGRAM_VERTEX;
  
  volatile Element mElement_RGBA_4444;
  
  volatile Element mElement_RGBA_5551;
  
  volatile Element mElement_RGBA_8888;
  
  volatile Element mElement_RGB_565;
  
  volatile Element mElement_RGB_888;
  
  volatile Element mElement_SAMPLER;
  
  volatile Element mElement_SCRIPT;
  
  volatile Element mElement_SHORT_2;
  
  volatile Element mElement_SHORT_3;
  
  volatile Element mElement_SHORT_4;
  
  volatile Element mElement_TYPE;
  
  volatile Element mElement_U16;
  
  volatile Element mElement_U32;
  
  volatile Element mElement_U64;
  
  volatile Element mElement_U8;
  
  volatile Element mElement_UCHAR_2;
  
  volatile Element mElement_UCHAR_3;
  
  volatile Element mElement_UCHAR_4;
  
  volatile Element mElement_UINT_2;
  
  volatile Element mElement_UINT_3;
  
  volatile Element mElement_UINT_4;
  
  volatile Element mElement_ULONG_2;
  
  volatile Element mElement_ULONG_3;
  
  volatile Element mElement_ULONG_4;
  
  volatile Element mElement_USHORT_2;
  
  volatile Element mElement_USHORT_3;
  
  volatile Element mElement_USHORT_4;
  
  volatile Element mElement_YUV;
  
  MessageThread mMessageThread;
  
  ProgramRaster mProgramRaster_CULL_BACK;
  
  ProgramRaster mProgramRaster_CULL_FRONT;
  
  ProgramRaster mProgramRaster_CULL_NONE;
  
  ProgramStore mProgramStore_BLEND_ALPHA_DEPTH_NO_DEPTH;
  
  ProgramStore mProgramStore_BLEND_ALPHA_DEPTH_TEST;
  
  ProgramStore mProgramStore_BLEND_NONE_DEPTH_NO_DEPTH;
  
  ProgramStore mProgramStore_BLEND_NONE_DEPTH_TEST;
  
  ReentrantReadWriteLock mRWLock;
  
  volatile Sampler mSampler_CLAMP_LINEAR;
  
  volatile Sampler mSampler_CLAMP_LINEAR_MIP_LINEAR;
  
  volatile Sampler mSampler_CLAMP_NEAREST;
  
  volatile Sampler mSampler_MIRRORED_REPEAT_LINEAR;
  
  volatile Sampler mSampler_MIRRORED_REPEAT_LINEAR_MIP_LINEAR;
  
  volatile Sampler mSampler_MIRRORED_REPEAT_NEAREST;
  
  volatile Sampler mSampler_WRAP_LINEAR;
  
  volatile Sampler mSampler_WRAP_LINEAR_MIP_LINEAR;
  
  volatile Sampler mSampler_WRAP_NEAREST;
  
  public void setErrorHandler(RSErrorHandler paramRSErrorHandler) {
    this.mErrorCallback = paramRSErrorHandler;
  }
  
  public RSErrorHandler getErrorHandler() {
    return this.mErrorCallback;
  }
  
  public enum Priority {
    LOW(15),
    NORMAL(15);
    
    private static final Priority[] $VALUES;
    
    int mID;
    
    static {
      Priority priority = new Priority("NORMAL", 1, -8);
      $VALUES = new Priority[] { LOW, priority };
    }
    
    Priority(int param1Int1) {
      this.mID = param1Int1;
    }
  }
  
  void validateObject(BaseObj paramBaseObj) {
    if (paramBaseObj == null || 
      paramBaseObj.mRS == this)
      return; 
    throw new RSIllegalArgumentException("Attempting to use an object across contexts.");
  }
  
  void validate() {
    if (this.mContext != 0L)
      return; 
    throw new RSInvalidStateException("Calling RS with no Context active.");
  }
  
  public void setPriority(Priority paramPriority) {
    validate();
    nContextSetPriority(paramPriority.mID);
  }
  
  static class MessageThread extends Thread {
    boolean mRun = true;
    
    RenderScript mRS;
    
    int[] mAuxData = new int[2];
    
    static final int RS_MESSAGE_TO_CLIENT_USER = 4;
    
    static final int RS_MESSAGE_TO_CLIENT_RESIZE = 2;
    
    static final int RS_MESSAGE_TO_CLIENT_NONE = 0;
    
    static final int RS_MESSAGE_TO_CLIENT_NEW_BUFFER = 5;
    
    static final int RS_MESSAGE_TO_CLIENT_EXCEPTION = 1;
    
    static final int RS_MESSAGE_TO_CLIENT_ERROR = 3;
    
    static final int RS_ERROR_FATAL_UNKNOWN = 4096;
    
    static final int RS_ERROR_FATAL_DEBUG = 2048;
    
    MessageThread(RenderScript param1RenderScript) {
      super("RSMessageThread");
      this.mRS = param1RenderScript;
    }
    
    public void run() {
      int[] arrayOfInt = new int[16];
      RenderScript renderScript = this.mRS;
      renderScript.nContextInitToClient(renderScript.mContext);
      while (this.mRun) {
        StringBuilder stringBuilder;
        arrayOfInt[0] = 0;
        renderScript = this.mRS;
        int i = renderScript.nContextPeekMessage(renderScript.mContext, this.mAuxData);
        int arrayOfInt1[] = this.mAuxData, j = arrayOfInt1[1];
        int k = arrayOfInt1[0];
        if (i == 4) {
          StringBuilder stringBuilder1;
          arrayOfInt1 = arrayOfInt;
          if (j >> 2 >= arrayOfInt.length) {
            i = j + 3 >> 2;
            if (i >= 0) {
              arrayOfInt1 = new int[i];
            } else {
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append("RSMessageThread NegativeArraySize  : workaround!!   size:");
              stringBuilder1.append(j);
              Log.e("RenderScript_jni", stringBuilder1.toString());
              continue;
            } 
          } 
          RenderScript renderScript1 = this.mRS;
          if (renderScript1.nContextGetUserMessage(renderScript1.mContext, (int[])stringBuilder1) == 4) {
            if (this.mRS.mMessageCallback != null) {
              this.mRS.mMessageCallback.mData = (int[])stringBuilder1;
              this.mRS.mMessageCallback.mID = k;
              this.mRS.mMessageCallback.mLength = j;
              this.mRS.mMessageCallback.run();
              stringBuilder = stringBuilder1;
              continue;
            } 
            throw new RSInvalidStateException("Received a message from the script with no message handler installed.");
          } 
          throw new RSDriverException("Error processing message from RenderScript.");
        } 
        if (i == 3) {
          RenderScript renderScript1 = this.mRS;
          String str = renderScript1.nContextGetErrorMessage(renderScript1.mContext);
          if (k < 4096 && (k < 2048 || (this.mRS.mContextType == RenderScript.ContextType.DEBUG && this.mRS.mErrorCallback != null))) {
            if (this.mRS.mErrorCallback != null) {
              this.mRS.mErrorCallback.mErrorMessage = str;
              this.mRS.mErrorCallback.mErrorNum = k;
              this.mRS.mErrorCallback.run();
              continue;
            } 
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("non fatal RS error, ");
            stringBuilder1.append(str);
            Log.e("RenderScript_jni", stringBuilder1.toString());
            continue;
          } 
          stringBuilder = new StringBuilder();
          stringBuilder.append("Fatal error ");
          stringBuilder.append(k);
          stringBuilder.append(", details: ");
          stringBuilder.append(str);
          throw new RSRuntimeException(stringBuilder.toString());
        } 
        if (i == 5) {
          RenderScript renderScript1 = this.mRS;
          if (renderScript1.nContextGetUserMessage(renderScript1.mContext, (int[])stringBuilder) == 5) {
            long l1 = stringBuilder[1], l2 = stringBuilder[0];
            Allocation.sendBufferNotification((l1 << 32L) + (l2 & 0xFFFFFFFFL));
            continue;
          } 
          throw new RSDriverException("Error processing message from RenderScript.");
        } 
        try {
          sleep(1L, 0);
        } catch (InterruptedException interruptedException) {}
      } 
    }
  }
  
  RenderScript(Context paramContext) {
    this.mContextType = ContextType.NORMAL;
    if (paramContext != null)
      this.mApplicationContext = paramContext.getApplicationContext(); 
    this.mRWLock = new ReentrantReadWriteLock();
    try {
      registerNativeAllocation.invoke(sRuntime, new Object[] { Integer.valueOf(4194304) });
      return;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Couldn't invoke registerNativeAllocation:");
      stringBuilder.append(exception);
      Log.e("RenderScript_jni", stringBuilder.toString());
      stringBuilder = new StringBuilder();
      stringBuilder.append("Couldn't invoke registerNativeAllocation:");
      stringBuilder.append(exception);
      throw new RSRuntimeException(stringBuilder.toString());
    } 
  }
  
  public final Context getApplicationContext() {
    return this.mApplicationContext;
  }
  
  static String getCachePath() {
    // Byte code:
    //   0: ldc android/renderscript/RenderScript
    //   2: monitorenter
    //   3: getstatic android/renderscript/RenderScript.mCachePath : Ljava/lang/String;
    //   6: ifnonnull -> 57
    //   9: getstatic android/renderscript/RenderScriptCacheDir.mCacheDir : Ljava/io/File;
    //   12: ifnull -> 44
    //   15: new java/io/File
    //   18: astore_0
    //   19: aload_0
    //   20: getstatic android/renderscript/RenderScriptCacheDir.mCacheDir : Ljava/io/File;
    //   23: ldc_w 'com.android.renderscript.cache'
    //   26: invokespecial <init> : (Ljava/io/File;Ljava/lang/String;)V
    //   29: aload_0
    //   30: invokevirtual getAbsolutePath : ()Ljava/lang/String;
    //   33: putstatic android/renderscript/RenderScript.mCachePath : Ljava/lang/String;
    //   36: aload_0
    //   37: invokevirtual mkdirs : ()Z
    //   40: pop
    //   41: goto -> 57
    //   44: new android/renderscript/RSRuntimeException
    //   47: astore_0
    //   48: aload_0
    //   49: ldc_w 'RenderScript code cache directory uninitialized.'
    //   52: invokespecial <init> : (Ljava/lang/String;)V
    //   55: aload_0
    //   56: athrow
    //   57: getstatic android/renderscript/RenderScript.mCachePath : Ljava/lang/String;
    //   60: astore_0
    //   61: ldc android/renderscript/RenderScript
    //   63: monitorexit
    //   64: aload_0
    //   65: areturn
    //   66: astore_0
    //   67: ldc android/renderscript/RenderScript
    //   69: monitorexit
    //   70: aload_0
    //   71: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1404	-> 3
    //   #1405	-> 9
    //   #1406	-> 9
    //   #1409	-> 15
    //   #1410	-> 29
    //   #1411	-> 36
    //   #1407	-> 44
    //   #1413	-> 57
    //   #1403	-> 66
    // Exception table:
    //   from	to	target	type
    //   3	9	66	finally
    //   9	15	66	finally
    //   15	29	66	finally
    //   29	36	66	finally
    //   36	41	66	finally
    //   44	57	66	finally
    //   57	61	66	finally
  }
  
  private static RenderScript internalCreate(Context paramContext, int paramInt1, ContextType paramContextType, int paramInt2) {
    if (!sInitialized) {
      Log.e("RenderScript_jni", "RenderScript.create() called when disabled; someone is likely to crash");
      return null;
    } 
    if ((paramInt2 & 0xFFFFFFF1) == 0) {
      RenderScript renderScript = new RenderScript(paramContext);
      long l = renderScript.nDeviceCreate();
      renderScript.mContext = l = renderScript.nContextCreate(l, paramInt2, paramInt1, paramContextType.mID);
      renderScript.mContextType = paramContextType;
      renderScript.mContextFlags = paramInt2;
      renderScript.mContextSdkVersion = paramInt1;
      if (l != 0L) {
        renderScript.nContextSetCacheDir(getCachePath());
        MessageThread messageThread = new MessageThread(renderScript);
        messageThread.start();
        return renderScript;
      } 
      throw new RSDriverException("Failed to create RS context.");
    } 
    throw new RSIllegalArgumentException("Invalid flags passed.");
  }
  
  public static RenderScript create(Context paramContext) {
    return create(paramContext, ContextType.NORMAL);
  }
  
  public static RenderScript create(Context paramContext, ContextType paramContextType) {
    return create(paramContext, paramContextType, 0);
  }
  
  public static RenderScript create(Context paramContext, ContextType paramContextType, int paramInt) {
    int i = (paramContext.getApplicationInfo()).targetSdkVersion;
    return create(paramContext, i, paramContextType, paramInt);
  }
  
  public static RenderScript create(Context paramContext, int paramInt) {
    return create(paramContext, paramInt, ContextType.NORMAL, 0);
  }
  
  private static RenderScript create(Context paramContext, int paramInt1, ContextType paramContextType, int paramInt2) {
    if (paramInt1 < 23)
      return internalCreate(paramContext, paramInt1, paramContextType, paramInt2); 
    synchronized (mProcessContextList) {
      for (RenderScript renderScript1 : mProcessContextList) {
        if (renderScript1.mContextType == paramContextType && renderScript1.mContextFlags == paramInt2 && renderScript1.mContextSdkVersion == paramInt1)
          return renderScript1; 
      } 
      RenderScript renderScript = internalCreate(paramContext, paramInt1, paramContextType, paramInt2);
      renderScript.mIsProcessContext = true;
      mProcessContextList.add(renderScript);
      return renderScript;
    } 
  }
  
  public static void releaseAllContexts() {
    ArrayList<RenderScript> arrayList;
    synchronized (mProcessContextList) {
      ArrayList<RenderScript> arrayList1 = mProcessContextList;
      ArrayList<RenderScript> arrayList2 = new ArrayList();
      this();
      mProcessContextList = arrayList2;
      for (RenderScript renderScript : arrayList1) {
        renderScript.mIsProcessContext = false;
        renderScript.destroy();
      } 
      arrayList1.clear();
      return;
    } 
  }
  
  public static RenderScript createMultiContext(Context paramContext, ContextType paramContextType, int paramInt1, int paramInt2) {
    return internalCreate(paramContext, paramInt2, paramContextType, paramInt1);
  }
  
  public void contextDump() {
    validate();
    nContextDump(0);
  }
  
  public void finish() {
    nContextFinish();
  }
  
  private void helpDestroy() {
    // Byte code:
    //   0: iconst_0
    //   1: istore_1
    //   2: aload_0
    //   3: monitorenter
    //   4: aload_0
    //   5: getfield mDestroyed : Z
    //   8: ifne -> 18
    //   11: iconst_1
    //   12: istore_1
    //   13: aload_0
    //   14: iconst_1
    //   15: putfield mDestroyed : Z
    //   18: aload_0
    //   19: monitorexit
    //   20: iload_1
    //   21: ifeq -> 100
    //   24: aload_0
    //   25: invokevirtual nContextFinish : ()V
    //   28: aload_0
    //   29: aload_0
    //   30: getfield mContext : J
    //   33: invokevirtual nContextDeinitToClient : (J)V
    //   36: aload_0
    //   37: getfield mMessageThread : Landroid/renderscript/RenderScript$MessageThread;
    //   40: iconst_0
    //   41: putfield mRun : Z
    //   44: aload_0
    //   45: getfield mMessageThread : Landroid/renderscript/RenderScript$MessageThread;
    //   48: invokevirtual interrupt : ()V
    //   51: iconst_0
    //   52: istore_2
    //   53: iconst_0
    //   54: istore_1
    //   55: iload_2
    //   56: ifne -> 77
    //   59: aload_0
    //   60: getfield mMessageThread : Landroid/renderscript/RenderScript$MessageThread;
    //   63: invokevirtual join : ()V
    //   66: iconst_1
    //   67: istore_2
    //   68: goto -> 74
    //   71: astore_3
    //   72: iconst_1
    //   73: istore_1
    //   74: goto -> 55
    //   77: iload_1
    //   78: ifeq -> 96
    //   81: ldc 'RenderScript_jni'
    //   83: ldc_w 'Interrupted during wait for MessageThread to join'
    //   86: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   89: pop
    //   90: invokestatic currentThread : ()Ljava/lang/Thread;
    //   93: invokevirtual interrupt : ()V
    //   96: aload_0
    //   97: invokevirtual nContextDestroy : ()V
    //   100: return
    //   101: astore_3
    //   102: aload_0
    //   103: monitorexit
    //   104: aload_3
    //   105: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1615	-> 0
    //   #1616	-> 2
    //   #1617	-> 4
    //   #1618	-> 11
    //   #1619	-> 13
    //   #1621	-> 18
    //   #1623	-> 20
    //   #1624	-> 24
    //   #1626	-> 28
    //   #1627	-> 36
    //   #1630	-> 44
    //   #1634	-> 51
    //   #1635	-> 55
    //   #1637	-> 59
    //   #1638	-> 66
    //   #1639	-> 71
    //   #1640	-> 72
    //   #1641	-> 74
    //   #1643	-> 77
    //   #1644	-> 81
    //   #1645	-> 90
    //   #1648	-> 96
    //   #1650	-> 100
    //   #1621	-> 101
    // Exception table:
    //   from	to	target	type
    //   4	11	101	finally
    //   13	18	101	finally
    //   18	20	101	finally
    //   59	66	71	java/lang/InterruptedException
    //   102	104	101	finally
  }
  
  protected void finalize() throws Throwable {
    helpDestroy();
    super.finalize();
  }
  
  public void destroy() {
    if (this.mIsProcessContext)
      return; 
    validate();
    helpDestroy();
  }
  
  boolean isAlive() {
    boolean bool;
    if (this.mContext != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  long safeID(BaseObj paramBaseObj) {
    if (paramBaseObj != null)
      return paramBaseObj.getID(this); 
    return 0L;
  }
  
  static native void _nInit();
  
  static native int rsnSystemGetPointerSize();
  
  native void nContextDeinitToClient(long paramLong);
  
  native String nContextGetErrorMessage(long paramLong);
  
  native int nContextGetUserMessage(long paramLong, int[] paramArrayOfint);
  
  native void nContextInitToClient(long paramLong);
  
  native int nContextPeekMessage(long paramLong, int[] paramArrayOfint);
  
  native long nDeviceCreate();
  
  native void nDeviceDestroy(long paramLong);
  
  native void nDeviceSetConfig(long paramLong, int paramInt1, int paramInt2);
  
  native long rsnAllocationAdapterCreate(long paramLong1, long paramLong2, long paramLong3);
  
  native void rsnAllocationAdapterOffset(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9);
  
  native void rsnAllocationCopyFromBitmap(long paramLong1, long paramLong2, Bitmap paramBitmap);
  
  native void rsnAllocationCopyToBitmap(long paramLong1, long paramLong2, Bitmap paramBitmap);
  
  native long rsnAllocationCreateBitmapBackedAllocation(long paramLong1, long paramLong2, int paramInt1, Bitmap paramBitmap, int paramInt2);
  
  native long rsnAllocationCreateFromBitmap(long paramLong1, long paramLong2, int paramInt1, Bitmap paramBitmap, int paramInt2);
  
  native long rsnAllocationCreateTyped(long paramLong1, long paramLong2, int paramInt1, int paramInt2, long paramLong3);
  
  native long rsnAllocationCubeCreateFromBitmap(long paramLong1, long paramLong2, int paramInt1, Bitmap paramBitmap, int paramInt2);
  
  native void rsnAllocationData1D(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, Object paramObject, int paramInt4, int paramInt5, int paramInt6, boolean paramBoolean);
  
  native void rsnAllocationData2D(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, long paramLong3, int paramInt7, int paramInt8, int paramInt9, int paramInt10);
  
  native void rsnAllocationData2D(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, Object paramObject, int paramInt7, int paramInt8, int paramInt9, boolean paramBoolean);
  
  native void rsnAllocationData2D(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Bitmap paramBitmap);
  
  native void rsnAllocationData3D(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, long paramLong3, int paramInt8, int paramInt9, int paramInt10, int paramInt11);
  
  native void rsnAllocationData3D(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, Object paramObject, int paramInt8, int paramInt9, int paramInt10, boolean paramBoolean);
  
  native void rsnAllocationElementData(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, byte[] paramArrayOfbyte, int paramInt6);
  
  native void rsnAllocationElementRead(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, byte[] paramArrayOfbyte, int paramInt6);
  
  native void rsnAllocationGenerateMipmaps(long paramLong1, long paramLong2);
  
  native ByteBuffer rsnAllocationGetByteBuffer(long paramLong1, long paramLong2, long[] paramArrayOflong, int paramInt1, int paramInt2, int paramInt3);
  
  native Surface rsnAllocationGetSurface(long paramLong1, long paramLong2);
  
  native long rsnAllocationGetType(long paramLong1, long paramLong2);
  
  native long rsnAllocationIoReceive(long paramLong1, long paramLong2);
  
  native void rsnAllocationIoSend(long paramLong1, long paramLong2);
  
  native void rsnAllocationRead(long paramLong1, long paramLong2, Object paramObject, int paramInt1, int paramInt2, boolean paramBoolean);
  
  native void rsnAllocationRead1D(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, Object paramObject, int paramInt4, int paramInt5, int paramInt6, boolean paramBoolean);
  
  native void rsnAllocationRead2D(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, Object paramObject, int paramInt7, int paramInt8, int paramInt9, boolean paramBoolean);
  
  native void rsnAllocationRead3D(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, Object paramObject, int paramInt8, int paramInt9, int paramInt10, boolean paramBoolean);
  
  native void rsnAllocationResize1D(long paramLong1, long paramLong2, int paramInt);
  
  native void rsnAllocationSetSurface(long paramLong1, long paramLong2, Surface paramSurface);
  
  native void rsnAllocationSetupBufferQueue(long paramLong1, long paramLong2, int paramInt);
  
  native void rsnAllocationShareBufferQueue(long paramLong1, long paramLong2, long paramLong3);
  
  native void rsnAllocationSyncAll(long paramLong1, long paramLong2, int paramInt);
  
  native void rsnAssignName(long paramLong1, long paramLong2, byte[] paramArrayOfbyte);
  
  native long rsnClosureCreate(long paramLong1, long paramLong2, long paramLong3, long[] paramArrayOflong1, long[] paramArrayOflong2, int[] paramArrayOfint, long[] paramArrayOflong3, long[] paramArrayOflong4);
  
  native void rsnClosureSetArg(long paramLong1, long paramLong2, int paramInt1, long paramLong3, int paramInt2);
  
  native void rsnClosureSetGlobal(long paramLong1, long paramLong2, long paramLong3, long paramLong4, int paramInt);
  
  native void rsnContextBindProgramFragment(long paramLong1, long paramLong2);
  
  native void rsnContextBindProgramRaster(long paramLong1, long paramLong2);
  
  native void rsnContextBindProgramStore(long paramLong1, long paramLong2);
  
  native void rsnContextBindProgramVertex(long paramLong1, long paramLong2);
  
  native void rsnContextBindRootScript(long paramLong1, long paramLong2);
  
  native void rsnContextBindSampler(long paramLong, int paramInt1, int paramInt2);
  
  native long rsnContextCreate(long paramLong, int paramInt1, int paramInt2, int paramInt3);
  
  native long rsnContextCreateGL(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10, int paramInt11, int paramInt12, float paramFloat, int paramInt13);
  
  native void rsnContextDestroy(long paramLong);
  
  native void rsnContextDump(long paramLong, int paramInt);
  
  native void rsnContextFinish(long paramLong);
  
  native void rsnContextPause(long paramLong);
  
  native void rsnContextResume(long paramLong);
  
  native void rsnContextSendMessage(long paramLong, int paramInt, int[] paramArrayOfint);
  
  native void rsnContextSetCacheDir(long paramLong, String paramString);
  
  native void rsnContextSetPriority(long paramLong, int paramInt);
  
  native void rsnContextSetSurface(long paramLong, int paramInt1, int paramInt2, Surface paramSurface);
  
  native void rsnContextSetSurfaceTexture(long paramLong, int paramInt1, int paramInt2, SurfaceTexture paramSurfaceTexture);
  
  native long rsnElementCreate(long paramLong1, long paramLong2, int paramInt1, boolean paramBoolean, int paramInt2);
  
  native long rsnElementCreate2(long paramLong, long[] paramArrayOflong, String[] paramArrayOfString, int[] paramArrayOfint);
  
  native void rsnElementGetNativeData(long paramLong1, long paramLong2, int[] paramArrayOfint);
  
  native void rsnElementGetSubElements(long paramLong1, long paramLong2, long[] paramArrayOflong, String[] paramArrayOfString, int[] paramArrayOfint);
  
  native long rsnFileA3DCreateFromAsset(long paramLong, AssetManager paramAssetManager, String paramString);
  
  native long rsnFileA3DCreateFromAssetStream(long paramLong1, long paramLong2);
  
  native long rsnFileA3DCreateFromFile(long paramLong, String paramString);
  
  native long rsnFileA3DGetEntryByIndex(long paramLong1, long paramLong2, int paramInt);
  
  native void rsnFileA3DGetIndexEntries(long paramLong1, long paramLong2, int paramInt, int[] paramArrayOfint, String[] paramArrayOfString);
  
  native int rsnFileA3DGetNumIndexEntries(long paramLong1, long paramLong2);
  
  native long rsnFontCreateFromAsset(long paramLong, AssetManager paramAssetManager, String paramString, float paramFloat, int paramInt);
  
  native long rsnFontCreateFromAssetStream(long paramLong1, String paramString, float paramFloat, int paramInt, long paramLong2);
  
  native long rsnFontCreateFromFile(long paramLong, String paramString, float paramFloat, int paramInt);
  
  native String rsnGetName(long paramLong1, long paramLong2);
  
  native long rsnInvokeClosureCreate(long paramLong1, long paramLong2, byte[] paramArrayOfbyte, long[] paramArrayOflong1, long[] paramArrayOflong2, int[] paramArrayOfint);
  
  native long rsnMeshCreate(long paramLong, long[] paramArrayOflong1, long[] paramArrayOflong2, int[] paramArrayOfint);
  
  native int rsnMeshGetIndexCount(long paramLong1, long paramLong2);
  
  native void rsnMeshGetIndices(long paramLong1, long paramLong2, long[] paramArrayOflong, int[] paramArrayOfint, int paramInt);
  
  native int rsnMeshGetVertexBufferCount(long paramLong1, long paramLong2);
  
  native void rsnMeshGetVertices(long paramLong1, long paramLong2, long[] paramArrayOflong, int paramInt);
  
  native void rsnObjDestroy(long paramLong1, long paramLong2);
  
  native void rsnProgramBindConstants(long paramLong1, long paramLong2, int paramInt, long paramLong3);
  
  native void rsnProgramBindSampler(long paramLong1, long paramLong2, int paramInt, long paramLong3);
  
  native void rsnProgramBindTexture(long paramLong1, long paramLong2, int paramInt, long paramLong3);
  
  native long rsnProgramFragmentCreate(long paramLong, String paramString, String[] paramArrayOfString, long[] paramArrayOflong);
  
  native long rsnProgramRasterCreate(long paramLong, boolean paramBoolean, int paramInt);
  
  native long rsnProgramStoreCreate(long paramLong, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, int paramInt1, int paramInt2, int paramInt3);
  
  native long rsnProgramVertexCreate(long paramLong, String paramString, String[] paramArrayOfString, long[] paramArrayOflong);
  
  native long rsnSamplerCreate(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float paramFloat);
  
  native void rsnScriptBindAllocation(long paramLong1, long paramLong2, long paramLong3, int paramInt);
  
  native long rsnScriptCCreate(long paramLong, String paramString1, String paramString2, byte[] paramArrayOfbyte, int paramInt);
  
  native long rsnScriptFieldIDCreate(long paramLong1, long paramLong2, int paramInt);
  
  native void rsnScriptForEach(long paramLong1, long paramLong2, int paramInt, long[] paramArrayOflong, long paramLong3, byte[] paramArrayOfbyte, int[] paramArrayOfint);
  
  native double rsnScriptGetVarD(long paramLong1, long paramLong2, int paramInt);
  
  native float rsnScriptGetVarF(long paramLong1, long paramLong2, int paramInt);
  
  native int rsnScriptGetVarI(long paramLong1, long paramLong2, int paramInt);
  
  native long rsnScriptGetVarJ(long paramLong1, long paramLong2, int paramInt);
  
  native void rsnScriptGetVarV(long paramLong1, long paramLong2, int paramInt, byte[] paramArrayOfbyte);
  
  native long rsnScriptGroup2Create(long paramLong, String paramString1, String paramString2, long[] paramArrayOflong);
  
  native void rsnScriptGroup2Execute(long paramLong1, long paramLong2);
  
  native long rsnScriptGroupCreate(long paramLong, long[] paramArrayOflong1, long[] paramArrayOflong2, long[] paramArrayOflong3, long[] paramArrayOflong4, long[] paramArrayOflong5);
  
  native void rsnScriptGroupExecute(long paramLong1, long paramLong2);
  
  native void rsnScriptGroupSetInput(long paramLong1, long paramLong2, long paramLong3, long paramLong4);
  
  native void rsnScriptGroupSetOutput(long paramLong1, long paramLong2, long paramLong3, long paramLong4);
  
  native void rsnScriptIntrinsicBLAS_BNNM(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, long paramLong3, int paramInt4, long paramLong4, int paramInt5, long paramLong5, int paramInt6, int paramInt7);
  
  native void rsnScriptIntrinsicBLAS_Complex(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, float paramFloat1, float paramFloat2, long paramLong3, long paramLong4, float paramFloat3, float paramFloat4, long paramLong5, int paramInt10, int paramInt11, int paramInt12, int paramInt13);
  
  native void rsnScriptIntrinsicBLAS_Double(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, double paramDouble1, long paramLong3, long paramLong4, double paramDouble2, long paramLong5, int paramInt10, int paramInt11, int paramInt12, int paramInt13);
  
  native void rsnScriptIntrinsicBLAS_Single(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, float paramFloat1, long paramLong3, long paramLong4, float paramFloat2, long paramLong5, int paramInt10, int paramInt11, int paramInt12, int paramInt13);
  
  native void rsnScriptIntrinsicBLAS_Z(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, double paramDouble1, double paramDouble2, long paramLong3, long paramLong4, double paramDouble3, double paramDouble4, long paramLong5, int paramInt10, int paramInt11, int paramInt12, int paramInt13);
  
  native long rsnScriptIntrinsicCreate(long paramLong1, int paramInt, long paramLong2);
  
  native void rsnScriptInvoke(long paramLong1, long paramLong2, int paramInt);
  
  native long rsnScriptInvokeIDCreate(long paramLong1, long paramLong2, int paramInt);
  
  native void rsnScriptInvokeV(long paramLong1, long paramLong2, int paramInt, byte[] paramArrayOfbyte);
  
  native long rsnScriptKernelIDCreate(long paramLong1, long paramLong2, int paramInt1, int paramInt2);
  
  native void rsnScriptReduce(long paramLong1, long paramLong2, int paramInt, long[] paramArrayOflong, long paramLong3, int[] paramArrayOfint);
  
  native void rsnScriptSetTimeZone(long paramLong1, long paramLong2, byte[] paramArrayOfbyte);
  
  native void rsnScriptSetVarD(long paramLong1, long paramLong2, int paramInt, double paramDouble);
  
  native void rsnScriptSetVarF(long paramLong1, long paramLong2, int paramInt, float paramFloat);
  
  native void rsnScriptSetVarI(long paramLong1, long paramLong2, int paramInt1, int paramInt2);
  
  native void rsnScriptSetVarJ(long paramLong1, long paramLong2, int paramInt, long paramLong3);
  
  native void rsnScriptSetVarObj(long paramLong1, long paramLong2, int paramInt, long paramLong3);
  
  native void rsnScriptSetVarV(long paramLong1, long paramLong2, int paramInt, byte[] paramArrayOfbyte);
  
  native void rsnScriptSetVarVE(long paramLong1, long paramLong2, int paramInt, byte[] paramArrayOfbyte, long paramLong3, int[] paramArrayOfint);
  
  native long rsnTypeCreate(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, boolean paramBoolean2, int paramInt4);
  
  native void rsnTypeGetNativeData(long paramLong1, long paramLong2, long[] paramArrayOflong);
}
