package android.renderscript;

import android.util.Log;
import android.util.Pair;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class ScriptGroup extends BaseObj {
  private static final String TAG = "ScriptGroup";
  
  private List<Closure> mClosures;
  
  IO[] mInputs;
  
  private List<Input> mInputs2;
  
  private String mName;
  
  IO[] mOutputs;
  
  private Future[] mOutputs2;
  
  class IO {
    Allocation mAllocation;
    
    Script.KernelID mKID;
    
    IO(ScriptGroup this$0) {
      this.mKID = (Script.KernelID)this$0;
    }
  }
  
  class ConnectLine {
    Type mAllocationType;
    
    Script.KernelID mFrom;
    
    Script.FieldID mToF;
    
    Script.KernelID mToK;
    
    ConnectLine(ScriptGroup this$0, Script.KernelID param1KernelID1, Script.KernelID param1KernelID2) {
      this.mFrom = param1KernelID1;
      this.mToK = param1KernelID2;
      this.mAllocationType = (Type)this$0;
    }
    
    ConnectLine(ScriptGroup this$0, Script.KernelID param1KernelID, Script.FieldID param1FieldID) {
      this.mFrom = param1KernelID;
      this.mToF = param1FieldID;
      this.mAllocationType = (Type)this$0;
    }
  }
  
  class Node {
    ArrayList<Script.KernelID> mKernels = new ArrayList<>();
    
    ArrayList<ScriptGroup.ConnectLine> mInputs = new ArrayList<>();
    
    ArrayList<ScriptGroup.ConnectLine> mOutputs = new ArrayList<>();
    
    int dagNumber;
    
    Node mNext;
    
    Script mScript;
    
    Node(ScriptGroup this$0) {
      this.mScript = (Script)this$0;
    }
  }
  
  public static final class Closure extends BaseObj {
    private static final String TAG = "Closure";
    
    private Object[] mArgs;
    
    private Map<Script.FieldID, Object> mBindings;
    
    private FieldPacker mFP;
    
    private Map<Script.FieldID, ScriptGroup.Future> mGlobalFuture;
    
    private ScriptGroup.Future mReturnFuture;
    
    private Allocation mReturnValue;
    
    Closure(long param1Long, RenderScript param1RenderScript) {
      super(param1Long, param1RenderScript);
    }
    
    Closure(RenderScript param1RenderScript, Script.KernelID param1KernelID, Type param1Type, Object[] param1ArrayOfObject, Map<Script.FieldID, Object> param1Map) {
      super(0L, param1RenderScript);
      this.mArgs = param1ArrayOfObject;
      this.mReturnValue = Allocation.createTyped(param1RenderScript, param1Type);
      this.mBindings = param1Map;
      this.mGlobalFuture = new HashMap<>();
      int i = param1ArrayOfObject.length + param1Map.size();
      long[] arrayOfLong1 = new long[i];
      long[] arrayOfLong2 = new long[i];
      int[] arrayOfInt = new int[i];
      long[] arrayOfLong3 = new long[i];
      long[] arrayOfLong4 = new long[i];
      byte b;
      for (b = 0; b < param1ArrayOfObject.length; b++) {
        arrayOfLong1[b] = 0L;
        retrieveValueAndDependenceInfo(param1RenderScript, b, (Script.FieldID)null, param1ArrayOfObject[b], arrayOfLong2, arrayOfInt, arrayOfLong3, arrayOfLong4);
      } 
      for (Map.Entry<Script.FieldID, Object> entry : param1Map.entrySet()) {
        param1ArrayOfObject = (Object[])entry.getValue();
        Script.FieldID fieldID = (Script.FieldID)entry.getKey();
        arrayOfLong1[b] = fieldID.getID(param1RenderScript);
        retrieveValueAndDependenceInfo(param1RenderScript, b, fieldID, param1ArrayOfObject, arrayOfLong2, arrayOfInt, arrayOfLong3, arrayOfLong4);
        b++;
      } 
      long l = param1RenderScript.nClosureCreate(param1KernelID.getID(param1RenderScript), this.mReturnValue.getID(param1RenderScript), arrayOfLong1, arrayOfLong2, arrayOfInt, arrayOfLong3, arrayOfLong4);
      setID(l);
      this.guard.open("destroy");
    }
    
    Closure(RenderScript param1RenderScript, Script.InvokeID param1InvokeID, Object[] param1ArrayOfObject, Map<Script.FieldID, Object> param1Map) {
      super(0L, param1RenderScript);
      this.mFP = FieldPacker.createFromArray(param1ArrayOfObject);
      this.mArgs = param1ArrayOfObject;
      this.mBindings = param1Map;
      this.mGlobalFuture = new HashMap<>();
      int i = param1Map.size();
      long[] arrayOfLong2 = new long[i];
      long[] arrayOfLong3 = new long[i];
      int[] arrayOfInt = new int[i];
      long[] arrayOfLong4 = new long[i];
      long[] arrayOfLong5 = new long[i];
      long[] arrayOfLong1;
      Iterator<Map.Entry> iterator;
      for (iterator = param1Map.entrySet().iterator(), i = 0, arrayOfLong1 = arrayOfLong5; iterator.hasNext(); ) {
        Map.Entry entry = iterator.next();
        arrayOfLong5 = (long[])entry.getValue();
        Script.FieldID fieldID = (Script.FieldID)entry.getKey();
        arrayOfLong2[i] = fieldID.getID(param1RenderScript);
        retrieveValueAndDependenceInfo(param1RenderScript, i, fieldID, arrayOfLong5, arrayOfLong3, arrayOfInt, arrayOfLong4, arrayOfLong1);
        i++;
      } 
      long l = param1RenderScript.nInvokeClosureCreate(param1InvokeID.getID(param1RenderScript), this.mFP.getData(), arrayOfLong2, arrayOfLong3, arrayOfInt);
      setID(l);
      this.guard.open("destroy");
    }
    
    public void destroy() {
      super.destroy();
      Allocation allocation = this.mReturnValue;
      if (allocation != null)
        allocation.destroy(); 
    }
    
    protected void finalize() throws Throwable {
      this.mReturnValue = null;
      super.finalize();
    }
    
    private void retrieveValueAndDependenceInfo(RenderScript param1RenderScript, int param1Int, Script.FieldID param1FieldID, Object param1Object, long[] param1ArrayOflong1, int[] param1ArrayOfint, long[] param1ArrayOflong2, long[] param1ArrayOflong3) {
      ScriptGroup.Input input;
      Script.FieldID fieldID;
      if (param1Object instanceof ScriptGroup.Future) {
        long l;
        ScriptGroup.Future future = (ScriptGroup.Future)param1Object;
        param1Object = future.getValue();
        param1ArrayOflong2[param1Int] = future.getClosure().getID(param1RenderScript);
        fieldID = future.getFieldID();
        if (fieldID != null) {
          l = fieldID.getID(param1RenderScript);
        } else {
          l = 0L;
        } 
        param1ArrayOflong3[param1Int] = l;
      } else {
        fieldID[param1Int] = 0L;
        param1ArrayOflong3[param1Int] = 0L;
      } 
      if (param1Object instanceof ScriptGroup.Input) {
        input = (ScriptGroup.Input)param1Object;
        if (param1Int < this.mArgs.length) {
          input.addReference(this, param1Int);
        } else {
          input.addReference(this, param1FieldID);
        } 
        param1ArrayOflong1[param1Int] = 0L;
        param1ArrayOfint[param1Int] = 0;
      } else {
        ValueAndSize valueAndSize = new ValueAndSize((RenderScript)input, param1Object);
        param1ArrayOflong1[param1Int] = valueAndSize.value;
        param1ArrayOfint[param1Int] = valueAndSize.size;
      } 
    }
    
    public ScriptGroup.Future getReturn() {
      if (this.mReturnFuture == null)
        this.mReturnFuture = new ScriptGroup.Future(this, null, this.mReturnValue); 
      return this.mReturnFuture;
    }
    
    public ScriptGroup.Future getGlobal(Script.FieldID param1FieldID) {
      ScriptGroup.Future future = this.mGlobalFuture.get(param1FieldID);
      Object object = future;
      if (future == null) {
        future = (ScriptGroup.Future)this.mBindings.get(param1FieldID);
        object = future;
        if (future instanceof ScriptGroup.Future)
          object = future.getValue(); 
        object = new ScriptGroup.Future(this, param1FieldID, object);
        this.mGlobalFuture.put(param1FieldID, object);
      } 
      return (ScriptGroup.Future)object;
    }
    
    void setArg(int param1Int, Object param1Object) {
      Object object = param1Object;
      if (param1Object instanceof ScriptGroup.Future)
        object = ((ScriptGroup.Future)param1Object).getValue(); 
      this.mArgs[param1Int] = object;
      param1Object = new ValueAndSize(this.mRS, object);
      this.mRS.nClosureSetArg(getID(this.mRS), param1Int, ((ValueAndSize)param1Object).value, ((ValueAndSize)param1Object).size);
    }
    
    void setGlobal(Script.FieldID param1FieldID, Object param1Object) {
      Object object = param1Object;
      if (param1Object instanceof ScriptGroup.Future)
        object = ((ScriptGroup.Future)param1Object).getValue(); 
      this.mBindings.put(param1FieldID, object);
      param1Object = new ValueAndSize(this.mRS, object);
      this.mRS.nClosureSetGlobal(getID(this.mRS), param1FieldID.getID(this.mRS), ((ValueAndSize)param1Object).value, ((ValueAndSize)param1Object).size);
    }
    
    class ValueAndSize {
      public int size;
      
      public long value;
      
      public ValueAndSize(ScriptGroup.Closure this$0, Object param2Object) {
        if (param2Object instanceof Allocation) {
          this.value = ((Allocation)param2Object).getID((RenderScript)this$0);
          this.size = -1;
        } else if (param2Object instanceof Boolean) {
          long l;
          if (((Boolean)param2Object).booleanValue()) {
            l = 1L;
          } else {
            l = 0L;
          } 
          this.value = l;
          this.size = 4;
        } else if (param2Object instanceof Integer) {
          this.value = ((Integer)param2Object).longValue();
          this.size = 4;
        } else if (param2Object instanceof Long) {
          this.value = ((Long)param2Object).longValue();
          this.size = 8;
        } else if (param2Object instanceof Float) {
          this.value = Float.floatToRawIntBits(((Float)param2Object).floatValue());
          this.size = 4;
        } else if (param2Object instanceof Double) {
          this.value = Double.doubleToRawLongBits(((Double)param2Object).doubleValue());
          this.size = 8;
        } 
      }
    }
  }
  
  class ValueAndSize {
    public int size;
    
    public long value;
    
    public ValueAndSize(ScriptGroup this$0, Object param1Object) {
      if (param1Object instanceof Allocation) {
        this.value = ((Allocation)param1Object).getID((RenderScript)this$0);
        this.size = -1;
      } else if (param1Object instanceof Boolean) {
        long l;
        if (((Boolean)param1Object).booleanValue()) {
          l = 1L;
        } else {
          l = 0L;
        } 
        this.value = l;
        this.size = 4;
      } else if (param1Object instanceof Integer) {
        this.value = ((Integer)param1Object).longValue();
        this.size = 4;
      } else if (param1Object instanceof Long) {
        this.value = ((Long)param1Object).longValue();
        this.size = 8;
      } else if (param1Object instanceof Float) {
        this.value = Float.floatToRawIntBits(((Float)param1Object).floatValue());
        this.size = 4;
      } else if (param1Object instanceof Double) {
        this.value = Double.doubleToRawLongBits(((Double)param1Object).doubleValue());
        this.size = 8;
      } 
    }
  }
  
  class Future {
    ScriptGroup.Closure mClosure;
    
    Script.FieldID mFieldID;
    
    Object mValue;
    
    Future(ScriptGroup this$0, Script.FieldID param1FieldID, Object param1Object) {
      this.mClosure = (ScriptGroup.Closure)this$0;
      this.mFieldID = param1FieldID;
      this.mValue = param1Object;
    }
    
    ScriptGroup.Closure getClosure() {
      return this.mClosure;
    }
    
    Script.FieldID getFieldID() {
      return this.mFieldID;
    }
    
    Object getValue() {
      return this.mValue;
    }
  }
  
  class Input {
    List<Pair<ScriptGroup.Closure, Integer>> mArgIndex;
    
    List<Pair<ScriptGroup.Closure, Script.FieldID>> mFieldID;
    
    Object mValue;
    
    Input() {
      this.mFieldID = new ArrayList<>();
      this.mArgIndex = new ArrayList<>();
    }
    
    void addReference(ScriptGroup.Closure param1Closure, int param1Int) {
      this.mArgIndex.add(Pair.create(param1Closure, Integer.valueOf(param1Int)));
    }
    
    void addReference(ScriptGroup.Closure param1Closure, Script.FieldID param1FieldID) {
      this.mFieldID.add(Pair.create(param1Closure, param1FieldID));
    }
    
    void set(Object param1Object) {
      this.mValue = param1Object;
      for (Pair<ScriptGroup.Closure, Integer> pair : this.mArgIndex) {
        ScriptGroup.Closure closure = (ScriptGroup.Closure)pair.first;
        int i = ((Integer)pair.second).intValue();
        closure.setArg(i, param1Object);
      } 
      for (Pair<ScriptGroup.Closure, Script.FieldID> pair : this.mFieldID) {
        ScriptGroup.Closure closure = (ScriptGroup.Closure)pair.first;
        Script.FieldID fieldID = (Script.FieldID)pair.second;
        closure.setGlobal(fieldID, param1Object);
      } 
    }
    
    Object get() {
      return this.mValue;
    }
  }
  
  ScriptGroup(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
    this.guard.open("destroy");
  }
  
  ScriptGroup(RenderScript paramRenderScript, String paramString, List<Closure> paramList, List<Input> paramList1, Future[] paramArrayOfFuture) {
    super(0L, paramRenderScript);
    this.mName = paramString;
    this.mClosures = paramList;
    this.mInputs2 = paramList1;
    this.mOutputs2 = paramArrayOfFuture;
    long[] arrayOfLong = new long[paramList.size()];
    for (byte b = 0; b < arrayOfLong.length; b++)
      arrayOfLong[b] = ((Closure)paramList.get(b)).getID(paramRenderScript); 
    long l = paramRenderScript.nScriptGroup2Create(paramString, RenderScript.getCachePath(), arrayOfLong);
    setID(l);
    this.guard.open("destroy");
  }
  
  public Object[] execute(Object... paramVarArgs) {
    String str;
    if (paramVarArgs.length < this.mInputs2.size()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(toString());
      stringBuilder.append(" receives ");
      stringBuilder.append(paramVarArgs.length);
      stringBuilder.append(" inputs, less than expected ");
      List<Input> list = this.mInputs2;
      stringBuilder.append(list.size());
      str = stringBuilder.toString();
      Log.e("ScriptGroup", str);
      return null;
    } 
    if (str.length > this.mInputs2.size()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(toString());
      stringBuilder.append(" receives ");
      stringBuilder.append(str.length);
      stringBuilder.append(" inputs, more than expected ");
      List<Input> list = this.mInputs2;
      stringBuilder.append(list.size());
      String str1 = stringBuilder.toString();
      Log.i("ScriptGroup", str1);
    } 
    byte b1;
    for (b1 = 0; b1 < this.mInputs2.size(); b1++) {
      String str1 = str[b1];
      if (str1 instanceof Future || str1 instanceof Input) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(toString());
        stringBuilder.append(": input ");
        stringBuilder.append(b1);
        stringBuilder.append(" is a future or unbound value");
        Log.e("ScriptGroup", stringBuilder.toString());
        return null;
      } 
      Input input = this.mInputs2.get(b1);
      input.set(str1);
    } 
    this.mRS.nScriptGroup2Execute(getID(this.mRS));
    Future[] arrayOfFuture = this.mOutputs2;
    Object[] arrayOfObject = new Object[arrayOfFuture.length];
    b1 = 0;
    int i;
    byte b2;
    for (i = arrayOfFuture.length, b2 = 0; b2 < i; ) {
      Future future = arrayOfFuture[b2];
      Object object2 = future.getValue();
      Object object1 = object2;
      if (object2 instanceof Input)
        object1 = ((Input)object2).get(); 
      arrayOfObject[b1] = object1;
      b2++;
      b1++;
    } 
    return arrayOfObject;
  }
  
  public void setInput(Script.KernelID paramKernelID, Allocation paramAllocation) {
    byte b = 0;
    while (true) {
      IO[] arrayOfIO = this.mInputs;
      if (b < arrayOfIO.length) {
        if ((arrayOfIO[b]).mKID == paramKernelID) {
          (this.mInputs[b]).mAllocation = paramAllocation;
          this.mRS.nScriptGroupSetInput(getID(this.mRS), paramKernelID.getID(this.mRS), this.mRS.safeID(paramAllocation));
          return;
        } 
        b++;
        continue;
      } 
      break;
    } 
    throw new RSIllegalArgumentException("Script not found");
  }
  
  public void setOutput(Script.KernelID paramKernelID, Allocation paramAllocation) {
    byte b = 0;
    while (true) {
      IO[] arrayOfIO = this.mOutputs;
      if (b < arrayOfIO.length) {
        if ((arrayOfIO[b]).mKID == paramKernelID) {
          (this.mOutputs[b]).mAllocation = paramAllocation;
          this.mRS.nScriptGroupSetOutput(getID(this.mRS), paramKernelID.getID(this.mRS), this.mRS.safeID(paramAllocation));
          return;
        } 
        b++;
        continue;
      } 
      break;
    } 
    throw new RSIllegalArgumentException("Script not found");
  }
  
  public void execute() {
    this.mRS.nScriptGroupExecute(getID(this.mRS));
  }
  
  class Builder {
    private RenderScript mRS;
    
    private ArrayList<ScriptGroup.Node> mNodes = new ArrayList<>();
    
    private ArrayList<ScriptGroup.ConnectLine> mLines = new ArrayList<>();
    
    private int mKernelCount;
    
    public Builder(ScriptGroup this$0) {
      this.mRS = (RenderScript)this$0;
    }
    
    private void validateCycle(ScriptGroup.Node param1Node1, ScriptGroup.Node param1Node2) {
      for (byte b = 0; b < param1Node1.mOutputs.size(); b++) {
        ScriptGroup.ConnectLine connectLine = param1Node1.mOutputs.get(b);
        if (connectLine.mToK != null) {
          ScriptGroup.Node node = findNode(connectLine.mToK.mScript);
          if (!node.equals(param1Node2)) {
            validateCycle(node, param1Node2);
          } else {
            throw new RSInvalidStateException("Loops in group not allowed.");
          } 
        } 
        if (connectLine.mToF != null) {
          ScriptGroup.Node node = findNode(connectLine.mToF.mScript);
          if (!node.equals(param1Node2)) {
            validateCycle(node, param1Node2);
          } else {
            throw new RSInvalidStateException("Loops in group not allowed.");
          } 
        } 
      } 
    }
    
    private void mergeDAGs(int param1Int1, int param1Int2) {
      for (byte b = 0; b < this.mNodes.size(); b++) {
        if (((ScriptGroup.Node)this.mNodes.get(b)).dagNumber == param1Int2)
          ((ScriptGroup.Node)this.mNodes.get(b)).dagNumber = param1Int1; 
      } 
    }
    
    private void validateDAGRecurse(ScriptGroup.Node param1Node, int param1Int) {
      if (param1Node.dagNumber != 0 && param1Node.dagNumber != param1Int) {
        mergeDAGs(param1Node.dagNumber, param1Int);
        return;
      } 
      param1Node.dagNumber = param1Int;
      for (byte b = 0; b < param1Node.mOutputs.size(); b++) {
        ScriptGroup.ConnectLine connectLine = param1Node.mOutputs.get(b);
        if (connectLine.mToK != null) {
          ScriptGroup.Node node = findNode(connectLine.mToK.mScript);
          validateDAGRecurse(node, param1Int);
        } 
        if (connectLine.mToF != null) {
          ScriptGroup.Node node = findNode(connectLine.mToF.mScript);
          validateDAGRecurse(node, param1Int);
        } 
      } 
    }
    
    private void validateDAG() {
      byte b;
      for (b = 0; b < this.mNodes.size(); b++) {
        ScriptGroup.Node node = this.mNodes.get(b);
        if (node.mInputs.size() == 0)
          if (node.mOutputs.size() != 0 || this.mNodes.size() <= 1) {
            validateDAGRecurse(node, b + 1);
          } else {
            throw new RSInvalidStateException("Groups cannot contain unconnected scripts");
          }  
      } 
      int i = ((ScriptGroup.Node)this.mNodes.get(0)).dagNumber;
      for (b = 0; b < this.mNodes.size(); ) {
        if (((ScriptGroup.Node)this.mNodes.get(b)).dagNumber == i) {
          b++;
          continue;
        } 
        throw new RSInvalidStateException("Multiple DAGs in group not allowed.");
      } 
    }
    
    private ScriptGroup.Node findNode(Script param1Script) {
      for (byte b = 0; b < this.mNodes.size(); b++) {
        if (param1Script == ((ScriptGroup.Node)this.mNodes.get(b)).mScript)
          return this.mNodes.get(b); 
      } 
      return null;
    }
    
    private ScriptGroup.Node findNode(Script.KernelID param1KernelID) {
      for (byte b = 0; b < this.mNodes.size(); b++) {
        ScriptGroup.Node node = this.mNodes.get(b);
        for (byte b1 = 0; b1 < node.mKernels.size(); b1++) {
          if (param1KernelID == node.mKernels.get(b1))
            return node; 
        } 
      } 
      return null;
    }
    
    public Builder addKernel(Script.KernelID param1KernelID) {
      if (this.mLines.size() == 0) {
        if (findNode(param1KernelID) != null)
          return this; 
        this.mKernelCount++;
        ScriptGroup.Node node1 = findNode(param1KernelID.mScript);
        ScriptGroup.Node node2 = node1;
        if (node1 == null) {
          node2 = new ScriptGroup.Node(param1KernelID.mScript);
          this.mNodes.add(node2);
        } 
        node2.mKernels.add(param1KernelID);
        return this;
      } 
      throw new RSInvalidStateException("Kernels may not be added once connections exist.");
    }
    
    public Builder addConnection(Type param1Type, Script.KernelID param1KernelID, Script.FieldID param1FieldID) {
      ScriptGroup.Node node = findNode(param1KernelID);
      if (node != null) {
        ScriptGroup.Node node1 = findNode(param1FieldID.mScript);
        if (node1 != null) {
          ScriptGroup.ConnectLine connectLine = new ScriptGroup.ConnectLine(param1Type, param1KernelID, param1FieldID);
          this.mLines.add(new ScriptGroup.ConnectLine(param1Type, param1KernelID, param1FieldID));
          node.mOutputs.add(connectLine);
          node1.mInputs.add(connectLine);
          validateCycle(node, node);
          return this;
        } 
        throw new RSInvalidStateException("To script not found.");
      } 
      throw new RSInvalidStateException("From script not found.");
    }
    
    public Builder addConnection(Type param1Type, Script.KernelID param1KernelID1, Script.KernelID param1KernelID2) {
      ScriptGroup.Node node = findNode(param1KernelID1);
      if (node != null) {
        ScriptGroup.Node node1 = findNode(param1KernelID2);
        if (node1 != null) {
          ScriptGroup.ConnectLine connectLine = new ScriptGroup.ConnectLine(param1Type, param1KernelID1, param1KernelID2);
          this.mLines.add(new ScriptGroup.ConnectLine(param1Type, param1KernelID1, param1KernelID2));
          node.mOutputs.add(connectLine);
          node1.mInputs.add(connectLine);
          validateCycle(node, node);
          return this;
        } 
        throw new RSInvalidStateException("To script not found.");
      } 
      throw new RSInvalidStateException("From script not found.");
    }
    
    public ScriptGroup create() {
      if (this.mNodes.size() != 0) {
        byte b1;
        for (b1 = 0; b1 < this.mNodes.size(); b1++)
          ((ScriptGroup.Node)this.mNodes.get(b1)).dagNumber = 0; 
        validateDAG();
        ArrayList<ScriptGroup.IO> arrayList1 = new ArrayList();
        ArrayList<ScriptGroup.IO> arrayList2 = new ArrayList();
        long[] arrayOfLong = new long[this.mKernelCount];
        byte b2;
        for (b1 = 0, b2 = 0; b1 < this.mNodes.size(); b1++) {
          ScriptGroup.Node node = this.mNodes.get(b1);
          for (byte b = 0; b < node.mKernels.size(); b++, b2++) {
            Script.KernelID kernelID = node.mKernels.get(b);
            arrayOfLong[b2] = kernelID.getID(this.mRS);
            boolean bool1 = false;
            boolean bool2 = false;
            byte b3;
            for (b3 = 0; b3 < node.mInputs.size(); b3++) {
              if (((ScriptGroup.ConnectLine)node.mInputs.get(b3)).mToK == kernelID)
                bool1 = true; 
            } 
            for (b3 = 0; b3 < node.mOutputs.size(); b3++) {
              if (((ScriptGroup.ConnectLine)node.mOutputs.get(b3)).mFrom == kernelID)
                bool2 = true; 
            } 
            if (!bool1)
              arrayList1.add(new ScriptGroup.IO(kernelID)); 
            if (!bool2)
              arrayList2.add(new ScriptGroup.IO(kernelID)); 
          } 
        } 
        if (b2 == this.mKernelCount) {
          long[] arrayOfLong3 = new long[this.mLines.size()];
          long[] arrayOfLong2 = new long[this.mLines.size()];
          long[] arrayOfLong1 = new long[this.mLines.size()];
          long[] arrayOfLong4 = new long[this.mLines.size()];
          for (b1 = 0; b1 < this.mLines.size(); b1++) {
            ScriptGroup.ConnectLine connectLine = this.mLines.get(b1);
            arrayOfLong3[b1] = connectLine.mFrom.getID(this.mRS);
            if (connectLine.mToK != null)
              arrayOfLong2[b1] = connectLine.mToK.getID(this.mRS); 
            if (connectLine.mToF != null)
              arrayOfLong1[b1] = connectLine.mToF.getID(this.mRS); 
            arrayOfLong4[b1] = connectLine.mAllocationType.getID(this.mRS);
          } 
          long l = this.mRS.nScriptGroupCreate(arrayOfLong, arrayOfLong3, arrayOfLong2, arrayOfLong1, arrayOfLong4);
          if (l != 0L) {
            ScriptGroup scriptGroup = new ScriptGroup(l, this.mRS);
            scriptGroup.mOutputs = new ScriptGroup.IO[arrayList2.size()];
            for (b1 = 0; b1 < arrayList2.size(); b1++)
              scriptGroup.mOutputs[b1] = arrayList2.get(b1); 
            scriptGroup.mInputs = new ScriptGroup.IO[arrayList1.size()];
            for (b1 = 0; b1 < arrayList1.size(); b1++)
              scriptGroup.mInputs[b1] = arrayList1.get(b1); 
            return scriptGroup;
          } 
          throw new RSRuntimeException("Object creation error, should not happen.");
        } 
        throw new RSRuntimeException("Count mismatch, should not happen.");
      } 
      throw new RSInvalidStateException("Empty script groups are not allowed");
    }
  }
  
  class Binding {
    private final Script.FieldID mField;
    
    private final Object mValue;
    
    public Binding(ScriptGroup this$0, Object param1Object) {
      this.mField = (Script.FieldID)this$0;
      this.mValue = param1Object;
    }
    
    Script.FieldID getField() {
      return this.mField;
    }
    
    Object getValue() {
      return this.mValue;
    }
  }
  
  class Builder2 {
    private static final String TAG = "ScriptGroup.Builder2";
    
    List<ScriptGroup.Closure> mClosures;
    
    List<ScriptGroup.Input> mInputs;
    
    RenderScript mRS;
    
    public Builder2(ScriptGroup this$0) {
      this.mRS = (RenderScript)this$0;
      this.mClosures = new ArrayList<>();
      this.mInputs = new ArrayList<>();
    }
    
    private ScriptGroup.Closure addKernelInternal(Script.KernelID param1KernelID, Type param1Type, Object[] param1ArrayOfObject, Map<Script.FieldID, Object> param1Map) {
      ScriptGroup.Closure closure = new ScriptGroup.Closure(this.mRS, param1KernelID, param1Type, param1ArrayOfObject, param1Map);
      this.mClosures.add(closure);
      return closure;
    }
    
    private ScriptGroup.Closure addInvokeInternal(Script.InvokeID param1InvokeID, Object[] param1ArrayOfObject, Map<Script.FieldID, Object> param1Map) {
      ScriptGroup.Closure closure = new ScriptGroup.Closure(this.mRS, param1InvokeID, param1ArrayOfObject, param1Map);
      this.mClosures.add(closure);
      return closure;
    }
    
    public ScriptGroup.Input addInput() {
      ScriptGroup.Input input = new ScriptGroup.Input();
      this.mInputs.add(input);
      return input;
    }
    
    public ScriptGroup.Closure addKernel(Script.KernelID param1KernelID, Type param1Type, Object... param1VarArgs) {
      ArrayList<Object> arrayList = new ArrayList();
      HashMap<Object, Object> hashMap = new HashMap<>();
      if (!seperateArgsAndBindings(param1VarArgs, arrayList, (Map)hashMap))
        return null; 
      return addKernelInternal(param1KernelID, param1Type, arrayList.toArray(), (Map)hashMap);
    }
    
    public ScriptGroup.Closure addInvoke(Script.InvokeID param1InvokeID, Object... param1VarArgs) {
      ArrayList<Object> arrayList = new ArrayList();
      HashMap<Object, Object> hashMap = new HashMap<>();
      if (!seperateArgsAndBindings(param1VarArgs, arrayList, (Map)hashMap))
        return null; 
      return addInvokeInternal(param1InvokeID, arrayList.toArray(), (Map)hashMap);
    }
    
    public ScriptGroup create(String param1String, ScriptGroup.Future... param1VarArgs) {
      if (param1String != null && !param1String.isEmpty() && param1String.length() <= 100 && 
        param1String.equals(param1String.replaceAll("[^a-zA-Z0-9-]", "_"))) {
        ScriptGroup scriptGroup = new ScriptGroup(this.mRS, param1String, this.mClosures, this.mInputs, param1VarArgs);
        this.mClosures = new ArrayList<>();
        this.mInputs = new ArrayList<>();
        return scriptGroup;
      } 
      throw new RSIllegalArgumentException("invalid script group name");
    }
    
    private boolean seperateArgsAndBindings(Object[] param1ArrayOfObject, ArrayList<Object> param1ArrayList, Map<Script.FieldID, Object> param1Map) {
      byte b2, b1 = 0;
      while (true) {
        b2 = b1;
        if (b1 < param1ArrayOfObject.length) {
          if (param1ArrayOfObject[b1] instanceof ScriptGroup.Binding) {
            b2 = b1;
            break;
          } 
          param1ArrayList.add(param1ArrayOfObject[b1]);
          b1++;
          continue;
        } 
        break;
      } 
      for (; b2 < param1ArrayOfObject.length; b2++) {
        if (!(param1ArrayOfObject[b2] instanceof ScriptGroup.Binding))
          return false; 
        ScriptGroup.Binding binding = (ScriptGroup.Binding)param1ArrayOfObject[b2];
        param1Map.put(binding.getField(), binding.getValue());
      } 
      return true;
    }
  }
  
  public void destroy() {
    super.destroy();
    List<Closure> list = this.mClosures;
    if (list != null)
      for (Closure closure : list)
        closure.destroy();  
  }
}
