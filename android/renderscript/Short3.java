package android.renderscript;

public class Short3 {
  public short x;
  
  public short y;
  
  public short z;
  
  public Short3() {}
  
  public Short3(short paramShort) {
    this.z = paramShort;
    this.y = paramShort;
    this.x = paramShort;
  }
  
  public Short3(short paramShort1, short paramShort2, short paramShort3) {
    this.x = paramShort1;
    this.y = paramShort2;
    this.z = paramShort3;
  }
  
  public Short3(Short3 paramShort3) {
    this.x = paramShort3.x;
    this.y = paramShort3.y;
    this.z = paramShort3.z;
  }
  
  public void add(Short3 paramShort3) {
    this.x = (short)(this.x + paramShort3.x);
    this.y = (short)(this.y + paramShort3.y);
    this.z = (short)(this.z + paramShort3.z);
  }
  
  public static Short3 add(Short3 paramShort31, Short3 paramShort32) {
    Short3 short3 = new Short3();
    short3.x = (short)(paramShort31.x + paramShort32.x);
    short3.y = (short)(paramShort31.y + paramShort32.y);
    short3.z = (short)(paramShort31.z + paramShort32.z);
    return short3;
  }
  
  public void add(short paramShort) {
    this.x = (short)(this.x + paramShort);
    this.y = (short)(this.y + paramShort);
    this.z = (short)(this.z + paramShort);
  }
  
  public static Short3 add(Short3 paramShort3, short paramShort) {
    Short3 short3 = new Short3();
    short3.x = (short)(paramShort3.x + paramShort);
    short3.y = (short)(paramShort3.y + paramShort);
    short3.z = (short)(paramShort3.z + paramShort);
    return short3;
  }
  
  public void sub(Short3 paramShort3) {
    this.x = (short)(this.x - paramShort3.x);
    this.y = (short)(this.y - paramShort3.y);
    this.z = (short)(this.z - paramShort3.z);
  }
  
  public static Short3 sub(Short3 paramShort31, Short3 paramShort32) {
    Short3 short3 = new Short3();
    short3.x = (short)(paramShort31.x - paramShort32.x);
    short3.y = (short)(paramShort31.y - paramShort32.y);
    short3.z = (short)(paramShort31.z - paramShort32.z);
    return short3;
  }
  
  public void sub(short paramShort) {
    this.x = (short)(this.x - paramShort);
    this.y = (short)(this.y - paramShort);
    this.z = (short)(this.z - paramShort);
  }
  
  public static Short3 sub(Short3 paramShort3, short paramShort) {
    Short3 short3 = new Short3();
    short3.x = (short)(paramShort3.x - paramShort);
    short3.y = (short)(paramShort3.y - paramShort);
    short3.z = (short)(paramShort3.z - paramShort);
    return short3;
  }
  
  public void mul(Short3 paramShort3) {
    this.x = (short)(this.x * paramShort3.x);
    this.y = (short)(this.y * paramShort3.y);
    this.z = (short)(this.z * paramShort3.z);
  }
  
  public static Short3 mul(Short3 paramShort31, Short3 paramShort32) {
    Short3 short3 = new Short3();
    short3.x = (short)(paramShort31.x * paramShort32.x);
    short3.y = (short)(paramShort31.y * paramShort32.y);
    short3.z = (short)(paramShort31.z * paramShort32.z);
    return short3;
  }
  
  public void mul(short paramShort) {
    this.x = (short)(this.x * paramShort);
    this.y = (short)(this.y * paramShort);
    this.z = (short)(this.z * paramShort);
  }
  
  public static Short3 mul(Short3 paramShort3, short paramShort) {
    Short3 short3 = new Short3();
    short3.x = (short)(paramShort3.x * paramShort);
    short3.y = (short)(paramShort3.y * paramShort);
    short3.z = (short)(paramShort3.z * paramShort);
    return short3;
  }
  
  public void div(Short3 paramShort3) {
    this.x = (short)(this.x / paramShort3.x);
    this.y = (short)(this.y / paramShort3.y);
    this.z = (short)(this.z / paramShort3.z);
  }
  
  public static Short3 div(Short3 paramShort31, Short3 paramShort32) {
    Short3 short3 = new Short3();
    short3.x = (short)(paramShort31.x / paramShort32.x);
    short3.y = (short)(paramShort31.y / paramShort32.y);
    short3.z = (short)(paramShort31.z / paramShort32.z);
    return short3;
  }
  
  public void div(short paramShort) {
    this.x = (short)(this.x / paramShort);
    this.y = (short)(this.y / paramShort);
    this.z = (short)(this.z / paramShort);
  }
  
  public static Short3 div(Short3 paramShort3, short paramShort) {
    Short3 short3 = new Short3();
    short3.x = (short)(paramShort3.x / paramShort);
    short3.y = (short)(paramShort3.y / paramShort);
    short3.z = (short)(paramShort3.z / paramShort);
    return short3;
  }
  
  public void mod(Short3 paramShort3) {
    this.x = (short)(this.x % paramShort3.x);
    this.y = (short)(this.y % paramShort3.y);
    this.z = (short)(this.z % paramShort3.z);
  }
  
  public static Short3 mod(Short3 paramShort31, Short3 paramShort32) {
    Short3 short3 = new Short3();
    short3.x = (short)(paramShort31.x % paramShort32.x);
    short3.y = (short)(paramShort31.y % paramShort32.y);
    short3.z = (short)(paramShort31.z % paramShort32.z);
    return short3;
  }
  
  public void mod(short paramShort) {
    this.x = (short)(this.x % paramShort);
    this.y = (short)(this.y % paramShort);
    this.z = (short)(this.z % paramShort);
  }
  
  public static Short3 mod(Short3 paramShort3, short paramShort) {
    Short3 short3 = new Short3();
    short3.x = (short)(paramShort3.x % paramShort);
    short3.y = (short)(paramShort3.y % paramShort);
    short3.z = (short)(paramShort3.z % paramShort);
    return short3;
  }
  
  public short length() {
    return 3;
  }
  
  public void negate() {
    this.x = (short)-this.x;
    this.y = (short)-this.y;
    this.z = (short)-this.z;
  }
  
  public short dotProduct(Short3 paramShort3) {
    return (short)(this.x * paramShort3.x + this.y * paramShort3.y + this.z * paramShort3.z);
  }
  
  public static short dotProduct(Short3 paramShort31, Short3 paramShort32) {
    return (short)(paramShort32.x * paramShort31.x + paramShort32.y * paramShort31.y + paramShort32.z * paramShort31.z);
  }
  
  public void addMultiple(Short3 paramShort3, short paramShort) {
    this.x = (short)(this.x + paramShort3.x * paramShort);
    this.y = (short)(this.y + paramShort3.y * paramShort);
    this.z = (short)(this.z + paramShort3.z * paramShort);
  }
  
  public void set(Short3 paramShort3) {
    this.x = paramShort3.x;
    this.y = paramShort3.y;
    this.z = paramShort3.z;
  }
  
  public void setValues(short paramShort1, short paramShort2, short paramShort3) {
    this.x = paramShort1;
    this.y = paramShort2;
    this.z = paramShort3;
  }
  
  public short elementSum() {
    return (short)(this.x + this.y + this.z);
  }
  
  public short get(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt == 2)
          return this.z; 
        throw new IndexOutOfBoundsException("Index: i");
      } 
      return this.y;
    } 
    return this.x;
  }
  
  public void setAt(int paramInt, short paramShort) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt == 2) {
          this.z = paramShort;
          return;
        } 
        throw new IndexOutOfBoundsException("Index: i");
      } 
      this.y = paramShort;
      return;
    } 
    this.x = paramShort;
  }
  
  public void addAt(int paramInt, short paramShort) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt == 2) {
          this.z = (short)(this.z + paramShort);
          return;
        } 
        throw new IndexOutOfBoundsException("Index: i");
      } 
      this.y = (short)(this.y + paramShort);
      return;
    } 
    this.x = (short)(this.x + paramShort);
  }
  
  public void copyTo(short[] paramArrayOfshort, int paramInt) {
    paramArrayOfshort[paramInt] = this.x;
    paramArrayOfshort[paramInt + 1] = this.y;
    paramArrayOfshort[paramInt + 2] = this.z;
  }
}
