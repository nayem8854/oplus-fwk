package android.renderscript;

public class AllocationAdapter extends Allocation {
  Type mWindow;
  
  AllocationAdapter(long paramLong, RenderScript paramRenderScript, Allocation paramAllocation, Type paramType) {
    super(paramLong, paramRenderScript, paramAllocation.mType, paramAllocation.mUsage);
    this.mAdaptedAllocation = paramAllocation;
    this.mWindow = paramType;
  }
  
  void initLOD(int paramInt) {
    if (paramInt >= 0) {
      int i = this.mAdaptedAllocation.mType.getX();
      int j = this.mAdaptedAllocation.mType.getY();
      int k = this.mAdaptedAllocation.mType.getZ();
      for (byte b = 0; b < paramInt; ) {
        if (i != 1 || j != 1 || k != 1) {
          int m = i;
          if (i > 1)
            m = i >> 1; 
          int n = j;
          if (j > 1)
            n = j >> 1; 
          int i1 = k;
          if (k > 1)
            i1 = k >> 1; 
          b++;
          i = m;
          j = n;
          k = i1;
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Attempting to set lod (");
        stringBuilder1.append(paramInt);
        stringBuilder1.append(") out of range.");
        throw new RSIllegalArgumentException(stringBuilder1.toString());
      } 
      this.mCurrentDimX = i;
      this.mCurrentDimY = j;
      this.mCurrentDimZ = k;
      this.mCurrentCount = this.mCurrentDimX;
      if (this.mCurrentDimY > 1)
        this.mCurrentCount *= this.mCurrentDimY; 
      if (this.mCurrentDimZ > 1)
        this.mCurrentCount *= this.mCurrentDimZ; 
      this.mSelectedY = 0;
      this.mSelectedZ = 0;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Attempting to set negative lod (");
    stringBuilder.append(paramInt);
    stringBuilder.append(").");
    throw new RSIllegalArgumentException(stringBuilder.toString());
  }
  
  private void updateOffsets() {
    int i = 0, j = 0, k = 0, m = 0, n = 0, i1 = 0;
    byte b = 0;
    int i2 = b;
    if (this.mSelectedArray != null) {
      if (this.mSelectedArray.length > 0)
        j = this.mSelectedArray[0]; 
      if (this.mSelectedArray.length > 1)
        m = this.mSelectedArray[2]; 
      if (this.mSelectedArray.length > 2)
        i1 = this.mSelectedArray[2]; 
      i = j;
      k = m;
      n = i1;
      i2 = b;
      if (this.mSelectedArray.length > 3) {
        i2 = this.mSelectedArray[3];
        n = i1;
        k = m;
        i = j;
      } 
    } 
    this.mRS.nAllocationAdapterOffset(getID(this.mRS), this.mSelectedX, this.mSelectedY, this.mSelectedZ, this.mSelectedLOD, this.mSelectedFace.mID, i, k, n, i2);
  }
  
  public void setLOD(int paramInt) {
    if (this.mAdaptedAllocation.getType().hasMipmaps()) {
      if (!this.mWindow.hasMipmaps()) {
        initLOD(paramInt);
        this.mSelectedLOD = paramInt;
        updateOffsets();
        return;
      } 
      throw new RSInvalidStateException("Cannot set LOD when the adapter includes mipmaps.");
    } 
    throw new RSInvalidStateException("Cannot set LOD when the allocation type does not include mipmaps.");
  }
  
  public void setFace(Type.CubemapFace paramCubemapFace) {
    if (this.mAdaptedAllocation.getType().hasFaces()) {
      if (!this.mWindow.hasFaces()) {
        if (paramCubemapFace != null) {
          this.mSelectedFace = paramCubemapFace;
          updateOffsets();
          return;
        } 
        throw new RSIllegalArgumentException("Cannot set null face.");
      } 
      throw new RSInvalidStateException("Cannot set face when the adapter includes faces.");
    } 
    throw new RSInvalidStateException("Cannot set Face when the allocation type does not include faces.");
  }
  
  public void setX(int paramInt) {
    if (this.mAdaptedAllocation.getType().getX() > paramInt) {
      if (this.mWindow.getX() != this.mAdaptedAllocation.getType().getX()) {
        if (this.mWindow.getX() + paramInt < this.mAdaptedAllocation.getType().getX()) {
          this.mSelectedX = paramInt;
          updateOffsets();
          return;
        } 
        throw new RSInvalidStateException("Cannot set (X + window) which would be larger than dimension of allocation.");
      } 
      throw new RSInvalidStateException("Cannot set X when the adapter includes X.");
    } 
    throw new RSInvalidStateException("Cannot set X greater than dimension of allocation.");
  }
  
  public void setY(int paramInt) {
    if (this.mAdaptedAllocation.getType().getY() != 0) {
      if (this.mAdaptedAllocation.getType().getY() > paramInt) {
        if (this.mWindow.getY() != this.mAdaptedAllocation.getType().getY()) {
          if (this.mWindow.getY() + paramInt < this.mAdaptedAllocation.getType().getY()) {
            this.mSelectedY = paramInt;
            updateOffsets();
            return;
          } 
          throw new RSInvalidStateException("Cannot set (Y + window) which would be larger than dimension of allocation.");
        } 
        throw new RSInvalidStateException("Cannot set Y when the adapter includes Y.");
      } 
      throw new RSInvalidStateException("Cannot set Y greater than dimension of allocation.");
    } 
    throw new RSInvalidStateException("Cannot set Y when the allocation type does not include Y dim.");
  }
  
  public void setZ(int paramInt) {
    if (this.mAdaptedAllocation.getType().getZ() != 0) {
      if (this.mAdaptedAllocation.getType().getZ() > paramInt) {
        if (this.mWindow.getZ() != this.mAdaptedAllocation.getType().getZ()) {
          if (this.mWindow.getZ() + paramInt < this.mAdaptedAllocation.getType().getZ()) {
            this.mSelectedZ = paramInt;
            updateOffsets();
            return;
          } 
          throw new RSInvalidStateException("Cannot set (Z + window) which would be larger than dimension of allocation.");
        } 
        throw new RSInvalidStateException("Cannot set Z when the adapter includes Z.");
      } 
      throw new RSInvalidStateException("Cannot set Z greater than dimension of allocation.");
    } 
    throw new RSInvalidStateException("Cannot set Z when the allocation type does not include Z dim.");
  }
  
  public void setArray(int paramInt1, int paramInt2) {
    if (this.mAdaptedAllocation.getType().getArray(paramInt1) != 0) {
      if (this.mAdaptedAllocation.getType().getArray(paramInt1) > paramInt2) {
        if (this.mWindow.getArray(paramInt1) != this.mAdaptedAllocation.getType().getArray(paramInt1)) {
          if (this.mWindow.getArray(paramInt1) + paramInt2 < this.mAdaptedAllocation.getType().getArray(paramInt1)) {
            this.mSelectedArray[paramInt1] = paramInt2;
            updateOffsets();
            return;
          } 
          throw new RSInvalidStateException("Cannot set (arrayNum + window) which would be larger than dimension of allocation.");
        } 
        throw new RSInvalidStateException("Cannot set arrayNum when the adapter includes arrayNum.");
      } 
      throw new RSInvalidStateException("Cannot set arrayNum greater than dimension of allocation.");
    } 
    throw new RSInvalidStateException("Cannot set arrayNum when the allocation type does not include arrayNum dim.");
  }
  
  public static AllocationAdapter create1D(RenderScript paramRenderScript, Allocation paramAllocation) {
    paramRenderScript.validate();
    Type type = Type.createX(paramRenderScript, paramAllocation.getElement(), paramAllocation.getType().getX());
    return createTyped(paramRenderScript, paramAllocation, type);
  }
  
  public static AllocationAdapter create2D(RenderScript paramRenderScript, Allocation paramAllocation) {
    paramRenderScript.validate();
    Type type = Type.createXY(paramRenderScript, paramAllocation.getElement(), paramAllocation.getType().getX(), paramAllocation.getType().getY());
    return createTyped(paramRenderScript, paramAllocation, type);
  }
  
  public static AllocationAdapter createTyped(RenderScript paramRenderScript, Allocation paramAllocation, Type paramType) {
    paramRenderScript.validate();
    if (paramAllocation.mAdaptedAllocation == null) {
      if (paramAllocation.getType().getElement().equals(paramType.getElement())) {
        if (!paramType.hasFaces() && !paramType.hasMipmaps()) {
          Type type = paramAllocation.getType();
          if (paramType.getX() <= type.getX() && 
            paramType.getY() <= type.getY() && 
            paramType.getZ() <= type.getZ() && 
            paramType.getArrayCount() <= type.getArrayCount()) {
            if (paramType.getArrayCount() > 0)
              for (byte b = 0; b < paramType.getArray(b); ) {
                if (paramType.getArray(b) <= type.getArray(b)) {
                  b++;
                  continue;
                } 
                throw new RSInvalidStateException("Type cannot have dimension larger than the source allocation.");
              }  
            long l = paramRenderScript.nAllocationAdapterCreate(paramAllocation.getID(paramRenderScript), paramType.getID(paramRenderScript));
            if (l != 0L)
              return new AllocationAdapter(l, paramRenderScript, paramAllocation, paramType); 
            throw new RSRuntimeException("AllocationAdapter creation failed.");
          } 
          throw new RSInvalidStateException("Type cannot have dimension larger than the source allocation.");
        } 
        throw new RSInvalidStateException("Adapters do not support window types with Mipmaps or Faces.");
      } 
      throw new RSInvalidStateException("Element must match Allocation type.");
    } 
    throw new RSInvalidStateException("Adapters cannot be nested.");
  }
  
  public void resize(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new android/renderscript/RSInvalidStateException
    //   5: astore_2
    //   6: aload_2
    //   7: ldc 'Resize not allowed for Adapters.'
    //   9: invokespecial <init> : (Ljava/lang/String;)V
    //   12: aload_2
    //   13: athrow
    //   14: astore_2
    //   15: aload_0
    //   16: monitorexit
    //   17: aload_2
    //   18: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #313	-> 2
    //   #313	-> 14
    // Exception table:
    //   from	to	target	type
    //   2	14	14	finally
  }
}
