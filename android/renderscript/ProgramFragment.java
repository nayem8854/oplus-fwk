package android.renderscript;

public class ProgramFragment extends Program {
  ProgramFragment(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
  }
  
  class Builder extends Program.BaseProgramBuilder {
    public Builder(ProgramFragment this$0) {
      super((RenderScript)this$0);
    }
    
    public ProgramFragment create() {
      this.mRS.validate();
      long[] arrayOfLong = new long[(this.mInputCount + this.mOutputCount + this.mConstantCount + this.mTextureCount) * 2];
      String[] arrayOfString = new String[this.mTextureCount];
      int i = 0;
      byte b;
      for (b = 0; b < this.mInputCount; b++) {
        int j = i + 1;
        arrayOfLong[i] = Program.ProgramParam.INPUT.mID;
        i = j + 1;
        arrayOfLong[j] = this.mInputs[b].getID(this.mRS);
      } 
      for (b = 0; b < this.mOutputCount; b++) {
        int j = i + 1;
        arrayOfLong[i] = Program.ProgramParam.OUTPUT.mID;
        i = j + 1;
        arrayOfLong[j] = this.mOutputs[b].getID(this.mRS);
      } 
      for (b = 0; b < this.mConstantCount; b++) {
        int j = i + 1;
        arrayOfLong[i] = Program.ProgramParam.CONSTANT.mID;
        i = j + 1;
        arrayOfLong[j] = this.mConstants[b].getID(this.mRS);
      } 
      for (b = 0; b < this.mTextureCount; b++) {
        int j = i + 1;
        arrayOfLong[i] = Program.ProgramParam.TEXTURE_TYPE.mID;
        i = j + 1;
        arrayOfLong[j] = (this.mTextureTypes[b]).mID;
        arrayOfString[b] = this.mTextureNames[b];
      } 
      long l = this.mRS.nProgramFragmentCreate(this.mShader, arrayOfString, arrayOfLong);
      ProgramFragment programFragment = new ProgramFragment(l, this.mRS);
      initProgram(programFragment);
      return programFragment;
    }
  }
}
