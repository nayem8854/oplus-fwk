package android.renderscript;

public class Float4 {
  public float w;
  
  public float x;
  
  public float y;
  
  public float z;
  
  public Float4() {}
  
  public Float4(Float4 paramFloat4) {
    this.x = paramFloat4.x;
    this.y = paramFloat4.y;
    this.z = paramFloat4.z;
    this.w = paramFloat4.w;
  }
  
  public Float4(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    this.x = paramFloat1;
    this.y = paramFloat2;
    this.z = paramFloat3;
    this.w = paramFloat4;
  }
  
  public static Float4 add(Float4 paramFloat41, Float4 paramFloat42) {
    Float4 float4 = new Float4();
    paramFloat41.x += paramFloat42.x;
    paramFloat41.y += paramFloat42.y;
    paramFloat41.z += paramFloat42.z;
    paramFloat41.w += paramFloat42.w;
    return float4;
  }
  
  public void add(Float4 paramFloat4) {
    this.x += paramFloat4.x;
    this.y += paramFloat4.y;
    this.z += paramFloat4.z;
    this.w += paramFloat4.w;
  }
  
  public void add(float paramFloat) {
    this.x += paramFloat;
    this.y += paramFloat;
    this.z += paramFloat;
    this.w += paramFloat;
  }
  
  public static Float4 add(Float4 paramFloat4, float paramFloat) {
    Float4 float4 = new Float4();
    paramFloat4.x += paramFloat;
    paramFloat4.y += paramFloat;
    paramFloat4.z += paramFloat;
    paramFloat4.w += paramFloat;
    return float4;
  }
  
  public void sub(Float4 paramFloat4) {
    this.x -= paramFloat4.x;
    this.y -= paramFloat4.y;
    this.z -= paramFloat4.z;
    this.w -= paramFloat4.w;
  }
  
  public void sub(float paramFloat) {
    this.x -= paramFloat;
    this.y -= paramFloat;
    this.z -= paramFloat;
    this.w -= paramFloat;
  }
  
  public static Float4 sub(Float4 paramFloat4, float paramFloat) {
    Float4 float4 = new Float4();
    paramFloat4.x -= paramFloat;
    paramFloat4.y -= paramFloat;
    paramFloat4.z -= paramFloat;
    paramFloat4.w -= paramFloat;
    return float4;
  }
  
  public static Float4 sub(Float4 paramFloat41, Float4 paramFloat42) {
    Float4 float4 = new Float4();
    paramFloat41.x -= paramFloat42.x;
    paramFloat41.y -= paramFloat42.y;
    paramFloat41.z -= paramFloat42.z;
    paramFloat41.w -= paramFloat42.w;
    return float4;
  }
  
  public void mul(Float4 paramFloat4) {
    this.x *= paramFloat4.x;
    this.y *= paramFloat4.y;
    this.z *= paramFloat4.z;
    this.w *= paramFloat4.w;
  }
  
  public void mul(float paramFloat) {
    this.x *= paramFloat;
    this.y *= paramFloat;
    this.z *= paramFloat;
    this.w *= paramFloat;
  }
  
  public static Float4 mul(Float4 paramFloat41, Float4 paramFloat42) {
    Float4 float4 = new Float4();
    paramFloat41.x *= paramFloat42.x;
    paramFloat41.y *= paramFloat42.y;
    paramFloat41.z *= paramFloat42.z;
    paramFloat41.w *= paramFloat42.w;
    return float4;
  }
  
  public static Float4 mul(Float4 paramFloat4, float paramFloat) {
    Float4 float4 = new Float4();
    paramFloat4.x *= paramFloat;
    paramFloat4.y *= paramFloat;
    paramFloat4.z *= paramFloat;
    paramFloat4.w *= paramFloat;
    return float4;
  }
  
  public void div(Float4 paramFloat4) {
    this.x /= paramFloat4.x;
    this.y /= paramFloat4.y;
    this.z /= paramFloat4.z;
    this.w /= paramFloat4.w;
  }
  
  public void div(float paramFloat) {
    this.x /= paramFloat;
    this.y /= paramFloat;
    this.z /= paramFloat;
    this.w /= paramFloat;
  }
  
  public static Float4 div(Float4 paramFloat4, float paramFloat) {
    Float4 float4 = new Float4();
    paramFloat4.x /= paramFloat;
    paramFloat4.y /= paramFloat;
    paramFloat4.z /= paramFloat;
    paramFloat4.w /= paramFloat;
    return float4;
  }
  
  public static Float4 div(Float4 paramFloat41, Float4 paramFloat42) {
    Float4 float4 = new Float4();
    paramFloat41.x /= paramFloat42.x;
    paramFloat41.y /= paramFloat42.y;
    paramFloat41.z /= paramFloat42.z;
    paramFloat41.w /= paramFloat42.w;
    return float4;
  }
  
  public float dotProduct(Float4 paramFloat4) {
    return this.x * paramFloat4.x + this.y * paramFloat4.y + this.z * paramFloat4.z + this.w * paramFloat4.w;
  }
  
  public static float dotProduct(Float4 paramFloat41, Float4 paramFloat42) {
    return paramFloat42.x * paramFloat41.x + paramFloat42.y * paramFloat41.y + paramFloat42.z * paramFloat41.z + paramFloat42.w * paramFloat41.w;
  }
  
  public void addMultiple(Float4 paramFloat4, float paramFloat) {
    this.x += paramFloat4.x * paramFloat;
    this.y += paramFloat4.y * paramFloat;
    this.z += paramFloat4.z * paramFloat;
    this.w += paramFloat4.w * paramFloat;
  }
  
  public void set(Float4 paramFloat4) {
    this.x = paramFloat4.x;
    this.y = paramFloat4.y;
    this.z = paramFloat4.z;
    this.w = paramFloat4.w;
  }
  
  public void negate() {
    this.x = -this.x;
    this.y = -this.y;
    this.z = -this.z;
    this.w = -this.w;
  }
  
  public int length() {
    return 4;
  }
  
  public float elementSum() {
    return this.x + this.y + this.z + this.w;
  }
  
  public float get(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt == 3)
            return this.w; 
          throw new IndexOutOfBoundsException("Index: i");
        } 
        return this.z;
      } 
      return this.y;
    } 
    return this.x;
  }
  
  public void setAt(int paramInt, float paramFloat) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt == 3) {
            this.w = paramFloat;
            return;
          } 
          throw new IndexOutOfBoundsException("Index: i");
        } 
        this.z = paramFloat;
        return;
      } 
      this.y = paramFloat;
      return;
    } 
    this.x = paramFloat;
  }
  
  public void addAt(int paramInt, float paramFloat) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt == 3) {
            this.w += paramFloat;
            return;
          } 
          throw new IndexOutOfBoundsException("Index: i");
        } 
        this.z += paramFloat;
        return;
      } 
      this.y += paramFloat;
      return;
    } 
    this.x += paramFloat;
  }
  
  public void setValues(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    this.x = paramFloat1;
    this.y = paramFloat2;
    this.z = paramFloat3;
    this.w = paramFloat4;
  }
  
  public void copyTo(float[] paramArrayOffloat, int paramInt) {
    paramArrayOffloat[paramInt] = this.x;
    paramArrayOffloat[paramInt + 1] = this.y;
    paramArrayOffloat[paramInt + 2] = this.z;
    paramArrayOffloat[paramInt + 3] = this.w;
  }
}
