package android.renderscript;

public final class ScriptIntrinsicLUT extends ScriptIntrinsic {
  private final Matrix4f mMatrix = new Matrix4f();
  
  private final byte[] mCache = new byte[1024];
  
  private boolean mDirty = true;
  
  private Allocation mTables;
  
  private ScriptIntrinsicLUT(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
    this.mTables = Allocation.createSized(paramRenderScript, Element.U8(paramRenderScript), 1024);
    for (byte b = 0; b < 'Ā'; b++) {
      byte[] arrayOfByte = this.mCache;
      arrayOfByte[b] = (byte)b;
      arrayOfByte[b + 256] = (byte)b;
      arrayOfByte[b + 512] = (byte)b;
      arrayOfByte[b + 768] = (byte)b;
    } 
    setVar(0, this.mTables);
  }
  
  public static ScriptIntrinsicLUT create(RenderScript paramRenderScript, Element paramElement) {
    long l = paramRenderScript.nScriptIntrinsicCreate(3, paramElement.getID(paramRenderScript));
    return new ScriptIntrinsicLUT(l, paramRenderScript);
  }
  
  public void destroy() {
    this.mTables.destroy();
    super.destroy();
  }
  
  private void validate(int paramInt1, int paramInt2) {
    if (paramInt1 >= 0 && paramInt1 <= 255) {
      if (paramInt2 >= 0 && paramInt2 <= 255)
        return; 
      throw new RSIllegalArgumentException("Value out of range (0-255).");
    } 
    throw new RSIllegalArgumentException("Index out of range (0-255).");
  }
  
  public void setRed(int paramInt1, int paramInt2) {
    validate(paramInt1, paramInt2);
    this.mCache[paramInt1] = (byte)paramInt2;
    this.mDirty = true;
  }
  
  public void setGreen(int paramInt1, int paramInt2) {
    validate(paramInt1, paramInt2);
    this.mCache[paramInt1 + 256] = (byte)paramInt2;
    this.mDirty = true;
  }
  
  public void setBlue(int paramInt1, int paramInt2) {
    validate(paramInt1, paramInt2);
    this.mCache[paramInt1 + 512] = (byte)paramInt2;
    this.mDirty = true;
  }
  
  public void setAlpha(int paramInt1, int paramInt2) {
    validate(paramInt1, paramInt2);
    this.mCache[paramInt1 + 768] = (byte)paramInt2;
    this.mDirty = true;
  }
  
  public void forEach(Allocation paramAllocation1, Allocation paramAllocation2) {
    forEach(paramAllocation1, paramAllocation2, (Script.LaunchOptions)null);
  }
  
  public void forEach(Allocation paramAllocation1, Allocation paramAllocation2, Script.LaunchOptions paramLaunchOptions) {
    if (this.mDirty) {
      this.mDirty = false;
      this.mTables.copyFromUnchecked(this.mCache);
    } 
    forEach(0, paramAllocation1, paramAllocation2, (FieldPacker)null, paramLaunchOptions);
  }
  
  public Script.KernelID getKernelID() {
    return createKernelID(0, 3, (Element)null, (Element)null);
  }
}
