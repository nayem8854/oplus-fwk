package android.renderscript;

import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Environment;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class Font extends BaseObj {
  private static Map<String, FontFamily> sFontFamilyMap;
  
  private static final String[] sMonoNames;
  
  private static final String[] sSansNames = new String[] { "sans-serif", "arial", "helvetica", "tahoma", "verdana" };
  
  private static final String[] sSerifNames = new String[] { "serif", "times", "times new roman", "palatino", "georgia", "baskerville", "goudy", "fantasy", "cursive", "ITC Stone Serif" };
  
  static {
    sMonoNames = new String[] { "monospace", "courier", "courier new", "monaco" };
    initFontFamilyMap();
  }
  
  class FontFamily {
    String mBoldFileName;
    
    String mBoldItalicFileName;
    
    String mItalicFileName;
    
    String[] mNames;
    
    String mNormalFileName;
    
    private FontFamily() {}
  }
  
  class Style extends Enum<Style> {
    private static final Style[] $VALUES;
    
    public static final Style BOLD = new Style("BOLD", 1);
    
    public static final Style BOLD_ITALIC;
    
    public static final Style ITALIC = new Style("ITALIC", 2);
    
    public static Style[] values() {
      return (Style[])$VALUES.clone();
    }
    
    public static Style valueOf(String param1String) {
      return Enum.<Style>valueOf(Style.class, param1String);
    }
    
    private Style(Font this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static final Style NORMAL = new Style("NORMAL", 0);
    
    static {
      Style style = new Style("BOLD_ITALIC", 3);
      $VALUES = new Style[] { NORMAL, BOLD, ITALIC, style };
    }
  }
  
  private static void addFamilyToMap(FontFamily paramFontFamily) {
    for (byte b = 0; b < paramFontFamily.mNames.length; b++)
      sFontFamilyMap.put(paramFontFamily.mNames[b], paramFontFamily); 
  }
  
  private static void initFontFamilyMap() {
    sFontFamilyMap = new HashMap<>();
    FontFamily fontFamily = new FontFamily();
    fontFamily.mNames = sSansNames;
    fontFamily.mNormalFileName = "Roboto-Regular.ttf";
    fontFamily.mBoldFileName = "Roboto-Bold.ttf";
    fontFamily.mItalicFileName = "Roboto-Italic.ttf";
    fontFamily.mBoldItalicFileName = "Roboto-BoldItalic.ttf";
    addFamilyToMap(fontFamily);
    fontFamily = new FontFamily();
    fontFamily.mNames = sSerifNames;
    fontFamily.mNormalFileName = "NotoSerif-Regular.ttf";
    fontFamily.mBoldFileName = "NotoSerif-Bold.ttf";
    fontFamily.mItalicFileName = "NotoSerif-Italic.ttf";
    fontFamily.mBoldItalicFileName = "NotoSerif-BoldItalic.ttf";
    addFamilyToMap(fontFamily);
    fontFamily = new FontFamily();
    fontFamily.mNames = sMonoNames;
    fontFamily.mNormalFileName = "DroidSansMono.ttf";
    fontFamily.mBoldFileName = "DroidSansMono.ttf";
    fontFamily.mItalicFileName = "DroidSansMono.ttf";
    fontFamily.mBoldItalicFileName = "DroidSansMono.ttf";
    addFamilyToMap(fontFamily);
  }
  
  static String getFontFileName(String paramString, Style paramStyle) {
    FontFamily fontFamily = sFontFamilyMap.get(paramString);
    if (fontFamily != null) {
      int i = null.$SwitchMap$android$renderscript$Font$Style[paramStyle.ordinal()];
      if (i != 1) {
        if (i != 2) {
          if (i != 3) {
            if (i == 4)
              return fontFamily.mBoldItalicFileName; 
          } else {
            return fontFamily.mItalicFileName;
          } 
        } else {
          return fontFamily.mBoldFileName;
        } 
      } else {
        return fontFamily.mNormalFileName;
      } 
    } 
    return "DroidSans.ttf";
  }
  
  Font(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
    this.guard.open("destroy");
  }
  
  public static Font createFromFile(RenderScript paramRenderScript, Resources paramResources, String paramString, float paramFloat) {
    paramRenderScript.validate();
    int i = (paramResources.getDisplayMetrics()).densityDpi;
    long l = paramRenderScript.nFontCreateFromFile(paramString, paramFloat, i);
    if (l != 0L)
      return new Font(l, paramRenderScript); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unable to create font from file ");
    stringBuilder.append(paramString);
    throw new RSRuntimeException(stringBuilder.toString());
  }
  
  public static Font createFromFile(RenderScript paramRenderScript, Resources paramResources, File paramFile, float paramFloat) {
    return createFromFile(paramRenderScript, paramResources, paramFile.getAbsolutePath(), paramFloat);
  }
  
  public static Font createFromAsset(RenderScript paramRenderScript, Resources paramResources, String paramString, float paramFloat) {
    paramRenderScript.validate();
    AssetManager assetManager = paramResources.getAssets();
    int i = (paramResources.getDisplayMetrics()).densityDpi;
    long l = paramRenderScript.nFontCreateFromAsset(assetManager, paramString, paramFloat, i);
    if (l != 0L)
      return new Font(l, paramRenderScript); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unable to create font from asset ");
    stringBuilder.append(paramString);
    throw new RSRuntimeException(stringBuilder.toString());
  }
  
  public static Font createFromResource(RenderScript paramRenderScript, Resources paramResources, int paramInt, float paramFloat) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("R.");
    stringBuilder.append(Integer.toString(paramInt));
    String str = stringBuilder.toString();
    paramRenderScript.validate();
    try {
      InputStream inputStream = paramResources.openRawResource(paramInt);
      int i = (paramResources.getDisplayMetrics()).densityDpi;
      if (inputStream instanceof AssetManager.AssetInputStream) {
        long l = ((AssetManager.AssetInputStream)inputStream).getNativeAsset();
        l = paramRenderScript.nFontCreateFromAssetStream(str, paramFloat, i, l);
        if (l != 0L)
          return new Font(l, paramRenderScript); 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Unable to create font from resource ");
        stringBuilder1.append(paramInt);
        throw new RSRuntimeException(stringBuilder1.toString());
      } 
      throw new RSRuntimeException("Unsupported asset stream created");
    } catch (Exception exception) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Unable to open resource ");
      stringBuilder1.append(paramInt);
      throw new RSRuntimeException(stringBuilder1.toString());
    } 
  }
  
  public static Font create(RenderScript paramRenderScript, Resources paramResources, String paramString, Style paramStyle, float paramFloat) {
    String str2 = getFontFileName(paramString, paramStyle);
    String str3 = Environment.getRootDirectory().getAbsolutePath();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str3);
    stringBuilder.append("/fonts/");
    stringBuilder.append(str2);
    String str1 = stringBuilder.toString();
    return createFromFile(paramRenderScript, paramResources, str1, paramFloat);
  }
}
