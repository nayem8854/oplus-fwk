package android.renderscript;

public final class ScriptIntrinsicBlur extends ScriptIntrinsic {
  private Allocation mInput;
  
  private final float[] mValues = new float[9];
  
  private ScriptIntrinsicBlur(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
  }
  
  public static ScriptIntrinsicBlur create(RenderScript paramRenderScript, Element paramElement) {
    if (paramElement.isCompatible(Element.U8_4(paramRenderScript)) || paramElement.isCompatible(Element.U8(paramRenderScript))) {
      long l = paramRenderScript.nScriptIntrinsicCreate(5, paramElement.getID(paramRenderScript));
      ScriptIntrinsicBlur scriptIntrinsicBlur = new ScriptIntrinsicBlur(l, paramRenderScript);
      scriptIntrinsicBlur.setRadius(5.0F);
      return scriptIntrinsicBlur;
    } 
    throw new RSIllegalArgumentException("Unsupported element type.");
  }
  
  public void setInput(Allocation paramAllocation) {
    if (paramAllocation.getType().getY() != 0) {
      Element element = paramAllocation.getElement();
      if (element.isCompatible(Element.U8_4(this.mRS)) || element.isCompatible(Element.U8(this.mRS))) {
        this.mInput = paramAllocation;
        setVar(1, paramAllocation);
        return;
      } 
      throw new RSIllegalArgumentException("Unsupported element type.");
    } 
    throw new RSIllegalArgumentException("Input set to a 1D Allocation");
  }
  
  public void setRadius(float paramFloat) {
    if (paramFloat > 0.0F && paramFloat <= 25.0F) {
      setVar(0, paramFloat);
      return;
    } 
    throw new RSIllegalArgumentException("Radius out of range (0 < r <= 25).");
  }
  
  public void forEach(Allocation paramAllocation) {
    if (paramAllocation.getType().getY() != 0) {
      forEach(0, (Allocation)null, paramAllocation, (FieldPacker)null);
      return;
    } 
    throw new RSIllegalArgumentException("Output is a 1D Allocation");
  }
  
  public void forEach(Allocation paramAllocation, Script.LaunchOptions paramLaunchOptions) {
    if (paramAllocation.getType().getY() != 0) {
      forEach(0, (Allocation)null, paramAllocation, (FieldPacker)null, paramLaunchOptions);
      return;
    } 
    throw new RSIllegalArgumentException("Output is a 1D Allocation");
  }
  
  public Script.KernelID getKernelID() {
    return createKernelID(0, 2, (Element)null, (Element)null);
  }
  
  public Script.FieldID getFieldID_Input() {
    return createFieldID(1, (Element)null);
  }
}
