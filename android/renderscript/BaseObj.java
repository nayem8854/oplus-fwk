package android.renderscript;

import dalvik.system.CloseGuard;
import java.io.UnsupportedEncodingException;

public class BaseObj {
  final CloseGuard guard;
  
  private boolean mDestroyed;
  
  private long mID;
  
  private String mName;
  
  RenderScript mRS;
  
  BaseObj(long paramLong, RenderScript paramRenderScript) {
    this.guard = CloseGuard.get();
    paramRenderScript.validate();
    this.mRS = paramRenderScript;
    this.mID = paramLong;
    this.mDestroyed = false;
  }
  
  void setID(long paramLong) {
    if (this.mID == 0L) {
      this.mID = paramLong;
      return;
    } 
    throw new RSRuntimeException("Internal Error, reset of object ID.");
  }
  
  long getID(RenderScript paramRenderScript) {
    this.mRS.validate();
    if (!this.mDestroyed) {
      if (this.mID != 0L) {
        if (paramRenderScript == null || paramRenderScript == this.mRS)
          return this.mID; 
        throw new RSInvalidStateException("using object with mismatched context.");
      } 
      throw new RSRuntimeException("Internal error: Object id 0.");
    } 
    throw new RSInvalidStateException("using a destroyed object.");
  }
  
  void checkValid() {
    if (this.mID != 0L)
      return; 
    throw new RSIllegalArgumentException("Invalid object.");
  }
  
  public void setName(String paramString) {
    if (paramString != null) {
      if (paramString.length() >= 1) {
        if (this.mName == null)
          try {
            byte[] arrayOfByte = paramString.getBytes("UTF-8");
            this.mRS.nAssignName(this.mID, arrayOfByte);
            this.mName = paramString;
            return;
          } catch (UnsupportedEncodingException unsupportedEncodingException) {
            throw new RuntimeException(unsupportedEncodingException);
          }  
        throw new RSIllegalArgumentException("setName object already has a name.");
      } 
      throw new RSIllegalArgumentException("setName does not accept a zero length string.");
    } 
    throw new RSIllegalArgumentException("setName requires a string of non-zero length.");
  }
  
  public String getName() {
    return this.mName;
  }
  
  private void helpDestroy() {
    // Byte code:
    //   0: iconst_0
    //   1: istore_1
    //   2: aload_0
    //   3: monitorenter
    //   4: aload_0
    //   5: getfield mDestroyed : Z
    //   8: ifne -> 18
    //   11: iconst_1
    //   12: istore_1
    //   13: aload_0
    //   14: iconst_1
    //   15: putfield mDestroyed : Z
    //   18: aload_0
    //   19: monitorexit
    //   20: iload_1
    //   21: ifeq -> 89
    //   24: aload_0
    //   25: getfield guard : Ldalvik/system/CloseGuard;
    //   28: invokevirtual close : ()V
    //   31: aload_0
    //   32: getfield mRS : Landroid/renderscript/RenderScript;
    //   35: getfield mRWLock : Ljava/util/concurrent/locks/ReentrantReadWriteLock;
    //   38: invokevirtual readLock : ()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;
    //   41: astore_2
    //   42: aload_2
    //   43: invokevirtual lock : ()V
    //   46: aload_0
    //   47: getfield mRS : Landroid/renderscript/RenderScript;
    //   50: invokevirtual isAlive : ()Z
    //   53: ifeq -> 75
    //   56: aload_0
    //   57: getfield mID : J
    //   60: lstore_3
    //   61: lload_3
    //   62: lconst_0
    //   63: lcmp
    //   64: ifeq -> 75
    //   67: aload_0
    //   68: getfield mRS : Landroid/renderscript/RenderScript;
    //   71: lload_3
    //   72: invokevirtual nObjDestroy : (J)V
    //   75: aload_2
    //   76: invokevirtual unlock : ()V
    //   79: aload_0
    //   80: aconst_null
    //   81: putfield mRS : Landroid/renderscript/RenderScript;
    //   84: aload_0
    //   85: lconst_0
    //   86: putfield mID : J
    //   89: return
    //   90: astore_2
    //   91: aload_0
    //   92: monitorexit
    //   93: aload_2
    //   94: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #119	-> 0
    //   #120	-> 2
    //   #121	-> 4
    //   #122	-> 11
    //   #123	-> 13
    //   #125	-> 18
    //   #127	-> 20
    //   #128	-> 24
    //   #130	-> 31
    //   #131	-> 42
    //   #133	-> 46
    //   #134	-> 67
    //   #136	-> 75
    //   #137	-> 79
    //   #138	-> 84
    //   #140	-> 89
    //   #125	-> 90
    // Exception table:
    //   from	to	target	type
    //   4	11	90	finally
    //   13	18	90	finally
    //   18	20	90	finally
    //   91	93	90	finally
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.guard != null)
        this.guard.warnIfOpen(); 
      helpDestroy();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public void destroy() {
    if (!this.mDestroyed) {
      helpDestroy();
      return;
    } 
    throw new RSInvalidStateException("Object already destroyed.");
  }
  
  void updateFromNative() {
    this.mRS.validate();
    RenderScript renderScript = this.mRS;
    this.mName = renderScript.nGetName(getID(renderScript));
  }
  
  public int hashCode() {
    long l = this.mID;
    return (int)(l >> 32L ^ 0xFFFFFFFL & l);
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mID != ((BaseObj)paramObject).mID)
      bool = false; 
    return bool;
  }
}
