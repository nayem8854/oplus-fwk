package android.renderscript;

public final class ScriptIntrinsicConvolve5x5 extends ScriptIntrinsic {
  private Allocation mInput;
  
  private final float[] mValues = new float[25];
  
  private ScriptIntrinsicConvolve5x5(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
  }
  
  public static ScriptIntrinsicConvolve5x5 create(RenderScript paramRenderScript, Element paramElement) {
    if (paramElement.isCompatible(Element.U8(paramRenderScript)) || 
      paramElement.isCompatible(Element.U8_2(paramRenderScript)) || 
      paramElement.isCompatible(Element.U8_3(paramRenderScript)) || 
      paramElement.isCompatible(Element.U8_4(paramRenderScript)) || 
      paramElement.isCompatible(Element.F32(paramRenderScript)) || 
      paramElement.isCompatible(Element.F32_2(paramRenderScript)) || 
      paramElement.isCompatible(Element.F32_3(paramRenderScript)) || 
      paramElement.isCompatible(Element.F32_4(paramRenderScript))) {
      long l = paramRenderScript.nScriptIntrinsicCreate(4, paramElement.getID(paramRenderScript));
      return new ScriptIntrinsicConvolve5x5(l, paramRenderScript);
    } 
    throw new RSIllegalArgumentException("Unsupported element type.");
  }
  
  public void setInput(Allocation paramAllocation) {
    this.mInput = paramAllocation;
    setVar(1, paramAllocation);
  }
  
  public void setCoefficients(float[] paramArrayOffloat) {
    FieldPacker fieldPacker = new FieldPacker(100);
    byte b = 0;
    while (true) {
      float[] arrayOfFloat = this.mValues;
      if (b < arrayOfFloat.length) {
        arrayOfFloat[b] = paramArrayOffloat[b];
        fieldPacker.addF32(arrayOfFloat[b]);
        b++;
        continue;
      } 
      break;
    } 
    setVar(0, fieldPacker);
  }
  
  public void forEach(Allocation paramAllocation) {
    forEach(0, (Allocation)null, paramAllocation, (FieldPacker)null);
  }
  
  public void forEach(Allocation paramAllocation, Script.LaunchOptions paramLaunchOptions) {
    forEach(0, (Allocation)null, paramAllocation, (FieldPacker)null, paramLaunchOptions);
  }
  
  public Script.KernelID getKernelID() {
    return createKernelID(0, 2, null, null);
  }
  
  public Script.FieldID getFieldID_Input() {
    return createFieldID(1, null);
  }
}
