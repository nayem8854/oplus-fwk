package android.renderscript;

public class ProgramVertexFixedFunction extends ProgramVertex {
  ProgramVertexFixedFunction(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
  }
  
  public void bindConstants(Constants paramConstants) {
    this.mRS.validate();
    bindConstants(paramConstants.getAllocation(), 0);
  }
  
  class InternalBuilder extends Program.BaseProgramBuilder {
    public InternalBuilder(ProgramVertexFixedFunction this$0) {
      super((RenderScript)this$0);
    }
    
    public InternalBuilder addInput(Element param1Element) throws IllegalStateException {
      if (this.mInputCount < 8) {
        if (!param1Element.isComplex()) {
          Element[] arrayOfElement = this.mInputs;
          int i = this.mInputCount;
          this.mInputCount = i + 1;
          arrayOfElement[i] = param1Element;
          return this;
        } 
        throw new RSIllegalArgumentException("Complex elements not allowed.");
      } 
      throw new RSIllegalArgumentException("Max input count exceeded.");
    }
    
    public ProgramVertexFixedFunction create() {
      this.mRS.validate();
      long[] arrayOfLong = new long[(this.mInputCount + this.mOutputCount + this.mConstantCount + this.mTextureCount) * 2];
      String[] arrayOfString = new String[this.mTextureCount];
      int i = 0;
      byte b;
      for (b = 0; b < this.mInputCount; b++) {
        int j = i + 1;
        arrayOfLong[i] = Program.ProgramParam.INPUT.mID;
        i = j + 1;
        arrayOfLong[j] = this.mInputs[b].getID(this.mRS);
      } 
      for (b = 0; b < this.mOutputCount; b++) {
        int j = i + 1;
        arrayOfLong[i] = Program.ProgramParam.OUTPUT.mID;
        i = j + 1;
        arrayOfLong[j] = this.mOutputs[b].getID(this.mRS);
      } 
      for (b = 0; b < this.mConstantCount; b++) {
        int j = i + 1;
        arrayOfLong[i] = Program.ProgramParam.CONSTANT.mID;
        i = j + 1;
        arrayOfLong[j] = this.mConstants[b].getID(this.mRS);
      } 
      for (b = 0; b < this.mTextureCount; b++) {
        int j = i + 1;
        arrayOfLong[i] = Program.ProgramParam.TEXTURE_TYPE.mID;
        i = j + 1;
        arrayOfLong[j] = (this.mTextureTypes[b]).mID;
        arrayOfString[b] = this.mTextureNames[b];
      } 
      long l = this.mRS.nProgramVertexCreate(this.mShader, arrayOfString, arrayOfLong);
      ProgramVertexFixedFunction programVertexFixedFunction = new ProgramVertexFixedFunction(l, this.mRS);
      initProgram(programVertexFixedFunction);
      return programVertexFixedFunction;
    }
  }
  
  class Builder {
    RenderScript mRS;
    
    String mShader;
    
    boolean mTextureMatrixEnable;
    
    public Builder(ProgramVertexFixedFunction this$0) {
      this.mRS = (RenderScript)this$0;
    }
    
    public Builder setTextureMatrixEnable(boolean param1Boolean) {
      this.mTextureMatrixEnable = param1Boolean;
      return this;
    }
    
    static Type getConstantInputType(RenderScript param1RenderScript) {
      Element.Builder builder1 = new Element.Builder(param1RenderScript);
      builder1.add(Element.MATRIX4X4(param1RenderScript), "MV");
      builder1.add(Element.MATRIX4X4(param1RenderScript), "P");
      builder1.add(Element.MATRIX4X4(param1RenderScript), "TexMatrix");
      builder1.add(Element.MATRIX4X4(param1RenderScript), "MVP");
      Type.Builder builder = new Type.Builder(param1RenderScript, builder1.create());
      builder.setX(1);
      return builder.create();
    }
    
    private void buildShaderString() {
      this.mShader = "//rs_shader_internal\n";
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this.mShader);
      stringBuilder.append("varying vec4 varColor;\n");
      this.mShader = stringBuilder.toString();
      stringBuilder = new StringBuilder();
      stringBuilder.append(this.mShader);
      stringBuilder.append("varying vec2 varTex0;\n");
      this.mShader = stringBuilder.toString();
      stringBuilder = new StringBuilder();
      stringBuilder.append(this.mShader);
      stringBuilder.append("void main() {\n");
      this.mShader = stringBuilder.toString();
      stringBuilder = new StringBuilder();
      stringBuilder.append(this.mShader);
      stringBuilder.append("  gl_Position = UNI_MVP * ATTRIB_position;\n");
      this.mShader = stringBuilder.toString();
      stringBuilder = new StringBuilder();
      stringBuilder.append(this.mShader);
      stringBuilder.append("  gl_PointSize = 1.0;\n");
      this.mShader = stringBuilder.toString();
      stringBuilder = new StringBuilder();
      stringBuilder.append(this.mShader);
      stringBuilder.append("  varColor = ATTRIB_color;\n");
      this.mShader = stringBuilder.toString();
      if (this.mTextureMatrixEnable) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(this.mShader);
        stringBuilder.append("  varTex0 = (UNI_TexMatrix * vec4(ATTRIB_texture0, 0.0, 1.0)).xy;\n");
        this.mShader = stringBuilder.toString();
      } else {
        stringBuilder = new StringBuilder();
        stringBuilder.append(this.mShader);
        stringBuilder.append("  varTex0 = ATTRIB_texture0;\n");
        this.mShader = stringBuilder.toString();
      } 
      stringBuilder = new StringBuilder();
      stringBuilder.append(this.mShader);
      stringBuilder.append("}\n");
      this.mShader = stringBuilder.toString();
    }
    
    public ProgramVertexFixedFunction create() {
      buildShaderString();
      ProgramVertexFixedFunction.InternalBuilder internalBuilder = new ProgramVertexFixedFunction.InternalBuilder(this.mRS);
      internalBuilder.setShader(this.mShader);
      internalBuilder.addConstant(getConstantInputType(this.mRS));
      Element.Builder builder = new Element.Builder(this.mRS);
      builder.add(Element.F32_4(this.mRS), "position");
      builder.add(Element.F32_4(this.mRS), "color");
      builder.add(Element.F32_3(this.mRS), "normal");
      builder.add(Element.F32_2(this.mRS), "texture0");
      internalBuilder.addInput(builder.create());
      return internalBuilder.create();
    }
  }
  
  class Constants {
    static final int MODELVIEW_OFFSET = 0;
    
    static final int PROJECTION_OFFSET = 16;
    
    static final int TEXTURE_OFFSET = 32;
    
    Allocation mAlloc;
    
    private FieldPacker mIOBuffer;
    
    Matrix4f mModel;
    
    Matrix4f mProjection;
    
    Matrix4f mTexture;
    
    Allocation getAllocation() {
      return this.mAlloc;
    }
    
    public Constants(ProgramVertexFixedFunction this$0) {
      Type type = ProgramVertexFixedFunction.Builder.getConstantInputType((RenderScript)this$0);
      this.mAlloc = Allocation.createTyped((RenderScript)this$0, type);
      int i = type.getElement().getBytesSize();
      int j = type.getCount();
      this.mIOBuffer = new FieldPacker(i * j);
      this.mModel = new Matrix4f();
      this.mProjection = new Matrix4f();
      this.mTexture = new Matrix4f();
      setModelview(new Matrix4f());
      setProjection(new Matrix4f());
      setTexture(new Matrix4f());
    }
    
    public void destroy() {
      this.mAlloc.destroy();
      this.mAlloc = null;
    }
    
    private void addToBuffer(int param1Int, Matrix4f param1Matrix4f) {
      this.mIOBuffer.reset(param1Int);
      for (param1Int = 0; param1Int < 16; param1Int++)
        this.mIOBuffer.addF32(param1Matrix4f.mMat[param1Int]); 
      FieldPacker fieldPacker = this.mIOBuffer;
      fieldPacker.reset((fieldPacker.getData()).length);
      this.mAlloc.setFromFieldPacker(0, this.mIOBuffer);
    }
    
    public void setModelview(Matrix4f param1Matrix4f) {
      this.mModel.load(param1Matrix4f);
      addToBuffer(0, param1Matrix4f);
    }
    
    public void setProjection(Matrix4f param1Matrix4f) {
      this.mProjection.load(param1Matrix4f);
      addToBuffer(64, param1Matrix4f);
    }
    
    public void setTexture(Matrix4f param1Matrix4f) {
      this.mTexture.load(param1Matrix4f);
      addToBuffer(128, param1Matrix4f);
    }
  }
}
