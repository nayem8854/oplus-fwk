package android.renderscript;

public final class ScriptIntrinsicColorMatrix extends ScriptIntrinsic {
  private final Matrix4f mMatrix = new Matrix4f();
  
  private final Float4 mAdd = new Float4();
  
  private ScriptIntrinsicColorMatrix(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
  }
  
  @Deprecated
  public static ScriptIntrinsicColorMatrix create(RenderScript paramRenderScript, Element paramElement) {
    return create(paramRenderScript);
  }
  
  public static ScriptIntrinsicColorMatrix create(RenderScript paramRenderScript) {
    long l = paramRenderScript.nScriptIntrinsicCreate(2, 0L);
    return new ScriptIntrinsicColorMatrix(l, paramRenderScript);
  }
  
  private void setMatrix() {
    FieldPacker fieldPacker = new FieldPacker(64);
    fieldPacker.addMatrix(this.mMatrix);
    setVar(0, fieldPacker);
  }
  
  public void setColorMatrix(Matrix4f paramMatrix4f) {
    this.mMatrix.load(paramMatrix4f);
    setMatrix();
  }
  
  public void setColorMatrix(Matrix3f paramMatrix3f) {
    this.mMatrix.load(paramMatrix3f);
    setMatrix();
  }
  
  public void setAdd(Float4 paramFloat4) {
    this.mAdd.x = paramFloat4.x;
    this.mAdd.y = paramFloat4.y;
    this.mAdd.z = paramFloat4.z;
    this.mAdd.w = paramFloat4.w;
    FieldPacker fieldPacker = new FieldPacker(16);
    fieldPacker.addF32(paramFloat4.x);
    fieldPacker.addF32(paramFloat4.y);
    fieldPacker.addF32(paramFloat4.z);
    fieldPacker.addF32(paramFloat4.w);
    setVar(1, fieldPacker);
  }
  
  public void setAdd(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    this.mAdd.x = paramFloat1;
    this.mAdd.y = paramFloat2;
    this.mAdd.z = paramFloat3;
    this.mAdd.w = paramFloat4;
    FieldPacker fieldPacker = new FieldPacker(16);
    fieldPacker.addF32(this.mAdd.x);
    fieldPacker.addF32(this.mAdd.y);
    fieldPacker.addF32(this.mAdd.z);
    fieldPacker.addF32(this.mAdd.w);
    setVar(1, fieldPacker);
  }
  
  public void setGreyscale() {
    this.mMatrix.loadIdentity();
    this.mMatrix.set(0, 0, 0.299F);
    this.mMatrix.set(1, 0, 0.587F);
    this.mMatrix.set(2, 0, 0.114F);
    this.mMatrix.set(0, 1, 0.299F);
    this.mMatrix.set(1, 1, 0.587F);
    this.mMatrix.set(2, 1, 0.114F);
    this.mMatrix.set(0, 2, 0.299F);
    this.mMatrix.set(1, 2, 0.587F);
    this.mMatrix.set(2, 2, 0.114F);
    setMatrix();
  }
  
  public void setYUVtoRGB() {
    this.mMatrix.loadIdentity();
    this.mMatrix.set(0, 0, 1.0F);
    this.mMatrix.set(1, 0, 0.0F);
    this.mMatrix.set(2, 0, 1.13983F);
    this.mMatrix.set(0, 1, 1.0F);
    this.mMatrix.set(1, 1, -0.39465F);
    this.mMatrix.set(2, 1, -0.5806F);
    this.mMatrix.set(0, 2, 1.0F);
    this.mMatrix.set(1, 2, 2.03211F);
    this.mMatrix.set(2, 2, 0.0F);
    setMatrix();
  }
  
  public void setRGBtoYUV() {
    this.mMatrix.loadIdentity();
    this.mMatrix.set(0, 0, 0.299F);
    this.mMatrix.set(1, 0, 0.587F);
    this.mMatrix.set(2, 0, 0.114F);
    this.mMatrix.set(0, 1, -0.14713F);
    this.mMatrix.set(1, 1, -0.28886F);
    this.mMatrix.set(2, 1, 0.436F);
    this.mMatrix.set(0, 2, 0.615F);
    this.mMatrix.set(1, 2, -0.51499F);
    this.mMatrix.set(2, 2, -0.10001F);
    setMatrix();
  }
  
  public void forEach(Allocation paramAllocation1, Allocation paramAllocation2) {
    forEach(paramAllocation1, paramAllocation2, (Script.LaunchOptions)null);
  }
  
  public void forEach(Allocation paramAllocation1, Allocation paramAllocation2, Script.LaunchOptions paramLaunchOptions) {
    if (paramAllocation1.getElement().isCompatible(Element.U8(this.mRS)) || 
      paramAllocation1.getElement().isCompatible(Element.U8_2(this.mRS)) || 
      paramAllocation1.getElement().isCompatible(Element.U8_3(this.mRS)) || 
      paramAllocation1.getElement().isCompatible(Element.U8_4(this.mRS)) || 
      paramAllocation1.getElement().isCompatible(Element.F32(this.mRS)) || 
      paramAllocation1.getElement().isCompatible(Element.F32_2(this.mRS)) || 
      paramAllocation1.getElement().isCompatible(Element.F32_3(this.mRS)) || 
      paramAllocation1.getElement().isCompatible(Element.F32_4(this.mRS))) {
      if (paramAllocation2.getElement().isCompatible(Element.U8(this.mRS)) || 
        paramAllocation2.getElement().isCompatible(Element.U8_2(this.mRS)) || 
        paramAllocation2.getElement().isCompatible(Element.U8_3(this.mRS)) || 
        paramAllocation2.getElement().isCompatible(Element.U8_4(this.mRS)) || 
        paramAllocation2.getElement().isCompatible(Element.F32(this.mRS)) || 
        paramAllocation2.getElement().isCompatible(Element.F32_2(this.mRS)) || 
        paramAllocation2.getElement().isCompatible(Element.F32_3(this.mRS)) || 
        paramAllocation2.getElement().isCompatible(Element.F32_4(this.mRS))) {
        forEach(0, paramAllocation1, paramAllocation2, (FieldPacker)null, paramLaunchOptions);
        return;
      } 
      throw new RSIllegalArgumentException("Unsupported element type.");
    } 
    throw new RSIllegalArgumentException("Unsupported element type.");
  }
  
  public Script.KernelID getKernelID() {
    return createKernelID(0, 3, (Element)null, (Element)null);
  }
}
