package android.renderscript;

public class Matrix4f {
  final float[] mMat;
  
  public Matrix4f() {
    this.mMat = new float[16];
    loadIdentity();
  }
  
  public Matrix4f(float[] paramArrayOffloat) {
    float[] arrayOfFloat = new float[16];
    System.arraycopy(paramArrayOffloat, 0, arrayOfFloat, 0, arrayOfFloat.length);
  }
  
  public float[] getArray() {
    return this.mMat;
  }
  
  public float get(int paramInt1, int paramInt2) {
    return this.mMat[paramInt1 * 4 + paramInt2];
  }
  
  public void set(int paramInt1, int paramInt2, float paramFloat) {
    this.mMat[paramInt1 * 4 + paramInt2] = paramFloat;
  }
  
  public void loadIdentity() {
    float[] arrayOfFloat = this.mMat;
    arrayOfFloat[0] = 1.0F;
    arrayOfFloat[1] = 0.0F;
    arrayOfFloat[2] = 0.0F;
    arrayOfFloat[3] = 0.0F;
    arrayOfFloat[4] = 0.0F;
    arrayOfFloat[5] = 1.0F;
    arrayOfFloat[6] = 0.0F;
    arrayOfFloat[7] = 0.0F;
    arrayOfFloat[8] = 0.0F;
    arrayOfFloat[9] = 0.0F;
    arrayOfFloat[10] = 1.0F;
    arrayOfFloat[11] = 0.0F;
    arrayOfFloat[12] = 0.0F;
    arrayOfFloat[13] = 0.0F;
    arrayOfFloat[14] = 0.0F;
    arrayOfFloat[15] = 1.0F;
  }
  
  public void load(Matrix4f paramMatrix4f) {
    float[] arrayOfFloat1 = paramMatrix4f.getArray(), arrayOfFloat2 = this.mMat;
    System.arraycopy(arrayOfFloat1, 0, arrayOfFloat2, 0, arrayOfFloat2.length);
  }
  
  public void load(Matrix3f paramMatrix3f) {
    this.mMat[0] = paramMatrix3f.mMat[0];
    this.mMat[1] = paramMatrix3f.mMat[1];
    this.mMat[2] = paramMatrix3f.mMat[2];
    float[] arrayOfFloat2 = this.mMat;
    arrayOfFloat2[3] = 0.0F;
    arrayOfFloat2[4] = paramMatrix3f.mMat[3];
    this.mMat[5] = paramMatrix3f.mMat[4];
    this.mMat[6] = paramMatrix3f.mMat[5];
    arrayOfFloat2 = this.mMat;
    arrayOfFloat2[7] = 0.0F;
    arrayOfFloat2[8] = paramMatrix3f.mMat[6];
    this.mMat[9] = paramMatrix3f.mMat[7];
    this.mMat[10] = paramMatrix3f.mMat[8];
    float[] arrayOfFloat1 = this.mMat;
    arrayOfFloat1[11] = 0.0F;
    arrayOfFloat1[12] = 0.0F;
    arrayOfFloat1[13] = 0.0F;
    arrayOfFloat1[14] = 0.0F;
    arrayOfFloat1[15] = 1.0F;
  }
  
  public void loadRotate(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    float[] arrayOfFloat = this.mMat;
    arrayOfFloat[3] = 0.0F;
    arrayOfFloat[7] = 0.0F;
    arrayOfFloat[11] = 0.0F;
    arrayOfFloat[12] = 0.0F;
    arrayOfFloat[13] = 0.0F;
    arrayOfFloat[14] = 0.0F;
    arrayOfFloat[15] = 1.0F;
    paramFloat1 = 0.017453292F * paramFloat1;
    float f1 = (float)Math.cos(paramFloat1);
    float f2 = (float)Math.sin(paramFloat1);
    paramFloat1 = (float)Math.sqrt((paramFloat2 * paramFloat2 + paramFloat3 * paramFloat3 + paramFloat4 * paramFloat4));
    if (paramFloat1 == 1.0F) {
      float f = 1.0F / paramFloat1;
      paramFloat1 = paramFloat2 * f;
      paramFloat2 = paramFloat3 * f;
      paramFloat3 = paramFloat4 * f;
    } else {
      paramFloat1 = paramFloat2;
      paramFloat2 = paramFloat3;
      paramFloat3 = paramFloat4;
    } 
    float f4 = 1.0F - f1;
    paramFloat4 = paramFloat1 * paramFloat2;
    float f5 = paramFloat2 * paramFloat3;
    float f6 = paramFloat3 * paramFloat1;
    float f3 = paramFloat1 * f2;
    float f7 = paramFloat2 * f2;
    f2 = paramFloat3 * f2;
    arrayOfFloat = this.mMat;
    arrayOfFloat[0] = paramFloat1 * paramFloat1 * f4 + f1;
    arrayOfFloat[4] = paramFloat4 * f4 - f2;
    arrayOfFloat[8] = f6 * f4 + f7;
    arrayOfFloat[1] = paramFloat4 * f4 + f2;
    arrayOfFloat[5] = paramFloat2 * paramFloat2 * f4 + f1;
    arrayOfFloat[9] = f5 * f4 - f3;
    arrayOfFloat[2] = f6 * f4 - f7;
    arrayOfFloat[6] = f5 * f4 + f3;
    arrayOfFloat[10] = paramFloat3 * paramFloat3 * f4 + f1;
  }
  
  public void loadScale(float paramFloat1, float paramFloat2, float paramFloat3) {
    loadIdentity();
    float[] arrayOfFloat = this.mMat;
    arrayOfFloat[0] = paramFloat1;
    arrayOfFloat[5] = paramFloat2;
    arrayOfFloat[10] = paramFloat3;
  }
  
  public void loadTranslate(float paramFloat1, float paramFloat2, float paramFloat3) {
    loadIdentity();
    float[] arrayOfFloat = this.mMat;
    arrayOfFloat[12] = paramFloat1;
    arrayOfFloat[13] = paramFloat2;
    arrayOfFloat[14] = paramFloat3;
  }
  
  public void loadMultiply(Matrix4f paramMatrix4f1, Matrix4f paramMatrix4f2) {
    for (byte b = 0; b < 4; b++) {
      float f1 = 0.0F;
      float f2 = 0.0F;
      float f3 = 0.0F;
      float f4 = 0.0F;
      for (byte b1 = 0; b1 < 4; b1++) {
        float f = paramMatrix4f2.get(b, b1);
        f1 += paramMatrix4f1.get(b1, 0) * f;
        f2 += paramMatrix4f1.get(b1, 1) * f;
        f3 += paramMatrix4f1.get(b1, 2) * f;
        f4 += paramMatrix4f1.get(b1, 3) * f;
      } 
      set(b, 0, f1);
      set(b, 1, f2);
      set(b, 2, f3);
      set(b, 3, f4);
    } 
  }
  
  public void loadOrtho(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6) {
    loadIdentity();
    float[] arrayOfFloat = this.mMat;
    arrayOfFloat[0] = 2.0F / (paramFloat2 - paramFloat1);
    arrayOfFloat[5] = 2.0F / (paramFloat4 - paramFloat3);
    arrayOfFloat[10] = -2.0F / (paramFloat6 - paramFloat5);
    arrayOfFloat[12] = -(paramFloat2 + paramFloat1) / (paramFloat2 - paramFloat1);
    arrayOfFloat[13] = -(paramFloat4 + paramFloat3) / (paramFloat4 - paramFloat3);
    arrayOfFloat[14] = -(paramFloat6 + paramFloat5) / (paramFloat6 - paramFloat5);
  }
  
  public void loadOrthoWindow(int paramInt1, int paramInt2) {
    loadOrtho(0.0F, paramInt1, paramInt2, 0.0F, -1.0F, 1.0F);
  }
  
  public void loadFrustum(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6) {
    loadIdentity();
    float[] arrayOfFloat = this.mMat;
    arrayOfFloat[0] = paramFloat5 * 2.0F / (paramFloat2 - paramFloat1);
    arrayOfFloat[5] = 2.0F * paramFloat5 / (paramFloat4 - paramFloat3);
    arrayOfFloat[8] = (paramFloat2 + paramFloat1) / (paramFloat2 - paramFloat1);
    arrayOfFloat[9] = (paramFloat4 + paramFloat3) / (paramFloat4 - paramFloat3);
    arrayOfFloat[10] = -(paramFloat6 + paramFloat5) / (paramFloat6 - paramFloat5);
    arrayOfFloat[11] = -1.0F;
    arrayOfFloat[14] = -2.0F * paramFloat6 * paramFloat5 / (paramFloat6 - paramFloat5);
    arrayOfFloat[15] = 0.0F;
  }
  
  public void loadPerspective(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    paramFloat1 = (float)Math.tan((float)(paramFloat1 * Math.PI / 360.0D)) * paramFloat3;
    float f = -paramFloat1;
    loadFrustum(f * paramFloat2, paramFloat1 * paramFloat2, f, paramFloat1, paramFloat3, paramFloat4);
  }
  
  public void loadProjectionNormalized(int paramInt1, int paramInt2) {
    Matrix4f matrix4f1 = new Matrix4f();
    Matrix4f matrix4f2 = new Matrix4f();
    if (paramInt1 > paramInt2) {
      float f = paramInt1 / paramInt2;
      matrix4f1.loadFrustum(-f, f, -1.0F, 1.0F, 1.0F, 100.0F);
    } else {
      float f = paramInt2 / paramInt1;
      matrix4f1.loadFrustum(-1.0F, 1.0F, -f, f, 1.0F, 100.0F);
    } 
    matrix4f2.loadRotate(180.0F, 0.0F, 1.0F, 0.0F);
    matrix4f1.loadMultiply(matrix4f1, matrix4f2);
    matrix4f2.loadScale(-2.0F, 2.0F, 1.0F);
    matrix4f1.loadMultiply(matrix4f1, matrix4f2);
    matrix4f2.loadTranslate(0.0F, 0.0F, 2.0F);
    matrix4f1.loadMultiply(matrix4f1, matrix4f2);
    load(matrix4f1);
  }
  
  public void multiply(Matrix4f paramMatrix4f) {
    Matrix4f matrix4f = new Matrix4f();
    matrix4f.loadMultiply(this, paramMatrix4f);
    load(matrix4f);
  }
  
  public void rotate(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    Matrix4f matrix4f = new Matrix4f();
    matrix4f.loadRotate(paramFloat1, paramFloat2, paramFloat3, paramFloat4);
    multiply(matrix4f);
  }
  
  public void scale(float paramFloat1, float paramFloat2, float paramFloat3) {
    Matrix4f matrix4f = new Matrix4f();
    matrix4f.loadScale(paramFloat1, paramFloat2, paramFloat3);
    multiply(matrix4f);
  }
  
  public void translate(float paramFloat1, float paramFloat2, float paramFloat3) {
    Matrix4f matrix4f = new Matrix4f();
    matrix4f.loadTranslate(paramFloat1, paramFloat2, paramFloat3);
    multiply(matrix4f);
  }
  
  private float computeCofactor(int paramInt1, int paramInt2) {
    int i = (paramInt1 + 1) % 4;
    int j = (paramInt1 + 2) % 4;
    int k = (paramInt1 + 3) % 4;
    int m = (paramInt2 + 1) % 4;
    int n = (paramInt2 + 2) % 4;
    int i1 = (paramInt2 + 3) % 4;
    float arrayOfFloat[] = this.mMat, f = arrayOfFloat[m * 4 + i] * (arrayOfFloat[n * 4 + j] * arrayOfFloat[i1 * 4 + k] - arrayOfFloat[i1 * 4 + j] * arrayOfFloat[n * 4 + k]) - arrayOfFloat[n * 4 + i] * (arrayOfFloat[m * 4 + j] * arrayOfFloat[i1 * 4 + k] - arrayOfFloat[i1 * 4 + j] * arrayOfFloat[m * 4 + k]) + arrayOfFloat[i1 * 4 + i] * (arrayOfFloat[m * 4 + j] * arrayOfFloat[n * 4 + k] - arrayOfFloat[n * 4 + j] * arrayOfFloat[m * 4 + k]);
    if ((paramInt1 + paramInt2 & 0x1) != 0)
      f = -f; 
    return f;
  }
  
  public boolean inverse() {
    Matrix4f matrix4f = new Matrix4f();
    byte b;
    for (b = 0; b < 4; b++) {
      for (byte b1 = 0; b1 < 4; b1++)
        matrix4f.mMat[b * 4 + b1] = computeCofactor(b, b1); 
    } 
    float arrayOfFloat1[] = this.mMat, f = arrayOfFloat1[0], arrayOfFloat2[] = matrix4f.mMat;
    f = f * arrayOfFloat2[0] + arrayOfFloat1[4] * arrayOfFloat2[1] + arrayOfFloat1[8] * arrayOfFloat2[2] + arrayOfFloat1[12] * arrayOfFloat2[3];
    if (Math.abs(f) < 1.0E-6D)
      return false; 
    f = 1.0F / f;
    for (b = 0; b < 16; b++)
      this.mMat[b] = matrix4f.mMat[b] * f; 
    return true;
  }
  
  public boolean inverseTranspose() {
    Matrix4f matrix4f = new Matrix4f();
    byte b;
    for (b = 0; b < 4; b++) {
      for (byte b1 = 0; b1 < 4; b1++)
        matrix4f.mMat[b1 * 4 + b] = computeCofactor(b, b1); 
    } 
    float arrayOfFloat1[] = this.mMat, f = arrayOfFloat1[0], arrayOfFloat2[] = matrix4f.mMat;
    f = f * arrayOfFloat2[0] + arrayOfFloat1[4] * arrayOfFloat2[4] + arrayOfFloat1[8] * arrayOfFloat2[8] + arrayOfFloat1[12] * arrayOfFloat2[12];
    if (Math.abs(f) < 1.0E-6D)
      return false; 
    f = 1.0F / f;
    for (b = 0; b < 16; b++)
      this.mMat[b] = matrix4f.mMat[b] * f; 
    return true;
  }
  
  public void transpose() {
    for (byte b = 0; b < 3; b++) {
      for (int i = b + 1; i < 4; i++) {
        float arrayOfFloat[] = this.mMat, f = arrayOfFloat[b * 4 + i];
        arrayOfFloat[b * 4 + i] = arrayOfFloat[i * 4 + b];
        arrayOfFloat[i * 4 + b] = f;
      } 
    } 
  }
}
