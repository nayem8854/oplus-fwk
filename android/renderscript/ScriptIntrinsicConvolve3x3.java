package android.renderscript;

public final class ScriptIntrinsicConvolve3x3 extends ScriptIntrinsic {
  private Allocation mInput;
  
  private final float[] mValues = new float[9];
  
  private ScriptIntrinsicConvolve3x3(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
  }
  
  public static ScriptIntrinsicConvolve3x3 create(RenderScript paramRenderScript, Element paramElement) {
    if (paramElement.isCompatible(Element.U8(paramRenderScript)) || 
      paramElement.isCompatible(Element.U8_2(paramRenderScript)) || 
      paramElement.isCompatible(Element.U8_3(paramRenderScript)) || 
      paramElement.isCompatible(Element.U8_4(paramRenderScript)) || 
      paramElement.isCompatible(Element.F32(paramRenderScript)) || 
      paramElement.isCompatible(Element.F32_2(paramRenderScript)) || 
      paramElement.isCompatible(Element.F32_3(paramRenderScript)) || 
      paramElement.isCompatible(Element.F32_4(paramRenderScript))) {
      long l = paramRenderScript.nScriptIntrinsicCreate(1, paramElement.getID(paramRenderScript));
      ScriptIntrinsicConvolve3x3 scriptIntrinsicConvolve3x3 = new ScriptIntrinsicConvolve3x3(l, paramRenderScript);
      scriptIntrinsicConvolve3x3.setCoefficients(new float[] { 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 0.0F, 0.0F });
      return scriptIntrinsicConvolve3x3;
    } 
    throw new RSIllegalArgumentException("Unsupported element type.");
  }
  
  public void setInput(Allocation paramAllocation) {
    this.mInput = paramAllocation;
    setVar(1, paramAllocation);
  }
  
  public void setCoefficients(float[] paramArrayOffloat) {
    FieldPacker fieldPacker = new FieldPacker(36);
    byte b = 0;
    while (true) {
      float[] arrayOfFloat = this.mValues;
      if (b < arrayOfFloat.length) {
        arrayOfFloat[b] = paramArrayOffloat[b];
        fieldPacker.addF32(arrayOfFloat[b]);
        b++;
        continue;
      } 
      break;
    } 
    setVar(0, fieldPacker);
  }
  
  public void forEach(Allocation paramAllocation) {
    forEach(0, (Allocation)null, paramAllocation, (FieldPacker)null);
  }
  
  public void forEach(Allocation paramAllocation, Script.LaunchOptions paramLaunchOptions) {
    forEach(0, (Allocation)null, paramAllocation, (FieldPacker)null, paramLaunchOptions);
  }
  
  public Script.KernelID getKernelID() {
    return createKernelID(0, 2, null, null);
  }
  
  public Script.FieldID getFieldID_Input() {
    return createFieldID(1, null);
  }
}
