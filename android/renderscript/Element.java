package android.renderscript;

public class Element extends BaseObj {
  int[] mArraySizes;
  
  String[] mElementNames;
  
  Element[] mElements;
  
  DataKind mKind;
  
  boolean mNormalized;
  
  int[] mOffsetInBytes;
  
  int mSize;
  
  DataType mType;
  
  int mVectorSize;
  
  int[] mVisibleElementMap;
  
  private void updateVisibleSubElements() {
    if (this.mElements == null)
      return; 
    int i = 0;
    int j = this.mElementNames.length;
    int k;
    for (k = 0; k < j; k++, i = n) {
      int n = i;
      if (this.mElementNames[k].charAt(0) != '#')
        n = i + 1; 
    } 
    this.mVisibleElementMap = new int[i];
    int m;
    for (i = 0, m = 0; i < j; i++, m = k) {
      k = m;
      if (this.mElementNames[i].charAt(0) != '#') {
        this.mVisibleElementMap[m] = i;
        k = m + 1;
      } 
    } 
  }
  
  public int getBytesSize() {
    return this.mSize;
  }
  
  public int getVectorSize() {
    return this.mVectorSize;
  }
  
  class DataType extends Enum<DataType> {
    private static final DataType[] $VALUES;
    
    public static final DataType BOOLEAN;
    
    public static final DataType FLOAT_16 = new DataType("FLOAT_16", 1, 1, 2);
    
    public static final DataType FLOAT_32 = new DataType("FLOAT_32", 2, 2, 4);
    
    public static final DataType FLOAT_64 = new DataType("FLOAT_64", 3, 3, 8);
    
    public static final DataType MATRIX_2X2;
    
    public static final DataType MATRIX_3X3;
    
    public static final DataType MATRIX_4X4;
    
    public static DataType valueOf(String param1String) {
      return Enum.<DataType>valueOf(DataType.class, param1String);
    }
    
    public static DataType[] values() {
      return (DataType[])$VALUES.clone();
    }
    
    public static final DataType NONE = new DataType("NONE", 0, 0, 0);
    
    public static final DataType RS_ALLOCATION;
    
    public static final DataType RS_ELEMENT;
    
    public static final DataType RS_FONT;
    
    public static final DataType RS_MESH;
    
    public static final DataType RS_PROGRAM_FRAGMENT;
    
    public static final DataType RS_PROGRAM_RASTER;
    
    public static final DataType RS_PROGRAM_STORE;
    
    public static final DataType RS_PROGRAM_VERTEX;
    
    public static final DataType RS_SAMPLER;
    
    public static final DataType RS_SCRIPT;
    
    public static final DataType RS_TYPE;
    
    public static final DataType SIGNED_16;
    
    public static final DataType SIGNED_32;
    
    public static final DataType SIGNED_64;
    
    public static final DataType SIGNED_8 = new DataType("SIGNED_8", 4, 4, 1);
    
    public static final DataType UNSIGNED_16;
    
    public static final DataType UNSIGNED_32;
    
    public static final DataType UNSIGNED_4_4_4_4;
    
    public static final DataType UNSIGNED_5_5_5_1;
    
    public static final DataType UNSIGNED_5_6_5;
    
    public static final DataType UNSIGNED_64;
    
    public static final DataType UNSIGNED_8;
    
    int mID;
    
    int mSize;
    
    static {
      SIGNED_16 = new DataType("SIGNED_16", 5, 5, 2);
      SIGNED_32 = new DataType("SIGNED_32", 6, 6, 4);
      SIGNED_64 = new DataType("SIGNED_64", 7, 7, 8);
      UNSIGNED_8 = new DataType("UNSIGNED_8", 8, 8, 1);
      UNSIGNED_16 = new DataType("UNSIGNED_16", 9, 9, 2);
      UNSIGNED_32 = new DataType("UNSIGNED_32", 10, 10, 4);
      UNSIGNED_64 = new DataType("UNSIGNED_64", 11, 11, 8);
      BOOLEAN = new DataType("BOOLEAN", 12, 12, 1);
      UNSIGNED_5_6_5 = new DataType("UNSIGNED_5_6_5", 13, 13, 2);
      UNSIGNED_5_5_5_1 = new DataType("UNSIGNED_5_5_5_1", 14, 14, 2);
      UNSIGNED_4_4_4_4 = new DataType("UNSIGNED_4_4_4_4", 15, 15, 2);
      MATRIX_4X4 = new DataType("MATRIX_4X4", 16, 16, 64);
      MATRIX_3X3 = new DataType("MATRIX_3X3", 17, 17, 36);
      MATRIX_2X2 = new DataType("MATRIX_2X2", 18, 18, 16);
      RS_ELEMENT = new DataType("RS_ELEMENT", 19, 1000);
      RS_TYPE = new DataType("RS_TYPE", 20, 1001);
      RS_ALLOCATION = new DataType("RS_ALLOCATION", 21, 1002);
      RS_SAMPLER = new DataType("RS_SAMPLER", 22, 1003);
      RS_SCRIPT = new DataType("RS_SCRIPT", 23, 1004);
      RS_MESH = new DataType("RS_MESH", 24, 1005);
      RS_PROGRAM_FRAGMENT = new DataType("RS_PROGRAM_FRAGMENT", 25, 1006);
      RS_PROGRAM_VERTEX = new DataType("RS_PROGRAM_VERTEX", 26, 1007);
      RS_PROGRAM_RASTER = new DataType("RS_PROGRAM_RASTER", 27, 1008);
      RS_PROGRAM_STORE = new DataType("RS_PROGRAM_STORE", 28, 1009);
      DataType dataType = new DataType("RS_FONT", 29, 1010);
      $VALUES = new DataType[] { 
          NONE, FLOAT_16, FLOAT_32, FLOAT_64, SIGNED_8, SIGNED_16, SIGNED_32, SIGNED_64, UNSIGNED_8, UNSIGNED_16, 
          UNSIGNED_32, UNSIGNED_64, BOOLEAN, UNSIGNED_5_6_5, UNSIGNED_5_5_5_1, UNSIGNED_4_4_4_4, MATRIX_4X4, MATRIX_3X3, MATRIX_2X2, RS_ELEMENT, 
          RS_TYPE, RS_ALLOCATION, RS_SAMPLER, RS_SCRIPT, RS_MESH, RS_PROGRAM_FRAGMENT, RS_PROGRAM_VERTEX, RS_PROGRAM_RASTER, RS_PROGRAM_STORE, dataType };
    }
    
    private DataType(Element this$0, int param1Int1, int param1Int2, int param1Int3) {
      super((String)this$0, param1Int1);
      this.mID = param1Int2;
      this.mSize = param1Int3;
    }
    
    private DataType(Element this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.mID = param1Int2;
      this.mSize = 4;
      if (RenderScript.sPointerSize == 8)
        this.mSize = 32; 
    }
  }
  
  class DataKind extends Enum<DataKind> {
    private static final DataKind[] $VALUES;
    
    public static final DataKind PIXEL_A;
    
    public static final DataKind PIXEL_DEPTH;
    
    public static final DataKind PIXEL_L = new DataKind("PIXEL_L", 1, 7);
    
    public static final DataKind PIXEL_LA;
    
    public static final DataKind PIXEL_RGB;
    
    public static final DataKind PIXEL_RGBA;
    
    public static final DataKind PIXEL_YUV;
    
    public static DataKind valueOf(String param1String) {
      return Enum.<DataKind>valueOf(DataKind.class, param1String);
    }
    
    public static DataKind[] values() {
      return (DataKind[])$VALUES.clone();
    }
    
    public static final DataKind USER = new DataKind("USER", 0, 0);
    
    int mID;
    
    static {
      PIXEL_A = new DataKind("PIXEL_A", 2, 8);
      PIXEL_LA = new DataKind("PIXEL_LA", 3, 9);
      PIXEL_RGB = new DataKind("PIXEL_RGB", 4, 10);
      PIXEL_RGBA = new DataKind("PIXEL_RGBA", 5, 11);
      PIXEL_DEPTH = new DataKind("PIXEL_DEPTH", 6, 12);
      DataKind dataKind = new DataKind("PIXEL_YUV", 7, 13);
      $VALUES = new DataKind[] { USER, PIXEL_L, PIXEL_A, PIXEL_LA, PIXEL_RGB, PIXEL_RGBA, PIXEL_DEPTH, dataKind };
    }
    
    private DataKind(Element this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.mID = param1Int2;
    }
  }
  
  public boolean isComplex() {
    if (this.mElements == null)
      return false; 
    byte b = 0;
    while (true) {
      Element[] arrayOfElement = this.mElements;
      if (b < arrayOfElement.length) {
        if ((arrayOfElement[b]).mElements != null)
          return true; 
        b++;
        continue;
      } 
      break;
    } 
    return false;
  }
  
  public int getSubElementCount() {
    int[] arrayOfInt = this.mVisibleElementMap;
    if (arrayOfInt == null)
      return 0; 
    return arrayOfInt.length;
  }
  
  public Element getSubElement(int paramInt) {
    int[] arrayOfInt = this.mVisibleElementMap;
    if (arrayOfInt != null) {
      if (paramInt >= 0 && paramInt < arrayOfInt.length)
        return this.mElements[arrayOfInt[paramInt]]; 
      throw new RSIllegalArgumentException("Illegal sub-element index");
    } 
    throw new RSIllegalArgumentException("Element contains no sub-elements");
  }
  
  public String getSubElementName(int paramInt) {
    int[] arrayOfInt = this.mVisibleElementMap;
    if (arrayOfInt != null) {
      if (paramInt >= 0 && paramInt < arrayOfInt.length)
        return this.mElementNames[arrayOfInt[paramInt]]; 
      throw new RSIllegalArgumentException("Illegal sub-element index");
    } 
    throw new RSIllegalArgumentException("Element contains no sub-elements");
  }
  
  public int getSubElementArraySize(int paramInt) {
    int[] arrayOfInt = this.mVisibleElementMap;
    if (arrayOfInt != null) {
      if (paramInt >= 0 && paramInt < arrayOfInt.length)
        return this.mArraySizes[arrayOfInt[paramInt]]; 
      throw new RSIllegalArgumentException("Illegal sub-element index");
    } 
    throw new RSIllegalArgumentException("Element contains no sub-elements");
  }
  
  public int getSubElementOffsetBytes(int paramInt) {
    int[] arrayOfInt = this.mVisibleElementMap;
    if (arrayOfInt != null) {
      if (paramInt >= 0 && paramInt < arrayOfInt.length)
        return this.mOffsetInBytes[arrayOfInt[paramInt]]; 
      throw new RSIllegalArgumentException("Illegal sub-element index");
    } 
    throw new RSIllegalArgumentException("Element contains no sub-elements");
  }
  
  public DataType getDataType() {
    return this.mType;
  }
  
  public DataKind getDataKind() {
    return this.mKind;
  }
  
  public static Element BOOLEAN(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_BOOLEAN : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_BOOLEAN : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.BOOLEAN : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_BOOLEAN : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_BOOLEAN : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #316	-> 0
    //   #317	-> 7
    //   #318	-> 9
    //   #319	-> 16
    //   #321	-> 27
    //   #323	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element U8(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_U8 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_U8 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_8 : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_U8 : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_U8 : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #334	-> 0
    //   #335	-> 7
    //   #336	-> 9
    //   #337	-> 16
    //   #339	-> 27
    //   #341	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element I8(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_I8 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_I8 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.SIGNED_8 : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_I8 : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_I8 : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #352	-> 0
    //   #353	-> 7
    //   #354	-> 9
    //   #355	-> 16
    //   #357	-> 27
    //   #359	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element U16(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_U16 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_U16 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_16 : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_U16 : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_U16 : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #363	-> 0
    //   #364	-> 7
    //   #365	-> 9
    //   #366	-> 16
    //   #368	-> 27
    //   #370	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element I16(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_I16 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_I16 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.SIGNED_16 : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_I16 : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_I16 : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #374	-> 0
    //   #375	-> 7
    //   #376	-> 9
    //   #377	-> 16
    //   #379	-> 27
    //   #381	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element U32(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_U32 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_U32 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_32 : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_U32 : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_U32 : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #385	-> 0
    //   #386	-> 7
    //   #387	-> 9
    //   #388	-> 16
    //   #390	-> 27
    //   #392	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element I32(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_I32 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_I32 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.SIGNED_32 : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_I32 : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_I32 : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #396	-> 0
    //   #397	-> 7
    //   #398	-> 9
    //   #399	-> 16
    //   #401	-> 27
    //   #403	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element U64(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_U64 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_U64 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_64 : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_U64 : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_U64 : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #407	-> 0
    //   #408	-> 7
    //   #409	-> 9
    //   #410	-> 16
    //   #412	-> 27
    //   #414	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element I64(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_I64 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_I64 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.SIGNED_64 : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_I64 : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_I64 : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #418	-> 0
    //   #419	-> 7
    //   #420	-> 9
    //   #421	-> 16
    //   #423	-> 27
    //   #425	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element F16(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_F16 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_F16 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.FLOAT_16 : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_F16 : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_F16 : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #429	-> 0
    //   #430	-> 7
    //   #431	-> 9
    //   #432	-> 16
    //   #434	-> 27
    //   #436	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element F32(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_F32 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_F32 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.FLOAT_32 : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_F32 : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_F32 : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #440	-> 0
    //   #441	-> 7
    //   #442	-> 9
    //   #443	-> 16
    //   #445	-> 27
    //   #447	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element F64(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_F64 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_F64 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.FLOAT_64 : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_F64 : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_F64 : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #451	-> 0
    //   #452	-> 7
    //   #453	-> 9
    //   #454	-> 16
    //   #456	-> 27
    //   #458	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element ELEMENT(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_ELEMENT : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_ELEMENT : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.RS_ELEMENT : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_ELEMENT : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_ELEMENT : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #462	-> 0
    //   #463	-> 7
    //   #464	-> 9
    //   #465	-> 16
    //   #467	-> 27
    //   #469	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element TYPE(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_TYPE : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_TYPE : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.RS_TYPE : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_TYPE : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_TYPE : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #473	-> 0
    //   #474	-> 7
    //   #475	-> 9
    //   #476	-> 16
    //   #478	-> 27
    //   #480	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element ALLOCATION(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_ALLOCATION : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_ALLOCATION : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.RS_ALLOCATION : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_ALLOCATION : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_ALLOCATION : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #484	-> 0
    //   #485	-> 7
    //   #486	-> 9
    //   #487	-> 16
    //   #489	-> 27
    //   #491	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element SAMPLER(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_SAMPLER : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_SAMPLER : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.RS_SAMPLER : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_SAMPLER : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_SAMPLER : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #495	-> 0
    //   #496	-> 7
    //   #497	-> 9
    //   #498	-> 16
    //   #500	-> 27
    //   #502	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element SCRIPT(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_SCRIPT : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_SCRIPT : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.RS_SCRIPT : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_SCRIPT : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_SCRIPT : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #506	-> 0
    //   #507	-> 7
    //   #508	-> 9
    //   #509	-> 16
    //   #511	-> 27
    //   #513	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element MESH(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_MESH : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_MESH : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.RS_MESH : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_MESH : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_MESH : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #517	-> 0
    //   #518	-> 7
    //   #519	-> 9
    //   #520	-> 16
    //   #522	-> 27
    //   #524	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element PROGRAM_FRAGMENT(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_PROGRAM_FRAGMENT : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_PROGRAM_FRAGMENT : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.RS_PROGRAM_FRAGMENT : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_PROGRAM_FRAGMENT : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_PROGRAM_FRAGMENT : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #528	-> 0
    //   #529	-> 7
    //   #530	-> 9
    //   #531	-> 16
    //   #533	-> 27
    //   #535	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element PROGRAM_VERTEX(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_PROGRAM_VERTEX : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_PROGRAM_VERTEX : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.RS_PROGRAM_VERTEX : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_PROGRAM_VERTEX : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_PROGRAM_VERTEX : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #539	-> 0
    //   #540	-> 7
    //   #541	-> 9
    //   #542	-> 16
    //   #544	-> 27
    //   #546	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element PROGRAM_RASTER(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_PROGRAM_RASTER : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_PROGRAM_RASTER : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.RS_PROGRAM_RASTER : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_PROGRAM_RASTER : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_PROGRAM_RASTER : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #550	-> 0
    //   #551	-> 7
    //   #552	-> 9
    //   #553	-> 16
    //   #555	-> 27
    //   #557	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element PROGRAM_STORE(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_PROGRAM_STORE : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_PROGRAM_STORE : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.RS_PROGRAM_STORE : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_PROGRAM_STORE : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_PROGRAM_STORE : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #561	-> 0
    //   #562	-> 7
    //   #563	-> 9
    //   #564	-> 16
    //   #566	-> 27
    //   #568	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element FONT(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_FONT : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_FONT : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.RS_FONT : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_FONT : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_FONT : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #572	-> 0
    //   #573	-> 7
    //   #574	-> 9
    //   #575	-> 16
    //   #577	-> 27
    //   #579	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element A_8(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_A_8 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 40
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_A_8 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 30
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_8 : Landroid/renderscript/Element$DataType;
    //   21: getstatic android/renderscript/Element$DataKind.PIXEL_A : Landroid/renderscript/Element$DataKind;
    //   24: invokestatic createPixel : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;Landroid/renderscript/Element$DataKind;)Landroid/renderscript/Element;
    //   27: putfield mElement_A_8 : Landroid/renderscript/Element;
    //   30: aload_0
    //   31: monitorexit
    //   32: goto -> 40
    //   35: astore_1
    //   36: aload_0
    //   37: monitorexit
    //   38: aload_1
    //   39: athrow
    //   40: aload_0
    //   41: getfield mElement_A_8 : Landroid/renderscript/Element;
    //   44: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #583	-> 0
    //   #584	-> 7
    //   #585	-> 9
    //   #586	-> 16
    //   #588	-> 30
    //   #590	-> 40
    // Exception table:
    //   from	to	target	type
    //   9	16	35	finally
    //   16	30	35	finally
    //   30	32	35	finally
    //   36	38	35	finally
  }
  
  public static Element RGB_565(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_RGB_565 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 40
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_RGB_565 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 30
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_5_6_5 : Landroid/renderscript/Element$DataType;
    //   21: getstatic android/renderscript/Element$DataKind.PIXEL_RGB : Landroid/renderscript/Element$DataKind;
    //   24: invokestatic createPixel : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;Landroid/renderscript/Element$DataKind;)Landroid/renderscript/Element;
    //   27: putfield mElement_RGB_565 : Landroid/renderscript/Element;
    //   30: aload_0
    //   31: monitorexit
    //   32: goto -> 40
    //   35: astore_1
    //   36: aload_0
    //   37: monitorexit
    //   38: aload_1
    //   39: athrow
    //   40: aload_0
    //   41: getfield mElement_RGB_565 : Landroid/renderscript/Element;
    //   44: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #594	-> 0
    //   #595	-> 7
    //   #596	-> 9
    //   #597	-> 16
    //   #599	-> 30
    //   #601	-> 40
    // Exception table:
    //   from	to	target	type
    //   9	16	35	finally
    //   16	30	35	finally
    //   30	32	35	finally
    //   36	38	35	finally
  }
  
  public static Element RGB_888(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_RGB_888 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 40
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_RGB_888 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 30
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_8 : Landroid/renderscript/Element$DataType;
    //   21: getstatic android/renderscript/Element$DataKind.PIXEL_RGB : Landroid/renderscript/Element$DataKind;
    //   24: invokestatic createPixel : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;Landroid/renderscript/Element$DataKind;)Landroid/renderscript/Element;
    //   27: putfield mElement_RGB_888 : Landroid/renderscript/Element;
    //   30: aload_0
    //   31: monitorexit
    //   32: goto -> 40
    //   35: astore_1
    //   36: aload_0
    //   37: monitorexit
    //   38: aload_1
    //   39: athrow
    //   40: aload_0
    //   41: getfield mElement_RGB_888 : Landroid/renderscript/Element;
    //   44: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #605	-> 0
    //   #606	-> 7
    //   #607	-> 9
    //   #608	-> 16
    //   #610	-> 30
    //   #612	-> 40
    // Exception table:
    //   from	to	target	type
    //   9	16	35	finally
    //   16	30	35	finally
    //   30	32	35	finally
    //   36	38	35	finally
  }
  
  public static Element RGBA_5551(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_RGBA_5551 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 40
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_RGBA_5551 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 30
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_5_5_5_1 : Landroid/renderscript/Element$DataType;
    //   21: getstatic android/renderscript/Element$DataKind.PIXEL_RGBA : Landroid/renderscript/Element$DataKind;
    //   24: invokestatic createPixel : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;Landroid/renderscript/Element$DataKind;)Landroid/renderscript/Element;
    //   27: putfield mElement_RGBA_5551 : Landroid/renderscript/Element;
    //   30: aload_0
    //   31: monitorexit
    //   32: goto -> 40
    //   35: astore_1
    //   36: aload_0
    //   37: monitorexit
    //   38: aload_1
    //   39: athrow
    //   40: aload_0
    //   41: getfield mElement_RGBA_5551 : Landroid/renderscript/Element;
    //   44: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #616	-> 0
    //   #617	-> 7
    //   #618	-> 9
    //   #619	-> 16
    //   #621	-> 30
    //   #623	-> 40
    // Exception table:
    //   from	to	target	type
    //   9	16	35	finally
    //   16	30	35	finally
    //   30	32	35	finally
    //   36	38	35	finally
  }
  
  public static Element RGBA_4444(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_RGBA_4444 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 40
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_RGBA_4444 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 30
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_4_4_4_4 : Landroid/renderscript/Element$DataType;
    //   21: getstatic android/renderscript/Element$DataKind.PIXEL_RGBA : Landroid/renderscript/Element$DataKind;
    //   24: invokestatic createPixel : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;Landroid/renderscript/Element$DataKind;)Landroid/renderscript/Element;
    //   27: putfield mElement_RGBA_4444 : Landroid/renderscript/Element;
    //   30: aload_0
    //   31: monitorexit
    //   32: goto -> 40
    //   35: astore_1
    //   36: aload_0
    //   37: monitorexit
    //   38: aload_1
    //   39: athrow
    //   40: aload_0
    //   41: getfield mElement_RGBA_4444 : Landroid/renderscript/Element;
    //   44: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #627	-> 0
    //   #628	-> 7
    //   #629	-> 9
    //   #630	-> 16
    //   #632	-> 30
    //   #634	-> 40
    // Exception table:
    //   from	to	target	type
    //   9	16	35	finally
    //   16	30	35	finally
    //   30	32	35	finally
    //   36	38	35	finally
  }
  
  public static Element RGBA_8888(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_RGBA_8888 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 40
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_RGBA_8888 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 30
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_8 : Landroid/renderscript/Element$DataType;
    //   21: getstatic android/renderscript/Element$DataKind.PIXEL_RGBA : Landroid/renderscript/Element$DataKind;
    //   24: invokestatic createPixel : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;Landroid/renderscript/Element$DataKind;)Landroid/renderscript/Element;
    //   27: putfield mElement_RGBA_8888 : Landroid/renderscript/Element;
    //   30: aload_0
    //   31: monitorexit
    //   32: goto -> 40
    //   35: astore_1
    //   36: aload_0
    //   37: monitorexit
    //   38: aload_1
    //   39: athrow
    //   40: aload_0
    //   41: getfield mElement_RGBA_8888 : Landroid/renderscript/Element;
    //   44: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #638	-> 0
    //   #639	-> 7
    //   #640	-> 9
    //   #641	-> 16
    //   #643	-> 30
    //   #645	-> 40
    // Exception table:
    //   from	to	target	type
    //   9	16	35	finally
    //   16	30	35	finally
    //   30	32	35	finally
    //   36	38	35	finally
  }
  
  public static Element F16_2(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_HALF_2 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_HALF_2 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.FLOAT_16 : Landroid/renderscript/Element$DataType;
    //   21: iconst_2
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_HALF_2 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_HALF_2 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #649	-> 0
    //   #650	-> 7
    //   #651	-> 9
    //   #652	-> 16
    //   #654	-> 28
    //   #656	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element F16_3(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_HALF_3 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_HALF_3 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.FLOAT_16 : Landroid/renderscript/Element$DataType;
    //   21: iconst_3
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_HALF_3 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_HALF_3 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #660	-> 0
    //   #661	-> 7
    //   #662	-> 9
    //   #663	-> 16
    //   #665	-> 28
    //   #667	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element F16_4(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_HALF_4 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_HALF_4 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.FLOAT_16 : Landroid/renderscript/Element$DataType;
    //   21: iconst_4
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_HALF_4 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_HALF_4 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #671	-> 0
    //   #672	-> 7
    //   #673	-> 9
    //   #674	-> 16
    //   #676	-> 28
    //   #678	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element F32_2(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_FLOAT_2 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_FLOAT_2 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.FLOAT_32 : Landroid/renderscript/Element$DataType;
    //   21: iconst_2
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_FLOAT_2 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_FLOAT_2 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #682	-> 0
    //   #683	-> 7
    //   #684	-> 9
    //   #685	-> 16
    //   #687	-> 28
    //   #689	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element F32_3(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_FLOAT_3 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_FLOAT_3 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.FLOAT_32 : Landroid/renderscript/Element$DataType;
    //   21: iconst_3
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_FLOAT_3 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_FLOAT_3 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #693	-> 0
    //   #694	-> 7
    //   #695	-> 9
    //   #696	-> 16
    //   #698	-> 28
    //   #700	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element F32_4(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_FLOAT_4 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_FLOAT_4 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.FLOAT_32 : Landroid/renderscript/Element$DataType;
    //   21: iconst_4
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_FLOAT_4 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_FLOAT_4 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #704	-> 0
    //   #705	-> 7
    //   #706	-> 9
    //   #707	-> 16
    //   #709	-> 28
    //   #711	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element F64_2(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_DOUBLE_2 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_DOUBLE_2 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.FLOAT_64 : Landroid/renderscript/Element$DataType;
    //   21: iconst_2
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_DOUBLE_2 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_DOUBLE_2 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #715	-> 0
    //   #716	-> 7
    //   #717	-> 9
    //   #718	-> 16
    //   #720	-> 28
    //   #722	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element F64_3(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_DOUBLE_3 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_DOUBLE_3 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.FLOAT_64 : Landroid/renderscript/Element$DataType;
    //   21: iconst_3
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_DOUBLE_3 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_DOUBLE_3 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #726	-> 0
    //   #727	-> 7
    //   #728	-> 9
    //   #729	-> 16
    //   #731	-> 28
    //   #733	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element F64_4(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_DOUBLE_4 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_DOUBLE_4 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.FLOAT_64 : Landroid/renderscript/Element$DataType;
    //   21: iconst_4
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_DOUBLE_4 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_DOUBLE_4 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #737	-> 0
    //   #738	-> 7
    //   #739	-> 9
    //   #740	-> 16
    //   #742	-> 28
    //   #744	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element U8_2(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_UCHAR_2 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_UCHAR_2 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_8 : Landroid/renderscript/Element$DataType;
    //   21: iconst_2
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_UCHAR_2 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_UCHAR_2 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #748	-> 0
    //   #749	-> 7
    //   #750	-> 9
    //   #751	-> 16
    //   #753	-> 28
    //   #755	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element U8_3(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_UCHAR_3 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_UCHAR_3 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_8 : Landroid/renderscript/Element$DataType;
    //   21: iconst_3
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_UCHAR_3 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_UCHAR_3 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #759	-> 0
    //   #760	-> 7
    //   #761	-> 9
    //   #762	-> 16
    //   #764	-> 28
    //   #766	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element U8_4(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_UCHAR_4 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_UCHAR_4 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_8 : Landroid/renderscript/Element$DataType;
    //   21: iconst_4
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_UCHAR_4 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_UCHAR_4 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #770	-> 0
    //   #771	-> 7
    //   #772	-> 9
    //   #773	-> 16
    //   #775	-> 28
    //   #777	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element I8_2(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_CHAR_2 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_CHAR_2 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.SIGNED_8 : Landroid/renderscript/Element$DataType;
    //   21: iconst_2
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_CHAR_2 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_CHAR_2 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #781	-> 0
    //   #782	-> 7
    //   #783	-> 9
    //   #784	-> 16
    //   #786	-> 28
    //   #788	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element I8_3(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_CHAR_3 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_CHAR_3 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.SIGNED_8 : Landroid/renderscript/Element$DataType;
    //   21: iconst_3
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_CHAR_3 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_CHAR_3 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #792	-> 0
    //   #793	-> 7
    //   #794	-> 9
    //   #795	-> 16
    //   #797	-> 28
    //   #799	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element I8_4(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_CHAR_4 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_CHAR_4 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.SIGNED_8 : Landroid/renderscript/Element$DataType;
    //   21: iconst_4
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_CHAR_4 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_CHAR_4 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #803	-> 0
    //   #804	-> 7
    //   #805	-> 9
    //   #806	-> 16
    //   #808	-> 28
    //   #810	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element U16_2(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_USHORT_2 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_USHORT_2 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_16 : Landroid/renderscript/Element$DataType;
    //   21: iconst_2
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_USHORT_2 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_USHORT_2 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #814	-> 0
    //   #815	-> 7
    //   #816	-> 9
    //   #817	-> 16
    //   #819	-> 28
    //   #821	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element U16_3(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_USHORT_3 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_USHORT_3 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_16 : Landroid/renderscript/Element$DataType;
    //   21: iconst_3
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_USHORT_3 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_USHORT_3 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #825	-> 0
    //   #826	-> 7
    //   #827	-> 9
    //   #828	-> 16
    //   #830	-> 28
    //   #832	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element U16_4(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_USHORT_4 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_USHORT_4 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_16 : Landroid/renderscript/Element$DataType;
    //   21: iconst_4
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_USHORT_4 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_USHORT_4 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #836	-> 0
    //   #837	-> 7
    //   #838	-> 9
    //   #839	-> 16
    //   #841	-> 28
    //   #843	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element I16_2(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_SHORT_2 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_SHORT_2 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.SIGNED_16 : Landroid/renderscript/Element$DataType;
    //   21: iconst_2
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_SHORT_2 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_SHORT_2 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #847	-> 0
    //   #848	-> 7
    //   #849	-> 9
    //   #850	-> 16
    //   #852	-> 28
    //   #854	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element I16_3(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_SHORT_3 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_SHORT_3 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.SIGNED_16 : Landroid/renderscript/Element$DataType;
    //   21: iconst_3
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_SHORT_3 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_SHORT_3 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #858	-> 0
    //   #859	-> 7
    //   #860	-> 9
    //   #861	-> 16
    //   #863	-> 28
    //   #865	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element I16_4(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_SHORT_4 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_SHORT_4 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.SIGNED_16 : Landroid/renderscript/Element$DataType;
    //   21: iconst_4
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_SHORT_4 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_SHORT_4 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #869	-> 0
    //   #870	-> 7
    //   #871	-> 9
    //   #872	-> 16
    //   #874	-> 28
    //   #876	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element U32_2(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_UINT_2 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_UINT_2 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_32 : Landroid/renderscript/Element$DataType;
    //   21: iconst_2
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_UINT_2 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_UINT_2 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #880	-> 0
    //   #881	-> 7
    //   #882	-> 9
    //   #883	-> 16
    //   #885	-> 28
    //   #887	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element U32_3(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_UINT_3 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_UINT_3 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_32 : Landroid/renderscript/Element$DataType;
    //   21: iconst_3
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_UINT_3 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_UINT_3 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #891	-> 0
    //   #892	-> 7
    //   #893	-> 9
    //   #894	-> 16
    //   #896	-> 28
    //   #898	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element U32_4(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_UINT_4 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_UINT_4 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_32 : Landroid/renderscript/Element$DataType;
    //   21: iconst_4
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_UINT_4 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_UINT_4 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #902	-> 0
    //   #903	-> 7
    //   #904	-> 9
    //   #905	-> 16
    //   #907	-> 28
    //   #909	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element I32_2(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_INT_2 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_INT_2 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.SIGNED_32 : Landroid/renderscript/Element$DataType;
    //   21: iconst_2
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_INT_2 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_INT_2 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #913	-> 0
    //   #914	-> 7
    //   #915	-> 9
    //   #916	-> 16
    //   #918	-> 28
    //   #920	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element I32_3(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_INT_3 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_INT_3 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.SIGNED_32 : Landroid/renderscript/Element$DataType;
    //   21: iconst_3
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_INT_3 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_INT_3 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #924	-> 0
    //   #925	-> 7
    //   #926	-> 9
    //   #927	-> 16
    //   #929	-> 28
    //   #931	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element I32_4(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_INT_4 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_INT_4 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.SIGNED_32 : Landroid/renderscript/Element$DataType;
    //   21: iconst_4
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_INT_4 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_INT_4 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #935	-> 0
    //   #936	-> 7
    //   #937	-> 9
    //   #938	-> 16
    //   #940	-> 28
    //   #942	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element U64_2(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_ULONG_2 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_ULONG_2 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_64 : Landroid/renderscript/Element$DataType;
    //   21: iconst_2
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_ULONG_2 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_ULONG_2 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #946	-> 0
    //   #947	-> 7
    //   #948	-> 9
    //   #949	-> 16
    //   #951	-> 28
    //   #953	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element U64_3(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_ULONG_3 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_ULONG_3 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_64 : Landroid/renderscript/Element$DataType;
    //   21: iconst_3
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_ULONG_3 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_ULONG_3 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #957	-> 0
    //   #958	-> 7
    //   #959	-> 9
    //   #960	-> 16
    //   #962	-> 28
    //   #964	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element U64_4(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_ULONG_4 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_ULONG_4 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_64 : Landroid/renderscript/Element$DataType;
    //   21: iconst_4
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_ULONG_4 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_ULONG_4 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #968	-> 0
    //   #969	-> 7
    //   #970	-> 9
    //   #971	-> 16
    //   #973	-> 28
    //   #975	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element I64_2(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_LONG_2 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_LONG_2 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.SIGNED_64 : Landroid/renderscript/Element$DataType;
    //   21: iconst_2
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_LONG_2 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_LONG_2 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #979	-> 0
    //   #980	-> 7
    //   #981	-> 9
    //   #982	-> 16
    //   #984	-> 28
    //   #986	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element I64_3(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_LONG_3 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_LONG_3 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.SIGNED_64 : Landroid/renderscript/Element$DataType;
    //   21: iconst_3
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_LONG_3 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_LONG_3 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #990	-> 0
    //   #991	-> 7
    //   #992	-> 9
    //   #993	-> 16
    //   #995	-> 28
    //   #997	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element I64_4(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_LONG_4 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 38
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_LONG_4 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.SIGNED_64 : Landroid/renderscript/Element$DataType;
    //   21: iconst_4
    //   22: invokestatic createVector : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    //   25: putfield mElement_LONG_4 : Landroid/renderscript/Element;
    //   28: aload_0
    //   29: monitorexit
    //   30: goto -> 38
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: athrow
    //   38: aload_0
    //   39: getfield mElement_LONG_4 : Landroid/renderscript/Element;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1001	-> 0
    //   #1002	-> 7
    //   #1003	-> 9
    //   #1004	-> 16
    //   #1006	-> 28
    //   #1008	-> 38
    // Exception table:
    //   from	to	target	type
    //   9	16	33	finally
    //   16	28	33	finally
    //   28	30	33	finally
    //   34	36	33	finally
  }
  
  public static Element YUV(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_YUV : Landroid/renderscript/Element;
    //   4: ifnonnull -> 40
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_YUV : Landroid/renderscript/Element;
    //   13: ifnonnull -> 30
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.UNSIGNED_8 : Landroid/renderscript/Element$DataType;
    //   21: getstatic android/renderscript/Element$DataKind.PIXEL_YUV : Landroid/renderscript/Element$DataKind;
    //   24: invokestatic createPixel : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;Landroid/renderscript/Element$DataKind;)Landroid/renderscript/Element;
    //   27: putfield mElement_YUV : Landroid/renderscript/Element;
    //   30: aload_0
    //   31: monitorexit
    //   32: goto -> 40
    //   35: astore_1
    //   36: aload_0
    //   37: monitorexit
    //   38: aload_1
    //   39: athrow
    //   40: aload_0
    //   41: getfield mElement_YUV : Landroid/renderscript/Element;
    //   44: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1012	-> 0
    //   #1013	-> 7
    //   #1014	-> 9
    //   #1015	-> 16
    //   #1017	-> 30
    //   #1019	-> 40
    // Exception table:
    //   from	to	target	type
    //   9	16	35	finally
    //   16	30	35	finally
    //   30	32	35	finally
    //   36	38	35	finally
  }
  
  public static Element MATRIX_4X4(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_MATRIX_4X4 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_MATRIX_4X4 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.MATRIX_4X4 : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_MATRIX_4X4 : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_MATRIX_4X4 : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1023	-> 0
    //   #1024	-> 7
    //   #1025	-> 9
    //   #1026	-> 16
    //   #1028	-> 27
    //   #1030	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element MATRIX4X4(RenderScript paramRenderScript) {
    return MATRIX_4X4(paramRenderScript);
  }
  
  public static Element MATRIX_3X3(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_MATRIX_3X3 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_MATRIX_3X3 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.MATRIX_3X3 : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_MATRIX_3X3 : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_MATRIX_3X3 : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1040	-> 0
    //   #1041	-> 7
    //   #1042	-> 9
    //   #1043	-> 16
    //   #1045	-> 27
    //   #1047	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  public static Element MATRIX_2X2(RenderScript paramRenderScript) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mElement_MATRIX_2X2 : Landroid/renderscript/Element;
    //   4: ifnonnull -> 37
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mElement_MATRIX_2X2 : Landroid/renderscript/Element;
    //   13: ifnonnull -> 27
    //   16: aload_0
    //   17: aload_0
    //   18: getstatic android/renderscript/Element$DataType.MATRIX_2X2 : Landroid/renderscript/Element$DataType;
    //   21: invokestatic createUser : (Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    //   24: putfield mElement_MATRIX_2X2 : Landroid/renderscript/Element;
    //   27: aload_0
    //   28: monitorexit
    //   29: goto -> 37
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    //   37: aload_0
    //   38: getfield mElement_MATRIX_2X2 : Landroid/renderscript/Element;
    //   41: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1051	-> 0
    //   #1052	-> 7
    //   #1053	-> 9
    //   #1054	-> 16
    //   #1056	-> 27
    //   #1058	-> 37
    // Exception table:
    //   from	to	target	type
    //   9	16	32	finally
    //   16	27	32	finally
    //   27	29	32	finally
    //   33	35	32	finally
  }
  
  Element(long paramLong, RenderScript paramRenderScript, Element[] paramArrayOfElement, String[] paramArrayOfString, int[] paramArrayOfint) {
    super(paramLong, paramRenderScript);
    this.mSize = 0;
    this.mVectorSize = 1;
    this.mElements = paramArrayOfElement;
    this.mElementNames = paramArrayOfString;
    this.mArraySizes = paramArrayOfint;
    this.mType = DataType.NONE;
    this.mKind = DataKind.USER;
    this.mOffsetInBytes = new int[this.mElements.length];
    byte b = 0;
    while (true) {
      paramArrayOfElement = this.mElements;
      if (b < paramArrayOfElement.length) {
        int arrayOfInt[] = this.mOffsetInBytes, i = this.mSize;
        arrayOfInt[b] = i;
        this.mSize = i + (paramArrayOfElement[b]).mSize * this.mArraySizes[b];
        b++;
        continue;
      } 
      break;
    } 
    updateVisibleSubElements();
  }
  
  Element(long paramLong, RenderScript paramRenderScript, DataType paramDataType, DataKind paramDataKind, boolean paramBoolean, int paramInt) {
    super(paramLong, paramRenderScript);
    if (paramDataType != DataType.UNSIGNED_5_6_5 && paramDataType != DataType.UNSIGNED_4_4_4_4 && paramDataType != DataType.UNSIGNED_5_5_5_1) {
      if (paramInt == 3) {
        this.mSize = paramDataType.mSize * 4;
      } else {
        this.mSize = paramDataType.mSize * paramInt;
      } 
    } else {
      this.mSize = paramDataType.mSize;
    } 
    this.mType = paramDataType;
    this.mKind = paramDataKind;
    this.mNormalized = paramBoolean;
    this.mVectorSize = paramInt;
  }
  
  Element(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
  }
  
  void updateFromNative() {
    boolean bool;
    super.updateFromNative();
    int[] arrayOfInt = new int[5];
    this.mRS.nElementGetNativeData(getID(this.mRS), arrayOfInt);
    null = arrayOfInt[2];
    int j = 0;
    if (null == 1) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mNormalized = bool;
    this.mVectorSize = arrayOfInt[3];
    this.mSize = 0;
    for (DataType dataType : DataType.values()) {
      if (dataType.mID == arrayOfInt[0]) {
        this.mType = dataType;
        this.mSize = dataType.mSize * this.mVectorSize;
      } 
    } 
    int i, k;
    for (DataKind[] arrayOfDataKind = DataKind.values(); i < k; ) {
      DataKind dataKind = arrayOfDataKind[i];
      if (dataKind.mID == arrayOfInt[1])
        this.mKind = dataKind; 
      i++;
    } 
    j = arrayOfInt[4];
    if (j > 0) {
      this.mElements = new Element[j];
      this.mElementNames = new String[j];
      this.mArraySizes = new int[j];
      this.mOffsetInBytes = new int[j];
      long[] arrayOfLong = new long[j];
      this.mRS.nElementGetSubElements(getID(this.mRS), arrayOfLong, this.mElementNames, this.mArraySizes);
      for (i = 0; i < j; i++) {
        this.mElements[i] = new Element(arrayOfLong[i], this.mRS);
        this.mElements[i].updateFromNative();
        arrayOfInt = this.mOffsetInBytes;
        k = this.mSize;
        arrayOfInt[i] = k;
        this.mSize = k + (this.mElements[i]).mSize * this.mArraySizes[i];
      } 
    } 
    updateVisibleSubElements();
  }
  
  static Element createUser(RenderScript paramRenderScript, DataType paramDataType) {
    DataKind dataKind = DataKind.USER;
    long l = paramRenderScript.nElementCreate(paramDataType.mID, dataKind.mID, false, 1);
    return new Element(l, paramRenderScript, paramDataType, dataKind, false, 1);
  }
  
  public static Element createVector(RenderScript paramRenderScript, DataType paramDataType, int paramInt) {
    if (paramInt >= 2 && paramInt <= 4) {
      switch (null.$SwitchMap$android$renderscript$Element$DataType[paramDataType.ordinal()]) {
        default:
          throw new RSIllegalArgumentException("Cannot create vector of non-primitive type.");
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
          break;
      } 
      DataKind dataKind = DataKind.USER;
      long l = paramRenderScript.nElementCreate(paramDataType.mID, dataKind.mID, false, paramInt);
      return new Element(l, paramRenderScript, paramDataType, dataKind, false, paramInt);
    } 
    throw new RSIllegalArgumentException("Vector size out of range 2-4.");
  }
  
  public static Element createPixel(RenderScript paramRenderScript, DataType paramDataType, DataKind paramDataKind) {
    if (paramDataKind == DataKind.PIXEL_L || paramDataKind == DataKind.PIXEL_A || paramDataKind == DataKind.PIXEL_LA || paramDataKind == DataKind.PIXEL_RGB || paramDataKind == DataKind.PIXEL_RGBA || paramDataKind == DataKind.PIXEL_DEPTH || paramDataKind == DataKind.PIXEL_YUV) {
      if (paramDataType == DataType.UNSIGNED_8 || paramDataType == DataType.UNSIGNED_16 || paramDataType == DataType.UNSIGNED_5_6_5 || paramDataType == DataType.UNSIGNED_4_4_4_4 || paramDataType == DataType.UNSIGNED_5_5_5_1) {
        if (paramDataType != DataType.UNSIGNED_5_6_5 || paramDataKind == DataKind.PIXEL_RGB) {
          if (paramDataType != DataType.UNSIGNED_5_5_5_1 || paramDataKind == DataKind.PIXEL_RGBA) {
            if (paramDataType != DataType.UNSIGNED_4_4_4_4 || paramDataKind == DataKind.PIXEL_RGBA) {
              if (paramDataType != DataType.UNSIGNED_16 || paramDataKind == DataKind.PIXEL_DEPTH) {
                byte b = 1;
                int i = null.$SwitchMap$android$renderscript$Element$DataKind[paramDataKind.ordinal()];
                if (i != 1) {
                  if (i != 2) {
                    if (i != 3) {
                      if (i == 4)
                        b = 2; 
                    } else {
                      b = 4;
                    } 
                  } else {
                    b = 3;
                  } 
                } else {
                  b = 2;
                } 
                long l = paramRenderScript.nElementCreate(paramDataType.mID, paramDataKind.mID, true, b);
                return new Element(l, paramRenderScript, paramDataType, paramDataKind, true, b);
              } 
              throw new RSIllegalArgumentException("Bad kind and type combo");
            } 
            throw new RSIllegalArgumentException("Bad kind and type combo");
          } 
          throw new RSIllegalArgumentException("Bad kind and type combo");
        } 
        throw new RSIllegalArgumentException("Bad kind and type combo");
      } 
      throw new RSIllegalArgumentException("Unsupported DataType");
    } 
    throw new RSIllegalArgumentException("Unsupported DataKind");
  }
  
  public boolean isCompatible(Element paramElement) {
    boolean bool = equals(paramElement);
    boolean bool1 = true;
    if (bool)
      return true; 
    if (this.mSize != paramElement.mSize || this.mType == DataType.NONE || this.mType != paramElement.mType || this.mVectorSize != paramElement.mVectorSize)
      bool1 = false; 
    return bool1;
  }
  
  class Builder {
    int[] mArraySizes;
    
    int mCount;
    
    String[] mElementNames;
    
    Element[] mElements;
    
    RenderScript mRS;
    
    int mSkipPadding;
    
    public Builder(Element this$0) {
      this.mRS = (RenderScript)this$0;
      this.mCount = 0;
      this.mElements = new Element[8];
      this.mElementNames = new String[8];
      this.mArraySizes = new int[8];
    }
    
    public Builder add(Element param1Element, String param1String, int param1Int) {
      if (param1Int >= 1) {
        if (this.mSkipPadding != 0 && 
          param1String.startsWith("#padding_")) {
          this.mSkipPadding = 0;
          return this;
        } 
        if (param1Element.mVectorSize == 3) {
          this.mSkipPadding = 1;
        } else {
          this.mSkipPadding = 0;
        } 
        int i = this.mCount;
        Element[] arrayOfElement = this.mElements;
        if (i == arrayOfElement.length) {
          Element[] arrayOfElement1 = new Element[i + 8];
          String[] arrayOfString = new String[i + 8];
          int[] arrayOfInt = new int[i + 8];
          System.arraycopy(arrayOfElement, 0, arrayOfElement1, 0, i);
          System.arraycopy(this.mElementNames, 0, arrayOfString, 0, this.mCount);
          System.arraycopy(this.mArraySizes, 0, arrayOfInt, 0, this.mCount);
          this.mElements = arrayOfElement1;
          this.mElementNames = arrayOfString;
          this.mArraySizes = arrayOfInt;
        } 
        arrayOfElement = this.mElements;
        i = this.mCount;
        arrayOfElement[i] = param1Element;
        this.mElementNames[i] = param1String;
        this.mArraySizes[i] = param1Int;
        this.mCount = i + 1;
        return this;
      } 
      throw new RSIllegalArgumentException("Array size cannot be less than 1.");
    }
    
    public Builder add(Element param1Element, String param1String) {
      return add(param1Element, param1String, 1);
    }
    
    public Element create() {
      this.mRS.validate();
      int i = this.mCount;
      Element[] arrayOfElement = new Element[i];
      String[] arrayOfString = new String[i];
      int[] arrayOfInt = new int[i];
      System.arraycopy(this.mElements, 0, arrayOfElement, 0, i);
      System.arraycopy(this.mElementNames, 0, arrayOfString, 0, this.mCount);
      System.arraycopy(this.mArraySizes, 0, arrayOfInt, 0, this.mCount);
      long[] arrayOfLong = new long[arrayOfElement.length];
      for (i = 0; i < arrayOfElement.length; i++)
        arrayOfLong[i] = arrayOfElement[i].getID(this.mRS); 
      long l = this.mRS.nElementCreate2(arrayOfLong, arrayOfString, arrayOfInt);
      return new Element(l, this.mRS, arrayOfElement, arrayOfString, arrayOfInt);
    }
  }
}
