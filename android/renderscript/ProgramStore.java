package android.renderscript;

public class ProgramStore extends BaseObj {
  BlendDstFunc mBlendDst;
  
  BlendSrcFunc mBlendSrc;
  
  boolean mColorMaskA;
  
  boolean mColorMaskB;
  
  boolean mColorMaskG;
  
  boolean mColorMaskR;
  
  DepthFunc mDepthFunc;
  
  boolean mDepthMask;
  
  boolean mDither;
  
  class DepthFunc extends Enum<DepthFunc> {
    private static final DepthFunc[] $VALUES;
    
    public static DepthFunc valueOf(String param1String) {
      return Enum.<DepthFunc>valueOf(DepthFunc.class, param1String);
    }
    
    public static DepthFunc[] values() {
      return (DepthFunc[])$VALUES.clone();
    }
    
    public static final DepthFunc ALWAYS = new DepthFunc("ALWAYS", 0, 0);
    
    public static final DepthFunc EQUAL;
    
    public static final DepthFunc GREATER = new DepthFunc("GREATER", 3, 3);
    
    public static final DepthFunc GREATER_OR_EQUAL = new DepthFunc("GREATER_OR_EQUAL", 4, 4);
    
    public static final DepthFunc LESS = new DepthFunc("LESS", 1, 1);
    
    public static final DepthFunc LESS_OR_EQUAL = new DepthFunc("LESS_OR_EQUAL", 2, 2);
    
    public static final DepthFunc NOT_EQUAL;
    
    int mID;
    
    static {
      EQUAL = new DepthFunc("EQUAL", 5, 5);
      DepthFunc depthFunc = new DepthFunc("NOT_EQUAL", 6, 6);
      $VALUES = new DepthFunc[] { ALWAYS, LESS, LESS_OR_EQUAL, GREATER, GREATER_OR_EQUAL, EQUAL, depthFunc };
    }
    
    private DepthFunc(ProgramStore this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.mID = param1Int2;
    }
  }
  
  class BlendSrcFunc extends Enum<BlendSrcFunc> {
    private static final BlendSrcFunc[] $VALUES;
    
    public static final BlendSrcFunc DST_ALPHA;
    
    public static final BlendSrcFunc DST_COLOR;
    
    public static final BlendSrcFunc ONE = new BlendSrcFunc("ONE", 1, 1);
    
    public static final BlendSrcFunc ONE_MINUS_DST_ALPHA;
    
    public static final BlendSrcFunc ONE_MINUS_DST_COLOR;
    
    public static final BlendSrcFunc ONE_MINUS_SRC_ALPHA;
    
    public static final BlendSrcFunc SRC_ALPHA;
    
    public static final BlendSrcFunc SRC_ALPHA_SATURATE;
    
    public static BlendSrcFunc valueOf(String param1String) {
      return Enum.<BlendSrcFunc>valueOf(BlendSrcFunc.class, param1String);
    }
    
    public static BlendSrcFunc[] values() {
      return (BlendSrcFunc[])$VALUES.clone();
    }
    
    public static final BlendSrcFunc ZERO = new BlendSrcFunc("ZERO", 0, 0);
    
    int mID;
    
    static {
      DST_COLOR = new BlendSrcFunc("DST_COLOR", 2, 2);
      ONE_MINUS_DST_COLOR = new BlendSrcFunc("ONE_MINUS_DST_COLOR", 3, 3);
      SRC_ALPHA = new BlendSrcFunc("SRC_ALPHA", 4, 4);
      ONE_MINUS_SRC_ALPHA = new BlendSrcFunc("ONE_MINUS_SRC_ALPHA", 5, 5);
      DST_ALPHA = new BlendSrcFunc("DST_ALPHA", 6, 6);
      ONE_MINUS_DST_ALPHA = new BlendSrcFunc("ONE_MINUS_DST_ALPHA", 7, 7);
      BlendSrcFunc blendSrcFunc = new BlendSrcFunc("SRC_ALPHA_SATURATE", 8, 8);
      $VALUES = new BlendSrcFunc[] { ZERO, ONE, DST_COLOR, ONE_MINUS_DST_COLOR, SRC_ALPHA, ONE_MINUS_SRC_ALPHA, DST_ALPHA, ONE_MINUS_DST_ALPHA, blendSrcFunc };
    }
    
    private BlendSrcFunc(ProgramStore this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.mID = param1Int2;
    }
  }
  
  class BlendDstFunc extends Enum<BlendDstFunc> {
    private static final BlendDstFunc[] $VALUES;
    
    public static final BlendDstFunc DST_ALPHA;
    
    public static final BlendDstFunc ONE = new BlendDstFunc("ONE", 1, 1);
    
    public static final BlendDstFunc ONE_MINUS_DST_ALPHA;
    
    public static final BlendDstFunc ONE_MINUS_SRC_ALPHA;
    
    public static final BlendDstFunc ONE_MINUS_SRC_COLOR;
    
    public static final BlendDstFunc SRC_ALPHA;
    
    public static final BlendDstFunc SRC_COLOR = new BlendDstFunc("SRC_COLOR", 2, 2);
    
    public static BlendDstFunc valueOf(String param1String) {
      return Enum.<BlendDstFunc>valueOf(BlendDstFunc.class, param1String);
    }
    
    public static BlendDstFunc[] values() {
      return (BlendDstFunc[])$VALUES.clone();
    }
    
    public static final BlendDstFunc ZERO = new BlendDstFunc("ZERO", 0, 0);
    
    int mID;
    
    static {
      ONE_MINUS_SRC_COLOR = new BlendDstFunc("ONE_MINUS_SRC_COLOR", 3, 3);
      SRC_ALPHA = new BlendDstFunc("SRC_ALPHA", 4, 4);
      ONE_MINUS_SRC_ALPHA = new BlendDstFunc("ONE_MINUS_SRC_ALPHA", 5, 5);
      DST_ALPHA = new BlendDstFunc("DST_ALPHA", 6, 6);
      BlendDstFunc blendDstFunc = new BlendDstFunc("ONE_MINUS_DST_ALPHA", 7, 7);
      $VALUES = new BlendDstFunc[] { ZERO, ONE, SRC_COLOR, ONE_MINUS_SRC_COLOR, SRC_ALPHA, ONE_MINUS_SRC_ALPHA, DST_ALPHA, blendDstFunc };
    }
    
    private BlendDstFunc(ProgramStore this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.mID = param1Int2;
    }
  }
  
  ProgramStore(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
  }
  
  public DepthFunc getDepthFunc() {
    return this.mDepthFunc;
  }
  
  public boolean isDepthMaskEnabled() {
    return this.mDepthMask;
  }
  
  public boolean isColorMaskRedEnabled() {
    return this.mColorMaskR;
  }
  
  public boolean isColorMaskGreenEnabled() {
    return this.mColorMaskG;
  }
  
  public boolean isColorMaskBlueEnabled() {
    return this.mColorMaskB;
  }
  
  public boolean isColorMaskAlphaEnabled() {
    return this.mColorMaskA;
  }
  
  public BlendSrcFunc getBlendSrcFunc() {
    return this.mBlendSrc;
  }
  
  public BlendDstFunc getBlendDstFunc() {
    return this.mBlendDst;
  }
  
  public boolean isDitherEnabled() {
    return this.mDither;
  }
  
  public static ProgramStore BLEND_NONE_DEPTH_TEST(RenderScript paramRenderScript) {
    if (paramRenderScript.mProgramStore_BLEND_NONE_DEPTH_TEST == null) {
      Builder builder = new Builder(paramRenderScript);
      builder.setDepthFunc(DepthFunc.LESS);
      builder.setBlendFunc(BlendSrcFunc.ONE, BlendDstFunc.ZERO);
      builder.setDitherEnabled(false);
      builder.setDepthMaskEnabled(true);
      paramRenderScript.mProgramStore_BLEND_NONE_DEPTH_TEST = builder.create();
    } 
    return paramRenderScript.mProgramStore_BLEND_NONE_DEPTH_TEST;
  }
  
  public static ProgramStore BLEND_NONE_DEPTH_NONE(RenderScript paramRenderScript) {
    if (paramRenderScript.mProgramStore_BLEND_NONE_DEPTH_NO_DEPTH == null) {
      Builder builder = new Builder(paramRenderScript);
      builder.setDepthFunc(DepthFunc.ALWAYS);
      builder.setBlendFunc(BlendSrcFunc.ONE, BlendDstFunc.ZERO);
      builder.setDitherEnabled(false);
      builder.setDepthMaskEnabled(false);
      paramRenderScript.mProgramStore_BLEND_NONE_DEPTH_NO_DEPTH = builder.create();
    } 
    return paramRenderScript.mProgramStore_BLEND_NONE_DEPTH_NO_DEPTH;
  }
  
  public static ProgramStore BLEND_ALPHA_DEPTH_TEST(RenderScript paramRenderScript) {
    if (paramRenderScript.mProgramStore_BLEND_ALPHA_DEPTH_TEST == null) {
      Builder builder = new Builder(paramRenderScript);
      builder.setDepthFunc(DepthFunc.LESS);
      builder.setBlendFunc(BlendSrcFunc.SRC_ALPHA, BlendDstFunc.ONE_MINUS_SRC_ALPHA);
      builder.setDitherEnabled(false);
      builder.setDepthMaskEnabled(true);
      paramRenderScript.mProgramStore_BLEND_ALPHA_DEPTH_TEST = builder.create();
    } 
    return paramRenderScript.mProgramStore_BLEND_ALPHA_DEPTH_TEST;
  }
  
  public static ProgramStore BLEND_ALPHA_DEPTH_NONE(RenderScript paramRenderScript) {
    if (paramRenderScript.mProgramStore_BLEND_ALPHA_DEPTH_NO_DEPTH == null) {
      Builder builder = new Builder(paramRenderScript);
      builder.setDepthFunc(DepthFunc.ALWAYS);
      builder.setBlendFunc(BlendSrcFunc.SRC_ALPHA, BlendDstFunc.ONE_MINUS_SRC_ALPHA);
      builder.setDitherEnabled(false);
      builder.setDepthMaskEnabled(false);
      paramRenderScript.mProgramStore_BLEND_ALPHA_DEPTH_NO_DEPTH = builder.create();
    } 
    return paramRenderScript.mProgramStore_BLEND_ALPHA_DEPTH_NO_DEPTH;
  }
  
  class Builder {
    ProgramStore.BlendDstFunc mBlendDst;
    
    ProgramStore.BlendSrcFunc mBlendSrc;
    
    boolean mColorMaskA;
    
    boolean mColorMaskB;
    
    boolean mColorMaskG;
    
    boolean mColorMaskR;
    
    ProgramStore.DepthFunc mDepthFunc;
    
    boolean mDepthMask;
    
    boolean mDither;
    
    RenderScript mRS;
    
    public Builder(ProgramStore this$0) {
      this.mRS = (RenderScript)this$0;
      this.mDepthFunc = ProgramStore.DepthFunc.ALWAYS;
      this.mDepthMask = false;
      this.mColorMaskR = true;
      this.mColorMaskG = true;
      this.mColorMaskB = true;
      this.mColorMaskA = true;
      this.mBlendSrc = ProgramStore.BlendSrcFunc.ONE;
      this.mBlendDst = ProgramStore.BlendDstFunc.ZERO;
    }
    
    public Builder setDepthFunc(ProgramStore.DepthFunc param1DepthFunc) {
      this.mDepthFunc = param1DepthFunc;
      return this;
    }
    
    public Builder setDepthMaskEnabled(boolean param1Boolean) {
      this.mDepthMask = param1Boolean;
      return this;
    }
    
    public Builder setColorMaskEnabled(boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, boolean param1Boolean4) {
      this.mColorMaskR = param1Boolean1;
      this.mColorMaskG = param1Boolean2;
      this.mColorMaskB = param1Boolean3;
      this.mColorMaskA = param1Boolean4;
      return this;
    }
    
    public Builder setBlendFunc(ProgramStore.BlendSrcFunc param1BlendSrcFunc, ProgramStore.BlendDstFunc param1BlendDstFunc) {
      this.mBlendSrc = param1BlendSrcFunc;
      this.mBlendDst = param1BlendDstFunc;
      return this;
    }
    
    public Builder setDitherEnabled(boolean param1Boolean) {
      this.mDither = param1Boolean;
      return this;
    }
    
    public ProgramStore create() {
      this.mRS.validate();
      long l = this.mRS.nProgramStoreCreate(this.mColorMaskR, this.mColorMaskG, this.mColorMaskB, this.mColorMaskA, this.mDepthMask, this.mDither, this.mBlendSrc.mID, this.mBlendDst.mID, this.mDepthFunc.mID);
      ProgramStore programStore = new ProgramStore(l, this.mRS);
      programStore.mDepthFunc = this.mDepthFunc;
      programStore.mDepthMask = this.mDepthMask;
      programStore.mColorMaskR = this.mColorMaskR;
      programStore.mColorMaskG = this.mColorMaskG;
      programStore.mColorMaskB = this.mColorMaskB;
      programStore.mColorMaskA = this.mColorMaskA;
      programStore.mBlendSrc = this.mBlendSrc;
      programStore.mBlendDst = this.mBlendDst;
      programStore.mDither = this.mDither;
      return programStore;
    }
  }
}
