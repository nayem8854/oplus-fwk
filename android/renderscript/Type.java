package android.renderscript;

public class Type extends BaseObj {
  static final int mMaxArrays = 4;
  
  int[] mArrays;
  
  boolean mDimFaces;
  
  boolean mDimMipmaps;
  
  int mDimX;
  
  int mDimY;
  
  int mDimYuv;
  
  int mDimZ;
  
  Element mElement;
  
  int mElementCount;
  
  class CubemapFace extends Enum<CubemapFace> {
    private static final CubemapFace[] $VALUES;
    
    public static final CubemapFace NEGATIVE_X = new CubemapFace("NEGATIVE_X", 1, 1);
    
    public static final CubemapFace NEGATIVE_Y;
    
    public static final CubemapFace NEGATIVE_Z;
    
    public static CubemapFace valueOf(String param1String) {
      return Enum.<CubemapFace>valueOf(CubemapFace.class, param1String);
    }
    
    public static CubemapFace[] values() {
      return (CubemapFace[])$VALUES.clone();
    }
    
    public static final CubemapFace POSITIVE_X = new CubemapFace("POSITIVE_X", 0, 0);
    
    public static final CubemapFace POSITIVE_Y = new CubemapFace("POSITIVE_Y", 2, 2);
    
    public static final CubemapFace POSITIVE_Z;
    
    @Deprecated
    public static final CubemapFace POSITVE_X;
    
    @Deprecated
    public static final CubemapFace POSITVE_Y;
    
    @Deprecated
    public static final CubemapFace POSITVE_Z;
    
    int mID;
    
    static {
      NEGATIVE_Y = new CubemapFace("NEGATIVE_Y", 3, 3);
      POSITIVE_Z = new CubemapFace("POSITIVE_Z", 4, 4);
      NEGATIVE_Z = new CubemapFace("NEGATIVE_Z", 5, 5);
      POSITVE_X = new CubemapFace("POSITVE_X", 6, 0);
      POSITVE_Y = new CubemapFace("POSITVE_Y", 7, 2);
      CubemapFace cubemapFace = new CubemapFace("POSITVE_Z", 8, 4);
      $VALUES = new CubemapFace[] { POSITIVE_X, NEGATIVE_X, POSITIVE_Y, NEGATIVE_Y, POSITIVE_Z, NEGATIVE_Z, POSITVE_X, POSITVE_Y, cubemapFace };
    }
    
    private CubemapFace(Type this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.mID = param1Int2;
    }
  }
  
  public Element getElement() {
    return this.mElement;
  }
  
  public int getX() {
    return this.mDimX;
  }
  
  public int getY() {
    return this.mDimY;
  }
  
  public int getZ() {
    return this.mDimZ;
  }
  
  public int getYuv() {
    return this.mDimYuv;
  }
  
  public boolean hasMipmaps() {
    return this.mDimMipmaps;
  }
  
  public boolean hasFaces() {
    return this.mDimFaces;
  }
  
  public int getCount() {
    return this.mElementCount;
  }
  
  public int getArray(int paramInt) {
    if (paramInt >= 0 && paramInt < 4) {
      int[] arrayOfInt = this.mArrays;
      if (arrayOfInt == null || paramInt >= arrayOfInt.length)
        return 0; 
      return arrayOfInt[paramInt];
    } 
    throw new RSIllegalArgumentException("Array dimension out of range.");
  }
  
  public int getArrayCount() {
    int[] arrayOfInt = this.mArrays;
    if (arrayOfInt != null)
      return arrayOfInt.length; 
    return 0;
  }
  
  void calcElementCount() {
    boolean bool = hasMipmaps();
    int i = getX();
    int j = getY();
    int k = getZ();
    byte b = 1;
    if (hasFaces())
      b = 6; 
    int m = i;
    if (i == 0)
      m = 1; 
    i = j;
    if (j == 0)
      i = 1; 
    j = k;
    if (k == 0)
      j = 1; 
    k = m * i * j * b;
    int n = m;
    while (bool && (n > 1 || i > 1 || j > 1)) {
      m = n;
      if (n > 1)
        m = n >> 1; 
      int i1 = i;
      if (i > 1)
        i1 = i >> 1; 
      int i2 = j;
      if (j > 1)
        i2 = j >> 1; 
      k += m * i1 * i2 * b;
      n = m;
      i = i1;
      j = i2;
    } 
    i = k;
    if (this.mArrays != null) {
      m = 0;
      while (true) {
        int[] arrayOfInt = this.mArrays;
        i = k;
        if (m < arrayOfInt.length) {
          k *= arrayOfInt[m];
          m++;
          continue;
        } 
        break;
      } 
    } 
    this.mElementCount = i;
  }
  
  Type(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
  }
  
  void updateFromNative() {
    long[] arrayOfLong = new long[6];
    this.mRS.nTypeGetNativeData(getID(this.mRS), arrayOfLong);
    boolean bool1 = false;
    this.mDimX = (int)arrayOfLong[0];
    this.mDimY = (int)arrayOfLong[1];
    this.mDimZ = (int)arrayOfLong[2];
    if (arrayOfLong[3] == 1L) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mDimMipmaps = bool2;
    boolean bool2 = bool1;
    if (arrayOfLong[4] == 1L)
      bool2 = true; 
    this.mDimFaces = bool2;
    long l = arrayOfLong[5];
    if (l != 0L) {
      Element element = new Element(l, this.mRS);
      element.updateFromNative();
    } 
    calcElementCount();
  }
  
  public static Type createX(RenderScript paramRenderScript, Element paramElement, int paramInt) {
    if (paramInt >= 1) {
      long l = paramRenderScript.nTypeCreate(paramElement.getID(paramRenderScript), paramInt, 0, 0, false, false, 0);
      Type type = new Type(l, paramRenderScript);
      type.mElement = paramElement;
      type.mDimX = paramInt;
      type.calcElementCount();
      return type;
    } 
    throw new RSInvalidStateException("Dimension must be >= 1.");
  }
  
  public static Type createXY(RenderScript paramRenderScript, Element paramElement, int paramInt1, int paramInt2) {
    if (paramInt1 >= 1 && paramInt2 >= 1) {
      long l = paramRenderScript.nTypeCreate(paramElement.getID(paramRenderScript), paramInt1, paramInt2, 0, false, false, 0);
      Type type = new Type(l, paramRenderScript);
      type.mElement = paramElement;
      type.mDimX = paramInt1;
      type.mDimY = paramInt2;
      type.calcElementCount();
      return type;
    } 
    throw new RSInvalidStateException("Dimension must be >= 1.");
  }
  
  public static Type createXYZ(RenderScript paramRenderScript, Element paramElement, int paramInt1, int paramInt2, int paramInt3) {
    if (paramInt1 >= 1 && paramInt2 >= 1 && paramInt3 >= 1) {
      long l = paramRenderScript.nTypeCreate(paramElement.getID(paramRenderScript), paramInt1, paramInt2, paramInt3, false, false, 0);
      Type type = new Type(l, paramRenderScript);
      type.mElement = paramElement;
      type.mDimX = paramInt1;
      type.mDimY = paramInt2;
      type.mDimZ = paramInt3;
      type.calcElementCount();
      return type;
    } 
    throw new RSInvalidStateException("Dimension must be >= 1.");
  }
  
  class Builder {
    int mYuv;
    
    RenderScript mRS;
    
    Element mElement;
    
    int mDimZ;
    
    int mDimY;
    
    int mDimX = 1;
    
    boolean mDimMipmaps;
    
    boolean mDimFaces;
    
    int[] mArray = new int[4];
    
    public Builder(Type this$0, Element param1Element) {
      param1Element.checkValid();
      this.mRS = (RenderScript)this$0;
      this.mElement = param1Element;
    }
    
    public Builder setX(int param1Int) {
      if (param1Int >= 1) {
        this.mDimX = param1Int;
        return this;
      } 
      throw new RSIllegalArgumentException("Values of less than 1 for Dimension X are not valid.");
    }
    
    public Builder setY(int param1Int) {
      if (param1Int >= 1) {
        this.mDimY = param1Int;
        return this;
      } 
      throw new RSIllegalArgumentException("Values of less than 1 for Dimension Y are not valid.");
    }
    
    public Builder setZ(int param1Int) {
      if (param1Int >= 1) {
        this.mDimZ = param1Int;
        return this;
      } 
      throw new RSIllegalArgumentException("Values of less than 1 for Dimension Z are not valid.");
    }
    
    public Builder setArray(int param1Int1, int param1Int2) {
      if (param1Int1 >= 0 && param1Int1 < 4) {
        this.mArray[param1Int1] = param1Int2;
        return this;
      } 
      throw new RSIllegalArgumentException("Array dimension out of range.");
    }
    
    public Builder setMipmaps(boolean param1Boolean) {
      this.mDimMipmaps = param1Boolean;
      return this;
    }
    
    public Builder setFaces(boolean param1Boolean) {
      this.mDimFaces = param1Boolean;
      return this;
    }
    
    public Builder setYuvFormat(int param1Int) {
      if (param1Int == 17 || param1Int == 35 || param1Int == 842094169) {
        this.mYuv = param1Int;
        return this;
      } 
      throw new RSIllegalArgumentException("Only ImageFormat.NV21, .YV12, and .YUV_420_888 are supported..");
    }
    
    public Type create() {
      if (this.mDimZ > 0)
        if (this.mDimX >= 1 && this.mDimY >= 1) {
          if (this.mDimFaces)
            throw new RSInvalidStateException("Cube maps not supported with 3D types."); 
        } else {
          throw new RSInvalidStateException("Both X and Y dimension required when Z is present.");
        }  
      if (this.mDimY <= 0 || 
        this.mDimX >= 1) {
        if (!this.mDimFaces || 
          this.mDimY >= 1) {
          if (this.mYuv == 0 || (
            this.mDimZ == 0 && !this.mDimFaces && !this.mDimMipmaps)) {
            int[] arrayOfInt = null;
            for (byte b = 3; b >= 0; ) {
              int[] arrayOfInt1 = arrayOfInt;
              if (this.mArray[b] != 0) {
                arrayOfInt1 = arrayOfInt;
                if (arrayOfInt == null)
                  arrayOfInt1 = new int[b]; 
              } 
              if (this.mArray[b] != 0 || arrayOfInt1 == null) {
                b--;
                arrayOfInt = arrayOfInt1;
                continue;
              } 
              throw new RSInvalidStateException("Array dimensions must be contigous from 0.");
            } 
            RenderScript renderScript = this.mRS;
            long l = renderScript.nTypeCreate(this.mElement.getID(renderScript), this.mDimX, this.mDimY, this.mDimZ, this.mDimMipmaps, this.mDimFaces, this.mYuv);
            Type type = new Type(l, this.mRS);
            type.mElement = this.mElement;
            type.mDimX = this.mDimX;
            type.mDimY = this.mDimY;
            type.mDimZ = this.mDimZ;
            type.mDimMipmaps = this.mDimMipmaps;
            type.mDimFaces = this.mDimFaces;
            type.mDimYuv = this.mYuv;
            type.mArrays = arrayOfInt;
            type.calcElementCount();
            return type;
          } 
          throw new RSInvalidStateException("YUV only supports basic 2D.");
        } 
        throw new RSInvalidStateException("Cube maps require 2D Types.");
      } 
      throw new RSInvalidStateException("X dimension required when Y is present.");
    }
  }
}
