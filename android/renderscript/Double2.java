package android.renderscript;

public class Double2 {
  public double x;
  
  public double y;
  
  public Double2() {}
  
  public Double2(Double2 paramDouble2) {
    this.x = paramDouble2.x;
    this.y = paramDouble2.y;
  }
  
  public Double2(double paramDouble1, double paramDouble2) {
    this.x = paramDouble1;
    this.y = paramDouble2;
  }
  
  public static Double2 add(Double2 paramDouble21, Double2 paramDouble22) {
    Double2 double2 = new Double2();
    paramDouble21.x += paramDouble22.x;
    paramDouble21.y += paramDouble22.y;
    return double2;
  }
  
  public void add(Double2 paramDouble2) {
    this.x += paramDouble2.x;
    this.y += paramDouble2.y;
  }
  
  public void add(double paramDouble) {
    this.x += paramDouble;
    this.y += paramDouble;
  }
  
  public static Double2 add(Double2 paramDouble2, double paramDouble) {
    Double2 double2 = new Double2();
    paramDouble2.x += paramDouble;
    paramDouble2.y += paramDouble;
    return double2;
  }
  
  public void sub(Double2 paramDouble2) {
    this.x -= paramDouble2.x;
    this.y -= paramDouble2.y;
  }
  
  public static Double2 sub(Double2 paramDouble21, Double2 paramDouble22) {
    Double2 double2 = new Double2();
    paramDouble21.x -= paramDouble22.x;
    paramDouble21.y -= paramDouble22.y;
    return double2;
  }
  
  public void sub(double paramDouble) {
    this.x -= paramDouble;
    this.y -= paramDouble;
  }
  
  public static Double2 sub(Double2 paramDouble2, double paramDouble) {
    Double2 double2 = new Double2();
    paramDouble2.x -= paramDouble;
    paramDouble2.y -= paramDouble;
    return double2;
  }
  
  public void mul(Double2 paramDouble2) {
    this.x *= paramDouble2.x;
    this.y *= paramDouble2.y;
  }
  
  public static Double2 mul(Double2 paramDouble21, Double2 paramDouble22) {
    Double2 double2 = new Double2();
    paramDouble21.x *= paramDouble22.x;
    paramDouble21.y *= paramDouble22.y;
    return double2;
  }
  
  public void mul(double paramDouble) {
    this.x *= paramDouble;
    this.y *= paramDouble;
  }
  
  public static Double2 mul(Double2 paramDouble2, double paramDouble) {
    Double2 double2 = new Double2();
    paramDouble2.x *= paramDouble;
    paramDouble2.y *= paramDouble;
    return double2;
  }
  
  public void div(Double2 paramDouble2) {
    this.x /= paramDouble2.x;
    this.y /= paramDouble2.y;
  }
  
  public static Double2 div(Double2 paramDouble21, Double2 paramDouble22) {
    Double2 double2 = new Double2();
    paramDouble21.x /= paramDouble22.x;
    paramDouble21.y /= paramDouble22.y;
    return double2;
  }
  
  public void div(double paramDouble) {
    this.x /= paramDouble;
    this.y /= paramDouble;
  }
  
  public static Double2 div(Double2 paramDouble2, double paramDouble) {
    Double2 double2 = new Double2();
    paramDouble2.x /= paramDouble;
    paramDouble2.y /= paramDouble;
    return double2;
  }
  
  public double dotProduct(Double2 paramDouble2) {
    return this.x * paramDouble2.x + this.y * paramDouble2.y;
  }
  
  public static Double dotProduct(Double2 paramDouble21, Double2 paramDouble22) {
    return Double.valueOf(paramDouble22.x * paramDouble21.x + paramDouble22.y * paramDouble21.y);
  }
  
  public void addMultiple(Double2 paramDouble2, double paramDouble) {
    this.x += paramDouble2.x * paramDouble;
    this.y += paramDouble2.y * paramDouble;
  }
  
  public void set(Double2 paramDouble2) {
    this.x = paramDouble2.x;
    this.y = paramDouble2.y;
  }
  
  public void negate() {
    this.x = -this.x;
    this.y = -this.y;
  }
  
  public int length() {
    return 2;
  }
  
  public double elementSum() {
    return this.x + this.y;
  }
  
  public double get(int paramInt) {
    if (paramInt != 0) {
      if (paramInt == 1)
        return this.y; 
      throw new IndexOutOfBoundsException("Index: i");
    } 
    return this.x;
  }
  
  public void setAt(int paramInt, double paramDouble) {
    if (paramInt != 0) {
      if (paramInt == 1) {
        this.y = paramDouble;
        return;
      } 
      throw new IndexOutOfBoundsException("Index: i");
    } 
    this.x = paramDouble;
  }
  
  public void addAt(int paramInt, double paramDouble) {
    if (paramInt != 0) {
      if (paramInt == 1) {
        this.y += paramDouble;
        return;
      } 
      throw new IndexOutOfBoundsException("Index: i");
    } 
    this.x += paramDouble;
  }
  
  public void setValues(double paramDouble1, double paramDouble2) {
    this.x = paramDouble1;
    this.y = paramDouble2;
  }
  
  public void copyTo(double[] paramArrayOfdouble, int paramInt) {
    paramArrayOfdouble[paramInt] = this.x;
    paramArrayOfdouble[paramInt + 1] = this.y;
  }
}
