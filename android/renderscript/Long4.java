package android.renderscript;

public class Long4 {
  public long w;
  
  public long x;
  
  public long y;
  
  public long z;
  
  public Long4() {}
  
  public Long4(long paramLong) {
    this.w = paramLong;
    this.z = paramLong;
    this.y = paramLong;
    this.x = paramLong;
  }
  
  public Long4(long paramLong1, long paramLong2, long paramLong3, long paramLong4) {
    this.x = paramLong1;
    this.y = paramLong2;
    this.z = paramLong3;
    this.w = paramLong4;
  }
  
  public Long4(Long4 paramLong4) {
    this.x = paramLong4.x;
    this.y = paramLong4.y;
    this.z = paramLong4.z;
    this.w = paramLong4.w;
  }
  
  public void add(Long4 paramLong4) {
    this.x += paramLong4.x;
    this.y += paramLong4.y;
    this.z += paramLong4.z;
    this.w += paramLong4.w;
  }
  
  public static Long4 add(Long4 paramLong41, Long4 paramLong42) {
    Long4 long4 = new Long4();
    paramLong41.x += paramLong42.x;
    paramLong41.y += paramLong42.y;
    paramLong41.z += paramLong42.z;
    paramLong41.w += paramLong42.w;
    return long4;
  }
  
  public void add(long paramLong) {
    this.x += paramLong;
    this.y += paramLong;
    this.z += paramLong;
    this.w += paramLong;
  }
  
  public static Long4 add(Long4 paramLong4, long paramLong) {
    Long4 long4 = new Long4();
    paramLong4.x += paramLong;
    paramLong4.y += paramLong;
    paramLong4.z += paramLong;
    paramLong4.w += paramLong;
    return long4;
  }
  
  public void sub(Long4 paramLong4) {
    this.x -= paramLong4.x;
    this.y -= paramLong4.y;
    this.z -= paramLong4.z;
    this.w -= paramLong4.w;
  }
  
  public static Long4 sub(Long4 paramLong41, Long4 paramLong42) {
    Long4 long4 = new Long4();
    paramLong41.x -= paramLong42.x;
    paramLong41.y -= paramLong42.y;
    paramLong41.z -= paramLong42.z;
    paramLong41.w -= paramLong42.w;
    return long4;
  }
  
  public void sub(long paramLong) {
    this.x -= paramLong;
    this.y -= paramLong;
    this.z -= paramLong;
    this.w -= paramLong;
  }
  
  public static Long4 sub(Long4 paramLong4, long paramLong) {
    Long4 long4 = new Long4();
    paramLong4.x -= paramLong;
    paramLong4.y -= paramLong;
    paramLong4.z -= paramLong;
    paramLong4.w -= paramLong;
    return long4;
  }
  
  public void mul(Long4 paramLong4) {
    this.x *= paramLong4.x;
    this.y *= paramLong4.y;
    this.z *= paramLong4.z;
    this.w *= paramLong4.w;
  }
  
  public static Long4 mul(Long4 paramLong41, Long4 paramLong42) {
    Long4 long4 = new Long4();
    paramLong41.x *= paramLong42.x;
    paramLong41.y *= paramLong42.y;
    paramLong41.z *= paramLong42.z;
    paramLong41.w *= paramLong42.w;
    return long4;
  }
  
  public void mul(long paramLong) {
    this.x *= paramLong;
    this.y *= paramLong;
    this.z *= paramLong;
    this.w *= paramLong;
  }
  
  public static Long4 mul(Long4 paramLong4, long paramLong) {
    Long4 long4 = new Long4();
    paramLong4.x *= paramLong;
    paramLong4.y *= paramLong;
    paramLong4.z *= paramLong;
    paramLong4.w *= paramLong;
    return long4;
  }
  
  public void div(Long4 paramLong4) {
    this.x /= paramLong4.x;
    this.y /= paramLong4.y;
    this.z /= paramLong4.z;
    this.w /= paramLong4.w;
  }
  
  public static Long4 div(Long4 paramLong41, Long4 paramLong42) {
    Long4 long4 = new Long4();
    paramLong41.x /= paramLong42.x;
    paramLong41.y /= paramLong42.y;
    paramLong41.z /= paramLong42.z;
    paramLong41.w /= paramLong42.w;
    return long4;
  }
  
  public void div(long paramLong) {
    this.x /= paramLong;
    this.y /= paramLong;
    this.z /= paramLong;
    this.w /= paramLong;
  }
  
  public static Long4 div(Long4 paramLong4, long paramLong) {
    Long4 long4 = new Long4();
    paramLong4.x /= paramLong;
    paramLong4.y /= paramLong;
    paramLong4.z /= paramLong;
    paramLong4.w /= paramLong;
    return long4;
  }
  
  public void mod(Long4 paramLong4) {
    this.x %= paramLong4.x;
    this.y %= paramLong4.y;
    this.z %= paramLong4.z;
    this.w %= paramLong4.w;
  }
  
  public static Long4 mod(Long4 paramLong41, Long4 paramLong42) {
    Long4 long4 = new Long4();
    paramLong41.x %= paramLong42.x;
    paramLong41.y %= paramLong42.y;
    paramLong41.z %= paramLong42.z;
    paramLong41.w %= paramLong42.w;
    return long4;
  }
  
  public void mod(long paramLong) {
    this.x %= paramLong;
    this.y %= paramLong;
    this.z %= paramLong;
    this.w %= paramLong;
  }
  
  public static Long4 mod(Long4 paramLong4, long paramLong) {
    Long4 long4 = new Long4();
    paramLong4.x %= paramLong;
    paramLong4.y %= paramLong;
    paramLong4.z %= paramLong;
    paramLong4.w %= paramLong;
    return long4;
  }
  
  public long length() {
    return 4L;
  }
  
  public void negate() {
    this.x = -this.x;
    this.y = -this.y;
    this.z = -this.z;
    this.w = -this.w;
  }
  
  public long dotProduct(Long4 paramLong4) {
    return this.x * paramLong4.x + this.y * paramLong4.y + this.z * paramLong4.z + this.w * paramLong4.w;
  }
  
  public static long dotProduct(Long4 paramLong41, Long4 paramLong42) {
    return paramLong42.x * paramLong41.x + paramLong42.y * paramLong41.y + paramLong42.z * paramLong41.z + paramLong42.w * paramLong41.w;
  }
  
  public void addMultiple(Long4 paramLong4, long paramLong) {
    this.x += paramLong4.x * paramLong;
    this.y += paramLong4.y * paramLong;
    this.z += paramLong4.z * paramLong;
    this.w += paramLong4.w * paramLong;
  }
  
  public void set(Long4 paramLong4) {
    this.x = paramLong4.x;
    this.y = paramLong4.y;
    this.z = paramLong4.z;
    this.w = paramLong4.w;
  }
  
  public void setValues(long paramLong1, long paramLong2, long paramLong3, long paramLong4) {
    this.x = paramLong1;
    this.y = paramLong2;
    this.z = paramLong3;
    this.w = paramLong4;
  }
  
  public long elementSum() {
    return this.x + this.y + this.z + this.w;
  }
  
  public long get(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt == 3)
            return this.w; 
          throw new IndexOutOfBoundsException("Index: i");
        } 
        return this.z;
      } 
      return this.y;
    } 
    return this.x;
  }
  
  public void setAt(int paramInt, long paramLong) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt == 3) {
            this.w = paramLong;
            return;
          } 
          throw new IndexOutOfBoundsException("Index: i");
        } 
        this.z = paramLong;
        return;
      } 
      this.y = paramLong;
      return;
    } 
    this.x = paramLong;
  }
  
  public void addAt(int paramInt, long paramLong) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt == 3) {
            this.w += paramLong;
            return;
          } 
          throw new IndexOutOfBoundsException("Index: i");
        } 
        this.z += paramLong;
        return;
      } 
      this.y += paramLong;
      return;
    } 
    this.x += paramLong;
  }
  
  public void copyTo(long[] paramArrayOflong, int paramInt) {
    paramArrayOflong[paramInt] = this.x;
    paramArrayOflong[paramInt + 1] = this.y;
    paramArrayOflong[paramInt + 2] = this.z;
    paramArrayOflong[paramInt + 3] = this.w;
  }
}
