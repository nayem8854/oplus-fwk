package android.renderscript;

public class Int2 {
  public int x;
  
  public int y;
  
  public Int2() {}
  
  public Int2(int paramInt) {
    this.y = paramInt;
    this.x = paramInt;
  }
  
  public Int2(int paramInt1, int paramInt2) {
    this.x = paramInt1;
    this.y = paramInt2;
  }
  
  public Int2(Int2 paramInt2) {
    this.x = paramInt2.x;
    this.y = paramInt2.y;
  }
  
  public void add(Int2 paramInt2) {
    this.x += paramInt2.x;
    this.y += paramInt2.y;
  }
  
  public static Int2 add(Int2 paramInt21, Int2 paramInt22) {
    Int2 int2 = new Int2();
    paramInt21.x += paramInt22.x;
    paramInt21.y += paramInt22.y;
    return int2;
  }
  
  public void add(int paramInt) {
    this.x += paramInt;
    this.y += paramInt;
  }
  
  public static Int2 add(Int2 paramInt2, int paramInt) {
    Int2 int2 = new Int2();
    paramInt2.x += paramInt;
    paramInt2.y += paramInt;
    return int2;
  }
  
  public void sub(Int2 paramInt2) {
    this.x -= paramInt2.x;
    this.y -= paramInt2.y;
  }
  
  public static Int2 sub(Int2 paramInt21, Int2 paramInt22) {
    Int2 int2 = new Int2();
    paramInt21.x -= paramInt22.x;
    paramInt21.y -= paramInt22.y;
    return int2;
  }
  
  public void sub(int paramInt) {
    this.x -= paramInt;
    this.y -= paramInt;
  }
  
  public static Int2 sub(Int2 paramInt2, int paramInt) {
    Int2 int2 = new Int2();
    paramInt2.x -= paramInt;
    paramInt2.y -= paramInt;
    return int2;
  }
  
  public void mul(Int2 paramInt2) {
    this.x *= paramInt2.x;
    this.y *= paramInt2.y;
  }
  
  public static Int2 mul(Int2 paramInt21, Int2 paramInt22) {
    Int2 int2 = new Int2();
    paramInt21.x *= paramInt22.x;
    paramInt21.y *= paramInt22.y;
    return int2;
  }
  
  public void mul(int paramInt) {
    this.x *= paramInt;
    this.y *= paramInt;
  }
  
  public static Int2 mul(Int2 paramInt2, int paramInt) {
    Int2 int2 = new Int2();
    paramInt2.x *= paramInt;
    paramInt2.y *= paramInt;
    return int2;
  }
  
  public void div(Int2 paramInt2) {
    this.x /= paramInt2.x;
    this.y /= paramInt2.y;
  }
  
  public static Int2 div(Int2 paramInt21, Int2 paramInt22) {
    Int2 int2 = new Int2();
    paramInt21.x /= paramInt22.x;
    paramInt21.y /= paramInt22.y;
    return int2;
  }
  
  public void div(int paramInt) {
    this.x /= paramInt;
    this.y /= paramInt;
  }
  
  public static Int2 div(Int2 paramInt2, int paramInt) {
    Int2 int2 = new Int2();
    paramInt2.x /= paramInt;
    paramInt2.y /= paramInt;
    return int2;
  }
  
  public void mod(Int2 paramInt2) {
    this.x %= paramInt2.x;
    this.y %= paramInt2.y;
  }
  
  public static Int2 mod(Int2 paramInt21, Int2 paramInt22) {
    Int2 int2 = new Int2();
    paramInt21.x %= paramInt22.x;
    paramInt21.y %= paramInt22.y;
    return int2;
  }
  
  public void mod(int paramInt) {
    this.x %= paramInt;
    this.y %= paramInt;
  }
  
  public static Int2 mod(Int2 paramInt2, int paramInt) {
    Int2 int2 = new Int2();
    paramInt2.x %= paramInt;
    paramInt2.y %= paramInt;
    return int2;
  }
  
  public int length() {
    return 2;
  }
  
  public void negate() {
    this.x = -this.x;
    this.y = -this.y;
  }
  
  public int dotProduct(Int2 paramInt2) {
    return this.x * paramInt2.x + this.y * paramInt2.y;
  }
  
  public static int dotProduct(Int2 paramInt21, Int2 paramInt22) {
    return paramInt22.x * paramInt21.x + paramInt22.y * paramInt21.y;
  }
  
  public void addMultiple(Int2 paramInt2, int paramInt) {
    this.x += paramInt2.x * paramInt;
    this.y += paramInt2.y * paramInt;
  }
  
  public void set(Int2 paramInt2) {
    this.x = paramInt2.x;
    this.y = paramInt2.y;
  }
  
  public void setValues(int paramInt1, int paramInt2) {
    this.x = paramInt1;
    this.y = paramInt2;
  }
  
  public int elementSum() {
    return this.x + this.y;
  }
  
  public int get(int paramInt) {
    if (paramInt != 0) {
      if (paramInt == 1)
        return this.y; 
      throw new IndexOutOfBoundsException("Index: i");
    } 
    return this.x;
  }
  
  public void setAt(int paramInt1, int paramInt2) {
    if (paramInt1 != 0) {
      if (paramInt1 == 1) {
        this.y = paramInt2;
        return;
      } 
      throw new IndexOutOfBoundsException("Index: i");
    } 
    this.x = paramInt2;
  }
  
  public void addAt(int paramInt1, int paramInt2) {
    if (paramInt1 != 0) {
      if (paramInt1 == 1) {
        this.y += paramInt2;
        return;
      } 
      throw new IndexOutOfBoundsException("Index: i");
    } 
    this.x += paramInt2;
  }
  
  public void copyTo(int[] paramArrayOfint, int paramInt) {
    paramArrayOfint[paramInt] = this.x;
    paramArrayOfint[paramInt + 1] = this.y;
  }
}
