package android.renderscript;

import android.content.Context;
import android.util.SparseArray;
import java.io.UnsupportedEncodingException;

public class Script extends BaseObj {
  public static final class KernelID extends BaseObj {
    Script mScript;
    
    int mSig;
    
    int mSlot;
    
    KernelID(long param1Long, RenderScript param1RenderScript, Script param1Script, int param1Int1, int param1Int2) {
      super(param1Long, param1RenderScript);
      this.mScript = param1Script;
      this.mSlot = param1Int1;
      this.mSig = param1Int2;
    }
  }
  
  private final SparseArray<KernelID> mKIDs = new SparseArray();
  
  long[] mInIdsBuffer;
  
  protected KernelID createKernelID(int paramInt1, int paramInt2, Element paramElement1, Element paramElement2) {
    KernelID kernelID = (KernelID)this.mKIDs.get(paramInt1);
    if (kernelID != null)
      return kernelID; 
    long l = this.mRS.nScriptKernelIDCreate(getID(this.mRS), paramInt1, paramInt2);
    if (l != 0L) {
      kernelID = new KernelID(l, this.mRS, this, paramInt1, paramInt2);
      this.mKIDs.put(paramInt1, kernelID);
      return kernelID;
    } 
    throw new RSDriverException("Failed to create KernelID");
  }
  
  public static final class InvokeID extends BaseObj {
    Script mScript;
    
    int mSlot;
    
    InvokeID(long param1Long, RenderScript param1RenderScript, Script param1Script, int param1Int) {
      super(param1Long, param1RenderScript);
      this.mScript = param1Script;
      this.mSlot = param1Int;
    }
  }
  
  private final SparseArray<InvokeID> mIIDs = new SparseArray();
  
  protected InvokeID createInvokeID(int paramInt) {
    InvokeID invokeID = (InvokeID)this.mIIDs.get(paramInt);
    if (invokeID != null)
      return invokeID; 
    long l = this.mRS.nScriptInvokeIDCreate(getID(this.mRS), paramInt);
    if (l != 0L) {
      invokeID = new InvokeID(l, this.mRS, this, paramInt);
      this.mIIDs.put(paramInt, invokeID);
      return invokeID;
    } 
    throw new RSDriverException("Failed to create KernelID");
  }
  
  public static final class FieldID extends BaseObj {
    Script mScript;
    
    int mSlot;
    
    FieldID(long param1Long, RenderScript param1RenderScript, Script param1Script, int param1Int) {
      super(param1Long, param1RenderScript);
      this.mScript = param1Script;
      this.mSlot = param1Int;
    }
  }
  
  private final SparseArray<FieldID> mFIDs = new SparseArray();
  
  protected FieldID createFieldID(int paramInt, Element paramElement) {
    FieldID fieldID = (FieldID)this.mFIDs.get(paramInt);
    if (fieldID != null)
      return fieldID; 
    long l = this.mRS.nScriptFieldIDCreate(getID(this.mRS), paramInt);
    if (l != 0L) {
      fieldID = new FieldID(l, this.mRS, this, paramInt);
      this.mFIDs.put(paramInt, fieldID);
      return fieldID;
    } 
    throw new RSDriverException("Failed to create FieldID");
  }
  
  protected void invoke(int paramInt) {
    this.mRS.nScriptInvoke(getID(this.mRS), paramInt);
  }
  
  protected void invoke(int paramInt, FieldPacker paramFieldPacker) {
    if (paramFieldPacker != null) {
      this.mRS.nScriptInvokeV(getID(this.mRS), paramInt, paramFieldPacker.getData());
    } else {
      this.mRS.nScriptInvoke(getID(this.mRS), paramInt);
    } 
  }
  
  protected void forEach(int paramInt, Allocation paramAllocation1, Allocation paramAllocation2, FieldPacker paramFieldPacker) {
    forEach(paramInt, paramAllocation1, paramAllocation2, paramFieldPacker, (LaunchOptions)null);
  }
  
  protected void forEach(int paramInt, Allocation paramAllocation1, Allocation paramAllocation2, FieldPacker paramFieldPacker, LaunchOptions paramLaunchOptions) {
    this.mRS.validate();
    this.mRS.validateObject(paramAllocation1);
    this.mRS.validateObject(paramAllocation2);
    if (paramAllocation1 != null || paramAllocation2 != null || paramLaunchOptions != null) {
      long[] arrayOfLong = null;
      if (paramAllocation1 != null) {
        arrayOfLong = this.mInIdsBuffer;
        arrayOfLong[0] = paramAllocation1.getID(this.mRS);
      } 
      long l = 0L;
      if (paramAllocation2 != null)
        l = paramAllocation2.getID(this.mRS); 
      if (paramFieldPacker != null) {
        byte[] arrayOfByte = paramFieldPacker.getData();
      } else {
        paramAllocation1 = null;
      } 
      if (paramLaunchOptions != null) {
        int i = paramLaunchOptions.xstart;
        int j = paramLaunchOptions.xend;
        int k = paramLaunchOptions.ystart;
        int m = paramLaunchOptions.yend;
        int n = paramLaunchOptions.zstart;
        int[] arrayOfInt = { i, j, k, m, n, paramLaunchOptions.zend };
      } else {
        paramAllocation2 = null;
      } 
      this.mRS.nScriptForEach(getID(this.mRS), paramInt, arrayOfLong, l, (byte[])paramAllocation1, (int[])paramAllocation2);
      return;
    } 
    throw new RSIllegalArgumentException("At least one of input allocation, output allocation, or LaunchOptions is required to be non-null.");
  }
  
  protected void forEach(int paramInt, Allocation[] paramArrayOfAllocation, Allocation paramAllocation, FieldPacker paramFieldPacker) {
    forEach(paramInt, paramArrayOfAllocation, paramAllocation, paramFieldPacker, (LaunchOptions)null);
  }
  
  protected void forEach(int paramInt, Allocation[] paramArrayOfAllocation, Allocation paramAllocation, FieldPacker paramFieldPacker, LaunchOptions paramLaunchOptions) {
    this.mRS.validate();
    if (paramArrayOfAllocation != null) {
      int i;
      byte b;
      for (i = paramArrayOfAllocation.length, b = 0; b < i; ) {
        Allocation allocation = paramArrayOfAllocation[b];
        this.mRS.validateObject(allocation);
        b++;
      } 
    } 
    this.mRS.validateObject(paramAllocation);
    if (paramArrayOfAllocation != null || paramAllocation != null) {
      long l;
      if (paramArrayOfAllocation != null) {
        long[] arrayOfLong2 = new long[paramArrayOfAllocation.length];
        for (byte b = 0; b < paramArrayOfAllocation.length; b++)
          arrayOfLong2[b] = paramArrayOfAllocation[b].getID(this.mRS); 
        long[] arrayOfLong1 = arrayOfLong2;
      } else {
        paramArrayOfAllocation = null;
      } 
      if (paramAllocation != null) {
        l = paramAllocation.getID(this.mRS);
      } else {
        l = 0L;
      } 
      if (paramFieldPacker != null) {
        byte[] arrayOfByte = paramFieldPacker.getData();
      } else {
        paramAllocation = null;
      } 
      if (paramLaunchOptions != null) {
        int k = paramLaunchOptions.xstart;
        int j = paramLaunchOptions.xend;
        int i = paramLaunchOptions.ystart;
        int m = paramLaunchOptions.yend;
        int n = paramLaunchOptions.zstart;
        int[] arrayOfInt = { k, j, i, m, n, paramLaunchOptions.zend };
      } else {
        paramFieldPacker = null;
      } 
      this.mRS.nScriptForEach(getID(this.mRS), paramInt, (long[])paramArrayOfAllocation, l, (byte[])paramAllocation, (int[])paramFieldPacker);
      return;
    } 
    throw new RSIllegalArgumentException("At least one of ain or aout is required to be non-null.");
  }
  
  protected void reduce(int paramInt, Allocation[] paramArrayOfAllocation, Allocation paramAllocation, LaunchOptions paramLaunchOptions) {
    this.mRS.validate();
    if (paramArrayOfAllocation != null && paramArrayOfAllocation.length >= 1) {
      if (paramAllocation != null) {
        int i, j;
        for (i = paramArrayOfAllocation.length, j = 0; j < i; ) {
          Allocation allocation = paramArrayOfAllocation[j];
          this.mRS.validateObject(allocation);
          j++;
        } 
        long[] arrayOfLong = new long[paramArrayOfAllocation.length];
        for (j = 0; j < paramArrayOfAllocation.length; j++)
          arrayOfLong[j] = paramArrayOfAllocation[j].getID(this.mRS); 
        long l = paramAllocation.getID(this.mRS);
        if (paramLaunchOptions != null) {
          j = paramLaunchOptions.xstart;
          int k = paramLaunchOptions.xend;
          int m = paramLaunchOptions.ystart;
          int n = paramLaunchOptions.yend;
          i = paramLaunchOptions.zstart;
          int[] arrayOfInt = { j, k, m, n, i, paramLaunchOptions.zend };
        } else {
          paramArrayOfAllocation = null;
        } 
        this.mRS.nScriptReduce(getID(this.mRS), paramInt, arrayOfLong, l, (int[])paramArrayOfAllocation);
        return;
      } 
      throw new RSIllegalArgumentException("aout is required to be non-null.");
    } 
    throw new RSIllegalArgumentException("At least one input is required.");
  }
  
  Script(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
    this.mInIdsBuffer = new long[1];
    this.guard.open("destroy");
  }
  
  public void bindAllocation(Allocation paramAllocation, int paramInt) {
    this.mRS.validate();
    this.mRS.validateObject(paramAllocation);
    if (paramAllocation != null) {
      Context context = this.mRS.getApplicationContext();
      if ((context.getApplicationInfo()).targetSdkVersion >= 20) {
        Type type = paramAllocation.mType;
        if (type.hasMipmaps() || type.hasFaces() || type.getY() != 0 || 
          type.getZ() != 0)
          throw new RSIllegalArgumentException("API 20+ only allows simple 1D allocations to be used with bind."); 
      } 
      this.mRS.nScriptBindAllocation(getID(this.mRS), paramAllocation.getID(this.mRS), paramInt);
    } else {
      this.mRS.nScriptBindAllocation(getID(this.mRS), 0L, paramInt);
    } 
  }
  
  public void setVar(int paramInt, float paramFloat) {
    this.mRS.nScriptSetVarF(getID(this.mRS), paramInt, paramFloat);
  }
  
  public float getVarF(int paramInt) {
    return this.mRS.nScriptGetVarF(getID(this.mRS), paramInt);
  }
  
  public void setVar(int paramInt, double paramDouble) {
    this.mRS.nScriptSetVarD(getID(this.mRS), paramInt, paramDouble);
  }
  
  public double getVarD(int paramInt) {
    return this.mRS.nScriptGetVarD(getID(this.mRS), paramInt);
  }
  
  public void setVar(int paramInt1, int paramInt2) {
    this.mRS.nScriptSetVarI(getID(this.mRS), paramInt1, paramInt2);
  }
  
  public int getVarI(int paramInt) {
    return this.mRS.nScriptGetVarI(getID(this.mRS), paramInt);
  }
  
  public void setVar(int paramInt, long paramLong) {
    this.mRS.nScriptSetVarJ(getID(this.mRS), paramInt, paramLong);
  }
  
  public long getVarJ(int paramInt) {
    return this.mRS.nScriptGetVarJ(getID(this.mRS), paramInt);
  }
  
  public void setVar(int paramInt, boolean paramBoolean) {
    this.mRS.nScriptSetVarI(getID(this.mRS), paramInt, paramBoolean);
  }
  
  public boolean getVarB(int paramInt) {
    boolean bool;
    if (this.mRS.nScriptGetVarI(getID(this.mRS), paramInt) > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setVar(int paramInt, BaseObj paramBaseObj) {
    long l2;
    this.mRS.validate();
    this.mRS.validateObject(paramBaseObj);
    RenderScript renderScript = this.mRS;
    long l1 = getID(this.mRS);
    if (paramBaseObj == null) {
      l2 = 0L;
    } else {
      l2 = paramBaseObj.getID(this.mRS);
    } 
    renderScript.nScriptSetVarObj(l1, paramInt, l2);
  }
  
  public void setVar(int paramInt, FieldPacker paramFieldPacker) {
    this.mRS.nScriptSetVarV(getID(this.mRS), paramInt, paramFieldPacker.getData());
  }
  
  public void setVar(int paramInt, FieldPacker paramFieldPacker, Element paramElement, int[] paramArrayOfint) {
    this.mRS.nScriptSetVarVE(getID(this.mRS), paramInt, paramFieldPacker.getData(), paramElement.getID(this.mRS), paramArrayOfint);
  }
  
  public void getVarV(int paramInt, FieldPacker paramFieldPacker) {
    this.mRS.nScriptGetVarV(getID(this.mRS), paramInt, paramFieldPacker.getData());
  }
  
  public void setTimeZone(String paramString) {
    this.mRS.validate();
    try {
      this.mRS.nScriptSetTimeZone(getID(this.mRS), paramString.getBytes("UTF-8"));
      return;
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      throw new RuntimeException(unsupportedEncodingException);
    } 
  }
  
  class Builder {
    RenderScript mRS;
    
    Builder(Script this$0) {
      this.mRS = (RenderScript)this$0;
    }
  }
  
  class FieldBase {
    protected Allocation mAllocation;
    
    protected Element mElement;
    
    protected void init(RenderScript param1RenderScript, int param1Int) {
      this.mAllocation = Allocation.createSized(param1RenderScript, this.mElement, param1Int, 1);
    }
    
    protected void init(RenderScript param1RenderScript, int param1Int1, int param1Int2) {
      Element element = this.mElement;
      this.mAllocation = Allocation.createSized(param1RenderScript, element, param1Int1, param1Int2 | 0x1);
    }
    
    public Element getElement() {
      return this.mElement;
    }
    
    public Type getType() {
      return this.mAllocation.getType();
    }
    
    public Allocation getAllocation() {
      return this.mAllocation;
    }
    
    public void updateAllocation() {}
  }
  
  class LaunchOptions {
    private int strategy;
    
    private int xend;
    
    private int xstart;
    
    private int yend;
    
    private int ystart;
    
    private int zend;
    
    private int zstart;
    
    public LaunchOptions() {
      this.xstart = 0;
      this.ystart = 0;
      this.xend = 0;
      this.yend = 0;
      this.zstart = 0;
      this.zend = 0;
    }
    
    public LaunchOptions setX(int param1Int1, int param1Int2) {
      if (param1Int1 >= 0 && param1Int2 > param1Int1) {
        this.xstart = param1Int1;
        this.xend = param1Int2;
        return this;
      } 
      throw new RSIllegalArgumentException("Invalid dimensions");
    }
    
    public LaunchOptions setY(int param1Int1, int param1Int2) {
      if (param1Int1 >= 0 && param1Int2 > param1Int1) {
        this.ystart = param1Int1;
        this.yend = param1Int2;
        return this;
      } 
      throw new RSIllegalArgumentException("Invalid dimensions");
    }
    
    public LaunchOptions setZ(int param1Int1, int param1Int2) {
      if (param1Int1 >= 0 && param1Int2 > param1Int1) {
        this.zstart = param1Int1;
        this.zend = param1Int2;
        return this;
      } 
      throw new RSIllegalArgumentException("Invalid dimensions");
    }
    
    public int getXStart() {
      return this.xstart;
    }
    
    public int getXEnd() {
      return this.xend;
    }
    
    public int getYStart() {
      return this.ystart;
    }
    
    public int getYEnd() {
      return this.yend;
    }
    
    public int getZStart() {
      return this.zstart;
    }
    
    public int getZEnd() {
      return this.zend;
    }
  }
}
