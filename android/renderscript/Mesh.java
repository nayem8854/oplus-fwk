package android.renderscript;

import java.util.Vector;

public class Mesh extends BaseObj {
  Allocation[] mIndexBuffers;
  
  Primitive[] mPrimitives;
  
  Allocation[] mVertexBuffers;
  
  class Primitive extends Enum<Primitive> {
    private static final Primitive[] $VALUES;
    
    public static final Primitive LINE = new Primitive("LINE", 1, 1);
    
    public static final Primitive LINE_STRIP = new Primitive("LINE_STRIP", 2, 2);
    
    public static Primitive valueOf(String param1String) {
      return Enum.<Primitive>valueOf(Primitive.class, param1String);
    }
    
    public static Primitive[] values() {
      return (Primitive[])$VALUES.clone();
    }
    
    public static final Primitive POINT = new Primitive("POINT", 0, 0);
    
    public static final Primitive TRIANGLE = new Primitive("TRIANGLE", 3, 3);
    
    public static final Primitive TRIANGLE_FAN;
    
    public static final Primitive TRIANGLE_STRIP = new Primitive("TRIANGLE_STRIP", 4, 4);
    
    int mID;
    
    static {
      Primitive primitive = new Primitive("TRIANGLE_FAN", 5, 5);
      $VALUES = new Primitive[] { POINT, LINE, LINE_STRIP, TRIANGLE, TRIANGLE_STRIP, primitive };
    }
    
    private Primitive(Mesh this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.mID = param1Int2;
    }
  }
  
  Mesh(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
    this.guard.open("destroy");
  }
  
  public int getVertexAllocationCount() {
    Allocation[] arrayOfAllocation = this.mVertexBuffers;
    if (arrayOfAllocation == null)
      return 0; 
    return arrayOfAllocation.length;
  }
  
  public Allocation getVertexAllocation(int paramInt) {
    return this.mVertexBuffers[paramInt];
  }
  
  public int getPrimitiveCount() {
    Allocation[] arrayOfAllocation = this.mIndexBuffers;
    if (arrayOfAllocation == null)
      return 0; 
    return arrayOfAllocation.length;
  }
  
  public Allocation getIndexSetAllocation(int paramInt) {
    return this.mIndexBuffers[paramInt];
  }
  
  public Primitive getPrimitive(int paramInt) {
    return this.mPrimitives[paramInt];
  }
  
  void updateFromNative() {
    super.updateFromNative();
    int i = this.mRS.nMeshGetVertexBufferCount(getID(this.mRS));
    int j = this.mRS.nMeshGetIndexCount(getID(this.mRS));
    long[] arrayOfLong1 = new long[i];
    long[] arrayOfLong2 = new long[j];
    int[] arrayOfInt = new int[j];
    this.mRS.nMeshGetVertices(getID(this.mRS), arrayOfLong1, i);
    this.mRS.nMeshGetIndices(getID(this.mRS), arrayOfLong2, arrayOfInt, j);
    this.mVertexBuffers = new Allocation[i];
    this.mIndexBuffers = new Allocation[j];
    this.mPrimitives = new Primitive[j];
    byte b;
    for (b = 0; b < i; b++) {
      if (arrayOfLong1[b] != 0L) {
        this.mVertexBuffers[b] = new Allocation(arrayOfLong1[b], this.mRS, null, 1);
        this.mVertexBuffers[b].updateFromNative();
      } 
    } 
    for (b = 0; b < j; b++) {
      if (arrayOfLong2[b] != 0L) {
        this.mIndexBuffers[b] = new Allocation(arrayOfLong2[b], this.mRS, null, 1);
        this.mIndexBuffers[b].updateFromNative();
      } 
      this.mPrimitives[b] = Primitive.values()[arrayOfInt[b]];
    } 
  }
  
  class Builder {
    Vector mIndexTypes;
    
    RenderScript mRS;
    
    int mUsage;
    
    int mVertexTypeCount;
    
    Entry[] mVertexTypes;
    
    class Entry {
      Element e;
      
      Mesh.Primitive prim;
      
      int size;
      
      Type t;
      
      final Mesh.Builder this$0;
      
      int usage;
    }
    
    public Builder(Mesh this$0, int param1Int) {
      this.mRS = (RenderScript)this$0;
      this.mUsage = param1Int;
      this.mVertexTypeCount = 0;
      this.mVertexTypes = new Entry[16];
      this.mIndexTypes = new Vector();
    }
    
    public int getCurrentVertexTypeIndex() {
      return this.mVertexTypeCount - 1;
    }
    
    public int getCurrentIndexSetIndex() {
      return this.mIndexTypes.size() - 1;
    }
    
    public Builder addVertexType(Type param1Type) throws IllegalStateException {
      int i = this.mVertexTypeCount;
      Entry[] arrayOfEntry = this.mVertexTypes;
      if (i < arrayOfEntry.length) {
        arrayOfEntry[i] = new Entry();
        (this.mVertexTypes[this.mVertexTypeCount]).t = param1Type;
        (this.mVertexTypes[this.mVertexTypeCount]).e = null;
        this.mVertexTypeCount++;
        return this;
      } 
      throw new IllegalStateException("Max vertex types exceeded.");
    }
    
    public Builder addVertexType(Element param1Element, int param1Int) throws IllegalStateException {
      int i = this.mVertexTypeCount;
      Entry[] arrayOfEntry = this.mVertexTypes;
      if (i < arrayOfEntry.length) {
        arrayOfEntry[i] = new Entry();
        (this.mVertexTypes[this.mVertexTypeCount]).t = null;
        (this.mVertexTypes[this.mVertexTypeCount]).e = param1Element;
        (this.mVertexTypes[this.mVertexTypeCount]).size = param1Int;
        this.mVertexTypeCount++;
        return this;
      } 
      throw new IllegalStateException("Max vertex types exceeded.");
    }
    
    public Builder addIndexSetType(Type param1Type, Mesh.Primitive param1Primitive) {
      Entry entry = new Entry();
      entry.t = param1Type;
      entry.e = null;
      entry.size = 0;
      entry.prim = param1Primitive;
      this.mIndexTypes.addElement(entry);
      return this;
    }
    
    public Builder addIndexSetType(Mesh.Primitive param1Primitive) {
      Entry entry = new Entry();
      entry.t = null;
      entry.e = null;
      entry.size = 0;
      entry.prim = param1Primitive;
      this.mIndexTypes.addElement(entry);
      return this;
    }
    
    public Builder addIndexSetType(Element param1Element, int param1Int, Mesh.Primitive param1Primitive) {
      Entry entry = new Entry();
      entry.t = null;
      entry.e = param1Element;
      entry.size = param1Int;
      entry.prim = param1Primitive;
      this.mIndexTypes.addElement(entry);
      return this;
    }
    
    Type newType(Element param1Element, int param1Int) {
      Type.Builder builder = new Type.Builder(this.mRS, param1Element);
      builder.setX(param1Int);
      return builder.create();
    }
    
    public Mesh create() {
      this.mRS.validate();
      long[] arrayOfLong1 = new long[this.mVertexTypeCount];
      long[] arrayOfLong2 = new long[this.mIndexTypes.size()];
      int[] arrayOfInt = new int[this.mIndexTypes.size()];
      Allocation[] arrayOfAllocation1 = new Allocation[this.mVertexTypeCount];
      Allocation[] arrayOfAllocation2 = new Allocation[this.mIndexTypes.size()];
      Mesh.Primitive[] arrayOfPrimitive = new Mesh.Primitive[this.mIndexTypes.size()];
      byte b;
      for (b = 0; b < this.mVertexTypeCount; b++) {
        Allocation allocation;
        Entry entry = this.mVertexTypes[b];
        if (entry.t != null) {
          allocation = Allocation.createTyped(this.mRS, entry.t, this.mUsage);
        } else if (((Entry)allocation).e != null) {
          allocation = Allocation.createSized(this.mRS, ((Entry)allocation).e, ((Entry)allocation).size, this.mUsage);
        } else {
          throw new IllegalStateException("Builder corrupt, no valid element in entry.");
        } 
        arrayOfAllocation1[b] = allocation;
        arrayOfLong1[b] = allocation.getID(this.mRS);
      } 
      for (b = 0; b < this.mIndexTypes.size(); b++) {
        Allocation allocation;
        long l1;
        Entry entry = this.mIndexTypes.elementAt(b);
        if (entry.t != null) {
          allocation = Allocation.createTyped(this.mRS, entry.t, this.mUsage);
        } else if (entry.e != null) {
          allocation = Allocation.createSized(this.mRS, entry.e, entry.size, this.mUsage);
        } else {
          throw new IllegalStateException("Builder corrupt, no valid element in entry.");
        } 
        if (allocation == null) {
          l1 = 0L;
        } else {
          l1 = allocation.getID(this.mRS);
        } 
        arrayOfAllocation2[b] = allocation;
        arrayOfPrimitive[b] = entry.prim;
        arrayOfLong2[b] = l1;
        arrayOfInt[b] = entry.prim.mID;
      } 
      long l = this.mRS.nMeshCreate(arrayOfLong1, arrayOfLong2, arrayOfInt);
      Mesh mesh = new Mesh(l, this.mRS);
      mesh.mVertexBuffers = arrayOfAllocation1;
      mesh.mIndexBuffers = arrayOfAllocation2;
      mesh.mPrimitives = arrayOfPrimitive;
      return mesh;
    }
  }
  
  class AllocationBuilder {
    Vector mIndexTypes;
    
    RenderScript mRS;
    
    int mVertexTypeCount;
    
    Entry[] mVertexTypes;
    
    class Entry {
      Allocation a;
      
      Mesh.Primitive prim;
      
      final Mesh.AllocationBuilder this$0;
    }
    
    public AllocationBuilder(Mesh this$0) {
      this.mRS = (RenderScript)this$0;
      this.mVertexTypeCount = 0;
      this.mVertexTypes = new Entry[16];
      this.mIndexTypes = new Vector();
    }
    
    public int getCurrentVertexTypeIndex() {
      return this.mVertexTypeCount - 1;
    }
    
    public int getCurrentIndexSetIndex() {
      return this.mIndexTypes.size() - 1;
    }
    
    public AllocationBuilder addVertexAllocation(Allocation param1Allocation) throws IllegalStateException {
      int i = this.mVertexTypeCount;
      Entry[] arrayOfEntry = this.mVertexTypes;
      if (i < arrayOfEntry.length) {
        arrayOfEntry[i] = new Entry();
        (this.mVertexTypes[this.mVertexTypeCount]).a = param1Allocation;
        this.mVertexTypeCount++;
        return this;
      } 
      throw new IllegalStateException("Max vertex types exceeded.");
    }
    
    public AllocationBuilder addIndexSetAllocation(Allocation param1Allocation, Mesh.Primitive param1Primitive) {
      Entry entry = new Entry();
      entry.a = param1Allocation;
      entry.prim = param1Primitive;
      this.mIndexTypes.addElement(entry);
      return this;
    }
    
    public AllocationBuilder addIndexSetType(Mesh.Primitive param1Primitive) {
      Entry entry = new Entry();
      entry.a = null;
      entry.prim = param1Primitive;
      this.mIndexTypes.addElement(entry);
      return this;
    }
    
    public Mesh create() {
      this.mRS.validate();
      long[] arrayOfLong1 = new long[this.mVertexTypeCount];
      long[] arrayOfLong2 = new long[this.mIndexTypes.size()];
      int[] arrayOfInt = new int[this.mIndexTypes.size()];
      Allocation[] arrayOfAllocation1 = new Allocation[this.mIndexTypes.size()];
      Mesh.Primitive[] arrayOfPrimitive = new Mesh.Primitive[this.mIndexTypes.size()];
      Allocation[] arrayOfAllocation2 = new Allocation[this.mVertexTypeCount];
      byte b;
      for (b = 0; b < this.mVertexTypeCount; b++) {
        Entry entry = this.mVertexTypes[b];
        arrayOfAllocation2[b] = entry.a;
        arrayOfLong1[b] = entry.a.getID(this.mRS);
      } 
      for (b = 0; b < this.mIndexTypes.size(); b++) {
        long l1;
        Entry entry = this.mIndexTypes.elementAt(b);
        if (entry.a == null) {
          l1 = 0L;
        } else {
          l1 = entry.a.getID(this.mRS);
        } 
        arrayOfAllocation1[b] = entry.a;
        arrayOfPrimitive[b] = entry.prim;
        arrayOfLong2[b] = l1;
        arrayOfInt[b] = entry.prim.mID;
      } 
      long l = this.mRS.nMeshCreate(arrayOfLong1, arrayOfLong2, arrayOfInt);
      Mesh mesh = new Mesh(l, this.mRS);
      mesh.mVertexBuffers = arrayOfAllocation2;
      mesh.mIndexBuffers = arrayOfAllocation1;
      mesh.mPrimitives = arrayOfPrimitive;
      return mesh;
    }
  }
  
  class TriangleMeshBuilder {
    float mNX = 0.0F;
    
    float mNY = 0.0F;
    
    float mNZ = -1.0F;
    
    float mS0 = 0.0F;
    
    float mT0 = 0.0F;
    
    float mR = 1.0F;
    
    float mG = 1.0F;
    
    float mB = 1.0F;
    
    float mA = 1.0F;
    
    public static final int COLOR = 1;
    
    public static final int NORMAL = 2;
    
    public static final int TEXTURE_0 = 256;
    
    Element mElement;
    
    int mFlags;
    
    int mIndexCount;
    
    short[] mIndexData;
    
    int mMaxIndex;
    
    RenderScript mRS;
    
    int mVtxCount;
    
    float[] mVtxData;
    
    int mVtxSize;
    
    public TriangleMeshBuilder(Mesh this$0, int param1Int1, int param1Int2) {
      this.mRS = (RenderScript)this$0;
      this.mVtxCount = 0;
      this.mMaxIndex = 0;
      this.mIndexCount = 0;
      this.mVtxData = new float[128];
      this.mIndexData = new short[128];
      this.mVtxSize = param1Int1;
      this.mFlags = param1Int2;
      if (param1Int1 >= 2 && param1Int1 <= 3)
        return; 
      throw new IllegalArgumentException("Vertex size out of range.");
    }
    
    private void makeSpace(int param1Int) {
      int i = this.mVtxCount;
      float[] arrayOfFloat = this.mVtxData;
      if (i + param1Int >= arrayOfFloat.length) {
        float[] arrayOfFloat1 = new float[arrayOfFloat.length * 2];
        System.arraycopy(arrayOfFloat, 0, arrayOfFloat1, 0, arrayOfFloat.length);
        this.mVtxData = arrayOfFloat1;
      } 
    }
    
    private void latch() {
      if ((this.mFlags & 0x1) != 0) {
        makeSpace(4);
        float[] arrayOfFloat = this.mVtxData;
        int i = this.mVtxCount, j = i + 1;
        arrayOfFloat[i] = this.mR;
        this.mVtxCount = i = j + 1;
        arrayOfFloat[j] = this.mG;
        this.mVtxCount = j = i + 1;
        arrayOfFloat[i] = this.mB;
        this.mVtxCount = j + 1;
        arrayOfFloat[j] = this.mA;
      } 
      if ((this.mFlags & 0x100) != 0) {
        makeSpace(2);
        float[] arrayOfFloat = this.mVtxData;
        int j = this.mVtxCount, i = j + 1;
        arrayOfFloat[j] = this.mS0;
        this.mVtxCount = i + 1;
        arrayOfFloat[i] = this.mT0;
      } 
      if ((this.mFlags & 0x2) != 0) {
        makeSpace(4);
        float[] arrayOfFloat = this.mVtxData;
        int j = this.mVtxCount, i = j + 1;
        arrayOfFloat[j] = this.mNX;
        this.mVtxCount = j = i + 1;
        arrayOfFloat[i] = this.mNY;
        this.mVtxCount = i = j + 1;
        arrayOfFloat[j] = this.mNZ;
        this.mVtxCount = i + 1;
        arrayOfFloat[i] = 0.0F;
      } 
      this.mMaxIndex++;
    }
    
    public TriangleMeshBuilder addVertex(float param1Float1, float param1Float2) {
      if (this.mVtxSize == 2) {
        makeSpace(2);
        float[] arrayOfFloat = this.mVtxData;
        int i = this.mVtxCount, j = i + 1;
        arrayOfFloat[i] = param1Float1;
        this.mVtxCount = j + 1;
        arrayOfFloat[j] = param1Float2;
        latch();
        return this;
      } 
      throw new IllegalStateException("add mistmatch with declared components.");
    }
    
    public TriangleMeshBuilder addVertex(float param1Float1, float param1Float2, float param1Float3) {
      if (this.mVtxSize == 3) {
        makeSpace(4);
        float[] arrayOfFloat = this.mVtxData;
        int i = this.mVtxCount, j = i + 1;
        arrayOfFloat[i] = param1Float1;
        this.mVtxCount = i = j + 1;
        arrayOfFloat[j] = param1Float2;
        this.mVtxCount = j = i + 1;
        arrayOfFloat[i] = param1Float3;
        this.mVtxCount = j + 1;
        arrayOfFloat[j] = 1.0F;
        latch();
        return this;
      } 
      throw new IllegalStateException("add mistmatch with declared components.");
    }
    
    public TriangleMeshBuilder setTexture(float param1Float1, float param1Float2) {
      if ((this.mFlags & 0x100) != 0) {
        this.mS0 = param1Float1;
        this.mT0 = param1Float2;
        return this;
      } 
      throw new IllegalStateException("add mistmatch with declared components.");
    }
    
    public TriangleMeshBuilder setNormal(float param1Float1, float param1Float2, float param1Float3) {
      if ((this.mFlags & 0x2) != 0) {
        this.mNX = param1Float1;
        this.mNY = param1Float2;
        this.mNZ = param1Float3;
        return this;
      } 
      throw new IllegalStateException("add mistmatch with declared components.");
    }
    
    public TriangleMeshBuilder setColor(float param1Float1, float param1Float2, float param1Float3, float param1Float4) {
      if ((this.mFlags & 0x1) != 0) {
        this.mR = param1Float1;
        this.mG = param1Float2;
        this.mB = param1Float3;
        this.mA = param1Float4;
        return this;
      } 
      throw new IllegalStateException("add mistmatch with declared components.");
    }
    
    public TriangleMeshBuilder addTriangle(int param1Int1, int param1Int2, int param1Int3) {
      int i = this.mMaxIndex;
      if (param1Int1 < i && param1Int1 >= 0 && param1Int2 < i && param1Int2 >= 0 && param1Int3 < i && param1Int3 >= 0) {
        i = this.mIndexCount;
        short[] arrayOfShort1 = this.mIndexData;
        if (i + 3 >= arrayOfShort1.length) {
          short[] arrayOfShort = new short[arrayOfShort1.length * 2];
          System.arraycopy(arrayOfShort1, 0, arrayOfShort, 0, arrayOfShort1.length);
          this.mIndexData = arrayOfShort;
        } 
        short[] arrayOfShort2 = this.mIndexData;
        int j = this.mIndexCount;
        this.mIndexCount = i = j + 1;
        arrayOfShort2[j] = (short)param1Int1;
        this.mIndexCount = param1Int1 = i + 1;
        arrayOfShort2[i] = (short)param1Int2;
        this.mIndexCount = param1Int1 + 1;
        arrayOfShort2[param1Int1] = (short)param1Int3;
        return this;
      } 
      throw new IllegalStateException("Index provided greater than vertex count.");
    }
    
    public Mesh create(boolean param1Boolean) {
      Element.Builder builder1 = new Element.Builder(this.mRS);
      builder1.add(Element.createVector(this.mRS, Element.DataType.FLOAT_32, this.mVtxSize), "position");
      if ((this.mFlags & 0x1) != 0)
        builder1.add(Element.F32_4(this.mRS), "color"); 
      if ((this.mFlags & 0x100) != 0)
        builder1.add(Element.F32_2(this.mRS), "texture0"); 
      if ((this.mFlags & 0x2) != 0)
        builder1.add(Element.F32_3(this.mRS), "normal"); 
      this.mElement = builder1.create();
      int i = 1;
      if (param1Boolean)
        i = 0x1 | 0x4; 
      Mesh.Builder builder = new Mesh.Builder(this.mRS, i);
      builder.addVertexType(this.mElement, this.mMaxIndex);
      builder.addIndexSetType(Element.U16(this.mRS), this.mIndexCount, Mesh.Primitive.TRIANGLE);
      Mesh mesh = builder.create();
      mesh.getVertexAllocation(0).copy1DRangeFromUnchecked(0, this.mMaxIndex, this.mVtxData);
      if (param1Boolean)
        mesh.getVertexAllocation(0).syncAll(1); 
      mesh.getIndexSetAllocation(0).copy1DRangeFromUnchecked(0, this.mIndexCount, this.mIndexData);
      if (param1Boolean)
        mesh.getIndexSetAllocation(0).syncAll(1); 
      return mesh;
    }
  }
}
