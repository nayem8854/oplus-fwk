package android.renderscript;

public class Float3 {
  public float x;
  
  public float y;
  
  public float z;
  
  public Float3() {}
  
  public Float3(Float3 paramFloat3) {
    this.x = paramFloat3.x;
    this.y = paramFloat3.y;
    this.z = paramFloat3.z;
  }
  
  public Float3(float paramFloat1, float paramFloat2, float paramFloat3) {
    this.x = paramFloat1;
    this.y = paramFloat2;
    this.z = paramFloat3;
  }
  
  public static Float3 add(Float3 paramFloat31, Float3 paramFloat32) {
    Float3 float3 = new Float3();
    paramFloat31.x += paramFloat32.x;
    paramFloat31.y += paramFloat32.y;
    paramFloat31.z += paramFloat32.z;
    return float3;
  }
  
  public void add(Float3 paramFloat3) {
    this.x += paramFloat3.x;
    this.y += paramFloat3.y;
    this.z += paramFloat3.z;
  }
  
  public void add(float paramFloat) {
    this.x += paramFloat;
    this.y += paramFloat;
    this.z += paramFloat;
  }
  
  public static Float3 add(Float3 paramFloat3, float paramFloat) {
    Float3 float3 = new Float3();
    paramFloat3.x += paramFloat;
    paramFloat3.y += paramFloat;
    paramFloat3.z += paramFloat;
    return float3;
  }
  
  public void sub(Float3 paramFloat3) {
    this.x -= paramFloat3.x;
    this.y -= paramFloat3.y;
    this.z -= paramFloat3.z;
  }
  
  public static Float3 sub(Float3 paramFloat31, Float3 paramFloat32) {
    Float3 float3 = new Float3();
    paramFloat31.x -= paramFloat32.x;
    paramFloat31.y -= paramFloat32.y;
    paramFloat31.z -= paramFloat32.z;
    return float3;
  }
  
  public void sub(float paramFloat) {
    this.x -= paramFloat;
    this.y -= paramFloat;
    this.z -= paramFloat;
  }
  
  public static Float3 sub(Float3 paramFloat3, float paramFloat) {
    Float3 float3 = new Float3();
    paramFloat3.x -= paramFloat;
    paramFloat3.y -= paramFloat;
    paramFloat3.z -= paramFloat;
    return float3;
  }
  
  public void mul(Float3 paramFloat3) {
    this.x *= paramFloat3.x;
    this.y *= paramFloat3.y;
    this.z *= paramFloat3.z;
  }
  
  public static Float3 mul(Float3 paramFloat31, Float3 paramFloat32) {
    Float3 float3 = new Float3();
    paramFloat31.x *= paramFloat32.x;
    paramFloat31.y *= paramFloat32.y;
    paramFloat31.z *= paramFloat32.z;
    return float3;
  }
  
  public void mul(float paramFloat) {
    this.x *= paramFloat;
    this.y *= paramFloat;
    this.z *= paramFloat;
  }
  
  public static Float3 mul(Float3 paramFloat3, float paramFloat) {
    Float3 float3 = new Float3();
    paramFloat3.x *= paramFloat;
    paramFloat3.y *= paramFloat;
    paramFloat3.z *= paramFloat;
    return float3;
  }
  
  public void div(Float3 paramFloat3) {
    this.x /= paramFloat3.x;
    this.y /= paramFloat3.y;
    this.z /= paramFloat3.z;
  }
  
  public static Float3 div(Float3 paramFloat31, Float3 paramFloat32) {
    Float3 float3 = new Float3();
    paramFloat31.x /= paramFloat32.x;
    paramFloat31.y /= paramFloat32.y;
    paramFloat31.z /= paramFloat32.z;
    return float3;
  }
  
  public void div(float paramFloat) {
    this.x /= paramFloat;
    this.y /= paramFloat;
    this.z /= paramFloat;
  }
  
  public static Float3 div(Float3 paramFloat3, float paramFloat) {
    Float3 float3 = new Float3();
    paramFloat3.x /= paramFloat;
    paramFloat3.y /= paramFloat;
    paramFloat3.z /= paramFloat;
    return float3;
  }
  
  public Float dotProduct(Float3 paramFloat3) {
    return new Float(this.x * paramFloat3.x + this.y * paramFloat3.y + this.z * paramFloat3.z);
  }
  
  public static Float dotProduct(Float3 paramFloat31, Float3 paramFloat32) {
    return new Float(paramFloat32.x * paramFloat31.x + paramFloat32.y * paramFloat31.y + paramFloat32.z * paramFloat31.z);
  }
  
  public void addMultiple(Float3 paramFloat3, float paramFloat) {
    this.x += paramFloat3.x * paramFloat;
    this.y += paramFloat3.y * paramFloat;
    this.z += paramFloat3.z * paramFloat;
  }
  
  public void set(Float3 paramFloat3) {
    this.x = paramFloat3.x;
    this.y = paramFloat3.y;
    this.z = paramFloat3.z;
  }
  
  public void negate() {
    this.x = -this.x;
    this.y = -this.y;
    this.z = -this.z;
  }
  
  public int length() {
    return 3;
  }
  
  public Float elementSum() {
    return new Float(this.x + this.y + this.z);
  }
  
  public float get(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt == 2)
          return this.z; 
        throw new IndexOutOfBoundsException("Index: i");
      } 
      return this.y;
    } 
    return this.x;
  }
  
  public void setAt(int paramInt, float paramFloat) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt == 2) {
          this.z = paramFloat;
          return;
        } 
        throw new IndexOutOfBoundsException("Index: i");
      } 
      this.y = paramFloat;
      return;
    } 
    this.x = paramFloat;
  }
  
  public void addAt(int paramInt, float paramFloat) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt == 2) {
          this.z += paramFloat;
          return;
        } 
        throw new IndexOutOfBoundsException("Index: i");
      } 
      this.y += paramFloat;
      return;
    } 
    this.x += paramFloat;
  }
  
  public void setValues(float paramFloat1, float paramFloat2, float paramFloat3) {
    this.x = paramFloat1;
    this.y = paramFloat2;
    this.z = paramFloat3;
  }
  
  public void copyTo(float[] paramArrayOffloat, int paramInt) {
    paramArrayOffloat[paramInt] = this.x;
    paramArrayOffloat[paramInt + 1] = this.y;
    paramArrayOffloat[paramInt + 2] = this.z;
  }
}
