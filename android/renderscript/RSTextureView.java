package android.renderscript;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.util.AttributeSet;
import android.view.TextureView;

public class RSTextureView extends TextureView implements TextureView.SurfaceTextureListener {
  private RenderScriptGL mRS;
  
  private SurfaceTexture mSurfaceTexture;
  
  public RSTextureView(Context paramContext) {
    super(paramContext);
    init();
  }
  
  public RSTextureView(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    init();
  }
  
  private void init() {
    setSurfaceTextureListener(this);
  }
  
  public void onSurfaceTextureAvailable(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2) {
    this.mSurfaceTexture = paramSurfaceTexture;
    RenderScriptGL renderScriptGL = this.mRS;
    if (renderScriptGL != null)
      renderScriptGL.setSurfaceTexture(paramSurfaceTexture, paramInt1, paramInt2); 
  }
  
  public void onSurfaceTextureSizeChanged(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2) {
    this.mSurfaceTexture = paramSurfaceTexture;
    RenderScriptGL renderScriptGL = this.mRS;
    if (renderScriptGL != null)
      renderScriptGL.setSurfaceTexture(paramSurfaceTexture, paramInt1, paramInt2); 
  }
  
  public boolean onSurfaceTextureDestroyed(SurfaceTexture paramSurfaceTexture) {
    this.mSurfaceTexture = paramSurfaceTexture;
    RenderScriptGL renderScriptGL = this.mRS;
    if (renderScriptGL != null)
      renderScriptGL.setSurfaceTexture((SurfaceTexture)null, 0, 0); 
    return true;
  }
  
  public void onSurfaceTextureUpdated(SurfaceTexture paramSurfaceTexture) {
    this.mSurfaceTexture = paramSurfaceTexture;
  }
  
  public void pause() {
    RenderScriptGL renderScriptGL = this.mRS;
    if (renderScriptGL != null)
      renderScriptGL.pause(); 
  }
  
  public void resume() {
    RenderScriptGL renderScriptGL = this.mRS;
    if (renderScriptGL != null)
      renderScriptGL.resume(); 
  }
  
  public RenderScriptGL createRenderScriptGL(RenderScriptGL.SurfaceConfig paramSurfaceConfig) {
    RenderScriptGL renderScriptGL = new RenderScriptGL(getContext(), paramSurfaceConfig);
    setRenderScriptGL(renderScriptGL);
    SurfaceTexture surfaceTexture = this.mSurfaceTexture;
    if (surfaceTexture != null)
      this.mRS.setSurfaceTexture(surfaceTexture, getWidth(), getHeight()); 
    return renderScriptGL;
  }
  
  public void destroyRenderScriptGL() {
    this.mRS.destroy();
    this.mRS = null;
  }
  
  public void setRenderScriptGL(RenderScriptGL paramRenderScriptGL) {
    this.mRS = paramRenderScriptGL;
    SurfaceTexture surfaceTexture = this.mSurfaceTexture;
    if (surfaceTexture != null)
      paramRenderScriptGL.setSurfaceTexture(surfaceTexture, getWidth(), getHeight()); 
  }
  
  public RenderScriptGL getRenderScriptGL() {
    return this.mRS;
  }
}
