package android.renderscript;

import android.content.res.Resources;

public class ScriptC extends Script {
  private static final String TAG = "ScriptC";
  
  protected ScriptC(int paramInt, RenderScript paramRenderScript) {
    super(paramInt, paramRenderScript);
  }
  
  protected ScriptC(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
  }
  
  protected ScriptC(RenderScript paramRenderScript, Resources paramResources, int paramInt) {
    super(0L, paramRenderScript);
    long l = internalCreate(paramRenderScript, paramResources, paramInt);
    if (l != 0L) {
      setID(l);
      return;
    } 
    throw new RSRuntimeException("Loading of ScriptC script failed.");
  }
  
  protected ScriptC(RenderScript paramRenderScript, String paramString, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) {
    super(0L, paramRenderScript);
    long l;
    if (RenderScript.sPointerSize == 4) {
      l = internalStringCreate(paramRenderScript, paramString, paramArrayOfbyte1);
    } else {
      l = internalStringCreate(paramRenderScript, paramString, paramArrayOfbyte2);
    } 
    if (l != 0L) {
      setID(l);
      return;
    } 
    throw new RSRuntimeException("Loading of ScriptC script failed.");
  }
  
  private static long internalCreate(RenderScript paramRenderScript, Resources paramResources, int paramInt) {
    // Byte code:
    //   0: ldc android/renderscript/ScriptC
    //   2: monitorenter
    //   3: aload_1
    //   4: iload_2
    //   5: invokevirtual openRawResource : (I)Ljava/io/InputStream;
    //   8: astore_3
    //   9: sipush #1024
    //   12: newarray byte
    //   14: astore #4
    //   16: iconst_0
    //   17: istore #5
    //   19: aload #4
    //   21: arraylength
    //   22: iload #5
    //   24: isub
    //   25: istore #6
    //   27: aload #4
    //   29: astore #7
    //   31: iload #6
    //   33: istore #8
    //   35: iload #6
    //   37: ifne -> 69
    //   40: aload #4
    //   42: arraylength
    //   43: iconst_2
    //   44: imul
    //   45: newarray byte
    //   47: astore #7
    //   49: aload #4
    //   51: iconst_0
    //   52: aload #7
    //   54: iconst_0
    //   55: aload #4
    //   57: arraylength
    //   58: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   61: aload #7
    //   63: arraylength
    //   64: iload #5
    //   66: isub
    //   67: istore #8
    //   69: aload_3
    //   70: aload #7
    //   72: iload #5
    //   74: iload #8
    //   76: invokevirtual read : ([BII)I
    //   79: istore #8
    //   81: iload #8
    //   83: ifgt -> 116
    //   86: aload_3
    //   87: invokevirtual close : ()V
    //   90: aload_1
    //   91: iload_2
    //   92: invokevirtual getResourceEntryName : (I)Ljava/lang/String;
    //   95: astore_1
    //   96: aload_0
    //   97: aload_1
    //   98: invokestatic getCachePath : ()Ljava/lang/String;
    //   101: aload #7
    //   103: iload #5
    //   105: invokevirtual nScriptCCreate : (Ljava/lang/String;Ljava/lang/String;[BI)J
    //   108: lstore #9
    //   110: ldc android/renderscript/ScriptC
    //   112: monitorexit
    //   113: lload #9
    //   115: lreturn
    //   116: iload #5
    //   118: iload #8
    //   120: iadd
    //   121: istore #5
    //   123: aload #7
    //   125: astore #4
    //   127: goto -> 19
    //   130: astore_0
    //   131: aload_3
    //   132: invokevirtual close : ()V
    //   135: aload_0
    //   136: athrow
    //   137: astore_0
    //   138: new android/content/res/Resources$NotFoundException
    //   141: astore_0
    //   142: aload_0
    //   143: invokespecial <init> : ()V
    //   146: aload_0
    //   147: athrow
    //   148: astore_0
    //   149: ldc android/renderscript/ScriptC
    //   151: monitorexit
    //   152: aload_0
    //   153: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #90	-> 3
    //   #93	-> 9
    //   #94	-> 16
    //   #96	-> 19
    //   #97	-> 27
    //   #98	-> 40
    //   #99	-> 49
    //   #100	-> 61
    //   #101	-> 61
    //   #103	-> 69
    //   #104	-> 81
    //   #105	-> 86
    //   #110	-> 86
    //   #111	-> 90
    //   #114	-> 90
    //   #116	-> 90
    //   #119	-> 96
    //   #107	-> 116
    //   #108	-> 123
    //   #110	-> 130
    //   #111	-> 135
    //   #112	-> 137
    //   #113	-> 138
    //   #89	-> 148
    // Exception table:
    //   from	to	target	type
    //   3	9	148	finally
    //   9	16	130	finally
    //   19	27	130	finally
    //   40	49	130	finally
    //   49	61	130	finally
    //   61	69	130	finally
    //   69	81	130	finally
    //   86	90	137	java/io/IOException
    //   86	90	148	finally
    //   90	96	148	finally
    //   96	110	148	finally
    //   131	135	137	java/io/IOException
    //   131	135	148	finally
    //   135	137	137	java/io/IOException
    //   135	137	148	finally
    //   138	148	148	finally
  }
  
  private static long internalStringCreate(RenderScript paramRenderScript, String paramString, byte[] paramArrayOfbyte) {
    // Byte code:
    //   0: ldc android/renderscript/ScriptC
    //   2: monitorenter
    //   3: aload_0
    //   4: aload_1
    //   5: invokestatic getCachePath : ()Ljava/lang/String;
    //   8: aload_2
    //   9: aload_2
    //   10: arraylength
    //   11: invokevirtual nScriptCCreate : (Ljava/lang/String;Ljava/lang/String;[BI)J
    //   14: lstore_3
    //   15: ldc android/renderscript/ScriptC
    //   17: monitorexit
    //   18: lload_3
    //   19: lreturn
    //   20: astore_0
    //   21: ldc android/renderscript/ScriptC
    //   23: monitorexit
    //   24: aload_0
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #124	-> 3
    //   #124	-> 20
    // Exception table:
    //   from	to	target	type
    //   3	15	20	finally
  }
}
