package android.renderscript;

public class Short2 {
  public short x;
  
  public short y;
  
  public Short2() {}
  
  public Short2(short paramShort) {
    this.y = paramShort;
    this.x = paramShort;
  }
  
  public Short2(short paramShort1, short paramShort2) {
    this.x = paramShort1;
    this.y = paramShort2;
  }
  
  public Short2(Short2 paramShort2) {
    this.x = paramShort2.x;
    this.y = paramShort2.y;
  }
  
  public void add(Short2 paramShort2) {
    this.x = (short)(this.x + paramShort2.x);
    this.y = (short)(this.y + paramShort2.y);
  }
  
  public static Short2 add(Short2 paramShort21, Short2 paramShort22) {
    Short2 short2 = new Short2();
    short2.x = (short)(paramShort21.x + paramShort22.x);
    short2.y = (short)(paramShort21.y + paramShort22.y);
    return short2;
  }
  
  public void add(short paramShort) {
    this.x = (short)(this.x + paramShort);
    this.y = (short)(this.y + paramShort);
  }
  
  public static Short2 add(Short2 paramShort2, short paramShort) {
    Short2 short2 = new Short2();
    short2.x = (short)(paramShort2.x + paramShort);
    short2.y = (short)(paramShort2.y + paramShort);
    return short2;
  }
  
  public void sub(Short2 paramShort2) {
    this.x = (short)(this.x - paramShort2.x);
    this.y = (short)(this.y - paramShort2.y);
  }
  
  public static Short2 sub(Short2 paramShort21, Short2 paramShort22) {
    Short2 short2 = new Short2();
    short2.x = (short)(paramShort21.x - paramShort22.x);
    short2.y = (short)(paramShort21.y - paramShort22.y);
    return short2;
  }
  
  public void sub(short paramShort) {
    this.x = (short)(this.x - paramShort);
    this.y = (short)(this.y - paramShort);
  }
  
  public static Short2 sub(Short2 paramShort2, short paramShort) {
    Short2 short2 = new Short2();
    short2.x = (short)(paramShort2.x - paramShort);
    short2.y = (short)(paramShort2.y - paramShort);
    return short2;
  }
  
  public void mul(Short2 paramShort2) {
    this.x = (short)(this.x * paramShort2.x);
    this.y = (short)(this.y * paramShort2.y);
  }
  
  public static Short2 mul(Short2 paramShort21, Short2 paramShort22) {
    Short2 short2 = new Short2();
    short2.x = (short)(paramShort21.x * paramShort22.x);
    short2.y = (short)(paramShort21.y * paramShort22.y);
    return short2;
  }
  
  public void mul(short paramShort) {
    this.x = (short)(this.x * paramShort);
    this.y = (short)(this.y * paramShort);
  }
  
  public static Short2 mul(Short2 paramShort2, short paramShort) {
    Short2 short2 = new Short2();
    short2.x = (short)(paramShort2.x * paramShort);
    short2.y = (short)(paramShort2.y * paramShort);
    return short2;
  }
  
  public void div(Short2 paramShort2) {
    this.x = (short)(this.x / paramShort2.x);
    this.y = (short)(this.y / paramShort2.y);
  }
  
  public static Short2 div(Short2 paramShort21, Short2 paramShort22) {
    Short2 short2 = new Short2();
    short2.x = (short)(paramShort21.x / paramShort22.x);
    short2.y = (short)(paramShort21.y / paramShort22.y);
    return short2;
  }
  
  public void div(short paramShort) {
    this.x = (short)(this.x / paramShort);
    this.y = (short)(this.y / paramShort);
  }
  
  public static Short2 div(Short2 paramShort2, short paramShort) {
    Short2 short2 = new Short2();
    short2.x = (short)(paramShort2.x / paramShort);
    short2.y = (short)(paramShort2.y / paramShort);
    return short2;
  }
  
  public void mod(Short2 paramShort2) {
    this.x = (short)(this.x % paramShort2.x);
    this.y = (short)(this.y % paramShort2.y);
  }
  
  public static Short2 mod(Short2 paramShort21, Short2 paramShort22) {
    Short2 short2 = new Short2();
    short2.x = (short)(paramShort21.x % paramShort22.x);
    short2.y = (short)(paramShort21.y % paramShort22.y);
    return short2;
  }
  
  public void mod(short paramShort) {
    this.x = (short)(this.x % paramShort);
    this.y = (short)(this.y % paramShort);
  }
  
  public static Short2 mod(Short2 paramShort2, short paramShort) {
    Short2 short2 = new Short2();
    short2.x = (short)(paramShort2.x % paramShort);
    short2.y = (short)(paramShort2.y % paramShort);
    return short2;
  }
  
  public short length() {
    return 2;
  }
  
  public void negate() {
    this.x = (short)-this.x;
    this.y = (short)-this.y;
  }
  
  public short dotProduct(Short2 paramShort2) {
    return (short)(this.x * paramShort2.x + this.y * paramShort2.y);
  }
  
  public static short dotProduct(Short2 paramShort21, Short2 paramShort22) {
    return (short)(paramShort22.x * paramShort21.x + paramShort22.y * paramShort21.y);
  }
  
  public void addMultiple(Short2 paramShort2, short paramShort) {
    this.x = (short)(this.x + paramShort2.x * paramShort);
    this.y = (short)(this.y + paramShort2.y * paramShort);
  }
  
  public void set(Short2 paramShort2) {
    this.x = paramShort2.x;
    this.y = paramShort2.y;
  }
  
  public void setValues(short paramShort1, short paramShort2) {
    this.x = paramShort1;
    this.y = paramShort2;
  }
  
  public short elementSum() {
    return (short)(this.x + this.y);
  }
  
  public short get(int paramInt) {
    if (paramInt != 0) {
      if (paramInt == 1)
        return this.y; 
      throw new IndexOutOfBoundsException("Index: i");
    } 
    return this.x;
  }
  
  public void setAt(int paramInt, short paramShort) {
    if (paramInt != 0) {
      if (paramInt == 1) {
        this.y = paramShort;
        return;
      } 
      throw new IndexOutOfBoundsException("Index: i");
    } 
    this.x = paramShort;
  }
  
  public void addAt(int paramInt, short paramShort) {
    if (paramInt != 0) {
      if (paramInt == 1) {
        this.y = (short)(this.y + paramShort);
        return;
      } 
      throw new IndexOutOfBoundsException("Index: i");
    } 
    this.x = (short)(this.x + paramShort);
  }
  
  public void copyTo(short[] paramArrayOfshort, int paramInt) {
    paramArrayOfshort[paramInt] = this.x;
    paramArrayOfshort[paramInt + 1] = this.y;
  }
}
