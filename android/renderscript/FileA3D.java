package android.renderscript;

import android.content.res.AssetManager;
import android.content.res.Resources;
import java.io.File;
import java.io.InputStream;

public class FileA3D extends BaseObj {
  IndexEntry[] mFileEntries;
  
  InputStream mInputStream;
  
  class EntryType extends Enum<EntryType> {
    private static final EntryType[] $VALUES;
    
    public static final EntryType MESH;
    
    public static EntryType valueOf(String param1String) {
      return Enum.<EntryType>valueOf(EntryType.class, param1String);
    }
    
    public static EntryType[] values() {
      return (EntryType[])$VALUES.clone();
    }
    
    public static final EntryType UNKNOWN = new EntryType("UNKNOWN", 0, 0);
    
    int mID;
    
    static {
      EntryType entryType = new EntryType("MESH", 1, 1);
      $VALUES = new EntryType[] { UNKNOWN, entryType };
    }
    
    private EntryType(FileA3D this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.mID = param1Int2;
    }
    
    static EntryType toEntryType(int param1Int) {
      return values()[param1Int];
    }
  }
  
  class IndexEntry {
    FileA3D.EntryType mEntryType;
    
    long mID;
    
    int mIndex;
    
    BaseObj mLoadedObj;
    
    String mName;
    
    RenderScript mRS;
    
    public String getName() {
      return this.mName;
    }
    
    public FileA3D.EntryType getEntryType() {
      return this.mEntryType;
    }
    
    public BaseObj getObject() {
      this.mRS.validate();
      return internalCreate(this.mRS, this);
    }
    
    public Mesh getMesh() {
      return (Mesh)getObject();
    }
    
    static BaseObj internalCreate(RenderScript param1RenderScript, IndexEntry param1IndexEntry) {
      // Byte code:
      //   0: ldc android/renderscript/FileA3D$IndexEntry
      //   2: monitorenter
      //   3: aload_1
      //   4: getfield mLoadedObj : Landroid/renderscript/BaseObj;
      //   7: ifnull -> 20
      //   10: aload_1
      //   11: getfield mLoadedObj : Landroid/renderscript/BaseObj;
      //   14: astore_0
      //   15: ldc android/renderscript/FileA3D$IndexEntry
      //   17: monitorexit
      //   18: aload_0
      //   19: areturn
      //   20: aload_1
      //   21: getfield mEntryType : Landroid/renderscript/FileA3D$EntryType;
      //   24: astore_2
      //   25: getstatic android/renderscript/FileA3D$EntryType.UNKNOWN : Landroid/renderscript/FileA3D$EntryType;
      //   28: astore_3
      //   29: aload_2
      //   30: aload_3
      //   31: if_acmpne -> 39
      //   34: ldc android/renderscript/FileA3D$IndexEntry
      //   36: monitorexit
      //   37: aconst_null
      //   38: areturn
      //   39: aload_0
      //   40: aload_1
      //   41: getfield mID : J
      //   44: aload_1
      //   45: getfield mIndex : I
      //   48: invokevirtual nFileA3DGetEntryByIndex : (JI)J
      //   51: lstore #4
      //   53: lload #4
      //   55: lconst_0
      //   56: lcmp
      //   57: ifne -> 65
      //   60: ldc android/renderscript/FileA3D$IndexEntry
      //   62: monitorexit
      //   63: aconst_null
      //   64: areturn
      //   65: getstatic android/renderscript/FileA3D$1.$SwitchMap$android$renderscript$FileA3D$EntryType : [I
      //   68: aload_1
      //   69: getfield mEntryType : Landroid/renderscript/FileA3D$EntryType;
      //   72: invokevirtual ordinal : ()I
      //   75: iaload
      //   76: iconst_1
      //   77: if_icmpne -> 110
      //   80: new android/renderscript/Mesh
      //   83: astore_3
      //   84: aload_3
      //   85: lload #4
      //   87: aload_0
      //   88: invokespecial <init> : (JLandroid/renderscript/RenderScript;)V
      //   91: aload_1
      //   92: aload_3
      //   93: putfield mLoadedObj : Landroid/renderscript/BaseObj;
      //   96: aload_3
      //   97: invokevirtual updateFromNative : ()V
      //   100: aload_1
      //   101: getfield mLoadedObj : Landroid/renderscript/BaseObj;
      //   104: astore_0
      //   105: ldc android/renderscript/FileA3D$IndexEntry
      //   107: monitorexit
      //   108: aload_0
      //   109: areturn
      //   110: new android/renderscript/RSRuntimeException
      //   113: astore_0
      //   114: aload_0
      //   115: ldc 'Unrecognized object type in file.'
      //   117: invokespecial <init> : (Ljava/lang/String;)V
      //   120: aload_0
      //   121: athrow
      //   122: astore_0
      //   123: ldc android/renderscript/FileA3D$IndexEntry
      //   125: monitorexit
      //   126: aload_0
      //   127: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #134	-> 3
      //   #135	-> 10
      //   #139	-> 20
      //   #140	-> 34
      //   #143	-> 39
      //   #144	-> 53
      //   #145	-> 60
      //   #148	-> 65
      //   #150	-> 80
      //   #151	-> 96
      //   #157	-> 96
      //   #158	-> 100
      //   #154	-> 110
      //   #133	-> 122
      // Exception table:
      //   from	to	target	type
      //   3	10	122	finally
      //   10	15	122	finally
      //   20	29	122	finally
      //   39	53	122	finally
      //   65	80	122	finally
      //   80	96	122	finally
      //   96	100	122	finally
      //   100	105	122	finally
      //   110	122	122	finally
    }
    
    IndexEntry(FileA3D this$0, int param1Int, long param1Long, String param1String, FileA3D.EntryType param1EntryType) {
      this.mRS = (RenderScript)this$0;
      this.mIndex = param1Int;
      this.mID = param1Long;
      this.mName = param1String;
      this.mEntryType = param1EntryType;
      this.mLoadedObj = null;
    }
  }
  
  FileA3D(long paramLong, RenderScript paramRenderScript, InputStream paramInputStream) {
    super(paramLong, paramRenderScript);
    this.mInputStream = paramInputStream;
    this.guard.open("destroy");
  }
  
  private void initEntries() {
    int i = this.mRS.nFileA3DGetNumIndexEntries(getID(this.mRS));
    if (i <= 0)
      return; 
    this.mFileEntries = new IndexEntry[i];
    int[] arrayOfInt = new int[i];
    String[] arrayOfString = new String[i];
    this.mRS.nFileA3DGetIndexEntries(getID(this.mRS), i, arrayOfInt, arrayOfString);
    for (byte b = 0; b < i; b++)
      this.mFileEntries[b] = new IndexEntry(this.mRS, b, getID(this.mRS), arrayOfString[b], EntryType.toEntryType(arrayOfInt[b])); 
  }
  
  public int getIndexEntryCount() {
    IndexEntry[] arrayOfIndexEntry = this.mFileEntries;
    if (arrayOfIndexEntry == null)
      return 0; 
    return arrayOfIndexEntry.length;
  }
  
  public IndexEntry getIndexEntry(int paramInt) {
    if (getIndexEntryCount() != 0 && paramInt >= 0) {
      IndexEntry[] arrayOfIndexEntry = this.mFileEntries;
      if (paramInt < arrayOfIndexEntry.length)
        return arrayOfIndexEntry[paramInt]; 
    } 
    return null;
  }
  
  public static FileA3D createFromAsset(RenderScript paramRenderScript, AssetManager paramAssetManager, String paramString) {
    paramRenderScript.validate();
    long l = paramRenderScript.nFileA3DCreateFromAsset(paramAssetManager, paramString);
    if (l != 0L) {
      FileA3D fileA3D = new FileA3D(l, paramRenderScript, null);
      fileA3D.initEntries();
      return fileA3D;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unable to create a3d file from asset ");
    stringBuilder.append(paramString);
    throw new RSRuntimeException(stringBuilder.toString());
  }
  
  public static FileA3D createFromFile(RenderScript paramRenderScript, String paramString) {
    long l = paramRenderScript.nFileA3DCreateFromFile(paramString);
    if (l != 0L) {
      FileA3D fileA3D = new FileA3D(l, paramRenderScript, null);
      fileA3D.initEntries();
      return fileA3D;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unable to create a3d file from ");
    stringBuilder.append(paramString);
    throw new RSRuntimeException(stringBuilder.toString());
  }
  
  public static FileA3D createFromFile(RenderScript paramRenderScript, File paramFile) {
    return createFromFile(paramRenderScript, paramFile.getAbsolutePath());
  }
  
  public static FileA3D createFromResource(RenderScript paramRenderScript, Resources paramResources, int paramInt) {
    paramRenderScript.validate();
    try {
      InputStream inputStream = paramResources.openRawResource(paramInt);
      if (inputStream instanceof AssetManager.AssetInputStream) {
        long l = ((AssetManager.AssetInputStream)inputStream).getNativeAsset();
        l = paramRenderScript.nFileA3DCreateFromAssetStream(l);
        if (l != 0L) {
          FileA3D fileA3D = new FileA3D(l, paramRenderScript, inputStream);
          fileA3D.initEntries();
          return fileA3D;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unable to create a3d file from resource ");
        stringBuilder.append(paramInt);
        throw new RSRuntimeException(stringBuilder.toString());
      } 
      throw new RSRuntimeException("Unsupported asset stream");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to open resource ");
      stringBuilder.append(paramInt);
      throw new RSRuntimeException(stringBuilder.toString());
    } 
  }
}
