package android.renderscript;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class RSSurfaceView extends SurfaceView implements SurfaceHolder.Callback {
  private RenderScriptGL mRS;
  
  private SurfaceHolder mSurfaceHolder;
  
  public RSSurfaceView(Context paramContext) {
    super(paramContext);
    init();
  }
  
  public RSSurfaceView(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    init();
  }
  
  private void init() {
    SurfaceHolder surfaceHolder = getHolder();
    surfaceHolder.addCallback(this);
  }
  
  public void surfaceCreated(SurfaceHolder paramSurfaceHolder) {
    this.mSurfaceHolder = paramSurfaceHolder;
  }
  
  public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mRS : Landroid/renderscript/RenderScriptGL;
    //   6: ifnull -> 19
    //   9: aload_0
    //   10: getfield mRS : Landroid/renderscript/RenderScriptGL;
    //   13: aconst_null
    //   14: iconst_0
    //   15: iconst_0
    //   16: invokevirtual setSurface : (Landroid/view/SurfaceHolder;II)V
    //   19: aload_0
    //   20: monitorexit
    //   21: return
    //   22: astore_1
    //   23: aload_0
    //   24: monitorexit
    //   25: aload_1
    //   26: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #88	-> 0
    //   #90	-> 2
    //   #91	-> 9
    //   #93	-> 19
    //   #94	-> 21
    //   #93	-> 22
    // Exception table:
    //   from	to	target	type
    //   2	9	22	finally
    //   9	19	22	finally
    //   19	21	22	finally
    //   23	25	22	finally
  }
  
  public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mRS : Landroid/renderscript/RenderScriptGL;
    //   6: ifnull -> 20
    //   9: aload_0
    //   10: getfield mRS : Landroid/renderscript/RenderScriptGL;
    //   13: aload_1
    //   14: iload_3
    //   15: iload #4
    //   17: invokevirtual setSurface : (Landroid/view/SurfaceHolder;II)V
    //   20: aload_0
    //   21: monitorexit
    //   22: return
    //   23: astore_1
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_1
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #102	-> 0
    //   #103	-> 2
    //   #104	-> 9
    //   #106	-> 20
    //   #107	-> 22
    //   #106	-> 23
    // Exception table:
    //   from	to	target	type
    //   2	9	23	finally
    //   9	20	23	finally
    //   20	22	23	finally
    //   24	26	23	finally
  }
  
  public void pause() {
    RenderScriptGL renderScriptGL = this.mRS;
    if (renderScriptGL != null)
      renderScriptGL.pause(); 
  }
  
  public void resume() {
    RenderScriptGL renderScriptGL = this.mRS;
    if (renderScriptGL != null)
      renderScriptGL.resume(); 
  }
  
  public RenderScriptGL createRenderScriptGL(RenderScriptGL.SurfaceConfig paramSurfaceConfig) {
    RenderScriptGL renderScriptGL = new RenderScriptGL(getContext(), paramSurfaceConfig);
    setRenderScriptGL(renderScriptGL);
    return renderScriptGL;
  }
  
  public void destroyRenderScriptGL() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mRS : Landroid/renderscript/RenderScriptGL;
    //   6: invokevirtual destroy : ()V
    //   9: aload_0
    //   10: aconst_null
    //   11: putfield mRS : Landroid/renderscript/RenderScriptGL;
    //   14: aload_0
    //   15: monitorexit
    //   16: return
    //   17: astore_1
    //   18: aload_0
    //   19: monitorexit
    //   20: aload_1
    //   21: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #149	-> 0
    //   #150	-> 2
    //   #151	-> 9
    //   #152	-> 14
    //   #153	-> 16
    //   #152	-> 17
    // Exception table:
    //   from	to	target	type
    //   2	9	17	finally
    //   9	14	17	finally
    //   14	16	17	finally
    //   18	20	17	finally
  }
  
  public void setRenderScriptGL(RenderScriptGL paramRenderScriptGL) {
    this.mRS = paramRenderScriptGL;
  }
  
  public RenderScriptGL getRenderScriptGL() {
    return this.mRS;
  }
}
