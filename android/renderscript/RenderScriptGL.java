package android.renderscript;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.view.Surface;
import android.view.SurfaceHolder;

public class RenderScriptGL extends RenderScript {
  int mHeight;
  
  SurfaceConfig mSurfaceConfig;
  
  int mWidth;
  
  class SurfaceConfig {
    int mDepthMin = 0;
    
    int mDepthPref = 0;
    
    int mStencilMin = 0;
    
    int mStencilPref = 0;
    
    int mColorMin = 8;
    
    int mColorPref = 8;
    
    int mAlphaMin = 0;
    
    int mAlphaPref = 0;
    
    int mSamplesMin = 1;
    
    int mSamplesPref = 1;
    
    float mSamplesQ = 1.0F;
    
    public SurfaceConfig(RenderScriptGL this$0) {
      this.mDepthMin = ((SurfaceConfig)this$0).mDepthMin;
      this.mDepthPref = ((SurfaceConfig)this$0).mDepthPref;
      this.mStencilMin = ((SurfaceConfig)this$0).mStencilMin;
      this.mStencilPref = ((SurfaceConfig)this$0).mStencilPref;
      this.mColorMin = ((SurfaceConfig)this$0).mColorMin;
      this.mColorPref = ((SurfaceConfig)this$0).mColorPref;
      this.mAlphaMin = ((SurfaceConfig)this$0).mAlphaMin;
      this.mAlphaPref = ((SurfaceConfig)this$0).mAlphaPref;
      this.mSamplesMin = ((SurfaceConfig)this$0).mSamplesMin;
      this.mSamplesPref = ((SurfaceConfig)this$0).mSamplesPref;
      this.mSamplesQ = ((SurfaceConfig)this$0).mSamplesQ;
    }
    
    private void validateRange(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      if (param1Int1 >= param1Int3 && param1Int1 <= param1Int4) {
        if (param1Int2 >= param1Int1)
          return; 
        throw new RSIllegalArgumentException("preferred must be >= Minimum.");
      } 
      throw new RSIllegalArgumentException("Minimum value provided out of range.");
    }
    
    public void setColor(int param1Int1, int param1Int2) {
      validateRange(param1Int1, param1Int2, 5, 8);
      this.mColorMin = param1Int1;
      this.mColorPref = param1Int2;
    }
    
    public void setAlpha(int param1Int1, int param1Int2) {
      validateRange(param1Int1, param1Int2, 0, 8);
      this.mAlphaMin = param1Int1;
      this.mAlphaPref = param1Int2;
    }
    
    public void setDepth(int param1Int1, int param1Int2) {
      validateRange(param1Int1, param1Int2, 0, 24);
      this.mDepthMin = param1Int1;
      this.mDepthPref = param1Int2;
    }
    
    public void setSamples(int param1Int1, int param1Int2, float param1Float) {
      validateRange(param1Int1, param1Int2, 1, 32);
      if (param1Float >= 0.0F && param1Float <= 1.0F) {
        this.mSamplesMin = param1Int1;
        this.mSamplesPref = param1Int2;
        this.mSamplesQ = param1Float;
        return;
      } 
      throw new RSIllegalArgumentException("Quality out of 0-1 range.");
    }
    
    public SurfaceConfig() {}
  }
  
  public RenderScriptGL(Context paramContext, SurfaceConfig paramSurfaceConfig) {
    super(paramContext);
    this.mSurfaceConfig = new SurfaceConfig(paramSurfaceConfig);
    int i = (paramContext.getApplicationInfo()).targetSdkVersion;
    this.mWidth = 0;
    this.mHeight = 0;
    long l = nDeviceCreate();
    int j = (paramContext.getResources().getDisplayMetrics()).densityDpi;
    this.mContext = nContextCreateGL(l, 0, i, this.mSurfaceConfig.mColorMin, this.mSurfaceConfig.mColorPref, this.mSurfaceConfig.mAlphaMin, this.mSurfaceConfig.mAlphaPref, this.mSurfaceConfig.mDepthMin, this.mSurfaceConfig.mDepthPref, this.mSurfaceConfig.mStencilMin, this.mSurfaceConfig.mStencilPref, this.mSurfaceConfig.mSamplesMin, this.mSurfaceConfig.mSamplesPref, this.mSurfaceConfig.mSamplesQ, j);
    if (this.mContext != 0L) {
      this.mMessageThread = new RenderScript.MessageThread(this);
      this.mMessageThread.start();
      return;
    } 
    throw new RSDriverException("Failed to create RS context.");
  }
  
  public void setSurface(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2) {
    validate();
    Surface surface = null;
    if (paramSurfaceHolder != null)
      surface = paramSurfaceHolder.getSurface(); 
    this.mWidth = paramInt1;
    this.mHeight = paramInt2;
    nContextSetSurface(paramInt1, paramInt2, surface);
  }
  
  public void setSurfaceTexture(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2) {
    validate();
    Surface surface = null;
    if (paramSurfaceTexture != null)
      surface = new Surface(paramSurfaceTexture); 
    this.mWidth = paramInt1;
    this.mHeight = paramInt2;
    nContextSetSurface(paramInt1, paramInt2, surface);
  }
  
  public int getHeight() {
    return this.mHeight;
  }
  
  public int getWidth() {
    return this.mWidth;
  }
  
  public void pause() {
    validate();
    nContextPause();
  }
  
  public void resume() {
    validate();
    nContextResume();
  }
  
  public void bindRootScript(Script paramScript) {
    validate();
    nContextBindRootScript((int)safeID(paramScript));
  }
  
  public void bindProgramStore(ProgramStore paramProgramStore) {
    validate();
    nContextBindProgramStore((int)safeID(paramProgramStore));
  }
  
  public void bindProgramFragment(ProgramFragment paramProgramFragment) {
    validate();
    nContextBindProgramFragment((int)safeID(paramProgramFragment));
  }
  
  public void bindProgramRaster(ProgramRaster paramProgramRaster) {
    validate();
    nContextBindProgramRaster((int)safeID(paramProgramRaster));
  }
  
  public void bindProgramVertex(ProgramVertex paramProgramVertex) {
    validate();
    nContextBindProgramVertex((int)safeID(paramProgramVertex));
  }
}
