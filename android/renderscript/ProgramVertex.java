package android.renderscript;

public class ProgramVertex extends Program {
  ProgramVertex(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
  }
  
  public int getInputCount() {
    boolean bool;
    if (this.mInputs != null) {
      bool = this.mInputs.length;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public Element getInput(int paramInt) {
    if (paramInt >= 0 && paramInt < this.mInputs.length)
      return this.mInputs[paramInt]; 
    throw new IllegalArgumentException("Slot ID out of range.");
  }
  
  class Builder extends Program.BaseProgramBuilder {
    public Builder(ProgramVertex this$0) {
      super((RenderScript)this$0);
    }
    
    public Builder addInput(Element param1Element) throws IllegalStateException {
      if (this.mInputCount < 8) {
        if (!param1Element.isComplex()) {
          Element[] arrayOfElement = this.mInputs;
          int i = this.mInputCount;
          this.mInputCount = i + 1;
          arrayOfElement[i] = param1Element;
          return this;
        } 
        throw new RSIllegalArgumentException("Complex elements not allowed.");
      } 
      throw new RSIllegalArgumentException("Max input count exceeded.");
    }
    
    public ProgramVertex create() {
      this.mRS.validate();
      long[] arrayOfLong = new long[(this.mInputCount + this.mOutputCount + this.mConstantCount + this.mTextureCount) * 2];
      String[] arrayOfString = new String[this.mTextureCount];
      int i = 0;
      byte b;
      for (b = 0; b < this.mInputCount; b++) {
        int j = i + 1;
        arrayOfLong[i] = Program.ProgramParam.INPUT.mID;
        i = j + 1;
        arrayOfLong[j] = this.mInputs[b].getID(this.mRS);
      } 
      for (b = 0; b < this.mOutputCount; b++) {
        int j = i + 1;
        arrayOfLong[i] = Program.ProgramParam.OUTPUT.mID;
        i = j + 1;
        arrayOfLong[j] = this.mOutputs[b].getID(this.mRS);
      } 
      for (b = 0; b < this.mConstantCount; b++) {
        int j = i + 1;
        arrayOfLong[i] = Program.ProgramParam.CONSTANT.mID;
        i = j + 1;
        arrayOfLong[j] = this.mConstants[b].getID(this.mRS);
      } 
      for (b = 0; b < this.mTextureCount; b++) {
        int j = i + 1;
        arrayOfLong[i] = Program.ProgramParam.TEXTURE_TYPE.mID;
        i = j + 1;
        arrayOfLong[j] = (this.mTextureTypes[b]).mID;
        arrayOfString[b] = this.mTextureNames[b];
      } 
      long l = this.mRS.nProgramVertexCreate(this.mShader, arrayOfString, arrayOfLong);
      ProgramVertex programVertex = new ProgramVertex(l, this.mRS);
      initProgram(programVertex);
      return programVertex;
    }
  }
}
