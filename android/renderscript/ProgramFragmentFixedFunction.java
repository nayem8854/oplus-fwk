package android.renderscript;

public class ProgramFragmentFixedFunction extends ProgramFragment {
  ProgramFragmentFixedFunction(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
  }
  
  class InternalBuilder extends Program.BaseProgramBuilder {
    public InternalBuilder(ProgramFragmentFixedFunction this$0) {
      super((RenderScript)this$0);
    }
    
    public ProgramFragmentFixedFunction create() {
      this.mRS.validate();
      long[] arrayOfLong = new long[(this.mInputCount + this.mOutputCount + this.mConstantCount + this.mTextureCount) * 2];
      String[] arrayOfString = new String[this.mTextureCount];
      int i = 0;
      byte b;
      for (b = 0; b < this.mInputCount; b++) {
        int j = i + 1;
        arrayOfLong[i] = Program.ProgramParam.INPUT.mID;
        i = j + 1;
        arrayOfLong[j] = this.mInputs[b].getID(this.mRS);
      } 
      for (b = 0; b < this.mOutputCount; b++) {
        int j = i + 1;
        arrayOfLong[i] = Program.ProgramParam.OUTPUT.mID;
        i = j + 1;
        arrayOfLong[j] = this.mOutputs[b].getID(this.mRS);
      } 
      for (b = 0; b < this.mConstantCount; b++) {
        int j = i + 1;
        arrayOfLong[i] = Program.ProgramParam.CONSTANT.mID;
        i = j + 1;
        arrayOfLong[j] = this.mConstants[b].getID(this.mRS);
      } 
      for (b = 0; b < this.mTextureCount; b++) {
        int j = i + 1;
        arrayOfLong[i] = Program.ProgramParam.TEXTURE_TYPE.mID;
        i = j + 1;
        arrayOfLong[j] = (this.mTextureTypes[b]).mID;
        arrayOfString[b] = this.mTextureNames[b];
      } 
      long l = this.mRS.nProgramFragmentCreate(this.mShader, arrayOfString, arrayOfLong);
      ProgramFragmentFixedFunction programFragmentFixedFunction = new ProgramFragmentFixedFunction(l, this.mRS);
      initProgram(programFragmentFixedFunction);
      return programFragmentFixedFunction;
    }
  }
  
  class Builder {
    public static final int MAX_TEXTURE = 2;
    
    int mNumTextures;
    
    boolean mPointSpriteEnable;
    
    RenderScript mRS;
    
    String mShader;
    
    Slot[] mSlots;
    
    boolean mVaryingColorEnable;
    
    public enum EnvMode {
      DECAL,
      MODULATE,
      REPLACE(1);
      
      private static final EnvMode[] $VALUES;
      
      int mID;
      
      static {
        EnvMode envMode = new EnvMode("DECAL", 2, 3);
        $VALUES = new EnvMode[] { REPLACE, MODULATE, envMode };
      }
      
      EnvMode(int param2Int1) {
        this.mID = param2Int1;
      }
    }
    
    public enum Format {
      ALPHA(1),
      LUMINANCE_ALPHA(2),
      RGB(3),
      RGBA(3);
      
      private static final Format[] $VALUES;
      
      int mID;
      
      static {
        Format format = new Format("RGBA", 3, 4);
        $VALUES = new Format[] { ALPHA, LUMINANCE_ALPHA, RGB, format };
      }
      
      Format(int param2Int1) {
        this.mID = param2Int1;
      }
    }
    
    private class Slot {
      ProgramFragmentFixedFunction.Builder.EnvMode env;
      
      ProgramFragmentFixedFunction.Builder.Format format;
      
      final ProgramFragmentFixedFunction.Builder this$0;
      
      Slot(ProgramFragmentFixedFunction.Builder.EnvMode param2EnvMode, ProgramFragmentFixedFunction.Builder.Format param2Format) {
        this.env = param2EnvMode;
        this.format = param2Format;
      }
    }
    
    private void buildShaderString() {
      this.mShader = "//rs_shader_internal\n";
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this.mShader);
      stringBuilder.append("varying lowp vec4 varColor;\n");
      this.mShader = stringBuilder.toString();
      stringBuilder = new StringBuilder();
      stringBuilder.append(this.mShader);
      stringBuilder.append("varying vec2 varTex0;\n");
      this.mShader = stringBuilder.toString();
      stringBuilder = new StringBuilder();
      stringBuilder.append(this.mShader);
      stringBuilder.append("void main() {\n");
      this.mShader = stringBuilder.toString();
      if (this.mVaryingColorEnable) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(this.mShader);
        stringBuilder.append("  lowp vec4 col = varColor;\n");
        this.mShader = stringBuilder.toString();
      } else {
        stringBuilder = new StringBuilder();
        stringBuilder.append(this.mShader);
        stringBuilder.append("  lowp vec4 col = UNI_Color;\n");
        this.mShader = stringBuilder.toString();
      } 
      if (this.mNumTextures != 0)
        if (this.mPointSpriteEnable) {
          stringBuilder = new StringBuilder();
          stringBuilder.append(this.mShader);
          stringBuilder.append("  vec2 t0 = gl_PointCoord;\n");
          this.mShader = stringBuilder.toString();
        } else {
          stringBuilder = new StringBuilder();
          stringBuilder.append(this.mShader);
          stringBuilder.append("  vec2 t0 = varTex0.xy;\n");
          this.mShader = stringBuilder.toString();
        }  
      for (byte b = 0; b < this.mNumTextures; b++) {
        int i = ProgramFragmentFixedFunction.null.$SwitchMap$android$renderscript$ProgramFragmentFixedFunction$Builder$EnvMode[(this.mSlots[b]).env.ordinal()];
        if (i != 1) {
          if (i != 2) {
            if (i == 3) {
              stringBuilder = new StringBuilder();
              stringBuilder.append(this.mShader);
              stringBuilder.append("  col = texture2D(UNI_Tex0, t0);\n");
              this.mShader = stringBuilder.toString();
            } 
          } else {
            i = ProgramFragmentFixedFunction.null.$SwitchMap$android$renderscript$ProgramFragmentFixedFunction$Builder$Format[(this.mSlots[b]).format.ordinal()];
            if (i != 1) {
              if (i != 2) {
                if (i != 3) {
                  if (i == 4) {
                    stringBuilder = new StringBuilder();
                    stringBuilder.append(this.mShader);
                    stringBuilder.append("  col.rgba *= texture2D(UNI_Tex0, t0).rgba;\n");
                    this.mShader = stringBuilder.toString();
                  } 
                } else {
                  stringBuilder = new StringBuilder();
                  stringBuilder.append(this.mShader);
                  stringBuilder.append("  col.rgb *= texture2D(UNI_Tex0, t0).rgb;\n");
                  this.mShader = stringBuilder.toString();
                } 
              } else {
                stringBuilder = new StringBuilder();
                stringBuilder.append(this.mShader);
                stringBuilder.append("  col.rgba *= texture2D(UNI_Tex0, t0).rgba;\n");
                this.mShader = stringBuilder.toString();
              } 
            } else {
              stringBuilder = new StringBuilder();
              stringBuilder.append(this.mShader);
              stringBuilder.append("  col.a *= texture2D(UNI_Tex0, t0).a;\n");
              this.mShader = stringBuilder.toString();
            } 
          } 
        } else {
          i = ProgramFragmentFixedFunction.null.$SwitchMap$android$renderscript$ProgramFragmentFixedFunction$Builder$Format[(this.mSlots[b]).format.ordinal()];
          if (i != 1) {
            if (i != 2) {
              if (i != 3) {
                if (i == 4) {
                  stringBuilder = new StringBuilder();
                  stringBuilder.append(this.mShader);
                  stringBuilder.append("  col.rgba = texture2D(UNI_Tex0, t0).rgba;\n");
                  this.mShader = stringBuilder.toString();
                } 
              } else {
                stringBuilder = new StringBuilder();
                stringBuilder.append(this.mShader);
                stringBuilder.append("  col.rgb = texture2D(UNI_Tex0, t0).rgb;\n");
                this.mShader = stringBuilder.toString();
              } 
            } else {
              stringBuilder = new StringBuilder();
              stringBuilder.append(this.mShader);
              stringBuilder.append("  col.rgba = texture2D(UNI_Tex0, t0).rgba;\n");
              this.mShader = stringBuilder.toString();
            } 
          } else {
            stringBuilder = new StringBuilder();
            stringBuilder.append(this.mShader);
            stringBuilder.append("  col.a = texture2D(UNI_Tex0, t0).a;\n");
            this.mShader = stringBuilder.toString();
          } 
        } 
      } 
      stringBuilder = new StringBuilder();
      stringBuilder.append(this.mShader);
      stringBuilder.append("  gl_FragColor = col;\n");
      this.mShader = stringBuilder.toString();
      stringBuilder = new StringBuilder();
      stringBuilder.append(this.mShader);
      stringBuilder.append("}\n");
      this.mShader = stringBuilder.toString();
    }
    
    public Builder(ProgramFragmentFixedFunction this$0) {
      this.mRS = (RenderScript)this$0;
      this.mSlots = new Slot[2];
      this.mPointSpriteEnable = false;
    }
    
    public Builder setTexture(EnvMode param1EnvMode, Format param1Format, int param1Int) throws IllegalArgumentException {
      if (param1Int >= 0 && param1Int < 2) {
        this.mSlots[param1Int] = new Slot(param1EnvMode, param1Format);
        return this;
      } 
      throw new IllegalArgumentException("MAX_TEXTURE exceeded.");
    }
    
    public Builder setPointSpriteTexCoordinateReplacement(boolean param1Boolean) {
      this.mPointSpriteEnable = param1Boolean;
      return this;
    }
    
    public Builder setVaryingColor(boolean param1Boolean) {
      this.mVaryingColorEnable = param1Boolean;
      return this;
    }
    
    public ProgramFragmentFixedFunction create() {
      Type type;
      ProgramFragmentFixedFunction.InternalBuilder internalBuilder = new ProgramFragmentFixedFunction.InternalBuilder(this.mRS);
      this.mNumTextures = 0;
      byte b;
      for (b = 0; b < 2; b++) {
        if (this.mSlots[b] != null)
          this.mNumTextures++; 
      } 
      buildShaderString();
      internalBuilder.setShader(this.mShader);
      Element.Builder builder = null;
      if (!this.mVaryingColorEnable) {
        builder = new Element.Builder(this.mRS);
        builder.add(Element.F32_4(this.mRS), "Color");
        Type.Builder builder1 = new Type.Builder(this.mRS, builder.create());
        builder1.setX(1);
        type = builder1.create();
        internalBuilder.addConstant(type);
      } 
      for (b = 0; b < this.mNumTextures; b++)
        internalBuilder.addTexture(Program.TextureType.TEXTURE_2D); 
      ProgramFragmentFixedFunction programFragmentFixedFunction = internalBuilder.create();
      programFragmentFixedFunction.mTextureCount = 2;
      if (!this.mVaryingColorEnable) {
        Allocation allocation = Allocation.createTyped(this.mRS, type);
        FieldPacker fieldPacker = new FieldPacker(16);
        Float4 float4 = new Float4(1.0F, 1.0F, 1.0F, 1.0F);
        fieldPacker.addF32(float4);
        allocation.setFromFieldPacker(0, fieldPacker);
        programFragmentFixedFunction.bindConstants(allocation, 0);
      } 
      return programFragmentFixedFunction;
    }
  }
}
