package android.renderscript;

public class Double3 {
  public double x;
  
  public double y;
  
  public double z;
  
  public Double3() {}
  
  public Double3(Double3 paramDouble3) {
    this.x = paramDouble3.x;
    this.y = paramDouble3.y;
    this.z = paramDouble3.z;
  }
  
  public Double3(double paramDouble1, double paramDouble2, double paramDouble3) {
    this.x = paramDouble1;
    this.y = paramDouble2;
    this.z = paramDouble3;
  }
  
  public static Double3 add(Double3 paramDouble31, Double3 paramDouble32) {
    Double3 double3 = new Double3();
    paramDouble31.x += paramDouble32.x;
    paramDouble31.y += paramDouble32.y;
    paramDouble31.z += paramDouble32.z;
    return double3;
  }
  
  public void add(Double3 paramDouble3) {
    this.x += paramDouble3.x;
    this.y += paramDouble3.y;
    this.z += paramDouble3.z;
  }
  
  public void add(double paramDouble) {
    this.x += paramDouble;
    this.y += paramDouble;
    this.z += paramDouble;
  }
  
  public static Double3 add(Double3 paramDouble3, double paramDouble) {
    Double3 double3 = new Double3();
    paramDouble3.x += paramDouble;
    paramDouble3.y += paramDouble;
    paramDouble3.z += paramDouble;
    return double3;
  }
  
  public void sub(Double3 paramDouble3) {
    this.x -= paramDouble3.x;
    this.y -= paramDouble3.y;
    this.z -= paramDouble3.z;
  }
  
  public static Double3 sub(Double3 paramDouble31, Double3 paramDouble32) {
    Double3 double3 = new Double3();
    paramDouble31.x -= paramDouble32.x;
    paramDouble31.y -= paramDouble32.y;
    paramDouble31.z -= paramDouble32.z;
    return double3;
  }
  
  public void sub(double paramDouble) {
    this.x -= paramDouble;
    this.y -= paramDouble;
    this.z -= paramDouble;
  }
  
  public static Double3 sub(Double3 paramDouble3, double paramDouble) {
    Double3 double3 = new Double3();
    paramDouble3.x -= paramDouble;
    paramDouble3.y -= paramDouble;
    paramDouble3.z -= paramDouble;
    return double3;
  }
  
  public void mul(Double3 paramDouble3) {
    this.x *= paramDouble3.x;
    this.y *= paramDouble3.y;
    this.z *= paramDouble3.z;
  }
  
  public static Double3 mul(Double3 paramDouble31, Double3 paramDouble32) {
    Double3 double3 = new Double3();
    paramDouble31.x *= paramDouble32.x;
    paramDouble31.y *= paramDouble32.y;
    paramDouble31.z *= paramDouble32.z;
    return double3;
  }
  
  public void mul(double paramDouble) {
    this.x *= paramDouble;
    this.y *= paramDouble;
    this.z *= paramDouble;
  }
  
  public static Double3 mul(Double3 paramDouble3, double paramDouble) {
    Double3 double3 = new Double3();
    paramDouble3.x *= paramDouble;
    paramDouble3.y *= paramDouble;
    paramDouble3.z *= paramDouble;
    return double3;
  }
  
  public void div(Double3 paramDouble3) {
    this.x /= paramDouble3.x;
    this.y /= paramDouble3.y;
    this.z /= paramDouble3.z;
  }
  
  public static Double3 div(Double3 paramDouble31, Double3 paramDouble32) {
    Double3 double3 = new Double3();
    paramDouble31.x /= paramDouble32.x;
    paramDouble31.y /= paramDouble32.y;
    paramDouble31.z /= paramDouble32.z;
    return double3;
  }
  
  public void div(double paramDouble) {
    this.x /= paramDouble;
    this.y /= paramDouble;
    this.z /= paramDouble;
  }
  
  public static Double3 div(Double3 paramDouble3, double paramDouble) {
    Double3 double3 = new Double3();
    paramDouble3.x /= paramDouble;
    paramDouble3.y /= paramDouble;
    paramDouble3.z /= paramDouble;
    return double3;
  }
  
  public double dotProduct(Double3 paramDouble3) {
    return this.x * paramDouble3.x + this.y * paramDouble3.y + this.z * paramDouble3.z;
  }
  
  public static double dotProduct(Double3 paramDouble31, Double3 paramDouble32) {
    return paramDouble32.x * paramDouble31.x + paramDouble32.y * paramDouble31.y + paramDouble32.z * paramDouble31.z;
  }
  
  public void addMultiple(Double3 paramDouble3, double paramDouble) {
    this.x += paramDouble3.x * paramDouble;
    this.y += paramDouble3.y * paramDouble;
    this.z += paramDouble3.z * paramDouble;
  }
  
  public void set(Double3 paramDouble3) {
    this.x = paramDouble3.x;
    this.y = paramDouble3.y;
    this.z = paramDouble3.z;
  }
  
  public void negate() {
    this.x = -this.x;
    this.y = -this.y;
    this.z = -this.z;
  }
  
  public int length() {
    return 3;
  }
  
  public double elementSum() {
    return this.x + this.y + this.z;
  }
  
  public double get(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt == 2)
          return this.z; 
        throw new IndexOutOfBoundsException("Index: i");
      } 
      return this.y;
    } 
    return this.x;
  }
  
  public void setAt(int paramInt, double paramDouble) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt == 2) {
          this.z = paramDouble;
          return;
        } 
        throw new IndexOutOfBoundsException("Index: i");
      } 
      this.y = paramDouble;
      return;
    } 
    this.x = paramDouble;
  }
  
  public void addAt(int paramInt, double paramDouble) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt == 2) {
          this.z += paramDouble;
          return;
        } 
        throw new IndexOutOfBoundsException("Index: i");
      } 
      this.y += paramDouble;
      return;
    } 
    this.x += paramDouble;
  }
  
  public void setValues(double paramDouble1, double paramDouble2, double paramDouble3) {
    this.x = paramDouble1;
    this.y = paramDouble2;
    this.z = paramDouble3;
  }
  
  public void copyTo(double[] paramArrayOfdouble, int paramInt) {
    paramArrayOfdouble[paramInt] = this.x;
    paramArrayOfdouble[paramInt + 1] = this.y;
    paramArrayOfdouble[paramInt + 2] = this.z;
  }
}
