package android.renderscript;

public class ProgramRaster extends BaseObj {
  CullMode mCullMode;
  
  boolean mPointSprite;
  
  class CullMode extends Enum<CullMode> {
    private static final CullMode[] $VALUES;
    
    public static CullMode valueOf(String param1String) {
      return Enum.<CullMode>valueOf(CullMode.class, param1String);
    }
    
    public static CullMode[] values() {
      return (CullMode[])$VALUES.clone();
    }
    
    public static final CullMode BACK = new CullMode("BACK", 0, 0);
    
    public static final CullMode FRONT = new CullMode("FRONT", 1, 1);
    
    public static final CullMode NONE;
    
    int mID;
    
    static {
      CullMode cullMode = new CullMode("NONE", 2, 2);
      $VALUES = new CullMode[] { BACK, FRONT, cullMode };
    }
    
    private CullMode(ProgramRaster this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.mID = param1Int2;
    }
  }
  
  ProgramRaster(long paramLong, RenderScript paramRenderScript) {
    super(paramLong, paramRenderScript);
    this.mPointSprite = false;
    this.mCullMode = CullMode.BACK;
  }
  
  public boolean isPointSpriteEnabled() {
    return this.mPointSprite;
  }
  
  public CullMode getCullMode() {
    return this.mCullMode;
  }
  
  public static ProgramRaster CULL_BACK(RenderScript paramRenderScript) {
    if (paramRenderScript.mProgramRaster_CULL_BACK == null) {
      Builder builder = new Builder(paramRenderScript);
      builder.setCullMode(CullMode.BACK);
      paramRenderScript.mProgramRaster_CULL_BACK = builder.create();
    } 
    return paramRenderScript.mProgramRaster_CULL_BACK;
  }
  
  public static ProgramRaster CULL_FRONT(RenderScript paramRenderScript) {
    if (paramRenderScript.mProgramRaster_CULL_FRONT == null) {
      Builder builder = new Builder(paramRenderScript);
      builder.setCullMode(CullMode.FRONT);
      paramRenderScript.mProgramRaster_CULL_FRONT = builder.create();
    } 
    return paramRenderScript.mProgramRaster_CULL_FRONT;
  }
  
  public static ProgramRaster CULL_NONE(RenderScript paramRenderScript) {
    if (paramRenderScript.mProgramRaster_CULL_NONE == null) {
      Builder builder = new Builder(paramRenderScript);
      builder.setCullMode(CullMode.NONE);
      paramRenderScript.mProgramRaster_CULL_NONE = builder.create();
    } 
    return paramRenderScript.mProgramRaster_CULL_NONE;
  }
  
  class Builder {
    ProgramRaster.CullMode mCullMode;
    
    boolean mPointSprite;
    
    RenderScript mRS;
    
    public Builder(ProgramRaster this$0) {
      this.mRS = (RenderScript)this$0;
      this.mPointSprite = false;
      this.mCullMode = ProgramRaster.CullMode.BACK;
    }
    
    public Builder setPointSpriteEnabled(boolean param1Boolean) {
      this.mPointSprite = param1Boolean;
      return this;
    }
    
    public Builder setCullMode(ProgramRaster.CullMode param1CullMode) {
      this.mCullMode = param1CullMode;
      return this;
    }
    
    public ProgramRaster create() {
      this.mRS.validate();
      long l = this.mRS.nProgramRasterCreate(this.mPointSprite, this.mCullMode.mID);
      ProgramRaster programRaster = new ProgramRaster(l, this.mRS);
      programRaster.mPointSprite = this.mPointSprite;
      programRaster.mCullMode = this.mCullMode;
      return programRaster;
    }
  }
}
