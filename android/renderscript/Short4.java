package android.renderscript;

public class Short4 {
  public short w;
  
  public short x;
  
  public short y;
  
  public short z;
  
  public Short4() {}
  
  public Short4(short paramShort) {
    this.w = paramShort;
    this.z = paramShort;
    this.y = paramShort;
    this.x = paramShort;
  }
  
  public Short4(short paramShort1, short paramShort2, short paramShort3, short paramShort4) {
    this.x = paramShort1;
    this.y = paramShort2;
    this.z = paramShort3;
    this.w = paramShort4;
  }
  
  public Short4(Short4 paramShort4) {
    this.x = paramShort4.x;
    this.y = paramShort4.y;
    this.z = paramShort4.z;
    this.w = paramShort4.w;
  }
  
  public void add(Short4 paramShort4) {
    this.x = (short)(this.x + paramShort4.x);
    this.y = (short)(this.y + paramShort4.y);
    this.z = (short)(this.z + paramShort4.z);
    this.w = (short)(this.w + paramShort4.w);
  }
  
  public static Short4 add(Short4 paramShort41, Short4 paramShort42) {
    Short4 short4 = new Short4();
    short4.x = (short)(paramShort41.x + paramShort42.x);
    short4.y = (short)(paramShort41.y + paramShort42.y);
    short4.z = (short)(paramShort41.z + paramShort42.z);
    short4.w = (short)(paramShort41.w + paramShort42.w);
    return short4;
  }
  
  public void add(short paramShort) {
    this.x = (short)(this.x + paramShort);
    this.y = (short)(this.y + paramShort);
    this.z = (short)(this.z + paramShort);
    this.w = (short)(this.w + paramShort);
  }
  
  public static Short4 add(Short4 paramShort4, short paramShort) {
    Short4 short4 = new Short4();
    short4.x = (short)(paramShort4.x + paramShort);
    short4.y = (short)(paramShort4.y + paramShort);
    short4.z = (short)(paramShort4.z + paramShort);
    short4.w = (short)(paramShort4.w + paramShort);
    return short4;
  }
  
  public void sub(Short4 paramShort4) {
    this.x = (short)(this.x - paramShort4.x);
    this.y = (short)(this.y - paramShort4.y);
    this.z = (short)(this.z - paramShort4.z);
    this.w = (short)(this.w - paramShort4.w);
  }
  
  public static Short4 sub(Short4 paramShort41, Short4 paramShort42) {
    Short4 short4 = new Short4();
    short4.x = (short)(paramShort41.x - paramShort42.x);
    short4.y = (short)(paramShort41.y - paramShort42.y);
    short4.z = (short)(paramShort41.z - paramShort42.z);
    short4.w = (short)(paramShort41.w - paramShort42.w);
    return short4;
  }
  
  public void sub(short paramShort) {
    this.x = (short)(this.x - paramShort);
    this.y = (short)(this.y - paramShort);
    this.z = (short)(this.z - paramShort);
    this.w = (short)(this.w - paramShort);
  }
  
  public static Short4 sub(Short4 paramShort4, short paramShort) {
    Short4 short4 = new Short4();
    short4.x = (short)(paramShort4.x - paramShort);
    short4.y = (short)(paramShort4.y - paramShort);
    short4.z = (short)(paramShort4.z - paramShort);
    short4.w = (short)(paramShort4.w - paramShort);
    return short4;
  }
  
  public void mul(Short4 paramShort4) {
    this.x = (short)(this.x * paramShort4.x);
    this.y = (short)(this.y * paramShort4.y);
    this.z = (short)(this.z * paramShort4.z);
    this.w = (short)(this.w * paramShort4.w);
  }
  
  public static Short4 mul(Short4 paramShort41, Short4 paramShort42) {
    Short4 short4 = new Short4();
    short4.x = (short)(paramShort41.x * paramShort42.x);
    short4.y = (short)(paramShort41.y * paramShort42.y);
    short4.z = (short)(paramShort41.z * paramShort42.z);
    short4.w = (short)(paramShort41.w * paramShort42.w);
    return short4;
  }
  
  public void mul(short paramShort) {
    this.x = (short)(this.x * paramShort);
    this.y = (short)(this.y * paramShort);
    this.z = (short)(this.z * paramShort);
    this.w = (short)(this.w * paramShort);
  }
  
  public static Short4 mul(Short4 paramShort4, short paramShort) {
    Short4 short4 = new Short4();
    short4.x = (short)(paramShort4.x * paramShort);
    short4.y = (short)(paramShort4.y * paramShort);
    short4.z = (short)(paramShort4.z * paramShort);
    short4.w = (short)(paramShort4.w * paramShort);
    return short4;
  }
  
  public void div(Short4 paramShort4) {
    this.x = (short)(this.x / paramShort4.x);
    this.y = (short)(this.y / paramShort4.y);
    this.z = (short)(this.z / paramShort4.z);
    this.w = (short)(this.w / paramShort4.w);
  }
  
  public static Short4 div(Short4 paramShort41, Short4 paramShort42) {
    Short4 short4 = new Short4();
    short4.x = (short)(paramShort41.x / paramShort42.x);
    short4.y = (short)(paramShort41.y / paramShort42.y);
    short4.z = (short)(paramShort41.z / paramShort42.z);
    short4.w = (short)(paramShort41.w / paramShort42.w);
    return short4;
  }
  
  public void div(short paramShort) {
    this.x = (short)(this.x / paramShort);
    this.y = (short)(this.y / paramShort);
    this.z = (short)(this.z / paramShort);
    this.w = (short)(this.w / paramShort);
  }
  
  public static Short4 div(Short4 paramShort4, short paramShort) {
    Short4 short4 = new Short4();
    short4.x = (short)(paramShort4.x / paramShort);
    short4.y = (short)(paramShort4.y / paramShort);
    short4.z = (short)(paramShort4.z / paramShort);
    short4.w = (short)(paramShort4.w / paramShort);
    return short4;
  }
  
  public void mod(Short4 paramShort4) {
    this.x = (short)(this.x % paramShort4.x);
    this.y = (short)(this.y % paramShort4.y);
    this.z = (short)(this.z % paramShort4.z);
    this.w = (short)(this.w % paramShort4.w);
  }
  
  public static Short4 mod(Short4 paramShort41, Short4 paramShort42) {
    Short4 short4 = new Short4();
    short4.x = (short)(paramShort41.x % paramShort42.x);
    short4.y = (short)(paramShort41.y % paramShort42.y);
    short4.z = (short)(paramShort41.z % paramShort42.z);
    short4.w = (short)(paramShort41.w % paramShort42.w);
    return short4;
  }
  
  public void mod(short paramShort) {
    this.x = (short)(this.x % paramShort);
    this.y = (short)(this.y % paramShort);
    this.z = (short)(this.z % paramShort);
    this.w = (short)(this.w % paramShort);
  }
  
  public static Short4 mod(Short4 paramShort4, short paramShort) {
    Short4 short4 = new Short4();
    short4.x = (short)(paramShort4.x % paramShort);
    short4.y = (short)(paramShort4.y % paramShort);
    short4.z = (short)(paramShort4.z % paramShort);
    short4.w = (short)(paramShort4.w % paramShort);
    return short4;
  }
  
  public short length() {
    return 4;
  }
  
  public void negate() {
    this.x = (short)-this.x;
    this.y = (short)-this.y;
    this.z = (short)-this.z;
    this.w = (short)-this.w;
  }
  
  public short dotProduct(Short4 paramShort4) {
    return (short)(this.x * paramShort4.x + this.y * paramShort4.y + this.z * paramShort4.z + this.w * paramShort4.w);
  }
  
  public static short dotProduct(Short4 paramShort41, Short4 paramShort42) {
    return (short)(paramShort42.x * paramShort41.x + paramShort42.y * paramShort41.y + paramShort42.z * paramShort41.z + paramShort42.w * paramShort41.w);
  }
  
  public void addMultiple(Short4 paramShort4, short paramShort) {
    this.x = (short)(this.x + paramShort4.x * paramShort);
    this.y = (short)(this.y + paramShort4.y * paramShort);
    this.z = (short)(this.z + paramShort4.z * paramShort);
    this.w = (short)(this.w + paramShort4.w * paramShort);
  }
  
  public void set(Short4 paramShort4) {
    this.x = paramShort4.x;
    this.y = paramShort4.y;
    this.z = paramShort4.z;
    this.w = paramShort4.w;
  }
  
  public void setValues(short paramShort1, short paramShort2, short paramShort3, short paramShort4) {
    this.x = paramShort1;
    this.y = paramShort2;
    this.z = paramShort3;
    this.w = paramShort4;
  }
  
  public short elementSum() {
    return (short)(this.x + this.y + this.z + this.w);
  }
  
  public short get(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt == 3)
            return this.w; 
          throw new IndexOutOfBoundsException("Index: i");
        } 
        return this.z;
      } 
      return this.y;
    } 
    return this.x;
  }
  
  public void setAt(int paramInt, short paramShort) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt == 3) {
            this.w = paramShort;
            return;
          } 
          throw new IndexOutOfBoundsException("Index: i");
        } 
        this.z = paramShort;
        return;
      } 
      this.y = paramShort;
      return;
    } 
    this.x = paramShort;
  }
  
  public void addAt(int paramInt, short paramShort) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt == 3) {
            this.w = (short)(this.w + paramShort);
            return;
          } 
          throw new IndexOutOfBoundsException("Index: i");
        } 
        this.z = (short)(this.z + paramShort);
        return;
      } 
      this.y = (short)(this.y + paramShort);
      return;
    } 
    this.x = (short)(this.x + paramShort);
  }
  
  public void copyTo(short[] paramArrayOfshort, int paramInt) {
    paramArrayOfshort[paramInt] = this.x;
    paramArrayOfshort[paramInt + 1] = this.y;
    paramArrayOfshort[paramInt + 2] = this.z;
    paramArrayOfshort[paramInt + 3] = this.w;
  }
}
