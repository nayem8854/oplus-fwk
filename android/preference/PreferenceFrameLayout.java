package android.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.android.internal.R;

@Deprecated
public class PreferenceFrameLayout extends FrameLayout {
  private static final int DEFAULT_BORDER_BOTTOM = 0;
  
  private static final int DEFAULT_BORDER_LEFT = 0;
  
  private static final int DEFAULT_BORDER_RIGHT = 0;
  
  private static final int DEFAULT_BORDER_TOP = 0;
  
  private final int mBorderBottom;
  
  private final int mBorderLeft;
  
  private final int mBorderRight;
  
  private final int mBorderTop;
  
  private boolean mPaddingApplied;
  
  public PreferenceFrameLayout(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public PreferenceFrameLayout(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 17957045);
  }
  
  public PreferenceFrameLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public PreferenceFrameLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.PreferenceFrameLayout, paramInt1, paramInt2);
    float f = (paramContext.getResources().getDisplayMetrics()).density;
    int i = (int)(f * 0.0F + 0.5F);
    paramInt2 = (int)(f * 0.0F + 0.5F);
    int j = (int)(f * 0.0F + 0.5F);
    paramInt1 = (int)(0.0F * f + 0.5F);
    this.mBorderTop = typedArray.getDimensionPixelSize(3, i);
    this.mBorderBottom = typedArray.getDimensionPixelSize(0, paramInt2);
    this.mBorderLeft = typedArray.getDimensionPixelSize(1, j);
    this.mBorderRight = typedArray.getDimensionPixelSize(2, paramInt1);
    typedArray.recycle();
  }
  
  public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  public void addView(View paramView) {
    int n, i1, i2, i3, i = getPaddingTop();
    int j = getPaddingBottom();
    int k = getPaddingLeft();
    int m = getPaddingRight();
    ViewGroup.LayoutParams layoutParams = paramView.getLayoutParams();
    if (layoutParams instanceof LayoutParams) {
      LayoutParams layoutParams1 = (LayoutParams)paramView.getLayoutParams();
    } else {
      layoutParams = null;
    } 
    if (layoutParams != null && ((LayoutParams)layoutParams).removeBorders) {
      n = i;
      i1 = j;
      i2 = k;
      i3 = m;
      if (this.mPaddingApplied) {
        n = i - this.mBorderTop;
        i1 = j - this.mBorderBottom;
        i2 = k - this.mBorderLeft;
        i3 = m - this.mBorderRight;
        this.mPaddingApplied = false;
      } 
    } else {
      n = i;
      i1 = j;
      i2 = k;
      i3 = m;
      if (!this.mPaddingApplied) {
        n = i + this.mBorderTop;
        i1 = j + this.mBorderBottom;
        i2 = k + this.mBorderLeft;
        i3 = m + this.mBorderRight;
        this.mPaddingApplied = true;
      } 
    } 
    k = getPaddingTop();
    j = getPaddingBottom();
    i = getPaddingLeft();
    m = getPaddingRight();
    if (k != n || j != i1 || i != i2 || m != i3)
      setPadding(i2, n, i3, i1); 
    super.addView(paramView);
  }
  
  class LayoutParams extends FrameLayout.LayoutParams {
    public boolean removeBorders = false;
    
    public LayoutParams(PreferenceFrameLayout this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
      TypedArray typedArray = this$0.obtainStyledAttributes(param1AttributeSet, R.styleable.PreferenceFrameLayout_Layout);
      this.removeBorders = typedArray.getBoolean(0, false);
      typedArray.recycle();
    }
    
    public LayoutParams(PreferenceFrameLayout this$0, int param1Int1) {
      super(this$0, param1Int1);
    }
  }
}
