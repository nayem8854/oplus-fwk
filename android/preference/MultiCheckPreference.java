package android.preference;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import com.android.internal.R;
import java.util.Arrays;

@Deprecated
public class MultiCheckPreference extends DialogPreference {
  private CharSequence[] mEntries;
  
  private String[] mEntryValues;
  
  private boolean[] mOrigValues;
  
  private boolean[] mSetValues;
  
  private String mSummary;
  
  public MultiCheckPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray2 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ListPreference, paramInt1, paramInt2);
    CharSequence[] arrayOfCharSequence = typedArray2.getTextArray(0);
    if (arrayOfCharSequence != null)
      setEntries(arrayOfCharSequence); 
    setEntryValuesCS(typedArray2.getTextArray(1));
    typedArray2.recycle();
    TypedArray typedArray1 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Preference, 0, 0);
    this.mSummary = typedArray1.getString(7);
    typedArray1.recycle();
  }
  
  public MultiCheckPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public MultiCheckPreference(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842897);
  }
  
  public MultiCheckPreference(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public void setEntries(CharSequence[] paramArrayOfCharSequence) {
    this.mEntries = paramArrayOfCharSequence;
    this.mSetValues = new boolean[paramArrayOfCharSequence.length];
    this.mOrigValues = new boolean[paramArrayOfCharSequence.length];
  }
  
  public void setEntries(int paramInt) {
    setEntries(getContext().getResources().getTextArray(paramInt));
  }
  
  public CharSequence[] getEntries() {
    return this.mEntries;
  }
  
  public void setEntryValues(String[] paramArrayOfString) {
    this.mEntryValues = paramArrayOfString;
    Arrays.fill(this.mSetValues, false);
    Arrays.fill(this.mOrigValues, false);
  }
  
  public void setEntryValues(int paramInt) {
    setEntryValuesCS(getContext().getResources().getTextArray(paramInt));
  }
  
  private void setEntryValuesCS(CharSequence[] paramArrayOfCharSequence) {
    setValues((boolean[])null);
    if (paramArrayOfCharSequence != null) {
      this.mEntryValues = new String[paramArrayOfCharSequence.length];
      for (byte b = 0; b < paramArrayOfCharSequence.length; b++)
        this.mEntryValues[b] = paramArrayOfCharSequence[b].toString(); 
    } 
  }
  
  public String[] getEntryValues() {
    return this.mEntryValues;
  }
  
  public boolean getValue(int paramInt) {
    return this.mSetValues[paramInt];
  }
  
  public void setValue(int paramInt, boolean paramBoolean) {
    this.mSetValues[paramInt] = paramBoolean;
  }
  
  public void setValues(boolean[] paramArrayOfboolean) {
    boolean[] arrayOfBoolean = this.mSetValues;
    if (arrayOfBoolean != null) {
      Arrays.fill(arrayOfBoolean, false);
      Arrays.fill(this.mOrigValues, false);
      if (paramArrayOfboolean != null) {
        int i;
        arrayOfBoolean = this.mSetValues;
        if (paramArrayOfboolean.length < arrayOfBoolean.length) {
          i = paramArrayOfboolean.length;
        } else {
          i = arrayOfBoolean.length;
        } 
        System.arraycopy(paramArrayOfboolean, 0, arrayOfBoolean, 0, i);
      } 
    } 
  }
  
  public CharSequence getSummary() {
    String str = this.mSummary;
    if (str == null)
      return super.getSummary(); 
    return str;
  }
  
  public void setSummary(CharSequence paramCharSequence) {
    super.setSummary(paramCharSequence);
    if (paramCharSequence == null && this.mSummary != null) {
      this.mSummary = null;
    } else if (paramCharSequence != null && !paramCharSequence.equals(this.mSummary)) {
      this.mSummary = paramCharSequence.toString();
    } 
  }
  
  public boolean[] getValues() {
    return this.mSetValues;
  }
  
  public int findIndexOfValue(String paramString) {
    if (paramString != null) {
      String[] arrayOfString = this.mEntryValues;
      if (arrayOfString != null)
        for (int i = arrayOfString.length - 1; i >= 0; i--) {
          if (this.mEntryValues[i].equals(paramString))
            return i; 
        }  
    } 
    return -1;
  }
  
  protected void onPrepareDialogBuilder(AlertDialog.Builder paramBuilder) {
    super.onPrepareDialogBuilder(paramBuilder);
    if (this.mEntries != null && this.mEntryValues != null) {
      boolean[] arrayOfBoolean = this.mSetValues;
      this.mOrigValues = Arrays.copyOf(arrayOfBoolean, arrayOfBoolean.length);
      paramBuilder.setMultiChoiceItems(this.mEntries, this.mSetValues, (DialogInterface.OnMultiChoiceClickListener)new Object(this));
      return;
    } 
    throw new IllegalStateException("ListPreference requires an entries array and an entryValues array.");
  }
  
  protected void onDialogClosed(boolean paramBoolean) {
    super.onDialogClosed(paramBoolean);
    if (paramBoolean && 
      callChangeListener(getValues()))
      return; 
    boolean[] arrayOfBoolean1 = this.mOrigValues, arrayOfBoolean2 = this.mSetValues;
    System.arraycopy(arrayOfBoolean1, 0, arrayOfBoolean2, 0, arrayOfBoolean2.length);
  }
  
  protected Object onGetDefaultValue(TypedArray paramTypedArray, int paramInt) {
    return paramTypedArray.getString(paramInt);
  }
  
  protected void onSetInitialValue(boolean paramBoolean, Object paramObject) {}
  
  protected Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    if (isPersistent())
      return parcelable; 
    SavedState savedState = new SavedState(parcelable);
    savedState.values = getValues();
    return (Parcelable)savedState;
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable) {
    if (paramParcelable == null || !paramParcelable.getClass().equals(SavedState.class)) {
      super.onRestoreInstanceState(paramParcelable);
      return;
    } 
    SavedState savedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(savedState.getSuperState());
    setValues(savedState.values);
  }
  
  class SavedState extends Preference.BaseSavedState {
    public SavedState(MultiCheckPreference this$0) {
      super((Parcel)this$0);
      this.values = this$0.createBooleanArray();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeBooleanArray(this.values);
    }
    
    public SavedState(MultiCheckPreference this$0) {
      super((Parcelable)this$0);
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    boolean[] values;
  }
}
