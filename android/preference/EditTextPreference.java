package android.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.EditText;

@Deprecated
public class EditTextPreference extends DialogPreference {
  private EditText mEditText;
  
  private String mText;
  
  private boolean mTextSet;
  
  public EditTextPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    EditText editText = new EditText(paramContext, paramAttributeSet);
    editText.setId(16908291);
    this.mEditText.setEnabled(true);
  }
  
  public EditTextPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public EditTextPreference(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842898);
  }
  
  public EditTextPreference(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public void setText(String paramString) {
    int i = TextUtils.equals(this.mText, paramString) ^ true;
    if (i != 0 || !this.mTextSet) {
      this.mText = paramString;
      this.mTextSet = true;
      persistString(paramString);
      if (i != 0) {
        notifyDependencyChange(shouldDisableDependents());
        notifyChanged();
      } 
    } 
  }
  
  public String getText() {
    return this.mText;
  }
  
  protected void onBindDialogView(View paramView) {
    super.onBindDialogView(paramView);
    EditText editText = this.mEditText;
    editText.setText(getText());
    ViewParent viewParent = editText.getParent();
    if (viewParent != paramView) {
      if (viewParent != null)
        ((ViewGroup)viewParent).removeView((View)editText); 
      onAddEditTextToDialogView(paramView, editText);
    } 
  }
  
  protected void onAddEditTextToDialogView(View paramView, EditText paramEditText) {
    ViewGroup viewGroup = (ViewGroup)paramView.findViewById(16908940);
    if (viewGroup != null)
      viewGroup.addView((View)paramEditText, -1, -2); 
  }
  
  protected void onDialogClosed(boolean paramBoolean) {
    super.onDialogClosed(paramBoolean);
    if (paramBoolean) {
      String str = this.mEditText.getText().toString();
      if (callChangeListener(str))
        setText(str); 
    } 
  }
  
  protected Object onGetDefaultValue(TypedArray paramTypedArray, int paramInt) {
    return paramTypedArray.getString(paramInt);
  }
  
  protected void onSetInitialValue(boolean paramBoolean, Object paramObject) {
    if (paramBoolean) {
      paramObject = getPersistedString(this.mText);
    } else {
      paramObject = paramObject;
    } 
    setText((String)paramObject);
  }
  
  public boolean shouldDisableDependents() {
    return (TextUtils.isEmpty(this.mText) || super.shouldDisableDependents());
  }
  
  public EditText getEditText() {
    return this.mEditText;
  }
  
  protected boolean needInputMethod() {
    return true;
  }
  
  protected Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    if (isPersistent())
      return parcelable; 
    SavedState savedState = new SavedState(parcelable);
    savedState.text = getText();
    return (Parcelable)savedState;
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable) {
    if (paramParcelable == null || !paramParcelable.getClass().equals(SavedState.class)) {
      super.onRestoreInstanceState(paramParcelable);
      return;
    } 
    SavedState savedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(savedState.getSuperState());
    setText(savedState.text);
  }
  
  class SavedState extends Preference.BaseSavedState {
    public SavedState(EditTextPreference this$0) {
      super((Parcel)this$0);
      this.text = this$0.readString();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeString(this.text);
    }
    
    public SavedState(EditTextPreference this$0) {
      super((Parcelable)this$0);
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    String text;
  }
}
