package android.preference;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Deprecated
public class PreferenceGroupAdapter extends BaseAdapter implements Preference.OnPreferenceChangeInternalListener {
  private PreferenceLayout mTempPreferenceLayout = new PreferenceLayout();
  
  private boolean mHasReturnedViewTypeCount = false;
  
  private volatile boolean mIsSyncing = false;
  
  private Handler mHandler = new Handler();
  
  private Runnable mSyncRunnable = (Runnable)new Object(this);
  
  private int mHighlightedPosition = -1;
  
  private static ViewGroup.LayoutParams sWrapperLayoutParams = new ViewGroup.LayoutParams(-1, -2);
  
  private static final String TAG = "PreferenceGroupAdapter";
  
  private Drawable mHighlightedDrawable;
  
  private PreferenceGroup mPreferenceGroup;
  
  private ArrayList<PreferenceLayout> mPreferenceLayouts;
  
  private List<Preference> mPreferenceList;
  
  class PreferenceLayout implements Comparable<PreferenceLayout> {
    private String name;
    
    private int resId;
    
    private int widgetResId;
    
    private PreferenceLayout() {}
    
    public int compareTo(PreferenceLayout param1PreferenceLayout) {
      int i = this.name.compareTo(param1PreferenceLayout.name);
      if (i == 0) {
        int j = this.resId;
        i = param1PreferenceLayout.resId;
        if (j == i) {
          j = this.widgetResId;
          i = param1PreferenceLayout.widgetResId;
          if (j == i)
            return 0; 
          return j - i;
        } 
        return j - i;
      } 
      return i;
    }
  }
  
  public PreferenceGroupAdapter(PreferenceGroup paramPreferenceGroup) {
    this.mPreferenceGroup = paramPreferenceGroup;
    paramPreferenceGroup.setOnPreferenceChangeInternalListener(this);
    this.mPreferenceList = new ArrayList<>();
    this.mPreferenceLayouts = new ArrayList<>();
    syncMyPreferences();
  }
  
  private void syncMyPreferences() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIsSyncing : Z
    //   6: ifeq -> 12
    //   9: aload_0
    //   10: monitorexit
    //   11: return
    //   12: aload_0
    //   13: iconst_1
    //   14: putfield mIsSyncing : Z
    //   17: aload_0
    //   18: monitorexit
    //   19: new java/util/ArrayList
    //   22: dup
    //   23: aload_0
    //   24: getfield mPreferenceList : Ljava/util/List;
    //   27: invokeinterface size : ()I
    //   32: invokespecial <init> : (I)V
    //   35: astore_1
    //   36: aload_0
    //   37: aload_1
    //   38: aload_0
    //   39: getfield mPreferenceGroup : Landroid/preference/PreferenceGroup;
    //   42: invokespecial flattenPreferenceGroup : (Ljava/util/List;Landroid/preference/PreferenceGroup;)V
    //   45: aload_0
    //   46: aload_1
    //   47: putfield mPreferenceList : Ljava/util/List;
    //   50: aload_0
    //   51: invokevirtual notifyDataSetChanged : ()V
    //   54: aload_0
    //   55: monitorenter
    //   56: aload_0
    //   57: iconst_0
    //   58: putfield mIsSyncing : Z
    //   61: aload_0
    //   62: invokevirtual notifyAll : ()V
    //   65: aload_0
    //   66: monitorexit
    //   67: return
    //   68: astore_1
    //   69: aload_0
    //   70: monitorexit
    //   71: aload_1
    //   72: athrow
    //   73: astore_1
    //   74: aload_0
    //   75: monitorexit
    //   76: aload_1
    //   77: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #145	-> 0
    //   #146	-> 2
    //   #147	-> 9
    //   #150	-> 12
    //   #151	-> 17
    //   #153	-> 19
    //   #154	-> 36
    //   #155	-> 45
    //   #157	-> 50
    //   #159	-> 54
    //   #160	-> 56
    //   #161	-> 61
    //   #162	-> 65
    //   #163	-> 67
    //   #162	-> 68
    //   #151	-> 73
    // Exception table:
    //   from	to	target	type
    //   2	9	73	finally
    //   9	11	73	finally
    //   12	17	73	finally
    //   17	19	73	finally
    //   56	61	68	finally
    //   61	65	68	finally
    //   65	67	68	finally
    //   69	71	68	finally
    //   74	76	73	finally
  }
  
  private void flattenPreferenceGroup(List<Preference> paramList, PreferenceGroup paramPreferenceGroup) {
    paramPreferenceGroup.sortPreferences();
    int i = paramPreferenceGroup.getPreferenceCount();
    for (byte b = 0; b < i; b++) {
      Preference preference = paramPreferenceGroup.getPreference(b);
      paramList.add(preference);
      if (!this.mHasReturnedViewTypeCount && preference.isRecycleEnabled())
        addPreferenceClassName(preference); 
      if (preference instanceof PreferenceGroup) {
        PreferenceGroup preferenceGroup = (PreferenceGroup)preference;
        if (preferenceGroup.isOnSameScreenAsChildren())
          flattenPreferenceGroup(paramList, preferenceGroup); 
      } 
      preference.setOnPreferenceChangeInternalListener(this);
    } 
  }
  
  private PreferenceLayout createPreferenceLayout(Preference paramPreference, PreferenceLayout paramPreferenceLayout) {
    if (paramPreferenceLayout == null)
      paramPreferenceLayout = new PreferenceLayout(); 
    PreferenceLayout.access$202(paramPreferenceLayout, paramPreference.getClass().getName());
    PreferenceLayout.access$302(paramPreferenceLayout, paramPreference.getLayoutResource());
    PreferenceLayout.access$402(paramPreferenceLayout, paramPreference.getWidgetLayoutResource());
    return paramPreferenceLayout;
  }
  
  private void addPreferenceClassName(Preference paramPreference) {
    PreferenceLayout preferenceLayout = createPreferenceLayout(paramPreference, null);
    int i = Collections.binarySearch((List)this.mPreferenceLayouts, preferenceLayout);
    if (i < 0)
      this.mPreferenceLayouts.add(i * -1 - 1, preferenceLayout); 
  }
  
  public int getCount() {
    return this.mPreferenceList.size();
  }
  
  public Preference getItem(int paramInt) {
    if (paramInt < 0 || paramInt >= getCount())
      return null; 
    return this.mPreferenceList.get(paramInt);
  }
  
  public long getItemId(int paramInt) {
    if (paramInt < 0 || paramInt >= getCount())
      return Long.MIN_VALUE; 
    return getItem(paramInt).getId();
  }
  
  public void setHighlighted(int paramInt) {
    this.mHighlightedPosition = paramInt;
  }
  
  public void setHighlightedDrawable(Drawable paramDrawable) {
    this.mHighlightedDrawable = paramDrawable;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    FrameLayout frameLayout;
    Preference preference = getItem(paramInt);
    PreferenceLayout preferenceLayout = createPreferenceLayout(preference, this.mTempPreferenceLayout);
    if (Collections.binarySearch((List)this.mPreferenceLayouts, preferenceLayout) < 0 || 
      getItemViewType(paramInt) == getHighlightItemViewType())
      paramView = null; 
    View view = preference.getView(paramView, paramViewGroup);
    paramView = view;
    if (paramInt == this.mHighlightedPosition) {
      paramView = view;
      if (this.mHighlightedDrawable != null) {
        frameLayout = new FrameLayout(paramViewGroup.getContext());
        frameLayout.setLayoutParams(sWrapperLayoutParams);
        frameLayout.setBackgroundDrawable(this.mHighlightedDrawable);
        frameLayout.addView(view);
      } 
    } 
    return (View)frameLayout;
  }
  
  public boolean isEnabled(int paramInt) {
    if (paramInt < 0 || paramInt >= getCount())
      return true; 
    return getItem(paramInt).isSelectable();
  }
  
  public boolean areAllItemsEnabled() {
    return false;
  }
  
  public void onPreferenceChange(Preference paramPreference) {
    notifyDataSetChanged();
  }
  
  public void onPreferenceHierarchyChange(Preference paramPreference) {
    this.mHandler.removeCallbacks(this.mSyncRunnable);
    this.mHandler.post(this.mSyncRunnable);
  }
  
  public boolean hasStableIds() {
    return true;
  }
  
  private int getHighlightItemViewType() {
    return getViewTypeCount() - 1;
  }
  
  public int getItemViewType(int paramInt) {
    if (paramInt == this.mHighlightedPosition)
      return getHighlightItemViewType(); 
    if (!this.mHasReturnedViewTypeCount)
      this.mHasReturnedViewTypeCount = true; 
    Preference preference = getItem(paramInt);
    if (!preference.isRecycleEnabled())
      return -1; 
    PreferenceLayout preferenceLayout = createPreferenceLayout(preference, this.mTempPreferenceLayout);
    paramInt = Collections.binarySearch((List)this.mPreferenceLayouts, preferenceLayout);
    if (paramInt < 0)
      return -1; 
    return paramInt;
  }
  
  public int getViewTypeCount() {
    if (!this.mHasReturnedViewTypeCount)
      this.mHasReturnedViewTypeCount = true; 
    return Math.max(1, this.mPreferenceLayouts.size()) + 1;
  }
}
