package android.preference;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;

@Deprecated
public class SeekBarDialogPreference extends DialogPreference {
  private final Drawable mMyIcon;
  
  public SeekBarDialogPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    createActionButtons();
    this.mMyIcon = getDialogIcon();
    setDialogIcon((Drawable)null);
  }
  
  public SeekBarDialogPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public SeekBarDialogPreference(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 17957070);
  }
  
  public SeekBarDialogPreference(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public void createActionButtons() {
    setPositiveButtonText(17039370);
    setNegativeButtonText(17039360);
  }
  
  protected void onBindDialogView(View paramView) {
    super.onBindDialogView(paramView);
    ImageView imageView = (ImageView)paramView.findViewById(16908294);
    Drawable drawable = this.mMyIcon;
    if (drawable != null) {
      imageView.setImageDrawable(drawable);
    } else {
      imageView.setVisibility(8);
    } 
  }
  
  protected static SeekBar getSeekBar(View paramView) {
    return (SeekBar)paramView.findViewById(16909403);
  }
}
