package android.preference;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.audiopolicy.AudioProductStrategy;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.provider.Settings;
import android.service.notification.ZenModeConfig;
import android.util.Log;
import android.widget.SeekBar;
import com.android.internal.os.SomeArgs;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

@Deprecated
public class SeekBarVolumizer implements SeekBar.OnSeekBarChangeListener, Handler.Callback {
  private final Handler mVolumeHandler = new VolumeHandler();
  
  private final AudioManager.VolumeGroupCallback mVolumeGroupCallback = new AudioManager.VolumeGroupCallback() {
      final SeekBarVolumizer this$0;
      
      public void onAudioVolumeGroupChanged(int param1Int1, int param1Int2) {
        if (SeekBarVolumizer.this.mHandler == null)
          return; 
        SomeArgs someArgs = SomeArgs.obtain();
        someArgs.arg1 = Integer.valueOf(param1Int1);
        someArgs.arg2 = Integer.valueOf(param1Int2);
        SeekBarVolumizer.this.mVolumeHandler.sendMessage(SeekBarVolumizer.this.mHandler.obtainMessage(1, someArgs));
      }
    };
  
  private final H mUiHandler = new H();
  
  private final Receiver mReceiver = new Receiver();
  
  private int mLastProgress = -1;
  
  private int mVolumeBeforeMute = -1;
  
  private static final int CHECK_RINGTONE_PLAYBACK_DELAY_MS = 1000;
  
  private static final int MSG_GROUP_VOLUME_CHANGED = 1;
  
  private static final int MSG_INIT_SAMPLE = 3;
  
  private static final int MSG_SET_STREAM_VOLUME = 0;
  
  private static final int MSG_START_SAMPLE = 1;
  
  private static final int MSG_STOP_SAMPLE = 2;
  
  private static final String TAG = "SeekBarVolumizer";
  
  private boolean mAffectedByRingerMode;
  
  private boolean mAllowAlarms;
  
  private boolean mAllowMedia;
  
  private boolean mAllowRinger;
  
  private AudioAttributes mAttributes;
  
  private final AudioManager mAudioManager;
  
  private final Callback mCallback;
  
  private final Context mContext;
  
  private final Uri mDefaultUri;
  
  private Handler mHandler;
  
  private int mLastAudibleStreamVolume;
  
  private final int mMaxStreamVolume;
  
  private boolean mMuted;
  
  private final NotificationManager mNotificationManager;
  
  private boolean mNotificationOrRing;
  
  private NotificationManager.Policy mNotificationPolicy;
  
  private int mOriginalStreamVolume;
  
  private boolean mPlaySample;
  
  private int mRingerMode;
  
  private Ringtone mRingtone;
  
  private SeekBar mSeekBar;
  
  private final int mStreamType;
  
  private int mVolumeGroupId;
  
  private Observer mVolumeObserver;
  
  private int mZenMode;
  
  public SeekBarVolumizer(Context paramContext, int paramInt, Uri paramUri, Callback paramCallback) {
    this(paramContext, paramInt, paramUri, paramCallback, true);
  }
  
  public SeekBarVolumizer(Context paramContext, int paramInt, Uri paramUri, Callback paramCallback, boolean paramBoolean) {
    this.mContext = paramContext;
    this.mAudioManager = (AudioManager)paramContext.getSystemService(AudioManager.class);
    NotificationManager notificationManager = (NotificationManager)paramContext.getSystemService(NotificationManager.class);
    NotificationManager.Policy policy = notificationManager.getConsolidatedNotificationPolicy();
    int i = policy.priorityCategories;
    boolean bool1 = false;
    if ((i & 0x20) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mAllowAlarms = bool2;
    boolean bool2 = bool1;
    if ((this.mNotificationPolicy.priorityCategories & 0x40) != 0)
      bool2 = true; 
    this.mAllowMedia = bool2;
    this.mAllowRinger = ZenModeConfig.areAllPriorityOnlyRingerSoundsMuted(this.mNotificationPolicy) ^ true;
    this.mStreamType = paramInt;
    this.mAffectedByRingerMode = this.mAudioManager.isStreamAffectedByRingerMode(paramInt);
    this.mNotificationOrRing = bool2 = isNotificationOrRing(this.mStreamType);
    if (bool2)
      this.mRingerMode = this.mAudioManager.getRingerModeInternal(); 
    this.mZenMode = this.mNotificationManager.getZenMode();
    if (hasAudioProductStrategies()) {
      this.mVolumeGroupId = getVolumeGroupIdForLegacyStreamType(this.mStreamType);
      this.mAttributes = getAudioAttributesForLegacyStreamType(this.mStreamType);
    } 
    this.mMaxStreamVolume = this.mAudioManager.getStreamMaxVolume(this.mStreamType);
    this.mCallback = paramCallback;
    this.mOriginalStreamVolume = this.mAudioManager.getStreamVolume(this.mStreamType);
    this.mLastAudibleStreamVolume = this.mAudioManager.getLastAudibleStreamVolume(this.mStreamType);
    this.mMuted = bool2 = this.mAudioManager.isStreamMute(this.mStreamType);
    this.mPlaySample = paramBoolean;
    Callback callback = this.mCallback;
    if (callback != null)
      callback.onMuted(bool2, isZenMuted()); 
    Uri uri = paramUri;
    if (paramUri == null) {
      paramInt = this.mStreamType;
      if (paramInt == 2) {
        uri = Settings.System.DEFAULT_RINGTONE_URI;
      } else if (paramInt == 5) {
        uri = Settings.System.DEFAULT_NOTIFICATION_URI;
      } else {
        uri = Settings.System.DEFAULT_ALARM_ALERT_URI;
      } 
    } 
    this.mDefaultUri = uri;
  }
  
  private boolean hasAudioProductStrategies() {
    boolean bool;
    if (AudioManager.getAudioProductStrategies().size() > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private int getVolumeGroupIdForLegacyStreamType(int paramInt) {
    for (AudioProductStrategy audioProductStrategy : AudioManager.getAudioProductStrategies()) {
      int i = audioProductStrategy.getVolumeGroupIdForLegacyStreamType(paramInt);
      if (i != -1)
        return i; 
    } 
    Stream<AudioProductStrategy> stream = AudioManager.getAudioProductStrategies().stream();
    -$.Lambda.SeekBarVolumizer.ezNr2aLs33rVlzIsAVW8OXqqpIs ezNr2aLs33rVlzIsAVW8OXqqpIs = _$$Lambda$SeekBarVolumizer$ezNr2aLs33rVlzIsAVW8OXqqpIs.INSTANCE;
    stream = stream.map((Function<? super AudioProductStrategy, ? extends AudioProductStrategy>)ezNr2aLs33rVlzIsAVW8OXqqpIs);
    -$.Lambda.SeekBarVolumizer.pv2-5S-FjgAtIix6Vp68yZJoqvQ pv2-5S-FjgAtIix6Vp68yZJoqvQ = _$$Lambda$SeekBarVolumizer$pv2_5S_FjgAtIix6Vp68yZJoqvQ.INSTANCE;
    stream = stream.filter((Predicate<? super AudioProductStrategy>)pv2-5S-FjgAtIix6Vp68yZJoqvQ);
    Optional<AudioProductStrategy> optional = stream.findFirst();
    return ((Integer)optional.orElse(Integer.valueOf(-1))).intValue();
  }
  
  private AudioAttributes getAudioAttributesForLegacyStreamType(int paramInt) {
    for (AudioProductStrategy audioProductStrategy : AudioManager.getAudioProductStrategies()) {
      AudioAttributes audioAttributes = audioProductStrategy.getAudioAttributesForLegacyStreamType(paramInt);
      if (audioAttributes != null)
        return audioAttributes; 
    } 
    AudioAttributes.Builder builder = new AudioAttributes.Builder();
    builder = builder.setContentType(0);
    return builder.setUsage(0).build();
  }
  
  private static boolean isNotificationOrRing(int paramInt) {
    return (paramInt == 2 || paramInt == 5);
  }
  
  private static boolean isAlarmsStream(int paramInt) {
    boolean bool;
    if (paramInt == 4) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static boolean isMediaStream(int paramInt) {
    boolean bool;
    if (paramInt == 3) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setSeekBar(SeekBar paramSeekBar) {
    SeekBar seekBar = this.mSeekBar;
    if (seekBar != null)
      seekBar.setOnSeekBarChangeListener(null); 
    this.mSeekBar = paramSeekBar;
    paramSeekBar.setOnSeekBarChangeListener(null);
    this.mSeekBar.setMax(this.mMaxStreamVolume);
    updateSeekBar();
    this.mSeekBar.setOnSeekBarChangeListener(this);
  }
  
  private boolean isZenMuted() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mNotificationOrRing : Z
    //   4: istore_1
    //   5: iconst_1
    //   6: istore_2
    //   7: iload_1
    //   8: ifeq -> 19
    //   11: aload_0
    //   12: getfield mZenMode : I
    //   15: iconst_3
    //   16: if_icmpeq -> 99
    //   19: aload_0
    //   20: getfield mZenMode : I
    //   23: istore_3
    //   24: iload_3
    //   25: iconst_2
    //   26: if_icmpeq -> 99
    //   29: iload_3
    //   30: iconst_1
    //   31: if_icmpne -> 94
    //   34: aload_0
    //   35: getfield mAllowAlarms : Z
    //   38: ifne -> 53
    //   41: aload_0
    //   42: getfield mStreamType : I
    //   45: istore_3
    //   46: iload_3
    //   47: invokestatic isAlarmsStream : (I)Z
    //   50: ifne -> 99
    //   53: aload_0
    //   54: getfield mAllowMedia : Z
    //   57: ifne -> 72
    //   60: aload_0
    //   61: getfield mStreamType : I
    //   64: istore_3
    //   65: iload_3
    //   66: invokestatic isMediaStream : (I)Z
    //   69: ifne -> 99
    //   72: aload_0
    //   73: getfield mAllowRinger : Z
    //   76: ifne -> 94
    //   79: aload_0
    //   80: getfield mStreamType : I
    //   83: istore_3
    //   84: iload_3
    //   85: invokestatic isNotificationOrRing : (I)Z
    //   88: ifeq -> 94
    //   91: goto -> 99
    //   94: iconst_0
    //   95: istore_2
    //   96: goto -> 99
    //   99: iload_2
    //   100: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #249	-> 0
    //   #252	-> 46
    //   #253	-> 65
    //   #254	-> 84
    //   #249	-> 99
  }
  
  protected void updateSeekBar() {
    boolean bool = isZenMuted();
    this.mSeekBar.setEnabled(bool ^ true);
    if (bool) {
      this.mSeekBar.setProgress(this.mLastAudibleStreamVolume, true);
    } else if (this.mNotificationOrRing && this.mRingerMode == 1) {
      this.mSeekBar.setProgress(0, true);
    } else if (this.mMuted) {
      this.mSeekBar.setProgress(0, true);
    } else {
      SeekBar seekBar = this.mSeekBar;
      int i = this.mLastProgress;
      if (i <= -1)
        i = this.mOriginalStreamVolume; 
      seekBar.setProgress(i, true);
    } 
  }
  
  public boolean handleMessage(Message paramMessage) {
    int i = paramMessage.what;
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          if (i != 3) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("invalid SeekBarVolumizer message: ");
            stringBuilder.append(paramMessage.what);
            Log.e("SeekBarVolumizer", stringBuilder.toString());
          } else if (this.mPlaySample) {
            onInitSample();
          } 
        } else if (this.mPlaySample) {
          onStopSample();
        } 
      } else if (this.mPlaySample) {
        onStartSample();
      } 
    } else {
      if (this.mMuted && this.mLastProgress > 0) {
        this.mAudioManager.adjustStreamVolume(this.mStreamType, 100, 0);
      } else if (!this.mMuted && this.mLastProgress == 0) {
        this.mAudioManager.adjustStreamVolume(this.mStreamType, -100, 0);
      } 
      this.mAudioManager.setStreamVolume(this.mStreamType, this.mLastProgress, 1024);
    } 
    return true;
  }
  
  private void onInitSample() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mContext : Landroid/content/Context;
    //   6: aload_0
    //   7: getfield mDefaultUri : Landroid/net/Uri;
    //   10: invokestatic getRingtone : (Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;
    //   13: astore_1
    //   14: aload_0
    //   15: aload_1
    //   16: putfield mRingtone : Landroid/media/Ringtone;
    //   19: aload_1
    //   20: ifnull -> 31
    //   23: aload_1
    //   24: aload_0
    //   25: getfield mStreamType : I
    //   28: invokevirtual setStreamType : (I)V
    //   31: aload_0
    //   32: monitorexit
    //   33: return
    //   34: astore_1
    //   35: aload_0
    //   36: monitorexit
    //   37: aload_1
    //   38: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #305	-> 0
    //   #306	-> 2
    //   #307	-> 19
    //   #308	-> 23
    //   #310	-> 31
    //   #311	-> 33
    //   #310	-> 34
    // Exception table:
    //   from	to	target	type
    //   2	19	34	finally
    //   23	31	34	finally
    //   31	33	34	finally
    //   35	37	34	finally
  }
  
  private void postStartSample() {
    long l;
    Handler handler = this.mHandler;
    if (handler == null)
      return; 
    handler.removeMessages(1);
    handler = this.mHandler;
    Message message = handler.obtainMessage(1);
    if (isSamplePlaying()) {
      l = 1000L;
    } else {
      l = 0L;
    } 
    handler.sendMessageDelayed(message, l);
  }
  
  private void onStartSample() {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual isSamplePlaying : ()Z
    //   4: ifne -> 131
    //   7: aload_0
    //   8: getfield mCallback : Landroid/preference/SeekBarVolumizer$Callback;
    //   11: astore_1
    //   12: aload_1
    //   13: ifnull -> 23
    //   16: aload_1
    //   17: aload_0
    //   18: invokeinterface onSampleStarting : (Landroid/preference/SeekBarVolumizer;)V
    //   23: aload_0
    //   24: monitorenter
    //   25: aload_0
    //   26: getfield mRingtone : Landroid/media/Ringtone;
    //   29: astore_1
    //   30: aload_1
    //   31: ifnull -> 121
    //   34: aload_0
    //   35: getfield mRingtone : Landroid/media/Ringtone;
    //   38: astore_1
    //   39: new android/media/AudioAttributes$Builder
    //   42: astore_2
    //   43: aload_0
    //   44: getfield mRingtone : Landroid/media/Ringtone;
    //   47: astore_3
    //   48: aload_2
    //   49: aload_3
    //   50: invokevirtual getAudioAttributes : ()Landroid/media/AudioAttributes;
    //   53: invokespecial <init> : (Landroid/media/AudioAttributes;)V
    //   56: aload_2
    //   57: sipush #128
    //   60: invokevirtual setFlags : (I)Landroid/media/AudioAttributes$Builder;
    //   63: astore_2
    //   64: aload_2
    //   65: invokevirtual build : ()Landroid/media/AudioAttributes;
    //   68: astore_2
    //   69: aload_1
    //   70: aload_2
    //   71: invokevirtual setAudioAttributes : (Landroid/media/AudioAttributes;)V
    //   74: aload_0
    //   75: getfield mRingtone : Landroid/media/Ringtone;
    //   78: invokevirtual play : ()V
    //   81: goto -> 121
    //   84: astore_1
    //   85: new java/lang/StringBuilder
    //   88: astore_2
    //   89: aload_2
    //   90: invokespecial <init> : ()V
    //   93: aload_2
    //   94: ldc_w 'Error playing ringtone, stream '
    //   97: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   100: pop
    //   101: aload_2
    //   102: aload_0
    //   103: getfield mStreamType : I
    //   106: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   109: pop
    //   110: ldc 'SeekBarVolumizer'
    //   112: aload_2
    //   113: invokevirtual toString : ()Ljava/lang/String;
    //   116: aload_1
    //   117: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   120: pop
    //   121: aload_0
    //   122: monitorexit
    //   123: goto -> 131
    //   126: astore_1
    //   127: aload_0
    //   128: monitorexit
    //   129: aload_1
    //   130: athrow
    //   131: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #321	-> 0
    //   #322	-> 7
    //   #323	-> 16
    //   #326	-> 23
    //   #327	-> 25
    //   #329	-> 34
    //   #330	-> 48
    //   #331	-> 56
    //   #332	-> 64
    //   #329	-> 69
    //   #333	-> 74
    //   #336	-> 81
    //   #334	-> 84
    //   #335	-> 85
    //   #338	-> 121
    //   #340	-> 131
    // Exception table:
    //   from	to	target	type
    //   25	30	126	finally
    //   34	48	84	finally
    //   48	56	84	finally
    //   56	64	84	finally
    //   64	69	84	finally
    //   69	74	84	finally
    //   74	81	84	finally
    //   85	121	126	finally
    //   121	123	126	finally
    //   127	129	126	finally
  }
  
  private void postStopSample() {
    Handler handler = this.mHandler;
    if (handler == null)
      return; 
    handler.removeMessages(1);
    this.mHandler.removeMessages(2);
    handler = this.mHandler;
    handler.sendMessage(handler.obtainMessage(2));
  }
  
  private void onStopSample() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mRingtone : Landroid/media/Ringtone;
    //   6: ifnull -> 16
    //   9: aload_0
    //   10: getfield mRingtone : Landroid/media/Ringtone;
    //   13: invokevirtual stop : ()V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_1
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_1
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #351	-> 0
    //   #352	-> 2
    //   #353	-> 9
    //   #355	-> 16
    //   #356	-> 18
    //   #355	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	9	19	finally
    //   9	16	19	finally
    //   16	18	19	finally
    //   20	22	19	finally
  }
  
  public void stop() {
    if (this.mHandler == null)
      return; 
    postStopSample();
    this.mContext.getContentResolver().unregisterContentObserver(this.mVolumeObserver);
    this.mReceiver.setListening(false);
    if (hasAudioProductStrategies())
      unregisterVolumeGroupCb(); 
    this.mSeekBar.setOnSeekBarChangeListener(null);
    this.mHandler.getLooper().quitSafely();
    this.mHandler = null;
    this.mVolumeObserver = null;
  }
  
  public void start() {
    if (this.mHandler != null)
      return; 
    HandlerThread handlerThread = new HandlerThread("SeekBarVolumizer.CallbackHandler");
    handlerThread.start();
    Handler handler = new Handler(handlerThread.getLooper(), this);
    handler.sendEmptyMessage(3);
    this.mVolumeObserver = new Observer(this.mHandler);
    ContentResolver contentResolver = this.mContext.getContentResolver();
    String str = Settings.System.VOLUME_SETTINGS_INT[this.mStreamType];
    Uri uri = Settings.System.getUriFor(str);
    Observer observer = this.mVolumeObserver;
    contentResolver.registerContentObserver(uri, false, observer);
    this.mReceiver.setListening(true);
    if (hasAudioProductStrategies())
      registerVolumeGroupCb(); 
  }
  
  public void revertVolume() {
    this.mAudioManager.setStreamVolume(this.mStreamType, this.mOriginalStreamVolume, 0);
  }
  
  public void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean) {
    if (paramBoolean)
      postSetVolume(paramInt); 
    Callback callback = this.mCallback;
    if (callback != null)
      callback.onProgressChanged(paramSeekBar, paramInt, paramBoolean); 
  }
  
  private void postSetVolume(int paramInt) {
    Handler handler = this.mHandler;
    if (handler == null)
      return; 
    this.mLastProgress = paramInt;
    handler.removeMessages(0);
    handler = this.mHandler;
    handler.sendMessage(handler.obtainMessage(0));
  }
  
  public void onStartTrackingTouch(SeekBar paramSeekBar) {}
  
  public void onStopTrackingTouch(SeekBar paramSeekBar) {
    postStartSample();
  }
  
  public boolean isSamplePlaying() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mRingtone : Landroid/media/Ringtone;
    //   6: ifnull -> 24
    //   9: aload_0
    //   10: getfield mRingtone : Landroid/media/Ringtone;
    //   13: invokevirtual isPlaying : ()Z
    //   16: ifeq -> 24
    //   19: iconst_1
    //   20: istore_1
    //   21: goto -> 26
    //   24: iconst_0
    //   25: istore_1
    //   26: aload_0
    //   27: monitorexit
    //   28: iload_1
    //   29: ireturn
    //   30: astore_2
    //   31: aload_0
    //   32: monitorexit
    //   33: aload_2
    //   34: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #418	-> 0
    //   #419	-> 2
    //   #420	-> 30
    // Exception table:
    //   from	to	target	type
    //   2	19	30	finally
    //   26	28	30	finally
    //   31	33	30	finally
  }
  
  public void startSample() {
    postStartSample();
  }
  
  public void stopSample() {
    postStopSample();
  }
  
  public SeekBar getSeekBar() {
    return this.mSeekBar;
  }
  
  public void changeVolumeBy(int paramInt) {
    this.mSeekBar.incrementProgressBy(paramInt);
    postSetVolume(this.mSeekBar.getProgress());
    postStartSample();
    this.mVolumeBeforeMute = -1;
  }
  
  public void muteVolume() {
    int i = this.mVolumeBeforeMute;
    if (i != -1) {
      this.mSeekBar.setProgress(i, true);
      postSetVolume(this.mVolumeBeforeMute);
      postStartSample();
      this.mVolumeBeforeMute = -1;
    } else {
      this.mVolumeBeforeMute = this.mSeekBar.getProgress();
      this.mSeekBar.setProgress(0, true);
      postStopSample();
      postSetVolume(0);
    } 
  }
  
  public void onSaveInstanceState(VolumePreference.VolumeStore paramVolumeStore) {
    int i = this.mLastProgress;
    if (i >= 0) {
      paramVolumeStore.volume = i;
      paramVolumeStore.originalVolume = this.mOriginalStreamVolume;
    } 
  }
  
  public void onRestoreInstanceState(VolumePreference.VolumeStore paramVolumeStore) {
    if (paramVolumeStore.volume != -1) {
      this.mOriginalStreamVolume = paramVolumeStore.originalVolume;
      int i = paramVolumeStore.volume;
      postSetVolume(i);
    } 
  }
  
  class Callback {
    public abstract void onMuted(boolean param1Boolean1, boolean param1Boolean2);
    
    public abstract void onProgressChanged(SeekBar param1SeekBar, int param1Int, boolean param1Boolean);
    
    public abstract void onSampleStarting(SeekBarVolumizer param1SeekBarVolumizer);
  }
  
  private final class H extends Handler {
    private static final int UPDATE_SLIDER = 1;
    
    final SeekBarVolumizer this$0;
    
    private H() {}
    
    public void handleMessage(Message param1Message) {
      if (param1Message.what == 1 && 
        SeekBarVolumizer.this.mSeekBar != null) {
        SeekBarVolumizer.access$602(SeekBarVolumizer.this, param1Message.arg1);
        SeekBarVolumizer.access$702(SeekBarVolumizer.this, param1Message.arg2);
        boolean bool = ((Boolean)param1Message.obj).booleanValue();
        if (bool != SeekBarVolumizer.this.mMuted) {
          SeekBarVolumizer.access$802(SeekBarVolumizer.this, bool);
          if (SeekBarVolumizer.this.mCallback != null)
            SeekBarVolumizer.this.mCallback.onMuted(SeekBarVolumizer.this.mMuted, SeekBarVolumizer.this.isZenMuted()); 
        } 
        SeekBarVolumizer.this.updateSeekBar();
      } 
    }
    
    public void postUpdateSlider(int param1Int1, int param1Int2, boolean param1Boolean) {
      obtainMessage(1, param1Int1, param1Int2, new Boolean(param1Boolean)).sendToTarget();
    }
  }
  
  private void updateSlider() {
    if (this.mSeekBar != null) {
      AudioManager audioManager = this.mAudioManager;
      if (audioManager != null) {
        int i = audioManager.getStreamVolume(this.mStreamType);
        int j = this.mAudioManager.getLastAudibleStreamVolume(this.mStreamType);
        boolean bool = this.mAudioManager.isStreamMute(this.mStreamType);
        this.mUiHandler.postUpdateSlider(i, j, bool);
      } 
    } 
  }
  
  private final class Observer extends ContentObserver {
    final SeekBarVolumizer this$0;
    
    public Observer(Handler param1Handler) {
      super(param1Handler);
    }
    
    public void onChange(boolean param1Boolean) {
      super.onChange(param1Boolean);
      SeekBarVolumizer.this.updateSlider();
    }
  }
  
  private final class Receiver extends BroadcastReceiver {
    private boolean mListening;
    
    final SeekBarVolumizer this$0;
    
    private Receiver() {}
    
    public void setListening(boolean param1Boolean) {
      if (this.mListening == param1Boolean)
        return; 
      this.mListening = param1Boolean;
      if (param1Boolean) {
        IntentFilter intentFilter = new IntentFilter("android.media.VOLUME_CHANGED_ACTION");
        intentFilter.addAction("android.media.INTERNAL_RINGER_MODE_CHANGED_ACTION");
        intentFilter.addAction("android.app.action.INTERRUPTION_FILTER_CHANGED");
        intentFilter.addAction("android.app.action.NOTIFICATION_POLICY_CHANGED");
        intentFilter.addAction("android.media.STREAM_DEVICES_CHANGED_ACTION");
        SeekBarVolumizer.this.mContext.registerReceiver(this, intentFilter);
      } else {
        SeekBarVolumizer.this.mContext.unregisterReceiver(this);
      } 
    }
    
    public void onReceive(Context param1Context, Intent param1Intent) {
      String str = param1Intent.getAction();
      if ("android.media.VOLUME_CHANGED_ACTION".equals(str)) {
        int i = param1Intent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_TYPE", -1);
        int j = param1Intent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_VALUE", -1);
        if (SeekBarVolumizer.this.hasAudioProductStrategies())
          updateVolumeSlider(i, j); 
      } else {
        SeekBarVolumizer seekBarVolumizer;
        if ("android.media.INTERNAL_RINGER_MODE_CHANGED_ACTION".equals(str)) {
          if (SeekBarVolumizer.this.mNotificationOrRing) {
            seekBarVolumizer = SeekBarVolumizer.this;
            SeekBarVolumizer.access$1502(seekBarVolumizer, seekBarVolumizer.mAudioManager.getRingerModeInternal());
          } 
          if (SeekBarVolumizer.this.mAffectedByRingerMode)
            SeekBarVolumizer.this.updateSlider(); 
        } else if ("android.media.STREAM_DEVICES_CHANGED_ACTION".equals(seekBarVolumizer)) {
          int i = param1Intent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_TYPE", -1);
          if (SeekBarVolumizer.this.hasAudioProductStrategies()) {
            int j = SeekBarVolumizer.this.mAudioManager.getStreamVolume(i);
            updateVolumeSlider(i, j);
          } else {
            int j = SeekBarVolumizer.this.getVolumeGroupIdForLegacyStreamType(i);
            if (j != -1) {
              seekBarVolumizer = SeekBarVolumizer.this;
              if (j == seekBarVolumizer.mVolumeGroupId) {
                j = SeekBarVolumizer.this.mAudioManager.getStreamVolume(i);
                updateVolumeSlider(i, j);
              } 
            } 
          } 
        } else if ("android.app.action.INTERRUPTION_FILTER_CHANGED".equals(seekBarVolumizer)) {
          seekBarVolumizer = SeekBarVolumizer.this;
          SeekBarVolumizer.access$2002(seekBarVolumizer, seekBarVolumizer.mNotificationManager.getZenMode());
          SeekBarVolumizer.this.updateSlider();
        } else if ("android.app.action.NOTIFICATION_POLICY_CHANGED".equals(seekBarVolumizer)) {
          seekBarVolumizer = SeekBarVolumizer.this;
          SeekBarVolumizer.access$2202(seekBarVolumizer, seekBarVolumizer.mNotificationManager.getConsolidatedNotificationPolicy());
          seekBarVolumizer = SeekBarVolumizer.this;
          int i = seekBarVolumizer.mNotificationPolicy.priorityCategories;
          boolean bool1 = false;
          if ((i & 0x20) != 0) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          SeekBarVolumizer.access$2302(seekBarVolumizer, bool2);
          seekBarVolumizer = SeekBarVolumizer.this;
          boolean bool2 = bool1;
          if ((seekBarVolumizer.mNotificationPolicy.priorityCategories & 0x40) != 0)
            bool2 = true; 
          SeekBarVolumizer.access$2402(seekBarVolumizer, bool2);
          seekBarVolumizer = SeekBarVolumizer.this;
          NotificationManager.Policy policy = seekBarVolumizer.mNotificationPolicy;
          SeekBarVolumizer.access$2502(seekBarVolumizer, ZenModeConfig.areAllPriorityOnlyRingerSoundsMuted(policy) ^ true);
          SeekBarVolumizer.this.updateSlider();
        } 
      } 
    }
    
    private void updateVolumeSlider(int param1Int1, int param1Int2) {
      boolean bool = SeekBarVolumizer.this.mNotificationOrRing;
      boolean bool1 = true;
      if (bool) {
        bool = SeekBarVolumizer.isNotificationOrRing(param1Int1);
      } else if (param1Int1 == SeekBarVolumizer.this.mStreamType) {
        bool = true;
      } else {
        bool = false;
      } 
      if (SeekBarVolumizer.this.mSeekBar != null && bool && param1Int2 != -1) {
        bool = bool1;
        if (!SeekBarVolumizer.this.mAudioManager.isStreamMute(SeekBarVolumizer.this.mStreamType))
          if (param1Int2 == 0) {
            bool = bool1;
          } else {
            bool = false;
          }  
        SeekBarVolumizer.this.mUiHandler.postUpdateSlider(param1Int2, SeekBarVolumizer.this.mLastAudibleStreamVolume, bool);
      } 
    }
  }
  
  private void registerVolumeGroupCb() {
    if (this.mVolumeGroupId != -1) {
      this.mAudioManager.registerVolumeGroupCallback((Executor)_$$Lambda$_14QHG018Z6p13d3hzJuGTWnNeo.INSTANCE, this.mVolumeGroupCallback);
      updateSlider();
    } 
  }
  
  private void unregisterVolumeGroupCb() {
    if (this.mVolumeGroupId != -1)
      this.mAudioManager.unregisterVolumeGroupCallback(this.mVolumeGroupCallback); 
  }
  
  private class VolumeHandler extends Handler {
    final SeekBarVolumizer this$0;
    
    private VolumeHandler() {}
    
    public void handleMessage(Message param1Message) {
      SomeArgs someArgs = (SomeArgs)param1Message.obj;
      if (param1Message.what == 1) {
        int i = ((Integer)someArgs.arg1).intValue();
        if (SeekBarVolumizer.this.mVolumeGroupId == i) {
          SeekBarVolumizer seekBarVolumizer = SeekBarVolumizer.this;
          if (seekBarVolumizer.mVolumeGroupId == -1)
            return; 
          SeekBarVolumizer.this.updateSlider();
          return;
        } 
        return;
      } 
    }
  }
}
