package android.preference;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.media.RingtoneManager;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.android.internal.R;

@Deprecated
public class RingtonePreference extends Preference implements PreferenceManager.OnActivityResultListener {
  private static final String TAG = "RingtonePreference";
  
  private int mRequestCode;
  
  private int mRingtoneType;
  
  private boolean mShowDefault;
  
  private boolean mShowSilent;
  
  public RingtonePreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.RingtonePreference, paramInt1, paramInt2);
    this.mRingtoneType = typedArray.getInt(0, 1);
    this.mShowDefault = typedArray.getBoolean(1, true);
    this.mShowSilent = typedArray.getBoolean(2, true);
    typedArray.recycle();
  }
  
  public RingtonePreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public RingtonePreference(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842899);
  }
  
  public RingtonePreference(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public int getRingtoneType() {
    return this.mRingtoneType;
  }
  
  public void setRingtoneType(int paramInt) {
    this.mRingtoneType = paramInt;
  }
  
  public boolean getShowDefault() {
    return this.mShowDefault;
  }
  
  public void setShowDefault(boolean paramBoolean) {
    this.mShowDefault = paramBoolean;
  }
  
  public boolean getShowSilent() {
    return this.mShowSilent;
  }
  
  public void setShowSilent(boolean paramBoolean) {
    this.mShowSilent = paramBoolean;
  }
  
  protected void onClick() {
    Intent intent = new Intent("android.intent.action.RINGTONE_PICKER");
    onPrepareRingtonePickerIntent(intent);
    PreferenceFragment preferenceFragment = getPreferenceManager().getFragment();
    if (preferenceFragment != null) {
      preferenceFragment.startActivityForResult(intent, this.mRequestCode);
    } else {
      getPreferenceManager().getActivity().startActivityForResult(intent, this.mRequestCode);
    } 
  }
  
  protected void onPrepareRingtonePickerIntent(Intent paramIntent) {
    Uri uri = onRestoreRingtone();
    paramIntent.putExtra("android.intent.extra.ringtone.EXISTING_URI", uri);
    paramIntent.putExtra("android.intent.extra.ringtone.SHOW_DEFAULT", this.mShowDefault);
    if (this.mShowDefault) {
      uri = RingtoneManager.getDefaultUri(getRingtoneType());
      paramIntent.putExtra("android.intent.extra.ringtone.DEFAULT_URI", uri);
    } 
    paramIntent.putExtra("android.intent.extra.ringtone.SHOW_SILENT", this.mShowSilent);
    paramIntent.putExtra("android.intent.extra.ringtone.TYPE", this.mRingtoneType);
    paramIntent.putExtra("android.intent.extra.ringtone.TITLE", getTitle());
    paramIntent.putExtra("android.intent.extra.ringtone.AUDIO_ATTRIBUTES_FLAGS", 64);
  }
  
  protected void onSaveRingtone(Uri paramUri) {
    String str;
    if (paramUri != null) {
      str = paramUri.toString();
    } else {
      str = "";
    } 
    persistString(str);
  }
  
  protected Uri onRestoreRingtone() {
    Uri uri = null;
    String str = getPersistedString(null);
    if (!TextUtils.isEmpty(str))
      uri = Uri.parse(str); 
    return uri;
  }
  
  protected Object onGetDefaultValue(TypedArray paramTypedArray, int paramInt) {
    return paramTypedArray.getString(paramInt);
  }
  
  protected void onSetInitialValue(boolean paramBoolean, Object paramObject) {
    paramObject = paramObject;
    if (paramBoolean)
      return; 
    if (!TextUtils.isEmpty((CharSequence)paramObject))
      onSaveRingtone(Uri.parse((String)paramObject)); 
  }
  
  protected void onAttachedToHierarchy(PreferenceManager paramPreferenceManager) {
    super.onAttachedToHierarchy(paramPreferenceManager);
    paramPreferenceManager.registerOnActivityResultListener(this);
    this.mRequestCode = paramPreferenceManager.getNextRequestCode();
  }
  
  public boolean onActivityResult(int paramInt1, int paramInt2, Intent paramIntent) {
    if (paramInt1 == this.mRequestCode) {
      if (paramIntent != null) {
        String str;
        Uri uri = (Uri)paramIntent.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI");
        if (uri != null) {
          str = uri.toString();
        } else {
          str = "";
        } 
        if (callChangeListener(str))
          onSaveRingtone(uri); 
      } 
      return true;
    } 
    return false;
  }
}
