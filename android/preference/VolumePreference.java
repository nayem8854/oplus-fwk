package android.preference;

import android.app.Dialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.SeekBar;
import com.android.internal.R;

@Deprecated
public class VolumePreference extends SeekBarDialogPreference implements PreferenceManager.OnActivityStopListener, View.OnKeyListener, SeekBarVolumizer.Callback {
  private SeekBarVolumizer mSeekBarVolumizer;
  
  private int mStreamType;
  
  public VolumePreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.VolumePreference, paramInt1, paramInt2);
    this.mStreamType = typedArray.getInt(0, 0);
    typedArray.recycle();
  }
  
  public VolumePreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public VolumePreference(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 17957070);
  }
  
  public VolumePreference(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public void setStreamType(int paramInt) {
    this.mStreamType = paramInt;
  }
  
  protected void onBindDialogView(View paramView) {
    super.onBindDialogView(paramView);
    SeekBar seekBar = (SeekBar)paramView.findViewById(16909403);
    SeekBarVolumizer seekBarVolumizer = new SeekBarVolumizer(getContext(), this.mStreamType, null, this);
    seekBarVolumizer.start();
    this.mSeekBarVolumizer.setSeekBar(seekBar);
    getPreferenceManager().registerOnActivityStopListener(this);
    paramView.setOnKeyListener(this);
    paramView.setFocusableInTouchMode(true);
    paramView.requestFocus();
  }
  
  public boolean onKey(View paramView, int paramInt, KeyEvent paramKeyEvent) {
    boolean bool;
    if (this.mSeekBarVolumizer == null)
      return true; 
    if (paramKeyEvent.getAction() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (paramInt != 24) {
      if (paramInt != 25) {
        if (paramInt != 164)
          return false; 
        if (bool)
          this.mSeekBarVolumizer.muteVolume(); 
        return true;
      } 
      if (bool)
        this.mSeekBarVolumizer.changeVolumeBy(-1); 
      return true;
    } 
    if (bool)
      this.mSeekBarVolumizer.changeVolumeBy(1); 
    return true;
  }
  
  protected void onDialogClosed(boolean paramBoolean) {
    super.onDialogClosed(paramBoolean);
    if (!paramBoolean) {
      SeekBarVolumizer seekBarVolumizer = this.mSeekBarVolumizer;
      if (seekBarVolumizer != null)
        seekBarVolumizer.revertVolume(); 
    } 
    cleanup();
  }
  
  public void onActivityStop() {
    SeekBarVolumizer seekBarVolumizer = this.mSeekBarVolumizer;
    if (seekBarVolumizer != null)
      seekBarVolumizer.stopSample(); 
  }
  
  private void cleanup() {
    getPreferenceManager().unregisterOnActivityStopListener(this);
    if (this.mSeekBarVolumizer != null) {
      Dialog dialog = getDialog();
      if (dialog != null && dialog.isShowing()) {
        View view = dialog.getWindow().getDecorView().findViewById(16909403);
        if (view != null)
          view.setOnKeyListener(null); 
        this.mSeekBarVolumizer.revertVolume();
      } 
      this.mSeekBarVolumizer.stop();
      this.mSeekBarVolumizer = null;
    } 
  }
  
  public void onSampleStarting(SeekBarVolumizer paramSeekBarVolumizer) {
    SeekBarVolumizer seekBarVolumizer = this.mSeekBarVolumizer;
    if (seekBarVolumizer != null && paramSeekBarVolumizer != seekBarVolumizer)
      seekBarVolumizer.stopSample(); 
  }
  
  public void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean) {}
  
  public void onMuted(boolean paramBoolean1, boolean paramBoolean2) {}
  
  protected Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    if (isPersistent())
      return parcelable; 
    SavedState savedState = new SavedState(parcelable);
    SeekBarVolumizer seekBarVolumizer = this.mSeekBarVolumizer;
    if (seekBarVolumizer != null)
      seekBarVolumizer.onSaveInstanceState(savedState.getVolumeStore()); 
    return (Parcelable)savedState;
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable) {
    if (paramParcelable == null || !paramParcelable.getClass().equals(SavedState.class)) {
      super.onRestoreInstanceState(paramParcelable);
      return;
    } 
    SavedState savedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(savedState.getSuperState());
    SeekBarVolumizer seekBarVolumizer = this.mSeekBarVolumizer;
    if (seekBarVolumizer != null)
      seekBarVolumizer.onRestoreInstanceState(savedState.getVolumeStore()); 
  }
  
  class VolumeStore {
    public int volume = -1;
    
    public int originalVolume = -1;
  }
  
  private static class SavedState extends Preference.BaseSavedState {
    public SavedState(Parcel param1Parcel) {
      super(param1Parcel);
      VolumePreference.VolumeStore volumeStore = new VolumePreference.VolumeStore();
      volumeStore.volume = param1Parcel.readInt();
      this.mVolumeStore.originalVolume = param1Parcel.readInt();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeInt(this.mVolumeStore.volume);
      param1Parcel.writeInt(this.mVolumeStore.originalVolume);
    }
    
    VolumePreference.VolumeStore getVolumeStore() {
      return this.mVolumeStore;
    }
    
    public SavedState(Parcelable param1Parcelable) {
      super(param1Parcelable);
      this.mVolumeStore = new VolumePreference.VolumeStore();
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    VolumePreference.VolumeStore mVolumeStore;
  }
}
