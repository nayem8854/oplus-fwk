package android.preference;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import com.android.internal.R;
import java.util.HashSet;
import java.util.Set;

@Deprecated
public class MultiSelectListPreference extends DialogPreference {
  private Set<String> mValues = new HashSet<>();
  
  private boolean mPreferenceChanged;
  
  private Set<String> mNewValues = new HashSet<>();
  
  private CharSequence[] mEntryValues;
  
  private CharSequence[] mEntries;
  
  public MultiSelectListPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.MultiSelectListPreference, paramInt1, paramInt2);
    this.mEntries = typedArray.getTextArray(0);
    this.mEntryValues = typedArray.getTextArray(1);
    typedArray.recycle();
  }
  
  public MultiSelectListPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public MultiSelectListPreference(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842897);
  }
  
  public MultiSelectListPreference(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public void setEntries(CharSequence[] paramArrayOfCharSequence) {
    this.mEntries = paramArrayOfCharSequence;
  }
  
  public void setEntries(int paramInt) {
    setEntries(getContext().getResources().getTextArray(paramInt));
  }
  
  public CharSequence[] getEntries() {
    return this.mEntries;
  }
  
  public void setEntryValues(CharSequence[] paramArrayOfCharSequence) {
    this.mEntryValues = paramArrayOfCharSequence;
  }
  
  public void setEntryValues(int paramInt) {
    setEntryValues(getContext().getResources().getTextArray(paramInt));
  }
  
  public CharSequence[] getEntryValues() {
    return this.mEntryValues;
  }
  
  public void setValues(Set<String> paramSet) {
    this.mValues.clear();
    this.mValues.addAll(paramSet);
    persistStringSet(paramSet);
  }
  
  public Set<String> getValues() {
    return this.mValues;
  }
  
  public int findIndexOfValue(String paramString) {
    if (paramString != null) {
      CharSequence[] arrayOfCharSequence = this.mEntryValues;
      if (arrayOfCharSequence != null)
        for (int i = arrayOfCharSequence.length - 1; i >= 0; i--) {
          if (this.mEntryValues[i].equals(paramString))
            return i; 
        }  
    } 
    return -1;
  }
  
  protected void onPrepareDialogBuilder(AlertDialog.Builder paramBuilder) {
    super.onPrepareDialogBuilder(paramBuilder);
    if (this.mEntries != null && this.mEntryValues != null) {
      boolean[] arrayOfBoolean = getSelectedItems();
      paramBuilder.setMultiChoiceItems(this.mEntries, arrayOfBoolean, (DialogInterface.OnMultiChoiceClickListener)new Object(this));
      this.mNewValues.clear();
      this.mNewValues.addAll(this.mValues);
      return;
    } 
    throw new IllegalStateException("MultiSelectListPreference requires an entries array and an entryValues array.");
  }
  
  private boolean[] getSelectedItems() {
    CharSequence[] arrayOfCharSequence = this.mEntryValues;
    int i = arrayOfCharSequence.length;
    Set<String> set = this.mValues;
    boolean[] arrayOfBoolean = new boolean[i];
    for (byte b = 0; b < i; b++)
      arrayOfBoolean[b] = set.contains(arrayOfCharSequence[b].toString()); 
    return arrayOfBoolean;
  }
  
  protected void onDialogClosed(boolean paramBoolean) {
    super.onDialogClosed(paramBoolean);
    if (paramBoolean && this.mPreferenceChanged) {
      Set<String> set = this.mNewValues;
      if (callChangeListener(set))
        setValues(set); 
    } 
    this.mPreferenceChanged = false;
  }
  
  protected Object onGetDefaultValue(TypedArray paramTypedArray, int paramInt) {
    CharSequence[] arrayOfCharSequence = paramTypedArray.getTextArray(paramInt);
    int i = arrayOfCharSequence.length;
    HashSet<String> hashSet = new HashSet();
    for (paramInt = 0; paramInt < i; paramInt++)
      hashSet.add(arrayOfCharSequence[paramInt].toString()); 
    return hashSet;
  }
  
  protected void onSetInitialValue(boolean paramBoolean, Object<String> paramObject) {
    if (paramBoolean) {
      paramObject = (Object<String>)getPersistedStringSet(this.mValues);
    } else {
      paramObject = paramObject;
    } 
    setValues((Set<String>)paramObject);
  }
  
  protected Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    if (isPersistent())
      return parcelable; 
    SavedState savedState = new SavedState(parcelable);
    savedState.values = getValues();
    return (Parcelable)savedState;
  }
  
  class SavedState extends Preference.BaseSavedState {
    public SavedState(MultiSelectListPreference this$0) {
      super((Parcel)this$0);
      this.values = new HashSet<>();
      String[] arrayOfString = this$0.readStringArray();
      int i = arrayOfString.length;
      for (byte b = 0; b < i; b++)
        this.values.add(arrayOfString[b]); 
    }
    
    public SavedState(MultiSelectListPreference this$0) {
      super((Parcelable)this$0);
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeStringArray(this.values.<String>toArray(new String[0]));
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    Set<String> values;
  }
}
