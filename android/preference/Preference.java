package android.preference;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.AbsSavedState;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.internal.R;
import com.android.internal.util.CharSequences;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Deprecated
public class Preference implements Comparable<Preference> {
  private int mOrder = Integer.MAX_VALUE;
  
  private boolean mEnabled = true;
  
  private boolean mSelectable = true;
  
  private boolean mPersistent = true;
  
  private boolean mDependencyMet = true;
  
  private boolean mParentDependencyMet = true;
  
  private boolean mRecycleEnabled = true;
  
  private boolean mSingleLineTitle = true;
  
  private boolean mShouldDisableView = true;
  
  private int mLayoutResId = 17367237;
  
  public static final int DEFAULT_ORDER = 2147483647;
  
  private boolean mBaseMethodCalled;
  
  private Context mContext;
  
  private Object mDefaultValue;
  
  private String mDependencyKey;
  
  private List<Preference> mDependents;
  
  private Bundle mExtras;
  
  private String mFragment;
  
  private boolean mHasSingleLineTitleAttr;
  
  private Drawable mIcon;
  
  private int mIconResId;
  
  private boolean mIconSpaceReserved;
  
  private long mId;
  
  private Intent mIntent;
  
  private String mKey;
  
  private OnPreferenceChangeInternalListener mListener;
  
  private OnPreferenceChangeListener mOnChangeListener;
  
  private OnPreferenceClickListener mOnClickListener;
  
  private PreferenceGroup mParentGroup;
  
  private PreferenceDataStore mPreferenceDataStore;
  
  private PreferenceManager mPreferenceManager;
  
  private boolean mRequiresKey;
  
  private CharSequence mSummary;
  
  private CharSequence mTitle;
  
  private int mTitleRes;
  
  private int mWidgetLayoutResId;
  
  public Preference(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    this.mContext = paramContext;
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Preference, paramInt1, paramInt2);
    for (paramInt1 = typedArray.getIndexCount() - 1; paramInt1 >= 0; paramInt1--) {
      paramInt2 = typedArray.getIndex(paramInt1);
      switch (paramInt2) {
        case 16:
          this.mIconSpaceReserved = typedArray.getBoolean(paramInt2, this.mIconSpaceReserved);
          break;
        case 15:
          this.mSingleLineTitle = typedArray.getBoolean(paramInt2, this.mSingleLineTitle);
          this.mHasSingleLineTitleAttr = true;
          break;
        case 14:
          this.mRecycleEnabled = typedArray.getBoolean(paramInt2, this.mRecycleEnabled);
          break;
        case 13:
          this.mFragment = typedArray.getString(paramInt2);
          break;
        case 12:
          this.mShouldDisableView = typedArray.getBoolean(paramInt2, this.mShouldDisableView);
          break;
        case 11:
          this.mDefaultValue = onGetDefaultValue(typedArray, paramInt2);
          break;
        case 10:
          this.mDependencyKey = typedArray.getString(paramInt2);
          break;
        case 9:
          this.mWidgetLayoutResId = typedArray.getResourceId(paramInt2, this.mWidgetLayoutResId);
          break;
        case 8:
          this.mOrder = typedArray.getInt(paramInt2, this.mOrder);
          break;
        case 7:
          this.mSummary = typedArray.getText(paramInt2);
          break;
        case 6:
          this.mKey = typedArray.getString(paramInt2);
          break;
        case 5:
          this.mSelectable = typedArray.getBoolean(paramInt2, true);
          break;
        case 4:
          this.mTitleRes = typedArray.getResourceId(paramInt2, 0);
          this.mTitle = typedArray.getText(paramInt2);
          break;
        case 3:
          this.mLayoutResId = typedArray.getResourceId(paramInt2, this.mLayoutResId);
          break;
        case 2:
          this.mEnabled = typedArray.getBoolean(paramInt2, true);
          break;
        case 1:
          this.mPersistent = typedArray.getBoolean(paramInt2, this.mPersistent);
          break;
        case 0:
          this.mIconResId = typedArray.getResourceId(paramInt2, 0);
          break;
      } 
    } 
    typedArray.recycle();
  }
  
  public Preference(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public Preference(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842894);
  }
  
  public Preference(Context paramContext) {
    this(paramContext, null);
  }
  
  protected Object onGetDefaultValue(TypedArray paramTypedArray, int paramInt) {
    return null;
  }
  
  public void setIntent(Intent paramIntent) {
    this.mIntent = paramIntent;
  }
  
  public Intent getIntent() {
    return this.mIntent;
  }
  
  public void setFragment(String paramString) {
    this.mFragment = paramString;
  }
  
  public String getFragment() {
    return this.mFragment;
  }
  
  public void setPreferenceDataStore(PreferenceDataStore paramPreferenceDataStore) {
    this.mPreferenceDataStore = paramPreferenceDataStore;
  }
  
  public PreferenceDataStore getPreferenceDataStore() {
    PreferenceDataStore preferenceDataStore = this.mPreferenceDataStore;
    if (preferenceDataStore != null)
      return preferenceDataStore; 
    PreferenceManager preferenceManager = this.mPreferenceManager;
    if (preferenceManager != null)
      return preferenceManager.getPreferenceDataStore(); 
    return null;
  }
  
  public Bundle getExtras() {
    if (this.mExtras == null)
      this.mExtras = new Bundle(); 
    return this.mExtras;
  }
  
  public Bundle peekExtras() {
    return this.mExtras;
  }
  
  public void setLayoutResource(int paramInt) {
    if (paramInt != this.mLayoutResId)
      this.mRecycleEnabled = false; 
    this.mLayoutResId = paramInt;
  }
  
  public int getLayoutResource() {
    return this.mLayoutResId;
  }
  
  public void setWidgetLayoutResource(int paramInt) {
    if (paramInt != this.mWidgetLayoutResId)
      this.mRecycleEnabled = false; 
    this.mWidgetLayoutResId = paramInt;
  }
  
  public int getWidgetLayoutResource() {
    return this.mWidgetLayoutResId;
  }
  
  public View getView(View paramView, ViewGroup paramViewGroup) {
    View view = paramView;
    if (paramView == null)
      view = onCreateView(paramViewGroup); 
    onBindView(view);
    return view;
  }
  
  protected View onCreateView(ViewGroup paramViewGroup) {
    Context context = this.mContext;
    LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService("layout_inflater");
    View view = layoutInflater.inflate(this.mLayoutResId, paramViewGroup, false);
    paramViewGroup = (ViewGroup)view.findViewById(16908312);
    if (paramViewGroup != null) {
      int i = this.mWidgetLayoutResId;
      if (i != 0) {
        layoutInflater.inflate(i, paramViewGroup);
      } else {
        paramViewGroup.setVisibility(8);
      } 
    } 
    return view;
  }
  
  protected void onBindView(View paramView) {
    TextView textView = (TextView)paramView.findViewById(16908310);
    byte b = 8;
    if (textView != null) {
      CharSequence charSequence = getTitle();
      if (!TextUtils.isEmpty(charSequence)) {
        textView.setText(charSequence);
        textView.setVisibility(0);
        if (this.mHasSingleLineTitleAttr)
          textView.setSingleLine(this.mSingleLineTitle); 
      } else {
        textView.setVisibility(8);
      } 
    } 
    textView = (TextView)paramView.findViewById(16908304);
    if (textView != null) {
      CharSequence charSequence = getSummary();
      if (!TextUtils.isEmpty(charSequence)) {
        textView.setText(charSequence);
        textView.setVisibility(0);
      } else {
        textView.setVisibility(8);
      } 
    } 
    ImageView imageView = (ImageView)paramView.findViewById(16908294);
    if (imageView != null) {
      if (this.mIconResId != 0 || this.mIcon != null) {
        if (this.mIcon == null)
          this.mIcon = getContext().getDrawable(this.mIconResId); 
        Drawable drawable = this.mIcon;
        if (drawable != null)
          imageView.setImageDrawable(drawable); 
      } 
      if (this.mIcon != null) {
        imageView.setVisibility(0);
      } else {
        byte b1;
        if (this.mIconSpaceReserved) {
          b1 = 4;
        } else {
          b1 = 8;
        } 
        imageView.setVisibility(b1);
      } 
    } 
    View view = paramView.findViewById(16908350);
    if (view != null)
      if (this.mIcon != null) {
        view.setVisibility(0);
      } else {
        byte b1 = b;
        if (this.mIconSpaceReserved)
          b1 = 4; 
        view.setVisibility(b1);
      }  
    if (this.mShouldDisableView)
      setEnabledStateOnViews(paramView, isEnabled()); 
  }
  
  private void setEnabledStateOnViews(View paramView, boolean paramBoolean) {
    paramView.setEnabled(paramBoolean);
    if (paramView instanceof ViewGroup) {
      ViewGroup viewGroup = (ViewGroup)paramView;
      for (int i = viewGroup.getChildCount() - 1; i >= 0; i--)
        setEnabledStateOnViews(viewGroup.getChildAt(i), paramBoolean); 
    } 
  }
  
  public void setOrder(int paramInt) {
    if (paramInt != this.mOrder) {
      this.mOrder = paramInt;
      notifyHierarchyChanged();
    } 
  }
  
  public int getOrder() {
    return this.mOrder;
  }
  
  public void setTitle(CharSequence paramCharSequence) {
    if ((paramCharSequence == null && this.mTitle != null) || (paramCharSequence != null && !paramCharSequence.equals(this.mTitle))) {
      this.mTitleRes = 0;
      this.mTitle = paramCharSequence;
      notifyChanged();
    } 
  }
  
  public void setTitle(int paramInt) {
    setTitle(this.mContext.getString(paramInt));
    this.mTitleRes = paramInt;
  }
  
  public int getTitleRes() {
    return this.mTitleRes;
  }
  
  public CharSequence getTitle() {
    return this.mTitle;
  }
  
  public void setIcon(Drawable paramDrawable) {
    if ((paramDrawable == null && this.mIcon != null) || (paramDrawable != null && this.mIcon != paramDrawable)) {
      this.mIcon = paramDrawable;
      notifyChanged();
    } 
  }
  
  public void setIcon(int paramInt) {
    if (this.mIconResId != paramInt) {
      this.mIconResId = paramInt;
      setIcon(this.mContext.getDrawable(paramInt));
    } 
  }
  
  public Drawable getIcon() {
    if (this.mIcon == null && this.mIconResId != 0)
      this.mIcon = getContext().getDrawable(this.mIconResId); 
    return this.mIcon;
  }
  
  public CharSequence getSummary() {
    return this.mSummary;
  }
  
  public void setSummary(CharSequence paramCharSequence) {
    if ((paramCharSequence == null && this.mSummary != null) || (paramCharSequence != null && !paramCharSequence.equals(this.mSummary))) {
      this.mSummary = paramCharSequence;
      notifyChanged();
    } 
  }
  
  public void setSummary(int paramInt) {
    setSummary(this.mContext.getString(paramInt));
  }
  
  public void setEnabled(boolean paramBoolean) {
    if (this.mEnabled != paramBoolean) {
      this.mEnabled = paramBoolean;
      notifyDependencyChange(shouldDisableDependents());
      notifyChanged();
    } 
  }
  
  public boolean isEnabled() {
    boolean bool;
    if (this.mEnabled && this.mDependencyMet && this.mParentDependencyMet) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setSelectable(boolean paramBoolean) {
    if (this.mSelectable != paramBoolean) {
      this.mSelectable = paramBoolean;
      notifyChanged();
    } 
  }
  
  public boolean isSelectable() {
    return this.mSelectable;
  }
  
  public void setShouldDisableView(boolean paramBoolean) {
    this.mShouldDisableView = paramBoolean;
    notifyChanged();
  }
  
  public boolean getShouldDisableView() {
    return this.mShouldDisableView;
  }
  
  public void setRecycleEnabled(boolean paramBoolean) {
    this.mRecycleEnabled = paramBoolean;
    notifyChanged();
  }
  
  public boolean isRecycleEnabled() {
    return this.mRecycleEnabled;
  }
  
  public void setSingleLineTitle(boolean paramBoolean) {
    this.mHasSingleLineTitleAttr = true;
    this.mSingleLineTitle = paramBoolean;
    notifyChanged();
  }
  
  public boolean isSingleLineTitle() {
    return this.mSingleLineTitle;
  }
  
  public void setIconSpaceReserved(boolean paramBoolean) {
    this.mIconSpaceReserved = paramBoolean;
    notifyChanged();
  }
  
  public boolean isIconSpaceReserved() {
    return this.mIconSpaceReserved;
  }
  
  long getId() {
    return this.mId;
  }
  
  protected void onClick() {}
  
  public void setKey(String paramString) {
    this.mKey = paramString;
    if (this.mRequiresKey && !hasKey())
      requireKey(); 
  }
  
  public String getKey() {
    return this.mKey;
  }
  
  void requireKey() {
    if (this.mKey != null) {
      this.mRequiresKey = true;
      return;
    } 
    throw new IllegalStateException("Preference does not have a key assigned.");
  }
  
  public boolean hasKey() {
    return TextUtils.isEmpty(this.mKey) ^ true;
  }
  
  public boolean isPersistent() {
    return this.mPersistent;
  }
  
  protected boolean shouldPersist() {
    boolean bool;
    if (this.mPreferenceManager != null && isPersistent() && hasKey()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setPersistent(boolean paramBoolean) {
    this.mPersistent = paramBoolean;
  }
  
  protected boolean callChangeListener(Object paramObject) {
    OnPreferenceChangeListener onPreferenceChangeListener = this.mOnChangeListener;
    return (onPreferenceChangeListener == null || onPreferenceChangeListener.onPreferenceChange(this, paramObject));
  }
  
  public void setOnPreferenceChangeListener(OnPreferenceChangeListener paramOnPreferenceChangeListener) {
    this.mOnChangeListener = paramOnPreferenceChangeListener;
  }
  
  public OnPreferenceChangeListener getOnPreferenceChangeListener() {
    return this.mOnChangeListener;
  }
  
  public void setOnPreferenceClickListener(OnPreferenceClickListener paramOnPreferenceClickListener) {
    this.mOnClickListener = paramOnPreferenceClickListener;
  }
  
  public OnPreferenceClickListener getOnPreferenceClickListener() {
    return this.mOnClickListener;
  }
  
  public void performClick(PreferenceScreen paramPreferenceScreen) {
    if (!isEnabled())
      return; 
    onClick();
    OnPreferenceClickListener onPreferenceClickListener = this.mOnClickListener;
    if (onPreferenceClickListener != null && onPreferenceClickListener.onPreferenceClick(this))
      return; 
    PreferenceManager preferenceManager = getPreferenceManager();
    if (preferenceManager != null) {
      PreferenceManager.OnPreferenceTreeClickListener onPreferenceTreeClickListener = preferenceManager.getOnPreferenceTreeClickListener();
      if (paramPreferenceScreen != null && onPreferenceTreeClickListener != null && 
        onPreferenceTreeClickListener.onPreferenceTreeClick(paramPreferenceScreen, this))
        return; 
    } 
    if (this.mIntent != null) {
      Context context = getContext();
      context.startActivity(this.mIntent);
    } 
  }
  
  public boolean onKey(View paramView, int paramInt, KeyEvent paramKeyEvent) {
    return false;
  }
  
  public Context getContext() {
    return this.mContext;
  }
  
  public SharedPreferences getSharedPreferences() {
    if (this.mPreferenceManager == null || getPreferenceDataStore() != null)
      return null; 
    return this.mPreferenceManager.getSharedPreferences();
  }
  
  public SharedPreferences.Editor getEditor() {
    if (this.mPreferenceManager == null || getPreferenceDataStore() != null)
      return null; 
    return this.mPreferenceManager.getEditor();
  }
  
  public boolean shouldCommit() {
    PreferenceManager preferenceManager = this.mPreferenceManager;
    if (preferenceManager == null)
      return false; 
    return preferenceManager.shouldCommit();
  }
  
  public int compareTo(Preference paramPreference) {
    int i = this.mOrder, j = paramPreference.mOrder;
    if (i != j)
      return i - j; 
    CharSequence charSequence2 = this.mTitle, charSequence1 = paramPreference.mTitle;
    if (charSequence2 == charSequence1)
      return 0; 
    if (charSequence2 == null)
      return 1; 
    if (charSequence1 == null)
      return -1; 
    return CharSequences.compareToIgnoreCase(charSequence2, charSequence1);
  }
  
  final void setOnPreferenceChangeInternalListener(OnPreferenceChangeInternalListener paramOnPreferenceChangeInternalListener) {
    this.mListener = paramOnPreferenceChangeInternalListener;
  }
  
  protected void notifyChanged() {
    OnPreferenceChangeInternalListener onPreferenceChangeInternalListener = this.mListener;
    if (onPreferenceChangeInternalListener != null)
      onPreferenceChangeInternalListener.onPreferenceChange(this); 
  }
  
  protected void notifyHierarchyChanged() {
    OnPreferenceChangeInternalListener onPreferenceChangeInternalListener = this.mListener;
    if (onPreferenceChangeInternalListener != null)
      onPreferenceChangeInternalListener.onPreferenceHierarchyChange(this); 
  }
  
  public PreferenceManager getPreferenceManager() {
    return this.mPreferenceManager;
  }
  
  protected void onAttachedToHierarchy(PreferenceManager paramPreferenceManager) {
    this.mPreferenceManager = paramPreferenceManager;
    this.mId = paramPreferenceManager.getNextId();
    dispatchSetInitialValue();
  }
  
  protected void onAttachedToActivity() {
    registerDependency();
  }
  
  void assignParent(PreferenceGroup paramPreferenceGroup) {
    this.mParentGroup = paramPreferenceGroup;
  }
  
  private void registerDependency() {
    if (TextUtils.isEmpty(this.mDependencyKey))
      return; 
    Preference preference = findPreferenceInHierarchy(this.mDependencyKey);
    if (preference != null) {
      preference.registerDependent(this);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Dependency \"");
    stringBuilder.append(this.mDependencyKey);
    stringBuilder.append("\" not found for preference \"");
    stringBuilder.append(this.mKey);
    stringBuilder.append("\" (title: \"");
    stringBuilder.append(this.mTitle);
    stringBuilder.append("\"");
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  private void unregisterDependency() {
    String str = this.mDependencyKey;
    if (str != null) {
      Preference preference = findPreferenceInHierarchy(str);
      if (preference != null)
        preference.unregisterDependent(this); 
    } 
  }
  
  protected Preference findPreferenceInHierarchy(String paramString) {
    if (!TextUtils.isEmpty(paramString)) {
      PreferenceManager preferenceManager = this.mPreferenceManager;
      if (preferenceManager != null)
        return preferenceManager.findPreference(paramString); 
    } 
    return null;
  }
  
  private void registerDependent(Preference paramPreference) {
    if (this.mDependents == null)
      this.mDependents = new ArrayList<>(); 
    this.mDependents.add(paramPreference);
    paramPreference.onDependencyChanged(this, shouldDisableDependents());
  }
  
  private void unregisterDependent(Preference paramPreference) {
    List<Preference> list = this.mDependents;
    if (list != null)
      list.remove(paramPreference); 
  }
  
  public void notifyDependencyChange(boolean paramBoolean) {
    List<Preference> list = this.mDependents;
    if (list == null)
      return; 
    int i = list.size();
    for (byte b = 0; b < i; b++)
      ((Preference)list.get(b)).onDependencyChanged(this, paramBoolean); 
  }
  
  public void onDependencyChanged(Preference paramPreference, boolean paramBoolean) {
    if (this.mDependencyMet == paramBoolean) {
      this.mDependencyMet = paramBoolean ^ true;
      notifyDependencyChange(shouldDisableDependents());
      notifyChanged();
    } 
  }
  
  public void onParentChanged(Preference paramPreference, boolean paramBoolean) {
    if (this.mParentDependencyMet == paramBoolean) {
      this.mParentDependencyMet = paramBoolean ^ true;
      notifyDependencyChange(shouldDisableDependents());
      notifyChanged();
    } 
  }
  
  public boolean shouldDisableDependents() {
    return isEnabled() ^ true;
  }
  
  public void setDependency(String paramString) {
    unregisterDependency();
    this.mDependencyKey = paramString;
    registerDependency();
  }
  
  public String getDependency() {
    return this.mDependencyKey;
  }
  
  public PreferenceGroup getParent() {
    return this.mParentGroup;
  }
  
  protected void onPrepareForRemoval() {
    unregisterDependency();
  }
  
  public void setDefaultValue(Object paramObject) {
    this.mDefaultValue = paramObject;
  }
  
  private void dispatchSetInitialValue() {
    if (getPreferenceDataStore() != null) {
      onSetInitialValue(true, this.mDefaultValue);
      return;
    } 
    boolean bool = shouldPersist();
    if (!bool || !getSharedPreferences().contains(this.mKey)) {
      Object object = this.mDefaultValue;
      if (object != null)
        onSetInitialValue(false, object); 
      return;
    } 
    onSetInitialValue(true, null);
  }
  
  protected void onSetInitialValue(boolean paramBoolean, Object paramObject) {}
  
  private void tryCommit(SharedPreferences.Editor paramEditor) {
    if (this.mPreferenceManager.shouldCommit())
      try {
        paramEditor.apply();
      } catch (AbstractMethodError abstractMethodError) {
        paramEditor.commit();
      }  
  }
  
  protected boolean persistString(String paramString) {
    if (!shouldPersist())
      return false; 
    if (TextUtils.equals(paramString, getPersistedString(null)))
      return true; 
    PreferenceDataStore preferenceDataStore = getPreferenceDataStore();
    if (preferenceDataStore != null) {
      preferenceDataStore.putString(this.mKey, paramString);
    } else {
      SharedPreferences.Editor editor = this.mPreferenceManager.getEditor();
      editor.putString(this.mKey, paramString);
      tryCommit(editor);
    } 
    return true;
  }
  
  protected String getPersistedString(String paramString) {
    if (!shouldPersist())
      return paramString; 
    PreferenceDataStore preferenceDataStore = getPreferenceDataStore();
    if (preferenceDataStore != null)
      return preferenceDataStore.getString(this.mKey, paramString); 
    return this.mPreferenceManager.getSharedPreferences().getString(this.mKey, paramString);
  }
  
  public boolean persistStringSet(Set<String> paramSet) {
    if (!shouldPersist())
      return false; 
    if (paramSet.equals(getPersistedStringSet(null)))
      return true; 
    PreferenceDataStore preferenceDataStore = getPreferenceDataStore();
    if (preferenceDataStore != null) {
      preferenceDataStore.putStringSet(this.mKey, paramSet);
    } else {
      SharedPreferences.Editor editor = this.mPreferenceManager.getEditor();
      editor.putStringSet(this.mKey, paramSet);
      tryCommit(editor);
    } 
    return true;
  }
  
  public Set<String> getPersistedStringSet(Set<String> paramSet) {
    if (!shouldPersist())
      return paramSet; 
    PreferenceDataStore preferenceDataStore = getPreferenceDataStore();
    if (preferenceDataStore != null)
      return preferenceDataStore.getStringSet(this.mKey, paramSet); 
    return this.mPreferenceManager.getSharedPreferences().getStringSet(this.mKey, paramSet);
  }
  
  protected boolean persistInt(int paramInt) {
    if (!shouldPersist())
      return false; 
    if (paramInt == getPersistedInt(paramInt ^ 0xFFFFFFFF))
      return true; 
    PreferenceDataStore preferenceDataStore = getPreferenceDataStore();
    if (preferenceDataStore != null) {
      preferenceDataStore.putInt(this.mKey, paramInt);
    } else {
      SharedPreferences.Editor editor = this.mPreferenceManager.getEditor();
      editor.putInt(this.mKey, paramInt);
      tryCommit(editor);
    } 
    return true;
  }
  
  protected int getPersistedInt(int paramInt) {
    if (!shouldPersist())
      return paramInt; 
    PreferenceDataStore preferenceDataStore = getPreferenceDataStore();
    if (preferenceDataStore != null)
      return preferenceDataStore.getInt(this.mKey, paramInt); 
    return this.mPreferenceManager.getSharedPreferences().getInt(this.mKey, paramInt);
  }
  
  protected boolean persistFloat(float paramFloat) {
    if (!shouldPersist())
      return false; 
    if (paramFloat == getPersistedFloat(Float.NaN))
      return true; 
    PreferenceDataStore preferenceDataStore = getPreferenceDataStore();
    if (preferenceDataStore != null) {
      preferenceDataStore.putFloat(this.mKey, paramFloat);
    } else {
      SharedPreferences.Editor editor = this.mPreferenceManager.getEditor();
      editor.putFloat(this.mKey, paramFloat);
      tryCommit(editor);
    } 
    return true;
  }
  
  protected float getPersistedFloat(float paramFloat) {
    if (!shouldPersist())
      return paramFloat; 
    PreferenceDataStore preferenceDataStore = getPreferenceDataStore();
    if (preferenceDataStore != null)
      return preferenceDataStore.getFloat(this.mKey, paramFloat); 
    return this.mPreferenceManager.getSharedPreferences().getFloat(this.mKey, paramFloat);
  }
  
  protected boolean persistLong(long paramLong) {
    if (!shouldPersist())
      return false; 
    if (paramLong == getPersistedLong(paramLong ^ 0xFFFFFFFFFFFFFFFFL))
      return true; 
    PreferenceDataStore preferenceDataStore = getPreferenceDataStore();
    if (preferenceDataStore != null) {
      preferenceDataStore.putLong(this.mKey, paramLong);
    } else {
      SharedPreferences.Editor editor = this.mPreferenceManager.getEditor();
      editor.putLong(this.mKey, paramLong);
      tryCommit(editor);
    } 
    return true;
  }
  
  protected long getPersistedLong(long paramLong) {
    if (!shouldPersist())
      return paramLong; 
    PreferenceDataStore preferenceDataStore = getPreferenceDataStore();
    if (preferenceDataStore != null)
      return preferenceDataStore.getLong(this.mKey, paramLong); 
    return this.mPreferenceManager.getSharedPreferences().getLong(this.mKey, paramLong);
  }
  
  protected boolean persistBoolean(boolean paramBoolean) {
    if (!shouldPersist())
      return false; 
    if (paramBoolean == getPersistedBoolean(paramBoolean ^ true))
      return true; 
    PreferenceDataStore preferenceDataStore = getPreferenceDataStore();
    if (preferenceDataStore != null) {
      preferenceDataStore.putBoolean(this.mKey, paramBoolean);
    } else {
      SharedPreferences.Editor editor = this.mPreferenceManager.getEditor();
      editor.putBoolean(this.mKey, paramBoolean);
      tryCommit(editor);
    } 
    return true;
  }
  
  protected boolean getPersistedBoolean(boolean paramBoolean) {
    if (!shouldPersist())
      return paramBoolean; 
    PreferenceDataStore preferenceDataStore = getPreferenceDataStore();
    if (preferenceDataStore != null)
      return preferenceDataStore.getBoolean(this.mKey, paramBoolean); 
    return this.mPreferenceManager.getSharedPreferences().getBoolean(this.mKey, paramBoolean);
  }
  
  public String toString() {
    return getFilterableStringBuilder().toString();
  }
  
  StringBuilder getFilterableStringBuilder() {
    StringBuilder stringBuilder = new StringBuilder();
    CharSequence charSequence = getTitle();
    if (!TextUtils.isEmpty(charSequence)) {
      stringBuilder.append(charSequence);
      stringBuilder.append(' ');
    } 
    charSequence = getSummary();
    if (!TextUtils.isEmpty(charSequence)) {
      stringBuilder.append(charSequence);
      stringBuilder.append(' ');
    } 
    if (stringBuilder.length() > 0)
      stringBuilder.setLength(stringBuilder.length() - 1); 
    return stringBuilder;
  }
  
  public void saveHierarchyState(Bundle paramBundle) {
    dispatchSaveInstanceState(paramBundle);
  }
  
  void dispatchSaveInstanceState(Bundle paramBundle) {
    if (hasKey()) {
      this.mBaseMethodCalled = false;
      Parcelable parcelable = onSaveInstanceState();
      if (this.mBaseMethodCalled) {
        if (parcelable != null)
          paramBundle.putParcelable(this.mKey, parcelable); 
      } else {
        throw new IllegalStateException("Derived class did not call super.onSaveInstanceState()");
      } 
    } 
  }
  
  protected Parcelable onSaveInstanceState() {
    this.mBaseMethodCalled = true;
    return (Parcelable)BaseSavedState.EMPTY_STATE;
  }
  
  public void restoreHierarchyState(Bundle paramBundle) {
    dispatchRestoreInstanceState(paramBundle);
  }
  
  void dispatchRestoreInstanceState(Bundle paramBundle) {
    if (hasKey()) {
      paramBundle = paramBundle.getParcelable(this.mKey);
      if (paramBundle != null) {
        this.mBaseMethodCalled = false;
        onRestoreInstanceState(paramBundle);
        if (!this.mBaseMethodCalled)
          throw new IllegalStateException("Derived class did not call super.onRestoreInstanceState()"); 
      } 
    } 
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable) {
    this.mBaseMethodCalled = true;
    if (paramParcelable == BaseSavedState.EMPTY_STATE || paramParcelable == null)
      return; 
    throw new IllegalArgumentException("Wrong state class -- expecting Preference State");
  }
  
  @Deprecated
  class BaseSavedState extends AbsSavedState {
    public BaseSavedState(Preference this$0) {
      super((Parcel)this$0);
    }
    
    public BaseSavedState(Preference this$0) {
      super((Parcelable)this$0);
    }
    
    public static final Parcelable.Creator<BaseSavedState> CREATOR = (Parcelable.Creator<BaseSavedState>)new Object();
  }
  
  static interface OnPreferenceChangeInternalListener {
    void onPreferenceChange(Preference param1Preference);
    
    void onPreferenceHierarchyChange(Preference param1Preference);
  }
  
  @Deprecated
  public static interface OnPreferenceChangeListener {
    boolean onPreferenceChange(Preference param1Preference, Object param1Object);
  }
  
  @Deprecated
  public static interface OnPreferenceClickListener {
    boolean onPreferenceClick(Preference param1Preference);
  }
}
