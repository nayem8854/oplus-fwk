package android.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.SeekBar;
import com.android.internal.R;

@Deprecated
public class SeekBarPreference extends Preference implements SeekBar.OnSeekBarChangeListener {
  private int mMax;
  
  private int mProgress;
  
  private boolean mTrackingTouch;
  
  public SeekBarPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray2 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ProgressBar, paramInt1, paramInt2);
    setMax(typedArray2.getInt(2, this.mMax));
    typedArray2.recycle();
    TypedArray typedArray1 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.SeekBarPreference, paramInt1, paramInt2);
    paramInt1 = typedArray1.getResourceId(0, 17367260);
    typedArray1.recycle();
    setLayoutResource(paramInt1);
  }
  
  public SeekBarPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public SeekBarPreference(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 17957071);
  }
  
  public SeekBarPreference(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  protected void onBindView(View paramView) {
    super.onBindView(paramView);
    SeekBar seekBar = (SeekBar)paramView.findViewById(16909403);
    seekBar.setOnSeekBarChangeListener(this);
    seekBar.setMax(this.mMax);
    seekBar.setProgress(this.mProgress);
    seekBar.setEnabled(isEnabled());
  }
  
  protected void onSetInitialValue(boolean paramBoolean, Object paramObject) {
    int i;
    if (paramBoolean) {
      i = getPersistedInt(this.mProgress);
    } else {
      i = ((Integer)paramObject).intValue();
    } 
    setProgress(i);
  }
  
  protected Object onGetDefaultValue(TypedArray paramTypedArray, int paramInt) {
    return Integer.valueOf(paramTypedArray.getInt(paramInt, 0));
  }
  
  public boolean onKey(View paramView, int paramInt, KeyEvent paramKeyEvent) {
    if (paramKeyEvent.getAction() != 0)
      return false; 
    SeekBar seekBar = (SeekBar)paramView.findViewById(16909403);
    if (seekBar == null)
      return false; 
    return seekBar.onKeyDown(paramInt, paramKeyEvent);
  }
  
  public void setMax(int paramInt) {
    if (paramInt != this.mMax) {
      this.mMax = paramInt;
      notifyChanged();
    } 
  }
  
  public void setProgress(int paramInt) {
    setProgress(paramInt, true);
  }
  
  private void setProgress(int paramInt, boolean paramBoolean) {
    int i = paramInt;
    if (paramInt > this.mMax)
      i = this.mMax; 
    paramInt = i;
    if (i < 0)
      paramInt = 0; 
    if (paramInt != this.mProgress) {
      this.mProgress = paramInt;
      persistInt(paramInt);
      if (paramBoolean)
        notifyChanged(); 
    } 
  }
  
  public int getProgress() {
    return this.mProgress;
  }
  
  void syncProgress(SeekBar paramSeekBar) {
    int i = paramSeekBar.getProgress();
    if (i != this.mProgress)
      if (callChangeListener(Integer.valueOf(i))) {
        setProgress(i, false);
      } else {
        paramSeekBar.setProgress(this.mProgress);
      }  
  }
  
  public void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean) {
    if (paramBoolean && !this.mTrackingTouch)
      syncProgress(paramSeekBar); 
  }
  
  public void onStartTrackingTouch(SeekBar paramSeekBar) {
    this.mTrackingTouch = true;
  }
  
  public void onStopTrackingTouch(SeekBar paramSeekBar) {
    this.mTrackingTouch = false;
    if (paramSeekBar.getProgress() != this.mProgress)
      syncProgress(paramSeekBar); 
  }
  
  protected Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    if (isPersistent())
      return parcelable; 
    SavedState savedState = new SavedState(parcelable);
    savedState.progress = this.mProgress;
    savedState.max = this.mMax;
    return (Parcelable)savedState;
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable) {
    if (!paramParcelable.getClass().equals(SavedState.class)) {
      super.onRestoreInstanceState(paramParcelable);
      return;
    } 
    SavedState savedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(savedState.getSuperState());
    this.mProgress = savedState.progress;
    this.mMax = savedState.max;
    notifyChanged();
  }
  
  class SavedState extends Preference.BaseSavedState {
    public SavedState(SeekBarPreference this$0) {
      super((Parcel)this$0);
      this.progress = this$0.readInt();
      this.max = this$0.readInt();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeInt(this.progress);
      param1Parcel.writeInt(this.max);
    }
    
    public SavedState(SeekBarPreference this$0) {
      super((Parcelable)this$0);
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
        public SeekBarPreference.SavedState createFromParcel(Parcel param1Parcel) {
          return new SeekBarPreference.SavedState(param1Parcel);
        }
        
        public SeekBarPreference.SavedState[] newArray(int param1Int) {
          return new SeekBarPreference.SavedState[param1Int];
        }
      };
    
    int max;
    
    int progress;
  }
  
  class null implements Parcelable.Creator<SavedState> {
    public SeekBarPreference.SavedState createFromParcel(Parcel param1Parcel) {
      return new SeekBarPreference.SavedState(param1Parcel);
    }
    
    public SeekBarPreference.SavedState[] newArray(int param1Int) {
      return new SeekBarPreference.SavedState[param1Int];
    }
  }
}
