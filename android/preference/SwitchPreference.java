package android.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;
import android.widget.CompoundButton;
import android.widget.Switch;
import com.android.internal.R;

@Deprecated
public class SwitchPreference extends TwoStatePreference {
  private final Listener mListener = new Listener();
  
  private CharSequence mSwitchOff;
  
  private CharSequence mSwitchOn;
  
  class Listener implements CompoundButton.OnCheckedChangeListener {
    final SwitchPreference this$0;
    
    private Listener() {}
    
    public void onCheckedChanged(CompoundButton param1CompoundButton, boolean param1Boolean) {
      if (!SwitchPreference.this.callChangeListener(Boolean.valueOf(param1Boolean))) {
        param1CompoundButton.setChecked(param1Boolean ^ true);
        return;
      } 
      SwitchPreference.this.setChecked(param1Boolean);
    }
  }
  
  public SwitchPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.SwitchPreference, paramInt1, paramInt2);
    setSummaryOn(typedArray.getString(0));
    setSummaryOff(typedArray.getString(1));
    setSwitchTextOn(typedArray.getString(3));
    setSwitchTextOff(typedArray.getString(4));
    setDisableDependentsState(typedArray.getBoolean(2, false));
    typedArray.recycle();
  }
  
  public SwitchPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public SwitchPreference(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16843629);
  }
  
  public SwitchPreference(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  protected void onBindView(View paramView) {
    super.onBindView(paramView);
    View view = paramView.findViewById(16908352);
    if (view != null && view instanceof Checkable) {
      if (view instanceof Switch) {
        Switch switch_ = (Switch)view;
        switch_.setOnCheckedChangeListener(null);
      } 
      ((Checkable)view).setChecked(this.mChecked);
      if (view instanceof Switch) {
        Switch switch_ = (Switch)view;
        switch_.setTextOn(this.mSwitchOn);
        switch_.setTextOff(this.mSwitchOff);
        switch_.setOnCheckedChangeListener(this.mListener);
      } 
    } 
    syncSummaryView(paramView);
  }
  
  public void setSwitchTextOn(CharSequence paramCharSequence) {
    this.mSwitchOn = paramCharSequence;
    notifyChanged();
  }
  
  public void setSwitchTextOff(CharSequence paramCharSequence) {
    this.mSwitchOff = paramCharSequence;
    notifyChanged();
  }
  
  public void setSwitchTextOn(int paramInt) {
    setSwitchTextOn(getContext().getString(paramInt));
  }
  
  public void setSwitchTextOff(int paramInt) {
    setSwitchTextOff(getContext().getString(paramInt));
  }
  
  public CharSequence getSwitchTextOn() {
    return this.mSwitchOn;
  }
  
  public CharSequence getSwitchTextOff() {
    return this.mSwitchOff;
  }
}
