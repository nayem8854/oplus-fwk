package android.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

@Deprecated
public abstract class TwoStatePreference extends Preference {
  boolean mChecked;
  
  private boolean mCheckedSet;
  
  private boolean mDisableDependentsState;
  
  private CharSequence mSummaryOff;
  
  private CharSequence mSummaryOn;
  
  public TwoStatePreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  public TwoStatePreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public TwoStatePreference(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public TwoStatePreference(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  protected void onClick() {
    super.onClick();
    int i = isChecked() ^ true;
    if (callChangeListener(Boolean.valueOf(i)))
      setChecked(i); 
  }
  
  public void setChecked(boolean paramBoolean) {
    boolean bool;
    if (this.mChecked != paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool || !this.mCheckedSet) {
      this.mChecked = paramBoolean;
      this.mCheckedSet = true;
      persistBoolean(paramBoolean);
      if (bool) {
        notifyDependencyChange(shouldDisableDependents());
        notifyChanged();
      } 
    } 
  }
  
  public boolean isChecked() {
    return this.mChecked;
  }
  
  public boolean shouldDisableDependents() {
    boolean bool = this.mDisableDependentsState;
    boolean bool1 = true;
    if (bool) {
      bool = this.mChecked;
    } else if (!this.mChecked) {
      bool = true;
    } else {
      bool = false;
    } 
    boolean bool2 = bool1;
    if (!bool)
      if (super.shouldDisableDependents()) {
        bool2 = bool1;
      } else {
        bool2 = false;
      }  
    return bool2;
  }
  
  public void setSummaryOn(CharSequence paramCharSequence) {
    this.mSummaryOn = paramCharSequence;
    if (isChecked())
      notifyChanged(); 
  }
  
  public void setSummaryOn(int paramInt) {
    setSummaryOn(getContext().getString(paramInt));
  }
  
  public CharSequence getSummaryOn() {
    return this.mSummaryOn;
  }
  
  public void setSummaryOff(CharSequence paramCharSequence) {
    this.mSummaryOff = paramCharSequence;
    if (!isChecked())
      notifyChanged(); 
  }
  
  public void setSummaryOff(int paramInt) {
    setSummaryOff(getContext().getString(paramInt));
  }
  
  public CharSequence getSummaryOff() {
    return this.mSummaryOff;
  }
  
  public boolean getDisableDependentsState() {
    return this.mDisableDependentsState;
  }
  
  public void setDisableDependentsState(boolean paramBoolean) {
    this.mDisableDependentsState = paramBoolean;
  }
  
  protected Object onGetDefaultValue(TypedArray paramTypedArray, int paramInt) {
    return Boolean.valueOf(paramTypedArray.getBoolean(paramInt, false));
  }
  
  protected void onSetInitialValue(boolean paramBoolean, Object paramObject) {
    if (paramBoolean) {
      paramBoolean = getPersistedBoolean(this.mChecked);
    } else {
      paramBoolean = ((Boolean)paramObject).booleanValue();
    } 
    setChecked(paramBoolean);
  }
  
  void syncSummaryView(View paramView) {
    TextView textView = (TextView)paramView.findViewById(16908304);
    if (textView != null) {
      byte b1 = 1;
      if (this.mChecked && !TextUtils.isEmpty(this.mSummaryOn)) {
        textView.setText(this.mSummaryOn);
        b2 = 0;
      } else {
        b2 = b1;
        if (!this.mChecked) {
          b2 = b1;
          if (!TextUtils.isEmpty(this.mSummaryOff)) {
            textView.setText(this.mSummaryOff);
            b2 = 0;
          } 
        } 
      } 
      b1 = b2;
      if (b2) {
        CharSequence charSequence = getSummary();
        b1 = b2;
        if (!TextUtils.isEmpty(charSequence)) {
          textView.setText(charSequence);
          b1 = 0;
        } 
      } 
      byte b2 = 8;
      if (b1 == 0)
        b2 = 0; 
      if (b2 != textView.getVisibility())
        textView.setVisibility(b2); 
    } 
  }
  
  protected Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    if (isPersistent())
      return parcelable; 
    SavedState savedState = new SavedState(parcelable);
    savedState.checked = isChecked();
    return (Parcelable)savedState;
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable) {
    if (paramParcelable == null || !paramParcelable.getClass().equals(SavedState.class)) {
      super.onRestoreInstanceState(paramParcelable);
      return;
    } 
    SavedState savedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(savedState.getSuperState());
    setChecked(savedState.checked);
  }
  
  class SavedState extends Preference.BaseSavedState {
    public SavedState(TwoStatePreference this$0) {
      super((Parcel)this$0);
      int i = this$0.readInt();
      boolean bool = true;
      if (i != 1)
        bool = false; 
      this.checked = bool;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeInt(this.checked);
    }
    
    public SavedState(TwoStatePreference this$0) {
      super((Parcelable)this$0);
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
        public TwoStatePreference.SavedState createFromParcel(Parcel param1Parcel) {
          return new TwoStatePreference.SavedState(param1Parcel);
        }
        
        public TwoStatePreference.SavedState[] newArray(int param1Int) {
          return new TwoStatePreference.SavedState[param1Int];
        }
      };
    
    boolean checked;
  }
  
  class null implements Parcelable.Creator<SavedState> {
    public TwoStatePreference.SavedState createFromParcel(Parcel param1Parcel) {
      return new TwoStatePreference.SavedState(param1Parcel);
    }
    
    public TwoStatePreference.SavedState[] newArray(int param1Int) {
      return new TwoStatePreference.SavedState[param1Int];
    }
  }
}
