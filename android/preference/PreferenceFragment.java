package android.preference;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import com.android.internal.R;

@Deprecated
public abstract class PreferenceFragment extends Fragment implements PreferenceManager.OnPreferenceTreeClickListener {
  private int mLayoutResId = 17367256;
  
  private Handler mHandler = (Handler)new Object(this);
  
  private final Runnable mRequestFocus = (Runnable)new Object(this);
  
  public void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    PreferenceManager preferenceManager = new PreferenceManager(getActivity(), 100);
    preferenceManager.setFragment(this);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle) {
    TypedArray typedArray = getActivity().obtainStyledAttributes(null, R.styleable.PreferenceFragment, 16844038, 0);
    this.mLayoutResId = typedArray.getResourceId(0, this.mLayoutResId);
    typedArray.recycle();
    return paramLayoutInflater.inflate(this.mLayoutResId, paramViewGroup, false);
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle) {
    super.onViewCreated(paramView, paramBundle);
    TypedArray typedArray = getActivity().obtainStyledAttributes(null, R.styleable.PreferenceFragment, 16844038, 0);
    ListView listView = (ListView)paramView.findViewById(16908298);
    if (listView != null && 
      typedArray.hasValueOrEmpty(1)) {
      Drawable drawable = typedArray.getDrawable(1);
      listView.setDivider(drawable);
    } 
    typedArray.recycle();
  }
  
  public void onActivityCreated(Bundle paramBundle) {
    super.onActivityCreated(paramBundle);
    if (this.mHavePrefs)
      bindPreferences(); 
    this.mInitDone = true;
    if (paramBundle != null) {
      paramBundle = paramBundle.getBundle("android:preferences");
      if (paramBundle != null) {
        PreferenceScreen preferenceScreen = getPreferenceScreen();
        if (preferenceScreen != null)
          preferenceScreen.restoreHierarchyState(paramBundle); 
      } 
    } 
  }
  
  public void onStart() {
    super.onStart();
    this.mPreferenceManager.setOnPreferenceTreeClickListener(this);
  }
  
  public void onStop() {
    super.onStop();
    this.mPreferenceManager.dispatchActivityStop();
    this.mPreferenceManager.setOnPreferenceTreeClickListener(null);
  }
  
  public void onDestroyView() {
    ListView listView = this.mList;
    if (listView != null)
      listView.setOnKeyListener(null); 
    this.mList = null;
    this.mHandler.removeCallbacks(this.mRequestFocus);
    this.mHandler.removeMessages(1);
    super.onDestroyView();
  }
  
  public void onDestroy() {
    super.onDestroy();
    this.mPreferenceManager.dispatchActivityDestroy();
  }
  
  public void onSaveInstanceState(Bundle paramBundle) {
    super.onSaveInstanceState(paramBundle);
    PreferenceScreen preferenceScreen = getPreferenceScreen();
    if (preferenceScreen != null) {
      Bundle bundle = new Bundle();
      preferenceScreen.saveHierarchyState(bundle);
      paramBundle.putBundle("android:preferences", bundle);
    } 
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent) {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    this.mPreferenceManager.dispatchActivityResult(paramInt1, paramInt2, paramIntent);
  }
  
  public PreferenceManager getPreferenceManager() {
    return this.mPreferenceManager;
  }
  
  public void setPreferenceScreen(PreferenceScreen paramPreferenceScreen) {
    if (this.mPreferenceManager.setPreferences(paramPreferenceScreen) && paramPreferenceScreen != null) {
      onUnbindPreferences();
      this.mHavePrefs = true;
      if (this.mInitDone)
        postBindPreferences(); 
    } 
  }
  
  public PreferenceScreen getPreferenceScreen() {
    return this.mPreferenceManager.getPreferenceScreen();
  }
  
  public void addPreferencesFromIntent(Intent paramIntent) {
    requirePreferenceManager();
    setPreferenceScreen(this.mPreferenceManager.inflateFromIntent(paramIntent, getPreferenceScreen()));
  }
  
  public void addPreferencesFromResource(int paramInt) {
    requirePreferenceManager();
    PreferenceManager preferenceManager = this.mPreferenceManager;
    Activity activity = getActivity();
    PreferenceScreen preferenceScreen = getPreferenceScreen();
    setPreferenceScreen(preferenceManager.inflateFromResource((Context)activity, paramInt, preferenceScreen));
  }
  
  public boolean onPreferenceTreeClick(PreferenceScreen paramPreferenceScreen, Preference paramPreference) {
    if (paramPreference.getFragment() != null && 
      getActivity() instanceof OnPreferenceStartFragmentCallback)
      return ((OnPreferenceStartFragmentCallback)getActivity()).onPreferenceStartFragment(this, paramPreference); 
    return false;
  }
  
  public Preference findPreference(CharSequence paramCharSequence) {
    PreferenceManager preferenceManager = this.mPreferenceManager;
    if (preferenceManager == null)
      return null; 
    return preferenceManager.findPreference(paramCharSequence);
  }
  
  private void requirePreferenceManager() {
    if (this.mPreferenceManager != null)
      return; 
    throw new RuntimeException("This should be called after super.onCreate.");
  }
  
  private void postBindPreferences() {
    if (this.mHandler.hasMessages(1))
      return; 
    this.mHandler.obtainMessage(1).sendToTarget();
  }
  
  private void bindPreferences() {
    PreferenceScreen preferenceScreen = getPreferenceScreen();
    if (preferenceScreen != null) {
      View view = getView();
      if (view != null) {
        View view1 = view.findViewById(16908310);
        if (view1 instanceof TextView) {
          CharSequence charSequence = preferenceScreen.getTitle();
          if (TextUtils.isEmpty(charSequence)) {
            view1.setVisibility(8);
          } else {
            ((TextView)view1).setText(charSequence);
            view1.setVisibility(0);
          } 
        } 
      } 
      preferenceScreen.bind(getListView());
    } 
    onBindPreferences();
  }
  
  protected void onBindPreferences() {}
  
  protected void onUnbindPreferences() {}
  
  public ListView getListView() {
    ensureList();
    return this.mList;
  }
  
  public boolean hasListView() {
    if (this.mList != null)
      return true; 
    View view = getView();
    if (view == null)
      return false; 
    view = view.findViewById(16908298);
    if (!(view instanceof ListView))
      return false; 
    ListView listView = (ListView)view;
    if (listView == null)
      return false; 
    return true;
  }
  
  private void ensureList() {
    if (this.mList != null)
      return; 
    View view = getView();
    if (view != null) {
      view = view.findViewById(16908298);
      if (view instanceof ListView) {
        ListView listView = (ListView)view;
        if (listView != null) {
          listView.setOnKeyListener(this.mListOnKeyListener);
          this.mHandler.post(this.mRequestFocus);
          return;
        } 
        throw new RuntimeException("Your content must have a ListView whose id attribute is 'android.R.id.list'");
      } 
      throw new RuntimeException("Content has view with id attribute 'android.R.id.list' that is not a ListView class");
    } 
    throw new IllegalStateException("Content view not yet created");
  }
  
  private View.OnKeyListener mListOnKeyListener = (View.OnKeyListener)new Object(this);
  
  private static final int FIRST_REQUEST_CODE = 100;
  
  private static final int MSG_BIND_PREFERENCES = 1;
  
  private static final String PREFERENCES_TAG = "android:preferences";
  
  private boolean mHavePrefs;
  
  private boolean mInitDone;
  
  private ListView mList;
  
  private PreferenceManager mPreferenceManager;
  
  @Deprecated
  class OnPreferenceStartFragmentCallback {
    public abstract boolean onPreferenceStartFragment(PreferenceFragment param1PreferenceFragment, Preference param1Preference);
  }
}
