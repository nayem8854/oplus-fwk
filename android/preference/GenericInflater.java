package android.preference;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.util.AttributeSet;
import android.util.Xml;
import android.view.InflateException;
import java.io.IOException;
import java.util.HashMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

@Deprecated
abstract class GenericInflater<T, P extends GenericInflater.Parent> {
  private final boolean DEBUG = false;
  
  private final Object[] mConstructorArgs = new Object[2];
  
  private static final Class[] mConstructorSignature = new Class[] { Context.class, AttributeSet.class };
  
  private static final HashMap sConstructorMap = new HashMap<>();
  
  protected final Context mContext;
  
  private String mDefaultPackage;
  
  private Factory<T> mFactory;
  
  private boolean mFactorySet;
  
  public static interface Factory<T> {
    T onCreateItem(String param1String, Context param1Context, AttributeSet param1AttributeSet);
  }
  
  class FactoryMerger<T> implements Factory<T> {
    private final GenericInflater.Factory<T> mF1;
    
    private final GenericInflater.Factory<T> mF2;
    
    FactoryMerger(GenericInflater this$0, GenericInflater.Factory<T> param1Factory1) {
      this.mF1 = (GenericInflater.Factory<T>)this$0;
      this.mF2 = param1Factory1;
    }
    
    public T onCreateItem(String param1String, Context param1Context, AttributeSet param1AttributeSet) {
      T t = this.mF1.onCreateItem(param1String, param1Context, param1AttributeSet);
      if (t != null)
        return t; 
      return this.mF2.onCreateItem(param1String, param1Context, param1AttributeSet);
    }
  }
  
  protected GenericInflater(Context paramContext) {
    this.mContext = paramContext;
  }
  
  protected GenericInflater(GenericInflater<T, P> paramGenericInflater, Context paramContext) {
    this.mContext = paramContext;
    this.mFactory = paramGenericInflater.mFactory;
  }
  
  public void setDefaultPackage(String paramString) {
    this.mDefaultPackage = paramString;
  }
  
  public String getDefaultPackage() {
    return this.mDefaultPackage;
  }
  
  public Context getContext() {
    return this.mContext;
  }
  
  public final Factory<T> getFactory() {
    return this.mFactory;
  }
  
  public void setFactory(Factory<T> paramFactory) {
    if (!this.mFactorySet) {
      if (paramFactory != null) {
        this.mFactorySet = true;
        Factory<T> factory = this.mFactory;
        if (factory == null) {
          this.mFactory = paramFactory;
        } else {
          this.mFactory = new FactoryMerger<>(paramFactory, factory);
        } 
        return;
      } 
      throw new NullPointerException("Given factory can not be null");
    } 
    throw new IllegalStateException("A factory has already been set on this inflater");
  }
  
  public T inflate(int paramInt, P paramP) {
    boolean bool;
    if (paramP != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return inflate(paramInt, paramP, bool);
  }
  
  public T inflate(XmlPullParser paramXmlPullParser, P paramP) {
    boolean bool;
    if (paramP != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return inflate(paramXmlPullParser, paramP, bool);
  }
  
  public T inflate(int paramInt, P paramP, boolean paramBoolean) {
    XmlResourceParser xmlResourceParser = getContext().getResources().getXml(paramInt);
    try {
      paramP = (P)inflate((XmlPullParser)xmlResourceParser, paramP, paramBoolean);
      return (T)paramP;
    } finally {
      xmlResourceParser.close();
    } 
  }
  
  public T inflate(XmlPullParser paramXmlPullParser, P paramP, boolean paramBoolean) {
    synchronized (this.mConstructorArgs) {
      AttributeSet attributeSet = Xml.asAttributeSet(paramXmlPullParser);
      this.mConstructorArgs[0] = this.mContext;
      try {
        int i;
        while (true) {
          i = paramXmlPullParser.next();
          if (i != 2 && i != 1)
            continue; 
          break;
        } 
        if (i == 2) {
          T t = createItemFromTag(paramXmlPullParser, paramXmlPullParser.getName(), attributeSet);
          paramP = onMergeRoots(paramP, paramBoolean, (P)t);
          rInflate(paramXmlPullParser, (T)paramP, attributeSet);
          return (T)paramP;
        } 
        InflateException inflateException = new InflateException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(paramXmlPullParser.getPositionDescription());
        stringBuilder.append(": No start tag found!");
        this(stringBuilder.toString());
        throw inflateException;
      } catch (InflateException inflateException) {
        throw inflateException;
      } catch (XmlPullParserException xmlPullParserException) {
        InflateException inflateException = new InflateException();
        this(xmlPullParserException.getMessage());
        inflateException.initCause((Throwable)xmlPullParserException);
        throw inflateException;
      } catch (IOException iOException) {
        InflateException inflateException = new InflateException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(xmlPullParserException.getPositionDescription());
        stringBuilder.append(": ");
        stringBuilder.append(iOException.getMessage());
        this(stringBuilder.toString());
        inflateException.initCause(iOException);
        throw inflateException;
      } 
    } 
  }
  
  public final T createItem(String paramString1, String paramString2, AttributeSet paramAttributeSet) throws ClassNotFoundException, InflateException {
    // Byte code:
    //   0: getstatic android/preference/GenericInflater.sConstructorMap : Ljava/util/HashMap;
    //   3: aload_1
    //   4: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   7: checkcast java/lang/reflect/Constructor
    //   10: astore #4
    //   12: aload #4
    //   14: astore #5
    //   16: aload #4
    //   18: ifnonnull -> 146
    //   21: aload #4
    //   23: astore #6
    //   25: aload_0
    //   26: getfield mContext : Landroid/content/Context;
    //   29: invokevirtual getClassLoader : ()Ljava/lang/ClassLoader;
    //   32: astore #7
    //   34: aload_2
    //   35: ifnull -> 92
    //   38: aload #4
    //   40: astore #6
    //   42: new java/lang/StringBuilder
    //   45: astore #5
    //   47: aload #4
    //   49: astore #6
    //   51: aload #5
    //   53: invokespecial <init> : ()V
    //   56: aload #4
    //   58: astore #6
    //   60: aload #5
    //   62: aload_2
    //   63: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   66: pop
    //   67: aload #4
    //   69: astore #6
    //   71: aload #5
    //   73: aload_1
    //   74: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   77: pop
    //   78: aload #4
    //   80: astore #6
    //   82: aload #5
    //   84: invokevirtual toString : ()Ljava/lang/String;
    //   87: astore #5
    //   89: goto -> 95
    //   92: aload_1
    //   93: astore #5
    //   95: aload #4
    //   97: astore #6
    //   99: aload #7
    //   101: aload #5
    //   103: invokevirtual loadClass : (Ljava/lang/String;)Ljava/lang/Class;
    //   106: astore #5
    //   108: aload #4
    //   110: astore #6
    //   112: aload #5
    //   114: getstatic android/preference/GenericInflater.mConstructorSignature : [Ljava/lang/Class;
    //   117: invokevirtual getConstructor : ([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    //   120: astore #5
    //   122: aload #5
    //   124: astore #6
    //   126: aload #5
    //   128: iconst_1
    //   129: invokevirtual setAccessible : (Z)V
    //   132: aload #5
    //   134: astore #6
    //   136: getstatic android/preference/GenericInflater.sConstructorMap : Ljava/util/HashMap;
    //   139: aload_1
    //   140: aload #5
    //   142: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   145: pop
    //   146: aload #5
    //   148: astore #6
    //   150: aload_0
    //   151: getfield mConstructorArgs : [Ljava/lang/Object;
    //   154: astore #4
    //   156: aload #4
    //   158: iconst_1
    //   159: aload_3
    //   160: aastore
    //   161: aload #5
    //   163: astore #6
    //   165: aload #5
    //   167: aload #4
    //   169: invokevirtual newInstance : ([Ljava/lang/Object;)Ljava/lang/Object;
    //   172: astore #5
    //   174: aload #5
    //   176: areturn
    //   177: astore_1
    //   178: new java/lang/StringBuilder
    //   181: dup
    //   182: invokespecial <init> : ()V
    //   185: astore_2
    //   186: aload_2
    //   187: aload_3
    //   188: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   193: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   196: pop
    //   197: aload_2
    //   198: ldc ': Error inflating class '
    //   200: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   203: pop
    //   204: aload_2
    //   205: aload #6
    //   207: invokevirtual getClass : ()Ljava/lang/Class;
    //   210: invokevirtual getName : ()Ljava/lang/String;
    //   213: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   216: pop
    //   217: new android/view/InflateException
    //   220: dup
    //   221: aload_2
    //   222: invokevirtual toString : ()Ljava/lang/String;
    //   225: invokespecial <init> : (Ljava/lang/String;)V
    //   228: astore_2
    //   229: aload_2
    //   230: aload_1
    //   231: invokevirtual initCause : (Ljava/lang/Throwable;)Ljava/lang/Throwable;
    //   234: pop
    //   235: aload_2
    //   236: athrow
    //   237: astore_1
    //   238: aload_1
    //   239: athrow
    //   240: astore #6
    //   242: new java/lang/StringBuilder
    //   245: dup
    //   246: invokespecial <init> : ()V
    //   249: astore #5
    //   251: aload #5
    //   253: aload_3
    //   254: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   259: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   262: pop
    //   263: aload #5
    //   265: ldc ': Error inflating class '
    //   267: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   270: pop
    //   271: aload_2
    //   272: ifnull -> 303
    //   275: new java/lang/StringBuilder
    //   278: dup
    //   279: invokespecial <init> : ()V
    //   282: astore_3
    //   283: aload_3
    //   284: aload_2
    //   285: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   288: pop
    //   289: aload_3
    //   290: aload_1
    //   291: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   294: pop
    //   295: aload_3
    //   296: invokevirtual toString : ()Ljava/lang/String;
    //   299: astore_1
    //   300: goto -> 303
    //   303: aload #5
    //   305: aload_1
    //   306: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   309: pop
    //   310: new android/view/InflateException
    //   313: dup
    //   314: aload #5
    //   316: invokevirtual toString : ()Ljava/lang/String;
    //   319: invokespecial <init> : (Ljava/lang/String;)V
    //   322: astore_1
    //   323: aload_1
    //   324: aload #6
    //   326: invokevirtual initCause : (Ljava/lang/Throwable;)Ljava/lang/Throwable;
    //   329: pop
    //   330: aload_1
    //   331: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #377	-> 0
    //   #380	-> 12
    //   #383	-> 21
    //   #384	-> 34
    //   #383	-> 95
    //   #385	-> 108
    //   #386	-> 122
    //   #387	-> 132
    //   #390	-> 146
    //   #391	-> 156
    //   #392	-> 161
    //   #405	-> 177
    //   #406	-> 178
    //   #407	-> 186
    //   #409	-> 204
    //   #410	-> 229
    //   #411	-> 235
    //   #402	-> 237
    //   #404	-> 238
    //   #394	-> 240
    //   #395	-> 242
    //   #396	-> 251
    //   #398	-> 271
    //   #399	-> 323
    //   #400	-> 330
    // Exception table:
    //   from	to	target	type
    //   25	34	240	java/lang/NoSuchMethodException
    //   25	34	237	java/lang/ClassNotFoundException
    //   25	34	177	java/lang/Exception
    //   42	47	240	java/lang/NoSuchMethodException
    //   42	47	237	java/lang/ClassNotFoundException
    //   42	47	177	java/lang/Exception
    //   51	56	240	java/lang/NoSuchMethodException
    //   51	56	237	java/lang/ClassNotFoundException
    //   51	56	177	java/lang/Exception
    //   60	67	240	java/lang/NoSuchMethodException
    //   60	67	237	java/lang/ClassNotFoundException
    //   60	67	177	java/lang/Exception
    //   71	78	240	java/lang/NoSuchMethodException
    //   71	78	237	java/lang/ClassNotFoundException
    //   71	78	177	java/lang/Exception
    //   82	89	240	java/lang/NoSuchMethodException
    //   82	89	237	java/lang/ClassNotFoundException
    //   82	89	177	java/lang/Exception
    //   99	108	240	java/lang/NoSuchMethodException
    //   99	108	237	java/lang/ClassNotFoundException
    //   99	108	177	java/lang/Exception
    //   112	122	240	java/lang/NoSuchMethodException
    //   112	122	237	java/lang/ClassNotFoundException
    //   112	122	177	java/lang/Exception
    //   126	132	240	java/lang/NoSuchMethodException
    //   126	132	237	java/lang/ClassNotFoundException
    //   126	132	177	java/lang/Exception
    //   136	146	240	java/lang/NoSuchMethodException
    //   136	146	237	java/lang/ClassNotFoundException
    //   136	146	177	java/lang/Exception
    //   150	156	240	java/lang/NoSuchMethodException
    //   150	156	237	java/lang/ClassNotFoundException
    //   150	156	177	java/lang/Exception
    //   165	174	240	java/lang/NoSuchMethodException
    //   165	174	237	java/lang/ClassNotFoundException
    //   165	174	177	java/lang/Exception
  }
  
  protected T onCreateItem(String paramString, AttributeSet paramAttributeSet) throws ClassNotFoundException {
    return createItem(paramString, this.mDefaultPackage, paramAttributeSet);
  }
  
  private final T createItemFromTag(XmlPullParser paramXmlPullParser, String paramString, AttributeSet paramAttributeSet) {
    InflateException inflateException;
    try {
      if (this.mFactory == null) {
        paramXmlPullParser = null;
      } else {
        paramXmlPullParser = (XmlPullParser)this.mFactory.onCreateItem(paramString, this.mContext, paramAttributeSet);
      } 
      XmlPullParser xmlPullParser = paramXmlPullParser;
      if (paramXmlPullParser == null)
        if (-1 == paramString.indexOf('.')) {
          xmlPullParser = (XmlPullParser)onCreateItem(paramString, paramAttributeSet);
        } else {
          xmlPullParser = (XmlPullParser)createItem(paramString, null, paramAttributeSet);
        }  
      return (T)xmlPullParser;
    } catch (InflateException inflateException1) {
      throw inflateException1;
    } catch (ClassNotFoundException classNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramAttributeSet.getPositionDescription());
      stringBuilder.append(": Error inflating class ");
      stringBuilder.append(paramString);
      inflateException = new InflateException(stringBuilder.toString());
      inflateException.initCause(classNotFoundException);
      throw inflateException;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramAttributeSet.getPositionDescription());
      stringBuilder.append(": Error inflating class ");
      stringBuilder.append((String)inflateException);
      inflateException = new InflateException(stringBuilder.toString());
      inflateException.initCause(exception);
      throw inflateException;
    } 
  }
  
  private void rInflate(XmlPullParser paramXmlPullParser, T paramT, AttributeSet paramAttributeSet) throws XmlPullParserException, IOException {
    int i = paramXmlPullParser.getDepth();
    while (true) {
      int j = paramXmlPullParser.next();
      if ((j != 3 || 
        paramXmlPullParser.getDepth() > i) && j != 1) {
        if (j != 2)
          continue; 
        if (onCreateCustomFromTag(paramXmlPullParser, paramT, paramAttributeSet))
          continue; 
        String str = paramXmlPullParser.getName();
        str = (String)createItemFromTag(paramXmlPullParser, str, paramAttributeSet);
        ((Parent<String>)paramT).addItemFromInflater(str);
        rInflate(paramXmlPullParser, (T)str, paramAttributeSet);
        continue;
      } 
      break;
    } 
  }
  
  protected boolean onCreateCustomFromTag(XmlPullParser paramXmlPullParser, T paramT, AttributeSet paramAttributeSet) throws XmlPullParserException {
    return false;
  }
  
  protected P onMergeRoots(P paramP1, boolean paramBoolean, P paramP2) {
    return paramP2;
  }
  
  public abstract GenericInflater cloneInContext(Context paramContext);
  
  public static interface Parent<T> {
    void addItemFromInflater(T param1T);
  }
}
