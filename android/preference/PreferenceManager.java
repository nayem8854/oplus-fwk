package android.preference;

import android.annotation.SystemApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.util.Log;
import java.util.HashSet;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;

@Deprecated
public class PreferenceManager {
  public static final String KEY_HAS_SET_DEFAULT_VALUES = "_has_set_default_values";
  
  public static final String METADATA_KEY_PREFERENCES = "android.preference";
  
  private static final int STORAGE_CREDENTIAL_PROTECTED = 2;
  
  private static final int STORAGE_DEFAULT = 0;
  
  private static final int STORAGE_DEVICE_PROTECTED = 1;
  
  private static final String TAG = "PreferenceManager";
  
  private Activity mActivity;
  
  private List<OnActivityDestroyListener> mActivityDestroyListeners;
  
  private List<OnActivityResultListener> mActivityResultListeners;
  
  private List<OnActivityStopListener> mActivityStopListeners;
  
  private Context mContext;
  
  private SharedPreferences.Editor mEditor;
  
  private PreferenceFragment mFragment;
  
  private long mNextId = 0L;
  
  private int mNextRequestCode;
  
  private boolean mNoCommit;
  
  private OnPreferenceTreeClickListener mOnPreferenceTreeClickListener;
  
  private PreferenceDataStore mPreferenceDataStore;
  
  private PreferenceScreen mPreferenceScreen;
  
  private List<DialogInterface> mPreferencesScreens;
  
  private SharedPreferences mSharedPreferences;
  
  private int mSharedPreferencesMode;
  
  private String mSharedPreferencesName;
  
  private int mStorage = 0;
  
  public PreferenceManager(Activity paramActivity, int paramInt) {
    this.mActivity = paramActivity;
    this.mNextRequestCode = paramInt;
    init((Context)paramActivity);
  }
  
  PreferenceManager(Context paramContext) {
    init(paramContext);
  }
  
  private void init(Context paramContext) {
    this.mContext = paramContext;
    setSharedPreferencesName(getDefaultSharedPreferencesName(paramContext));
  }
  
  void setFragment(PreferenceFragment paramPreferenceFragment) {
    this.mFragment = paramPreferenceFragment;
  }
  
  PreferenceFragment getFragment() {
    return this.mFragment;
  }
  
  public void setPreferenceDataStore(PreferenceDataStore paramPreferenceDataStore) {
    this.mPreferenceDataStore = paramPreferenceDataStore;
  }
  
  public PreferenceDataStore getPreferenceDataStore() {
    return this.mPreferenceDataStore;
  }
  
  private List<ResolveInfo> queryIntentActivities(Intent paramIntent) {
    return this.mContext.getPackageManager().queryIntentActivities(paramIntent, 128);
  }
  
  PreferenceScreen inflateFromIntent(Intent paramIntent, PreferenceScreen paramPreferenceScreen) {
    List<ResolveInfo> list = queryIntentActivities(paramIntent);
    HashSet<String> hashSet = new HashSet();
    for (int i = list.size() - 1; i >= 0; i--, paramPreferenceScreen = preferenceScreen) {
      ActivityInfo activityInfo = ((ResolveInfo)list.get(i)).activityInfo;
      Bundle bundle = activityInfo.metaData;
      PreferenceScreen preferenceScreen = paramPreferenceScreen;
      if (bundle != null)
        if (!bundle.containsKey("android.preference")) {
          preferenceScreen = paramPreferenceScreen;
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(activityInfo.packageName);
          stringBuilder.append(":");
          Bundle bundle1 = activityInfo.metaData;
          stringBuilder.append(bundle1.getInt("android.preference"));
          String str = stringBuilder.toString();
          PreferenceScreen preferenceScreen1 = paramPreferenceScreen;
          if (!hashSet.contains(str)) {
            XmlResourceParser xmlResourceParser;
            hashSet.add(str);
            try {
              Context context = this.mContext.createPackageContext(activityInfo.packageName, 0);
              PreferenceInflater preferenceInflater = new PreferenceInflater(context, this);
              PackageManager packageManager = context.getPackageManager();
              xmlResourceParser = activityInfo.loadXmlMetaData(packageManager, "android.preference");
              PreferenceScreen preferenceScreen2 = (PreferenceScreen)preferenceInflater.inflate((XmlPullParser)xmlResourceParser, paramPreferenceScreen, true);
              xmlResourceParser.close();
            } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
              StringBuilder stringBuilder1 = new StringBuilder();
              stringBuilder1.append("Could not create context for ");
              stringBuilder1.append(((ActivityInfo)xmlResourceParser).packageName);
              stringBuilder1.append(": ");
              stringBuilder1.append(Log.getStackTraceString((Throwable)nameNotFoundException));
              String str1 = stringBuilder1.toString();
              Log.w("PreferenceManager", str1);
              preferenceScreen = paramPreferenceScreen;
            } 
          } 
        }  
    } 
    paramPreferenceScreen.onAttachedToHierarchy(this);
    return paramPreferenceScreen;
  }
  
  public PreferenceScreen inflateFromResource(Context paramContext, int paramInt, PreferenceScreen paramPreferenceScreen) {
    setNoCommit(true);
    PreferenceInflater preferenceInflater = new PreferenceInflater(paramContext, this);
    PreferenceScreen preferenceScreen = (PreferenceScreen)preferenceInflater.inflate(paramInt, paramPreferenceScreen, true);
    preferenceScreen.onAttachedToHierarchy(this);
    setNoCommit(false);
    return preferenceScreen;
  }
  
  public PreferenceScreen createPreferenceScreen(Context paramContext) {
    PreferenceScreen preferenceScreen = new PreferenceScreen(paramContext, null);
    preferenceScreen.onAttachedToHierarchy(this);
    return preferenceScreen;
  }
  
  long getNextId() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mNextId : J
    //   6: lstore_1
    //   7: aload_0
    //   8: lconst_1
    //   9: lload_1
    //   10: ladd
    //   11: putfield mNextId : J
    //   14: aload_0
    //   15: monitorexit
    //   16: lload_1
    //   17: lreturn
    //   18: astore_3
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_3
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #364	-> 0
    //   #365	-> 2
    //   #366	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	16	18	finally
    //   19	21	18	finally
  }
  
  public String getSharedPreferencesName() {
    return this.mSharedPreferencesName;
  }
  
  public void setSharedPreferencesName(String paramString) {
    this.mSharedPreferencesName = paramString;
    this.mSharedPreferences = null;
  }
  
  public int getSharedPreferencesMode() {
    return this.mSharedPreferencesMode;
  }
  
  public void setSharedPreferencesMode(int paramInt) {
    this.mSharedPreferencesMode = paramInt;
    this.mSharedPreferences = null;
  }
  
  public void setStorageDefault() {
    this.mStorage = 0;
    this.mSharedPreferences = null;
  }
  
  public void setStorageDeviceProtected() {
    this.mStorage = 1;
    this.mSharedPreferences = null;
  }
  
  @SystemApi
  public void setStorageCredentialProtected() {
    this.mStorage = 2;
    this.mSharedPreferences = null;
  }
  
  public boolean isStorageDefault() {
    boolean bool;
    if (this.mStorage == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isStorageDeviceProtected() {
    int i = this.mStorage;
    boolean bool = true;
    if (i != 1)
      bool = false; 
    return bool;
  }
  
  @SystemApi
  public boolean isStorageCredentialProtected() {
    boolean bool;
    if (this.mStorage == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public SharedPreferences getSharedPreferences() {
    if (this.mPreferenceDataStore != null)
      return null; 
    if (this.mSharedPreferences == null) {
      Context context;
      int i = this.mStorage;
      if (i != 1) {
        if (i != 2) {
          context = this.mContext;
        } else {
          context = this.mContext.createCredentialProtectedStorageContext();
        } 
      } else {
        context = this.mContext.createDeviceProtectedStorageContext();
      } 
      this.mSharedPreferences = context.getSharedPreferences(this.mSharedPreferencesName, this.mSharedPreferencesMode);
    } 
    return this.mSharedPreferences;
  }
  
  public static SharedPreferences getDefaultSharedPreferences(Context paramContext) {
    String str = getDefaultSharedPreferencesName(paramContext);
    int i = getDefaultSharedPreferencesMode();
    return paramContext.getSharedPreferences(str, i);
  }
  
  public static String getDefaultSharedPreferencesName(Context paramContext) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramContext.getPackageName());
    stringBuilder.append("_preferences");
    return stringBuilder.toString();
  }
  
  private static int getDefaultSharedPreferencesMode() {
    return 0;
  }
  
  PreferenceScreen getPreferenceScreen() {
    return this.mPreferenceScreen;
  }
  
  boolean setPreferences(PreferenceScreen paramPreferenceScreen) {
    if (paramPreferenceScreen != this.mPreferenceScreen) {
      this.mPreferenceScreen = paramPreferenceScreen;
      return true;
    } 
    return false;
  }
  
  public Preference findPreference(CharSequence paramCharSequence) {
    PreferenceScreen preferenceScreen = this.mPreferenceScreen;
    if (preferenceScreen == null)
      return null; 
    return preferenceScreen.findPreference(paramCharSequence);
  }
  
  public static void setDefaultValues(Context paramContext, int paramInt, boolean paramBoolean) {
    String str = getDefaultSharedPreferencesName(paramContext);
    int i = getDefaultSharedPreferencesMode();
    setDefaultValues(paramContext, str, i, paramInt, paramBoolean);
  }
  
  public static void setDefaultValues(Context paramContext, String paramString, int paramInt1, int paramInt2, boolean paramBoolean) {
    SharedPreferences sharedPreferences = paramContext.getSharedPreferences("_has_set_default_values", 0);
    if (paramBoolean || !sharedPreferences.getBoolean("_has_set_default_values", false)) {
      PreferenceManager preferenceManager = new PreferenceManager(paramContext);
      preferenceManager.setSharedPreferencesName(paramString);
      preferenceManager.setSharedPreferencesMode(paramInt1);
      preferenceManager.inflateFromResource(paramContext, paramInt2, null);
      SharedPreferences.Editor editor = sharedPreferences.edit().putBoolean("_has_set_default_values", true);
      try {
        editor.apply();
      } catch (AbstractMethodError abstractMethodError) {
        editor.commit();
      } 
    } 
  }
  
  SharedPreferences.Editor getEditor() {
    if (this.mPreferenceDataStore != null)
      return null; 
    if (this.mNoCommit) {
      if (this.mEditor == null)
        this.mEditor = getSharedPreferences().edit(); 
      return this.mEditor;
    } 
    return getSharedPreferences().edit();
  }
  
  boolean shouldCommit() {
    return this.mNoCommit ^ true;
  }
  
  private void setNoCommit(boolean paramBoolean) {
    if (!paramBoolean) {
      SharedPreferences.Editor editor = this.mEditor;
      if (editor != null)
        try {
          editor.apply();
        } catch (AbstractMethodError abstractMethodError) {
          this.mEditor.commit();
        }  
    } 
    this.mNoCommit = paramBoolean;
  }
  
  Activity getActivity() {
    return this.mActivity;
  }
  
  Context getContext() {
    return this.mContext;
  }
  
  void registerOnActivityResultListener(OnActivityResultListener paramOnActivityResultListener) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mActivityResultListeners : Ljava/util/List;
    //   6: ifnonnull -> 22
    //   9: new java/util/ArrayList
    //   12: astore_2
    //   13: aload_2
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: aload_2
    //   19: putfield mActivityResultListeners : Ljava/util/List;
    //   22: aload_0
    //   23: getfield mActivityResultListeners : Ljava/util/List;
    //   26: aload_1
    //   27: invokeinterface contains : (Ljava/lang/Object;)Z
    //   32: ifne -> 46
    //   35: aload_0
    //   36: getfield mActivityResultListeners : Ljava/util/List;
    //   39: aload_1
    //   40: invokeinterface add : (Ljava/lang/Object;)Z
    //   45: pop
    //   46: aload_0
    //   47: monitorexit
    //   48: return
    //   49: astore_1
    //   50: aload_0
    //   51: monitorexit
    //   52: aload_1
    //   53: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #774	-> 0
    //   #775	-> 2
    //   #776	-> 9
    //   #779	-> 22
    //   #780	-> 35
    //   #782	-> 46
    //   #783	-> 48
    //   #782	-> 49
    // Exception table:
    //   from	to	target	type
    //   2	9	49	finally
    //   9	22	49	finally
    //   22	35	49	finally
    //   35	46	49	finally
    //   46	48	49	finally
    //   50	52	49	finally
  }
  
  void unregisterOnActivityResultListener(OnActivityResultListener paramOnActivityResultListener) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mActivityResultListeners : Ljava/util/List;
    //   6: ifnull -> 20
    //   9: aload_0
    //   10: getfield mActivityResultListeners : Ljava/util/List;
    //   13: aload_1
    //   14: invokeinterface remove : (Ljava/lang/Object;)Z
    //   19: pop
    //   20: aload_0
    //   21: monitorexit
    //   22: return
    //   23: astore_1
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_1
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #792	-> 0
    //   #793	-> 2
    //   #794	-> 9
    //   #796	-> 20
    //   #797	-> 22
    //   #796	-> 23
    // Exception table:
    //   from	to	target	type
    //   2	9	23	finally
    //   9	20	23	finally
    //   20	22	23	finally
    //   24	26	23	finally
  }
  
  void dispatchActivityResult(int paramInt1, int paramInt2, Intent paramIntent) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mActivityResultListeners : Ljava/util/List;
    //   6: ifnonnull -> 12
    //   9: aload_0
    //   10: monitorexit
    //   11: return
    //   12: new java/util/ArrayList
    //   15: astore #4
    //   17: aload #4
    //   19: aload_0
    //   20: getfield mActivityResultListeners : Ljava/util/List;
    //   23: invokespecial <init> : (Ljava/util/Collection;)V
    //   26: aload_0
    //   27: monitorexit
    //   28: aload #4
    //   30: invokeinterface size : ()I
    //   35: istore #5
    //   37: iconst_0
    //   38: istore #6
    //   40: iload #6
    //   42: iload #5
    //   44: if_icmpge -> 79
    //   47: aload #4
    //   49: iload #6
    //   51: invokeinterface get : (I)Ljava/lang/Object;
    //   56: checkcast android/preference/PreferenceManager$OnActivityResultListener
    //   59: iload_1
    //   60: iload_2
    //   61: aload_3
    //   62: invokeinterface onActivityResult : (IILandroid/content/Intent;)Z
    //   67: ifeq -> 73
    //   70: goto -> 79
    //   73: iinc #6, 1
    //   76: goto -> 40
    //   79: return
    //   80: astore_3
    //   81: aload_0
    //   82: monitorexit
    //   83: aload_3
    //   84: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #806	-> 0
    //   #807	-> 2
    //   #808	-> 12
    //   #809	-> 26
    //   #811	-> 28
    //   #812	-> 37
    //   #813	-> 47
    //   #814	-> 70
    //   #812	-> 73
    //   #817	-> 79
    //   #809	-> 80
    // Exception table:
    //   from	to	target	type
    //   2	11	80	finally
    //   12	26	80	finally
    //   26	28	80	finally
    //   81	83	80	finally
  }
  
  public void registerOnActivityStopListener(OnActivityStopListener paramOnActivityStopListener) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mActivityStopListeners : Ljava/util/List;
    //   6: ifnonnull -> 22
    //   9: new java/util/ArrayList
    //   12: astore_2
    //   13: aload_2
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: aload_2
    //   19: putfield mActivityStopListeners : Ljava/util/List;
    //   22: aload_0
    //   23: getfield mActivityStopListeners : Ljava/util/List;
    //   26: aload_1
    //   27: invokeinterface contains : (Ljava/lang/Object;)Z
    //   32: ifne -> 46
    //   35: aload_0
    //   36: getfield mActivityStopListeners : Ljava/util/List;
    //   39: aload_1
    //   40: invokeinterface add : (Ljava/lang/Object;)Z
    //   45: pop
    //   46: aload_0
    //   47: monitorexit
    //   48: return
    //   49: astore_1
    //   50: aload_0
    //   51: monitorexit
    //   52: aload_1
    //   53: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #827	-> 0
    //   #828	-> 2
    //   #829	-> 9
    //   #832	-> 22
    //   #833	-> 35
    //   #835	-> 46
    //   #836	-> 48
    //   #835	-> 49
    // Exception table:
    //   from	to	target	type
    //   2	9	49	finally
    //   9	22	49	finally
    //   22	35	49	finally
    //   35	46	49	finally
    //   46	48	49	finally
    //   50	52	49	finally
  }
  
  public void unregisterOnActivityStopListener(OnActivityStopListener paramOnActivityStopListener) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mActivityStopListeners : Ljava/util/List;
    //   6: ifnull -> 20
    //   9: aload_0
    //   10: getfield mActivityStopListeners : Ljava/util/List;
    //   13: aload_1
    //   14: invokeinterface remove : (Ljava/lang/Object;)Z
    //   19: pop
    //   20: aload_0
    //   21: monitorexit
    //   22: return
    //   23: astore_1
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_1
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #846	-> 0
    //   #847	-> 2
    //   #848	-> 9
    //   #850	-> 20
    //   #851	-> 22
    //   #850	-> 23
    // Exception table:
    //   from	to	target	type
    //   2	9	23	finally
    //   9	20	23	finally
    //   20	22	23	finally
    //   24	26	23	finally
  }
  
  void dispatchActivityStop() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mActivityStopListeners : Ljava/util/List;
    //   6: ifnonnull -> 12
    //   9: aload_0
    //   10: monitorexit
    //   11: return
    //   12: new java/util/ArrayList
    //   15: astore_1
    //   16: aload_1
    //   17: aload_0
    //   18: getfield mActivityStopListeners : Ljava/util/List;
    //   21: invokespecial <init> : (Ljava/util/Collection;)V
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_1
    //   27: invokeinterface size : ()I
    //   32: istore_2
    //   33: iconst_0
    //   34: istore_3
    //   35: iload_3
    //   36: iload_2
    //   37: if_icmpge -> 61
    //   40: aload_1
    //   41: iload_3
    //   42: invokeinterface get : (I)Ljava/lang/Object;
    //   47: checkcast android/preference/PreferenceManager$OnActivityStopListener
    //   50: invokeinterface onActivityStop : ()V
    //   55: iinc #3, 1
    //   58: goto -> 35
    //   61: return
    //   62: astore_1
    //   63: aload_0
    //   64: monitorexit
    //   65: aload_1
    //   66: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #861	-> 0
    //   #862	-> 2
    //   #863	-> 12
    //   #864	-> 24
    //   #866	-> 26
    //   #867	-> 33
    //   #868	-> 40
    //   #867	-> 55
    //   #870	-> 61
    //   #864	-> 62
    // Exception table:
    //   from	to	target	type
    //   2	11	62	finally
    //   12	24	62	finally
    //   24	26	62	finally
    //   63	65	62	finally
  }
  
  void registerOnActivityDestroyListener(OnActivityDestroyListener paramOnActivityDestroyListener) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mActivityDestroyListeners : Ljava/util/List;
    //   6: ifnonnull -> 22
    //   9: new java/util/ArrayList
    //   12: astore_2
    //   13: aload_2
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: aload_2
    //   19: putfield mActivityDestroyListeners : Ljava/util/List;
    //   22: aload_0
    //   23: getfield mActivityDestroyListeners : Ljava/util/List;
    //   26: aload_1
    //   27: invokeinterface contains : (Ljava/lang/Object;)Z
    //   32: ifne -> 46
    //   35: aload_0
    //   36: getfield mActivityDestroyListeners : Ljava/util/List;
    //   39: aload_1
    //   40: invokeinterface add : (Ljava/lang/Object;)Z
    //   45: pop
    //   46: aload_0
    //   47: monitorexit
    //   48: return
    //   49: astore_1
    //   50: aload_0
    //   51: monitorexit
    //   52: aload_1
    //   53: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #879	-> 0
    //   #880	-> 2
    //   #881	-> 9
    //   #884	-> 22
    //   #885	-> 35
    //   #887	-> 46
    //   #888	-> 48
    //   #887	-> 49
    // Exception table:
    //   from	to	target	type
    //   2	9	49	finally
    //   9	22	49	finally
    //   22	35	49	finally
    //   35	46	49	finally
    //   46	48	49	finally
    //   50	52	49	finally
  }
  
  void unregisterOnActivityDestroyListener(OnActivityDestroyListener paramOnActivityDestroyListener) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mActivityDestroyListeners : Ljava/util/List;
    //   6: ifnull -> 20
    //   9: aload_0
    //   10: getfield mActivityDestroyListeners : Ljava/util/List;
    //   13: aload_1
    //   14: invokeinterface remove : (Ljava/lang/Object;)Z
    //   19: pop
    //   20: aload_0
    //   21: monitorexit
    //   22: return
    //   23: astore_1
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_1
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #897	-> 0
    //   #898	-> 2
    //   #899	-> 9
    //   #901	-> 20
    //   #902	-> 22
    //   #901	-> 23
    // Exception table:
    //   from	to	target	type
    //   2	9	23	finally
    //   9	20	23	finally
    //   20	22	23	finally
    //   24	26	23	finally
  }
  
  void dispatchActivityDestroy() {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aload_0
    //   3: monitorenter
    //   4: aload_0
    //   5: getfield mActivityDestroyListeners : Ljava/util/List;
    //   8: ifnull -> 23
    //   11: new java/util/ArrayList
    //   14: astore_1
    //   15: aload_1
    //   16: aload_0
    //   17: getfield mActivityDestroyListeners : Ljava/util/List;
    //   20: invokespecial <init> : (Ljava/util/Collection;)V
    //   23: aload_0
    //   24: monitorexit
    //   25: aload_1
    //   26: ifnull -> 64
    //   29: aload_1
    //   30: invokeinterface size : ()I
    //   35: istore_2
    //   36: iconst_0
    //   37: istore_3
    //   38: iload_3
    //   39: iload_2
    //   40: if_icmpge -> 64
    //   43: aload_1
    //   44: iload_3
    //   45: invokeinterface get : (I)Ljava/lang/Object;
    //   50: checkcast android/preference/PreferenceManager$OnActivityDestroyListener
    //   53: invokeinterface onActivityDestroy : ()V
    //   58: iinc #3, 1
    //   61: goto -> 38
    //   64: aload_0
    //   65: invokespecial dismissAllScreens : ()V
    //   68: return
    //   69: astore_1
    //   70: aload_0
    //   71: monitorexit
    //   72: aload_1
    //   73: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #910	-> 0
    //   #912	-> 2
    //   #913	-> 4
    //   #914	-> 11
    //   #916	-> 23
    //   #918	-> 25
    //   #919	-> 29
    //   #920	-> 36
    //   #921	-> 43
    //   #920	-> 58
    //   #926	-> 64
    //   #927	-> 68
    //   #916	-> 69
    // Exception table:
    //   from	to	target	type
    //   4	11	69	finally
    //   11	23	69	finally
    //   23	25	69	finally
    //   70	72	69	finally
  }
  
  int getNextRequestCode() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mNextRequestCode : I
    //   6: istore_1
    //   7: aload_0
    //   8: iload_1
    //   9: iconst_1
    //   10: iadd
    //   11: putfield mNextRequestCode : I
    //   14: aload_0
    //   15: monitorexit
    //   16: iload_1
    //   17: ireturn
    //   18: astore_2
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_2
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #938	-> 0
    //   #939	-> 2
    //   #940	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	16	18	finally
    //   19	21	18	finally
  }
  
  void addPreferencesScreen(DialogInterface paramDialogInterface) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mPreferencesScreens : Ljava/util/List;
    //   6: ifnonnull -> 22
    //   9: new java/util/ArrayList
    //   12: astore_2
    //   13: aload_2
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: aload_2
    //   19: putfield mPreferencesScreens : Ljava/util/List;
    //   22: aload_0
    //   23: getfield mPreferencesScreens : Ljava/util/List;
    //   26: aload_1
    //   27: invokeinterface add : (Ljava/lang/Object;)Z
    //   32: pop
    //   33: aload_0
    //   34: monitorexit
    //   35: return
    //   36: astore_1
    //   37: aload_0
    //   38: monitorexit
    //   39: aload_1
    //   40: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #944	-> 0
    //   #946	-> 2
    //   #947	-> 9
    //   #950	-> 22
    //   #951	-> 33
    //   #952	-> 35
    //   #951	-> 36
    // Exception table:
    //   from	to	target	type
    //   2	9	36	finally
    //   9	22	36	finally
    //   22	33	36	finally
    //   33	35	36	finally
    //   37	39	36	finally
  }
  
  void removePreferencesScreen(DialogInterface paramDialogInterface) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mPreferencesScreens : Ljava/util/List;
    //   6: ifnonnull -> 12
    //   9: aload_0
    //   10: monitorexit
    //   11: return
    //   12: aload_0
    //   13: getfield mPreferencesScreens : Ljava/util/List;
    //   16: aload_1
    //   17: invokeinterface remove : (Ljava/lang/Object;)Z
    //   22: pop
    //   23: aload_0
    //   24: monitorexit
    //   25: return
    //   26: astore_1
    //   27: aload_0
    //   28: monitorexit
    //   29: aload_1
    //   30: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #955	-> 0
    //   #957	-> 2
    //   #958	-> 9
    //   #961	-> 12
    //   #962	-> 23
    //   #963	-> 25
    //   #962	-> 26
    // Exception table:
    //   from	to	target	type
    //   2	9	26	finally
    //   9	11	26	finally
    //   12	23	26	finally
    //   23	25	26	finally
    //   27	29	26	finally
  }
  
  void dispatchNewIntent(Intent paramIntent) {
    dismissAllScreens();
  }
  
  private void dismissAllScreens() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mPreferencesScreens : Ljava/util/List;
    //   6: ifnonnull -> 12
    //   9: aload_0
    //   10: monitorexit
    //   11: return
    //   12: new java/util/ArrayList
    //   15: astore_1
    //   16: aload_1
    //   17: aload_0
    //   18: getfield mPreferencesScreens : Ljava/util/List;
    //   21: invokespecial <init> : (Ljava/util/Collection;)V
    //   24: aload_0
    //   25: getfield mPreferencesScreens : Ljava/util/List;
    //   28: invokeinterface clear : ()V
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: invokevirtual size : ()I
    //   39: iconst_1
    //   40: isub
    //   41: istore_2
    //   42: iload_2
    //   43: iflt -> 65
    //   46: aload_1
    //   47: iload_2
    //   48: invokevirtual get : (I)Ljava/lang/Object;
    //   51: checkcast android/content/DialogInterface
    //   54: invokeinterface dismiss : ()V
    //   59: iinc #2, -1
    //   62: goto -> 42
    //   65: return
    //   66: astore_1
    //   67: aload_0
    //   68: monitorexit
    //   69: aload_1
    //   70: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #978	-> 0
    //   #980	-> 2
    //   #981	-> 9
    //   #984	-> 12
    //   #985	-> 24
    //   #986	-> 33
    //   #988	-> 35
    //   #989	-> 46
    //   #988	-> 59
    //   #991	-> 65
    //   #986	-> 66
    // Exception table:
    //   from	to	target	type
    //   2	9	66	finally
    //   9	11	66	finally
    //   12	24	66	finally
    //   24	33	66	finally
    //   33	35	66	finally
    //   67	69	66	finally
  }
  
  void setOnPreferenceTreeClickListener(OnPreferenceTreeClickListener paramOnPreferenceTreeClickListener) {
    this.mOnPreferenceTreeClickListener = paramOnPreferenceTreeClickListener;
  }
  
  OnPreferenceTreeClickListener getOnPreferenceTreeClickListener() {
    return this.mOnPreferenceTreeClickListener;
  }
  
  @Deprecated
  public static interface OnActivityDestroyListener {
    void onActivityDestroy();
  }
  
  @Deprecated
  public static interface OnActivityResultListener {
    boolean onActivityResult(int param1Int1, int param1Int2, Intent param1Intent);
  }
  
  @Deprecated
  public static interface OnActivityStopListener {
    void onActivityStop();
  }
  
  @Deprecated
  public static interface OnPreferenceTreeClickListener {
    boolean onPreferenceTreeClick(PreferenceScreen param1PreferenceScreen, Preference param1Preference);
  }
}
