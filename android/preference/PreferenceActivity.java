package android.preference;

import android.animation.LayoutTransition;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentBreadCrumbs;
import android.app.FragmentTransaction;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.android.internal.R;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public abstract class PreferenceActivity extends ListActivity implements PreferenceManager.OnPreferenceTreeClickListener, PreferenceFragment.OnPreferenceStartFragmentCallback {
  private final ArrayList<Header> mHeaders = new ArrayList<>();
  
  private int mPreferenceHeaderItemResId = 0;
  
  private boolean mPreferenceHeaderRemoveEmptyIcon = false;
  
  private Handler mHandler = (Handler)new Object(this);
  
  private static final String BACK_STACK_PREFS = ":android:prefs";
  
  private static final String CUR_HEADER_TAG = ":android:cur_header";
  
  public static final String EXTRA_NO_HEADERS = ":android:no_headers";
  
  private static final String EXTRA_PREFS_SET_BACK_TEXT = "extra_prefs_set_back_text";
  
  private static final String EXTRA_PREFS_SET_NEXT_TEXT = "extra_prefs_set_next_text";
  
  private static final String EXTRA_PREFS_SHOW_BUTTON_BAR = "extra_prefs_show_button_bar";
  
  private static final String EXTRA_PREFS_SHOW_SKIP = "extra_prefs_show_skip";
  
  public static final String EXTRA_SHOW_FRAGMENT = ":android:show_fragment";
  
  public static final String EXTRA_SHOW_FRAGMENT_ARGUMENTS = ":android:show_fragment_args";
  
  public static final String EXTRA_SHOW_FRAGMENT_SHORT_TITLE = ":android:show_fragment_short_title";
  
  public static final String EXTRA_SHOW_FRAGMENT_TITLE = ":android:show_fragment_title";
  
  private static final int FIRST_REQUEST_CODE = 100;
  
  private static final String HEADERS_TAG = ":android:headers";
  
  public static final long HEADER_ID_UNDEFINED = -1L;
  
  private static final int MSG_BIND_PREFERENCES = 1;
  
  private static final int MSG_BUILD_HEADERS = 2;
  
  private static final String PREFERENCES_TAG = ":android:preferences";
  
  private static final String TAG = "PreferenceActivity";
  
  private CharSequence mActivityTitle;
  
  private Header mCurHeader;
  
  private FragmentBreadCrumbs mFragmentBreadCrumbs;
  
  private ViewGroup mHeadersContainer;
  
  private FrameLayout mListFooter;
  
  private Button mNextButton;
  
  private PreferenceManager mPreferenceManager;
  
  private ViewGroup mPrefsContainer;
  
  private Bundle mSavedInstanceState;
  
  private boolean mSinglePane;
  
  class HeaderViewHolder {
    ImageView icon;
    
    TextView summary;
    
    TextView title;
    
    private HeaderViewHolder() {}
  }
  
  private static class HeaderAdapter extends ArrayAdapter<Header> {
    private LayoutInflater mInflater;
    
    private int mLayoutResId;
    
    private boolean mRemoveIconIfEmpty;
    
    class HeaderViewHolder {
      ImageView icon;
      
      TextView summary;
      
      TextView title;
      
      private HeaderViewHolder() {}
    }
    
    public HeaderAdapter(Context param1Context, List<PreferenceActivity.Header> param1List, int param1Int, boolean param1Boolean) {
      super(param1Context, 0, param1List);
      this.mInflater = (LayoutInflater)param1Context.getSystemService("layout_inflater");
      this.mLayoutResId = param1Int;
      this.mRemoveIconIfEmpty = param1Boolean;
    }
    
    public View getView(int param1Int, View param1View, ViewGroup param1ViewGroup) {
      HeaderViewHolder headerViewHolder1, headerViewHolder2;
      if (param1View == null) {
        View view = this.mInflater.inflate(this.mLayoutResId, param1ViewGroup, false);
        headerViewHolder1 = new HeaderViewHolder();
        headerViewHolder1.icon = (ImageView)view.findViewById(16908294);
        headerViewHolder1.title = (TextView)view.findViewById(16908310);
        headerViewHolder1.summary = (TextView)view.findViewById(16908304);
        view.setTag(headerViewHolder1);
      } else {
        headerViewHolder2 = headerViewHolder1;
        headerViewHolder1 = (HeaderViewHolder)headerViewHolder2.getTag();
      } 
      PreferenceActivity.Header header = (PreferenceActivity.Header)getItem(param1Int);
      if (this.mRemoveIconIfEmpty) {
        if (header.iconRes == 0) {
          headerViewHolder1.icon.setVisibility(8);
        } else {
          headerViewHolder1.icon.setVisibility(0);
          headerViewHolder1.icon.setImageResource(header.iconRes);
        } 
      } else {
        headerViewHolder1.icon.setImageResource(header.iconRes);
      } 
      headerViewHolder1.title.setText(header.getTitle(getContext().getResources()));
      CharSequence charSequence = header.getSummary(getContext().getResources());
      if (!TextUtils.isEmpty(charSequence)) {
        headerViewHolder1.summary.setVisibility(0);
        headerViewHolder1.summary.setText(charSequence);
      } else {
        headerViewHolder1.summary.setVisibility(8);
      } 
      return (View)headerViewHolder2;
    }
  }
  
  @Deprecated
  class Header implements Parcelable {
    public int titleRes;
    
    public CharSequence title;
    
    public int summaryRes;
    
    public CharSequence summary;
    
    public Intent intent;
    
    public long id = -1L;
    
    public int iconRes;
    
    public Bundle fragmentArguments;
    
    public String fragment;
    
    public Bundle extras;
    
    public int breadCrumbTitleRes;
    
    public CharSequence breadCrumbTitle;
    
    public int breadCrumbShortTitleRes;
    
    public CharSequence breadCrumbShortTitle;
    
    public CharSequence getTitle(Resources param1Resources) {
      int i = this.titleRes;
      if (i != 0)
        return param1Resources.getText(i); 
      return this.title;
    }
    
    public CharSequence getSummary(Resources param1Resources) {
      int i = this.summaryRes;
      if (i != 0)
        return param1Resources.getText(i); 
      return this.summary;
    }
    
    public CharSequence getBreadCrumbTitle(Resources param1Resources) {
      int i = this.breadCrumbTitleRes;
      if (i != 0)
        return param1Resources.getText(i); 
      return this.breadCrumbTitle;
    }
    
    public CharSequence getBreadCrumbShortTitle(Resources param1Resources) {
      int i = this.breadCrumbShortTitleRes;
      if (i != 0)
        return param1Resources.getText(i); 
      return this.breadCrumbShortTitle;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeLong(this.id);
      param1Parcel.writeInt(this.titleRes);
      TextUtils.writeToParcel(this.title, param1Parcel, param1Int);
      param1Parcel.writeInt(this.summaryRes);
      TextUtils.writeToParcel(this.summary, param1Parcel, param1Int);
      param1Parcel.writeInt(this.breadCrumbTitleRes);
      TextUtils.writeToParcel(this.breadCrumbTitle, param1Parcel, param1Int);
      param1Parcel.writeInt(this.breadCrumbShortTitleRes);
      TextUtils.writeToParcel(this.breadCrumbShortTitle, param1Parcel, param1Int);
      param1Parcel.writeInt(this.iconRes);
      param1Parcel.writeString(this.fragment);
      param1Parcel.writeBundle(this.fragmentArguments);
      if (this.intent != null) {
        param1Parcel.writeInt(1);
        this.intent.writeToParcel(param1Parcel, param1Int);
      } else {
        param1Parcel.writeInt(0);
      } 
      param1Parcel.writeBundle(this.extras);
    }
    
    public void readFromParcel(Parcel param1Parcel) {
      this.id = param1Parcel.readLong();
      this.titleRes = param1Parcel.readInt();
      this.title = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel);
      this.summaryRes = param1Parcel.readInt();
      this.summary = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel);
      this.breadCrumbTitleRes = param1Parcel.readInt();
      this.breadCrumbTitle = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel);
      this.breadCrumbShortTitleRes = param1Parcel.readInt();
      this.breadCrumbShortTitle = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel);
      this.iconRes = param1Parcel.readInt();
      this.fragment = param1Parcel.readString();
      this.fragmentArguments = param1Parcel.readBundle();
      if (param1Parcel.readInt() != 0)
        this.intent = Intent.CREATOR.createFromParcel(param1Parcel); 
      this.extras = param1Parcel.readBundle();
    }
    
    Header(PreferenceActivity this$0) {
      readFromParcel((Parcel)this$0);
    }
    
    public static final Parcelable.Creator<Header> CREATOR = new Parcelable.Creator<Header>() {
        public PreferenceActivity.Header createFromParcel(Parcel param2Parcel) {
          return new PreferenceActivity.Header(param2Parcel);
        }
        
        public PreferenceActivity.Header[] newArray(int param2Int) {
          return new PreferenceActivity.Header[param2Int];
        }
      };
    
    public Header() {}
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
    if (paramMenuItem.getItemId() == 16908332) {
      onBackPressed();
      return true;
    } 
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  protected void onCreate(Bundle paramBundle) {
    ArrayList<Parcelable> arrayList;
    super.onCreate(paramBundle);
    TypedArray typedArray = obtainStyledAttributes(null, R.styleable.PreferenceActivity, 17957042, 0);
    int i = typedArray.getResourceId(0, 17367253);
    this.mPreferenceHeaderItemResId = typedArray.getResourceId(1, 17367247);
    this.mPreferenceHeaderRemoveEmptyIcon = typedArray.getBoolean(2, false);
    typedArray.recycle();
    setContentView(i);
    this.mListFooter = (FrameLayout)findViewById(16909130);
    this.mPrefsContainer = (ViewGroup)findViewById(16909314);
    this.mHeadersContainer = (ViewGroup)findViewById(16909038);
    boolean bool = onIsHidingHeaders();
    if (bool || !onIsMultiPane()) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mSinglePane = bool;
    String str = getIntent().getStringExtra(":android:show_fragment");
    Bundle bundle = getIntent().getBundleExtra(":android:show_fragment_args");
    int j = getIntent().getIntExtra(":android:show_fragment_title", 0);
    i = getIntent().getIntExtra(":android:show_fragment_short_title", 0);
    this.mActivityTitle = getTitle();
    if (paramBundle != null) {
      arrayList = paramBundle.getParcelableArrayList(":android:headers");
      if (arrayList != null) {
        this.mHeaders.addAll(arrayList);
        int k = paramBundle.getInt(":android:cur_header", -1);
        if (k >= 0 && k < this.mHeaders.size()) {
          setSelectedHeader(this.mHeaders.get(k));
        } else if (!this.mSinglePane && str == null) {
          switchToHeader(onGetInitialHeader());
        } 
      } else {
        showBreadCrumbs(getTitle(), (CharSequence)null);
      } 
    } else {
      if (!onIsHidingHeaders())
        onBuildHeaders(this.mHeaders); 
      if (str != null) {
        switchToHeader(str, (Bundle)arrayList);
      } else if (!this.mSinglePane && this.mHeaders.size() > 0) {
        switchToHeader(onGetInitialHeader());
      } 
    } 
    if (this.mHeaders.size() > 0) {
      setListAdapter((ListAdapter)new HeaderAdapter((Context)this, this.mHeaders, this.mPreferenceHeaderItemResId, this.mPreferenceHeaderRemoveEmptyIcon));
      if (!this.mSinglePane)
        getListView().setChoiceMode(1); 
    } 
    if (this.mSinglePane && str != null && j != 0) {
      CharSequence charSequence = getText(j);
      if (i != 0) {
        CharSequence charSequence1 = getText(i);
      } else {
        paramBundle = null;
      } 
      showBreadCrumbs(charSequence, (CharSequence)paramBundle);
    } 
    if (this.mHeaders.size() == 0 && str == null) {
      setContentView(17367255);
      this.mListFooter = (FrameLayout)findViewById(16909130);
      this.mPrefsContainer = (ViewGroup)findViewById(16909312);
      PreferenceManager preferenceManager = new PreferenceManager((Activity)this, 100);
      preferenceManager.setOnPreferenceTreeClickListener(this);
      this.mHeadersContainer = null;
    } else if (this.mSinglePane) {
      if (str != null || this.mCurHeader != null) {
        this.mHeadersContainer.setVisibility(8);
      } else {
        this.mPrefsContainer.setVisibility(8);
      } 
      ViewGroup viewGroup = (ViewGroup)findViewById(16909313);
      viewGroup.setLayoutTransition(new LayoutTransition());
    } else if (this.mHeaders.size() > 0) {
      Header header = this.mCurHeader;
      if (header != null)
        setSelectedHeader(header); 
    } 
    Intent intent = getIntent();
    if (intent.getBooleanExtra("extra_prefs_show_button_bar", false)) {
      findViewById(16908811).setVisibility(0);
      Button button2 = (Button)findViewById(16908784);
      button2.setOnClickListener((View.OnClickListener)new Object(this));
      Button button1 = (Button)findViewById(16909444);
      button1.setOnClickListener((View.OnClickListener)new Object(this));
      Button button3 = (Button)findViewById(16909208);
      button3.setOnClickListener((View.OnClickListener)new Object(this));
      if (intent.hasExtra("extra_prefs_set_next_text")) {
        String str1 = intent.getStringExtra("extra_prefs_set_next_text");
        if (TextUtils.isEmpty(str1)) {
          this.mNextButton.setVisibility(8);
        } else {
          this.mNextButton.setText(str1);
        } 
      } 
      if (intent.hasExtra("extra_prefs_set_back_text")) {
        String str1 = intent.getStringExtra("extra_prefs_set_back_text");
        if (TextUtils.isEmpty(str1)) {
          button2.setVisibility(8);
        } else {
          button2.setText(str1);
        } 
      } 
      if (intent.getBooleanExtra("extra_prefs_show_skip", false))
        button1.setVisibility(0); 
    } 
  }
  
  public void onBackPressed() {
    if (this.mCurHeader != null && this.mSinglePane && getFragmentManager().getBackStackEntryCount() == 0 && 
      getIntent().getStringExtra(":android:show_fragment") == null) {
      this.mCurHeader = null;
      this.mPrefsContainer.setVisibility(8);
      this.mHeadersContainer.setVisibility(0);
      CharSequence charSequence = this.mActivityTitle;
      if (charSequence != null)
        showBreadCrumbs(charSequence, (CharSequence)null); 
      getListView().clearChoices();
    } else {
      super.onBackPressed();
    } 
  }
  
  public boolean hasHeaders() {
    boolean bool;
    ViewGroup viewGroup = this.mHeadersContainer;
    if (viewGroup != null && viewGroup.getVisibility() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public List<Header> getHeaders() {
    return this.mHeaders;
  }
  
  public boolean isMultiPane() {
    return this.mSinglePane ^ true;
  }
  
  public boolean onIsMultiPane() {
    return getResources().getBoolean(17891611);
  }
  
  public boolean onIsHidingHeaders() {
    return getIntent().getBooleanExtra(":android:no_headers", false);
  }
  
  public Header onGetInitialHeader() {
    for (byte b = 0; b < this.mHeaders.size(); b++) {
      Header header = this.mHeaders.get(b);
      if (header.fragment != null)
        return header; 
    } 
    throw new IllegalStateException("Must have at least one header with a fragment");
  }
  
  public Header onGetNewHeader() {
    return null;
  }
  
  public void onBuildHeaders(List<Header> paramList) {}
  
  public void invalidateHeaders() {
    if (!this.mHandler.hasMessages(2))
      this.mHandler.sendEmptyMessage(2); 
  }
  
  public void loadHeadersFromResource(int paramInt, List<Header> paramList) {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aconst_null
    //   3: astore #4
    //   5: aconst_null
    //   6: astore #5
    //   8: aconst_null
    //   9: astore #6
    //   11: aconst_null
    //   12: astore #7
    //   14: aconst_null
    //   15: astore #8
    //   17: aload_0
    //   18: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   21: astore #9
    //   23: aload #8
    //   25: astore #5
    //   27: aload #9
    //   29: iload_1
    //   30: invokevirtual getXml : (I)Landroid/content/res/XmlResourceParser;
    //   33: astore #6
    //   35: aload #6
    //   37: astore #5
    //   39: aload #6
    //   41: astore_3
    //   42: aload #6
    //   44: astore #4
    //   46: aload #6
    //   48: invokestatic asAttributeSet : (Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
    //   51: astore #8
    //   53: aload #6
    //   55: astore #5
    //   57: aload #6
    //   59: astore_3
    //   60: aload #6
    //   62: astore #4
    //   64: aload #6
    //   66: invokeinterface next : ()I
    //   71: istore_1
    //   72: iload_1
    //   73: iconst_1
    //   74: if_icmpeq -> 85
    //   77: iload_1
    //   78: iconst_2
    //   79: if_icmpeq -> 85
    //   82: goto -> 53
    //   85: aload #6
    //   87: astore #5
    //   89: aload #6
    //   91: astore_3
    //   92: aload #6
    //   94: astore #4
    //   96: aload #6
    //   98: invokeinterface getName : ()Ljava/lang/String;
    //   103: astore #7
    //   105: aload #6
    //   107: astore #5
    //   109: aload #6
    //   111: astore_3
    //   112: aload #6
    //   114: astore #4
    //   116: ldc_w 'preference-headers'
    //   119: aload #7
    //   121: invokevirtual equals : (Ljava/lang/Object;)Z
    //   124: ifeq -> 791
    //   127: aconst_null
    //   128: astore #7
    //   130: aload #6
    //   132: astore #5
    //   134: aload #6
    //   136: astore_3
    //   137: aload #6
    //   139: astore #4
    //   141: aload #6
    //   143: invokeinterface getDepth : ()I
    //   148: istore_1
    //   149: aload #6
    //   151: astore #5
    //   153: aload #6
    //   155: astore_3
    //   156: aload #6
    //   158: astore #4
    //   160: aload #6
    //   162: invokeinterface next : ()I
    //   167: istore #10
    //   169: iload #10
    //   171: iconst_1
    //   172: if_icmpeq -> 778
    //   175: iload #10
    //   177: iconst_3
    //   178: if_icmpne -> 209
    //   181: aload #6
    //   183: astore #5
    //   185: aload #6
    //   187: astore_3
    //   188: aload #6
    //   190: astore #4
    //   192: aload #6
    //   194: invokeinterface getDepth : ()I
    //   199: iload_1
    //   200: if_icmple -> 206
    //   203: goto -> 209
    //   206: goto -> 778
    //   209: iload #10
    //   211: iconst_3
    //   212: if_icmpeq -> 775
    //   215: iload #10
    //   217: iconst_4
    //   218: if_icmpne -> 224
    //   221: goto -> 775
    //   224: aload #6
    //   226: astore #5
    //   228: aload #6
    //   230: astore_3
    //   231: aload #6
    //   233: astore #4
    //   235: aload #6
    //   237: invokeinterface getName : ()Ljava/lang/String;
    //   242: astore #9
    //   244: aload #6
    //   246: astore #5
    //   248: aload #6
    //   250: astore_3
    //   251: aload #6
    //   253: astore #4
    //   255: ldc_w 'header'
    //   258: aload #9
    //   260: invokevirtual equals : (Ljava/lang/Object;)Z
    //   263: ifeq -> 763
    //   266: aload #6
    //   268: astore #5
    //   270: aload #6
    //   272: astore_3
    //   273: aload #6
    //   275: astore #4
    //   277: new android/preference/PreferenceActivity$Header
    //   280: astore #9
    //   282: aload #6
    //   284: astore #5
    //   286: aload #6
    //   288: astore_3
    //   289: aload #6
    //   291: astore #4
    //   293: aload #9
    //   295: invokespecial <init> : ()V
    //   298: aload #6
    //   300: astore #5
    //   302: aload #6
    //   304: astore_3
    //   305: aload #6
    //   307: astore #4
    //   309: getstatic com/android/internal/R$styleable.PreferenceHeader : [I
    //   312: astore #11
    //   314: aload_0
    //   315: aload #8
    //   317: aload #11
    //   319: invokevirtual obtainStyledAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   322: astore #5
    //   324: aload #9
    //   326: aload #5
    //   328: iconst_1
    //   329: iconst_m1
    //   330: invokevirtual getResourceId : (II)I
    //   333: i2l
    //   334: putfield id : J
    //   337: aload #5
    //   339: iconst_2
    //   340: invokevirtual peekValue : (I)Landroid/util/TypedValue;
    //   343: astore_3
    //   344: aload_3
    //   345: ifnull -> 384
    //   348: aload_3
    //   349: getfield type : I
    //   352: iconst_3
    //   353: if_icmpne -> 384
    //   356: aload_3
    //   357: getfield resourceId : I
    //   360: ifeq -> 375
    //   363: aload #9
    //   365: aload_3
    //   366: getfield resourceId : I
    //   369: putfield titleRes : I
    //   372: goto -> 384
    //   375: aload #9
    //   377: aload_3
    //   378: getfield string : Ljava/lang/CharSequence;
    //   381: putfield title : Ljava/lang/CharSequence;
    //   384: aload #5
    //   386: iconst_3
    //   387: invokevirtual peekValue : (I)Landroid/util/TypedValue;
    //   390: astore_3
    //   391: aload_3
    //   392: ifnull -> 431
    //   395: aload_3
    //   396: getfield type : I
    //   399: iconst_3
    //   400: if_icmpne -> 431
    //   403: aload_3
    //   404: getfield resourceId : I
    //   407: ifeq -> 422
    //   410: aload #9
    //   412: aload_3
    //   413: getfield resourceId : I
    //   416: putfield summaryRes : I
    //   419: goto -> 431
    //   422: aload #9
    //   424: aload_3
    //   425: getfield string : Ljava/lang/CharSequence;
    //   428: putfield summary : Ljava/lang/CharSequence;
    //   431: aload #5
    //   433: iconst_5
    //   434: invokevirtual peekValue : (I)Landroid/util/TypedValue;
    //   437: astore_3
    //   438: aload_3
    //   439: ifnull -> 478
    //   442: aload_3
    //   443: getfield type : I
    //   446: iconst_3
    //   447: if_icmpne -> 478
    //   450: aload_3
    //   451: getfield resourceId : I
    //   454: ifeq -> 469
    //   457: aload #9
    //   459: aload_3
    //   460: getfield resourceId : I
    //   463: putfield breadCrumbTitleRes : I
    //   466: goto -> 478
    //   469: aload #9
    //   471: aload_3
    //   472: getfield string : Ljava/lang/CharSequence;
    //   475: putfield breadCrumbTitle : Ljava/lang/CharSequence;
    //   478: aload #5
    //   480: bipush #6
    //   482: invokevirtual peekValue : (I)Landroid/util/TypedValue;
    //   485: astore_3
    //   486: aload_3
    //   487: ifnull -> 526
    //   490: aload_3
    //   491: getfield type : I
    //   494: iconst_3
    //   495: if_icmpne -> 526
    //   498: aload_3
    //   499: getfield resourceId : I
    //   502: ifeq -> 517
    //   505: aload #9
    //   507: aload_3
    //   508: getfield resourceId : I
    //   511: putfield breadCrumbShortTitleRes : I
    //   514: goto -> 526
    //   517: aload #9
    //   519: aload_3
    //   520: getfield string : Ljava/lang/CharSequence;
    //   523: putfield breadCrumbShortTitle : Ljava/lang/CharSequence;
    //   526: aload #9
    //   528: aload #5
    //   530: iconst_0
    //   531: iconst_0
    //   532: invokevirtual getResourceId : (II)I
    //   535: putfield iconRes : I
    //   538: aload #9
    //   540: aload #5
    //   542: iconst_4
    //   543: invokevirtual getString : (I)Ljava/lang/String;
    //   546: putfield fragment : Ljava/lang/String;
    //   549: aload #5
    //   551: invokevirtual recycle : ()V
    //   554: aload #7
    //   556: ifnonnull -> 572
    //   559: new android/os/Bundle
    //   562: astore #5
    //   564: aload #5
    //   566: invokespecial <init> : ()V
    //   569: goto -> 576
    //   572: aload #7
    //   574: astore #5
    //   576: aload #6
    //   578: invokeinterface getDepth : ()I
    //   583: istore #12
    //   585: aload #6
    //   587: invokeinterface next : ()I
    //   592: istore #10
    //   594: iload #10
    //   596: iconst_1
    //   597: if_icmpeq -> 713
    //   600: iload #10
    //   602: iconst_3
    //   603: if_icmpne -> 618
    //   606: aload #6
    //   608: invokeinterface getDepth : ()I
    //   613: iload #12
    //   615: if_icmple -> 713
    //   618: iload #10
    //   620: iconst_3
    //   621: if_icmpeq -> 710
    //   624: iload #10
    //   626: iconst_4
    //   627: if_icmpne -> 633
    //   630: goto -> 710
    //   633: aload #6
    //   635: invokeinterface getName : ()Ljava/lang/String;
    //   640: astore #7
    //   642: aload #7
    //   644: ldc_w 'extra'
    //   647: invokevirtual equals : (Ljava/lang/Object;)Z
    //   650: ifeq -> 675
    //   653: aload_0
    //   654: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   657: ldc_w 'extra'
    //   660: aload #8
    //   662: aload #5
    //   664: invokevirtual parseBundleExtra : (Ljava/lang/String;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    //   667: aload #6
    //   669: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   672: goto -> 710
    //   675: aload #7
    //   677: ldc_w 'intent'
    //   680: invokevirtual equals : (Ljava/lang/Object;)Z
    //   683: ifeq -> 705
    //   686: aload #9
    //   688: aload_0
    //   689: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   692: aload #6
    //   694: aload #8
    //   696: invokestatic parseIntent : (Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Landroid/content/Intent;
    //   699: putfield intent : Landroid/content/Intent;
    //   702: goto -> 710
    //   705: aload #6
    //   707: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   710: goto -> 585
    //   713: aload #5
    //   715: astore #7
    //   717: aload #5
    //   719: invokevirtual size : ()I
    //   722: ifle -> 735
    //   725: aload #9
    //   727: aload #5
    //   729: putfield fragmentArguments : Landroid/os/Bundle;
    //   732: aconst_null
    //   733: astore #7
    //   735: aload #6
    //   737: astore #5
    //   739: aload_2
    //   740: aload #9
    //   742: invokeinterface add : (Ljava/lang/Object;)Z
    //   747: pop
    //   748: goto -> 149
    //   751: astore_2
    //   752: goto -> 920
    //   755: astore_2
    //   756: goto -> 924
    //   759: astore_2
    //   760: goto -> 958
    //   763: aload #6
    //   765: astore #5
    //   767: aload #6
    //   769: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   772: goto -> 149
    //   775: goto -> 149
    //   778: aload #6
    //   780: ifnull -> 790
    //   783: aload #6
    //   785: invokeinterface close : ()V
    //   790: return
    //   791: aload #6
    //   793: astore #5
    //   795: new java/lang/RuntimeException
    //   798: astore_2
    //   799: aload #6
    //   801: astore #5
    //   803: new java/lang/StringBuilder
    //   806: astore_3
    //   807: aload #6
    //   809: astore #5
    //   811: aload_3
    //   812: invokespecial <init> : ()V
    //   815: aload #6
    //   817: astore #5
    //   819: aload_3
    //   820: ldc_w 'XML document must start with <preference-headers> tag; found'
    //   823: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   826: pop
    //   827: aload #6
    //   829: astore #5
    //   831: aload_3
    //   832: aload #7
    //   834: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   837: pop
    //   838: aload #6
    //   840: astore #5
    //   842: aload_3
    //   843: ldc_w ' at '
    //   846: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   849: pop
    //   850: aload #6
    //   852: astore #5
    //   854: aload_3
    //   855: aload #6
    //   857: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   862: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   865: pop
    //   866: aload #6
    //   868: astore #5
    //   870: aload_2
    //   871: aload_3
    //   872: invokevirtual toString : ()Ljava/lang/String;
    //   875: invokespecial <init> : (Ljava/lang/String;)V
    //   878: aload #6
    //   880: astore #5
    //   882: aload_2
    //   883: athrow
    //   884: astore_2
    //   885: goto -> 924
    //   888: astore_2
    //   889: goto -> 958
    //   892: astore_2
    //   893: aload #5
    //   895: astore #6
    //   897: goto -> 920
    //   900: astore_2
    //   901: aload_3
    //   902: astore #6
    //   904: goto -> 924
    //   907: astore_2
    //   908: aload #4
    //   910: astore #6
    //   912: goto -> 958
    //   915: astore_2
    //   916: aload #5
    //   918: astore #6
    //   920: goto -> 992
    //   923: astore_2
    //   924: aload #6
    //   926: astore #5
    //   928: new java/lang/RuntimeException
    //   931: astore #7
    //   933: aload #6
    //   935: astore #5
    //   937: aload #7
    //   939: ldc_w 'Error parsing headers'
    //   942: aload_2
    //   943: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   946: aload #6
    //   948: astore #5
    //   950: aload #7
    //   952: athrow
    //   953: astore_2
    //   954: aload #7
    //   956: astore #6
    //   958: aload #6
    //   960: astore #5
    //   962: new java/lang/RuntimeException
    //   965: astore #7
    //   967: aload #6
    //   969: astore #5
    //   971: aload #7
    //   973: ldc_w 'Error parsing headers'
    //   976: aload_2
    //   977: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   980: aload #6
    //   982: astore #5
    //   984: aload #7
    //   986: athrow
    //   987: astore_2
    //   988: aload #5
    //   990: astore #6
    //   992: aload #6
    //   994: ifnull -> 1004
    //   997: aload #6
    //   999: invokeinterface close : ()V
    //   1004: aload_2
    //   1005: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #828	-> 0
    //   #830	-> 17
    //   #831	-> 35
    //   #834	-> 53
    //   #839	-> 85
    //   #840	-> 105
    //   #846	-> 127
    //   #848	-> 130
    //   #849	-> 149
    //   #850	-> 181
    //   #851	-> 209
    //   #852	-> 221
    //   #855	-> 224
    //   #856	-> 244
    //   #857	-> 266
    //   #859	-> 298
    //   #861	-> 324
    //   #864	-> 337
    //   #866	-> 344
    //   #867	-> 356
    //   #868	-> 363
    //   #870	-> 375
    //   #873	-> 384
    //   #875	-> 391
    //   #876	-> 403
    //   #877	-> 410
    //   #879	-> 422
    //   #882	-> 431
    //   #884	-> 438
    //   #885	-> 450
    //   #886	-> 457
    //   #888	-> 469
    //   #891	-> 478
    //   #893	-> 486
    //   #894	-> 498
    //   #895	-> 505
    //   #897	-> 517
    //   #900	-> 526
    //   #902	-> 538
    //   #904	-> 549
    //   #906	-> 554
    //   #907	-> 559
    //   #906	-> 572
    //   #910	-> 576
    //   #911	-> 585
    //   #912	-> 606
    //   #913	-> 618
    //   #914	-> 630
    //   #917	-> 633
    //   #918	-> 642
    //   #919	-> 653
    //   #920	-> 667
    //   #922	-> 675
    //   #923	-> 686
    //   #926	-> 705
    //   #928	-> 710
    //   #911	-> 710
    //   #930	-> 713
    //   #931	-> 725
    //   #932	-> 732
    //   #935	-> 735
    //   #936	-> 748
    //   #946	-> 751
    //   #943	-> 755
    //   #941	-> 759
    //   #937	-> 763
    //   #851	-> 775
    //   #849	-> 775
    //   #946	-> 778
    //   #948	-> 790
    //   #841	-> 791
    //   #843	-> 850
    //   #943	-> 884
    //   #941	-> 888
    //   #946	-> 892
    //   #943	-> 900
    //   #941	-> 907
    //   #946	-> 915
    //   #943	-> 923
    //   #944	-> 924
    //   #941	-> 953
    //   #942	-> 958
    //   #946	-> 987
    //   #947	-> 1004
    // Exception table:
    //   from	to	target	type
    //   17	23	953	org/xmlpull/v1/XmlPullParserException
    //   17	23	923	java/io/IOException
    //   17	23	915	finally
    //   27	35	907	org/xmlpull/v1/XmlPullParserException
    //   27	35	900	java/io/IOException
    //   27	35	892	finally
    //   46	53	907	org/xmlpull/v1/XmlPullParserException
    //   46	53	900	java/io/IOException
    //   46	53	892	finally
    //   64	72	907	org/xmlpull/v1/XmlPullParserException
    //   64	72	900	java/io/IOException
    //   64	72	892	finally
    //   96	105	907	org/xmlpull/v1/XmlPullParserException
    //   96	105	900	java/io/IOException
    //   96	105	892	finally
    //   116	127	907	org/xmlpull/v1/XmlPullParserException
    //   116	127	900	java/io/IOException
    //   116	127	892	finally
    //   141	149	907	org/xmlpull/v1/XmlPullParserException
    //   141	149	900	java/io/IOException
    //   141	149	892	finally
    //   160	169	907	org/xmlpull/v1/XmlPullParserException
    //   160	169	900	java/io/IOException
    //   160	169	892	finally
    //   192	203	907	org/xmlpull/v1/XmlPullParserException
    //   192	203	900	java/io/IOException
    //   192	203	892	finally
    //   235	244	907	org/xmlpull/v1/XmlPullParserException
    //   235	244	900	java/io/IOException
    //   235	244	892	finally
    //   255	266	907	org/xmlpull/v1/XmlPullParserException
    //   255	266	900	java/io/IOException
    //   255	266	892	finally
    //   277	282	907	org/xmlpull/v1/XmlPullParserException
    //   277	282	900	java/io/IOException
    //   277	282	892	finally
    //   293	298	907	org/xmlpull/v1/XmlPullParserException
    //   293	298	900	java/io/IOException
    //   293	298	892	finally
    //   309	314	907	org/xmlpull/v1/XmlPullParserException
    //   309	314	900	java/io/IOException
    //   309	314	892	finally
    //   314	324	759	org/xmlpull/v1/XmlPullParserException
    //   314	324	755	java/io/IOException
    //   314	324	751	finally
    //   324	337	759	org/xmlpull/v1/XmlPullParserException
    //   324	337	755	java/io/IOException
    //   324	337	751	finally
    //   337	344	759	org/xmlpull/v1/XmlPullParserException
    //   337	344	755	java/io/IOException
    //   337	344	751	finally
    //   348	356	759	org/xmlpull/v1/XmlPullParserException
    //   348	356	755	java/io/IOException
    //   348	356	751	finally
    //   356	363	759	org/xmlpull/v1/XmlPullParserException
    //   356	363	755	java/io/IOException
    //   356	363	751	finally
    //   363	372	759	org/xmlpull/v1/XmlPullParserException
    //   363	372	755	java/io/IOException
    //   363	372	751	finally
    //   375	384	759	org/xmlpull/v1/XmlPullParserException
    //   375	384	755	java/io/IOException
    //   375	384	751	finally
    //   384	391	759	org/xmlpull/v1/XmlPullParserException
    //   384	391	755	java/io/IOException
    //   384	391	751	finally
    //   395	403	759	org/xmlpull/v1/XmlPullParserException
    //   395	403	755	java/io/IOException
    //   395	403	751	finally
    //   403	410	759	org/xmlpull/v1/XmlPullParserException
    //   403	410	755	java/io/IOException
    //   403	410	751	finally
    //   410	419	759	org/xmlpull/v1/XmlPullParserException
    //   410	419	755	java/io/IOException
    //   410	419	751	finally
    //   422	431	759	org/xmlpull/v1/XmlPullParserException
    //   422	431	755	java/io/IOException
    //   422	431	751	finally
    //   431	438	759	org/xmlpull/v1/XmlPullParserException
    //   431	438	755	java/io/IOException
    //   431	438	751	finally
    //   442	450	759	org/xmlpull/v1/XmlPullParserException
    //   442	450	755	java/io/IOException
    //   442	450	751	finally
    //   450	457	759	org/xmlpull/v1/XmlPullParserException
    //   450	457	755	java/io/IOException
    //   450	457	751	finally
    //   457	466	759	org/xmlpull/v1/XmlPullParserException
    //   457	466	755	java/io/IOException
    //   457	466	751	finally
    //   469	478	759	org/xmlpull/v1/XmlPullParserException
    //   469	478	755	java/io/IOException
    //   469	478	751	finally
    //   478	486	759	org/xmlpull/v1/XmlPullParserException
    //   478	486	755	java/io/IOException
    //   478	486	751	finally
    //   490	498	759	org/xmlpull/v1/XmlPullParserException
    //   490	498	755	java/io/IOException
    //   490	498	751	finally
    //   498	505	759	org/xmlpull/v1/XmlPullParserException
    //   498	505	755	java/io/IOException
    //   498	505	751	finally
    //   505	514	759	org/xmlpull/v1/XmlPullParserException
    //   505	514	755	java/io/IOException
    //   505	514	751	finally
    //   517	526	759	org/xmlpull/v1/XmlPullParserException
    //   517	526	755	java/io/IOException
    //   517	526	751	finally
    //   526	538	759	org/xmlpull/v1/XmlPullParserException
    //   526	538	755	java/io/IOException
    //   526	538	751	finally
    //   538	549	759	org/xmlpull/v1/XmlPullParserException
    //   538	549	755	java/io/IOException
    //   538	549	751	finally
    //   549	554	759	org/xmlpull/v1/XmlPullParserException
    //   549	554	755	java/io/IOException
    //   549	554	751	finally
    //   559	569	759	org/xmlpull/v1/XmlPullParserException
    //   559	569	755	java/io/IOException
    //   559	569	751	finally
    //   576	585	759	org/xmlpull/v1/XmlPullParserException
    //   576	585	755	java/io/IOException
    //   576	585	751	finally
    //   585	594	759	org/xmlpull/v1/XmlPullParserException
    //   585	594	755	java/io/IOException
    //   585	594	751	finally
    //   606	618	759	org/xmlpull/v1/XmlPullParserException
    //   606	618	755	java/io/IOException
    //   606	618	751	finally
    //   633	642	759	org/xmlpull/v1/XmlPullParserException
    //   633	642	755	java/io/IOException
    //   633	642	751	finally
    //   642	653	759	org/xmlpull/v1/XmlPullParserException
    //   642	653	755	java/io/IOException
    //   642	653	751	finally
    //   653	667	759	org/xmlpull/v1/XmlPullParserException
    //   653	667	755	java/io/IOException
    //   653	667	751	finally
    //   667	672	759	org/xmlpull/v1/XmlPullParserException
    //   667	672	755	java/io/IOException
    //   667	672	751	finally
    //   675	686	759	org/xmlpull/v1/XmlPullParserException
    //   675	686	755	java/io/IOException
    //   675	686	751	finally
    //   686	702	759	org/xmlpull/v1/XmlPullParserException
    //   686	702	755	java/io/IOException
    //   686	702	751	finally
    //   705	710	759	org/xmlpull/v1/XmlPullParserException
    //   705	710	755	java/io/IOException
    //   705	710	751	finally
    //   717	725	759	org/xmlpull/v1/XmlPullParserException
    //   717	725	755	java/io/IOException
    //   717	725	751	finally
    //   725	732	759	org/xmlpull/v1/XmlPullParserException
    //   725	732	755	java/io/IOException
    //   725	732	751	finally
    //   739	748	888	org/xmlpull/v1/XmlPullParserException
    //   739	748	884	java/io/IOException
    //   739	748	987	finally
    //   767	772	888	org/xmlpull/v1/XmlPullParserException
    //   767	772	884	java/io/IOException
    //   767	772	987	finally
    //   795	799	888	org/xmlpull/v1/XmlPullParserException
    //   795	799	884	java/io/IOException
    //   795	799	987	finally
    //   803	807	888	org/xmlpull/v1/XmlPullParserException
    //   803	807	884	java/io/IOException
    //   803	807	987	finally
    //   811	815	888	org/xmlpull/v1/XmlPullParserException
    //   811	815	884	java/io/IOException
    //   811	815	987	finally
    //   819	827	888	org/xmlpull/v1/XmlPullParserException
    //   819	827	884	java/io/IOException
    //   819	827	987	finally
    //   831	838	888	org/xmlpull/v1/XmlPullParserException
    //   831	838	884	java/io/IOException
    //   831	838	987	finally
    //   842	850	888	org/xmlpull/v1/XmlPullParserException
    //   842	850	884	java/io/IOException
    //   842	850	987	finally
    //   854	866	888	org/xmlpull/v1/XmlPullParserException
    //   854	866	884	java/io/IOException
    //   854	866	987	finally
    //   870	878	888	org/xmlpull/v1/XmlPullParserException
    //   870	878	884	java/io/IOException
    //   870	878	987	finally
    //   882	884	888	org/xmlpull/v1/XmlPullParserException
    //   882	884	884	java/io/IOException
    //   882	884	987	finally
    //   928	933	987	finally
    //   937	946	987	finally
    //   950	953	987	finally
    //   962	967	987	finally
    //   971	980	987	finally
    //   984	987	987	finally
  }
  
  protected boolean isValidFragment(String paramString) {
    if ((getApplicationInfo()).targetSdkVersion < 19)
      return true; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Subclasses of PreferenceActivity must override isValidFragment(String) to verify that the Fragment class is valid! ");
    stringBuilder.append(getClass().getName());
    stringBuilder.append(" has not checked if fragment ");
    stringBuilder.append(paramString);
    stringBuilder.append(" is valid.");
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public void setListFooter(View paramView) {
    this.mListFooter.removeAllViews();
    this.mListFooter.addView(paramView, (ViewGroup.LayoutParams)new FrameLayout.LayoutParams(-1, -2));
  }
  
  protected void onStop() {
    super.onStop();
    PreferenceManager preferenceManager = this.mPreferenceManager;
    if (preferenceManager != null)
      preferenceManager.dispatchActivityStop(); 
  }
  
  protected void onDestroy() {
    this.mHandler.removeMessages(1);
    this.mHandler.removeMessages(2);
    super.onDestroy();
    PreferenceManager preferenceManager = this.mPreferenceManager;
    if (preferenceManager != null)
      preferenceManager.dispatchActivityDestroy(); 
  }
  
  protected void onSaveInstanceState(Bundle paramBundle) {
    super.onSaveInstanceState(paramBundle);
    if (this.mHeaders.size() > 0) {
      paramBundle.putParcelableArrayList(":android:headers", (ArrayList)this.mHeaders);
      Header header = this.mCurHeader;
      if (header != null) {
        int i = this.mHeaders.indexOf(header);
        if (i >= 0)
          paramBundle.putInt(":android:cur_header", i); 
      } 
    } 
    if (this.mPreferenceManager != null) {
      PreferenceScreen preferenceScreen = getPreferenceScreen();
      if (preferenceScreen != null) {
        Bundle bundle = new Bundle();
        preferenceScreen.saveHierarchyState(bundle);
        paramBundle.putBundle(":android:preferences", bundle);
      } 
    } 
  }
  
  protected void onRestoreInstanceState(Bundle paramBundle) {
    if (this.mPreferenceManager != null) {
      Bundle bundle = paramBundle.getBundle(":android:preferences");
      if (bundle != null) {
        PreferenceScreen preferenceScreen = getPreferenceScreen();
        if (preferenceScreen != null) {
          preferenceScreen.restoreHierarchyState(bundle);
          this.mSavedInstanceState = paramBundle;
          return;
        } 
      } 
    } 
    super.onRestoreInstanceState(paramBundle);
    if (!this.mSinglePane) {
      Header header = this.mCurHeader;
      if (header != null)
        setSelectedHeader(header); 
    } 
  }
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent) {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    PreferenceManager preferenceManager = this.mPreferenceManager;
    if (preferenceManager != null)
      preferenceManager.dispatchActivityResult(paramInt1, paramInt2, paramIntent); 
  }
  
  public void onContentChanged() {
    super.onContentChanged();
    if (this.mPreferenceManager != null)
      postBindPreferences(); 
  }
  
  protected void onListItemClick(ListView paramListView, View paramView, int paramInt, long paramLong) {
    if (!isResumed())
      return; 
    super.onListItemClick(paramListView, paramView, paramInt, paramLong);
    if (this.mAdapter != null) {
      Object object = this.mAdapter.getItem(paramInt);
      if (object instanceof Header)
        onHeaderClick((Header)object, paramInt); 
    } 
  }
  
  public void onHeaderClick(Header paramHeader, int paramInt) {
    if (paramHeader.fragment != null) {
      switchToHeader(paramHeader);
    } else if (paramHeader.intent != null) {
      startActivity(paramHeader.intent);
    } 
  }
  
  public Intent onBuildStartFragmentIntent(String paramString, Bundle paramBundle, int paramInt1, int paramInt2) {
    Intent intent = new Intent("android.intent.action.MAIN");
    intent.setClass((Context)this, getClass());
    intent.putExtra(":android:show_fragment", paramString);
    intent.putExtra(":android:show_fragment_args", paramBundle);
    intent.putExtra(":android:show_fragment_title", paramInt1);
    intent.putExtra(":android:show_fragment_short_title", paramInt2);
    intent.putExtra(":android:no_headers", true);
    return intent;
  }
  
  public void startWithFragment(String paramString, Bundle paramBundle, Fragment paramFragment, int paramInt) {
    startWithFragment(paramString, paramBundle, paramFragment, paramInt, 0, 0);
  }
  
  public void startWithFragment(String paramString, Bundle paramBundle, Fragment paramFragment, int paramInt1, int paramInt2, int paramInt3) {
    Intent intent = onBuildStartFragmentIntent(paramString, paramBundle, paramInt2, paramInt3);
    if (paramFragment == null) {
      startActivity(intent);
    } else {
      paramFragment.startActivityForResult(intent, paramInt1);
    } 
  }
  
  public void showBreadCrumbs(CharSequence paramCharSequence1, CharSequence paramCharSequence2) {
    if (this.mFragmentBreadCrumbs == null) {
      View view = findViewById(16908310);
      try {
        FragmentBreadCrumbs fragmentBreadCrumbs = (FragmentBreadCrumbs)view;
        if (fragmentBreadCrumbs == null) {
          if (paramCharSequence1 != null)
            setTitle(paramCharSequence1); 
          return;
        } 
        if (this.mSinglePane) {
          fragmentBreadCrumbs.setVisibility(8);
          View view1 = findViewById(16908799);
          if (view1 != null)
            view1.setVisibility(8); 
          setTitle(paramCharSequence1);
        } 
        this.mFragmentBreadCrumbs.setMaxVisible(2);
        this.mFragmentBreadCrumbs.setActivity((Activity)this);
      } catch (ClassCastException classCastException) {
        setTitle(paramCharSequence1);
        return;
      } 
    } 
    if (this.mFragmentBreadCrumbs.getVisibility() != 0) {
      setTitle(paramCharSequence1);
    } else {
      this.mFragmentBreadCrumbs.setTitle(paramCharSequence1, (CharSequence)classCastException);
      this.mFragmentBreadCrumbs.setParentTitle(null, null, null);
    } 
  }
  
  public void setParentTitle(CharSequence paramCharSequence1, CharSequence paramCharSequence2, View.OnClickListener paramOnClickListener) {
    FragmentBreadCrumbs fragmentBreadCrumbs = this.mFragmentBreadCrumbs;
    if (fragmentBreadCrumbs != null)
      fragmentBreadCrumbs.setParentTitle(paramCharSequence1, paramCharSequence2, paramOnClickListener); 
  }
  
  void setSelectedHeader(Header paramHeader) {
    this.mCurHeader = paramHeader;
    int i = this.mHeaders.indexOf(paramHeader);
    if (i >= 0) {
      getListView().setItemChecked(i, true);
    } else {
      getListView().clearChoices();
    } 
    showBreadCrumbs(paramHeader);
  }
  
  void showBreadCrumbs(Header paramHeader) {
    if (paramHeader != null) {
      CharSequence charSequence1 = paramHeader.getBreadCrumbTitle(getResources());
      CharSequence charSequence2 = charSequence1;
      if (charSequence1 == null)
        charSequence2 = paramHeader.getTitle(getResources()); 
      charSequence1 = charSequence2;
      if (charSequence2 == null)
        charSequence1 = getTitle(); 
      showBreadCrumbs(charSequence1, paramHeader.getBreadCrumbShortTitle(getResources()));
    } else {
      showBreadCrumbs(getTitle(), (CharSequence)null);
    } 
  }
  
  private void switchToHeaderInner(String paramString, Bundle paramBundle) {
    FragmentTransaction fragmentTransaction;
    getFragmentManager().popBackStack(":android:prefs", 1);
    if (isValidFragment(paramString)) {
      char c;
      Fragment fragment = Fragment.instantiate((Context)this, paramString, paramBundle);
      fragmentTransaction = getFragmentManager().beginTransaction();
      if (this.mSinglePane) {
        c = Character.MIN_VALUE;
      } else {
        c = 'ဃ';
      } 
      fragmentTransaction.setTransition(c);
      fragmentTransaction.replace(16909312, fragment);
      fragmentTransaction.commitAllowingStateLoss();
      if (this.mSinglePane && this.mPrefsContainer.getVisibility() == 8) {
        this.mPrefsContainer.setVisibility(0);
        this.mHeadersContainer.setVisibility(8);
      } 
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid fragment for this activity: ");
    stringBuilder.append((String)fragmentTransaction);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void switchToHeader(String paramString, Bundle paramBundle) {
    Header header2, header1 = null;
    byte b = 0;
    while (true) {
      header2 = header1;
      if (b < this.mHeaders.size()) {
        if (paramString.equals(((Header)this.mHeaders.get(b)).fragment)) {
          header2 = this.mHeaders.get(b);
          break;
        } 
        b++;
        continue;
      } 
      break;
    } 
    setSelectedHeader(header2);
    switchToHeaderInner(paramString, paramBundle);
  }
  
  public void switchToHeader(Header paramHeader) {
    if (this.mCurHeader == paramHeader) {
      getFragmentManager().popBackStack(":android:prefs", 1);
    } else {
      if (paramHeader.fragment != null) {
        switchToHeaderInner(paramHeader.fragment, paramHeader.fragmentArguments);
        setSelectedHeader(paramHeader);
        return;
      } 
      throw new IllegalStateException("can't switch to header that has no fragment");
    } 
  }
  
  Header findBestMatchingHeader(Header paramHeader, ArrayList<Header> paramArrayList) {
    ArrayList<Header> arrayList = new ArrayList();
    byte b;
    for (b = 0; b < paramArrayList.size(); b++) {
      Header header = paramArrayList.get(b);
      if (paramHeader == header || (paramHeader.id != -1L && paramHeader.id == header.id)) {
        arrayList.clear();
        arrayList.add(header);
        break;
      } 
      if (paramHeader.fragment != null) {
        if (paramHeader.fragment.equals(header.fragment))
          arrayList.add(header); 
      } else if (paramHeader.intent != null) {
        if (paramHeader.intent.equals(header.intent))
          arrayList.add(header); 
      } else if (paramHeader.title != null && 
        paramHeader.title.equals(header.title)) {
        arrayList.add(header);
      } 
    } 
    int i = arrayList.size();
    if (i == 1)
      return arrayList.get(0); 
    if (i > 1)
      for (b = 0; b < i; b++) {
        Header header = arrayList.get(b);
        if (paramHeader.fragmentArguments != null) {
          Bundle bundle2 = paramHeader.fragmentArguments, bundle1 = header.fragmentArguments;
          if (bundle2.equals(bundle1))
            return header; 
        } 
        if (paramHeader.extras != null && paramHeader.extras.equals(header.extras))
          return header; 
        if (paramHeader.title != null && paramHeader.title.equals(header.title))
          return header; 
      }  
    return null;
  }
  
  public void startPreferenceFragment(Fragment paramFragment, boolean paramBoolean) {
    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
    fragmentTransaction.replace(16909312, paramFragment);
    if (paramBoolean) {
      fragmentTransaction.setTransition(4097);
      fragmentTransaction.addToBackStack(":android:prefs");
    } else {
      fragmentTransaction.setTransition(4099);
    } 
    fragmentTransaction.commitAllowingStateLoss();
  }
  
  public void startPreferencePanel(String paramString, Bundle paramBundle, int paramInt1, CharSequence paramCharSequence, Fragment paramFragment, int paramInt2) {
    Fragment fragment = Fragment.instantiate((Context)this, paramString, paramBundle);
    if (paramFragment != null)
      fragment.setTargetFragment(paramFragment, paramInt2); 
    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
    fragmentTransaction.replace(16909312, fragment);
    if (paramInt1 != 0) {
      fragmentTransaction.setBreadCrumbTitle(paramInt1);
    } else if (paramCharSequence != null) {
      fragmentTransaction.setBreadCrumbTitle(paramCharSequence);
    } 
    fragmentTransaction.setTransition(4097);
    fragmentTransaction.addToBackStack(":android:prefs");
    fragmentTransaction.commitAllowingStateLoss();
  }
  
  public void finishPreferencePanel(Fragment paramFragment, int paramInt, Intent paramIntent) {
    onBackPressed();
    if (paramFragment != null && 
      paramFragment.getTargetFragment() != null)
      paramFragment.getTargetFragment().onActivityResult(paramFragment.getTargetRequestCode(), paramInt, paramIntent); 
  }
  
  public boolean onPreferenceStartFragment(PreferenceFragment paramPreferenceFragment, Preference paramPreference) {
    String str = paramPreference.getFragment();
    Bundle bundle = paramPreference.getExtras();
    int i = paramPreference.getTitleRes();
    CharSequence charSequence = paramPreference.getTitle();
    startPreferencePanel(str, bundle, i, charSequence, (Fragment)null, 0);
    return true;
  }
  
  private void postBindPreferences() {
    if (this.mHandler.hasMessages(1))
      return; 
    this.mHandler.obtainMessage(1).sendToTarget();
  }
  
  private void bindPreferences() {
    PreferenceScreen preferenceScreen = getPreferenceScreen();
    if (preferenceScreen != null) {
      preferenceScreen.bind(getListView());
      Bundle bundle = this.mSavedInstanceState;
      if (bundle != null) {
        super.onRestoreInstanceState(bundle);
        this.mSavedInstanceState = null;
      } 
    } 
  }
  
  @Deprecated
  public PreferenceManager getPreferenceManager() {
    return this.mPreferenceManager;
  }
  
  private void requirePreferenceManager() {
    if (this.mPreferenceManager == null) {
      if (this.mAdapter == null)
        throw new RuntimeException("This should be called after super.onCreate."); 
      throw new RuntimeException("Modern two-pane PreferenceActivity requires use of a PreferenceFragment");
    } 
  }
  
  @Deprecated
  public void setPreferenceScreen(PreferenceScreen paramPreferenceScreen) {
    requirePreferenceManager();
    if (this.mPreferenceManager.setPreferences(paramPreferenceScreen) && paramPreferenceScreen != null) {
      postBindPreferences();
      CharSequence charSequence = getPreferenceScreen().getTitle();
      if (charSequence != null)
        setTitle(charSequence); 
    } 
  }
  
  @Deprecated
  public PreferenceScreen getPreferenceScreen() {
    PreferenceManager preferenceManager = this.mPreferenceManager;
    if (preferenceManager != null)
      return preferenceManager.getPreferenceScreen(); 
    return null;
  }
  
  @Deprecated
  public void addPreferencesFromIntent(Intent paramIntent) {
    requirePreferenceManager();
    setPreferenceScreen(this.mPreferenceManager.inflateFromIntent(paramIntent, getPreferenceScreen()));
  }
  
  @Deprecated
  public void addPreferencesFromResource(int paramInt) {
    requirePreferenceManager();
    PreferenceManager preferenceManager = this.mPreferenceManager;
    PreferenceScreen preferenceScreen = getPreferenceScreen();
    setPreferenceScreen(preferenceManager.inflateFromResource((Context)this, paramInt, preferenceScreen));
  }
  
  @Deprecated
  public boolean onPreferenceTreeClick(PreferenceScreen paramPreferenceScreen, Preference paramPreference) {
    return false;
  }
  
  @Deprecated
  public Preference findPreference(CharSequence paramCharSequence) {
    PreferenceManager preferenceManager = this.mPreferenceManager;
    if (preferenceManager == null)
      return null; 
    return preferenceManager.findPreference(paramCharSequence);
  }
  
  protected void onNewIntent(Intent paramIntent) {
    PreferenceManager preferenceManager = this.mPreferenceManager;
    if (preferenceManager != null)
      preferenceManager.dispatchNewIntent(paramIntent); 
  }
  
  protected boolean hasNextButton() {
    boolean bool;
    if (this.mNextButton != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected Button getNextButton() {
    return this.mNextButton;
  }
}
