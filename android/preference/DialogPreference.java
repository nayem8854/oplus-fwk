package android.preference;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import com.android.internal.R;

@Deprecated
public abstract class DialogPreference extends Preference implements DialogInterface.OnClickListener, DialogInterface.OnDismissListener, PreferenceManager.OnActivityDestroyListener {
  private AlertDialog.Builder mBuilder;
  
  private Dialog mDialog;
  
  private Drawable mDialogIcon;
  
  private int mDialogLayoutResId;
  
  private CharSequence mDialogMessage;
  
  private CharSequence mDialogTitle;
  
  private final Runnable mDismissRunnable = (Runnable)new Object(this);
  
  private CharSequence mNegativeButtonText;
  
  private CharSequence mPositiveButtonText;
  
  private int mWhichButtonClicked;
  
  public DialogPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.DialogPreference, paramInt1, paramInt2);
    String str = typedArray.getString(0);
    if (str == null)
      this.mDialogTitle = getTitle(); 
    this.mDialogMessage = typedArray.getString(1);
    this.mDialogIcon = typedArray.getDrawable(2);
    this.mPositiveButtonText = typedArray.getString(3);
    this.mNegativeButtonText = typedArray.getString(4);
    this.mDialogLayoutResId = typedArray.getResourceId(5, this.mDialogLayoutResId);
    typedArray.recycle();
  }
  
  public DialogPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public DialogPreference(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842897);
  }
  
  public DialogPreference(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public void setDialogTitle(CharSequence paramCharSequence) {
    this.mDialogTitle = paramCharSequence;
  }
  
  public void setDialogTitle(int paramInt) {
    setDialogTitle(getContext().getString(paramInt));
  }
  
  public CharSequence getDialogTitle() {
    return this.mDialogTitle;
  }
  
  public void setDialogMessage(CharSequence paramCharSequence) {
    this.mDialogMessage = paramCharSequence;
  }
  
  public void setDialogMessage(int paramInt) {
    setDialogMessage(getContext().getString(paramInt));
  }
  
  public CharSequence getDialogMessage() {
    return this.mDialogMessage;
  }
  
  public void setDialogIcon(Drawable paramDrawable) {
    this.mDialogIcon = paramDrawable;
  }
  
  public void setDialogIcon(int paramInt) {
    this.mDialogIcon = getContext().getDrawable(paramInt);
  }
  
  public Drawable getDialogIcon() {
    return this.mDialogIcon;
  }
  
  public void setPositiveButtonText(CharSequence paramCharSequence) {
    this.mPositiveButtonText = paramCharSequence;
  }
  
  public void setPositiveButtonText(int paramInt) {
    setPositiveButtonText(getContext().getString(paramInt));
  }
  
  public CharSequence getPositiveButtonText() {
    return this.mPositiveButtonText;
  }
  
  public void setNegativeButtonText(CharSequence paramCharSequence) {
    this.mNegativeButtonText = paramCharSequence;
  }
  
  public void setNegativeButtonText(int paramInt) {
    setNegativeButtonText(getContext().getString(paramInt));
  }
  
  public CharSequence getNegativeButtonText() {
    return this.mNegativeButtonText;
  }
  
  public void setDialogLayoutResource(int paramInt) {
    this.mDialogLayoutResId = paramInt;
  }
  
  public int getDialogLayoutResource() {
    return this.mDialogLayoutResId;
  }
  
  protected void onPrepareDialogBuilder(AlertDialog.Builder paramBuilder) {}
  
  protected void onClick() {
    Dialog dialog = this.mDialog;
    if (dialog != null && dialog.isShowing())
      return; 
    showDialog((Bundle)null);
  }
  
  protected void showDialog(Bundle paramBundle) {
    Context context = getContext();
    this.mWhichButtonClicked = -2;
    AlertDialog.Builder builder3 = new AlertDialog.Builder(context);
    CharSequence charSequence2 = this.mDialogTitle;
    builder3 = builder3.setTitle(charSequence2);
    Drawable drawable = this.mDialogIcon;
    AlertDialog.Builder builder1 = builder3.setIcon(drawable);
    CharSequence charSequence3 = this.mPositiveButtonText;
    AlertDialog.Builder builder2 = builder1.setPositiveButton(charSequence3, this);
    CharSequence charSequence1 = this.mNegativeButtonText;
    this.mBuilder = builder2.setNegativeButton(charSequence1, this);
    View view = onCreateDialogView();
    if (view != null) {
      onBindDialogView(view);
      this.mBuilder.setView(view);
    } else {
      this.mBuilder.setMessage(this.mDialogMessage);
    } 
    onPrepareDialogBuilder(this.mBuilder);
    getPreferenceManager().registerOnActivityDestroyListener(this);
    AlertDialog alertDialog = this.mBuilder.create();
    if (paramBundle != null)
      alertDialog.onRestoreInstanceState(paramBundle); 
    if (needInputMethod())
      requestInputMethod((Dialog)alertDialog); 
    alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
          final DialogPreference this$0;
          
          public void onShow(DialogInterface param1DialogInterface) {
            DialogPreference.this.removeDismissCallbacks();
          }
        });
    alertDialog.setOnDismissListener(this);
    alertDialog.show();
  }
  
  private View getDecorView() {
    Dialog dialog = this.mDialog;
    if (dialog != null && dialog.getWindow() != null)
      return this.mDialog.getWindow().getDecorView(); 
    return null;
  }
  
  void postDismiss() {
    removeDismissCallbacks();
    View view = getDecorView();
    if (view != null)
      view.post(this.mDismissRunnable); 
  }
  
  private void removeDismissCallbacks() {
    View view = getDecorView();
    if (view != null)
      view.removeCallbacks(this.mDismissRunnable); 
  }
  
  protected boolean needInputMethod() {
    return false;
  }
  
  private void requestInputMethod(Dialog paramDialog) {
    Window window = paramDialog.getWindow();
    window.setSoftInputMode(5);
  }
  
  protected View onCreateDialogView() {
    if (this.mDialogLayoutResId == 0)
      return null; 
    LayoutInflater layoutInflater = LayoutInflater.from(this.mBuilder.getContext());
    return layoutInflater.inflate(this.mDialogLayoutResId, null);
  }
  
  protected void onBindDialogView(View paramView) {
    View view = paramView.findViewById(16908299);
    if (view != null) {
      CharSequence charSequence = getDialogMessage();
      byte b = 8;
      if (!TextUtils.isEmpty(charSequence)) {
        if (view instanceof TextView)
          ((TextView)view).setText(charSequence); 
        b = 0;
      } 
      if (view.getVisibility() != b)
        view.setVisibility(b); 
    } 
  }
  
  public void onClick(DialogInterface paramDialogInterface, int paramInt) {
    this.mWhichButtonClicked = paramInt;
  }
  
  public void onDismiss(DialogInterface paramDialogInterface) {
    boolean bool;
    removeDismissCallbacks();
    getPreferenceManager().unregisterOnActivityDestroyListener(this);
    this.mDialog = null;
    if (this.mWhichButtonClicked == -1) {
      bool = true;
    } else {
      bool = false;
    } 
    onDialogClosed(bool);
  }
  
  protected void onDialogClosed(boolean paramBoolean) {}
  
  public Dialog getDialog() {
    return this.mDialog;
  }
  
  public void onActivityDestroy() {
    Dialog dialog = this.mDialog;
    if (dialog == null || !dialog.isShowing())
      return; 
    this.mDialog.dismiss();
  }
  
  protected Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    Dialog dialog = this.mDialog;
    if (dialog == null || !dialog.isShowing())
      return parcelable; 
    SavedState savedState = new SavedState(parcelable);
    savedState.isDialogShowing = true;
    savedState.dialogBundle = this.mDialog.onSaveInstanceState();
    return (Parcelable)savedState;
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable) {
    if (paramParcelable == null || !paramParcelable.getClass().equals(SavedState.class)) {
      super.onRestoreInstanceState(paramParcelable);
      return;
    } 
    SavedState savedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(savedState.getSuperState());
    if (savedState.isDialogShowing)
      showDialog(savedState.dialogBundle); 
  }
  
  class SavedState extends Preference.BaseSavedState {
    public SavedState(DialogPreference this$0) {
      super((Parcel)this$0);
      int i = this$0.readInt();
      boolean bool = true;
      if (i != 1)
        bool = false; 
      this.isDialogShowing = bool;
      this.dialogBundle = this$0.readBundle();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeInt(this.isDialogShowing);
      param1Parcel.writeBundle(this.dialogBundle);
    }
    
    public SavedState(DialogPreference this$0) {
      super((Parcelable)this$0);
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
        public DialogPreference.SavedState createFromParcel(Parcel param1Parcel) {
          return new DialogPreference.SavedState(param1Parcel);
        }
        
        public DialogPreference.SavedState[] newArray(int param1Int) {
          return new DialogPreference.SavedState[param1Int];
        }
      };
    
    Bundle dialogBundle;
    
    boolean isDialogShowing;
  }
  
  class null implements Parcelable.Creator<SavedState> {
    public DialogPreference.SavedState createFromParcel(Parcel param1Parcel) {
      return new DialogPreference.SavedState(param1Parcel);
    }
    
    public DialogPreference.SavedState[] newArray(int param1Int) {
      return new DialogPreference.SavedState[param1Int];
    }
  }
}
