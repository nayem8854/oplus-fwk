package android.preference;

import java.util.Set;

@Deprecated
public interface PreferenceDataStore {
  default void putString(String paramString1, String paramString2) {
    throw new UnsupportedOperationException("Not implemented on this data store");
  }
  
  default void putStringSet(String paramString, Set<String> paramSet) {
    throw new UnsupportedOperationException("Not implemented on this data store");
  }
  
  default void putInt(String paramString, int paramInt) {
    throw new UnsupportedOperationException("Not implemented on this data store");
  }
  
  default void putLong(String paramString, long paramLong) {
    throw new UnsupportedOperationException("Not implemented on this data store");
  }
  
  default void putFloat(String paramString, float paramFloat) {
    throw new UnsupportedOperationException("Not implemented on this data store");
  }
  
  default void putBoolean(String paramString, boolean paramBoolean) {
    throw new UnsupportedOperationException("Not implemented on this data store");
  }
  
  default String getString(String paramString1, String paramString2) {
    return paramString2;
  }
  
  default Set<String> getStringSet(String paramString, Set<String> paramSet) {
    return paramSet;
  }
  
  default int getInt(String paramString, int paramInt) {
    return paramInt;
  }
  
  default long getLong(String paramString, long paramLong) {
    return paramLong;
  }
  
  default float getFloat(String paramString, float paramFloat) {
    return paramFloat;
  }
  
  default boolean getBoolean(String paramString, boolean paramBoolean) {
    return paramBoolean;
  }
}
