package android.preference;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.android.internal.R;

@Deprecated
public class ListPreference extends DialogPreference {
  private int mClickedDialogEntryIndex;
  
  private CharSequence[] mEntries;
  
  private CharSequence[] mEntryValues;
  
  private String mSummary;
  
  private String mValue;
  
  private boolean mValueSet;
  
  public ListPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray2 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ListPreference, paramInt1, paramInt2);
    this.mEntries = typedArray2.getTextArray(0);
    this.mEntryValues = typedArray2.getTextArray(1);
    typedArray2.recycle();
    TypedArray typedArray1 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Preference, paramInt1, paramInt2);
    this.mSummary = typedArray1.getString(7);
    typedArray1.recycle();
  }
  
  public ListPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public ListPreference(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842897);
  }
  
  public ListPreference(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public void setEntries(CharSequence[] paramArrayOfCharSequence) {
    this.mEntries = paramArrayOfCharSequence;
  }
  
  public void setEntries(int paramInt) {
    setEntries(getContext().getResources().getTextArray(paramInt));
  }
  
  public CharSequence[] getEntries() {
    return this.mEntries;
  }
  
  public void setEntryValues(CharSequence[] paramArrayOfCharSequence) {
    this.mEntryValues = paramArrayOfCharSequence;
  }
  
  public void setEntryValues(int paramInt) {
    setEntryValues(getContext().getResources().getTextArray(paramInt));
  }
  
  public CharSequence[] getEntryValues() {
    return this.mEntryValues;
  }
  
  public void setValue(String paramString) {
    int i = TextUtils.equals(this.mValue, paramString) ^ true;
    if (i != 0 || !this.mValueSet) {
      this.mValue = paramString;
      this.mValueSet = true;
      persistString(paramString);
      if (i != 0)
        notifyChanged(); 
    } 
  }
  
  public CharSequence getSummary() {
    CharSequence charSequence = getEntry();
    String str = this.mSummary;
    if (str == null)
      return super.getSummary(); 
    if (charSequence == null)
      charSequence = ""; 
    return String.format(str, new Object[] { charSequence });
  }
  
  public void setSummary(CharSequence paramCharSequence) {
    super.setSummary(paramCharSequence);
    if (paramCharSequence == null && this.mSummary != null) {
      this.mSummary = null;
    } else if (paramCharSequence != null && !paramCharSequence.equals(this.mSummary)) {
      this.mSummary = paramCharSequence.toString();
    } 
  }
  
  public void setValueIndex(int paramInt) {
    CharSequence[] arrayOfCharSequence = this.mEntryValues;
    if (arrayOfCharSequence != null)
      setValue(arrayOfCharSequence[paramInt].toString()); 
  }
  
  public String getValue() {
    return this.mValue;
  }
  
  public CharSequence getEntry() {
    int i = getValueIndex();
    if (i >= 0) {
      CharSequence[] arrayOfCharSequence = this.mEntries;
      if (arrayOfCharSequence != null && i < arrayOfCharSequence.length)
        return arrayOfCharSequence[i]; 
    } 
    return null;
  }
  
  public int findIndexOfValue(String paramString) {
    if (paramString != null) {
      CharSequence[] arrayOfCharSequence = this.mEntryValues;
      if (arrayOfCharSequence != null)
        for (int i = arrayOfCharSequence.length - 1; i >= 0; i--) {
          if (this.mEntryValues[i].equals(paramString))
            return i; 
        }  
    } 
    return -1;
  }
  
  private int getValueIndex() {
    return findIndexOfValue(this.mValue);
  }
  
  protected void onPrepareDialogBuilder(AlertDialog.Builder paramBuilder) {
    super.onPrepareDialogBuilder(paramBuilder);
    if (this.mEntries != null && this.mEntryValues != null) {
      int i = getValueIndex();
      paramBuilder.setSingleChoiceItems(this.mEntries, i, (DialogInterface.OnClickListener)new Object(this));
      paramBuilder.setPositiveButton(null, null);
      return;
    } 
    throw new IllegalStateException("ListPreference requires an entries array and an entryValues array.");
  }
  
  protected void onDialogClosed(boolean paramBoolean) {
    super.onDialogClosed(paramBoolean);
    if (paramBoolean) {
      int i = this.mClickedDialogEntryIndex;
      if (i >= 0) {
        CharSequence[] arrayOfCharSequence = this.mEntryValues;
        if (arrayOfCharSequence != null) {
          String str = arrayOfCharSequence[i].toString();
          if (callChangeListener(str))
            setValue(str); 
        } 
      } 
    } 
  }
  
  protected Object onGetDefaultValue(TypedArray paramTypedArray, int paramInt) {
    return paramTypedArray.getString(paramInt);
  }
  
  protected void onSetInitialValue(boolean paramBoolean, Object paramObject) {
    if (paramBoolean) {
      paramObject = getPersistedString(this.mValue);
    } else {
      paramObject = paramObject;
    } 
    setValue((String)paramObject);
  }
  
  protected Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    if (isPersistent())
      return parcelable; 
    SavedState savedState = new SavedState(parcelable);
    savedState.value = getValue();
    return (Parcelable)savedState;
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable) {
    if (paramParcelable == null || !paramParcelable.getClass().equals(SavedState.class)) {
      super.onRestoreInstanceState(paramParcelable);
      return;
    } 
    SavedState savedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(savedState.getSuperState());
    setValue(savedState.value);
  }
  
  class SavedState extends Preference.BaseSavedState {
    public SavedState(ListPreference this$0) {
      super((Parcel)this$0);
      this.value = this$0.readString();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeString(this.value);
    }
    
    public SavedState(ListPreference this$0) {
      super((Parcelable)this$0);
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    String value;
  }
}
