package android.preference;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.android.internal.R;

@Deprecated
public final class PreferenceScreen extends PreferenceGroup implements AdapterView.OnItemClickListener, DialogInterface.OnDismissListener {
  private Dialog mDialog;
  
  private Drawable mDividerDrawable;
  
  private boolean mDividerSpecified;
  
  private int mLayoutResId = 17367256;
  
  private ListView mListView;
  
  private ListAdapter mRootAdapter;
  
  public PreferenceScreen(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet, 16842891);
    TypedArray typedArray = paramContext.obtainStyledAttributes(null, R.styleable.PreferenceScreen, 16842891, 0);
    this.mLayoutResId = typedArray.getResourceId(1, this.mLayoutResId);
    if (typedArray.hasValueOrEmpty(0)) {
      this.mDividerDrawable = typedArray.getDrawable(0);
      this.mDividerSpecified = true;
    } 
    typedArray.recycle();
  }
  
  public ListAdapter getRootAdapter() {
    if (this.mRootAdapter == null)
      this.mRootAdapter = onCreateRootAdapter(); 
    return this.mRootAdapter;
  }
  
  protected ListAdapter onCreateRootAdapter() {
    return (ListAdapter)new PreferenceGroupAdapter(this);
  }
  
  public void bind(ListView paramListView) {
    paramListView.setOnItemClickListener(this);
    paramListView.setAdapter(getRootAdapter());
    onAttachedToActivity();
  }
  
  protected void onClick() {
    if (getIntent() != null || getFragment() != null || getPreferenceCount() == 0)
      return; 
    showDialog((Bundle)null);
  }
  
  private void showDialog(Bundle paramBundle) {
    Context context = getContext();
    ListView listView1 = this.mListView;
    if (listView1 != null)
      listView1.setAdapter(null); 
    LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService("layout_inflater");
    View view2 = layoutInflater.inflate(this.mLayoutResId, null);
    View view1 = view2.findViewById(16908310);
    ListView listView2 = (ListView)view2.findViewById(16908298);
    if (this.mDividerSpecified)
      listView2.setDivider(this.mDividerDrawable); 
    bind(this.mListView);
    CharSequence charSequence = getTitle();
    Dialog dialog = new Dialog(context, context.getThemeResId());
    if (TextUtils.isEmpty(charSequence)) {
      if (view1 != null)
        view1.setVisibility(8); 
      dialog.getWindow().requestFeature(1);
    } else if (view1 instanceof TextView) {
      ((TextView)view1).setText(charSequence);
      view1.setVisibility(0);
    } else {
      dialog.setTitle(charSequence);
    } 
    dialog.setContentView(view2);
    dialog.setOnDismissListener(this);
    if (paramBundle != null)
      dialog.onRestoreInstanceState(paramBundle); 
    getPreferenceManager().addPreferencesScreen((DialogInterface)dialog);
    dialog.show();
  }
  
  public void onDismiss(DialogInterface paramDialogInterface) {
    this.mDialog = null;
    getPreferenceManager().removePreferencesScreen(paramDialogInterface);
  }
  
  public Dialog getDialog() {
    return this.mDialog;
  }
  
  public void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong) {
    int i = paramInt;
    if (paramAdapterView instanceof ListView)
      i = paramInt - ((ListView)paramAdapterView).getHeaderViewsCount(); 
    Object object = getRootAdapter().getItem(i);
    if (!(object instanceof Preference))
      return; 
    object = object;
    object.performClick(this);
  }
  
  protected boolean isOnSameScreenAsChildren() {
    return false;
  }
  
  protected Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    Dialog dialog = this.mDialog;
    if (dialog == null || !dialog.isShowing())
      return parcelable; 
    SavedState savedState = new SavedState(parcelable);
    savedState.isDialogShowing = true;
    savedState.dialogBundle = dialog.onSaveInstanceState();
    return (Parcelable)savedState;
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable) {
    if (paramParcelable == null || !paramParcelable.getClass().equals(SavedState.class)) {
      super.onRestoreInstanceState(paramParcelable);
      return;
    } 
    SavedState savedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(savedState.getSuperState());
    if (savedState.isDialogShowing)
      showDialog(savedState.dialogBundle); 
  }
  
  class SavedState extends Preference.BaseSavedState {
    public SavedState(PreferenceScreen this$0) {
      super((Parcel)this$0);
      int i = this$0.readInt();
      boolean bool = true;
      if (i != 1)
        bool = false; 
      this.isDialogShowing = bool;
      this.dialogBundle = this$0.readBundle();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeInt(this.isDialogShowing);
      param1Parcel.writeBundle(this.dialogBundle);
    }
    
    public SavedState(PreferenceScreen this$0) {
      super((Parcelable)this$0);
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    Bundle dialogBundle;
    
    boolean isDialogShowing;
  }
}
