package android.preference;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.AttributeSet;
import com.android.internal.util.XmlUtils;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

@Deprecated
class PreferenceInflater extends GenericInflater<Preference, PreferenceGroup> {
  private static final String EXTRA_TAG_NAME = "extra";
  
  private static final String INTENT_TAG_NAME = "intent";
  
  private static final String TAG = "PreferenceInflater";
  
  private PreferenceManager mPreferenceManager;
  
  public PreferenceInflater(Context paramContext, PreferenceManager paramPreferenceManager) {
    super(paramContext);
    init(paramPreferenceManager);
  }
  
  PreferenceInflater(GenericInflater<Preference, PreferenceGroup> paramGenericInflater, PreferenceManager paramPreferenceManager, Context paramContext) {
    super(paramGenericInflater, paramContext);
    init(paramPreferenceManager);
  }
  
  public GenericInflater<Preference, PreferenceGroup> cloneInContext(Context paramContext) {
    return new PreferenceInflater(this, this.mPreferenceManager, paramContext);
  }
  
  private void init(PreferenceManager paramPreferenceManager) {
    this.mPreferenceManager = paramPreferenceManager;
    setDefaultPackage("android.preference.");
  }
  
  protected boolean onCreateCustomFromTag(XmlPullParser paramXmlPullParser, Preference paramPreference, AttributeSet paramAttributeSet) throws XmlPullParserException {
    XmlPullParserException xmlPullParserException;
    String str = paramXmlPullParser.getName();
    if (str.equals("intent"))
      try {
        Intent intent = Intent.parseIntent(getContext().getResources(), paramXmlPullParser, paramAttributeSet);
        if (intent != null)
          paramPreference.setIntent(intent); 
        return true;
      } catch (IOException iOException) {
        xmlPullParserException = new XmlPullParserException("Error parsing preference");
        xmlPullParserException.initCause(iOException);
        throw xmlPullParserException;
      }  
    if (str.equals("extra")) {
      Resources resources = getContext().getResources();
      Bundle bundle = xmlPullParserException.getExtras();
      resources.parseBundleExtra("extra", paramAttributeSet, bundle);
      try {
        XmlUtils.skipCurrentTag((XmlPullParser)iOException);
        return true;
      } catch (IOException iOException1) {
        XmlPullParserException xmlPullParserException1 = new XmlPullParserException("Error parsing preference");
        xmlPullParserException1.initCause(iOException1);
        throw xmlPullParserException1;
      } 
    } 
    return false;
  }
  
  protected PreferenceGroup onMergeRoots(PreferenceGroup paramPreferenceGroup1, boolean paramBoolean, PreferenceGroup paramPreferenceGroup2) {
    if (paramPreferenceGroup1 == null) {
      paramPreferenceGroup2.onAttachedToHierarchy(this.mPreferenceManager);
      return paramPreferenceGroup2;
    } 
    return paramPreferenceGroup1;
  }
}
