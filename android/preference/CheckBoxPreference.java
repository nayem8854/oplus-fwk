package android.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;
import com.android.internal.R;

@Deprecated
public class CheckBoxPreference extends TwoStatePreference {
  public CheckBoxPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public CheckBoxPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CheckBoxPreference, paramInt1, paramInt2);
    setSummaryOn(typedArray.getString(0));
    setSummaryOff(typedArray.getString(1));
    setDisableDependentsState(typedArray.getBoolean(2, false));
    typedArray.recycle();
  }
  
  public CheckBoxPreference(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842895);
  }
  
  public CheckBoxPreference(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  protected void onBindView(View paramView) {
    super.onBindView(paramView);
    View view = paramView.findViewById(16908289);
    if (view != null && view instanceof Checkable)
      ((Checkable)view).setChecked(this.mChecked); 
    syncSummaryView(paramView);
  }
}
