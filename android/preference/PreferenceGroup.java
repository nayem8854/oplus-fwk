package android.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.android.internal.R;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public abstract class PreferenceGroup extends Preference implements GenericInflater.Parent<Preference> {
  private List<Preference> mPreferenceList;
  
  private boolean mOrderingAsAdded = true;
  
  private int mCurrentPreferenceOrder = 0;
  
  private boolean mAttachedToActivity = false;
  
  public PreferenceGroup(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    this.mPreferenceList = new ArrayList<>();
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.PreferenceGroup, paramInt1, paramInt2);
    this.mOrderingAsAdded = typedArray.getBoolean(0, this.mOrderingAsAdded);
    typedArray.recycle();
  }
  
  public PreferenceGroup(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public PreferenceGroup(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public void setOrderingAsAdded(boolean paramBoolean) {
    this.mOrderingAsAdded = paramBoolean;
  }
  
  public boolean isOrderingAsAdded() {
    return this.mOrderingAsAdded;
  }
  
  public void addItemFromInflater(Preference paramPreference) {
    addPreference(paramPreference);
  }
  
  public int getPreferenceCount() {
    return this.mPreferenceList.size();
  }
  
  public Preference getPreference(int paramInt) {
    return this.mPreferenceList.get(paramInt);
  }
  
  public boolean addPreference(Preference paramPreference) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mPreferenceList : Ljava/util/List;
    //   4: aload_1
    //   5: invokeinterface contains : (Ljava/lang/Object;)Z
    //   10: ifeq -> 15
    //   13: iconst_1
    //   14: ireturn
    //   15: aload_1
    //   16: invokevirtual getOrder : ()I
    //   19: ldc 2147483647
    //   21: if_icmpne -> 66
    //   24: aload_0
    //   25: getfield mOrderingAsAdded : Z
    //   28: ifeq -> 48
    //   31: aload_0
    //   32: getfield mCurrentPreferenceOrder : I
    //   35: istore_2
    //   36: aload_0
    //   37: iload_2
    //   38: iconst_1
    //   39: iadd
    //   40: putfield mCurrentPreferenceOrder : I
    //   43: aload_1
    //   44: iload_2
    //   45: invokevirtual setOrder : (I)V
    //   48: aload_1
    //   49: instanceof android/preference/PreferenceGroup
    //   52: ifeq -> 66
    //   55: aload_1
    //   56: checkcast android/preference/PreferenceGroup
    //   59: aload_0
    //   60: getfield mOrderingAsAdded : Z
    //   63: invokevirtual setOrderingAsAdded : (Z)V
    //   66: aload_0
    //   67: aload_1
    //   68: invokevirtual onPrepareAddPreference : (Landroid/preference/Preference;)Z
    //   71: ifne -> 76
    //   74: iconst_0
    //   75: ireturn
    //   76: aload_0
    //   77: monitorenter
    //   78: aload_0
    //   79: getfield mPreferenceList : Ljava/util/List;
    //   82: aload_1
    //   83: invokestatic binarySearch : (Ljava/util/List;Ljava/lang/Object;)I
    //   86: istore_3
    //   87: iload_3
    //   88: istore_2
    //   89: iload_3
    //   90: ifge -> 99
    //   93: iload_3
    //   94: iconst_m1
    //   95: imul
    //   96: iconst_1
    //   97: isub
    //   98: istore_2
    //   99: aload_0
    //   100: getfield mPreferenceList : Ljava/util/List;
    //   103: iload_2
    //   104: aload_1
    //   105: invokeinterface add : (ILjava/lang/Object;)V
    //   110: aload_0
    //   111: monitorexit
    //   112: aload_1
    //   113: aload_0
    //   114: invokevirtual getPreferenceManager : ()Landroid/preference/PreferenceManager;
    //   117: invokevirtual onAttachedToHierarchy : (Landroid/preference/PreferenceManager;)V
    //   120: aload_1
    //   121: aload_0
    //   122: invokevirtual assignParent : (Landroid/preference/PreferenceGroup;)V
    //   125: aload_0
    //   126: getfield mAttachedToActivity : Z
    //   129: ifeq -> 136
    //   132: aload_1
    //   133: invokevirtual onAttachedToActivity : ()V
    //   136: aload_0
    //   137: invokevirtual notifyHierarchyChanged : ()V
    //   140: iconst_1
    //   141: ireturn
    //   142: astore_1
    //   143: aload_0
    //   144: monitorexit
    //   145: aload_1
    //   146: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #142	-> 0
    //   #144	-> 13
    //   #147	-> 15
    //   #148	-> 24
    //   #149	-> 31
    //   #152	-> 48
    //   #155	-> 55
    //   #159	-> 66
    //   #160	-> 74
    //   #163	-> 76
    //   #164	-> 78
    //   #165	-> 87
    //   #166	-> 93
    //   #168	-> 99
    //   #169	-> 110
    //   #171	-> 112
    //   #172	-> 120
    //   #174	-> 125
    //   #175	-> 132
    //   #178	-> 136
    //   #180	-> 140
    //   #169	-> 142
    // Exception table:
    //   from	to	target	type
    //   78	87	142	finally
    //   99	110	142	finally
    //   110	112	142	finally
    //   143	145	142	finally
  }
  
  public boolean removePreference(Preference paramPreference) {
    boolean bool = removePreferenceInt(paramPreference);
    notifyHierarchyChanged();
    return bool;
  }
  
  private boolean removePreferenceInt(Preference paramPreference) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: invokevirtual onPrepareForRemoval : ()V
    //   6: aload_1
    //   7: invokevirtual getParent : ()Landroid/preference/PreferenceGroup;
    //   10: aload_0
    //   11: if_acmpne -> 19
    //   14: aload_1
    //   15: aconst_null
    //   16: invokevirtual assignParent : (Landroid/preference/PreferenceGroup;)V
    //   19: aload_0
    //   20: getfield mPreferenceList : Ljava/util/List;
    //   23: aload_1
    //   24: invokeinterface remove : (Ljava/lang/Object;)Z
    //   29: istore_2
    //   30: aload_0
    //   31: monitorexit
    //   32: iload_2
    //   33: ireturn
    //   34: astore_1
    //   35: aload_0
    //   36: monitorexit
    //   37: aload_1
    //   38: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #196	-> 0
    //   #197	-> 2
    //   #198	-> 6
    //   #199	-> 14
    //   #201	-> 19
    //   #202	-> 34
    // Exception table:
    //   from	to	target	type
    //   2	6	34	finally
    //   6	14	34	finally
    //   14	19	34	finally
    //   19	32	34	finally
    //   35	37	34	finally
  }
  
  public void removeAll() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mPreferenceList : Ljava/util/List;
    //   6: astore_1
    //   7: aload_1
    //   8: invokeinterface size : ()I
    //   13: iconst_1
    //   14: isub
    //   15: istore_2
    //   16: iload_2
    //   17: iflt -> 41
    //   20: aload_0
    //   21: aload_1
    //   22: iconst_0
    //   23: invokeinterface get : (I)Ljava/lang/Object;
    //   28: checkcast android/preference/Preference
    //   31: invokespecial removePreferenceInt : (Landroid/preference/Preference;)Z
    //   34: pop
    //   35: iinc #2, -1
    //   38: goto -> 16
    //   41: aload_0
    //   42: monitorexit
    //   43: aload_0
    //   44: invokevirtual notifyHierarchyChanged : ()V
    //   47: return
    //   48: astore_1
    //   49: aload_0
    //   50: monitorexit
    //   51: aload_1
    //   52: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #209	-> 0
    //   #210	-> 2
    //   #211	-> 7
    //   #212	-> 20
    //   #211	-> 35
    //   #214	-> 41
    //   #215	-> 43
    //   #216	-> 47
    //   #214	-> 48
    // Exception table:
    //   from	to	target	type
    //   2	7	48	finally
    //   7	16	48	finally
    //   20	35	48	finally
    //   41	43	48	finally
    //   49	51	48	finally
  }
  
  protected boolean onPrepareAddPreference(Preference paramPreference) {
    paramPreference.onParentChanged(this, shouldDisableDependents());
    return true;
  }
  
  public Preference findPreference(CharSequence paramCharSequence) {
    if (TextUtils.equals(getKey(), paramCharSequence))
      return this; 
    int i = getPreferenceCount();
    for (byte b = 0; b < i; b++) {
      Preference preference = getPreference(b);
      String str = preference.getKey();
      if (str != null && str.equals(paramCharSequence))
        return preference; 
      if (preference instanceof PreferenceGroup) {
        PreferenceGroup preferenceGroup = (PreferenceGroup)preference;
        Preference preference1 = preferenceGroup.findPreference(paramCharSequence);
        if (preference1 != null)
          return preference1; 
      } 
    } 
    return null;
  }
  
  protected boolean isOnSameScreenAsChildren() {
    return true;
  }
  
  protected void onAttachedToActivity() {
    super.onAttachedToActivity();
    this.mAttachedToActivity = true;
    int i = getPreferenceCount();
    for (byte b = 0; b < i; b++)
      getPreference(b).onAttachedToActivity(); 
  }
  
  protected void onPrepareForRemoval() {
    super.onPrepareForRemoval();
    this.mAttachedToActivity = false;
  }
  
  public void notifyDependencyChange(boolean paramBoolean) {
    super.notifyDependencyChange(paramBoolean);
    int i = getPreferenceCount();
    for (byte b = 0; b < i; b++)
      getPreference(b).onParentChanged(this, paramBoolean); 
  }
  
  void sortPreferences() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mPreferenceList : Ljava/util/List;
    //   6: invokestatic sort : (Ljava/util/List;)V
    //   9: aload_0
    //   10: monitorexit
    //   11: return
    //   12: astore_1
    //   13: aload_0
    //   14: monitorexit
    //   15: aload_1
    //   16: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #314	-> 0
    //   #315	-> 2
    //   #316	-> 9
    //   #317	-> 11
    //   #316	-> 12
    // Exception table:
    //   from	to	target	type
    //   2	9	12	finally
    //   9	11	12	finally
    //   13	15	12	finally
  }
  
  protected void dispatchSaveInstanceState(Bundle paramBundle) {
    super.dispatchSaveInstanceState(paramBundle);
    int i = getPreferenceCount();
    for (byte b = 0; b < i; b++)
      getPreference(b).dispatchSaveInstanceState(paramBundle); 
  }
  
  protected void dispatchRestoreInstanceState(Bundle paramBundle) {
    super.dispatchRestoreInstanceState(paramBundle);
    int i = getPreferenceCount();
    for (byte b = 0; b < i; b++)
      getPreference(b).dispatchRestoreInstanceState(paramBundle); 
  }
}
