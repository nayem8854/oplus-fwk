package android.provider;

import android.content.ClipDescription;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.IntentSender;
import android.content.MimeTypeFilter;
import android.content.UriMatcher;
import android.content.pm.ProviderInfo;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.ParcelableException;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Objects;
import libcore.io.IoUtils;

public abstract class DocumentsProvider extends ContentProvider {
  private static final int MATCH_CHILDREN = 6;
  
  private static final int MATCH_CHILDREN_TREE = 8;
  
  private static final int MATCH_DOCUMENT = 5;
  
  private static final int MATCH_DOCUMENT_TREE = 7;
  
  private static final int MATCH_RECENT = 3;
  
  private static final int MATCH_ROOT = 2;
  
  private static final int MATCH_ROOTS = 1;
  
  private static final int MATCH_SEARCH = 4;
  
  private static final String TAG = "DocumentsProvider";
  
  private String mAuthority;
  
  private UriMatcher mMatcher;
  
  public void attachInfo(Context paramContext, ProviderInfo paramProviderInfo) {
    registerAuthority(paramProviderInfo.authority);
    if (paramProviderInfo.exported) {
      if (paramProviderInfo.grantUriPermissions) {
        if ("android.permission.MANAGE_DOCUMENTS".equals(paramProviderInfo.readPermission)) {
          String str = paramProviderInfo.writePermission;
          if ("android.permission.MANAGE_DOCUMENTS".equals(str)) {
            super.attachInfo(paramContext, paramProviderInfo);
            return;
          } 
        } 
        throw new SecurityException("Provider must be protected by MANAGE_DOCUMENTS");
      } 
      throw new SecurityException("Provider must grantUriPermissions");
    } 
    throw new SecurityException("Provider must be exported");
  }
  
  public void attachInfoForTesting(Context paramContext, ProviderInfo paramProviderInfo) {
    registerAuthority(paramProviderInfo.authority);
    super.attachInfoForTesting(paramContext, paramProviderInfo);
  }
  
  private void registerAuthority(String paramString) {
    this.mAuthority = paramString;
    UriMatcher uriMatcher = new UriMatcher(-1);
    uriMatcher.addURI(this.mAuthority, "root", 1);
    this.mMatcher.addURI(this.mAuthority, "root/*", 2);
    this.mMatcher.addURI(this.mAuthority, "root/*/recent", 3);
    this.mMatcher.addURI(this.mAuthority, "root/*/search", 4);
    this.mMatcher.addURI(this.mAuthority, "document/*", 5);
    this.mMatcher.addURI(this.mAuthority, "document/*/children", 6);
    this.mMatcher.addURI(this.mAuthority, "tree/*/document/*", 7);
    this.mMatcher.addURI(this.mAuthority, "tree/*/document/*/children", 8);
  }
  
  public boolean isChildDocument(String paramString1, String paramString2) {
    return false;
  }
  
  private void enforceTree(Uri paramUri) {
    if (DocumentsContract.isTreeUri(paramUri)) {
      String str2 = DocumentsContract.getTreeDocumentId(paramUri);
      String str1 = DocumentsContract.getDocumentId(paramUri);
      if (Objects.equals(str2, str1))
        return; 
      if (!isChildDocument(str2, str1)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Document ");
        stringBuilder.append(str1);
        stringBuilder.append(" is not a descendant of ");
        stringBuilder.append(str2);
        throw new SecurityException(stringBuilder.toString());
      } 
    } 
  }
  
  public String createDocument(String paramString1, String paramString2, String paramString3) throws FileNotFoundException {
    throw new UnsupportedOperationException("Create not supported");
  }
  
  public String renameDocument(String paramString1, String paramString2) throws FileNotFoundException {
    throw new UnsupportedOperationException("Rename not supported");
  }
  
  public void deleteDocument(String paramString) throws FileNotFoundException {
    throw new UnsupportedOperationException("Delete not supported");
  }
  
  public String copyDocument(String paramString1, String paramString2) throws FileNotFoundException {
    throw new UnsupportedOperationException("Copy not supported");
  }
  
  public String moveDocument(String paramString1, String paramString2, String paramString3) throws FileNotFoundException {
    throw new UnsupportedOperationException("Move not supported");
  }
  
  public void removeDocument(String paramString1, String paramString2) throws FileNotFoundException {
    throw new UnsupportedOperationException("Remove not supported");
  }
  
  public DocumentsContract.Path findDocumentPath(String paramString1, String paramString2) throws FileNotFoundException {
    throw new UnsupportedOperationException("findDocumentPath not supported.");
  }
  
  public IntentSender createWebLinkIntent(String paramString, Bundle paramBundle) throws FileNotFoundException {
    throw new UnsupportedOperationException("createWebLink is not supported.");
  }
  
  public Cursor queryRecentDocuments(String paramString, String[] paramArrayOfString) throws FileNotFoundException {
    throw new UnsupportedOperationException("Recent not supported");
  }
  
  public Cursor queryRecentDocuments(String paramString, String[] paramArrayOfString, Bundle paramBundle, CancellationSignal paramCancellationSignal) throws FileNotFoundException {
    Preconditions.checkNotNull(paramString, "rootId can not be null");
    Cursor cursor = queryRecentDocuments(paramString, paramArrayOfString);
    Bundle bundle = new Bundle();
    cursor.setExtras(bundle);
    bundle.putStringArray("android.content.extra.HONORED_ARGS", new String[0]);
    return cursor;
  }
  
  public Cursor queryChildDocuments(String paramString, String[] paramArrayOfString, Bundle paramBundle) throws FileNotFoundException {
    String str = getSortClause(paramBundle);
    return queryChildDocuments(paramString, paramArrayOfString, str);
  }
  
  public Cursor queryChildDocumentsForManage(String paramString1, String[] paramArrayOfString, String paramString2) throws FileNotFoundException {
    throw new UnsupportedOperationException("Manage not supported");
  }
  
  public Cursor querySearchDocuments(String paramString1, String paramString2, String[] paramArrayOfString) throws FileNotFoundException {
    throw new UnsupportedOperationException("Search not supported");
  }
  
  public Cursor querySearchDocuments(String paramString, String[] paramArrayOfString, Bundle paramBundle) throws FileNotFoundException {
    Preconditions.checkNotNull(paramString, "rootId can not be null");
    Preconditions.checkNotNull(paramBundle, "queryArgs can not be null");
    return querySearchDocuments(paramString, DocumentsContract.getSearchDocumentsQuery(paramBundle), paramArrayOfString);
  }
  
  public void ejectRoot(String paramString) {
    throw new UnsupportedOperationException("Eject not supported");
  }
  
  public Bundle getDocumentMetadata(String paramString) throws FileNotFoundException {
    throw new UnsupportedOperationException("Metadata not supported");
  }
  
  public String getDocumentType(String paramString) throws FileNotFoundException {
    Cursor cursor = queryDocument(paramString, (String[])null);
    try {
      if (cursor.moveToFirst())
        return cursor.getString(cursor.getColumnIndexOrThrow("mime_type")); 
      return null;
    } finally {
      IoUtils.closeQuietly((AutoCloseable)cursor);
    } 
  }
  
  public AssetFileDescriptor openDocumentThumbnail(String paramString, Point paramPoint, CancellationSignal paramCancellationSignal) throws FileNotFoundException {
    throw new UnsupportedOperationException("Thumbnails not supported");
  }
  
  public AssetFileDescriptor openTypedDocument(String paramString1, String paramString2, Bundle paramBundle, CancellationSignal paramCancellationSignal) throws FileNotFoundException {
    throw new FileNotFoundException("The requested MIME type is not supported.");
  }
  
  public final Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2) {
    throw new UnsupportedOperationException("Pre-Android-O query format not supported.");
  }
  
  public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal) {
    throw new UnsupportedOperationException("Pre-Android-O query format not supported.");
  }
  
  public final Cursor query(Uri paramUri, String[] paramArrayOfString, Bundle paramBundle, CancellationSignal paramCancellationSignal) {
    try {
      String str1;
      UnsupportedOperationException unsupportedOperationException;
      StringBuilder stringBuilder;
      String str2;
      switch (this.mMatcher.match(paramUri)) {
        default:
          unsupportedOperationException = new UnsupportedOperationException();
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Unsupported Uri ");
          stringBuilder.append(paramUri);
          this(stringBuilder.toString());
          throw unsupportedOperationException;
        case 6:
        case 8:
          enforceTree(paramUri);
          if (DocumentsContract.isManageMode(paramUri)) {
            str1 = DocumentsContract.getDocumentId(paramUri);
            str2 = getSortClause((Bundle)stringBuilder);
            return queryChildDocumentsForManage(str1, (String[])unsupportedOperationException, str2);
          } 
          return queryChildDocuments(DocumentsContract.getDocumentId((Uri)str1), (String[])unsupportedOperationException, (Bundle)str2);
        case 5:
        case 7:
          enforceTree((Uri)str1);
          return queryDocument(DocumentsContract.getDocumentId((Uri)str1), (String[])unsupportedOperationException);
        case 4:
          return querySearchDocuments(DocumentsContract.getRootId((Uri)str1), (String[])unsupportedOperationException, (Bundle)str2);
        case 3:
          str1 = DocumentsContract.getRootId((Uri)str1);
          return queryRecentDocuments(str1, (String[])unsupportedOperationException, (Bundle)str2, paramCancellationSignal);
        case 1:
          break;
      } 
      return queryRoots((String[])unsupportedOperationException);
    } catch (FileNotFoundException fileNotFoundException) {
      Log.w("DocumentsProvider", "Failed during query", fileNotFoundException);
      return null;
    } 
  }
  
  private static String getSortClause(Bundle paramBundle) {
    if (paramBundle == null)
      paramBundle = Bundle.EMPTY; 
    String str1 = paramBundle.getString("android:query-arg-sql-sort-order");
    String str2 = str1;
    if (str1 == null) {
      str2 = str1;
      if (paramBundle.containsKey("android:query-arg-sort-columns"))
        str2 = ContentResolver.createSqlSortClause(paramBundle); 
    } 
    return str2;
  }
  
  public final String getType(Uri paramUri) {
    try {
      int i = this.mMatcher.match(paramUri);
      if (i != 2) {
        if (i != 5 && i != 7)
          return null; 
        enforceTree(paramUri);
        return getDocumentType(DocumentsContract.getDocumentId(paramUri));
      } 
      return "vnd.android.document/root";
    } catch (FileNotFoundException fileNotFoundException) {
      Log.w("DocumentsProvider", "Failed during getType", fileNotFoundException);
      return null;
    } 
  }
  
  public Uri canonicalize(Uri paramUri) {
    Context context = getContext();
    if (this.mMatcher.match(paramUri) != 7)
      return null; 
    enforceTree(paramUri);
    Uri uri = DocumentsContract.buildDocumentUri(paramUri.getAuthority(), DocumentsContract.getDocumentId(paramUri));
    int i = getCallingOrSelfUriPermissionModeFlags(context, paramUri);
    context.grantUriPermission(getCallingPackage(), uri, i);
    return uri;
  }
  
  private static int getCallingOrSelfUriPermissionModeFlags(Context paramContext, Uri paramUri) {
    int i = 0;
    if (paramContext.checkCallingOrSelfUriPermission(paramUri, 1) == 0)
      i = false | true; 
    int j = i;
    if (paramContext.checkCallingOrSelfUriPermission(paramUri, 2) == 0)
      j = i | 0x2; 
    i = j;
    if (paramContext.checkCallingOrSelfUriPermission(paramUri, 65) == 0)
      i = j | 0x40; 
    return i;
  }
  
  public final Uri insert(Uri paramUri, ContentValues paramContentValues) {
    throw new UnsupportedOperationException("Insert not supported");
  }
  
  public final int delete(Uri paramUri, String paramString, String[] paramArrayOfString) {
    throw new UnsupportedOperationException("Delete not supported");
  }
  
  public final int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString) {
    throw new UnsupportedOperationException("Update not supported");
  }
  
  public Bundle call(String paramString1, String paramString2, Bundle paramBundle) {
    if (!paramString1.startsWith("android:"))
      return super.call(paramString1, paramString2, paramBundle); 
    try {
      return callUnchecked(paramString1, paramString2, paramBundle);
    } catch (FileNotFoundException fileNotFoundException) {
      throw new ParcelableException(fileNotFoundException);
    } 
  }
  
  private Bundle callUnchecked(String paramString1, String paramString2, Bundle paramBundle) throws FileNotFoundException {
    String str1;
    Context context = getContext();
    Bundle bundle = new Bundle();
    if ("android:ejectRoot".equals(paramString1)) {
      Uri uri1 = paramBundle.<Uri>getParcelable("uri");
      enforceWritePermissionInner(uri1, getCallingPackage(), getCallingAttributionTag(), null);
      str1 = DocumentsContract.getRootId(uri1);
      ejectRoot(str1);
      return bundle;
    } 
    Uri uri = paramBundle.<Uri>getParcelable("uri");
    String str2 = uri.getAuthority();
    paramString2 = DocumentsContract.getDocumentId(uri);
    if (this.mAuthority.equals(str2)) {
      String str3, str4;
      enforceTree(uri);
      if ("android:isChildDocument".equals(str1)) {
        boolean bool;
        str1 = getCallingPackage();
        String str = getCallingAttributionTag();
        enforceReadPermissionInner(uri, str1, str, null);
        Uri uri1 = paramBundle.<Uri>getParcelable("android.content.extra.TARGET_URI");
        str1 = uri1.getAuthority();
        str3 = DocumentsContract.getDocumentId(uri1);
        str4 = this.mAuthority;
        if (str4.equals(str1) && 
          isChildDocument(paramString2, str3)) {
          bool = true;
        } else {
          bool = false;
        } 
        bundle.putBoolean("result", bool);
      } else {
        Uri uri1;
        if ("android:createDocument".equals(str1)) {
          str1 = getCallingPackage();
          String str = getCallingAttributionTag();
          enforceWritePermissionInner((Uri)str4, str1, str, null);
          str1 = str3.getString("mime_type");
          str3 = str3.getString("_display_name");
          str1 = createDocument(paramString2, str1, str3);
          uri1 = DocumentsContract.buildDocumentUriMaybeUsingTree((Uri)str4, str1);
          bundle.putParcelable("uri", uri1);
        } else {
          IntentSender intentSender;
          String str;
          if ("android:createWebLinkIntent".equals(uri1)) {
            String str5 = getCallingPackage();
            str = getCallingAttributionTag();
            enforceWritePermissionInner((Uri)str4, str5, str, null);
            Bundle bundle1 = str3.getBundle("options");
            intentSender = createWebLinkIntent(paramString2, bundle1);
            bundle.putParcelable("result", (Parcelable)intentSender);
          } else {
            Uri uri2;
            if ("android:renameDocument".equals(intentSender)) {
              String str5 = getCallingPackage();
              str2 = getCallingAttributionTag();
              enforceWritePermissionInner((Uri)str4, str5, str2, null);
              str5 = str3.getString("_display_name");
              str5 = renameDocument(paramString2, str5);
              if (str5 != null) {
                uri2 = DocumentsContract.buildDocumentUriMaybeUsingTree((Uri)str4, str5);
                if (!DocumentsContract.isTreeUri(uri2)) {
                  int i = getCallingOrSelfUriPermissionModeFlags((Context)str, (Uri)str4);
                  str.grantUriPermission(getCallingPackage(), uri2, i);
                } 
                bundle.putParcelable("uri", uri2);
                revokeDocumentPermission(paramString2);
              } 
            } else {
              String str5;
              if ("android:deleteDocument".equals(uri2)) {
                str5 = getCallingPackage();
                str3 = getCallingAttributionTag();
                enforceWritePermissionInner((Uri)str4, str5, str3, null);
                deleteDocument(paramString2);
                revokeDocumentPermission(paramString2);
              } else {
                Uri uri3;
                if ("android:copyDocument".equals(str5)) {
                  Uri uri4 = str3.<Uri>getParcelable("android.content.extra.TARGET_URI");
                  str3 = DocumentsContract.getDocumentId(uri4);
                  str5 = getCallingPackage();
                  String str6 = getCallingAttributionTag();
                  enforceReadPermissionInner((Uri)str4, str5, str6, null);
                  enforceWritePermissionInner(uri4, getCallingPackage(), getCallingAttributionTag(), null);
                  str5 = copyDocument(paramString2, str3);
                  if (str5 != null) {
                    uri3 = DocumentsContract.buildDocumentUriMaybeUsingTree((Uri)str4, str5);
                    if (!DocumentsContract.isTreeUri(uri3)) {
                      int i = getCallingOrSelfUriPermissionModeFlags((Context)str, (Uri)str4);
                      str.grantUriPermission(getCallingPackage(), uri3, i);
                    } 
                    bundle.putParcelable("uri", uri3);
                  } 
                } else {
                  Uri uri4;
                  if ("android:moveDocument".equals(uri3)) {
                    uri3 = str3.<Uri>getParcelable("parentUri");
                    str2 = DocumentsContract.getDocumentId(uri3);
                    Uri uri5 = str3.<Uri>getParcelable("android.content.extra.TARGET_URI");
                    str3 = DocumentsContract.getDocumentId(uri5);
                    String str7 = getCallingPackage();
                    String str8 = getCallingAttributionTag();
                    enforceWritePermissionInner((Uri)str4, str7, str8, null);
                    str8 = getCallingPackage();
                    str7 = getCallingAttributionTag();
                    enforceReadPermissionInner(uri3, str8, str7, null);
                    enforceWritePermissionInner(uri5, getCallingPackage(), getCallingAttributionTag(), null);
                    String str6 = moveDocument(paramString2, str2, str3);
                    if (str6 != null) {
                      uri4 = DocumentsContract.buildDocumentUriMaybeUsingTree((Uri)str4, str6);
                      if (!DocumentsContract.isTreeUri(uri4)) {
                        int i = getCallingOrSelfUriPermissionModeFlags((Context)str, (Uri)str4);
                        str.grantUriPermission(getCallingPackage(), uri4, i);
                      } 
                      bundle.putParcelable("uri", uri4);
                    } 
                  } else {
                    String str6;
                    if ("android:removeDocument".equals(uri4)) {
                      Uri uri5 = str3.<Uri>getParcelable("parentUri");
                      str6 = DocumentsContract.getDocumentId(uri5);
                      str3 = getCallingPackage();
                      str2 = getCallingAttributionTag();
                      enforceReadPermissionInner(uri5, str3, str2, null);
                      String str7 = getCallingPackage();
                      str3 = getCallingAttributionTag();
                      enforceWritePermissionInner((Uri)str4, str7, str3, null);
                      removeDocument(paramString2, str6);
                    } else {
                      DocumentsContract.Path path1, path2;
                      if ("android:findDocumentPath".equals(str6)) {
                        boolean bool = DocumentsContract.isTreeUri((Uri)str4);
                        if (bool) {
                          str3 = getCallingPackage();
                          str6 = getCallingAttributionTag();
                          enforceReadPermissionInner((Uri)str4, str3, str6, null);
                        } else {
                          getContext().enforceCallingPermission("android.permission.MANAGE_DOCUMENTS", null);
                        } 
                        if (bool) {
                          str3 = DocumentsContract.getTreeDocumentId((Uri)str4);
                        } else {
                          str3 = null;
                        } 
                        DocumentsContract.Path path = findDocumentPath(str3, paramString2);
                        path2 = path;
                        if (bool) {
                          path1 = path;
                          if (!Objects.equals(path.getPath().get(0), str3)) {
                            StringBuilder stringBuilder2 = new StringBuilder();
                            stringBuilder2.append("Provider doesn't return path from the tree root. Expected: ");
                            stringBuilder2.append(str3);
                            stringBuilder2.append(" found: ");
                            stringBuilder2.append(path.getPath().get(0));
                            String str7 = stringBuilder2.toString();
                            Log.wtf("DocumentsProvider", str7);
                            LinkedList<String> linkedList = new LinkedList<>(path.getPath());
                            while (linkedList.size() > 1 && !Objects.equals(linkedList.getFirst(), str3))
                              linkedList.removeFirst(); 
                            path1 = new DocumentsContract.Path(null, linkedList);
                          } 
                          path2 = path1;
                          if (path1.getRootId() != null) {
                            StringBuilder stringBuilder2 = new StringBuilder();
                            stringBuilder2.append("Provider returns root id :");
                            stringBuilder2.append(path1.getRootId());
                            stringBuilder2.append(" unexpectedly. Erase root id.");
                            String str7 = stringBuilder2.toString();
                            Log.wtf("DocumentsProvider", str7);
                            path2 = new DocumentsContract.Path(null, path1.getPath());
                          } 
                        } 
                        bundle.putParcelable("result", path2);
                        return bundle;
                      } 
                      if ("android:getDocumentMetadata".equals(path1))
                        return getDocumentMetadata((String)path2); 
                      StringBuilder stringBuilder1 = new StringBuilder();
                      stringBuilder1.append("Method not supported ");
                      stringBuilder1.append((String)path1);
                      throw new UnsupportedOperationException(stringBuilder1.toString());
                    } 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
      return bundle;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Requested authority ");
    stringBuilder.append(str2);
    stringBuilder.append(" doesn't match provider ");
    stringBuilder.append(this.mAuthority);
    throw new SecurityException(stringBuilder.toString());
  }
  
  public final void revokeDocumentPermission(String paramString) {
    Context context = getContext();
    context.revokeUriPermission(DocumentsContract.buildDocumentUri(this.mAuthority, paramString), -1);
    context.revokeUriPermission(DocumentsContract.buildTreeDocumentUri(this.mAuthority, paramString), -1);
  }
  
  public final ParcelFileDescriptor openFile(Uri paramUri, String paramString) throws FileNotFoundException {
    enforceTree(paramUri);
    return openDocument(DocumentsContract.getDocumentId(paramUri), paramString, (CancellationSignal)null);
  }
  
  public final ParcelFileDescriptor openFile(Uri paramUri, String paramString, CancellationSignal paramCancellationSignal) throws FileNotFoundException {
    enforceTree(paramUri);
    return openDocument(DocumentsContract.getDocumentId(paramUri), paramString, paramCancellationSignal);
  }
  
  public final AssetFileDescriptor openAssetFile(Uri paramUri, String paramString) throws FileNotFoundException {
    AssetFileDescriptor assetFileDescriptor;
    enforceTree(paramUri);
    String str = DocumentsContract.getDocumentId(paramUri);
    paramUri = null;
    ParcelFileDescriptor parcelFileDescriptor = openDocument(str, paramString, (CancellationSignal)null);
    if (parcelFileDescriptor != null)
      assetFileDescriptor = new AssetFileDescriptor(parcelFileDescriptor, 0L, -1L); 
    return assetFileDescriptor;
  }
  
  public final AssetFileDescriptor openAssetFile(Uri paramUri, String paramString, CancellationSignal paramCancellationSignal) throws FileNotFoundException {
    enforceTree(paramUri);
    ParcelFileDescriptor parcelFileDescriptor = openDocument(DocumentsContract.getDocumentId(paramUri), paramString, paramCancellationSignal);
    if (parcelFileDescriptor != null) {
      AssetFileDescriptor assetFileDescriptor = new AssetFileDescriptor(parcelFileDescriptor, 0L, -1L);
    } else {
      parcelFileDescriptor = null;
    } 
    return (AssetFileDescriptor)parcelFileDescriptor;
  }
  
  public final AssetFileDescriptor openTypedAssetFile(Uri paramUri, String paramString, Bundle paramBundle) throws FileNotFoundException {
    return openTypedAssetFileImpl(paramUri, paramString, paramBundle, (CancellationSignal)null);
  }
  
  public final AssetFileDescriptor openTypedAssetFile(Uri paramUri, String paramString, Bundle paramBundle, CancellationSignal paramCancellationSignal) throws FileNotFoundException {
    return openTypedAssetFileImpl(paramUri, paramString, paramBundle, paramCancellationSignal);
  }
  
  public String[] getDocumentStreamTypes(String paramString1, String paramString2) {
    Cursor cursor1 = null, cursor2 = null;
    try {
      Cursor cursor = queryDocument(paramString1, (String[])null);
      cursor2 = cursor;
      cursor1 = cursor;
      if (cursor.moveToFirst()) {
        cursor2 = cursor;
        cursor1 = cursor;
        String str = cursor.getString(cursor.getColumnIndexOrThrow("mime_type"));
        cursor2 = cursor;
        cursor1 = cursor;
        long l = cursor.getLong(cursor.getColumnIndexOrThrow("flags"));
        if ((0x200L & l) == 0L && str != null) {
          cursor2 = cursor;
          cursor1 = cursor;
          if (MimeTypeFilter.matches(str, paramString2))
            return new String[] { str }; 
        } 
      } 
      return null;
    } catch (FileNotFoundException fileNotFoundException) {
      return null;
    } finally {
      IoUtils.closeQuietly((AutoCloseable)cursor2);
    } 
  }
  
  public String[] getStreamTypes(Uri paramUri, String paramString) {
    enforceTree(paramUri);
    return getDocumentStreamTypes(DocumentsContract.getDocumentId(paramUri), paramString);
  }
  
  private final AssetFileDescriptor openTypedAssetFileImpl(Uri paramUri, String paramString, Bundle paramBundle, CancellationSignal paramCancellationSignal) throws FileNotFoundException {
    Point point;
    enforceTree(paramUri);
    String str1 = DocumentsContract.getDocumentId(paramUri);
    if (paramBundle != null && paramBundle.containsKey("android.content.extra.SIZE")) {
      point = paramBundle.<Point>getParcelable("android.content.extra.SIZE");
      return openDocumentThumbnail(str1, point, paramCancellationSignal);
    } 
    if ("*/*".equals(paramString))
      return openAssetFile((Uri)point, "r"); 
    String str2 = getType((Uri)point);
    if (str2 != null && ClipDescription.compareMimeTypes(str2, paramString))
      return openAssetFile((Uri)point, "r"); 
    return openTypedDocument(str1, paramString, paramBundle, paramCancellationSignal);
  }
  
  public abstract ParcelFileDescriptor openDocument(String paramString1, String paramString2, CancellationSignal paramCancellationSignal) throws FileNotFoundException;
  
  public abstract Cursor queryChildDocuments(String paramString1, String[] paramArrayOfString, String paramString2) throws FileNotFoundException;
  
  public abstract Cursor queryDocument(String paramString, String[] paramArrayOfString) throws FileNotFoundException;
  
  public abstract Cursor queryRoots(String[] paramArrayOfString) throws FileNotFoundException;
}
