package android.provider;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.text.TextUtils;
import com.android.internal.util.ArrayUtils;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.function.Function;

public class SettingsStringUtil {
  public static final String DELIMITER = ":";
  
  public static abstract class ColonDelimitedSet<T> extends HashSet<T> {
    public ColonDelimitedSet(String param1String) {
      for (String str : TextUtils.split(TextUtils.emptyIfNull(param1String), ":"))
        add(itemFromString(str)); 
    }
    
    protected abstract T itemFromString(String param1String);
    
    protected String itemToString(T param1T) {
      return String.valueOf(param1T);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      Iterator<T> iterator = iterator();
      if (iterator.hasNext()) {
        stringBuilder.append(itemToString(iterator.next()));
        while (iterator.hasNext()) {
          stringBuilder.append(":");
          stringBuilder.append(itemToString(iterator.next()));
        } 
      } 
      return stringBuilder.toString();
    }
    
    class OfStrings extends ColonDelimitedSet<String> {
      public OfStrings(SettingsStringUtil.ColonDelimitedSet this$0) {
        super((String)this$0);
      }
      
      protected String itemFromString(String param2String) {
        return param2String;
      }
      
      public static String addAll(String param2String, Collection<String> param2Collection) {
        OfStrings ofStrings = new OfStrings(param2String);
        if (ofStrings.addAll(param2Collection))
          param2String = ofStrings.toString(); 
        return param2String;
      }
      
      public static String add(String param2String1, String param2String2) {
        OfStrings ofStrings = new OfStrings(param2String1);
        if (ofStrings.contains(param2String2))
          return param2String1; 
        ofStrings.add(param2String2);
        return ofStrings.toString();
      }
      
      public static String remove(String param2String1, String param2String2) {
        OfStrings ofStrings = new OfStrings(param2String1);
        if (!ofStrings.contains(param2String2))
          return param2String1; 
        ofStrings.remove(param2String2);
        return ofStrings.toString();
      }
      
      public static boolean contains(String param2String1, String param2String2) {
        boolean bool;
        String[] arrayOfString = TextUtils.split(param2String1, ":");
        if (ArrayUtils.indexOf((Object[])arrayOfString, param2String2) != -1) {
          bool = true;
        } else {
          bool = false;
        } 
        return bool;
      }
    }
  }
  
  class OfStrings extends ColonDelimitedSet<String> {
    public OfStrings(SettingsStringUtil this$0) {
      super((String)this$0);
    }
    
    protected String itemFromString(String param1String) {
      return param1String;
    }
    
    public static String addAll(String param1String, Collection<String> param1Collection) {
      OfStrings ofStrings = new OfStrings(param1String);
      if (ofStrings.addAll(param1Collection))
        param1String = ofStrings.toString(); 
      return param1String;
    }
    
    public static String add(String param1String1, String param1String2) {
      OfStrings ofStrings = new OfStrings(param1String1);
      if (ofStrings.contains(param1String2))
        return param1String1; 
      ofStrings.add(param1String2);
      return ofStrings.toString();
    }
    
    public static String remove(String param1String1, String param1String2) {
      OfStrings ofStrings = new OfStrings(param1String1);
      if (!ofStrings.contains(param1String2))
        return param1String1; 
      ofStrings.remove(param1String2);
      return ofStrings.toString();
    }
    
    public static boolean contains(String param1String1, String param1String2) {
      boolean bool;
      String[] arrayOfString = TextUtils.split(param1String1, ":");
      if (ArrayUtils.indexOf((Object[])arrayOfString, param1String2) != -1) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
  
  class ComponentNameSet extends ColonDelimitedSet<ComponentName> {
    public ComponentNameSet(SettingsStringUtil this$0) {
      super((String)this$0);
    }
    
    protected ComponentName itemFromString(String param1String) {
      return ComponentName.unflattenFromString(param1String);
    }
    
    protected String itemToString(ComponentName param1ComponentName) {
      String str;
      if (param1ComponentName != null) {
        str = param1ComponentName.flattenToString();
      } else {
        str = "null";
      } 
      return str;
    }
    
    public static String add(String param1String, ComponentName param1ComponentName) {
      ComponentNameSet componentNameSet = new ComponentNameSet(param1String);
      if (componentNameSet.contains(param1ComponentName))
        return param1String; 
      componentNameSet.add(param1ComponentName);
      return componentNameSet.toString();
    }
    
    public static String remove(String param1String, ComponentName param1ComponentName) {
      ComponentNameSet componentNameSet = new ComponentNameSet(param1String);
      if (!componentNameSet.contains(param1ComponentName))
        return param1String; 
      componentNameSet.remove(param1ComponentName);
      return componentNameSet.toString();
    }
    
    public static boolean contains(String param1String, ComponentName param1ComponentName) {
      String str = param1ComponentName.flattenToString();
      return SettingsStringUtil.ColonDelimitedSet.OfStrings.contains(param1String, str);
    }
  }
  
  public static class SettingStringHelper {
    private final ContentResolver mContentResolver;
    
    private final String mSettingName;
    
    private final int mUserId;
    
    public SettingStringHelper(ContentResolver param1ContentResolver, String param1String, int param1Int) {
      this.mContentResolver = param1ContentResolver;
      this.mUserId = param1Int;
      this.mSettingName = param1String;
    }
    
    public String read() {
      return Settings.Secure.getStringForUser(this.mContentResolver, this.mSettingName, this.mUserId);
    }
    
    public boolean write(String param1String) {
      return Settings.Secure.putStringForUser(this.mContentResolver, this.mSettingName, param1String, this.mUserId);
    }
    
    public boolean modify(Function<String, String> param1Function) {
      return write(param1Function.apply(read()));
    }
  }
}
