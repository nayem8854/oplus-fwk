package android.provider;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import java.util.concurrent.Semaphore;

public class SearchRecentSuggestions {
  private static final String LOG_TAG = "SearchSuggestions";
  
  private static final int MAX_HISTORY_COUNT = 250;
  
  class SuggestionColumns implements BaseColumns {
    public static final String DATE = "date";
    
    public static final String DISPLAY1 = "display1";
    
    public static final String DISPLAY2 = "display2";
    
    public static final String QUERY = "query";
  }
  
  public static final String[] QUERIES_PROJECTION_1LINE = new String[] { "_id", "date", "query", "display1" };
  
  public static final String[] QUERIES_PROJECTION_2LINE = new String[] { "_id", "date", "query", "display1", "display2" };
  
  public static final int QUERIES_PROJECTION_DATE_INDEX = 1;
  
  public static final int QUERIES_PROJECTION_DISPLAY1_INDEX = 3;
  
  public static final int QUERIES_PROJECTION_DISPLAY2_INDEX = 4;
  
  public static final int QUERIES_PROJECTION_QUERY_INDEX = 2;
  
  private static final Semaphore sWritesInProgress = new Semaphore(0);
  
  private final String mAuthority;
  
  private final Context mContext;
  
  private final Uri mSuggestionsUri;
  
  private final boolean mTwoLineDisplay;
  
  public SearchRecentSuggestions(Context paramContext, String paramString, int paramInt) {
    if (!TextUtils.isEmpty(paramString) && (paramInt & 0x1) != 0) {
      boolean bool;
      if ((paramInt & 0x2) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mTwoLineDisplay = bool;
      this.mContext = paramContext;
      this.mAuthority = new String(paramString);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("content://");
      stringBuilder.append(this.mAuthority);
      stringBuilder.append("/suggestions");
      this.mSuggestionsUri = Uri.parse(stringBuilder.toString());
      return;
    } 
    throw new IllegalArgumentException();
  }
  
  public void saveRecentQuery(final String queryString, final String line2) {
    if (TextUtils.isEmpty(queryString))
      return; 
    if (this.mTwoLineDisplay || TextUtils.isEmpty(line2)) {
      Thread thread = new Thread("saveRecentQuery") {
          final SearchRecentSuggestions this$0;
          
          final String val$line2;
          
          final String val$queryString;
          
          public void run() {
            SearchRecentSuggestions.this.saveRecentQueryBlocking(queryString, line2);
            SearchRecentSuggestions.sWritesInProgress.release();
          }
        };
      thread.start();
      return;
    } 
    throw new IllegalArgumentException();
  }
  
  void waitForSave() {
    do {
      sWritesInProgress.acquireUninterruptibly();
    } while (sWritesInProgress.availablePermits() > 0);
  }
  
  private void saveRecentQueryBlocking(String paramString1, String paramString2) {
    ContentResolver contentResolver = this.mContext.getContentResolver();
    long l = System.currentTimeMillis();
    try {
      ContentValues contentValues = new ContentValues();
      this();
      contentValues.put("display1", paramString1);
      if (this.mTwoLineDisplay)
        contentValues.put("display2", paramString2); 
      contentValues.put("query", paramString1);
      contentValues.put("date", Long.valueOf(l));
      contentResolver.insert(this.mSuggestionsUri, contentValues);
    } catch (RuntimeException runtimeException) {
      Log.e("SearchSuggestions", "saveRecentQuery", runtimeException);
    } 
    truncateHistory(contentResolver, 250);
  }
  
  public void clearHistory() {
    ContentResolver contentResolver = this.mContext.getContentResolver();
    truncateHistory(contentResolver, 0);
  }
  
  protected void truncateHistory(ContentResolver paramContentResolver, int paramInt) {
    String str;
    if (paramInt >= 0) {
      StringBuilder stringBuilder = null;
      if (paramInt > 0) {
        try {
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("_id IN (SELECT _id FROM suggestions ORDER BY date DESC LIMIT -1 OFFSET ");
          stringBuilder.append(String.valueOf(paramInt));
          stringBuilder.append(")");
          str = stringBuilder.toString();
          paramContentResolver.delete(this.mSuggestionsUri, str, null);
        } catch (RuntimeException runtimeException) {
          Log.e("SearchSuggestions", "truncateHistory", runtimeException);
        } 
        return;
      } 
    } else {
      throw new IllegalArgumentException();
    } 
    runtimeException.delete(this.mSuggestionsUri, str, null);
  }
}
