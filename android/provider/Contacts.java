package android.provider;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import java.io.InputStream;

@Deprecated
public class Contacts {
  @Deprecated
  public static final String AUTHORITY = "contacts";
  
  @Deprecated
  public static final Uri CONTENT_URI = Uri.parse("content://contacts");
  
  @Deprecated
  public static final int KIND_EMAIL = 1;
  
  @Deprecated
  public static final int KIND_IM = 3;
  
  @Deprecated
  public static final int KIND_ORGANIZATION = 4;
  
  @Deprecated
  public static final int KIND_PHONE = 5;
  
  @Deprecated
  public static final int KIND_POSTAL = 2;
  
  private static final String TAG = "Contacts";
  
  @Deprecated
  class Settings implements BaseColumns, SettingsColumns {
    @Deprecated
    public static final String CONTENT_DIRECTORY = "settings";
    
    @Deprecated
    public static final Uri CONTENT_URI = Uri.parse("content://contacts/settings");
    
    @Deprecated
    public static final String DEFAULT_SORT_ORDER = "key ASC";
    
    @Deprecated
    public static final String SYNC_EVERYTHING = "syncEverything";
    
    @Deprecated
    public static String getSetting(ContentResolver param1ContentResolver, String param1String1, String param1String2) {
      Cursor cursor = param1ContentResolver.query(CONTENT_URI, new String[] { "value" }, "key=?", new String[] { param1String2 }, null);
      try {
        boolean bool = cursor.moveToNext();
        if (!bool)
          return null; 
        param1String1 = cursor.getString(0);
        return param1String1;
      } finally {
        cursor.close();
      } 
    }
    
    @Deprecated
    public static void setSetting(ContentResolver param1ContentResolver, String param1String1, String param1String2, String param1String3) {
      ContentValues contentValues = new ContentValues();
      contentValues.put("key", param1String2);
      contentValues.put("value", param1String3);
      param1ContentResolver.update(CONTENT_URI, contentValues, null, null);
    }
  }
  
  @Deprecated
  class People implements BaseColumns, PeopleColumns, PhonesColumns, PresenceColumns {
    @Deprecated
    public static final Uri CONTENT_FILTER_URI = Uri.parse("content://contacts/people/filter");
    
    @Deprecated
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/person";
    
    @Deprecated
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/person";
    
    @Deprecated
    public static final Uri CONTENT_URI = Uri.parse("content://contacts/people");
    
    @Deprecated
    public static final String DEFAULT_SORT_ORDER = "name ASC";
    
    @Deprecated
    public static final Uri DELETED_CONTENT_URI = Uri.parse("content://contacts/deleted_people");
    
    private static final String[] GROUPS_PROJECTION;
    
    @Deprecated
    public static final String PRIMARY_EMAIL_ID = "primary_email";
    
    @Deprecated
    public static final String PRIMARY_ORGANIZATION_ID = "primary_organization";
    
    @Deprecated
    public static final String PRIMARY_PHONE_ID = "primary_phone";
    
    @Deprecated
    public static final Uri WITH_EMAIL_OR_IM_FILTER_URI = Uri.parse("content://contacts/people/with_email_or_im_filter");
    
    static {
      GROUPS_PROJECTION = new String[] { "_id" };
    }
    
    @Deprecated
    public static void markAsContacted(ContentResolver param1ContentResolver, long param1Long) {}
    
    @Deprecated
    public static long tryGetMyContactsGroupId(ContentResolver param1ContentResolver) {
      Cursor cursor = param1ContentResolver.query(Contacts.Groups.CONTENT_URI, GROUPS_PROJECTION, "system_id='Contacts'", null, null);
      if (cursor != null)
        try {
          if (cursor.moveToFirst())
            return cursor.getLong(0); 
        } finally {
          cursor.close();
        }  
      return 0L;
    }
    
    @Deprecated
    public static Uri addToMyContactsGroup(ContentResolver param1ContentResolver, long param1Long) {
      long l = tryGetMyContactsGroupId(param1ContentResolver);
      if (l != 0L)
        return addToGroup(param1ContentResolver, param1Long, l); 
      throw new IllegalStateException("Failed to find the My Contacts group");
    }
    
    @Deprecated
    public static Uri addToGroup(ContentResolver param1ContentResolver, long param1Long, String param1String) {
      long l1 = 0L;
      Cursor cursor = param1ContentResolver.query(Contacts.Groups.CONTENT_URI, GROUPS_PROJECTION, "name=?", new String[] { param1String }, null);
      long l2 = l1;
      if (cursor != null)
        try {
          if (cursor.moveToFirst())
            l1 = cursor.getLong(0); 
          cursor.close();
        } finally {
          cursor.close();
        }  
      if (l2 != 0L)
        return addToGroup(param1ContentResolver, param1Long, l2); 
      throw new IllegalStateException("Failed to find the My Contacts group");
    }
    
    @Deprecated
    public static Uri addToGroup(ContentResolver param1ContentResolver, long param1Long1, long param1Long2) {
      ContentValues contentValues = new ContentValues();
      contentValues.put("person", Long.valueOf(param1Long1));
      contentValues.put("group_id", Long.valueOf(param1Long2));
      return param1ContentResolver.insert(Contacts.GroupMembership.CONTENT_URI, contentValues);
    }
    
    @Deprecated
    public static Uri createPersonInMyContactsGroup(ContentResolver param1ContentResolver, ContentValues param1ContentValues) {
      Uri uri = param1ContentResolver.insert(CONTENT_URI, param1ContentValues);
      if (uri == null) {
        Log.e("Contacts", "Failed to create the contact");
        return null;
      } 
      if (addToMyContactsGroup(param1ContentResolver, ContentUris.parseId(uri)) == null) {
        param1ContentResolver.delete(uri, null, null);
        return null;
      } 
      return uri;
    }
    
    @Deprecated
    public static Cursor queryGroups(ContentResolver param1ContentResolver, long param1Long) {
      Uri uri = Contacts.GroupMembership.CONTENT_URI;
      return param1ContentResolver.query(uri, null, "person=?", new String[] { String.valueOf(param1Long) }, "name ASC");
    }
    
    @Deprecated
    public static void setPhotoData(ContentResolver param1ContentResolver, Uri param1Uri, byte[] param1ArrayOfbyte) {
      Uri uri = Uri.withAppendedPath(param1Uri, "photo");
      ContentValues contentValues = new ContentValues();
      contentValues.put("data", param1ArrayOfbyte);
      param1ContentResolver.update(uri, contentValues, null, null);
    }
    
    @Deprecated
    public static InputStream openContactPhotoInputStream(ContentResolver param1ContentResolver, Uri param1Uri) {
      param1Uri = Uri.withAppendedPath(param1Uri, "photo");
      Cursor cursor = param1ContentResolver.query(param1Uri, new String[] { "data" }, null, null, null);
      if (cursor != null)
        try {
        
        } finally {
          if (cursor != null)
            cursor.close(); 
        }  
      if (cursor != null)
        cursor.close(); 
      return null;
    }
    
    @Deprecated
    public static Bitmap loadContactPhoto(Context param1Context, Uri param1Uri, int param1Int, BitmapFactory.Options param1Options) {
      Bitmap bitmap1;
      if (param1Uri == null)
        return loadPlaceholderPhoto(param1Int, param1Context, param1Options); 
      InputStream inputStream = openContactPhotoInputStream(param1Context.getContentResolver(), param1Uri);
      param1Uri = null;
      if (inputStream != null)
        bitmap1 = BitmapFactory.decodeStream(inputStream, null, param1Options); 
      Bitmap bitmap2 = bitmap1;
      if (bitmap1 == null)
        bitmap2 = loadPlaceholderPhoto(param1Int, param1Context, param1Options); 
      return bitmap2;
    }
    
    private static Bitmap loadPlaceholderPhoto(int param1Int, Context param1Context, BitmapFactory.Options param1Options) {
      if (param1Int == 0)
        return null; 
      return BitmapFactory.decodeResource(param1Context.getResources(), param1Int, param1Options);
    }
    
    @Deprecated
    public static final class Phones implements BaseColumns, Contacts.PhonesColumns, Contacts.PeopleColumns {
      @Deprecated
      public static final String CONTENT_DIRECTORY = "phones";
      
      @Deprecated
      public static final String DEFAULT_SORT_ORDER = "number ASC";
    }
    
    @Deprecated
    public static final class ContactMethods implements BaseColumns, Contacts.ContactMethodsColumns, Contacts.PeopleColumns {
      @Deprecated
      public static final String CONTENT_DIRECTORY = "contact_methods";
      
      @Deprecated
      public static final String DEFAULT_SORT_ORDER = "data ASC";
    }
    
    @Deprecated
    public static class Extensions implements BaseColumns, Contacts.ExtensionsColumns {
      @Deprecated
      public static final String CONTENT_DIRECTORY = "extensions";
      
      @Deprecated
      public static final String DEFAULT_SORT_ORDER = "name ASC";
      
      @Deprecated
      public static final String PERSON_ID = "person";
    }
  }
  
  @Deprecated
  class Groups implements BaseColumns, GroupsColumns {
    @Deprecated
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/contactsgroup";
    
    @Deprecated
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/contactsgroup";
    
    @Deprecated
    public static final Uri CONTENT_URI = Uri.parse("content://contacts/groups");
    
    @Deprecated
    public static final String DEFAULT_SORT_ORDER = "name ASC";
    
    @Deprecated
    public static final Uri DELETED_CONTENT_URI = Uri.parse("content://contacts/deleted_groups");
    
    @Deprecated
    public static final String GROUP_ANDROID_STARRED = "Starred in Android";
    
    @Deprecated
    public static final String GROUP_MY_CONTACTS = "Contacts";
  }
  
  @Deprecated
  class Phones implements BaseColumns, PhonesColumns, PeopleColumns {
    @Deprecated
    public static final Uri CONTENT_FILTER_URL = Uri.parse("content://contacts/phones/filter");
    
    @Deprecated
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/phone";
    
    @Deprecated
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/phone";
    
    @Deprecated
    public static final CharSequence getDisplayLabel(Context param1Context, int param1Int, CharSequence param1CharSequence, CharSequence[] param1ArrayOfCharSequence) {
      CharSequence charSequence;
      String str = "";
      if (param1Int != 0) {
        CharSequence[] arrayOfCharSequence;
        if (param1ArrayOfCharSequence != null) {
          arrayOfCharSequence = param1ArrayOfCharSequence;
        } else {
          arrayOfCharSequence = arrayOfCharSequence.getResources().getTextArray(17235971);
        } 
        charSequence = arrayOfCharSequence[param1Int - 1];
      } else {
        charSequence = str;
        if (!TextUtils.isEmpty(param1CharSequence))
          charSequence = param1CharSequence; 
      } 
      return charSequence;
    }
    
    @Deprecated
    public static final CharSequence getDisplayLabel(Context param1Context, int param1Int, CharSequence param1CharSequence) {
      return getDisplayLabel(param1Context, param1Int, param1CharSequence, null);
    }
    
    @Deprecated
    public static final Uri CONTENT_URI = Uri.parse("content://contacts/phones");
    
    @Deprecated
    public static final String DEFAULT_SORT_ORDER = "name ASC";
    
    @Deprecated
    public static final String PERSON_ID = "person";
    
    static {
    
    }
  }
  
  @Deprecated
  class GroupMembership implements BaseColumns, GroupsColumns {
    @Deprecated
    public static final String CONTENT_DIRECTORY = "groupmembership";
    
    @Deprecated
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/contactsgroupmembership";
    
    @Deprecated
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/contactsgroupmembership";
    
    @Deprecated
    public static final Uri CONTENT_URI = Uri.parse("content://contacts/groupmembership");
    
    @Deprecated
    public static final String DEFAULT_SORT_ORDER = "group_id ASC";
    
    @Deprecated
    public static final String GROUP_ID = "group_id";
    
    @Deprecated
    public static final String GROUP_SYNC_ACCOUNT = "group_sync_account";
    
    @Deprecated
    public static final String GROUP_SYNC_ACCOUNT_TYPE = "group_sync_account_type";
    
    @Deprecated
    public static final String GROUP_SYNC_ID = "group_sync_id";
    
    @Deprecated
    public static final String PERSON_ID = "person";
    
    @Deprecated
    public static final Uri RAW_CONTENT_URI = Uri.parse("content://contacts/groupmembershipraw");
  }
  
  @Deprecated
  class ContactMethods implements BaseColumns, ContactMethodsColumns, PeopleColumns {
    @Deprecated
    public static final String CONTENT_EMAIL_ITEM_TYPE = "vnd.android.cursor.item/email";
    
    @Deprecated
    public static final String CONTENT_EMAIL_TYPE = "vnd.android.cursor.dir/email";
    
    @Deprecated
    public static final Uri CONTENT_EMAIL_URI = Uri.parse("content://contacts/contact_methods/email");
    
    @Deprecated
    public static final String CONTENT_IM_ITEM_TYPE = "vnd.android.cursor.item/jabber-im";
    
    @Deprecated
    public static final String CONTENT_POSTAL_ITEM_TYPE = "vnd.android.cursor.item/postal-address";
    
    @Deprecated
    public static final String CONTENT_POSTAL_TYPE = "vnd.android.cursor.dir/postal-address";
    
    @Deprecated
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/contact-methods";
    
    @Deprecated
    public static String encodePredefinedImProtocol(int param1Int) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("pre:");
      stringBuilder.append(param1Int);
      return stringBuilder.toString();
    }
    
    @Deprecated
    public static String encodeCustomImProtocol(String param1String) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("custom:");
      stringBuilder.append(param1String);
      return stringBuilder.toString();
    }
    
    @Deprecated
    public static Object decodeImProtocol(String param1String) {
      if (param1String == null)
        return null; 
      if (param1String.startsWith("pre:"))
        return Integer.valueOf(Integer.parseInt(param1String.substring(4))); 
      if (param1String.startsWith("custom:"))
        return param1String.substring(7); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("the value is not a valid encoded protocol, ");
      stringBuilder.append(param1String);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    @Deprecated
    public static String lookupProviderNameFromId(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "JABBER";
        case 6:
          return "ICQ";
        case 5:
          return "GTalk";
        case 4:
          return "QQ";
        case 3:
          return "SKYPE";
        case 2:
          return "Yahoo";
        case 1:
          return "MSN";
        case 0:
          break;
      } 
      return "AIM";
    }
    
    @Deprecated
    public static final CharSequence getDisplayLabel(Context param1Context, int param1Int1, int param1Int2, CharSequence param1CharSequence) {
      CharSequence charSequence;
      String str = "";
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          charSequence = param1Context.getString(17039375);
        } else if (param1Int2 != 0) {
          CharSequence[] arrayOfCharSequence = charSequence.getResources().getTextArray(17235972);
          CharSequence charSequence1 = arrayOfCharSequence[param1Int2 - 1];
        } else {
          charSequence = str;
          if (!TextUtils.isEmpty(param1CharSequence))
            charSequence = param1CharSequence; 
        } 
      } else if (param1Int2 != 0) {
        CharSequence[] arrayOfCharSequence = charSequence.getResources().getTextArray(17235968);
        CharSequence charSequence1 = arrayOfCharSequence[param1Int2 - 1];
      } else {
        charSequence = str;
        if (!TextUtils.isEmpty(param1CharSequence))
          charSequence = param1CharSequence; 
      } 
      return charSequence;
    }
    
    @Deprecated
    public void addPostalLocation(Context param1Context, long param1Long, double param1Double1, double param1Double2) {
      ContentResolver contentResolver = param1Context.getContentResolver();
      ContentValues contentValues = new ContentValues(2);
      contentValues.put("data", Double.valueOf(param1Double1));
      contentValues.put("aux_data", Double.valueOf(param1Double2));
      Uri uri = contentResolver.insert(CONTENT_URI, contentValues);
      long l = ContentUris.parseId(uri);
      contentValues.clear();
      contentValues.put("aux_data", Long.valueOf(l));
      contentResolver.update(ContentUris.withAppendedId(CONTENT_URI, param1Long), contentValues, null, null);
    }
    
    @Deprecated
    public static final Uri CONTENT_URI = Uri.parse("content://contacts/contact_methods");
    
    @Deprecated
    public static final String DEFAULT_SORT_ORDER = "name ASC";
    
    @Deprecated
    public static final String PERSON_ID = "person";
    
    @Deprecated
    public static final String POSTAL_LOCATION_LATITUDE = "data";
    
    @Deprecated
    public static final String POSTAL_LOCATION_LONGITUDE = "aux_data";
    
    @Deprecated
    public static final int PROTOCOL_AIM = 0;
    
    @Deprecated
    public static final int PROTOCOL_GOOGLE_TALK = 5;
    
    @Deprecated
    public static final int PROTOCOL_ICQ = 6;
    
    @Deprecated
    public static final int PROTOCOL_JABBER = 7;
    
    @Deprecated
    public static final int PROTOCOL_MSN = 1;
    
    @Deprecated
    public static final int PROTOCOL_QQ = 4;
    
    @Deprecated
    public static final int PROTOCOL_SKYPE = 3;
    
    @Deprecated
    public static final int PROTOCOL_YAHOO = 2;
    
    static {
    
    }
    
    class ProviderNames {
      public static final String AIM = "AIM";
      
      public static final String GTALK = "GTalk";
      
      public static final String ICQ = "ICQ";
      
      public static final String JABBER = "JABBER";
      
      public static final String MSN = "MSN";
      
      public static final String QQ = "QQ";
      
      public static final String SKYPE = "SKYPE";
      
      public static final String XMPP = "XMPP";
      
      public static final String YAHOO = "Yahoo";
    }
  }
  
  @Deprecated
  class Presence implements BaseColumns, PresenceColumns, PeopleColumns {
    @Deprecated
    public static final Uri CONTENT_URI = Uri.parse("content://contacts/presence");
    
    @Deprecated
    public static final String PERSON_ID = "person";
    
    @Deprecated
    public static final int getPresenceIconResourceId(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2 && param1Int != 3) {
          if (param1Int != 4) {
            if (param1Int != 5)
              return 17301610; 
            return 17301611;
          } 
          return 17301608;
        } 
        return 17301607;
      } 
      return 17301609;
    }
    
    @Deprecated
    public static final void setPresenceIcon(ImageView param1ImageView, int param1Int) {
      param1ImageView.setImageResource(getPresenceIconResourceId(param1Int));
    }
  }
  
  @Deprecated
  class Organizations implements BaseColumns, OrganizationColumns {
    @Deprecated
    public static final String CONTENT_DIRECTORY = "organizations";
    
    @Deprecated
    public static final CharSequence getDisplayLabel(Context param1Context, int param1Int, CharSequence param1CharSequence) {
      CharSequence charSequence;
      String str = "";
      if (param1Int != 0) {
        CharSequence[] arrayOfCharSequence = param1Context.getResources().getTextArray(17235970);
        charSequence = arrayOfCharSequence[param1Int - 1];
      } else {
        charSequence = str;
        if (!TextUtils.isEmpty(param1CharSequence))
          charSequence = param1CharSequence; 
      } 
      return charSequence;
    }
    
    @Deprecated
    public static final Uri CONTENT_URI = Uri.parse("content://contacts/organizations");
    
    @Deprecated
    public static final String DEFAULT_SORT_ORDER = "company, title, isprimary ASC";
  }
  
  @Deprecated
  class Photos implements BaseColumns, PhotosColumns {
    @Deprecated
    public static final String CONTENT_DIRECTORY = "photo";
    
    @Deprecated
    public static final Uri CONTENT_URI = Uri.parse("content://contacts/photos");
    
    @Deprecated
    public static final String DEFAULT_SORT_ORDER = "person ASC";
  }
  
  @Deprecated
  class Extensions implements BaseColumns, ExtensionsColumns {
    @Deprecated
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/contact_extensions";
    
    @Deprecated
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/contact_extensions";
    
    @Deprecated
    public static final Uri CONTENT_URI = Uri.parse("content://contacts/extensions");
    
    @Deprecated
    public static final String DEFAULT_SORT_ORDER = "person, name ASC";
    
    @Deprecated
    public static final String PERSON_ID = "person";
  }
  
  @Deprecated
  public static final class Intents {
    @Deprecated
    public static final String ATTACH_IMAGE = "com.android.contacts.action.ATTACH_IMAGE";
    
    @Deprecated
    public static final String EXTRA_CREATE_DESCRIPTION = "com.android.contacts.action.CREATE_DESCRIPTION";
    
    @Deprecated
    public static final String EXTRA_FORCE_CREATE = "com.android.contacts.action.FORCE_CREATE";
    
    @Deprecated
    public static final String EXTRA_TARGET_RECT = "target_rect";
    
    @Deprecated
    public static final String SEARCH_SUGGESTION_CLICKED = "android.provider.Contacts.SEARCH_SUGGESTION_CLICKED";
    
    @Deprecated
    public static final String SEARCH_SUGGESTION_CREATE_CONTACT_CLICKED = "android.provider.Contacts.SEARCH_SUGGESTION_CREATE_CONTACT_CLICKED";
    
    @Deprecated
    public static final String SEARCH_SUGGESTION_DIAL_NUMBER_CLICKED = "android.provider.Contacts.SEARCH_SUGGESTION_DIAL_NUMBER_CLICKED";
    
    @Deprecated
    public static final String SHOW_OR_CREATE_CONTACT = "com.android.contacts.action.SHOW_OR_CREATE_CONTACT";
    
    @Deprecated
    public static final class UI {
      @Deprecated
      public static final String FILTER_CONTACTS_ACTION = "com.android.contacts.action.FILTER_CONTACTS";
      
      @Deprecated
      public static final String FILTER_TEXT_EXTRA_KEY = "com.android.contacts.extra.FILTER_TEXT";
      
      @Deprecated
      public static final String GROUP_NAME_EXTRA_KEY = "com.android.contacts.extra.GROUP";
      
      @Deprecated
      public static final String LIST_ALL_CONTACTS_ACTION = "com.android.contacts.action.LIST_ALL_CONTACTS";
      
      @Deprecated
      public static final String LIST_CONTACTS_WITH_PHONES_ACTION = "com.android.contacts.action.LIST_CONTACTS_WITH_PHONES";
      
      @Deprecated
      public static final String LIST_DEFAULT = "com.android.contacts.action.LIST_DEFAULT";
      
      @Deprecated
      public static final String LIST_FREQUENT_ACTION = "com.android.contacts.action.LIST_FREQUENT";
      
      @Deprecated
      public static final String LIST_GROUP_ACTION = "com.android.contacts.action.LIST_GROUP";
      
      @Deprecated
      public static final String LIST_STARRED_ACTION = "com.android.contacts.action.LIST_STARRED";
      
      @Deprecated
      public static final String LIST_STREQUENT_ACTION = "com.android.contacts.action.LIST_STREQUENT";
      
      @Deprecated
      public static final String TITLE_EXTRA_KEY = "com.android.contacts.extra.TITLE_EXTRA";
    }
    
    @Deprecated
    public static final class Insert {
      @Deprecated
      public static final String ACTION = "android.intent.action.INSERT";
      
      @Deprecated
      public static final String COMPANY = "company";
      
      @Deprecated
      public static final String EMAIL = "email";
      
      @Deprecated
      public static final String EMAIL_ISPRIMARY = "email_isprimary";
      
      @Deprecated
      public static final String EMAIL_TYPE = "email_type";
      
      @Deprecated
      public static final String FULL_MODE = "full_mode";
      
      @Deprecated
      public static final String IM_HANDLE = "im_handle";
      
      @Deprecated
      public static final String IM_ISPRIMARY = "im_isprimary";
      
      @Deprecated
      public static final String IM_PROTOCOL = "im_protocol";
      
      @Deprecated
      public static final String JOB_TITLE = "job_title";
      
      @Deprecated
      public static final String NAME = "name";
      
      @Deprecated
      public static final String NOTES = "notes";
      
      @Deprecated
      public static final String PHONE = "phone";
      
      @Deprecated
      public static final String PHONETIC_NAME = "phonetic_name";
      
      @Deprecated
      public static final String PHONE_ISPRIMARY = "phone_isprimary";
      
      @Deprecated
      public static final String PHONE_TYPE = "phone_type";
      
      @Deprecated
      public static final String POSTAL = "postal";
      
      @Deprecated
      public static final String POSTAL_ISPRIMARY = "postal_isprimary";
      
      @Deprecated
      public static final String POSTAL_TYPE = "postal_type";
      
      @Deprecated
      public static final String SECONDARY_EMAIL = "secondary_email";
      
      @Deprecated
      public static final String SECONDARY_EMAIL_TYPE = "secondary_email_type";
      
      @Deprecated
      public static final String SECONDARY_PHONE = "secondary_phone";
      
      @Deprecated
      public static final String SECONDARY_PHONE_TYPE = "secondary_phone_type";
      
      @Deprecated
      public static final String TERTIARY_EMAIL = "tertiary_email";
      
      @Deprecated
      public static final String TERTIARY_EMAIL_TYPE = "tertiary_email_type";
      
      @Deprecated
      public static final String TERTIARY_PHONE = "tertiary_phone";
      
      @Deprecated
      public static final String TERTIARY_PHONE_TYPE = "tertiary_phone_type";
    }
  }
  
  @Deprecated
  public static final class UI {
    @Deprecated
    public static final String FILTER_CONTACTS_ACTION = "com.android.contacts.action.FILTER_CONTACTS";
    
    @Deprecated
    public static final String FILTER_TEXT_EXTRA_KEY = "com.android.contacts.extra.FILTER_TEXT";
    
    @Deprecated
    public static final String GROUP_NAME_EXTRA_KEY = "com.android.contacts.extra.GROUP";
    
    @Deprecated
    public static final String LIST_ALL_CONTACTS_ACTION = "com.android.contacts.action.LIST_ALL_CONTACTS";
    
    @Deprecated
    public static final String LIST_CONTACTS_WITH_PHONES_ACTION = "com.android.contacts.action.LIST_CONTACTS_WITH_PHONES";
    
    @Deprecated
    public static final String LIST_DEFAULT = "com.android.contacts.action.LIST_DEFAULT";
    
    @Deprecated
    public static final String LIST_FREQUENT_ACTION = "com.android.contacts.action.LIST_FREQUENT";
    
    @Deprecated
    public static final String LIST_GROUP_ACTION = "com.android.contacts.action.LIST_GROUP";
    
    @Deprecated
    public static final String LIST_STARRED_ACTION = "com.android.contacts.action.LIST_STARRED";
    
    @Deprecated
    public static final String LIST_STREQUENT_ACTION = "com.android.contacts.action.LIST_STREQUENT";
    
    @Deprecated
    public static final String TITLE_EXTRA_KEY = "com.android.contacts.extra.TITLE_EXTRA";
  }
  
  @Deprecated
  public static final class Insert {
    @Deprecated
    public static final String ACTION = "android.intent.action.INSERT";
    
    @Deprecated
    public static final String COMPANY = "company";
    
    @Deprecated
    public static final String EMAIL = "email";
    
    @Deprecated
    public static final String EMAIL_ISPRIMARY = "email_isprimary";
    
    @Deprecated
    public static final String EMAIL_TYPE = "email_type";
    
    @Deprecated
    public static final String FULL_MODE = "full_mode";
    
    @Deprecated
    public static final String IM_HANDLE = "im_handle";
    
    @Deprecated
    public static final String IM_ISPRIMARY = "im_isprimary";
    
    @Deprecated
    public static final String IM_PROTOCOL = "im_protocol";
    
    @Deprecated
    public static final String JOB_TITLE = "job_title";
    
    @Deprecated
    public static final String NAME = "name";
    
    @Deprecated
    public static final String NOTES = "notes";
    
    @Deprecated
    public static final String PHONE = "phone";
    
    @Deprecated
    public static final String PHONETIC_NAME = "phonetic_name";
    
    @Deprecated
    public static final String PHONE_ISPRIMARY = "phone_isprimary";
    
    @Deprecated
    public static final String PHONE_TYPE = "phone_type";
    
    @Deprecated
    public static final String POSTAL = "postal";
    
    @Deprecated
    public static final String POSTAL_ISPRIMARY = "postal_isprimary";
    
    @Deprecated
    public static final String POSTAL_TYPE = "postal_type";
    
    @Deprecated
    public static final String SECONDARY_EMAIL = "secondary_email";
    
    @Deprecated
    public static final String SECONDARY_EMAIL_TYPE = "secondary_email_type";
    
    @Deprecated
    public static final String SECONDARY_PHONE = "secondary_phone";
    
    @Deprecated
    public static final String SECONDARY_PHONE_TYPE = "secondary_phone_type";
    
    @Deprecated
    public static final String TERTIARY_EMAIL = "tertiary_email";
    
    @Deprecated
    public static final String TERTIARY_EMAIL_TYPE = "tertiary_email_type";
    
    @Deprecated
    public static final String TERTIARY_PHONE = "tertiary_phone";
    
    @Deprecated
    public static final String TERTIARY_PHONE_TYPE = "tertiary_phone_type";
  }
  
  static interface ProviderNames {
    public static final String AIM = "AIM";
    
    public static final String GTALK = "GTalk";
    
    public static final String ICQ = "ICQ";
    
    public static final String JABBER = "JABBER";
    
    public static final String MSN = "MSN";
    
    public static final String QQ = "QQ";
    
    public static final String SKYPE = "SKYPE";
    
    public static final String XMPP = "XMPP";
    
    public static final String YAHOO = "Yahoo";
  }
  
  @Deprecated
  public static interface ContactMethodsColumns {
    @Deprecated
    public static final String AUX_DATA = "aux_data";
    
    @Deprecated
    public static final String DATA = "data";
    
    @Deprecated
    public static final String ISPRIMARY = "isprimary";
    
    @Deprecated
    public static final String KIND = "kind";
    
    @Deprecated
    public static final String LABEL = "label";
    
    @Deprecated
    public static final int MOBILE_EMAIL_TYPE_INDEX = 2;
    
    @Deprecated
    public static final String MOBILE_EMAIL_TYPE_NAME = "_AUTO_CELL";
    
    @Deprecated
    public static final String TYPE = "type";
    
    @Deprecated
    public static final int TYPE_CUSTOM = 0;
    
    @Deprecated
    public static final int TYPE_HOME = 1;
    
    @Deprecated
    public static final int TYPE_OTHER = 3;
    
    @Deprecated
    public static final int TYPE_WORK = 2;
  }
  
  @Deprecated
  public static interface ExtensionsColumns {
    @Deprecated
    public static final String NAME = "name";
    
    @Deprecated
    public static final String VALUE = "value";
  }
  
  @Deprecated
  public static interface GroupsColumns {
    @Deprecated
    public static final String NAME = "name";
    
    @Deprecated
    public static final String NOTES = "notes";
    
    @Deprecated
    public static final String SHOULD_SYNC = "should_sync";
    
    @Deprecated
    public static final String SYSTEM_ID = "system_id";
  }
  
  @Deprecated
  public static interface OrganizationColumns {
    @Deprecated
    public static final String COMPANY = "company";
    
    @Deprecated
    public static final String ISPRIMARY = "isprimary";
    
    @Deprecated
    public static final String LABEL = "label";
    
    @Deprecated
    public static final String PERSON_ID = "person";
    
    @Deprecated
    public static final String TITLE = "title";
    
    @Deprecated
    public static final String TYPE = "type";
    
    @Deprecated
    public static final int TYPE_CUSTOM = 0;
    
    @Deprecated
    public static final int TYPE_OTHER = 2;
    
    @Deprecated
    public static final int TYPE_WORK = 1;
  }
  
  @Deprecated
  public static interface PeopleColumns {
    @Deprecated
    public static final String CUSTOM_RINGTONE = "custom_ringtone";
    
    @Deprecated
    public static final String DISPLAY_NAME = "display_name";
    
    @Deprecated
    public static final String LAST_TIME_CONTACTED = "last_time_contacted";
    
    @Deprecated
    public static final String NAME = "name";
    
    @Deprecated
    public static final String NOTES = "notes";
    
    @Deprecated
    public static final String PHONETIC_NAME = "phonetic_name";
    
    @Deprecated
    public static final String PHOTO_VERSION = "photo_version";
    
    @Deprecated
    public static final String SEND_TO_VOICEMAIL = "send_to_voicemail";
    
    @Deprecated
    public static final String SORT_STRING = "sort_string";
    
    @Deprecated
    public static final String STARRED = "starred";
    
    @Deprecated
    public static final String TIMES_CONTACTED = "times_contacted";
  }
  
  @Deprecated
  public static interface PhonesColumns {
    @Deprecated
    public static final String ISPRIMARY = "isprimary";
    
    @Deprecated
    public static final String LABEL = "label";
    
    @Deprecated
    public static final String NUMBER = "number";
    
    @Deprecated
    public static final String NUMBER_KEY = "number_key";
    
    @Deprecated
    public static final String TYPE = "type";
    
    @Deprecated
    public static final int TYPE_CUSTOM = 0;
    
    @Deprecated
    public static final int TYPE_FAX_HOME = 5;
    
    @Deprecated
    public static final int TYPE_FAX_WORK = 4;
    
    @Deprecated
    public static final int TYPE_HOME = 1;
    
    @Deprecated
    public static final int TYPE_MOBILE = 2;
    
    @Deprecated
    public static final int TYPE_OTHER = 7;
    
    @Deprecated
    public static final int TYPE_PAGER = 6;
    
    @Deprecated
    public static final int TYPE_WORK = 3;
  }
  
  @Deprecated
  public static interface PhotosColumns {
    @Deprecated
    public static final String DATA = "data";
    
    @Deprecated
    public static final String DOWNLOAD_REQUIRED = "download_required";
    
    @Deprecated
    public static final String EXISTS_ON_SERVER = "exists_on_server";
    
    @Deprecated
    public static final String LOCAL_VERSION = "local_version";
    
    @Deprecated
    public static final String PERSON_ID = "person";
    
    @Deprecated
    public static final String SYNC_ERROR = "sync_error";
  }
  
  @Deprecated
  public static interface PresenceColumns {
    public static final int AVAILABLE = 5;
    
    public static final int AWAY = 2;
    
    public static final int DO_NOT_DISTURB = 4;
    
    public static final int IDLE = 3;
    
    @Deprecated
    public static final String IM_ACCOUNT = "im_account";
    
    @Deprecated
    public static final String IM_HANDLE = "im_handle";
    
    @Deprecated
    public static final String IM_PROTOCOL = "im_protocol";
    
    public static final int INVISIBLE = 1;
    
    public static final int OFFLINE = 0;
    
    public static final String PRESENCE_CUSTOM_STATUS = "status";
    
    public static final String PRESENCE_STATUS = "mode";
    
    public static final String PRIORITY = "priority";
  }
  
  @Deprecated
  public static interface SettingsColumns {
    @Deprecated
    public static final String KEY = "key";
    
    @Deprecated
    public static final String VALUE = "value";
    
    @Deprecated
    public static final String _SYNC_ACCOUNT = "_sync_account";
    
    @Deprecated
    public static final String _SYNC_ACCOUNT_TYPE = "_sync_account_type";
  }
}
