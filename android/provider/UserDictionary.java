package android.provider;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import java.util.Locale;

public class UserDictionary {
  public static final String AUTHORITY = "user_dictionary";
  
  public static final Uri CONTENT_URI = Uri.parse("content://user_dictionary");
  
  private static final int FREQUENCY_MAX = 255;
  
  private static final int FREQUENCY_MIN = 0;
  
  class Words implements BaseColumns {
    public static final String APP_ID = "appid";
    
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.google.userword";
    
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.google.userword";
    
    public static final Uri CONTENT_URI = Uri.parse("content://user_dictionary/words");
    
    public static final String DEFAULT_SORT_ORDER = "frequency DESC";
    
    public static final String FREQUENCY = "frequency";
    
    public static final String LOCALE = "locale";
    
    @Deprecated
    public static final int LOCALE_TYPE_ALL = 0;
    
    @Deprecated
    public static final int LOCALE_TYPE_CURRENT = 1;
    
    public static final String SHORTCUT = "shortcut";
    
    public static final String WORD = "word";
    
    public static final String _ID = "_id";
    
    @Deprecated
    public static void addWord(Context param1Context, String param1String, int param1Int1, int param1Int2) {
      Locale locale;
      if (param1Int2 != 0 && param1Int2 != 1)
        return; 
      if (param1Int2 == 1) {
        locale = Locale.getDefault();
      } else {
        locale = null;
      } 
      addWord(param1Context, param1String, param1Int1, null, locale);
    }
    
    public static void addWord(Context param1Context, String param1String1, int param1Int, String param1String2, Locale param1Locale) {
      String str;
      ContentResolver contentResolver = param1Context.getContentResolver();
      if (TextUtils.isEmpty(param1String1))
        return; 
      int i = param1Int;
      if (param1Int < 0)
        i = 0; 
      param1Int = i;
      if (i > 255)
        param1Int = 255; 
      ContentValues contentValues = new ContentValues(5);
      contentValues.put("word", param1String1);
      contentValues.put("frequency", Integer.valueOf(param1Int));
      if (param1Locale == null) {
        param1Context = null;
      } else {
        str = param1Locale.toString();
      } 
      contentValues.put("locale", str);
      contentValues.put("appid", Integer.valueOf(0));
      contentValues.put("shortcut", param1String2);
      contentResolver.insert(CONTENT_URI, contentValues);
    }
  }
}
