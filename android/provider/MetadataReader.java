package android.provider;

import android.media.ExifInterface;
import android.os.Bundle;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public final class MetadataReader {
  private static final String[] DEFAULT_EXIF_TAGS = new String[] { 
      "FNumber", "Copyright", "DateTime", "ExposureTime", "FocalLength", "FNumber", "GPSLatitude", "GPSLatitudeRef", "GPSLongitude", "GPSLongitudeRef", 
      "ImageLength", "ImageWidth", "ISOSpeedRatings", "Make", "Model", "Orientation", "ShutterSpeedValue" };
  
  private static final String JPEG_MIME_TYPE = "image/jpeg";
  
  private static final String JPG_MIME_TYPE = "image/jpg";
  
  private static final int TYPE_DOUBLE = 1;
  
  private static final int TYPE_INT = 0;
  
  private static final Map<String, Integer> TYPE_MAPPING;
  
  private static final int TYPE_STRING = 2;
  
  static {
    HashMap<Object, Object> hashMap = new HashMap<>();
    Integer integer2 = Integer.valueOf(2);
    hashMap.put("Artist", integer2);
    Map<String, Integer> map = TYPE_MAPPING;
    Integer integer1 = Integer.valueOf(0);
    map.put("BitsPerSample", integer1);
    TYPE_MAPPING.put("Compression", integer1);
    TYPE_MAPPING.put("Copyright", integer2);
    TYPE_MAPPING.put("DateTime", integer2);
    TYPE_MAPPING.put("ImageDescription", integer2);
    TYPE_MAPPING.put("ImageLength", integer1);
    TYPE_MAPPING.put("ImageWidth", integer1);
    TYPE_MAPPING.put("JPEGInterchangeFormat", integer1);
    TYPE_MAPPING.put("JPEGInterchangeFormatLength", integer1);
    TYPE_MAPPING.put("Make", integer2);
    TYPE_MAPPING.put("Model", integer2);
    TYPE_MAPPING.put("Orientation", integer1);
    TYPE_MAPPING.put("PhotometricInterpretation", integer1);
    TYPE_MAPPING.put("PlanarConfiguration", integer1);
    map = TYPE_MAPPING;
    Integer integer3 = Integer.valueOf(1);
    map.put("PrimaryChromaticities", integer3);
    TYPE_MAPPING.put("ReferenceBlackWhite", integer3);
    TYPE_MAPPING.put("ResolutionUnit", integer1);
    TYPE_MAPPING.put("RowsPerStrip", integer1);
    TYPE_MAPPING.put("SamplesPerPixel", integer1);
    TYPE_MAPPING.put("Software", integer2);
    TYPE_MAPPING.put("StripByteCounts", integer1);
    TYPE_MAPPING.put("StripOffsets", integer1);
    TYPE_MAPPING.put("TransferFunction", integer1);
    TYPE_MAPPING.put("WhitePoint", integer3);
    TYPE_MAPPING.put("XResolution", integer3);
    TYPE_MAPPING.put("YCbCrCoefficients", integer3);
    TYPE_MAPPING.put("YCbCrPositioning", integer1);
    TYPE_MAPPING.put("YCbCrSubSampling", integer1);
    TYPE_MAPPING.put("YResolution", integer3);
    TYPE_MAPPING.put("ApertureValue", integer3);
    TYPE_MAPPING.put("BrightnessValue", integer3);
    TYPE_MAPPING.put("CFAPattern", integer2);
    TYPE_MAPPING.put("ColorSpace", integer1);
    TYPE_MAPPING.put("ComponentsConfiguration", integer2);
    TYPE_MAPPING.put("CompressedBitsPerPixel", integer3);
    TYPE_MAPPING.put("Contrast", integer1);
    TYPE_MAPPING.put("CustomRendered", integer1);
    TYPE_MAPPING.put("DateTimeDigitized", integer2);
    TYPE_MAPPING.put("DateTimeOriginal", integer2);
    TYPE_MAPPING.put("DeviceSettingDescription", integer2);
    TYPE_MAPPING.put("DigitalZoomRatio", integer3);
    TYPE_MAPPING.put("ExifVersion", integer2);
    TYPE_MAPPING.put("ExposureBiasValue", integer3);
    TYPE_MAPPING.put("ExposureIndex", integer3);
    TYPE_MAPPING.put("ExposureMode", integer1);
    TYPE_MAPPING.put("ExposureProgram", integer1);
    TYPE_MAPPING.put("ExposureTime", integer3);
    TYPE_MAPPING.put("FNumber", integer3);
    TYPE_MAPPING.put("FileSource", integer2);
    TYPE_MAPPING.put("Flash", integer1);
    TYPE_MAPPING.put("FlashEnergy", integer3);
    TYPE_MAPPING.put("FlashpixVersion", integer2);
    TYPE_MAPPING.put("FocalLength", integer3);
    TYPE_MAPPING.put("FocalLengthIn35mmFilm", integer1);
    TYPE_MAPPING.put("FocalPlaneResolutionUnit", integer1);
    TYPE_MAPPING.put("FocalPlaneXResolution", integer3);
    TYPE_MAPPING.put("FocalPlaneYResolution", integer3);
    TYPE_MAPPING.put("GainControl", integer1);
    TYPE_MAPPING.put("ISOSpeedRatings", integer1);
    TYPE_MAPPING.put("ImageUniqueID", integer2);
    TYPE_MAPPING.put("LightSource", integer1);
    TYPE_MAPPING.put("MakerNote", integer2);
    TYPE_MAPPING.put("MaxApertureValue", integer3);
    TYPE_MAPPING.put("MeteringMode", integer1);
    TYPE_MAPPING.put("NewSubfileType", integer1);
    TYPE_MAPPING.put("OECF", integer2);
    TYPE_MAPPING.put("PixelXDimension", integer1);
    TYPE_MAPPING.put("PixelYDimension", integer1);
    TYPE_MAPPING.put("RelatedSoundFile", integer2);
    TYPE_MAPPING.put("Saturation", integer1);
    TYPE_MAPPING.put("SceneCaptureType", integer1);
    TYPE_MAPPING.put("SceneType", integer2);
    TYPE_MAPPING.put("SensingMethod", integer1);
    TYPE_MAPPING.put("Sharpness", integer1);
    TYPE_MAPPING.put("ShutterSpeedValue", integer3);
    TYPE_MAPPING.put("SpatialFrequencyResponse", integer2);
    TYPE_MAPPING.put("SpectralSensitivity", integer2);
    TYPE_MAPPING.put("SubfileType", integer1);
    TYPE_MAPPING.put("SubSecTime", integer2);
    TYPE_MAPPING.put("SubSecTimeDigitized", integer2);
    TYPE_MAPPING.put("SubSecTimeOriginal", integer2);
    TYPE_MAPPING.put("SubjectArea", integer1);
    TYPE_MAPPING.put("SubjectDistance", integer3);
    TYPE_MAPPING.put("SubjectDistanceRange", integer1);
    TYPE_MAPPING.put("SubjectLocation", integer1);
    TYPE_MAPPING.put("UserComment", integer2);
    TYPE_MAPPING.put("WhiteBalance", integer1);
    TYPE_MAPPING.put("GPSAltitude", integer3);
    TYPE_MAPPING.put("GPSAltitudeRef", integer1);
    TYPE_MAPPING.put("GPSAreaInformation", integer2);
    TYPE_MAPPING.put("GPSDOP", integer3);
    TYPE_MAPPING.put("GPSDateStamp", integer2);
    TYPE_MAPPING.put("GPSDestBearing", integer3);
    TYPE_MAPPING.put("GPSDestBearingRef", integer2);
    TYPE_MAPPING.put("GPSDestDistance", integer3);
    TYPE_MAPPING.put("GPSDestDistanceRef", integer2);
    TYPE_MAPPING.put("GPSDestLatitude", integer3);
    TYPE_MAPPING.put("GPSDestLatitudeRef", integer2);
    TYPE_MAPPING.put("GPSDestLongitude", integer3);
    TYPE_MAPPING.put("GPSDestLongitudeRef", integer2);
    TYPE_MAPPING.put("GPSDifferential", integer1);
    TYPE_MAPPING.put("GPSImgDirection", integer3);
    TYPE_MAPPING.put("GPSImgDirectionRef", integer2);
    TYPE_MAPPING.put("GPSLatitude", integer2);
    TYPE_MAPPING.put("GPSLatitudeRef", integer2);
    TYPE_MAPPING.put("GPSLongitude", integer2);
    TYPE_MAPPING.put("GPSLongitudeRef", integer2);
    TYPE_MAPPING.put("GPSMapDatum", integer2);
    TYPE_MAPPING.put("GPSMeasureMode", integer2);
    TYPE_MAPPING.put("GPSProcessingMethod", integer2);
    TYPE_MAPPING.put("GPSSatellites", integer2);
    TYPE_MAPPING.put("GPSSpeed", integer3);
    TYPE_MAPPING.put("GPSSpeedRef", integer2);
    TYPE_MAPPING.put("GPSStatus", integer2);
    TYPE_MAPPING.put("GPSTimeStamp", integer2);
    TYPE_MAPPING.put("GPSTrack", integer3);
    TYPE_MAPPING.put("GPSTrackRef", integer2);
    TYPE_MAPPING.put("GPSVersionID", integer2);
    TYPE_MAPPING.put("InteroperabilityIndex", integer2);
    TYPE_MAPPING.put("ThumbnailImageLength", integer1);
    TYPE_MAPPING.put("ThumbnailImageWidth", integer1);
    TYPE_MAPPING.put("DNGVersion", integer1);
    TYPE_MAPPING.put("DefaultCropSize", integer1);
    TYPE_MAPPING.put("PreviewImageStart", integer1);
    TYPE_MAPPING.put("PreviewImageLength", integer1);
    TYPE_MAPPING.put("AspectFrame", integer1);
    TYPE_MAPPING.put("SensorBottomBorder", integer1);
    TYPE_MAPPING.put("SensorLeftBorder", integer1);
    TYPE_MAPPING.put("SensorRightBorder", integer1);
    TYPE_MAPPING.put("SensorTopBorder", integer1);
    TYPE_MAPPING.put("ISO", integer1);
  }
  
  public static boolean isSupportedMimeType(String paramString) {
    return ("image/jpg".equals(paramString) || "image/jpeg".equals(paramString));
  }
  
  public static void getMetadata(Bundle paramBundle, InputStream paramInputStream, String paramString, String[] paramArrayOfString) throws IOException {
    ArrayList<String> arrayList = new ArrayList();
    if (isSupportedMimeType(paramString)) {
      Bundle bundle = getExifData(paramInputStream, paramArrayOfString);
      if (bundle.size() > 0) {
        paramBundle.putBundle("android:documentExif", bundle);
        arrayList.add("android:documentExif");
      } 
    } 
    String[] arrayOfString = arrayList.<String>toArray(new String[arrayList.size()]);
    paramBundle.putStringArray("android:documentMetadataTypes", arrayOfString);
  }
  
  private static Bundle getExifData(InputStream paramInputStream, String[] paramArrayOfString) throws IOException {
    String[] arrayOfString = paramArrayOfString;
    if (paramArrayOfString == null)
      arrayOfString = DEFAULT_EXIF_TAGS; 
    ExifInterface exifInterface = new ExifInterface(paramInputStream);
    Bundle bundle = new Bundle();
    int i;
    byte b;
    for (i = arrayOfString.length, b = 0; b < i; ) {
      String str = arrayOfString[b];
      if (((Integer)TYPE_MAPPING.get(str)).equals(Integer.valueOf(0))) {
        int j = exifInterface.getAttributeInt(str, -2147483648);
        if (j != Integer.MIN_VALUE)
          bundle.putInt(str, j); 
      } else if (((Integer)TYPE_MAPPING.get(str)).equals(Integer.valueOf(1))) {
        double d = exifInterface.getAttributeDouble(str, Double.MIN_VALUE);
        if (d != Double.MIN_VALUE)
          bundle.putDouble(str, d); 
      } else if (((Integer)TYPE_MAPPING.get(str)).equals(Integer.valueOf(2))) {
        String str1 = exifInterface.getAttribute(str);
        if (str1 != null)
          bundle.putString(str, str1); 
      } 
      b++;
    } 
    return bundle;
  }
}
