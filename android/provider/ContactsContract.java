package android.provider;

import android.accounts.Account;
import android.annotation.SystemApi;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.CursorEntityIterator;
import android.content.EntityIterator;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Rect;
import android.net.Uri;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Pair;
import android.util.SeempLog;
import android.view.View;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public final class ContactsContract {
  public static final String AUTHORITY = "com.android.contacts";
  
  public static final Uri AUTHORITY_URI = Uri.parse("content://com.android.contacts");
  
  public static final String CALLER_IS_SYNCADAPTER = "caller_is_syncadapter";
  
  public static final String DEFERRED_SNIPPETING = "deferred_snippeting";
  
  public static final String DEFERRED_SNIPPETING_QUERY = "deferred_snippeting_query";
  
  public static final String DIRECTORY_PARAM_KEY = "directory";
  
  public static final String HIDDEN_COLUMN_PREFIX = "x_";
  
  public static final String LIMIT_PARAM_KEY = "limit";
  
  public static final String PRIMARY_ACCOUNT_NAME = "name_for_primary_account";
  
  public static final String PRIMARY_ACCOUNT_TYPE = "type_for_primary_account";
  
  public static final String REMOVE_DUPLICATE_ENTRIES = "remove_duplicate_entries";
  
  public static final String STREQUENT_PHONE_ONLY = "strequent_phone_only";
  
  public static final class Authorization {
    public static final String AUTHORIZATION_METHOD = "authorize";
    
    public static final String KEY_AUTHORIZED_URI = "authorized_uri";
    
    public static final String KEY_URI_TO_AUTHORIZE = "uri_to_authorize";
  }
  
  class Directory implements BaseColumns {
    public static final String ACCOUNT_NAME = "accountName";
    
    public static final String ACCOUNT_TYPE = "accountType";
    
    public static final String CALLER_PACKAGE_PARAM_KEY = "callerPackage";
    
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/contact_directory";
    
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/contact_directories";
    
    public static final Uri CONTENT_URI;
    
    public static final long DEFAULT = 0L;
    
    public static final String DIRECTORY_AUTHORITY = "authority";
    
    public static final String DISPLAY_NAME = "displayName";
    
    static {
      Uri uri = ContactsContract.AUTHORITY_URI;
      CONTENT_URI = Uri.withAppendedPath(uri, "directories");
    }
    
    public static final Uri ENTERPRISE_CONTENT_URI = Uri.withAppendedPath(ContactsContract.AUTHORITY_URI, "directories_enterprise");
    
    public static final long ENTERPRISE_DEFAULT = 1000000000L;
    
    public static final long ENTERPRISE_DIRECTORY_ID_BASE = 1000000000L;
    
    public static final Uri ENTERPRISE_FILE_URI = Uri.withAppendedPath(ContactsContract.AUTHORITY_URI, "directory_file_enterprise");
    
    public static final long ENTERPRISE_LOCAL_INVISIBLE = 1000000001L;
    
    public static final String EXPORT_SUPPORT = "exportSupport";
    
    public static final int EXPORT_SUPPORT_ANY_ACCOUNT = 2;
    
    public static final int EXPORT_SUPPORT_NONE = 0;
    
    public static final int EXPORT_SUPPORT_SAME_ACCOUNT_ONLY = 1;
    
    public static final long LOCAL_INVISIBLE = 1L;
    
    public static final String PACKAGE_NAME = "packageName";
    
    public static final String PHOTO_SUPPORT = "photoSupport";
    
    public static final int PHOTO_SUPPORT_FULL = 3;
    
    public static final int PHOTO_SUPPORT_FULL_SIZE_ONLY = 2;
    
    public static final int PHOTO_SUPPORT_NONE = 0;
    
    public static final int PHOTO_SUPPORT_THUMBNAIL_ONLY = 1;
    
    public static final String SHORTCUT_SUPPORT = "shortcutSupport";
    
    public static final int SHORTCUT_SUPPORT_DATA_ITEMS_ONLY = 1;
    
    public static final int SHORTCUT_SUPPORT_FULL = 2;
    
    public static final int SHORTCUT_SUPPORT_NONE = 0;
    
    public static final String TYPE_RESOURCE_ID = "typeResourceId";
    
    public static boolean isRemoteDirectoryId(long param1Long) {
      boolean bool;
      if (param1Long != 0L && param1Long != 1L && param1Long != 1000000000L && param1Long != 1000000001L) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public static boolean isRemoteDirectory(long param1Long) {
      return isRemoteDirectoryId(param1Long);
    }
    
    public static boolean isEnterpriseDirectoryId(long param1Long) {
      boolean bool;
      if (param1Long >= 1000000000L) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public static void notifyDirectoryChange(ContentResolver param1ContentResolver) {
      ContentValues contentValues = new ContentValues();
      param1ContentResolver.update(CONTENT_URI, contentValues, null, null);
    }
  }
  
  class SyncState implements SyncStateContract.Columns {
    public static final String CONTENT_DIRECTORY = "syncstate";
    
    public static final Uri CONTENT_URI;
    
    static {
      Uri uri = ContactsContract.AUTHORITY_URI;
      CONTENT_URI = Uri.withAppendedPath(uri, "syncstate");
    }
    
    public static byte[] get(ContentProviderClient param1ContentProviderClient, Account param1Account) throws RemoteException {
      return SyncStateContract.Helpers.get(param1ContentProviderClient, CONTENT_URI, param1Account);
    }
    
    public static Pair<Uri, byte[]> getWithUri(ContentProviderClient param1ContentProviderClient, Account param1Account) throws RemoteException {
      return SyncStateContract.Helpers.getWithUri(param1ContentProviderClient, CONTENT_URI, param1Account);
    }
    
    public static void set(ContentProviderClient param1ContentProviderClient, Account param1Account, byte[] param1ArrayOfbyte) throws RemoteException {
      SyncStateContract.Helpers.set(param1ContentProviderClient, CONTENT_URI, param1Account, param1ArrayOfbyte);
    }
    
    public static ContentProviderOperation newSetOperation(Account param1Account, byte[] param1ArrayOfbyte) {
      return SyncStateContract.Helpers.newSetOperation(CONTENT_URI, param1Account, param1ArrayOfbyte);
    }
  }
  
  class ProfileSyncState implements SyncStateContract.Columns {
    public static final String CONTENT_DIRECTORY = "syncstate";
    
    public static final Uri CONTENT_URI;
    
    static {
      Uri uri = ContactsContract.Profile.CONTENT_URI;
      CONTENT_URI = Uri.withAppendedPath(uri, "syncstate");
    }
    
    public static byte[] get(ContentProviderClient param1ContentProviderClient, Account param1Account) throws RemoteException {
      return SyncStateContract.Helpers.get(param1ContentProviderClient, CONTENT_URI, param1Account);
    }
    
    public static Pair<Uri, byte[]> getWithUri(ContentProviderClient param1ContentProviderClient, Account param1Account) throws RemoteException {
      return SyncStateContract.Helpers.getWithUri(param1ContentProviderClient, CONTENT_URI, param1Account);
    }
    
    public static void set(ContentProviderClient param1ContentProviderClient, Account param1Account, byte[] param1ArrayOfbyte) throws RemoteException {
      SyncStateContract.Helpers.set(param1ContentProviderClient, CONTENT_URI, param1Account, param1ArrayOfbyte);
    }
    
    public static ContentProviderOperation newSetOperation(Account param1Account, byte[] param1ArrayOfbyte) {
      return SyncStateContract.Helpers.newSetOperation(CONTENT_URI, param1Account, param1ArrayOfbyte);
    }
  }
  
  class Contacts implements BaseColumns, ContactsColumns, ContactOptionsColumns, ContactNameColumns, ContactStatusColumns, ContactCounts {
    public static final Uri CONTENT_FILTER_URI;
    
    @Deprecated
    public static final Uri CONTENT_FREQUENT_URI;
    
    public static final Uri CONTENT_GROUP_URI;
    
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/contact";
    
    public static final Uri CONTENT_LOOKUP_URI = Uri.withAppendedPath(CONTENT_URI, "lookup");
    
    public static final Uri CONTENT_MULTI_VCARD_URI;
    
    public static final Uri CONTENT_STREQUENT_FILTER_URI;
    
    public static final Uri CONTENT_STREQUENT_URI;
    
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/contact";
    
    public static final Uri CONTENT_URI = Uri.withAppendedPath(ContactsContract.AUTHORITY_URI, "contacts");
    
    public static final String CONTENT_VCARD_TYPE = "text/x-vcard";
    
    public static final Uri CONTENT_VCARD_URI = Uri.withAppendedPath(CONTENT_URI, "as_vcard");
    
    public static final Uri CORP_CONTENT_URI = Uri.withAppendedPath(ContactsContract.AUTHORITY_URI, "contacts_corp");
    
    public static long ENTERPRISE_CONTACT_ID_BASE = 0L;
    
    public static String ENTERPRISE_CONTACT_LOOKUP_PREFIX;
    
    public static final Uri ENTERPRISE_CONTENT_FILTER_URI;
    
    public static final String QUERY_PARAMETER_VCARD_NO_PHOTO = "no_photo";
    
    static {
      CONTENT_MULTI_VCARD_URI = Uri.withAppendedPath(CONTENT_URI, "as_multi_vcard");
      CONTENT_FILTER_URI = Uri.withAppendedPath(CONTENT_URI, "filter");
      ENTERPRISE_CONTENT_FILTER_URI = Uri.withAppendedPath(CONTENT_URI, "filter_enterprise");
      CONTENT_STREQUENT_URI = Uri.withAppendedPath(CONTENT_URI, "strequent");
      CONTENT_FREQUENT_URI = Uri.withAppendedPath(CONTENT_URI, "frequent");
      CONTENT_STREQUENT_FILTER_URI = Uri.withAppendedPath(CONTENT_STREQUENT_URI, "filter");
      CONTENT_GROUP_URI = Uri.withAppendedPath(CONTENT_URI, "group");
      ENTERPRISE_CONTACT_ID_BASE = 1000000000L;
      ENTERPRISE_CONTACT_LOOKUP_PREFIX = "c-";
    }
    
    public static Uri getLookupUri(ContentResolver param1ContentResolver, Uri param1Uri) {
      SeempLog.record(86);
      Cursor cursor = param1ContentResolver.query(param1Uri, new String[] { "lookup", "_id" }, null, null, null);
      if (cursor == null)
        return null; 
      try {
        if (cursor.moveToFirst()) {
          String str = cursor.getString(0);
          long l = cursor.getLong(1);
          return getLookupUri(l, str);
        } 
        return null;
      } finally {
        cursor.close();
      } 
    }
    
    public static Uri getLookupUri(long param1Long, String param1String) {
      SeempLog.record(86);
      if (TextUtils.isEmpty(param1String))
        return null; 
      return ContentUris.withAppendedId(Uri.withAppendedPath(CONTENT_LOOKUP_URI, param1String), param1Long);
    }
    
    public static Uri lookupContact(ContentResolver param1ContentResolver, Uri param1Uri) {
      SeempLog.record(87);
      if (param1Uri == null)
        return null; 
      Cursor cursor = param1ContentResolver.query(param1Uri, new String[] { "_id" }, null, null, null);
      if (cursor == null)
        return null; 
      try {
        if (cursor.moveToFirst()) {
          long l = cursor.getLong(0);
          param1Uri = ContentUris.withAppendedId(CONTENT_URI, l);
          return param1Uri;
        } 
        return null;
      } finally {
        cursor.close();
      } 
    }
    
    @Deprecated
    public static void markAsContacted(ContentResolver param1ContentResolver, long param1Long) {}
    
    public static boolean isEnterpriseContactId(long param1Long) {
      boolean bool;
      if (param1Long >= ENTERPRISE_CONTACT_ID_BASE && param1Long < 9223372034707292160L) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public static final class Data implements BaseColumns, ContactsContract.DataColumns {
      public static final String CONTENT_DIRECTORY = "data";
    }
    
    class Entity implements BaseColumns, ContactsContract.ContactsColumns, ContactsContract.ContactNameColumns, ContactsContract.RawContactsColumns, ContactsContract.BaseSyncColumns, ContactsContract.SyncColumns, ContactsContract.DataColumns, ContactsContract.StatusColumns, ContactsContract.ContactOptionsColumns, ContactsContract.ContactStatusColumns, ContactsContract.DataUsageStatColumns {
      public static final String CONTENT_DIRECTORY = "entities";
      
      public static final String DATA_ID = "data_id";
      
      public static final String RAW_CONTACT_ID = "raw_contact_id";
    }
    
    @Deprecated
    public static final class StreamItems implements ContactsContract.StreamItemsColumns {
      @Deprecated
      public static final String CONTENT_DIRECTORY = "stream_items";
    }
    
    public static final class AggregationSuggestions implements BaseColumns, ContactsContract.ContactsColumns, ContactsContract.ContactOptionsColumns, ContactsContract.ContactStatusColumns {
      public static final String CONTENT_DIRECTORY = "suggestions";
      
      public static final String PARAMETER_MATCH_NAME = "name";
      
      class Builder {
        private long mContactId;
        
        private int mLimit;
        
        private final ArrayList<String> mValues = new ArrayList<>();
        
        public Builder setContactId(long param3Long) {
          this.mContactId = param3Long;
          return this;
        }
        
        public Builder addNameParameter(String param3String) {
          this.mValues.add(param3String);
          return this;
        }
        
        public Builder setLimit(int param3Int) {
          this.mLimit = param3Int;
          return this;
        }
        
        public Uri build() {
          Uri.Builder builder = ContactsContract.Contacts.CONTENT_URI.buildUpon();
          builder.appendEncodedPath(String.valueOf(this.mContactId));
          builder.appendPath("suggestions");
          int i = this.mLimit;
          if (i != 0)
            builder.appendQueryParameter("limit", String.valueOf(i)); 
          int j = this.mValues.size();
          for (i = 0; i < j; i++) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("name:");
            ArrayList<String> arrayList = this.mValues;
            stringBuilder.append(arrayList.get(i));
            String str = stringBuilder.toString();
            builder.appendQueryParameter("query", str);
          } 
          return builder.build();
        }
      }
      
      public static final Builder builder() {
        return new Builder();
      }
    }
    
    class Builder {
      private long mContactId;
      
      private int mLimit;
      
      private final ArrayList<String> mValues = new ArrayList<>();
      
      public Builder setContactId(long param2Long) {
        this.mContactId = param2Long;
        return this;
      }
      
      public Builder addNameParameter(String param2String) {
        this.mValues.add(param2String);
        return this;
      }
      
      public Builder setLimit(int param2Int) {
        this.mLimit = param2Int;
        return this;
      }
      
      public Uri build() {
        Uri.Builder builder = ContactsContract.Contacts.CONTENT_URI.buildUpon();
        builder.appendEncodedPath(String.valueOf(this.mContactId));
        builder.appendPath("suggestions");
        int i = this.mLimit;
        if (i != 0)
          builder.appendQueryParameter("limit", String.valueOf(i)); 
        int j = this.mValues.size();
        for (i = 0; i < j; i++) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("name:");
          ArrayList<String> arrayList = this.mValues;
          stringBuilder.append(arrayList.get(i));
          String str = stringBuilder.toString();
          builder.appendQueryParameter("query", str);
        } 
        return builder.build();
      }
    }
    
    class Photo implements BaseColumns, ContactsContract.DataColumnsWithJoins {
      public static final String CONTENT_DIRECTORY = "photo";
      
      public static final String DISPLAY_PHOTO = "display_photo";
      
      public static final String PHOTO = "data15";
      
      public static final String PHOTO_FILE_ID = "data14";
    }
    
    public static InputStream openContactPhotoInputStream(ContentResolver param1ContentResolver, Uri param1Uri, boolean param1Boolean) {
      SeempLog.record(88);
      if (param1Boolean) {
        Uri uri = Uri.withAppendedPath(param1Uri, "display_photo");
        try {
          AssetFileDescriptor assetFileDescriptor = param1ContentResolver.openAssetFileDescriptor(uri, "r");
          if (assetFileDescriptor != null)
            return assetFileDescriptor.createInputStream(); 
        } catch (IOException iOException) {}
      } 
      param1Uri = Uri.withAppendedPath(param1Uri, "photo");
      if (param1Uri == null)
        return null; 
      Cursor cursor = param1ContentResolver.query(param1Uri, new String[] { "data15" }, null, null, null);
      if (cursor != null)
        try {
        
        } finally {
          if (cursor != null)
            cursor.close(); 
        }  
      if (cursor != null)
        cursor.close(); 
      return null;
    }
    
    public static InputStream openContactPhotoInputStream(ContentResolver param1ContentResolver, Uri param1Uri) {
      SeempLog.record(88);
      return openContactPhotoInputStream(param1ContentResolver, param1Uri, false);
    }
    
    public static Uri createCorpLookupUriFromEnterpriseLookupUri(Uri param1Uri) {
      List<String> list = param1Uri.getPathSegments();
      if (list == null || list.size() <= 2)
        return null; 
      String str = list.get(2);
      if (TextUtils.isEmpty(str) || !str.startsWith(ENTERPRISE_CONTACT_LOOKUP_PREFIX))
        return null; 
      str = str.substring(ENTERPRISE_CONTACT_LOOKUP_PREFIX.length());
      return Uri.withAppendedPath(CONTENT_LOOKUP_URI, str);
    }
  }
  
  class Profile implements BaseColumns, ContactsColumns, ContactOptionsColumns, ContactNameColumns, ContactStatusColumns {
    public static final Uri CONTENT_RAW_CONTACTS_URI;
    
    public static final Uri CONTENT_URI;
    
    public static final Uri CONTENT_VCARD_URI;
    
    public static final long MIN_ID = 9223372034707292160L;
    
    static {
      Uri uri = Uri.withAppendedPath(ContactsContract.AUTHORITY_URI, "profile");
      CONTENT_VCARD_URI = Uri.withAppendedPath(uri, "as_vcard");
      CONTENT_RAW_CONTACTS_URI = Uri.withAppendedPath(CONTENT_URI, "raw_contacts");
    }
  }
  
  public static boolean isProfileId(long paramLong) {
    boolean bool;
    if (paramLong >= 9223372034707292160L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  class DeletedContacts implements DeletedContactsColumns {
    public static final Uri CONTENT_URI = Uri.withAppendedPath(ContactsContract.AUTHORITY_URI, "deleted_contacts");
    
    private static final int DAYS_KEPT = 30;
    
    public static final long DAYS_KEPT_MILLISECONDS = 2592000000L;
  }
  
  class RawContacts implements BaseColumns, RawContactsColumns, ContactOptionsColumns, ContactNameColumns, SyncColumns {
    public static final int AGGREGATION_MODE_DEFAULT = 0;
    
    public static final int AGGREGATION_MODE_DISABLED = 3;
    
    @Deprecated
    public static final int AGGREGATION_MODE_IMMEDIATE = 1;
    
    public static final int AGGREGATION_MODE_SUSPENDED = 2;
    
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/raw_contact";
    
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/raw_contact";
    
    public static final Uri CONTENT_URI = Uri.withAppendedPath(ContactsContract.AUTHORITY_URI, "raw_contacts");
    
    public static Uri getContactLookupUri(ContentResolver param1ContentResolver, Uri param1Uri) {
      SeempLog.record(89);
      param1Uri = Uri.withAppendedPath(param1Uri, "data");
      Cursor cursor = param1ContentResolver.query(param1Uri, new String[] { "contact_id", "lookup" }, null, null, null);
      if (cursor != null)
        try {
          if (cursor.moveToFirst()) {
            long l = cursor.getLong(0);
            String str = cursor.getString(1);
            return ContactsContract.Contacts.getLookupUri(l, str);
          } 
        } finally {
          if (cursor != null)
            cursor.close(); 
        }  
      if (cursor != null)
        cursor.close(); 
      return null;
    }
    
    public static String getLocalAccountName(Context param1Context) {
      return TextUtils.nullIfEmpty(param1Context.getString(17039953));
    }
    
    public static String getLocalAccountType(Context param1Context) {
      return TextUtils.nullIfEmpty(param1Context.getString(17039954));
    }
    
    class Data implements BaseColumns, ContactsContract.DataColumns {
      public static final String CONTENT_DIRECTORY = "data";
    }
    
    class Entity implements BaseColumns, ContactsContract.DataColumns {
      public static final String CONTENT_DIRECTORY = "entity";
      
      public static final String DATA_ID = "data_id";
    }
    
    @Deprecated
    class StreamItems implements BaseColumns, ContactsContract.StreamItemsColumns {
      @Deprecated
      public static final String CONTENT_DIRECTORY = "stream_items";
    }
    
    class DisplayPhoto {
      public static final String CONTENT_DIRECTORY = "display_photo";
    }
    
    public static EntityIterator newEntityIterator(Cursor param1Cursor) {
      return (EntityIterator)new EntityIteratorImpl(param1Cursor);
    }
    
    private static class EntityIteratorImpl extends CursorEntityIterator {
      private static final String[] DATA_KEYS = new String[] { 
          "data1", "data2", "data3", "data4", "data5", "data6", "data7", "data8", "data9", "data10", 
          "data11", "data12", "data13", "data14", "data15", "data_sync1", "data_sync2", "data_sync3", "data_sync4" };
      
      public EntityIteratorImpl(Cursor param2Cursor) {
        super(param2Cursor);
      }
      
      public android.content.Entity getEntityAndIncrementCursor(Cursor param2Cursor) throws RemoteException {
        int i = param2Cursor.getColumnIndexOrThrow("_id");
        long l = param2Cursor.getLong(i);
        ContentValues contentValues = new ContentValues();
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "account_name");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "account_type");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "data_set");
        DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues, "_id");
        DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues, "dirty");
        DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues, "version");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "sourceid");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "sync1");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "sync2");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "sync3");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "sync4");
        DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues, "deleted");
        DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues, "contact_id");
        DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues, "starred");
        android.content.Entity entity = new android.content.Entity(contentValues);
        while (l == param2Cursor.getLong(i)) {
          ContentValues contentValues1 = new ContentValues();
          contentValues1.put("_id", Long.valueOf(param2Cursor.getLong(param2Cursor.getColumnIndexOrThrow("data_id"))));
          DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues1, "res_package");
          DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues1, "mimetype");
          DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues1, "is_primary");
          DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues1, "is_super_primary");
          DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues1, "data_version");
          DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues1, "group_sourceid");
          DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues1, "data_version");
          for (String str : DATA_KEYS) {
            int j = param2Cursor.getColumnIndexOrThrow(str);
            int k = param2Cursor.getType(j);
            if (k != 0)
              if (k != 1 && k != 2 && k != 3) {
                if (k == 4) {
                  contentValues1.put(str, param2Cursor.getBlob(j));
                } else {
                  throw new IllegalStateException("Invalid or unhandled data type");
                } 
              } else {
                contentValues1.put(str, param2Cursor.getString(j));
              }  
          } 
          entity.addSubValue(ContactsContract.Data.CONTENT_URI, contentValues1);
          if (!param2Cursor.moveToNext())
            break; 
        } 
        return entity;
      }
    }
  }
  
  public static final class DisplayPhoto {
    public static final String CONTENT_DIRECTORY = "display_photo";
  }
  
  @Deprecated
  class StreamItems implements BaseColumns, StreamItemsColumns {
    @Deprecated
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/stream_item";
    
    @Deprecated
    public static final Uri CONTENT_LIMIT_URI;
    
    @Deprecated
    public static final Uri CONTENT_PHOTO_URI;
    
    @Deprecated
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/stream_item";
    
    @Deprecated
    public static final Uri CONTENT_URI;
    
    @Deprecated
    public static final String MAX_ITEMS = "max_items";
    
    static {
      Uri uri = Uri.withAppendedPath(ContactsContract.AUTHORITY_URI, "stream_items");
      CONTENT_PHOTO_URI = Uri.withAppendedPath(uri, "photo");
      uri = ContactsContract.AUTHORITY_URI;
      CONTENT_LIMIT_URI = Uri.withAppendedPath(uri, "stream_items_limit");
    }
    
    @Deprecated
    public static final class StreamItemPhotos implements BaseColumns, ContactsContract.StreamItemPhotosColumns {
      @Deprecated
      public static final String CONTENT_DIRECTORY = "photo";
      
      @Deprecated
      public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/stream_item_photo";
      
      @Deprecated
      public static final String CONTENT_TYPE = "vnd.android.cursor.dir/stream_item_photo";
    }
  }
  
  @Deprecated
  class StreamItemPhotos implements BaseColumns, StreamItemPhotosColumns {
    @Deprecated
    public static final String PHOTO = "photo";
  }
  
  class PhotoFiles implements BaseColumns, PhotoFilesColumns {}
  
  class Data implements DataColumnsWithJoins, ContactCounts {
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/data";
    
    public static final Uri CONTENT_URI = Uri.withAppendedPath(ContactsContract.AUTHORITY_URI, "data");
    
    static final Uri ENTERPRISE_CONTENT_URI = Uri.withAppendedPath(ContactsContract.AUTHORITY_URI, "data_enterprise");
    
    public static final String VISIBLE_CONTACTS_ONLY = "visible_contacts_only";
    
    public static Uri getContactLookupUri(ContentResolver param1ContentResolver, Uri param1Uri) {
      SeempLog.record(89);
      Cursor cursor = param1ContentResolver.query(param1Uri, new String[] { "contact_id", "lookup" }, null, null, null);
      if (cursor != null)
        try {
          if (cursor.moveToFirst()) {
            long l = cursor.getLong(0);
            String str = cursor.getString(1);
            return ContactsContract.Contacts.getLookupUri(l, str);
          } 
        } finally {
          if (cursor != null)
            cursor.close(); 
        }  
      if (cursor != null)
        cursor.close(); 
      return null;
    }
  }
  
  class RawContactsEntity implements BaseColumns, DataColumns, RawContactsColumns {
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/raw_contact_entity";
    
    public static final Uri CONTENT_URI;
    
    public static final Uri CORP_CONTENT_URI;
    
    public static final String DATA_ID = "data_id";
    
    public static final String FOR_EXPORT_ONLY = "for_export_only";
    
    public static final Uri PROFILE_CONTENT_URI;
    
    static {
      Uri uri = ContactsContract.AUTHORITY_URI;
      CONTENT_URI = Uri.withAppendedPath(uri, "raw_contact_entities");
      uri = ContactsContract.AUTHORITY_URI;
      CORP_CONTENT_URI = Uri.withAppendedPath(uri, "raw_contact_entities_corp");
      uri = ContactsContract.Profile.CONTENT_URI;
      PROFILE_CONTENT_URI = Uri.withAppendedPath(uri, "raw_contact_entities");
    }
  }
  
  class PhoneLookup implements BaseColumns, PhoneLookupColumns, ContactsColumns, ContactOptionsColumns, ContactNameColumns {
    public static final Uri CONTENT_FILTER_URI = Uri.withAppendedPath(ContactsContract.AUTHORITY_URI, "phone_lookup");
    
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/phone_lookup";
    
    public static final Uri ENTERPRISE_CONTENT_FILTER_URI = Uri.withAppendedPath(ContactsContract.AUTHORITY_URI, "phone_lookup_enterprise");
    
    public static final String QUERY_PARAMETER_SIP_ADDRESS = "sip";
  }
  
  class StatusUpdates implements StatusColumns, PresenceColumns {
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/status-update";
    
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/status-update";
    
    private StatusUpdates() {}
    
    public static final Uri CONTENT_URI = Uri.withAppendedPath(ContactsContract.AUTHORITY_URI, "status_updates");
    
    public static final Uri PROFILE_CONTENT_URI;
    
    static {
      Uri uri = ContactsContract.Profile.CONTENT_URI;
      PROFILE_CONTENT_URI = Uri.withAppendedPath(uri, "status_updates");
    }
    
    public static final int getPresenceIconResourceId(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2 && param1Int != 3) {
          if (param1Int != 4) {
            if (param1Int != 5)
              return 17301610; 
            return 17301611;
          } 
          return 17301608;
        } 
        return 17301607;
      } 
      return 17301609;
    }
    
    public static final int getPresencePrecedence(int param1Int) {
      return param1Int;
    }
  }
  
  @Deprecated
  class Presence extends StatusUpdates {}
  
  public static class SearchSnippets {
    public static final String DEFERRED_SNIPPETING_KEY = "deferred_snippeting";
    
    public static final String SNIPPET = "snippet";
    
    public static final String SNIPPET_ARGS_PARAM_KEY = "snippet_args";
  }
  
  public static final class CommonDataKinds {
    public static final String PACKAGE_COMMON = "common";
    
    class StructuredName implements ContactsContract.DataColumnsWithJoins, ContactsContract.ContactCounts {
      public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/name";
      
      public static final String DISPLAY_NAME = "data1";
      
      public static final String FAMILY_NAME = "data3";
      
      public static final String FULL_NAME_STYLE = "data10";
      
      public static final String GIVEN_NAME = "data2";
      
      public static final String MIDDLE_NAME = "data5";
      
      public static final String PHONETIC_FAMILY_NAME = "data9";
      
      public static final String PHONETIC_GIVEN_NAME = "data7";
      
      public static final String PHONETIC_MIDDLE_NAME = "data8";
      
      public static final String PHONETIC_NAME_STYLE = "data11";
      
      public static final String PREFIX = "data4";
      
      public static final String SUFFIX = "data6";
    }
    
    class Nickname implements ContactsContract.DataColumnsWithJoins, CommonColumns, ContactsContract.ContactCounts {
      public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/nickname";
      
      public static final String NAME = "data1";
      
      public static final int TYPE_DEFAULT = 1;
      
      public static final int TYPE_INITIALS = 5;
      
      public static final int TYPE_MAIDEN_NAME = 3;
      
      @Deprecated
      public static final int TYPE_MAINDEN_NAME = 3;
      
      public static final int TYPE_OTHER_NAME = 2;
      
      public static final int TYPE_SHORT_NAME = 4;
    }
    
    class Phone implements ContactsContract.DataColumnsWithJoins, CommonColumns, ContactsContract.ContactCounts {
      public static final Uri CONTENT_FILTER_URI;
      
      public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/phone_v2";
      
      public static final String CONTENT_TYPE = "vnd.android.cursor.dir/phone_v2";
      
      public static final Uri CONTENT_URI = Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, "phones");
      
      public static final Uri ENTERPRISE_CONTENT_FILTER_URI;
      
      public static final Uri ENTERPRISE_CONTENT_URI;
      
      public static final String NORMALIZED_NUMBER = "data4";
      
      public static final String NUMBER = "data1";
      
      public static final String SEARCH_DISPLAY_NAME_KEY = "search_display_name";
      
      public static final String SEARCH_PHONE_NUMBER_KEY = "search_phone_number";
      
      public static final int TYPE_ASSISTANT = 19;
      
      public static final int TYPE_CALLBACK = 8;
      
      public static final int TYPE_CAR = 9;
      
      public static final int TYPE_COMPANY_MAIN = 10;
      
      public static final int TYPE_FAX_HOME = 5;
      
      public static final int TYPE_FAX_WORK = 4;
      
      public static final int TYPE_HOME = 1;
      
      public static final int TYPE_ISDN = 11;
      
      public static final int TYPE_MAIN = 12;
      
      public static final int TYPE_MMS = 20;
      
      public static final int TYPE_MOBILE = 2;
      
      public static final int TYPE_OTHER = 7;
      
      public static final int TYPE_OTHER_FAX = 13;
      
      public static final int TYPE_PAGER = 6;
      
      public static final int TYPE_RADIO = 14;
      
      public static final int TYPE_TELEX = 15;
      
      public static final int TYPE_TTY_TDD = 16;
      
      public static final int TYPE_WORK = 3;
      
      public static final int TYPE_WORK_MOBILE = 17;
      
      public static final int TYPE_WORK_PAGER = 18;
      
      static {
        Uri uri = ContactsContract.Data.ENTERPRISE_CONTENT_URI;
        ENTERPRISE_CONTENT_URI = Uri.withAppendedPath(uri, "phones");
        CONTENT_FILTER_URI = Uri.withAppendedPath(CONTENT_URI, "filter");
        ENTERPRISE_CONTENT_FILTER_URI = Uri.withAppendedPath(CONTENT_URI, "filter_enterprise");
      }
      
      @Deprecated
      public static final CharSequence getDisplayLabel(Context param2Context, int param2Int, CharSequence param2CharSequence, CharSequence[] param2ArrayOfCharSequence) {
        return getTypeLabel(param2Context.getResources(), param2Int, param2CharSequence);
      }
      
      @Deprecated
      public static final CharSequence getDisplayLabel(Context param2Context, int param2Int, CharSequence param2CharSequence) {
        return getTypeLabel(param2Context.getResources(), param2Int, param2CharSequence);
      }
      
      public static final int getTypeLabelResource(int param2Int) {
        switch (param2Int) {
          default:
            return 17041076;
          case 20:
            return 17041082;
          case 19:
            return 17041072;
          case 18:
            return 17041092;
          case 17:
            return 17041091;
          case 16:
            return 17041089;
          case 15:
            return 17041088;
          case 14:
            return 17041087;
          case 13:
            return 17041085;
          case 12:
            return 17041081;
          case 11:
            return 17041080;
          case 10:
            return 17041075;
          case 9:
            return 17041074;
          case 8:
            return 17041073;
          case 7:
            return 17041084;
          case 6:
            return 17041086;
          case 5:
            return 17041077;
          case 4:
            return 17041078;
          case 3:
            return 17041090;
          case 2:
            return 17041083;
          case 1:
            break;
        } 
        return 17041079;
      }
      
      public static final CharSequence getTypeLabel(Resources param2Resources, int param2Int, CharSequence param2CharSequence) {
        if ((param2Int == 0 || param2Int == 19) && !TextUtils.isEmpty(param2CharSequence))
          return param2CharSequence; 
        param2Int = getTypeLabelResource(param2Int);
        return param2Resources.getText(param2Int);
      }
    }
    
    class Email implements ContactsContract.DataColumnsWithJoins, CommonColumns, ContactsContract.ContactCounts {
      public static final String ADDRESS = "data1";
      
      public static final Uri CONTENT_FILTER_URI;
      
      public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/email_v2";
      
      public static final Uri CONTENT_LOOKUP_URI;
      
      public static final String CONTENT_TYPE = "vnd.android.cursor.dir/email_v2";
      
      public static final Uri CONTENT_URI;
      
      public static final String DISPLAY_NAME = "data4";
      
      public static final Uri ENTERPRISE_CONTENT_FILTER_URI;
      
      public static final Uri ENTERPRISE_CONTENT_LOOKUP_URI;
      
      public static final int TYPE_HOME = 1;
      
      public static final int TYPE_MOBILE = 4;
      
      public static final int TYPE_OTHER = 3;
      
      public static final int TYPE_WORK = 2;
      
      static {
        Uri uri = Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, "emails");
        CONTENT_LOOKUP_URI = Uri.withAppendedPath(uri, "lookup");
        uri = CONTENT_URI;
        ENTERPRISE_CONTENT_LOOKUP_URI = Uri.withAppendedPath(uri, "lookup_enterprise");
        CONTENT_FILTER_URI = Uri.withAppendedPath(CONTENT_URI, "filter");
        ENTERPRISE_CONTENT_FILTER_URI = Uri.withAppendedPath(CONTENT_URI, "filter_enterprise");
      }
      
      public static final int getTypeLabelResource(int param2Int) {
        if (param2Int != 1) {
          if (param2Int != 2) {
            if (param2Int != 3) {
              if (param2Int != 4)
                return 17040102; 
              return 17040104;
            } 
            return 17040105;
          } 
          return 17040106;
        } 
        return 17040103;
      }
      
      public static final CharSequence getTypeLabel(Resources param2Resources, int param2Int, CharSequence param2CharSequence) {
        if (param2Int == 0 && !TextUtils.isEmpty(param2CharSequence))
          return param2CharSequence; 
        param2Int = getTypeLabelResource(param2Int);
        return param2Resources.getText(param2Int);
      }
    }
    
    class StructuredPostal implements ContactsContract.DataColumnsWithJoins, CommonColumns, ContactsContract.ContactCounts {
      public static final String CITY = "data7";
      
      public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/postal-address_v2";
      
      public static final String CONTENT_TYPE = "vnd.android.cursor.dir/postal-address_v2";
      
      public static final Uri CONTENT_URI = Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, "postals");
      
      public static final String COUNTRY = "data10";
      
      public static final String FORMATTED_ADDRESS = "data1";
      
      public static final String NEIGHBORHOOD = "data6";
      
      public static final String POBOX = "data5";
      
      public static final String POSTCODE = "data9";
      
      public static final String REGION = "data8";
      
      public static final String STREET = "data4";
      
      public static final int TYPE_HOME = 1;
      
      public static final int TYPE_OTHER = 3;
      
      public static final int TYPE_WORK = 2;
      
      public static final int getTypeLabelResource(int param2Int) {
        if (param2Int != 1) {
          if (param2Int != 2) {
            if (param2Int != 3)
              return 17041119; 
            return 17041121;
          } 
          return 17041122;
        } 
        return 17041120;
      }
      
      public static final CharSequence getTypeLabel(Resources param2Resources, int param2Int, CharSequence param2CharSequence) {
        if (param2Int == 0 && !TextUtils.isEmpty(param2CharSequence))
          return param2CharSequence; 
        param2Int = getTypeLabelResource(param2Int);
        return param2Resources.getText(param2Int);
      }
    }
    
    class Im implements ContactsContract.DataColumnsWithJoins, CommonColumns, ContactsContract.ContactCounts {
      public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/im";
      
      public static final String CUSTOM_PROTOCOL = "data6";
      
      public static final String PROTOCOL = "data5";
      
      public static final int PROTOCOL_AIM = 0;
      
      public static final int PROTOCOL_CUSTOM = -1;
      
      public static final int PROTOCOL_GOOGLE_TALK = 5;
      
      public static final int PROTOCOL_ICQ = 6;
      
      public static final int PROTOCOL_JABBER = 7;
      
      public static final int PROTOCOL_MSN = 1;
      
      public static final int PROTOCOL_NETMEETING = 8;
      
      public static final int PROTOCOL_QQ = 4;
      
      public static final int PROTOCOL_SKYPE = 3;
      
      public static final int PROTOCOL_YAHOO = 2;
      
      public static final int TYPE_HOME = 1;
      
      public static final int TYPE_OTHER = 3;
      
      public static final int TYPE_WORK = 2;
      
      public static final int getTypeLabelResource(int param2Int) {
        if (param2Int != 1) {
          if (param2Int != 2) {
            if (param2Int != 3)
              return 17040336; 
            return 17040338;
          } 
          return 17040339;
        } 
        return 17040337;
      }
      
      public static final CharSequence getTypeLabel(Resources param2Resources, int param2Int, CharSequence param2CharSequence) {
        if (param2Int == 0 && !TextUtils.isEmpty(param2CharSequence))
          return param2CharSequence; 
        param2Int = getTypeLabelResource(param2Int);
        return param2Resources.getText(param2Int);
      }
      
      public static final int getProtocolLabelResource(int param2Int) {
        switch (param2Int) {
          default:
            return 17040327;
          case 8:
            return 17040332;
          case 7:
            return 17040330;
          case 6:
            return 17040329;
          case 5:
            return 17040328;
          case 4:
            return 17040333;
          case 3:
            return 17040334;
          case 2:
            return 17040335;
          case 1:
            return 17040331;
          case 0:
            break;
        } 
        return 17040326;
      }
      
      public static final CharSequence getProtocolLabel(Resources param2Resources, int param2Int, CharSequence param2CharSequence) {
        if (param2Int == -1 && !TextUtils.isEmpty(param2CharSequence))
          return param2CharSequence; 
        param2Int = getProtocolLabelResource(param2Int);
        return param2Resources.getText(param2Int);
      }
    }
    
    class Organization implements ContactsContract.DataColumnsWithJoins, CommonColumns, ContactsContract.ContactCounts {
      public static final String COMPANY = "data1";
      
      public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/organization";
      
      public static final String DEPARTMENT = "data5";
      
      public static final String JOB_DESCRIPTION = "data6";
      
      public static final String OFFICE_LOCATION = "data9";
      
      public static final String PHONETIC_NAME = "data8";
      
      public static final String PHONETIC_NAME_STYLE = "data10";
      
      public static final String SYMBOL = "data7";
      
      public static final String TITLE = "data4";
      
      public static final int TYPE_OTHER = 2;
      
      public static final int TYPE_WORK = 1;
      
      public static final int getTypeLabelResource(int param2Int) {
        if (param2Int != 1) {
          if (param2Int != 2)
            return 17040756; 
          return 17040757;
        } 
        return 17040758;
      }
      
      public static final CharSequence getTypeLabel(Resources param2Resources, int param2Int, CharSequence param2CharSequence) {
        if (param2Int == 0 && !TextUtils.isEmpty(param2CharSequence))
          return param2CharSequence; 
        param2Int = getTypeLabelResource(param2Int);
        return param2Resources.getText(param2Int);
      }
    }
    
    class Relation implements ContactsContract.DataColumnsWithJoins, CommonColumns, ContactsContract.ContactCounts {
      public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/relation";
      
      public static final String NAME = "data1";
      
      public static final int TYPE_ASSISTANT = 1;
      
      public static final int TYPE_BROTHER = 2;
      
      public static final int TYPE_CHILD = 3;
      
      public static final int TYPE_DOMESTIC_PARTNER = 4;
      
      public static final int TYPE_FATHER = 5;
      
      public static final int TYPE_FRIEND = 6;
      
      public static final int TYPE_MANAGER = 7;
      
      public static final int TYPE_MOTHER = 8;
      
      public static final int TYPE_PARENT = 9;
      
      public static final int TYPE_PARTNER = 10;
      
      public static final int TYPE_REFERRED_BY = 11;
      
      public static final int TYPE_RELATIVE = 12;
      
      public static final int TYPE_SISTER = 13;
      
      public static final int TYPE_SPOUSE = 14;
      
      public static final int getTypeLabelResource(int param2Int) {
        switch (param2Int) {
          default:
            return 17040756;
          case 14:
            return 17041167;
          case 13:
            return 17041166;
          case 12:
            return 17041165;
          case 11:
            return 17041164;
          case 10:
            return 17041163;
          case 9:
            return 17041162;
          case 8:
            return 17041161;
          case 7:
            return 17041160;
          case 6:
            return 17041159;
          case 5:
            return 17041158;
          case 4:
            return 17041157;
          case 3:
            return 17041155;
          case 2:
            return 17041154;
          case 1:
            break;
        } 
        return 17041153;
      }
      
      public static final CharSequence getTypeLabel(Resources param2Resources, int param2Int, CharSequence param2CharSequence) {
        if (param2Int == 0 && !TextUtils.isEmpty(param2CharSequence))
          return param2CharSequence; 
        param2Int = getTypeLabelResource(param2Int);
        return param2Resources.getText(param2Int);
      }
    }
    
    class Event implements ContactsContract.DataColumnsWithJoins, CommonColumns, ContactsContract.ContactCounts {
      public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/contact_event";
      
      public static final String START_DATE = "data1";
      
      public static final int TYPE_ANNIVERSARY = 1;
      
      public static final int TYPE_BIRTHDAY = 3;
      
      public static final int TYPE_OTHER = 2;
      
      public static int getTypeResource(Integer param2Integer) {
        if (param2Integer == null)
          return 17040123; 
        int i = param2Integer.intValue();
        if (i != 1) {
          if (i != 2) {
            if (i != 3)
              return 17040122; 
            return 17040121;
          } 
          return 17040123;
        } 
        return 17040120;
      }
      
      public static final CharSequence getTypeLabel(Resources param2Resources, int param2Int, CharSequence param2CharSequence) {
        if (param2Int == 0 && !TextUtils.isEmpty(param2CharSequence))
          return param2CharSequence; 
        param2Int = getTypeResource(Integer.valueOf(param2Int));
        return param2Resources.getText(param2Int);
      }
    }
    
    class Photo implements ContactsContract.DataColumnsWithJoins, ContactsContract.ContactCounts {
      public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/photo";
      
      public static final String PHOTO = "data15";
      
      public static final String PHOTO_FILE_ID = "data14";
    }
    
    class Note implements ContactsContract.DataColumnsWithJoins, ContactsContract.ContactCounts {
      public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/note";
      
      public static final String NOTE = "data1";
    }
    
    class GroupMembership implements ContactsContract.DataColumnsWithJoins, ContactsContract.ContactCounts {
      public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/group_membership";
      
      public static final String GROUP_ROW_ID = "data1";
      
      public static final String GROUP_SOURCE_ID = "group_sourceid";
    }
    
    class Website implements ContactsContract.DataColumnsWithJoins, CommonColumns, ContactsContract.ContactCounts {
      public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/website";
      
      public static final int TYPE_BLOG = 2;
      
      public static final int TYPE_FTP = 6;
      
      public static final int TYPE_HOME = 4;
      
      public static final int TYPE_HOMEPAGE = 1;
      
      public static final int TYPE_OTHER = 7;
      
      public static final int TYPE_PROFILE = 3;
      
      public static final int TYPE_WORK = 5;
      
      public static final String URL = "data1";
    }
    
    class SipAddress implements ContactsContract.DataColumnsWithJoins, CommonColumns, ContactsContract.ContactCounts {
      public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/sip_address";
      
      public static final String SIP_ADDRESS = "data1";
      
      public static final int TYPE_HOME = 1;
      
      public static final int TYPE_OTHER = 3;
      
      public static final int TYPE_WORK = 2;
      
      public static final int getTypeLabelResource(int param2Int) {
        if (param2Int != 1) {
          if (param2Int != 2) {
            if (param2Int != 3)
              return 17041300; 
            return 17041302;
          } 
          return 17041303;
        } 
        return 17041301;
      }
      
      public static final CharSequence getTypeLabel(Resources param2Resources, int param2Int, CharSequence param2CharSequence) {
        if (param2Int == 0 && !TextUtils.isEmpty(param2CharSequence))
          return param2CharSequence; 
        param2Int = getTypeLabelResource(param2Int);
        return param2Resources.getText(param2Int);
      }
    }
    
    class Identity implements ContactsContract.DataColumnsWithJoins, ContactsContract.ContactCounts {
      public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/identity";
      
      public static final String IDENTITY = "data1";
      
      public static final String NAMESPACE = "data2";
    }
    
    class Callable implements ContactsContract.DataColumnsWithJoins, CommonColumns, ContactsContract.ContactCounts {
      public static final Uri CONTENT_FILTER_URI;
      
      public static final Uri CONTENT_URI;
      
      static {
        Uri uri = Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, "callables");
        CONTENT_FILTER_URI = Uri.withAppendedPath(uri, "filter");
      }
      
      public static final Uri ENTERPRISE_CONTENT_FILTER_URI = Uri.withAppendedPath(CONTENT_URI, "filter_enterprise");
    }
    
    class Contactables implements ContactsContract.DataColumnsWithJoins, CommonColumns, ContactsContract.ContactCounts {
      public static final Uri CONTENT_FILTER_URI;
      
      public static final Uri CONTENT_URI;
      
      public static final String VISIBLE_CONTACTS_ONLY = "visible_contacts_only";
      
      static {
        Uri uri = Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, "contactables");
        CONTENT_FILTER_URI = Uri.withAppendedPath(uri, "filter");
      }
    }
    
    public static interface BaseTypes {
      public static final int TYPE_CUSTOM = 0;
    }
    
    class CommonColumns implements BaseTypes {
      public static final String DATA = "data1";
      
      public static final String LABEL = "data3";
      
      public static final String TYPE = "data2";
    }
  }
  
  class StructuredName implements DataColumnsWithJoins, ContactCounts {
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/name";
    
    public static final String DISPLAY_NAME = "data1";
    
    public static final String FAMILY_NAME = "data3";
    
    public static final String FULL_NAME_STYLE = "data10";
    
    public static final String GIVEN_NAME = "data2";
    
    public static final String MIDDLE_NAME = "data5";
    
    public static final String PHONETIC_FAMILY_NAME = "data9";
    
    public static final String PHONETIC_GIVEN_NAME = "data7";
    
    public static final String PHONETIC_MIDDLE_NAME = "data8";
    
    public static final String PHONETIC_NAME_STYLE = "data11";
    
    public static final String PREFIX = "data4";
    
    public static final String SUFFIX = "data6";
  }
  
  class Nickname implements DataColumnsWithJoins, CommonDataKinds.CommonColumns, ContactCounts {
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/nickname";
    
    public static final String NAME = "data1";
    
    public static final int TYPE_DEFAULT = 1;
    
    public static final int TYPE_INITIALS = 5;
    
    public static final int TYPE_MAIDEN_NAME = 3;
    
    @Deprecated
    public static final int TYPE_MAINDEN_NAME = 3;
    
    public static final int TYPE_OTHER_NAME = 2;
    
    public static final int TYPE_SHORT_NAME = 4;
  }
  
  class Phone implements DataColumnsWithJoins, CommonDataKinds.CommonColumns, ContactCounts {
    public static final Uri CONTENT_FILTER_URI;
    
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/phone_v2";
    
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/phone_v2";
    
    public static final Uri CONTENT_URI = Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, "phones");
    
    public static final Uri ENTERPRISE_CONTENT_FILTER_URI;
    
    public static final Uri ENTERPRISE_CONTENT_URI;
    
    public static final String NORMALIZED_NUMBER = "data4";
    
    public static final String NUMBER = "data1";
    
    public static final String SEARCH_DISPLAY_NAME_KEY = "search_display_name";
    
    public static final String SEARCH_PHONE_NUMBER_KEY = "search_phone_number";
    
    public static final int TYPE_ASSISTANT = 19;
    
    public static final int TYPE_CALLBACK = 8;
    
    public static final int TYPE_CAR = 9;
    
    public static final int TYPE_COMPANY_MAIN = 10;
    
    public static final int TYPE_FAX_HOME = 5;
    
    public static final int TYPE_FAX_WORK = 4;
    
    public static final int TYPE_HOME = 1;
    
    public static final int TYPE_ISDN = 11;
    
    public static final int TYPE_MAIN = 12;
    
    public static final int TYPE_MMS = 20;
    
    public static final int TYPE_MOBILE = 2;
    
    public static final int TYPE_OTHER = 7;
    
    public static final int TYPE_OTHER_FAX = 13;
    
    public static final int TYPE_PAGER = 6;
    
    public static final int TYPE_RADIO = 14;
    
    public static final int TYPE_TELEX = 15;
    
    public static final int TYPE_TTY_TDD = 16;
    
    public static final int TYPE_WORK = 3;
    
    public static final int TYPE_WORK_MOBILE = 17;
    
    public static final int TYPE_WORK_PAGER = 18;
    
    static {
      Uri uri = ContactsContract.Data.ENTERPRISE_CONTENT_URI;
      ENTERPRISE_CONTENT_URI = Uri.withAppendedPath(uri, "phones");
      CONTENT_FILTER_URI = Uri.withAppendedPath(CONTENT_URI, "filter");
      ENTERPRISE_CONTENT_FILTER_URI = Uri.withAppendedPath(CONTENT_URI, "filter_enterprise");
    }
    
    @Deprecated
    public static final CharSequence getDisplayLabel(Context param1Context, int param1Int, CharSequence param1CharSequence, CharSequence[] param1ArrayOfCharSequence) {
      return getTypeLabel(param1Context.getResources(), param1Int, param1CharSequence);
    }
    
    @Deprecated
    public static final CharSequence getDisplayLabel(Context param1Context, int param1Int, CharSequence param1CharSequence) {
      return getTypeLabel(param1Context.getResources(), param1Int, param1CharSequence);
    }
    
    public static final int getTypeLabelResource(int param1Int) {
      switch (param1Int) {
        default:
          return 17041076;
        case 20:
          return 17041082;
        case 19:
          return 17041072;
        case 18:
          return 17041092;
        case 17:
          return 17041091;
        case 16:
          return 17041089;
        case 15:
          return 17041088;
        case 14:
          return 17041087;
        case 13:
          return 17041085;
        case 12:
          return 17041081;
        case 11:
          return 17041080;
        case 10:
          return 17041075;
        case 9:
          return 17041074;
        case 8:
          return 17041073;
        case 7:
          return 17041084;
        case 6:
          return 17041086;
        case 5:
          return 17041077;
        case 4:
          return 17041078;
        case 3:
          return 17041090;
        case 2:
          return 17041083;
        case 1:
          break;
      } 
      return 17041079;
    }
    
    public static final CharSequence getTypeLabel(Resources param1Resources, int param1Int, CharSequence param1CharSequence) {
      if ((param1Int == 0 || param1Int == 19) && !TextUtils.isEmpty(param1CharSequence))
        return param1CharSequence; 
      param1Int = getTypeLabelResource(param1Int);
      return param1Resources.getText(param1Int);
    }
  }
  
  class Email implements DataColumnsWithJoins, CommonDataKinds.CommonColumns, ContactCounts {
    public static final String ADDRESS = "data1";
    
    public static final Uri CONTENT_FILTER_URI;
    
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/email_v2";
    
    public static final Uri CONTENT_LOOKUP_URI;
    
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/email_v2";
    
    public static final Uri CONTENT_URI;
    
    public static final String DISPLAY_NAME = "data4";
    
    public static final Uri ENTERPRISE_CONTENT_FILTER_URI;
    
    public static final Uri ENTERPRISE_CONTENT_LOOKUP_URI;
    
    public static final int TYPE_HOME = 1;
    
    public static final int TYPE_MOBILE = 4;
    
    public static final int TYPE_OTHER = 3;
    
    public static final int TYPE_WORK = 2;
    
    static {
      Uri uri = Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, "emails");
      CONTENT_LOOKUP_URI = Uri.withAppendedPath(uri, "lookup");
      uri = CONTENT_URI;
      ENTERPRISE_CONTENT_LOOKUP_URI = Uri.withAppendedPath(uri, "lookup_enterprise");
      CONTENT_FILTER_URI = Uri.withAppendedPath(CONTENT_URI, "filter");
      ENTERPRISE_CONTENT_FILTER_URI = Uri.withAppendedPath(CONTENT_URI, "filter_enterprise");
    }
    
    public static final int getTypeLabelResource(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return 17040102; 
            return 17040104;
          } 
          return 17040105;
        } 
        return 17040106;
      } 
      return 17040103;
    }
    
    public static final CharSequence getTypeLabel(Resources param1Resources, int param1Int, CharSequence param1CharSequence) {
      if (param1Int == 0 && !TextUtils.isEmpty(param1CharSequence))
        return param1CharSequence; 
      param1Int = getTypeLabelResource(param1Int);
      return param1Resources.getText(param1Int);
    }
  }
  
  class StructuredPostal implements DataColumnsWithJoins, CommonDataKinds.CommonColumns, ContactCounts {
    public static final String CITY = "data7";
    
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/postal-address_v2";
    
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/postal-address_v2";
    
    public static final Uri CONTENT_URI = Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, "postals");
    
    public static final String COUNTRY = "data10";
    
    public static final String FORMATTED_ADDRESS = "data1";
    
    public static final String NEIGHBORHOOD = "data6";
    
    public static final String POBOX = "data5";
    
    public static final String POSTCODE = "data9";
    
    public static final String REGION = "data8";
    
    public static final String STREET = "data4";
    
    public static final int TYPE_HOME = 1;
    
    public static final int TYPE_OTHER = 3;
    
    public static final int TYPE_WORK = 2;
    
    public static final int getTypeLabelResource(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return 17041119; 
          return 17041121;
        } 
        return 17041122;
      } 
      return 17041120;
    }
    
    public static final CharSequence getTypeLabel(Resources param1Resources, int param1Int, CharSequence param1CharSequence) {
      if (param1Int == 0 && !TextUtils.isEmpty(param1CharSequence))
        return param1CharSequence; 
      param1Int = getTypeLabelResource(param1Int);
      return param1Resources.getText(param1Int);
    }
  }
  
  class Im implements DataColumnsWithJoins, CommonDataKinds.CommonColumns, ContactCounts {
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/im";
    
    public static final String CUSTOM_PROTOCOL = "data6";
    
    public static final String PROTOCOL = "data5";
    
    public static final int PROTOCOL_AIM = 0;
    
    public static final int PROTOCOL_CUSTOM = -1;
    
    public static final int PROTOCOL_GOOGLE_TALK = 5;
    
    public static final int PROTOCOL_ICQ = 6;
    
    public static final int PROTOCOL_JABBER = 7;
    
    public static final int PROTOCOL_MSN = 1;
    
    public static final int PROTOCOL_NETMEETING = 8;
    
    public static final int PROTOCOL_QQ = 4;
    
    public static final int PROTOCOL_SKYPE = 3;
    
    public static final int PROTOCOL_YAHOO = 2;
    
    public static final int TYPE_HOME = 1;
    
    public static final int TYPE_OTHER = 3;
    
    public static final int TYPE_WORK = 2;
    
    public static final int getTypeLabelResource(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return 17040336; 
          return 17040338;
        } 
        return 17040339;
      } 
      return 17040337;
    }
    
    public static final CharSequence getTypeLabel(Resources param1Resources, int param1Int, CharSequence param1CharSequence) {
      if (param1Int == 0 && !TextUtils.isEmpty(param1CharSequence))
        return param1CharSequence; 
      param1Int = getTypeLabelResource(param1Int);
      return param1Resources.getText(param1Int);
    }
    
    public static final int getProtocolLabelResource(int param1Int) {
      switch (param1Int) {
        default:
          return 17040327;
        case 8:
          return 17040332;
        case 7:
          return 17040330;
        case 6:
          return 17040329;
        case 5:
          return 17040328;
        case 4:
          return 17040333;
        case 3:
          return 17040334;
        case 2:
          return 17040335;
        case 1:
          return 17040331;
        case 0:
          break;
      } 
      return 17040326;
    }
    
    public static final CharSequence getProtocolLabel(Resources param1Resources, int param1Int, CharSequence param1CharSequence) {
      if (param1Int == -1 && !TextUtils.isEmpty(param1CharSequence))
        return param1CharSequence; 
      param1Int = getProtocolLabelResource(param1Int);
      return param1Resources.getText(param1Int);
    }
  }
  
  class Organization implements DataColumnsWithJoins, CommonDataKinds.CommonColumns, ContactCounts {
    public static final String COMPANY = "data1";
    
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/organization";
    
    public static final String DEPARTMENT = "data5";
    
    public static final String JOB_DESCRIPTION = "data6";
    
    public static final String OFFICE_LOCATION = "data9";
    
    public static final String PHONETIC_NAME = "data8";
    
    public static final String PHONETIC_NAME_STYLE = "data10";
    
    public static final String SYMBOL = "data7";
    
    public static final String TITLE = "data4";
    
    public static final int TYPE_OTHER = 2;
    
    public static final int TYPE_WORK = 1;
    
    public static final int getTypeLabelResource(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return 17040756; 
        return 17040757;
      } 
      return 17040758;
    }
    
    public static final CharSequence getTypeLabel(Resources param1Resources, int param1Int, CharSequence param1CharSequence) {
      if (param1Int == 0 && !TextUtils.isEmpty(param1CharSequence))
        return param1CharSequence; 
      param1Int = getTypeLabelResource(param1Int);
      return param1Resources.getText(param1Int);
    }
  }
  
  class Relation implements DataColumnsWithJoins, CommonDataKinds.CommonColumns, ContactCounts {
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/relation";
    
    public static final String NAME = "data1";
    
    public static final int TYPE_ASSISTANT = 1;
    
    public static final int TYPE_BROTHER = 2;
    
    public static final int TYPE_CHILD = 3;
    
    public static final int TYPE_DOMESTIC_PARTNER = 4;
    
    public static final int TYPE_FATHER = 5;
    
    public static final int TYPE_FRIEND = 6;
    
    public static final int TYPE_MANAGER = 7;
    
    public static final int TYPE_MOTHER = 8;
    
    public static final int TYPE_PARENT = 9;
    
    public static final int TYPE_PARTNER = 10;
    
    public static final int TYPE_REFERRED_BY = 11;
    
    public static final int TYPE_RELATIVE = 12;
    
    public static final int TYPE_SISTER = 13;
    
    public static final int TYPE_SPOUSE = 14;
    
    public static final int getTypeLabelResource(int param1Int) {
      switch (param1Int) {
        default:
          return 17040756;
        case 14:
          return 17041167;
        case 13:
          return 17041166;
        case 12:
          return 17041165;
        case 11:
          return 17041164;
        case 10:
          return 17041163;
        case 9:
          return 17041162;
        case 8:
          return 17041161;
        case 7:
          return 17041160;
        case 6:
          return 17041159;
        case 5:
          return 17041158;
        case 4:
          return 17041157;
        case 3:
          return 17041155;
        case 2:
          return 17041154;
        case 1:
          break;
      } 
      return 17041153;
    }
    
    public static final CharSequence getTypeLabel(Resources param1Resources, int param1Int, CharSequence param1CharSequence) {
      if (param1Int == 0 && !TextUtils.isEmpty(param1CharSequence))
        return param1CharSequence; 
      param1Int = getTypeLabelResource(param1Int);
      return param1Resources.getText(param1Int);
    }
  }
  
  class Event implements DataColumnsWithJoins, CommonDataKinds.CommonColumns, ContactCounts {
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/contact_event";
    
    public static final String START_DATE = "data1";
    
    public static final int TYPE_ANNIVERSARY = 1;
    
    public static final int TYPE_BIRTHDAY = 3;
    
    public static final int TYPE_OTHER = 2;
    
    public static int getTypeResource(Integer param1Integer) {
      if (param1Integer == null)
        return 17040123; 
      int i = param1Integer.intValue();
      if (i != 1) {
        if (i != 2) {
          if (i != 3)
            return 17040122; 
          return 17040121;
        } 
        return 17040123;
      } 
      return 17040120;
    }
    
    public static final CharSequence getTypeLabel(Resources param1Resources, int param1Int, CharSequence param1CharSequence) {
      if (param1Int == 0 && !TextUtils.isEmpty(param1CharSequence))
        return param1CharSequence; 
      param1Int = getTypeResource(Integer.valueOf(param1Int));
      return param1Resources.getText(param1Int);
    }
  }
  
  class Photo implements DataColumnsWithJoins, ContactCounts {
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/photo";
    
    public static final String PHOTO = "data15";
    
    public static final String PHOTO_FILE_ID = "data14";
  }
  
  class Note implements DataColumnsWithJoins, ContactCounts {
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/note";
    
    public static final String NOTE = "data1";
  }
  
  class GroupMembership implements DataColumnsWithJoins, ContactCounts {
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/group_membership";
    
    public static final String GROUP_ROW_ID = "data1";
    
    public static final String GROUP_SOURCE_ID = "group_sourceid";
  }
  
  class Website implements DataColumnsWithJoins, CommonDataKinds.CommonColumns, ContactCounts {
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/website";
    
    public static final int TYPE_BLOG = 2;
    
    public static final int TYPE_FTP = 6;
    
    public static final int TYPE_HOME = 4;
    
    public static final int TYPE_HOMEPAGE = 1;
    
    public static final int TYPE_OTHER = 7;
    
    public static final int TYPE_PROFILE = 3;
    
    public static final int TYPE_WORK = 5;
    
    public static final String URL = "data1";
  }
  
  class SipAddress implements DataColumnsWithJoins, CommonDataKinds.CommonColumns, ContactCounts {
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/sip_address";
    
    public static final String SIP_ADDRESS = "data1";
    
    public static final int TYPE_HOME = 1;
    
    public static final int TYPE_OTHER = 3;
    
    public static final int TYPE_WORK = 2;
    
    public static final int getTypeLabelResource(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return 17041300; 
          return 17041302;
        } 
        return 17041303;
      } 
      return 17041301;
    }
    
    public static final CharSequence getTypeLabel(Resources param1Resources, int param1Int, CharSequence param1CharSequence) {
      if (param1Int == 0 && !TextUtils.isEmpty(param1CharSequence))
        return param1CharSequence; 
      param1Int = getTypeLabelResource(param1Int);
      return param1Resources.getText(param1Int);
    }
  }
  
  class Identity implements DataColumnsWithJoins, ContactCounts {
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/identity";
    
    public static final String IDENTITY = "data1";
    
    public static final String NAMESPACE = "data2";
  }
  
  class Callable implements DataColumnsWithJoins, CommonDataKinds.CommonColumns, ContactCounts {
    public static final Uri CONTENT_FILTER_URI;
    
    public static final Uri CONTENT_URI;
    
    static {
      Uri uri = Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, "callables");
      CONTENT_FILTER_URI = Uri.withAppendedPath(uri, "filter");
    }
    
    public static final Uri ENTERPRISE_CONTENT_FILTER_URI = Uri.withAppendedPath(CONTENT_URI, "filter_enterprise");
  }
  
  class Contactables implements DataColumnsWithJoins, CommonDataKinds.CommonColumns, ContactCounts {
    public static final Uri CONTENT_FILTER_URI;
    
    public static final Uri CONTENT_URI;
    
    public static final String VISIBLE_CONTACTS_ONLY = "visible_contacts_only";
    
    static {
      Uri uri = Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, "contactables");
      CONTENT_FILTER_URI = Uri.withAppendedPath(uri, "filter");
    }
  }
  
  class Groups implements BaseColumns, GroupsColumns, SyncColumns {
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/group";
    
    public static final Uri CONTENT_SUMMARY_URI = Uri.withAppendedPath(ContactsContract.AUTHORITY_URI, "groups_summary");
    
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/group";
    
    public static final Uri CONTENT_URI = Uri.withAppendedPath(ContactsContract.AUTHORITY_URI, "groups");
    
    static {
    
    }
    
    public static EntityIterator newEntityIterator(Cursor param1Cursor) {
      return (EntityIterator)new EntityIteratorImpl(param1Cursor);
    }
    
    private static class EntityIteratorImpl extends CursorEntityIterator {
      public EntityIteratorImpl(Cursor param2Cursor) {
        super(param2Cursor);
      }
      
      public android.content.Entity getEntityAndIncrementCursor(Cursor param2Cursor) throws RemoteException {
        ContentValues contentValues = new ContentValues();
        DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues, "_id");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "account_name");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "account_type");
        DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues, "dirty");
        DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues, "version");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "sourceid");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "res_package");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "title");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "title_res");
        DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues, "group_visible");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "sync1");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "sync2");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "sync3");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "sync4");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "system_id");
        DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues, "deleted");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "notes");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "should_sync");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "favorites");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "auto_add");
        param2Cursor.moveToNext();
        return new android.content.Entity(contentValues);
      }
    }
  }
  
  class AggregationExceptions implements BaseColumns {
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/aggregation_exception";
    
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/aggregation_exception";
    
    public static final Uri CONTENT_URI;
    
    public static final String RAW_CONTACT_ID1 = "raw_contact_id1";
    
    public static final String RAW_CONTACT_ID2 = "raw_contact_id2";
    
    public static final String TYPE = "type";
    
    public static final int TYPE_AUTOMATIC = 0;
    
    public static final int TYPE_KEEP_SEPARATE = 2;
    
    public static final int TYPE_KEEP_TOGETHER = 1;
    
    static {
      Uri uri = ContactsContract.AUTHORITY_URI;
      CONTENT_URI = Uri.withAppendedPath(uri, "aggregation_exceptions");
    }
  }
  
  class Settings implements SettingsColumns {
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/setting";
    
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/setting";
    
    public static final Uri CONTENT_URI;
    
    static {
      Uri uri = ContactsContract.AUTHORITY_URI;
      CONTENT_URI = Uri.withAppendedPath(uri, "settings");
    }
  }
  
  public static final class ProviderStatus {
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/provider_status";
    
    public static final Uri CONTENT_URI;
    
    public static final String DATABASE_CREATION_TIMESTAMP = "database_creation_timestamp";
    
    public static final String STATUS = "status";
    
    public static final int STATUS_BUSY = 1;
    
    public static final int STATUS_EMPTY = 2;
    
    public static final int STATUS_NORMAL = 0;
    
    static {
      Uri uri = ContactsContract.AUTHORITY_URI;
      CONTENT_URI = Uri.withAppendedPath(uri, "provider_status");
    }
  }
  
  @Deprecated
  public static final class DataUsageFeedback {
    public static final Uri DELETE_USAGE_URI;
    
    public static final Uri FEEDBACK_URI;
    
    public static final String USAGE_TYPE = "type";
    
    public static final String USAGE_TYPE_CALL = "call";
    
    public static final String USAGE_TYPE_LONG_TEXT = "long_text";
    
    public static final String USAGE_TYPE_SHORT_TEXT = "short_text";
    
    static {
      Uri uri = ContactsContract.Data.CONTENT_URI;
      FEEDBACK_URI = Uri.withAppendedPath(uri, "usagefeedback");
      uri = ContactsContract.Contacts.CONTENT_URI;
      DELETE_USAGE_URI = Uri.withAppendedPath(uri, "delete_usage");
    }
  }
  
  public static final class PinnedPositions {
    public static final int DEMOTED = -1;
    
    public static final String UNDEMOTE_METHOD = "undemote";
    
    public static final int UNPINNED = 0;
    
    public static void undemote(ContentResolver param1ContentResolver, long param1Long) {
      Uri uri = ContactsContract.AUTHORITY_URI;
      param1ContentResolver.call(uri, "undemote", String.valueOf(param1Long), null);
    }
    
    public static void pin(ContentResolver param1ContentResolver, long param1Long, int param1Int) {
      Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(param1Long));
      ContentValues contentValues = new ContentValues();
      contentValues.put("pinned", Integer.valueOf(param1Int));
      param1ContentResolver.update(uri, contentValues, null, null);
    }
  }
  
  public static final class QuickContact {
    public static final String ACTION_QUICK_CONTACT = "android.provider.action.QUICK_CONTACT";
    
    public static final String EXTRA_EXCLUDE_MIMES = "android.provider.extra.EXCLUDE_MIMES";
    
    public static final String EXTRA_MODE = "android.provider.extra.MODE";
    
    public static final String EXTRA_PRIORITIZED_MIMETYPE = "android.provider.extra.PRIORITIZED_MIMETYPE";
    
    @Deprecated
    public static final String EXTRA_TARGET_RECT = "android.provider.extra.TARGET_RECT";
    
    public static final int MODE_DEFAULT = 3;
    
    public static final int MODE_LARGE = 3;
    
    public static final int MODE_MEDIUM = 2;
    
    public static final int MODE_SMALL = 1;
    
    public static Intent composeQuickContactsIntent(Context param1Context, View param1View, Uri param1Uri, int param1Int, String[] param1ArrayOfString) {
      float f = (param1Context.getResources().getCompatibilityInfo()).applicationScale;
      int[] arrayOfInt = new int[2];
      param1View.getLocationOnScreen(arrayOfInt);
      Rect rect = new Rect();
      rect.left = (int)(arrayOfInt[0] * f + 0.5F);
      rect.top = (int)(arrayOfInt[1] * f + 0.5F);
      rect.right = (int)((arrayOfInt[0] + param1View.getWidth()) * f + 0.5F);
      rect.bottom = (int)((arrayOfInt[1] + param1View.getHeight()) * f + 0.5F);
      return composeQuickContactsIntent(param1Context, rect, param1Uri, param1Int, param1ArrayOfString);
    }
    
    public static Intent composeQuickContactsIntent(Context param1Context, Rect param1Rect, Uri param1Uri, int param1Int, String[] param1ArrayOfString) {
      int i;
      while (param1Context instanceof ContextWrapper && !(param1Context instanceof android.app.Activity))
        param1Context = ((ContextWrapper)param1Context).getBaseContext(); 
      if (param1Context instanceof android.app.Activity) {
        i = 0;
      } else {
        i = 268468224;
      } 
      Intent intent = (new Intent("android.provider.action.QUICK_CONTACT")).addFlags(i | 0x20000000);
      intent.setData(param1Uri);
      intent.setSourceBounds(param1Rect);
      intent.putExtra("android.provider.extra.MODE", param1Int);
      intent.putExtra("android.provider.extra.EXCLUDE_MIMES", param1ArrayOfString);
      return intent;
    }
    
    public static Intent rebuildManagedQuickContactsIntent(String param1String, long param1Long1, boolean param1Boolean, long param1Long2, Intent param1Intent) {
      Intent intent = new Intent("android.provider.action.QUICK_CONTACT");
      Uri uri2 = null;
      if (!TextUtils.isEmpty(param1String)) {
        Uri uri;
        if (param1Boolean) {
          uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, param1String);
        } else {
          uri = ContactsContract.Contacts.getLookupUri(param1Long1, (String)uri);
        } 
        uri2 = uri;
      } 
      Uri uri1 = uri2;
      if (uri2 != null) {
        uri1 = uri2;
        if (param1Long2 != 0L) {
          Uri.Builder builder = uri2.buildUpon();
          builder = builder.appendQueryParameter("directory", String.valueOf(param1Long2));
          uri1 = builder.build();
        } 
      } 
      intent.setData(uri1);
      intent.setFlags(param1Intent.getFlags() | 0x10000000);
      intent.setSourceBounds(param1Intent.getSourceBounds());
      intent.putExtra("android.provider.extra.MODE", param1Intent.getIntExtra("android.provider.extra.MODE", 3));
      String[] arrayOfString = param1Intent.getStringArrayExtra("android.provider.extra.EXCLUDE_MIMES");
      intent.putExtra("android.provider.extra.EXCLUDE_MIMES", arrayOfString);
      return intent;
    }
    
    public static void showQuickContact(Context param1Context, View param1View, Uri param1Uri, int param1Int, String[] param1ArrayOfString) {
      Intent intent = composeQuickContactsIntent(param1Context, param1View, param1Uri, param1Int, param1ArrayOfString);
      ContactsInternal.startQuickContactWithErrorToast(param1Context, intent);
    }
    
    public static void showQuickContact(Context param1Context, Rect param1Rect, Uri param1Uri, int param1Int, String[] param1ArrayOfString) {
      Intent intent = composeQuickContactsIntent(param1Context, param1Rect, param1Uri, param1Int, param1ArrayOfString);
      ContactsInternal.startQuickContactWithErrorToast(param1Context, intent);
    }
    
    public static void showQuickContact(Context param1Context, View param1View, Uri param1Uri, String[] param1ArrayOfString, String param1String) {
      Intent intent = composeQuickContactsIntent(param1Context, param1View, param1Uri, 3, param1ArrayOfString);
      intent.putExtra("android.provider.extra.PRIORITIZED_MIMETYPE", param1String);
      ContactsInternal.startQuickContactWithErrorToast(param1Context, intent);
    }
    
    public static void showQuickContact(Context param1Context, Rect param1Rect, Uri param1Uri, String[] param1ArrayOfString, String param1String) {
      Intent intent = composeQuickContactsIntent(param1Context, param1Rect, param1Uri, 3, param1ArrayOfString);
      intent.putExtra("android.provider.extra.PRIORITIZED_MIMETYPE", param1String);
      ContactsInternal.startQuickContactWithErrorToast(param1Context, intent);
    }
  }
  
  public static final class DisplayPhoto {
    public static final Uri CONTENT_MAX_DIMENSIONS_URI;
    
    public static final Uri CONTENT_URI = Uri.withAppendedPath(ContactsContract.AUTHORITY_URI, "display_photo");
    
    public static final String DISPLAY_MAX_DIM = "display_max_dim";
    
    public static final String THUMBNAIL_MAX_DIM = "thumbnail_max_dim";
    
    static {
      Uri uri = ContactsContract.AUTHORITY_URI;
      CONTENT_MAX_DIMENSIONS_URI = Uri.withAppendedPath(uri, "photo_dimensions");
    }
  }
  
  public static final class Intents {
    public static final String ACTION_GET_MULTIPLE_PHONES = "com.android.contacts.action.GET_MULTIPLE_PHONES";
    
    public static final String ACTION_PROFILE_CHANGED = "android.provider.Contacts.PROFILE_CHANGED";
    
    public static final String ACTION_VOICE_SEND_MESSAGE_TO_CONTACTS = "android.provider.action.VOICE_SEND_MESSAGE_TO_CONTACTS";
    
    public static final String ATTACH_IMAGE = "com.android.contacts.action.ATTACH_IMAGE";
    
    public static final String CONTACTS_DATABASE_CREATED = "android.provider.Contacts.DATABASE_CREATED";
    
    public static final String EXTRA_CREATE_DESCRIPTION = "com.android.contacts.action.CREATE_DESCRIPTION";
    
    @Deprecated
    public static final String EXTRA_EXCLUDE_MIMES = "exclude_mimes";
    
    public static final String EXTRA_FORCE_CREATE = "com.android.contacts.action.FORCE_CREATE";
    
    @Deprecated
    public static final String EXTRA_MODE = "mode";
    
    public static final String EXTRA_PHONE_URIS = "com.android.contacts.extra.PHONE_URIS";
    
    public static final String EXTRA_RECIPIENT_CONTACT_CHAT_ID = "android.provider.extra.RECIPIENT_CONTACT_CHAT_ID";
    
    public static final String EXTRA_RECIPIENT_CONTACT_NAME = "android.provider.extra.RECIPIENT_CONTACT_NAME";
    
    public static final String EXTRA_RECIPIENT_CONTACT_URI = "android.provider.extra.RECIPIENT_CONTACT_URI";
    
    @Deprecated
    public static final String EXTRA_TARGET_RECT = "target_rect";
    
    public static final String INVITE_CONTACT = "com.android.contacts.action.INVITE_CONTACT";
    
    public static final String METADATA_ACCOUNT_TYPE = "android.provider.account_type";
    
    public static final String METADATA_MIMETYPE = "android.provider.mimetype";
    
    @Deprecated
    public static final int MODE_LARGE = 3;
    
    @Deprecated
    public static final int MODE_MEDIUM = 2;
    
    @Deprecated
    public static final int MODE_SMALL = 1;
    
    public static final String SEARCH_SUGGESTION_CLICKED = "android.provider.Contacts.SEARCH_SUGGESTION_CLICKED";
    
    public static final String SEARCH_SUGGESTION_CREATE_CONTACT_CLICKED = "android.provider.Contacts.SEARCH_SUGGESTION_CREATE_CONTACT_CLICKED";
    
    public static final String SEARCH_SUGGESTION_DIAL_NUMBER_CLICKED = "android.provider.Contacts.SEARCH_SUGGESTION_DIAL_NUMBER_CLICKED";
    
    public static final String SHOW_OR_CREATE_CONTACT = "com.android.contacts.action.SHOW_OR_CREATE_CONTACT";
    
    public static final class Insert {
      public static final String ACTION = "android.intent.action.INSERT";
      
      public static final String COMPANY = "company";
      
      public static final String DATA = "data";
      
      public static final String EMAIL = "email";
      
      public static final String EMAIL_ISPRIMARY = "email_isprimary";
      
      public static final String EMAIL_TYPE = "email_type";
      
      public static final String EXTRA_ACCOUNT = "android.provider.extra.ACCOUNT";
      
      public static final String EXTRA_DATA_SET = "android.provider.extra.DATA_SET";
      
      public static final String FULL_MODE = "full_mode";
      
      public static final String IM_HANDLE = "im_handle";
      
      public static final String IM_ISPRIMARY = "im_isprimary";
      
      public static final String IM_PROTOCOL = "im_protocol";
      
      public static final String JOB_TITLE = "job_title";
      
      public static final String NAME = "name";
      
      public static final String NOTES = "notes";
      
      public static final String PHONE = "phone";
      
      public static final String PHONETIC_NAME = "phonetic_name";
      
      public static final String PHONE_ISPRIMARY = "phone_isprimary";
      
      public static final String PHONE_TYPE = "phone_type";
      
      public static final String POSTAL = "postal";
      
      public static final String POSTAL_ISPRIMARY = "postal_isprimary";
      
      public static final String POSTAL_TYPE = "postal_type";
      
      public static final String SECONDARY_EMAIL = "secondary_email";
      
      public static final String SECONDARY_EMAIL_TYPE = "secondary_email_type";
      
      public static final String SECONDARY_PHONE = "secondary_phone";
      
      public static final String SECONDARY_PHONE_TYPE = "secondary_phone_type";
      
      public static final String TERTIARY_EMAIL = "tertiary_email";
      
      public static final String TERTIARY_EMAIL_TYPE = "tertiary_email_type";
      
      public static final String TERTIARY_PHONE = "tertiary_phone";
      
      public static final String TERTIARY_PHONE_TYPE = "tertiary_phone_type";
    }
  }
  
  public static final class Insert {
    public static final String ACTION = "android.intent.action.INSERT";
    
    public static final String COMPANY = "company";
    
    public static final String DATA = "data";
    
    public static final String EMAIL = "email";
    
    public static final String EMAIL_ISPRIMARY = "email_isprimary";
    
    public static final String EMAIL_TYPE = "email_type";
    
    public static final String EXTRA_ACCOUNT = "android.provider.extra.ACCOUNT";
    
    public static final String EXTRA_DATA_SET = "android.provider.extra.DATA_SET";
    
    public static final String FULL_MODE = "full_mode";
    
    public static final String IM_HANDLE = "im_handle";
    
    public static final String IM_ISPRIMARY = "im_isprimary";
    
    public static final String IM_PROTOCOL = "im_protocol";
    
    public static final String JOB_TITLE = "job_title";
    
    public static final String NAME = "name";
    
    public static final String NOTES = "notes";
    
    public static final String PHONE = "phone";
    
    public static final String PHONETIC_NAME = "phonetic_name";
    
    public static final String PHONE_ISPRIMARY = "phone_isprimary";
    
    public static final String PHONE_TYPE = "phone_type";
    
    public static final String POSTAL = "postal";
    
    public static final String POSTAL_ISPRIMARY = "postal_isprimary";
    
    public static final String POSTAL_TYPE = "postal_type";
    
    public static final String SECONDARY_EMAIL = "secondary_email";
    
    public static final String SECONDARY_EMAIL_TYPE = "secondary_email_type";
    
    public static final String SECONDARY_PHONE = "secondary_phone";
    
    public static final String SECONDARY_PHONE_TYPE = "secondary_phone_type";
    
    public static final String TERTIARY_EMAIL = "tertiary_email";
    
    public static final String TERTIARY_EMAIL_TYPE = "tertiary_email_type";
    
    public static final String TERTIARY_PHONE = "tertiary_phone";
    
    public static final String TERTIARY_PHONE_TYPE = "tertiary_phone_type";
  }
  
  @SystemApi
  @Deprecated
  class MetadataSync implements BaseColumns, MetadataSyncColumns {
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/contact_metadata";
    
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/contact_metadata";
    
    public static final Uri CONTENT_URI;
    
    public static final String METADATA_AUTHORITY = "com.android.contacts.metadata";
    
    public static final Uri METADATA_AUTHORITY_URI;
    
    static {
      Uri uri = Uri.parse("content://com.android.contacts.metadata");
      CONTENT_URI = Uri.withAppendedPath(uri, "metadata_sync");
    }
  }
  
  @SystemApi
  @Deprecated
  class MetadataSyncState implements BaseColumns, MetadataSyncStateColumns {
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/contact_metadata_sync_state";
    
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/contact_metadata_sync_state";
    
    public static final Uri CONTENT_URI;
    
    static {
      Uri uri = ContactsContract.MetadataSync.METADATA_AUTHORITY_URI;
      CONTENT_URI = Uri.withAppendedPath(uri, "metadata_sync_state");
    }
  }
  
  protected static interface BaseSyncColumns {
    public static final String SYNC1 = "sync1";
    
    public static final String SYNC2 = "sync2";
    
    public static final String SYNC3 = "sync3";
    
    public static final String SYNC4 = "sync4";
  }
  
  public static interface BaseTypes {
    public static final int TYPE_CUSTOM = 0;
  }
  
  class CommonColumns implements CommonDataKinds.BaseTypes {
    public static final String DATA = "data1";
    
    public static final String LABEL = "data3";
    
    public static final String TYPE = "data2";
  }
  
  static interface ContactCounts {
    public static final String EXTRA_ADDRESS_BOOK_INDEX = "android.provider.extra.ADDRESS_BOOK_INDEX";
    
    public static final String EXTRA_ADDRESS_BOOK_INDEX_COUNTS = "android.provider.extra.ADDRESS_BOOK_INDEX_COUNTS";
    
    public static final String EXTRA_ADDRESS_BOOK_INDEX_TITLES = "android.provider.extra.ADDRESS_BOOK_INDEX_TITLES";
  }
  
  protected static interface ContactNameColumns {
    public static final String DISPLAY_NAME_ALTERNATIVE = "display_name_alt";
    
    public static final String DISPLAY_NAME_PRIMARY = "display_name";
    
    public static final String DISPLAY_NAME_SOURCE = "display_name_source";
    
    public static final String PHONETIC_NAME = "phonetic_name";
    
    public static final String PHONETIC_NAME_STYLE = "phonetic_name_style";
    
    public static final String SORT_KEY_ALTERNATIVE = "sort_key_alt";
    
    public static final String SORT_KEY_PRIMARY = "sort_key";
  }
  
  protected static interface ContactOptionsColumns {
    public static final String CUSTOM_RINGTONE = "custom_ringtone";
    
    @Deprecated
    public static final String LAST_TIME_CONTACTED = "last_time_contacted";
    
    public static final String LR_LAST_TIME_CONTACTED = "last_time_contacted";
    
    public static final String LR_TIMES_CONTACTED = "times_contacted";
    
    public static final String PINNED = "pinned";
    
    public static final String RAW_LAST_TIME_CONTACTED = "x_last_time_contacted";
    
    public static final String RAW_TIMES_CONTACTED = "x_times_contacted";
    
    public static final String SEND_TO_VOICEMAIL = "send_to_voicemail";
    
    public static final String STARRED = "starred";
    
    @Deprecated
    public static final String TIMES_CONTACTED = "times_contacted";
  }
  
  protected static interface ContactStatusColumns {
    public static final String CONTACT_CHAT_CAPABILITY = "contact_chat_capability";
    
    public static final String CONTACT_PRESENCE = "contact_presence";
    
    public static final String CONTACT_STATUS = "contact_status";
    
    public static final String CONTACT_STATUS_ICON = "contact_status_icon";
    
    public static final String CONTACT_STATUS_LABEL = "contact_status_label";
    
    public static final String CONTACT_STATUS_RES_PACKAGE = "contact_status_res_package";
    
    public static final String CONTACT_STATUS_TIMESTAMP = "contact_status_ts";
  }
  
  protected static interface ContactsColumns {
    public static final String CONTACT_LAST_UPDATED_TIMESTAMP = "contact_last_updated_timestamp";
    
    public static final String DISPLAY_NAME = "display_name";
    
    public static final String HAS_PHONE_NUMBER = "has_phone_number";
    
    public static final String IN_DEFAULT_DIRECTORY = "in_default_directory";
    
    public static final String IN_VISIBLE_GROUP = "in_visible_group";
    
    public static final String IS_USER_PROFILE = "is_user_profile";
    
    public static final String LOOKUP_KEY = "lookup";
    
    public static final String NAME_RAW_CONTACT_ID = "name_raw_contact_id";
    
    public static final String PHOTO_FILE_ID = "photo_file_id";
    
    public static final String PHOTO_ID = "photo_id";
    
    public static final String PHOTO_THUMBNAIL_URI = "photo_thumb_uri";
    
    public static final String PHOTO_URI = "photo_uri";
  }
  
  protected static interface DataColumns {
    public static final String CARRIER_PRESENCE = "carrier_presence";
    
    public static final int CARRIER_PRESENCE_VT_CAPABLE = 1;
    
    public static final String DATA1 = "data1";
    
    public static final String DATA10 = "data10";
    
    public static final String DATA11 = "data11";
    
    public static final String DATA12 = "data12";
    
    public static final String DATA13 = "data13";
    
    public static final String DATA14 = "data14";
    
    public static final String DATA15 = "data15";
    
    public static final String DATA2 = "data2";
    
    public static final String DATA3 = "data3";
    
    public static final String DATA4 = "data4";
    
    public static final String DATA5 = "data5";
    
    public static final String DATA6 = "data6";
    
    public static final String DATA7 = "data7";
    
    public static final String DATA8 = "data8";
    
    public static final String DATA9 = "data9";
    
    public static final String DATA_VERSION = "data_version";
    
    @Deprecated
    public static final String HASH_ID = "hash_id";
    
    public static final String IS_PRIMARY = "is_primary";
    
    public static final String IS_READ_ONLY = "is_read_only";
    
    public static final String IS_SUPER_PRIMARY = "is_super_primary";
    
    public static final String MIMETYPE = "mimetype";
    
    public static final String PREFERRED_PHONE_ACCOUNT_COMPONENT_NAME = "preferred_phone_account_component_name";
    
    public static final String PREFERRED_PHONE_ACCOUNT_ID = "preferred_phone_account_id";
    
    public static final String RAW_CONTACT_ID = "raw_contact_id";
    
    public static final String RES_PACKAGE = "res_package";
    
    public static final String SYNC1 = "data_sync1";
    
    public static final String SYNC2 = "data_sync2";
    
    public static final String SYNC3 = "data_sync3";
    
    public static final String SYNC4 = "data_sync4";
  }
  
  class DataColumnsWithJoins implements BaseColumns, DataColumns, StatusColumns, RawContactsColumns, ContactsColumns, ContactNameColumns, ContactOptionsColumns, ContactStatusColumns, DataUsageStatColumns {}
  
  protected static interface DataUsageStatColumns {
    @Deprecated
    public static final String LAST_TIME_USED = "last_time_used";
    
    public static final String LR_LAST_TIME_USED = "last_time_used";
    
    public static final String LR_TIMES_USED = "times_used";
    
    public static final String RAW_LAST_TIME_USED = "x_last_time_used";
    
    public static final String RAW_TIMES_USED = "x_times_used";
    
    @Deprecated
    public static final String TIMES_USED = "times_used";
  }
  
  protected static interface DeletedContactsColumns {
    public static final String CONTACT_DELETED_TIMESTAMP = "contact_deleted_timestamp";
    
    public static final String CONTACT_ID = "contact_id";
  }
  
  public static interface DisplayNameSources {
    public static final int EMAIL = 10;
    
    public static final int NICKNAME = 35;
    
    public static final int ORGANIZATION = 30;
    
    public static final int PHONE = 20;
    
    public static final int STRUCTURED_NAME = 40;
    
    public static final int STRUCTURED_PHONETIC_NAME = 37;
    
    public static final int UNDEFINED = 0;
  }
  
  public static interface FullNameStyle {
    public static final int CHINESE = 3;
    
    public static final int CJK = 2;
    
    public static final int JAPANESE = 4;
    
    public static final int KOREAN = 5;
    
    public static final int UNDEFINED = 0;
    
    public static final int WESTERN = 1;
  }
  
  protected static interface GroupsColumns {
    public static final String ACCOUNT_TYPE_AND_DATA_SET = "account_type_and_data_set";
    
    public static final String AUTO_ADD = "auto_add";
    
    public static final String DATA_SET = "data_set";
    
    public static final String DELETED = "deleted";
    
    public static final String FAVORITES = "favorites";
    
    public static final String GROUP_IS_READ_ONLY = "group_is_read_only";
    
    public static final String GROUP_VISIBLE = "group_visible";
    
    public static final String NOTES = "notes";
    
    public static final String PARAM_RETURN_GROUP_COUNT_PER_ACCOUNT = "return_group_count_per_account";
    
    public static final String RES_PACKAGE = "res_package";
    
    public static final String SHOULD_SYNC = "should_sync";
    
    public static final String SUMMARY_COUNT = "summ_count";
    
    public static final String SUMMARY_GROUP_COUNT_PER_ACCOUNT = "group_count_per_account";
    
    public static final String SUMMARY_WITH_PHONES = "summ_phones";
    
    public static final String SYSTEM_ID = "system_id";
    
    public static final String TITLE = "title";
    
    public static final String TITLE_RES = "title_res";
  }
  
  @SystemApi
  @Deprecated
  protected static interface MetadataSyncColumns {
    public static final String ACCOUNT_NAME = "account_name";
    
    public static final String ACCOUNT_TYPE = "account_type";
    
    public static final String DATA = "data";
    
    public static final String DATA_SET = "data_set";
    
    public static final String DELETED = "deleted";
    
    public static final String RAW_CONTACT_BACKUP_ID = "raw_contact_backup_id";
  }
  
  @SystemApi
  @Deprecated
  protected static interface MetadataSyncStateColumns {
    public static final String ACCOUNT_NAME = "account_name";
    
    public static final String ACCOUNT_TYPE = "account_type";
    
    public static final String DATA_SET = "data_set";
    
    public static final String STATE = "state";
  }
  
  protected static interface PhoneLookupColumns {
    public static final String CONTACT_ID = "contact_id";
    
    public static final String DATA_ID = "data_id";
    
    public static final String LABEL = "label";
    
    public static final String NORMALIZED_NUMBER = "normalized_number";
    
    public static final String NUMBER = "number";
    
    public static final String TYPE = "type";
  }
  
  public static interface PhoneticNameStyle {
    public static final int JAPANESE = 4;
    
    public static final int KOREAN = 5;
    
    public static final int PINYIN = 3;
    
    public static final int UNDEFINED = 0;
  }
  
  protected static interface PhotoFilesColumns {
    public static final String FILESIZE = "filesize";
    
    public static final String HEIGHT = "height";
    
    public static final String WIDTH = "width";
  }
  
  protected static interface PresenceColumns {
    public static final String CUSTOM_PROTOCOL = "custom_protocol";
    
    public static final String DATA_ID = "presence_data_id";
    
    public static final String IM_ACCOUNT = "im_account";
    
    public static final String IM_HANDLE = "im_handle";
    
    public static final String PROTOCOL = "protocol";
  }
  
  protected static interface RawContactsColumns {
    public static final String ACCOUNT_TYPE_AND_DATA_SET = "account_type_and_data_set";
    
    public static final String AGGREGATION_MODE = "aggregation_mode";
    
    public static final String BACKUP_ID = "backup_id";
    
    public static final String CONTACT_ID = "contact_id";
    
    public static final String DATA_SET = "data_set";
    
    public static final String DELETED = "deleted";
    
    @Deprecated
    public static final String METADATA_DIRTY = "metadata_dirty";
    
    public static final String RAW_CONTACT_IS_READ_ONLY = "raw_contact_is_read_only";
    
    public static final String RAW_CONTACT_IS_USER_PROFILE = "raw_contact_is_user_profile";
  }
  
  protected static interface SettingsColumns {
    public static final String ACCOUNT_NAME = "account_name";
    
    public static final String ACCOUNT_TYPE = "account_type";
    
    public static final String ANY_UNSYNCED = "any_unsynced";
    
    public static final String DATA_SET = "data_set";
    
    public static final String SHOULD_SYNC = "should_sync";
    
    public static final String UNGROUPED_COUNT = "summ_count";
    
    public static final String UNGROUPED_VISIBLE = "ungrouped_visible";
    
    public static final String UNGROUPED_WITH_PHONES = "summ_phones";
  }
  
  protected static interface StatusColumns {
    public static final int AVAILABLE = 5;
    
    public static final int AWAY = 2;
    
    public static final int CAPABILITY_HAS_CAMERA = 4;
    
    public static final int CAPABILITY_HAS_VIDEO = 2;
    
    public static final int CAPABILITY_HAS_VOICE = 1;
    
    public static final String CHAT_CAPABILITY = "chat_capability";
    
    public static final int DO_NOT_DISTURB = 4;
    
    public static final int IDLE = 3;
    
    public static final int INVISIBLE = 1;
    
    public static final int OFFLINE = 0;
    
    public static final String PRESENCE = "mode";
    
    @Deprecated
    public static final String PRESENCE_CUSTOM_STATUS = "status";
    
    @Deprecated
    public static final String PRESENCE_STATUS = "mode";
    
    public static final String STATUS = "status";
    
    public static final String STATUS_ICON = "status_icon";
    
    public static final String STATUS_LABEL = "status_label";
    
    public static final String STATUS_RES_PACKAGE = "status_res_package";
    
    public static final String STATUS_TIMESTAMP = "status_ts";
  }
  
  @Deprecated
  protected static interface StreamItemPhotosColumns {
    @Deprecated
    public static final String PHOTO_FILE_ID = "photo_file_id";
    
    @Deprecated
    public static final String PHOTO_URI = "photo_uri";
    
    @Deprecated
    public static final String SORT_INDEX = "sort_index";
    
    @Deprecated
    public static final String STREAM_ITEM_ID = "stream_item_id";
    
    @Deprecated
    public static final String SYNC1 = "stream_item_photo_sync1";
    
    @Deprecated
    public static final String SYNC2 = "stream_item_photo_sync2";
    
    @Deprecated
    public static final String SYNC3 = "stream_item_photo_sync3";
    
    @Deprecated
    public static final String SYNC4 = "stream_item_photo_sync4";
  }
  
  @Deprecated
  protected static interface StreamItemsColumns {
    @Deprecated
    public static final String ACCOUNT_NAME = "account_name";
    
    @Deprecated
    public static final String ACCOUNT_TYPE = "account_type";
    
    @Deprecated
    public static final String COMMENTS = "comments";
    
    @Deprecated
    public static final String CONTACT_ID = "contact_id";
    
    @Deprecated
    public static final String CONTACT_LOOKUP_KEY = "contact_lookup";
    
    @Deprecated
    public static final String DATA_SET = "data_set";
    
    @Deprecated
    public static final String RAW_CONTACT_ID = "raw_contact_id";
    
    @Deprecated
    public static final String RAW_CONTACT_SOURCE_ID = "raw_contact_source_id";
    
    @Deprecated
    public static final String RES_ICON = "icon";
    
    @Deprecated
    public static final String RES_LABEL = "label";
    
    @Deprecated
    public static final String RES_PACKAGE = "res_package";
    
    @Deprecated
    public static final String SYNC1 = "stream_item_sync1";
    
    @Deprecated
    public static final String SYNC2 = "stream_item_sync2";
    
    @Deprecated
    public static final String SYNC3 = "stream_item_sync3";
    
    @Deprecated
    public static final String SYNC4 = "stream_item_sync4";
    
    @Deprecated
    public static final String TEXT = "text";
    
    @Deprecated
    public static final String TIMESTAMP = "timestamp";
  }
  
  class SyncColumns implements BaseSyncColumns {
    public static final String ACCOUNT_NAME = "account_name";
    
    public static final String ACCOUNT_TYPE = "account_type";
    
    public static final String DIRTY = "dirty";
    
    public static final String SOURCE_ID = "sourceid";
    
    public static final String VERSION = "version";
  }
  
  @Deprecated
  class SyncStateColumns implements SyncStateContract.Columns {}
}
