package android.provider.oplus;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SqliteWrapper;
import android.net.Uri;
import android.provider.BaseColumns;
import android.telephony.Rlog;
import android.telephony.ServiceState;
import android.telephony.SmsMessage;
import android.telephony.SubscriptionManager;
import android.text.TextUtils;
import android.util.Patterns;
import com.android.internal.telephony.SmsApplication;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Telephony {
  private static final String TAG = "Telephony";
  
  class Sms implements BaseColumns, TextBasedSmsColumns {
    public static String getDefaultSmsPackage(Context param1Context) {
      ComponentName componentName = SmsApplication.getDefaultSmsApplication(param1Context, false);
      if (componentName != null)
        return componentName.getPackageName(); 
      return null;
    }
    
    public static Cursor query(ContentResolver param1ContentResolver, String[] param1ArrayOfString) {
      return param1ContentResolver.query(CONTENT_URI, param1ArrayOfString, null, null, "date DESC");
    }
    
    public static Cursor query(ContentResolver param1ContentResolver, String[] param1ArrayOfString, String param1String1, String param1String2) {
      Uri uri = CONTENT_URI;
      if (param1String2 == null)
        param1String2 = "date DESC"; 
      return param1ContentResolver.query(uri, param1ArrayOfString, param1String1, null, param1String2);
    }
    
    public static final Uri CONTENT_URI = Uri.parse("content://sms");
    
    public static final String DEFAULT_SORT_ORDER = "date DESC";
    
    public static Uri addMessageToUri(ContentResolver param1ContentResolver, Uri param1Uri, String param1String1, String param1String2, String param1String3, Long param1Long, boolean param1Boolean1, boolean param1Boolean2) {
      return addMessageToUri(SubscriptionManager.getDefaultSmsSubscriptionId(), param1ContentResolver, param1Uri, param1String1, param1String2, param1String3, param1Long, param1Boolean1, param1Boolean2, -1L);
    }
    
    public static Uri addMessageToUri(int param1Int, ContentResolver param1ContentResolver, Uri param1Uri, String param1String1, String param1String2, String param1String3, Long param1Long, boolean param1Boolean1, boolean param1Boolean2) {
      return addMessageToUri(param1Int, param1ContentResolver, param1Uri, param1String1, param1String2, param1String3, param1Long, param1Boolean1, param1Boolean2, -1L);
    }
    
    public static Uri addMessageToUri(ContentResolver param1ContentResolver, Uri param1Uri, String param1String1, String param1String2, String param1String3, Long param1Long, boolean param1Boolean1, boolean param1Boolean2, long param1Long1) {
      return addMessageToUri(SubscriptionManager.getDefaultSmsSubscriptionId(), param1ContentResolver, param1Uri, param1String1, param1String2, param1String3, param1Long, param1Boolean1, param1Boolean2, param1Long1);
    }
    
    public static Uri addMessageToUri(int param1Int, ContentResolver param1ContentResolver, Uri param1Uri, String param1String1, String param1String2, String param1String3, Long param1Long, boolean param1Boolean1, boolean param1Boolean2, long param1Long1) {
      return addMessageToUri(param1Int, param1ContentResolver, param1Uri, param1String1, param1String2, param1String3, param1Long, param1Boolean1, param1Boolean2, param1Long1, -1);
    }
    
    public static Uri addMessageToUri(int param1Int1, ContentResolver param1ContentResolver, Uri param1Uri, String param1String1, String param1String2, String param1String3, Long param1Long, boolean param1Boolean1, boolean param1Boolean2, long param1Long1, int param1Int2) {
      ContentValues contentValues = new ContentValues(8);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Telephony addMessageToUri sub id: ");
      stringBuilder.append(param1Int1);
      Rlog.v("Telephony", stringBuilder.toString());
      contentValues.put("sub_id", Integer.valueOf(param1Int1));
      contentValues.put("address", param1String1);
      if (param1Long != null)
        contentValues.put("date", param1Long); 
      if (param1Boolean1) {
        param1Int1 = 1;
      } else {
        param1Int1 = 0;
      } 
      contentValues.put("read", Integer.valueOf(param1Int1));
      contentValues.put("subject", param1String3);
      contentValues.put("body", param1String2);
      contentValues.put("priority", Integer.valueOf(param1Int2));
      if (param1Boolean2)
        contentValues.put("status", Integer.valueOf(32)); 
      if (param1Long1 != -1L)
        contentValues.put("thread_id", Long.valueOf(param1Long1)); 
      return param1ContentResolver.insert(param1Uri, contentValues);
    }
    
    public static boolean moveMessageToFolder(Context param1Context, Uri param1Uri, int param1Int1, int param1Int2) {
      boolean bool1 = false;
      if (param1Uri == null)
        return false; 
      boolean bool2 = false;
      boolean bool3 = false;
      switch (param1Int1) {
        default:
          return false;
        case 5:
        case 6:
          bool2 = true;
          break;
        case 2:
        case 4:
          bool3 = true;
          break;
        case 1:
        case 3:
          break;
      } 
      ContentValues contentValues = new ContentValues(3);
      contentValues.put("type", Integer.valueOf(param1Int1));
      if (bool2) {
        contentValues.put("read", Integer.valueOf(0));
      } else if (bool3) {
        contentValues.put("read", Integer.valueOf(1));
      } 
      contentValues.put("error_code", Integer.valueOf(param1Int2));
      if (1 == SqliteWrapper.update(param1Context, param1Context.getContentResolver(), param1Uri, contentValues, null, null))
        bool1 = true; 
      return bool1;
    }
    
    public static boolean isOutgoingFolder(int param1Int) {
      return (param1Int == 5 || param1Int == 4 || param1Int == 2 || param1Int == 6);
    }
    
    public static final class Inbox implements BaseColumns, Telephony.TextBasedSmsColumns {
      public static final Uri CONTENT_URI = Uri.parse("content://sms/inbox");
      
      public static final String DEFAULT_SORT_ORDER = "date DESC";
      
      public static Uri addMessage(ContentResolver param2ContentResolver, String param2String1, String param2String2, String param2String3, Long param2Long, boolean param2Boolean) {
        return Telephony.Sms.addMessageToUri(SubscriptionManager.getDefaultSmsSubscriptionId(), param2ContentResolver, CONTENT_URI, param2String1, param2String2, param2String3, param2Long, param2Boolean, false);
      }
      
      public static Uri addMessage(int param2Int, ContentResolver param2ContentResolver, String param2String1, String param2String2, String param2String3, Long param2Long, boolean param2Boolean) {
        return Telephony.Sms.addMessageToUri(param2Int, param2ContentResolver, CONTENT_URI, param2String1, param2String2, param2String3, param2Long, param2Boolean, false);
      }
    }
    
    public static final class Sent implements BaseColumns, Telephony.TextBasedSmsColumns {
      public static final Uri CONTENT_URI = Uri.parse("content://sms/sent");
      
      public static final String DEFAULT_SORT_ORDER = "date DESC";
      
      public static Uri addMessage(ContentResolver param2ContentResolver, String param2String1, String param2String2, String param2String3, Long param2Long) {
        return Telephony.Sms.addMessageToUri(SubscriptionManager.getDefaultSmsSubscriptionId(), param2ContentResolver, CONTENT_URI, param2String1, param2String2, param2String3, param2Long, true, false);
      }
      
      public static Uri addMessage(int param2Int, ContentResolver param2ContentResolver, String param2String1, String param2String2, String param2String3, Long param2Long) {
        return Telephony.Sms.addMessageToUri(param2Int, param2ContentResolver, CONTENT_URI, param2String1, param2String2, param2String3, param2Long, true, false);
      }
    }
    
    public static final class Draft implements BaseColumns, Telephony.TextBasedSmsColumns {
      public static final Uri CONTENT_URI = Uri.parse("content://sms/draft");
      
      public static final String DEFAULT_SORT_ORDER = "date DESC";
      
      public static Uri addMessage(ContentResolver param2ContentResolver, String param2String1, String param2String2, String param2String3, Long param2Long) {
        return Telephony.Sms.addMessageToUri(SubscriptionManager.getDefaultSmsSubscriptionId(), param2ContentResolver, CONTENT_URI, param2String1, param2String2, param2String3, param2Long, true, false);
      }
      
      public static Uri addMessage(int param2Int, ContentResolver param2ContentResolver, String param2String1, String param2String2, String param2String3, Long param2Long) {
        return Telephony.Sms.addMessageToUri(param2Int, param2ContentResolver, CONTENT_URI, param2String1, param2String2, param2String3, param2Long, true, false);
      }
    }
    
    public static final class Outbox implements BaseColumns, Telephony.TextBasedSmsColumns {
      public static final Uri CONTENT_URI = Uri.parse("content://sms/outbox");
      
      public static final String DEFAULT_SORT_ORDER = "date DESC";
      
      public static Uri addMessage(ContentResolver param2ContentResolver, String param2String1, String param2String2, String param2String3, Long param2Long, boolean param2Boolean, long param2Long1) {
        return Telephony.Sms.addMessageToUri(SubscriptionManager.getDefaultSmsSubscriptionId(), param2ContentResolver, CONTENT_URI, param2String1, param2String2, param2String3, param2Long, true, param2Boolean, param2Long1);
      }
      
      public static Uri addMessage(int param2Int, ContentResolver param2ContentResolver, String param2String1, String param2String2, String param2String3, Long param2Long, boolean param2Boolean, long param2Long1) {
        return Telephony.Sms.addMessageToUri(param2Int, param2ContentResolver, CONTENT_URI, param2String1, param2String2, param2String3, param2Long, true, param2Boolean, param2Long1);
      }
    }
    
    public static final class Conversations implements BaseColumns, Telephony.TextBasedSmsColumns {
      public static final Uri CONTENT_URI = Uri.parse("content://sms/conversations");
      
      public static final String DEFAULT_SORT_ORDER = "date DESC";
      
      public static final String MESSAGE_COUNT = "msg_count";
      
      public static final String SNIPPET = "snippet";
    }
    
    class Intents {
      public static final String ACTION_CHANGE_DEFAULT = "android.provider.Telephony.ACTION_CHANGE_DEFAULT";
      
      public static final String ACTION_DEFAULT_SMS_PACKAGE_CHANGED = "android.provider.action.DEFAULT_SMS_PACKAGE_CHANGED";
      
      public static final String ACTION_EXTERNAL_PROVIDER_CHANGE = "android.provider.action.EXTERNAL_PROVIDER_CHANGE";
      
      public static final String DATA_SMS_RECEIVED_ACTION = "android.intent.action.DATA_SMS_RECEIVED";
      
      public static final String EXTRA_IS_DEFAULT_SMS_APP = "android.provider.extra.IS_DEFAULT_SMS_APP";
      
      public static final String EXTRA_PACKAGE_NAME = "package";
      
      public static final String MMS_DOWNLOADED_ACTION = "android.provider.Telephony.MMS_DOWNLOADED";
      
      public static final int RESULT_SMS_DUPLICATED = 5;
      
      public static final int RESULT_SMS_GENERIC_ERROR = 2;
      
      public static final int RESULT_SMS_HANDLED = 1;
      
      public static final int RESULT_SMS_OUT_OF_MEMORY = 3;
      
      public static final int RESULT_SMS_UNSUPPORTED = 4;
      
      public static final String SIM_FULL_ACTION = "android.provider.Telephony.SIM_FULL";
      
      public static final String SMS_CARRIER_PROVISION_ACTION = "android.provider.Telephony.SMS_CARRIER_PROVISION";
      
      public static final String SMS_CB_RECEIVED_ACTION = "android.provider.Telephony.SMS_CB_RECEIVED";
      
      public static final String SMS_DELIVER_ACTION = "android.provider.Telephony.SMS_DELIVER";
      
      public static final String SMS_EMERGENCY_CB_RECEIVED_ACTION = "android.provider.Telephony.SMS_EMERGENCY_CB_RECEIVED";
      
      public static final String SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED";
      
      public static final String SMS_REJECTED_ACTION = "android.provider.Telephony.SMS_REJECTED";
      
      public static final String SMS_SERVICE_CATEGORY_PROGRAM_DATA_RECEIVED_ACTION = "android.provider.Telephony.SMS_SERVICE_CATEGORY_PROGRAM_DATA_RECEIVED";
      
      public static final String WAP_PUSH_DELIVER_ACTION = "android.provider.Telephony.WAP_PUSH_DELIVER";
      
      public static final String WAP_PUSH_RECEIVED_ACTION = "android.provider.Telephony.WAP_PUSH_RECEIVED";
      
      public static SmsMessage[] getMessagesFromIntent(Intent param2Intent) {
        try {
          Object[] arrayOfObject = (Object[])param2Intent.getSerializableExtra("pdus");
          if (arrayOfObject == null) {
            Rlog.e("Telephony", "pdus does not exist in the intent");
            return null;
          } 
          String str = param2Intent.getStringExtra("format");
          int i = SubscriptionManager.getDefaultSmsSubscriptionId();
          int j = param2Intent.getIntExtra("subscription", i);
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(" getMessagesFromIntent sub_id : ");
          stringBuilder.append(j);
          Rlog.v("Telephony", stringBuilder.toString());
          int k = arrayOfObject.length;
          SmsMessage[] arrayOfSmsMessage = new SmsMessage[k];
          for (i = 0; i < k; i++) {
            byte[] arrayOfByte = (byte[])arrayOfObject[i];
            arrayOfSmsMessage[i] = SmsMessage.createFromPdu(arrayOfByte, str);
            if (arrayOfSmsMessage[i] != null)
              arrayOfSmsMessage[i].setSubId(j); 
          } 
          return arrayOfSmsMessage;
        } catch (ClassCastException classCastException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("getMessagesFromIntent: ");
          stringBuilder.append(classCastException);
          Rlog.e("Telephony", stringBuilder.toString());
          return null;
        } 
      }
    }
  }
  
  public static final class Intents {
    public static final String ACTION_CHANGE_DEFAULT = "android.provider.Telephony.ACTION_CHANGE_DEFAULT";
    
    public static final String ACTION_DEFAULT_SMS_PACKAGE_CHANGED = "android.provider.action.DEFAULT_SMS_PACKAGE_CHANGED";
    
    public static final String ACTION_EXTERNAL_PROVIDER_CHANGE = "android.provider.action.EXTERNAL_PROVIDER_CHANGE";
    
    public static final String DATA_SMS_RECEIVED_ACTION = "android.intent.action.DATA_SMS_RECEIVED";
    
    public static final String EXTRA_IS_DEFAULT_SMS_APP = "android.provider.extra.IS_DEFAULT_SMS_APP";
    
    public static final String EXTRA_PACKAGE_NAME = "package";
    
    public static final String MMS_DOWNLOADED_ACTION = "android.provider.Telephony.MMS_DOWNLOADED";
    
    public static final int RESULT_SMS_DUPLICATED = 5;
    
    public static final int RESULT_SMS_GENERIC_ERROR = 2;
    
    public static final int RESULT_SMS_HANDLED = 1;
    
    public static final int RESULT_SMS_OUT_OF_MEMORY = 3;
    
    public static final int RESULT_SMS_UNSUPPORTED = 4;
    
    public static final String SIM_FULL_ACTION = "android.provider.Telephony.SIM_FULL";
    
    public static final String SMS_CARRIER_PROVISION_ACTION = "android.provider.Telephony.SMS_CARRIER_PROVISION";
    
    public static final String SMS_CB_RECEIVED_ACTION = "android.provider.Telephony.SMS_CB_RECEIVED";
    
    public static final String SMS_DELIVER_ACTION = "android.provider.Telephony.SMS_DELIVER";
    
    public static final String SMS_EMERGENCY_CB_RECEIVED_ACTION = "android.provider.Telephony.SMS_EMERGENCY_CB_RECEIVED";
    
    public static final String SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED";
    
    public static final String SMS_REJECTED_ACTION = "android.provider.Telephony.SMS_REJECTED";
    
    public static final String SMS_SERVICE_CATEGORY_PROGRAM_DATA_RECEIVED_ACTION = "android.provider.Telephony.SMS_SERVICE_CATEGORY_PROGRAM_DATA_RECEIVED";
    
    public static final String WAP_PUSH_DELIVER_ACTION = "android.provider.Telephony.WAP_PUSH_DELIVER";
    
    public static final String WAP_PUSH_RECEIVED_ACTION = "android.provider.Telephony.WAP_PUSH_RECEIVED";
    
    public static SmsMessage[] getMessagesFromIntent(Intent param1Intent) {
      try {
        Object[] arrayOfObject = (Object[])param1Intent.getSerializableExtra("pdus");
        if (arrayOfObject == null) {
          Rlog.e("Telephony", "pdus does not exist in the intent");
          return null;
        } 
        String str = param1Intent.getStringExtra("format");
        int i = SubscriptionManager.getDefaultSmsSubscriptionId();
        int j = param1Intent.getIntExtra("subscription", i);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" getMessagesFromIntent sub_id : ");
        stringBuilder.append(j);
        Rlog.v("Telephony", stringBuilder.toString());
        int k = arrayOfObject.length;
        SmsMessage[] arrayOfSmsMessage = new SmsMessage[k];
        for (i = 0; i < k; i++) {
          byte[] arrayOfByte = (byte[])arrayOfObject[i];
          arrayOfSmsMessage[i] = SmsMessage.createFromPdu(arrayOfByte, str);
          if (arrayOfSmsMessage[i] != null)
            arrayOfSmsMessage[i].setSubId(j); 
        } 
        return arrayOfSmsMessage;
      } catch (ClassCastException classCastException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("getMessagesFromIntent: ");
        stringBuilder.append(classCastException);
        Rlog.e("Telephony", stringBuilder.toString());
        return null;
      } 
    }
  }
  
  class Threads implements ThreadsColumns {
    public static final int BROADCAST_THREAD = 1;
    
    public static final int COMMON_THREAD = 0;
    
    public static final Uri CONTENT_URI;
    
    private static final String[] ID_PROJECTION = new String[] { "_id" };
    
    public static final Uri OBSOLETE_THREADS_URI;
    
    private static final Uri THREAD_ID_CONTENT_URI = Uri.parse("content://mms-sms/threadID");
    
    static {
      Uri uri = Uri.withAppendedPath(Telephony.MmsSms.CONTENT_URI, "conversations");
      OBSOLETE_THREADS_URI = Uri.withAppendedPath(uri, "obsolete");
    }
    
    public static long getOrCreateThreadId(Context param1Context, String param1String) {
      HashSet<String> hashSet = new HashSet();
      hashSet.add(param1String);
      return getOrCreateThreadId(param1Context, hashSet);
    }
    
    public static long getOrCreateThreadId(Context param1Context, Set<String> param1Set) {
      Uri.Builder builder = THREAD_ID_CONTENT_URI.buildUpon();
      for (String str1 : param1Set) {
        String str2 = str1;
        if (Telephony.Mms.isEmailAddress(str1))
          str2 = Telephony.Mms.extractAddrSpec(str1); 
        builder.appendQueryParameter("recipient", str2);
      } 
      Uri uri = builder.build();
      Cursor cursor = SqliteWrapper.query(param1Context, param1Context.getContentResolver(), uri, ID_PROJECTION, null, null, null);
      if (cursor != null)
        try {
          if (cursor.moveToFirst())
            return cursor.getLong(0); 
          Rlog.e("Telephony", "getOrCreateThreadId returned no rows!");
        } finally {
          cursor.close();
        }  
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getOrCreateThreadId failed with ");
      stringBuilder.append(param1Set.size());
      stringBuilder.append(" recipients");
      Rlog.e("Telephony", stringBuilder.toString());
      throw new IllegalArgumentException("Unable to find or allocate a thread ID.");
    }
  }
  
  class Mms implements BaseMmsColumns {
    public static final Uri CONTENT_URI;
    
    public static final String DEFAULT_SORT_ORDER = "date DESC";
    
    public static final Pattern NAME_ADDR_EMAIL_PATTERN;
    
    public static final Uri REPORT_REQUEST_URI;
    
    static {
      Uri uri = Uri.parse("content://mms");
      REPORT_REQUEST_URI = Uri.withAppendedPath(uri, "report-request");
    }
    
    public static final Uri REPORT_STATUS_URI = Uri.withAppendedPath(CONTENT_URI, "report-status");
    
    static {
      NAME_ADDR_EMAIL_PATTERN = Pattern.compile("\\s*(\"[^\"]*\"|[^<>\"]+)\\s*<([^<>]+)>\\s*");
    }
    
    public static Cursor query(ContentResolver param1ContentResolver, String[] param1ArrayOfString) {
      return param1ContentResolver.query(CONTENT_URI, param1ArrayOfString, null, null, "date DESC");
    }
    
    public static Cursor query(ContentResolver param1ContentResolver, String[] param1ArrayOfString, String param1String1, String param1String2) {
      Uri uri = CONTENT_URI;
      if (param1String2 == null)
        param1String2 = "date DESC"; 
      return param1ContentResolver.query(uri, param1ArrayOfString, param1String1, null, param1String2);
    }
    
    public static String extractAddrSpec(String param1String) {
      Matcher matcher = NAME_ADDR_EMAIL_PATTERN.matcher(param1String);
      if (matcher.matches())
        return matcher.group(2); 
      return param1String;
    }
    
    public static boolean isEmailAddress(String param1String) {
      if (TextUtils.isEmpty(param1String))
        return false; 
      param1String = extractAddrSpec(param1String);
      Matcher matcher = Patterns.EMAIL_ADDRESS.matcher(param1String);
      return matcher.matches();
    }
    
    public static boolean isPhoneNumber(String param1String) {
      if (TextUtils.isEmpty(param1String))
        return false; 
      Matcher matcher = Patterns.PHONE.matcher(param1String);
      return matcher.matches();
    }
    
    public static final class Inbox implements Telephony.BaseMmsColumns {
      public static final Uri CONTENT_URI = Uri.parse("content://mms/inbox");
      
      public static final String DEFAULT_SORT_ORDER = "date DESC";
    }
    
    public static final class Sent implements Telephony.BaseMmsColumns {
      public static final Uri CONTENT_URI = Uri.parse("content://mms/sent");
      
      public static final String DEFAULT_SORT_ORDER = "date DESC";
    }
    
    public static final class Draft implements Telephony.BaseMmsColumns {
      public static final Uri CONTENT_URI = Uri.parse("content://mms/drafts");
      
      public static final String DEFAULT_SORT_ORDER = "date DESC";
    }
    
    public static final class Outbox implements Telephony.BaseMmsColumns {
      public static final Uri CONTENT_URI = Uri.parse("content://mms/outbox");
      
      public static final String DEFAULT_SORT_ORDER = "date DESC";
    }
    
    class Addr implements BaseColumns {
      public static final String ADDRESS = "address";
      
      public static final String CHARSET = "charset";
      
      public static final String CONTACT_ID = "contact_id";
      
      public static final String MSG_ID = "msg_id";
      
      public static final String TYPE = "type";
    }
    
    class Part implements BaseColumns {
      public static final String CHARSET = "chset";
      
      public static final String CONTENT_DISPOSITION = "cd";
      
      public static final String CONTENT_ID = "cid";
      
      public static final String CONTENT_LOCATION = "cl";
      
      public static final String CONTENT_TYPE = "ct";
      
      public static final String CT_START = "ctt_s";
      
      public static final String CT_TYPE = "ctt_t";
      
      public static final String FILENAME = "fn";
      
      public static final String MSG_ID = "mid";
      
      public static final String NAME = "name";
      
      public static final String SEQ = "seq";
      
      public static final String TEXT = "text";
      
      public static final String _DATA = "_data";
    }
    
    class Rate {
      public static final Uri CONTENT_URI = Uri.withAppendedPath(Telephony.Mms.CONTENT_URI, "rate");
      
      public static final String SENT_TIME = "sent_time";
    }
    
    class Intents {
      public static final String CONTENT_CHANGED_ACTION = "android.intent.action.CONTENT_CHANGED";
      
      public static final String DELETED_CONTENTS = "deleted_contents";
    }
  }
  
  public static final class Rate {
    public static final Uri CONTENT_URI = Uri.withAppendedPath(Telephony.Mms.CONTENT_URI, "rate");
    
    public static final String SENT_TIME = "sent_time";
  }
  
  public static final class Intents {
    public static final String CONTENT_CHANGED_ACTION = "android.intent.action.CONTENT_CHANGED";
    
    public static final String DELETED_CONTENTS = "deleted_contents";
  }
  
  class MmsSms implements BaseColumns {
    public static final Uri CONTENT_CONVERSATIONS_URI = Uri.parse("content://mms-sms/conversations");
    
    public static final Uri CONTENT_DRAFT_URI;
    
    public static final Uri CONTENT_FILTER_BYPHONE_URI = Uri.parse("content://mms-sms/messages/byphone");
    
    public static final Uri CONTENT_LOCKED_URI;
    
    public static final Uri CONTENT_UNDELIVERED_URI = Uri.parse("content://mms-sms/undelivered");
    
    public static final Uri CONTENT_URI = Uri.parse("content://mms-sms/");
    
    public static final int ERR_TYPE_GENERIC = 1;
    
    public static final int ERR_TYPE_GENERIC_PERMANENT = 10;
    
    public static final int ERR_TYPE_MMS_PROTO_PERMANENT = 12;
    
    public static final int ERR_TYPE_MMS_PROTO_TRANSIENT = 3;
    
    public static final int ERR_TYPE_SMS_PROTO_PERMANENT = 11;
    
    public static final int ERR_TYPE_SMS_PROTO_TRANSIENT = 2;
    
    public static final int ERR_TYPE_TRANSPORT_FAILURE = 4;
    
    public static final int MMS_PROTO = 1;
    
    public static final int NO_ERROR = 0;
    
    public static final Uri SEARCH_URI;
    
    public static final int SMS_PROTO = 0;
    
    public static final String TYPE_DISCRIMINATOR_COLUMN = "transport_type";
    
    static {
      CONTENT_DRAFT_URI = Uri.parse("content://mms-sms/draft");
      CONTENT_LOCKED_URI = Uri.parse("content://mms-sms/locked");
      SEARCH_URI = Uri.parse("content://mms-sms/search");
    }
    
    public static final class PendingMessages implements BaseColumns {
      public static final Uri CONTENT_URI = Uri.withAppendedPath(Telephony.MmsSms.CONTENT_URI, "pending");
      
      public static final String DUE_TIME = "due_time";
      
      public static final String ERROR_CODE = "err_code";
      
      public static final String ERROR_TYPE = "err_type";
      
      public static final String LAST_TRY = "last_try";
      
      public static final String MSG_ID = "msg_id";
      
      public static final String MSG_TYPE = "msg_type";
      
      public static final String PROTO_TYPE = "proto_type";
      
      public static final String RETRY_INDEX = "retry_index";
      
      public static final String SUBSCRIPTION_ID = "pending_sub_id";
    }
    
    class WordsTable {
      public static final String ID = "_id";
      
      public static final String INDEXED_TEXT = "index_text";
      
      public static final String SOURCE_ROW_ID = "source_id";
      
      public static final String TABLE_ID = "table_to_use";
    }
  }
  
  public static final class WordsTable {
    public static final String ID = "_id";
    
    public static final String INDEXED_TEXT = "index_text";
    
    public static final String SOURCE_ROW_ID = "source_id";
    
    public static final String TABLE_ID = "table_to_use";
  }
  
  class Carriers implements BaseColumns {
    public static final String APN = "apn";
    
    public static final String AUTH_TYPE = "authtype";
    
    public static final String BEARER = "bearer";
    
    public static final String BEARER_BITMASK = "bearer_bitmask";
    
    public static final int CARRIER_DELETED = 5;
    
    public static final int CARRIER_DELETED_BUT_PRESENT_IN_XML = 6;
    
    public static final int CARRIER_EDITED = 4;
    
    public static final String CARRIER_ENABLED = "carrier_enabled";
    
    public static final Uri CONTENT_URI = Uri.parse("content://telephony/carriers");
    
    public static final String CURRENT = "current";
    
    public static final String DEFAULT_SORT_ORDER = "name ASC";
    
    public static final String EDITED = "edited";
    
    public static final String MAX_CONNS = "max_conns";
    
    public static final String MAX_CONNS_TIME = "max_conns_time";
    
    public static final String MCC = "mcc";
    
    public static final String MMSC = "mmsc";
    
    public static final String MMSPORT = "mmsport";
    
    public static final String MMSPROXY = "mmsproxy";
    
    public static final String MNC = "mnc";
    
    public static final String MODEM_COGNITIVE = "modem_cognitive";
    
    public static final String MTU = "mtu";
    
    public static final String MVNO_MATCH_DATA = "mvno_match_data";
    
    public static final String MVNO_TYPE = "mvno_type";
    
    public static final String NAME = "name";
    
    public static final String NUMERIC = "numeric";
    
    public static final String PASSWORD = "password";
    
    public static final String PORT = "port";
    
    public static final String PROFILE_ID = "profile_id";
    
    public static final String PROTOCOL = "protocol";
    
    public static final String PROXY = "proxy";
    
    public static final String ROAMING_PROTOCOL = "roaming_protocol";
    
    public static final String SERVER = "server";
    
    public static final String SOURCE_TYPE = "sourcetype";
    
    public static final String SUBSCRIPTION_ID = "sub_id";
    
    public static final String TYPE = "type";
    
    public static final int UNEDITED = 0;
    
    public static final String USER = "user";
    
    public static final int USER_DELETED = 2;
    
    public static final int USER_DELETED_BUT_PRESENT_IN_XML = 3;
    
    public static final int USER_EDITED = 1;
    
    public static final String USER_VISIBLE = "user_visible";
    
    public static final String WAIT_TIME = "wait_time";
  }
  
  class CellBroadcasts implements BaseColumns {
    public static final String CID = "cid";
    
    public static final String CMAS_CATEGORY = "cmas_category";
    
    public static final String CMAS_CERTAINTY = "cmas_certainty";
    
    public static final String CMAS_MESSAGE_CLASS = "cmas_message_class";
    
    public static final String CMAS_RESPONSE_TYPE = "cmas_response_type";
    
    public static final String CMAS_SEVERITY = "cmas_severity";
    
    public static final String CMAS_URGENCY = "cmas_urgency";
    
    public static final Uri CONTENT_URI = Uri.parse("content://cellbroadcasts");
    
    public static final String DEFAULT_SORT_ORDER = "date DESC";
    
    public static final String DELIVERY_TIME = "date";
    
    public static final String ETWS_WARNING_TYPE = "etws_warning_type";
    
    public static final String GEOGRAPHICAL_SCOPE = "geo_scope";
    
    public static final String LAC = "lac";
    
    public static final String LANGUAGE_CODE = "language";
    
    public static final String MESSAGE_BODY = "body";
    
    public static final String MESSAGE_FORMAT = "format";
    
    public static final String MESSAGE_PRIORITY = "priority";
    
    public static final String MESSAGE_READ = "read";
    
    public static final String PLMN = "plmn";
    
    public static final String[] QUERY_COLUMNS = new String[] { 
        "_id", "geo_scope", "plmn", "lac", "cid", "serial_number", "service_category", "language", "body", "date", 
        "read", "format", "priority", "etws_warning_type", "cmas_message_class", "cmas_category", "cmas_response_type", "cmas_severity", "cmas_urgency", "cmas_certainty" };
    
    public static final String SERIAL_NUMBER = "serial_number";
    
    public static final String SERVICE_CATEGORY = "service_category";
    
    public static final String V1_MESSAGE_CODE = "message_code";
    
    public static final String V1_MESSAGE_IDENTIFIER = "message_id";
  }
  
  public static final class ServiceStateTable {
    public static final String AUTHORITY = "service-state";
    
    public static final String CDMA_DEFAULT_ROAMING_INDICATOR = "cdma_default_roaming_indicator";
    
    public static final String CDMA_ERI_ICON_INDEX = "cdma_eri_icon_index";
    
    public static final String CDMA_ERI_ICON_MODE = "cdma_eri_icon_mode";
    
    public static final String CDMA_ROAMING_INDICATOR = "cdma_roaming_indicator";
    
    public static final Uri CONTENT_URI = Uri.parse("content://service-state/");
    
    public static final String CSS_INDICATOR = "css_indicator";
    
    public static final String DATA_OPERATOR_ALPHA_LONG = "data_operator_alpha_long";
    
    public static final String DATA_OPERATOR_ALPHA_SHORT = "data_operator_alpha_short";
    
    public static final String DATA_OPERATOR_NUMERIC = "data_operator_numeric";
    
    public static final String DATA_REG_STATE = "data_reg_state";
    
    public static final String DATA_ROAMING_TYPE = "data_roaming_type";
    
    public static final String IS_DATA_ROAMING_FROM_REGISTRATION = "is_data_roaming_from_registration";
    
    public static final String IS_EMERGENCY_ONLY = "is_emergency_only";
    
    public static final String IS_MANUAL_NETWORK_SELECTION = "is_manual_network_selection";
    
    public static final String IS_USING_CARRIER_AGGREGATION = "is_using_carrier_aggregation";
    
    public static final String NETWORK_ID = "network_id";
    
    public static final String RIL_DATA_RADIO_TECHNOLOGY = "ril_data_radio_technology";
    
    public static final String RIL_VOICE_RADIO_TECHNOLOGY = "ril_voice_radio_technology";
    
    public static final String SYSTEM_ID = "system_id";
    
    public static final String VOICE_OPERATOR_ALPHA_LONG = "voice_operator_alpha_long";
    
    public static final String VOICE_OPERATOR_ALPHA_SHORT = "voice_operator_alpha_short";
    
    public static final String VOICE_OPERATOR_NUMERIC = "voice_operator_numeric";
    
    public static final String VOICE_REG_STATE = "voice_reg_state";
    
    public static final String VOICE_ROAMING_TYPE = "voice_roaming_type";
    
    public static Uri getUriForSubscriptionIdAndField(int param1Int, String param1String) {
      Uri.Builder builder = CONTENT_URI.buildUpon().appendEncodedPath(String.valueOf(param1Int));
      return 
        builder.appendEncodedPath(param1String).build();
    }
    
    public static Uri getUriForSubscriptionId(int param1Int) {
      return CONTENT_URI.buildUpon().appendEncodedPath(String.valueOf(param1Int)).build();
    }
    
    public static ContentValues getContentValuesForServiceState(ServiceState param1ServiceState) {
      ContentValues contentValues = new ContentValues();
      contentValues.put("voice_reg_state", Integer.valueOf(param1ServiceState.getVoiceRegState()));
      contentValues.put("data_reg_state", Integer.valueOf(param1ServiceState.getDataRegState()));
      contentValues.put("voice_roaming_type", Integer.valueOf(param1ServiceState.getVoiceRoamingType()));
      contentValues.put("data_roaming_type", Integer.valueOf(param1ServiceState.getDataRoamingType()));
      contentValues.put("voice_operator_alpha_long", param1ServiceState.getVoiceOperatorAlphaLong());
      contentValues.put("voice_operator_alpha_short", param1ServiceState.getVoiceOperatorAlphaShort());
      contentValues.put("voice_operator_numeric", param1ServiceState.getVoiceOperatorNumeric());
      contentValues.put("data_operator_alpha_long", param1ServiceState.getOperatorAlphaLong());
      contentValues.put("data_operator_alpha_short", param1ServiceState.getDataOperatorAlphaShort());
      contentValues.put("data_operator_numeric", param1ServiceState.getDataOperatorNumeric());
      contentValues.put("is_manual_network_selection", Boolean.valueOf(param1ServiceState.getIsManualSelection()));
      contentValues.put("ril_voice_radio_technology", Integer.valueOf(param1ServiceState.getRilVoiceRadioTechnology()));
      contentValues.put("ril_data_radio_technology", Integer.valueOf(param1ServiceState.getRilDataRadioTechnology()));
      contentValues.put("css_indicator", Integer.valueOf(param1ServiceState.getCssIndicator()));
      contentValues.put("network_id", Integer.valueOf(param1ServiceState.getCdmaNetworkId()));
      contentValues.put("system_id", Integer.valueOf(param1ServiceState.getCdmaSystemId()));
      contentValues.put("cdma_roaming_indicator", Integer.valueOf(param1ServiceState.getCdmaRoamingIndicator()));
      contentValues.put("cdma_default_roaming_indicator", Integer.valueOf(param1ServiceState.getCdmaDefaultRoamingIndicator()));
      contentValues.put("cdma_eri_icon_index", Integer.valueOf(param1ServiceState.getCdmaEriIconIndex()));
      contentValues.put("cdma_eri_icon_mode", Integer.valueOf(param1ServiceState.getCdmaEriIconMode()));
      contentValues.put("is_emergency_only", Boolean.valueOf(param1ServiceState.isEmergencyOnly()));
      contentValues.put("is_data_roaming_from_registration", Boolean.valueOf(param1ServiceState.getDataRoamingFromRegistration()));
      contentValues.put("is_using_carrier_aggregation", Boolean.valueOf(param1ServiceState.isUsingCarrierAggregation()));
      return contentValues;
    }
  }
  
  public static final class ScrapSpace {
    public static final Uri CONTENT_URI = Uri.parse("content://mms/scrapSpace");
    
    public static final String SCRAP_FILE_PATH = "/sdcard/mms/scrapSpace/.temp.jpg";
  }
  
  class WapPush implements BaseColumns {
    public static final String ACTION = "action";
    
    public static final String ADDR = "address";
    
    public static final Uri CONTENT_URI = Uri.parse("content://wappush");
    
    public static final Uri CONTENT_URI_SI = Uri.parse("content://wappush/si");
    
    public static final Uri CONTENT_URI_SL = Uri.parse("content://wappush/sl");
    
    public static final Uri CONTENT_URI_THREAD = Uri.parse("content://wappush/thread_id");
    
    public static final String CREATE = "created";
    
    public static final String DATE = "date";
    
    public static final String DEFAULT_SORT_ORDER = "date ASC";
    
    public static final String ERROR = "error";
    
    public static final String EXPIRATION = "expiration";
    
    public static final String LOCKED = "locked";
    
    public static final String READ = "read";
    
    public static final String SEEN = "seen";
    
    public static final String SERVICE_ADDR = "service_center";
    
    public static final String SIID = "siid";
    
    public static final String SIM_ID = "sim_id";
    
    public static final int STATUS_LOCKED = 1;
    
    public static final int STATUS_READ = 1;
    
    public static final int STATUS_SEEN = 1;
    
    public static final int STATUS_UNLOCKED = 0;
    
    public static final int STATUS_UNREAD = 0;
    
    public static final int STATUS_UNSEEN = 0;
    
    public static final String TEXT = "text";
    
    public static final String THREAD_ID = "thread_id";
    
    public static final String TYPE = "type";
    
    public static final int TYPE_SI = 0;
    
    public static final int TYPE_SL = 1;
    
    public static final String URL = "url";
  }
  
  class BaseMmsColumns implements BaseColumns {
    @Deprecated
    public static final String ADAPTATION_ALLOWED = "adp_a";
    
    @Deprecated
    public static final String APPLIC_ID = "apl_id";
    
    @Deprecated
    public static final String AUX_APPLIC_ID = "aux_apl_id";
    
    @Deprecated
    public static final String CANCEL_ID = "cl_id";
    
    @Deprecated
    public static final String CANCEL_STATUS = "cl_st";
    
    public static final String CONTENT_CLASS = "ct_cls";
    
    public static final String CONTENT_LOCATION = "ct_l";
    
    public static final String CONTENT_TYPE = "ct_t";
    
    public static final String CREATOR = "creator";
    
    public static final String DATE = "date";
    
    public static final String DATE_SENT = "date_sent";
    
    public static final String DELIVERY_REPORT = "d_rpt";
    
    public static final String DELIVERY_TIME = "d_tm";
    
    @Deprecated
    public static final String DELIVERY_TIME_TOKEN = "d_tm_tok";
    
    @Deprecated
    public static final String DISTRIBUTION_INDICATOR = "d_ind";
    
    @Deprecated
    public static final String DRM_CONTENT = "drm_c";
    
    @Deprecated
    public static final String ELEMENT_DESCRIPTOR = "e_des";
    
    public static final String EXPIRY = "exp";
    
    @Deprecated
    public static final String LIMIT = "limit";
    
    public static final String LOCKED = "locked";
    
    @Deprecated
    public static final String MBOX_QUOTAS = "mb_qt";
    
    @Deprecated
    public static final String MBOX_QUOTAS_TOKEN = "mb_qt_tok";
    
    @Deprecated
    public static final String MBOX_TOTALS = "mb_t";
    
    @Deprecated
    public static final String MBOX_TOTALS_TOKEN = "mb_t_tok";
    
    public static final String MESSAGE_BOX = "msg_box";
    
    public static final int MESSAGE_BOX_ALL = 0;
    
    public static final int MESSAGE_BOX_DRAFTS = 3;
    
    public static final int MESSAGE_BOX_FAILED = 5;
    
    public static final int MESSAGE_BOX_INBOX = 1;
    
    public static final int MESSAGE_BOX_OUTBOX = 4;
    
    public static final int MESSAGE_BOX_SENT = 2;
    
    public static final String MESSAGE_CLASS = "m_cls";
    
    @Deprecated
    public static final String MESSAGE_COUNT = "m_cnt";
    
    public static final String MESSAGE_ID = "m_id";
    
    public static final String MESSAGE_SIZE = "m_size";
    
    public static final String MESSAGE_TYPE = "m_type";
    
    public static final String MMS_VERSION = "v";
    
    @Deprecated
    public static final String MM_FLAGS = "mm_flg";
    
    @Deprecated
    public static final String MM_FLAGS_TOKEN = "mm_flg_tok";
    
    @Deprecated
    public static final String MM_STATE = "mm_st";
    
    @Deprecated
    public static final String PREVIOUSLY_SENT_BY = "p_s_by";
    
    @Deprecated
    public static final String PREVIOUSLY_SENT_DATE = "p_s_d";
    
    public static final String PRIORITY = "pri";
    
    @Deprecated
    public static final String QUOTAS = "qt";
    
    public static final String READ = "read";
    
    public static final String READ_REPORT = "rr";
    
    public static final String READ_STATUS = "read_status";
    
    @Deprecated
    public static final String RECOMMENDED_RETRIEVAL_MODE = "r_r_mod";
    
    @Deprecated
    public static final String RECOMMENDED_RETRIEVAL_MODE_TEXT = "r_r_mod_txt";
    
    @Deprecated
    public static final String REPLACE_ID = "repl_id";
    
    @Deprecated
    public static final String REPLY_APPLIC_ID = "r_apl_id";
    
    @Deprecated
    public static final String REPLY_CHARGING = "r_chg";
    
    @Deprecated
    public static final String REPLY_CHARGING_DEADLINE = "r_chg_dl";
    
    @Deprecated
    public static final String REPLY_CHARGING_DEADLINE_TOKEN = "r_chg_dl_tok";
    
    @Deprecated
    public static final String REPLY_CHARGING_ID = "r_chg_id";
    
    @Deprecated
    public static final String REPLY_CHARGING_SIZE = "r_chg_sz";
    
    public static final String REPORT_ALLOWED = "rpt_a";
    
    public static final String RESPONSE_STATUS = "resp_st";
    
    public static final String RESPONSE_TEXT = "resp_txt";
    
    public static final String RETRIEVE_STATUS = "retr_st";
    
    public static final String RETRIEVE_TEXT = "retr_txt";
    
    public static final String RETRIEVE_TEXT_CHARSET = "retr_txt_cs";
    
    public static final String SEEN = "seen";
    
    @Deprecated
    public static final String SENDER_VISIBILITY = "s_vis";
    
    @Deprecated
    public static final String START = "start";
    
    public static final String STATUS = "st";
    
    @Deprecated
    public static final String STATUS_TEXT = "st_txt";
    
    @Deprecated
    public static final String STORE = "store";
    
    @Deprecated
    public static final String STORED = "stored";
    
    @Deprecated
    public static final String STORE_STATUS = "store_st";
    
    @Deprecated
    public static final String STORE_STATUS_TEXT = "store_st_txt";
    
    public static final String SUBJECT = "sub";
    
    public static final String SUBJECT_CHARSET = "sub_cs";
    
    public static final String SUBSCRIPTION_ID = "sub_id";
    
    public static final String TEXT_ONLY = "text_only";
    
    public static final String THREAD_ID = "thread_id";
    
    @Deprecated
    public static final String TOTALS = "totals";
    
    public static final String TRANSACTION_ID = "tr_id";
  }
  
  class CanonicalAddressesColumns implements BaseColumns {
    public static final String ADDRESS = "address";
  }
  
  public static interface TextBasedSmsColumns {
    public static final String ADDRESS = "address";
    
    public static final String BODY = "body";
    
    public static final String CREATOR = "creator";
    
    public static final String DATE = "date";
    
    public static final String DATE_SENT = "date_sent";
    
    public static final String ERROR_CODE = "error_code";
    
    public static final String GROUPADDRESS = "oppo_groupaddress";
    
    public static final String LOCKED = "locked";
    
    public static final String MASS = "oppo_mass";
    
    public static final int MESSAGE_TYPE_ALL = 0;
    
    public static final int MESSAGE_TYPE_DRAFT = 3;
    
    public static final int MESSAGE_TYPE_FAILED = 5;
    
    public static final int MESSAGE_TYPE_INBOX = 1;
    
    public static final int MESSAGE_TYPE_OUTBOX = 4;
    
    public static final int MESSAGE_TYPE_QUEUED = 6;
    
    public static final int MESSAGE_TYPE_SENT = 2;
    
    public static final String MSG_ID = "msgid";
    
    public static final String MTU = "mtu";
    
    public static final String PERSON = "person";
    
    public static final String PRIORITY = "priority";
    
    public static final String PROTOCOL = "protocol";
    
    public static final String READ = "read";
    
    public static final String REPLY_PATH_PRESENT = "reply_path_present";
    
    public static final String SEEN = "seen";
    
    public static final String SERVICE_CENTER = "service_center";
    
    public static final String STATUS = "status";
    
    public static final int STATUS_COMPLETE = 0;
    
    public static final int STATUS_FAILED = 64;
    
    public static final int STATUS_NONE = -1;
    
    public static final int STATUS_PENDING = 32;
    
    public static final String SUBJECT = "subject";
    
    public static final String SUBSCRIPTION_ID = "sub_id";
    
    public static final String THREAD_ID = "thread_id";
    
    public static final String TYPE = "type";
  }
  
  class ThreadsColumns implements BaseColumns {
    public static final String ARCHIVED = "archived";
    
    public static final String ATTACHMENT_INFO = "attachment_info";
    
    public static final String DATE = "date";
    
    public static final String ERROR = "error";
    
    public static final String HAS_ATTACHMENT = "has_attachment";
    
    public static final String MESSAGE_COUNT = "message_count";
    
    public static final String NOTIFICATION = "notification";
    
    public static final String READ = "read";
    
    public static final String RECIPIENT_IDS = "recipient_ids";
    
    public static final String SNIPPET = "snippet";
    
    public static final String SNIPPET_CHARSET = "snippet_cs";
    
    public static final String TYPE = "type";
  }
}
