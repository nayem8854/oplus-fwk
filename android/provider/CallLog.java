package android.provider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.UserInfo;
import android.database.Cursor;
import android.location.Country;
import android.location.CountryDetector;
import android.net.Uri;
import android.os.UserHandle;
import android.os.UserManager;
import android.telecom.CallerInfo;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;
import java.util.List;

public class CallLog {
  public static final String AUTHORITY = "call_log";
  
  public static final Uri CONTENT_URI = Uri.parse("content://call_log");
  
  private static final String LOG_TAG = "CallLog";
  
  public static final String SHADOW_AUTHORITY = "call_log_shadow";
  
  private static final boolean VERBOSE_LOG = false;
  
  class Calls implements BaseColumns {
    public static final String ADD_FOR_ALL_USERS = "add_for_all_users";
    
    public static final String ALLOW_VOICEMAILS_PARAM_KEY = "allow_voicemails";
    
    public static final int ANSWERED_EXTERNALLY_TYPE = 7;
    
    public static final int BLOCKED_TYPE = 6;
    
    public static final String BLOCK_REASON = "block_reason";
    
    public static final int BLOCK_REASON_BLOCKED_NUMBER = 3;
    
    public static final int BLOCK_REASON_CALL_SCREENING_SERVICE = 1;
    
    public static final int BLOCK_REASON_DIRECT_TO_VOICEMAIL = 2;
    
    public static final int BLOCK_REASON_NOT_BLOCKED = 0;
    
    public static final int BLOCK_REASON_NOT_IN_CONTACTS = 7;
    
    public static final int BLOCK_REASON_PAY_PHONE = 6;
    
    public static final int BLOCK_REASON_RESTRICTED_NUMBER = 5;
    
    public static final int BLOCK_REASON_UNKNOWN_NUMBER = 4;
    
    public static final String CACHED_FORMATTED_NUMBER = "formatted_number";
    
    public static final String CACHED_LOOKUP_URI = "lookup_uri";
    
    public static final String CACHED_MATCHED_NUMBER = "matched_number";
    
    public static final String CACHED_NAME = "name";
    
    public static final String CACHED_NORMALIZED_NUMBER = "normalized_number";
    
    public static final String CACHED_NUMBER_LABEL = "numberlabel";
    
    public static final String CACHED_NUMBER_TYPE = "numbertype";
    
    public static final String CACHED_PHOTO_ID = "photo_id";
    
    public static final String CACHED_PHOTO_URI = "photo_uri";
    
    public static final String CALL_SCREENING_APP_NAME = "call_screening_app_name";
    
    public static final String CALL_SCREENING_COMPONENT_NAME = "call_screening_component_name";
    
    public static final Uri CONTENT_FILTER_URI = Uri.parse("content://call_log/calls/filter");
    
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/calls";
    
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/calls";
    
    public static final Uri CONTENT_URI = Uri.parse("content://call_log/calls");
    
    private static final Uri CONTENT_URI_LIMIT_1;
    
    public static final Uri CONTENT_URI_WITH_VOICEMAIL;
    
    public static final String COUNTRY_ISO = "countryiso";
    
    public static final String DATA_USAGE = "data_usage";
    
    public static final String DATE = "date";
    
    public static final String DEFAULT_SORT_ORDER = "date DESC";
    
    public static final String DURATION = "duration";
    
    public static final String EXTRA_CALL_TYPE_FILTER = "android.provider.extra.CALL_TYPE_FILTER";
    
    public static final String FEATURES = "features";
    
    public static final int FEATURES_ASSISTED_DIALING_USED = 16;
    
    public static final int FEATURES_HD_CALL = 4;
    
    public static final int FEATURES_PULLED_EXTERNALLY = 2;
    
    public static final int FEATURES_RTT = 32;
    
    public static final int FEATURES_VIDEO = 1;
    
    public static final int FEATURES_VOLTE = 64;
    
    public static final int FEATURES_WIFI = 8;
    
    public static final String GEOCODED_LOCATION = "geocoded_location";
    
    public static final int INCOMING_TYPE = 1;
    
    public static final String IS_READ = "is_read";
    
    public static final String LAST_MODIFIED = "last_modified";
    
    public static final String LIMIT_PARAM_KEY = "limit";
    
    private static final int MIN_DURATION_FOR_NORMALIZED_NUMBER_UPDATE_MS = 10000;
    
    public static final int MISSED_TYPE = 3;
    
    public static final String NEW = "new";
    
    public static final String NUMBER = "number";
    
    public static final String NUMBER_PRESENTATION = "presentation";
    
    public static final String OFFSET_PARAM_KEY = "offset";
    
    public static final int OUTGOING_TYPE = 2;
    
    public static final String PHONE_ACCOUNT_ADDRESS = "phone_account_address";
    
    public static final String PHONE_ACCOUNT_COMPONENT_NAME = "subscription_component_name";
    
    public static final String PHONE_ACCOUNT_HIDDEN = "phone_account_hidden";
    
    public static final String PHONE_ACCOUNT_ID = "subscription_id";
    
    public static final String POST_DIAL_DIGITS = "post_dial_digits";
    
    public static final int PRESENTATION_ALLOWED = 1;
    
    public static final int PRESENTATION_PAYPHONE = 4;
    
    public static final int PRESENTATION_RESTRICTED = 2;
    
    public static final int PRESENTATION_UNKNOWN = 3;
    
    public static final int REJECTED_TYPE = 5;
    
    public static final Uri SHADOW_CONTENT_URI = Uri.parse("content://call_log_shadow/calls");
    
    public static final String SUB_ID = "sub_id";
    
    public static final String TRANSCRIPTION = "transcription";
    
    public static final String TRANSCRIPTION_STATE = "transcription_state";
    
    public static final String TYPE = "type";
    
    public static final String VIA_NUMBER = "via_number";
    
    public static final int VOICEMAIL_TYPE = 4;
    
    public static final String VOICEMAIL_URI = "voicemail_uri";
    
    static {
      Uri.Builder builder = CONTENT_URI.buildUpon();
      builder = builder.appendQueryParameter("limit", "1");
      CONTENT_URI_LIMIT_1 = builder.build();
      builder = CONTENT_URI.buildUpon();
      builder = builder.appendQueryParameter("allow_voicemails", "true");
      CONTENT_URI_WITH_VOICEMAIL = builder.build();
    }
    
    public static Uri addCall(CallerInfo param1CallerInfo, Context param1Context, String param1String, int param1Int1, int param1Int2, int param1Int3, PhoneAccountHandle param1PhoneAccountHandle, long param1Long, int param1Int4, Long param1Long1) {
      return addCall(param1CallerInfo, param1Context, param1String, "", "", param1Int1, param1Int2, param1Int3, param1PhoneAccountHandle, param1Long, param1Int4, param1Long1, false, null, false, 0, null, null);
    }
    
    public static Uri addCall(CallerInfo param1CallerInfo, Context param1Context, String param1String1, String param1String2, String param1String3, int param1Int1, int param1Int2, int param1Int3, PhoneAccountHandle param1PhoneAccountHandle, long param1Long, int param1Int4, Long param1Long1, boolean param1Boolean, UserHandle param1UserHandle) {
      return addCall(param1CallerInfo, param1Context, param1String1, param1String2, param1String3, param1Int1, param1Int2, param1Int3, param1PhoneAccountHandle, param1Long, param1Int4, param1Long1, param1Boolean, param1UserHandle, false, 0, null, null);
    }
    
    public static Uri addCall(CallerInfo param1CallerInfo, Context param1Context, String param1String1, String param1String2, String param1String3, int param1Int1, int param1Int2, int param1Int3, PhoneAccountHandle param1PhoneAccountHandle, long param1Long, int param1Int4, Long param1Long1, boolean param1Boolean1, UserHandle param1UserHandle, boolean param1Boolean2, int param1Int5, CharSequence param1CharSequence, String param1String4) {
      Uri uri;
      String str2;
      ContentResolver contentResolver = param1Context.getContentResolver();
      String str1 = getLogAccountAddress(param1Context, param1PhoneAccountHandle);
      param1Int1 = getLogNumberPresentation(param1String1, param1Int1);
      if (param1CallerInfo != null) {
        str2 = param1CallerInfo.getName();
      } else {
        str2 = "";
      } 
      if (param1Int1 != 1)
        if (param1CallerInfo != null) {
          param1String1 = "";
          str2 = "";
        } else {
          param1String1 = "";
        }  
      String str3 = null;
      if (param1PhoneAccountHandle != null) {
        str3 = param1PhoneAccountHandle.getComponentName().flattenToString();
        String str5 = param1PhoneAccountHandle.getId(), str4 = str3;
        str3 = str5;
      } else {
        param1PhoneAccountHandle = null;
      } 
      ContentValues contentValues = new ContentValues(6);
      contentValues.put("number", param1String1);
      contentValues.put("post_dial_digits", param1String2);
      contentValues.put("via_number", param1String3);
      contentValues.put("presentation", Integer.valueOf(param1Int1));
      contentValues.put("type", Integer.valueOf(param1Int2));
      contentValues.put("features", Integer.valueOf(param1Int3));
      contentValues.put("date", Long.valueOf(param1Long));
      contentValues.put("duration", Long.valueOf(param1Int4));
      if (param1Long1 != null)
        contentValues.put("data_usage", param1Long1); 
      contentValues.put("subscription_component_name", (String)param1PhoneAccountHandle);
      contentValues.put("subscription_id", str3);
      contentValues.put("phone_account_address", str1);
      contentValues.put("new", Integer.valueOf(1));
      contentValues.put("name", str2);
      contentValues.put("add_for_all_users", Integer.valueOf(param1Boolean1));
      if (param1Int2 == 3)
        contentValues.put("is_read", Integer.valueOf(param1Boolean2)); 
      contentValues.put("block_reason", Integer.valueOf(param1Int5));
      contentValues.put("call_screening_app_name", charSequenceToString(param1CharSequence));
      contentValues.put("call_screening_component_name", param1String4);
      if (param1CallerInfo != null && param1CallerInfo.getContactId() > 0L) {
        Cursor cursor;
        if (param1CallerInfo.normalizedNumber != null) {
          param1String3 = param1CallerInfo.normalizedNumber;
          Uri uri1 = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
          param1Long = param1CallerInfo.getContactId();
          cursor = contentResolver.query(uri1, new String[] { "_id" }, "contact_id =? AND data4 =?", new String[] { String.valueOf(param1Long), param1String3 }, null);
        } else {
          if (param1CallerInfo.getPhoneNumber() != null) {
            param1String2 = param1CallerInfo.getPhoneNumber();
          } else {
            param1String2 = param1String1;
          } 
          Uri uri2 = ContactsContract.CommonDataKinds.Callable.CONTENT_FILTER_URI;
          param1String2 = Uri.encode(param1String2);
          Uri uri1 = Uri.withAppendedPath(uri2, param1String2);
          param1Long = param1CallerInfo.getContactId();
          cursor = contentResolver.query(uri1, new String[] { "_id" }, "contact_id =?", new String[] { String.valueOf(param1Long) }, null);
        } 
        if (cursor != null)
          try {
            if (cursor.getCount() > 0 && cursor.moveToFirst()) {
              param1String3 = cursor.getString(0);
              updateDataUsageStatForData(contentResolver, param1String3);
              if (param1Int4 >= 10000 && param1Int2 == 2) {
                String str = param1CallerInfo.normalizedNumber;
                boolean bool = TextUtils.isEmpty(str);
                if (bool)
                  try {
                    updateNormalizedNumber(param1Context, contentResolver, param1String3, param1String1);
                  } finally {} 
              } 
            } 
          } finally {
            cursor.close();
          }  
      } 
      param1CallerInfo = null;
      UserManager userManager = (UserManager)param1Context.getSystemService(UserManager.class);
      param1Int2 = userManager.getUserHandle();
      if (param1Boolean1) {
        Uri uri1 = addEntryAndRemoveExpiredEntries(param1Context, userManager, UserHandle.SYSTEM, contentValues);
        if (uri1 == null || 
          "call_log_shadow".equals(uri1.getAuthority()))
          return null; 
        if (param1Int2 == 0)
          uri = uri1; 
        List<UserInfo> list = userManager.getUsers(true);
        param1Int3 = list.size();
        for (param1Int1 = 0; param1Int1 < param1Int3; param1Int1++) {
          UserInfo userInfo = list.get(param1Int1);
          UserHandle userHandle = userInfo.getUserHandle();
          param1Int4 = userHandle.getIdentifier();
          if (!userHandle.isSystem())
            if (shouldHaveSharedCallLogEntries(param1Context, userManager, param1Int4))
              if (userManager.isUserRunning(userHandle) && 
                userManager.isUserUnlocked(userHandle)) {
                Uri uri2 = addEntryAndRemoveExpiredEntries(param1Context, userManager, userHandle, contentValues);
                if (param1Int4 == param1Int2)
                  uri = uri2; 
              }   
        } 
      } else {
        if (param1UserHandle == null)
          param1UserHandle = UserHandle.of(param1Int2); 
        uri = addEntryAndRemoveExpiredEntries(param1Context, userManager, param1UserHandle, contentValues);
      } 
      return uri;
    }
    
    private static String charSequenceToString(CharSequence param1CharSequence) {
      if (param1CharSequence == null) {
        param1CharSequence = null;
      } else {
        param1CharSequence = param1CharSequence.toString();
      } 
      return (String)param1CharSequence;
    }
    
    public static boolean shouldHaveSharedCallLogEntries(Context param1Context, UserManager param1UserManager, int param1Int) {
      UserHandle userHandle = UserHandle.of(param1Int);
      boolean bool = param1UserManager.hasUserRestriction("no_outgoing_calls", userHandle);
      boolean bool1 = false;
      if (bool)
        return false; 
      UserInfo userInfo = param1UserManager.getUserInfo(param1Int);
      bool = bool1;
      if (userInfo != null) {
        bool = bool1;
        if (!userInfo.isManagedProfile())
          bool = true; 
      } 
      return bool;
    }
    
    public static String getLastOutgoingCall(Context param1Context) {
      Cursor cursor;
      null = param1Context.getContentResolver();
      param1Context = null;
      try {
        Cursor cursor1 = null.query(CONTENT_URI_LIMIT_1, new String[] { "number" }, "type = 2", null, "date DESC");
        if (cursor1 != null) {
          cursor = cursor1;
          if (cursor1.moveToFirst()) {
            cursor = cursor1;
            return cursor1.getString(0);
          } 
        } 
        return "";
      } finally {
        if (cursor != null)
          cursor.close(); 
      } 
    }
    
    private static Uri addEntryAndRemoveExpiredEntries(Context param1Context, UserManager param1UserManager, UserHandle param1UserHandle, ContentValues param1ContentValues) {
      ContentResolver contentResolver = param1Context.getContentResolver();
      if (param1UserManager.isUserUnlocked(param1UserHandle)) {
        uri = CONTENT_URI;
      } else {
        uri = SHADOW_CONTENT_URI;
      } 
      int i = param1UserHandle.getIdentifier();
      Uri uri = ContentProvider.maybeAddUserId(uri, i);
      try {
        Uri uri1 = contentResolver.insert(uri, param1ContentValues);
        if (uri1 != null) {
          String str = uri1.getLastPathSegment();
          if (str != null && str.equals("0")) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Failed to insert into call log due to appops denial; resultUri=");
            stringBuilder.append(uri1);
            Log.w("CallLog", stringBuilder.toString());
          } 
        } else {
          Log.w("CallLog", "Failed to insert into call log; null result uri.");
        } 
        if (param1ContentValues.containsKey("subscription_id") && 
          !TextUtils.isEmpty(param1ContentValues.getAsString("subscription_id")) && 
          param1ContentValues.containsKey("subscription_component_name") && 
          !TextUtils.isEmpty(param1ContentValues.getAsString("subscription_component_name"))) {
          String str1 = param1ContentValues.getAsString("subscription_component_name");
          String str2 = param1ContentValues.getAsString("subscription_id");
          contentResolver.delete(uri, "_id IN (SELECT _id FROM calls WHERE subscription_component_name = ? AND subscription_id = ? ORDER BY date DESC LIMIT -1 OFFSET 500)", new String[] { str1, str2 });
        } else {
          contentResolver.delete(uri, "_id IN (SELECT _id FROM calls ORDER BY date DESC LIMIT -1 OFFSET 500)", null);
        } 
        return uri1;
      } catch (IllegalArgumentException illegalArgumentException) {
        Log.w("CallLog", "Failed to insert calllog", illegalArgumentException);
        return null;
      } 
    }
    
    private static void updateDataUsageStatForData(ContentResolver param1ContentResolver, String param1String) {
      Uri.Builder builder2 = ContactsContract.DataUsageFeedback.FEEDBACK_URI.buildUpon();
      Uri.Builder builder1 = builder2.appendPath(param1String);
      builder1 = builder1.appendQueryParameter("type", "call");
      Uri uri = builder1.build();
      param1ContentResolver.update(uri, new ContentValues(), null, null);
    }
    
    private static void updateNormalizedNumber(Context param1Context, ContentResolver param1ContentResolver, String param1String1, String param1String2) {
      if (TextUtils.isEmpty(param1String2) || TextUtils.isEmpty(param1String1))
        return; 
      String str = getCurrentCountryIso(param1Context);
      if (TextUtils.isEmpty(str))
        return; 
      param1String2 = PhoneNumberUtils.formatNumberToE164(param1String2, str);
      if (TextUtils.isEmpty(param1String2))
        return; 
      ContentValues contentValues = new ContentValues();
      contentValues.put("data4", param1String2);
      param1ContentResolver.update(ContactsContract.Data.CONTENT_URI, contentValues, "_id=?", new String[] { param1String1 });
    }
    
    private static int getLogNumberPresentation(String param1String, int param1Int) {
      if (param1Int == 2)
        return param1Int; 
      if (param1Int == 4)
        return param1Int; 
      if (TextUtils.isEmpty(param1String) || param1Int == 3)
        return 3; 
      return 1;
    }
    
    private static String getLogAccountAddress(Context param1Context, PhoneAccountHandle param1PhoneAccountHandle) {
      String str;
      TelecomManager telecomManager = null;
      try {
        TelecomManager telecomManager1 = TelecomManager.from(param1Context);
      } catch (UnsupportedOperationException unsupportedOperationException) {}
      Context context = null;
      param1Context = context;
      if (telecomManager != null) {
        param1Context = context;
        if (param1PhoneAccountHandle != null) {
          PhoneAccount phoneAccount = telecomManager.getPhoneAccount(param1PhoneAccountHandle);
          param1Context = context;
          if (phoneAccount != null) {
            Uri uri = phoneAccount.getSubscriptionAddress();
            param1Context = context;
            if (uri != null)
              str = uri.getSchemeSpecificPart(); 
          } 
        } 
      } 
      return str;
    }
    
    private static String getCurrentCountryIso(Context param1Context) {
      String str;
      Context context = null;
      CountryDetector countryDetector = (CountryDetector)param1Context.getSystemService("country_detector");
      param1Context = context;
      if (countryDetector != null) {
        Country country = countryDetector.detectCountry();
        param1Context = context;
        if (country != null)
          str = country.getCountryIso(); 
      } 
      return str;
    }
  }
}
