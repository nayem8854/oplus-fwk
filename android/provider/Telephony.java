package android.provider;

import android.annotation.SystemApi;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SqliteWrapper;
import android.net.Uri;
import android.telephony.Rlog;
import android.telephony.SmsMessage;
import android.telephony.SubscriptionManager;
import android.text.TextUtils;
import android.util.Patterns;
import android.util.SeempLog;
import com.android.internal.telephony.SmsApplication;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Telephony {
  private static final String TAG = "Telephony";
  
  public static interface TextBasedSmsChangesColumns {
    public static final Uri CONTENT_URI = Uri.parse("content://sms-changes");
    
    public static final String ID = "_id";
    
    public static final String NEW_READ_STATUS = "new_read_status";
    
    public static final String ORIG_ROW_ID = "orig_rowid";
    
    public static final String SUB_ID = "sub_id";
    
    public static final String TYPE = "type";
    
    public static final int TYPE_DELETE = 1;
    
    public static final int TYPE_UPDATE = 0;
  }
  
  class Sms implements BaseColumns, TextBasedSmsColumns {
    public static String getDefaultSmsPackage(Context param1Context) {
      ComponentName componentName = SmsApplication.getDefaultSmsApplication(param1Context, false);
      if (componentName != null)
        return componentName.getPackageName(); 
      return null;
    }
    
    public static Cursor query(ContentResolver param1ContentResolver, String[] param1ArrayOfString) {
      SeempLog.record(10);
      return param1ContentResolver.query(CONTENT_URI, param1ArrayOfString, null, null, "date DESC");
    }
    
    public static Cursor query(ContentResolver param1ContentResolver, String[] param1ArrayOfString, String param1String1, String param1String2) {
      SeempLog.record(10);
      Uri uri = CONTENT_URI;
      if (param1String2 == null)
        param1String2 = "date DESC"; 
      return param1ContentResolver.query(uri, param1ArrayOfString, param1String1, null, param1String2);
    }
    
    public static final Uri CONTENT_URI = Uri.parse("content://sms");
    
    public static final String DEFAULT_SORT_ORDER = "date DESC";
    
    public static Uri addMessageToUri(ContentResolver param1ContentResolver, Uri param1Uri, String param1String1, String param1String2, String param1String3, Long param1Long, boolean param1Boolean1, boolean param1Boolean2) {
      return addMessageToUri(SubscriptionManager.getDefaultSmsSubscriptionId(), param1ContentResolver, param1Uri, param1String1, param1String2, param1String3, param1Long, param1Boolean1, param1Boolean2, -1L);
    }
    
    public static Uri addMessageToUri(int param1Int, ContentResolver param1ContentResolver, Uri param1Uri, String param1String1, String param1String2, String param1String3, Long param1Long, boolean param1Boolean1, boolean param1Boolean2) {
      return addMessageToUri(param1Int, param1ContentResolver, param1Uri, param1String1, param1String2, param1String3, param1Long, param1Boolean1, param1Boolean2, -1L);
    }
    
    public static Uri addMessageToUri(ContentResolver param1ContentResolver, Uri param1Uri, String param1String1, String param1String2, String param1String3, Long param1Long, boolean param1Boolean1, boolean param1Boolean2, long param1Long1) {
      return addMessageToUri(SubscriptionManager.getDefaultSmsSubscriptionId(), param1ContentResolver, param1Uri, param1String1, param1String2, param1String3, param1Long, param1Boolean1, param1Boolean2, param1Long1);
    }
    
    public static Uri addMessageToUri(int param1Int, ContentResolver param1ContentResolver, Uri param1Uri, String param1String1, String param1String2, String param1String3, Long param1Long, boolean param1Boolean1, boolean param1Boolean2, long param1Long1) {
      return addMessageToUri(param1Int, param1ContentResolver, param1Uri, param1String1, param1String2, param1String3, param1Long, param1Boolean1, param1Boolean2, param1Long1, -1);
    }
    
    public static Uri addMessageToUri(int param1Int1, ContentResolver param1ContentResolver, Uri param1Uri, String param1String1, String param1String2, String param1String3, Long param1Long, boolean param1Boolean1, boolean param1Boolean2, long param1Long1, int param1Int2) {
      ContentValues contentValues = new ContentValues(8);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Telephony addMessageToUri sub id: ");
      stringBuilder.append(param1Int1);
      Rlog.v("Telephony", stringBuilder.toString());
      contentValues.put("sub_id", Integer.valueOf(param1Int1));
      contentValues.put("address", param1String1);
      if (param1Long != null)
        contentValues.put("date", param1Long); 
      if (param1Boolean1) {
        param1Int1 = 1;
      } else {
        param1Int1 = 0;
      } 
      contentValues.put("read", Integer.valueOf(param1Int1));
      contentValues.put("subject", param1String3);
      contentValues.put("body", param1String2);
      contentValues.put("priority", Integer.valueOf(param1Int2));
      if (param1Boolean2)
        contentValues.put("status", Integer.valueOf(32)); 
      if (param1Long1 != -1L)
        contentValues.put("thread_id", Long.valueOf(param1Long1)); 
      return param1ContentResolver.insert(param1Uri, contentValues);
    }
    
    public static boolean moveMessageToFolder(Context param1Context, Uri param1Uri, int param1Int1, int param1Int2) {
      boolean bool1 = false;
      if (param1Uri == null)
        return false; 
      boolean bool2 = false;
      boolean bool3 = false;
      switch (param1Int1) {
        default:
          return false;
        case 5:
        case 6:
          bool2 = true;
          break;
        case 2:
        case 4:
          bool3 = true;
          break;
        case 1:
        case 3:
          break;
      } 
      ContentValues contentValues = new ContentValues(3);
      contentValues.put("type", Integer.valueOf(param1Int1));
      if (bool2) {
        contentValues.put("read", Integer.valueOf(0));
      } else if (bool3) {
        contentValues.put("read", Integer.valueOf(1));
      } 
      contentValues.put("error_code", Integer.valueOf(param1Int2));
      if (1 == SqliteWrapper.update(param1Context, param1Context.getContentResolver(), param1Uri, contentValues, null, null))
        bool1 = true; 
      return bool1;
    }
    
    public static boolean isOutgoingFolder(int param1Int) {
      return (param1Int == 5 || param1Int == 4 || param1Int == 2 || param1Int == 6);
    }
    
    public static final class Inbox implements BaseColumns, Telephony.TextBasedSmsColumns {
      public static final Uri CONTENT_URI = Uri.parse("content://sms/inbox");
      
      public static final String DEFAULT_SORT_ORDER = "date DESC";
      
      public static Uri addMessage(ContentResolver param2ContentResolver, String param2String1, String param2String2, String param2String3, Long param2Long, boolean param2Boolean) {
        return Telephony.Sms.addMessageToUri(SubscriptionManager.getDefaultSmsSubscriptionId(), param2ContentResolver, CONTENT_URI, param2String1, param2String2, param2String3, param2Long, param2Boolean, false);
      }
      
      public static Uri addMessage(int param2Int, ContentResolver param2ContentResolver, String param2String1, String param2String2, String param2String3, Long param2Long, boolean param2Boolean) {
        return Telephony.Sms.addMessageToUri(param2Int, param2ContentResolver, CONTENT_URI, param2String1, param2String2, param2String3, param2Long, param2Boolean, false);
      }
    }
    
    public static final class Sent implements BaseColumns, Telephony.TextBasedSmsColumns {
      public static final Uri CONTENT_URI = Uri.parse("content://sms/sent");
      
      public static final String DEFAULT_SORT_ORDER = "date DESC";
      
      public static Uri addMessage(ContentResolver param2ContentResolver, String param2String1, String param2String2, String param2String3, Long param2Long) {
        return Telephony.Sms.addMessageToUri(SubscriptionManager.getDefaultSmsSubscriptionId(), param2ContentResolver, CONTENT_URI, param2String1, param2String2, param2String3, param2Long, true, false);
      }
      
      public static Uri addMessage(int param2Int, ContentResolver param2ContentResolver, String param2String1, String param2String2, String param2String3, Long param2Long) {
        return Telephony.Sms.addMessageToUri(param2Int, param2ContentResolver, CONTENT_URI, param2String1, param2String2, param2String3, param2Long, true, false);
      }
    }
    
    public static final class Draft implements BaseColumns, Telephony.TextBasedSmsColumns {
      public static final Uri CONTENT_URI = Uri.parse("content://sms/draft");
      
      public static final String DEFAULT_SORT_ORDER = "date DESC";
      
      public static Uri addMessage(ContentResolver param2ContentResolver, String param2String1, String param2String2, String param2String3, Long param2Long) {
        return Telephony.Sms.addMessageToUri(SubscriptionManager.getDefaultSmsSubscriptionId(), param2ContentResolver, CONTENT_URI, param2String1, param2String2, param2String3, param2Long, true, false);
      }
      
      public static Uri addMessage(int param2Int, ContentResolver param2ContentResolver, String param2String1, String param2String2, String param2String3, Long param2Long) {
        return Telephony.Sms.addMessageToUri(param2Int, param2ContentResolver, CONTENT_URI, param2String1, param2String2, param2String3, param2Long, true, false);
      }
    }
    
    public static final class Outbox implements BaseColumns, Telephony.TextBasedSmsColumns {
      public static final Uri CONTENT_URI = Uri.parse("content://sms/outbox");
      
      public static final String DEFAULT_SORT_ORDER = "date DESC";
      
      public static Uri addMessage(ContentResolver param2ContentResolver, String param2String1, String param2String2, String param2String3, Long param2Long, boolean param2Boolean, long param2Long1) {
        return Telephony.Sms.addMessageToUri(SubscriptionManager.getDefaultSmsSubscriptionId(), param2ContentResolver, CONTENT_URI, param2String1, param2String2, param2String3, param2Long, true, param2Boolean, param2Long1);
      }
      
      public static Uri addMessage(int param2Int, ContentResolver param2ContentResolver, String param2String1, String param2String2, String param2String3, Long param2Long, boolean param2Boolean, long param2Long1) {
        return Telephony.Sms.addMessageToUri(param2Int, param2ContentResolver, CONTENT_URI, param2String1, param2String2, param2String3, param2Long, true, param2Boolean, param2Long1);
      }
    }
    
    public static final class Conversations implements BaseColumns, Telephony.TextBasedSmsColumns {
      public static final Uri CONTENT_URI = Uri.parse("content://sms/conversations");
      
      public static final String DEFAULT_SORT_ORDER = "date DESC";
      
      public static final String MESSAGE_COUNT = "msg_count";
      
      public static final String SNIPPET = "snippet";
    }
    
    class Intents {
      public static final String ACTION_CHANGE_DEFAULT = "android.provider.Telephony.ACTION_CHANGE_DEFAULT";
      
      public static final String ACTION_DEFAULT_SMS_PACKAGE_CHANGED = "android.provider.action.DEFAULT_SMS_PACKAGE_CHANGED";
      
      public static final String ACTION_EXTERNAL_PROVIDER_CHANGE = "android.provider.action.EXTERNAL_PROVIDER_CHANGE";
      
      @SystemApi
      public static final String ACTION_SMS_EMERGENCY_CB_RECEIVED = "android.provider.action.SMS_EMERGENCY_CB_RECEIVED";
      
      public static final String ACTION_SMS_MMS_DB_CREATED = "android.provider.action.SMS_MMS_DB_CREATED";
      
      public static final String ACTION_SMS_MMS_DB_LOST = "android.provider.action.SMS_MMS_DB_LOST";
      
      public static final String DATA_SMS_RECEIVED_ACTION = "android.intent.action.DATA_SMS_RECEIVED";
      
      public static final String EXTRA_IS_CORRUPTED = "android.provider.extra.IS_CORRUPTED";
      
      public static final String EXTRA_IS_DEFAULT_SMS_APP = "android.provider.extra.IS_DEFAULT_SMS_APP";
      
      public static final String EXTRA_IS_INITIAL_CREATE = "android.provider.extra.IS_INITIAL_CREATE";
      
      public static final String EXTRA_PACKAGE_NAME = "package";
      
      public static final String MMS_DOWNLOADED_ACTION = "android.provider.Telephony.MMS_DOWNLOADED";
      
      public static final int RESULT_SMS_DATABASE_ERROR = 10;
      
      public static final int RESULT_SMS_DISPATCH_FAILURE = 6;
      
      public static final int RESULT_SMS_DUPLICATED = 5;
      
      public static final int RESULT_SMS_GENERIC_ERROR = 2;
      
      public static final int RESULT_SMS_HANDLED = 1;
      
      public static final int RESULT_SMS_INVALID_URI = 11;
      
      public static final int RESULT_SMS_NULL_MESSAGE = 8;
      
      public static final int RESULT_SMS_NULL_PDU = 7;
      
      public static final int RESULT_SMS_OUT_OF_MEMORY = 3;
      
      public static final int RESULT_SMS_RECEIVED_WHILE_ENCRYPTED = 9;
      
      public static final int RESULT_SMS_UNSUPPORTED = 4;
      
      @Deprecated
      public static final String SECRET_CODE_ACTION = "android.provider.Telephony.SECRET_CODE";
      
      public static final String SIM_FULL_ACTION = "android.provider.Telephony.SIM_FULL";
      
      public static final String SMS_CARRIER_PROVISION_ACTION = "android.provider.Telephony.SMS_CARRIER_PROVISION";
      
      public static final String SMS_CB_RECEIVED_ACTION = "android.provider.Telephony.SMS_CB_RECEIVED";
      
      public static final String SMS_DELIVER_ACTION = "android.provider.Telephony.SMS_DELIVER";
      
      public static final String SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED";
      
      public static final String SMS_REJECTED_ACTION = "android.provider.Telephony.SMS_REJECTED";
      
      public static final String SMS_SERVICE_CATEGORY_PROGRAM_DATA_RECEIVED_ACTION = "android.provider.Telephony.SMS_SERVICE_CATEGORY_PROGRAM_DATA_RECEIVED";
      
      public static final String WAP_PUSH_DELIVER_ACTION = "android.provider.Telephony.WAP_PUSH_DELIVER";
      
      public static final String WAP_PUSH_RECEIVED_ACTION = "android.provider.Telephony.WAP_PUSH_RECEIVED";
      
      public static SmsMessage[] getMessagesFromIntent(Intent param2Intent) {
        try {
          Object[] arrayOfObject = (Object[])param2Intent.getSerializableExtra("pdus");
          if (arrayOfObject == null) {
            Rlog.e("Telephony", "pdus does not exist in the intent");
            return null;
          } 
          String str = param2Intent.getStringExtra("format");
          int i = SubscriptionManager.getDefaultSmsSubscriptionId();
          i = param2Intent.getIntExtra("android.telephony.extra.SUBSCRIPTION_INDEX", i);
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(" getMessagesFromIntent sub_id : ");
          stringBuilder.append(i);
          Rlog.v("Telephony", stringBuilder.toString());
          int j = arrayOfObject.length;
          SmsMessage[] arrayOfSmsMessage = new SmsMessage[j];
          for (i = 0; i < j; i++) {
            byte[] arrayOfByte = (byte[])arrayOfObject[i];
            arrayOfSmsMessage[i] = SmsMessage.createFromPdu(arrayOfByte, str);
          } 
          return arrayOfSmsMessage;
        } catch (ClassCastException classCastException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("getMessagesFromIntent: ");
          stringBuilder.append(classCastException);
          Rlog.e("Telephony", stringBuilder.toString());
          return null;
        } 
      }
    }
  }
  
  public static final class Intents {
    public static final String ACTION_CHANGE_DEFAULT = "android.provider.Telephony.ACTION_CHANGE_DEFAULT";
    
    public static final String ACTION_DEFAULT_SMS_PACKAGE_CHANGED = "android.provider.action.DEFAULT_SMS_PACKAGE_CHANGED";
    
    public static final String ACTION_EXTERNAL_PROVIDER_CHANGE = "android.provider.action.EXTERNAL_PROVIDER_CHANGE";
    
    @SystemApi
    public static final String ACTION_SMS_EMERGENCY_CB_RECEIVED = "android.provider.action.SMS_EMERGENCY_CB_RECEIVED";
    
    public static final String ACTION_SMS_MMS_DB_CREATED = "android.provider.action.SMS_MMS_DB_CREATED";
    
    public static final String ACTION_SMS_MMS_DB_LOST = "android.provider.action.SMS_MMS_DB_LOST";
    
    public static final String DATA_SMS_RECEIVED_ACTION = "android.intent.action.DATA_SMS_RECEIVED";
    
    public static final String EXTRA_IS_CORRUPTED = "android.provider.extra.IS_CORRUPTED";
    
    public static final String EXTRA_IS_DEFAULT_SMS_APP = "android.provider.extra.IS_DEFAULT_SMS_APP";
    
    public static final String EXTRA_IS_INITIAL_CREATE = "android.provider.extra.IS_INITIAL_CREATE";
    
    public static final String EXTRA_PACKAGE_NAME = "package";
    
    public static final String MMS_DOWNLOADED_ACTION = "android.provider.Telephony.MMS_DOWNLOADED";
    
    public static final int RESULT_SMS_DATABASE_ERROR = 10;
    
    public static final int RESULT_SMS_DISPATCH_FAILURE = 6;
    
    public static final int RESULT_SMS_DUPLICATED = 5;
    
    public static final int RESULT_SMS_GENERIC_ERROR = 2;
    
    public static final int RESULT_SMS_HANDLED = 1;
    
    public static final int RESULT_SMS_INVALID_URI = 11;
    
    public static final int RESULT_SMS_NULL_MESSAGE = 8;
    
    public static final int RESULT_SMS_NULL_PDU = 7;
    
    public static final int RESULT_SMS_OUT_OF_MEMORY = 3;
    
    public static final int RESULT_SMS_RECEIVED_WHILE_ENCRYPTED = 9;
    
    public static final int RESULT_SMS_UNSUPPORTED = 4;
    
    @Deprecated
    public static final String SECRET_CODE_ACTION = "android.provider.Telephony.SECRET_CODE";
    
    public static final String SIM_FULL_ACTION = "android.provider.Telephony.SIM_FULL";
    
    public static final String SMS_CARRIER_PROVISION_ACTION = "android.provider.Telephony.SMS_CARRIER_PROVISION";
    
    public static final String SMS_CB_RECEIVED_ACTION = "android.provider.Telephony.SMS_CB_RECEIVED";
    
    public static final String SMS_DELIVER_ACTION = "android.provider.Telephony.SMS_DELIVER";
    
    public static final String SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED";
    
    public static final String SMS_REJECTED_ACTION = "android.provider.Telephony.SMS_REJECTED";
    
    public static final String SMS_SERVICE_CATEGORY_PROGRAM_DATA_RECEIVED_ACTION = "android.provider.Telephony.SMS_SERVICE_CATEGORY_PROGRAM_DATA_RECEIVED";
    
    public static final String WAP_PUSH_DELIVER_ACTION = "android.provider.Telephony.WAP_PUSH_DELIVER";
    
    public static final String WAP_PUSH_RECEIVED_ACTION = "android.provider.Telephony.WAP_PUSH_RECEIVED";
    
    public static SmsMessage[] getMessagesFromIntent(Intent param1Intent) {
      try {
        Object[] arrayOfObject = (Object[])param1Intent.getSerializableExtra("pdus");
        if (arrayOfObject == null) {
          Rlog.e("Telephony", "pdus does not exist in the intent");
          return null;
        } 
        String str = param1Intent.getStringExtra("format");
        int i = SubscriptionManager.getDefaultSmsSubscriptionId();
        i = param1Intent.getIntExtra("android.telephony.extra.SUBSCRIPTION_INDEX", i);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" getMessagesFromIntent sub_id : ");
        stringBuilder.append(i);
        Rlog.v("Telephony", stringBuilder.toString());
        int j = arrayOfObject.length;
        SmsMessage[] arrayOfSmsMessage = new SmsMessage[j];
        for (i = 0; i < j; i++) {
          byte[] arrayOfByte = (byte[])arrayOfObject[i];
          arrayOfSmsMessage[i] = SmsMessage.createFromPdu(arrayOfByte, str);
        } 
        return arrayOfSmsMessage;
      } catch (ClassCastException classCastException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("getMessagesFromIntent: ");
        stringBuilder.append(classCastException);
        Rlog.e("Telephony", stringBuilder.toString());
        return null;
      } 
    }
  }
  
  class CarrierColumns implements BaseColumns {
    public static final Uri CONTENT_URI = Uri.parse("content://carrier_information/carrier");
    
    public static final String EXPIRATION_TIME = "expiration_time";
    
    public static final String KEY_IDENTIFIER = "key_identifier";
    
    public static final String KEY_TYPE = "key_type";
    
    public static final String LAST_MODIFIED = "last_modified";
    
    public static final String MCC = "mcc";
    
    public static final String MNC = "mnc";
    
    public static final String MVNO_MATCH_DATA = "mvno_match_data";
    
    public static final String MVNO_TYPE = "mvno_type";
    
    public static final String PUBLIC_KEY = "public_key";
  }
  
  class Threads implements ThreadsColumns {
    public static final int BROADCAST_THREAD = 1;
    
    public static final int COMMON_THREAD = 0;
    
    public static final Uri CONTENT_URI;
    
    private static final String[] ID_PROJECTION = new String[] { "_id" };
    
    public static final Uri OBSOLETE_THREADS_URI;
    
    private static final Uri THREAD_ID_CONTENT_URI = Uri.parse("content://mms-sms/threadID");
    
    static {
      Uri uri = Uri.withAppendedPath(Telephony.MmsSms.CONTENT_URI, "conversations");
      OBSOLETE_THREADS_URI = Uri.withAppendedPath(uri, "obsolete");
    }
    
    public static long getOrCreateThreadId(Context param1Context, String param1String) {
      HashSet<String> hashSet = new HashSet();
      hashSet.add(param1String);
      return getOrCreateThreadId(param1Context, hashSet);
    }
    
    public static long getOrCreateThreadId(Context param1Context, Set<String> param1Set) {
      Uri.Builder builder = THREAD_ID_CONTENT_URI.buildUpon();
      for (String str1 : param1Set) {
        String str2 = str1;
        if (Telephony.Mms.isEmailAddress(str1))
          str2 = Telephony.Mms.extractAddrSpec(str1); 
        builder.appendQueryParameter("recipient", str2);
      } 
      Uri uri = builder.build();
      Cursor cursor = SqliteWrapper.query(param1Context, param1Context.getContentResolver(), uri, ID_PROJECTION, null, null, null);
      if (cursor != null)
        try {
          if (cursor.moveToFirst())
            return cursor.getLong(0); 
          Rlog.e("Telephony", "getOrCreateThreadId returned no rows!");
        } finally {
          cursor.close();
        }  
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getOrCreateThreadId failed with ");
      stringBuilder.append(param1Set.size());
      stringBuilder.append(" recipients");
      Rlog.e("Telephony", stringBuilder.toString());
      throw new IllegalArgumentException("Unable to find or allocate a thread ID.");
    }
  }
  
  public static interface RcsColumns {
    public static final String AUTHORITY = "rcs";
    
    public static final Uri CONTENT_AND_AUTHORITY = Uri.parse("content://rcs");
    
    public static final boolean IS_RCS_TABLE_SCHEMA_CODE_COMPLETE = false;
    
    public static final long TIMESTAMP_NOT_SET = 0L;
    
    public static final int TRANSACTION_FAILED = -2147483648;
    
    public static interface RcsThreadColumns {
      public static final String RCS_THREAD_ID_COLUMN = "rcs_thread_id";
      
      public static final Uri RCS_THREAD_URI = Uri.withAppendedPath(Telephony.RcsColumns.CONTENT_AND_AUTHORITY, "thread");
      
      public static final String RCS_THREAD_URI_PART = "thread";
    }
    
    class Rcs1To1ThreadColumns implements RcsThreadColumns {
      public static final String FALLBACK_THREAD_ID_COLUMN = "rcs_fallback_thread_id";
      
      public static final Uri RCS_1_TO_1_THREAD_URI = Uri.withAppendedPath(Telephony.RcsColumns.CONTENT_AND_AUTHORITY, "p2p_thread");
      
      public static final String RCS_1_TO_1_THREAD_URI_PART = "p2p_thread";
    }
    
    class RcsGroupThreadColumns implements RcsThreadColumns {
      public static final String CONFERENCE_URI_COLUMN = "conference_uri";
      
      public static final String GROUP_ICON_COLUMN = "group_icon";
      
      public static final String GROUP_NAME_COLUMN = "group_name";
      
      public static final String OWNER_PARTICIPANT_COLUMN = "owner_participant";
      
      public static final Uri RCS_GROUP_THREAD_URI = Uri.withAppendedPath(Telephony.RcsColumns.CONTENT_AND_AUTHORITY, "group_thread");
      
      public static final String RCS_GROUP_THREAD_URI_PART = "group_thread";
    }
    
    public static interface RcsParticipantColumns {
      public static final String CANONICAL_ADDRESS_ID_COLUMN = "canonical_address_id";
      
      public static final String RCS_ALIAS_COLUMN = "rcs_alias";
      
      public static final String RCS_PARTICIPANT_ID_COLUMN = "rcs_participant_id";
      
      public static final Uri RCS_PARTICIPANT_URI = Uri.withAppendedPath(Telephony.RcsColumns.CONTENT_AND_AUTHORITY, "participant");
      
      public static final String RCS_PARTICIPANT_URI_PART = "participant";
    }
    
    class RcsIncomingMessageColumns implements RcsMessageColumns {
      public static final String ARRIVAL_TIMESTAMP_COLUMN = "arrival_timestamp";
      
      public static final Uri INCOMING_MESSAGE_URI = Uri.withAppendedPath(Telephony.RcsColumns.CONTENT_AND_AUTHORITY, "incoming_message");
      
      public static final String INCOMING_MESSAGE_URI_PART = "incoming_message";
      
      public static final String SEEN_TIMESTAMP_COLUMN = "seen_timestamp";
      
      public static final String SENDER_PARTICIPANT_ID_COLUMN = "sender_participant";
    }
    
    class RcsOutgoingMessageColumns implements RcsMessageColumns {
      public static final Uri OUTGOING_MESSAGE_URI = Uri.withAppendedPath(Telephony.RcsColumns.CONTENT_AND_AUTHORITY, "outgoing_message");
      
      public static final String OUTGOING_MESSAGE_URI_PART = "outgoing_message";
    }
    
    class RcsUnifiedMessageColumns implements RcsIncomingMessageColumns, RcsOutgoingMessageColumns {
      public static final String MESSAGE_TYPE_COLUMN = "message_type";
      
      public static final int MESSAGE_TYPE_INCOMING = 1;
      
      public static final int MESSAGE_TYPE_OUTGOING = 0;
      
      public static final String UNIFIED_INCOMING_MESSAGE_VIEW = "unified_incoming_message_view";
      
      public static final Uri UNIFIED_MESSAGE_URI = Uri.withAppendedPath(Telephony.RcsColumns.CONTENT_AND_AUTHORITY, "message");
      
      public static final String UNIFIED_MESSAGE_URI_PART = "message";
      
      public static final String UNIFIED_OUTGOING_MESSAGE_VIEW = "unified_outgoing_message_view";
    }
    
    public static interface RcsFileTransferColumns {
      public static final String CONTENT_TYPE_COLUMN = "content_type";
      
      public static final String CONTENT_URI_COLUMN = "content_uri";
      
      public static final String DURATION_MILLIS_COLUMN = "duration";
      
      public static final String FILE_SIZE_COLUMN = "file_size";
      
      public static final String FILE_TRANSFER_ID_COLUMN = "rcs_file_transfer_id";
      
      public static final Uri FILE_TRANSFER_URI = Uri.withAppendedPath(Telephony.RcsColumns.CONTENT_AND_AUTHORITY, "file_transfer");
      
      public static final String FILE_TRANSFER_URI_PART = "file_transfer";
      
      public static final String HEIGHT_COLUMN = "height";
      
      public static final String PREVIEW_TYPE_COLUMN = "preview_type";
      
      public static final String PREVIEW_URI_COLUMN = "preview_uri";
      
      public static final String SESSION_ID_COLUMN = "session_id";
      
      public static final String SUCCESSFULLY_TRANSFERRED_BYTES = "transfer_offset";
      
      public static final String TRANSFER_STATUS_COLUMN = "transfer_status";
      
      public static final String WIDTH_COLUMN = "width";
    }
    
    class RcsUnifiedEventHelper implements RcsParticipantEventColumns, RcsThreadEventColumns {
      public static final Uri RCS_EVENT_QUERY_URI = Uri.withAppendedPath(Telephony.RcsColumns.CONTENT_AND_AUTHORITY, "event");
      
      public static final String RCS_EVENT_QUERY_URI_PATH = "event";
    }
    
    public static interface RcsCanonicalAddressHelper {
      static long getOrCreateCanonicalAddressId(ContentResolver param2ContentResolver, String param2String) {
        Uri.Builder builder = Telephony.RcsColumns.CONTENT_AND_AUTHORITY.buildUpon();
        builder.appendPath("canonical-address");
        builder.appendQueryParameter("address", param2String);
        Uri uri = builder.build();
        Cursor cursor = param2ContentResolver.query(uri, null, null, null);
        if (cursor != null)
          try {
            if (cursor.moveToFirst()) {
              long l = cursor.getLong(cursor.getColumnIndex("_id"));
              if (cursor != null)
                cursor.close(); 
              return l;
            } 
          } finally {} 
        Rlog.e("Telephony", "getOrCreateCanonicalAddressId returned no rows");
        if (cursor != null)
          cursor.close(); 
        Rlog.e("Telephony", "getOrCreateCanonicalAddressId failed");
        throw new IllegalArgumentException("Unable to find or allocate a canonical address ID");
      }
    }
    
    public static interface RcsEventTypes {
      public static final int ICON_CHANGED_EVENT_TYPE = 8;
      
      public static final int NAME_CHANGED_EVENT_TYPE = 16;
      
      public static final int PARTICIPANT_ALIAS_CHANGED_EVENT_TYPE = 1;
      
      public static final int PARTICIPANT_JOINED_EVENT_TYPE = 2;
      
      public static final int PARTICIPANT_LEFT_EVENT_TYPE = 4;
    }
    
    public static interface RcsMessageColumns {
      public static final String GLOBAL_ID_COLUMN = "rcs_message_global_id";
      
      public static final String LATITUDE_COLUMN = "latitude";
      
      public static final String LONGITUDE_COLUMN = "longitude";
      
      public static final String MESSAGE_ID_COLUMN = "rcs_message_row_id";
      
      public static final String MESSAGE_TEXT_COLUMN = "rcs_text";
      
      public static final String MESSAGE_TYPE_COLUMN = "rcs_message_type";
      
      public static final String ORIGINATION_TIMESTAMP_COLUMN = "origination_timestamp";
      
      public static final String STATUS_COLUMN = "status";
      
      public static final String SUB_ID_COLUMN = "sub_id";
    }
    
    class RcsMessageDeliveryColumns implements RcsOutgoingMessageColumns {
      public static final String DELIVERED_TIMESTAMP_COLUMN = "delivered_timestamp";
      
      public static final String DELIVERY_URI_PART = "delivery";
      
      public static final String SEEN_TIMESTAMP_COLUMN = "seen_timestamp";
    }
    
    public static interface RcsParticipantEventColumns {
      public static final String ALIAS_CHANGE_EVENT_URI_PART = "alias_change_event";
      
      public static final String NEW_ALIAS_COLUMN = "new_alias";
    }
    
    class RcsParticipantHelpers implements RcsParticipantColumns {
      public static final String RCS_PARTICIPANT_WITH_ADDRESS_VIEW = "rcs_participant_with_address_view";
      
      public static final String RCS_PARTICIPANT_WITH_THREAD_VIEW = "rcs_participant_with_thread_view";
    }
    
    public static interface RcsThreadEventColumns {
      public static final String DESTINATION_PARTICIPANT_ID_COLUMN = "destination_participant";
      
      public static final String EVENT_ID_COLUMN = "event_id";
      
      public static final String EVENT_TYPE_COLUMN = "event_type";
      
      public static final String ICON_CHANGED_URI_PART = "icon_changed_event";
      
      public static final String NAME_CHANGED_URI_PART = "name_changed_event";
      
      public static final String NEW_ICON_URI_COLUMN = "new_icon_uri";
      
      public static final String NEW_NAME_COLUMN = "new_name";
      
      public static final String PARTICIPANT_JOINED_URI_PART = "participant_joined_event";
      
      public static final String PARTICIPANT_LEFT_URI_PART = "participant_left_event";
      
      public static final String SOURCE_PARTICIPANT_ID_COLUMN = "source_participant";
      
      public static final String TIMESTAMP_COLUMN = "origination_timestamp";
    }
    
    class RcsUnifiedThreadColumns implements RcsThreadColumns, Rcs1To1ThreadColumns, RcsGroupThreadColumns {
      public static final int THREAD_TYPE_1_TO_1 = 0;
      
      public static final String THREAD_TYPE_COLUMN = "thread_type";
      
      public static final int THREAD_TYPE_GROUP = 1;
    }
  }
  
  public static interface RcsThreadColumns {
    public static final String RCS_THREAD_ID_COLUMN = "rcs_thread_id";
    
    public static final Uri RCS_THREAD_URI = Uri.withAppendedPath(Telephony.RcsColumns.CONTENT_AND_AUTHORITY, "thread");
    
    public static final String RCS_THREAD_URI_PART = "thread";
  }
  
  class Rcs1To1ThreadColumns implements RcsColumns.RcsThreadColumns {
    public static final String FALLBACK_THREAD_ID_COLUMN = "rcs_fallback_thread_id";
    
    public static final Uri RCS_1_TO_1_THREAD_URI = Uri.withAppendedPath(Telephony.RcsColumns.CONTENT_AND_AUTHORITY, "p2p_thread");
    
    public static final String RCS_1_TO_1_THREAD_URI_PART = "p2p_thread";
  }
  
  class RcsGroupThreadColumns implements RcsColumns.RcsThreadColumns {
    public static final String CONFERENCE_URI_COLUMN = "conference_uri";
    
    public static final String GROUP_ICON_COLUMN = "group_icon";
    
    public static final String GROUP_NAME_COLUMN = "group_name";
    
    public static final String OWNER_PARTICIPANT_COLUMN = "owner_participant";
    
    public static final Uri RCS_GROUP_THREAD_URI = Uri.withAppendedPath(Telephony.RcsColumns.CONTENT_AND_AUTHORITY, "group_thread");
    
    public static final String RCS_GROUP_THREAD_URI_PART = "group_thread";
  }
  
  public static interface RcsParticipantColumns {
    public static final String CANONICAL_ADDRESS_ID_COLUMN = "canonical_address_id";
    
    public static final String RCS_ALIAS_COLUMN = "rcs_alias";
    
    public static final String RCS_PARTICIPANT_ID_COLUMN = "rcs_participant_id";
    
    public static final Uri RCS_PARTICIPANT_URI = Uri.withAppendedPath(Telephony.RcsColumns.CONTENT_AND_AUTHORITY, "participant");
    
    public static final String RCS_PARTICIPANT_URI_PART = "participant";
  }
  
  class RcsIncomingMessageColumns implements RcsColumns.RcsMessageColumns {
    public static final String ARRIVAL_TIMESTAMP_COLUMN = "arrival_timestamp";
    
    public static final Uri INCOMING_MESSAGE_URI = Uri.withAppendedPath(Telephony.RcsColumns.CONTENT_AND_AUTHORITY, "incoming_message");
    
    public static final String INCOMING_MESSAGE_URI_PART = "incoming_message";
    
    public static final String SEEN_TIMESTAMP_COLUMN = "seen_timestamp";
    
    public static final String SENDER_PARTICIPANT_ID_COLUMN = "sender_participant";
  }
  
  class RcsOutgoingMessageColumns implements RcsColumns.RcsMessageColumns {
    public static final Uri OUTGOING_MESSAGE_URI = Uri.withAppendedPath(Telephony.RcsColumns.CONTENT_AND_AUTHORITY, "outgoing_message");
    
    public static final String OUTGOING_MESSAGE_URI_PART = "outgoing_message";
  }
  
  class RcsUnifiedMessageColumns implements RcsColumns.RcsIncomingMessageColumns, RcsColumns.RcsOutgoingMessageColumns {
    public static final String MESSAGE_TYPE_COLUMN = "message_type";
    
    public static final int MESSAGE_TYPE_INCOMING = 1;
    
    public static final int MESSAGE_TYPE_OUTGOING = 0;
    
    public static final String UNIFIED_INCOMING_MESSAGE_VIEW = "unified_incoming_message_view";
    
    public static final Uri UNIFIED_MESSAGE_URI = Uri.withAppendedPath(Telephony.RcsColumns.CONTENT_AND_AUTHORITY, "message");
    
    public static final String UNIFIED_MESSAGE_URI_PART = "message";
    
    public static final String UNIFIED_OUTGOING_MESSAGE_VIEW = "unified_outgoing_message_view";
  }
  
  public static interface RcsFileTransferColumns {
    public static final String CONTENT_TYPE_COLUMN = "content_type";
    
    public static final String CONTENT_URI_COLUMN = "content_uri";
    
    public static final String DURATION_MILLIS_COLUMN = "duration";
    
    public static final String FILE_SIZE_COLUMN = "file_size";
    
    public static final String FILE_TRANSFER_ID_COLUMN = "rcs_file_transfer_id";
    
    public static final Uri FILE_TRANSFER_URI = Uri.withAppendedPath(Telephony.RcsColumns.CONTENT_AND_AUTHORITY, "file_transfer");
    
    public static final String FILE_TRANSFER_URI_PART = "file_transfer";
    
    public static final String HEIGHT_COLUMN = "height";
    
    public static final String PREVIEW_TYPE_COLUMN = "preview_type";
    
    public static final String PREVIEW_URI_COLUMN = "preview_uri";
    
    public static final String SESSION_ID_COLUMN = "session_id";
    
    public static final String SUCCESSFULLY_TRANSFERRED_BYTES = "transfer_offset";
    
    public static final String TRANSFER_STATUS_COLUMN = "transfer_status";
    
    public static final String WIDTH_COLUMN = "width";
  }
  
  class RcsUnifiedEventHelper implements RcsColumns.RcsParticipantEventColumns, RcsColumns.RcsThreadEventColumns {
    public static final Uri RCS_EVENT_QUERY_URI = Uri.withAppendedPath(Telephony.RcsColumns.CONTENT_AND_AUTHORITY, "event");
    
    public static final String RCS_EVENT_QUERY_URI_PATH = "event";
  }
  
  public static interface RcsCanonicalAddressHelper {
    static long getOrCreateCanonicalAddressId(ContentResolver param1ContentResolver, String param1String) {
      Uri.Builder builder = Telephony.RcsColumns.CONTENT_AND_AUTHORITY.buildUpon();
      builder.appendPath("canonical-address");
      builder.appendQueryParameter("address", param1String);
      Uri uri = builder.build();
      Cursor cursor = param1ContentResolver.query(uri, null, null, null);
      if (cursor != null)
        try {
          if (cursor.moveToFirst()) {
            long l = cursor.getLong(cursor.getColumnIndex("_id"));
            if (cursor != null)
              cursor.close(); 
            return l;
          } 
        } finally {} 
      Rlog.e("Telephony", "getOrCreateCanonicalAddressId returned no rows");
      if (cursor != null)
        cursor.close(); 
      Rlog.e("Telephony", "getOrCreateCanonicalAddressId failed");
      throw new IllegalArgumentException("Unable to find or allocate a canonical address ID");
    }
  }
  
  class Mms implements BaseMmsColumns {
    public static final Uri CONTENT_URI;
    
    public static final String DEFAULT_SORT_ORDER = "date DESC";
    
    public static final Pattern NAME_ADDR_EMAIL_PATTERN;
    
    public static final Uri REPORT_REQUEST_URI;
    
    static {
      Uri uri = Uri.parse("content://mms");
      REPORT_REQUEST_URI = Uri.withAppendedPath(uri, "report-request");
    }
    
    public static final Uri REPORT_STATUS_URI = Uri.withAppendedPath(CONTENT_URI, "report-status");
    
    static {
      NAME_ADDR_EMAIL_PATTERN = Pattern.compile("\\s*(\"[^\"]*\"|[^<>\"]+)\\s*<([^<>]+)>\\s*");
    }
    
    public static Cursor query(ContentResolver param1ContentResolver, String[] param1ArrayOfString) {
      SeempLog.record(10);
      return param1ContentResolver.query(CONTENT_URI, param1ArrayOfString, null, null, "date DESC");
    }
    
    public static Cursor query(ContentResolver param1ContentResolver, String[] param1ArrayOfString, String param1String1, String param1String2) {
      SeempLog.record(10);
      Uri uri = CONTENT_URI;
      if (param1String2 == null)
        param1String2 = "date DESC"; 
      return param1ContentResolver.query(uri, param1ArrayOfString, param1String1, null, param1String2);
    }
    
    public static String extractAddrSpec(String param1String) {
      Matcher matcher = NAME_ADDR_EMAIL_PATTERN.matcher(param1String);
      if (matcher.matches())
        return matcher.group(2); 
      return param1String;
    }
    
    public static boolean isEmailAddress(String param1String) {
      if (TextUtils.isEmpty(param1String))
        return false; 
      param1String = extractAddrSpec(param1String);
      Matcher matcher = Patterns.EMAIL_ADDRESS.matcher(param1String);
      return matcher.matches();
    }
    
    public static boolean isPhoneNumber(String param1String) {
      if (TextUtils.isEmpty(param1String))
        return false; 
      Matcher matcher = Patterns.PHONE.matcher(param1String);
      return matcher.matches();
    }
    
    public static final class Inbox implements Telephony.BaseMmsColumns {
      public static final Uri CONTENT_URI = Uri.parse("content://mms/inbox");
      
      public static final String DEFAULT_SORT_ORDER = "date DESC";
    }
    
    public static final class Sent implements Telephony.BaseMmsColumns {
      public static final Uri CONTENT_URI = Uri.parse("content://mms/sent");
      
      public static final String DEFAULT_SORT_ORDER = "date DESC";
    }
    
    public static final class Draft implements Telephony.BaseMmsColumns {
      public static final Uri CONTENT_URI = Uri.parse("content://mms/drafts");
      
      public static final String DEFAULT_SORT_ORDER = "date DESC";
    }
    
    public static final class Outbox implements Telephony.BaseMmsColumns {
      public static final Uri CONTENT_URI = Uri.parse("content://mms/outbox");
      
      public static final String DEFAULT_SORT_ORDER = "date DESC";
    }
    
    class Addr implements BaseColumns {
      public static final String ADDRESS = "address";
      
      public static final String CHARSET = "charset";
      
      public static final String CONTACT_ID = "contact_id";
      
      public static final String MSG_ID = "msg_id";
      
      public static final String TYPE = "type";
      
      public static Uri getAddrUriForMessage(String param2String) {
        Uri.Builder builder = Telephony.Mms.CONTENT_URI.buildUpon();
        return builder.appendPath(String.valueOf(param2String)).appendPath("addr").build();
      }
    }
    
    class Part implements BaseColumns {
      public static final String CHARSET = "chset";
      
      public static final String CONTENT_DISPOSITION = "cd";
      
      public static final String CONTENT_ID = "cid";
      
      public static final String CONTENT_LOCATION = "cl";
      
      public static final String CONTENT_TYPE = "ct";
      
      public static final Uri CONTENT_URI = Uri.withAppendedPath(Telephony.Mms.CONTENT_URI, "part");
      
      public static final String CT_START = "ctt_s";
      
      public static final String CT_TYPE = "ctt_t";
      
      public static final String FILENAME = "fn";
      
      public static final String MSG_ID = "mid";
      
      public static final String NAME = "name";
      
      public static final String SEQ = "seq";
      
      private static final String TABLE_PART = "part";
      
      public static final String TEXT = "text";
      
      public static final String _DATA = "_data";
      
      public static Uri getPartUriForMessage(String param2String) {
        Uri.Builder builder2 = Telephony.Mms.CONTENT_URI.buildUpon();
        Uri.Builder builder1 = builder2.appendPath(String.valueOf(param2String)).appendPath("part");
        return builder1.build();
      }
    }
    
    class Rate {
      public static final Uri CONTENT_URI = Uri.withAppendedPath(Telephony.Mms.CONTENT_URI, "rate");
      
      public static final String SENT_TIME = "sent_time";
    }
    
    class Intents {
      public static final String CONTENT_CHANGED_ACTION = "android.intent.action.CONTENT_CHANGED";
      
      public static final String DELETED_CONTENTS = "deleted_contents";
    }
  }
  
  public static final class Rate {
    public static final Uri CONTENT_URI = Uri.withAppendedPath(Telephony.Mms.CONTENT_URI, "rate");
    
    public static final String SENT_TIME = "sent_time";
  }
  
  public static final class Intents {
    public static final String CONTENT_CHANGED_ACTION = "android.intent.action.CONTENT_CHANGED";
    
    public static final String DELETED_CONTENTS = "deleted_contents";
  }
  
  class MmsSms implements BaseColumns {
    public static final Uri CONTENT_CONVERSATIONS_URI = Uri.parse("content://mms-sms/conversations");
    
    public static final Uri CONTENT_DRAFT_URI;
    
    public static final Uri CONTENT_FILTER_BYPHONE_URI = Uri.parse("content://mms-sms/messages/byphone");
    
    public static final Uri CONTENT_LOCKED_URI;
    
    public static final Uri CONTENT_UNDELIVERED_URI = Uri.parse("content://mms-sms/undelivered");
    
    public static final Uri CONTENT_URI = Uri.parse("content://mms-sms/");
    
    public static final int ERR_TYPE_GENERIC = 1;
    
    public static final int ERR_TYPE_GENERIC_PERMANENT = 10;
    
    public static final int ERR_TYPE_MMS_PROTO_PERMANENT = 12;
    
    public static final int ERR_TYPE_MMS_PROTO_TRANSIENT = 3;
    
    public static final int ERR_TYPE_SMS_PROTO_PERMANENT = 11;
    
    public static final int ERR_TYPE_SMS_PROTO_TRANSIENT = 2;
    
    public static final int ERR_TYPE_TRANSPORT_FAILURE = 4;
    
    public static final int MMS_PROTO = 1;
    
    public static final int NO_ERROR = 0;
    
    public static final Uri SEARCH_URI;
    
    public static final int SMS_PROTO = 0;
    
    public static final String TYPE_DISCRIMINATOR_COLUMN = "transport_type";
    
    static {
      CONTENT_DRAFT_URI = Uri.parse("content://mms-sms/draft");
      CONTENT_LOCKED_URI = Uri.parse("content://mms-sms/locked");
      SEARCH_URI = Uri.parse("content://mms-sms/search");
    }
    
    public static final class PendingMessages implements BaseColumns {
      public static final Uri CONTENT_URI = Uri.withAppendedPath(Telephony.MmsSms.CONTENT_URI, "pending");
      
      public static final String DUE_TIME = "due_time";
      
      public static final String ERROR_CODE = "err_code";
      
      public static final String ERROR_TYPE = "err_type";
      
      public static final String LAST_TRY = "last_try";
      
      public static final String MSG_ID = "msg_id";
      
      public static final String MSG_TYPE = "msg_type";
      
      public static final String PROTO_TYPE = "proto_type";
      
      public static final String RETRY_INDEX = "retry_index";
      
      public static final String SUBSCRIPTION_ID = "pending_sub_id";
    }
    
    class WordsTable {
      public static final String ID = "_id";
      
      public static final String INDEXED_TEXT = "index_text";
      
      public static final String SOURCE_ROW_ID = "source_id";
      
      public static final String TABLE_ID = "table_to_use";
    }
  }
  
  public static final class WordsTable {
    public static final String ID = "_id";
    
    public static final String INDEXED_TEXT = "index_text";
    
    public static final String SOURCE_ROW_ID = "source_id";
    
    public static final String TABLE_ID = "table_to_use";
  }
  
  class Carriers implements BaseColumns {
    public static final String APN = "apn";
    
    public static final long APN_READING_PERMISSION_CHANGE_ID = 124107808L;
    
    @SystemApi
    public static final String APN_SET_ID = "apn_set_id";
    
    public static final String AUTH_TYPE = "authtype";
    
    @Deprecated
    public static final String BEARER = "bearer";
    
    @Deprecated
    public static final String BEARER_BITMASK = "bearer_bitmask";
    
    public static final int CARRIER_DELETED = 5;
    
    public static final int CARRIER_DELETED_BUT_PRESENT_IN_XML = 6;
    
    @SystemApi
    public static final int CARRIER_EDITED = 4;
    
    public static final String CARRIER_ENABLED = "carrier_enabled";
    
    public static final String CARRIER_ID = "carrier_id";
    
    public static final Uri CONTENT_URI = Uri.parse("content://telephony/carriers");
    
    public static final String CURRENT = "current";
    
    public static final String DEFAULT_SORT_ORDER = "name ASC";
    
    public static final Uri DPC_URI = Uri.parse("content://telephony/carriers/dpc");
    
    @SystemApi
    public static final String EDITED_STATUS = "edited";
    
    public static final String ENFORCE_KEY = "enforced";
    
    public static final Uri ENFORCE_MANAGED_URI;
    
    public static final Uri FILTERED_URI = Uri.parse("content://telephony/carriers/filtered");
    
    public static final int INVALID_APN_ID = -1;
    
    @SystemApi
    public static final String MAX_CONNECTIONS = "max_conns";
    
    public static final String MCC = "mcc";
    
    public static final String MMSC = "mmsc";
    
    public static final String MMSPORT = "mmsport";
    
    public static final String MMSPROXY = "mmsproxy";
    
    public static final String MNC = "mnc";
    
    @SystemApi
    public static final String MODEM_PERSIST = "modem_cognitive";
    
    @SystemApi
    public static final String MTU = "mtu";
    
    public static final String MVNO_MATCH_DATA = "mvno_match_data";
    
    public static final String MVNO_TYPE = "mvno_type";
    
    public static final String NAME = "name";
    
    public static final String NETWORK_TYPE_BITMASK = "network_type_bitmask";
    
    @SystemApi
    public static final int NO_APN_SET_ID = 0;
    
    public static final String NUMERIC = "numeric";
    
    public static final String OWNED_BY = "owned_by";
    
    public static final int OWNED_BY_DPC = 0;
    
    public static final int OWNED_BY_OTHERS = 1;
    
    public static final String PASSWORD = "password";
    
    public static final String PORT = "port";
    
    public static final String PROFILE_ID = "profile_id";
    
    public static final String PROTOCOL = "protocol";
    
    public static final String PROXY = "proxy";
    
    public static final String ROAMING_PROTOCOL = "roaming_protocol";
    
    public static final String SERVER = "server";
    
    public static final Uri SIM_APN_URI = Uri.parse("content://telephony/carriers/sim_apn_list");
    
    public static final String SKIP_464XLAT = "skip_464xlat";
    
    public static final int SKIP_464XLAT_DEFAULT = -1;
    
    public static final int SKIP_464XLAT_DISABLE = 0;
    
    public static final int SKIP_464XLAT_ENABLE = 1;
    
    public static final String SUBSCRIPTION_ID = "sub_id";
    
    @SystemApi
    public static final String TIME_LIMIT_FOR_MAX_CONNECTIONS = "max_conns_time";
    
    public static final String TYPE = "type";
    
    @SystemApi
    public static final int UNEDITED = 0;
    
    public static final String USER = "user";
    
    @SystemApi
    public static final int USER_DELETED = 2;
    
    public static final int USER_DELETED_BUT_PRESENT_IN_XML = 3;
    
    @SystemApi
    public static final String USER_EDITABLE = "user_editable";
    
    @SystemApi
    public static final int USER_EDITED = 1;
    
    @SystemApi
    public static final String USER_VISIBLE = "user_visible";
    
    @SystemApi
    public static final String WAIT_TIME_RETRY = "wait_time";
    
    static {
      ENFORCE_MANAGED_URI = Uri.parse("content://telephony/carriers/enforce_managed");
    }
    
    @Retention(RetentionPolicy.SOURCE)
    class EditStatus implements Annotation {}
  }
  
  @SystemApi
  class CellBroadcasts implements BaseColumns {
    @SystemApi
    public static final String AUTHORITY_LEGACY = "cellbroadcast-legacy";
    
    @SystemApi
    public static final Uri AUTHORITY_LEGACY_URI = Uri.parse("content://cellbroadcast-legacy");
    
    @SystemApi
    public static final String CALL_METHOD_GET_PREFERENCE = "get_preference";
    
    public static final String CID = "cid";
    
    public static final String CMAS_CATEGORY = "cmas_category";
    
    public static final String CMAS_CERTAINTY = "cmas_certainty";
    
    public static final String CMAS_MESSAGE_CLASS = "cmas_message_class";
    
    public static final String CMAS_RESPONSE_TYPE = "cmas_response_type";
    
    public static final String CMAS_SEVERITY = "cmas_severity";
    
    public static final String CMAS_URGENCY = "cmas_urgency";
    
    public static final Uri CONTENT_URI = Uri.parse("content://cellbroadcasts");
    
    public static final String DATA_CODING_SCHEME = "dcs";
    
    public static final String DEFAULT_SORT_ORDER = "date DESC";
    
    public static final String DELIVERY_TIME = "date";
    
    public static final String ETWS_IS_PRIMARY = "etws_is_primary";
    
    public static final String ETWS_WARNING_TYPE = "etws_warning_type";
    
    public static final String GEOGRAPHICAL_SCOPE = "geo_scope";
    
    public static final String GEOMETRIES = "geometries";
    
    public static final String LAC = "lac";
    
    public static final String LANGUAGE_CODE = "language";
    
    public static final String LOCATION_CHECK_TIME = "location_check_time";
    
    public static final String MAXIMUM_WAIT_TIME = "maximum_wait_time";
    
    public static final String MESSAGE_BODY = "body";
    
    public static final String MESSAGE_BROADCASTED = "message_broadcasted";
    
    public static final String MESSAGE_DISPLAYED = "message_displayed";
    
    public static final String MESSAGE_FORMAT = "format";
    
    public static final Uri MESSAGE_HISTORY_URI = Uri.parse("content://cellbroadcasts/history");
    
    public static final String MESSAGE_PRIORITY = "priority";
    
    public static final String MESSAGE_READ = "read";
    
    public static final String PLMN = "plmn";
    
    public static final String[] QUERY_COLUMNS = new String[] { 
        "_id", "geo_scope", "plmn", "lac", "cid", "serial_number", "service_category", "language", "body", "date", 
        "read", "format", "priority", "etws_warning_type", "cmas_message_class", "cmas_category", "cmas_response_type", "cmas_severity", "cmas_urgency", "cmas_certainty" };
    
    public static final String RECEIVED_TIME = "received_time";
    
    public static final String SERIAL_NUMBER = "serial_number";
    
    public static final String SERVICE_CATEGORY = "service_category";
    
    public static final String SLOT_INDEX = "slot_index";
    
    public static final String SUBSCRIPTION_ID = "sub_id";
    
    static {
    
    }
    
    @SystemApi
    class Preference {
      public static final String ENABLE_ALERT_VIBRATION_PREF = "enable_alert_vibrate";
      
      public static final String ENABLE_AREA_UPDATE_INFO_PREF = "enable_area_update_info_alerts";
      
      public static final String ENABLE_CMAS_AMBER_PREF = "enable_cmas_amber_alerts";
      
      public static final String ENABLE_CMAS_EXTREME_THREAT_PREF = "enable_cmas_extreme_threat_alerts";
      
      public static final String ENABLE_CMAS_IN_SECOND_LANGUAGE_PREF = "receive_cmas_in_second_language";
      
      public static final String ENABLE_CMAS_PRESIDENTIAL_PREF = "enable_cmas_presidential_alerts";
      
      public static final String ENABLE_CMAS_SEVERE_THREAT_PREF = "enable_cmas_severe_threat_alerts";
      
      public static final String ENABLE_EMERGENCY_PERF = "enable_emergency_alerts";
      
      public static final String ENABLE_PUBLIC_SAFETY_PREF = "enable_public_safety_messages";
      
      public static final String ENABLE_STATE_LOCAL_TEST_PREF = "enable_state_local_test_alerts";
      
      public static final String ENABLE_TEST_ALERT_PREF = "enable_test_alerts";
    }
  }
  
  @SystemApi
  public static final class Preference {
    public static final String ENABLE_ALERT_VIBRATION_PREF = "enable_alert_vibrate";
    
    public static final String ENABLE_AREA_UPDATE_INFO_PREF = "enable_area_update_info_alerts";
    
    public static final String ENABLE_CMAS_AMBER_PREF = "enable_cmas_amber_alerts";
    
    public static final String ENABLE_CMAS_EXTREME_THREAT_PREF = "enable_cmas_extreme_threat_alerts";
    
    public static final String ENABLE_CMAS_IN_SECOND_LANGUAGE_PREF = "receive_cmas_in_second_language";
    
    public static final String ENABLE_CMAS_PRESIDENTIAL_PREF = "enable_cmas_presidential_alerts";
    
    public static final String ENABLE_CMAS_SEVERE_THREAT_PREF = "enable_cmas_severe_threat_alerts";
    
    public static final String ENABLE_EMERGENCY_PERF = "enable_emergency_alerts";
    
    public static final String ENABLE_PUBLIC_SAFETY_PREF = "enable_public_safety_messages";
    
    public static final String ENABLE_STATE_LOCAL_TEST_PREF = "enable_state_local_test_alerts";
    
    public static final String ENABLE_TEST_ALERT_PREF = "enable_test_alerts";
  }
  
  public static final class ServiceStateTable {
    public static final String AUTHORITY = "service-state";
    
    public static final Uri CONTENT_URI = Uri.parse("content://service-state/");
    
    public static final String IS_MANUAL_NETWORK_SELECTION = "is_manual_network_selection";
    
    public static final String VOICE_OPERATOR_NUMERIC = "voice_operator_numeric";
    
    public static final String VOICE_REG_STATE = "voice_reg_state";
    
    public static Uri getUriForSubscriptionIdAndField(int param1Int, String param1String) {
      Uri.Builder builder = CONTENT_URI.buildUpon().appendEncodedPath(String.valueOf(param1Int));
      return 
        builder.appendEncodedPath(param1String).build();
    }
    
    public static Uri getUriForSubscriptionId(int param1Int) {
      return CONTENT_URI.buildUpon().appendEncodedPath(String.valueOf(param1Int)).build();
    }
  }
  
  class CarrierId implements BaseColumns {
    public static final String AUTHORITY = "carrier_id";
    
    public static final String CARRIER_ID = "carrier_id";
    
    public static final String CARRIER_NAME = "carrier_name";
    
    public static final Uri CONTENT_URI = Uri.parse("content://carrier_id");
    
    public static final String PARENT_CARRIER_ID = "parent_carrier_id";
    
    public static final String SPECIFIC_CARRIER_ID = "specific_carrier_id";
    
    public static final String SPECIFIC_CARRIER_ID_NAME = "specific_carrier_id_name";
    
    public static Uri getUriForSubscriptionId(int param1Int) {
      Uri.Builder builder = CONTENT_URI.buildUpon();
      builder = builder.appendEncodedPath(String.valueOf(param1Int));
      return 
        builder.build();
    }
    
    public static Uri getSpecificCarrierIdUriForSubscriptionId(int param1Int) {
      Uri uri = Uri.withAppendedPath(CONTENT_URI, "specific");
      return Uri.withAppendedPath(uri, String.valueOf(param1Int));
    }
    
    public static final class All implements BaseColumns {
      public static final String APN = "apn";
      
      public static final Uri CONTENT_URI = Uri.parse("content://carrier_id/all");
      
      public static final String GID1 = "gid1";
      
      public static final String GID2 = "gid2";
      
      public static final String ICCID_PREFIX = "iccid_prefix";
      
      public static final String IMSI_PREFIX_XPATTERN = "imsi_prefix_xpattern";
      
      public static final String MCCMNC = "mccmnc";
      
      public static final String PLMN = "plmn";
      
      public static final String PRIVILEGE_ACCESS_RULE = "privilege_access_rule";
      
      public static final String SPN = "spn";
    }
  }
  
  public static final class SimInfo {
    public static final int COLOR_DEFAULT = 0;
    
    public static final String COLUMN_ACCESS_RULES = "access_rules";
    
    public static final String COLUMN_ACCESS_RULES_FROM_CARRIER_CONFIGS = "access_rules_from_carrier_configs";
    
    public static final String COLUMN_ALLOWED_NETWORK_TYPES = "allowed_network_types";
    
    public static final String COLUMN_CARD_ID = "card_id";
    
    public static final String COLUMN_CARRIER_ID = "carrier_id";
    
    public static final String COLUMN_CARRIER_NAME = "carrier_name";
    
    public static final String COLUMN_CB_ALERT_REMINDER_INTERVAL = "alert_reminder_interval";
    
    public static final String COLUMN_CB_ALERT_SOUND_DURATION = "alert_sound_duration";
    
    public static final String COLUMN_CB_ALERT_SPEECH = "enable_alert_speech";
    
    public static final String COLUMN_CB_ALERT_VIBRATE = "enable_alert_vibrate";
    
    public static final String COLUMN_CB_AMBER_ALERT = "enable_cmas_amber_alerts";
    
    public static final String COLUMN_CB_CHANNEL_50_ALERT = "enable_channel_50_alerts";
    
    public static final String COLUMN_CB_CMAS_TEST_ALERT = "enable_cmas_test_alerts";
    
    public static final String COLUMN_CB_EMERGENCY_ALERT = "enable_emergency_alerts";
    
    public static final String COLUMN_CB_ETWS_TEST_ALERT = "enable_etws_test_alerts";
    
    public static final String COLUMN_CB_EXTREME_THREAT_ALERT = "enable_cmas_extreme_threat_alerts";
    
    public static final String COLUMN_CB_OPT_OUT_DIALOG = "show_cmas_opt_out_dialog";
    
    public static final String COLUMN_CB_SEVERE_THREAT_ALERT = "enable_cmas_severe_threat_alerts";
    
    public static final String COLUMN_COLOR = "color";
    
    public static final String COLUMN_DATA_ENABLED_OVERRIDE_RULES = "data_enabled_override_rules";
    
    public static final String COLUMN_DATA_ROAMING = "data_roaming";
    
    public static final String COLUMN_DISPLAY_NAME = "display_name";
    
    public static final String COLUMN_DISPLAY_NUMBER_FORMAT = "display_number_format";
    
    public static final String COLUMN_EHPLMNS = "ehplmns";
    
    public static final String COLUMN_ENHANCED_4G_MODE_ENABLED = "volte_vt_enabled";
    
    public static final String COLUMN_GROUP_OWNER = "group_owner";
    
    public static final String COLUMN_GROUP_UUID = "group_uuid";
    
    public static final String COLUMN_HPLMNS = "hplmns";
    
    public static final String COLUMN_ICC_ID = "icc_id";
    
    public static final String COLUMN_IMSI = "imsi";
    
    public static final String COLUMN_IMS_RCS_UCE_ENABLED = "ims_rcs_uce_enabled";
    
    public static final String COLUMN_ISO_COUNTRY_CODE = "iso_country_code";
    
    public static final String COLUMN_IS_EMBEDDED = "is_embedded";
    
    public static final String COLUMN_IS_METERED = "is_metered";
    
    public static final String COLUMN_IS_OPPORTUNISTIC = "is_opportunistic";
    
    public static final String COLUMN_IS_REMOVABLE = "is_removable";
    
    public static final String COLUMN_MCC = "mcc";
    
    public static final String COLUMN_MCC_STRING = "mcc_string";
    
    public static final String COLUMN_MNC = "mnc";
    
    public static final String COLUMN_MNC_STRING = "mnc_string";
    
    public static final String COLUMN_NAME_SOURCE = "name_source";
    
    public static final String COLUMN_NUMBER = "number";
    
    public static final String COLUMN_PROFILE_CLASS = "profile_class";
    
    public static final String COLUMN_SIM_PROVISIONING_STATUS = "sim_provisioning_status";
    
    public static final String COLUMN_SIM_SLOT_INDEX = "sim_id";
    
    public static final String COLUMN_SUBSCRIPTION_TYPE = "subscription_type";
    
    public static final String COLUMN_UICC_APPLICATIONS_ENABLED = "uicc_applications_enabled";
    
    public static final String COLUMN_UNIQUE_KEY_SUBSCRIPTION_ID = "_id";
    
    public static final String COLUMN_VT_IMS_ENABLED = "vt_ims_enabled";
    
    public static final String COLUMN_WFC_IMS_ENABLED = "wfc_ims_enabled";
    
    public static final String COLUMN_WFC_IMS_MODE = "wfc_ims_mode";
    
    public static final String COLUMN_WFC_IMS_ROAMING_ENABLED = "wfc_ims_roaming_enabled";
    
    public static final String COLUMN_WFC_IMS_ROAMING_MODE = "wfc_ims_roaming_mode";
    
    public static final Uri CONTENT_URI = Uri.parse("content://telephony/siminfo");
    
    public static final int DATA_ROAMING_DISABLE = 0;
    
    public static final int DATA_ROAMING_ENABLE = 1;
    
    public static final int DISPLAY_NUMBER_DEFAULT = 1;
    
    public static final int NAME_SOURCE_CARRIER = 3;
    
    public static final int NAME_SOURCE_CARRIER_ID = 0;
    
    public static final int NAME_SOURCE_SIM_PNN = 4;
    
    public static final int NAME_SOURCE_SIM_SPN = 1;
    
    public static final int NAME_SOURCE_USER_INPUT = 2;
    
    public static final int PROFILE_CLASS_OPERATIONAL = 2;
    
    public static final int PROFILE_CLASS_PROVISIONING = 1;
    
    public static final int PROFILE_CLASS_TESTING = 0;
    
    public static final int PROFILE_CLASS_UNSET = -1;
    
    public static final int SIM_NOT_INSERTED = -1;
    
    public static final int SIM_PROVISIONED = 0;
    
    public static final int SUBSCRIPTION_TYPE_LOCAL_SIM = 0;
    
    public static final int SUBSCRIPTION_TYPE_REMOTE_SIM = 1;
  }
  
  class BaseMmsColumns implements BaseColumns {
    @Deprecated
    public static final String ADAPTATION_ALLOWED = "adp_a";
    
    @Deprecated
    public static final String APPLIC_ID = "apl_id";
    
    @Deprecated
    public static final String AUX_APPLIC_ID = "aux_apl_id";
    
    @Deprecated
    public static final String CANCEL_ID = "cl_id";
    
    @Deprecated
    public static final String CANCEL_STATUS = "cl_st";
    
    public static final String CONTENT_CLASS = "ct_cls";
    
    public static final String CONTENT_LOCATION = "ct_l";
    
    public static final String CONTENT_TYPE = "ct_t";
    
    public static final String CREATOR = "creator";
    
    public static final String DATE = "date";
    
    public static final String DATE_SENT = "date_sent";
    
    public static final String DELIVERY_REPORT = "d_rpt";
    
    public static final String DELIVERY_TIME = "d_tm";
    
    @Deprecated
    public static final String DELIVERY_TIME_TOKEN = "d_tm_tok";
    
    @Deprecated
    public static final String DISTRIBUTION_INDICATOR = "d_ind";
    
    @Deprecated
    public static final String DRM_CONTENT = "drm_c";
    
    @Deprecated
    public static final String ELEMENT_DESCRIPTOR = "e_des";
    
    public static final String EXPIRY = "exp";
    
    @Deprecated
    public static final String LIMIT = "limit";
    
    public static final String LOCKED = "locked";
    
    @Deprecated
    public static final String MBOX_QUOTAS = "mb_qt";
    
    @Deprecated
    public static final String MBOX_QUOTAS_TOKEN = "mb_qt_tok";
    
    @Deprecated
    public static final String MBOX_TOTALS = "mb_t";
    
    @Deprecated
    public static final String MBOX_TOTALS_TOKEN = "mb_t_tok";
    
    public static final String MESSAGE_BOX = "msg_box";
    
    public static final int MESSAGE_BOX_ALL = 0;
    
    public static final int MESSAGE_BOX_DRAFTS = 3;
    
    public static final int MESSAGE_BOX_FAILED = 5;
    
    public static final int MESSAGE_BOX_INBOX = 1;
    
    public static final int MESSAGE_BOX_OUTBOX = 4;
    
    public static final int MESSAGE_BOX_SENT = 2;
    
    public static final String MESSAGE_CLASS = "m_cls";
    
    @Deprecated
    public static final String MESSAGE_COUNT = "m_cnt";
    
    public static final String MESSAGE_ID = "m_id";
    
    public static final String MESSAGE_SIZE = "m_size";
    
    public static final String MESSAGE_TYPE = "m_type";
    
    public static final String MMS_VERSION = "v";
    
    @Deprecated
    public static final String MM_FLAGS = "mm_flg";
    
    @Deprecated
    public static final String MM_FLAGS_TOKEN = "mm_flg_tok";
    
    @Deprecated
    public static final String MM_STATE = "mm_st";
    
    @Deprecated
    public static final String PREVIOUSLY_SENT_BY = "p_s_by";
    
    @Deprecated
    public static final String PREVIOUSLY_SENT_DATE = "p_s_d";
    
    public static final String PRIORITY = "pri";
    
    @Deprecated
    public static final String QUOTAS = "qt";
    
    public static final String READ = "read";
    
    public static final String READ_REPORT = "rr";
    
    public static final String READ_STATUS = "read_status";
    
    @Deprecated
    public static final String RECOMMENDED_RETRIEVAL_MODE = "r_r_mod";
    
    @Deprecated
    public static final String RECOMMENDED_RETRIEVAL_MODE_TEXT = "r_r_mod_txt";
    
    @Deprecated
    public static final String REPLACE_ID = "repl_id";
    
    @Deprecated
    public static final String REPLY_APPLIC_ID = "r_apl_id";
    
    @Deprecated
    public static final String REPLY_CHARGING = "r_chg";
    
    @Deprecated
    public static final String REPLY_CHARGING_DEADLINE = "r_chg_dl";
    
    @Deprecated
    public static final String REPLY_CHARGING_DEADLINE_TOKEN = "r_chg_dl_tok";
    
    @Deprecated
    public static final String REPLY_CHARGING_ID = "r_chg_id";
    
    @Deprecated
    public static final String REPLY_CHARGING_SIZE = "r_chg_sz";
    
    public static final String REPORT_ALLOWED = "rpt_a";
    
    public static final String RESPONSE_STATUS = "resp_st";
    
    public static final String RESPONSE_TEXT = "resp_txt";
    
    public static final String RETRIEVE_STATUS = "retr_st";
    
    public static final String RETRIEVE_TEXT = "retr_txt";
    
    public static final String RETRIEVE_TEXT_CHARSET = "retr_txt_cs";
    
    public static final String SEEN = "seen";
    
    @Deprecated
    public static final String SENDER_VISIBILITY = "s_vis";
    
    @Deprecated
    public static final String START = "start";
    
    public static final String STATUS = "st";
    
    @Deprecated
    public static final String STATUS_TEXT = "st_txt";
    
    @Deprecated
    public static final String STORE = "store";
    
    @Deprecated
    public static final String STORED = "stored";
    
    @Deprecated
    public static final String STORE_STATUS = "store_st";
    
    @Deprecated
    public static final String STORE_STATUS_TEXT = "store_st_txt";
    
    public static final String SUBJECT = "sub";
    
    public static final String SUBJECT_CHARSET = "sub_cs";
    
    public static final String SUBSCRIPTION_ID = "sub_id";
    
    public static final String TEXT_ONLY = "text_only";
    
    public static final String THREAD_ID = "thread_id";
    
    @Deprecated
    public static final String TOTALS = "totals";
    
    public static final String TRANSACTION_ID = "tr_id";
  }
  
  class CanonicalAddressesColumns implements BaseColumns {
    public static final String ADDRESS = "address";
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface EditStatus {}
  
  public static interface RcsEventTypes {
    public static final int ICON_CHANGED_EVENT_TYPE = 8;
    
    public static final int NAME_CHANGED_EVENT_TYPE = 16;
    
    public static final int PARTICIPANT_ALIAS_CHANGED_EVENT_TYPE = 1;
    
    public static final int PARTICIPANT_JOINED_EVENT_TYPE = 2;
    
    public static final int PARTICIPANT_LEFT_EVENT_TYPE = 4;
  }
  
  public static interface RcsMessageColumns {
    public static final String GLOBAL_ID_COLUMN = "rcs_message_global_id";
    
    public static final String LATITUDE_COLUMN = "latitude";
    
    public static final String LONGITUDE_COLUMN = "longitude";
    
    public static final String MESSAGE_ID_COLUMN = "rcs_message_row_id";
    
    public static final String MESSAGE_TEXT_COLUMN = "rcs_text";
    
    public static final String MESSAGE_TYPE_COLUMN = "rcs_message_type";
    
    public static final String ORIGINATION_TIMESTAMP_COLUMN = "origination_timestamp";
    
    public static final String STATUS_COLUMN = "status";
    
    public static final String SUB_ID_COLUMN = "sub_id";
  }
  
  class RcsMessageDeliveryColumns implements RcsColumns.RcsOutgoingMessageColumns {
    public static final String DELIVERED_TIMESTAMP_COLUMN = "delivered_timestamp";
    
    public static final String DELIVERY_URI_PART = "delivery";
    
    public static final String SEEN_TIMESTAMP_COLUMN = "seen_timestamp";
  }
  
  public static interface RcsParticipantEventColumns {
    public static final String ALIAS_CHANGE_EVENT_URI_PART = "alias_change_event";
    
    public static final String NEW_ALIAS_COLUMN = "new_alias";
  }
  
  class RcsParticipantHelpers implements RcsColumns.RcsParticipantColumns {
    public static final String RCS_PARTICIPANT_WITH_ADDRESS_VIEW = "rcs_participant_with_address_view";
    
    public static final String RCS_PARTICIPANT_WITH_THREAD_VIEW = "rcs_participant_with_thread_view";
  }
  
  public static interface RcsThreadEventColumns {
    public static final String DESTINATION_PARTICIPANT_ID_COLUMN = "destination_participant";
    
    public static final String EVENT_ID_COLUMN = "event_id";
    
    public static final String EVENT_TYPE_COLUMN = "event_type";
    
    public static final String ICON_CHANGED_URI_PART = "icon_changed_event";
    
    public static final String NAME_CHANGED_URI_PART = "name_changed_event";
    
    public static final String NEW_ICON_URI_COLUMN = "new_icon_uri";
    
    public static final String NEW_NAME_COLUMN = "new_name";
    
    public static final String PARTICIPANT_JOINED_URI_PART = "participant_joined_event";
    
    public static final String PARTICIPANT_LEFT_URI_PART = "participant_left_event";
    
    public static final String SOURCE_PARTICIPANT_ID_COLUMN = "source_participant";
    
    public static final String TIMESTAMP_COLUMN = "origination_timestamp";
  }
  
  class RcsUnifiedThreadColumns implements RcsColumns.RcsThreadColumns, RcsColumns.Rcs1To1ThreadColumns, RcsColumns.RcsGroupThreadColumns {
    public static final int THREAD_TYPE_1_TO_1 = 0;
    
    public static final String THREAD_TYPE_COLUMN = "thread_type";
    
    public static final int THREAD_TYPE_GROUP = 1;
  }
  
  public static interface TextBasedSmsColumns {
    public static final String ADDRESS = "address";
    
    public static final String BODY = "body";
    
    public static final String CREATOR = "creator";
    
    public static final String DATE = "date";
    
    public static final String DATE_SENT = "date_sent";
    
    public static final String ERROR_CODE = "error_code";
    
    public static final String LOCKED = "locked";
    
    public static final int MESSAGE_TYPE_ALL = 0;
    
    public static final int MESSAGE_TYPE_DRAFT = 3;
    
    public static final int MESSAGE_TYPE_FAILED = 5;
    
    public static final int MESSAGE_TYPE_INBOX = 1;
    
    public static final int MESSAGE_TYPE_OUTBOX = 4;
    
    public static final int MESSAGE_TYPE_QUEUED = 6;
    
    public static final int MESSAGE_TYPE_SENT = 2;
    
    public static final String MTU = "mtu";
    
    public static final String PERSON = "person";
    
    public static final String PRIORITY = "priority";
    
    public static final String PROTOCOL = "protocol";
    
    public static final String READ = "read";
    
    public static final String REPLY_PATH_PRESENT = "reply_path_present";
    
    public static final String SEEN = "seen";
    
    public static final String SERVICE_CENTER = "service_center";
    
    public static final String STATUS = "status";
    
    public static final int STATUS_COMPLETE = 0;
    
    public static final int STATUS_FAILED = 64;
    
    public static final int STATUS_NONE = -1;
    
    public static final int STATUS_PENDING = 32;
    
    public static final String SUBJECT = "subject";
    
    public static final String SUBSCRIPTION_ID = "sub_id";
    
    public static final String THREAD_ID = "thread_id";
    
    public static final String TYPE = "type";
  }
  
  class ThreadsColumns implements BaseColumns {
    public static final String ARCHIVED = "archived";
    
    public static final String ATTACHMENT_INFO = "attachment_info";
    
    public static final String DATE = "date";
    
    public static final String ERROR = "error";
    
    public static final String HAS_ATTACHMENT = "has_attachment";
    
    public static final String MESSAGE_COUNT = "message_count";
    
    public static final String NOTIFICATION = "notification";
    
    public static final String READ = "read";
    
    public static final String RECIPIENT_IDS = "recipient_ids";
    
    public static final String SNIPPET = "snippet";
    
    public static final String SNIPPET_CHARSET = "snippet_cs";
    
    public static final String TYPE = "type";
  }
}
