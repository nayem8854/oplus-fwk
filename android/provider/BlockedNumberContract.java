package android.provider;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.telecom.Log;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class BlockedNumberContract {
  public static final String AUTHORITY = "com.android.blockednumber";
  
  public static final Uri AUTHORITY_URI = Uri.parse("content://com.android.blockednumber");
  
  public static final String EXTRA_CALL_PRESENTATION = "extra_call_presentation";
  
  public static final String EXTRA_CONTACT_EXIST = "extra_contact_exist";
  
  public static final String EXTRA_ENHANCED_SETTING_KEY = "extra_enhanced_setting_key";
  
  public static final String EXTRA_ENHANCED_SETTING_VALUE = "extra_enhanced_setting_value";
  
  private static final String LOG_TAG = BlockedNumberContract.class.getSimpleName();
  
  public static final String METHOD_CAN_CURRENT_USER_BLOCK_NUMBERS = "can_current_user_block_numbers";
  
  public static final String METHOD_IS_BLOCKED = "is_blocked";
  
  public static final String METHOD_UNBLOCK = "unblock";
  
  public static final String RES_BLOCK_STATUS = "block_status";
  
  public static final String RES_CAN_BLOCK_NUMBERS = "can_block";
  
  public static final String RES_ENHANCED_SETTING_IS_ENABLED = "enhanced_setting_enabled";
  
  public static final String RES_NUMBER_IS_BLOCKED = "blocked";
  
  public static final String RES_NUM_ROWS_DELETED = "num_deleted";
  
  public static final String RES_SHOW_EMERGENCY_CALL_NOTIFICATION = "show_emergency_call_notification";
  
  public static final int STATUS_BLOCKED_IN_LIST = 1;
  
  public static final int STATUS_BLOCKED_NOT_IN_CONTACTS = 5;
  
  public static final int STATUS_BLOCKED_PAYPHONE = 4;
  
  public static final int STATUS_BLOCKED_RESTRICTED = 2;
  
  public static final int STATUS_BLOCKED_UNKNOWN_NUMBER = 3;
  
  public static final int STATUS_NOT_BLOCKED = 0;
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface BlockStatus {}
  
  public static class BlockedNumbers {
    public static final String COLUMN_E164_NUMBER = "e164_number";
    
    public static final String COLUMN_ID = "_id";
    
    public static final String COLUMN_ORIGINAL_NUMBER = "original_number";
    
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/blocked_number";
    
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/blocked_number";
    
    public static final Uri CONTENT_URI = Uri.withAppendedPath(BlockedNumberContract.AUTHORITY_URI, "blocked");
  }
  
  public static boolean isBlocked(Context paramContext, String paramString) {
    try {
      boolean bool;
      Bundle bundle = paramContext.getContentResolver().call(AUTHORITY_URI, "is_blocked", paramString, null);
      if (bundle != null && bundle.getBoolean("blocked", false)) {
        bool = true;
      } else {
        bool = false;
      } 
      String str = LOG_TAG;
      paramString = Log.piiHandle(paramString);
      Log.d(str, "isBlocked: phoneNumber=%s, isBlocked=%b", new Object[] { paramString, Boolean.valueOf(bool) });
      return bool;
    } catch (NullPointerException|IllegalArgumentException nullPointerException) {
      Log.w(null, "isBlocked: provider not ready.", new Object[0]);
      return false;
    } 
  }
  
  public static int unblock(Context paramContext, String paramString) {
    Log.d(LOG_TAG, "unblock: phoneNumber=%s", new Object[] { Log.piiHandle(paramString) });
    Bundle bundle = paramContext.getContentResolver().call(AUTHORITY_URI, "unblock", paramString, null);
    return bundle.getInt("num_deleted", 0);
  }
  
  public static boolean canCurrentUserBlockNumbers(Context paramContext) {
    boolean bool = false;
    try {
      Bundle bundle = paramContext.getContentResolver().call(AUTHORITY_URI, "can_current_user_block_numbers", null, null);
      boolean bool1 = bool;
      if (bundle != null) {
        boolean bool2 = bundle.getBoolean("can_block", false);
        bool1 = bool;
        if (bool2)
          bool1 = true; 
      } 
      return bool1;
    } catch (NullPointerException|IllegalArgumentException nullPointerException) {
      Log.w(null, "canCurrentUserBlockNumbers: provider not ready.", new Object[0]);
      return false;
    } 
  }
  
  public static class SystemContract {
    public static final String ACTION_BLOCK_SUPPRESSION_STATE_CHANGED = "android.provider.action.BLOCK_SUPPRESSION_STATE_CHANGED";
    
    public static final String ENHANCED_SETTING_KEY_BLOCK_PAYPHONE = "block_payphone_calls_setting";
    
    public static final String ENHANCED_SETTING_KEY_BLOCK_PRIVATE = "block_private_number_calls_setting";
    
    public static final String ENHANCED_SETTING_KEY_BLOCK_UNKNOWN = "block_unknown_calls_setting";
    
    public static final String ENHANCED_SETTING_KEY_BLOCK_UNREGISTERED = "block_numbers_not_in_contacts_setting";
    
    public static final String ENHANCED_SETTING_KEY_SHOW_EMERGENCY_CALL_NOTIFICATION = "show_emergency_call_notification";
    
    public static final String METHOD_END_BLOCK_SUPPRESSION = "end_block_suppression";
    
    public static final String METHOD_GET_BLOCK_SUPPRESSION_STATUS = "get_block_suppression_status";
    
    public static final String METHOD_GET_ENHANCED_BLOCK_SETTING = "get_enhanced_block_setting";
    
    public static final String METHOD_NOTIFY_EMERGENCY_CONTACT = "notify_emergency_contact";
    
    public static final String METHOD_SET_ENHANCED_BLOCK_SETTING = "set_enhanced_block_setting";
    
    public static final String METHOD_SHOULD_SHOW_EMERGENCY_CALL_NOTIFICATION = "should_show_emergency_call_notification";
    
    public static final String METHOD_SHOULD_SYSTEM_BLOCK_NUMBER = "should_system_block_number";
    
    public static final String RES_BLOCKING_SUPPRESSED_UNTIL_TIMESTAMP = "blocking_suppressed_until_timestamp";
    
    public static final String RES_IS_BLOCKING_SUPPRESSED = "blocking_suppressed";
    
    public static void notifyEmergencyContact(Context param1Context) {
      try {
        Log.i(BlockedNumberContract.LOG_TAG, "notifyEmergencyContact; caller=%s", new Object[] { param1Context.getOpPackageName() });
        param1Context.getContentResolver().call(BlockedNumberContract.AUTHORITY_URI, "notify_emergency_contact", null, null);
      } catch (NullPointerException|IllegalArgumentException nullPointerException) {
        Log.w(null, "notifyEmergencyContact: provider not ready.", new Object[0]);
      } 
    }
    
    public static void endBlockSuppression(Context param1Context) {
      String str = param1Context.getOpPackageName();
      Log.i(BlockedNumberContract.LOG_TAG, "endBlockSuppression: caller=%s", new Object[] { str });
      param1Context.getContentResolver().call(BlockedNumberContract.AUTHORITY_URI, "end_block_suppression", null, null);
    }
    
    public static int shouldSystemBlockNumber(Context param1Context, String param1String, Bundle param1Bundle) {
      try {
        boolean bool;
        String str3 = param1Context.getOpPackageName();
        Bundle bundle = param1Context.getContentResolver().call(BlockedNumberContract.AUTHORITY_URI, "should_system_block_number", param1String, param1Bundle);
        if (bundle != null) {
          bool = bundle.getInt("block_status", 0);
        } else {
          bool = false;
        } 
        String str1 = BlockedNumberContract.LOG_TAG;
        String str2 = Log.piiHandle(param1String);
        param1String = blockStatusToString(bool);
        Log.d(str1, "shouldSystemBlockNumber: number=%s, caller=%s, result=%s", new Object[] { str2, str3, param1String });
        return bool;
      } catch (NullPointerException|IllegalArgumentException nullPointerException) {
        Log.w(null, "shouldSystemBlockNumber: provider not ready.", new Object[0]);
        return 0;
      } 
    }
    
    public static BlockSuppressionStatus getBlockSuppressionStatus(Context param1Context) {
      Bundle bundle = param1Context.getContentResolver().call(BlockedNumberContract.AUTHORITY_URI, "get_block_suppression_status", null, null);
      boolean bool = bundle.getBoolean("blocking_suppressed", false);
      BlockSuppressionStatus blockSuppressionStatus = new BlockSuppressionStatus(bool, bundle.getLong("blocking_suppressed_until_timestamp", 0L));
      String str2 = BlockedNumberContract.LOG_TAG;
      String str1 = param1Context.getOpPackageName();
      Log.d(str2, "getBlockSuppressionStatus: caller=%s, status=%s", new Object[] { str1, blockSuppressionStatus });
      return blockSuppressionStatus;
    }
    
    public static boolean shouldShowEmergencyCallNotification(Context param1Context) {
      boolean bool = false;
      try {
        Bundle bundle = param1Context.getContentResolver().call(BlockedNumberContract.AUTHORITY_URI, "should_show_emergency_call_notification", null, null);
        boolean bool1 = bool;
        if (bundle != null) {
          boolean bool2 = bundle.getBoolean("show_emergency_call_notification", false);
          bool1 = bool;
          if (bool2)
            bool1 = true; 
        } 
        return bool1;
      } catch (NullPointerException|IllegalArgumentException nullPointerException) {
        Log.w(null, "shouldShowEmergencyCallNotification: provider not ready.", new Object[0]);
        return false;
      } 
    }
    
    public static boolean getEnhancedBlockSetting(Context param1Context, String param1String) {
      Bundle bundle = new Bundle();
      bundle.putString("extra_enhanced_setting_key", param1String);
      boolean bool = false;
      try {
        Bundle bundle1 = param1Context.getContentResolver().call(BlockedNumberContract.AUTHORITY_URI, "get_enhanced_block_setting", null, bundle);
        boolean bool1 = bool;
        if (bundle1 != null) {
          boolean bool2 = bundle1.getBoolean("enhanced_setting_enabled", false);
          bool1 = bool;
          if (bool2)
            bool1 = true; 
        } 
        return bool1;
      } catch (NullPointerException|IllegalArgumentException nullPointerException) {
        Log.w(null, "getEnhancedBlockSetting: provider not ready.", new Object[0]);
        return false;
      } 
    }
    
    public static void setEnhancedBlockSetting(Context param1Context, String param1String, boolean param1Boolean) {
      Bundle bundle = new Bundle();
      bundle.putString("extra_enhanced_setting_key", param1String);
      bundle.putBoolean("extra_enhanced_setting_value", param1Boolean);
      param1Context.getContentResolver().call(BlockedNumberContract.AUTHORITY_URI, "set_enhanced_block_setting", null, bundle);
    }
    
    public static String blockStatusToString(int param1Int) {
      if (param1Int != 0) {
        if (param1Int != 1) {
          if (param1Int != 2) {
            if (param1Int != 3) {
              if (param1Int != 4) {
                if (param1Int != 5)
                  return "unknown"; 
                return "blocked - not in contacts";
              } 
              return "blocked - payphone";
            } 
            return "blocked - unknown";
          } 
          return "blocked - restricted";
        } 
        return "blocked - in list";
      } 
      return "not blocked";
    }
    
    public static class BlockSuppressionStatus {
      public final boolean isSuppressed;
      
      public final long untilTimestampMillis;
      
      public BlockSuppressionStatus(boolean param2Boolean, long param2Long) {
        this.isSuppressed = param2Boolean;
        this.untilTimestampMillis = param2Long;
      }
      
      public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[BlockSuppressionStatus; isSuppressed=");
        stringBuilder.append(this.isSuppressed);
        stringBuilder.append(", until=");
        stringBuilder.append(this.untilTimestampMillis);
        stringBuilder.append("]");
        return stringBuilder.toString();
      }
    }
  }
  
  public static class BlockSuppressionStatus {
    public final boolean isSuppressed;
    
    public final long untilTimestampMillis;
    
    public BlockSuppressionStatus(boolean param1Boolean, long param1Long) {
      this.isSuppressed = param1Boolean;
      this.untilTimestampMillis = param1Long;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("[BlockSuppressionStatus; isSuppressed=");
      stringBuilder.append(this.isSuppressed);
      stringBuilder.append(", until=");
      stringBuilder.append(this.untilTimestampMillis);
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
  }
}
