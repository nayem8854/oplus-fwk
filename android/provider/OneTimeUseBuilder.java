package android.provider;

public abstract class OneTimeUseBuilder<T> {
  private boolean used = false;
  
  protected void markUsed() {
    checkNotUsed();
    this.used = true;
  }
  
  protected void checkNotUsed() {
    if (!this.used)
      return; 
    throw new IllegalStateException("This Builder should not be reused. Use a new Builder instance instead");
  }
  
  public abstract T build();
}
