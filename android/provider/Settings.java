package android.provider;

import android.annotation.SystemApi;
import android.app.ActivityThread;
import android.app.AppOpsManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.IContentProvider;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.os.Bundle;
import android.os.LocaleList;
import android.os.Process;
import android.os.RemoteCallback;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.AndroidException;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.Log;
import android.util.MemoryIntArray;
import android.util.SeempLog;
import com.android.internal.util.Preconditions;
import com.android.internal.widget.ILockSettings;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class Settings {
  @SystemApi
  public static final String ACTION_ACCESSIBILITY_DETAILS_SETTINGS = "android.settings.ACCESSIBILITY_DETAILS_SETTINGS";
  
  public static final String ACTION_ACCESSIBILITY_SETTINGS = "android.settings.ACCESSIBILITY_SETTINGS";
  
  public static final String ACTION_ADD_ACCOUNT = "android.settings.ADD_ACCOUNT_SETTINGS";
  
  public static final String ACTION_AIRPLANE_MODE_SETTINGS = "android.settings.AIRPLANE_MODE_SETTINGS";
  
  public static final String ACTION_ALL_APPS_NOTIFICATION_SETTINGS = "android.settings.ALL_APPS_NOTIFICATION_SETTINGS";
  
  public static final String ACTION_APN_SETTINGS = "android.settings.APN_SETTINGS";
  
  public static final String ACTION_APPLICATION_DETAILS_SETTINGS = "android.settings.APPLICATION_DETAILS_SETTINGS";
  
  public static final String ACTION_APPLICATION_DEVELOPMENT_SETTINGS = "android.settings.APPLICATION_DEVELOPMENT_SETTINGS";
  
  public static final String ACTION_APPLICATION_SETTINGS = "android.settings.APPLICATION_SETTINGS";
  
  public static final String ACTION_APP_NOTIFICATION_BUBBLE_SETTINGS = "android.settings.APP_NOTIFICATION_BUBBLE_SETTINGS";
  
  public static final String ACTION_APP_NOTIFICATION_REDACTION = "android.settings.ACTION_APP_NOTIFICATION_REDACTION";
  
  public static final String ACTION_APP_NOTIFICATION_SETTINGS = "android.settings.APP_NOTIFICATION_SETTINGS";
  
  public static final String ACTION_APP_OPEN_BY_DEFAULT_SETTINGS = "com.android.settings.APP_OPEN_BY_DEFAULT_SETTINGS";
  
  public static final String ACTION_APP_OPS_SETTINGS = "android.settings.APP_OPS_SETTINGS";
  
  public static final String ACTION_APP_SEARCH_SETTINGS = "android.settings.APP_SEARCH_SETTINGS";
  
  public static final String ACTION_APP_USAGE_SETTINGS = "android.settings.action.APP_USAGE_SETTINGS";
  
  public static final String ACTION_ASSIST_GESTURE_SETTINGS = "android.settings.ASSIST_GESTURE_SETTINGS";
  
  public static final String ACTION_BATTERY_SAVER_SETTINGS = "android.settings.BATTERY_SAVER_SETTINGS";
  
  public static final String ACTION_BIOMETRIC_ENROLL = "android.settings.BIOMETRIC_ENROLL";
  
  public static final String ACTION_BLUETOOTH_SETTINGS = "android.settings.BLUETOOTH_SETTINGS";
  
  @SystemApi
  public static final String ACTION_BUGREPORT_HANDLER_SETTINGS = "android.settings.BUGREPORT_HANDLER_SETTINGS";
  
  public static final String ACTION_CAPTIONING_SETTINGS = "android.settings.CAPTIONING_SETTINGS";
  
  public static final String ACTION_CAST_SETTINGS = "android.settings.CAST_SETTINGS";
  
  public static final String ACTION_CHANNEL_NOTIFICATION_SETTINGS = "android.settings.CHANNEL_NOTIFICATION_SETTINGS";
  
  public static final String ACTION_CONDITION_PROVIDER_SETTINGS = "android.settings.ACTION_CONDITION_PROVIDER_SETTINGS";
  
  public static final String ACTION_CONVERSATION_SETTINGS = "android.settings.CONVERSATION_SETTINGS";
  
  public static final String ACTION_DARK_THEME_SETTINGS = "android.settings.DARK_THEME_SETTINGS";
  
  public static final String ACTION_DATA_ROAMING_SETTINGS = "android.settings.DATA_ROAMING_SETTINGS";
  
  public static final String ACTION_DATA_SAVER_SETTINGS = "android.settings.DATA_SAVER_SETTINGS";
  
  public static final String ACTION_DATA_USAGE_SETTINGS = "android.settings.DATA_USAGE_SETTINGS";
  
  public static final String ACTION_DATE_SETTINGS = "android.settings.DATE_SETTINGS";
  
  public static final String ACTION_DEVICE_CONTROLS_SETTINGS = "android.settings.ACTION_DEVICE_CONTROLS_SETTINGS";
  
  public static final String ACTION_DEVICE_INFO_SETTINGS = "android.settings.DEVICE_INFO_SETTINGS";
  
  public static final String ACTION_DISPLAY_SETTINGS = "android.settings.DISPLAY_SETTINGS";
  
  public static final String ACTION_DREAM_SETTINGS = "android.settings.DREAM_SETTINGS";
  
  public static final String ACTION_ENABLE_MMS_DATA_REQUEST = "android.settings.ENABLE_MMS_DATA_REQUEST";
  
  @SystemApi
  public static final String ACTION_ENTERPRISE_PRIVACY_SETTINGS = "android.settings.ENTERPRISE_PRIVACY_SETTINGS";
  
  @Deprecated
  public static final String ACTION_FINGERPRINT_ENROLL = "android.settings.FINGERPRINT_ENROLL";
  
  public static final String ACTION_FOREGROUND_SERVICES_SETTINGS = "android.settings.FOREGROUND_SERVICES_SETTINGS";
  
  public static final String ACTION_HARD_KEYBOARD_SETTINGS = "android.settings.HARD_KEYBOARD_SETTINGS";
  
  public static final String ACTION_HOME_SETTINGS = "android.settings.HOME_SETTINGS";
  
  public static final String ACTION_IGNORE_BACKGROUND_DATA_RESTRICTIONS_SETTINGS = "android.settings.IGNORE_BACKGROUND_DATA_RESTRICTIONS_SETTINGS";
  
  public static final String ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS = "android.settings.IGNORE_BATTERY_OPTIMIZATION_SETTINGS";
  
  public static final String ACTION_INPUT_METHOD_SETTINGS = "android.settings.INPUT_METHOD_SETTINGS";
  
  public static final String ACTION_INPUT_METHOD_SUBTYPE_SETTINGS = "android.settings.INPUT_METHOD_SUBTYPE_SETTINGS";
  
  public static final String ACTION_INTERNAL_STORAGE_SETTINGS = "android.settings.INTERNAL_STORAGE_SETTINGS";
  
  public static final String ACTION_LOCALE_SETTINGS = "android.settings.LOCALE_SETTINGS";
  
  @SystemApi
  public static final String ACTION_LOCATION_CONTROLLER_EXTRA_PACKAGE_SETTINGS = "android.settings.LOCATION_CONTROLLER_EXTRA_PACKAGE_SETTINGS";
  
  public static final String ACTION_LOCATION_SCANNING_SETTINGS = "android.settings.LOCATION_SCANNING_SETTINGS";
  
  public static final String ACTION_LOCATION_SOURCE_SETTINGS = "android.settings.LOCATION_SOURCE_SETTINGS";
  
  public static final String ACTION_MANAGED_PROFILE_SETTINGS = "android.settings.MANAGED_PROFILE_SETTINGS";
  
  public static final String ACTION_MANAGE_ALL_APPLICATIONS_SETTINGS = "android.settings.MANAGE_ALL_APPLICATIONS_SETTINGS";
  
  public static final String ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION = "android.settings.MANAGE_ALL_FILES_ACCESS_PERMISSION";
  
  public static final String ACTION_MANAGE_APPLICATIONS_SETTINGS = "android.settings.MANAGE_APPLICATIONS_SETTINGS";
  
  public static final String ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION = "android.settings.MANAGE_APP_ALL_FILES_ACCESS_PERMISSION";
  
  @SystemApi
  public static final String ACTION_MANAGE_APP_OVERLAY_PERMISSION = "android.settings.MANAGE_APP_OVERLAY_PERMISSION";
  
  public static final String ACTION_MANAGE_CROSS_PROFILE_ACCESS = "android.settings.MANAGE_CROSS_PROFILE_ACCESS";
  
  public static final String ACTION_MANAGE_DEFAULT_APPS_SETTINGS = "android.settings.MANAGE_DEFAULT_APPS_SETTINGS";
  
  @SystemApi
  public static final String ACTION_MANAGE_DOMAIN_URLS = "android.settings.MANAGE_DOMAIN_URLS";
  
  @SystemApi
  public static final String ACTION_MANAGE_MORE_DEFAULT_APPS_SETTINGS = "android.settings.MANAGE_MORE_DEFAULT_APPS_SETTINGS";
  
  public static final String ACTION_MANAGE_OVERLAY_PERMISSION = "android.settings.action.MANAGE_OVERLAY_PERMISSION";
  
  public static final String ACTION_MANAGE_UNKNOWN_APP_SOURCES = "android.settings.MANAGE_UNKNOWN_APP_SOURCES";
  
  public static final String ACTION_MANAGE_WRITE_SETTINGS = "android.settings.action.MANAGE_WRITE_SETTINGS";
  
  public static final String ACTION_MEDIA_CONTROLS_SETTINGS = "android.settings.ACTION_MEDIA_CONTROLS_SETTINGS";
  
  public static final String ACTION_MEMORY_CARD_SETTINGS = "android.settings.MEMORY_CARD_SETTINGS";
  
  public static final String ACTION_MMS_MESSAGE_SETTING = "android.settings.MMS_MESSAGE_SETTING";
  
  public static final String ACTION_MOBILE_DATA_USAGE = "android.settings.MOBILE_DATA_USAGE";
  
  public static final String ACTION_MONITORING_CERT_INFO = "com.android.settings.MONITORING_CERT_INFO";
  
  public static final String ACTION_NETWORK_OPERATOR_SETTINGS = "android.settings.NETWORK_OPERATOR_SETTINGS";
  
  public static final String ACTION_NFCSHARING_SETTINGS = "android.settings.NFCSHARING_SETTINGS";
  
  public static final String ACTION_NFC_PAYMENT_SETTINGS = "android.settings.NFC_PAYMENT_SETTINGS";
  
  public static final String ACTION_NFC_SETTINGS = "android.settings.NFC_SETTINGS";
  
  public static final String ACTION_NIGHT_DISPLAY_SETTINGS = "android.settings.NIGHT_DISPLAY_SETTINGS";
  
  public static final String ACTION_NOTIFICATION_ASSISTANT_SETTINGS = "android.settings.NOTIFICATION_ASSISTANT_SETTINGS";
  
  public static final String ACTION_NOTIFICATION_HISTORY = "android.settings.NOTIFICATION_HISTORY";
  
  public static final String ACTION_NOTIFICATION_LISTENER_DETAIL_SETTINGS = "android.settings.NOTIFICATION_LISTENER_DETAIL_SETTINGS";
  
  public static final String ACTION_NOTIFICATION_LISTENER_SETTINGS = "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS";
  
  @SystemApi
  public static final String ACTION_NOTIFICATION_POLICY_ACCESS_DETAIL_SETTINGS = "android.settings.NOTIFICATION_POLICY_ACCESS_DETAIL_SETTINGS";
  
  public static final String ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS = "android.settings.NOTIFICATION_POLICY_ACCESS_SETTINGS";
  
  public static final String ACTION_NOTIFICATION_SETTINGS = "android.settings.NOTIFICATION_SETTINGS";
  
  public static final String ACTION_PAIRING_SETTINGS = "android.settings.PAIRING_SETTINGS";
  
  public static final String ACTION_PICTURE_IN_PICTURE_SETTINGS = "android.settings.PICTURE_IN_PICTURE_SETTINGS";
  
  public static final String ACTION_POWER_MENU_SETTINGS = "android.settings.ACTION_POWER_MENU_SETTINGS";
  
  public static final String ACTION_PRINT_SETTINGS = "android.settings.ACTION_PRINT_SETTINGS";
  
  public static final String ACTION_PRIVACY_SETTINGS = "android.settings.PRIVACY_SETTINGS";
  
  public static final String ACTION_PROCESS_WIFI_EASY_CONNECT_URI = "android.settings.PROCESS_WIFI_EASY_CONNECT_URI";
  
  public static final String ACTION_QUICK_ACCESS_WALLET_SETTINGS = "android.settings.QUICK_ACCESS_WALLET_SETTINGS";
  
  public static final String ACTION_QUICK_LAUNCH_SETTINGS = "android.settings.QUICK_LAUNCH_SETTINGS";
  
  @SystemApi
  public static final String ACTION_REQUEST_ENABLE_CONTENT_CAPTURE = "android.settings.REQUEST_ENABLE_CONTENT_CAPTURE";
  
  public static final String ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS = "android.settings.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS";
  
  public static final String ACTION_REQUEST_SET_AUTOFILL_SERVICE = "android.settings.REQUEST_SET_AUTOFILL_SERVICE";
  
  public static final String ACTION_SEARCH_SETTINGS = "android.search.action.SEARCH_SETTINGS";
  
  public static final String ACTION_SECURITY_SETTINGS = "android.settings.SECURITY_SETTINGS";
  
  public static final String ACTION_SETTINGS = "android.settings.SETTINGS";
  
  @SystemApi
  public static final String ACTION_SHOW_ADMIN_SUPPORT_DETAILS = "android.settings.SHOW_ADMIN_SUPPORT_DETAILS";
  
  public static final String ACTION_SHOW_REGULATORY_INFO = "android.settings.SHOW_REGULATORY_INFO";
  
  public static final String ACTION_SHOW_REMOTE_BUGREPORT_DIALOG = "android.settings.SHOW_REMOTE_BUGREPORT_DIALOG";
  
  public static final String ACTION_SHOW_WORK_POLICY_INFO = "android.settings.SHOW_WORK_POLICY_INFO";
  
  public static final String ACTION_SOUND_SETTINGS = "android.settings.SOUND_SETTINGS";
  
  public static final String ACTION_STORAGE_MANAGER_SETTINGS = "android.settings.STORAGE_MANAGER_SETTINGS";
  
  @Deprecated
  public static final String ACTION_STORAGE_VOLUME_ACCESS_SETTINGS = "android.settings.STORAGE_VOLUME_ACCESS_SETTINGS";
  
  public static final String ACTION_SYNC_SETTINGS = "android.settings.SYNC_SETTINGS";
  
  public static final String ACTION_SYSTEM_UPDATE_SETTINGS = "android.settings.SYSTEM_UPDATE_SETTINGS";
  
  @SystemApi
  public static final String ACTION_TETHER_PROVISIONING_UI = "android.settings.TETHER_PROVISIONING_UI";
  
  @SystemApi
  public static final String ACTION_TETHER_SETTINGS = "android.settings.TETHER_SETTINGS";
  
  public static final String ACTION_TRUSTED_CREDENTIALS_USER = "com.android.settings.TRUSTED_CREDENTIALS_USER";
  
  public static final String ACTION_USAGE_ACCESS_SETTINGS = "android.settings.USAGE_ACCESS_SETTINGS";
  
  public static final String ACTION_USER_DICTIONARY_INSERT = "com.android.settings.USER_DICTIONARY_INSERT";
  
  public static final String ACTION_USER_DICTIONARY_SETTINGS = "android.settings.USER_DICTIONARY_SETTINGS";
  
  public static final String ACTION_USER_SETTINGS = "android.settings.USER_SETTINGS";
  
  public static final String ACTION_VIEW_ADVANCED_POWER_USAGE_DETAIL = "android.settings.VIEW_ADVANCED_POWER_USAGE_DETAIL";
  
  public static final String ACTION_VOICE_CONTROL_AIRPLANE_MODE = "android.settings.VOICE_CONTROL_AIRPLANE_MODE";
  
  public static final String ACTION_VOICE_CONTROL_BATTERY_SAVER_MODE = "android.settings.VOICE_CONTROL_BATTERY_SAVER_MODE";
  
  public static final String ACTION_VOICE_CONTROL_DO_NOT_DISTURB_MODE = "android.settings.VOICE_CONTROL_DO_NOT_DISTURB_MODE";
  
  public static final String ACTION_VOICE_INPUT_SETTINGS = "android.settings.VOICE_INPUT_SETTINGS";
  
  public static final String ACTION_VPN_SETTINGS = "android.settings.VPN_SETTINGS";
  
  public static final String ACTION_VR_LISTENER_SETTINGS = "android.settings.VR_LISTENER_SETTINGS";
  
  public static final String ACTION_WEBVIEW_SETTINGS = "android.settings.WEBVIEW_SETTINGS";
  
  public static final String ACTION_WIFI_ADD_NETWORKS = "android.settings.WIFI_ADD_NETWORKS";
  
  public static final String ACTION_WIFI_IP_SETTINGS = "android.settings.WIFI_IP_SETTINGS";
  
  public static final String ACTION_WIFI_SETTINGS = "android.settings.WIFI_SETTINGS";
  
  public static final String ACTION_WIRELESS_SETTINGS = "android.settings.WIRELESS_SETTINGS";
  
  public static final String ACTION_ZEN_MODE_AUTOMATION_SETTINGS = "android.settings.ZEN_MODE_AUTOMATION_SETTINGS";
  
  public static final String ACTION_ZEN_MODE_EVENT_RULE_SETTINGS = "android.settings.ZEN_MODE_EVENT_RULE_SETTINGS";
  
  public static final String ACTION_ZEN_MODE_EXTERNAL_RULE_SETTINGS = "android.settings.ZEN_MODE_EXTERNAL_RULE_SETTINGS";
  
  public static final String ACTION_ZEN_MODE_PRIORITY_SETTINGS = "android.settings.ZEN_MODE_PRIORITY_SETTINGS";
  
  public static final String ACTION_ZEN_MODE_SCHEDULE_RULE_SETTINGS = "android.settings.ZEN_MODE_SCHEDULE_RULE_SETTINGS";
  
  public static final String ACTION_ZEN_MODE_SETTINGS = "android.settings.ZEN_MODE_SETTINGS";
  
  public static final int ADD_WIFI_RESULT_ADD_OR_UPDATE_FAILED = 1;
  
  public static final int ADD_WIFI_RESULT_ALREADY_EXISTS = 2;
  
  public static final int ADD_WIFI_RESULT_SUCCESS = 0;
  
  public static final String AUTHORITY = "settings";
  
  public static final String CALL_METHOD_DELETE_CONFIG = "DELETE_config";
  
  public static final String CALL_METHOD_DELETE_GLOBAL = "DELETE_global";
  
  public static final String CALL_METHOD_DELETE_SECURE = "DELETE_secure";
  
  public static final String CALL_METHOD_DELETE_SYSTEM = "DELETE_system";
  
  public static final String CALL_METHOD_FLAGS_KEY = "_flags";
  
  public static final String CALL_METHOD_GENERATION_INDEX_KEY = "_generation_index";
  
  public static final String CALL_METHOD_GENERATION_KEY = "_generation";
  
  public static final String CALL_METHOD_GET_CONFIG = "GET_config";
  
  public static final String CALL_METHOD_GET_GLOBAL = "GET_global";
  
  public static final String CALL_METHOD_GET_SECURE = "GET_secure";
  
  public static final String CALL_METHOD_GET_SYSTEM = "GET_system";
  
  public static final String CALL_METHOD_LIST_CONFIG = "LIST_config";
  
  public static final String CALL_METHOD_LIST_GLOBAL = "LIST_global";
  
  public static final String CALL_METHOD_LIST_SECURE = "LIST_secure";
  
  public static final String CALL_METHOD_LIST_SYSTEM = "LIST_system";
  
  public static final String CALL_METHOD_MAKE_DEFAULT_KEY = "_make_default";
  
  public static final String CALL_METHOD_MONITOR_CALLBACK_KEY = "_monitor_callback_key";
  
  public static final String CALL_METHOD_OVERRIDEABLE_BY_RESTORE_KEY = "_overrideable_by_restore";
  
  public static final String CALL_METHOD_PREFIX_KEY = "_prefix";
  
  public static final String CALL_METHOD_PUT_CONFIG = "PUT_config";
  
  public static final String CALL_METHOD_PUT_GLOBAL = "PUT_global";
  
  public static final String CALL_METHOD_PUT_SECURE = "PUT_secure";
  
  public static final String CALL_METHOD_PUT_SYSTEM = "PUT_system";
  
  public static final String CALL_METHOD_REGISTER_MONITOR_CALLBACK_CONFIG = "REGISTER_MONITOR_CALLBACK_config";
  
  public static final String CALL_METHOD_RESET_CONFIG = "RESET_config";
  
  public static final String CALL_METHOD_RESET_GLOBAL = "RESET_global";
  
  public static final String CALL_METHOD_RESET_MODE_KEY = "_reset_mode";
  
  public static final String CALL_METHOD_RESET_SECURE = "RESET_secure";
  
  public static final String CALL_METHOD_SET_ALL_CONFIG = "SET_ALL_config";
  
  public static final String CALL_METHOD_TAG_KEY = "_tag";
  
  public static final String CALL_METHOD_TRACK_GENERATION_KEY = "_track_generation";
  
  public static final String CALL_METHOD_USER_KEY = "_user";
  
  public static final boolean DEFAULT_OVERRIDEABLE_BY_RESTORE = false;
  
  public static final String DEVICE_NAME_SETTINGS = "android.settings.DEVICE_NAME";
  
  public static final int ENABLE_MMS_DATA_REQUEST_REASON_INCOMING_MMS = 0;
  
  public static final int ENABLE_MMS_DATA_REQUEST_REASON_OUTGOING_MMS = 1;
  
  public static final String EXTRA_ACCESS_CALLBACK = "access_callback";
  
  public static final String EXTRA_ACCOUNT_TYPES = "account_types";
  
  public static final String EXTRA_AIRPLANE_MODE_ENABLED = "airplane_mode_enabled";
  
  public static final String EXTRA_APP_PACKAGE = "android.provider.extra.APP_PACKAGE";
  
  public static final String EXTRA_APP_UID = "app_uid";
  
  public static final String EXTRA_AUTHORITIES = "authorities";
  
  public static final String EXTRA_BATTERY_SAVER_MODE_ENABLED = "android.settings.extra.battery_saver_mode_enabled";
  
  public static final String EXTRA_BIOMETRIC_AUTHENTICATORS_ALLOWED = "android.provider.extra.BIOMETRIC_AUTHENTICATORS_ALLOWED";
  
  public static final String EXTRA_CALLING_PACKAGE = "calling_package";
  
  public static final String EXTRA_CHANNEL_ID = "android.provider.extra.CHANNEL_ID";
  
  public static final String EXTRA_CONVERSATION_ID = "android.provider.extra.CONVERSATION_ID";
  
  public static final String EXTRA_DO_NOT_DISTURB_MODE_ENABLED = "android.settings.extra.do_not_disturb_mode_enabled";
  
  public static final String EXTRA_DO_NOT_DISTURB_MODE_MINUTES = "android.settings.extra.do_not_disturb_mode_minutes";
  
  public static final String EXTRA_EASY_CONNECT_ATTEMPTED_SSID = "android.provider.extra.EASY_CONNECT_ATTEMPTED_SSID";
  
  public static final String EXTRA_EASY_CONNECT_BAND_LIST = "android.provider.extra.EASY_CONNECT_BAND_LIST";
  
  public static final String EXTRA_EASY_CONNECT_CHANNEL_LIST = "android.provider.extra.EASY_CONNECT_CHANNEL_LIST";
  
  public static final String EXTRA_EASY_CONNECT_ERROR_CODE = "android.provider.extra.EASY_CONNECT_ERROR_CODE";
  
  public static final String EXTRA_ENABLE_MMS_DATA_REQUEST_REASON = "android.settings.extra.ENABLE_MMS_DATA_REQUEST_REASON";
  
  public static final String EXTRA_INPUT_DEVICE_IDENTIFIER = "input_device_identifier";
  
  public static final String EXTRA_INPUT_METHOD_ID = "input_method_id";
  
  public static final String EXTRA_MONITOR_CALLBACK_TYPE = "monitor_callback_type";
  
  public static final String EXTRA_NAMESPACE = "namespace";
  
  public static final String EXTRA_NAMESPACE_UPDATED_CALLBACK = "namespace_updated_callback";
  
  public static final String EXTRA_NETWORK_TEMPLATE = "network_template";
  
  public static final String EXTRA_NOTIFICATION_LISTENER_COMPONENT_NAME = "android.provider.extra.NOTIFICATION_LISTENER_COMPONENT_NAME";
  
  public static final String EXTRA_NUMBER_OF_CERTIFICATES = "android.settings.extra.number_of_certificates";
  
  public static final String EXTRA_SUB_ID = "android.provider.extra.SUB_ID";
  
  public static final String EXTRA_WIFI_NETWORK_LIST = "android.provider.extra.WIFI_NETWORK_LIST";
  
  public static final String EXTRA_WIFI_NETWORK_RESULT_LIST = "android.provider.extra.WIFI_NETWORK_RESULT_LIST";
  
  public static final String INTENT_CATEGORY_USAGE_ACCESS_CONFIG = "android.intent.category.USAGE_ACCESS_CONFIG";
  
  private static final String JID_RESOURCE_PREFIX = "android";
  
  public static final String KEY_CONFIG_SET_RETURN = "config_set_return";
  
  private static final boolean LOCAL_LOGV = false;
  
  public static final String METADATA_USAGE_ACCESS_REASON = "android.settings.metadata.USAGE_ACCESS_REASON";
  
  private static final String[] PM_CHANGE_NETWORK_STATE;
  
  private static final String[] PM_SYSTEM_ALERT_WINDOW;
  
  private static final String[] PM_WRITE_SETTINGS;
  
  public static final int RESET_MODE_PACKAGE_DEFAULTS = 1;
  
  public static final int RESET_MODE_TRUSTED_DEFAULTS = 4;
  
  public static final int RESET_MODE_UNTRUSTED_CHANGES = 3;
  
  public static final int RESET_MODE_UNTRUSTED_DEFAULTS = 2;
  
  private static final String TAG = "Settings";
  
  public static final String ZEN_MODE_BLOCKED_EFFECTS_SETTINGS = "android.settings.ZEN_MODE_BLOCKED_EFFECTS_SETTINGS";
  
  public static final String ZEN_MODE_ONBOARDING = "android.settings.ZEN_MODE_ONBOARDING";
  
  private static boolean sInSystemServer = false;
  
  private static final Object sInSystemServerLock = new Object();
  
  public static void setInSystemServer() {
    synchronized (sInSystemServerLock) {
      sInSystemServer = true;
      return;
    } 
  }
  
  public static boolean isInSystemServer() {
    synchronized (sInSystemServerLock) {
      return sInSystemServer;
    } 
  }
  
  class SettingNotFoundException extends AndroidException {
    public SettingNotFoundException(Settings this$0) {
      super((String)this$0);
    }
  }
  
  class NameValueTable implements BaseColumns {
    public static final String IS_PRESERVED_IN_RESTORE = "is_preserved_in_restore";
    
    public static final String NAME = "name";
    
    public static final String VALUE = "value";
    
    protected static boolean putString(ContentResolver param1ContentResolver, Uri param1Uri, String param1String1, String param1String2) {
      try {
        ContentValues contentValues = new ContentValues();
        this();
        contentValues.put("name", param1String1);
        contentValues.put("value", param1String2);
        param1ContentResolver.insert(param1Uri, contentValues);
        return true;
      } catch (SQLException sQLException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Can't set key ");
        stringBuilder.append(param1String1);
        stringBuilder.append(" in ");
        stringBuilder.append(param1Uri);
        Log.w("Settings", stringBuilder.toString(), (Throwable)sQLException);
        return false;
      } 
    }
    
    public static Uri getUriFor(Uri param1Uri, String param1String) {
      return Uri.withAppendedPath(param1Uri, param1String);
    }
  }
  
  private static final class GenerationTracker {
    private final MemoryIntArray mArray;
    
    private int mCurrentGeneration;
    
    private final Runnable mErrorHandler;
    
    private final int mIndex;
    
    public GenerationTracker(MemoryIntArray param1MemoryIntArray, int param1Int1, int param1Int2, Runnable param1Runnable) {
      this.mArray = param1MemoryIntArray;
      this.mIndex = param1Int1;
      this.mErrorHandler = param1Runnable;
      this.mCurrentGeneration = param1Int2;
    }
    
    public boolean isGenerationChanged() {
      int i = readCurrentGeneration();
      if (i >= 0) {
        if (i == this.mCurrentGeneration)
          return false; 
        this.mCurrentGeneration = i;
      } 
      return true;
    }
    
    public int getCurrentGeneration() {
      return this.mCurrentGeneration;
    }
    
    private int readCurrentGeneration() {
      try {
        return this.mArray.get(this.mIndex);
      } catch (IOException iOException) {
        Log.e("Settings", "Error getting current generation", iOException);
        Runnable runnable = this.mErrorHandler;
        if (runnable != null)
          runnable.run(); 
        return -1;
      } 
    }
    
    public void destroy() {
      try {
        this.mArray.close();
      } catch (IOException iOException) {
        Log.e("Settings", "Error closing backing array", iOException);
        Runnable runnable = this.mErrorHandler;
        if (runnable != null)
          runnable.run(); 
      } 
    }
  }
  
  private static final class ContentProviderHolder {
    private IContentProvider mContentProvider;
    
    private final Object mLock = new Object();
    
    private final Uri mUri;
    
    public ContentProviderHolder(Uri param1Uri) {
      this.mUri = param1Uri;
    }
    
    public IContentProvider getProvider(ContentResolver param1ContentResolver) {
      synchronized (this.mLock) {
        if (this.mContentProvider == null) {
          Uri uri = this.mUri;
          this.mContentProvider = param1ContentResolver.acquireProvider(uri.getAuthority());
        } 
        return this.mContentProvider;
      } 
    }
    
    public void clearProviderForTest() {
      synchronized (this.mLock) {
        this.mContentProvider = null;
        return;
      } 
    }
  }
  
  private static class NameValueCache {
    private static final boolean DEBUG = false;
    
    private static final String NAME_EQ_PLACEHOLDER = "name=?";
    
    private static final String[] SELECT_VALUE_PROJECTION = new String[] { "value" };
    
    private final String mCallGetCommand;
    
    private final String mCallListCommand;
    
    private final String mCallSetAllCommand;
    
    private final String mCallSetCommand;
    
    private Settings.GenerationTracker mGenerationTracker;
    
    private final Settings.ContentProviderHolder mProviderHolder;
    
    private final Uri mUri;
    
    private final ArrayMap<String, String> mValues = new ArrayMap();
    
    public NameValueCache(Uri param1Uri, String param1String1, String param1String2, Settings.ContentProviderHolder param1ContentProviderHolder) {
      this(param1Uri, param1String1, param1String2, null, null, param1ContentProviderHolder);
    }
    
    NameValueCache(Uri param1Uri, String param1String1, String param1String2, String param1String3, String param1String4, Settings.ContentProviderHolder param1ContentProviderHolder) {
      this.mUri = param1Uri;
      this.mCallGetCommand = param1String1;
      this.mCallSetCommand = param1String2;
      this.mCallListCommand = param1String3;
      this.mCallSetAllCommand = param1String4;
      this.mProviderHolder = param1ContentProviderHolder;
    }
    
    public boolean putStringForUser(ContentResolver param1ContentResolver, String param1String1, String param1String2, String param1String3, boolean param1Boolean1, int param1Int, boolean param1Boolean2) {
      try {
        Bundle bundle = new Bundle();
        this();
        try {
          bundle.putString("value", param1String2);
          try {
            bundle.putInt("_user", param1Int);
            if (param1String3 != null)
              bundle.putString("_tag", param1String3); 
            if (param1Boolean1)
              bundle.putBoolean("_make_default", true); 
            if (param1Boolean2)
              bundle.putBoolean("_overrideable_by_restore", true); 
            Settings.ContentProviderHolder contentProviderHolder = this.mProviderHolder;
            try {
              IContentProvider iContentProvider = contentProviderHolder.getProvider(param1ContentResolver);
              String str2 = param1ContentResolver.getPackageName(), str1 = param1ContentResolver.getAttributionTag();
              Settings.ContentProviderHolder contentProviderHolder1 = this.mProviderHolder;
              String str4 = contentProviderHolder1.mUri.getAuthority(), str3 = this.mCallSetCommand;
              iContentProvider.call(str2, str1, str4, str3, param1String1, bundle);
              return true;
            } catch (RemoteException null) {}
          } catch (RemoteException null) {}
        } catch (RemoteException null) {}
      } catch (RemoteException remoteException) {}
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Can't set key ");
      stringBuilder.append(param1String1);
      stringBuilder.append(" in ");
      stringBuilder.append(this.mUri);
      Log.w("Settings", stringBuilder.toString(), (Throwable)remoteException);
      return false;
    }
    
    public boolean setStringsForPrefix(ContentResolver param1ContentResolver, String param1String, HashMap<String, String> param1HashMap) {
      if (this.mCallSetAllCommand == null)
        return false; 
      try {
        Bundle bundle2 = new Bundle();
        this();
        bundle2.putString("_prefix", param1String);
        bundle2.putSerializable("_flags", param1HashMap);
        IContentProvider iContentProvider = this.mProviderHolder.getProvider(param1ContentResolver);
        String str2 = param1ContentResolver.getPackageName(), str1 = param1ContentResolver.getAttributionTag();
        Settings.ContentProviderHolder contentProviderHolder = this.mProviderHolder;
        String str4 = contentProviderHolder.mUri.getAuthority(), str3 = this.mCallSetAllCommand;
        Bundle bundle1 = iContentProvider.call(str2, str1, str4, str3, null, bundle2);
        return bundle1.getBoolean("config_set_return");
      } catch (RemoteException remoteException) {
        return false;
      } 
    }
    
    public String getStringForUser(ContentResolver param1ContentResolver, String param1String, int param1Int) {
      // Byte code:
      //   0: iload_3
      //   1: invokestatic myUserId : ()I
      //   4: if_icmpne -> 13
      //   7: iconst_1
      //   8: istore #4
      //   10: goto -> 16
      //   13: iconst_0
      //   14: istore #4
      //   16: iconst_m1
      //   17: istore #5
      //   19: iload #4
      //   21: ifeq -> 174
      //   24: aload_0
      //   25: monitorenter
      //   26: aload_0
      //   27: getfield mGenerationTracker : Landroid/provider/Settings$GenerationTracker;
      //   30: astore #6
      //   32: iload #5
      //   34: istore #7
      //   36: aload #6
      //   38: ifnull -> 164
      //   41: aload_0
      //   42: getfield mGenerationTracker : Landroid/provider/Settings$GenerationTracker;
      //   45: invokevirtual isGenerationChanged : ()Z
      //   48: ifeq -> 61
      //   51: aload_0
      //   52: getfield mValues : Landroid/util/ArrayMap;
      //   55: invokevirtual clear : ()V
      //   58: goto -> 90
      //   61: aload_0
      //   62: getfield mValues : Landroid/util/ArrayMap;
      //   65: aload_2
      //   66: invokevirtual containsKey : (Ljava/lang/Object;)Z
      //   69: ifeq -> 90
      //   72: aload_0
      //   73: getfield mValues : Landroid/util/ArrayMap;
      //   76: aload_2
      //   77: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   80: checkcast java/lang/String
      //   83: astore #6
      //   85: aload_0
      //   86: monitorexit
      //   87: aload #6
      //   89: areturn
      //   90: iload #5
      //   92: istore #7
      //   94: aload_0
      //   95: getfield mGenerationTracker : Landroid/provider/Settings$GenerationTracker;
      //   98: ifnull -> 110
      //   101: aload_0
      //   102: getfield mGenerationTracker : Landroid/provider/Settings$GenerationTracker;
      //   105: invokevirtual getCurrentGeneration : ()I
      //   108: istore #7
      //   110: goto -> 164
      //   113: astore #8
      //   115: new java/lang/StringBuilder
      //   118: astore #6
      //   120: aload #6
      //   122: invokespecial <init> : ()V
      //   125: aload #6
      //   127: ldc 'Setting: getStringForUser FD closed by unknown reason, '
      //   129: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   132: pop
      //   133: aload #6
      //   135: aload #8
      //   137: invokevirtual getMessage : ()Ljava/lang/String;
      //   140: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   143: pop
      //   144: ldc 'Settings'
      //   146: aload #6
      //   148: invokevirtual toString : ()Ljava/lang/String;
      //   151: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   154: pop
      //   155: aload_0
      //   156: aconst_null
      //   157: putfield mGenerationTracker : Landroid/provider/Settings$GenerationTracker;
      //   160: iload #5
      //   162: istore #7
      //   164: aload_0
      //   165: monitorexit
      //   166: goto -> 177
      //   169: astore_1
      //   170: aload_0
      //   171: monitorexit
      //   172: aload_1
      //   173: athrow
      //   174: iconst_m1
      //   175: istore #7
      //   177: aload_0
      //   178: getfield mProviderHolder : Landroid/provider/Settings$ContentProviderHolder;
      //   181: aload_1
      //   182: invokevirtual getProvider : (Landroid/content/ContentResolver;)Landroid/content/IContentProvider;
      //   185: astore #9
      //   187: aload #9
      //   189: ifnonnull -> 228
      //   192: new java/lang/StringBuilder
      //   195: dup
      //   196: invokespecial <init> : ()V
      //   199: astore_1
      //   200: aload_1
      //   201: ldc 'Can't get provider for '
      //   203: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   206: pop
      //   207: aload_1
      //   208: aload_0
      //   209: getfield mUri : Landroid/net/Uri;
      //   212: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   215: pop
      //   216: ldc 'Settings'
      //   218: aload_1
      //   219: invokevirtual toString : ()Ljava/lang/String;
      //   222: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   225: pop
      //   226: aconst_null
      //   227: areturn
      //   228: iload #7
      //   230: istore #5
      //   232: aload_0
      //   233: getfield mCallGetCommand : Ljava/lang/String;
      //   236: ifnull -> 812
      //   239: iload #4
      //   241: ifne -> 277
      //   244: iload #7
      //   246: istore #5
      //   248: new android/os/Bundle
      //   251: astore #6
      //   253: iload #7
      //   255: istore #5
      //   257: aload #6
      //   259: invokespecial <init> : ()V
      //   262: iload #7
      //   264: istore #5
      //   266: aload #6
      //   268: ldc '_user'
      //   270: iload_3
      //   271: invokevirtual putInt : (Ljava/lang/String;I)V
      //   274: goto -> 280
      //   277: aconst_null
      //   278: astore #6
      //   280: iload #7
      //   282: istore #5
      //   284: aload_0
      //   285: monitorenter
      //   286: iload #4
      //   288: ifeq -> 355
      //   291: iload #7
      //   293: istore_3
      //   294: aload_0
      //   295: getfield mGenerationTracker : Landroid/provider/Settings$GenerationTracker;
      //   298: ifnonnull -> 355
      //   301: aload #6
      //   303: astore #8
      //   305: aload #6
      //   307: ifnonnull -> 326
      //   310: iload #7
      //   312: istore_3
      //   313: new android/os/Bundle
      //   316: astore #8
      //   318: iload #7
      //   320: istore_3
      //   321: aload #8
      //   323: invokespecial <init> : ()V
      //   326: iload #7
      //   328: istore_3
      //   329: aload #8
      //   331: ldc '_track_generation'
      //   333: aconst_null
      //   334: invokevirtual putString : (Ljava/lang/String;Ljava/lang/String;)V
      //   337: aload #8
      //   339: astore #6
      //   341: iconst_1
      //   342: istore #10
      //   344: goto -> 358
      //   347: astore #6
      //   349: iload_3
      //   350: istore #7
      //   352: goto -> 798
      //   355: iconst_0
      //   356: istore #10
      //   358: aload_0
      //   359: monitorexit
      //   360: iload #7
      //   362: istore #5
      //   364: invokestatic isInSystemServer : ()Z
      //   367: ifeq -> 476
      //   370: iload #7
      //   372: istore #5
      //   374: invokestatic getCallingUid : ()I
      //   377: invokestatic myUid : ()I
      //   380: if_icmpeq -> 476
      //   383: iload #7
      //   385: istore #5
      //   387: invokestatic clearCallingIdentity : ()J
      //   390: lstore #11
      //   392: aload_1
      //   393: invokevirtual getPackageName : ()Ljava/lang/String;
      //   396: astore #8
      //   398: aload_1
      //   399: invokevirtual getAttributionTag : ()Ljava/lang/String;
      //   402: astore #13
      //   404: aload_0
      //   405: getfield mProviderHolder : Landroid/provider/Settings$ContentProviderHolder;
      //   408: astore #14
      //   410: aload #14
      //   412: invokestatic access$000 : (Landroid/provider/Settings$ContentProviderHolder;)Landroid/net/Uri;
      //   415: invokevirtual getAuthority : ()Ljava/lang/String;
      //   418: astore #14
      //   420: aload_0
      //   421: getfield mCallGetCommand : Ljava/lang/String;
      //   424: astore #15
      //   426: aload #9
      //   428: aload #8
      //   430: aload #13
      //   432: aload #14
      //   434: aload #15
      //   436: aload_2
      //   437: aload #6
      //   439: invokeinterface call : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
      //   444: astore #6
      //   446: iload #7
      //   448: istore #5
      //   450: lload #11
      //   452: invokestatic restoreCallingIdentity : (J)V
      //   455: goto -> 554
      //   458: astore #6
      //   460: iload #7
      //   462: istore #5
      //   464: lload #11
      //   466: invokestatic restoreCallingIdentity : (J)V
      //   469: iload #7
      //   471: istore #5
      //   473: aload #6
      //   475: athrow
      //   476: iload #7
      //   478: istore #5
      //   480: aload_1
      //   481: invokevirtual getPackageName : ()Ljava/lang/String;
      //   484: astore #13
      //   486: iload #7
      //   488: istore #5
      //   490: aload_1
      //   491: invokevirtual getAttributionTag : ()Ljava/lang/String;
      //   494: astore #8
      //   496: iload #7
      //   498: istore #5
      //   500: aload_0
      //   501: getfield mProviderHolder : Landroid/provider/Settings$ContentProviderHolder;
      //   504: astore #14
      //   506: iload #7
      //   508: istore #5
      //   510: aload #14
      //   512: invokestatic access$000 : (Landroid/provider/Settings$ContentProviderHolder;)Landroid/net/Uri;
      //   515: invokevirtual getAuthority : ()Ljava/lang/String;
      //   518: astore #14
      //   520: iload #7
      //   522: istore #5
      //   524: aload_0
      //   525: getfield mCallGetCommand : Ljava/lang/String;
      //   528: astore #15
      //   530: iload #7
      //   532: istore #5
      //   534: aload #9
      //   536: aload #13
      //   538: aload #8
      //   540: aload #14
      //   542: aload #15
      //   544: aload_2
      //   545: aload #6
      //   547: invokeinterface call : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
      //   552: astore #6
      //   554: aload #6
      //   556: ifnull -> 789
      //   559: iload #7
      //   561: istore #5
      //   563: aload #6
      //   565: ldc 'value'
      //   567: invokevirtual getString : (Ljava/lang/String;)Ljava/lang/String;
      //   570: astore #8
      //   572: iload #4
      //   574: ifeq -> 786
      //   577: iload #7
      //   579: istore #5
      //   581: aload_0
      //   582: monitorenter
      //   583: iload #7
      //   585: istore_3
      //   586: iload #10
      //   588: ifeq -> 726
      //   591: iload #7
      //   593: istore #5
      //   595: aload #6
      //   597: ldc '_track_generation'
      //   599: invokevirtual getParcelable : (Ljava/lang/String;)Landroid/os/Parcelable;
      //   602: checkcast android/util/MemoryIntArray
      //   605: astore #13
      //   607: iload #7
      //   609: istore #5
      //   611: aload #6
      //   613: ldc '_generation_index'
      //   615: iconst_m1
      //   616: invokevirtual getInt : (Ljava/lang/String;I)I
      //   619: istore #4
      //   621: iload #7
      //   623: istore_3
      //   624: aload #13
      //   626: ifnull -> 726
      //   629: iload #7
      //   631: istore_3
      //   632: iload #4
      //   634: iflt -> 726
      //   637: iload #7
      //   639: istore #5
      //   641: aload #6
      //   643: ldc '_generation'
      //   645: iconst_0
      //   646: invokevirtual getInt : (Ljava/lang/String;I)I
      //   649: istore_3
      //   650: iload #7
      //   652: istore #5
      //   654: aload_0
      //   655: getfield mGenerationTracker : Landroid/provider/Settings$GenerationTracker;
      //   658: ifnull -> 672
      //   661: iload #7
      //   663: istore #5
      //   665: aload_0
      //   666: getfield mGenerationTracker : Landroid/provider/Settings$GenerationTracker;
      //   669: invokevirtual destroy : ()V
      //   672: iload #7
      //   674: istore #5
      //   676: new android/provider/Settings$GenerationTracker
      //   679: astore #14
      //   681: iload #7
      //   683: istore #5
      //   685: new android/provider/_$$Lambda$Settings$NameValueCache$qSyMM6rUAHCa_5rsP_atfAqR3sA
      //   688: astore #6
      //   690: iload #7
      //   692: istore #5
      //   694: aload #6
      //   696: aload_0
      //   697: invokespecial <init> : (Landroid/provider/Settings$NameValueCache;)V
      //   700: iload #7
      //   702: istore #5
      //   704: aload #14
      //   706: aload #13
      //   708: iload #4
      //   710: iload_3
      //   711: aload #6
      //   713: invokespecial <init> : (Landroid/util/MemoryIntArray;IILjava/lang/Runnable;)V
      //   716: iload #7
      //   718: istore #5
      //   720: aload_0
      //   721: aload #14
      //   723: putfield mGenerationTracker : Landroid/provider/Settings$GenerationTracker;
      //   726: iload_3
      //   727: istore #5
      //   729: aload_0
      //   730: getfield mGenerationTracker : Landroid/provider/Settings$GenerationTracker;
      //   733: ifnull -> 771
      //   736: iload_3
      //   737: istore #5
      //   739: aload_0
      //   740: getfield mGenerationTracker : Landroid/provider/Settings$GenerationTracker;
      //   743: astore #6
      //   745: iload_3
      //   746: istore #5
      //   748: iload_3
      //   749: aload #6
      //   751: invokevirtual getCurrentGeneration : ()I
      //   754: if_icmpne -> 771
      //   757: iload_3
      //   758: istore #5
      //   760: aload_0
      //   761: getfield mValues : Landroid/util/ArrayMap;
      //   764: aload_2
      //   765: aload #8
      //   767: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   770: pop
      //   771: iload_3
      //   772: istore #5
      //   774: aload_0
      //   775: monitorexit
      //   776: goto -> 786
      //   779: astore #6
      //   781: aload_0
      //   782: monitorexit
      //   783: aload #6
      //   785: athrow
      //   786: aload #8
      //   788: areturn
      //   789: iload #7
      //   791: istore #5
      //   793: goto -> 812
      //   796: astore #6
      //   798: iload #7
      //   800: istore_3
      //   801: aload_0
      //   802: monitorexit
      //   803: iload #7
      //   805: istore #5
      //   807: aload #6
      //   809: athrow
      //   810: astore #6
      //   812: aconst_null
      //   813: astore #13
      //   815: aconst_null
      //   816: astore #14
      //   818: aload #14
      //   820: astore #6
      //   822: aload #13
      //   824: astore #8
      //   826: ldc 'name=?'
      //   828: iconst_1
      //   829: anewarray java/lang/String
      //   832: dup
      //   833: iconst_0
      //   834: aload_2
      //   835: aastore
      //   836: aconst_null
      //   837: invokestatic createSqlQueryBundle : (Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
      //   840: astore #15
      //   842: aload #14
      //   844: astore #6
      //   846: aload #13
      //   848: astore #8
      //   850: invokestatic isInSystemServer : ()Z
      //   853: ifeq -> 950
      //   856: aload #14
      //   858: astore #6
      //   860: aload #13
      //   862: astore #8
      //   864: invokestatic getCallingUid : ()I
      //   867: invokestatic myUid : ()I
      //   870: if_icmpeq -> 950
      //   873: aload #14
      //   875: astore #6
      //   877: aload #13
      //   879: astore #8
      //   881: invokestatic clearCallingIdentity : ()J
      //   884: lstore #11
      //   886: aload #9
      //   888: aload_1
      //   889: invokevirtual getPackageName : ()Ljava/lang/String;
      //   892: aload_1
      //   893: invokevirtual getAttributionTag : ()Ljava/lang/String;
      //   896: aload_0
      //   897: getfield mUri : Landroid/net/Uri;
      //   900: getstatic android/provider/Settings$NameValueCache.SELECT_VALUE_PROJECTION : [Ljava/lang/String;
      //   903: aload #15
      //   905: aconst_null
      //   906: invokeinterface query : (Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;[Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;
      //   911: astore_1
      //   912: aload_1
      //   913: astore #6
      //   915: aload_1
      //   916: astore #8
      //   918: lload #11
      //   920: invokestatic restoreCallingIdentity : (J)V
      //   923: goto -> 984
      //   926: astore_1
      //   927: aload #14
      //   929: astore #6
      //   931: aload #13
      //   933: astore #8
      //   935: lload #11
      //   937: invokestatic restoreCallingIdentity : (J)V
      //   940: aload #14
      //   942: astore #6
      //   944: aload #13
      //   946: astore #8
      //   948: aload_1
      //   949: athrow
      //   950: aload #14
      //   952: astore #6
      //   954: aload #13
      //   956: astore #8
      //   958: aload #9
      //   960: aload_1
      //   961: invokevirtual getPackageName : ()Ljava/lang/String;
      //   964: aload_1
      //   965: invokevirtual getAttributionTag : ()Ljava/lang/String;
      //   968: aload_0
      //   969: getfield mUri : Landroid/net/Uri;
      //   972: getstatic android/provider/Settings$NameValueCache.SELECT_VALUE_PROJECTION : [Ljava/lang/String;
      //   975: aload #15
      //   977: aconst_null
      //   978: invokeinterface query : (Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;[Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;
      //   983: astore_1
      //   984: aload_1
      //   985: ifnonnull -> 1054
      //   988: new java/lang/StringBuilder
      //   991: astore #6
      //   993: aload #6
      //   995: invokespecial <init> : ()V
      //   998: aload #6
      //   1000: ldc 'Can't get key '
      //   1002: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1005: pop
      //   1006: aload #6
      //   1008: aload_2
      //   1009: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1012: pop
      //   1013: aload #6
      //   1015: ldc ' from '
      //   1017: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1020: pop
      //   1021: aload #6
      //   1023: aload_0
      //   1024: getfield mUri : Landroid/net/Uri;
      //   1027: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   1030: pop
      //   1031: ldc 'Settings'
      //   1033: aload #6
      //   1035: invokevirtual toString : ()Ljava/lang/String;
      //   1038: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   1041: pop
      //   1042: aload_1
      //   1043: ifnull -> 1052
      //   1046: aload_1
      //   1047: invokeinterface close : ()V
      //   1052: aconst_null
      //   1053: areturn
      //   1054: aload_1
      //   1055: invokeinterface moveToNext : ()Z
      //   1060: ifeq -> 1075
      //   1063: aload_1
      //   1064: iconst_0
      //   1065: invokeinterface getString : (I)Ljava/lang/String;
      //   1070: astore #6
      //   1072: goto -> 1078
      //   1075: aconst_null
      //   1076: astore #6
      //   1078: aload_0
      //   1079: monitorenter
      //   1080: aload_0
      //   1081: getfield mGenerationTracker : Landroid/provider/Settings$GenerationTracker;
      //   1084: ifnull -> 1114
      //   1087: aload_0
      //   1088: getfield mGenerationTracker : Landroid/provider/Settings$GenerationTracker;
      //   1091: astore #8
      //   1093: iload #5
      //   1095: aload #8
      //   1097: invokevirtual getCurrentGeneration : ()I
      //   1100: if_icmpne -> 1114
      //   1103: aload_0
      //   1104: getfield mValues : Landroid/util/ArrayMap;
      //   1107: aload_2
      //   1108: aload #6
      //   1110: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   1113: pop
      //   1114: aload_0
      //   1115: monitorexit
      //   1116: aload_1
      //   1117: ifnull -> 1126
      //   1120: aload_1
      //   1121: invokeinterface close : ()V
      //   1126: aload #6
      //   1128: areturn
      //   1129: astore #6
      //   1131: aload_0
      //   1132: monitorexit
      //   1133: aload #6
      //   1135: athrow
      //   1136: astore #6
      //   1138: aload_1
      //   1139: astore_2
      //   1140: aload #6
      //   1142: astore_1
      //   1143: goto -> 1262
      //   1146: astore #6
      //   1148: aload_1
      //   1149: astore #8
      //   1151: aload #6
      //   1153: astore_1
      //   1154: goto -> 1165
      //   1157: astore_1
      //   1158: aload #6
      //   1160: astore_2
      //   1161: goto -> 1262
      //   1164: astore_1
      //   1165: aload #8
      //   1167: astore #6
      //   1169: new java/lang/StringBuilder
      //   1172: astore #13
      //   1174: aload #8
      //   1176: astore #6
      //   1178: aload #13
      //   1180: invokespecial <init> : ()V
      //   1183: aload #8
      //   1185: astore #6
      //   1187: aload #13
      //   1189: ldc 'Can't get key '
      //   1191: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1194: pop
      //   1195: aload #8
      //   1197: astore #6
      //   1199: aload #13
      //   1201: aload_2
      //   1202: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1205: pop
      //   1206: aload #8
      //   1208: astore #6
      //   1210: aload #13
      //   1212: ldc ' from '
      //   1214: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1217: pop
      //   1218: aload #8
      //   1220: astore #6
      //   1222: aload #13
      //   1224: aload_0
      //   1225: getfield mUri : Landroid/net/Uri;
      //   1228: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   1231: pop
      //   1232: aload #8
      //   1234: astore #6
      //   1236: ldc 'Settings'
      //   1238: aload #13
      //   1240: invokevirtual toString : ()Ljava/lang/String;
      //   1243: aload_1
      //   1244: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   1247: pop
      //   1248: aload #8
      //   1250: ifnull -> 1260
      //   1253: aload #8
      //   1255: invokeinterface close : ()V
      //   1260: aconst_null
      //   1261: areturn
      //   1262: aload_2
      //   1263: ifnull -> 1272
      //   1266: aload_2
      //   1267: invokeinterface close : ()V
      //   1272: aload_1
      //   1273: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2705	-> 0
      //   #2706	-> 16
      //   #2707	-> 19
      //   #2708	-> 24
      //   #2709	-> 26
      //   #2714	-> 41
      //   #2720	-> 51
      //   #2721	-> 61
      //   #2722	-> 72
      //   #2724	-> 90
      //   #2725	-> 101
      //   #2732	-> 110
      //   #2729	-> 113
      //   #2730	-> 115
      //   #2731	-> 155
      //   #2735	-> 164
      //   #2707	-> 174
      //   #2741	-> 177
      //   #2744	-> 187
      //   #2745	-> 192
      //   #2746	-> 226
      //   #2754	-> 228
      //   #2756	-> 239
      //   #2757	-> 239
      //   #2758	-> 244
      //   #2759	-> 262
      //   #2757	-> 277
      //   #2761	-> 280
      //   #2762	-> 280
      //   #2763	-> 286
      //   #2764	-> 301
      //   #2765	-> 301
      //   #2766	-> 310
      //   #2768	-> 326
      //   #2775	-> 347
      //   #2783	-> 360
      //   #2784	-> 383
      //   #2786	-> 392
      //   #2787	-> 410
      //   #2786	-> 426
      //   #2790	-> 446
      //   #2791	-> 455
      //   #2792	-> 455
      //   #2790	-> 458
      //   #2791	-> 469
      //   #2793	-> 476
      //   #2794	-> 506
      //   #2793	-> 530
      //   #2796	-> 554
      //   #2797	-> 559
      //   #2799	-> 572
      //   #2800	-> 577
      //   #2801	-> 583
      //   #2802	-> 591
      //   #2804	-> 607
      //   #2806	-> 621
      //   #2807	-> 637
      //   #2815	-> 650
      //   #2816	-> 661
      //   #2818	-> 672
      //   #2832	-> 726
      //   #2835	-> 726
      //   #2836	-> 745
      //   #2837	-> 757
      //   #2839	-> 771
      //   #2845	-> 786
      //   #2852	-> 789
      //   #2775	-> 796
      //   #2849	-> 810
      //   #2855	-> 812
      //   #2857	-> 818
      //   #2860	-> 842
      //   #2861	-> 873
      //   #2863	-> 886
      //   #2866	-> 912
      //   #2867	-> 923
      //   #2868	-> 923
      //   #2866	-> 926
      //   #2867	-> 940
      //   #2869	-> 950
      //   #2872	-> 984
      //   #2873	-> 988
      //   #2874	-> 1042
      //   #2893	-> 1042
      //   #2874	-> 1052
      //   #2877	-> 1054
      //   #2878	-> 1078
      //   #2879	-> 1080
      //   #2880	-> 1093
      //   #2881	-> 1103
      //   #2883	-> 1114
      //   #2888	-> 1116
      //   #2893	-> 1116
      //   #2888	-> 1126
      //   #2883	-> 1129
      //   #2893	-> 1136
      //   #2889	-> 1146
      //   #2893	-> 1157
      //   #2889	-> 1164
      //   #2890	-> 1165
      //   #2891	-> 1248
      //   #2893	-> 1248
      //   #2891	-> 1260
      //   #2893	-> 1262
      //   #2894	-> 1272
      // Exception table:
      //   from	to	target	type
      //   26	32	169	finally
      //   41	51	113	java/lang/IllegalStateException
      //   41	51	169	finally
      //   51	58	113	java/lang/IllegalStateException
      //   51	58	169	finally
      //   61	72	113	java/lang/IllegalStateException
      //   61	72	169	finally
      //   72	85	113	java/lang/IllegalStateException
      //   72	85	169	finally
      //   85	87	169	finally
      //   94	101	113	java/lang/IllegalStateException
      //   94	101	169	finally
      //   101	110	113	java/lang/IllegalStateException
      //   101	110	169	finally
      //   115	155	169	finally
      //   155	160	169	finally
      //   164	166	169	finally
      //   170	172	169	finally
      //   248	253	810	android/os/RemoteException
      //   257	262	810	android/os/RemoteException
      //   266	274	810	android/os/RemoteException
      //   284	286	810	android/os/RemoteException
      //   294	301	347	finally
      //   313	318	347	finally
      //   321	326	347	finally
      //   329	337	347	finally
      //   358	360	796	finally
      //   364	370	810	android/os/RemoteException
      //   374	383	810	android/os/RemoteException
      //   387	392	810	android/os/RemoteException
      //   392	410	458	finally
      //   410	426	458	finally
      //   426	446	458	finally
      //   450	455	810	android/os/RemoteException
      //   464	469	810	android/os/RemoteException
      //   473	476	810	android/os/RemoteException
      //   480	486	810	android/os/RemoteException
      //   490	496	810	android/os/RemoteException
      //   500	506	810	android/os/RemoteException
      //   510	520	810	android/os/RemoteException
      //   524	530	810	android/os/RemoteException
      //   534	554	810	android/os/RemoteException
      //   563	572	810	android/os/RemoteException
      //   581	583	810	android/os/RemoteException
      //   595	607	779	finally
      //   611	621	779	finally
      //   641	650	779	finally
      //   654	661	779	finally
      //   665	672	779	finally
      //   676	681	779	finally
      //   685	690	779	finally
      //   694	700	779	finally
      //   704	716	779	finally
      //   720	726	779	finally
      //   729	736	779	finally
      //   739	745	779	finally
      //   748	757	779	finally
      //   760	771	779	finally
      //   774	776	779	finally
      //   781	783	779	finally
      //   783	786	810	android/os/RemoteException
      //   801	803	347	finally
      //   807	810	810	android/os/RemoteException
      //   826	842	1164	android/os/RemoteException
      //   826	842	1157	finally
      //   850	856	1164	android/os/RemoteException
      //   850	856	1157	finally
      //   864	873	1164	android/os/RemoteException
      //   864	873	1157	finally
      //   881	886	1164	android/os/RemoteException
      //   881	886	1157	finally
      //   886	912	926	finally
      //   918	923	1164	android/os/RemoteException
      //   918	923	1157	finally
      //   935	940	1164	android/os/RemoteException
      //   935	940	1157	finally
      //   948	950	1164	android/os/RemoteException
      //   948	950	1157	finally
      //   958	984	1164	android/os/RemoteException
      //   958	984	1157	finally
      //   988	1042	1146	android/os/RemoteException
      //   988	1042	1136	finally
      //   1054	1072	1146	android/os/RemoteException
      //   1054	1072	1136	finally
      //   1078	1080	1146	android/os/RemoteException
      //   1078	1080	1136	finally
      //   1080	1093	1129	finally
      //   1093	1103	1129	finally
      //   1103	1114	1129	finally
      //   1114	1116	1129	finally
      //   1131	1133	1129	finally
      //   1133	1136	1146	android/os/RemoteException
      //   1133	1136	1136	finally
      //   1169	1174	1157	finally
      //   1178	1183	1157	finally
      //   1187	1195	1157	finally
      //   1199	1206	1157	finally
      //   1210	1218	1157	finally
      //   1222	1232	1157	finally
      //   1236	1248	1157	finally
    }
    
    public ArrayMap<String, String> getStringsForPrefix(ContentResolver param1ContentResolver, String param1String, List<String> param1List) {
      String str = param1String.substring(0, param1String.length() - 1);
      DeviceConfig.enforceReadPermission((Context)ActivityThread.currentApplication(), str);
      ArrayMap<String, String> arrayMap = new ArrayMap();
      int i = -1;
      /* monitor enter ThisExpression{InnerObjectType{ObjectType{android/provider/Settings}.Landroid/provider/Settings$NameValueCache;}} */
      try {
        Settings.GenerationTracker generationTracker = this.mGenerationTracker;
        int j = i;
        if (generationTracker != null)
          try {
            if (this.mGenerationTracker.isGenerationChanged()) {
              this.mValues.clear();
            } else {
              boolean bool = this.mValues.containsKey(param1String);
              if (bool) {
                if (!param1List.isEmpty()) {
                  for (String str1 : param1List) {
                    if (this.mValues.containsKey(str1))
                      arrayMap.put(str1, this.mValues.get(str1)); 
                  } 
                } else {
                  for (j = 0; j < this.mValues.size(); j++) {
                    String str1 = (String)this.mValues.keyAt(j);
                    if (str1.startsWith(param1String) && !str1.equals(param1String))
                      arrayMap.put(str1, this.mValues.get(str1)); 
                  } 
                } 
                return arrayMap;
              } 
            } 
            j = i;
            if (this.mGenerationTracker != null)
              j = this.mGenerationTracker.getCurrentGeneration(); 
          } catch (IllegalStateException illegalStateException) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Setting: getStringsForPrefix FD closed by unknown reason, ");
            stringBuilder.append(illegalStateException.getMessage());
            Log.d("Settings", stringBuilder.toString());
            this.mGenerationTracker = null;
            j = i;
          }  
        /* monitor exit ThisExpression{InnerObjectType{ObjectType{android/provider/Settings}.Landroid/provider/Settings$NameValueCache;}} */
        if (this.mCallListCommand == null)
          return arrayMap; 
        IContentProvider iContentProvider = this.mProviderHolder.getProvider(param1ContentResolver);
        return arrayMap;
      } finally {
        param1ContentResolver = null;
      } 
    }
    
    public void clearGenerationTrackerForTest() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mGenerationTracker : Landroid/provider/Settings$GenerationTracker;
      //   6: ifnull -> 16
      //   9: aload_0
      //   10: getfield mGenerationTracker : Landroid/provider/Settings$GenerationTracker;
      //   13: invokevirtual destroy : ()V
      //   16: aload_0
      //   17: getfield mValues : Landroid/util/ArrayMap;
      //   20: invokevirtual clear : ()V
      //   23: aload_0
      //   24: aconst_null
      //   25: putfield mGenerationTracker : Landroid/provider/Settings$GenerationTracker;
      //   28: aload_0
      //   29: monitorexit
      //   30: return
      //   31: astore_1
      //   32: aload_0
      //   33: monitorexit
      //   34: aload_1
      //   35: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #3044	-> 0
      //   #3045	-> 2
      //   #3046	-> 9
      //   #3048	-> 16
      //   #3049	-> 23
      //   #3050	-> 28
      //   #3051	-> 30
      //   #3050	-> 31
      // Exception table:
      //   from	to	target	type
      //   2	9	31	finally
      //   9	16	31	finally
      //   16	23	31	finally
      //   23	28	31	finally
      //   28	30	31	finally
      //   32	34	31	finally
    }
  }
  
  public static boolean canDrawOverlays(Context paramContext) {
    int i = Process.myUid();
    String str = paramContext.getOpPackageName();
    return isCallingPackageAllowedToDrawOverlays(paramContext, i, str, false);
  }
  
  class System extends NameValueTable implements OplusSettings.Mtk_System, OplusSettings.Qcom_System, OplusSettings.Oplus_System {
    public static final String ACCELEROMETER_ROTATION = "accelerometer_rotation";
    
    public static final String ADAPTIVE_SLEEP = "adaptive_sleep";
    
    @Deprecated
    public static final String ADB_ENABLED = "adb_enabled";
    
    public static final String ADVANCED_SETTINGS = "advanced_settings";
    
    public static final int ADVANCED_SETTINGS_DEFAULT = 0;
    
    @Deprecated
    public static final String AIRPLANE_MODE_ON = "airplane_mode_on";
    
    @Deprecated
    public static final String AIRPLANE_MODE_RADIOS = "airplane_mode_radios";
    
    @Deprecated
    public static final String AIRPLANE_MODE_TOGGLEABLE_RADIOS = "airplane_mode_toggleable_radios";
    
    public static final String ALARM_ALERT = "alarm_alert";
    
    public static final String ALARM_ALERT_CACHE = "alarm_alert_cache";
    
    public static final Uri ALARM_ALERT_CACHE_URI;
    
    @Deprecated
    public static final String ALWAYS_FINISH_ACTIVITIES = "always_finish_activities";
    
    @Deprecated
    public static final String ANDROID_ID = "android_id";
    
    @Deprecated
    public static final String ANIMATOR_DURATION_SCALE = "animator_duration_scale";
    
    public static final String APPEND_FOR_LAST_AUDIBLE = "_last_audible";
    
    @Deprecated
    public static final String AUTO_TIME = "auto_time";
    
    @Deprecated
    public static final String AUTO_TIME_ZONE = "auto_time_zone";
    
    public static final String BLUETOOTH_DISCOVERABILITY = "bluetooth_discoverability";
    
    public static final String BLUETOOTH_DISCOVERABILITY_TIMEOUT = "bluetooth_discoverability_timeout";
    
    @Deprecated
    public static final String BLUETOOTH_ON = "bluetooth_on";
    
    public static final String CALL_CONNECTED_TONE_ENABLED = "call_connected_tone_enabled";
    
    @Deprecated
    public static final String CAR_DOCK_SOUND = "car_dock_sound";
    
    @Deprecated
    public static final String CAR_UNDOCK_SOUND = "car_undock_sound";
    
    public static final Map<String, String> CLONE_FROM_PARENT_ON_VALUE;
    
    private static final Set<String> CLONE_TO_MANAGED_PROFILE;
    
    public static final Uri CONTENT_URI;
    
    @Deprecated
    public static final String DATA_ROAMING = "data_roaming";
    
    public static final String DATE_FORMAT = "date_format";
    
    @Deprecated
    public static final String DEBUG_APP = "debug_app";
    
    public static final String DEBUG_ENABLE_ENHANCED_CALL_BLOCKING = "debug.enable_enhanced_calling";
    
    public static final Uri DEFAULT_ALARM_ALERT_URI;
    
    private static final float DEFAULT_FONT_SCALE = 1.0F;
    
    public static final Uri DEFAULT_NOTIFICATION_URI;
    
    public static final Uri DEFAULT_RINGTONE_URI;
    
    @Deprecated
    public static final String DESK_DOCK_SOUND = "desk_dock_sound";
    
    @Deprecated
    public static final String DESK_UNDOCK_SOUND = "desk_undock_sound";
    
    @Deprecated
    public static final String DEVICE_PROVISIONED = "device_provisioned";
    
    @Deprecated
    public static final String DIM_SCREEN = "dim_screen";
    
    public static final String DISPLAY_COLOR_MODE = "display_color_mode";
    
    @Deprecated
    public static final String DOCK_SOUNDS_ENABLED = "dock_sounds_enabled";
    
    public static final String DTMF_TONE_TYPE_WHEN_DIALING = "dtmf_tone_type";
    
    public static final String DTMF_TONE_WHEN_DIALING = "dtmf_tone";
    
    public static final String EGG_MODE = "egg_mode";
    
    public static final String END_BUTTON_BEHAVIOR = "end_button_behavior";
    
    public static final int END_BUTTON_BEHAVIOR_DEFAULT = 2;
    
    public static final int END_BUTTON_BEHAVIOR_HOME = 1;
    
    public static final int END_BUTTON_BEHAVIOR_SLEEP = 2;
    
    public static final String FONT_SCALE = "font_scale";
    
    public static final String HAPTIC_FEEDBACK_ENABLED = "haptic_feedback_enabled";
    
    public static final String HAPTIC_FEEDBACK_INTENSITY = "haptic_feedback_intensity";
    
    public static final String HEARING_AID = "hearing_aid";
    
    public static final String HIDE_ROTATION_LOCK_TOGGLE_FOR_ACCESSIBILITY = "hide_rotation_lock_toggle_for_accessibility";
    
    @Deprecated
    public static final String HTTP_PROXY = "http_proxy";
    
    @Deprecated
    public static final String INSTALL_NON_MARKET_APPS = "install_non_market_apps";
    
    public static final Set<String> INSTANT_APP_SETTINGS;
    
    public static final String[] LEGACY_RESTORE_SETTINGS;
    
    @Deprecated
    public static final String LOCATION_PROVIDERS_ALLOWED = "location_providers_allowed";
    
    public static final String LOCKSCREEN_DISABLED = "lockscreen.disabled";
    
    public static final String LOCKSCREEN_SOUNDS_ENABLED = "lockscreen_sounds_enabled";
    
    @Deprecated
    public static final String LOCK_PATTERN_ENABLED = "lock_pattern_autolock";
    
    @Deprecated
    public static final String LOCK_PATTERN_TACTILE_FEEDBACK_ENABLED = "lock_pattern_tactile_feedback_enabled";
    
    @Deprecated
    public static final String LOCK_PATTERN_VISIBLE = "lock_pattern_visible_pattern";
    
    @Deprecated
    public static final String LOCK_SOUND = "lock_sound";
    
    public static final String LOCK_TO_APP_ENABLED = "lock_to_app_enabled";
    
    @Deprecated
    public static final String LOGGING_ID = "logging_id";
    
    @Deprecated
    public static final String LOW_BATTERY_SOUND = "low_battery_sound";
    
    public static final String MASTER_BALANCE = "master_balance";
    
    public static final String MASTER_MONO = "master_mono";
    
    private static int MAX_BRIGHTNESS = 0;
    
    public static final String MEDIA_BUTTON_RECEIVER = "media_button_receiver";
    
    public static final String MIN_REFRESH_RATE = "min_refresh_rate";
    
    @Deprecated
    public static final String MODE_RINGER = "mode_ringer";
    
    public static final String MODE_RINGER_STREAMS_AFFECTED = "mode_ringer_streams_affected";
    
    private static final HashSet<String> MOVED_TO_GLOBAL;
    
    private static final HashSet<String> MOVED_TO_SECURE;
    
    private static final HashSet<String> MOVED_TO_SECURE_THEN_GLOBAL;
    
    public static final String MULTI_AUDIO_FOCUS_ENABLED = "multi_audio_focus_enabled";
    
    public static final String MUTE_STREAMS_AFFECTED = "mute_streams_affected";
    
    @Deprecated
    public static final String NETWORK_PREFERENCE = "network_preference";
    
    @Deprecated
    public static final String NEXT_ALARM_FORMATTED = "next_alarm_formatted";
    
    @Deprecated
    public static final String NOTIFICATIONS_USE_RING_VOLUME = "notifications_use_ring_volume";
    
    public static final String NOTIFICATION_LIGHT_PULSE = "notification_light_pulse";
    
    public static final String NOTIFICATION_SOUND = "notification_sound";
    
    public static final String NOTIFICATION_SOUND_CACHE = "notification_sound_cache";
    
    public static final Uri NOTIFICATION_SOUND_CACHE_URI;
    
    public static final String NOTIFICATION_VIBRATION_INTENSITY = "notification_vibration_intensity";
    
    @Deprecated
    public static final String PARENTAL_CONTROL_ENABLED = "parental_control_enabled";
    
    @Deprecated
    public static final String PARENTAL_CONTROL_LAST_UPDATE = "parental_control_last_update";
    
    @Deprecated
    public static final String PARENTAL_CONTROL_REDIRECT_URL = "parental_control_redirect_url";
    
    public static final String PEAK_REFRESH_RATE = "peak_refresh_rate";
    
    public static final String POINTER_LOCATION = "pointer_location";
    
    public static final String POINTER_SPEED = "pointer_speed";
    
    @Deprecated
    public static final String POWER_SOUNDS_ENABLED = "power_sounds_enabled";
    
    public static final Set<String> PRIVATE_SETTINGS;
    
    public static final Set<String> PUBLIC_SETTINGS;
    
    @Deprecated
    public static final String RADIO_BLUETOOTH = "bluetooth";
    
    @Deprecated
    public static final String RADIO_CELL = "cell";
    
    @Deprecated
    public static final String RADIO_NFC = "nfc";
    
    @Deprecated
    public static final String RADIO_WIFI = "wifi";
    
    @Deprecated
    public static final String RADIO_WIMAX = "wimax";
    
    public static final String RINGTONE = "ringtone";
    
    public static final String RINGTONE_CACHE = "ringtone_cache";
    
    public static final Uri RINGTONE_CACHE_URI;
    
    public static final String RING_VIBRATION_INTENSITY = "ring_vibration_intensity";
    
    public static final String SCREEN_AUTO_BRIGHTNESS_ADJ = "screen_auto_brightness_adj";
    
    public static final String SCREEN_AUTO_BRIGHTNESS_ADJ_TALKBACK = "screen_auto_brightness_adj_talkback";
    
    public static final String SCREEN_BRIGHTNESS = "screen_brightness";
    
    public static final String SCREEN_BRIGHTNESS_FLOAT = "screen_brightness_float";
    
    public static final String SCREEN_BRIGHTNESS_FOR_VR = "screen_brightness_for_vr";
    
    public static final String SCREEN_BRIGHTNESS_FOR_VR_FLOAT = "screen_brightness_for_vr_float";
    
    public static final String SCREEN_BRIGHTNESS_MODE = "screen_brightness_mode";
    
    public static final int SCREEN_BRIGHTNESS_MODE_AUTOMATIC = 1;
    
    public static final int SCREEN_BRIGHTNESS_MODE_MANUAL = 0;
    
    public static final String SCREEN_OFF_TIMEOUT = "screen_off_timeout";
    
    @Deprecated
    public static final String SETTINGS_CLASSNAME = "settings_classname";
    
    public static final String SETUP_WIZARD_HAS_RUN = "setup_wizard_has_run";
    
    public static final String SHOW_BATTERY_PERCENT = "status_bar_show_battery_percent";
    
    public static final String SHOW_GTALK_SERVICE_STATUS = "SHOW_GTALK_SERVICE_STATUS";
    
    @Deprecated
    public static final String SHOW_PROCESSES = "show_processes";
    
    public static final String SHOW_TOUCHES = "show_touches";
    
    @Deprecated
    public static final String SHOW_WEB_SUGGESTIONS = "show_web_suggestions";
    
    public static final String SIP_ADDRESS_ONLY = "SIP_ADDRESS_ONLY";
    
    public static final String SIP_ALWAYS = "SIP_ALWAYS";
    
    @Deprecated
    public static final String SIP_ASK_ME_EACH_TIME = "SIP_ASK_ME_EACH_TIME";
    
    public static final String SIP_CALL_OPTIONS = "sip_call_options";
    
    public static final String SIP_RECEIVE_CALLS = "sip_receive_calls";
    
    public static final String SOUND_EFFECTS_ENABLED = "sound_effects_enabled";
    
    @Deprecated
    public static final String STAY_ON_WHILE_PLUGGED_IN = "stay_on_while_plugged_in";
    
    public static final String SYSTEM_LOCALES = "system_locales";
    
    public static final String TEXT_AUTO_CAPS = "auto_caps";
    
    public static final String TEXT_AUTO_PUNCTUATE = "auto_punctuate";
    
    public static final String TEXT_AUTO_REPLACE = "auto_replace";
    
    public static final String TEXT_SHOW_PASSWORD = "show_password";
    
    public static final String TIME_12_24 = "time_12_24";
    
    @Deprecated
    public static final String TRANSITION_ANIMATION_SCALE = "transition_animation_scale";
    
    public static final String TTY_MODE = "tty_mode";
    
    @Deprecated
    public static final String UNLOCK_SOUND = "unlock_sound";
    
    @Deprecated
    public static final String USB_MASS_STORAGE_ENABLED = "usb_mass_storage_enabled";
    
    public static final String USER_ROTATION = "user_rotation";
    
    @Deprecated
    public static final String USE_GOOGLE_MAIL = "use_google_mail";
    
    public static final String VIBRATE_INPUT_DEVICES = "vibrate_input_devices";
    
    public static final String VIBRATE_IN_SILENT = "vibrate_in_silent";
    
    public static final String VIBRATE_ON = "vibrate_on";
    
    public static final String VIBRATE_WHEN_RINGING = "vibrate_when_ringing";
    
    public static final String VOLUME_ACCESSIBILITY = "volume_a11y";
    
    public static final String VOLUME_ALARM = "volume_alarm";
    
    public static final String VOLUME_ASSISTANT = "volume_assistant";
    
    public static final String VOLUME_BLUETOOTH_SCO = "volume_bluetooth_sco";
    
    public static final String VOLUME_MASTER = "volume_master";
    
    public static final String VOLUME_MUSIC = "volume_music";
    
    public static final String VOLUME_NOTIFICATION = "volume_notification";
    
    public static final String VOLUME_RING = "volume_ring";
    
    public static final String[] VOLUME_SETTINGS;
    
    public static final String[] VOLUME_SETTINGS_INT;
    
    public static final String VOLUME_SYSTEM = "volume_system";
    
    public static final String VOLUME_VOICE = "volume_voice";
    
    @Deprecated
    public static final String WAIT_FOR_DEBUGGER = "wait_for_debugger";
    
    @Deprecated
    public static final String WALLPAPER_ACTIVITY = "wallpaper_activity";
    
    public static final String WHEN_TO_MAKE_WIFI_CALLS = "when_to_make_wifi_calls";
    
    @Deprecated
    public static final String WIFI_MAX_DHCP_RETRY_COUNT = "wifi_max_dhcp_retry_count";
    
    @Deprecated
    public static final String WIFI_MOBILE_DATA_TRANSITION_WAKELOCK_TIMEOUT_MS = "wifi_mobile_data_transition_wakelock_timeout_ms";
    
    @Deprecated
    public static final String WIFI_NETWORKS_AVAILABLE_NOTIFICATION_ON = "wifi_networks_available_notification_on";
    
    @Deprecated
    public static final String WIFI_NETWORKS_AVAILABLE_REPEAT_DELAY = "wifi_networks_available_repeat_delay";
    
    @Deprecated
    public static final String WIFI_NUM_OPEN_NETWORKS_KEPT = "wifi_num_open_networks_kept";
    
    @Deprecated
    public static final String WIFI_ON = "wifi_on";
    
    @Deprecated
    public static final String WIFI_SLEEP_POLICY = "wifi_sleep_policy";
    
    @Deprecated
    public static final int WIFI_SLEEP_POLICY_DEFAULT = 0;
    
    @Deprecated
    public static final int WIFI_SLEEP_POLICY_NEVER = 2;
    
    @Deprecated
    public static final int WIFI_SLEEP_POLICY_NEVER_WHILE_PLUGGED = 1;
    
    @Deprecated
    public static final String WIFI_STATIC_DNS1 = "wifi_static_dns1";
    
    @Deprecated
    public static final String WIFI_STATIC_DNS2 = "wifi_static_dns2";
    
    @Deprecated
    public static final String WIFI_STATIC_GATEWAY = "wifi_static_gateway";
    
    @Deprecated
    public static final String WIFI_STATIC_IP = "wifi_static_ip";
    
    @Deprecated
    public static final String WIFI_STATIC_NETMASK = "wifi_static_netmask";
    
    @Deprecated
    public static final String WIFI_USE_STATIC_IP = "wifi_use_static_ip";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_ACCEPTABLE_PACKET_LOSS_PERCENTAGE = "wifi_watchdog_acceptable_packet_loss_percentage";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_AP_COUNT = "wifi_watchdog_ap_count";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_BACKGROUND_CHECK_DELAY_MS = "wifi_watchdog_background_check_delay_ms";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_BACKGROUND_CHECK_ENABLED = "wifi_watchdog_background_check_enabled";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_BACKGROUND_CHECK_TIMEOUT_MS = "wifi_watchdog_background_check_timeout_ms";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_INITIAL_IGNORED_PING_COUNT = "wifi_watchdog_initial_ignored_ping_count";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_MAX_AP_CHECKS = "wifi_watchdog_max_ap_checks";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_ON = "wifi_watchdog_on";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_PING_COUNT = "wifi_watchdog_ping_count";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_PING_DELAY_MS = "wifi_watchdog_ping_delay_ms";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_PING_TIMEOUT_MS = "wifi_watchdog_ping_timeout_ms";
    
    @Deprecated
    public static final String WINDOW_ANIMATION_SCALE = "window_animation_scale";
    
    public static final String WINDOW_ORIENTATION_LISTENER_LOG = "window_orientation_listener_log";
    
    private static final Settings.NameValueCache sNameValueCache;
    
    private static final Settings.ContentProviderHolder sProviderHolder;
    
    static {
      Uri uri = Uri.parse("content://settings/system");
      sProviderHolder = new Settings.ContentProviderHolder(uri);
      sNameValueCache = new Settings.NameValueCache(CONTENT_URI, "GET_system", "PUT_system", sProviderHolder);
      HashSet<String> hashSet = new HashSet(30);
      hashSet.add("adaptive_sleep");
      MOVED_TO_SECURE.add("android_id");
      MOVED_TO_SECURE.add("http_proxy");
      MOVED_TO_SECURE.add("location_providers_allowed");
      MOVED_TO_SECURE.add("lock_biometric_weak_flags");
      MOVED_TO_SECURE.add("lock_pattern_autolock");
      MOVED_TO_SECURE.add("lock_pattern_visible_pattern");
      MOVED_TO_SECURE.add("lock_pattern_tactile_feedback_enabled");
      MOVED_TO_SECURE.add("logging_id");
      MOVED_TO_SECURE.add("parental_control_enabled");
      MOVED_TO_SECURE.add("parental_control_last_update");
      MOVED_TO_SECURE.add("parental_control_redirect_url");
      MOVED_TO_SECURE.add("settings_classname");
      MOVED_TO_SECURE.add("use_google_mail");
      MOVED_TO_SECURE.add("wifi_networks_available_notification_on");
      MOVED_TO_SECURE.add("wifi_networks_available_repeat_delay");
      MOVED_TO_SECURE.add("wifi_num_open_networks_kept");
      MOVED_TO_SECURE.add("wifi_on");
      MOVED_TO_SECURE.add("wifi_watchdog_acceptable_packet_loss_percentage");
      MOVED_TO_SECURE.add("wifi_watchdog_ap_count");
      MOVED_TO_SECURE.add("wifi_watchdog_background_check_delay_ms");
      MOVED_TO_SECURE.add("wifi_watchdog_background_check_enabled");
      MOVED_TO_SECURE.add("wifi_watchdog_background_check_timeout_ms");
      MOVED_TO_SECURE.add("wifi_watchdog_initial_ignored_ping_count");
      MOVED_TO_SECURE.add("wifi_watchdog_max_ap_checks");
      MOVED_TO_SECURE.add("wifi_watchdog_on");
      MOVED_TO_SECURE.add("wifi_watchdog_ping_count");
      MOVED_TO_SECURE.add("wifi_watchdog_ping_delay_ms");
      MOVED_TO_SECURE.add("wifi_watchdog_ping_timeout_ms");
      MOVED_TO_SECURE.add("install_non_market_apps");
      MOVED_TO_GLOBAL = new HashSet<>();
      MOVED_TO_SECURE_THEN_GLOBAL = hashSet = new HashSet<>();
      hashSet.add("adb_enabled");
      MOVED_TO_SECURE_THEN_GLOBAL.add("bluetooth_on");
      MOVED_TO_SECURE_THEN_GLOBAL.add("data_roaming");
      MOVED_TO_SECURE_THEN_GLOBAL.add("device_provisioned");
      MOVED_TO_SECURE_THEN_GLOBAL.add("usb_mass_storage_enabled");
      MOVED_TO_SECURE_THEN_GLOBAL.add("http_proxy");
      MOVED_TO_GLOBAL.add("airplane_mode_on");
      MOVED_TO_GLOBAL.add("airplane_mode_radios");
      MOVED_TO_GLOBAL.add("airplane_mode_toggleable_radios");
      MOVED_TO_GLOBAL.add("auto_time");
      MOVED_TO_GLOBAL.add("auto_time_zone");
      MOVED_TO_GLOBAL.add("car_dock_sound");
      MOVED_TO_GLOBAL.add("car_undock_sound");
      MOVED_TO_GLOBAL.add("desk_dock_sound");
      MOVED_TO_GLOBAL.add("desk_undock_sound");
      MOVED_TO_GLOBAL.add("dock_sounds_enabled");
      MOVED_TO_GLOBAL.add("lock_sound");
      MOVED_TO_GLOBAL.add("unlock_sound");
      MOVED_TO_GLOBAL.add("low_battery_sound");
      MOVED_TO_GLOBAL.add("power_sounds_enabled");
      MOVED_TO_GLOBAL.add("stay_on_while_plugged_in");
      MOVED_TO_GLOBAL.add("wifi_sleep_policy");
      MOVED_TO_GLOBAL.add("mode_ringer");
      MOVED_TO_GLOBAL.add("window_animation_scale");
      MOVED_TO_GLOBAL.add("transition_animation_scale");
      MOVED_TO_GLOBAL.add("animator_duration_scale");
      MOVED_TO_GLOBAL.add("fancy_ime_animations");
      MOVED_TO_GLOBAL.add("compatibility_mode");
      MOVED_TO_GLOBAL.add("emergency_tone");
      MOVED_TO_GLOBAL.add("call_auto_retry");
      MOVED_TO_GLOBAL.add("debug_app");
      MOVED_TO_GLOBAL.add("wait_for_debugger");
      MOVED_TO_GLOBAL.add("always_finish_activities");
      MOVED_TO_GLOBAL.add("tzinfo_content_url");
      MOVED_TO_GLOBAL.add("tzinfo_metadata_url");
      MOVED_TO_GLOBAL.add("selinux_content_url");
      MOVED_TO_GLOBAL.add("selinux_metadata_url");
      MOVED_TO_GLOBAL.add("sms_short_codes_content_url");
      MOVED_TO_GLOBAL.add("sms_short_codes_metadata_url");
      MOVED_TO_GLOBAL.add("cert_pin_content_url");
      MOVED_TO_GLOBAL.add("cert_pin_metadata_url");
      VOLUME_SETTINGS = new String[] { "volume_voice", "volume_system", "volume_ring", "volume_music", "volume_alarm", "volume_notification", "volume_bluetooth_sco" };
      VOLUME_SETTINGS_INT = new String[] { 
          "volume_voice", "volume_system", "volume_ring", "volume_music", "volume_alarm", "volume_notification", "volume_bluetooth_sco", "", "", "", 
          "volume_a11y", "volume_assistant" };
      DEFAULT_RINGTONE_URI = getUriFor("ringtone");
      RINGTONE_CACHE_URI = getUriFor("ringtone_cache");
      DEFAULT_NOTIFICATION_URI = getUriFor("notification_sound");
      NOTIFICATION_SOUND_CACHE_URI = getUriFor("notification_sound_cache");
      DEFAULT_ALARM_ALERT_URI = getUriFor("alarm_alert");
      ALARM_ALERT_CACHE_URI = getUriFor("alarm_alert_cache");
      LEGACY_RESTORE_SETTINGS = new String[0];
      ArraySet<String> arraySet2 = new ArraySet();
      arraySet2.add("end_button_behavior");
      PUBLIC_SETTINGS.add("wifi_use_static_ip");
      PUBLIC_SETTINGS.add("wifi_static_ip");
      PUBLIC_SETTINGS.add("wifi_static_gateway");
      PUBLIC_SETTINGS.add("wifi_static_netmask");
      PUBLIC_SETTINGS.add("wifi_static_dns1");
      PUBLIC_SETTINGS.add("wifi_static_dns2");
      PUBLIC_SETTINGS.add("bluetooth_discoverability");
      PUBLIC_SETTINGS.add("bluetooth_discoverability_timeout");
      PUBLIC_SETTINGS.add("next_alarm_formatted");
      PUBLIC_SETTINGS.add("font_scale");
      PUBLIC_SETTINGS.add("system_locales");
      PUBLIC_SETTINGS.add("dim_screen");
      PUBLIC_SETTINGS.add("screen_off_timeout");
      PUBLIC_SETTINGS.add("screen_brightness");
      PUBLIC_SETTINGS.add("screen_auto_brightness_adj_talkback");
      PUBLIC_SETTINGS.add("screen_brightness_float");
      PUBLIC_SETTINGS.add("screen_brightness_for_vr");
      PUBLIC_SETTINGS.add("screen_brightness_for_vr_float");
      PUBLIC_SETTINGS.add("screen_brightness_mode");
      PUBLIC_SETTINGS.add("mode_ringer_streams_affected");
      PUBLIC_SETTINGS.add("mute_streams_affected");
      PUBLIC_SETTINGS.add("vibrate_on");
      PUBLIC_SETTINGS.add("volume_ring");
      PUBLIC_SETTINGS.add("volume_system");
      PUBLIC_SETTINGS.add("volume_voice");
      PUBLIC_SETTINGS.add("volume_music");
      PUBLIC_SETTINGS.add("volume_alarm");
      PUBLIC_SETTINGS.add("volume_notification");
      PUBLIC_SETTINGS.add("volume_bluetooth_sco");
      PUBLIC_SETTINGS.add("volume_assistant");
      PUBLIC_SETTINGS.add("ringtone");
      PUBLIC_SETTINGS.add("notification_sound");
      PUBLIC_SETTINGS.add("alarm_alert");
      PUBLIC_SETTINGS.add("auto_replace");
      PUBLIC_SETTINGS.add("auto_caps");
      PUBLIC_SETTINGS.add("auto_punctuate");
      PUBLIC_SETTINGS.add("show_password");
      PUBLIC_SETTINGS.add("SHOW_GTALK_SERVICE_STATUS");
      PUBLIC_SETTINGS.add("wallpaper_activity");
      PUBLIC_SETTINGS.add("time_12_24");
      PUBLIC_SETTINGS.add("date_format");
      PUBLIC_SETTINGS.add("setup_wizard_has_run");
      PUBLIC_SETTINGS.add("accelerometer_rotation");
      PUBLIC_SETTINGS.add("user_rotation");
      PUBLIC_SETTINGS.add("dtmf_tone");
      PUBLIC_SETTINGS.add("sound_effects_enabled");
      PUBLIC_SETTINGS.add("haptic_feedback_enabled");
      PUBLIC_SETTINGS.add("show_web_suggestions");
      PUBLIC_SETTINGS.add("vibrate_when_ringing");
      PRIVATE_SETTINGS = (Set<String>)(arraySet2 = new ArraySet());
      arraySet2.add("wifi_use_static_ip");
      PRIVATE_SETTINGS.add("end_button_behavior");
      PRIVATE_SETTINGS.add("advanced_settings");
      PRIVATE_SETTINGS.add("screen_auto_brightness_adj");
      PRIVATE_SETTINGS.add("vibrate_input_devices");
      PRIVATE_SETTINGS.add("volume_master");
      PRIVATE_SETTINGS.add("master_mono");
      PRIVATE_SETTINGS.add("master_balance");
      PRIVATE_SETTINGS.add("notifications_use_ring_volume");
      PRIVATE_SETTINGS.add("vibrate_in_silent");
      PRIVATE_SETTINGS.add("media_button_receiver");
      PRIVATE_SETTINGS.add("hide_rotation_lock_toggle_for_accessibility");
      PRIVATE_SETTINGS.add("dtmf_tone_type");
      PRIVATE_SETTINGS.add("hearing_aid");
      PRIVATE_SETTINGS.add("tty_mode");
      PRIVATE_SETTINGS.add("notification_light_pulse");
      PRIVATE_SETTINGS.add("pointer_location");
      PRIVATE_SETTINGS.add("show_touches");
      PRIVATE_SETTINGS.add("window_orientation_listener_log");
      PRIVATE_SETTINGS.add("power_sounds_enabled");
      PRIVATE_SETTINGS.add("dock_sounds_enabled");
      PRIVATE_SETTINGS.add("lockscreen_sounds_enabled");
      PRIVATE_SETTINGS.add("lockscreen.disabled");
      PRIVATE_SETTINGS.add("low_battery_sound");
      PRIVATE_SETTINGS.add("desk_dock_sound");
      PRIVATE_SETTINGS.add("desk_undock_sound");
      PRIVATE_SETTINGS.add("car_dock_sound");
      PRIVATE_SETTINGS.add("car_undock_sound");
      PRIVATE_SETTINGS.add("lock_sound");
      PRIVATE_SETTINGS.add("unlock_sound");
      PRIVATE_SETTINGS.add("sip_receive_calls");
      PRIVATE_SETTINGS.add("sip_call_options");
      PRIVATE_SETTINGS.add("SIP_ALWAYS");
      PRIVATE_SETTINGS.add("SIP_ADDRESS_ONLY");
      PRIVATE_SETTINGS.add("SIP_ASK_ME_EACH_TIME");
      PRIVATE_SETTINGS.add("pointer_speed");
      PRIVATE_SETTINGS.add("lock_to_app_enabled");
      PRIVATE_SETTINGS.add("egg_mode");
      PRIVATE_SETTINGS.add("status_bar_show_battery_percent");
      PRIVATE_SETTINGS.add("display_color_mode");
      PRIVATE_SETTINGS.add("call_connected_tone_enabled");
      CLONE_TO_MANAGED_PROFILE = (Set<String>)(arraySet2 = new ArraySet());
      arraySet2.add("date_format");
      CLONE_TO_MANAGED_PROFILE.add("haptic_feedback_enabled");
      CLONE_TO_MANAGED_PROFILE.add("sound_effects_enabled");
      CLONE_TO_MANAGED_PROFILE.add("show_password");
      CLONE_TO_MANAGED_PROFILE.add("time_12_24");
      ArrayMap<String, String> arrayMap = new ArrayMap();
      arrayMap.put("ringtone", "sync_parent_sounds");
      CLONE_FROM_PARENT_ON_VALUE.put("notification_sound", "sync_parent_sounds");
      CLONE_FROM_PARENT_ON_VALUE.put("alarm_alert", "sync_parent_sounds");
      ArraySet<String> arraySet1 = new ArraySet();
      arraySet1.add("auto_replace");
      INSTANT_APP_SETTINGS.add("auto_caps");
      INSTANT_APP_SETTINGS.add("auto_punctuate");
      INSTANT_APP_SETTINGS.add("show_password");
      INSTANT_APP_SETTINGS.add("date_format");
      INSTANT_APP_SETTINGS.add("font_scale");
      INSTANT_APP_SETTINGS.add("haptic_feedback_enabled");
      INSTANT_APP_SETTINGS.add("time_12_24");
      INSTANT_APP_SETTINGS.add("sound_effects_enabled");
      INSTANT_APP_SETTINGS.add("accelerometer_rotation");
      MAX_BRIGHTNESS = 0;
    }
    
    public static void getMovedToGlobalSettings(Set<String> param1Set) {
      param1Set.addAll(MOVED_TO_GLOBAL);
      param1Set.addAll(MOVED_TO_SECURE_THEN_GLOBAL);
    }
    
    public static void getMovedToSecureSettings(Set<String> param1Set) {
      param1Set.addAll(MOVED_TO_SECURE);
    }
    
    public static void getNonLegacyMovedKeys(HashSet<String> param1HashSet) {
      param1HashSet.addAll(MOVED_TO_GLOBAL);
    }
    
    public static void clearProviderForTest() {
      sProviderHolder.clearProviderForTest();
      sNameValueCache.clearGenerationTrackerForTest();
    }
    
    public static String getString(ContentResolver param1ContentResolver, String param1String) {
      return getStringForUser(param1ContentResolver, param1String, param1ContentResolver.getUserId());
    }
    
    public static String getStringForUser(ContentResolver param1ContentResolver, String param1String, int param1Int) {
      SeempLog.record(SeempLog.getSeempGetApiIdFromValue(param1String));
      if (MOVED_TO_SECURE.contains(param1String)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Setting ");
        stringBuilder.append(param1String);
        stringBuilder.append(" has moved from android.provider.Settings.System to android.provider.Settings.Secure, returning read-only value.");
        Log.w("Settings", stringBuilder.toString());
        return Settings.Secure.getStringForUser(param1ContentResolver, param1String, param1Int);
      } 
      if (MOVED_TO_GLOBAL.contains(param1String) || MOVED_TO_SECURE_THEN_GLOBAL.contains(param1String)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Setting ");
        stringBuilder.append(param1String);
        stringBuilder.append(" has moved from android.provider.Settings.System to android.provider.Settings.Global, returning read-only value.");
        Log.w("Settings", stringBuilder.toString());
        return Settings.Global.getStringForUser(param1ContentResolver, param1String, param1Int);
      } 
      return sNameValueCache.getStringForUser(param1ContentResolver, param1String, param1Int);
    }
    
    public static boolean putString(ContentResolver param1ContentResolver, String param1String1, String param1String2) {
      return putStringForUser(param1ContentResolver, param1String1, param1String2, param1ContentResolver.getUserId());
    }
    
    @SystemApi
    public static boolean putString(ContentResolver param1ContentResolver, String param1String1, String param1String2, boolean param1Boolean) {
      return putStringForUser(param1ContentResolver, param1String1, param1String2, param1ContentResolver.getUserId(), param1Boolean);
    }
    
    public static boolean putStringForUser(ContentResolver param1ContentResolver, String param1String1, String param1String2, int param1Int) {
      return putStringForUser(param1ContentResolver, param1String1, param1String2, param1Int, false);
    }
    
    private static boolean putStringForUser(ContentResolver param1ContentResolver, String param1String1, String param1String2, int param1Int, boolean param1Boolean) {
      StringBuilder stringBuilder;
      SeempLog.record(SeempLog.getSeempPutApiIdFromValue(param1String1));
      if (MOVED_TO_SECURE.contains(param1String1)) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Setting ");
        stringBuilder.append(param1String1);
        stringBuilder.append(" has moved from android.provider.Settings.System to android.provider.Settings.Secure, value is unchanged.");
        Log.w("Settings", stringBuilder.toString());
        return false;
      } 
      if (MOVED_TO_GLOBAL.contains(param1String1) || MOVED_TO_SECURE_THEN_GLOBAL.contains(param1String1)) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Setting ");
        stringBuilder.append(param1String1);
        stringBuilder.append(" has moved from android.provider.Settings.System to android.provider.Settings.Global, value is unchanged.");
        Log.w("Settings", stringBuilder.toString());
        return false;
      } 
      return sNameValueCache.putStringForUser((ContentResolver)stringBuilder, param1String1, param1String2, null, false, param1Int, param1Boolean);
    }
    
    public static Uri getUriFor(String param1String) {
      if (MOVED_TO_SECURE.contains(param1String)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Setting ");
        stringBuilder.append(param1String);
        stringBuilder.append(" has moved from android.provider.Settings.System to android.provider.Settings.Secure, returning Secure URI.");
        Log.w("Settings", stringBuilder.toString());
        return Settings.Secure.getUriFor(Settings.Secure.CONTENT_URI, param1String);
      } 
      if (MOVED_TO_GLOBAL.contains(param1String) || MOVED_TO_SECURE_THEN_GLOBAL.contains(param1String)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Setting ");
        stringBuilder.append(param1String);
        stringBuilder.append(" has moved from android.provider.Settings.System to android.provider.Settings.Global, returning read-only global URI.");
        Log.w("Settings", stringBuilder.toString());
        return Settings.Global.getUriFor(Settings.Global.CONTENT_URI, param1String);
      } 
      return getUriFor(CONTENT_URI, param1String);
    }
    
    public static int getInt(ContentResolver param1ContentResolver, String param1String, int param1Int) {
      return getIntForUser(param1ContentResolver, param1String, param1Int, param1ContentResolver.getUserId());
    }
    
    public static int getIntForUser(ContentResolver param1ContentResolver, String param1String, int param1Int1, int param1Int2) {
      String str = getStringForUser(param1ContentResolver, param1String, param1Int2);
      try {
        param1Int2 = MAX_BRIGHTNESS;
        if (param1Int2 == 0 && "screen_brightness".equals(param1String))
          MAX_BRIGHTNESS = SystemProperties.getInt("sys.oppo.multibrightness", 0); 
        if ("screen_brightness".equals(param1String) && MAX_BRIGHTNESS > 255) {
          if (str != null)
            param1Int1 = param1Int2 = Math.round((Integer.parseInt(str) * 256 / (MAX_BRIGHTNESS + 1))); 
          return param1Int1;
        } 
        if (str != null)
          param1Int1 = param1Int2 = Integer.parseInt(str); 
        return param1Int1;
      } catch (NumberFormatException numberFormatException) {
        return param1Int1;
      } 
    }
    
    public static int getInt(ContentResolver param1ContentResolver, String param1String) throws Settings.SettingNotFoundException {
      return getIntForUser(param1ContentResolver, param1String, param1ContentResolver.getUserId());
    }
    
    public static int getIntForUser(ContentResolver param1ContentResolver, String param1String, int param1Int) throws Settings.SettingNotFoundException {
      String str = getStringForUser(param1ContentResolver, param1String, param1Int);
      try {
        param1Int = MAX_BRIGHTNESS;
        if (param1Int == 0 && "screen_brightness".equals(param1String))
          MAX_BRIGHTNESS = SystemProperties.getInt("sys.oppo.multibrightness", 0); 
        if ("screen_brightness".equals(param1String) && MAX_BRIGHTNESS > 255)
          return Math.round((Integer.parseInt(str) * 256 / (MAX_BRIGHTNESS + 1))); 
        return Integer.parseInt(str);
      } catch (NumberFormatException numberFormatException) {
        throw new Settings.SettingNotFoundException(param1String);
      } 
    }
    
    public static boolean putInt(ContentResolver param1ContentResolver, String param1String, int param1Int) {
      return putIntForUser(param1ContentResolver, param1String, param1Int, param1ContentResolver.getUserId());
    }
    
    public static boolean putIntForUser(ContentResolver param1ContentResolver, String param1String, int param1Int1, int param1Int2) {
      if (MAX_BRIGHTNESS == 0 && "screen_brightness".equals(param1String))
        MAX_BRIGHTNESS = SystemProperties.getInt("sys.oppo.multibrightness", 0); 
      int i = param1Int1;
      if ("screen_brightness".equals(param1String)) {
        int j = MAX_BRIGHTNESS;
        i = param1Int1;
        if (j > 255)
          i = Math.round(((j + 1) * param1Int1 / 256)); 
      } 
      if (param1ContentResolver.getPackageName().equals("com.google.android.marvin.talkback") && "screen_brightness".equals(param1String)) {
        param1Int1 = getInt(param1ContentResolver, "screen_brightness_mode", 0);
        if (1 == param1Int1) {
          Log.d("Settings", "adjust by talkback when automatic_brightness in on");
          putInt(param1ContentResolver, "screen_auto_brightness_adj_talkback", i);
        } 
      } 
      return putStringForUser(param1ContentResolver, param1String, Integer.toString(i), param1Int2);
    }
    
    public static long getLong(ContentResolver param1ContentResolver, String param1String, long param1Long) {
      return getLongForUser(param1ContentResolver, param1String, param1Long, param1ContentResolver.getUserId());
    }
    
    public static long getLongForUser(ContentResolver param1ContentResolver, String param1String, long param1Long, int param1Int) {
      String str = getStringForUser(param1ContentResolver, param1String, param1Int);
      if (str != null)
        try {
          long l = Long.parseLong(str);
        } catch (NumberFormatException numberFormatException) {} 
      return param1Long;
    }
    
    public static long getLong(ContentResolver param1ContentResolver, String param1String) throws Settings.SettingNotFoundException {
      return getLongForUser(param1ContentResolver, param1String, param1ContentResolver.getUserId());
    }
    
    public static long getLongForUser(ContentResolver param1ContentResolver, String param1String, int param1Int) throws Settings.SettingNotFoundException {
      String str = getStringForUser(param1ContentResolver, param1String, param1Int);
      try {
        return Long.parseLong(str);
      } catch (NumberFormatException numberFormatException) {
        throw new Settings.SettingNotFoundException(param1String);
      } 
    }
    
    public static boolean putLong(ContentResolver param1ContentResolver, String param1String, long param1Long) {
      return putLongForUser(param1ContentResolver, param1String, param1Long, param1ContentResolver.getUserId());
    }
    
    public static boolean putLongForUser(ContentResolver param1ContentResolver, String param1String, long param1Long, int param1Int) {
      return putStringForUser(param1ContentResolver, param1String, Long.toString(param1Long), param1Int);
    }
    
    public static float getFloat(ContentResolver param1ContentResolver, String param1String, float param1Float) {
      return getFloatForUser(param1ContentResolver, param1String, param1Float, param1ContentResolver.getUserId());
    }
    
    public static float getFloatForUser(ContentResolver param1ContentResolver, String param1String, float param1Float, int param1Int) {
      String str = getStringForUser(param1ContentResolver, param1String, param1Int);
      if (str != null)
        try {
          float f = Float.parseFloat(str);
        } catch (NumberFormatException numberFormatException) {
          return param1Float;
        }  
      return param1Float;
    }
    
    public static float getFloat(ContentResolver param1ContentResolver, String param1String) throws Settings.SettingNotFoundException {
      return getFloatForUser(param1ContentResolver, param1String, param1ContentResolver.getUserId());
    }
    
    public static float getFloatForUser(ContentResolver param1ContentResolver, String param1String, int param1Int) throws Settings.SettingNotFoundException {
      String str = getStringForUser(param1ContentResolver, param1String, param1Int);
      if (str != null)
        try {
          return Float.parseFloat(str);
        } catch (NumberFormatException numberFormatException) {
          throw new Settings.SettingNotFoundException(param1String);
        }  
      throw new Settings.SettingNotFoundException(param1String);
    }
    
    public static boolean putFloat(ContentResolver param1ContentResolver, String param1String, float param1Float) {
      return putFloatForUser(param1ContentResolver, param1String, param1Float, param1ContentResolver.getUserId());
    }
    
    public static boolean putFloatForUser(ContentResolver param1ContentResolver, String param1String, float param1Float, int param1Int) {
      return putStringForUser(param1ContentResolver, param1String, Float.toString(param1Float), param1Int);
    }
    
    public static void getConfiguration(ContentResolver param1ContentResolver, Configuration param1Configuration) {
      adjustConfigurationForUser(param1ContentResolver, param1Configuration, param1ContentResolver.getUserId(), false);
    }
    
    public static void adjustConfigurationForUser(ContentResolver param1ContentResolver, Configuration param1Configuration, int param1Int, boolean param1Boolean) {
      param1Configuration.fontScale = getFloatForUser(param1ContentResolver, "font_scale", 1.0F, param1Int);
      if (param1Configuration.fontScale < 0.0F)
        param1Configuration.fontScale = 1.0F; 
      String str = getStringForUser(param1ContentResolver, "system_locales", param1Int);
      if (str != null) {
        param1Configuration.setLocales(LocaleList.forLanguageTags(str));
      } else if (param1Boolean) {
        String str1 = param1Configuration.getLocales().toLanguageTags();
        putStringForUser(param1ContentResolver, "system_locales", str1, param1Int, false);
      } 
    }
    
    public static void clearConfiguration(Configuration param1Configuration) {
      param1Configuration.fontScale = 0.0F;
      if (!param1Configuration.userSetLocale && !param1Configuration.getLocales().isEmpty())
        param1Configuration.clearLocales(); 
    }
    
    public static boolean putConfiguration(ContentResolver param1ContentResolver, Configuration param1Configuration) {
      return putConfigurationForUser(param1ContentResolver, param1Configuration, param1ContentResolver.getUserId());
    }
    
    public static boolean putConfigurationForUser(ContentResolver param1ContentResolver, Configuration param1Configuration, int param1Int) {
      boolean bool = putFloatForUser(param1ContentResolver, "font_scale", param1Configuration.fontScale, param1Int);
      boolean bool1 = false;
      if (bool) {
        String str = param1Configuration.getLocales().toLanguageTags();
        if (putStringForUser(param1ContentResolver, "system_locales", str, param1Int, false))
          bool1 = true; 
      } 
      return bool1;
    }
    
    public static boolean hasInterestingConfigurationChanges(int param1Int) {
      return ((0x40000000 & param1Int) != 0 || (param1Int & 0x4) != 0);
    }
    
    @Deprecated
    public static boolean getShowGTalkServiceStatus(ContentResolver param1ContentResolver) {
      return getShowGTalkServiceStatusForUser(param1ContentResolver, param1ContentResolver.getUserId());
    }
    
    @Deprecated
    public static boolean getShowGTalkServiceStatusForUser(ContentResolver param1ContentResolver, int param1Int) {
      boolean bool = false;
      if (getIntForUser(param1ContentResolver, "SHOW_GTALK_SERVICE_STATUS", 0, param1Int) != 0)
        bool = true; 
      return bool;
    }
    
    @Deprecated
    public static void setShowGTalkServiceStatus(ContentResolver param1ContentResolver, boolean param1Boolean) {
      setShowGTalkServiceStatusForUser(param1ContentResolver, param1Boolean, param1ContentResolver.getUserId());
    }
    
    @Deprecated
    public static void setShowGTalkServiceStatusForUser(ContentResolver param1ContentResolver, boolean param1Boolean, int param1Int) {
      putIntForUser(param1ContentResolver, "SHOW_GTALK_SERVICE_STATUS", param1Boolean, param1Int);
    }
    
    public static void getCloneToManagedProfileSettings(Set<String> param1Set) {
      param1Set.addAll(CLONE_TO_MANAGED_PROFILE);
    }
    
    public static void getCloneFromParentOnValueSettings(Map<String, String> param1Map) {
      param1Map.putAll(CLONE_FROM_PARENT_ON_VALUE);
    }
    
    public static boolean canWrite(Context param1Context) {
      int i = Process.myUid();
      String str = param1Context.getOpPackageName();
      return Settings.isCallingPackageAllowedToWriteSettings(param1Context, i, str, false);
    }
    
    public static int getIntForBrightness(ContentResolver param1ContentResolver, String param1String, int param1Int1, int param1Int2) {
      String str = getStringForUser(param1ContentResolver, param1String, param1Int2);
      if (str != null)
        try {
          param1Int1 = param1Int2 = Integer.parseInt(str);
        } catch (NumberFormatException numberFormatException) {
          return param1Int1;
        }  
      return param1Int1;
    }
    
    public static int getIntForBrightness(ContentResolver param1ContentResolver, String param1String, int param1Int) throws Settings.SettingNotFoundException {
      String str = getStringForUser(param1ContentResolver, param1String, param1Int);
      try {
        return Integer.parseInt(str);
      } catch (NumberFormatException numberFormatException) {
        throw new Settings.SettingNotFoundException(param1String);
      } 
    }
    
    public static boolean putIntForBrightness(ContentResolver param1ContentResolver, String param1String, int param1Int1, int param1Int2) {
      return putStringForUser(param1ContentResolver, param1String, Integer.toString(param1Int1), param1Int2);
    }
  }
  
  class Secure extends NameValueTable implements OplusSettings.Mtk_Secure {
    public static final String ACCESSIBILITY_AUTOCLICK_DELAY = "accessibility_autoclick_delay";
    
    public static final String ACCESSIBILITY_AUTOCLICK_ENABLED = "accessibility_autoclick_enabled";
    
    public static final String ACCESSIBILITY_BUTTON_TARGETS = "accessibility_button_targets";
    
    public static final String ACCESSIBILITY_BUTTON_TARGET_COMPONENT = "accessibility_button_target_component";
    
    public static final String ACCESSIBILITY_CAPTIONING_BACKGROUND_COLOR = "accessibility_captioning_background_color";
    
    public static final String ACCESSIBILITY_CAPTIONING_EDGE_COLOR = "accessibility_captioning_edge_color";
    
    public static final String ACCESSIBILITY_CAPTIONING_EDGE_TYPE = "accessibility_captioning_edge_type";
    
    public static final String ACCESSIBILITY_CAPTIONING_ENABLED = "accessibility_captioning_enabled";
    
    public static final String ACCESSIBILITY_CAPTIONING_FONT_SCALE = "accessibility_captioning_font_scale";
    
    public static final String ACCESSIBILITY_CAPTIONING_FOREGROUND_COLOR = "accessibility_captioning_foreground_color";
    
    public static final String ACCESSIBILITY_CAPTIONING_LOCALE = "accessibility_captioning_locale";
    
    public static final String ACCESSIBILITY_CAPTIONING_PRESET = "accessibility_captioning_preset";
    
    public static final String ACCESSIBILITY_CAPTIONING_TYPEFACE = "accessibility_captioning_typeface";
    
    public static final String ACCESSIBILITY_CAPTIONING_WINDOW_COLOR = "accessibility_captioning_window_color";
    
    public static final String ACCESSIBILITY_DISPLAY_DALTONIZER = "accessibility_display_daltonizer";
    
    public static final String ACCESSIBILITY_DISPLAY_DALTONIZER_ENABLED = "accessibility_display_daltonizer_enabled";
    
    public static final String ACCESSIBILITY_DISPLAY_INVERSION_ENABLED = "accessibility_display_inversion_enabled";
    
    @Deprecated
    public static final String ACCESSIBILITY_DISPLAY_MAGNIFICATION_AUTO_UPDATE = "accessibility_display_magnification_auto_update";
    
    public static final String ACCESSIBILITY_DISPLAY_MAGNIFICATION_ENABLED = "accessibility_display_magnification_enabled";
    
    @SystemApi
    public static final String ACCESSIBILITY_DISPLAY_MAGNIFICATION_NAVBAR_ENABLED = "accessibility_display_magnification_navbar_enabled";
    
    public static final String ACCESSIBILITY_DISPLAY_MAGNIFICATION_SCALE = "accessibility_display_magnification_scale";
    
    public static final String ACCESSIBILITY_ENABLED = "accessibility_enabled";
    
    public static final String ACCESSIBILITY_HIGH_TEXT_CONTRAST_ENABLED = "high_text_contrast_enabled";
    
    public static final String ACCESSIBILITY_INTERACTIVE_UI_TIMEOUT_MS = "accessibility_interactive_ui_timeout_ms";
    
    public static final String ACCESSIBILITY_LARGE_POINTER_ICON = "accessibility_large_pointer_icon";
    
    public static final String ACCESSIBILITY_MAGNIFICATION_MODE = "accessibility_magnification_mode";
    
    public static final int ACCESSIBILITY_MAGNIFICATION_MODE_FULLSCREEN = 1;
    
    public static final int ACCESSIBILITY_MAGNIFICATION_MODE_WINDOW = 2;
    
    public static final String ACCESSIBILITY_NON_INTERACTIVE_UI_TIMEOUT_MS = "accessibility_non_interactive_ui_timeout_ms";
    
    public static final String ACCESSIBILITY_SHORTCUT_DIALOG_SHOWN = "accessibility_shortcut_dialog_shown";
    
    public static final String ACCESSIBILITY_SHORTCUT_ON_LOCK_SCREEN = "accessibility_shortcut_on_lock_screen";
    
    public static final String ACCESSIBILITY_SHORTCUT_TARGET_MAGNIFICATION_CONTROLLER = "com.android.server.accessibility.MagnificationController";
    
    public static final String ACCESSIBILITY_SHORTCUT_TARGET_SERVICE = "accessibility_shortcut_target_service";
    
    public static final String ACCESSIBILITY_SOFT_KEYBOARD_MODE = "accessibility_soft_keyboard_mode";
    
    @Deprecated
    public static final String ACCESSIBILITY_SPEAK_PASSWORD = "speak_password";
    
    public static final String ADAPTIVE_SLEEP = "adaptive_sleep";
    
    @Deprecated
    public static final String ADB_ENABLED = "adb_enabled";
    
    public static final String ALLOWED_GEOLOCATION_ORIGINS = "allowed_geolocation_origins";
    
    @Deprecated
    public static final String ALLOW_MOCK_LOCATION = "mock_location";
    
    public static final String ALWAYS_ON_VPN_APP = "always_on_vpn_app";
    
    public static final String ALWAYS_ON_VPN_LOCKDOWN = "always_on_vpn_lockdown";
    
    public static final String ALWAYS_ON_VPN_LOCKDOWN_WHITELIST = "always_on_vpn_lockdown_whitelist";
    
    public static final String ANDROID_ID = "android_id";
    
    public static final String ANR_SHOW_BACKGROUND = "anr_show_background";
    
    public static final String ASSISTANT = "assistant";
    
    public static final String ASSIST_DISCLOSURE_ENABLED = "assist_disclosure_enabled";
    
    public static final String ASSIST_GESTURE_ENABLED = "assist_gesture_enabled";
    
    public static final String ASSIST_GESTURE_SENSITIVITY = "assist_gesture_sensitivity";
    
    @SystemApi
    public static final String ASSIST_GESTURE_SETUP_COMPLETE = "assist_gesture_setup_complete";
    
    public static final String ASSIST_GESTURE_SILENCE_ALERTS_ENABLED = "assist_gesture_silence_alerts_enabled";
    
    public static final String ASSIST_GESTURE_WAKE_ENABLED = "assist_gesture_wake_enabled";
    
    public static final String ASSIST_SCREENSHOT_ENABLED = "assist_screenshot_enabled";
    
    public static final String ASSIST_STRUCTURE_ENABLED = "assist_structure_enabled";
    
    public static final String ATTENTIVE_TIMEOUT = "attentive_timeout";
    
    @SystemApi
    public static final String AUTOFILL_FEATURE_FIELD_CLASSIFICATION = "autofill_field_classification";
    
    public static final String AUTOFILL_SERVICE = "autofill_service";
    
    public static final String AUTOFILL_SERVICE_SEARCH_URI = "autofill_service_search_uri";
    
    @SystemApi
    public static final String AUTOFILL_USER_DATA_MAX_CATEGORY_COUNT = "autofill_user_data_max_category_count";
    
    @SystemApi
    public static final String AUTOFILL_USER_DATA_MAX_FIELD_CLASSIFICATION_IDS_SIZE = "autofill_user_data_max_field_classification_size";
    
    @SystemApi
    public static final String AUTOFILL_USER_DATA_MAX_USER_DATA_SIZE = "autofill_user_data_max_user_data_size";
    
    @SystemApi
    public static final String AUTOFILL_USER_DATA_MAX_VALUE_LENGTH = "autofill_user_data_max_value_length";
    
    @SystemApi
    public static final String AUTOFILL_USER_DATA_MIN_VALUE_LENGTH = "autofill_user_data_min_value_length";
    
    public static final String AUTOMATIC_STORAGE_MANAGER_BYTES_CLEARED = "automatic_storage_manager_bytes_cleared";
    
    public static final String AUTOMATIC_STORAGE_MANAGER_DAYS_TO_RETAIN = "automatic_storage_manager_days_to_retain";
    
    public static final int AUTOMATIC_STORAGE_MANAGER_DAYS_TO_RETAIN_DEFAULT = 90;
    
    public static final String AUTOMATIC_STORAGE_MANAGER_ENABLED = "automatic_storage_manager_enabled";
    
    public static final String AUTOMATIC_STORAGE_MANAGER_LAST_RUN = "automatic_storage_manager_last_run";
    
    public static final String AUTOMATIC_STORAGE_MANAGER_TURNED_OFF_BY_POLICY = "automatic_storage_manager_turned_off_by_policy";
    
    @SystemApi
    public static final String AUTO_REVOKE_DISABLED = "auto_revoke_disabled";
    
    public static final String AWARE_ENABLED = "aware_enabled";
    
    public static final String AWARE_LOCK_ENABLED = "aware_lock_enabled";
    
    public static final String AWARE_TAP_PAUSE_GESTURE_COUNT = "aware_tap_pause_gesture_count";
    
    public static final String AWARE_TAP_PAUSE_TOUCH_COUNT = "aware_tap_pause_touch_count";
    
    @Deprecated
    public static final String BACKGROUND_DATA = "background_data";
    
    public static final String BACKUP_AUTO_RESTORE = "backup_auto_restore";
    
    public static final String BACKUP_ENABLED = "backup_enabled";
    
    public static final String BACKUP_LOCAL_TRANSPORT_PARAMETERS = "backup_local_transport_parameters";
    
    public static final String BACKUP_MANAGER_CONSTANTS = "backup_manager_constants";
    
    public static final String BACKUP_PROVISIONED = "backup_provisioned";
    
    public static final String BACKUP_TRANSPORT = "backup_transport";
    
    public static final String BACK_GESTURE_INSET_SCALE_LEFT = "back_gesture_inset_scale_left";
    
    public static final String BACK_GESTURE_INSET_SCALE_RIGHT = "back_gesture_inset_scale_right";
    
    public static final String BIOMETRIC_DEBUG_ENABLED = "biometric_debug_enabled";
    
    @Deprecated
    public static final String BLUETOOTH_ON = "bluetooth_on";
    
    public static final String BLUETOOTH_ON_WHILE_DRIVING = "bluetooth_on_while_driving";
    
    public static final String BUBBLE_IMPORTANT_CONVERSATIONS = "bubble_important_conversations";
    
    @Deprecated
    public static final String BUGREPORT_IN_POWER_MENU = "bugreport_in_power_menu";
    
    public static final String CALL_SCREENING_DEFAULT_COMPONENT = "call_screening_default_component";
    
    public static final String CAMERA_DOUBLE_TAP_POWER_GESTURE_DISABLED = "camera_double_tap_power_gesture_disabled";
    
    public static final String CAMERA_DOUBLE_TWIST_TO_FLIP_ENABLED = "camera_double_twist_to_flip_enabled";
    
    public static final String CAMERA_GESTURE_DISABLED = "camera_gesture_disabled";
    
    public static final String CAMERA_LIFT_TRIGGER_ENABLED = "camera_lift_trigger_enabled";
    
    public static final int CAMERA_LIFT_TRIGGER_ENABLED_DEFAULT = 1;
    
    public static final String CARRIER_APPS_HANDLED = "carrier_apps_handled";
    
    public static final String CHARGING_SOUNDS_ENABLED = "charging_sounds_enabled";
    
    public static final String CHARGING_VIBRATION_ENABLED = "charging_vibration_enabled";
    
    private static final Set<String> CLONE_TO_MANAGED_PROFILE;
    
    public static final String CMAS_ADDITIONAL_BROADCAST_PKG = "cmas_additional_broadcast_pkg";
    
    @SystemApi
    public static final String COMPLETED_CATEGORY_PREFIX = "suggested.completed_category.";
    
    public static final String CONNECTIVITY_RELEASE_PENDING_INTENT_DELAY_MS = "connectivity_release_pending_intent_delay_ms";
    
    public static final String CONTENT_CAPTURE_ENABLED = "content_capture_enabled";
    
    public static final Uri CONTENT_URI;
    
    public static final String CONTROLS_ENABLED = "controls_enabled";
    
    public static final String CROSS_PROFILE_CALENDAR_ENABLED = "cross_profile_calendar_enabled";
    
    public static final String DARK_MODE_DIALOG_SEEN = "dark_mode_dialog_seen";
    
    public static final String DARK_THEME_CUSTOM_END_TIME = "dark_theme_custom_end_time";
    
    public static final String DARK_THEME_CUSTOM_START_TIME = "dark_theme_custom_start_time";
    
    @Deprecated
    public static final String DATA_ROAMING = "data_roaming";
    
    public static final String DEFAULT_INPUT_METHOD = "default_input_method";
    
    @Deprecated
    public static final String DEVELOPMENT_SETTINGS_ENABLED = "development_settings_enabled";
    
    public static final String DEVICE_PAIRED = "device_paired";
    
    @Deprecated
    public static final String DEVICE_PROVISIONED = "device_provisioned";
    
    public static final String DIALER_DEFAULT_APPLICATION = "dialer_default_application";
    
    public static final String DISABLED_PRINT_SERVICES = "disabled_print_services";
    
    public static final String DISABLED_SYSTEM_INPUT_METHODS = "disabled_system_input_methods";
    
    public static final String DISPLAY_DENSITY_FORCED = "display_density_forced";
    
    public static final String DISPLAY_WHITE_BALANCE_ENABLED = "display_white_balance_enabled";
    
    public static final String DOCKED_CLOCK_FACE = "docked_clock_face";
    
    public static final String DOUBLE_TAP_TO_WAKE = "double_tap_to_wake";
    
    @SystemApi
    public static final String DOZE_ALWAYS_ON = "doze_always_on";
    
    public static final String DOZE_DOUBLE_TAP_GESTURE = "doze_pulse_on_double_tap";
    
    public static final String DOZE_ENABLED = "doze_enabled";
    
    public static final String DOZE_PICK_UP_GESTURE = "doze_pulse_on_pick_up";
    
    public static final String DOZE_PULSE_ON_LONG_PRESS = "doze_pulse_on_long_press";
    
    public static final String DOZE_TAP_SCREEN_GESTURE = "doze_tap_gesture";
    
    public static final String DOZE_WAKE_DISPLAY_GESTURE = "doze_wake_display_gesture";
    
    public static final String DOZE_WAKE_LOCK_SCREEN_GESTURE = "doze_wake_screen_gesture";
    
    public static final String EMERGENCY_ASSISTANCE_APPLICATION = "emergency_assistance_application";
    
    public static final String ENABLED_ACCESSIBILITY_SERVICES = "enabled_accessibility_services";
    
    public static final String ENABLED_INPUT_METHODS = "enabled_input_methods";
    
    @Deprecated
    public static final String ENABLED_NOTIFICATION_ASSISTANT = "enabled_notification_assistant";
    
    @Deprecated
    public static final String ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners";
    
    @Deprecated
    public static final String ENABLED_NOTIFICATION_POLICY_ACCESS_PACKAGES = "enabled_notification_policy_access_packages";
    
    public static final String ENABLED_PRINT_SERVICES = "enabled_print_services";
    
    public static final String ENABLED_VR_LISTENERS = "enabled_vr_listeners";
    
    public static final String ENHANCED_VOICE_PRIVACY_ENABLED = "enhanced_voice_privacy_enabled";
    
    public static final String FACE_UNLOCK_ALWAYS_REQUIRE_CONFIRMATION = "face_unlock_always_require_confirmation";
    
    public static final String FACE_UNLOCK_APP_ENABLED = "face_unlock_app_enabled";
    
    public static final String FACE_UNLOCK_ATTENTION_REQUIRED = "face_unlock_attention_required";
    
    public static final String FACE_UNLOCK_DISMISSES_KEYGUARD = "face_unlock_dismisses_keyguard";
    
    public static final String FACE_UNLOCK_DIVERSITY_REQUIRED = "face_unlock_diversity_required";
    
    public static final String FACE_UNLOCK_KEYGUARD_ENABLED = "face_unlock_keyguard_enabled";
    
    public static final String FACE_UNLOCK_RE_ENROLL = "face_unlock_re_enroll";
    
    public static final String FLASHLIGHT_AVAILABLE = "flashlight_available";
    
    public static final String FLASHLIGHT_ENABLED = "flashlight_enabled";
    
    public static final String GLOBAL_ACTIONS_PANEL_AVAILABLE = "global_actions_panel_available";
    
    public static final String GLOBAL_ACTIONS_PANEL_DEBUG_ENABLED = "global_actions_panel_debug_enabled";
    
    public static final String GLOBAL_ACTIONS_PANEL_ENABLED = "global_actions_panel_enabled";
    
    @Deprecated
    public static final String HTTP_PROXY = "http_proxy";
    
    @SystemApi
    public static final String HUSH_GESTURE_USED = "hush_gesture_used";
    
    public static final String IMMERSIVE_MODE_CONFIRMATIONS = "immersive_mode_confirmations";
    
    public static final String INCALL_BACK_BUTTON_BEHAVIOR = "incall_back_button_behavior";
    
    public static final int INCALL_BACK_BUTTON_BEHAVIOR_DEFAULT = 0;
    
    public static final int INCALL_BACK_BUTTON_BEHAVIOR_HANGUP = 1;
    
    public static final int INCALL_BACK_BUTTON_BEHAVIOR_NONE = 0;
    
    public static final String INCALL_POWER_BUTTON_BEHAVIOR = "incall_power_button_behavior";
    
    public static final int INCALL_POWER_BUTTON_BEHAVIOR_DEFAULT = 1;
    
    public static final int INCALL_POWER_BUTTON_BEHAVIOR_HANGUP = 2;
    
    public static final int INCALL_POWER_BUTTON_BEHAVIOR_SCREEN_OFF = 1;
    
    public static final String INPUT_METHODS_SUBTYPE_HISTORY = "input_methods_subtype_history";
    
    public static final String INPUT_METHOD_SELECTOR_VISIBILITY = "input_method_selector_visibility";
    
    public static final String INSTALL_NON_MARKET_APPS = "install_non_market_apps";
    
    @SystemApi
    public static final String INSTANT_APPS_ENABLED = "instant_apps_enabled";
    
    public static final Set<String> INSTANT_APP_SETTINGS;
    
    public static final String IN_CALL_NOTIFICATION_ENABLED = "in_call_notification_enabled";
    
    public static final String KEYGUARD_SLICE_URI = "keyguard_slice_uri";
    
    @SystemApi
    public static final String LAST_SETUP_SHOWN = "last_setup_shown";
    
    public static final String[] LEGACY_RESTORE_SETTINGS;
    
    @SystemApi
    public static final String LOCATION_ACCESS_CHECK_DELAY_MILLIS = "location_access_check_delay_millis";
    
    @SystemApi
    public static final String LOCATION_ACCESS_CHECK_INTERVAL_MILLIS = "location_access_check_interval_millis";
    
    public static final String LOCATION_CHANGER = "location_changer";
    
    public static final int LOCATION_CHANGER_QUICK_SETTINGS = 2;
    
    public static final int LOCATION_CHANGER_SYSTEM_SETTINGS = 1;
    
    public static final int LOCATION_CHANGER_UNKNOWN = 0;
    
    public static final String LOCATION_COARSE_ACCURACY_M = "locationCoarseAccuracy";
    
    @Deprecated
    public static final String LOCATION_MODE = "location_mode";
    
    @Deprecated
    public static final int LOCATION_MODE_BATTERY_SAVING = 2;
    
    @Deprecated
    public static final int LOCATION_MODE_HIGH_ACCURACY = 3;
    
    public static final int LOCATION_MODE_OFF = 0;
    
    @SystemApi
    public static final int LOCATION_MODE_ON = 3;
    
    @Deprecated
    public static final int LOCATION_MODE_SENSORS_ONLY = 1;
    
    @SystemApi
    @Deprecated
    public static final String LOCATION_PERMISSIONS_UPGRADE_TO_Q_MODE = "location_permissions_upgrade_to_q_mode";
    
    @Deprecated
    public static final String LOCATION_PROVIDERS_ALLOWED = "location_providers_allowed";
    
    public static final String LOCKDOWN_IN_POWER_MENU = "lockdown_in_power_menu";
    
    @Deprecated
    public static final String LOCK_BIOMETRIC_WEAK_FLAGS = "lock_biometric_weak_flags";
    
    @Deprecated
    public static final String LOCK_PATTERN_ENABLED = "lock_pattern_autolock";
    
    @Deprecated
    public static final String LOCK_PATTERN_TACTILE_FEEDBACK_ENABLED = "lock_pattern_tactile_feedback_enabled";
    
    @Deprecated
    public static final String LOCK_PATTERN_VISIBLE = "lock_pattern_visible_pattern";
    
    @SystemApi
    public static final String LOCK_SCREEN_ALLOW_PRIVATE_NOTIFICATIONS = "lock_screen_allow_private_notifications";
    
    public static final String LOCK_SCREEN_ALLOW_REMOTE_INPUT = "lock_screen_allow_remote_input";
    
    @Deprecated
    public static final String LOCK_SCREEN_APPWIDGET_IDS = "lock_screen_appwidget_ids";
    
    public static final String LOCK_SCREEN_CUSTOM_CLOCK_FACE = "lock_screen_custom_clock_face";
    
    @Deprecated
    public static final String LOCK_SCREEN_FALLBACK_APPWIDGET_ID = "lock_screen_fallback_appwidget_id";
    
    public static final String LOCK_SCREEN_LOCK_AFTER_TIMEOUT = "lock_screen_lock_after_timeout";
    
    @Deprecated
    public static final String LOCK_SCREEN_OWNER_INFO = "lock_screen_owner_info";
    
    @Deprecated
    public static final String LOCK_SCREEN_OWNER_INFO_ENABLED = "lock_screen_owner_info_enabled";
    
    @SystemApi
    public static final String LOCK_SCREEN_SHOW_NOTIFICATIONS = "lock_screen_show_notifications";
    
    public static final String LOCK_SCREEN_SHOW_SILENT_NOTIFICATIONS = "lock_screen_show_silent_notifications";
    
    @Deprecated
    public static final String LOCK_SCREEN_STICKY_APPWIDGET = "lock_screen_sticky_appwidget";
    
    public static final String LOCK_SCREEN_WHEN_TRUST_LOST = "lock_screen_when_trust_lost";
    
    public static final String LOCK_TO_APP_EXIT_LOCKED = "lock_to_app_exit_locked";
    
    @Deprecated
    public static final String LOGGING_ID = "logging_id";
    
    public static final String LONG_PRESS_TIMEOUT = "long_press_timeout";
    
    public static final String LOW_POWER_MANUAL_ACTIVATION_COUNT = "low_power_manual_activation_count";
    
    public static final String LOW_POWER_WARNING_ACKNOWLEDGED = "low_power_warning_acknowledged";
    
    public static final String MANAGED_PROFILE_CONTACT_REMOTE_SEARCH = "managed_profile_contact_remote_search";
    
    public static final String MANAGED_PROVISIONING_DPC_DOWNLOADED = "managed_provisioning_dpc_downloaded";
    
    public static final String MANUAL_RINGER_TOGGLE_COUNT = "manual_ringer_toggle_count";
    
    public static final String MEDIA_CONTROLS_RESUME = "qs_media_resumption";
    
    public static final String MINIMAL_POST_PROCESSING_ALLOWED = "minimal_post_processing_allowed";
    
    public static final String MOUNT_PLAY_NOTIFICATION_SND = "mount_play_not_snd";
    
    public static final String MOUNT_UMS_AUTOSTART = "mount_ums_autostart";
    
    public static final String MOUNT_UMS_NOTIFY_ENABLED = "mount_ums_notify_enabled";
    
    public static final String MOUNT_UMS_PROMPT = "mount_ums_prompt";
    
    private static final HashSet<String> MOVED_TO_GLOBAL;
    
    private static final HashSet<String> MOVED_TO_LOCK_SETTINGS;
    
    public static final String MULTI_PRESS_TIMEOUT = "multi_press_timeout";
    
    public static final String NAVIGATION_MODE = "navigation_mode";
    
    public static final String NEARBY_SHARING_COMPONENT = "nearby_sharing_component";
    
    @Deprecated
    public static final String NETWORK_PREFERENCE = "network_preference";
    
    public static final String NFC_PAYMENT_DEFAULT_COMPONENT = "nfc_payment_default_component";
    
    public static final String NFC_PAYMENT_FOREGROUND = "nfc_payment_foreground";
    
    public static final String NIGHT_DISPLAY_ACTIVATED = "night_display_activated";
    
    public static final String NIGHT_DISPLAY_AUTO_MODE = "night_display_auto_mode";
    
    public static final String NIGHT_DISPLAY_COLOR_TEMPERATURE = "night_display_color_temperature";
    
    public static final String NIGHT_DISPLAY_CUSTOM_END_TIME = "night_display_custom_end_time";
    
    public static final String NIGHT_DISPLAY_CUSTOM_START_TIME = "night_display_custom_start_time";
    
    public static final String NIGHT_DISPLAY_LAST_ACTIVATED_TIME = "night_display_last_activated_time";
    
    public static final String NOTIFICATION_BADGING = "notification_badging";
    
    public static final String NOTIFICATION_DISMISS_RTL = "notification_dismiss_rtl";
    
    public static final String NOTIFICATION_HISTORY_ENABLED = "notification_history_enabled";
    
    public static final String NOTIFICATION_NEW_INTERRUPTION_MODEL = "new_interruption_model";
    
    public static final String NUM_ROTATION_SUGGESTIONS_ACCEPTED = "num_rotation_suggestions_accepted";
    
    @SystemApi
    public static final String ODI_CAPTIONS_ENABLED = "odi_captions_enabled";
    
    public static final String PACKAGES_TO_CLEAR_DATA_BEFORE_FULL_RESTORE = "packages_to_clear_data_before_full_restore";
    
    public static final String PARENTAL_CONTROL_ENABLED = "parental_control_enabled";
    
    public static final String PARENTAL_CONTROL_LAST_UPDATE = "parental_control_last_update";
    
    public static final String PARENTAL_CONTROL_REDIRECT_URL = "parental_control_redirect_url";
    
    public static final String PAYMENT_SERVICE_SEARCH_URI = "payment_service_search_uri";
    
    public static final String PEOPLE_STRIP = "people_strip";
    
    public static final String POWER_MENU_LOCKED_SHOW_CONTENT = "power_menu_locked_show_content";
    
    public static final String PREFERRED_TTY_MODE = "preferred_tty_mode";
    
    public static final String PRINT_SERVICE_SEARCH_URI = "print_service_search_uri";
    
    public static final String QS_AUTO_ADDED_TILES = "qs_auto_tiles";
    
    public static final String QS_TILES = "sysui_qs_tiles";
    
    public static final String RTT_CALLING_MODE = "rtt_calling_mode";
    
    public static final String SCREENSAVER_ACTIVATE_ON_DOCK = "screensaver_activate_on_dock";
    
    public static final String SCREENSAVER_ACTIVATE_ON_SLEEP = "screensaver_activate_on_sleep";
    
    public static final String SCREENSAVER_COMPONENTS = "screensaver_components";
    
    public static final String SCREENSAVER_DEFAULT_COMPONENT = "screensaver_default_component";
    
    public static final String SCREENSAVER_ENABLED = "screensaver_enabled";
    
    public static final String SEARCH_GLOBAL_SEARCH_ACTIVITY = "search_global_search_activity";
    
    public static final String SEARCH_MAX_RESULTS_PER_SOURCE = "search_max_results_per_source";
    
    public static final String SEARCH_MAX_RESULTS_TO_DISPLAY = "search_max_results_to_display";
    
    public static final String SEARCH_MAX_SHORTCUTS_RETURNED = "search_max_shortcuts_returned";
    
    public static final String SEARCH_MAX_SOURCE_EVENT_AGE_MILLIS = "search_max_source_event_age_millis";
    
    public static final String SEARCH_MAX_STAT_AGE_MILLIS = "search_max_stat_age_millis";
    
    public static final String SEARCH_MIN_CLICKS_FOR_SOURCE_RANKING = "search_min_clicks_for_source_ranking";
    
    public static final String SEARCH_MIN_IMPRESSIONS_FOR_SOURCE_RANKING = "search_min_impressions_for_source_ranking";
    
    public static final String SEARCH_NUM_PROMOTED_SOURCES = "search_num_promoted_sources";
    
    public static final String SEARCH_PER_SOURCE_CONCURRENT_QUERY_LIMIT = "search_per_source_concurrent_query_limit";
    
    public static final String SEARCH_PREFILL_MILLIS = "search_prefill_millis";
    
    public static final String SEARCH_PROMOTED_SOURCE_DEADLINE_MILLIS = "search_promoted_source_deadline_millis";
    
    public static final String SEARCH_QUERY_THREAD_CORE_POOL_SIZE = "search_query_thread_core_pool_size";
    
    public static final String SEARCH_QUERY_THREAD_MAX_POOL_SIZE = "search_query_thread_max_pool_size";
    
    public static final String SEARCH_SHORTCUT_REFRESH_CORE_POOL_SIZE = "search_shortcut_refresh_core_pool_size";
    
    public static final String SEARCH_SHORTCUT_REFRESH_MAX_POOL_SIZE = "search_shortcut_refresh_max_pool_size";
    
    public static final String SEARCH_SOURCE_TIMEOUT_MILLIS = "search_source_timeout_millis";
    
    public static final String SEARCH_THREAD_KEEPALIVE_SECONDS = "search_thread_keepalive_seconds";
    
    public static final String SEARCH_WEB_RESULTS_OVERRIDE_LIMIT = "search_web_results_override_limit";
    
    public static final String SECURE_FRP_MODE = "secure_frp_mode";
    
    public static final String SELECTED_INPUT_METHOD_SUBTYPE = "selected_input_method_subtype";
    
    public static final String SELECTED_SPELL_CHECKER = "selected_spell_checker";
    
    public static final String SELECTED_SPELL_CHECKER_SUBTYPE = "selected_spell_checker_subtype";
    
    public static final String SETTINGS_CLASSNAME = "settings_classname";
    
    public static final String SHOW_FIRST_CRASH_DIALOG_DEV_OPTION = "show_first_crash_dialog_dev_option";
    
    public static final String SHOW_IME_WITH_HARD_KEYBOARD = "show_ime_with_hard_keyboard";
    
    public static final String SHOW_MEDIA_WHEN_BYPASSING = "show_media_when_bypassing";
    
    public static final int SHOW_MODE_AUTO = 0;
    
    public static final int SHOW_MODE_HIDDEN = 1;
    
    public static final String SHOW_NOTE_ABOUT_NOTIFICATION_HIDING = "show_note_about_notification_hiding";
    
    public static final String SHOW_NOTIFICATION_SNOOZE = "show_notification_snooze";
    
    public static final String SHOW_ROTATION_SUGGESTIONS = "show_rotation_suggestions";
    
    public static final int SHOW_ROTATION_SUGGESTIONS_DEFAULT = 1;
    
    public static final int SHOW_ROTATION_SUGGESTIONS_DISABLED = 0;
    
    public static final int SHOW_ROTATION_SUGGESTIONS_ENABLED = 1;
    
    public static final String SHOW_ZEN_SETTINGS_SUGGESTION = "show_zen_settings_suggestion";
    
    public static final String SHOW_ZEN_UPGRADE_NOTIFICATION = "show_zen_upgrade_notification";
    
    public static final String SILENCE_ALARMS_GESTURE_COUNT = "silence_alarms_gesture_count";
    
    public static final String SILENCE_ALARMS_TOUCH_COUNT = "silence_alarms_touch_count";
    
    public static final String SILENCE_CALL_GESTURE_COUNT = "silence_call_gesture_count";
    
    public static final String SILENCE_CALL_TOUCH_COUNT = "silence_call_touch_count";
    
    public static final String SILENCE_GESTURE = "silence_gesture";
    
    public static final String SILENCE_TIMER_GESTURE_COUNT = "silence_timer_gesture_count";
    
    public static final String SILENCE_TIMER_TOUCH_COUNT = "silence_timer_touch_count";
    
    public static final String SKIP_DIRECTION = "skip_gesture_direction";
    
    public static final String SKIP_FIRST_USE_HINTS = "skip_first_use_hints";
    
    public static final String SKIP_GESTURE = "skip_gesture";
    
    public static final String SKIP_GESTURE_COUNT = "skip_gesture_count";
    
    public static final String SKIP_TOUCH_COUNT = "skip_touch_count";
    
    public static final String SLEEP_TIMEOUT = "sleep_timeout";
    
    public static final String SMS_DEFAULT_APPLICATION = "sms_default_application";
    
    public static final String SPELL_CHECKER_ENABLED = "spell_checker_enabled";
    
    public static final String SUPPRESS_AUTO_BATTERY_SAVER_SUGGESTION = "suppress_auto_battery_saver_suggestion";
    
    public static final String SUPPRESS_DOZE = "suppress_doze";
    
    public static final String SYNC_PARENT_SOUNDS = "sync_parent_sounds";
    
    public static final String SYSTEM_NAVIGATION_KEYS_ENABLED = "system_navigation_keys_enabled";
    
    public static final String TAP_GESTURE = "tap_gesture";
    
    @SystemApi
    public static final String THEME_CUSTOMIZATION_OVERLAY_PACKAGES = "theme_customization_overlay_packages";
    
    public static final String TOUCH_EXPLORATION_ENABLED = "touch_exploration_enabled";
    
    public static final String TOUCH_EXPLORATION_GRANTED_ACCESSIBILITY_SERVICES = "touch_exploration_granted_accessibility_services";
    
    public static final String TRUST_AGENTS_EXTEND_UNLOCK = "trust_agents_extend_unlock";
    
    public static final String TRUST_AGENTS_INITIALIZED = "trust_agents_initialized";
    
    @Deprecated
    public static final String TTS_DEFAULT_COUNTRY = "tts_default_country";
    
    @Deprecated
    public static final String TTS_DEFAULT_LANG = "tts_default_lang";
    
    public static final String TTS_DEFAULT_LOCALE = "tts_default_locale";
    
    public static final String TTS_DEFAULT_PITCH = "tts_default_pitch";
    
    public static final String TTS_DEFAULT_RATE = "tts_default_rate";
    
    public static final String TTS_DEFAULT_SYNTH = "tts_default_synth";
    
    @Deprecated
    public static final String TTS_DEFAULT_VARIANT = "tts_default_variant";
    
    public static final String TTS_ENABLED_PLUGINS = "tts_enabled_plugins";
    
    @Deprecated
    public static final String TTS_USE_DEFAULTS = "tts_use_defaults";
    
    public static final String TTY_MODE_ENABLED = "tty_mode_enabled";
    
    public static final String TV_APP_USES_NON_SYSTEM_INPUTS = "tv_app_uses_non_system_inputs";
    
    public static final String TV_INPUT_CUSTOM_LABELS = "tv_input_custom_labels";
    
    public static final String TV_INPUT_HIDDEN_INPUTS = "tv_input_hidden_inputs";
    
    public static final String TV_USER_SETUP_COMPLETE = "tv_user_setup_complete";
    
    public static final String UI_NIGHT_MODE = "ui_night_mode";
    
    public static final String UI_NIGHT_MODE_OVERRIDE_OFF = "ui_night_mode_override_off";
    
    public static final String UI_NIGHT_MODE_OVERRIDE_ON = "ui_night_mode_override_on";
    
    public static final String UNKNOWN_SOURCES_DEFAULT_REVERSED = "unknown_sources_default_reversed";
    
    public static final String UNSAFE_VOLUME_MUSIC_ACTIVE_MS = "unsafe_volume_music_active_ms";
    
    public static final String USB_AUDIO_AUTOMATIC_ROUTING_DISABLED = "usb_audio_automatic_routing_disabled";
    
    @Deprecated
    public static final String USB_MASS_STORAGE_ENABLED = "usb_mass_storage_enabled";
    
    @SystemApi
    public static final String USER_SETUP_COMPLETE = "user_setup_complete";
    
    @SystemApi
    public static final int USER_SETUP_PERSONALIZATION_COMPLETE = 10;
    
    @SystemApi
    public static final int USER_SETUP_PERSONALIZATION_NOT_STARTED = 0;
    
    @SystemApi
    public static final int USER_SETUP_PERSONALIZATION_PAUSED = 2;
    
    @SystemApi
    public static final int USER_SETUP_PERSONALIZATION_STARTED = 1;
    
    @SystemApi
    public static final String USER_SETUP_PERSONALIZATION_STATE = "user_setup_personalization_state";
    
    @Deprecated
    public static final String USE_GOOGLE_MAIL = "use_google_mail";
    
    public static final String VOICE_INTERACTION_SERVICE = "voice_interaction_service";
    
    public static final String VOICE_RECOGNITION_SERVICE = "voice_recognition_service";
    
    @SystemApi
    public static final String VOLUME_HUSH_GESTURE = "volume_hush_gesture";
    
    @SystemApi
    public static final int VOLUME_HUSH_MUTE = 2;
    
    @SystemApi
    public static final int VOLUME_HUSH_OFF = 0;
    
    @SystemApi
    public static final int VOLUME_HUSH_VIBRATE = 1;
    
    public static final String VR_DISPLAY_MODE = "vr_display_mode";
    
    public static final int VR_DISPLAY_MODE_LOW_PERSISTENCE = 0;
    
    public static final int VR_DISPLAY_MODE_OFF = 1;
    
    public static final String WAKE_GESTURE_ENABLED = "wake_gesture_enabled";
    
    @Deprecated
    public static final String WIFI_IDLE_MS = "wifi_idle_ms";
    
    @Deprecated
    public static final String WIFI_MAX_DHCP_RETRY_COUNT = "wifi_max_dhcp_retry_count";
    
    @Deprecated
    public static final String WIFI_MOBILE_DATA_TRANSITION_WAKELOCK_TIMEOUT_MS = "wifi_mobile_data_transition_wakelock_timeout_ms";
    
    @Deprecated
    public static final String WIFI_NETWORKS_AVAILABLE_NOTIFICATION_ON = "wifi_networks_available_notification_on";
    
    @Deprecated
    public static final String WIFI_NETWORKS_AVAILABLE_REPEAT_DELAY = "wifi_networks_available_repeat_delay";
    
    @Deprecated
    public static final String WIFI_NUM_OPEN_NETWORKS_KEPT = "wifi_num_open_networks_kept";
    
    @Deprecated
    public static final String WIFI_ON = "wifi_on";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_ACCEPTABLE_PACKET_LOSS_PERCENTAGE = "wifi_watchdog_acceptable_packet_loss_percentage";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_AP_COUNT = "wifi_watchdog_ap_count";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_BACKGROUND_CHECK_DELAY_MS = "wifi_watchdog_background_check_delay_ms";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_BACKGROUND_CHECK_ENABLED = "wifi_watchdog_background_check_enabled";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_BACKGROUND_CHECK_TIMEOUT_MS = "wifi_watchdog_background_check_timeout_ms";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_INITIAL_IGNORED_PING_COUNT = "wifi_watchdog_initial_ignored_ping_count";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_MAX_AP_CHECKS = "wifi_watchdog_max_ap_checks";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_ON = "wifi_watchdog_on";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_PING_COUNT = "wifi_watchdog_ping_count";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_PING_DELAY_MS = "wifi_watchdog_ping_delay_ms";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_PING_TIMEOUT_MS = "wifi_watchdog_ping_timeout_ms";
    
    @Deprecated
    public static final String WIFI_WATCHDOG_WATCH_LIST = "wifi_watchdog_watch_list";
    
    public static final String WINDOW_MAGNIFICATION = "window_magnification";
    
    public static final String ZEN_DURATION = "zen_duration";
    
    public static final int ZEN_DURATION_FOREVER = 0;
    
    public static final int ZEN_DURATION_PROMPT = -1;
    
    public static final String ZEN_SETTINGS_SUGGESTION_VIEWED = "zen_settings_suggestion_viewed";
    
    public static final String ZEN_SETTINGS_UPDATED = "zen_settings_updated";
    
    private static boolean sIsSystemProcess;
    
    private static ILockSettings sLockSettings;
    
    private static final Settings.NameValueCache sNameValueCache;
    
    private static final Settings.ContentProviderHolder sProviderHolder;
    
    static {
      Uri uri = Uri.parse("content://settings/secure");
      sProviderHolder = new Settings.ContentProviderHolder(uri);
      sNameValueCache = new Settings.NameValueCache(CONTENT_URI, "GET_secure", "PUT_secure", sProviderHolder);
      sLockSettings = null;
      HashSet<String> hashSet = new HashSet(3);
      hashSet.add("lock_pattern_autolock");
      MOVED_TO_LOCK_SETTINGS.add("lock_pattern_visible_pattern");
      MOVED_TO_LOCK_SETTINGS.add("lock_pattern_tactile_feedback_enabled");
      MOVED_TO_GLOBAL = hashSet = new HashSet<>();
      hashSet.add("adb_enabled");
      MOVED_TO_GLOBAL.add("assisted_gps_enabled");
      MOVED_TO_GLOBAL.add("bluetooth_on");
      MOVED_TO_GLOBAL.add("bugreport_in_power_menu");
      MOVED_TO_GLOBAL.add("cdma_cell_broadcast_sms");
      MOVED_TO_GLOBAL.add("roaming_settings");
      MOVED_TO_GLOBAL.add("subscription_mode");
      MOVED_TO_GLOBAL.add("data_activity_timeout_mobile");
      MOVED_TO_GLOBAL.add("data_activity_timeout_wifi");
      MOVED_TO_GLOBAL.add("data_roaming");
      MOVED_TO_GLOBAL.add("development_settings_enabled");
      MOVED_TO_GLOBAL.add("device_provisioned");
      MOVED_TO_GLOBAL.add("display_size_forced");
      MOVED_TO_GLOBAL.add("download_manager_max_bytes_over_mobile");
      MOVED_TO_GLOBAL.add("download_manager_recommended_max_bytes_over_mobile");
      MOVED_TO_GLOBAL.add("mobile_data");
      MOVED_TO_GLOBAL.add("netstats_dev_bucket_duration");
      MOVED_TO_GLOBAL.add("netstats_dev_delete_age");
      MOVED_TO_GLOBAL.add("netstats_dev_persist_bytes");
      MOVED_TO_GLOBAL.add("netstats_dev_rotate_age");
      MOVED_TO_GLOBAL.add("netstats_enabled");
      MOVED_TO_GLOBAL.add("netstats_global_alert_bytes");
      MOVED_TO_GLOBAL.add("netstats_poll_interval");
      MOVED_TO_GLOBAL.add("netstats_sample_enabled");
      MOVED_TO_GLOBAL.add("netstats_time_cache_max_age");
      MOVED_TO_GLOBAL.add("netstats_uid_bucket_duration");
      MOVED_TO_GLOBAL.add("netstats_uid_delete_age");
      MOVED_TO_GLOBAL.add("netstats_uid_persist_bytes");
      MOVED_TO_GLOBAL.add("netstats_uid_rotate_age");
      MOVED_TO_GLOBAL.add("netstats_uid_tag_bucket_duration");
      MOVED_TO_GLOBAL.add("netstats_uid_tag_delete_age");
      MOVED_TO_GLOBAL.add("netstats_uid_tag_persist_bytes");
      MOVED_TO_GLOBAL.add("netstats_uid_tag_rotate_age");
      MOVED_TO_GLOBAL.add("network_preference");
      MOVED_TO_GLOBAL.add("nitz_update_diff");
      MOVED_TO_GLOBAL.add("nitz_update_spacing");
      MOVED_TO_GLOBAL.add("ntp_server");
      MOVED_TO_GLOBAL.add("ntp_timeout");
      MOVED_TO_GLOBAL.add("ntp_server_2");
      MOVED_TO_GLOBAL.add("pdp_watchdog_error_poll_count");
      MOVED_TO_GLOBAL.add("pdp_watchdog_long_poll_interval_ms");
      MOVED_TO_GLOBAL.add("pdp_watchdog_max_pdp_reset_fail_count");
      MOVED_TO_GLOBAL.add("pdp_watchdog_poll_interval_ms");
      MOVED_TO_GLOBAL.add("pdp_watchdog_trigger_packet_count");
      MOVED_TO_GLOBAL.add("setup_prepaid_data_service_url");
      MOVED_TO_GLOBAL.add("setup_prepaid_detection_redir_host");
      MOVED_TO_GLOBAL.add("setup_prepaid_detection_target_url");
      MOVED_TO_GLOBAL.add("tether_dun_apn");
      MOVED_TO_GLOBAL.add("tether_dun_required");
      MOVED_TO_GLOBAL.add("tether_supported");
      MOVED_TO_GLOBAL.add("usb_mass_storage_enabled");
      MOVED_TO_GLOBAL.add("use_google_mail");
      MOVED_TO_GLOBAL.add("wifi_country_code");
      MOVED_TO_GLOBAL.add("wifi_framework_scan_interval_ms");
      MOVED_TO_GLOBAL.add("wifi_frequency_band");
      MOVED_TO_GLOBAL.add("wifi_idle_ms");
      MOVED_TO_GLOBAL.add("wifi_max_dhcp_retry_count");
      MOVED_TO_GLOBAL.add("wifi_mobile_data_transition_wakelock_timeout_ms");
      MOVED_TO_GLOBAL.add("wifi_networks_available_notification_on");
      MOVED_TO_GLOBAL.add("wifi_networks_available_repeat_delay");
      MOVED_TO_GLOBAL.add("wifi_num_open_networks_kept");
      MOVED_TO_GLOBAL.add("wifi_on");
      MOVED_TO_GLOBAL.add("wifi_p2p_device_name");
      MOVED_TO_GLOBAL.add("wifi_supplicant_scan_interval_ms");
      MOVED_TO_GLOBAL.add("wifi_verbose_logging_enabled");
      MOVED_TO_GLOBAL.add("wifi_enhanced_auto_join");
      MOVED_TO_GLOBAL.add("wifi_network_show_rssi");
      MOVED_TO_GLOBAL.add("wifi_watchdog_on");
      MOVED_TO_GLOBAL.add("wifi_watchdog_poor_network_test_enabled");
      MOVED_TO_GLOBAL.add("wifi_p2p_pending_factory_reset");
      MOVED_TO_GLOBAL.add("wimax_networks_available_notification_on");
      MOVED_TO_GLOBAL.add("verifier_timeout");
      MOVED_TO_GLOBAL.add("verifier_default_response");
      MOVED_TO_GLOBAL.add("data_stall_alarm_non_aggressive_delay_in_ms");
      MOVED_TO_GLOBAL.add("data_stall_alarm_aggressive_delay_in_ms");
      MOVED_TO_GLOBAL.add("gprs_register_check_period_ms");
      MOVED_TO_GLOBAL.add("wtf_is_fatal");
      MOVED_TO_GLOBAL.add("battery_discharge_duration_threshold");
      MOVED_TO_GLOBAL.add("battery_discharge_threshold");
      MOVED_TO_GLOBAL.add("send_action_app_error");
      MOVED_TO_GLOBAL.add("dropbox_age_seconds");
      MOVED_TO_GLOBAL.add("dropbox_max_files");
      MOVED_TO_GLOBAL.add("dropbox_quota_kb");
      MOVED_TO_GLOBAL.add("dropbox_quota_percent");
      MOVED_TO_GLOBAL.add("dropbox_reserve_percent");
      MOVED_TO_GLOBAL.add("dropbox:");
      MOVED_TO_GLOBAL.add("logcat_for_");
      MOVED_TO_GLOBAL.add("sys_free_storage_log_interval");
      MOVED_TO_GLOBAL.add("disk_free_change_reporting_threshold");
      MOVED_TO_GLOBAL.add("sys_storage_threshold_percentage");
      MOVED_TO_GLOBAL.add("sys_storage_threshold_max_bytes");
      MOVED_TO_GLOBAL.add("sys_storage_full_threshold_bytes");
      MOVED_TO_GLOBAL.add("sync_max_retry_delay_in_seconds");
      MOVED_TO_GLOBAL.add("connectivity_change_delay");
      MOVED_TO_GLOBAL.add("captive_portal_detection_enabled");
      MOVED_TO_GLOBAL.add("captive_portal_server");
      MOVED_TO_GLOBAL.add("nsd_on");
      MOVED_TO_GLOBAL.add("set_install_location");
      MOVED_TO_GLOBAL.add("default_install_location");
      MOVED_TO_GLOBAL.add("inet_condition_debounce_up_delay");
      MOVED_TO_GLOBAL.add("inet_condition_debounce_down_delay");
      MOVED_TO_GLOBAL.add("read_external_storage_enforced_default");
      MOVED_TO_GLOBAL.add("http_proxy");
      MOVED_TO_GLOBAL.add("global_http_proxy_host");
      MOVED_TO_GLOBAL.add("global_http_proxy_port");
      MOVED_TO_GLOBAL.add("global_http_proxy_exclusion_list");
      MOVED_TO_GLOBAL.add("set_global_http_proxy");
      MOVED_TO_GLOBAL.add("default_dns_server");
      MOVED_TO_GLOBAL.add("preferred_network_mode");
      MOVED_TO_GLOBAL.add("webview_data_reduction_proxy_key");
      LEGACY_RESTORE_SETTINGS = new String[] { "enabled_notification_listeners", "enabled_notification_assistant", "enabled_notification_policy_access_packages" };
      ArraySet<String> arraySet = new ArraySet();
      arraySet.add("accessibility_enabled");
      CLONE_TO_MANAGED_PROFILE.add("mock_location");
      CLONE_TO_MANAGED_PROFILE.add("allowed_geolocation_origins");
      CLONE_TO_MANAGED_PROFILE.add("content_capture_enabled");
      CLONE_TO_MANAGED_PROFILE.add("enabled_accessibility_services");
      CLONE_TO_MANAGED_PROFILE.add("location_changer");
      CLONE_TO_MANAGED_PROFILE.add("location_mode");
      CLONE_TO_MANAGED_PROFILE.add("location_providers_allowed");
      CLONE_TO_MANAGED_PROFILE.add("show_ime_with_hard_keyboard");
      INSTANT_APP_SETTINGS = (Set<String>)(arraySet = new ArraySet());
      arraySet.add("enabled_accessibility_services");
      INSTANT_APP_SETTINGS.add("speak_password");
      INSTANT_APP_SETTINGS.add("accessibility_display_inversion_enabled");
      INSTANT_APP_SETTINGS.add("accessibility_captioning_enabled");
      INSTANT_APP_SETTINGS.add("accessibility_captioning_preset");
      INSTANT_APP_SETTINGS.add("accessibility_captioning_edge_type");
      INSTANT_APP_SETTINGS.add("accessibility_captioning_edge_color");
      INSTANT_APP_SETTINGS.add("accessibility_captioning_locale");
      INSTANT_APP_SETTINGS.add("accessibility_captioning_background_color");
      INSTANT_APP_SETTINGS.add("accessibility_captioning_foreground_color");
      INSTANT_APP_SETTINGS.add("accessibility_captioning_typeface");
      INSTANT_APP_SETTINGS.add("accessibility_captioning_font_scale");
      INSTANT_APP_SETTINGS.add("accessibility_captioning_window_color");
      INSTANT_APP_SETTINGS.add("accessibility_display_daltonizer_enabled");
      INSTANT_APP_SETTINGS.add("accessibility_display_daltonizer");
      INSTANT_APP_SETTINGS.add("accessibility_autoclick_delay");
      INSTANT_APP_SETTINGS.add("accessibility_autoclick_enabled");
      INSTANT_APP_SETTINGS.add("accessibility_large_pointer_icon");
      INSTANT_APP_SETTINGS.add("default_input_method");
      INSTANT_APP_SETTINGS.add("enabled_input_methods");
      INSTANT_APP_SETTINGS.add("android_id");
      INSTANT_APP_SETTINGS.add("mock_location");
    }
    
    public static void getMovedToGlobalSettings(Set<String> param1Set) {
      param1Set.addAll(MOVED_TO_GLOBAL);
    }
    
    public static void clearProviderForTest() {
      sProviderHolder.clearProviderForTest();
      sNameValueCache.clearGenerationTrackerForTest();
    }
    
    public static String getString(ContentResolver param1ContentResolver, String param1String) {
      return getStringForUser(param1ContentResolver, param1String, param1ContentResolver.getUserId());
    }
    
    public static String getStringForUser(ContentResolver param1ContentResolver, String param1String, int param1Int) {
      // Byte code:
      //   0: getstatic android/provider/Settings$Secure.MOVED_TO_GLOBAL : Ljava/util/HashSet;
      //   3: aload_1
      //   4: invokevirtual contains : (Ljava/lang/Object;)Z
      //   7: ifeq -> 58
      //   10: new java/lang/StringBuilder
      //   13: dup
      //   14: invokespecial <init> : ()V
      //   17: astore_3
      //   18: aload_3
      //   19: ldc_w 'Setting '
      //   22: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   25: pop
      //   26: aload_3
      //   27: aload_1
      //   28: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   31: pop
      //   32: aload_3
      //   33: ldc_w ' has moved from android.provider.Settings.Secure to android.provider.Settings.Global.'
      //   36: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   39: pop
      //   40: ldc_w 'Settings'
      //   43: aload_3
      //   44: invokevirtual toString : ()Ljava/lang/String;
      //   47: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   50: pop
      //   51: aload_0
      //   52: aload_1
      //   53: iload_2
      //   54: invokestatic getStringForUser : (Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;
      //   57: areturn
      //   58: getstatic android/provider/Settings$Secure.MOVED_TO_LOCK_SETTINGS : Ljava/util/HashSet;
      //   61: aload_1
      //   62: invokevirtual contains : (Ljava/lang/Object;)Z
      //   65: ifeq -> 240
      //   68: ldc android/provider/Settings$Secure
      //   70: monitorenter
      //   71: getstatic android/provider/Settings$Secure.sLockSettings : Lcom/android/internal/widget/ILockSettings;
      //   74: astore_3
      //   75: iconst_1
      //   76: istore #4
      //   78: aload_3
      //   79: ifnonnull -> 119
      //   82: ldc_w 'lock_settings'
      //   85: invokestatic getService : (Ljava/lang/String;)Landroid/os/IBinder;
      //   88: astore_3
      //   89: aload_3
      //   90: invokestatic asInterface : (Landroid/os/IBinder;)Lcom/android/internal/widget/ILockSettings;
      //   93: putstatic android/provider/Settings$Secure.sLockSettings : Lcom/android/internal/widget/ILockSettings;
      //   96: invokestatic myUid : ()I
      //   99: sipush #1000
      //   102: if_icmpne -> 111
      //   105: iconst_1
      //   106: istore #5
      //   108: goto -> 114
      //   111: iconst_0
      //   112: istore #5
      //   114: iload #5
      //   116: putstatic android/provider/Settings$Secure.sIsSystemProcess : Z
      //   119: ldc android/provider/Settings$Secure
      //   121: monitorexit
      //   122: getstatic android/provider/Settings$Secure.sLockSettings : Lcom/android/internal/widget/ILockSettings;
      //   125: ifnull -> 240
      //   128: getstatic android/provider/Settings$Secure.sIsSystemProcess : Z
      //   131: ifne -> 240
      //   134: invokestatic currentApplication : ()Landroid/app/Application;
      //   137: astore_3
      //   138: aload_3
      //   139: ifnull -> 164
      //   142: aload_3
      //   143: invokevirtual getApplicationInfo : ()Landroid/content/pm/ApplicationInfo;
      //   146: ifnull -> 164
      //   149: aload_3
      //   150: invokevirtual getApplicationInfo : ()Landroid/content/pm/ApplicationInfo;
      //   153: getfield targetSdkVersion : I
      //   156: bipush #22
      //   158: if_icmpgt -> 164
      //   161: goto -> 167
      //   164: iconst_0
      //   165: istore #4
      //   167: iload #4
      //   169: ifeq -> 192
      //   172: getstatic android/provider/Settings$Secure.sLockSettings : Lcom/android/internal/widget/ILockSettings;
      //   175: aload_1
      //   176: ldc_w '0'
      //   179: iload_2
      //   180: invokeinterface getString : (Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
      //   185: astore_3
      //   186: aload_3
      //   187: areturn
      //   188: astore_3
      //   189: goto -> 240
      //   192: new java/lang/StringBuilder
      //   195: dup
      //   196: invokespecial <init> : ()V
      //   199: astore_0
      //   200: aload_0
      //   201: ldc_w 'Settings.Secure.'
      //   204: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   207: pop
      //   208: aload_0
      //   209: aload_1
      //   210: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   213: pop
      //   214: aload_0
      //   215: ldc_w ' is deprecated and no longer accessible. See API documentation for potential replacements.'
      //   218: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   221: pop
      //   222: new java/lang/SecurityException
      //   225: dup
      //   226: aload_0
      //   227: invokevirtual toString : ()Ljava/lang/String;
      //   230: invokespecial <init> : (Ljava/lang/String;)V
      //   233: athrow
      //   234: astore_0
      //   235: ldc android/provider/Settings$Secure
      //   237: monitorexit
      //   238: aload_0
      //   239: athrow
      //   240: getstatic android/provider/Settings$Secure.sNameValueCache : Landroid/provider/Settings$NameValueCache;
      //   243: aload_0
      //   244: aload_1
      //   245: iload_2
      //   246: invokevirtual getStringForUser : (Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;
      //   249: areturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5530	-> 0
      //   #5531	-> 10
      //   #5533	-> 51
      //   #5536	-> 58
      //   #5537	-> 68
      //   #5538	-> 71
      //   #5539	-> 82
      //   #5540	-> 82
      //   #5539	-> 89
      //   #5541	-> 96
      //   #5543	-> 119
      //   #5544	-> 122
      //   #5547	-> 134
      //   #5549	-> 138
      //   #5550	-> 142
      //   #5551	-> 149
      //   #5553	-> 167
      //   #5555	-> 172
      //   #5556	-> 188
      //   #5558	-> 189
      //   #5560	-> 192
      //   #5543	-> 234
      //   #5567	-> 240
      // Exception table:
      //   from	to	target	type
      //   71	75	234	finally
      //   82	89	234	finally
      //   89	96	234	finally
      //   96	105	234	finally
      //   114	119	234	finally
      //   119	122	234	finally
      //   172	186	188	android/os/RemoteException
      //   235	238	234	finally
    }
    
    public static boolean putString(ContentResolver param1ContentResolver, String param1String1, String param1String2, boolean param1Boolean) {
      int i = param1ContentResolver.getUserId();
      return putStringForUser(param1ContentResolver, param1String1, param1String2, (String)null, false, i, param1Boolean);
    }
    
    public static boolean putString(ContentResolver param1ContentResolver, String param1String1, String param1String2) {
      return putStringForUser(param1ContentResolver, param1String1, param1String2, param1ContentResolver.getUserId());
    }
    
    public static boolean putStringForUser(ContentResolver param1ContentResolver, String param1String1, String param1String2, int param1Int) {
      return putStringForUser(param1ContentResolver, param1String1, param1String2, (String)null, false, param1Int, false);
    }
    
    public static boolean putStringForUser(ContentResolver param1ContentResolver, String param1String1, String param1String2, String param1String3, boolean param1Boolean1, int param1Int, boolean param1Boolean2) {
      if (MOVED_TO_GLOBAL.contains(param1String1)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Setting ");
        stringBuilder.append(param1String1);
        stringBuilder.append(" has moved from android.provider.Settings.Secure to android.provider.Settings.Global");
        Log.w("Settings", stringBuilder.toString());
        return Settings.Global.putStringForUser(param1ContentResolver, param1String1, param1String2, param1String3, param1Boolean1, param1Int, false);
      } 
      return sNameValueCache.putStringForUser(param1ContentResolver, param1String1, param1String2, param1String3, param1Boolean1, param1Int, param1Boolean2);
    }
    
    @SystemApi
    public static boolean putString(ContentResolver param1ContentResolver, String param1String1, String param1String2, String param1String3, boolean param1Boolean) {
      int i = param1ContentResolver.getUserId();
      return putStringForUser(param1ContentResolver, param1String1, param1String2, param1String3, param1Boolean, i, false);
    }
    
    @SystemApi
    public static void resetToDefaults(ContentResolver param1ContentResolver, String param1String) {
      int i = param1ContentResolver.getUserId();
      resetToDefaultsAsUser(param1ContentResolver, param1String, 1, i);
    }
    
    public static void resetToDefaultsAsUser(ContentResolver param1ContentResolver, String param1String, int param1Int1, int param1Int2) {
      try {
        Bundle bundle = new Bundle();
        this();
        bundle.putInt("_user", param1Int2);
        if (param1String != null)
          bundle.putString("_tag", param1String); 
        bundle.putInt("_reset_mode", param1Int1);
        IContentProvider iContentProvider = sProviderHolder.getProvider(param1ContentResolver);
        param1String = param1ContentResolver.getPackageName();
        String str1 = param1ContentResolver.getAttributionTag();
        Settings.ContentProviderHolder contentProviderHolder = sProviderHolder;
        String str2 = contentProviderHolder.mUri.getAuthority();
        iContentProvider.call(param1String, str1, str2, "RESET_secure", null, bundle);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Can't reset do defaults for ");
        stringBuilder.append(CONTENT_URI);
        Log.w("Settings", stringBuilder.toString(), (Throwable)remoteException);
      } 
    }
    
    public static Uri getUriFor(String param1String) {
      if (MOVED_TO_GLOBAL.contains(param1String)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Setting ");
        stringBuilder.append(param1String);
        stringBuilder.append(" has moved from android.provider.Settings.Secure to android.provider.Settings.Global, returning global URI.");
        Log.w("Settings", stringBuilder.toString());
        return Settings.Global.getUriFor(Settings.Global.CONTENT_URI, param1String);
      } 
      return getUriFor(CONTENT_URI, param1String);
    }
    
    public static int getInt(ContentResolver param1ContentResolver, String param1String, int param1Int) {
      return getIntForUser(param1ContentResolver, param1String, param1Int, param1ContentResolver.getUserId());
    }
    
    public static int getIntForUser(ContentResolver param1ContentResolver, String param1String, int param1Int1, int param1Int2) {
      String str = getStringForUser(param1ContentResolver, param1String, param1Int2);
      if (str != null)
        try {
          param1Int1 = param1Int2 = Integer.parseInt(str);
        } catch (NumberFormatException numberFormatException) {
          return param1Int1;
        }  
      return param1Int1;
    }
    
    public static int getInt(ContentResolver param1ContentResolver, String param1String) throws Settings.SettingNotFoundException {
      return getIntForUser(param1ContentResolver, param1String, param1ContentResolver.getUserId());
    }
    
    public static int getIntForUser(ContentResolver param1ContentResolver, String param1String, int param1Int) throws Settings.SettingNotFoundException {
      String str = getStringForUser(param1ContentResolver, param1String, param1Int);
      try {
        return Integer.parseInt(str);
      } catch (NumberFormatException numberFormatException) {
        throw new Settings.SettingNotFoundException(param1String);
      } 
    }
    
    public static boolean putInt(ContentResolver param1ContentResolver, String param1String, int param1Int) {
      return putIntForUser(param1ContentResolver, param1String, param1Int, param1ContentResolver.getUserId());
    }
    
    public static boolean putIntForUser(ContentResolver param1ContentResolver, String param1String, int param1Int1, int param1Int2) {
      return putStringForUser(param1ContentResolver, param1String, Integer.toString(param1Int1), param1Int2);
    }
    
    public static long getLong(ContentResolver param1ContentResolver, String param1String, long param1Long) {
      return getLongForUser(param1ContentResolver, param1String, param1Long, param1ContentResolver.getUserId());
    }
    
    public static long getLongForUser(ContentResolver param1ContentResolver, String param1String, long param1Long, int param1Int) {
      String str = getStringForUser(param1ContentResolver, param1String, param1Int);
      if (str != null)
        try {
          long l = Long.parseLong(str);
        } catch (NumberFormatException numberFormatException) {} 
      return param1Long;
    }
    
    public static long getLong(ContentResolver param1ContentResolver, String param1String) throws Settings.SettingNotFoundException {
      return getLongForUser(param1ContentResolver, param1String, param1ContentResolver.getUserId());
    }
    
    public static long getLongForUser(ContentResolver param1ContentResolver, String param1String, int param1Int) throws Settings.SettingNotFoundException {
      String str = getStringForUser(param1ContentResolver, param1String, param1Int);
      try {
        return Long.parseLong(str);
      } catch (NumberFormatException numberFormatException) {
        throw new Settings.SettingNotFoundException(param1String);
      } 
    }
    
    public static boolean putLong(ContentResolver param1ContentResolver, String param1String, long param1Long) {
      return putLongForUser(param1ContentResolver, param1String, param1Long, param1ContentResolver.getUserId());
    }
    
    public static boolean putLongForUser(ContentResolver param1ContentResolver, String param1String, long param1Long, int param1Int) {
      return putStringForUser(param1ContentResolver, param1String, Long.toString(param1Long), param1Int);
    }
    
    public static float getFloat(ContentResolver param1ContentResolver, String param1String, float param1Float) {
      return getFloatForUser(param1ContentResolver, param1String, param1Float, param1ContentResolver.getUserId());
    }
    
    public static float getFloatForUser(ContentResolver param1ContentResolver, String param1String, float param1Float, int param1Int) {
      String str = getStringForUser(param1ContentResolver, param1String, param1Int);
      if (str != null)
        try {
          float f = Float.parseFloat(str);
        } catch (NumberFormatException numberFormatException) {
          return param1Float;
        }  
      return param1Float;
    }
    
    public static float getFloat(ContentResolver param1ContentResolver, String param1String) throws Settings.SettingNotFoundException {
      return getFloatForUser(param1ContentResolver, param1String, param1ContentResolver.getUserId());
    }
    
    public static float getFloatForUser(ContentResolver param1ContentResolver, String param1String, int param1Int) throws Settings.SettingNotFoundException {
      String str = getStringForUser(param1ContentResolver, param1String, param1Int);
      if (str != null)
        try {
          return Float.parseFloat(str);
        } catch (NumberFormatException numberFormatException) {
          throw new Settings.SettingNotFoundException(param1String);
        }  
      throw new Settings.SettingNotFoundException(param1String);
    }
    
    public static boolean putFloat(ContentResolver param1ContentResolver, String param1String, float param1Float) {
      return putFloatForUser(param1ContentResolver, param1String, param1Float, param1ContentResolver.getUserId());
    }
    
    public static boolean putFloatForUser(ContentResolver param1ContentResolver, String param1String, float param1Float, int param1Int) {
      return putStringForUser(param1ContentResolver, param1String, Float.toString(param1Float), param1Int);
    }
    
    public static void getCloneToManagedProfileSettings(Set<String> param1Set) {
      param1Set.addAll(CLONE_TO_MANAGED_PROFILE);
    }
    
    @Deprecated
    public static boolean isLocationProviderEnabled(ContentResolver param1ContentResolver, String param1String) {
      int i = param1ContentResolver.getUserId();
      String str = getStringForUser(param1ContentResolver, "location_providers_allowed", i);
      return TextUtils.delimitedStringContains(str, ',', param1String);
    }
    
    @Deprecated
    public static void setLocationProviderEnabled(ContentResolver param1ContentResolver, String param1String, boolean param1Boolean) {}
    
    @Retention(RetentionPolicy.SOURCE)
    class UserSetupPersonalization implements Annotation {}
  }
  
  class Global extends NameValueTable implements OplusSettings.Mtk_Global, OplusSettings.Qcom_Global {
    public static final String ACTIVITY_MANAGER_CONSTANTS = "activity_manager_constants";
    
    public static final String ACTIVITY_STARTS_LOGGING_ENABLED = "activity_starts_logging_enabled";
    
    public static final String ADAPTIVE_BATTERY_MANAGEMENT_ENABLED = "adaptive_battery_management_enabled";
    
    public static final String ADB_ALLOWED_CONNECTION_TIME = "adb_allowed_connection_time";
    
    public static final String ADB_ENABLED = "adb_enabled";
    
    public static final String ADB_WIFI_ENABLED = "adb_wifi_enabled";
    
    public static final String ADD_USERS_WHEN_LOCKED = "add_users_when_locked";
    
    public static final String ADVANCED_BATTERY_USAGE_AMOUNT = "advanced_battery_usage_amount";
    
    public static final String AIRPLANE_MODE_ON = "airplane_mode_on";
    
    public static final String AIRPLANE_MODE_RADIOS = "airplane_mode_radios";
    
    @SystemApi
    public static final String AIRPLANE_MODE_TOGGLEABLE_RADIOS = "airplane_mode_toggleable_radios";
    
    public static final String ALARM_MANAGER_CONSTANTS = "alarm_manager_constants";
    
    public static final String ALLOW_USER_SWITCHING_WHEN_SYSTEM_USER_LOCKED = "allow_user_switching_when_system_user_locked";
    
    public static final String ALWAYS_FINISH_ACTIVITIES = "always_finish_activities";
    
    public static final String ALWAYS_ON_DISPLAY_CONSTANTS = "always_on_display_constants";
    
    public static final String ANIMATOR_DURATION_SCALE = "animator_duration_scale";
    
    public static final String ANOMALY_CONFIG = "anomaly_config";
    
    public static final String ANOMALY_CONFIG_VERSION = "anomaly_config_version";
    
    public static final String ANOMALY_DETECTION_CONSTANTS = "anomaly_detection_constants";
    
    public static final String APN_DB_UPDATE_CONTENT_URL = "apn_db_content_url";
    
    public static final String APN_DB_UPDATE_METADATA_URL = "apn_db_metadata_url";
    
    public static final String APPLY_RAMPING_RINGER = "apply_ramping_ringer";
    
    public static final String APPOP_HISTORY_BASE_INTERVAL_MILLIS = "baseIntervalMillis";
    
    public static final String APPOP_HISTORY_INTERVAL_MULTIPLIER = "intervalMultiplier";
    
    public static final String APPOP_HISTORY_MODE = "mode";
    
    public static final String APPOP_HISTORY_PARAMETERS = "appop_history_parameters";
    
    public static final String APP_AUTO_RESTRICTION_ENABLED = "app_auto_restriction_enabled";
    
    public static final String APP_BINDING_CONSTANTS = "app_binding_constants";
    
    public static final String APP_IDLE_CONSTANTS = "app_idle_constants";
    
    public static final String APP_INTEGRITY_VERIFICATION_TIMEOUT = "app_integrity_verification_timeout";
    
    public static final String APP_OPS_CONSTANTS = "app_ops_constants";
    
    @SystemApi
    public static final String APP_STANDBY_ENABLED = "app_standby_enabled";
    
    public static final String APP_TIME_LIMIT_USAGE_SOURCE = "app_time_limit_usage_source";
    
    public static final String ART_VERIFIER_VERIFY_DEBUGGABLE = "art_verifier_verify_debuggable";
    
    public static final String ASSISTED_GPS_ENABLED = "assisted_gps_enabled";
    
    public static final String AUDIO_SAFE_VOLUME_STATE = "audio_safe_volume_state";
    
    @SystemApi
    public static final String AUTOFILL_COMPAT_MODE_ALLOWED_PACKAGES = "autofill_compat_mode_allowed_packages";
    
    public static final String AUTOFILL_LOGGING_LEVEL = "autofill_logging_level";
    
    public static final String AUTOFILL_MAX_PARTITIONS_SIZE = "autofill_max_partitions_size";
    
    public static final String AUTOFILL_MAX_VISIBLE_DATASETS = "autofill_max_visible_datasets";
    
    public static final String AUTOMATIC_POWER_SAVE_MODE = "automatic_power_save_mode";
    
    public static final String AUTO_REVOKE_PARAMETERS = "auto_revoke_parameters";
    
    public static final String AUTO_TIME = "auto_time";
    
    public static final String AUTO_TIME_ZONE = "auto_time_zone";
    
    public static final String AVERAGE_TIME_TO_DISCHARGE = "average_time_to_discharge";
    
    public static final String AWARE_ALLOWED = "aware_allowed";
    
    public static final String BACKUP_AGENT_TIMEOUT_PARAMETERS = "backup_agent_timeout_parameters";
    
    public static final String BATTERY_CHARGING_STATE_UPDATE_DELAY = "battery_charging_state_update_delay";
    
    public static final String BATTERY_DISCHARGE_DURATION_THRESHOLD = "battery_discharge_duration_threshold";
    
    public static final String BATTERY_DISCHARGE_THRESHOLD = "battery_discharge_threshold";
    
    public static final String BATTERY_ESTIMATES_LAST_UPDATE_TIME = "battery_estimates_last_update_time";
    
    public static final String BATTERY_SAVER_ADAPTIVE_CONSTANTS = "battery_saver_adaptive_constants";
    
    public static final String BATTERY_SAVER_ADAPTIVE_DEVICE_SPECIFIC_CONSTANTS = "battery_saver_adaptive_device_specific_constants";
    
    public static final String BATTERY_SAVER_CONSTANTS = "battery_saver_constants";
    
    public static final String BATTERY_SAVER_DEVICE_SPECIFIC_CONSTANTS = "battery_saver_device_specific_constants";
    
    public static final String BATTERY_STATS_CONSTANTS = "battery_stats_constants";
    
    public static final String BATTERY_TIP_CONSTANTS = "battery_tip_constants";
    
    public static final String BINDER_CALLS_STATS = "binder_calls_stats";
    
    public static final String BLE_SCAN_ALWAYS_AVAILABLE = "ble_scan_always_enabled";
    
    public static final String BLE_SCAN_BACKGROUND_MODE = "ble_scan_background_mode";
    
    public static final String BLE_SCAN_BALANCED_INTERVAL_MS = "ble_scan_balanced_interval_ms";
    
    public static final String BLE_SCAN_BALANCED_WINDOW_MS = "ble_scan_balanced_window_ms";
    
    public static final String BLE_SCAN_LOW_LATENCY_INTERVAL_MS = "ble_scan_low_latency_interval_ms";
    
    public static final String BLE_SCAN_LOW_LATENCY_WINDOW_MS = "ble_scan_low_latency_window_ms";
    
    public static final String BLE_SCAN_LOW_POWER_INTERVAL_MS = "ble_scan_low_power_interval_ms";
    
    public static final String BLE_SCAN_LOW_POWER_WINDOW_MS = "ble_scan_low_power_window_ms";
    
    public static final String BLOCKED_SLICES = "blocked_slices";
    
    public static final String BLOCKING_HELPER_DISMISS_TO_VIEW_RATIO_LIMIT = "blocking_helper_dismiss_to_view_ratio";
    
    public static final String BLOCKING_HELPER_STREAK_LIMIT = "blocking_helper_streak_limit";
    
    public static final String BLUETOOTH_A2DP_OPTIONAL_CODECS_ENABLED_PREFIX = "bluetooth_a2dp_optional_codecs_enabled_";
    
    public static final String BLUETOOTH_A2DP_SINK_PRIORITY_PREFIX = "bluetooth_a2dp_sink_priority_";
    
    public static final String BLUETOOTH_A2DP_SRC_PRIORITY_PREFIX = "bluetooth_a2dp_src_priority_";
    
    public static final String BLUETOOTH_A2DP_SUPPORTS_OPTIONAL_CODECS_PREFIX = "bluetooth_a2dp_supports_optional_codecs_";
    
    public static final String BLUETOOTH_BTSNOOP_DEFAULT_MODE = "bluetooth_btsnoop_default_mode";
    
    public static final String BLUETOOTH_CLASS_OF_DEVICE = "bluetooth_class_of_device";
    
    public static final String BLUETOOTH_DISABLED_PROFILES = "bluetooth_disabled_profiles";
    
    public static final String BLUETOOTH_HEADSET_PRIORITY_PREFIX = "bluetooth_headset_priority_";
    
    public static final String BLUETOOTH_HEARING_AID_PRIORITY_PREFIX = "bluetooth_hearing_aid_priority_";
    
    public static final String BLUETOOTH_INPUT_DEVICE_PRIORITY_PREFIX = "bluetooth_input_device_priority_";
    
    public static final String BLUETOOTH_INTEROPERABILITY_LIST = "bluetooth_interoperability_list";
    
    public static final String BLUETOOTH_MAP_CLIENT_PRIORITY_PREFIX = "bluetooth_map_client_priority_";
    
    public static final String BLUETOOTH_MAP_PRIORITY_PREFIX = "bluetooth_map_priority_";
    
    public static final String BLUETOOTH_ON = "bluetooth_on";
    
    public static final String BLUETOOTH_PAN_PRIORITY_PREFIX = "bluetooth_pan_priority_";
    
    public static final String BLUETOOTH_PBAP_CLIENT_PRIORITY_PREFIX = "bluetooth_pbap_client_priority_";
    
    public static final String BLUETOOTH_SAP_PRIORITY_PREFIX = "bluetooth_sap_priority_";
    
    public static final String BOOT_COUNT = "boot_count";
    
    public static final String BROADCAST_BG_CONSTANTS = "bcast_bg_constants";
    
    public static final String BROADCAST_FG_CONSTANTS = "bcast_fg_constants";
    
    public static final String BROADCAST_OFFLOAD_CONSTANTS = "bcast_offload_constants";
    
    public static final String BUGREPORT_IN_POWER_MENU = "bugreport_in_power_menu";
    
    public static final String CACHED_APPS_FREEZER_ENABLED = "cached_apps_freezer";
    
    public static final String CALL_AUTO_RETRY = "call_auto_retry";
    
    @Deprecated
    public static final String CAPTIVE_PORTAL_DETECTION_ENABLED = "captive_portal_detection_enabled";
    
    public static final String CAPTIVE_PORTAL_FALLBACK_PROBE_SPECS = "captive_portal_fallback_probe_specs";
    
    public static final String CAPTIVE_PORTAL_FALLBACK_URL = "captive_portal_fallback_url";
    
    public static final String CAPTIVE_PORTAL_HTTPS_URL = "captive_portal_https_url";
    
    public static final String CAPTIVE_PORTAL_HTTP_URL = "captive_portal_http_url";
    
    public static final String CAPTIVE_PORTAL_MODE = "captive_portal_mode";
    
    public static final int CAPTIVE_PORTAL_MODE_AVOID = 2;
    
    public static final int CAPTIVE_PORTAL_MODE_IGNORE = 0;
    
    public static final int CAPTIVE_PORTAL_MODE_PROMPT = 1;
    
    public static final String CAPTIVE_PORTAL_OTHER_FALLBACK_URLS = "captive_portal_other_fallback_urls";
    
    public static final String CAPTIVE_PORTAL_SERVER = "captive_portal_server";
    
    public static final String CAPTIVE_PORTAL_USER_AGENT = "captive_portal_user_agent";
    
    public static final String CAPTIVE_PORTAL_USE_HTTPS = "captive_portal_use_https";
    
    @SystemApi
    public static final String CARRIER_APP_NAMES = "carrier_app_names";
    
    @SystemApi
    public static final String CARRIER_APP_WHITELIST = "carrier_app_whitelist";
    
    public static final String CAR_DOCK_SOUND = "car_dock_sound";
    
    public static final String CAR_UNDOCK_SOUND = "car_undock_sound";
    
    public static final String CDMA_CELL_BROADCAST_SMS = "cdma_cell_broadcast_sms";
    
    public static final String CDMA_ROAMING_MODE = "roaming_settings";
    
    public static final String CDMA_SUBSCRIPTION_MODE = "subscription_mode";
    
    public static final String CELL_ON = "cell_on";
    
    public static final String CERT_PIN_UPDATE_CONTENT_URL = "cert_pin_content_url";
    
    public static final String CERT_PIN_UPDATE_METADATA_URL = "cert_pin_metadata_url";
    
    public static final String CHAINED_BATTERY_ATTRIBUTION_ENABLED = "chained_battery_attribution_enabled";
    
    @Deprecated
    public static final String CHARGING_SOUNDS_ENABLED = "charging_sounds_enabled";
    
    public static final String CHARGING_STARTED_SOUND = "charging_started_sound";
    
    @Deprecated
    public static final String CHARGING_VIBRATION_ENABLED = "charging_vibration_enabled";
    
    public static final String COMPATIBILITY_MODE = "compatibility_mode";
    
    public static final String CONNECTIVITY_CHANGE_DELAY = "connectivity_change_delay";
    
    public static final String CONNECTIVITY_METRICS_BUFFER_SIZE = "connectivity_metrics_buffer_size";
    
    public static final String CONNECTIVITY_SAMPLING_INTERVAL_IN_SECONDS = "connectivity_sampling_interval_in_seconds";
    
    public static final String CONTACTS_DATABASE_WAL_ENABLED = "contacts_database_wal_enabled";
    
    @Deprecated
    public static final String CONTACT_METADATA_SYNC = "contact_metadata_sync";
    
    public static final String CONTACT_METADATA_SYNC_ENABLED = "contact_metadata_sync_enabled";
    
    public static final Uri CONTENT_URI;
    
    public static final String CONVERSATION_ACTIONS_UPDATE_CONTENT_URL = "conversation_actions_content_url";
    
    public static final String CONVERSATION_ACTIONS_UPDATE_METADATA_URL = "conversation_actions_metadata_url";
    
    public static final String CUSTOM_BUGREPORT_HANDLER_APP = "custom_bugreport_handler_app";
    
    public static final String CUSTOM_BUGREPORT_HANDLER_USER = "custom_bugreport_handler_user";
    
    public static final String DATABASE_CREATION_BUILDID = "database_creation_buildid";
    
    public static final String DATABASE_DOWNGRADE_REASON = "database_downgrade_reason";
    
    public static final String DATA_ACTIVITY_TIMEOUT_MOBILE = "data_activity_timeout_mobile";
    
    public static final String DATA_ACTIVITY_TIMEOUT_WIFI = "data_activity_timeout_wifi";
    
    public static final String DATA_ROAMING = "data_roaming";
    
    public static final String DATA_STALL_ALARM_AGGRESSIVE_DELAY_IN_MS = "data_stall_alarm_aggressive_delay_in_ms";
    
    public static final String DATA_STALL_ALARM_NON_AGGRESSIVE_DELAY_IN_MS = "data_stall_alarm_non_aggressive_delay_in_ms";
    
    public static final String DATA_STALL_RECOVERY_ON_BAD_NETWORK = "data_stall_recovery_on_bad_network";
    
    public static final String DEBUG_APP = "debug_app";
    
    public static final String DEBUG_VIEW_ATTRIBUTES = "debug_view_attributes";
    
    public static final String DEBUG_VIEW_ATTRIBUTES_APPLICATION_PACKAGE = "debug_view_attributes_application_package";
    
    public static final long DEFAULT_ADB_ALLOWED_CONNECTION_TIME = 604800000L;
    
    public static final String DEFAULT_DNS_SERVER = "default_dns_server";
    
    public static final int DEFAULT_ENABLE_RESTRICTED_BUCKET = 0;
    
    public static final String DEFAULT_INSTALL_LOCATION = "default_install_location";
    
    public static final String DEFAULT_RESTRICT_BACKGROUND_DATA = "default_restrict_background_data";
    
    @SystemApi
    public static final String DEFAULT_SM_DP_PLUS = "default_sm_dp_plus";
    
    public static final String DESK_DOCK_SOUND = "desk_dock_sound";
    
    public static final String DESK_UNDOCK_SOUND = "desk_undock_sound";
    
    public static final String DEVELOPMENT_ENABLE_FREEFORM_WINDOWS_SUPPORT = "enable_freeform_support";
    
    public static final String DEVELOPMENT_ENABLE_SIZECOMPAT_FREEFORM = "enable_sizecompat_freeform";
    
    public static final String DEVELOPMENT_FORCE_DESKTOP_MODE_ON_EXTERNAL_DISPLAYS = "force_desktop_mode_on_external_displays";
    
    public static final String DEVELOPMENT_FORCE_RESIZABLE_ACTIVITIES = "force_resizable_activities";
    
    public static final String DEVELOPMENT_FORCE_RTL = "debug.force_rtl";
    
    public static final String DEVELOPMENT_RENDER_SHADOWS_IN_COMPOSITOR = "render_shadows_in_compositor";
    
    public static final String DEVELOPMENT_SETTINGS_ENABLED = "development_settings_enabled";
    
    @SystemApi
    public static final String DEVICE_DEMO_MODE = "device_demo_mode";
    
    public static final String DEVICE_IDLE_CONSTANTS = "device_idle_constants";
    
    public static final String DEVICE_NAME = "device_name";
    
    public static final String DEVICE_POLICY_CONSTANTS = "device_policy_constants";
    
    public static final String DEVICE_PROVISIONED = "device_provisioned";
    
    @SystemApi
    public static final String DEVICE_PROVISIONING_MOBILE_DATA_ENABLED = "device_provisioning_mobile_data";
    
    public static final String DISK_FREE_CHANGE_REPORTING_THRESHOLD = "disk_free_change_reporting_threshold";
    
    public static final String DISPLAY_PANEL_LPM = "display_panel_lpm";
    
    public static final String DISPLAY_SCALING_FORCE = "display_scaling_force";
    
    public static final String DISPLAY_SIZE_FORCED = "display_size_forced";
    
    public static final String DNS_RESOLVER_MAX_SAMPLES = "dns_resolver_max_samples";
    
    public static final String DNS_RESOLVER_MIN_SAMPLES = "dns_resolver_min_samples";
    
    public static final String DNS_RESOLVER_SAMPLE_VALIDITY_SECONDS = "dns_resolver_sample_validity_seconds";
    
    public static final String DNS_RESOLVER_SUCCESS_THRESHOLD_PERCENT = "dns_resolver_success_threshold_percent";
    
    public static final String DOCK_AUDIO_MEDIA_ENABLED = "dock_audio_media_enabled";
    
    public static final String DOCK_SOUNDS_ENABLED = "dock_sounds_enabled";
    
    public static final String DOCK_SOUNDS_ENABLED_WHEN_ACCESSIBILITY = "dock_sounds_enabled_when_accessbility";
    
    public static final String DOWNLOAD_MAX_BYTES_OVER_MOBILE = "download_manager_max_bytes_over_mobile";
    
    public static final String DOWNLOAD_RECOMMENDED_MAX_BYTES_OVER_MOBILE = "download_manager_recommended_max_bytes_over_mobile";
    
    public static final String DROPBOX_AGE_SECONDS = "dropbox_age_seconds";
    
    public static final String DROPBOX_MAX_FILES = "dropbox_max_files";
    
    public static final String DROPBOX_QUOTA_KB = "dropbox_quota_kb";
    
    public static final String DROPBOX_QUOTA_PERCENT = "dropbox_quota_percent";
    
    public static final String DROPBOX_RESERVE_PERCENT = "dropbox_reserve_percent";
    
    public static final String DROPBOX_TAG_PREFIX = "dropbox:";
    
    public static final String DYNAMIC_POWER_SAVINGS_DISABLE_THRESHOLD = "dynamic_power_savings_disable_threshold";
    
    public static final String DYNAMIC_POWER_SAVINGS_ENABLED = "dynamic_power_savings_enabled";
    
    public static final String EMERGENCY_AFFORDANCE_NEEDED = "emergency_affordance_needed";
    
    public static final String EMERGENCY_TONE = "emergency_tone";
    
    public static final String EMULATE_DISPLAY_CUTOUT = "emulate_display_cutout";
    
    public static final int EMULATE_DISPLAY_CUTOUT_OFF = 0;
    
    public static final int EMULATE_DISPLAY_CUTOUT_ON = 1;
    
    public static final String ENABLED_SUBSCRIPTION_FOR_SLOT = "enabled_subscription_for_slot";
    
    public static final String ENABLE_ACCESSIBILITY_GLOBAL_GESTURE_ENABLED = "enable_accessibility_global_gesture_enabled";
    
    public static final String ENABLE_ADB_INCREMENTAL_INSTALL_DEFAULT = "enable_adb_incremental_install_default";
    
    public static final String ENABLE_AUTOMATIC_SYSTEM_SERVER_HEAP_DUMPS = "enable_automatic_system_server_heap_dumps";
    
    public static final String ENABLE_CACHE_QUOTA_CALCULATION = "enable_cache_quota_calculation";
    
    public static final String ENABLE_CELLULAR_ON_BOOT = "enable_cellular_on_boot";
    
    public static final String ENABLE_DELETION_HELPER_NO_THRESHOLD_TOGGLE = "enable_deletion_helper_no_threshold_toggle";
    
    public static final String ENABLE_DISKSTATS_LOGGING = "enable_diskstats_logging";
    
    public static final String ENABLE_EPHEMERAL_FEATURE = "enable_ephemeral_feature";
    
    public static final String ENABLE_GNSS_RAW_MEAS_FULL_TRACKING = "enable_gnss_raw_meas_full_tracking";
    
    public static final String ENABLE_GPU_DEBUG_LAYERS = "enable_gpu_debug_layers";
    
    public static final String ENABLE_RADIO_BUG_DETECTION = "enable_radio_bug_detection";
    
    public static final String ENABLE_RESTRICTED_BUCKET = "enable_restricted_bucket";
    
    public static final String ENCODED_SURROUND_OUTPUT = "encoded_surround_output";
    
    public static final int ENCODED_SURROUND_OUTPUT_ALWAYS = 2;
    
    public static final int ENCODED_SURROUND_OUTPUT_AUTO = 0;
    
    public static final String ENCODED_SURROUND_OUTPUT_ENABLED_FORMATS = "encoded_surround_output_enabled_formats";
    
    public static final int ENCODED_SURROUND_OUTPUT_MANUAL = 3;
    
    public static final int ENCODED_SURROUND_OUTPUT_NEVER = 1;
    
    @Deprecated
    public static final String ENHANCED_4G_MODE_ENABLED = "volte_vt_enabled";
    
    public static final String ENHANCED_CONNECTIVITY_ENABLED = "enhanced_connectivity_enable";
    
    public static final String EPHEMERAL_COOKIE_MAX_SIZE_BYTES = "ephemeral_cookie_max_size_bytes";
    
    public static final String ERROR_LOGCAT_PREFIX = "logcat_for_";
    
    public static final String EUICC_FACTORY_RESET_TIMEOUT_MILLIS = "euicc_factory_reset_timeout_millis";
    
    @SystemApi
    public static final String EUICC_PROVISIONED = "euicc_provisioned";
    
    public static final String EUICC_REMOVING_INVISIBLE_PROFILES_TIMEOUT_MILLIS = "euicc_removing_invisible_profiles_timeout_millis";
    
    @SystemApi
    public static final String EUICC_SUPPORTED_COUNTRIES = "euicc_supported_countries";
    
    @SystemApi
    public static final String EUICC_UNSUPPORTED_COUNTRIES = "euicc_unsupported_countries";
    
    public static final String FANCY_IME_ANIMATIONS = "fancy_ime_animations";
    
    public static final String FORCED_APP_STANDBY_ENABLED = "forced_app_standby_enabled";
    
    public static final String FORCED_APP_STANDBY_FOR_SMALL_BATTERY_ENABLED = "forced_app_standby_for_small_battery_enabled";
    
    public static final String FORCE_ALLOW_ON_EXTERNAL = "force_allow_on_external";
    
    public static final String FOREGROUND_SERVICE_STARTS_LOGGING_ENABLED = "foreground_service_starts_logging_enabled";
    
    public static final String FPS_DEVISOR = "fps_divisor";
    
    public static final String FSTRIM_MANDATORY_INTERVAL = "fstrim_mandatory_interval";
    
    public static final String GAME_DRIVER_ALL_APPS = "game_driver_all_apps";
    
    public static final String GAME_DRIVER_BLACKLIST = "game_driver_blacklist";
    
    public static final String GAME_DRIVER_BLACKLISTS = "game_driver_blacklists";
    
    public static final String GAME_DRIVER_OPT_IN_APPS = "game_driver_opt_in_apps";
    
    public static final String GAME_DRIVER_OPT_OUT_APPS = "game_driver_opt_out_apps";
    
    public static final String GAME_DRIVER_PRERELEASE_OPT_IN_APPS = "game_driver_prerelease_opt_in_apps";
    
    public static final String GAME_DRIVER_SPHAL_LIBRARIES = "game_driver_sphal_libraries";
    
    public static final String GAME_DRIVER_WHITELIST = "game_driver_whitelist";
    
    public static final String GLOBAL_HTTP_PROXY_EXCLUSION_LIST = "global_http_proxy_exclusion_list";
    
    public static final String GLOBAL_HTTP_PROXY_HOST = "global_http_proxy_host";
    
    public static final String GLOBAL_HTTP_PROXY_PAC = "global_proxy_pac_url";
    
    public static final String GLOBAL_HTTP_PROXY_PORT = "global_http_proxy_port";
    
    public static final String GLOBAL_SETTINGS_ANGLE_DEBUG_PACKAGE = "angle_debug_package";
    
    public static final String GLOBAL_SETTINGS_ANGLE_GL_DRIVER_ALL_ANGLE = "angle_gl_driver_all_angle";
    
    public static final String GLOBAL_SETTINGS_ANGLE_GL_DRIVER_SELECTION_PKGS = "angle_gl_driver_selection_pkgs";
    
    public static final String GLOBAL_SETTINGS_ANGLE_GL_DRIVER_SELECTION_VALUES = "angle_gl_driver_selection_values";
    
    public static final String GLOBAL_SETTINGS_ANGLE_WHITELIST = "angle_whitelist";
    
    public static final String GLOBAL_SETTINGS_SHOW_ANGLE_IN_USE_DIALOG_BOX = "show_angle_in_use_dialog_box";
    
    public static final String GNSS_HAL_LOCATION_REQUEST_DURATION_MILLIS = "gnss_hal_location_request_duration_millis";
    
    public static final String GNSS_SATELLITE_BLACKLIST = "gnss_satellite_blacklist";
    
    public static final String GPRS_REGISTER_CHECK_PERIOD_MS = "gprs_register_check_period_ms";
    
    public static final String GPU_DEBUG_APP = "gpu_debug_app";
    
    public static final String GPU_DEBUG_LAYERS = "gpu_debug_layers";
    
    public static final String GPU_DEBUG_LAYERS_GLES = "gpu_debug_layers_gles";
    
    public static final String GPU_DEBUG_LAYER_APP = "gpu_debug_layer_app";
    
    public static final String HDMI_CEC_SWITCH_ENABLED = "hdmi_cec_switch_enabled";
    
    public static final String HDMI_CONTROL_AUTO_DEVICE_OFF_ENABLED = "hdmi_control_auto_device_off_enabled";
    
    public static final String HDMI_CONTROL_AUTO_WAKEUP_ENABLED = "hdmi_control_auto_wakeup_enabled";
    
    public static final String HDMI_CONTROL_ENABLED = "hdmi_control_enabled";
    
    public static final String HDMI_CONTROL_VOLUME_CONTROL_ENABLED = "hdmi_control_volume_control_enabled";
    
    public static final String HDMI_SYSTEM_AUDIO_CONTROL_ENABLED = "hdmi_system_audio_control_enabled";
    
    public static final String HEADS_UP_NOTIFICATIONS_ENABLED = "heads_up_notifications_enabled";
    
    public static final int HEADS_UP_OFF = 0;
    
    public static final int HEADS_UP_ON = 1;
    
    public static final String HIDDEN_API_BLACKLIST_EXEMPTIONS = "hidden_api_blacklist_exemptions";
    
    public static final String HIDDEN_API_POLICY = "hidden_api_policy";
    
    public static final String HIDE_ERROR_DIALOGS = "hide_error_dialogs";
    
    public static final String HTTP_PROXY = "http_proxy";
    
    public static final String INET_CONDITION_DEBOUNCE_DOWN_DELAY = "inet_condition_debounce_down_delay";
    
    public static final String INET_CONDITION_DEBOUNCE_UP_DELAY = "inet_condition_debounce_up_delay";
    
    public static final String INSTALLED_INSTANT_APP_MAX_CACHE_PERIOD = "installed_instant_app_max_cache_period";
    
    public static final String INSTALLED_INSTANT_APP_MIN_CACHE_PERIOD = "installed_instant_app_min_cache_period";
    
    @SystemApi
    public static final String INSTALL_CARRIER_APP_NOTIFICATION_PERSISTENT = "install_carrier_app_notification_persistent";
    
    @SystemApi
    public static final String INSTALL_CARRIER_APP_NOTIFICATION_SLEEP_MILLIS = "install_carrier_app_notification_sleep_millis";
    
    @Deprecated
    public static final String INSTALL_NON_MARKET_APPS = "install_non_market_apps";
    
    public static final String INSTANT_APP_DEXOPT_ENABLED = "instant_app_dexopt_enabled";
    
    public static final Set<String> INSTANT_APP_SETTINGS;
    
    public static final String INTEGRITY_CHECK_INCLUDES_RULE_PROVIDER = "verify_integrity_for_rule_provider";
    
    public static final String INTENT_FIREWALL_UPDATE_CONTENT_URL = "intent_firewall_content_url";
    
    public static final String INTENT_FIREWALL_UPDATE_METADATA_URL = "intent_firewall_metadata_url";
    
    public static final String ISOLATED_STORAGE_LOCAL = "isolated_storage_local";
    
    public static final String ISOLATED_STORAGE_REMOTE = "isolated_storage_remote";
    
    public static final String JOB_SCHEDULER_CONSTANTS = "job_scheduler_constants";
    
    public static final String JOB_SCHEDULER_QUOTA_CONTROLLER_CONSTANTS = "job_scheduler_quota_controller_constants";
    
    public static final String JOB_SCHEDULER_TIME_CONTROLLER_CONSTANTS = "job_scheduler_time_controller_constants";
    
    public static final String KEEP_PROFILE_IN_BACKGROUND = "keep_profile_in_background";
    
    public static final String KERNEL_CPU_THREAD_READER = "kernel_cpu_thread_reader";
    
    public static final String LANG_ID_UPDATE_CONTENT_URL = "lang_id_content_url";
    
    public static final String LANG_ID_UPDATE_METADATA_URL = "lang_id_metadata_url";
    
    public static final String[] LEGACY_RESTORE_SETTINGS;
    
    public static final String LID_BEHAVIOR = "lid_behavior";
    
    public static final String LOCATION_BACKGROUND_THROTTLE_INTERVAL_MS = "location_background_throttle_interval_ms";
    
    public static final String LOCATION_BACKGROUND_THROTTLE_PACKAGE_WHITELIST = "location_background_throttle_package_whitelist";
    
    public static final String LOCATION_BACKGROUND_THROTTLE_PROXIMITY_ALERT_INTERVAL_MS = "location_background_throttle_proximity_alert_interval_ms";
    
    public static final String LOCATION_GLOBAL_KILL_SWITCH = "location_global_kill_switch";
    
    public static final String LOCATION_IGNORE_SETTINGS_PACKAGE_WHITELIST = "location_ignore_settings_package_whitelist";
    
    public static final String LOCATION_SETTINGS_LINK_TO_PERMISSIONS_ENABLED = "location_settings_link_to_permissions_enabled";
    
    public static final String LOCK_SOUND = "lock_sound";
    
    public static final String LOOPER_STATS = "looper_stats";
    
    public static final String LOW_BATTERY_SOUND = "low_battery_sound";
    
    public static final String LOW_BATTERY_SOUND_TIMEOUT = "low_battery_sound_timeout";
    
    public static final String LOW_POWER_MODE = "low_power";
    
    public static final String LOW_POWER_MODE_STICKY = "low_power_sticky";
    
    public static final String LOW_POWER_MODE_STICKY_AUTO_DISABLE_ENABLED = "low_power_sticky_auto_disable_enabled";
    
    public static final String LOW_POWER_MODE_STICKY_AUTO_DISABLE_LEVEL = "low_power_sticky_auto_disable_level";
    
    public static final String LOW_POWER_MODE_SUGGESTION_PARAMS = "low_power_mode_suggestion_params";
    
    public static final String LOW_POWER_MODE_TRIGGER_LEVEL = "low_power_trigger_level";
    
    public static final String LOW_POWER_MODE_TRIGGER_LEVEL_MAX = "low_power_trigger_level_max";
    
    public static final String LTE_SERVICE_FORCED = "lte_service_forced";
    
    public static final String MAX_ERROR_BYTES_PREFIX = "max_error_bytes_for_";
    
    public static final String MAX_NOTIFICATION_ENQUEUE_RATE = "max_notification_enqueue_rate";
    
    public static final String MAX_SOUND_TRIGGER_DETECTION_SERVICE_OPS_PER_DAY = "max_sound_trigger_detection_service_ops_per_day";
    
    public static final String MDC_INITIAL_MAX_RETRY = "mdc_initial_max_retry";
    
    public static final String MHL_INPUT_SWITCHING_ENABLED = "mhl_input_switching_enabled";
    
    public static final String MHL_POWER_CHARGE_ENABLED = "mhl_power_charge_enabled";
    
    public static final String MIN_DURATION_BETWEEN_RECOVERY_STEPS_IN_MS = "min_duration_between_recovery_steps";
    
    public static final String MOBILE_DATA = "mobile_data";
    
    public static final String MOBILE_DATA_ALWAYS_ON = "mobile_data_always_on";
    
    public static final String MODEM_STACK_ENABLED_FOR_SLOT = "modem_stack_enabled_for_slot";
    
    public static final String MODE_RINGER = "mode_ringer";
    
    private static final HashSet<String> MOVED_TO_SECURE;
    
    public static final String MULTI_SIM_DATA_CALL_SUBSCRIPTION = "multi_sim_data_call";
    
    public static final String MULTI_SIM_SMS_PROMPT = "multi_sim_sms_prompt";
    
    public static final String MULTI_SIM_SMS_SUBSCRIPTION = "multi_sim_sms";
    
    public static final String[] MULTI_SIM_USER_PREFERRED_SUBS;
    
    public static final String MULTI_SIM_VOICE_CALL_SUBSCRIPTION = "multi_sim_voice_call";
    
    public static final String MULTI_SIM_VOICE_PROMPT = "multi_sim_voice_prompt";
    
    public static final String NATIVE_FLAGS_HEALTH_CHECK_ENABLED = "native_flags_health_check_enabled";
    
    public static final String NETPOLICY_OVERRIDE_ENABLED = "netpolicy_override_enabled";
    
    public static final String NETPOLICY_QUOTA_ENABLED = "netpolicy_quota_enabled";
    
    public static final String NETPOLICY_QUOTA_FRAC_JOBS = "netpolicy_quota_frac_jobs";
    
    public static final String NETPOLICY_QUOTA_FRAC_MULTIPATH = "netpolicy_quota_frac_multipath";
    
    public static final String NETPOLICY_QUOTA_LIMITED = "netpolicy_quota_limited";
    
    public static final String NETPOLICY_QUOTA_UNLIMITED = "netpolicy_quota_unlimited";
    
    public static final String NETSTATS_AUGMENT_ENABLED = "netstats_augment_enabled";
    
    public static final String NETSTATS_COMBINE_SUBTYPE_ENABLED = "netstats_combine_subtype_enabled";
    
    public static final String NETSTATS_DEV_BUCKET_DURATION = "netstats_dev_bucket_duration";
    
    public static final String NETSTATS_DEV_DELETE_AGE = "netstats_dev_delete_age";
    
    public static final String NETSTATS_DEV_PERSIST_BYTES = "netstats_dev_persist_bytes";
    
    public static final String NETSTATS_DEV_ROTATE_AGE = "netstats_dev_rotate_age";
    
    public static final String NETSTATS_ENABLED = "netstats_enabled";
    
    public static final String NETSTATS_GLOBAL_ALERT_BYTES = "netstats_global_alert_bytes";
    
    public static final String NETSTATS_POLL_INTERVAL = "netstats_poll_interval";
    
    public static final String NETSTATS_SAMPLE_ENABLED = "netstats_sample_enabled";
    
    @Deprecated
    public static final String NETSTATS_TIME_CACHE_MAX_AGE = "netstats_time_cache_max_age";
    
    public static final String NETSTATS_UID_BUCKET_DURATION = "netstats_uid_bucket_duration";
    
    public static final String NETSTATS_UID_DELETE_AGE = "netstats_uid_delete_age";
    
    public static final String NETSTATS_UID_PERSIST_BYTES = "netstats_uid_persist_bytes";
    
    public static final String NETSTATS_UID_ROTATE_AGE = "netstats_uid_rotate_age";
    
    public static final String NETSTATS_UID_TAG_BUCKET_DURATION = "netstats_uid_tag_bucket_duration";
    
    public static final String NETSTATS_UID_TAG_DELETE_AGE = "netstats_uid_tag_delete_age";
    
    public static final String NETSTATS_UID_TAG_PERSIST_BYTES = "netstats_uid_tag_persist_bytes";
    
    public static final String NETSTATS_UID_TAG_ROTATE_AGE = "netstats_uid_tag_rotate_age";
    
    public static final String NETWORK_ACCESS_TIMEOUT_MS = "network_access_timeout_ms";
    
    public static final String NETWORK_AVOID_BAD_WIFI = "network_avoid_bad_wifi";
    
    public static final String NETWORK_DEFAULT_DAILY_MULTIPATH_QUOTA_BYTES = "network_default_daily_multipath_quota_bytes";
    
    public static final String NETWORK_METERED_MULTIPATH_PREFERENCE = "network_metered_multipath_preference";
    
    public static final String NETWORK_PREFERENCE = "network_preference";
    
    public static final String NETWORK_RECOMMENDATIONS_ENABLED = "network_recommendations_enabled";
    
    public static final String NETWORK_RECOMMENDATIONS_PACKAGE = "network_recommendations_package";
    
    public static final String NETWORK_SCORER_APP = "network_scorer_app";
    
    public static final String NETWORK_SCORING_PROVISIONED = "network_scoring_provisioned";
    
    public static final String NETWORK_SCORING_UI_ENABLED = "network_scoring_ui_enabled";
    
    public static final String NETWORK_SWITCH_NOTIFICATION_DAILY_LIMIT = "network_switch_notification_daily_limit";
    
    public static final String NETWORK_SWITCH_NOTIFICATION_RATE_LIMIT_MILLIS = "network_switch_notification_rate_limit_millis";
    
    public static final String NETWORK_WATCHLIST_ENABLED = "network_watchlist_enabled";
    
    public static final String NETWORK_WATCHLIST_LAST_REPORT_TIME = "network_watchlist_last_report_time";
    
    public static final String NEW_CONTACT_AGGREGATOR = "new_contact_aggregator";
    
    public static final String NIGHT_DISPLAY_FORCED_AUTO_MODE_AVAILABLE = "night_display_forced_auto_mode_available";
    
    public static final String NITZ_UPDATE_DIFF = "nitz_update_diff";
    
    public static final String NITZ_UPDATE_SPACING = "nitz_update_spacing";
    
    public static final String NOTIFICATION_BUBBLES = "notification_bubbles";
    
    public static final String NOTIFICATION_SNOOZE_OPTIONS = "notification_snooze_options";
    
    public static final String NR_NSA_TRACKING_SCREEN_OFF_MODE = "nr_nsa_tracking_screen_off_mode";
    
    public static final String NSD_ON = "nsd_on";
    
    public static final String NTP_SERVER = "ntp_server";
    
    public static final String NTP_SERVER_2 = "ntp_server_2";
    
    public static final String NTP_TIMEOUT = "ntp_timeout";
    
    @SystemApi
    public static final String OTA_DISABLE_AUTOMATIC_UPDATE = "ota_disable_automatic_update";
    
    public static final String OVERLAY_DISPLAY_DEVICES = "overlay_display_devices";
    
    public static final String OVERRIDE_SETTINGS_PROVIDER_RESTORE_ANY_VERSION = "override_settings_provider_restore_any_version";
    
    public static final String PACKAGE_VERIFIER_DEFAULT_RESPONSE = "verifier_default_response";
    
    public static final String PACKAGE_VERIFIER_INCLUDE_ADB = "verifier_verify_adb_installs";
    
    public static final String PACKAGE_VERIFIER_SETTING_VISIBLE = "verifier_setting_visible";
    
    public static final String PACKAGE_VERIFIER_TIMEOUT = "verifier_timeout";
    
    public static final String PAC_CHANGE_DELAY = "pac_change_delay";
    
    public static final String PDP_WATCHDOG_ERROR_POLL_COUNT = "pdp_watchdog_error_poll_count";
    
    public static final String PDP_WATCHDOG_ERROR_POLL_INTERVAL_MS = "pdp_watchdog_error_poll_interval_ms";
    
    public static final String PDP_WATCHDOG_LONG_POLL_INTERVAL_MS = "pdp_watchdog_long_poll_interval_ms";
    
    public static final String PDP_WATCHDOG_MAX_PDP_RESET_FAIL_COUNT = "pdp_watchdog_max_pdp_reset_fail_count";
    
    public static final String PDP_WATCHDOG_POLL_INTERVAL_MS = "pdp_watchdog_poll_interval_ms";
    
    public static final String PDP_WATCHDOG_TRIGGER_PACKET_COUNT = "pdp_watchdog_trigger_packet_count";
    
    public static final String POLICY_CONTROL = "policy_control";
    
    public static final String POWER_BUTTON_LONG_PRESS = "power_button_long_press";
    
    public static final String POWER_BUTTON_SUPPRESSION_DELAY_AFTER_GESTURE_WAKE = "power_button_suppression_delay_after_gesture_wake";
    
    public static final String POWER_BUTTON_VERY_LONG_PRESS = "power_button_very_long_press";
    
    public static final String POWER_MANAGER_CONSTANTS = "power_manager_constants";
    
    public static final String POWER_SOUNDS_ENABLED = "power_sounds_enabled";
    
    public static final String PREFERRED_NETWORK_MODE = "preferred_network_mode";
    
    public static final String PRIVATE_DNS_DEFAULT_MODE = "private_dns_default_mode";
    
    public static final String PRIVATE_DNS_MODE = "private_dns_mode";
    
    public static final String PRIVATE_DNS_SPECIFIER = "private_dns_specifier";
    
    public static final String PROVISIONING_APN_ALARM_DELAY_IN_MS = "provisioning_apn_alarm_delay_in_ms";
    
    public static final String RADIO_BLUETOOTH = "bluetooth";
    
    public static final String RADIO_BUG_SYSTEM_ERROR_COUNT_THRESHOLD = "radio_bug_system_error_count_threshold";
    
    public static final String RADIO_BUG_WAKELOCK_TIMEOUT_COUNT_THRESHOLD = "radio_bug_wakelock_timeout_count_threshold";
    
    public static final String RADIO_CELL = "cell";
    
    public static final String RADIO_NFC = "nfc";
    
    public static final String RADIO_WIFI = "wifi";
    
    public static final String RADIO_WIMAX = "wimax";
    
    public static final String READ_EXTERNAL_STORAGE_ENFORCED_DEFAULT = "read_external_storage_enforced_default";
    
    public static final String RECOMMENDED_NETWORK_EVALUATOR_CACHE_EXPIRY_MS = "recommended_network_evaluator_cache_expiry_ms";
    
    @SystemApi
    public static final String REQUIRE_PASSWORD_TO_DECRYPT = "require_password_to_decrypt";
    
    public static final String SAFE_BOOT_DISALLOWED = "safe_boot_disallowed";
    
    public static final String SELINUX_STATUS = "selinux_status";
    
    public static final String SELINUX_UPDATE_CONTENT_URL = "selinux_content_url";
    
    public static final String SELINUX_UPDATE_METADATA_URL = "selinux_metadata_url";
    
    public static final String SEND_ACTION_APP_ERROR = "send_action_app_error";
    
    public static final String SETTINGS_USE_EXTERNAL_PROVIDER_API = "settings_use_external_provider_api";
    
    public static final String SETTINGS_USE_PSD_API = "settings_use_psd_api";
    
    public static final String SETUP_PREPAID_DATA_SERVICE_URL = "setup_prepaid_data_service_url";
    
    public static final String SETUP_PREPAID_DETECTION_REDIR_HOST = "setup_prepaid_detection_redir_host";
    
    public static final String SETUP_PREPAID_DETECTION_TARGET_URL = "setup_prepaid_detection_target_url";
    
    public static final String SET_GLOBAL_HTTP_PROXY = "set_global_http_proxy";
    
    public static final String SET_INSTALL_LOCATION = "set_install_location";
    
    public static final String SHORTCUT_MANAGER_CONSTANTS = "shortcut_manager_constants";
    
    public static final String SHOW_FIRST_CRASH_DIALOG = "show_first_crash_dialog";
    
    public static final String SHOW_HIDDEN_LAUNCHER_ICON_APPS_ENABLED = "show_hidden_icon_apps_enabled";
    
    public static final String SHOW_MEDIA_ON_QUICK_SETTINGS = "qs_media_player";
    
    public static final String SHOW_MUTE_IN_CRASH_DIALOG = "show_mute_in_crash_dialog";
    
    public static final String SHOW_NEW_APP_INSTALLED_NOTIFICATION_ENABLED = "show_new_app_installed_notification_enabled";
    
    public static final String SHOW_NOTIFICATION_CHANNEL_WARNINGS = "show_notification_channel_warnings";
    
    @Deprecated
    public static final String SHOW_PROCESSES = "show_processes";
    
    public static final String SHOW_RESTART_IN_CRASH_DIALOG = "show_restart_in_crash_dialog";
    
    public static final String SHOW_TEMPERATURE_WARNING = "show_temperature_warning";
    
    public static final String SHOW_USB_TEMPERATURE_ALARM = "show_usb_temperature_alarm";
    
    @Deprecated
    public static final String SHOW_ZEN_SETTINGS_SUGGESTION = "show_zen_settings_suggestion";
    
    @Deprecated
    public static final String SHOW_ZEN_UPGRADE_NOTIFICATION = "show_zen_upgrade_notification";
    
    public static final String SIGNED_CONFIG_VERSION = "signed_config_version";
    
    public static final String SMART_REPLIES_IN_NOTIFICATIONS_FLAGS = "smart_replies_in_notifications_flags";
    
    public static final String SMART_SELECTION_UPDATE_CONTENT_URL = "smart_selection_content_url";
    
    public static final String SMART_SELECTION_UPDATE_METADATA_URL = "smart_selection_metadata_url";
    
    public static final String SMART_SUGGESTIONS_IN_NOTIFICATIONS_FLAGS = "smart_suggestions_in_notifications_flags";
    
    public static final String SMS_OUTGOING_CHECK_INTERVAL_MS = "sms_outgoing_check_interval_ms";
    
    public static final String SMS_OUTGOING_CHECK_MAX_COUNT = "sms_outgoing_check_max_count";
    
    public static final String SMS_SHORT_CODES_UPDATE_CONTENT_URL = "sms_short_codes_content_url";
    
    public static final String SMS_SHORT_CODES_UPDATE_METADATA_URL = "sms_short_codes_metadata_url";
    
    public static final String SMS_SHORT_CODE_CONFIRMATION = "sms_short_code_confirmation";
    
    public static final String SMS_SHORT_CODE_RULE = "sms_short_code_rule";
    
    public static final String SOFT_AP_TIMEOUT_ENABLED = "soft_ap_timeout_enabled";
    
    public static final String SOUND_TRIGGER_DETECTION_SERVICE_OP_TIMEOUT = "sound_trigger_detection_service_op_timeout";
    
    public static final String SPEED_LABEL_CACHE_EVICTION_AGE_MILLIS = "speed_label_cache_eviction_age_millis";
    
    public static final String SQLITE_COMPATIBILITY_WAL_FLAGS = "sqlite_compatibility_wal_flags";
    
    public static final String STAY_ON_WHILE_PLUGGED_IN = "stay_on_while_plugged_in";
    
    public static final String STORAGE_BENCHMARK_INTERVAL = "storage_benchmark_interval";
    
    public static final String STORAGE_SETTINGS_CLOBBER_THRESHOLD = "storage_settings_clobber_threshold";
    
    public static final String SYNC_MANAGER_CONSTANTS = "sync_manager_constants";
    
    public static final String SYNC_MAX_RETRY_DELAY_IN_SECONDS = "sync_max_retry_delay_in_seconds";
    
    public static final String SYS_FREE_STORAGE_LOG_INTERVAL = "sys_free_storage_log_interval";
    
    public static final String SYS_STORAGE_CACHE_MAX_BYTES = "sys_storage_cache_max_bytes";
    
    public static final String SYS_STORAGE_CACHE_PERCENTAGE = "sys_storage_cache_percentage";
    
    public static final String SYS_STORAGE_FULL_THRESHOLD_BYTES = "sys_storage_full_threshold_bytes";
    
    public static final String SYS_STORAGE_THRESHOLD_MAX_BYTES = "sys_storage_threshold_max_bytes";
    
    public static final String SYS_STORAGE_THRESHOLD_PERCENTAGE = "sys_storage_threshold_percentage";
    
    public static final String SYS_TRACED = "sys_traced";
    
    public static final String SYS_UIDCPUPOWER = "sys_uidcpupower";
    
    public static final String SYS_VDSO = "sys_vdso";
    
    public static final String TCP_DEFAULT_INIT_RWND = "tcp_default_init_rwnd";
    
    public static final String TETHER_DUN_APN = "tether_dun_apn";
    
    public static final String TETHER_DUN_REQUIRED = "tether_dun_required";
    
    public static final String TETHER_ENABLE_LEGACY_DHCP_SERVER = "tether_enable_legacy_dhcp_server";
    
    @SystemApi
    public static final String TETHER_OFFLOAD_DISABLED = "tether_offload_disabled";
    
    @SystemApi
    public static final String TETHER_SUPPORTED = "tether_supported";
    
    public static final String TEXT_CLASSIFIER_ACTION_MODEL_PARAMS = "text_classifier_action_model_params";
    
    public static final String TEXT_CLASSIFIER_CONSTANTS = "text_classifier_constants";
    
    @SystemApi
    public static final String THEATER_MODE_ON = "theater_mode_on";
    
    public static final String TIME_ONLY_MODE_CONSTANTS = "time_only_mode_constants";
    
    public static final String TIME_REMAINING_ESTIMATE_BASED_ON_USAGE = "time_remaining_estimate_based_on_usage";
    
    public static final String TIME_REMAINING_ESTIMATE_MILLIS = "time_remaining_estimate_millis";
    
    static {
      Uri uri = Uri.parse("content://settings/global");
    }
    
    public static String zenModeToString(int param1Int) {
      if (param1Int == 1)
        return "ZEN_MODE_IMPORTANT_INTERRUPTIONS"; 
      if (param1Int == 3)
        return "ZEN_MODE_ALARMS"; 
      if (param1Int == 2)
        return "ZEN_MODE_NO_INTERRUPTIONS"; 
      return "ZEN_MODE_OFF";
    }
    
    public static boolean isValidZenMode(int param1Int) {
      if (param1Int != 0 && param1Int != 1 && param1Int != 2 && param1Int != 3)
        return false; 
      return true;
    }
    
    public static final String[] TRANSIENT_SETTINGS = new String[] { "location_global_kill_switch" };
    
    public static final String TRANSITION_ANIMATION_SCALE = "transition_animation_scale";
    
    public static final String TRUSTED_SOUND = "trusted_sound";
    
    public static final String TZINFO_UPDATE_CONTENT_URL = "tzinfo_content_url";
    
    public static final String TZINFO_UPDATE_METADATA_URL = "tzinfo_metadata_url";
    
    public static final String UNGAZE_SLEEP_ENABLED = "ungaze_sleep_enabled";
    
    public static final String UNINSTALLED_INSTANT_APP_MAX_CACHE_PERIOD = "uninstalled_instant_app_max_cache_period";
    
    public static final String UNINSTALLED_INSTANT_APP_MIN_CACHE_PERIOD = "uninstalled_instant_app_min_cache_period";
    
    public static final String UNLOCK_SOUND = "unlock_sound";
    
    public static final String UNUSED_STATIC_SHARED_LIB_MIN_CACHE_PERIOD = "unused_static_shared_lib_min_cache_period";
    
    public static final String USB_MASS_STORAGE_ENABLED = "usb_mass_storage_enabled";
    
    public static final String USER_ABSENT_RADIOS_OFF_FOR_SMALL_BATTERY_ENABLED = "user_absent_radios_off_for_small_battery_enabled";
    
    public static final String USER_ABSENT_TOUCH_OFF_FOR_SMALL_BATTERY_ENABLED = "user_absent_touch_off_for_small_battery_enabled";
    
    public static final String USER_SWITCHER_ENABLED = "user_switcher_enabled";
    
    public static final String USE_GOOGLE_MAIL = "use_google_mail";
    
    public static final String USE_OPEN_WIFI_PACKAGE = "use_open_wifi_package";
    
    public static final String VIBRATING_FOR_OUTGOING_CALL_ACCEPTED = "vibrating_for_outgoing_call_accepted";
    
    @Deprecated
    public static final String VT_IMS_ENABLED = "vt_ims_enabled";
    
    public static final String WAIT_FOR_DEBUGGER = "wait_for_debugger";
    
    public static final String WARNING_TEMPERATURE = "warning_temperature";
    
    public static final String WEBVIEW_DATA_REDUCTION_PROXY_KEY = "webview_data_reduction_proxy_key";
    
    public static final String WEBVIEW_FALLBACK_LOGIC_ENABLED = "webview_fallback_logic_enabled";
    
    @SystemApi
    public static final String WEBVIEW_MULTIPROCESS = "webview_multiprocess";
    
    public static final String WEBVIEW_PROVIDER = "webview_provider";
    
    @Deprecated
    public static final String WFC_IMS_ENABLED = "wfc_ims_enabled";
    
    @Deprecated
    public static final String WFC_IMS_MODE = "wfc_ims_mode";
    
    @Deprecated
    public static final String WFC_IMS_ROAMING_ENABLED = "wfc_ims_roaming_enabled";
    
    @Deprecated
    public static final String WFC_IMS_ROAMING_MODE = "wfc_ims_roaming_mode";
    
    public static final String WIFI_ALWAYS_REQUESTED = "wifi_always_requested";
    
    @SystemApi
    public static final String WIFI_BADGING_THRESHOLDS = "wifi_badging_thresholds";
    
    public static final String WIFI_BOUNCE_DELAY_OVERRIDE_MS = "wifi_bounce_delay_override_ms";
    
    @Deprecated
    public static final String WIFI_CONNECTED_MAC_RANDOMIZATION_ENABLED = "wifi_connected_mac_randomization_enabled";
    
    public static final String WIFI_COUNTRY_CODE = "wifi_country_code";
    
    public static final String WIFI_DEVICE_OWNER_CONFIGS_LOCKDOWN = "wifi_device_owner_configs_lockdown";
    
    public static final String WIFI_DISPLAY_CERTIFICATION_ON = "wifi_display_certification_on";
    
    public static final String WIFI_DISPLAY_ON = "wifi_display_on";
    
    public static final String WIFI_DISPLAY_WPS_CONFIG = "wifi_display_wps_config";
    
    public static final String WIFI_ENHANCED_AUTO_JOIN = "wifi_enhanced_auto_join";
    
    public static final String WIFI_EPHEMERAL_OUT_OF_RANGE_TIMEOUT_MS = "wifi_ephemeral_out_of_range_timeout_ms";
    
    public static final String WIFI_FRAMEWORK_SCAN_INTERVAL_MS = "wifi_framework_scan_interval_ms";
    
    public static final String WIFI_FREQUENCY_BAND = "wifi_frequency_band";
    
    public static final String WIFI_IDLE_MS = "wifi_idle_ms";
    
    public static final String WIFI_MAX_DHCP_RETRY_COUNT = "wifi_max_dhcp_retry_count";
    
    public static final String WIFI_MIGRATION_COMPLETED = "wifi_migration_completed";
    
    public static final String WIFI_MOBILE_DATA_TRANSITION_WAKELOCK_TIMEOUT_MS = "wifi_mobile_data_transition_wakelock_timeout_ms";
    
    @Deprecated
    public static final String WIFI_NETWORKS_AVAILABLE_NOTIFICATION_ON = "wifi_networks_available_notification_on";
    
    @Deprecated
    public static final String WIFI_NETWORKS_AVAILABLE_REPEAT_DELAY = "wifi_networks_available_repeat_delay";
    
    public static final String WIFI_NETWORK_SHOW_RSSI = "wifi_network_show_rssi";
    
    @Deprecated
    public static final String WIFI_NUM_OPEN_NETWORKS_KEPT = "wifi_num_open_networks_kept";
    
    public static final String WIFI_ON = "wifi_on";
    
    public static final String WIFI_ON_WHEN_PROXY_DISCONNECTED = "wifi_on_when_proxy_disconnected";
    
    public static final String WIFI_P2P_DEVICE_NAME = "wifi_p2p_device_name";
    
    public static final String WIFI_P2P_PENDING_FACTORY_RESET = "wifi_p2p_pending_factory_reset";
    
    public static final String WIFI_SCAN_ALWAYS_AVAILABLE = "wifi_scan_always_enabled";
    
    public static final String WIFI_SCAN_INTERVAL_WHEN_P2P_CONNECTED_MS = "wifi_scan_interval_p2p_connected_ms";
    
    public static final String WIFI_SCAN_THROTTLE_ENABLED = "wifi_scan_throttle_enabled";
    
    public static final String WIFI_SCORE_PARAMS = "wifi_score_params";
    
    @Deprecated
    public static final String WIFI_SLEEP_POLICY = "wifi_sleep_policy";
    
    @Deprecated
    public static final int WIFI_SLEEP_POLICY_DEFAULT = 0;
    
    @Deprecated
    public static final int WIFI_SLEEP_POLICY_NEVER = 2;
    
    @Deprecated
    public static final int WIFI_SLEEP_POLICY_NEVER_WHILE_PLUGGED = 1;
    
    public static final String WIFI_SUPPLICANT_SCAN_INTERVAL_MS = "wifi_supplicant_scan_interval_ms";
    
    public static final String WIFI_VERBOSE_LOGGING_ENABLED = "wifi_verbose_logging_enabled";
    
    @SystemApi
    @Deprecated
    public static final String WIFI_WAKEUP_ENABLED = "wifi_wakeup_enabled";
    
    public static final String WIFI_WATCHDOG_ON = "wifi_watchdog_on";
    
    public static final String WIFI_WATCHDOG_POOR_NETWORK_TEST_ENABLED = "wifi_watchdog_poor_network_test_enabled";
    
    public static final String WIMAX_NETWORKS_AVAILABLE_NOTIFICATION_ON = "wimax_networks_available_notification_on";
    
    public static final String WINDOW_ANIMATION_SCALE = "window_animation_scale";
    
    public static final String WIRELESS_CHARGING_STARTED_SOUND = "wireless_charging_started_sound";
    
    public static final String WTF_IS_FATAL = "wtf_is_fatal";
    
    @Deprecated
    public static final String ZEN_DURATION = "zen_duration";
    
    @Deprecated
    public static final int ZEN_DURATION_FOREVER = 0;
    
    @Deprecated
    public static final int ZEN_DURATION_PROMPT = -1;
    
    public static final String ZEN_MODE = "zen_mode";
    
    public static final int ZEN_MODE_ALARMS = 3;
    
    public static final String ZEN_MODE_CONFIG_ETAG = "zen_mode_config_etag";
    
    public static final int ZEN_MODE_IMPORTANT_INTERRUPTIONS = 1;
    
    public static final int ZEN_MODE_NO_INTERRUPTIONS = 2;
    
    public static final int ZEN_MODE_OFF = 0;
    
    public static final String ZEN_MODE_RINGER_LEVEL = "zen_mode_ringer_level";
    
    @Deprecated
    public static final String ZEN_SETTINGS_SUGGESTION_VIEWED = "zen_settings_suggestion_viewed";
    
    @Deprecated
    public static final String ZEN_SETTINGS_UPDATED = "zen_settings_updated";
    
    public static final String ZRAM_ENABLED = "zram_enabled";
    
    private static final Settings.NameValueCache sNameValueCache;
    
    private static final Settings.ContentProviderHolder sProviderHolder;
    
    static {
      LEGACY_RESTORE_SETTINGS = new String[0];
      sProviderHolder = new Settings.ContentProviderHolder(uri);
      sNameValueCache = new Settings.NameValueCache(CONTENT_URI, "GET_global", "PUT_global", sProviderHolder);
      HashSet<String> hashSet = new HashSet(8);
      hashSet.add("install_non_market_apps");
      MOVED_TO_SECURE.add("zen_duration");
      MOVED_TO_SECURE.add("show_zen_upgrade_notification");
      MOVED_TO_SECURE.add("show_zen_settings_suggestion");
      MOVED_TO_SECURE.add("zen_settings_updated");
      MOVED_TO_SECURE.add("zen_settings_suggestion_viewed");
      MOVED_TO_SECURE.add("charging_sounds_enabled");
      MOVED_TO_SECURE.add("charging_vibration_enabled");
      MULTI_SIM_USER_PREFERRED_SUBS = new String[] { "user_preferred_sub1", "user_preferred_sub2", "user_preferred_sub3" };
      ArraySet<String> arraySet = new ArraySet();
      arraySet.add("wait_for_debugger");
      INSTANT_APP_SETTINGS.add("device_provisioned");
      INSTANT_APP_SETTINGS.add("force_resizable_activities");
      INSTANT_APP_SETTINGS.add("debug.force_rtl");
      INSTANT_APP_SETTINGS.add("ephemeral_cookie_max_size_bytes");
      INSTANT_APP_SETTINGS.add("airplane_mode_on");
      INSTANT_APP_SETTINGS.add("window_animation_scale");
      INSTANT_APP_SETTINGS.add("transition_animation_scale");
      INSTANT_APP_SETTINGS.add("animator_duration_scale");
      INSTANT_APP_SETTINGS.add("debug_view_attributes");
      INSTANT_APP_SETTINGS.add("debug_view_attributes_application_package");
      INSTANT_APP_SETTINGS.add("wtf_is_fatal");
      INSTANT_APP_SETTINGS.add("send_action_app_error");
      INSTANT_APP_SETTINGS.add("zen_mode");
    }
    
    public static void getMovedToSecureSettings(Set<String> param1Set) {
      param1Set.addAll(MOVED_TO_SECURE);
    }
    
    public static void clearProviderForTest() {
      sProviderHolder.clearProviderForTest();
      sNameValueCache.clearGenerationTrackerForTest();
    }
    
    public static String getString(ContentResolver param1ContentResolver, String param1String) {
      return getStringForUser(param1ContentResolver, param1String, param1ContentResolver.getUserId());
    }
    
    public static String getStringForUser(ContentResolver param1ContentResolver, String param1String, int param1Int) {
      if (MOVED_TO_SECURE.contains(param1String)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Setting ");
        stringBuilder.append(param1String);
        stringBuilder.append(" has moved from android.provider.Settings.Global to android.provider.Settings.Secure, returning read-only value.");
        Log.w("Settings", stringBuilder.toString());
        return Settings.Secure.getStringForUser(param1ContentResolver, param1String, param1Int);
      } 
      return sNameValueCache.getStringForUser(param1ContentResolver, param1String, param1Int);
    }
    
    public static boolean putString(ContentResolver param1ContentResolver, String param1String1, String param1String2) {
      return putStringForUser(param1ContentResolver, param1String1, param1String2, (String)null, false, param1ContentResolver.getUserId(), false);
    }
    
    public static boolean putString(ContentResolver param1ContentResolver, String param1String1, String param1String2, String param1String3, boolean param1Boolean1, boolean param1Boolean2) {
      int i = param1ContentResolver.getUserId();
      return putStringForUser(param1ContentResolver, param1String1, param1String2, param1String3, param1Boolean1, i, param1Boolean2);
    }
    
    @SystemApi
    public static boolean putString(ContentResolver param1ContentResolver, String param1String1, String param1String2, String param1String3, boolean param1Boolean) {
      int i = param1ContentResolver.getUserId();
      return putStringForUser(param1ContentResolver, param1String1, param1String2, param1String3, param1Boolean, i, false);
    }
    
    @SystemApi
    public static void resetToDefaults(ContentResolver param1ContentResolver, String param1String) {
      int i = param1ContentResolver.getUserId();
      resetToDefaultsAsUser(param1ContentResolver, param1String, 1, i);
    }
    
    public static void resetToDefaultsAsUser(ContentResolver param1ContentResolver, String param1String, int param1Int1, int param1Int2) {
      try {
        Bundle bundle = new Bundle();
        this();
        bundle.putInt("_user", param1Int2);
        if (param1String != null)
          bundle.putString("_tag", param1String); 
        bundle.putInt("_reset_mode", param1Int1);
        IContentProvider iContentProvider = sProviderHolder.getProvider(param1ContentResolver);
        String str2 = param1ContentResolver.getPackageName(), str1 = param1ContentResolver.getAttributionTag();
        Settings.ContentProviderHolder contentProviderHolder = sProviderHolder;
        String str3 = contentProviderHolder.mUri.getAuthority();
        iContentProvider.call(str2, str1, str3, "RESET_global", null, bundle);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Can't reset do defaults for ");
        stringBuilder.append(CONTENT_URI);
        Log.w("Settings", stringBuilder.toString(), (Throwable)remoteException);
      } 
    }
    
    public static boolean putStringForUser(ContentResolver param1ContentResolver, String param1String1, String param1String2, int param1Int) {
      return putStringForUser(param1ContentResolver, param1String1, param1String2, (String)null, false, param1Int, false);
    }
    
    public static boolean putStringForUser(ContentResolver param1ContentResolver, String param1String1, String param1String2, String param1String3, boolean param1Boolean1, int param1Int, boolean param1Boolean2) {
      if (MOVED_TO_SECURE.contains(param1String1)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Setting ");
        stringBuilder.append(param1String1);
        stringBuilder.append(" has moved from android.provider.Settings.Global to android.provider.Settings.Secure, value is unchanged.");
        Log.w("Settings", stringBuilder.toString());
        return Settings.Secure.putStringForUser(param1ContentResolver, param1String1, param1String2, param1String3, param1Boolean1, param1Int, param1Boolean2);
      } 
      return sNameValueCache.putStringForUser(param1ContentResolver, param1String1, param1String2, param1String3, param1Boolean1, param1Int, param1Boolean2);
    }
    
    public static Uri getUriFor(String param1String) {
      return getUriFor(CONTENT_URI, param1String);
    }
    
    public static int getInt(ContentResolver param1ContentResolver, String param1String, int param1Int) {
      String str = getString(param1ContentResolver, param1String);
      if (str != null)
        try {
          int i = Integer.parseInt(str);
        } catch (NumberFormatException numberFormatException) {
          return param1Int;
        }  
      return param1Int;
    }
    
    public static int getInt(ContentResolver param1ContentResolver, String param1String) throws Settings.SettingNotFoundException {
      String str = getString(param1ContentResolver, param1String);
      try {
        return Integer.parseInt(str);
      } catch (NumberFormatException numberFormatException) {
        throw new Settings.SettingNotFoundException(param1String);
      } 
    }
    
    public static boolean putInt(ContentResolver param1ContentResolver, String param1String, int param1Int) {
      return putString(param1ContentResolver, param1String, Integer.toString(param1Int));
    }
    
    public static long getLong(ContentResolver param1ContentResolver, String param1String, long param1Long) {
      String str = getString(param1ContentResolver, param1String);
      if (str != null)
        try {
          long l = Long.parseLong(str);
        } catch (NumberFormatException numberFormatException) {} 
      return param1Long;
    }
    
    public static long getLong(ContentResolver param1ContentResolver, String param1String) throws Settings.SettingNotFoundException {
      String str = getString(param1ContentResolver, param1String);
      try {
        return Long.parseLong(str);
      } catch (NumberFormatException numberFormatException) {
        throw new Settings.SettingNotFoundException(param1String);
      } 
    }
    
    public static boolean putLong(ContentResolver param1ContentResolver, String param1String, long param1Long) {
      return putString(param1ContentResolver, param1String, Long.toString(param1Long));
    }
    
    public static float getFloat(ContentResolver param1ContentResolver, String param1String, float param1Float) {
      String str = getString(param1ContentResolver, param1String);
      if (str != null)
        try {
          float f = Float.parseFloat(str);
        } catch (NumberFormatException numberFormatException) {
          return param1Float;
        }  
      return param1Float;
    }
    
    public static float getFloat(ContentResolver param1ContentResolver, String param1String) throws Settings.SettingNotFoundException {
      String str = getString(param1ContentResolver, param1String);
      if (str != null)
        try {
          return Float.parseFloat(str);
        } catch (NumberFormatException numberFormatException) {
          throw new Settings.SettingNotFoundException(param1String);
        }  
      throw new Settings.SettingNotFoundException(param1String);
    }
    
    public static boolean putFloat(ContentResolver param1ContentResolver, String param1String, float param1Float) {
      return putString(param1ContentResolver, param1String, Float.toString(param1Float));
    }
  }
  
  class Config extends NameValueTable {
    private static final Settings.NameValueCache sNameValueCache = new Settings.NameValueCache(DeviceConfig.CONTENT_URI, "GET_config", "PUT_config", "LIST_config", "SET_ALL_config", sProviderHolder);
    
    private static final Settings.ContentProviderHolder sProviderHolder = new Settings.ContentProviderHolder(DeviceConfig.CONTENT_URI);
    
    static {
    
    }
    
    static String getString(ContentResolver param1ContentResolver, String param1String) {
      return sNameValueCache.getStringForUser(param1ContentResolver, param1String, param1ContentResolver.getUserId());
    }
    
    public static Map<String, String> getStrings(ContentResolver param1ContentResolver, String param1String, List<String> param1List) {
      ArrayList<String> arrayList = new ArrayList(param1List.size());
      for (String str : param1List)
        arrayList.add(createCompositeName(param1String, str)); 
      param1String = createPrefix(param1String);
      ArrayMap<String, String> arrayMap = sNameValueCache.getStringsForPrefix(param1ContentResolver, param1String, arrayList);
      int i = arrayMap.size();
      int j = param1String.length();
      ArrayMap arrayMap1 = new ArrayMap(i);
      for (byte b = 0; b < i; b++) {
        String str = ((String)arrayMap.keyAt(b)).substring(j);
        param1String = (String)arrayMap.valueAt(b);
        arrayMap1.put(str, param1String);
      } 
      return (Map<String, String>)arrayMap1;
    }
    
    static boolean putString(ContentResolver param1ContentResolver, String param1String1, String param1String2, String param1String3, boolean param1Boolean) {
      Settings.NameValueCache nameValueCache = sNameValueCache;
      param1String1 = createCompositeName(param1String1, param1String2);
      int i = param1ContentResolver.getUserId();
      return nameValueCache.putStringForUser(param1ContentResolver, param1String1, param1String3, null, param1Boolean, i, false);
    }
    
    public static boolean setStrings(ContentResolver param1ContentResolver, String param1String, Map<String, String> param1Map) throws DeviceConfig.BadConfigException {
      HashMap<Object, Object> hashMap = new HashMap<>(param1Map.keySet().size());
      for (Map.Entry<String, String> entry : param1Map.entrySet()) {
        String str2 = createCompositeName(param1String, (String)entry.getKey()), str1 = (String)entry.getValue();
        hashMap.put(str2, str1);
      } 
      if (sNameValueCache.setStringsForPrefix(param1ContentResolver, createPrefix(param1String), (HashMap)hashMap))
        return true; 
      throw new DeviceConfig.BadConfigException();
    }
    
    static void resetToDefaults(ContentResolver param1ContentResolver, int param1Int, String param1String) {
      try {
        Bundle bundle = new Bundle();
        this();
        bundle.putInt("_user", param1ContentResolver.getUserId());
        bundle.putInt("_reset_mode", param1Int);
        if (param1String != null)
          bundle.putString("_prefix", createPrefix(param1String)); 
        IContentProvider iContentProvider = sProviderHolder.getProvider(param1ContentResolver);
        String str2 = param1ContentResolver.getPackageName(), str1 = param1ContentResolver.getAttributionTag();
        Settings.ContentProviderHolder contentProviderHolder = sProviderHolder;
        String str3 = contentProviderHolder.mUri.getAuthority();
        iContentProvider.call(str2, str1, str3, "RESET_config", null, bundle);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Can't reset to defaults for ");
        stringBuilder.append(DeviceConfig.CONTENT_URI);
        Log.w("Settings", stringBuilder.toString(), (Throwable)remoteException);
      } 
    }
    
    public static void registerMonitorCallback(ContentResolver param1ContentResolver, RemoteCallback param1RemoteCallback) {
      registerMonitorCallbackAsUser(param1ContentResolver, param1ContentResolver.getUserId(), param1RemoteCallback);
    }
    
    private static void registerMonitorCallbackAsUser(ContentResolver param1ContentResolver, int param1Int, RemoteCallback param1RemoteCallback) {
      try {
        Bundle bundle = new Bundle();
        this();
        bundle.putInt("_user", param1Int);
        bundle.putParcelable("_monitor_callback_key", param1RemoteCallback);
        IContentProvider iContentProvider = sProviderHolder.getProvider(param1ContentResolver);
        String str2 = param1ContentResolver.getPackageName(), str1 = param1ContentResolver.getAttributionTag();
        Settings.ContentProviderHolder contentProviderHolder = sProviderHolder;
        String str3 = contentProviderHolder.mUri.getAuthority();
        iContentProvider.call(str2, str1, str3, "REGISTER_MONITOR_CALLBACK_config", null, bundle);
      } catch (RemoteException remoteException) {
        Log.w("Settings", "Can't register config monitor callback", (Throwable)remoteException);
      } 
    }
    
    public static void clearProviderForTest() {
      sProviderHolder.clearProviderForTest();
      sNameValueCache.clearGenerationTrackerForTest();
    }
    
    private static String createCompositeName(String param1String1, String param1String2) {
      Preconditions.checkNotNull(param1String1);
      Preconditions.checkNotNull(param1String2);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(createPrefix(param1String1));
      stringBuilder.append(param1String2);
      return stringBuilder.toString();
    }
    
    private static String createPrefix(String param1String) {
      Preconditions.checkNotNull(param1String);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1String);
      stringBuilder.append("/");
      return stringBuilder.toString();
    }
  }
  
  class Bookmarks implements BaseColumns {
    public static final Uri CONTENT_URI = Uri.parse("content://settings/bookmarks");
    
    public static final String FOLDER = "folder";
    
    public static final String ID = "_id";
    
    public static final String INTENT = "intent";
    
    public static final String ORDERING = "ordering";
    
    public static final String SHORTCUT = "shortcut";
    
    private static final String TAG = "Bookmarks";
    
    public static final String TITLE = "title";
    
    private static final String[] sIntentProjection = new String[] { "intent" };
    
    private static final String[] sShortcutProjection = new String[] { "_id", "shortcut" };
    
    private static final String sShortcutSelection = "shortcut=?";
    
    public static Intent getIntentForShortcut(ContentResolver param1ContentResolver, char param1Char) {
      ContentResolver contentResolver = null;
      Uri uri = CONTENT_URI;
      String[] arrayOfString = sIntentProjection;
      Cursor cursor = param1ContentResolver.query(uri, arrayOfString, "shortcut=?", new String[] { String.valueOf(param1Char) }, "ordering");
      param1ContentResolver = contentResolver;
      while (param1ContentResolver == null) {
        try {
          boolean bool = cursor.moveToNext();
        } finally {
          if (cursor != null)
            cursor.close(); 
        } 
      } 
      if (cursor != null)
        cursor.close(); 
      return (Intent)param1ContentResolver;
    }
    
    public static Uri add(ContentResolver param1ContentResolver, Intent param1Intent, String param1String1, String param1String2, char param1Char, int param1Int) {
      if (param1Char != '\000') {
        Uri uri = CONTENT_URI;
        param1ContentResolver.delete(uri, "shortcut=?", new String[] { String.valueOf(param1Char) });
      } 
      ContentValues contentValues = new ContentValues();
      if (param1String1 != null)
        contentValues.put("title", param1String1); 
      if (param1String2 != null)
        contentValues.put("folder", param1String2); 
      contentValues.put("intent", param1Intent.toUri(0));
      if (param1Char != '\000')
        contentValues.put("shortcut", Integer.valueOf(param1Char)); 
      contentValues.put("ordering", Integer.valueOf(param1Int));
      return param1ContentResolver.insert(CONTENT_URI, contentValues);
    }
    
    public static CharSequence getLabelForFolder(Resources param1Resources, String param1String) {
      return param1String;
    }
    
    public static CharSequence getTitle(Context param1Context, Cursor param1Cursor) {
      int i = param1Cursor.getColumnIndex("title");
      int j = param1Cursor.getColumnIndex("intent");
      if (i != -1 && j != -1) {
        String str2 = param1Cursor.getString(i);
        if (!TextUtils.isEmpty(str2))
          return str2; 
        str2 = param1Cursor.getString(j);
        boolean bool = TextUtils.isEmpty(str2);
        String str1 = "";
        if (bool)
          return ""; 
        try {
          Intent intent = Intent.parseUri(str2, 0);
          PackageManager packageManager = param1Context.getPackageManager();
          ResolveInfo resolveInfo = packageManager.resolveActivity(intent, 0);
          CharSequence charSequence = str1;
          if (resolveInfo != null)
            charSequence = resolveInfo.loadLabel(packageManager); 
          return charSequence;
        } catch (URISyntaxException uRISyntaxException) {
          return "";
        } 
      } 
      throw new IllegalArgumentException("The cursor must contain the TITLE and INTENT columns.");
    }
  }
  
  public static final class Panel {
    public static final String ACTION_INTERNET_CONNECTIVITY = "android.settings.panel.action.INTERNET_CONNECTIVITY";
    
    public static final String ACTION_NFC = "android.settings.panel.action.NFC";
    
    public static final String ACTION_VOLUME = "android.settings.panel.action.VOLUME";
    
    public static final String ACTION_WIFI = "android.settings.panel.action.WIFI";
  }
  
  static {
    PM_WRITE_SETTINGS = new String[] { "android.permission.WRITE_SETTINGS" };
    PM_CHANGE_NETWORK_STATE = new String[] { "android.permission.CHANGE_NETWORK_STATE", "android.permission.WRITE_SETTINGS" };
    PM_SYSTEM_ALERT_WINDOW = new String[] { "android.permission.SYSTEM_ALERT_WINDOW" };
  }
  
  public static boolean isCallingPackageAllowedToWriteSettings(Context paramContext, int paramInt, String paramString, boolean paramBoolean) {
    return isCallingPackageAllowedToPerformAppOpsProtectedOperation(paramContext, paramInt, paramString, paramBoolean, 23, PM_WRITE_SETTINGS, false);
  }
  
  @SystemApi
  public static boolean checkAndNoteWriteSettingsOperation(Context paramContext, int paramInt, String paramString, boolean paramBoolean) {
    return isCallingPackageAllowedToPerformAppOpsProtectedOperation(paramContext, paramInt, paramString, paramBoolean, 23, PM_WRITE_SETTINGS, true);
  }
  
  public static boolean checkAndNoteChangeNetworkStateOperation(Context paramContext, int paramInt, String paramString, boolean paramBoolean) {
    if (paramContext.checkCallingOrSelfPermission("android.permission.CHANGE_NETWORK_STATE") == 0)
      return true; 
    return isCallingPackageAllowedToPerformAppOpsProtectedOperation(paramContext, paramInt, paramString, paramBoolean, 23, PM_CHANGE_NETWORK_STATE, true);
  }
  
  public static boolean isCallingPackageAllowedToDrawOverlays(Context paramContext, int paramInt, String paramString, boolean paramBoolean) {
    return isCallingPackageAllowedToPerformAppOpsProtectedOperation(paramContext, paramInt, paramString, paramBoolean, 24, PM_SYSTEM_ALERT_WINDOW, false);
  }
  
  public static boolean checkAndNoteDrawOverlaysOperation(Context paramContext, int paramInt, String paramString, boolean paramBoolean) {
    return isCallingPackageAllowedToPerformAppOpsProtectedOperation(paramContext, paramInt, paramString, paramBoolean, 24, PM_SYSTEM_ALERT_WINDOW, true);
  }
  
  public static boolean isCallingPackageAllowedToPerformAppOpsProtectedOperation(Context paramContext, int paramInt1, String paramString, boolean paramBoolean1, int paramInt2, String[] paramArrayOfString, boolean paramBoolean2) {
    if (paramString == null)
      return false; 
    AppOpsManager appOpsManager = (AppOpsManager)paramContext.getSystemService("appops");
    if (paramBoolean2) {
      paramInt1 = appOpsManager.noteOpNoThrow(paramInt2, paramInt1, paramString);
    } else {
      paramInt1 = appOpsManager.checkOpNoThrow(paramInt2, paramInt1, paramString);
    } 
    if (paramInt1 != 0) {
      if (paramInt1 == 3)
        for (paramInt2 = paramArrayOfString.length, paramInt1 = 0; paramInt1 < paramInt2; ) {
          String str = paramArrayOfString[paramInt1];
          if (paramContext.checkCallingOrSelfPermission(str) == 0)
            return true; 
          paramInt1++;
        }  
      if (!paramBoolean1)
        return false; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append(" was not granted ");
      if (paramArrayOfString.length > 1) {
        stringBuilder.append(" either of these permissions: ");
      } else {
        stringBuilder.append(" this permission: ");
      } 
      for (paramInt1 = 0; paramInt1 < paramArrayOfString.length; paramInt1++) {
        String str;
        stringBuilder.append(paramArrayOfString[paramInt1]);
        if (paramInt1 == paramArrayOfString.length - 1) {
          str = ".";
        } else {
          str = ", ";
        } 
        stringBuilder.append(str);
      } 
      throw new SecurityException(stringBuilder.toString());
    } 
    return true;
  }
  
  public static String getPackageNameForUid(Context paramContext, int paramInt) {
    String[] arrayOfString = paramContext.getPackageManager().getPackagesForUid(paramInt);
    if (arrayOfString == null)
      return null; 
    return arrayOfString[0];
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface AddWifiResult {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface EnableMmsDataReason {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ResetMode {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface UserSetupPersonalization {}
}
