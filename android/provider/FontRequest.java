package android.provider;

import android.util.Base64;
import com.android.internal.util.Preconditions;
import java.util.Collections;
import java.util.List;

public final class FontRequest {
  private final List<List<byte[]>> mCertificates;
  
  private final String mIdentifier;
  
  private final String mProviderAuthority;
  
  private final String mProviderPackage;
  
  private final String mQuery;
  
  public FontRequest(String paramString1, String paramString2, String paramString3) {
    this.mProviderAuthority = (String)Preconditions.checkNotNull(paramString1);
    this.mQuery = (String)Preconditions.checkNotNull(paramString3);
    this.mProviderPackage = (String)Preconditions.checkNotNull(paramString2);
    this.mCertificates = Collections.emptyList();
    StringBuilder stringBuilder = new StringBuilder(this.mProviderAuthority);
    stringBuilder.append("-");
    stringBuilder.append(this.mProviderPackage);
    stringBuilder.append("-");
    stringBuilder.append(this.mQuery);
    this.mIdentifier = stringBuilder.toString();
  }
  
  public FontRequest(String paramString1, String paramString2, String paramString3, List<List<byte[]>> paramList) {
    this.mProviderAuthority = (String)Preconditions.checkNotNull(paramString1);
    this.mProviderPackage = (String)Preconditions.checkNotNull(paramString2);
    this.mQuery = (String)Preconditions.checkNotNull(paramString3);
    this.mCertificates = (List<List<byte[]>>)Preconditions.checkNotNull(paramList);
    StringBuilder stringBuilder = new StringBuilder(this.mProviderAuthority);
    stringBuilder.append("-");
    stringBuilder.append(this.mProviderPackage);
    stringBuilder.append("-");
    stringBuilder.append(this.mQuery);
    this.mIdentifier = stringBuilder.toString();
  }
  
  public String getProviderAuthority() {
    return this.mProviderAuthority;
  }
  
  public String getProviderPackage() {
    return this.mProviderPackage;
  }
  
  public String getQuery() {
    return this.mQuery;
  }
  
  public List<List<byte[]>> getCertificates() {
    return this.mCertificates;
  }
  
  public String getIdentifier() {
    return this.mIdentifier;
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("FontRequest {mProviderAuthority: ");
    stringBuilder2.append(this.mProviderAuthority);
    stringBuilder2.append(", mProviderPackage: ");
    stringBuilder2.append(this.mProviderPackage);
    stringBuilder2.append(", mQuery: ");
    stringBuilder2.append(this.mQuery);
    stringBuilder2.append(", mCertificates:");
    stringBuilder1.append(stringBuilder2.toString());
    for (byte b = 0; b < this.mCertificates.size(); b++) {
      stringBuilder1.append(" [");
      List<byte[]> list = this.mCertificates.get(b);
      for (byte b1 = 0; b1 < list.size(); b1++) {
        stringBuilder1.append(" \"");
        byte[] arrayOfByte = list.get(b1);
        stringBuilder1.append(Base64.encodeToString(arrayOfByte, 0));
        stringBuilder1.append("\"");
      } 
      stringBuilder1.append(" ]");
    } 
    stringBuilder1.append("}");
    return stringBuilder1.toString();
  }
}
