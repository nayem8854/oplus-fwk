package android.provider;

import android.content.ContentResolver;
import com.oplus.reflect.MethodParams;
import com.oplus.reflect.RefClass;
import com.oplus.reflect.RefStaticMethod;

public class OplusMirrorSettings {
  public static Class<?> TYPE = RefClass.load(OplusMirrorSettings.class, Settings.System.class);
  
  @MethodParams({ContentResolver.class, String.class, int.class, int.class})
  public static RefStaticMethod<Integer> getIntForBrightness;
}
