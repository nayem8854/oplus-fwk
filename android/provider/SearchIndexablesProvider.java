package android.provider;

import android.annotation.SystemApi;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

@SystemApi
public abstract class SearchIndexablesProvider extends ContentProvider {
  private static final int MATCH_DYNAMIC_RAW_CODE = 6;
  
  private static final int MATCH_NON_INDEXABLE_KEYS_CODE = 3;
  
  private static final int MATCH_RAW_CODE = 2;
  
  private static final int MATCH_RES_CODE = 1;
  
  private static final int MATCH_SITE_MAP_PAIRS_CODE = 4;
  
  private static final int MATCH_SLICE_URI_PAIRS_CODE = 5;
  
  private static final String TAG = "IndexablesProvider";
  
  private String mAuthority;
  
  private UriMatcher mMatcher;
  
  public void attachInfo(Context paramContext, ProviderInfo paramProviderInfo) {
    this.mAuthority = paramProviderInfo.authority;
    UriMatcher uriMatcher = new UriMatcher(-1);
    uriMatcher.addURI(this.mAuthority, "settings/indexables_xml_res", 1);
    this.mMatcher.addURI(this.mAuthority, "settings/indexables_raw", 2);
    this.mMatcher.addURI(this.mAuthority, "settings/non_indexables_key", 3);
    this.mMatcher.addURI(this.mAuthority, "settings/site_map_pairs", 4);
    this.mMatcher.addURI(this.mAuthority, "settings/slice_uri_pairs", 5);
    this.mMatcher.addURI(this.mAuthority, "settings/dynamic_indexables_raw", 6);
    if (paramProviderInfo.exported) {
      if (paramProviderInfo.grantUriPermissions) {
        if ("android.permission.READ_SEARCH_INDEXABLES".equals(paramProviderInfo.readPermission)) {
          super.attachInfo(paramContext, paramProviderInfo);
          return;
        } 
        throw new SecurityException("Provider must be protected by READ_SEARCH_INDEXABLES");
      } 
      throw new SecurityException("Provider must grantUriPermissions");
    } 
    throw new SecurityException("Provider must be exported");
  }
  
  public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2) {
    try {
      UnsupportedOperationException unsupportedOperationException;
      StringBuilder stringBuilder;
      switch (this.mMatcher.match(paramUri)) {
        default:
          unsupportedOperationException = new UnsupportedOperationException();
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Unknown Uri ");
          stringBuilder.append(paramUri);
          this(stringBuilder.toString());
          throw unsupportedOperationException;
        case 6:
          return queryDynamicRawData(null);
        case 5:
          return querySliceUriPairs();
        case 4:
          return querySiteMapPairs();
        case 3:
          return queryNonIndexableKeys(null);
        case 2:
          return queryRawData(null);
        case 1:
          break;
      } 
      return queryXmlResources(null);
    } catch (UnsupportedOperationException unsupportedOperationException) {
      throw unsupportedOperationException;
    } catch (Exception exception) {
      Log.e("IndexablesProvider", "Provider querying exception:", exception);
      return null;
    } 
  }
  
  public Cursor querySiteMapPairs() {
    return null;
  }
  
  public Cursor querySliceUriPairs() {
    return null;
  }
  
  public Cursor queryDynamicRawData(String[] paramArrayOfString) {
    return null;
  }
  
  public String getType(Uri paramUri) {
    int i = this.mMatcher.match(paramUri);
    if (i != 1) {
      if (i != 2)
        if (i != 3) {
          if (i != 6) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unknown URI ");
            stringBuilder.append(paramUri);
            throw new IllegalArgumentException(stringBuilder.toString());
          } 
        } else {
          return "vnd.android.cursor.dir/non_indexables_key";
        }  
      return "vnd.android.cursor.dir/indexables_raw";
    } 
    return "vnd.android.cursor.dir/indexables_xml_res";
  }
  
  public final Uri insert(Uri paramUri, ContentValues paramContentValues) {
    throw new UnsupportedOperationException("Insert not supported");
  }
  
  public final int delete(Uri paramUri, String paramString, String[] paramArrayOfString) {
    throw new UnsupportedOperationException("Delete not supported");
  }
  
  public final int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString) {
    throw new UnsupportedOperationException("Update not supported");
  }
  
  public abstract Cursor queryNonIndexableKeys(String[] paramArrayOfString);
  
  public abstract Cursor queryRawData(String[] paramArrayOfString);
  
  public abstract Cursor queryXmlResources(String[] paramArrayOfString);
}
