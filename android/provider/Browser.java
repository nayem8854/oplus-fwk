package android.provider;

import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.MatrixCursor;
import android.net.Uri;
import android.util.SeempLog;
import android.webkit.WebIconDatabase;

public class Browser {
  public static final Uri BOOKMARKS_URI = Uri.parse("content://browser/bookmarks");
  
  public static final String EXTRA_APPLICATION_ID = "com.android.browser.application_id";
  
  public static final String EXTRA_CREATE_NEW_TAB = "create_new_tab";
  
  public static final String EXTRA_HEADERS = "com.android.browser.headers";
  
  public static final String EXTRA_SHARE_FAVICON = "share_favicon";
  
  public static final String EXTRA_SHARE_SCREENSHOT = "share_screenshot";
  
  public static final String[] HISTORY_PROJECTION = new String[] { "_id", "url", "visits", "date", "bookmark", "title", "favicon", "thumbnail", "touch_icon", "user_entered" };
  
  public static final int HISTORY_PROJECTION_BOOKMARK_INDEX = 4;
  
  public static final int HISTORY_PROJECTION_DATE_INDEX = 3;
  
  public static final int HISTORY_PROJECTION_FAVICON_INDEX = 6;
  
  public static final int HISTORY_PROJECTION_ID_INDEX = 0;
  
  public static final int HISTORY_PROJECTION_THUMBNAIL_INDEX = 7;
  
  public static final int HISTORY_PROJECTION_TITLE_INDEX = 5;
  
  public static final int HISTORY_PROJECTION_TOUCH_ICON_INDEX = 8;
  
  public static final int HISTORY_PROJECTION_URL_INDEX = 1;
  
  public static final int HISTORY_PROJECTION_VISITS_INDEX = 2;
  
  public static final String INITIAL_ZOOM_LEVEL = "browser.initialZoomLevel";
  
  private static final String LOGTAG = "browser";
  
  private static final int MAX_HISTORY_COUNT = 250;
  
  public static final String[] SEARCHES_PROJECTION;
  
  public static final int SEARCHES_PROJECTION_DATE_INDEX = 2;
  
  public static final int SEARCHES_PROJECTION_SEARCH_INDEX = 1;
  
  public static final Uri SEARCHES_URI;
  
  public static final String[] TRUNCATE_HISTORY_PROJECTION = new String[] { "_id", "date" };
  
  public static final int TRUNCATE_HISTORY_PROJECTION_ID_INDEX = 0;
  
  public static final int TRUNCATE_N_OLDEST = 5;
  
  static {
    SEARCHES_URI = Uri.parse("content://browser/searches");
    SEARCHES_PROJECTION = new String[] { "_id", "search", "date" };
  }
  
  public static final void saveBookmark(Context paramContext, String paramString1, String paramString2) {}
  
  public static final void sendString(Context paramContext, String paramString) {
    sendString(paramContext, paramString, paramContext.getString(17041252));
  }
  
  public static final void sendString(Context paramContext, String paramString1, String paramString2) {
    Intent intent = new Intent("android.intent.action.SEND");
    intent.setType("text/plain");
    intent.putExtra("android.intent.extra.TEXT", paramString1);
    try {
      Intent intent1 = Intent.createChooser(intent, paramString2);
      intent1.setFlags(268435456);
      paramContext.startActivity(intent1);
    } catch (ActivityNotFoundException activityNotFoundException) {}
  }
  
  public static final Cursor getAllBookmarks(ContentResolver paramContentResolver) throws IllegalStateException {
    SeempLog.record(32);
    return (Cursor)new MatrixCursor(new String[] { "url" }, 0);
  }
  
  public static final Cursor getAllVisitedUrls(ContentResolver paramContentResolver) throws IllegalStateException {
    SeempLog.record(33);
    return (Cursor)new MatrixCursor(new String[] { "url" }, 0);
  }
  
  private static final void addOrUrlEquals(StringBuilder paramStringBuilder) {
    paramStringBuilder.append(" OR url = ");
  }
  
  private static final Cursor getVisitedLike(ContentResolver paramContentResolver, String paramString) {
    StringBuilder stringBuilder;
    SeempLog.record(34);
    boolean bool = false;
    String str2 = paramString;
    if (str2.startsWith("http://")) {
      paramString = str2.substring(7);
    } else {
      paramString = str2;
      if (str2.startsWith("https://")) {
        paramString = str2.substring(8);
        bool = true;
      } 
    } 
    str2 = paramString;
    if (paramString.startsWith("www."))
      str2 = paramString.substring(4); 
    if (bool) {
      stringBuilder = new StringBuilder("url = ");
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("https://");
      stringBuilder1.append(str2);
      DatabaseUtils.appendEscapedSQLString(stringBuilder, stringBuilder1.toString());
      addOrUrlEquals(stringBuilder);
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("https://www.");
      stringBuilder1.append(str2);
      DatabaseUtils.appendEscapedSQLString(stringBuilder, stringBuilder1.toString());
    } else {
      stringBuilder = new StringBuilder("url = ");
      DatabaseUtils.appendEscapedSQLString(stringBuilder, str2);
      addOrUrlEquals(stringBuilder);
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("www.");
      stringBuilder2.append(str2);
      String str = stringBuilder2.toString();
      DatabaseUtils.appendEscapedSQLString(stringBuilder, str);
      addOrUrlEquals(stringBuilder);
      StringBuilder stringBuilder3 = new StringBuilder();
      stringBuilder3.append("http://");
      stringBuilder3.append(str2);
      DatabaseUtils.appendEscapedSQLString(stringBuilder, stringBuilder3.toString());
      addOrUrlEquals(stringBuilder);
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("http://");
      stringBuilder1.append(str);
      DatabaseUtils.appendEscapedSQLString(stringBuilder, stringBuilder1.toString());
    } 
    Uri uri = BrowserContract.History.CONTENT_URI;
    String str1 = stringBuilder.toString();
    return paramContentResolver.query(uri, new String[] { "_id", "visits" }, str1, null, null);
  }
  
  public static final void updateVisitedHistory(ContentResolver paramContentResolver, String paramString, boolean paramBoolean) {}
  
  @Deprecated
  public static final String[] getVisitedHistory(ContentResolver paramContentResolver) {
    SeempLog.record(35);
    return new String[0];
  }
  
  public static final void truncateHistory(ContentResolver paramContentResolver) {}
  
  public static final boolean canClearHistory(ContentResolver paramContentResolver) {
    return false;
  }
  
  public static final void clearHistory(ContentResolver paramContentResolver) {
    SeempLog.record(37);
  }
  
  public static final void deleteHistoryTimeFrame(ContentResolver paramContentResolver, long paramLong1, long paramLong2) {}
  
  public static final void deleteFromHistory(ContentResolver paramContentResolver, String paramString) {}
  
  public static final void addSearchUrl(ContentResolver paramContentResolver, String paramString) {}
  
  public static final void clearSearches(ContentResolver paramContentResolver) {}
  
  public static final void requestAllIcons(ContentResolver paramContentResolver, String paramString, WebIconDatabase.IconListener paramIconListener) {
    SeempLog.record(36);
  }
  
  class BookmarkColumns implements BaseColumns {
    public static final String BOOKMARK = "bookmark";
    
    public static final String CREATED = "created";
    
    public static final String DATE = "date";
    
    public static final String FAVICON = "favicon";
    
    public static final String THUMBNAIL = "thumbnail";
    
    public static final String TITLE = "title";
    
    public static final String TOUCH_ICON = "touch_icon";
    
    public static final String URL = "url";
    
    public static final String USER_ENTERED = "user_entered";
    
    public static final String VISITS = "visits";
  }
  
  class SearchColumns implements BaseColumns {
    public static final String DATE = "date";
    
    public static final String SEARCH = "search";
    
    @Deprecated
    public static final String URL = "url";
  }
}
