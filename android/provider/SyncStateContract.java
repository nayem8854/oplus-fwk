package android.provider;

import android.accounts.Account;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Pair;

public class SyncStateContract {
  class Columns implements BaseColumns {
    public static final String ACCOUNT_NAME = "account_name";
    
    public static final String ACCOUNT_TYPE = "account_type";
    
    public static final String DATA = "data";
  }
  
  class Constants implements Columns {
    public static final String CONTENT_DIRECTORY = "syncstate";
  }
  
  public static final class Helpers {
    private static final String[] DATA_PROJECTION = new String[] { "data", "_id" };
    
    private static final String SELECT_BY_ACCOUNT = "account_name=? AND account_type=?";
    
    public static byte[] get(ContentProviderClient param1ContentProviderClient, Uri param1Uri, Account param1Account) throws RemoteException {
      Cursor cursor = param1ContentProviderClient.query(param1Uri, DATA_PROJECTION, "account_name=? AND account_type=?", new String[] { param1Account.name, param1Account.type }, null);
      if (cursor != null)
        try {
          if (cursor.moveToNext())
            return cursor.getBlob(cursor.getColumnIndexOrThrow("data")); 
          return null;
        } finally {
          cursor.close();
        }  
      throw new RemoteException();
    }
    
    public static void set(ContentProviderClient param1ContentProviderClient, Uri param1Uri, Account param1Account, byte[] param1ArrayOfbyte) throws RemoteException {
      ContentValues contentValues = new ContentValues();
      contentValues.put("data", param1ArrayOfbyte);
      contentValues.put("account_name", param1Account.name);
      contentValues.put("account_type", param1Account.type);
      param1ContentProviderClient.insert(param1Uri, contentValues);
    }
    
    public static Uri insert(ContentProviderClient param1ContentProviderClient, Uri param1Uri, Account param1Account, byte[] param1ArrayOfbyte) throws RemoteException {
      ContentValues contentValues = new ContentValues();
      contentValues.put("data", param1ArrayOfbyte);
      contentValues.put("account_name", param1Account.name);
      contentValues.put("account_type", param1Account.type);
      return param1ContentProviderClient.insert(param1Uri, contentValues);
    }
    
    public static void update(ContentProviderClient param1ContentProviderClient, Uri param1Uri, byte[] param1ArrayOfbyte) throws RemoteException {
      ContentValues contentValues = new ContentValues();
      contentValues.put("data", param1ArrayOfbyte);
      param1ContentProviderClient.update(param1Uri, contentValues, null, null);
    }
    
    public static Pair<Uri, byte[]> getWithUri(ContentProviderClient param1ContentProviderClient, Uri param1Uri, Account param1Account) throws RemoteException {
      Cursor cursor = param1ContentProviderClient.query(param1Uri, DATA_PROJECTION, "account_name=? AND account_type=?", new String[] { param1Account.name, param1Account.type }, null);
      if (cursor != null)
        try {
          if (cursor.moveToNext()) {
            long l = cursor.getLong(1);
            byte[] arrayOfByte = cursor.getBlob(cursor.getColumnIndexOrThrow("data"));
            return Pair.create(ContentUris.withAppendedId(param1Uri, l), arrayOfByte);
          } 
          return null;
        } finally {
          cursor.close();
        }  
      throw new RemoteException();
    }
    
    public static ContentProviderOperation newSetOperation(Uri param1Uri, Account param1Account, byte[] param1ArrayOfbyte) {
      ContentValues contentValues = new ContentValues();
      contentValues.put("data", param1ArrayOfbyte);
      ContentProviderOperation.Builder builder = ContentProviderOperation.newInsert(param1Uri);
      String str2 = param1Account.name;
      builder = builder.withValue("account_name", str2);
      String str1 = param1Account.type;
      builder = builder.withValue("account_type", str1);
      builder = builder.withValues(contentValues);
      return builder.build();
    }
    
    public static ContentProviderOperation newUpdateOperation(Uri param1Uri, byte[] param1ArrayOfbyte) {
      ContentValues contentValues = new ContentValues();
      contentValues.put("data", param1ArrayOfbyte);
      ContentProviderOperation.Builder builder = ContentProviderOperation.newUpdate(param1Uri);
      builder = builder.withValues(contentValues);
      return builder.build();
    }
  }
}
