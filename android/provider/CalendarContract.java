package android.provider;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorEntityIterator;
import android.content.Entity;
import android.content.EntityIterator;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.RemoteException;
import android.util.SeempLog;
import com.android.internal.util.Preconditions;

public final class CalendarContract {
  public static final String ACCOUNT_TYPE_LOCAL = "LOCAL";
  
  public static final String ACTION_EVENT_REMINDER = "android.intent.action.EVENT_REMINDER";
  
  public static final String ACTION_HANDLE_CUSTOM_EVENT = "android.provider.calendar.action.HANDLE_CUSTOM_EVENT";
  
  public static final String ACTION_VIEW_MANAGED_PROFILE_CALENDAR_EVENT = "android.provider.calendar.action.VIEW_MANAGED_PROFILE_CALENDAR_EVENT";
  
  public static final String AUTHORITY = "com.android.calendar";
  
  public static final String CALLER_IS_SYNCADAPTER = "caller_is_syncadapter";
  
  public static final Uri CONTENT_URI = Uri.parse("content://com.android.calendar");
  
  public static final Uri ENTERPRISE_CONTENT_URI = Uri.parse("content://com.android.calendar/enterprise");
  
  public static final String EXTRA_CUSTOM_APP_URI = "customAppUri";
  
  public static final String EXTRA_EVENT_ALL_DAY = "allDay";
  
  public static final String EXTRA_EVENT_BEGIN_TIME = "beginTime";
  
  public static final String EXTRA_EVENT_END_TIME = "endTime";
  
  public static final String EXTRA_EVENT_ID = "id";
  
  private static final String TAG = "Calendar";
  
  public static boolean startViewCalendarEventInManagedProfile(Context paramContext, long paramLong1, long paramLong2, long paramLong3, boolean paramBoolean, int paramInt) {
    Preconditions.checkNotNull(paramContext, "Context is null");
    DevicePolicyManager devicePolicyManager = (DevicePolicyManager)paramContext.getSystemService("device_policy");
    return devicePolicyManager.startViewCalendarEventInManagedProfile(paramLong1, paramLong2, paramLong3, paramBoolean, paramInt);
  }
  
  class CalendarEntity implements BaseColumns, SyncColumns, CalendarColumns {
    public static final Uri CONTENT_URI = Uri.parse("content://com.android.calendar/calendar_entities");
    
    public static EntityIterator newEntityIterator(Cursor param1Cursor) {
      return (EntityIterator)new EntityIteratorImpl(param1Cursor);
    }
    
    private static class EntityIteratorImpl extends CursorEntityIterator {
      public EntityIteratorImpl(Cursor param2Cursor) {
        super(param2Cursor);
      }
      
      public Entity getEntityAndIncrementCursor(Cursor param2Cursor) throws RemoteException {
        long l = param2Cursor.getLong(param2Cursor.getColumnIndexOrThrow("_id"));
        ContentValues contentValues = new ContentValues();
        contentValues.put("_id", Long.valueOf(l));
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "account_name");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "account_type");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "_sync_id");
        DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues, "dirty");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "mutators");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "cal_sync1");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "cal_sync2");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "cal_sync3");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "cal_sync4");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "cal_sync5");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "cal_sync6");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "cal_sync7");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "cal_sync8");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "cal_sync9");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "cal_sync10");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "name");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "calendar_displayName");
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "calendar_color");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "calendar_color_index");
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "calendar_access_level");
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "visible");
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "sync_events");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "calendar_location");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "calendar_timezone");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "ownerAccount");
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "canOrganizerRespond");
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "canModifyTimeZone");
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "maxReminders");
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "canPartiallyUpdate");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "allowedReminders");
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "deleted");
        Entity entity = new Entity(contentValues);
        param2Cursor.moveToNext();
        return entity;
      }
    }
  }
  
  class Calendars implements BaseColumns, SyncColumns, CalendarColumns {
    public static final String CALENDAR_LOCATION = "calendar_location";
    
    public static final Uri CONTENT_URI = Uri.parse("content://com.android.calendar/calendars");
    
    public static final String DEFAULT_SORT_ORDER = "calendar_displayName";
    
    public static final Uri ENTERPRISE_CONTENT_URI = Uri.parse("content://com.android.calendar/enterprise/calendars");
    
    public static final String NAME = "name";
    
    public static final String[] SYNC_WRITABLE_COLUMNS = new String[] { 
        "account_name", "account_type", "_sync_id", "dirty", "mutators", "ownerAccount", "maxReminders", "allowedReminders", "canModifyTimeZone", "canOrganizerRespond", 
        "canPartiallyUpdate", "calendar_location", "calendar_timezone", "calendar_access_level", "deleted", "cal_sync1", "cal_sync2", "cal_sync3", "cal_sync4", "cal_sync5", 
        "cal_sync6", "cal_sync7", "cal_sync8", "cal_sync9", "cal_sync10" };
  }
  
  class Attendees implements BaseColumns, AttendeesColumns, EventsColumns {
    private static final String ATTENDEES_WHERE = "event_id=?";
    
    public static final Uri CONTENT_URI = Uri.parse("content://com.android.calendar/attendees");
    
    public static final Cursor query(ContentResolver param1ContentResolver, long param1Long, String[] param1ArrayOfString) {
      SeempLog.record(54);
      String str = Long.toString(param1Long);
      return param1ContentResolver.query(CONTENT_URI, param1ArrayOfString, "event_id=?", new String[] { str }, null);
    }
  }
  
  class EventsEntity implements BaseColumns, SyncColumns, EventsColumns {
    public static final Uri CONTENT_URI = Uri.parse("content://com.android.calendar/event_entities");
    
    public static EntityIterator newEntityIterator(Cursor param1Cursor, ContentResolver param1ContentResolver) {
      return (EntityIterator)new EntityIteratorImpl(param1Cursor, param1ContentResolver);
    }
    
    public static EntityIterator newEntityIterator(Cursor param1Cursor, ContentProviderClient param1ContentProviderClient) {
      return (EntityIterator)new EntityIteratorImpl(param1Cursor, param1ContentProviderClient);
    }
    
    private static class EntityIteratorImpl extends CursorEntityIterator {
      private static final String[] ATTENDEES_PROJECTION = new String[] { "attendeeName", "attendeeEmail", "attendeeRelationship", "attendeeType", "attendeeStatus", "attendeeIdentity", "attendeeIdNamespace" };
      
      private static final int COLUMN_ATTENDEE_EMAIL = 1;
      
      private static final int COLUMN_ATTENDEE_IDENTITY = 5;
      
      private static final int COLUMN_ATTENDEE_ID_NAMESPACE = 6;
      
      private static final int COLUMN_ATTENDEE_NAME = 0;
      
      private static final int COLUMN_ATTENDEE_RELATIONSHIP = 2;
      
      private static final int COLUMN_ATTENDEE_STATUS = 4;
      
      private static final int COLUMN_ATTENDEE_TYPE = 3;
      
      private static final int COLUMN_ID = 0;
      
      private static final int COLUMN_METHOD = 1;
      
      private static final int COLUMN_MINUTES = 0;
      
      private static final int COLUMN_NAME = 1;
      
      private static final int COLUMN_VALUE = 2;
      
      private static final String[] EXTENDED_PROJECTION = new String[] { "_id", "name", "value" };
      
      private static final String[] REMINDERS_PROJECTION = new String[] { "minutes", "method" };
      
      private static final String WHERE_EVENT_ID = "event_id=?";
      
      private final ContentProviderClient mProvider;
      
      private final ContentResolver mResolver;
      
      static {
      
      }
      
      public EntityIteratorImpl(Cursor param2Cursor, ContentResolver param2ContentResolver) {
        super(param2Cursor);
        this.mResolver = param2ContentResolver;
        this.mProvider = null;
      }
      
      public EntityIteratorImpl(Cursor param2Cursor, ContentProviderClient param2ContentProviderClient) {
        super(param2Cursor);
        this.mResolver = null;
        this.mProvider = param2ContentProviderClient;
      }
      
      public Entity getEntityAndIncrementCursor(Cursor param2Cursor) throws RemoteException {
        Cursor cursor2, cursor1;
        long l = param2Cursor.getLong(param2Cursor.getColumnIndexOrThrow("_id"));
        ContentValues contentValues = new ContentValues();
        contentValues.put("_id", Long.valueOf(l));
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "calendar_id");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "title");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "description");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "eventLocation");
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "eventStatus");
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "selfAttendeeStatus");
        DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues, "dtstart");
        DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues, "dtend");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "duration");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "eventTimezone");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "eventEndTimezone");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "allDay");
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "accessLevel");
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "availability");
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "eventColor");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "eventColor_index");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "hasAlarm");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "hasExtendedProperties");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "rrule");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "rdate");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "exrule");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "exdate");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "original_sync_id");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "original_id");
        DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues, "originalInstanceTime");
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "originalAllDay");
        DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues, "lastDate");
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "hasAttendeeData");
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "guestsCanInviteOthers");
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "guestsCanModify");
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "guestsCanSeeGuests");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "customAppPackage");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "customAppUri");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "uid2445");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "organizer");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "isOrganizer");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "_sync_id");
        DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues, "dirty");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "mutators");
        DatabaseUtils.cursorLongToContentValuesIfPresent(param2Cursor, contentValues, "lastSynced");
        DatabaseUtils.cursorIntToContentValuesIfPresent(param2Cursor, contentValues, "deleted");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "sync_data1");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "sync_data2");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "sync_data3");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "sync_data4");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "sync_data5");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "sync_data6");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "sync_data7");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "sync_data8");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "sync_data9");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "sync_data10");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "cal_sync1");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "cal_sync2");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "cal_sync3");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "cal_sync4");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "cal_sync5");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "cal_sync6");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "cal_sync7");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "cal_sync8");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "cal_sync9");
        DatabaseUtils.cursorStringToContentValuesIfPresent(param2Cursor, contentValues, "cal_sync10");
        Entity entity = new Entity(contentValues);
        ContentResolver contentResolver2 = this.mResolver;
        if (contentResolver2 != null) {
          Uri uri = CalendarContract.Reminders.CONTENT_URI;
          String[] arrayOfString = REMINDERS_PROJECTION;
          String str = Long.toString(l);
          cursor2 = contentResolver2.query(uri, arrayOfString, "event_id=?", new String[] { str }, null);
        } else {
          ContentProviderClient contentProviderClient = this.mProvider;
          Uri uri = CalendarContract.Reminders.CONTENT_URI;
          String[] arrayOfString = REMINDERS_PROJECTION;
          String str = Long.toString(l);
          cursor2 = contentProviderClient.query(uri, arrayOfString, "event_id=?", new String[] { str }, null);
        } 
        while (cursor2.moveToNext()) {
          ContentValues contentValues1 = new ContentValues();
          this();
          contentValues1.put("minutes", Integer.valueOf(cursor2.getInt(0)));
          contentValues1.put("method", Integer.valueOf(cursor2.getInt(1)));
          entity.addSubValue(CalendarContract.Reminders.CONTENT_URI, contentValues1);
        } 
        cursor2.close();
        ContentResolver contentResolver1 = this.mResolver;
        if (contentResolver1 != null) {
          Uri uri = CalendarContract.Attendees.CONTENT_URI;
          String[] arrayOfString = ATTENDEES_PROJECTION;
          String str = Long.toString(l);
          cursor1 = contentResolver1.query(uri, arrayOfString, "event_id=?", new String[] { str }, null);
        } else {
          ContentProviderClient contentProviderClient = this.mProvider;
          Uri uri = CalendarContract.Attendees.CONTENT_URI;
          String[] arrayOfString = ATTENDEES_PROJECTION;
          String str = Long.toString(l);
          cursor1 = contentProviderClient.query(uri, arrayOfString, "event_id=?", new String[] { str }, null);
        } 
        while (cursor1.moveToNext()) {
          ContentValues contentValues1 = new ContentValues();
          this();
          String str = cursor1.getString(0);
          contentValues1.put("attendeeName", str);
          str = cursor1.getString(1);
          contentValues1.put("attendeeEmail", str);
          int i = cursor1.getInt(2);
          contentValues1.put("attendeeRelationship", Integer.valueOf(i));
          i = cursor1.getInt(3);
          contentValues1.put("attendeeType", Integer.valueOf(i));
          i = cursor1.getInt(4);
          contentValues1.put("attendeeStatus", Integer.valueOf(i));
          str = cursor1.getString(5);
          contentValues1.put("attendeeIdentity", str);
          str = cursor1.getString(6);
          contentValues1.put("attendeeIdNamespace", str);
          entity.addSubValue(CalendarContract.Attendees.CONTENT_URI, contentValues1);
        } 
        cursor1.close();
        ContentResolver contentResolver3 = this.mResolver;
        if (contentResolver3 != null) {
          Uri uri = CalendarContract.ExtendedProperties.CONTENT_URI;
          String[] arrayOfString = EXTENDED_PROJECTION;
          String str = Long.toString(l);
          Cursor cursor = contentResolver3.query(uri, arrayOfString, "event_id=?", new String[] { str }, null);
        } else {
          ContentProviderClient contentProviderClient = this.mProvider;
          Uri uri = CalendarContract.ExtendedProperties.CONTENT_URI;
          String[] arrayOfString = EXTENDED_PROJECTION;
          String str = Long.toString(l);
          cursor1 = contentProviderClient.query(uri, arrayOfString, "event_id=?", new String[] { str }, null);
        } 
        while (cursor1.moveToNext()) {
          ContentValues contentValues1 = new ContentValues();
          this();
          String str = cursor1.getString(0);
          contentValues1.put("_id", str);
          str = cursor1.getString(1);
          contentValues1.put("name", str);
          str = cursor1.getString(2);
          contentValues1.put("value", str);
          entity.addSubValue(CalendarContract.ExtendedProperties.CONTENT_URI, contentValues1);
        } 
        cursor1.close();
        param2Cursor.moveToNext();
        return entity;
      }
    }
  }
  
  class Events implements BaseColumns, SyncColumns, EventsColumns, CalendarColumns {
    public static final Uri CONTENT_EXCEPTION_URI = Uri.parse("content://com.android.calendar/exception");
    
    public static final Uri CONTENT_URI = Uri.parse("content://com.android.calendar/events");
    
    private static final String DEFAULT_SORT_ORDER = "";
    
    public static final Uri ENTERPRISE_CONTENT_URI = Uri.parse("content://com.android.calendar/enterprise/events");
    
    public static String[] PROVIDER_WRITABLE_COLUMNS = new String[] { 
        "account_name", "account_type", "cal_sync1", "cal_sync2", "cal_sync3", "cal_sync4", "cal_sync5", "cal_sync6", "cal_sync7", "cal_sync8", 
        "cal_sync9", "cal_sync10", "allowedReminders", "allowedAttendeeTypes", "allowedAvailability", "calendar_access_level", "calendar_color", "calendar_timezone", "canModifyTimeZone", "canOrganizerRespond", 
        "calendar_displayName", "canPartiallyUpdate", "sync_events", "visible" };
    
    public static final String[] SYNC_WRITABLE_COLUMNS = new String[] { 
        "_sync_id", "dirty", "mutators", "sync_data1", "sync_data2", "sync_data3", "sync_data4", "sync_data5", "sync_data6", "sync_data7", 
        "sync_data8", "sync_data9", "sync_data10" };
    
    static {
    
    }
  }
  
  class Instances implements BaseColumns, EventsColumns, CalendarColumns {
    public static final String BEGIN = "begin";
    
    public static final Uri CONTENT_BY_DAY_URI;
    
    public static final Uri CONTENT_SEARCH_BY_DAY_URI;
    
    public static final Uri CONTENT_SEARCH_URI;
    
    public static final Uri CONTENT_URI = Uri.parse("content://com.android.calendar/instances/when");
    
    private static final String DEFAULT_SORT_ORDER = "begin ASC";
    
    public static final String END = "end";
    
    public static final String END_DAY = "endDay";
    
    public static final String END_MINUTE = "endMinute";
    
    public static final Uri ENTERPRISE_CONTENT_BY_DAY_URI;
    
    public static final Uri ENTERPRISE_CONTENT_SEARCH_BY_DAY_URI;
    
    public static final Uri ENTERPRISE_CONTENT_SEARCH_URI;
    
    public static final Uri ENTERPRISE_CONTENT_URI;
    
    public static final String EVENT_ID = "event_id";
    
    public static final String START_DAY = "startDay";
    
    public static final String START_MINUTE = "startMinute";
    
    private static final String[] WHERE_CALENDARS_ARGS = new String[] { "1" };
    
    private static final String WHERE_CALENDARS_SELECTED = "visible=?";
    
    public static final Cursor query(ContentResolver param1ContentResolver, String[] param1ArrayOfString, long param1Long1, long param1Long2) {
      SeempLog.record(54);
      Uri.Builder builder = CONTENT_URI.buildUpon();
      ContentUris.appendId(builder, param1Long1);
      ContentUris.appendId(builder, param1Long2);
      return param1ContentResolver.query(builder.build(), param1ArrayOfString, "visible=?", WHERE_CALENDARS_ARGS, "begin ASC");
    }
    
    public static final Cursor query(ContentResolver param1ContentResolver, String[] param1ArrayOfString, long param1Long1, long param1Long2, String param1String) {
      SeempLog.record(54);
      Uri.Builder builder2 = CONTENT_SEARCH_URI.buildUpon();
      ContentUris.appendId(builder2, param1Long1);
      ContentUris.appendId(builder2, param1Long2);
      Uri.Builder builder1 = builder2.appendPath(param1String);
      return param1ContentResolver.query(builder1.build(), param1ArrayOfString, "visible=?", WHERE_CALENDARS_ARGS, "begin ASC");
    }
    
    static {
      CONTENT_BY_DAY_URI = Uri.parse("content://com.android.calendar/instances/whenbyday");
      CONTENT_SEARCH_URI = Uri.parse("content://com.android.calendar/instances/search");
      CONTENT_SEARCH_BY_DAY_URI = Uri.parse("content://com.android.calendar/instances/searchbyday");
      ENTERPRISE_CONTENT_URI = Uri.parse("content://com.android.calendar/enterprise/instances/when");
      ENTERPRISE_CONTENT_BY_DAY_URI = Uri.parse("content://com.android.calendar/enterprise/instances/whenbyday");
      ENTERPRISE_CONTENT_SEARCH_URI = Uri.parse("content://com.android.calendar/enterprise/instances/search");
      ENTERPRISE_CONTENT_SEARCH_BY_DAY_URI = Uri.parse("content://com.android.calendar/enterprise/instances/searchbyday");
    }
  }
  
  class CalendarCache implements CalendarCacheColumns {
    public static final String KEY_TIMEZONE_INSTANCES = "timezoneInstances";
    
    public static final String KEY_TIMEZONE_INSTANCES_PREVIOUS = "timezoneInstancesPrevious";
    
    public static final String KEY_TIMEZONE_TYPE = "timezoneType";
    
    public static final String TIMEZONE_TYPE_AUTO = "auto";
    
    public static final String TIMEZONE_TYPE_HOME = "home";
    
    public static final Uri URI = Uri.parse("content://com.android.calendar/properties");
  }
  
  class CalendarMetaData implements CalendarMetaDataColumns, BaseColumns {}
  
  class EventDays implements EventDaysColumns {
    public static final Uri CONTENT_URI = Uri.parse("content://com.android.calendar/instances/groupbyday");
    
    private static final String SELECTION = "selected=1";
    
    public static final Cursor query(ContentResolver param1ContentResolver, int param1Int1, int param1Int2, String[] param1ArrayOfString) {
      SeempLog.record(54);
      if (param1Int2 < 1)
        return null; 
      Uri.Builder builder = CONTENT_URI.buildUpon();
      ContentUris.appendId(builder, param1Int1);
      ContentUris.appendId(builder, (param1Int1 + param1Int2 - 1));
      return param1ContentResolver.query(builder.build(), param1ArrayOfString, "selected=1", null, "startDay");
    }
  }
  
  class Reminders implements BaseColumns, RemindersColumns, EventsColumns {
    public static final Uri CONTENT_URI = Uri.parse("content://com.android.calendar/reminders");
    
    private static final String REMINDERS_WHERE = "event_id=?";
    
    public static final Cursor query(ContentResolver param1ContentResolver, long param1Long, String[] param1ArrayOfString) {
      SeempLog.record(54);
      String str = Long.toString(param1Long);
      return param1ContentResolver.query(CONTENT_URI, param1ArrayOfString, "event_id=?", new String[] { str }, null);
    }
  }
  
  class CalendarAlerts implements BaseColumns, CalendarAlertsColumns, EventsColumns, CalendarColumns {
    public static final Uri CONTENT_URI = Uri.parse("content://com.android.calendar/calendar_alerts");
    
    public static final Uri CONTENT_URI_BY_INSTANCE = Uri.parse("content://com.android.calendar/calendar_alerts/by_instance");
    
    private static final boolean DEBUG = false;
    
    private static final String SORT_ORDER_ALARMTIME_ASC = "alarmTime ASC";
    
    public static final String TABLE_NAME = "CalendarAlerts";
    
    private static final String WHERE_ALARM_EXISTS = "event_id=? AND begin=? AND alarmTime=?";
    
    private static final String WHERE_FINDNEXTALARMTIME = "alarmTime>=?";
    
    private static final String WHERE_RESCHEDULE_MISSED_ALARMS = "state=0 AND alarmTime<? AND alarmTime>? AND end>=?";
    
    public static final Uri insert(ContentResolver param1ContentResolver, long param1Long1, long param1Long2, long param1Long3, long param1Long4, int param1Int) {
      SeempLog.record(51);
      ContentValues contentValues = new ContentValues();
      contentValues.put("event_id", Long.valueOf(param1Long1));
      contentValues.put("begin", Long.valueOf(param1Long2));
      contentValues.put("end", Long.valueOf(param1Long3));
      contentValues.put("alarmTime", Long.valueOf(param1Long4));
      param1Long1 = System.currentTimeMillis();
      contentValues.put("creationTime", Long.valueOf(param1Long1));
      Integer integer = Integer.valueOf(0);
      contentValues.put("receivedTime", integer);
      contentValues.put("notifyTime", integer);
      contentValues.put("state", integer);
      contentValues.put("minutes", Integer.valueOf(param1Int));
      return param1ContentResolver.insert(CONTENT_URI, contentValues);
    }
    
    public static final long findNextAlarmTime(ContentResolver param1ContentResolver, long param1Long) {
      SeempLog.record(53);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("alarmTime>=");
      stringBuilder.append(param1Long);
      stringBuilder.toString();
      Uri uri = CONTENT_URI;
      String str = Long.toString(param1Long);
      Cursor cursor = param1ContentResolver.query(uri, new String[] { "alarmTime" }, "alarmTime>=?", new String[] { str }, "alarmTime ASC");
      long l = -1L;
      param1Long = l;
      if (cursor != null)
        param1Long = l; 
      if (cursor != null)
        cursor.close(); 
      return param1Long;
    }
    
    public static final void rescheduleMissedAlarms(ContentResolver param1ContentResolver, Context param1Context, AlarmManager param1AlarmManager) {
      long l = System.currentTimeMillis();
      Uri uri = CONTENT_URI;
      String str1 = Long.toString(l), str2 = Long.toString(l - 86400000L), str3 = Long.toString(l);
      Cursor cursor = param1ContentResolver.query(uri, new String[] { "alarmTime" }, "state=0 AND alarmTime<? AND alarmTime>? AND end>=?", new String[] { str1, str2, str3 }, "alarmTime ASC");
      if (cursor == null)
        return; 
      l = -1L;
      try {
        while (cursor.moveToNext()) {
          long l1 = cursor.getLong(0);
          long l2 = l;
          if (l != l1) {
            scheduleAlarm(param1Context, param1AlarmManager, l1);
            l2 = l1;
          } 
          l = l2;
        } 
        return;
      } finally {
        cursor.close();
      } 
    }
    
    public static void scheduleAlarm(Context param1Context, AlarmManager param1AlarmManager, long param1Long) {
      AlarmManager alarmManager = param1AlarmManager;
      if (param1AlarmManager == null)
        alarmManager = (AlarmManager)param1Context.getSystemService("alarm"); 
      Intent intent = new Intent("android.intent.action.EVENT_REMINDER");
      intent.setData(ContentUris.withAppendedId(CalendarContract.CONTENT_URI, param1Long));
      intent.putExtra("alarmTime", param1Long);
      intent.setFlags(16777216);
      PendingIntent pendingIntent = PendingIntent.getBroadcast(param1Context, 0, intent, 0);
      alarmManager.setExactAndAllowWhileIdle(0, param1Long, pendingIntent);
    }
    
    public static final boolean alarmExists(ContentResolver param1ContentResolver, long param1Long1, long param1Long2, long param1Long3) {
      SeempLog.record(52);
      Uri uri = CONTENT_URI;
      String str1 = Long.toString(param1Long1), str2 = Long.toString(param1Long2), str3 = Long.toString(param1Long3);
      Cursor cursor = param1ContentResolver.query(uri, new String[] { "alarmTime" }, "event_id=? AND begin=? AND alarmTime=?", new String[] { str1, str2, str3 }, null);
      boolean bool1 = false;
      boolean bool2 = bool1;
      if (cursor != null)
        try {
          int i = cursor.getCount();
          bool2 = bool1;
        } finally {
          if (cursor != null)
            cursor.close(); 
        }  
      if (cursor != null)
        cursor.close(); 
      return bool2;
    }
  }
  
  class Colors implements ColorsColumns {
    public static final Uri CONTENT_URI = Uri.parse("content://com.android.calendar/colors");
    
    public static final String TABLE_NAME = "Colors";
  }
  
  class ExtendedProperties implements BaseColumns, ExtendedPropertiesColumns, EventsColumns {
    public static final Uri CONTENT_URI = Uri.parse("content://com.android.calendar/extendedproperties");
  }
  
  class SyncState implements SyncStateContract.Columns {
    private static final String CONTENT_DIRECTORY = "syncstate";
    
    public static final Uri CONTENT_URI;
    
    static {
      Uri uri = CalendarContract.CONTENT_URI;
      CONTENT_URI = Uri.withAppendedPath(uri, "syncstate");
    }
  }
  
  class EventsRawTimes implements BaseColumns, EventsRawTimesColumns {}
  
  protected static interface AttendeesColumns {
    public static final String ATTENDEE_EMAIL = "attendeeEmail";
    
    public static final String ATTENDEE_IDENTITY = "attendeeIdentity";
    
    public static final String ATTENDEE_ID_NAMESPACE = "attendeeIdNamespace";
    
    public static final String ATTENDEE_NAME = "attendeeName";
    
    public static final String ATTENDEE_RELATIONSHIP = "attendeeRelationship";
    
    public static final String ATTENDEE_STATUS = "attendeeStatus";
    
    public static final int ATTENDEE_STATUS_ACCEPTED = 1;
    
    public static final int ATTENDEE_STATUS_DECLINED = 2;
    
    public static final int ATTENDEE_STATUS_INVITED = 3;
    
    public static final int ATTENDEE_STATUS_NONE = 0;
    
    public static final int ATTENDEE_STATUS_TENTATIVE = 4;
    
    public static final String ATTENDEE_TYPE = "attendeeType";
    
    public static final String EVENT_ID = "event_id";
    
    public static final int RELATIONSHIP_ATTENDEE = 1;
    
    public static final int RELATIONSHIP_NONE = 0;
    
    public static final int RELATIONSHIP_ORGANIZER = 2;
    
    public static final int RELATIONSHIP_PERFORMER = 3;
    
    public static final int RELATIONSHIP_SPEAKER = 4;
    
    public static final int TYPE_NONE = 0;
    
    public static final int TYPE_OPTIONAL = 2;
    
    public static final int TYPE_REQUIRED = 1;
    
    public static final int TYPE_RESOURCE = 3;
  }
  
  protected static interface CalendarAlertsColumns {
    public static final String ALARM_TIME = "alarmTime";
    
    public static final String BEGIN = "begin";
    
    public static final String CREATION_TIME = "creationTime";
    
    public static final String DEFAULT_SORT_ORDER = "begin ASC,title ASC";
    
    public static final String END = "end";
    
    public static final String EVENT_ID = "event_id";
    
    public static final String MINUTES = "minutes";
    
    public static final String NOTIFY_TIME = "notifyTime";
    
    public static final String RECEIVED_TIME = "receivedTime";
    
    public static final String STATE = "state";
    
    public static final int STATE_DISMISSED = 2;
    
    public static final int STATE_FIRED = 1;
    
    public static final int STATE_SCHEDULED = 0;
  }
  
  protected static interface CalendarCacheColumns {
    public static final String KEY = "key";
    
    public static final String VALUE = "value";
  }
  
  protected static interface CalendarColumns {
    public static final String ALLOWED_ATTENDEE_TYPES = "allowedAttendeeTypes";
    
    public static final String ALLOWED_AVAILABILITY = "allowedAvailability";
    
    public static final String ALLOWED_REMINDERS = "allowedReminders";
    
    public static final String CALENDAR_ACCESS_LEVEL = "calendar_access_level";
    
    public static final String CALENDAR_COLOR = "calendar_color";
    
    public static final String CALENDAR_COLOR_KEY = "calendar_color_index";
    
    public static final String CALENDAR_DISPLAY_NAME = "calendar_displayName";
    
    public static final String CALENDAR_TIME_ZONE = "calendar_timezone";
    
    public static final int CAL_ACCESS_CONTRIBUTOR = 500;
    
    public static final int CAL_ACCESS_EDITOR = 600;
    
    public static final int CAL_ACCESS_FREEBUSY = 100;
    
    public static final int CAL_ACCESS_NONE = 0;
    
    public static final int CAL_ACCESS_OVERRIDE = 400;
    
    public static final int CAL_ACCESS_OWNER = 700;
    
    public static final int CAL_ACCESS_READ = 200;
    
    public static final int CAL_ACCESS_RESPOND = 300;
    
    public static final int CAL_ACCESS_ROOT = 800;
    
    public static final String CAN_MODIFY_TIME_ZONE = "canModifyTimeZone";
    
    public static final String CAN_ORGANIZER_RESPOND = "canOrganizerRespond";
    
    public static final String IS_PRIMARY = "isPrimary";
    
    public static final String MAX_REMINDERS = "maxReminders";
    
    public static final String OWNER_ACCOUNT = "ownerAccount";
    
    public static final String SYNC_EVENTS = "sync_events";
    
    public static final String VISIBLE = "visible";
  }
  
  protected static interface CalendarMetaDataColumns {
    public static final String LOCAL_TIMEZONE = "localTimezone";
    
    public static final String MAX_EVENTDAYS = "maxEventDays";
    
    public static final String MAX_INSTANCE = "maxInstance";
    
    public static final String MIN_EVENTDAYS = "minEventDays";
    
    public static final String MIN_INSTANCE = "minInstance";
  }
  
  protected static interface CalendarSyncColumns {
    public static final String CAL_SYNC1 = "cal_sync1";
    
    public static final String CAL_SYNC10 = "cal_sync10";
    
    public static final String CAL_SYNC2 = "cal_sync2";
    
    public static final String CAL_SYNC3 = "cal_sync3";
    
    public static final String CAL_SYNC4 = "cal_sync4";
    
    public static final String CAL_SYNC5 = "cal_sync5";
    
    public static final String CAL_SYNC6 = "cal_sync6";
    
    public static final String CAL_SYNC7 = "cal_sync7";
    
    public static final String CAL_SYNC8 = "cal_sync8";
    
    public static final String CAL_SYNC9 = "cal_sync9";
  }
  
  class ColorsColumns implements SyncStateContract.Columns {
    public static final String COLOR = "color";
    
    public static final String COLOR_KEY = "color_index";
    
    public static final String COLOR_TYPE = "color_type";
    
    public static final int TYPE_CALENDAR = 0;
    
    public static final int TYPE_EVENT = 1;
  }
  
  protected static interface EventDaysColumns {
    public static final String ENDDAY = "endDay";
    
    public static final String STARTDAY = "startDay";
  }
  
  protected static interface EventsColumns {
    public static final int ACCESS_CONFIDENTIAL = 1;
    
    public static final int ACCESS_DEFAULT = 0;
    
    public static final String ACCESS_LEVEL = "accessLevel";
    
    public static final int ACCESS_PRIVATE = 2;
    
    public static final int ACCESS_PUBLIC = 3;
    
    public static final String ALL_DAY = "allDay";
    
    public static final String AVAILABILITY = "availability";
    
    public static final int AVAILABILITY_BUSY = 0;
    
    public static final int AVAILABILITY_FREE = 1;
    
    public static final int AVAILABILITY_TENTATIVE = 2;
    
    public static final String CALENDAR_ID = "calendar_id";
    
    public static final String CAN_INVITE_OTHERS = "canInviteOthers";
    
    public static final String CUSTOM_APP_PACKAGE = "customAppPackage";
    
    public static final String CUSTOM_APP_URI = "customAppUri";
    
    public static final String DESCRIPTION = "description";
    
    public static final String DISPLAY_COLOR = "displayColor";
    
    public static final String DTEND = "dtend";
    
    public static final String DTSTART = "dtstart";
    
    public static final String DURATION = "duration";
    
    public static final String EVENT_COLOR = "eventColor";
    
    public static final String EVENT_COLOR_KEY = "eventColor_index";
    
    public static final String EVENT_END_TIMEZONE = "eventEndTimezone";
    
    public static final String EVENT_LOCATION = "eventLocation";
    
    public static final String EVENT_TIMEZONE = "eventTimezone";
    
    public static final String EXDATE = "exdate";
    
    public static final String EXRULE = "exrule";
    
    public static final String GUESTS_CAN_INVITE_OTHERS = "guestsCanInviteOthers";
    
    public static final String GUESTS_CAN_MODIFY = "guestsCanModify";
    
    public static final String GUESTS_CAN_SEE_GUESTS = "guestsCanSeeGuests";
    
    public static final String HAS_ALARM = "hasAlarm";
    
    public static final String HAS_ATTENDEE_DATA = "hasAttendeeData";
    
    public static final String HAS_EXTENDED_PROPERTIES = "hasExtendedProperties";
    
    public static final String IS_ORGANIZER = "isOrganizer";
    
    public static final String LAST_DATE = "lastDate";
    
    public static final String LAST_SYNCED = "lastSynced";
    
    public static final String ORGANIZER = "organizer";
    
    public static final String ORIGINAL_ALL_DAY = "originalAllDay";
    
    public static final String ORIGINAL_ID = "original_id";
    
    public static final String ORIGINAL_INSTANCE_TIME = "originalInstanceTime";
    
    public static final String ORIGINAL_SYNC_ID = "original_sync_id";
    
    public static final String RDATE = "rdate";
    
    public static final String RRULE = "rrule";
    
    public static final String SELF_ATTENDEE_STATUS = "selfAttendeeStatus";
    
    public static final String STATUS = "eventStatus";
    
    public static final int STATUS_CANCELED = 2;
    
    public static final int STATUS_CONFIRMED = 1;
    
    public static final int STATUS_TENTATIVE = 0;
    
    public static final String SYNC_DATA1 = "sync_data1";
    
    public static final String SYNC_DATA10 = "sync_data10";
    
    public static final String SYNC_DATA2 = "sync_data2";
    
    public static final String SYNC_DATA3 = "sync_data3";
    
    public static final String SYNC_DATA4 = "sync_data4";
    
    public static final String SYNC_DATA5 = "sync_data5";
    
    public static final String SYNC_DATA6 = "sync_data6";
    
    public static final String SYNC_DATA7 = "sync_data7";
    
    public static final String SYNC_DATA8 = "sync_data8";
    
    public static final String SYNC_DATA9 = "sync_data9";
    
    public static final String TITLE = "title";
    
    public static final String UID_2445 = "uid2445";
  }
  
  protected static interface EventsRawTimesColumns {
    public static final String DTEND_2445 = "dtend2445";
    
    public static final String DTSTART_2445 = "dtstart2445";
    
    public static final String EVENT_ID = "event_id";
    
    public static final String LAST_DATE_2445 = "lastDate2445";
    
    public static final String ORIGINAL_INSTANCE_TIME_2445 = "originalInstanceTime2445";
  }
  
  protected static interface ExtendedPropertiesColumns {
    public static final String EVENT_ID = "event_id";
    
    public static final String NAME = "name";
    
    public static final String VALUE = "value";
  }
  
  protected static interface RemindersColumns {
    public static final String EVENT_ID = "event_id";
    
    public static final String METHOD = "method";
    
    public static final int METHOD_ALARM = 4;
    
    public static final int METHOD_ALERT = 1;
    
    public static final int METHOD_DEFAULT = 0;
    
    public static final int METHOD_EMAIL = 2;
    
    public static final int METHOD_SMS = 3;
    
    public static final String MINUTES = "minutes";
    
    public static final int MINUTES_DEFAULT = -1;
  }
  
  class SyncColumns implements CalendarSyncColumns {
    public static final String ACCOUNT_NAME = "account_name";
    
    public static final String ACCOUNT_TYPE = "account_type";
    
    public static final String CAN_PARTIALLY_UPDATE = "canPartiallyUpdate";
    
    public static final String DELETED = "deleted";
    
    public static final String DIRTY = "dirty";
    
    public static final String MUTATORS = "mutators";
    
    public static final String _SYNC_ID = "_sync_id";
  }
}
