package android.provider;

import android.annotation.SystemApi;
import android.app.ActivityThread;
import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.util.ArrayMap;
import android.util.Log;
import android.util.Pair;
import com.android.internal.util.Preconditions;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;

@SystemApi
public final class DeviceConfig {
  public static final Uri CONTENT_URI = Uri.parse("content://settings/config");
  
  @SystemApi
  public static final String NAMESPACE_ACTIVITY_MANAGER = "activity_manager";
  
  @SystemApi
  public static final String NAMESPACE_ACTIVITY_MANAGER_NATIVE_BOOT = "activity_manager_native_boot";
  
  public static final String NAMESPACE_ANDROID = "android";
  
  @SystemApi
  public static final String NAMESPACE_APP_COMPAT = "app_compat";
  
  @SystemApi
  public static final String NAMESPACE_ATTENTION_MANAGER_SERVICE = "attention_manager_service";
  
  @SystemApi
  public static final String NAMESPACE_AUTOFILL = "autofill";
  
  @SystemApi
  public static final String NAMESPACE_BIOMETRICS = "biometrics";
  
  @SystemApi
  public static final String NAMESPACE_BLOBSTORE = "blobstore";
  
  public static final String NAMESPACE_CONFIGURATION = "configuration";
  
  @SystemApi
  public static final String NAMESPACE_CONNECTIVITY = "connectivity";
  
  public static final String NAMESPACE_CONNECTIVITY_THERMAL_POWER_MANAGER = "connectivity_thermal_power_manager";
  
  public static final String NAMESPACE_CONTACTS_PROVIDER = "contacts_provider";
  
  @SystemApi
  public static final String NAMESPACE_CONTENT_CAPTURE = "content_capture";
  
  @SystemApi
  @Deprecated
  public static final String NAMESPACE_DEX_BOOT = "dex_boot";
  
  @SystemApi
  public static final String NAMESPACE_DISPLAY_MANAGER = "display_manager";
  
  @SystemApi
  public static final String NAMESPACE_GAME_DRIVER = "game_driver";
  
  @SystemApi
  public static final String NAMESPACE_INPUT_NATIVE_BOOT = "input_native_boot";
  
  @SystemApi
  public static final String NAMESPACE_INTELLIGENCE_ATTENTION = "intelligence_attention";
  
  public static final String NAMESPACE_INTELLIGENCE_CONTENT_SUGGESTIONS = "intelligence_content_suggestions";
  
  @SystemApi
  public static final String NAMESPACE_MEDIA_NATIVE = "media_native";
  
  @SystemApi
  public static final String NAMESPACE_NETD_NATIVE = "netd_native";
  
  @SystemApi
  public static final String NAMESPACE_PACKAGE_MANAGER_SERVICE = "package_manager_service";
  
  @SystemApi
  public static final String NAMESPACE_PERMISSIONS = "permissions";
  
  @SystemApi
  public static final String NAMESPACE_PRIVACY = "privacy";
  
  @SystemApi
  public static final String NAMESPACE_ROLLBACK = "rollback";
  
  @SystemApi
  public static final String NAMESPACE_ROLLBACK_BOOT = "rollback_boot";
  
  @SystemApi
  public static final String NAMESPACE_RUNTIME = "runtime";
  
  @SystemApi
  public static final String NAMESPACE_RUNTIME_NATIVE = "runtime_native";
  
  @SystemApi
  public static final String NAMESPACE_RUNTIME_NATIVE_BOOT = "runtime_native_boot";
  
  @SystemApi
  public static final String NAMESPACE_SCHEDULER = "scheduler";
  
  public static final String NAMESPACE_SETTINGS_STATS = "settings_stats";
  
  public static final String NAMESPACE_SETTINGS_UI = "settings_ui";
  
  @SystemApi
  @Deprecated
  public static final String NAMESPACE_STORAGE = "storage";
  
  @SystemApi
  public static final String NAMESPACE_STORAGE_NATIVE_BOOT = "storage_native_boot";
  
  @SystemApi
  public static final String NAMESPACE_SYSTEMUI = "systemui";
  
  @SystemApi
  public static final String NAMESPACE_TELEPHONY = "telephony";
  
  @SystemApi
  public static final String NAMESPACE_TEXTCLASSIFIER = "textclassifier";
  
  public static final String NAMESPACE_WIDGET = "widget";
  
  public static final String NAMESPACE_WINDOW_MANAGER = "window_manager";
  
  @SystemApi
  public static final String NAMESPACE_WINDOW_MANAGER_NATIVE_BOOT = "window_manager_native_boot";
  
  private static final List<String> PUBLIC_NAMESPACES = Arrays.asList(new String[] { "textclassifier", "runtime" });
  
  private static final String TAG = "DeviceConfig";
  
  private static ArrayMap<OnPropertiesChangedListener, Pair<String, Executor>> sListeners;
  
  private static final Object sLock = new Object();
  
  private static Map<String, Pair<ContentObserver, Integer>> sNamespaces;
  
  static {
    sListeners = new ArrayMap();
    sNamespaces = new HashMap<>();
  }
  
  @SystemApi
  public static String getProperty(String paramString1, String paramString2) {
    return getProperties(paramString1, new String[] { paramString2 }).getString(paramString2, null);
  }
  
  @SystemApi
  public static Properties getProperties(String paramString, String... paramVarArgs) {
    ContentResolver contentResolver = ActivityThread.currentApplication().getContentResolver();
    return 
      new Properties(paramString, Settings.Config.getStrings(contentResolver, paramString, Arrays.asList(paramVarArgs)));
  }
  
  @SystemApi
  public static String getString(String paramString1, String paramString2, String paramString3) {
    paramString1 = getProperty(paramString1, paramString2);
    if (paramString1 == null)
      paramString1 = paramString3; 
    return paramString1;
  }
  
  @SystemApi
  public static boolean getBoolean(String paramString1, String paramString2, boolean paramBoolean) {
    paramString1 = getProperty(paramString1, paramString2);
    if (paramString1 != null)
      paramBoolean = Boolean.parseBoolean(paramString1); 
    return paramBoolean;
  }
  
  @SystemApi
  public static int getInt(String paramString1, String paramString2, int paramInt) {
    String str = getProperty(paramString1, paramString2);
    if (str == null)
      return paramInt; 
    try {
      return Integer.parseInt(str);
    } catch (NumberFormatException numberFormatException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Parsing integer failed for ");
      stringBuilder.append(paramString1);
      stringBuilder.append(":");
      stringBuilder.append(paramString2);
      Log.e("DeviceConfig", stringBuilder.toString());
      return paramInt;
    } 
  }
  
  @SystemApi
  public static long getLong(String paramString1, String paramString2, long paramLong) {
    String str = getProperty(paramString1, paramString2);
    if (str == null)
      return paramLong; 
    try {
      return Long.parseLong(str);
    } catch (NumberFormatException numberFormatException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Parsing long failed for ");
      stringBuilder.append(paramString1);
      stringBuilder.append(":");
      stringBuilder.append(paramString2);
      Log.e("DeviceConfig", stringBuilder.toString());
      return paramLong;
    } 
  }
  
  @SystemApi
  public static float getFloat(String paramString1, String paramString2, float paramFloat) {
    String str = getProperty(paramString1, paramString2);
    if (str == null)
      return paramFloat; 
    try {
      return Float.parseFloat(str);
    } catch (NumberFormatException numberFormatException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Parsing float failed for ");
      stringBuilder.append(paramString1);
      stringBuilder.append(":");
      stringBuilder.append(paramString2);
      Log.e("DeviceConfig", stringBuilder.toString());
      return paramFloat;
    } 
  }
  
  @SystemApi
  public static boolean setProperty(String paramString1, String paramString2, String paramString3, boolean paramBoolean) {
    ContentResolver contentResolver = ActivityThread.currentApplication().getContentResolver();
    return Settings.Config.putString(contentResolver, paramString1, paramString2, paramString3, paramBoolean);
  }
  
  @SystemApi
  public static boolean setProperties(Properties paramProperties) throws BadConfigException {
    ContentResolver contentResolver = ActivityThread.currentApplication().getContentResolver();
    String str = paramProperties.getNamespace();
    HashMap<String, String> hashMap = paramProperties.mMap;
    return Settings.Config.setStrings(contentResolver, str, hashMap);
  }
  
  @SystemApi
  public static void resetToDefaults(int paramInt, String paramString) {
    ContentResolver contentResolver = ActivityThread.currentApplication().getContentResolver();
    Settings.Config.resetToDefaults(contentResolver, paramInt, paramString);
  }
  
  @SystemApi
  public static void addOnPropertiesChangedListener(String paramString, Executor paramExecutor, OnPropertiesChangedListener paramOnPropertiesChangedListener) {
    enforceReadPermission(ActivityThread.currentApplication().getApplicationContext(), paramString);
    synchronized (sLock) {
      Pair pair = (Pair)sListeners.get(paramOnPropertiesChangedListener);
      if (pair == null) {
        ArrayMap<OnPropertiesChangedListener, Pair<String, Executor>> arrayMap = sListeners;
        pair = new Pair();
        this(paramString, paramExecutor);
        arrayMap.put(paramOnPropertiesChangedListener, pair);
        incrementNamespace(paramString);
      } else if (paramString.equals(pair.first)) {
        ArrayMap<OnPropertiesChangedListener, Pair<String, Executor>> arrayMap = sListeners;
        pair = new Pair();
        this(paramString, paramExecutor);
        arrayMap.put(paramOnPropertiesChangedListener, pair);
      } else {
        decrementNamespace((String)((Pair)sListeners.get(paramOnPropertiesChangedListener)).first);
        ArrayMap<OnPropertiesChangedListener, Pair<String, Executor>> arrayMap = sListeners;
        Pair pair1 = new Pair();
        this(paramString, paramExecutor);
        arrayMap.put(paramOnPropertiesChangedListener, pair1);
        incrementNamespace(paramString);
      } 
      return;
    } 
  }
  
  @SystemApi
  public static void removeOnPropertiesChangedListener(OnPropertiesChangedListener paramOnPropertiesChangedListener) {
    Preconditions.checkNotNull(paramOnPropertiesChangedListener);
    synchronized (sLock) {
      if (sListeners.containsKey(paramOnPropertiesChangedListener)) {
        decrementNamespace((String)((Pair)sListeners.get(paramOnPropertiesChangedListener)).first);
        sListeners.remove(paramOnPropertiesChangedListener);
      } 
      return;
    } 
  }
  
  private static Uri createNamespaceUri(String paramString) {
    Preconditions.checkNotNull(paramString);
    return CONTENT_URI.buildUpon().appendPath(paramString).build();
  }
  
  private static void incrementNamespace(String paramString) {
    Preconditions.checkNotNull(paramString);
    Pair pair = sNamespaces.get(paramString);
    if (pair != null) {
      sNamespaces.put(paramString, new Pair(pair.first, Integer.valueOf(((Integer)pair.second).intValue() + 1)));
    } else {
      Object object = new Object(null);
      ContentResolver contentResolver = ActivityThread.currentApplication().getContentResolver();
      contentResolver.registerContentObserver(createNamespaceUri(paramString), true, (ContentObserver)object);
      sNamespaces.put(paramString, new Pair(object, Integer.valueOf(1)));
    } 
  }
  
  private static void decrementNamespace(String paramString) {
    Preconditions.checkNotNull(paramString);
    Pair pair = sNamespaces.get(paramString);
    if (pair == null)
      return; 
    if (((Integer)pair.second).intValue() > 1) {
      sNamespaces.put(paramString, new Pair(pair.first, Integer.valueOf(((Integer)pair.second).intValue() - 1)));
    } else {
      ContentResolver contentResolver = ActivityThread.currentApplication().getContentResolver();
      ContentObserver contentObserver = (ContentObserver)pair.first;
      contentResolver.unregisterContentObserver(contentObserver);
      sNamespaces.remove(paramString);
    } 
  }
  
  private static void handleChange(Uri paramUri) {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic checkNotNull : (Ljava/lang/Object;)Ljava/lang/Object;
    //   4: pop
    //   5: aload_0
    //   6: invokevirtual getPathSegments : ()Ljava/util/List;
    //   9: astore_1
    //   10: aload_1
    //   11: ifnull -> 226
    //   14: aload_1
    //   15: invokeinterface size : ()I
    //   20: iconst_2
    //   21: if_icmpge -> 27
    //   24: goto -> 226
    //   27: aload_1
    //   28: iconst_1
    //   29: invokeinterface get : (I)Ljava/lang/Object;
    //   34: checkcast java/lang/String
    //   37: astore_0
    //   38: new android/provider/DeviceConfig$Properties$Builder
    //   41: dup
    //   42: aload_0
    //   43: invokespecial <init> : (Ljava/lang/String;)V
    //   46: astore_2
    //   47: aload_0
    //   48: iconst_0
    //   49: anewarray java/lang/String
    //   52: invokestatic getProperties : (Ljava/lang/String;[Ljava/lang/String;)Landroid/provider/DeviceConfig$Properties;
    //   55: astore_3
    //   56: iconst_2
    //   57: istore #4
    //   59: iload #4
    //   61: aload_1
    //   62: invokeinterface size : ()I
    //   67: if_icmpge -> 103
    //   70: aload_1
    //   71: iload #4
    //   73: invokeinterface get : (I)Ljava/lang/Object;
    //   78: checkcast java/lang/String
    //   81: astore #5
    //   83: aload_2
    //   84: aload #5
    //   86: aload_3
    //   87: aload #5
    //   89: aconst_null
    //   90: invokevirtual getString : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   93: invokevirtual setString : (Ljava/lang/String;Ljava/lang/String;)Landroid/provider/DeviceConfig$Properties$Builder;
    //   96: pop
    //   97: iinc #4, 1
    //   100: goto -> 59
    //   103: aload_2
    //   104: invokevirtual build : ()Landroid/provider/DeviceConfig$Properties;
    //   107: astore_1
    //   108: getstatic android/provider/DeviceConfig.sLock : Ljava/lang/Object;
    //   111: astore_2
    //   112: aload_2
    //   113: monitorenter
    //   114: iconst_0
    //   115: istore #4
    //   117: iload #4
    //   119: getstatic android/provider/DeviceConfig.sListeners : Landroid/util/ArrayMap;
    //   122: invokevirtual size : ()I
    //   125: if_icmpge -> 207
    //   128: aload_0
    //   129: getstatic android/provider/DeviceConfig.sListeners : Landroid/util/ArrayMap;
    //   132: iload #4
    //   134: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   137: checkcast android/util/Pair
    //   140: getfield first : Ljava/lang/Object;
    //   143: invokevirtual equals : (Ljava/lang/Object;)Z
    //   146: ifeq -> 201
    //   149: getstatic android/provider/DeviceConfig.sListeners : Landroid/util/ArrayMap;
    //   152: iload #4
    //   154: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   157: checkcast android/provider/DeviceConfig$OnPropertiesChangedListener
    //   160: astore #5
    //   162: getstatic android/provider/DeviceConfig.sListeners : Landroid/util/ArrayMap;
    //   165: iload #4
    //   167: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   170: checkcast android/util/Pair
    //   173: getfield second : Ljava/lang/Object;
    //   176: checkcast java/util/concurrent/Executor
    //   179: astore_3
    //   180: new android/provider/_$$Lambda$DeviceConfig$6U9gBH6h5Oab2DB_e83az4n_WEo
    //   183: astore #6
    //   185: aload #6
    //   187: aload #5
    //   189: aload_1
    //   190: invokespecial <init> : (Landroid/provider/DeviceConfig$OnPropertiesChangedListener;Landroid/provider/DeviceConfig$Properties;)V
    //   193: aload_3
    //   194: aload #6
    //   196: invokeinterface execute : (Ljava/lang/Runnable;)V
    //   201: iinc #4, 1
    //   204: goto -> 117
    //   207: aload_2
    //   208: monitorexit
    //   209: return
    //   210: astore_0
    //   211: aload_2
    //   212: monitorexit
    //   213: aload_0
    //   214: athrow
    //   215: astore_0
    //   216: ldc 'DeviceConfig'
    //   218: ldc_w 'OnPropertyChangedListener update failed: permission violation.'
    //   221: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   224: pop
    //   225: return
    //   226: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #784	-> 0
    //   #785	-> 5
    //   #788	-> 10
    //   #793	-> 27
    //   #794	-> 38
    //   #796	-> 47
    //   #797	-> 56
    //   #798	-> 70
    //   #799	-> 83
    //   #797	-> 97
    //   #805	-> 103
    //   #806	-> 103
    //   #808	-> 108
    //   #809	-> 114
    //   #810	-> 128
    //   #811	-> 149
    //   #812	-> 162
    //   #809	-> 201
    //   #817	-> 207
    //   #818	-> 209
    //   #817	-> 210
    //   #801	-> 215
    //   #803	-> 216
    //   #804	-> 225
    //   #789	-> 226
    // Exception table:
    //   from	to	target	type
    //   47	56	215	java/lang/SecurityException
    //   59	70	215	java/lang/SecurityException
    //   70	83	215	java/lang/SecurityException
    //   83	97	215	java/lang/SecurityException
    //   117	128	210	finally
    //   128	149	210	finally
    //   149	162	210	finally
    //   162	201	210	finally
    //   207	209	210	finally
    //   211	213	210	finally
  }
  
  public static void enforceReadPermission(Context paramContext, String paramString) {
    if (paramContext.checkCallingOrSelfPermission("android.permission.READ_DEVICE_CONFIG") != 0)
      if (!PUBLIC_NAMESPACES.contains(paramString))
        throw new SecurityException("Permission denial: reading from settings requires:android.permission.READ_DEVICE_CONFIG");  
  }
  
  public static List<String> getPublicNamespaces() {
    return PUBLIC_NAMESPACES;
  }
  
  @SystemApi
  public static class BadConfigException extends Exception {}
  
  @SystemApi
  public static interface OnPropertiesChangedListener {
    void onPropertiesChanged(DeviceConfig.Properties param1Properties);
  }
  
  @SystemApi
  public static class Properties {
    private Set<String> mKeyset;
    
    private final HashMap<String, String> mMap;
    
    private final String mNamespace;
    
    public Properties(String param1String, Map<String, String> param1Map) {
      Preconditions.checkNotNull(param1String);
      this.mNamespace = param1String;
      HashMap<Object, Object> hashMap = new HashMap<>();
      if (param1Map != null)
        hashMap.putAll(param1Map); 
    }
    
    public String getNamespace() {
      return this.mNamespace;
    }
    
    public Set<String> getKeyset() {
      if (this.mKeyset == null)
        this.mKeyset = Collections.unmodifiableSet(this.mMap.keySet()); 
      return this.mKeyset;
    }
    
    public String getString(String param1String1, String param1String2) {
      Preconditions.checkNotNull(param1String1);
      param1String1 = this.mMap.get(param1String1);
      if (param1String1 == null)
        param1String1 = param1String2; 
      return param1String1;
    }
    
    public boolean getBoolean(String param1String, boolean param1Boolean) {
      Preconditions.checkNotNull(param1String);
      param1String = this.mMap.get(param1String);
      if (param1String != null)
        param1Boolean = Boolean.parseBoolean(param1String); 
      return param1Boolean;
    }
    
    public int getInt(String param1String, int param1Int) {
      Preconditions.checkNotNull(param1String);
      String str = this.mMap.get(param1String);
      if (str == null)
        return param1Int; 
      try {
        return Integer.parseInt(str);
      } catch (NumberFormatException numberFormatException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Parsing int failed for ");
        stringBuilder.append(param1String);
        Log.e("DeviceConfig", stringBuilder.toString());
        return param1Int;
      } 
    }
    
    public long getLong(String param1String, long param1Long) {
      Preconditions.checkNotNull(param1String);
      String str = this.mMap.get(param1String);
      if (str == null)
        return param1Long; 
      try {
        return Long.parseLong(str);
      } catch (NumberFormatException numberFormatException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Parsing long failed for ");
        stringBuilder.append(param1String);
        Log.e("DeviceConfig", stringBuilder.toString());
        return param1Long;
      } 
    }
    
    public float getFloat(String param1String, float param1Float) {
      Preconditions.checkNotNull(param1String);
      String str = this.mMap.get(param1String);
      if (str == null)
        return param1Float; 
      try {
        return Float.parseFloat(str);
      } catch (NumberFormatException numberFormatException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Parsing float failed for ");
        stringBuilder.append(param1String);
        Log.e("DeviceConfig", stringBuilder.toString());
        return param1Float;
      } 
    }
    
    public static final class Builder {
      private final Map<String, String> mKeyValues = new HashMap<>();
      
      private final String mNamespace;
      
      public Builder(String param2String) {
        this.mNamespace = param2String;
      }
      
      public Builder setString(String param2String1, String param2String2) {
        this.mKeyValues.put(param2String1, param2String2);
        return this;
      }
      
      public Builder setBoolean(String param2String, boolean param2Boolean) {
        this.mKeyValues.put(param2String, Boolean.toString(param2Boolean));
        return this;
      }
      
      public Builder setInt(String param2String, int param2Int) {
        this.mKeyValues.put(param2String, Integer.toString(param2Int));
        return this;
      }
      
      public Builder setLong(String param2String, long param2Long) {
        this.mKeyValues.put(param2String, Long.toString(param2Long));
        return this;
      }
      
      public Builder setFloat(String param2String, float param2Float) {
        this.mKeyValues.put(param2String, Float.toString(param2Float));
        return this;
      }
      
      public DeviceConfig.Properties build() {
        return new DeviceConfig.Properties(this.mNamespace, this.mKeyValues);
      }
    }
  }
  
  public static final class Builder {
    private final Map<String, String> mKeyValues = new HashMap<>();
    
    private final String mNamespace;
    
    public Builder(String param1String) {
      this.mNamespace = param1String;
    }
    
    public Builder setString(String param1String1, String param1String2) {
      this.mKeyValues.put(param1String1, param1String2);
      return this;
    }
    
    public Builder setBoolean(String param1String, boolean param1Boolean) {
      this.mKeyValues.put(param1String, Boolean.toString(param1Boolean));
      return this;
    }
    
    public Builder setInt(String param1String, int param1Int) {
      this.mKeyValues.put(param1String, Integer.toString(param1Int));
      return this;
    }
    
    public Builder setLong(String param1String, long param1Long) {
      this.mKeyValues.put(param1String, Long.toString(param1Long));
      return this;
    }
    
    public Builder setFloat(String param1String, float param1Float) {
      this.mKeyValues.put(param1String, Float.toString(param1Float));
      return this;
    }
    
    public DeviceConfig.Properties build() {
      return new DeviceConfig.Properties(this.mNamespace, this.mKeyValues);
    }
  }
}
