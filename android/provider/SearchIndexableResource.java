package android.provider;

import android.annotation.SystemApi;
import android.content.Context;

@SystemApi
public class SearchIndexableResource extends SearchIndexableData {
  public int xmlResId;
  
  public SearchIndexableResource(int paramInt1, int paramInt2, String paramString, int paramInt3) {
    this.rank = paramInt1;
    this.xmlResId = paramInt2;
    this.className = paramString;
    this.iconResId = paramInt3;
  }
  
  public SearchIndexableResource(Context paramContext) {
    super(paramContext);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SearchIndexableResource[");
    stringBuilder.append(super.toString());
    stringBuilder.append(", ");
    stringBuilder.append("xmlResId: ");
    stringBuilder.append(this.xmlResId);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
}
