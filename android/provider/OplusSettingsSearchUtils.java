package android.provider;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

public class OplusSettingsSearchUtils {
  public static final String ARGS_HIGHT_LIGHT_TIME = ":settings:fragment_args_light_time";
  
  public static final String ARGS_KEY = ":settings:fragment_args_key";
  
  public static final String ARGS_OPLUS_CATEGORY = ":settings:fragment_args_color_category";
  
  public static final String ARGS_OPLUS_PREFERENCE = ":settings:fragment_args_color_preferece";
  
  public static final String ARGS_WAIT_TIME = ":settings:fragment_args_wait_time";
  
  private static final int DELAY_TIME = 150;
  
  public static final int HIGHT_LIGHT_OPLUS_PREFERENCE_DEFAULT = -1776412;
  
  public static final int HIGH_LIGHT_TIME_DEFAULT = 1000;
  
  private static final int LAST_TIME = 500;
  
  public static final String RAW_RENAME_EXTRA_KEY = "_settings_extra_key";
  
  private static final int START_TIME = 100;
  
  private static final int STOP_TIME = 250;
  
  public static final int WAIT_TIME_DEFAULT = 300;
  
  public static void highlightListView(ListView paramListView, int paramInt, boolean paramBoolean, Intent paramIntent) {
    highlightListView(paramListView, paramInt, paramBoolean, paramIntent, 0);
  }
  
  public static void highlightListView(final ListView listView, final int position, final boolean isCategory, Intent paramIntent, final int y) {
    if (listView == null || paramIntent == null)
      return; 
    String str = paramIntent.getStringExtra(":settings:fragment_args_key");
    if (TextUtils.isEmpty(str))
      return; 
    if (listView != null)
      listView.post(new Runnable() {
            final boolean val$isCategory;
            
            final ListView val$listView;
            
            final int val$position;
            
            final int val$y;
            
            public void run() {
              int i = position;
              if (i >= 0) {
                int j = y;
                if (j == 0) {
                  listView.setSelection(i);
                } else {
                  listView.setSelectionFromTop(i, j);
                } 
                OplusSettingsSearchUtils.showHightlight(listView, position, -1776412, isCategory);
              } 
            }
          }); 
  }
  
  public static void highlightPreference(PreferenceScreen paramPreferenceScreen, ListView paramListView, Bundle paramBundle) {
    highlightPreference(paramPreferenceScreen, paramListView, paramBundle, 0);
  }
  
  public static void highlightPreference(PreferenceScreen paramPreferenceScreen, ListView paramListView, Bundle paramBundle, int paramInt) {
    if (paramPreferenceScreen == null || paramListView == null || paramBundle == null)
      return; 
    String str = paramBundle.getString(":settings:fragment_args_key");
    if (TextUtils.isEmpty(str))
      return; 
    calculateHightlight(paramPreferenceScreen, paramListView, str, -1776412, paramInt);
  }
  
  public static void highlightPreference(ListView paramListView, Bundle paramBundle) {
    highlightPreference(paramListView, paramBundle, 0);
  }
  
  public static void highlightPreference(ListView paramListView, Bundle paramBundle, int paramInt) {
    if (paramListView == null || paramBundle == null)
      return; 
    String str = paramBundle.getString(":settings:fragment_args_key");
    if (TextUtils.isEmpty(str))
      return; 
    calculateHightlight(paramListView, str, -1776412, false, paramInt);
  }
  
  private static void calculateHightlight(final ListView listView, final String argsKey, final int argsColorPreference, final boolean isCategory, final int y) {
    if (listView != null)
      listView.post(new Runnable() {
            final int val$argsColorPreference;
            
            final String val$argsKey;
            
            final boolean val$isCategory;
            
            final ListView val$listView;
            
            final int val$y;
            
            public void run() {
              int i = OplusSettingsSearchUtils.canUseListViewForHighLighting(listView, argsKey);
              if (i > 1) {
                int j = y;
                if (j == 0) {
                  listView.setSelection(i);
                } else {
                  listView.setSelectionFromTop(i, j);
                } 
              } 
              if (i >= 0)
                OplusSettingsSearchUtils.showHightlight(listView, i, argsColorPreference, isCategory); 
            }
          }); 
  }
  
  private static void calculateHightlight(PreferenceScreen paramPreferenceScreen, ListView paramListView, String paramString, int paramInt1, int paramInt2) {
    boolean bool;
    Preference preference = paramPreferenceScreen.findPreference(paramString);
    if (preference == null)
      return; 
    if (preference instanceof android.preference.PreferenceCategory) {
      bool = true;
    } else {
      bool = false;
    } 
    calculateHightlight(paramListView, paramString, paramInt1, bool, paramInt2);
  }
  
  private static void showHightlight(final ListView listView, final int position, final int backgroundColor, boolean paramBoolean) {
    if (paramBoolean)
      return; 
    listView.postDelayed(new Runnable() {
          final int val$backgroundColor;
          
          final ListView val$listView;
          
          final int val$position;
          
          public void run() {
            int i = position - listView.getFirstVisiblePosition();
            if (i >= 0 && i < listView.getChildCount()) {
              final View view = listView.getChildAt(i);
              if (view == null)
                return; 
              final Drawable drawable = view.getBackground();
              AnimationDrawable animationDrawable = OplusSettingsSearchUtils.getAnimationDrawable(backgroundColor, drawable);
              view.setBackgroundDrawable((Drawable)animationDrawable);
              animationDrawable.start();
              view.postDelayed(new Runnable() {
                    final OplusSettingsSearchUtils.null this$0;
                    
                    final Drawable val$drawable;
                    
                    final View val$view;
                    
                    public void run() {
                      view.setBackground(drawable);
                    }
                  },  1000L);
            } 
          }
        }300L);
  }
  
  private static AnimationDrawable getAnimationDrawable(int paramInt, Drawable paramDrawable) {
    AnimationDrawable animationDrawable = new AnimationDrawable();
    byte b;
    for (b = 0; b < 6; b++) {
      LayerDrawable layerDrawable;
      double d = (b + 0.0D) * 255.0D / 6;
      ColorDrawable colorDrawable = new ColorDrawable(paramInt);
      colorDrawable.setAlpha((int)d);
      if (paramDrawable != null) {
        layerDrawable = new LayerDrawable(new Drawable[] { paramDrawable, (Drawable)colorDrawable });
        animationDrawable.addFrame((Drawable)layerDrawable, 16);
      } else {
        animationDrawable.addFrame((Drawable)layerDrawable, 16);
      } 
    } 
    animationDrawable.addFrame((Drawable)new ColorDrawable(paramInt), 250);
    for (b = 0; b < 31; b++) {
      LayerDrawable layerDrawable;
      double d = ((31 - b) - 0.0D) * 255.0D / 31;
      ColorDrawable colorDrawable = new ColorDrawable(paramInt);
      colorDrawable.setAlpha((int)d);
      if (paramDrawable != null) {
        layerDrawable = new LayerDrawable(new Drawable[] { paramDrawable, (Drawable)colorDrawable });
        animationDrawable.addFrame((Drawable)layerDrawable, 16);
      } else {
        animationDrawable.addFrame((Drawable)layerDrawable, 16);
        if (b == 31 - 1) {
          ColorDrawable colorDrawable1 = new ColorDrawable(0);
          animationDrawable.addFrame((Drawable)colorDrawable1, 300);
        } 
      } 
    } 
    if (paramDrawable != null)
      animationDrawable.addFrame(paramDrawable, 150); 
    return animationDrawable;
  }
  
  private static int canUseListViewForHighLighting(ListView paramListView, String paramString) {
    ListAdapter listAdapter = paramListView.getAdapter();
    if (listAdapter != null) {
      int i = listAdapter.getCount();
      for (byte b = 0; b < i; b++) {
        Object object = listAdapter.getItem(b);
        if (object instanceof Preference) {
          object = object;
          object = object.getKey();
          if (object != null && object.equals(paramString))
            return b; 
        } 
      } 
      return -1;
    } 
    return -1;
  }
}
