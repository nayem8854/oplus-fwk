package android.provider;

import android.app.admin.DevicePolicyManager;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.UriMatcher;
import android.net.Uri;
import android.os.UserHandle;
import android.text.TextUtils;
import android.widget.Toast;
import java.util.List;

public class ContactsInternal {
  private static final int CONTACTS_URI_LOOKUP = 1001;
  
  private static final int CONTACTS_URI_LOOKUP_ID = 1000;
  
  private static final UriMatcher sContactsUriMatcher = new UriMatcher(-1);
  
  static {
    UriMatcher uriMatcher = sContactsUriMatcher;
    uriMatcher.addURI("com.android.contacts", "contacts/lookup/*", 1001);
    uriMatcher.addURI("com.android.contacts", "contacts/lookup/*/#", 1000);
  }
  
  public static void startQuickContactWithErrorToast(Context paramContext, Intent paramIntent) {
    Uri uri = paramIntent.getData();
    int i = sContactsUriMatcher.match(uri);
    if (i == 1000 || i == 1001)
      if (maybeStartManagedQuickContact(paramContext, paramIntent))
        return;  
    startQuickContactWithErrorToastForUser(paramContext, paramIntent, paramContext.getUser());
  }
  
  public static void startQuickContactWithErrorToastForUser(Context paramContext, Intent paramIntent, UserHandle paramUserHandle) {
    try {
      paramContext.startActivityAsUser(paramIntent, paramUserHandle);
    } catch (ActivityNotFoundException activityNotFoundException) {
      Toast toast = Toast.makeText(paramContext, 17041138, 0);
      toast.show();
    } 
  }
  
  private static boolean maybeStartManagedQuickContact(Context paramContext, Intent paramIntent) {
    boolean bool;
    long l1, l2;
    Uri uri = paramIntent.getData();
    List<String> list = uri.getPathSegments();
    if (list.size() < 4) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool) {
      l1 = ContactsContract.Contacts.ENTERPRISE_CONTACT_ID_BASE;
    } else {
      l1 = ContentUris.parseId(uri);
    } 
    String str2 = list.get(2);
    String str1 = uri.getQueryParameter("directory");
    if (str1 == null) {
      l2 = 1000000000L;
    } else {
      l2 = Long.parseLong(str1);
    } 
    if (!TextUtils.isEmpty(str2)) {
      str1 = ContactsContract.Contacts.ENTERPRISE_CONTACT_LOOKUP_PREFIX;
      if (str2.startsWith(str1)) {
        if (ContactsContract.Contacts.isEnterpriseContactId(l1)) {
          if (ContactsContract.Directory.isEnterpriseDirectoryId(l2)) {
            DevicePolicyManager devicePolicyManager = (DevicePolicyManager)paramContext.getSystemService(DevicePolicyManager.class);
            str1 = ContactsContract.Contacts.ENTERPRISE_CONTACT_LOOKUP_PREFIX;
            int i = str1.length();
            str2 = str2.substring(i);
            long l = ContactsContract.Contacts.ENTERPRISE_CONTACT_ID_BASE;
            devicePolicyManager.startManagedQuickContact(str2, l1 - l, bool, l2 - 1000000000L, paramIntent);
            return true;
          } 
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Invalid enterprise directory id: ");
          stringBuilder1.append(l2);
          throw new IllegalArgumentException(stringBuilder1.toString());
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid enterprise contact id: ");
        stringBuilder.append(l1);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
    } 
    return false;
  }
}
