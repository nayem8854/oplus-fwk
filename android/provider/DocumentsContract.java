package android.provider;

import android.annotation.SystemApi;
import android.content.ContentInterface;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.MimeTypeFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.ParcelableException;
import android.os.RemoteException;
import android.os.UserHandle;
import android.util.Log;
import com.android.internal.util.Preconditions;
import dalvik.system.VMRuntime;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public final class DocumentsContract {
  @SystemApi
  public static final String ACTION_DOCUMENT_ROOT_SETTINGS = "android.provider.action.DOCUMENT_ROOT_SETTINGS";
  
  public static final String ACTION_DOCUMENT_SETTINGS = "android.provider.action.DOCUMENT_SETTINGS";
  
  @SystemApi
  public static final String ACTION_MANAGE_DOCUMENT = "android.provider.action.MANAGE_DOCUMENT";
  
  public static final String EXTERNAL_STORAGE_PRIMARY_EMULATED_ROOT_ID = "primary";
  
  public static final String EXTERNAL_STORAGE_PROVIDER_AUTHORITY = "com.android.externalstorage.documents";
  
  public static final String EXTRA_ERROR = "error";
  
  public static final String EXTRA_EXCLUDE_SELF = "android.provider.extra.EXCLUDE_SELF";
  
  public static final String EXTRA_INFO = "info";
  
  public static final String EXTRA_INITIAL_URI = "android.provider.extra.INITIAL_URI";
  
  public static final String EXTRA_LOADING = "loading";
  
  public static final String EXTRA_OPTIONS = "options";
  
  public static final String EXTRA_ORIENTATION = "android.provider.extra.ORIENTATION";
  
  @Deprecated
  public static final String EXTRA_PACKAGE_NAME = "android.intent.extra.PACKAGE_NAME";
  
  public static final String EXTRA_PARENT_URI = "parentUri";
  
  public static final String EXTRA_PROMPT = "android.provider.extra.PROMPT";
  
  public static final String EXTRA_RESULT = "result";
  
  @SystemApi
  public static final String EXTRA_SHOW_ADVANCED = "android.provider.extra.SHOW_ADVANCED";
  
  public static final String EXTRA_TARGET_URI = "android.content.extra.TARGET_URI";
  
  public static final String EXTRA_URI = "uri";
  
  public static final String EXTRA_URI_PERMISSIONS = "uriPermissions";
  
  public static final String METADATA_EXIF = "android:documentExif";
  
  public static final String METADATA_TREE_COUNT = "android:metadataTreeCount";
  
  public static final String METADATA_TREE_SIZE = "android:metadataTreeSize";
  
  public static final String METADATA_TYPES = "android:documentMetadataTypes";
  
  public static final String METHOD_COPY_DOCUMENT = "android:copyDocument";
  
  public static final String METHOD_CREATE_DOCUMENT = "android:createDocument";
  
  public static final String METHOD_CREATE_WEB_LINK_INTENT = "android:createWebLinkIntent";
  
  public static final String METHOD_DELETE_DOCUMENT = "android:deleteDocument";
  
  public static final String METHOD_EJECT_ROOT = "android:ejectRoot";
  
  public static final String METHOD_FIND_DOCUMENT_PATH = "android:findDocumentPath";
  
  public static final String METHOD_GET_DOCUMENT_METADATA = "android:getDocumentMetadata";
  
  public static final String METHOD_IS_CHILD_DOCUMENT = "android:isChildDocument";
  
  public static final String METHOD_MOVE_DOCUMENT = "android:moveDocument";
  
  public static final String METHOD_REMOVE_DOCUMENT = "android:removeDocument";
  
  public static final String METHOD_RENAME_DOCUMENT = "android:renameDocument";
  
  public static final String PACKAGE_DOCUMENTS_UI = "com.android.documentsui";
  
  private static final String PARAM_MANAGE = "manage";
  
  private static final String PARAM_QUERY = "query";
  
  private static final String PATH_CHILDREN = "children";
  
  private static final String PATH_DOCUMENT = "document";
  
  private static final String PATH_RECENT = "recent";
  
  private static final String PATH_ROOT = "root";
  
  private static final String PATH_SEARCH = "search";
  
  private static final String PATH_TREE = "tree";
  
  public static final String PROVIDER_INTERFACE = "android.content.action.DOCUMENTS_PROVIDER";
  
  public static final String QUERY_ARG_DISPLAY_NAME = "android:query-arg-display-name";
  
  public static final String QUERY_ARG_EXCLUDE_MEDIA = "android:query-arg-exclude-media";
  
  public static final String QUERY_ARG_FILE_SIZE_OVER = "android:query-arg-file-size-over";
  
  public static final String QUERY_ARG_LAST_MODIFIED_AFTER = "android:query-arg-last-modified-after";
  
  public static final String QUERY_ARG_MIME_TYPES = "android:query-arg-mime-types";
  
  private static final String TAG = "DocumentsContract";
  
  public static final class Document {
    public static final String COLUMN_DISPLAY_NAME = "_display_name";
    
    public static final String COLUMN_DOCUMENT_ID = "document_id";
    
    public static final String COLUMN_FLAGS = "flags";
    
    public static final String COLUMN_ICON = "icon";
    
    public static final String COLUMN_LAST_MODIFIED = "last_modified";
    
    public static final String COLUMN_MIME_TYPE = "mime_type";
    
    public static final String COLUMN_SIZE = "_size";
    
    public static final String COLUMN_SUMMARY = "summary";
    
    public static final int FLAG_DIR_BLOCKS_OPEN_DOCUMENT_TREE = 32768;
    
    public static final int FLAG_DIR_PREFERS_GRID = 16;
    
    public static final int FLAG_DIR_PREFERS_LAST_MODIFIED = 32;
    
    public static final int FLAG_DIR_SUPPORTS_CREATE = 8;
    
    public static final int FLAG_PARTIAL = 8192;
    
    public static final int FLAG_SUPPORTS_COPY = 128;
    
    public static final int FLAG_SUPPORTS_DELETE = 4;
    
    public static final int FLAG_SUPPORTS_METADATA = 16384;
    
    public static final int FLAG_SUPPORTS_MOVE = 256;
    
    public static final int FLAG_SUPPORTS_REMOVE = 1024;
    
    public static final int FLAG_SUPPORTS_RENAME = 64;
    
    public static final int FLAG_SUPPORTS_SETTINGS = 2048;
    
    public static final int FLAG_SUPPORTS_THUMBNAIL = 1;
    
    public static final int FLAG_SUPPORTS_WRITE = 2;
    
    public static final int FLAG_VIRTUAL_DOCUMENT = 512;
    
    public static final int FLAG_WEB_LINKABLE = 4096;
    
    public static final String MIME_TYPE_DIR = "vnd.android.document/directory";
  }
  
  public static final class Root {
    public static final String COLUMN_AVAILABLE_BYTES = "available_bytes";
    
    public static final String COLUMN_CAPACITY_BYTES = "capacity_bytes";
    
    public static final String COLUMN_DOCUMENT_ID = "document_id";
    
    public static final String COLUMN_FLAGS = "flags";
    
    public static final String COLUMN_ICON = "icon";
    
    public static final String COLUMN_MIME_TYPES = "mime_types";
    
    public static final String COLUMN_QUERY_ARGS = "query_args";
    
    public static final String COLUMN_ROOT_ID = "root_id";
    
    public static final String COLUMN_SUMMARY = "summary";
    
    public static final String COLUMN_TITLE = "title";
    
    @SystemApi
    public static final int FLAG_ADVANCED = 65536;
    
    public static final int FLAG_EMPTY = 64;
    
    @SystemApi
    public static final int FLAG_HAS_SETTINGS = 131072;
    
    public static final int FLAG_LOCAL_ONLY = 2;
    
    @SystemApi
    public static final int FLAG_REMOVABLE_SD = 262144;
    
    @SystemApi
    public static final int FLAG_REMOVABLE_USB = 524288;
    
    public static final int FLAG_SUPPORTS_CREATE = 1;
    
    public static final int FLAG_SUPPORTS_EJECT = 32;
    
    public static final int FLAG_SUPPORTS_IS_CHILD = 16;
    
    public static final int FLAG_SUPPORTS_RECENTS = 4;
    
    public static final int FLAG_SUPPORTS_SEARCH = 8;
    
    public static final String MIME_TYPE_ITEM = "vnd.android.document/root";
  }
  
  public static Uri buildRootsUri(String paramString) {
    Uri.Builder builder = (new Uri.Builder()).scheme("content");
    return 
      builder.authority(paramString).appendPath("root").build();
  }
  
  public static Uri buildRootUri(String paramString1, String paramString2) {
    Uri.Builder builder = (new Uri.Builder()).scheme("content");
    return 
      builder.authority(paramString1).appendPath("root").appendPath(paramString2).build();
  }
  
  public static Uri buildRecentDocumentsUri(String paramString1, String paramString2) {
    Uri.Builder builder2 = (new Uri.Builder()).scheme("content");
    Uri.Builder builder1 = builder2.authority(paramString1).appendPath("root").appendPath(paramString2);
    return builder1.appendPath("recent").build();
  }
  
  public static Uri buildTreeDocumentUri(String paramString1, String paramString2) {
    Uri.Builder builder = (new Uri.Builder()).scheme("content").authority(paramString1);
    return 
      builder.appendPath("tree").appendPath(paramString2).build();
  }
  
  public static Uri buildDocumentUri(String paramString1, String paramString2) {
    return getBaseDocumentUriBuilder(paramString1).appendPath(paramString2).build();
  }
  
  @SystemApi
  public static Uri buildDocumentUriAsUser(String paramString1, String paramString2, UserHandle paramUserHandle) {
    Uri uri = buildDocumentUri(paramString1, paramString2);
    int i = paramUserHandle.getIdentifier();
    return ContentProvider.maybeAddUserId(uri, i);
  }
  
  public static Uri buildBaseDocumentUri(String paramString) {
    return getBaseDocumentUriBuilder(paramString).build();
  }
  
  private static Uri.Builder getBaseDocumentUriBuilder(String paramString) {
    Uri.Builder builder = (new Uri.Builder()).scheme("content");
    return 
      builder.authority(paramString).appendPath("document");
  }
  
  public static Uri buildDocumentUriUsingTree(Uri paramUri, String paramString) {
    Uri.Builder builder2 = (new Uri.Builder()).scheme("content");
    builder2 = builder2.authority(paramUri.getAuthority()).appendPath("tree");
    Uri.Builder builder1 = builder2.appendPath(getTreeDocumentId(paramUri)).appendPath("document");
    return builder1.appendPath(paramString).build();
  }
  
  public static Uri buildDocumentUriMaybeUsingTree(Uri paramUri, String paramString) {
    if (isTreeUri(paramUri))
      return buildDocumentUriUsingTree(paramUri, paramString); 
    return buildDocumentUri(paramUri.getAuthority(), paramString);
  }
  
  public static Uri buildChildDocumentsUri(String paramString1, String paramString2) {
    Uri.Builder builder = (new Uri.Builder()).scheme("content").authority(paramString1);
    builder = builder.appendPath("document").appendPath(paramString2).appendPath("children");
    return builder.build();
  }
  
  public static Uri buildChildDocumentsUriUsingTree(Uri paramUri, String paramString) {
    Uri.Builder builder2 = (new Uri.Builder()).scheme("content");
    builder2 = builder2.authority(paramUri.getAuthority()).appendPath("tree");
    Uri.Builder builder1 = builder2.appendPath(getTreeDocumentId(paramUri)).appendPath("document");
    return builder1.appendPath(paramString).appendPath("children").build();
  }
  
  public static Uri buildSearchDocumentsUri(String paramString1, String paramString2, String paramString3) {
    Uri.Builder builder = (new Uri.Builder()).scheme("content").authority(paramString1);
    builder = builder.appendPath("root").appendPath(paramString2).appendPath("search");
    return builder.appendQueryParameter("query", paramString3).build();
  }
  
  public static boolean matchSearchQueryArguments(Bundle paramBundle, String paramString1, String paramString2, long paramLong1, long paramLong2) {
    if (paramBundle == null)
      return true; 
    String str = paramBundle.getString("android:query-arg-display-name", "");
    if (!str.isEmpty())
      if (!paramString1.toLowerCase().contains(str.toLowerCase()))
        return false;  
    long l = paramBundle.getLong("android:query-arg-file-size-over", -1L);
    if (l != -1L && paramLong2 < l)
      return false; 
    paramLong2 = paramBundle.getLong("android:query-arg-last-modified-after", -1L);
    if (paramLong2 != -1L && paramLong1 < paramLong2)
      return false; 
    String[] arrayOfString = paramBundle.getStringArray("android:query-arg-mime-types");
    if (arrayOfString != null && arrayOfString.length > 0) {
      paramString1 = Intent.normalizeMimeType(paramString2);
      int i;
      byte b;
      for (i = arrayOfString.length, b = 0; b < i; ) {
        paramString2 = arrayOfString[b];
        if (MimeTypeFilter.matches(paramString1, Intent.normalizeMimeType(paramString2)))
          return true; 
        b++;
      } 
      return false;
    } 
    return true;
  }
  
  public static String[] getHandledQueryArguments(Bundle paramBundle) {
    if (paramBundle == null)
      return new String[0]; 
    ArrayList<String> arrayList = new ArrayList();
    if (paramBundle.keySet().contains("android:query-arg-exclude-media"))
      arrayList.add("android:query-arg-exclude-media"); 
    if (paramBundle.keySet().contains("android:query-arg-display-name"))
      arrayList.add("android:query-arg-display-name"); 
    if (paramBundle.keySet().contains("android:query-arg-file-size-over"))
      arrayList.add("android:query-arg-file-size-over"); 
    if (paramBundle.keySet().contains("android:query-arg-last-modified-after"))
      arrayList.add("android:query-arg-last-modified-after"); 
    if (paramBundle.keySet().contains("android:query-arg-mime-types"))
      arrayList.add("android:query-arg-mime-types"); 
    return arrayList.<String>toArray(new String[0]);
  }
  
  public static boolean isDocumentUri(Context paramContext, Uri paramUri) {
    boolean bool = isContentUri(paramUri);
    boolean bool1 = false;
    if (bool && isDocumentsProvider(paramContext, paramUri.getAuthority())) {
      List<String> list = paramUri.getPathSegments();
      if (list.size() == 2)
        return "document".equals(list.get(0)); 
      if (list.size() == 4) {
        bool = bool1;
        if ("tree".equals(list.get(0))) {
          bool = bool1;
          if ("document".equals(list.get(2)))
            bool = true; 
        } 
        return bool;
      } 
    } 
    return false;
  }
  
  public static boolean isRootsUri(Context paramContext, Uri paramUri) {
    Preconditions.checkNotNull(paramContext, "context can not be null");
    return isRootUri(paramContext, paramUri, 1);
  }
  
  public static boolean isRootUri(Context paramContext, Uri paramUri) {
    Preconditions.checkNotNull(paramContext, "context can not be null");
    return isRootUri(paramContext, paramUri, 2);
  }
  
  public static boolean isContentUri(Uri paramUri) {
    boolean bool;
    if (paramUri != null && "content".equals(paramUri.getScheme())) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isTreeUri(Uri paramUri) {
    List<String> list = paramUri.getPathSegments();
    int i = list.size();
    boolean bool1 = false, bool2 = bool1;
    if (i >= 2) {
      bool2 = bool1;
      if ("tree".equals(list.get(0)))
        bool2 = true; 
    } 
    return bool2;
  }
  
  private static boolean isRootUri(Context paramContext, Uri paramUri, int paramInt) {
    boolean bool = isContentUri(paramUri);
    boolean bool1 = false;
    if (bool && isDocumentsProvider(paramContext, paramUri.getAuthority())) {
      List<String> list = paramUri.getPathSegments();
      bool = bool1;
      if (list.size() == paramInt) {
        bool = bool1;
        if ("root".equals(list.get(0)))
          bool = true; 
      } 
      return bool;
    } 
    return false;
  }
  
  private static boolean isDocumentsProvider(Context paramContext, String paramString) {
    Intent intent = new Intent("android.content.action.DOCUMENTS_PROVIDER");
    PackageManager packageManager = paramContext.getPackageManager();
    List list = packageManager.queryIntentContentProviders(intent, 0);
    for (ResolveInfo resolveInfo : list) {
      if (paramString.equals(resolveInfo.providerInfo.authority))
        return true; 
    } 
    return false;
  }
  
  public static String getRootId(Uri paramUri) {
    List<String> list = paramUri.getPathSegments();
    if (list.size() >= 2 && "root".equals(list.get(0)))
      return list.get(1); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid URI: ");
    stringBuilder.append(paramUri);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static String getDocumentId(Uri paramUri) {
    List<String> list = paramUri.getPathSegments();
    if (list.size() >= 2 && "document".equals(list.get(0)))
      return list.get(1); 
    if (list.size() >= 4 && "tree".equals(list.get(0)) && 
      "document".equals(list.get(2)))
      return list.get(3); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid URI: ");
    stringBuilder.append(paramUri);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static String getTreeDocumentId(Uri paramUri) {
    List<String> list = paramUri.getPathSegments();
    if (list.size() >= 2 && "tree".equals(list.get(0)))
      return list.get(1); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid URI: ");
    stringBuilder.append(paramUri);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static String getSearchDocumentsQuery(Uri paramUri) {
    return paramUri.getQueryParameter("query");
  }
  
  public static String getSearchDocumentsQuery(Bundle paramBundle) {
    Preconditions.checkNotNull(paramBundle, "bundle can not be null");
    return paramBundle.getString("android:query-arg-display-name", "");
  }
  
  @SystemApi
  public static Uri setManageMode(Uri paramUri) {
    Preconditions.checkNotNull(paramUri, "uri can not be null");
    return paramUri.buildUpon().appendQueryParameter("manage", "true").build();
  }
  
  @SystemApi
  public static boolean isManageMode(Uri paramUri) {
    Preconditions.checkNotNull(paramUri, "uri can not be null");
    return paramUri.getBooleanQueryParameter("manage", false);
  }
  
  public static Bitmap getDocumentThumbnail(ContentResolver paramContentResolver, Uri paramUri, Point paramPoint, CancellationSignal paramCancellationSignal) throws FileNotFoundException {
    try {
      return ContentResolver.loadThumbnail((ContentInterface)paramContentResolver, paramUri, Point.convert(paramPoint), paramCancellationSignal, 1);
    } catch (Exception exception) {
      if (!(exception instanceof android.os.OperationCanceledException)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to load thumbnail for ");
        stringBuilder.append(paramUri);
        stringBuilder.append(": ");
        stringBuilder.append(exception);
        Log.w("DocumentsContract", stringBuilder.toString());
      } 
      rethrowIfNecessary(exception);
      return null;
    } 
  }
  
  public static Uri createDocument(ContentResolver paramContentResolver, Uri paramUri, String paramString1, String paramString2) throws FileNotFoundException {
    try {
      Bundle bundle2 = new Bundle();
      this();
      bundle2.putParcelable("uri", paramUri);
      bundle2.putString("mime_type", paramString1);
      bundle2.putString("_display_name", paramString2);
      Bundle bundle1 = paramContentResolver.call(paramUri.getAuthority(), "android:createDocument", null, bundle2);
      return bundle1.<Uri>getParcelable("uri");
    } catch (Exception exception) {
      Log.w("DocumentsContract", "Failed to create document", exception);
      rethrowIfNecessary(exception);
      return null;
    } 
  }
  
  public static boolean isChildDocument(ContentResolver paramContentResolver, Uri paramUri1, Uri paramUri2) throws FileNotFoundException {
    Preconditions.checkNotNull(paramContentResolver, "content can not be null");
    Preconditions.checkNotNull(paramUri1, "parentDocumentUri can not be null");
    Preconditions.checkNotNull(paramUri2, "childDocumentUri can not be null");
    try {
      Bundle bundle2 = new Bundle();
      this();
      bundle2.putParcelable("uri", paramUri1);
      bundle2.putParcelable("android.content.extra.TARGET_URI", paramUri2);
      Bundle bundle1 = paramContentResolver.call(paramUri1.getAuthority(), "android:isChildDocument", null, bundle2);
      if (bundle1 != null) {
        if (bundle1.containsKey("result"))
          return bundle1.getBoolean("result"); 
        RemoteException remoteException1 = new RemoteException();
        this("Response did not include result field..");
        throw remoteException1;
      } 
      RemoteException remoteException = new RemoteException();
      this("Failed to get a response from isChildDocument query.");
      throw remoteException;
    } catch (Exception exception) {
      Log.w("DocumentsContract", "Failed to create document", exception);
      rethrowIfNecessary(exception);
      return false;
    } 
  }
  
  public static Uri renameDocument(ContentResolver paramContentResolver, Uri paramUri, String paramString) throws FileNotFoundException {
    try {
      Bundle bundle2 = new Bundle();
      this();
      bundle2.putParcelable("uri", paramUri);
      bundle2.putString("_display_name", paramString);
      Bundle bundle1 = paramContentResolver.call(paramUri.getAuthority(), "android:renameDocument", null, bundle2);
      Uri uri = bundle1.<Uri>getParcelable("uri");
      if (uri == null)
        uri = paramUri; 
      return uri;
    } catch (Exception exception) {
      Log.w("DocumentsContract", "Failed to rename document", exception);
      rethrowIfNecessary(exception);
      return null;
    } 
  }
  
  public static boolean deleteDocument(ContentResolver paramContentResolver, Uri paramUri) throws FileNotFoundException {
    try {
      Bundle bundle = new Bundle();
      this();
      bundle.putParcelable("uri", paramUri);
      paramContentResolver.call(paramUri.getAuthority(), "android:deleteDocument", null, bundle);
      return true;
    } catch (Exception exception) {
      Log.w("DocumentsContract", "Failed to delete document", exception);
      rethrowIfNecessary(exception);
      return false;
    } 
  }
  
  public static Uri copyDocument(ContentResolver paramContentResolver, Uri paramUri1, Uri paramUri2) throws FileNotFoundException {
    try {
      Bundle bundle2 = new Bundle();
      this();
      bundle2.putParcelable("uri", paramUri1);
      bundle2.putParcelable("android.content.extra.TARGET_URI", paramUri2);
      Bundle bundle1 = paramContentResolver.call(paramUri1.getAuthority(), "android:copyDocument", null, bundle2);
      return bundle1.<Uri>getParcelable("uri");
    } catch (Exception exception) {
      Log.w("DocumentsContract", "Failed to copy document", exception);
      rethrowIfNecessary(exception);
      return null;
    } 
  }
  
  public static Uri moveDocument(ContentResolver paramContentResolver, Uri paramUri1, Uri paramUri2, Uri paramUri3) throws FileNotFoundException {
    try {
      Bundle bundle2 = new Bundle();
      this();
      bundle2.putParcelable("uri", paramUri1);
      bundle2.putParcelable("parentUri", paramUri2);
      bundle2.putParcelable("android.content.extra.TARGET_URI", paramUri3);
      Bundle bundle1 = paramContentResolver.call(paramUri1.getAuthority(), "android:moveDocument", null, bundle2);
      return bundle1.<Uri>getParcelable("uri");
    } catch (Exception exception) {
      Log.w("DocumentsContract", "Failed to move document", exception);
      rethrowIfNecessary(exception);
      return null;
    } 
  }
  
  public static boolean removeDocument(ContentResolver paramContentResolver, Uri paramUri1, Uri paramUri2) throws FileNotFoundException {
    try {
      Bundle bundle = new Bundle();
      this();
      bundle.putParcelable("uri", paramUri1);
      bundle.putParcelable("parentUri", paramUri2);
      paramContentResolver.call(paramUri1.getAuthority(), "android:removeDocument", null, bundle);
      return true;
    } catch (Exception exception) {
      Log.w("DocumentsContract", "Failed to remove document", exception);
      rethrowIfNecessary(exception);
      return false;
    } 
  }
  
  public static void ejectRoot(ContentResolver paramContentResolver, Uri paramUri) {
    try {
      Bundle bundle = new Bundle();
      this();
      bundle.putParcelable("uri", paramUri);
      paramContentResolver.call(paramUri.getAuthority(), "android:ejectRoot", null, bundle);
    } catch (Exception exception) {
      Log.w("DocumentsContract", "Failed to eject", exception);
    } 
  }
  
  public static Bundle getDocumentMetadata(ContentResolver paramContentResolver, Uri paramUri) throws FileNotFoundException {
    Preconditions.checkNotNull(paramContentResolver, "content can not be null");
    Preconditions.checkNotNull(paramUri, "documentUri can not be null");
    try {
      Bundle bundle = new Bundle();
      this();
      bundle.putParcelable("uri", paramUri);
      return paramContentResolver.call(paramUri.getAuthority(), "android:getDocumentMetadata", null, bundle);
    } catch (Exception exception) {
      Log.w("DocumentsContract", "Failed to get document metadata");
      rethrowIfNecessary(exception);
      return null;
    } 
  }
  
  public static Path findDocumentPath(ContentResolver paramContentResolver, Uri paramUri) throws FileNotFoundException {
    try {
      Bundle bundle2 = new Bundle();
      this();
      bundle2.putParcelable("uri", paramUri);
      Bundle bundle1 = paramContentResolver.call(paramUri.getAuthority(), "android:findDocumentPath", null, bundle2);
      return bundle1.<Path>getParcelable("result");
    } catch (Exception exception) {
      Log.w("DocumentsContract", "Failed to find path", exception);
      rethrowIfNecessary(exception);
      return null;
    } 
  }
  
  public static IntentSender createWebLinkIntent(ContentResolver paramContentResolver, Uri paramUri, Bundle paramBundle) throws FileNotFoundException {
    try {
      Bundle bundle2 = new Bundle();
      this();
      bundle2.putParcelable("uri", paramUri);
      if (paramBundle != null)
        bundle2.putBundle("options", paramBundle); 
      Bundle bundle1 = paramContentResolver.call(paramUri.getAuthority(), "android:createWebLinkIntent", null, bundle2);
      return bundle1.<IntentSender>getParcelable("result");
    } catch (Exception exception) {
      Log.w("DocumentsContract", "Failed to create a web link intent", exception);
      rethrowIfNecessary(exception);
      return null;
    } 
  }
  
  public static AssetFileDescriptor openImageThumbnail(File paramFile) throws FileNotFoundException {
    ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.open(paramFile, 268435456);
    try {
      ExifInterface exifInterface = new ExifInterface();
      this(paramFile.getAbsolutePath());
      long[] arrayOfLong = exifInterface.getThumbnailRange();
      if (arrayOfLong != null) {
        Bundle bundle;
        int i = exifInterface.getAttributeInt("Orientation", -1);
        if (i != 3) {
          if (i != 6) {
            if (i != 8) {
              paramFile = null;
            } else {
              bundle = new Bundle();
              this(1);
              bundle.putInt("android.provider.extra.ORIENTATION", 270);
            } 
          } else {
            bundle = new Bundle();
            this(1);
            bundle.putInt("android.provider.extra.ORIENTATION", 90);
          } 
        } else {
          bundle = new Bundle();
          this(1);
          bundle.putInt("android.provider.extra.ORIENTATION", 180);
        } 
        return new AssetFileDescriptor(parcelFileDescriptor, arrayOfLong[0], arrayOfLong[1], bundle);
      } 
    } catch (IOException iOException) {}
    return new AssetFileDescriptor(parcelFileDescriptor, 0L, -1L, null);
  }
  
  private static void rethrowIfNecessary(Exception paramException) throws FileNotFoundException {
    if (VMRuntime.getRuntime().getTargetSdkVersion() >= 26)
      if (paramException instanceof ParcelableException) {
        ((ParcelableException)paramException).maybeRethrow(FileNotFoundException.class);
      } else if (paramException instanceof RemoteException) {
        ((RemoteException)paramException).rethrowAsRuntimeException();
      } else if (paramException instanceof RuntimeException) {
        throw (RuntimeException)paramException;
      }  
  }
  
  class Path implements Parcelable {
    public Path(DocumentsContract this$0, List<String> param1List) {
      Preconditions.checkCollectionNotEmpty(param1List, "path");
      Preconditions.checkCollectionElementsNotNull(param1List, "path");
      this.mRootId = (String)this$0;
      this.mPath = param1List;
    }
    
    public String getRootId() {
      return this.mRootId;
    }
    
    public List<String> getPath() {
      return this.mPath;
    }
    
    public boolean equals(Object<String> param1Object) {
      null = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || !(param1Object instanceof Path))
        return false; 
      Path path = (Path)param1Object;
      if (Objects.equals(this.mRootId, path.mRootId)) {
        param1Object = (Object<String>)this.mPath;
        List<String> list = path.mPath;
        if (Objects.equals(param1Object, list))
          return null; 
      } 
      return false;
    }
    
    public int hashCode() {
      return Objects.hash(new Object[] { this.mRootId, this.mPath });
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("DocumentsContract.Path{");
      stringBuilder.append("rootId=");
      String str = this.mRootId;
      stringBuilder.append(str);
      stringBuilder.append(", path=");
      List<String> list = this.mPath;
      stringBuilder.append(list);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeString(this.mRootId);
      param1Parcel.writeStringList(this.mPath);
    }
    
    public int describeContents() {
      return 0;
    }
    
    public static final Parcelable.Creator<Path> CREATOR = new Parcelable.Creator<Path>() {
        public DocumentsContract.Path createFromParcel(Parcel param2Parcel) {
          String str = param2Parcel.readString();
          ArrayList<String> arrayList = param2Parcel.createStringArrayList();
          return new DocumentsContract.Path(str, arrayList);
        }
        
        public DocumentsContract.Path[] newArray(int param2Int) {
          return new DocumentsContract.Path[param2Int];
        }
      };
    
    private final List<String> mPath;
    
    private final String mRootId;
  }
}
