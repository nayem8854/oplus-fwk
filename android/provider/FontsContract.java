package android.provider;

import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.Signature;
import android.database.Cursor;
import android.graphics.Typeface;
import android.graphics.fonts.Font;
import android.graphics.fonts.FontFamily;
import android.graphics.fonts.FontStyle;
import android.graphics.fonts.FontVariationAxis;
import android.net.Uri;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.util.LruCache;
import com.android.internal.util.Preconditions;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class FontsContract {
  private static final long SYNC_FONT_FETCH_TIMEOUT_MS = 500L;
  
  private static final String TAG = "FontsContract";
  
  private static final int THREAD_RENEWAL_THRESHOLD_MS = 10000;
  
  private static final Comparator<byte[]> sByteArrayComparator;
  
  private static volatile Context sContext;
  
  private static Handler sHandler;
  
  private static Set<String> sInQueueSet;
  
  class Columns implements BaseColumns {
    public static final String FILE_ID = "file_id";
    
    public static final String ITALIC = "font_italic";
    
    public static final String RESULT_CODE = "result_code";
    
    public static final int RESULT_CODE_FONT_NOT_FOUND = 1;
    
    public static final int RESULT_CODE_FONT_UNAVAILABLE = 2;
    
    public static final int RESULT_CODE_MALFORMED_QUERY = 3;
    
    public static final int RESULT_CODE_OK = 0;
    
    public static final String TTC_INDEX = "font_ttc_index";
    
    public static final String VARIATION_SETTINGS = "font_variation_settings";
    
    public static final String WEIGHT = "font_weight";
  }
  
  private static final Object sLock = new Object();
  
  private static final Runnable sReplaceDispatcherThreadRunnable;
  
  private static HandlerThread sThread;
  
  private static final LruCache<String, Typeface> sTypefaceCache = new LruCache(16);
  
  public static void setApplicationContextForResources(Context paramContext) {
    sContext = paramContext.getApplicationContext();
  }
  
  public static class FontInfo {
    private final FontVariationAxis[] mAxes;
    
    private final boolean mItalic;
    
    private final int mResultCode;
    
    private final int mTtcIndex;
    
    private final Uri mUri;
    
    private final int mWeight;
    
    public FontInfo(Uri param1Uri, int param1Int1, FontVariationAxis[] param1ArrayOfFontVariationAxis, int param1Int2, boolean param1Boolean, int param1Int3) {
      this.mUri = (Uri)Preconditions.checkNotNull(param1Uri);
      this.mTtcIndex = param1Int1;
      this.mAxes = param1ArrayOfFontVariationAxis;
      this.mWeight = param1Int2;
      this.mItalic = param1Boolean;
      this.mResultCode = param1Int3;
    }
    
    public Uri getUri() {
      return this.mUri;
    }
    
    public int getTtcIndex() {
      return this.mTtcIndex;
    }
    
    public FontVariationAxis[] getAxes() {
      return this.mAxes;
    }
    
    public int getWeight() {
      return this.mWeight;
    }
    
    public boolean isItalic() {
      return this.mItalic;
    }
    
    public int getResultCode() {
      return this.mResultCode;
    }
  }
  
  public static class FontFamilyResult {
    public static final int STATUS_OK = 0;
    
    public static final int STATUS_REJECTED = 3;
    
    public static final int STATUS_UNEXPECTED_DATA_PROVIDED = 2;
    
    public static final int STATUS_WRONG_CERTIFICATES = 1;
    
    private final FontsContract.FontInfo[] mFonts;
    
    private final int mStatusCode;
    
    public FontFamilyResult(int param1Int, FontsContract.FontInfo[] param1ArrayOfFontInfo) {
      this.mStatusCode = param1Int;
      this.mFonts = param1ArrayOfFontInfo;
    }
    
    public int getStatusCode() {
      return this.mStatusCode;
    }
    
    public FontsContract.FontInfo[] getFonts() {
      return this.mFonts;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    static @interface FontResultStatus {}
  }
  
  static {
    sReplaceDispatcherThreadRunnable = new Runnable() {
        public void run() {
          synchronized (FontsContract.sLock) {
            if (FontsContract.sThread != null) {
              FontsContract.sThread.quitSafely();
              FontsContract.access$102(null);
              FontsContract.access$202(null);
            } 
            return;
          } 
        }
      };
    sByteArrayComparator = (Comparator<byte[]>)_$$Lambda$FontsContract$3FDNQd_WsglsyDhif_aHVbzkfrA.INSTANCE;
  }
  
  public static Typeface getFontSync(FontRequest paramFontRequest) {
    String str = paramFontRequest.getIdentifier();
    Typeface typeface = (Typeface)sTypefaceCache.get(str);
    if (typeface != null)
      return typeface; 
    Object object = sLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      Typeface typeface1 = (Typeface)sTypefaceCache.get(str);
      if (typeface1 != null) {
        try {
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          return typeface1;
        } finally {}
      } else {
        if (sHandler == null) {
          HandlerThread handlerThread = new HandlerThread();
          this("fonts", 10);
          sThread = handlerThread;
          handlerThread.start();
          Handler handler1 = new Handler();
          this(sThread.getLooper());
          sHandler = handler1;
        } 
        ReentrantLock reentrantLock = new ReentrantLock();
        this();
        Condition condition = reentrantLock.newCondition();
        AtomicReference<Typeface> atomicReference = new AtomicReference();
        this();
        AtomicBoolean atomicBoolean1 = new AtomicBoolean();
        this(true);
        AtomicBoolean atomicBoolean2 = new AtomicBoolean();
        this(false);
        Handler handler = sHandler;
        _$$Lambda$FontsContract$rqfIZKvP1frnI9vP1hVA8jQN_RE _$$Lambda$FontsContract$rqfIZKvP1frnI9vP1hVA8jQN_RE = new _$$Lambda$FontsContract$rqfIZKvP1frnI9vP1hVA8jQN_RE();
        this(paramFontRequest, str, atomicReference, reentrantLock, atomicBoolean2, atomicBoolean1, condition);
        handler.post(_$$Lambda$FontsContract$rqfIZKvP1frnI9vP1hVA8jQN_RE);
        sHandler.removeCallbacks(sReplaceDispatcherThreadRunnable);
        sHandler.postDelayed(sReplaceDispatcherThreadRunnable, 10000L);
        long l = TimeUnit.MILLISECONDS.toNanos(500L);
        reentrantLock.lock();
        try {
          if (!atomicBoolean1.get()) {
            Typeface typeface2 = atomicReference.get();
            reentrantLock.unlock();
            return typeface2;
          } 
          while (true) {
            Typeface typeface2;
            long l1;
            try {
              l1 = condition.awaitNanos(l);
            } catch (InterruptedException interruptedException) {
              l1 = l;
            } 
            if (!atomicBoolean1.get()) {
              typeface2 = atomicReference.get();
              reentrantLock.unlock();
              return typeface2;
            } 
            l = l1;
            if (l1 <= 0L) {
              atomicBoolean2.set(true);
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Remote font fetch timed out: ");
              stringBuilder.append(typeface2.getProviderAuthority());
              stringBuilder.append("/");
              stringBuilder.append(typeface2.getQuery());
              String str1 = stringBuilder.toString();
              Log.w("FontsContract", str1);
              reentrantLock.unlock();
              return null;
            } 
          } 
        } finally {
          reentrantLock.unlock();
        } 
      } 
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    throw paramFontRequest;
  }
  
  public static class FontRequestCallback {
    public static final int FAIL_REASON_FONT_LOAD_ERROR = -3;
    
    public static final int FAIL_REASON_FONT_NOT_FOUND = 1;
    
    public static final int FAIL_REASON_FONT_UNAVAILABLE = 2;
    
    public static final int FAIL_REASON_MALFORMED_QUERY = 3;
    
    public static final int FAIL_REASON_PROVIDER_NOT_FOUND = -1;
    
    public static final int FAIL_REASON_WRONG_CERTIFICATES = -2;
    
    public void onTypefaceRetrieved(Typeface param1Typeface) {}
    
    public void onTypefaceRequestFailed(int param1Int) {}
    
    @Retention(RetentionPolicy.SOURCE)
    static @interface FontRequestFailReason {}
  }
  
  public static void requestFonts(Context paramContext, FontRequest paramFontRequest, Handler paramHandler, CancellationSignal paramCancellationSignal, FontRequestCallback paramFontRequestCallback) {
    Handler handler = new Handler();
    Typeface typeface = (Typeface)sTypefaceCache.get(paramFontRequest.getIdentifier());
    if (typeface != null) {
      handler.post(new _$$Lambda$FontsContract$p_tsXYYYpEH0_EJSp2uPrJ33dkU(paramFontRequestCallback, typeface));
      return;
    } 
    paramHandler.post(new _$$Lambda$FontsContract$dFs2m4XF5xdir4W3T_ncUQAVX8k(paramContext, paramCancellationSignal, paramFontRequest, handler, paramFontRequestCallback));
  }
  
  public static FontFamilyResult fetchFonts(Context paramContext, CancellationSignal paramCancellationSignal, FontRequest paramFontRequest) throws PackageManager.NameNotFoundException {
    if (paramContext.isRestricted())
      return new FontFamilyResult(3, null); 
    ProviderInfo providerInfo = getProvider(paramContext.getPackageManager(), paramFontRequest);
    if (providerInfo == null)
      return new FontFamilyResult(1, null); 
    try {
      FontInfo[] arrayOfFontInfo = getFontFromProvider(paramContext, paramFontRequest, providerInfo.authority, paramCancellationSignal);
      return new FontFamilyResult(0, arrayOfFontInfo);
    } catch (IllegalArgumentException illegalArgumentException) {
      return new FontFamilyResult(2, null);
    } 
  }
  
  public static Typeface buildTypeface(Context paramContext, CancellationSignal paramCancellationSignal, FontInfo[] paramArrayOfFontInfo) {
    if (paramContext.isRestricted())
      return null; 
    Map<Uri, ByteBuffer> map = prepareFontData(paramContext, paramArrayOfFontInfo, paramCancellationSignal);
    if (map.isEmpty())
      return null; 
    paramContext = null;
    int i = paramArrayOfFontInfo.length;
    byte b = 0;
    while (true) {
      if (b < i) {
        FontInfo fontInfo = paramArrayOfFontInfo[b];
        ByteBuffer byteBuffer = map.get(fontInfo.getUri());
        if (byteBuffer != null) {
          try {
            FontFamily.Builder builder;
            boolean bool;
            Font.Builder builder3 = new Font.Builder();
            this(byteBuffer);
            Font.Builder builder2 = builder3.setWeight(fontInfo.getWeight());
            if (fontInfo.isItalic()) {
              bool = true;
            } else {
              bool = false;
            } 
            builder2 = builder2.setSlant(bool);
            builder2 = builder2.setTtcIndex(fontInfo.getTtcIndex());
            Font.Builder builder1 = builder2.setFontVariationSettings(fontInfo.getAxes());
            Font font1 = builder1.build();
            if (paramContext == null) {
              FontFamily.Builder builder4 = new FontFamily.Builder();
              this(font1);
              builder = builder4;
            } else {
              builder.addFont(font1);
            } 
            b++;
          } catch (IllegalArgumentException illegalArgumentException) {
            return null;
          } catch (IOException iOException) {
            b++;
          } 
          continue;
        } 
      } else {
        break;
      } 
      b++;
    } 
    if (illegalArgumentException == null)
      return null; 
    FontFamily fontFamily = illegalArgumentException.build();
    FontStyle fontStyle = new FontStyle(400, 0);
    Font font = fontFamily.getFont(0);
    int j = fontStyle.getMatchScore(font.getStyle());
    for (b = 1; b < fontFamily.getSize(); b++, j = i) {
      Font font1 = fontFamily.getFont(b);
      int k = fontStyle.getMatchScore(font1.getStyle());
      i = j;
      if (k < j) {
        font = font1;
        i = k;
      } 
    } 
    return (new Typeface.CustomFallbackBuilder(fontFamily)).setStyle(font.getStyle()).build();
  }
  
  private static Map<Uri, ByteBuffer> prepareFontData(Context paramContext, FontInfo[] paramArrayOfFontInfo, CancellationSignal paramCancellationSignal) {
    HashMap<Object, Object> hashMap = new HashMap<>();
    ContentResolver contentResolver = paramContext.getContentResolver();
    int i;
    byte b;
    for (i = paramArrayOfFontInfo.length, b = 0; b < i; ) {
      FontInfo fontInfo = paramArrayOfFontInfo[b];
      if (fontInfo.getResultCode() == 0) {
        Uri uri = fontInfo.getUri();
        if (!hashMap.containsKey(uri)) {
          FontInfo fontInfo1, fontInfo2;
          MappedByteBuffer mappedByteBuffer1 = null;
          fontInfo = null;
          MappedByteBuffer mappedByteBuffer2 = null, mappedByteBuffer3 = null, mappedByteBuffer4 = null;
          try {
            ParcelFileDescriptor parcelFileDescriptor = contentResolver.openFileDescriptor(uri, "r", paramCancellationSignal);
            if (parcelFileDescriptor != null) {
              mappedByteBuffer3 = mappedByteBuffer4;
              mappedByteBuffer2 = mappedByteBuffer1;
              try {
                FileInputStream fileInputStream = new FileInputStream();
                mappedByteBuffer3 = mappedByteBuffer4;
                mappedByteBuffer2 = mappedByteBuffer1;
              } catch (IOException iOException) {
              
              } finally {
                if (parcelFileDescriptor != null)
                  try {
                    parcelFileDescriptor.close();
                  } finally {
                    mappedByteBuffer1 = null;
                    mappedByteBuffer2 = mappedByteBuffer3;
                  }  
                mappedByteBuffer2 = mappedByteBuffer3;
              } 
            } 
            if (parcelFileDescriptor != null) {
              fontInfo2 = fontInfo;
              parcelFileDescriptor.close();
            } 
          } catch (IOException iOException) {
            fontInfo1 = fontInfo2;
          } 
          hashMap.put(uri, fontInfo1);
        } 
      } 
      b++;
    } 
    return (Map)Collections.unmodifiableMap(hashMap);
  }
  
  public static ProviderInfo getProvider(PackageManager paramPackageManager, FontRequest paramFontRequest) throws PackageManager.NameNotFoundException {
    ArrayList<byte> arrayList;
    String str = paramFontRequest.getProviderAuthority();
    ProviderInfo providerInfo = paramPackageManager.resolveContentProvider(str, 0);
    if (providerInfo != null) {
      List<List<byte[]>> list;
      if (providerInfo.packageName.equals(paramFontRequest.getProviderPackage())) {
        if (providerInfo.applicationInfo.isSystemApp())
          return providerInfo; 
        PackageInfo packageInfo = paramPackageManager.getPackageInfo(providerInfo.packageName, 64);
        List<byte[]> list1 = convertToByteArrayList(packageInfo.signatures);
        Collections.sort((List)list1, (Comparator)sByteArrayComparator);
        list = paramFontRequest.getCertificates();
        for (byte b = 0; b < list.size(); b++) {
          arrayList = new ArrayList(list.get(b));
          Collections.sort(arrayList, (Comparator)sByteArrayComparator);
          if (equalsByteArrayList(list1, (List)arrayList))
            return providerInfo; 
        } 
        return null;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Found content provider ");
      stringBuilder1.append((String)arrayList);
      stringBuilder1.append(", but package was not ");
      stringBuilder1.append(list.getProviderPackage());
      throw new PackageManager.NameNotFoundException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("No package found for authority: ");
    stringBuilder.append((String)arrayList);
    throw new PackageManager.NameNotFoundException(stringBuilder.toString());
  }
  
  private static boolean equalsByteArrayList(List<byte[]> paramList1, List<byte[]> paramList2) {
    if (paramList1.size() != paramList2.size())
      return false; 
    for (byte b = 0; b < paramList1.size(); b++) {
      if (!Arrays.equals(paramList1.get(b), paramList2.get(b)))
        return false; 
    } 
    return true;
  }
  
  private static List<byte[]> convertToByteArrayList(Signature[] paramArrayOfSignature) {
    ArrayList<byte[]> arrayList = new ArrayList();
    for (byte b = 0; b < paramArrayOfSignature.length; b++)
      arrayList.add(paramArrayOfSignature[b].toByteArray()); 
    return (List<byte[]>)arrayList;
  }
  
  public static FontInfo[] getFontFromProvider(Context paramContext, FontRequest paramFontRequest, String paramString, CancellationSignal paramCancellationSignal) {
    ArrayList<FontInfo> arrayList2 = new ArrayList();
    Uri.Builder builder2 = (new Uri.Builder()).scheme("content");
    builder2 = builder2.authority(paramString);
    Uri uri1 = builder2.build();
    Uri.Builder builder3 = (new Uri.Builder()).scheme("content");
    Uri.Builder builder1 = builder3.authority(paramString);
    builder1 = builder1.appendPath("file");
    Uri uri2 = builder1.build();
    ContentResolver contentResolver = paramContext.getContentResolver();
    String str = paramFontRequest.getQuery();
    Cursor cursor = contentResolver.query(uri1, new String[] { "_id", "file_id", "font_ttc_index", "font_variation_settings", "font_weight", "font_italic", "result_code" }, "query = ?", new String[] { str }, null, paramCancellationSignal);
    ArrayList<FontInfo> arrayList1 = arrayList2;
    if (cursor != null)
      arrayList1 = arrayList2; 
    if (cursor != null)
      cursor.close(); 
    return arrayList1.<FontInfo>toArray(new FontInfo[0]);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  static @interface FontResultStatus {}
  
  @Retention(RetentionPolicy.SOURCE)
  static @interface FontRequestFailReason {}
}
