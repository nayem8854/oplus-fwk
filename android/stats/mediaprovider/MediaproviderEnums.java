package android.stats.mediaprovider;

public final class MediaproviderEnums {
  public static final int EXTERNAL_OTHER = 3;
  
  public static final int EXTERNAL_PRIMARY = 2;
  
  public static final int INTERNAL = 1;
  
  public static final int UNKNOWN = 0;
}
