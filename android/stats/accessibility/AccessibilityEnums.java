package android.stats.accessibility;

public final class AccessibilityEnums {
  public static final int A11Y_BUTTON = 1;
  
  public static final int A11Y_BUTTON_LONG_PRESS = 4;
  
  public static final int DISABLED = 2;
  
  public static final int ENABLED = 1;
  
  public static final int TRIPLE_TAP = 3;
  
  public static final int UNKNOWN = 0;
  
  public static final int UNKNOWN_TYPE = 0;
  
  public static final int VOLUME_KEY = 2;
}
