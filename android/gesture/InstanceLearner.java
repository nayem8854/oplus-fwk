package android.gesture;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeMap;

class InstanceLearner extends Learner {
  private static final Comparator<Prediction> sComparator = (Comparator<Prediction>)new Object();
  
  ArrayList<Prediction> classify(int paramInt1, int paramInt2, float[] paramArrayOffloat) {
    ArrayList<Prediction> arrayList = new ArrayList();
    ArrayList<Instance> arrayList1 = getInstances();
    int i = arrayList1.size();
    TreeMap<Object, Object> treeMap = new TreeMap<>();
    for (byte b = 0; b < i; b++) {
      Instance instance = arrayList1.get(b);
      if (instance.vector.length == paramArrayOffloat.length) {
        double d;
        if (paramInt1 == 2) {
          d = GestureUtils.minimumCosineDistance(instance.vector, paramArrayOffloat, paramInt2);
        } else {
          d = GestureUtils.squaredEuclideanDistance(instance.vector, paramArrayOffloat);
        } 
        if (d == 0.0D) {
          d = Double.MAX_VALUE;
        } else {
          d = 1.0D / d;
        } 
        Double double_ = (Double)treeMap.get(instance.label);
        if (double_ == null || d > double_.doubleValue())
          treeMap.put(instance.label, Double.valueOf(d)); 
      } 
    } 
    for (String str : treeMap.keySet()) {
      double d = ((Double)treeMap.get(str)).doubleValue();
      arrayList.add(new Prediction(str, d));
    } 
    Collections.sort(arrayList, sComparator);
    return arrayList;
  }
}
