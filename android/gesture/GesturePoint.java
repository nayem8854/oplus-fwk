package android.gesture;

import java.io.DataInputStream;
import java.io.IOException;

public class GesturePoint {
  public final long timestamp;
  
  public final float x;
  
  public final float y;
  
  public GesturePoint(float paramFloat1, float paramFloat2, long paramLong) {
    this.x = paramFloat1;
    this.y = paramFloat2;
    this.timestamp = paramLong;
  }
  
  static GesturePoint deserialize(DataInputStream paramDataInputStream) throws IOException {
    float f1 = paramDataInputStream.readFloat();
    float f2 = paramDataInputStream.readFloat();
    long l = paramDataInputStream.readLong();
    return new GesturePoint(f1, f2, l);
  }
  
  public Object clone() {
    return new GesturePoint(this.x, this.y, this.timestamp);
  }
}
