package android.gesture;

import java.util.ArrayList;

abstract class Learner {
  private final ArrayList<Instance> mInstances = new ArrayList<>();
  
  void addInstance(Instance paramInstance) {
    this.mInstances.add(paramInstance);
  }
  
  abstract ArrayList<Prediction> classify(int paramInt1, int paramInt2, float[] paramArrayOffloat);
  
  ArrayList<Instance> getInstances() {
    return this.mInstances;
  }
  
  void removeInstance(long paramLong) {
    ArrayList<Instance> arrayList = this.mInstances;
    int i = arrayList.size();
    for (byte b = 0; b < i; b++) {
      Instance instance = arrayList.get(b);
      if (paramLong == instance.id) {
        arrayList.remove(instance);
        return;
      } 
    } 
  }
  
  void removeInstances(String paramString) {
    // Byte code:
    //   0: new java/util/ArrayList
    //   3: dup
    //   4: invokespecial <init> : ()V
    //   7: astore_2
    //   8: aload_0
    //   9: getfield mInstances : Ljava/util/ArrayList;
    //   12: astore_3
    //   13: aload_3
    //   14: invokevirtual size : ()I
    //   17: istore #4
    //   19: iconst_0
    //   20: istore #5
    //   22: iload #5
    //   24: iload #4
    //   26: if_icmpge -> 89
    //   29: aload_3
    //   30: iload #5
    //   32: invokevirtual get : (I)Ljava/lang/Object;
    //   35: checkcast android/gesture/Instance
    //   38: astore #6
    //   40: aload #6
    //   42: getfield label : Ljava/lang/String;
    //   45: ifnonnull -> 52
    //   48: aload_1
    //   49: ifnull -> 76
    //   52: aload #6
    //   54: getfield label : Ljava/lang/String;
    //   57: ifnull -> 83
    //   60: aload #6
    //   62: getfield label : Ljava/lang/String;
    //   65: astore #7
    //   67: aload #7
    //   69: aload_1
    //   70: invokevirtual equals : (Ljava/lang/Object;)Z
    //   73: ifeq -> 83
    //   76: aload_2
    //   77: aload #6
    //   79: invokevirtual add : (Ljava/lang/Object;)Z
    //   82: pop
    //   83: iinc #5, 1
    //   86: goto -> 22
    //   89: aload_3
    //   90: aload_2
    //   91: invokevirtual removeAll : (Ljava/util/Collection;)Z
    //   94: pop
    //   95: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #68	-> 0
    //   #69	-> 8
    //   #70	-> 13
    //   #72	-> 19
    //   #73	-> 29
    //   #75	-> 40
    //   #76	-> 67
    //   #77	-> 76
    //   #72	-> 83
    //   #80	-> 89
    //   #81	-> 95
  }
}
