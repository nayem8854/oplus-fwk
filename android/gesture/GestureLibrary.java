package android.gesture;

import java.util.ArrayList;
import java.util.Set;

public abstract class GestureLibrary {
  protected final GestureStore mStore = new GestureStore();
  
  public boolean isReadOnly() {
    return false;
  }
  
  public Learner getLearner() {
    return this.mStore.getLearner();
  }
  
  public void setOrientationStyle(int paramInt) {
    this.mStore.setOrientationStyle(paramInt);
  }
  
  public int getOrientationStyle() {
    return this.mStore.getOrientationStyle();
  }
  
  public void setSequenceType(int paramInt) {
    this.mStore.setSequenceType(paramInt);
  }
  
  public int getSequenceType() {
    return this.mStore.getSequenceType();
  }
  
  public Set<String> getGestureEntries() {
    return this.mStore.getGestureEntries();
  }
  
  public ArrayList<Prediction> recognize(Gesture paramGesture) {
    return this.mStore.recognize(paramGesture);
  }
  
  public void addGesture(String paramString, Gesture paramGesture) {
    this.mStore.addGesture(paramString, paramGesture);
  }
  
  public void removeGesture(String paramString, Gesture paramGesture) {
    this.mStore.removeGesture(paramString, paramGesture);
  }
  
  public void removeEntry(String paramString) {
    this.mStore.removeEntry(paramString);
  }
  
  public ArrayList<Gesture> getGestures(String paramString) {
    return this.mStore.getGestures(paramString);
  }
  
  public abstract boolean load();
  
  public abstract boolean save();
}
