package android.gesture;

public class Prediction {
  public final String name;
  
  public double score;
  
  Prediction(String paramString, double paramDouble) {
    this.name = paramString;
    this.score = paramDouble;
  }
  
  public String toString() {
    return this.name;
  }
}
