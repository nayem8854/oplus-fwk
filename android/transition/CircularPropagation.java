package android.transition;

import android.graphics.Rect;
import android.view.ViewGroup;

public class CircularPropagation extends VisibilityPropagation {
  private static final String TAG = "CircularPropagation";
  
  private float mPropagationSpeed = 3.0F;
  
  public void setPropagationSpeed(float paramFloat) {
    if (paramFloat != 0.0F) {
      this.mPropagationSpeed = paramFloat;
      return;
    } 
    throw new IllegalArgumentException("propagationSpeed may not be 0");
  }
  
  public long getStartDelay(ViewGroup paramViewGroup, Transition paramTransition, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    int k, m;
    if (paramTransitionValues1 == null && paramTransitionValues2 == null)
      return 0L; 
    byte b = 1;
    if (paramTransitionValues2 == null || getViewVisibility(paramTransitionValues1) == 0) {
      b = -1;
    } else {
      paramTransitionValues1 = paramTransitionValues2;
    } 
    int i = getViewX(paramTransitionValues1);
    int j = getViewY(paramTransitionValues1);
    Rect rect = paramTransition.getEpicenter();
    if (rect != null) {
      k = rect.centerX();
      m = rect.centerY();
    } else {
      int[] arrayOfInt = new int[2];
      paramViewGroup.getLocationOnScreen(arrayOfInt);
      float f1 = (arrayOfInt[0] + paramViewGroup.getWidth() / 2);
      float f2 = paramViewGroup.getTranslationX();
      k = Math.round(f1 + f2);
      f1 = (arrayOfInt[1] + paramViewGroup.getHeight() / 2);
      f2 = paramViewGroup.getTranslationY();
      m = Math.round(f1 + f2);
    } 
    double d1 = distance(i, j, k, m);
    double d2 = distance(0.0F, 0.0F, paramViewGroup.getWidth(), paramViewGroup.getHeight());
    d2 = d1 / d2;
    long l1 = paramTransition.getDuration();
    long l2 = l1;
    if (l1 < 0L)
      l2 = 300L; 
    return Math.round(((float)(b * l2) / this.mPropagationSpeed) * d2);
  }
  
  private static double distance(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    double d1 = (paramFloat3 - paramFloat1);
    double d2 = (paramFloat4 - paramFloat2);
    return Math.hypot(d1, d2);
  }
}
