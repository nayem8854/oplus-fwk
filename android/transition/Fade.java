package android.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.android.internal.R;
import java.util.Map;

public class Fade extends Visibility {
  private static boolean DBG = false;
  
  public static final int IN = 1;
  
  private static final String LOG_TAG = "Fade";
  
  public static final int OUT = 2;
  
  static final String PROPNAME_TRANSITION_ALPHA = "android:fade:transitionAlpha";
  
  public Fade() {}
  
  public Fade(int paramInt) {
    setMode(paramInt);
  }
  
  public Fade(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Fade);
    int i = typedArray.getInt(0, getMode());
    setMode(i);
    typedArray.recycle();
  }
  
  public void captureStartValues(TransitionValues paramTransitionValues) {
    super.captureStartValues(paramTransitionValues);
    Map<String, Object> map = paramTransitionValues.values;
    View view = paramTransitionValues.view;
    float f = view.getTransitionAlpha();
    map.put("android:fade:transitionAlpha", Float.valueOf(f));
  }
  
  private Animator createAnimation(final View view, float paramFloat1, float paramFloat2) {
    if (paramFloat1 == paramFloat2)
      return null; 
    view.setTransitionAlpha(paramFloat1);
    ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view, "transitionAlpha", new float[] { paramFloat2 });
    if (DBG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Created animator ");
      stringBuilder.append(objectAnimator);
      Log.d("Fade", stringBuilder.toString());
    } 
    FadeAnimatorListener fadeAnimatorListener = new FadeAnimatorListener(view);
    objectAnimator.addListener((Animator.AnimatorListener)fadeAnimatorListener);
    addListener(new TransitionListenerAdapter() {
          final Fade this$0;
          
          final View val$view;
          
          public void onTransitionEnd(Transition param1Transition) {
            view.setTransitionAlpha(1.0F);
            param1Transition.removeListener(this);
          }
        });
    return (Animator)objectAnimator;
  }
  
  public Animator onAppear(ViewGroup paramViewGroup, View paramView, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    if (DBG) {
      if (paramTransitionValues1 != null) {
        View view = paramTransitionValues1.view;
      } else {
        paramViewGroup = null;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Fade.onAppear: startView, startVis, endView, endVis = ");
      stringBuilder.append(paramViewGroup);
      stringBuilder.append(", ");
      stringBuilder.append(paramView);
      Log.d("Fade", stringBuilder.toString());
    } 
    float f1 = getStartAlpha(paramTransitionValues1, 0.0F);
    float f2 = f1;
    if (f1 == 1.0F)
      f2 = 0.0F; 
    return createAnimation(paramView, f2, 1.0F);
  }
  
  public Animator onDisappear(ViewGroup paramViewGroup, View paramView, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    float f = getStartAlpha(paramTransitionValues1, 1.0F);
    return createAnimation(paramView, f, 0.0F);
  }
  
  private static float getStartAlpha(TransitionValues paramTransitionValues, float paramFloat) {
    float f = paramFloat;
    paramFloat = f;
    if (paramTransitionValues != null) {
      Float float_ = (Float)paramTransitionValues.values.get("android:fade:transitionAlpha");
      paramFloat = f;
      if (float_ != null)
        paramFloat = float_.floatValue(); 
    } 
    return paramFloat;
  }
  
  private static class FadeAnimatorListener extends AnimatorListenerAdapter {
    private boolean mLayerTypeChanged = false;
    
    private final View mView;
    
    public FadeAnimatorListener(View param1View) {
      this.mView = param1View;
    }
    
    public void onAnimationStart(Animator param1Animator) {
      if (this.mView.hasOverlappingRendering() && this.mView.getLayerType() == 0) {
        this.mLayerTypeChanged = true;
        this.mView.setLayerType(2, null);
      } 
    }
    
    public void onAnimationEnd(Animator param1Animator) {
      this.mView.setTransitionAlpha(1.0F);
      if (this.mLayerTypeChanged)
        this.mView.setLayerType(0, null); 
    }
  }
}
