package android.transition;

import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.android.internal.R;
import java.util.ArrayList;

public class TransitionSet extends Transition {
  ArrayList<Transition> mTransitions = new ArrayList<>();
  
  private boolean mPlayTogether = true;
  
  boolean mStarted = false;
  
  private int mChangeFlags = 0;
  
  static final int FLAG_CHANGE_EPICENTER = 8;
  
  private static final int FLAG_CHANGE_INTERPOLATOR = 1;
  
  private static final int FLAG_CHANGE_PATH_MOTION = 4;
  
  private static final int FLAG_CHANGE_PROPAGATION = 2;
  
  public static final int ORDERING_SEQUENTIAL = 1;
  
  public static final int ORDERING_TOGETHER = 0;
  
  int mCurrentListeners;
  
  public TransitionSet(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.TransitionSet);
    int i = typedArray.getInt(0, 0);
    setOrdering(i);
    typedArray.recycle();
  }
  
  public TransitionSet setOrdering(int paramInt) {
    if (paramInt != 0) {
      if (paramInt == 1) {
        this.mPlayTogether = false;
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid parameter for TransitionSet ordering: ");
        stringBuilder.append(paramInt);
        throw new AndroidRuntimeException(stringBuilder.toString());
      } 
    } else {
      this.mPlayTogether = true;
    } 
    return this;
  }
  
  public int getOrdering() {
    return this.mPlayTogether ^ true;
  }
  
  public TransitionSet addTransition(Transition paramTransition) {
    if (paramTransition != null) {
      addTransitionInternal(paramTransition);
      if (this.mDuration >= 0L)
        paramTransition.setDuration(this.mDuration); 
      if ((this.mChangeFlags & 0x1) != 0)
        paramTransition.setInterpolator(getInterpolator()); 
      if ((this.mChangeFlags & 0x2) != 0)
        paramTransition.setPropagation(getPropagation()); 
      if ((this.mChangeFlags & 0x4) != 0)
        paramTransition.setPathMotion(getPathMotion()); 
      if ((this.mChangeFlags & 0x8) != 0)
        paramTransition.setEpicenterCallback(getEpicenterCallback()); 
    } 
    return this;
  }
  
  private void addTransitionInternal(Transition paramTransition) {
    this.mTransitions.add(paramTransition);
    paramTransition.mParent = this;
  }
  
  public int getTransitionCount() {
    return this.mTransitions.size();
  }
  
  public Transition getTransitionAt(int paramInt) {
    if (paramInt < 0 || paramInt >= this.mTransitions.size())
      return null; 
    return this.mTransitions.get(paramInt);
  }
  
  public TransitionSet setDuration(long paramLong) {
    super.setDuration(paramLong);
    if (this.mDuration >= 0L) {
      ArrayList<Transition> arrayList = this.mTransitions;
      if (arrayList != null) {
        int i = arrayList.size();
        for (byte b = 0; b < i; b++)
          ((Transition)this.mTransitions.get(b)).setDuration(paramLong); 
      } 
    } 
    return this;
  }
  
  public TransitionSet setStartDelay(long paramLong) {
    return (TransitionSet)super.setStartDelay(paramLong);
  }
  
  public TransitionSet setInterpolator(TimeInterpolator paramTimeInterpolator) {
    this.mChangeFlags |= 0x1;
    ArrayList<Transition> arrayList = this.mTransitions;
    if (arrayList != null) {
      int i = arrayList.size();
      for (byte b = 0; b < i; b++)
        ((Transition)this.mTransitions.get(b)).setInterpolator(paramTimeInterpolator); 
    } 
    return (TransitionSet)super.setInterpolator(paramTimeInterpolator);
  }
  
  public TransitionSet addTarget(View paramView) {
    for (byte b = 0; b < this.mTransitions.size(); b++)
      ((Transition)this.mTransitions.get(b)).addTarget(paramView); 
    return (TransitionSet)super.addTarget(paramView);
  }
  
  public TransitionSet addTarget(int paramInt) {
    for (byte b = 0; b < this.mTransitions.size(); b++)
      ((Transition)this.mTransitions.get(b)).addTarget(paramInt); 
    return (TransitionSet)super.addTarget(paramInt);
  }
  
  public TransitionSet addTarget(String paramString) {
    for (byte b = 0; b < this.mTransitions.size(); b++)
      ((Transition)this.mTransitions.get(b)).addTarget(paramString); 
    return (TransitionSet)super.addTarget(paramString);
  }
  
  public TransitionSet addTarget(Class paramClass) {
    for (byte b = 0; b < this.mTransitions.size(); b++)
      ((Transition)this.mTransitions.get(b)).addTarget(paramClass); 
    return (TransitionSet)super.addTarget(paramClass);
  }
  
  public TransitionSet addListener(Transition.TransitionListener paramTransitionListener) {
    return (TransitionSet)super.addListener(paramTransitionListener);
  }
  
  public TransitionSet removeTarget(int paramInt) {
    for (byte b = 0; b < this.mTransitions.size(); b++)
      ((Transition)this.mTransitions.get(b)).removeTarget(paramInt); 
    return (TransitionSet)super.removeTarget(paramInt);
  }
  
  public TransitionSet removeTarget(View paramView) {
    for (byte b = 0; b < this.mTransitions.size(); b++)
      ((Transition)this.mTransitions.get(b)).removeTarget(paramView); 
    return (TransitionSet)super.removeTarget(paramView);
  }
  
  public TransitionSet removeTarget(Class paramClass) {
    for (byte b = 0; b < this.mTransitions.size(); b++)
      ((Transition)this.mTransitions.get(b)).removeTarget(paramClass); 
    return (TransitionSet)super.removeTarget(paramClass);
  }
  
  public TransitionSet removeTarget(String paramString) {
    for (byte b = 0; b < this.mTransitions.size(); b++)
      ((Transition)this.mTransitions.get(b)).removeTarget(paramString); 
    return (TransitionSet)super.removeTarget(paramString);
  }
  
  public Transition excludeTarget(View paramView, boolean paramBoolean) {
    for (byte b = 0; b < this.mTransitions.size(); b++)
      ((Transition)this.mTransitions.get(b)).excludeTarget(paramView, paramBoolean); 
    return super.excludeTarget(paramView, paramBoolean);
  }
  
  public Transition excludeTarget(String paramString, boolean paramBoolean) {
    for (byte b = 0; b < this.mTransitions.size(); b++)
      ((Transition)this.mTransitions.get(b)).excludeTarget(paramString, paramBoolean); 
    return super.excludeTarget(paramString, paramBoolean);
  }
  
  public Transition excludeTarget(int paramInt, boolean paramBoolean) {
    for (byte b = 0; b < this.mTransitions.size(); b++)
      ((Transition)this.mTransitions.get(b)).excludeTarget(paramInt, paramBoolean); 
    return super.excludeTarget(paramInt, paramBoolean);
  }
  
  public Transition excludeTarget(Class paramClass, boolean paramBoolean) {
    for (byte b = 0; b < this.mTransitions.size(); b++)
      ((Transition)this.mTransitions.get(b)).excludeTarget(paramClass, paramBoolean); 
    return super.excludeTarget(paramClass, paramBoolean);
  }
  
  public TransitionSet removeListener(Transition.TransitionListener paramTransitionListener) {
    return (TransitionSet)super.removeListener(paramTransitionListener);
  }
  
  public void setPathMotion(PathMotion paramPathMotion) {
    super.setPathMotion(paramPathMotion);
    this.mChangeFlags |= 0x4;
    if (this.mTransitions != null)
      for (byte b = 0; b < this.mTransitions.size(); b++)
        ((Transition)this.mTransitions.get(b)).setPathMotion(paramPathMotion);  
  }
  
  public TransitionSet removeTransition(Transition paramTransition) {
    this.mTransitions.remove(paramTransition);
    paramTransition.mParent = null;
    return this;
  }
  
  private void setupStartEndListeners() {
    TransitionSetListener transitionSetListener = new TransitionSetListener(this);
    for (Transition transition : this.mTransitions)
      transition.addListener(transitionSetListener); 
    this.mCurrentListeners = this.mTransitions.size();
  }
  
  class TransitionSetListener extends TransitionListenerAdapter {
    TransitionSet mTransitionSet;
    
    TransitionSetListener(TransitionSet this$0) {
      this.mTransitionSet = this$0;
    }
    
    public void onTransitionStart(Transition param1Transition) {
      if (!this.mTransitionSet.mStarted) {
        this.mTransitionSet.start();
        this.mTransitionSet.mStarted = true;
      } 
    }
    
    public void onTransitionEnd(Transition param1Transition) {
      TransitionSet transitionSet = this.mTransitionSet;
      transitionSet.mCurrentListeners--;
      if (this.mTransitionSet.mCurrentListeners == 0) {
        this.mTransitionSet.mStarted = false;
        this.mTransitionSet.end();
      } 
      param1Transition.removeListener(this);
    }
  }
  
  protected void createAnimators(ViewGroup paramViewGroup, TransitionValuesMaps paramTransitionValuesMaps1, TransitionValuesMaps paramTransitionValuesMaps2, ArrayList<TransitionValues> paramArrayList1, ArrayList<TransitionValues> paramArrayList2) {
    long l = getStartDelay();
    int i = this.mTransitions.size();
    for (byte b = 0; b < i; b++) {
      Transition transition = this.mTransitions.get(b);
      if (l > 0L && (this.mPlayTogether || b == 0)) {
        long l1 = transition.getStartDelay();
        if (l1 > 0L) {
          transition.setStartDelay(l + l1);
        } else {
          transition.setStartDelay(l);
        } 
      } 
      transition.createAnimators(paramViewGroup, paramTransitionValuesMaps1, paramTransitionValuesMaps2, paramArrayList1, paramArrayList2);
    } 
  }
  
  protected void runAnimators() {
    if (this.mTransitions.isEmpty()) {
      start();
      end();
      return;
    } 
    setupStartEndListeners();
    int i = this.mTransitions.size();
    if (!this.mPlayTogether) {
      for (byte b = 1; b < i; b++) {
        Transition transition1 = this.mTransitions.get(b - 1);
        Transition transition2 = this.mTransitions.get(b);
        transition1.addListener((Transition.TransitionListener)new Object(this, transition2));
      } 
      Transition transition = this.mTransitions.get(0);
      if (transition != null)
        transition.runAnimators(); 
    } else {
      for (byte b = 0; b < i; b++)
        ((Transition)this.mTransitions.get(b)).runAnimators(); 
    } 
  }
  
  public void captureStartValues(TransitionValues paramTransitionValues) {
    if (isValidTarget(paramTransitionValues.view))
      for (Transition transition : this.mTransitions) {
        if (transition.isValidTarget(paramTransitionValues.view)) {
          transition.captureStartValues(paramTransitionValues);
          paramTransitionValues.targetedTransitions.add(transition);
        } 
      }  
  }
  
  public void captureEndValues(TransitionValues paramTransitionValues) {
    if (isValidTarget(paramTransitionValues.view))
      for (Transition transition : this.mTransitions) {
        if (transition.isValidTarget(paramTransitionValues.view)) {
          transition.captureEndValues(paramTransitionValues);
          paramTransitionValues.targetedTransitions.add(transition);
        } 
      }  
  }
  
  void capturePropagationValues(TransitionValues paramTransitionValues) {
    super.capturePropagationValues(paramTransitionValues);
    int i = this.mTransitions.size();
    for (byte b = 0; b < i; b++)
      ((Transition)this.mTransitions.get(b)).capturePropagationValues(paramTransitionValues); 
  }
  
  public void pause(View paramView) {
    super.pause(paramView);
    int i = this.mTransitions.size();
    for (byte b = 0; b < i; b++)
      ((Transition)this.mTransitions.get(b)).pause(paramView); 
  }
  
  public void resume(View paramView) {
    super.resume(paramView);
    int i = this.mTransitions.size();
    for (byte b = 0; b < i; b++)
      ((Transition)this.mTransitions.get(b)).resume(paramView); 
  }
  
  protected void cancel() {
    super.cancel();
    int i = this.mTransitions.size();
    for (byte b = 0; b < i; b++)
      ((Transition)this.mTransitions.get(b)).cancel(); 
  }
  
  void forceToEnd(ViewGroup paramViewGroup) {
    super.forceToEnd(paramViewGroup);
    int i = this.mTransitions.size();
    for (byte b = 0; b < i; b++)
      ((Transition)this.mTransitions.get(b)).forceToEnd(paramViewGroup); 
  }
  
  TransitionSet setSceneRoot(ViewGroup paramViewGroup) {
    super.setSceneRoot(paramViewGroup);
    int i = this.mTransitions.size();
    for (byte b = 0; b < i; b++)
      ((Transition)this.mTransitions.get(b)).setSceneRoot(paramViewGroup); 
    return this;
  }
  
  void setCanRemoveViews(boolean paramBoolean) {
    super.setCanRemoveViews(paramBoolean);
    int i = this.mTransitions.size();
    for (byte b = 0; b < i; b++)
      ((Transition)this.mTransitions.get(b)).setCanRemoveViews(paramBoolean); 
  }
  
  public void setPropagation(TransitionPropagation paramTransitionPropagation) {
    super.setPropagation(paramTransitionPropagation);
    this.mChangeFlags |= 0x2;
    int i = this.mTransitions.size();
    for (byte b = 0; b < i; b++)
      ((Transition)this.mTransitions.get(b)).setPropagation(paramTransitionPropagation); 
  }
  
  public void setEpicenterCallback(Transition.EpicenterCallback paramEpicenterCallback) {
    super.setEpicenterCallback(paramEpicenterCallback);
    this.mChangeFlags |= 0x8;
    int i = this.mTransitions.size();
    for (byte b = 0; b < i; b++)
      ((Transition)this.mTransitions.get(b)).setEpicenterCallback(paramEpicenterCallback); 
  }
  
  String toString(String paramString) {
    String str = super.toString(paramString);
    for (byte b = 0; b < this.mTransitions.size(); b++) {
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append(str);
      stringBuilder2.append("\n");
      Transition transition = this.mTransitions.get(b);
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append("  ");
      stringBuilder2.append(transition.toString(stringBuilder1.toString()));
      str = stringBuilder2.toString();
    } 
    return str;
  }
  
  public TransitionSet clone() {
    TransitionSet transitionSet = (TransitionSet)super.clone();
    transitionSet.mTransitions = new ArrayList<>();
    int i = this.mTransitions.size();
    for (byte b = 0; b < i; b++)
      transitionSet.addTransitionInternal(((Transition)this.mTransitions.get(b)).clone()); 
    return transitionSet;
  }
  
  public TransitionSet() {}
}
