package android.transition;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.View;
import android.view.ViewGroup;

public class Rotate extends Transition {
  private static final String PROPNAME_ROTATION = "android:rotate:rotation";
  
  public void captureStartValues(TransitionValues paramTransitionValues) {
    paramTransitionValues.values.put("android:rotate:rotation", Float.valueOf(paramTransitionValues.view.getRotation()));
  }
  
  public void captureEndValues(TransitionValues paramTransitionValues) {
    paramTransitionValues.values.put("android:rotate:rotation", Float.valueOf(paramTransitionValues.view.getRotation()));
  }
  
  public Animator createAnimator(ViewGroup paramViewGroup, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    if (paramTransitionValues1 == null || paramTransitionValues2 == null)
      return null; 
    View view = paramTransitionValues2.view;
    float f1 = ((Float)paramTransitionValues1.values.get("android:rotate:rotation")).floatValue();
    float f2 = ((Float)paramTransitionValues2.values.get("android:rotate:rotation")).floatValue();
    if (f1 != f2) {
      view.setRotation(f1);
      return (Animator)ObjectAnimator.ofFloat(view, View.ROTATION, new float[] { f1, f2 });
    } 
    return null;
  }
}
