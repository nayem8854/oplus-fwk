package android.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.graphics.Path;
import android.view.View;

class TranslationAnimationCreator {
  static Animator createAnimation(View paramView, TransitionValues paramTransitionValues, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, TimeInterpolator paramTimeInterpolator, Transition paramTransition) {
    float f1 = paramView.getTranslationX();
    float f2 = paramView.getTranslationY();
    int[] arrayOfInt = (int[])paramTransitionValues.view.getTag(16909565);
    if (arrayOfInt != null) {
      paramFloat2 = (arrayOfInt[0] - paramInt1);
      paramFloat1 = (arrayOfInt[1] - paramInt2);
      paramFloat2 += f1;
      paramFloat1 += f2;
    } else {
      float f = paramFloat2;
      paramFloat2 = paramFloat1;
      paramFloat1 = f;
    } 
    int i = Math.round(paramFloat2 - f1);
    int j = Math.round(paramFloat1 - f2);
    paramView.setTranslationX(paramFloat2);
    paramView.setTranslationY(paramFloat1);
    if (paramFloat2 == paramFloat3 && paramFloat1 == paramFloat4)
      return null; 
    Path path = new Path();
    path.moveTo(paramFloat2, paramFloat1);
    path.lineTo(paramFloat3, paramFloat4);
    ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(paramView, View.TRANSLATION_X, View.TRANSLATION_Y, path);
    TransitionPositionListener transitionPositionListener = new TransitionPositionListener(paramTransitionValues.view, paramInt1 + i, paramInt2 + j, f1, f2);
    paramTransition.addListener(transitionPositionListener);
    objectAnimator.addListener((Animator.AnimatorListener)transitionPositionListener);
    objectAnimator.addPauseListener((Animator.AnimatorPauseListener)transitionPositionListener);
    objectAnimator.setInterpolator(paramTimeInterpolator);
    return (Animator)objectAnimator;
  }
  
  class TransitionPositionListener extends AnimatorListenerAdapter implements Transition.TransitionListener {
    private final View mMovingView;
    
    private float mPausedX;
    
    private float mPausedY;
    
    private final int mStartX;
    
    private final int mStartY;
    
    private final float mTerminalX;
    
    private final float mTerminalY;
    
    private int[] mTransitionPosition;
    
    private final View mViewInHierarchy;
    
    private TransitionPositionListener(TranslationAnimationCreator this$0, View param1View1, int param1Int1, int param1Int2, float param1Float1, float param1Float2) {
      this.mMovingView = (View)this$0;
      this.mViewInHierarchy = param1View1;
      this.mStartX = param1Int1 - Math.round(this$0.getTranslationX());
      this.mStartY = param1Int2 - Math.round(this.mMovingView.getTranslationY());
      this.mTerminalX = param1Float1;
      this.mTerminalY = param1Float2;
      int[] arrayOfInt = (int[])this.mViewInHierarchy.getTag(16909565);
      if (arrayOfInt != null)
        this.mViewInHierarchy.setTagInternal(16909565, null); 
    }
    
    public void onAnimationCancel(Animator param1Animator) {
      if (this.mTransitionPosition == null)
        this.mTransitionPosition = new int[2]; 
      this.mTransitionPosition[0] = Math.round(this.mStartX + this.mMovingView.getTranslationX());
      this.mTransitionPosition[1] = Math.round(this.mStartY + this.mMovingView.getTranslationY());
      this.mViewInHierarchy.setTagInternal(16909565, this.mTransitionPosition);
    }
    
    public void onAnimationEnd(Animator param1Animator) {}
    
    public void onAnimationPause(Animator param1Animator) {
      this.mPausedX = this.mMovingView.getTranslationX();
      this.mPausedY = this.mMovingView.getTranslationY();
      this.mMovingView.setTranslationX(this.mTerminalX);
      this.mMovingView.setTranslationY(this.mTerminalY);
    }
    
    public void onAnimationResume(Animator param1Animator) {
      this.mMovingView.setTranslationX(this.mPausedX);
      this.mMovingView.setTranslationY(this.mPausedY);
    }
    
    public void onTransitionStart(Transition param1Transition) {}
    
    public void onTransitionEnd(Transition param1Transition) {
      this.mMovingView.setTranslationX(this.mTerminalX);
      this.mMovingView.setTranslationY(this.mTerminalY);
      param1Transition.removeListener(this);
    }
    
    public void onTransitionCancel(Transition param1Transition) {}
    
    public void onTransitionPause(Transition param1Transition) {}
    
    public void onTransitionResume(Transition param1Transition) {}
  }
}
