package android.transition;

import android.util.ArrayMap;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class TransitionManager {
  private static String LOG_TAG = "TransitionManager";
  
  private static Transition sDefaultTransition = new AutoTransition();
  
  static {
    EMPTY_STRINGS = new String[0];
    sRunningTransitions = new ThreadLocal<>();
    sPendingTransitions = new ArrayList<>();
  }
  
  ArrayMap<Scene, Transition> mSceneTransitions = new ArrayMap<>();
  
  ArrayMap<Scene, ArrayMap<Scene, Transition>> mScenePairTransitions = new ArrayMap<>();
  
  private static final String[] EMPTY_STRINGS;
  
  private static ArrayList<ViewGroup> sPendingTransitions;
  
  private static ThreadLocal<WeakReference<ArrayMap<ViewGroup, ArrayList<Transition>>>> sRunningTransitions;
  
  public void setDefaultTransition(Transition paramTransition) {
    sDefaultTransition = paramTransition;
  }
  
  public static Transition getDefaultTransition() {
    return sDefaultTransition;
  }
  
  public void setTransition(Scene paramScene, Transition paramTransition) {
    this.mSceneTransitions.put(paramScene, paramTransition);
  }
  
  public void setTransition(Scene paramScene1, Scene paramScene2, Transition paramTransition) {
    ArrayMap<Object, Object> arrayMap1 = (ArrayMap)this.mScenePairTransitions.get(paramScene2);
    ArrayMap<Object, Object> arrayMap2 = arrayMap1;
    if (arrayMap1 == null) {
      arrayMap2 = new ArrayMap<>();
      this.mScenePairTransitions.put(paramScene2, arrayMap2);
    } 
    arrayMap2.put(paramScene1, paramTransition);
  }
  
  public Transition getTransition(Scene paramScene) {
    ViewGroup viewGroup = paramScene.getSceneRoot();
    if (viewGroup != null) {
      Scene scene = Scene.getCurrentScene(viewGroup);
      if (scene != null) {
        ArrayMap arrayMap = this.mScenePairTransitions.get(paramScene);
        if (arrayMap != null) {
          Transition transition1 = (Transition)arrayMap.get(scene);
          if (transition1 != null)
            return transition1; 
        } 
      } 
    } 
    Transition transition = this.mSceneTransitions.get(paramScene);
    if (transition == null)
      transition = sDefaultTransition; 
    return transition;
  }
  
  private static void changeScene(Scene paramScene, Transition paramTransition) {
    ViewGroup viewGroup = paramScene.getSceneRoot();
    if (!sPendingTransitions.contains(viewGroup)) {
      Scene scene = Scene.getCurrentScene(viewGroup);
      if (paramTransition == null) {
        if (scene != null)
          scene.exit(); 
        paramScene.enter();
      } else {
        sPendingTransitions.add(viewGroup);
        paramTransition = paramTransition.clone();
        paramTransition.setSceneRoot(viewGroup);
        if (scene != null && scene.isCreatedFromLayoutResource())
          paramTransition.setCanRemoveViews(true); 
        sceneChangeSetup(viewGroup, paramTransition);
        paramScene.enter();
        sceneChangeRunTransition(viewGroup, paramTransition);
      } 
    } 
  }
  
  private static ArrayMap<ViewGroup, ArrayList<Transition>> getRunningTransitions() {
    ThreadLocal<WeakReference<ArrayMap<ViewGroup, ArrayList<Transition>>>> threadLocal = sRunningTransitions;
    WeakReference<ArrayMap> weakReference = (WeakReference)threadLocal.get();
    if (weakReference != null) {
      ArrayMap<ViewGroup, ArrayList<Transition>> arrayMap1 = weakReference.get();
      if (arrayMap1 != null)
        return arrayMap1; 
    } 
    ArrayMap<Object, Object> arrayMap = new ArrayMap<>();
    weakReference = new WeakReference<>(arrayMap);
    sRunningTransitions.set(weakReference);
    return (ArrayMap)arrayMap;
  }
  
  private static void sceneChangeRunTransition(ViewGroup paramViewGroup, Transition paramTransition) {
    if (paramTransition != null && paramViewGroup != null) {
      MultiListener multiListener = new MultiListener(paramTransition, paramViewGroup);
      paramViewGroup.addOnAttachStateChangeListener(multiListener);
      paramViewGroup.getViewTreeObserver().addOnPreDrawListener(multiListener);
    } 
  }
  
  class MultiListener implements ViewTreeObserver.OnPreDrawListener, View.OnAttachStateChangeListener {
    ViewGroup mSceneRoot;
    
    Transition mTransition;
    
    final ViewTreeObserver mViewTreeObserver;
    
    MultiListener(TransitionManager this$0, ViewGroup param1ViewGroup) {
      this.mTransition = (Transition)this$0;
      this.mSceneRoot = param1ViewGroup;
      this.mViewTreeObserver = param1ViewGroup.getViewTreeObserver();
    }
    
    private void removeListeners() {
      if (this.mViewTreeObserver.isAlive()) {
        this.mViewTreeObserver.removeOnPreDrawListener(this);
      } else {
        this.mSceneRoot.getViewTreeObserver().removeOnPreDrawListener(this);
      } 
      this.mSceneRoot.removeOnAttachStateChangeListener(this);
    }
    
    public void onViewAttachedToWindow(View param1View) {}
    
    public void onViewDetachedFromWindow(View param1View) {
      removeListeners();
      TransitionManager.sPendingTransitions.remove(this.mSceneRoot);
      ArrayList arrayList = (ArrayList)TransitionManager.getRunningTransitions().get(this.mSceneRoot);
      if (arrayList != null && arrayList.size() > 0)
        for (Transition transition : arrayList)
          transition.resume(this.mSceneRoot);  
      this.mTransition.clearValues(true);
    }
    
    public boolean onPreDraw() {
      ArrayList<?> arrayList2;
      removeListeners();
      if (!TransitionManager.sPendingTransitions.remove(this.mSceneRoot))
        return true; 
      ArrayMap<ViewGroup, ArrayList> arrayMap = (ArrayMap)TransitionManager.getRunningTransitions();
      ArrayList<?> arrayList1 = (ArrayList)arrayMap.get(this.mSceneRoot);
      ArrayList arrayList = null;
      if (arrayList1 == null) {
        arrayList2 = new ArrayList();
        arrayMap.put(this.mSceneRoot, arrayList2);
      } else {
        arrayList2 = arrayList1;
        if (arrayList1.size() > 0) {
          arrayList = new ArrayList(arrayList1);
          arrayList2 = arrayList1;
        } 
      } 
      arrayList2.add(this.mTransition);
      this.mTransition.addListener((Transition.TransitionListener)new Object(this, arrayMap));
      this.mTransition.captureValues(this.mSceneRoot, false);
      if (arrayList != null)
        for (Transition transition : arrayList)
          transition.resume(this.mSceneRoot);  
      this.mTransition.playTransition(this.mSceneRoot);
      return true;
    }
  }
  
  private static void sceneChangeSetup(ViewGroup paramViewGroup, Transition paramTransition) {
    ArrayList arrayList = getRunningTransitions().get(paramViewGroup);
    if (arrayList != null && arrayList.size() > 0)
      for (Transition transition : arrayList)
        transition.pause(paramViewGroup);  
    if (paramTransition != null)
      paramTransition.captureValues(paramViewGroup, true); 
    Scene scene = Scene.getCurrentScene(paramViewGroup);
    if (scene != null)
      scene.exit(); 
  }
  
  public void transitionTo(Scene paramScene) {
    changeScene(paramScene, getTransition(paramScene));
  }
  
  public static void go(Scene paramScene) {
    changeScene(paramScene, sDefaultTransition);
  }
  
  public static void go(Scene paramScene, Transition paramTransition) {
    changeScene(paramScene, paramTransition);
  }
  
  public static void beginDelayedTransition(ViewGroup paramViewGroup) {
    beginDelayedTransition(paramViewGroup, null);
  }
  
  public static void beginDelayedTransition(ViewGroup paramViewGroup, Transition paramTransition) {
    if (!sPendingTransitions.contains(paramViewGroup) && paramViewGroup.isLaidOut()) {
      sPendingTransitions.add(paramViewGroup);
      Transition transition = paramTransition;
      if (paramTransition == null)
        transition = sDefaultTransition; 
      paramTransition = transition.clone();
      sceneChangeSetup(paramViewGroup, paramTransition);
      Scene.setCurrentScene(paramViewGroup, null);
      sceneChangeRunTransition(paramViewGroup, paramTransition);
    } 
  }
  
  public static void endTransitions(ViewGroup paramViewGroup) {
    sPendingTransitions.remove(paramViewGroup);
    ArrayList<?> arrayList = getRunningTransitions().get(paramViewGroup);
    if (arrayList != null && !arrayList.isEmpty()) {
      arrayList = new ArrayList(arrayList);
      for (int i = arrayList.size() - 1; i >= 0; i--) {
        Transition transition = (Transition)arrayList.get(i);
        transition.forceToEnd(paramViewGroup);
      } 
    } 
  }
}
