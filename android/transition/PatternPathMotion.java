package android.transition;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.util.AttributeSet;
import android.util.PathParser;
import com.android.internal.R;

public class PatternPathMotion extends PathMotion {
  private Path mOriginalPatternPath;
  
  private final Path mPatternPath = new Path();
  
  private final Matrix mTempMatrix = new Matrix();
  
  public PatternPathMotion() {
    this.mPatternPath.lineTo(1.0F, 0.0F);
    this.mOriginalPatternPath = this.mPatternPath;
  }
  
  public PatternPathMotion(Context paramContext, AttributeSet paramAttributeSet) {
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.PatternPathMotion);
    try {
      String str = typedArray.getString(0);
      if (str != null) {
        Path path = PathParser.createPathFromPathData(str);
        setPatternPath(path);
        return;
      } 
      RuntimeException runtimeException = new RuntimeException();
      this("pathData must be supplied for patternPathMotion");
      throw runtimeException;
    } finally {
      typedArray.recycle();
    } 
  }
  
  public PatternPathMotion(Path paramPath) {
    setPatternPath(paramPath);
  }
  
  public Path getPatternPath() {
    return this.mOriginalPatternPath;
  }
  
  public void setPatternPath(Path paramPath) {
    PathMeasure pathMeasure = new PathMeasure(paramPath, false);
    float f1 = pathMeasure.getLength();
    float[] arrayOfFloat = new float[2];
    pathMeasure.getPosTan(f1, arrayOfFloat, null);
    float f2 = arrayOfFloat[0];
    float f3 = arrayOfFloat[1];
    pathMeasure.getPosTan(0.0F, arrayOfFloat, null);
    f1 = arrayOfFloat[0];
    float f4 = arrayOfFloat[1];
    if (f1 != f2 || f4 != f3) {
      this.mTempMatrix.setTranslate(-f1, -f4);
      f1 = f2 - f1;
      f4 = f3 - f4;
      f3 = (float)Math.hypot(f1, f4);
      f3 = 1.0F / f3;
      this.mTempMatrix.postScale(f3, f3);
      double d = Math.atan2(f4, f1);
      this.mTempMatrix.postRotate((float)Math.toDegrees(-d));
      paramPath.transform(this.mTempMatrix, this.mPatternPath);
      this.mOriginalPatternPath = paramPath;
      return;
    } 
    throw new IllegalArgumentException("pattern must not end at the starting point");
  }
  
  public Path getPath(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    double d1 = (paramFloat3 - paramFloat1);
    double d2 = (paramFloat4 - paramFloat2);
    paramFloat3 = (float)Math.hypot(d1, d2);
    d2 = Math.atan2(d2, d1);
    this.mTempMatrix.setScale(paramFloat3, paramFloat3);
    this.mTempMatrix.postRotate((float)Math.toDegrees(d2));
    this.mTempMatrix.postTranslate(paramFloat1, paramFloat2);
    Path path = new Path();
    this.mPatternPath.transform(this.mTempMatrix, path);
    return path;
  }
}
