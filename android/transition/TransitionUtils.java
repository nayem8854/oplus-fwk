package android.transition;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.TypeEvaluator;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Picture;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class TransitionUtils {
  private static int MAX_IMAGE_SIZE = 1048576;
  
  static Animator mergeAnimators(Animator paramAnimator1, Animator paramAnimator2) {
    if (paramAnimator1 == null)
      return paramAnimator2; 
    if (paramAnimator2 == null)
      return paramAnimator1; 
    AnimatorSet animatorSet = new AnimatorSet();
    animatorSet.playTogether(new Animator[] { paramAnimator1, paramAnimator2 });
    return (Animator)animatorSet;
  }
  
  public static Transition mergeTransitions(Transition... paramVarArgs) {
    int i = 0;
    byte b = -1;
    byte b1;
    for (b1 = 0; b1 < paramVarArgs.length; b1++, i = j) {
      int j = i;
      if (paramVarArgs[b1] != null) {
        j = i + 1;
        b = b1;
      } 
    } 
    if (i == 0)
      return null; 
    if (i == 1)
      return paramVarArgs[b]; 
    TransitionSet transitionSet = new TransitionSet();
    for (b1 = 0; b1 < paramVarArgs.length; b1++) {
      if (paramVarArgs[b1] != null)
        transitionSet.addTransition(paramVarArgs[b1]); 
    } 
    return transitionSet;
  }
  
  public static View copyViewImage(ViewGroup paramViewGroup, View paramView1, View paramView2) {
    Matrix matrix = new Matrix();
    matrix.setTranslate(-paramView2.getScrollX(), -paramView2.getScrollY());
    paramView1.transformMatrixToGlobal(matrix);
    paramViewGroup.transformMatrixToLocal(matrix);
    RectF rectF = new RectF(0.0F, 0.0F, paramView1.getWidth(), paramView1.getHeight());
    matrix.mapRect(rectF);
    int i = Math.round(rectF.left);
    int j = Math.round(rectF.top);
    int k = Math.round(rectF.right);
    int m = Math.round(rectF.bottom);
    paramView2 = new ImageView(paramView1.getContext());
    paramView2.setScaleType(ImageView.ScaleType.CENTER_CROP);
    Bitmap bitmap = createViewBitmap(paramView1, matrix, rectF, paramViewGroup);
    if (bitmap != null)
      paramView2.setImageBitmap(bitmap); 
    int n = View.MeasureSpec.makeMeasureSpec(k - i, 1073741824);
    int i1 = View.MeasureSpec.makeMeasureSpec(m - j, 1073741824);
    paramView2.measure(n, i1);
    paramView2.layout(i, j, k, m);
    return paramView2;
  }
  
  public static Bitmap createDrawableBitmap(Drawable paramDrawable, View paramView) {
    int i = paramDrawable.getIntrinsicWidth();
    int j = paramDrawable.getIntrinsicHeight();
    if (i <= 0 || j <= 0)
      return null; 
    float f = Math.min(1.0F, MAX_IMAGE_SIZE / (i * j));
    if (paramDrawable instanceof BitmapDrawable && f == 1.0F)
      return ((BitmapDrawable)paramDrawable).getBitmap(); 
    int k = (int)(i * f);
    int m = (int)(j * f);
    Picture picture = new Picture();
    Canvas canvas = picture.beginRecording(i, j);
    Rect rect = paramDrawable.getBounds();
    j = rect.left;
    int n = rect.top;
    i = rect.right;
    int i1 = rect.bottom;
    paramDrawable.setBounds(0, 0, k, m);
    paramDrawable.draw(canvas);
    paramDrawable.setBounds(j, n, i, i1);
    picture.endRecording();
    return Bitmap.createBitmap(picture);
  }
  
  public static Bitmap createViewBitmap(View paramView, Matrix paramMatrix, RectF paramRectF, ViewGroup paramViewGroup) {
    Bitmap bitmap;
    int i = paramView.isAttachedToWindow() ^ true;
    ViewGroup viewGroup = null;
    int j = 0;
    if (i != 0) {
      if (paramViewGroup == null || !paramViewGroup.isAttachedToWindow())
        return null; 
      viewGroup = (ViewGroup)paramView.getParent();
      j = viewGroup.indexOfChild(paramView);
      paramViewGroup.getOverlay().add(paramView);
    } 
    Picture picture1 = null;
    int k = Math.round(paramRectF.width());
    int m = Math.round(paramRectF.height());
    Picture picture2 = picture1;
    if (k > 0) {
      picture2 = picture1;
      if (m > 0) {
        float f = Math.min(1.0F, MAX_IMAGE_SIZE / (k * m));
        k = (int)(k * f);
        m = (int)(m * f);
        paramMatrix.postTranslate(-paramRectF.left, -paramRectF.top);
        paramMatrix.postScale(f, f);
        picture2 = new Picture();
        Canvas canvas = picture2.beginRecording(k, m);
        canvas.concat(paramMatrix);
        paramView.draw(canvas);
        picture2.endRecording();
        bitmap = Bitmap.createBitmap(picture2);
      } 
    } 
    if (i != 0) {
      paramViewGroup.getOverlay().remove(paramView);
      viewGroup.addView(paramView, j);
    } 
    return bitmap;
  }
  
  class MatrixEvaluator implements TypeEvaluator<Matrix> {
    float[] mTempStartValues = new float[9];
    
    float[] mTempEndValues = new float[9];
    
    Matrix mTempMatrix = new Matrix();
    
    public Matrix evaluate(float param1Float, Matrix param1Matrix1, Matrix param1Matrix2) {
      param1Matrix1.getValues(this.mTempStartValues);
      param1Matrix2.getValues(this.mTempEndValues);
      for (byte b = 0; b < 9; b++) {
        float arrayOfFloat2[] = this.mTempEndValues, f1 = arrayOfFloat2[b], arrayOfFloat1[] = this.mTempStartValues, f2 = arrayOfFloat1[b];
        arrayOfFloat2[b] = arrayOfFloat1[b] + param1Float * (f1 - f2);
      } 
      this.mTempMatrix.setValues(this.mTempEndValues);
      return this.mTempMatrix;
    }
  }
}
