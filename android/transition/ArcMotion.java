package android.transition;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Path;
import android.util.AttributeSet;
import com.android.internal.R;

public class ArcMotion extends PathMotion {
  private static final float DEFAULT_MAX_TANGENT = (float)Math.tan(Math.toRadians(35.0D));
  
  private float mMinimumHorizontalAngle = 0.0F;
  
  private float mMinimumVerticalAngle = 0.0F;
  
  private float mMaximumAngle = 70.0F;
  
  private float mMinimumHorizontalTangent = 0.0F;
  
  private float mMinimumVerticalTangent = 0.0F;
  
  private float mMaximumTangent = DEFAULT_MAX_TANGENT;
  
  private static final float DEFAULT_MAX_ANGLE_DEGREES = 70.0F;
  
  private static final float DEFAULT_MIN_ANGLE_DEGREES = 0.0F;
  
  public ArcMotion(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ArcMotion);
    float f = typedArray.getFloat(1, 0.0F);
    setMinimumVerticalAngle(f);
    f = typedArray.getFloat(0, 0.0F);
    setMinimumHorizontalAngle(f);
    f = typedArray.getFloat(2, 70.0F);
    setMaximumAngle(f);
    typedArray.recycle();
  }
  
  public void setMinimumHorizontalAngle(float paramFloat) {
    this.mMinimumHorizontalAngle = paramFloat;
    this.mMinimumHorizontalTangent = toTangent(paramFloat);
  }
  
  public float getMinimumHorizontalAngle() {
    return this.mMinimumHorizontalAngle;
  }
  
  public void setMinimumVerticalAngle(float paramFloat) {
    this.mMinimumVerticalAngle = paramFloat;
    this.mMinimumVerticalTangent = toTangent(paramFloat);
  }
  
  public float getMinimumVerticalAngle() {
    return this.mMinimumVerticalAngle;
  }
  
  public void setMaximumAngle(float paramFloat) {
    this.mMaximumAngle = paramFloat;
    this.mMaximumTangent = toTangent(paramFloat);
  }
  
  public float getMaximumAngle() {
    return this.mMaximumAngle;
  }
  
  private static float toTangent(float paramFloat) {
    if (paramFloat >= 0.0F && paramFloat <= 90.0F)
      return (float)Math.tan(Math.toRadians((paramFloat / 2.0F))); 
    throw new IllegalArgumentException("Arc must be between 0 and 90 degrees");
  }
  
  public Path getPath(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    boolean bool;
    Path path = new Path();
    path.moveTo(paramFloat1, paramFloat2);
    float f1 = paramFloat3 - paramFloat1;
    float f2 = paramFloat4 - paramFloat2;
    float f3 = f1 * f1 + f2 * f2;
    float f4 = (paramFloat1 + paramFloat3) / 2.0F;
    float f5 = (paramFloat2 + paramFloat4) / 2.0F;
    float f6 = f3 * 0.25F;
    if (paramFloat2 > paramFloat4) {
      bool = true;
    } else {
      bool = false;
    } 
    if (f2 == 0.0F) {
      f3 = f4;
      f1 = Math.abs(f1) * 0.5F * this.mMinimumHorizontalTangent + f5;
      f2 = 0.0F;
    } else if (f1 == 0.0F) {
      f3 = Math.abs(f2) * 0.5F * this.mMinimumVerticalTangent + f4;
      f1 = f5;
      f2 = 0.0F;
    } else if (Math.abs(f1) < Math.abs(f2)) {
      f3 = Math.abs(f3 / f2 * 2.0F);
      if (bool) {
        f3 = paramFloat4 + f3;
        f1 = paramFloat3;
      } else {
        f3 = paramFloat2 + f3;
        f1 = paramFloat1;
      } 
      f2 = this.mMinimumVerticalTangent;
      float f = f6 * f2 * f2;
      f2 = f1;
      f1 = f3;
      f3 = f2;
      f2 = f;
    } else {
      f3 /= f1 * 2.0F;
      if (bool) {
        f3 = paramFloat1 + f3;
        f1 = paramFloat2;
      } else {
        f3 = paramFloat3 - f3;
        f1 = paramFloat4;
      } 
      f2 = this.mMinimumHorizontalTangent;
      f2 = f6 * f2 * f2;
    } 
    float f8 = f4 - f3;
    float f7 = f5 - f1;
    f8 = f8 * f8 + f7 * f7;
    f7 = this.mMaximumTangent;
    f7 = f6 * f7 * f7;
    if (f8 == 0.0F || f8 >= f2)
      if (f8 > f7) {
        f2 = f7;
      } else {
        f2 = 0.0F;
      }  
    if (f2 != 0.0F) {
      f2 /= f8;
      f2 = (float)Math.sqrt(f2);
      f3 = f4 + (f3 - f4) * f2;
      f2 = f5 + (f1 - f5) * f2;
      f1 = f3;
      f3 = f2;
    } else {
      f2 = f3;
      f3 = f1;
      f1 = f2;
    } 
    paramFloat1 = (paramFloat1 + f1) / 2.0F;
    paramFloat2 = (paramFloat2 + f3) / 2.0F;
    f1 = (f1 + paramFloat3) / 2.0F;
    f3 = (f3 + paramFloat4) / 2.0F;
    path.cubicTo(paramFloat1, paramFloat2, f1, f3, paramFloat3, paramFloat4);
    return path;
  }
  
  public ArcMotion() {}
}
