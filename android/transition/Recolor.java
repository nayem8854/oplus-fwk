package android.transition;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.Map;

public class Recolor extends Transition {
  private static final String PROPNAME_BACKGROUND = "android:recolor:background";
  
  private static final String PROPNAME_TEXT_COLOR = "android:recolor:textColor";
  
  public Recolor() {}
  
  public Recolor(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  private void captureValues(TransitionValues paramTransitionValues) {
    paramTransitionValues.values.put("android:recolor:background", paramTransitionValues.view.getBackground());
    if (paramTransitionValues.view instanceof TextView) {
      Map<String, Object> map = paramTransitionValues.values;
      TextView textView = (TextView)paramTransitionValues.view;
      int i = textView.getCurrentTextColor();
      map.put("android:recolor:textColor", Integer.valueOf(i));
    } 
  }
  
  public void captureStartValues(TransitionValues paramTransitionValues) {
    captureValues(paramTransitionValues);
  }
  
  public void captureEndValues(TransitionValues paramTransitionValues) {
    captureValues(paramTransitionValues);
  }
  
  public Animator createAnimator(ViewGroup paramViewGroup, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    if (paramTransitionValues1 == null || paramTransitionValues2 == null)
      return null; 
    View view = paramTransitionValues2.view;
    Drawable drawable2 = (Drawable)paramTransitionValues1.values.get("android:recolor:background");
    Drawable drawable1 = (Drawable)paramTransitionValues2.values.get("android:recolor:background");
    if (drawable2 instanceof ColorDrawable && drawable1 instanceof ColorDrawable) {
      ColorDrawable colorDrawable2 = (ColorDrawable)drawable2;
      ColorDrawable colorDrawable1 = (ColorDrawable)drawable1;
      if (colorDrawable2.getColor() != colorDrawable1.getColor()) {
        colorDrawable1.setColor(colorDrawable2.getColor());
        int i = colorDrawable2.getColor();
        int j = colorDrawable1.getColor();
        return (Animator)ObjectAnimator.ofArgb(drawable1, "color", new int[] { i, j });
      } 
    } 
    if (view instanceof TextView) {
      TextView textView = (TextView)view;
      int j = ((Integer)paramTransitionValues1.values.get("android:recolor:textColor")).intValue();
      int i = ((Integer)paramTransitionValues2.values.get("android:recolor:textColor")).intValue();
      if (j != i) {
        textView.setTextColor(i);
        return (Animator)ObjectAnimator.ofArgb(textView, "textColor", new int[] { j, i });
      } 
    } 
    return null;
  }
}
