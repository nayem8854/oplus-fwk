package android.transition;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import java.util.Map;

public class ChangeImageTransform extends Transition {
  private static Property<ImageView, Matrix> ANIMATED_TRANSFORM_PROPERTY;
  
  private static TypeEvaluator<Matrix> NULL_MATRIX_EVALUATOR;
  
  private static final String PROPNAME_BOUNDS = "android:changeImageTransform:bounds";
  
  private static final String PROPNAME_MATRIX = "android:changeImageTransform:matrix";
  
  private static final String TAG = "ChangeImageTransform";
  
  private static final String[] sTransitionProperties = new String[] { "android:changeImageTransform:matrix", "android:changeImageTransform:bounds" };
  
  static {
    NULL_MATRIX_EVALUATOR = new TypeEvaluator<Matrix>() {
        public Matrix evaluate(float param1Float, Matrix param1Matrix1, Matrix param1Matrix2) {
          return null;
        }
      };
    ANIMATED_TRANSFORM_PROPERTY = new Property<ImageView, Matrix>(Matrix.class, "animatedTransform") {
        public void set(ImageView param1ImageView, Matrix param1Matrix) {
          param1ImageView.animateTransform(param1Matrix);
        }
        
        public Matrix get(ImageView param1ImageView) {
          return null;
        }
      };
  }
  
  public ChangeImageTransform() {}
  
  public ChangeImageTransform(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  private void captureValues(TransitionValues paramTransitionValues) {
    Matrix matrix;
    View view = paramTransitionValues.view;
    if (!(view instanceof ImageView) || view.getVisibility() != 0)
      return; 
    ImageView imageView = (ImageView)view;
    Drawable drawable = imageView.getDrawable();
    if (drawable == null)
      return; 
    Map<String, Object> map = paramTransitionValues.values;
    int i = view.getLeft();
    int j = view.getTop();
    int k = view.getRight();
    int m = view.getBottom();
    Rect rect = new Rect(i, j, k, m);
    map.put("android:changeImageTransform:bounds", rect);
    ImageView.ScaleType scaleType = imageView.getScaleType();
    i = drawable.getIntrinsicWidth();
    m = drawable.getIntrinsicHeight();
    if (scaleType == ImageView.ScaleType.FIT_XY && i > 0 && m > 0) {
      float f1 = rect.width() / i;
      float f2 = rect.height() / m;
      matrix = new Matrix();
      matrix.setScale(f1, f2);
    } else {
      matrix = new Matrix(imageView.getImageMatrix());
    } 
    map.put("android:changeImageTransform:matrix", matrix);
  }
  
  public void captureStartValues(TransitionValues paramTransitionValues) {
    captureValues(paramTransitionValues);
  }
  
  public void captureEndValues(TransitionValues paramTransitionValues) {
    captureValues(paramTransitionValues);
  }
  
  public String[] getTransitionProperties() {
    return sTransitionProperties;
  }
  
  public Animator createAnimator(ViewGroup paramViewGroup, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    if (paramTransitionValues1 == null || paramTransitionValues2 == null)
      return null; 
    Rect rect1 = (Rect)paramTransitionValues1.values.get("android:changeImageTransform:bounds");
    Rect rect2 = (Rect)paramTransitionValues2.values.get("android:changeImageTransform:bounds");
    Matrix matrix2 = (Matrix)paramTransitionValues1.values.get("android:changeImageTransform:matrix");
    Matrix matrix1 = (Matrix)paramTransitionValues2.values.get("android:changeImageTransform:matrix");
    if (rect1 == null || rect2 == null || matrix2 == null || matrix1 == null)
      return null; 
    if (rect1.equals(rect2) && matrix2.equals(matrix1))
      return null; 
    ImageView imageView = (ImageView)paramTransitionValues2.view;
    Drawable drawable = imageView.getDrawable();
    int i = drawable.getIntrinsicWidth();
    int j = drawable.getIntrinsicHeight();
    if (i <= 0 || j <= 0) {
      objectAnimator = createNullAnimator(imageView);
      return (Animator)objectAnimator;
    } 
    ANIMATED_TRANSFORM_PROPERTY.set(imageView, matrix2);
    ObjectAnimator objectAnimator = createMatrixAnimator(imageView, matrix2, (Matrix)objectAnimator);
    return (Animator)objectAnimator;
  }
  
  private ObjectAnimator createNullAnimator(ImageView paramImageView) {
    return ObjectAnimator.ofObject(paramImageView, ANIMATED_TRANSFORM_PROPERTY, NULL_MATRIX_EVALUATOR, (Object[])new Matrix[] { Matrix.IDENTITY_MATRIX, Matrix.IDENTITY_MATRIX });
  }
  
  private ObjectAnimator createMatrixAnimator(ImageView paramImageView, Matrix paramMatrix1, Matrix paramMatrix2) {
    return ObjectAnimator.ofObject(paramImageView, ANIMATED_TRANSFORM_PROPERTY, new TransitionUtils.MatrixEvaluator(), (Object[])new Matrix[] { paramMatrix1, paramMatrix2 });
  }
}
