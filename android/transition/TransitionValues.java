package android.transition;

import android.util.ArrayMap;
import android.view.View;
import java.util.ArrayList;
import java.util.Map;

public class TransitionValues {
  public View view;
  
  @Deprecated
  public TransitionValues() {}
  
  public TransitionValues(View paramView) {
    this.view = paramView;
  }
  
  public final Map<String, Object> values = new ArrayMap<>();
  
  final ArrayList<Transition> targetedTransitions = new ArrayList<>();
  
  public boolean equals(Object paramObject) {
    if (paramObject instanceof TransitionValues && 
      this.view == ((TransitionValues)paramObject).view && 
      this.values.equals(((TransitionValues)paramObject).values))
      return true; 
    return false;
  }
  
  public int hashCode() {
    return this.view.hashCode() * 31 + this.values.hashCode();
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("TransitionValues@");
    stringBuilder.append(Integer.toHexString(hashCode()));
    stringBuilder.append(":\n");
    String str2 = stringBuilder.toString();
    stringBuilder = new StringBuilder();
    stringBuilder.append(str2);
    stringBuilder.append("    view = ");
    stringBuilder.append(this.view);
    stringBuilder.append("\n");
    str2 = stringBuilder.toString();
    stringBuilder = new StringBuilder();
    stringBuilder.append(str2);
    stringBuilder.append("    values:");
    String str1 = stringBuilder.toString();
    for (String str : this.values.keySet()) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(str1);
      stringBuilder1.append("    ");
      stringBuilder1.append(str);
      stringBuilder1.append(": ");
      stringBuilder1.append(this.values.get(str));
      stringBuilder1.append("\n");
      str1 = stringBuilder1.toString();
    } 
    return str1;
  }
}
