package android.transition;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public final class Scene {
  private Context mContext;
  
  Runnable mEnterAction;
  
  Runnable mExitAction;
  
  private View mLayout;
  
  private int mLayoutId = -1;
  
  private ViewGroup mSceneRoot;
  
  public static Scene getSceneForLayout(ViewGroup paramViewGroup, int paramInt, Context paramContext) {
    SparseArray<Scene> sparseArray1 = (SparseArray)paramViewGroup.getTag(16909381);
    SparseArray<Scene> sparseArray2 = sparseArray1;
    if (sparseArray1 == null) {
      sparseArray2 = new SparseArray();
      paramViewGroup.setTagInternal(16909381, sparseArray2);
    } 
    Scene scene2 = sparseArray2.get(paramInt);
    if (scene2 != null)
      return scene2; 
    Scene scene1 = new Scene(paramViewGroup, paramInt, paramContext);
    sparseArray2.put(paramInt, scene1);
    return scene1;
  }
  
  public Scene(ViewGroup paramViewGroup) {
    this.mSceneRoot = paramViewGroup;
  }
  
  private Scene(ViewGroup paramViewGroup, int paramInt, Context paramContext) {
    this.mContext = paramContext;
    this.mSceneRoot = paramViewGroup;
    this.mLayoutId = paramInt;
  }
  
  public Scene(ViewGroup paramViewGroup, View paramView) {
    this.mSceneRoot = paramViewGroup;
    this.mLayout = paramView;
  }
  
  @Deprecated
  public Scene(ViewGroup paramViewGroup1, ViewGroup paramViewGroup2) {
    this.mSceneRoot = paramViewGroup1;
    this.mLayout = paramViewGroup2;
  }
  
  public ViewGroup getSceneRoot() {
    return this.mSceneRoot;
  }
  
  public void exit() {
    if (getCurrentScene(this.mSceneRoot) == this) {
      Runnable runnable = this.mExitAction;
      if (runnable != null)
        runnable.run(); 
    } 
  }
  
  public void enter() {
    if (this.mLayoutId > 0 || this.mLayout != null) {
      getSceneRoot().removeAllViews();
      if (this.mLayoutId > 0) {
        LayoutInflater.from(this.mContext).inflate(this.mLayoutId, this.mSceneRoot);
      } else {
        this.mSceneRoot.addView(this.mLayout);
      } 
    } 
    Runnable runnable = this.mEnterAction;
    if (runnable != null)
      runnable.run(); 
    setCurrentScene(this.mSceneRoot, this);
  }
  
  static void setCurrentScene(ViewGroup paramViewGroup, Scene paramScene) {
    paramViewGroup.setTagInternal(16908900, paramScene);
  }
  
  public static Scene getCurrentScene(ViewGroup paramViewGroup) {
    return (Scene)paramViewGroup.getTag(16908900);
  }
  
  public void setEnterAction(Runnable paramRunnable) {
    this.mEnterAction = paramRunnable;
  }
  
  public void setExitAction(Runnable paramRunnable) {
    this.mExitAction = paramRunnable;
  }
  
  boolean isCreatedFromLayoutResource() {
    boolean bool;
    if (this.mLayoutId > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
