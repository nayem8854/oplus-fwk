package android.transition;

import android.animation.Animator;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;
import com.android.internal.R;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Map;

public abstract class Visibility extends Transition {
  public static final int MODE_IN = 1;
  
  public static final int MODE_OUT = 2;
  
  private static final String PROPNAME_PARENT = "android:visibility:parent";
  
  private static final String PROPNAME_SCREEN_LOCATION = "android:visibility:screenLocation";
  
  static final String PROPNAME_VISIBILITY = "android:visibility:visibility";
  
  private static final String[] sTransitionProperties = new String[] { "android:visibility:visibility", "android:visibility:parent" };
  
  @Retention(RetentionPolicy.SOURCE)
  class VisibilityMode implements Annotation {}
  
  class VisibilityInfo {
    ViewGroup endParent;
    
    int endVisibility;
    
    boolean fadeIn;
    
    ViewGroup startParent;
    
    int startVisibility;
    
    boolean visibilityChange;
    
    private VisibilityInfo() {}
  }
  
  private int mMode = 3;
  
  private boolean mSuppressLayout = true;
  
  public Visibility(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.VisibilityTransition);
    int i = typedArray.getInt(0, 0);
    typedArray.recycle();
    if (i != 0)
      setMode(i); 
  }
  
  public void setSuppressLayout(boolean paramBoolean) {
    this.mSuppressLayout = paramBoolean;
  }
  
  public void setMode(int paramInt) {
    if ((paramInt & 0xFFFFFFFC) == 0) {
      this.mMode = paramInt;
      return;
    } 
    throw new IllegalArgumentException("Only MODE_IN and MODE_OUT flags are allowed");
  }
  
  public int getMode() {
    return this.mMode;
  }
  
  public String[] getTransitionProperties() {
    return sTransitionProperties;
  }
  
  private void captureValues(TransitionValues paramTransitionValues) {
    int i = paramTransitionValues.view.getVisibility();
    paramTransitionValues.values.put("android:visibility:visibility", Integer.valueOf(i));
    paramTransitionValues.values.put("android:visibility:parent", paramTransitionValues.view.getParent());
    int[] arrayOfInt = new int[2];
    paramTransitionValues.view.getLocationOnScreen(arrayOfInt);
    paramTransitionValues.values.put("android:visibility:screenLocation", arrayOfInt);
  }
  
  public void captureStartValues(TransitionValues paramTransitionValues) {
    captureValues(paramTransitionValues);
  }
  
  public void captureEndValues(TransitionValues paramTransitionValues) {
    captureValues(paramTransitionValues);
  }
  
  public boolean isVisible(TransitionValues paramTransitionValues) {
    boolean bool1 = false;
    if (paramTransitionValues == null)
      return false; 
    int i = ((Integer)paramTransitionValues.values.get("android:visibility:visibility")).intValue();
    View view = (View)paramTransitionValues.values.get("android:visibility:parent");
    boolean bool2 = bool1;
    if (i == 0) {
      bool2 = bool1;
      if (view != null)
        bool2 = true; 
    } 
    return bool2;
  }
  
  private static VisibilityInfo getVisibilityChangeInfo(TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    VisibilityInfo visibilityInfo = new VisibilityInfo();
    visibilityInfo.visibilityChange = false;
    visibilityInfo.fadeIn = false;
    if (paramTransitionValues1 != null && paramTransitionValues1.values.containsKey("android:visibility:visibility")) {
      visibilityInfo.startVisibility = ((Integer)paramTransitionValues1.values.get("android:visibility:visibility")).intValue();
      visibilityInfo.startParent = (ViewGroup)paramTransitionValues1.values.get("android:visibility:parent");
    } else {
      visibilityInfo.startVisibility = -1;
      visibilityInfo.startParent = null;
    } 
    if (paramTransitionValues2 != null && paramTransitionValues2.values.containsKey("android:visibility:visibility")) {
      visibilityInfo.endVisibility = ((Integer)paramTransitionValues2.values.get("android:visibility:visibility")).intValue();
      visibilityInfo.endParent = (ViewGroup)paramTransitionValues2.values.get("android:visibility:parent");
    } else {
      visibilityInfo.endVisibility = -1;
      visibilityInfo.endParent = null;
    } 
    if (paramTransitionValues1 != null && paramTransitionValues2 != null) {
      if (visibilityInfo.startVisibility == visibilityInfo.endVisibility && visibilityInfo.startParent == visibilityInfo.endParent)
        return visibilityInfo; 
      if (visibilityInfo.startVisibility != visibilityInfo.endVisibility) {
        if (visibilityInfo.startVisibility == 0) {
          visibilityInfo.fadeIn = false;
          visibilityInfo.visibilityChange = true;
        } else if (visibilityInfo.endVisibility == 0) {
          visibilityInfo.fadeIn = true;
          visibilityInfo.visibilityChange = true;
        } 
      } else if (visibilityInfo.startParent != visibilityInfo.endParent) {
        if (visibilityInfo.endParent == null) {
          visibilityInfo.fadeIn = false;
          visibilityInfo.visibilityChange = true;
        } else if (visibilityInfo.startParent == null) {
          visibilityInfo.fadeIn = true;
          visibilityInfo.visibilityChange = true;
        } 
      } 
    } else if (paramTransitionValues1 == null && visibilityInfo.endVisibility == 0) {
      visibilityInfo.fadeIn = true;
      visibilityInfo.visibilityChange = true;
    } else if (paramTransitionValues2 == null && visibilityInfo.startVisibility == 0) {
      visibilityInfo.fadeIn = false;
      visibilityInfo.visibilityChange = true;
    } 
    return visibilityInfo;
  }
  
  public Animator createAnimator(ViewGroup paramViewGroup, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    VisibilityInfo visibilityInfo = getVisibilityChangeInfo(paramTransitionValues1, paramTransitionValues2);
    if (visibilityInfo.visibilityChange && (visibilityInfo.startParent != null || visibilityInfo.endParent != null)) {
      if (visibilityInfo.fadeIn)
        return onAppear(paramViewGroup, paramTransitionValues1, visibilityInfo.startVisibility, paramTransitionValues2, visibilityInfo.endVisibility); 
      return onDisappear(paramViewGroup, paramTransitionValues1, visibilityInfo.startVisibility, paramTransitionValues2, visibilityInfo.endVisibility);
    } 
    return null;
  }
  
  public Animator onAppear(ViewGroup paramViewGroup, TransitionValues paramTransitionValues1, int paramInt1, TransitionValues paramTransitionValues2, int paramInt2) {
    if ((this.mMode & 0x1) != 1 || paramTransitionValues2 == null)
      return null; 
    if (paramTransitionValues1 == null) {
      View view = (View)paramTransitionValues2.view.getParent();
      TransitionValues transitionValues2 = getMatchedTransitionValues(view, false);
      TransitionValues transitionValues1 = getTransitionValues(view, false);
      VisibilityInfo visibilityInfo = getVisibilityChangeInfo(transitionValues2, transitionValues1);
      if (visibilityInfo.visibilityChange)
        return null; 
    } 
    return onAppear(paramViewGroup, paramTransitionValues2.view, paramTransitionValues1, paramTransitionValues2);
  }
  
  public Animator onAppear(ViewGroup paramViewGroup, View paramView, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    return null;
  }
  
  public Animator onDisappear(ViewGroup paramViewGroup, TransitionValues paramTransitionValues1, int paramInt1, TransitionValues paramTransitionValues2, int paramInt2) {
    Animator animator;
    View view2;
    boolean bool2;
    if ((this.mMode & 0x2) != 2)
      return null; 
    if (paramTransitionValues1 == null)
      return null; 
    View view1 = paramTransitionValues1.view;
    if (paramTransitionValues2 != null) {
      view2 = paramTransitionValues2.view;
    } else {
      view2 = null;
    } 
    View view3 = null;
    View view4 = null, view5 = null;
    boolean bool1 = false;
    View view6 = (View)view1.getTag(16909567);
    if (view6 != null) {
      view2 = view6;
      bool2 = true;
    } else {
      paramInt1 = 0;
      if (view2 == null || view2.getParent() == null) {
        if (view2 != null) {
          view3 = view2;
        } else {
          paramInt1 = 1;
        } 
      } else if (paramInt2 == 4) {
        view5 = view2;
      } else if (view1 == view2) {
        view5 = view2;
      } else {
        paramInt1 = 1;
      } 
      view2 = view3;
      view4 = view5;
      bool2 = bool1;
      if (paramInt1 != 0)
        if (view1.getParent() == null) {
          view2 = view1;
          view4 = view5;
          bool2 = bool1;
        } else {
          view2 = view3;
          view4 = view5;
          bool2 = bool1;
          if (view1.getParent() instanceof View) {
            view2 = (View)view1.getParent();
            TransitionValues transitionValues2 = getTransitionValues(view2, true);
            TransitionValues transitionValues1 = getMatchedTransitionValues(view2, true);
            VisibilityInfo visibilityInfo = getVisibilityChangeInfo(transitionValues2, transitionValues1);
            if (!visibilityInfo.visibilityChange) {
              view2 = TransitionUtils.copyViewImage(paramViewGroup, view1, view2);
              View view = view5;
              bool2 = bool1;
            } else {
              paramInt1 = view2.getId();
              if (view2.getParent() == null) {
                view2 = view3;
                View view = view5;
                bool2 = bool1;
                if (paramInt1 != -1) {
                  view2 = view3;
                  view = view5;
                  bool2 = bool1;
                  if (paramViewGroup.findViewById(paramInt1) != null) {
                    view2 = view3;
                    view = view5;
                    bool2 = bool1;
                    if (this.mCanRemoveViews) {
                      view2 = view1;
                      view = view5;
                      bool2 = bool1;
                    } 
                  } 
                } 
              } else {
                bool2 = bool1;
                view4 = view5;
                view2 = view3;
              } 
            } 
          } 
        }  
    } 
    if (view2 != null) {
      if (!bool2) {
        ViewGroupOverlay viewGroupOverlay = paramViewGroup.getOverlay();
        int[] arrayOfInt = (int[])paramTransitionValues1.values.get("android:visibility:screenLocation");
        paramInt1 = arrayOfInt[0];
        paramInt2 = arrayOfInt[1];
        arrayOfInt = new int[2];
        paramViewGroup.getLocationOnScreen(arrayOfInt);
        view2.offsetLeftAndRight(paramInt1 - arrayOfInt[0] - view2.getLeft());
        view2.offsetTopAndBottom(paramInt2 - arrayOfInt[1] - view2.getTop());
        viewGroupOverlay.add(view2);
      } else {
        view5 = null;
      } 
      animator = onDisappear(paramViewGroup, view2, paramTransitionValues1, paramTransitionValues2);
      if (!bool2)
        if (animator == null) {
          view5.remove(view2);
        } else {
          view1.setTagInternal(16909567, view2);
          addListener((Transition.TransitionListener)new Object(this, (ViewGroupOverlay)view5, view2, view1));
        }  
      return animator;
    } 
    if (view4 != null) {
      paramInt1 = view4.getVisibility();
      view4.setTransitionVisibility(0);
      animator = onDisappear((ViewGroup)animator, view4, paramTransitionValues1, paramTransitionValues2);
      if (animator != null) {
        DisappearListener disappearListener = new DisappearListener(view4, paramInt2, this.mSuppressLayout);
        animator.addListener(disappearListener);
        animator.addPauseListener(disappearListener);
        addListener(disappearListener);
      } else {
        view4.setTransitionVisibility(paramInt1);
      } 
      return animator;
    } 
    return null;
  }
  
  public boolean isTransitionRequired(TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    boolean bool = false;
    if (paramTransitionValues1 == null && paramTransitionValues2 == null)
      return false; 
    if (paramTransitionValues1 != null && paramTransitionValues2 != null) {
      Map<String, Object> map = paramTransitionValues2.values;
      boolean bool1 = map.containsKey("android:visibility:visibility");
      map = paramTransitionValues1.values;
      if (bool1 != map.containsKey("android:visibility:visibility"))
        return false; 
    } 
    VisibilityInfo visibilityInfo = getVisibilityChangeInfo(paramTransitionValues1, paramTransitionValues2);
    null = bool;
    if (visibilityInfo.visibilityChange) {
      if (visibilityInfo.startVisibility != 0) {
        null = bool;
        return (visibilityInfo.endVisibility == 0) ? true : null;
      } 
    } else {
      return null;
    } 
    return true;
  }
  
  public Animator onDisappear(ViewGroup paramViewGroup, View paramView, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    return null;
  }
  
  public Visibility() {}
  
  class DisappearListener extends TransitionListenerAdapter implements Animator.AnimatorListener, Animator.AnimatorPauseListener {
    boolean mCanceled = false;
    
    private final int mFinalVisibility;
    
    private boolean mLayoutSuppressed;
    
    private final ViewGroup mParent;
    
    private final boolean mSuppressLayout;
    
    private final View mView;
    
    public DisappearListener(Visibility this$0, int param1Int, boolean param1Boolean) {
      this.mView = (View)this$0;
      this.mFinalVisibility = param1Int;
      this.mParent = (ViewGroup)this$0.getParent();
      this.mSuppressLayout = param1Boolean;
      suppressLayout(true);
    }
    
    public void onAnimationPause(Animator param1Animator) {
      if (!this.mCanceled)
        this.mView.setTransitionVisibility(this.mFinalVisibility); 
    }
    
    public void onAnimationResume(Animator param1Animator) {
      if (!this.mCanceled)
        this.mView.setTransitionVisibility(0); 
    }
    
    public void onAnimationCancel(Animator param1Animator) {
      this.mCanceled = true;
    }
    
    public void onAnimationRepeat(Animator param1Animator) {}
    
    public void onAnimationStart(Animator param1Animator) {}
    
    public void onAnimationEnd(Animator param1Animator) {
      hideViewWhenNotCanceled();
    }
    
    public void onTransitionEnd(Transition param1Transition) {
      hideViewWhenNotCanceled();
      param1Transition.removeListener(this);
    }
    
    public void onTransitionPause(Transition param1Transition) {
      suppressLayout(false);
    }
    
    public void onTransitionResume(Transition param1Transition) {
      suppressLayout(true);
    }
    
    private void hideViewWhenNotCanceled() {
      if (!this.mCanceled) {
        this.mView.setTransitionVisibility(this.mFinalVisibility);
        ViewGroup viewGroup = this.mParent;
        if (viewGroup != null)
          viewGroup.invalidate(); 
      } 
      suppressLayout(false);
    }
    
    private void suppressLayout(boolean param1Boolean) {
      if (this.mSuppressLayout && this.mLayoutSuppressed != param1Boolean) {
        ViewGroup viewGroup = this.mParent;
        if (viewGroup != null) {
          this.mLayoutSuppressed = param1Boolean;
          viewGroup.suppressLayout(param1Boolean);
        } 
      } 
    }
  }
}
