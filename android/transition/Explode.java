package android.transition;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

public class Explode extends Visibility {
  private static final String PROPNAME_SCREEN_BOUNDS = "android:explode:screenBounds";
  
  private static final String TAG = "Explode";
  
  private static final TimeInterpolator sAccelerate;
  
  private static final TimeInterpolator sDecelerate = new DecelerateInterpolator();
  
  static {
    sAccelerate = new AccelerateInterpolator();
  }
  
  private int[] mTempLoc = new int[2];
  
  public Explode() {
    setPropagation(new CircularPropagation());
  }
  
  public Explode(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    setPropagation(new CircularPropagation());
  }
  
  private void captureValues(TransitionValues paramTransitionValues) {
    View view = paramTransitionValues.view;
    view.getLocationOnScreen(this.mTempLoc);
    int arrayOfInt[] = this.mTempLoc, i = arrayOfInt[0];
    int j = arrayOfInt[1];
    int k = view.getWidth();
    int m = view.getHeight();
    paramTransitionValues.values.put("android:explode:screenBounds", new Rect(i, j, k + i, m + j));
  }
  
  public void captureStartValues(TransitionValues paramTransitionValues) {
    super.captureStartValues(paramTransitionValues);
    captureValues(paramTransitionValues);
  }
  
  public void captureEndValues(TransitionValues paramTransitionValues) {
    super.captureEndValues(paramTransitionValues);
    captureValues(paramTransitionValues);
  }
  
  public Animator onAppear(ViewGroup paramViewGroup, View paramView, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    if (paramTransitionValues2 == null)
      return null; 
    Rect rect = (Rect)paramTransitionValues2.values.get("android:explode:screenBounds");
    float f1 = paramView.getTranslationX();
    float f2 = paramView.getTranslationY();
    calculateOut(paramViewGroup, rect, this.mTempLoc);
    int[] arrayOfInt = this.mTempLoc;
    float f3 = arrayOfInt[0];
    float f4 = arrayOfInt[1];
    return TranslationAnimationCreator.createAnimation(paramView, paramTransitionValues2, rect.left, rect.top, f1 + f3, f2 + f4, f1, f2, sDecelerate, this);
  }
  
  public Animator onDisappear(ViewGroup paramViewGroup, View paramView, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    if (paramTransitionValues1 == null)
      return null; 
    Rect rect = (Rect)paramTransitionValues1.values.get("android:explode:screenBounds");
    int i = rect.left;
    int j = rect.top;
    float f1 = paramView.getTranslationX();
    float f2 = paramView.getTranslationY();
    float f3 = f1;
    float f4 = f2;
    int[] arrayOfInt2 = (int[])paramTransitionValues1.view.getTag(16909565);
    float f5 = f3, f6 = f4;
    if (arrayOfInt2 != null) {
      f5 = f3 + (arrayOfInt2[0] - rect.left);
      f6 = f4 + (arrayOfInt2[1] - rect.top);
      rect.offsetTo(arrayOfInt2[0], arrayOfInt2[1]);
    } 
    calculateOut(paramViewGroup, rect, this.mTempLoc);
    int[] arrayOfInt1 = this.mTempLoc;
    f3 = arrayOfInt1[0];
    f4 = arrayOfInt1[1];
    return TranslationAnimationCreator.createAnimation(paramView, paramTransitionValues1, i, j, f1, f2, f5 + f3, f6 + f4, sAccelerate, this);
  }
  
  private void calculateOut(View paramView, Rect paramRect, int[] paramArrayOfint) {
    int k, m;
    paramView.getLocationOnScreen(this.mTempLoc);
    int arrayOfInt[] = this.mTempLoc, i = arrayOfInt[0];
    int j = arrayOfInt[1];
    Rect rect = getEpicenter();
    if (rect == null) {
      k = paramView.getWidth() / 2;
      m = k + i + Math.round(paramView.getTranslationX());
      k = paramView.getHeight() / 2;
      k = k + j + Math.round(paramView.getTranslationY());
    } else {
      m = rect.centerX();
      k = rect.centerY();
    } 
    int n = paramRect.centerX();
    int i1 = paramRect.centerY();
    double d1 = (n - m);
    double d2 = (i1 - k);
    double d3 = d1, d4 = d2;
    if (d1 == 0.0D) {
      d3 = d1;
      d4 = d2;
      if (d2 == 0.0D) {
        d3 = Math.random() * 2.0D - 1.0D;
        d4 = Math.random() * 2.0D - 1.0D;
      } 
    } 
    d2 = Math.hypot(d3, d4);
    d3 /= d2;
    d4 /= d2;
    d2 = calculateMaxDistance(paramView, m - i, k - j);
    paramArrayOfint[0] = (int)Math.round(d2 * d3);
    paramArrayOfint[1] = (int)Math.round(d2 * d4);
  }
  
  private static double calculateMaxDistance(View paramView, int paramInt1, int paramInt2) {
    paramInt1 = Math.max(paramInt1, paramView.getWidth() - paramInt1);
    paramInt2 = Math.max(paramInt2, paramView.getHeight() - paramInt2);
    return Math.hypot(paramInt1, paramInt2);
  }
}
