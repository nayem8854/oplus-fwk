package android.transition;

import android.animation.Animator;
import android.animation.RectEvaluator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;
import android.view.ViewGroup;
import com.android.internal.R;
import java.util.Map;

public class ChangeBounds extends Transition {
  private static final String[] sTransitionProperties = new String[] { "android:changeBounds:bounds", "android:changeBounds:clip", "android:changeBounds:parent", "android:changeBounds:windowX", "android:changeBounds:windowY" };
  
  static {
    DRAWABLE_ORIGIN_PROPERTY = (Property<Drawable, PointF>)new Object(PointF.class, "boundsOrigin");
    TOP_LEFT_PROPERTY = (Property<ViewBounds, PointF>)new Object(PointF.class, "topLeft");
    BOTTOM_RIGHT_PROPERTY = (Property<ViewBounds, PointF>)new Object(PointF.class, "bottomRight");
    BOTTOM_RIGHT_ONLY_PROPERTY = (Property<View, PointF>)new Object(PointF.class, "bottomRight");
    TOP_LEFT_ONLY_PROPERTY = new Property<View, PointF>(PointF.class, "topLeft") {
        public void set(View param1View, PointF param1PointF) {
          int i = Math.round(param1PointF.x);
          int j = Math.round(param1PointF.y);
          int k = param1View.getRight();
          int m = param1View.getBottom();
          param1View.setLeftTopRightBottom(i, j, k, m);
        }
        
        public PointF get(View param1View) {
          return null;
        }
      };
    POSITION_PROPERTY = new Property<View, PointF>(PointF.class, "position") {
        public void set(View param1View, PointF param1PointF) {
          int i = Math.round(param1PointF.x);
          int j = Math.round(param1PointF.y);
          int k = param1View.getWidth();
          int m = param1View.getHeight();
          param1View.setLeftTopRightBottom(i, j, k + i, m + j);
        }
        
        public PointF get(View param1View) {
          return null;
        }
      };
    sRectEvaluator = new RectEvaluator();
  }
  
  int[] tempLocation = new int[2];
  
  boolean mResizeClip = false;
  
  boolean mReparent = false;
  
  private static final Property<View, PointF> BOTTOM_RIGHT_ONLY_PROPERTY;
  
  private static final Property<ViewBounds, PointF> BOTTOM_RIGHT_PROPERTY;
  
  private static final Property<Drawable, PointF> DRAWABLE_ORIGIN_PROPERTY;
  
  private static final String LOG_TAG = "ChangeBounds";
  
  private static final Property<View, PointF> POSITION_PROPERTY;
  
  private static final String PROPNAME_BOUNDS = "android:changeBounds:bounds";
  
  private static final String PROPNAME_CLIP = "android:changeBounds:clip";
  
  private static final String PROPNAME_PARENT = "android:changeBounds:parent";
  
  private static final String PROPNAME_WINDOW_X = "android:changeBounds:windowX";
  
  private static final String PROPNAME_WINDOW_Y = "android:changeBounds:windowY";
  
  private static final Property<View, PointF> TOP_LEFT_ONLY_PROPERTY;
  
  private static final Property<ViewBounds, PointF> TOP_LEFT_PROPERTY;
  
  private static RectEvaluator sRectEvaluator;
  
  public ChangeBounds(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ChangeBounds);
    boolean bool = typedArray.getBoolean(0, false);
    typedArray.recycle();
    setResizeClip(bool);
  }
  
  public String[] getTransitionProperties() {
    return sTransitionProperties;
  }
  
  public void setResizeClip(boolean paramBoolean) {
    this.mResizeClip = paramBoolean;
  }
  
  public boolean getResizeClip() {
    return this.mResizeClip;
  }
  
  @Deprecated
  public void setReparent(boolean paramBoolean) {
    this.mReparent = paramBoolean;
  }
  
  private void captureValues(TransitionValues paramTransitionValues) {
    View view = paramTransitionValues.view;
    if (view.isLaidOut() || view.getWidth() != 0 || view.getHeight() != 0) {
      Map<String, Object> map = paramTransitionValues.values;
      int i = view.getLeft(), j = view.getTop();
      Rect rect = new Rect(i, j, view.getRight(), view.getBottom());
      map.put("android:changeBounds:bounds", rect);
      paramTransitionValues.values.put("android:changeBounds:parent", paramTransitionValues.view.getParent());
      if (this.mReparent) {
        paramTransitionValues.view.getLocationInWindow(this.tempLocation);
        paramTransitionValues.values.put("android:changeBounds:windowX", Integer.valueOf(this.tempLocation[0]));
        paramTransitionValues.values.put("android:changeBounds:windowY", Integer.valueOf(this.tempLocation[1]));
      } 
      if (this.mResizeClip)
        paramTransitionValues.values.put("android:changeBounds:clip", view.getClipBounds()); 
    } 
  }
  
  public void captureStartValues(TransitionValues paramTransitionValues) {
    captureValues(paramTransitionValues);
  }
  
  public void captureEndValues(TransitionValues paramTransitionValues) {
    captureValues(paramTransitionValues);
  }
  
  private boolean parentMatches(View paramView1, View paramView2) {
    boolean bool = true;
    if (this.mReparent) {
      bool = true;
      boolean bool1 = true;
      TransitionValues transitionValues = getMatchedTransitionValues(paramView1, true);
      if (transitionValues == null) {
        if (paramView1 == paramView2) {
          bool = bool1;
        } else {
          bool = false;
        } 
      } else if (paramView2 != transitionValues.view) {
        bool = false;
      } 
    } 
    return bool;
  }
  
  public Animator createAnimator(ViewGroup paramViewGroup, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    // Byte code:
    //   0: aload_2
    //   1: ifnull -> 1159
    //   4: aload_3
    //   5: ifnonnull -> 11
    //   8: goto -> 1159
    //   11: aload_2
    //   12: getfield values : Ljava/util/Map;
    //   15: astore #4
    //   17: aload_3
    //   18: getfield values : Ljava/util/Map;
    //   21: astore #5
    //   23: aload #4
    //   25: ldc 'android:changeBounds:parent'
    //   27: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   32: checkcast android/view/ViewGroup
    //   35: astore #4
    //   37: aload #5
    //   39: ldc 'android:changeBounds:parent'
    //   41: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   46: checkcast android/view/ViewGroup
    //   49: astore #5
    //   51: aload #4
    //   53: ifnull -> 1157
    //   56: aload #5
    //   58: ifnonnull -> 64
    //   61: goto -> 1157
    //   64: aload_3
    //   65: getfield view : Landroid/view/View;
    //   68: astore #6
    //   70: aload_0
    //   71: aload #4
    //   73: aload #5
    //   75: invokespecial parentMatches : (Landroid/view/View;Landroid/view/View;)Z
    //   78: ifeq -> 876
    //   81: aload_2
    //   82: getfield values : Ljava/util/Map;
    //   85: ldc 'android:changeBounds:bounds'
    //   87: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   92: checkcast android/graphics/Rect
    //   95: astore #4
    //   97: aload_3
    //   98: getfield values : Ljava/util/Map;
    //   101: ldc 'android:changeBounds:bounds'
    //   103: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   108: checkcast android/graphics/Rect
    //   111: astore_1
    //   112: aload #4
    //   114: getfield left : I
    //   117: istore #7
    //   119: aload_1
    //   120: getfield left : I
    //   123: istore #8
    //   125: aload #4
    //   127: getfield top : I
    //   130: istore #9
    //   132: aload_1
    //   133: getfield top : I
    //   136: istore #10
    //   138: aload #4
    //   140: getfield right : I
    //   143: istore #11
    //   145: aload_1
    //   146: getfield right : I
    //   149: istore #12
    //   151: aload #4
    //   153: getfield bottom : I
    //   156: istore #13
    //   158: aload_1
    //   159: getfield bottom : I
    //   162: istore #14
    //   164: iload #11
    //   166: iload #7
    //   168: isub
    //   169: istore #15
    //   171: iload #13
    //   173: iload #9
    //   175: isub
    //   176: istore #16
    //   178: iload #12
    //   180: iload #8
    //   182: isub
    //   183: istore #17
    //   185: iload #14
    //   187: iload #10
    //   189: isub
    //   190: istore #18
    //   192: aload_2
    //   193: getfield values : Ljava/util/Map;
    //   196: ldc 'android:changeBounds:clip'
    //   198: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   203: checkcast android/graphics/Rect
    //   206: astore_2
    //   207: aload_3
    //   208: getfield values : Ljava/util/Map;
    //   211: ldc 'android:changeBounds:clip'
    //   213: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   218: checkcast android/graphics/Rect
    //   221: astore #4
    //   223: iconst_0
    //   224: istore #19
    //   226: iconst_0
    //   227: istore #20
    //   229: iload #15
    //   231: ifeq -> 239
    //   234: iload #16
    //   236: ifne -> 257
    //   239: iload #19
    //   241: istore #21
    //   243: iload #17
    //   245: ifeq -> 300
    //   248: iload #19
    //   250: istore #21
    //   252: iload #18
    //   254: ifeq -> 300
    //   257: iload #7
    //   259: iload #8
    //   261: if_icmpne -> 271
    //   264: iload #9
    //   266: iload #10
    //   268: if_icmpeq -> 276
    //   271: iconst_0
    //   272: iconst_1
    //   273: iadd
    //   274: istore #20
    //   276: iload #11
    //   278: iload #12
    //   280: if_icmpne -> 294
    //   283: iload #20
    //   285: istore #21
    //   287: iload #13
    //   289: iload #14
    //   291: if_icmpeq -> 300
    //   294: iload #20
    //   296: iconst_1
    //   297: iadd
    //   298: istore #21
    //   300: aload_2
    //   301: ifnull -> 313
    //   304: aload_2
    //   305: aload #4
    //   307: invokevirtual equals : (Ljava/lang/Object;)Z
    //   310: ifeq -> 330
    //   313: iload #21
    //   315: istore #20
    //   317: aload_2
    //   318: ifnonnull -> 336
    //   321: iload #21
    //   323: istore #20
    //   325: aload #4
    //   327: ifnull -> 336
    //   330: iload #21
    //   332: iconst_1
    //   333: iadd
    //   334: istore #20
    //   336: iload #20
    //   338: ifle -> 873
    //   341: aload #6
    //   343: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   346: instanceof android/view/ViewGroup
    //   349: ifeq -> 385
    //   352: aload #6
    //   354: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   357: checkcast android/view/ViewGroup
    //   360: astore_1
    //   361: aload_1
    //   362: iconst_1
    //   363: invokevirtual suppressLayout : (Z)V
    //   366: new android/transition/ChangeBounds$7
    //   369: dup
    //   370: aload_0
    //   371: aload_1
    //   372: invokespecial <init> : (Landroid/transition/ChangeBounds;Landroid/view/ViewGroup;)V
    //   375: astore_1
    //   376: aload_0
    //   377: aload_1
    //   378: invokevirtual addListener : (Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;
    //   381: pop
    //   382: goto -> 385
    //   385: aload_0
    //   386: getfield mResizeClip : Z
    //   389: ifne -> 658
    //   392: aload #6
    //   394: iload #7
    //   396: iload #9
    //   398: iload #11
    //   400: iload #13
    //   402: invokevirtual setLeftTopRightBottom : (IIII)V
    //   405: iload #20
    //   407: iconst_2
    //   408: if_icmpne -> 573
    //   411: iload #15
    //   413: iload #17
    //   415: if_icmpne -> 459
    //   418: iload #16
    //   420: iload #18
    //   422: if_icmpne -> 459
    //   425: aload_0
    //   426: invokevirtual getPathMotion : ()Landroid/transition/PathMotion;
    //   429: iload #7
    //   431: i2f
    //   432: iload #9
    //   434: i2f
    //   435: iload #8
    //   437: i2f
    //   438: iload #10
    //   440: i2f
    //   441: invokevirtual getPath : (FFFF)Landroid/graphics/Path;
    //   444: astore_1
    //   445: aload #6
    //   447: getstatic android/transition/ChangeBounds.POSITION_PROPERTY : Landroid/util/Property;
    //   450: aconst_null
    //   451: aload_1
    //   452: invokestatic ofObject : (Ljava/lang/Object;Landroid/util/Property;Landroid/animation/TypeConverter;Landroid/graphics/Path;)Landroid/animation/ObjectAnimator;
    //   455: astore_1
    //   456: goto -> 871
    //   459: new android/transition/ChangeBounds$ViewBounds
    //   462: dup
    //   463: aload #6
    //   465: invokespecial <init> : (Landroid/view/View;)V
    //   468: astore_2
    //   469: aload_0
    //   470: invokevirtual getPathMotion : ()Landroid/transition/PathMotion;
    //   473: iload #7
    //   475: i2f
    //   476: iload #9
    //   478: i2f
    //   479: iload #8
    //   481: i2f
    //   482: iload #10
    //   484: i2f
    //   485: invokevirtual getPath : (FFFF)Landroid/graphics/Path;
    //   488: astore_1
    //   489: getstatic android/transition/ChangeBounds.TOP_LEFT_PROPERTY : Landroid/util/Property;
    //   492: astore_3
    //   493: aload_2
    //   494: aload_3
    //   495: aconst_null
    //   496: aload_1
    //   497: invokestatic ofObject : (Ljava/lang/Object;Landroid/util/Property;Landroid/animation/TypeConverter;Landroid/graphics/Path;)Landroid/animation/ObjectAnimator;
    //   500: astore_3
    //   501: aload_0
    //   502: invokevirtual getPathMotion : ()Landroid/transition/PathMotion;
    //   505: iload #11
    //   507: i2f
    //   508: iload #13
    //   510: i2f
    //   511: iload #12
    //   513: i2f
    //   514: iload #14
    //   516: i2f
    //   517: invokevirtual getPath : (FFFF)Landroid/graphics/Path;
    //   520: astore_1
    //   521: aload_2
    //   522: getstatic android/transition/ChangeBounds.BOTTOM_RIGHT_PROPERTY : Landroid/util/Property;
    //   525: aconst_null
    //   526: aload_1
    //   527: invokestatic ofObject : (Ljava/lang/Object;Landroid/util/Property;Landroid/animation/TypeConverter;Landroid/graphics/Path;)Landroid/animation/ObjectAnimator;
    //   530: astore #4
    //   532: new android/animation/AnimatorSet
    //   535: dup
    //   536: invokespecial <init> : ()V
    //   539: astore_1
    //   540: aload_1
    //   541: iconst_2
    //   542: anewarray android/animation/Animator
    //   545: dup
    //   546: iconst_0
    //   547: aload_3
    //   548: aastore
    //   549: dup
    //   550: iconst_1
    //   551: aload #4
    //   553: aastore
    //   554: invokevirtual playTogether : ([Landroid/animation/Animator;)V
    //   557: aload_1
    //   558: new android/transition/ChangeBounds$8
    //   561: dup
    //   562: aload_0
    //   563: aload_2
    //   564: invokespecial <init> : (Landroid/transition/ChangeBounds;Landroid/transition/ChangeBounds$ViewBounds;)V
    //   567: invokevirtual addListener : (Landroid/animation/Animator$AnimatorListener;)V
    //   570: goto -> 871
    //   573: iload #7
    //   575: iload #8
    //   577: if_icmpne -> 624
    //   580: iload #9
    //   582: iload #10
    //   584: if_icmpeq -> 590
    //   587: goto -> 624
    //   590: aload_0
    //   591: invokevirtual getPathMotion : ()Landroid/transition/PathMotion;
    //   594: iload #11
    //   596: i2f
    //   597: iload #13
    //   599: i2f
    //   600: iload #12
    //   602: i2f
    //   603: iload #14
    //   605: i2f
    //   606: invokevirtual getPath : (FFFF)Landroid/graphics/Path;
    //   609: astore_1
    //   610: aload #6
    //   612: getstatic android/transition/ChangeBounds.BOTTOM_RIGHT_ONLY_PROPERTY : Landroid/util/Property;
    //   615: aconst_null
    //   616: aload_1
    //   617: invokestatic ofObject : (Ljava/lang/Object;Landroid/util/Property;Landroid/animation/TypeConverter;Landroid/graphics/Path;)Landroid/animation/ObjectAnimator;
    //   620: astore_1
    //   621: goto -> 871
    //   624: aload_0
    //   625: invokevirtual getPathMotion : ()Landroid/transition/PathMotion;
    //   628: iload #7
    //   630: i2f
    //   631: iload #9
    //   633: i2f
    //   634: iload #8
    //   636: i2f
    //   637: iload #10
    //   639: i2f
    //   640: invokevirtual getPath : (FFFF)Landroid/graphics/Path;
    //   643: astore_1
    //   644: aload #6
    //   646: getstatic android/transition/ChangeBounds.TOP_LEFT_ONLY_PROPERTY : Landroid/util/Property;
    //   649: aconst_null
    //   650: aload_1
    //   651: invokestatic ofObject : (Ljava/lang/Object;Landroid/util/Property;Landroid/animation/TypeConverter;Landroid/graphics/Path;)Landroid/animation/ObjectAnimator;
    //   654: astore_1
    //   655: goto -> 871
    //   658: iload #15
    //   660: iload #17
    //   662: invokestatic max : (II)I
    //   665: istore #20
    //   667: iload #16
    //   669: iload #18
    //   671: invokestatic max : (II)I
    //   674: istore #21
    //   676: aload #6
    //   678: iload #7
    //   680: iload #9
    //   682: iload #7
    //   684: iload #20
    //   686: iadd
    //   687: iload #9
    //   689: iload #21
    //   691: iadd
    //   692: invokevirtual setLeftTopRightBottom : (IIII)V
    //   695: iload #7
    //   697: iload #8
    //   699: if_icmpne -> 717
    //   702: iload #9
    //   704: iload #10
    //   706: if_icmpeq -> 712
    //   709: goto -> 717
    //   712: aconst_null
    //   713: astore_1
    //   714: goto -> 748
    //   717: aload_0
    //   718: invokevirtual getPathMotion : ()Landroid/transition/PathMotion;
    //   721: iload #7
    //   723: i2f
    //   724: iload #9
    //   726: i2f
    //   727: iload #8
    //   729: i2f
    //   730: iload #10
    //   732: i2f
    //   733: invokevirtual getPath : (FFFF)Landroid/graphics/Path;
    //   736: astore_1
    //   737: aload #6
    //   739: getstatic android/transition/ChangeBounds.POSITION_PROPERTY : Landroid/util/Property;
    //   742: aconst_null
    //   743: aload_1
    //   744: invokestatic ofObject : (Ljava/lang/Object;Landroid/util/Property;Landroid/animation/TypeConverter;Landroid/graphics/Path;)Landroid/animation/ObjectAnimator;
    //   747: astore_1
    //   748: aload_2
    //   749: ifnonnull -> 769
    //   752: new android/graphics/Rect
    //   755: dup
    //   756: iconst_0
    //   757: iconst_0
    //   758: iload #15
    //   760: iload #16
    //   762: invokespecial <init> : (IIII)V
    //   765: astore_2
    //   766: goto -> 769
    //   769: aload #4
    //   771: ifnonnull -> 791
    //   774: new android/graphics/Rect
    //   777: dup
    //   778: iconst_0
    //   779: iconst_0
    //   780: iload #17
    //   782: iload #18
    //   784: invokespecial <init> : (IIII)V
    //   787: astore_3
    //   788: goto -> 794
    //   791: aload #4
    //   793: astore_3
    //   794: aconst_null
    //   795: astore #5
    //   797: aload_2
    //   798: aload_3
    //   799: invokevirtual equals : (Ljava/lang/Object;)Z
    //   802: ifne -> 862
    //   805: aload #6
    //   807: aload_2
    //   808: invokevirtual setClipBounds : (Landroid/graphics/Rect;)V
    //   811: aload #6
    //   813: ldc_w 'clipBounds'
    //   816: getstatic android/transition/ChangeBounds.sRectEvaluator : Landroid/animation/RectEvaluator;
    //   819: iconst_2
    //   820: anewarray java/lang/Object
    //   823: dup
    //   824: iconst_0
    //   825: aload_2
    //   826: aastore
    //   827: dup
    //   828: iconst_1
    //   829: aload_3
    //   830: aastore
    //   831: invokestatic ofObject : (Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;
    //   834: astore_2
    //   835: aload_2
    //   836: new android/transition/ChangeBounds$9
    //   839: dup
    //   840: aload_0
    //   841: aload #6
    //   843: aload #4
    //   845: iload #8
    //   847: iload #10
    //   849: iload #12
    //   851: iload #14
    //   853: invokespecial <init> : (Landroid/transition/ChangeBounds;Landroid/view/View;Landroid/graphics/Rect;IIII)V
    //   856: invokevirtual addListener : (Landroid/animation/Animator$AnimatorListener;)V
    //   859: goto -> 865
    //   862: aload #5
    //   864: astore_2
    //   865: aload_1
    //   866: aload_2
    //   867: invokestatic mergeAnimators : (Landroid/animation/Animator;Landroid/animation/Animator;)Landroid/animation/Animator;
    //   870: astore_1
    //   871: aload_1
    //   872: areturn
    //   873: goto -> 1005
    //   876: aload_1
    //   877: aload_0
    //   878: getfield tempLocation : [I
    //   881: invokevirtual getLocationInWindow : ([I)V
    //   884: aload_2
    //   885: getfield values : Ljava/util/Map;
    //   888: ldc 'android:changeBounds:windowX'
    //   890: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   895: checkcast java/lang/Integer
    //   898: invokevirtual intValue : ()I
    //   901: aload_0
    //   902: getfield tempLocation : [I
    //   905: iconst_0
    //   906: iaload
    //   907: isub
    //   908: istore #10
    //   910: aload_2
    //   911: getfield values : Ljava/util/Map;
    //   914: ldc 'android:changeBounds:windowY'
    //   916: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   921: checkcast java/lang/Integer
    //   924: invokevirtual intValue : ()I
    //   927: aload_0
    //   928: getfield tempLocation : [I
    //   931: iconst_1
    //   932: iaload
    //   933: isub
    //   934: istore #20
    //   936: aload_3
    //   937: getfield values : Ljava/util/Map;
    //   940: ldc 'android:changeBounds:windowX'
    //   942: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   947: checkcast java/lang/Integer
    //   950: invokevirtual intValue : ()I
    //   953: aload_0
    //   954: getfield tempLocation : [I
    //   957: iconst_0
    //   958: iaload
    //   959: isub
    //   960: istore #21
    //   962: aload_3
    //   963: getfield values : Ljava/util/Map;
    //   966: ldc 'android:changeBounds:windowY'
    //   968: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   973: checkcast java/lang/Integer
    //   976: invokevirtual intValue : ()I
    //   979: aload_0
    //   980: getfield tempLocation : [I
    //   983: iconst_1
    //   984: iaload
    //   985: isub
    //   986: istore #19
    //   988: iload #10
    //   990: iload #21
    //   992: if_icmpne -> 1007
    //   995: iload #20
    //   997: iload #19
    //   999: if_icmpeq -> 1005
    //   1002: goto -> 1007
    //   1005: aconst_null
    //   1006: areturn
    //   1007: aload #6
    //   1009: invokevirtual getWidth : ()I
    //   1012: istore #15
    //   1014: aload #6
    //   1016: invokevirtual getHeight : ()I
    //   1019: istore #7
    //   1021: iload #15
    //   1023: iload #7
    //   1025: getstatic android/graphics/Bitmap$Config.ARGB_8888 : Landroid/graphics/Bitmap$Config;
    //   1028: invokestatic createBitmap : (IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    //   1031: astore_3
    //   1032: new android/graphics/Canvas
    //   1035: dup
    //   1036: aload_3
    //   1037: invokespecial <init> : (Landroid/graphics/Bitmap;)V
    //   1040: astore_2
    //   1041: aload #6
    //   1043: aload_2
    //   1044: invokevirtual draw : (Landroid/graphics/Canvas;)V
    //   1047: new android/graphics/drawable/BitmapDrawable
    //   1050: dup
    //   1051: aload_3
    //   1052: invokespecial <init> : (Landroid/graphics/Bitmap;)V
    //   1055: astore_2
    //   1056: aload_2
    //   1057: iload #10
    //   1059: iload #20
    //   1061: iload #10
    //   1063: iload #15
    //   1065: iadd
    //   1066: iload #20
    //   1068: iload #7
    //   1070: iadd
    //   1071: invokevirtual setBounds : (IIII)V
    //   1074: aload #6
    //   1076: invokevirtual getTransitionAlpha : ()F
    //   1079: fstore #22
    //   1081: aload #6
    //   1083: fconst_0
    //   1084: invokevirtual setTransitionAlpha : (F)V
    //   1087: aload_1
    //   1088: invokevirtual getOverlay : ()Landroid/view/ViewGroupOverlay;
    //   1091: aload_2
    //   1092: invokevirtual add : (Landroid/graphics/drawable/Drawable;)V
    //   1095: aload_0
    //   1096: invokevirtual getPathMotion : ()Landroid/transition/PathMotion;
    //   1099: iload #10
    //   1101: i2f
    //   1102: iload #20
    //   1104: i2f
    //   1105: iload #21
    //   1107: i2f
    //   1108: iload #19
    //   1110: i2f
    //   1111: invokevirtual getPath : (FFFF)Landroid/graphics/Path;
    //   1114: astore_3
    //   1115: getstatic android/transition/ChangeBounds.DRAWABLE_ORIGIN_PROPERTY : Landroid/util/Property;
    //   1118: aconst_null
    //   1119: aload_3
    //   1120: invokestatic ofObject : (Landroid/util/Property;Landroid/animation/TypeConverter;Landroid/graphics/Path;)Landroid/animation/PropertyValuesHolder;
    //   1123: astore_3
    //   1124: aload_2
    //   1125: iconst_1
    //   1126: anewarray android/animation/PropertyValuesHolder
    //   1129: dup
    //   1130: iconst_0
    //   1131: aload_3
    //   1132: aastore
    //   1133: invokestatic ofPropertyValuesHolder : (Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;
    //   1136: astore_3
    //   1137: aload_3
    //   1138: new android/transition/ChangeBounds$10
    //   1141: dup
    //   1142: aload_0
    //   1143: aload_1
    //   1144: aload_2
    //   1145: aload #6
    //   1147: fload #22
    //   1149: invokespecial <init> : (Landroid/transition/ChangeBounds;Landroid/view/ViewGroup;Landroid/graphics/drawable/BitmapDrawable;Landroid/view/View;F)V
    //   1152: invokevirtual addListener : (Landroid/animation/Animator$AnimatorListener;)V
    //   1155: aload_3
    //   1156: areturn
    //   1157: aconst_null
    //   1158: areturn
    //   1159: aconst_null
    //   1160: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #280	-> 0
    //   #283	-> 11
    //   #284	-> 17
    //   #285	-> 23
    //   #286	-> 37
    //   #287	-> 51
    //   #290	-> 64
    //   #291	-> 70
    //   #292	-> 81
    //   #293	-> 97
    //   #294	-> 112
    //   #295	-> 119
    //   #296	-> 125
    //   #297	-> 132
    //   #298	-> 138
    //   #299	-> 145
    //   #300	-> 151
    //   #301	-> 158
    //   #302	-> 164
    //   #303	-> 171
    //   #304	-> 178
    //   #305	-> 185
    //   #306	-> 192
    //   #307	-> 207
    //   #308	-> 223
    //   #309	-> 229
    //   #310	-> 257
    //   #311	-> 276
    //   #313	-> 300
    //   #315	-> 330
    //   #317	-> 336
    //   #318	-> 341
    //   #319	-> 352
    //   #320	-> 361
    //   #321	-> 366
    //   #348	-> 376
    //   #318	-> 385
    //   #351	-> 385
    //   #352	-> 392
    //   #353	-> 405
    //   #354	-> 411
    //   #355	-> 425
    //   #357	-> 445
    //   #359	-> 456
    //   #354	-> 459
    //   #360	-> 459
    //   #361	-> 469
    //   #363	-> 489
    //   #364	-> 493
    //   #366	-> 501
    //   #368	-> 521
    //   #370	-> 532
    //   #371	-> 540
    //   #372	-> 557
    //   #373	-> 557
    //   #378	-> 570
    //   #379	-> 573
    //   #385	-> 590
    //   #387	-> 610
    //   #389	-> 621
    //   #379	-> 624
    //   #380	-> 624
    //   #382	-> 644
    //   #384	-> 655
    //   #391	-> 658
    //   #392	-> 667
    //   #394	-> 676
    //   #397	-> 695
    //   #398	-> 695
    //   #399	-> 717
    //   #401	-> 737
    //   #404	-> 748
    //   #405	-> 748
    //   #406	-> 752
    //   #405	-> 769
    //   #408	-> 769
    //   #409	-> 774
    //   #408	-> 791
    //   #411	-> 794
    //   #412	-> 797
    //   #413	-> 805
    //   #414	-> 811
    //   #416	-> 835
    //   #412	-> 862
    //   #434	-> 865
    //   #437	-> 871
    //   #317	-> 873
    //   #439	-> 873
    //   #440	-> 876
    //   #441	-> 884
    //   #442	-> 910
    //   #443	-> 936
    //   #444	-> 962
    //   #446	-> 988
    //   #471	-> 1005
    //   #447	-> 1007
    //   #448	-> 1014
    //   #449	-> 1021
    //   #450	-> 1032
    //   #451	-> 1041
    //   #452	-> 1047
    //   #453	-> 1056
    //   #454	-> 1074
    //   #455	-> 1081
    //   #456	-> 1087
    //   #457	-> 1095
    //   #458	-> 1115
    //   #460	-> 1124
    //   #461	-> 1137
    //   #468	-> 1155
    //   #287	-> 1157
    //   #288	-> 1157
    //   #280	-> 1159
    //   #281	-> 1159
  }
  
  public ChangeBounds() {}
  
  class ViewBounds {
    private int mBottom;
    
    private int mBottomRightCalls;
    
    private int mLeft;
    
    private int mRight;
    
    private int mTop;
    
    private int mTopLeftCalls;
    
    private View mView;
    
    public ViewBounds(ChangeBounds this$0) {
      this.mView = (View)this$0;
    }
    
    public void setTopLeft(PointF param1PointF) {
      this.mLeft = Math.round(param1PointF.x);
      this.mTop = Math.round(param1PointF.y);
      int i = this.mTopLeftCalls + 1;
      if (i == this.mBottomRightCalls)
        setLeftTopRightBottom(); 
    }
    
    public void setBottomRight(PointF param1PointF) {
      this.mRight = Math.round(param1PointF.x);
      this.mBottom = Math.round(param1PointF.y);
      int i = this.mBottomRightCalls + 1;
      if (this.mTopLeftCalls == i)
        setLeftTopRightBottom(); 
    }
    
    private void setLeftTopRightBottom() {
      this.mView.setLeftTopRightBottom(this.mLeft, this.mTop, this.mRight, this.mBottom);
      this.mTopLeftCalls = 0;
      this.mBottomRightCalls = 0;
    }
  }
}
