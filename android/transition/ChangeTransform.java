package android.transition;

import android.animation.Animator;
import android.animation.FloatArrayEvaluator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.TypeEvaluator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.Property;
import android.view.GhostView;
import android.view.View;
import android.view.ViewGroup;
import com.android.internal.R;
import java.util.Map;

public class ChangeTransform extends Transition {
  private static final String[] sTransitionProperties = new String[] { "android:changeTransform:matrix", "android:changeTransform:transforms", "android:changeTransform:parentMatrix" };
  
  static {
    NON_TRANSLATIONS_PROPERTY = new Property<PathAnimatorMatrix, float[]>(float[].class, "nonTranslations") {
        public float[] get(ChangeTransform.PathAnimatorMatrix param1PathAnimatorMatrix) {
          return null;
        }
        
        public void set(ChangeTransform.PathAnimatorMatrix param1PathAnimatorMatrix, float[] param1ArrayOffloat) {
          param1PathAnimatorMatrix.setValues(param1ArrayOffloat);
        }
      };
    TRANSLATIONS_PROPERTY = new Property<PathAnimatorMatrix, PointF>(PointF.class, "translations") {
        public PointF get(ChangeTransform.PathAnimatorMatrix param1PathAnimatorMatrix) {
          return null;
        }
        
        public void set(ChangeTransform.PathAnimatorMatrix param1PathAnimatorMatrix, PointF param1PointF) {
          param1PathAnimatorMatrix.setTranslation(param1PointF);
        }
      };
  }
  
  private boolean mUseOverlay = true;
  
  private boolean mReparent = true;
  
  private Matrix mTempMatrix = new Matrix();
  
  private static final Property<PathAnimatorMatrix, float[]> NON_TRANSLATIONS_PROPERTY;
  
  private static final String PROPNAME_INTERMEDIATE_MATRIX = "android:changeTransform:intermediateMatrix";
  
  private static final String PROPNAME_INTERMEDIATE_PARENT_MATRIX = "android:changeTransform:intermediateParentMatrix";
  
  private static final String PROPNAME_MATRIX = "android:changeTransform:matrix";
  
  private static final String PROPNAME_PARENT = "android:changeTransform:parent";
  
  private static final String PROPNAME_PARENT_MATRIX = "android:changeTransform:parentMatrix";
  
  private static final String PROPNAME_TRANSFORMS = "android:changeTransform:transforms";
  
  private static final String TAG = "ChangeTransform";
  
  private static final Property<PathAnimatorMatrix, PointF> TRANSLATIONS_PROPERTY;
  
  public ChangeTransform(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ChangeTransform);
    this.mUseOverlay = typedArray.getBoolean(1, true);
    this.mReparent = typedArray.getBoolean(0, true);
    typedArray.recycle();
  }
  
  public boolean getReparentWithOverlay() {
    return this.mUseOverlay;
  }
  
  public void setReparentWithOverlay(boolean paramBoolean) {
    this.mUseOverlay = paramBoolean;
  }
  
  public boolean getReparent() {
    return this.mReparent;
  }
  
  public void setReparent(boolean paramBoolean) {
    this.mReparent = paramBoolean;
  }
  
  public String[] getTransitionProperties() {
    return sTransitionProperties;
  }
  
  private void captureValues(TransitionValues paramTransitionValues) {
    View view = paramTransitionValues.view;
    if (view.getVisibility() == 8)
      return; 
    paramTransitionValues.values.put("android:changeTransform:parent", view.getParent());
    Transforms transforms = new Transforms(view);
    paramTransitionValues.values.put("android:changeTransform:transforms", transforms);
    Matrix matrix = view.getMatrix();
    if (matrix == null || matrix.isIdentity()) {
      matrix = null;
    } else {
      matrix = new Matrix(matrix);
    } 
    paramTransitionValues.values.put("android:changeTransform:matrix", matrix);
    if (this.mReparent) {
      matrix = new Matrix();
      ViewGroup viewGroup = (ViewGroup)view.getParent();
      viewGroup.transformMatrixToGlobal(matrix);
      matrix.preTranslate(-viewGroup.getScrollX(), -viewGroup.getScrollY());
      paramTransitionValues.values.put("android:changeTransform:parentMatrix", matrix);
      Map<String, Object> map2 = paramTransitionValues.values;
      Object object = view.getTag(16909566);
      map2.put("android:changeTransform:intermediateMatrix", object);
      Map<String, Object> map1 = paramTransitionValues.values;
      object = view.getTag(16909278);
      map1.put("android:changeTransform:intermediateParentMatrix", object);
    } 
  }
  
  public void captureStartValues(TransitionValues paramTransitionValues) {
    captureValues(paramTransitionValues);
  }
  
  public void captureEndValues(TransitionValues paramTransitionValues) {
    captureValues(paramTransitionValues);
  }
  
  public Animator createAnimator(ViewGroup paramViewGroup, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    if (paramTransitionValues1 != null && paramTransitionValues2 != null) {
      Map<String, Object> map = paramTransitionValues1.values;
      if (map.containsKey("android:changeTransform:parent")) {
        map = paramTransitionValues2.values;
        if (map.containsKey("android:changeTransform:parent")) {
          boolean bool;
          ViewGroup viewGroup1 = (ViewGroup)paramTransitionValues1.values.get("android:changeTransform:parent");
          ViewGroup viewGroup2 = (ViewGroup)paramTransitionValues2.values.get("android:changeTransform:parent");
          if (this.mReparent && !parentsMatch(viewGroup1, viewGroup2)) {
            bool = true;
          } else {
            bool = false;
          } 
          Matrix matrix2 = (Matrix)paramTransitionValues1.values.get("android:changeTransform:intermediateMatrix");
          if (matrix2 != null)
            paramTransitionValues1.values.put("android:changeTransform:matrix", matrix2); 
          Map<String, Object> map1 = paramTransitionValues1.values;
          Matrix matrix1 = (Matrix)map1.get("android:changeTransform:intermediateParentMatrix");
          if (matrix1 != null)
            paramTransitionValues1.values.put("android:changeTransform:parentMatrix", matrix1); 
          if (bool)
            setMatricesForParent(paramTransitionValues1, paramTransitionValues2); 
          ObjectAnimator objectAnimator = createTransformAnimator(paramTransitionValues1, paramTransitionValues2, bool);
          if (bool && objectAnimator != null && this.mUseOverlay)
            createGhostView(paramViewGroup, paramTransitionValues1, paramTransitionValues2); 
          return (Animator)objectAnimator;
        } 
      } 
    } 
    return null;
  }
  
  private ObjectAnimator createTransformAnimator(TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2, boolean paramBoolean) {
    Matrix matrix1 = (Matrix)paramTransitionValues1.values.get("android:changeTransform:matrix");
    Matrix matrix2 = (Matrix)paramTransitionValues2.values.get("android:changeTransform:matrix");
    Matrix matrix3 = matrix1;
    if (matrix1 == null)
      matrix3 = Matrix.IDENTITY_MATRIX; 
    matrix1 = matrix2;
    if (matrix2 == null)
      matrix1 = Matrix.IDENTITY_MATRIX; 
    if (matrix3.equals(matrix1))
      return null; 
    Transforms transforms = (Transforms)paramTransitionValues2.values.get("android:changeTransform:transforms");
    View view = paramTransitionValues2.view;
    setIdentityTransforms(view);
    float[] arrayOfFloat1 = new float[9];
    matrix3.getValues(arrayOfFloat1);
    float[] arrayOfFloat2 = new float[9];
    matrix1.getValues(arrayOfFloat2);
    PathAnimatorMatrix pathAnimatorMatrix = new PathAnimatorMatrix(view, arrayOfFloat1);
    PropertyValuesHolder propertyValuesHolder2 = PropertyValuesHolder.ofObject(NON_TRANSLATIONS_PROPERTY, (TypeEvaluator)new FloatArrayEvaluator(new float[9]), (Object[])new float[][] { arrayOfFloat1, arrayOfFloat2 });
    Path path = getPathMotion().getPath(arrayOfFloat1[2], arrayOfFloat1[5], arrayOfFloat2[2], arrayOfFloat2[5]);
    PropertyValuesHolder propertyValuesHolder1 = PropertyValuesHolder.ofObject(TRANSLATIONS_PROPERTY, null, path);
    ObjectAnimator objectAnimator = ObjectAnimator.ofPropertyValuesHolder(pathAnimatorMatrix, new PropertyValuesHolder[] { propertyValuesHolder2, propertyValuesHolder1 });
    Object object = new Object(this, paramBoolean, matrix1, view, transforms, pathAnimatorMatrix);
    objectAnimator.addListener((Animator.AnimatorListener)object);
    objectAnimator.addPauseListener((Animator.AnimatorPauseListener)object);
    return objectAnimator;
  }
  
  private boolean parentsMatch(ViewGroup paramViewGroup1, ViewGroup paramViewGroup2) {
    boolean bool1 = false;
    boolean bool = isValidTarget(paramViewGroup1);
    boolean bool2 = false, bool3 = false;
    if (!bool || !isValidTarget(paramViewGroup2)) {
      if (paramViewGroup1 == paramViewGroup2)
        bool2 = true; 
      return bool2;
    } 
    TransitionValues transitionValues = getMatchedTransitionValues(paramViewGroup1, true);
    bool2 = bool1;
    if (transitionValues != null) {
      bool2 = bool3;
      if (paramViewGroup2 == transitionValues.view)
        bool2 = true; 
    } 
    return bool2;
  }
  
  private void createGhostView(ViewGroup paramViewGroup, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    TransitionSet transitionSet;
    View view = paramTransitionValues2.view;
    Matrix matrix = (Matrix)paramTransitionValues2.values.get("android:changeTransform:parentMatrix");
    matrix = new Matrix(matrix);
    paramViewGroup.transformMatrixToLocal(matrix);
    GhostView ghostView = GhostView.addGhost(view, paramViewGroup, matrix);
    ChangeTransform changeTransform = this;
    while (changeTransform.mParent != null)
      transitionSet = changeTransform.mParent; 
    GhostListener ghostListener = new GhostListener(view, paramTransitionValues1.view, ghostView);
    transitionSet.addListener(ghostListener);
    if (paramTransitionValues1.view != paramTransitionValues2.view)
      paramTransitionValues1.view.setTransitionAlpha(0.0F); 
    view.setTransitionAlpha(1.0F);
  }
  
  private void setMatricesForParent(TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    Matrix matrix3 = (Matrix)paramTransitionValues2.values.get("android:changeTransform:parentMatrix");
    paramTransitionValues2.view.setTagInternal(16909278, matrix3);
    Matrix matrix4 = this.mTempMatrix;
    matrix4.reset();
    matrix3.invert(matrix4);
    matrix3 = (Matrix)paramTransitionValues1.values.get("android:changeTransform:matrix");
    Matrix matrix2 = matrix3;
    if (matrix3 == null) {
      matrix2 = new Matrix();
      paramTransitionValues1.values.put("android:changeTransform:matrix", matrix2);
    } 
    Matrix matrix1 = (Matrix)paramTransitionValues1.values.get("android:changeTransform:parentMatrix");
    matrix2.postConcat(matrix1);
    matrix2.postConcat(matrix4);
  }
  
  private static void setIdentityTransforms(View paramView) {
    setTransforms(paramView, 0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 0.0F, 0.0F, 0.0F);
  }
  
  private static void setTransforms(View paramView, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8) {
    paramView.setTranslationX(paramFloat1);
    paramView.setTranslationY(paramFloat2);
    paramView.setTranslationZ(paramFloat3);
    paramView.setScaleX(paramFloat4);
    paramView.setScaleY(paramFloat5);
    paramView.setRotationX(paramFloat6);
    paramView.setRotationY(paramFloat7);
    paramView.setRotation(paramFloat8);
  }
  
  public ChangeTransform() {}
  
  class Transforms {
    public final float rotationX;
    
    public final float rotationY;
    
    public final float rotationZ;
    
    public final float scaleX;
    
    public final float scaleY;
    
    public final float translationX;
    
    public final float translationY;
    
    public final float translationZ;
    
    public Transforms(ChangeTransform this$0) {
      this.translationX = this$0.getTranslationX();
      this.translationY = this$0.getTranslationY();
      this.translationZ = this$0.getTranslationZ();
      this.scaleX = this$0.getScaleX();
      this.scaleY = this$0.getScaleY();
      this.rotationX = this$0.getRotationX();
      this.rotationY = this$0.getRotationY();
      this.rotationZ = this$0.getRotation();
    }
    
    public void restore(View param1View) {
      ChangeTransform.setTransforms(param1View, this.translationX, this.translationY, this.translationZ, this.scaleX, this.scaleY, this.rotationX, this.rotationY, this.rotationZ);
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof Transforms;
      boolean bool1 = false;
      if (!bool)
        return false; 
      param1Object = param1Object;
      bool = bool1;
      if (((Transforms)param1Object).translationX == this.translationX) {
        bool = bool1;
        if (((Transforms)param1Object).translationY == this.translationY) {
          bool = bool1;
          if (((Transforms)param1Object).translationZ == this.translationZ) {
            bool = bool1;
            if (((Transforms)param1Object).scaleX == this.scaleX) {
              bool = bool1;
              if (((Transforms)param1Object).scaleY == this.scaleY) {
                bool = bool1;
                if (((Transforms)param1Object).rotationX == this.rotationX) {
                  bool = bool1;
                  if (((Transforms)param1Object).rotationY == this.rotationY) {
                    bool = bool1;
                    if (((Transforms)param1Object).rotationZ == this.rotationZ)
                      bool = true; 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
      return bool;
    }
  }
  
  class GhostListener extends TransitionListenerAdapter {
    private GhostView mGhostView;
    
    private View mStartView;
    
    private View mView;
    
    public GhostListener(ChangeTransform this$0, View param1View1, GhostView param1GhostView) {
      this.mView = (View)this$0;
      this.mStartView = param1View1;
      this.mGhostView = param1GhostView;
    }
    
    public void onTransitionEnd(Transition param1Transition) {
      param1Transition.removeListener(this);
      GhostView.removeGhost(this.mView);
      this.mView.setTagInternal(16909566, null);
      this.mView.setTagInternal(16909278, null);
      this.mStartView.setTransitionAlpha(1.0F);
    }
    
    public void onTransitionPause(Transition param1Transition) {
      this.mGhostView.setVisibility(4);
    }
    
    public void onTransitionResume(Transition param1Transition) {
      this.mGhostView.setVisibility(0);
    }
  }
  
  class PathAnimatorMatrix {
    private final Matrix mMatrix = new Matrix();
    
    private float mTranslationX;
    
    private float mTranslationY;
    
    private final float[] mValues;
    
    private final View mView;
    
    public PathAnimatorMatrix(ChangeTransform this$0, float[] param1ArrayOffloat) {
      this.mView = (View)this$0;
      float[] arrayOfFloat = (float[])param1ArrayOffloat.clone();
      this.mTranslationX = arrayOfFloat[2];
      this.mTranslationY = arrayOfFloat[5];
      setAnimationMatrix();
    }
    
    public void setValues(float[] param1ArrayOffloat) {
      System.arraycopy(param1ArrayOffloat, 0, this.mValues, 0, param1ArrayOffloat.length);
      setAnimationMatrix();
    }
    
    public void setTranslation(PointF param1PointF) {
      this.mTranslationX = param1PointF.x;
      this.mTranslationY = param1PointF.y;
      setAnimationMatrix();
    }
    
    private void setAnimationMatrix() {
      float[] arrayOfFloat = this.mValues;
      arrayOfFloat[2] = this.mTranslationX;
      arrayOfFloat[5] = this.mTranslationY;
      this.mMatrix.setValues(arrayOfFloat);
      this.mView.setAnimationMatrix(this.mMatrix);
    }
    
    public Matrix getMatrix() {
      return this.mMatrix;
    }
  }
}
