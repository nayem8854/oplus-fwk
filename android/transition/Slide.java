package android.transition;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import com.android.internal.R;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Slide extends Visibility {
  private static final String PROPNAME_SCREEN_POSITION = "android:slide:screenPosition";
  
  private static final String TAG = "Slide";
  
  private static final TimeInterpolator sAccelerate;
  
  private static final CalculateSlide sCalculateBottom;
  
  private static final CalculateSlide sCalculateEnd;
  
  private static final CalculateSlide sCalculateLeft;
  
  private static final CalculateSlide sCalculateRight;
  
  private static final CalculateSlide sCalculateStart;
  
  private static final CalculateSlide sCalculateTop;
  
  private static final TimeInterpolator sDecelerate = new DecelerateInterpolator();
  
  static {
    sAccelerate = new AccelerateInterpolator();
    sCalculateLeft = new CalculateSlideHorizontal() {
        public float getGoneX(ViewGroup param1ViewGroup, View param1View, float param1Float) {
          return param1View.getTranslationX() - param1ViewGroup.getWidth() * param1Float;
        }
      };
    sCalculateStart = new CalculateSlideHorizontal() {
        public float getGoneX(ViewGroup param1ViewGroup, View param1View, float param1Float) {
          int i = param1ViewGroup.getLayoutDirection();
          boolean bool = true;
          if (i != 1)
            bool = false; 
          if (bool) {
            param1Float = param1View.getTranslationX() + param1ViewGroup.getWidth() * param1Float;
          } else {
            param1Float = param1View.getTranslationX() - param1ViewGroup.getWidth() * param1Float;
          } 
          return param1Float;
        }
      };
    sCalculateTop = new CalculateSlideVertical() {
        public float getGoneY(ViewGroup param1ViewGroup, View param1View, float param1Float) {
          return param1View.getTranslationY() - param1ViewGroup.getHeight() * param1Float;
        }
      };
    sCalculateRight = new CalculateSlideHorizontal() {
        public float getGoneX(ViewGroup param1ViewGroup, View param1View, float param1Float) {
          return param1View.getTranslationX() + param1ViewGroup.getWidth() * param1Float;
        }
      };
    sCalculateEnd = new CalculateSlideHorizontal() {
        public float getGoneX(ViewGroup param1ViewGroup, View param1View, float param1Float) {
          int i = param1ViewGroup.getLayoutDirection();
          boolean bool = true;
          if (i != 1)
            bool = false; 
          if (bool) {
            param1Float = param1View.getTranslationX() - param1ViewGroup.getWidth() * param1Float;
          } else {
            param1Float = param1View.getTranslationX() + param1ViewGroup.getWidth() * param1Float;
          } 
          return param1Float;
        }
      };
    sCalculateBottom = new CalculateSlideVertical() {
        public float getGoneY(ViewGroup param1ViewGroup, View param1View, float param1Float) {
          return param1View.getTranslationY() + param1ViewGroup.getHeight() * param1Float;
        }
      };
  }
  
  private CalculateSlide mSlideCalculator = sCalculateBottom;
  
  private int mSlideEdge = 80;
  
  private float mSlideFraction = 1.0F;
  
  class CalculateSlideHorizontal implements CalculateSlide {
    private CalculateSlideHorizontal() {}
    
    public float getGoneY(ViewGroup param1ViewGroup, View param1View, float param1Float) {
      return param1View.getTranslationY();
    }
  }
  
  class CalculateSlideVertical implements CalculateSlide {
    private CalculateSlideVertical() {}
    
    public float getGoneX(ViewGroup param1ViewGroup, View param1View, float param1Float) {
      return param1View.getTranslationX();
    }
  }
  
  public Slide() {
    setSlideEdge(80);
  }
  
  public Slide(int paramInt) {
    setSlideEdge(paramInt);
  }
  
  public Slide(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Slide);
    int i = typedArray.getInt(0, 80);
    typedArray.recycle();
    setSlideEdge(i);
  }
  
  private void captureValues(TransitionValues paramTransitionValues) {
    View view = paramTransitionValues.view;
    int[] arrayOfInt = new int[2];
    view.getLocationOnScreen(arrayOfInt);
    paramTransitionValues.values.put("android:slide:screenPosition", arrayOfInt);
  }
  
  public void captureStartValues(TransitionValues paramTransitionValues) {
    super.captureStartValues(paramTransitionValues);
    captureValues(paramTransitionValues);
  }
  
  public void captureEndValues(TransitionValues paramTransitionValues) {
    super.captureEndValues(paramTransitionValues);
    captureValues(paramTransitionValues);
  }
  
  public void setSlideEdge(int paramInt) {
    if (paramInt != 3) {
      if (paramInt != 5) {
        if (paramInt != 48) {
          if (paramInt != 80) {
            if (paramInt != 8388611) {
              if (paramInt == 8388613) {
                this.mSlideCalculator = sCalculateEnd;
              } else {
                throw new IllegalArgumentException("Invalid slide direction");
              } 
            } else {
              this.mSlideCalculator = sCalculateStart;
            } 
          } else {
            this.mSlideCalculator = sCalculateBottom;
          } 
        } else {
          this.mSlideCalculator = sCalculateTop;
        } 
      } else {
        this.mSlideCalculator = sCalculateRight;
      } 
    } else {
      this.mSlideCalculator = sCalculateLeft;
    } 
    this.mSlideEdge = paramInt;
    SidePropagation sidePropagation = new SidePropagation();
    sidePropagation.setSide(paramInt);
    setPropagation(sidePropagation);
  }
  
  public int getSlideEdge() {
    return this.mSlideEdge;
  }
  
  public Animator onAppear(ViewGroup paramViewGroup, View paramView, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    if (paramTransitionValues2 == null)
      return null; 
    int[] arrayOfInt = (int[])paramTransitionValues2.values.get("android:slide:screenPosition");
    float f1 = paramView.getTranslationX();
    float f2 = paramView.getTranslationY();
    float f3 = this.mSlideCalculator.getGoneX(paramViewGroup, paramView, this.mSlideFraction);
    float f4 = this.mSlideCalculator.getGoneY(paramViewGroup, paramView, this.mSlideFraction);
    int i = arrayOfInt[0], j = arrayOfInt[1];
    TimeInterpolator timeInterpolator = sDecelerate;
    return 
      TranslationAnimationCreator.createAnimation(paramView, paramTransitionValues2, i, j, f3, f4, f1, f2, timeInterpolator, this);
  }
  
  public Animator onDisappear(ViewGroup paramViewGroup, View paramView, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    if (paramTransitionValues1 == null)
      return null; 
    int[] arrayOfInt = (int[])paramTransitionValues1.values.get("android:slide:screenPosition");
    float f1 = paramView.getTranslationX();
    float f2 = paramView.getTranslationY();
    float f3 = this.mSlideCalculator.getGoneX(paramViewGroup, paramView, this.mSlideFraction);
    float f4 = this.mSlideCalculator.getGoneY(paramViewGroup, paramView, this.mSlideFraction);
    int i = arrayOfInt[0], j = arrayOfInt[1];
    TimeInterpolator timeInterpolator = sAccelerate;
    return 
      TranslationAnimationCreator.createAnimation(paramView, paramTransitionValues1, i, j, f1, f2, f3, f4, timeInterpolator, this);
  }
  
  public void setSlideFraction(float paramFloat) {
    this.mSlideFraction = paramFloat;
  }
  
  class CalculateSlide {
    public abstract float getGoneX(ViewGroup param1ViewGroup, View param1View, float param1Float);
    
    public abstract float getGoneY(ViewGroup param1ViewGroup, View param1View, float param1Float);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class GravityFlag implements Annotation {}
}
