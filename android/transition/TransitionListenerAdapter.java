package android.transition;

public abstract class TransitionListenerAdapter implements Transition.TransitionListener {
  public void onTransitionStart(Transition paramTransition) {}
  
  public void onTransitionEnd(Transition paramTransition) {}
  
  public void onTransitionCancel(Transition paramTransition) {}
  
  public void onTransitionPause(Transition paramTransition) {}
  
  public void onTransitionResume(Transition paramTransition) {}
}
