package android.transition;

import android.content.Context;
import android.util.AttributeSet;

public class AutoTransition extends TransitionSet {
  public AutoTransition() {
    init();
  }
  
  public AutoTransition(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    init();
  }
  
  private void init() {
    setOrdering(1);
    TransitionSet transitionSet = addTransition(new Fade(2));
    ChangeBounds changeBounds = new ChangeBounds();
    transitionSet = transitionSet.addTransition(changeBounds);
    Fade fade = new Fade(1);
    transitionSet.addTransition(fade);
  }
}
