package android.transition;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import java.util.Map;

public class ChangeText extends Transition {
  private int mChangeBehavior = 0;
  
  private static final String[] sTransitionProperties = new String[] { "android:textchange:text", "android:textchange:textSelectionStart", "android:textchange:textSelectionEnd" };
  
  private static final String PROPNAME_TEXT_SELECTION_START = "android:textchange:textSelectionStart";
  
  private static final String PROPNAME_TEXT_SELECTION_END = "android:textchange:textSelectionEnd";
  
  private static final String PROPNAME_TEXT_COLOR = "android:textchange:textColor";
  
  private static final String PROPNAME_TEXT = "android:textchange:text";
  
  private static final String LOG_TAG = "TextChange";
  
  public static final int CHANGE_BEHAVIOR_OUT_IN = 3;
  
  public static final int CHANGE_BEHAVIOR_OUT = 1;
  
  public static final int CHANGE_BEHAVIOR_KEEP = 0;
  
  public static final int CHANGE_BEHAVIOR_IN = 2;
  
  public ChangeText setChangeBehavior(int paramInt) {
    if (paramInt >= 0 && paramInt <= 3)
      this.mChangeBehavior = paramInt; 
    return this;
  }
  
  public String[] getTransitionProperties() {
    return sTransitionProperties;
  }
  
  public int getChangeBehavior() {
    return this.mChangeBehavior;
  }
  
  private void captureValues(TransitionValues paramTransitionValues) {
    if (paramTransitionValues.view instanceof TextView) {
      TextView textView = (TextView)paramTransitionValues.view;
      paramTransitionValues.values.put("android:textchange:text", textView.getText());
      if (textView instanceof EditText) {
        Map<String, Object> map = paramTransitionValues.values;
        int i = textView.getSelectionStart();
        map.put("android:textchange:textSelectionStart", Integer.valueOf(i));
        map = paramTransitionValues.values;
        i = textView.getSelectionEnd();
        map.put("android:textchange:textSelectionEnd", Integer.valueOf(i));
      } 
      if (this.mChangeBehavior > 0)
        paramTransitionValues.values.put("android:textchange:textColor", Integer.valueOf(textView.getCurrentTextColor())); 
    } 
  }
  
  public void captureStartValues(TransitionValues paramTransitionValues) {
    captureValues(paramTransitionValues);
  }
  
  public void captureEndValues(TransitionValues paramTransitionValues) {
    captureValues(paramTransitionValues);
  }
  
  public Animator createAnimator(ViewGroup paramViewGroup, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    CharSequence charSequence1;
    byte b1, b2, b3;
    if (paramTransitionValues1 == null || paramTransitionValues2 == null || !(paramTransitionValues1.view instanceof TextView) || !(paramTransitionValues2.view instanceof TextView))
      return null; 
    final TextView view = (TextView)paramTransitionValues2.view;
    Map<String, Object> map1 = paramTransitionValues1.values;
    Map<String, Object> map2 = paramTransitionValues2.values;
    paramTransitionValues2 = (TransitionValues)map1.get("android:textchange:text");
    CharSequence charSequence2 = "";
    if (paramTransitionValues2 != null) {
      charSequence1 = (CharSequence)map1.get("android:textchange:text");
    } else {
      charSequence1 = "";
    } 
    if (map2.get("android:textchange:text") != null)
      charSequence2 = (CharSequence)map2.get("android:textchange:text"); 
    boolean bool = textView instanceof EditText;
    int i = -1;
    if (bool) {
      byte b;
      if (map1.get("android:textchange:textSelectionStart") != null) {
        b = ((Integer)map1.get("android:textchange:textSelectionStart")).intValue();
      } else {
        b = -1;
      } 
      if (map1.get("android:textchange:textSelectionEnd") != null) {
        b1 = ((Integer)map1.get("android:textchange:textSelectionEnd")).intValue();
      } else {
        b1 = b;
      } 
      if (map2.get("android:textchange:textSelectionStart") != null)
        i = ((Integer)map2.get("android:textchange:textSelectionStart")).intValue(); 
      if (map2.get("android:textchange:textSelectionEnd") != null) {
        b2 = ((Integer)map2.get("android:textchange:textSelectionEnd")).intValue();
      } else {
        b2 = i;
      } 
      b3 = b2;
      b2 = b;
    } else {
      b3 = -1;
      b2 = -1;
      i = -1;
      b1 = -1;
    } 
    if (!charSequence1.equals(charSequence2)) {
      ValueAnimator valueAnimator;
      final int endColor;
      if (this.mChangeBehavior != 2) {
        textView.setText(charSequence1);
        if (textView instanceof EditText)
          setSelection((EditText)textView, b2, b1); 
      } 
      if (this.mChangeBehavior == 0) {
        j = 0;
        valueAnimator = ValueAnimator.ofFloat(new float[] { 0.0F, 1.0F });
        valueAnimator.addListener((Animator.AnimatorListener)new Object(this, charSequence1, textView, charSequence2, i, b3));
      } else {
        AnimatorSet animatorSet;
        ValueAnimator valueAnimator1;
        final int startColor = ((Integer)valueAnimator.get("android:textchange:textColor")).intValue();
        j = ((Integer)map2.get("android:textchange:textColor")).intValue();
        valueAnimator = null;
        map2 = null;
        int m = this.mChangeBehavior;
        if (m == 3 || m == 1) {
          valueAnimator = ValueAnimator.ofInt(new int[] { Color.alpha(k), 0 });
          valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                final ChangeText this$0;
                
                final int val$startColor;
                
                final TextView val$view;
                
                public void onAnimationUpdate(ValueAnimator param1ValueAnimator) {
                  int i = ((Integer)param1ValueAnimator.getAnimatedValue()).intValue();
                  view.setTextColor(i << 24 | startColor & 0xFFFFFF);
                }
              });
          valueAnimator.addListener((Animator.AnimatorListener)new Object(this, charSequence1, textView, charSequence2, i, b3, j));
        } 
        k = this.mChangeBehavior;
        if (k == 3 || k == 2) {
          valueAnimator1 = ValueAnimator.ofInt(new int[] { 0, Color.alpha(j) });
          valueAnimator1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                final ChangeText this$0;
                
                final int val$endColor;
                
                final TextView val$view;
                
                public void onAnimationUpdate(ValueAnimator param1ValueAnimator) {
                  int i = ((Integer)param1ValueAnimator.getAnimatedValue()).intValue();
                  view.setTextColor(i << 24 | endColor & 0xFFFFFF);
                }
              });
          valueAnimator1.addListener((Animator.AnimatorListener)new Object(this, textView, j));
        } 
        if (valueAnimator != null && valueAnimator1 != null) {
          AnimatorSet animatorSet1 = new AnimatorSet();
          animatorSet1.playSequentially(new Animator[] { (Animator)valueAnimator, (Animator)valueAnimator1 });
          animatorSet = animatorSet1;
        } else if (animatorSet == null) {
          valueAnimator = valueAnimator1;
        } 
      } 
      Object object = new Object(this, textView, charSequence2, i, b3, j, charSequence1, b2, b1);
      addListener((Transition.TransitionListener)object);
      return (Animator)valueAnimator;
    } 
    return null;
  }
  
  private void setSelection(EditText paramEditText, int paramInt1, int paramInt2) {
    if (paramInt1 >= 0 && paramInt2 >= 0)
      paramEditText.setSelection(paramInt1, paramInt2); 
  }
}
