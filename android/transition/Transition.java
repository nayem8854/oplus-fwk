package android.transition;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.util.ArrayMap;
import android.util.AttributeSet;
import android.util.LongSparseArray;
import android.util.SparseArray;
import android.util.SparseLongArray;
import android.view.InflateException;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowId;
import android.view.animation.AnimationUtils;
import android.widget.ListView;
import com.android.internal.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public abstract class Transition implements Cloneable {
  private static final int[] DEFAULT_MATCH_ORDER = new int[] { 2, 1, 3, 4 };
  
  private static final PathMotion STRAIGHT_PATH_MOTION = (PathMotion)new Object();
  
  private String mName = getClass().getName();
  
  long mStartDelay = -1L;
  
  long mDuration = -1L;
  
  TimeInterpolator mInterpolator = null;
  
  ArrayList<Integer> mTargetIds = new ArrayList<>();
  
  ArrayList<View> mTargets = new ArrayList<>();
  
  ArrayList<String> mTargetNames = null;
  
  ArrayList<Class> mTargetTypes = null;
  
  ArrayList<Integer> mTargetIdExcludes = null;
  
  ArrayList<View> mTargetExcludes = null;
  
  ArrayList<Class> mTargetTypeExcludes = null;
  
  ArrayList<String> mTargetNameExcludes = null;
  
  ArrayList<Integer> mTargetIdChildExcludes = null;
  
  ArrayList<View> mTargetChildExcludes = null;
  
  ArrayList<Class> mTargetTypeChildExcludes = null;
  
  private TransitionValuesMaps mStartValues = new TransitionValuesMaps();
  
  private TransitionValuesMaps mEndValues = new TransitionValuesMaps();
  
  TransitionSet mParent = null;
  
  int[] mMatchOrder = DEFAULT_MATCH_ORDER;
  
  private static ThreadLocal<ArrayMap<Animator, AnimationInfo>> sRunningAnimators = new ThreadLocal<>();
  
  ViewGroup mSceneRoot = null;
  
  boolean mCanRemoveViews = false;
  
  private ArrayList<Animator> mCurrentAnimators = new ArrayList<>();
  
  int mNumInstances = 0;
  
  boolean mPaused = false;
  
  private boolean mEnded = false;
  
  ArrayList<TransitionListener> mListeners = null;
  
  ArrayList<Animator> mAnimators = new ArrayList<>();
  
  PathMotion mPathMotion = STRAIGHT_PATH_MOTION;
  
  static final boolean DBG = false;
  
  private static final String LOG_TAG = "Transition";
  
  private static final int MATCH_FIRST = 1;
  
  public static final int MATCH_ID = 3;
  
  private static final String MATCH_ID_STR = "id";
  
  public static final int MATCH_INSTANCE = 1;
  
  private static final String MATCH_INSTANCE_STR = "instance";
  
  public static final int MATCH_ITEM_ID = 4;
  
  private static final String MATCH_ITEM_ID_STR = "itemId";
  
  private static final int MATCH_LAST = 4;
  
  public static final int MATCH_NAME = 2;
  
  private static final String MATCH_NAME_STR = "name";
  
  private static final String MATCH_VIEW_NAME_STR = "viewName";
  
  ArrayList<TransitionValues> mEndValuesList;
  
  EpicenterCallback mEpicenterCallback;
  
  ArrayMap<String, String> mNameOverrides;
  
  TransitionPropagation mPropagation;
  
  ArrayList<TransitionValues> mStartValuesList;
  
  public Transition(Context paramContext, AttributeSet paramAttributeSet) {
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.Transition);
    long l = typedArray.getInt(1, -1);
    if (l >= 0L)
      setDuration(l); 
    l = typedArray.getInt(2, -1);
    if (l > 0L)
      setStartDelay(l); 
    int i = typedArray.getResourceId(0, 0);
    if (i > 0)
      setInterpolator(AnimationUtils.loadInterpolator(paramContext, i)); 
    String str = typedArray.getString(3);
    if (str != null)
      setMatchOrder(parseMatchOrder(str)); 
    typedArray.recycle();
  }
  
  private static int[] parseMatchOrder(String paramString) {
    StringBuilder stringBuilder;
    StringTokenizer stringTokenizer = new StringTokenizer(paramString, ",");
    int[] arrayOfInt = new int[stringTokenizer.countTokens()];
    byte b = 0;
    while (stringTokenizer.hasMoreTokens()) {
      String str = stringTokenizer.nextToken().trim();
      if ("id".equalsIgnoreCase(str)) {
        arrayOfInt[b] = 3;
      } else if ("instance".equalsIgnoreCase(str)) {
        arrayOfInt[b] = 1;
      } else if ("name".equalsIgnoreCase(str)) {
        arrayOfInt[b] = 2;
      } else if ("viewName".equalsIgnoreCase(str)) {
        arrayOfInt[b] = 2;
      } else if ("itemId".equalsIgnoreCase(str)) {
        arrayOfInt[b] = 4;
      } else {
        int[] arrayOfInt1;
        if (str.isEmpty()) {
          arrayOfInt1 = new int[arrayOfInt.length - 1];
          System.arraycopy(arrayOfInt, 0, arrayOfInt1, 0, b);
          arrayOfInt = arrayOfInt1;
          b--;
        } else {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown match type in matchOrder: '");
          stringBuilder.append((String)arrayOfInt1);
          stringBuilder.append("'");
          throw new InflateException(stringBuilder.toString());
        } 
      } 
      b++;
    } 
    return (int[])stringBuilder;
  }
  
  public Transition setDuration(long paramLong) {
    this.mDuration = paramLong;
    return this;
  }
  
  public long getDuration() {
    return this.mDuration;
  }
  
  public Transition setStartDelay(long paramLong) {
    this.mStartDelay = paramLong;
    return this;
  }
  
  public long getStartDelay() {
    return this.mStartDelay;
  }
  
  public Transition setInterpolator(TimeInterpolator paramTimeInterpolator) {
    this.mInterpolator = paramTimeInterpolator;
    return this;
  }
  
  public TimeInterpolator getInterpolator() {
    return this.mInterpolator;
  }
  
  public String[] getTransitionProperties() {
    return null;
  }
  
  public Animator createAnimator(ViewGroup paramViewGroup, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    return null;
  }
  
  public void setMatchOrder(int... paramVarArgs) {
    if (paramVarArgs == null || paramVarArgs.length == 0) {
      this.mMatchOrder = DEFAULT_MATCH_ORDER;
      return;
    } 
    for (byte b = 0; b < paramVarArgs.length; ) {
      int i = paramVarArgs[b];
      if (isValidMatch(i)) {
        if (!alreadyContains(paramVarArgs, b)) {
          b++;
          continue;
        } 
        throw new IllegalArgumentException("matches contains a duplicate value");
      } 
      throw new IllegalArgumentException("matches contains invalid value");
    } 
    this.mMatchOrder = (int[])paramVarArgs.clone();
  }
  
  private static boolean isValidMatch(int paramInt) {
    boolean bool = true;
    if (paramInt < 1 || paramInt > 4)
      bool = false; 
    return bool;
  }
  
  private static boolean alreadyContains(int[] paramArrayOfint, int paramInt) {
    int i = paramArrayOfint[paramInt];
    for (byte b = 0; b < paramInt; b++) {
      if (paramArrayOfint[b] == i)
        return true; 
    } 
    return false;
  }
  
  private void matchInstances(ArrayMap<View, TransitionValues> paramArrayMap1, ArrayMap<View, TransitionValues> paramArrayMap2) {
    for (int i = paramArrayMap1.size() - 1; i >= 0; i--) {
      View view = paramArrayMap1.keyAt(i);
      if (view != null && isValidTarget(view)) {
        TransitionValues transitionValues = paramArrayMap2.remove(view);
        if (transitionValues != null && isValidTarget(transitionValues.view)) {
          TransitionValues transitionValues1 = paramArrayMap1.removeAt(i);
          this.mStartValuesList.add(transitionValues1);
          this.mEndValuesList.add(transitionValues);
        } 
      } 
    } 
  }
  
  private void matchItemIds(ArrayMap<View, TransitionValues> paramArrayMap1, ArrayMap<View, TransitionValues> paramArrayMap2, LongSparseArray<View> paramLongSparseArray1, LongSparseArray<View> paramLongSparseArray2) {
    int i = paramLongSparseArray1.size();
    for (byte b = 0; b < i; b++) {
      View view = paramLongSparseArray1.valueAt(b);
      if (view != null && isValidTarget(view)) {
        View view1 = paramLongSparseArray2.get(paramLongSparseArray1.keyAt(b));
        if (view1 != null && isValidTarget(view1)) {
          TransitionValues transitionValues1 = paramArrayMap1.get(view);
          TransitionValues transitionValues2 = paramArrayMap2.get(view1);
          if (transitionValues1 != null && transitionValues2 != null) {
            this.mStartValuesList.add(transitionValues1);
            this.mEndValuesList.add(transitionValues2);
            paramArrayMap1.remove(view);
            paramArrayMap2.remove(view1);
          } 
        } 
      } 
    } 
  }
  
  private void matchIds(ArrayMap<View, TransitionValues> paramArrayMap1, ArrayMap<View, TransitionValues> paramArrayMap2, SparseArray<View> paramSparseArray1, SparseArray<View> paramSparseArray2) {
    int i = paramSparseArray1.size();
    for (byte b = 0; b < i; b++) {
      View view = paramSparseArray1.valueAt(b);
      if (view != null && isValidTarget(view)) {
        View view1 = paramSparseArray2.get(paramSparseArray1.keyAt(b));
        if (view1 != null && isValidTarget(view1)) {
          TransitionValues transitionValues1 = paramArrayMap1.get(view);
          TransitionValues transitionValues2 = paramArrayMap2.get(view1);
          if (transitionValues1 != null && transitionValues2 != null) {
            this.mStartValuesList.add(transitionValues1);
            this.mEndValuesList.add(transitionValues2);
            paramArrayMap1.remove(view);
            paramArrayMap2.remove(view1);
          } 
        } 
      } 
    } 
  }
  
  private void matchNames(ArrayMap<View, TransitionValues> paramArrayMap1, ArrayMap<View, TransitionValues> paramArrayMap2, ArrayMap<String, View> paramArrayMap3, ArrayMap<String, View> paramArrayMap4) {
    int i = paramArrayMap3.size();
    for (byte b = 0; b < i; b++) {
      View view = paramArrayMap3.valueAt(b);
      if (view != null && isValidTarget(view)) {
        View view1 = paramArrayMap4.get(paramArrayMap3.keyAt(b));
        if (view1 != null && isValidTarget(view1)) {
          TransitionValues transitionValues1 = paramArrayMap1.get(view);
          TransitionValues transitionValues2 = paramArrayMap2.get(view1);
          if (transitionValues1 != null && transitionValues2 != null) {
            this.mStartValuesList.add(transitionValues1);
            this.mEndValuesList.add(transitionValues2);
            paramArrayMap1.remove(view);
            paramArrayMap2.remove(view1);
          } 
        } 
      } 
    } 
  }
  
  private void addUnmatched(ArrayMap<View, TransitionValues> paramArrayMap1, ArrayMap<View, TransitionValues> paramArrayMap2) {
    byte b;
    for (b = 0; b < paramArrayMap1.size(); b++) {
      TransitionValues transitionValues = paramArrayMap1.valueAt(b);
      if (isValidTarget(transitionValues.view)) {
        this.mStartValuesList.add(transitionValues);
        this.mEndValuesList.add(null);
      } 
    } 
    for (b = 0; b < paramArrayMap2.size(); b++) {
      TransitionValues transitionValues = paramArrayMap2.valueAt(b);
      if (isValidTarget(transitionValues.view)) {
        this.mEndValuesList.add(transitionValues);
        this.mStartValuesList.add(null);
      } 
    } 
  }
  
  private void matchStartAndEnd(TransitionValuesMaps paramTransitionValuesMaps1, TransitionValuesMaps paramTransitionValuesMaps2) {
    ArrayMap<View, TransitionValues> arrayMap1 = new ArrayMap<>(paramTransitionValuesMaps1.viewValues);
    ArrayMap<View, TransitionValues> arrayMap2 = new ArrayMap<>(paramTransitionValuesMaps2.viewValues);
    byte b = 0;
    while (true) {
      int[] arrayOfInt = this.mMatchOrder;
      if (b < arrayOfInt.length) {
        int i = arrayOfInt[b];
        if (i != 1) {
          if (i != 2) {
            if (i != 3) {
              if (i == 4)
                matchItemIds(arrayMap1, arrayMap2, paramTransitionValuesMaps1.itemIdValues, paramTransitionValuesMaps2.itemIdValues); 
            } else {
              matchIds(arrayMap1, arrayMap2, paramTransitionValuesMaps1.idValues, paramTransitionValuesMaps2.idValues);
            } 
          } else {
            matchNames(arrayMap1, arrayMap2, paramTransitionValuesMaps1.nameValues, paramTransitionValuesMaps2.nameValues);
          } 
        } else {
          matchInstances(arrayMap1, arrayMap2);
        } 
        b++;
        continue;
      } 
      break;
    } 
    addUnmatched(arrayMap1, arrayMap2);
  }
  
  protected void createAnimators(ViewGroup paramViewGroup, TransitionValuesMaps paramTransitionValuesMaps1, TransitionValuesMaps paramTransitionValuesMaps2, ArrayList<TransitionValues> paramArrayList1, ArrayList<TransitionValues> paramArrayList2) {
    ArrayMap<Animator, AnimationInfo> arrayMap = getRunningAnimators();
    long l = Long.MAX_VALUE;
    int i = this.mAnimators.size();
    SparseLongArray sparseLongArray = new SparseLongArray();
    int j = paramArrayList1.size();
    byte b;
    for (b = 0; b < j; b++) {
      byte b1;
      TransitionValues transitionValues1 = paramArrayList1.get(b);
      TransitionValues transitionValues2 = paramArrayList2.get(b);
      if (transitionValues1 != null && !transitionValues1.targetedTransitions.contains(this))
        transitionValues1 = null; 
      if (transitionValues2 != null && !transitionValues2.targetedTransitions.contains(this))
        transitionValues2 = null; 
      if (transitionValues1 == null && transitionValues2 == null)
        continue; 
      if (transitionValues1 == null || transitionValues2 == null || isTransitionRequired(transitionValues1, transitionValues2)) {
        b1 = 1;
      } else {
        b1 = 0;
      } 
      if (b1) {
        Animator animator = createAnimator(paramViewGroup, transitionValues1, transitionValues2);
        if (animator != null) {
          TransitionValues transitionValues;
          AnimationInfo animationInfo;
          if (transitionValues2 != null) {
            transitionValues = (TransitionValues)transitionValues2.view;
            String[] arrayOfString = getTransitionProperties();
            if (arrayOfString != null && arrayOfString.length > 0) {
              TransitionValues transitionValues3 = new TransitionValues((View)transitionValues);
              TransitionValues transitionValues4 = paramTransitionValuesMaps2.viewValues.get(transitionValues);
              if (transitionValues4 != null)
                for (b1 = 0; b1 < arrayOfString.length; b1++) {
                  Map<String, Object> map1 = transitionValues3.values;
                  String str1 = arrayOfString[b1];
                  Map<String, Object> map2 = transitionValues4.values;
                  String str2 = arrayOfString[b1];
                  map2 = (Map<String, Object>)map2.get(str2);
                  map1.put(str1, map2);
                }  
              int k = arrayMap.size();
              for (b1 = 0, animationInfo = (AnimationInfo)arrayOfString;; b1++);
            } else {
              animationInfo = null;
            } 
            View view = (View)animationInfo;
            animationInfo = (AnimationInfo)transitionValues;
            transitionValues = (TransitionValues)view;
          } else {
            transitionValues = null;
            if (transitionValues1 != null) {
              animationInfo = (AnimationInfo)transitionValues1.view;
            } else {
              animationInfo = null;
            } 
          } 
          if (animator != null) {
            TransitionPropagation transitionPropagation = this.mPropagation;
            if (transitionPropagation != null) {
              long l1 = transitionPropagation.getStartDelay(paramViewGroup, this, transitionValues1, transitionValues2);
              sparseLongArray.put(this.mAnimators.size(), l1);
              l = Math.min(l1, l);
            } 
            String str = getName();
            animationInfo = new AnimationInfo((View)animationInfo, str, this, paramViewGroup.getWindowId(), transitionValues);
            arrayMap.put(animator, animationInfo);
            this.mAnimators.add(animator);
          } 
        } 
      } 
      continue;
    } 
    if (sparseLongArray.size() != 0)
      for (b = 0; b < sparseLongArray.size(); b++) {
        j = sparseLongArray.keyAt(b);
        Animator animator = this.mAnimators.get(j);
        long l2 = sparseLongArray.valueAt(b), l1 = animator.getStartDelay();
        animator.setStartDelay(l2 - l + l1);
      }  
  }
  
  public boolean isValidTarget(View paramView) {
    if (paramView == null)
      return false; 
    int i = paramView.getId();
    ArrayList<Integer> arrayList3 = this.mTargetIdExcludes;
    if (arrayList3 != null && arrayList3.contains(Integer.valueOf(i)))
      return false; 
    ArrayList<View> arrayList2 = this.mTargetExcludes;
    if (arrayList2 != null && arrayList2.contains(paramView))
      return false; 
    ArrayList<Class> arrayList1 = this.mTargetTypeExcludes;
    if (arrayList1 != null && paramView != null) {
      int j = arrayList1.size();
      for (byte b = 0; b < j; b++) {
        Class clazz = this.mTargetTypeExcludes.get(b);
        if (clazz.isInstance(paramView))
          return false; 
      } 
    } 
    if (this.mTargetNameExcludes != null && paramView != null && paramView.getTransitionName() != null && 
      this.mTargetNameExcludes.contains(paramView.getTransitionName()))
      return false; 
    if (this.mTargetIds.size() == 0 && this.mTargets.size() == 0) {
      arrayList1 = this.mTargetTypes;
      if (arrayList1 == null || 
        arrayList1.isEmpty()) {
        ArrayList<String> arrayList4 = this.mTargetNames;
        if (arrayList4 == null || 
          arrayList4.isEmpty())
          return true; 
      } 
    } 
    if (this.mTargetIds.contains(Integer.valueOf(i)) || this.mTargets.contains(paramView))
      return true; 
    ArrayList<String> arrayList = this.mTargetNames;
    if (arrayList != null && arrayList.contains(paramView.getTransitionName()))
      return true; 
    if (this.mTargetTypes != null)
      for (byte b = 0; b < this.mTargetTypes.size(); b++) {
        if (((Class)this.mTargetTypes.get(b)).isInstance(paramView))
          return true; 
      }  
    return false;
  }
  
  private static ArrayMap<Animator, AnimationInfo> getRunningAnimators() {
    ArrayMap<Object, Object> arrayMap1 = (ArrayMap)sRunningAnimators.get();
    ArrayMap<Object, Object> arrayMap2 = arrayMap1;
    if (arrayMap1 == null) {
      arrayMap2 = new ArrayMap<>();
      sRunningAnimators.set(arrayMap2);
    } 
    return (ArrayMap)arrayMap2;
  }
  
  protected void runAnimators() {
    start();
    ArrayMap<Animator, AnimationInfo> arrayMap = getRunningAnimators();
    for (Animator animator : this.mAnimators) {
      if (arrayMap.containsKey(animator)) {
        start();
        runAnimator(animator, arrayMap);
      } 
    } 
    this.mAnimators.clear();
    end();
  }
  
  private void runAnimator(Animator paramAnimator, ArrayMap<Animator, AnimationInfo> paramArrayMap) {
    if (paramAnimator != null) {
      paramAnimator.addListener((Animator.AnimatorListener)new Object(this, paramArrayMap));
      animate(paramAnimator);
    } 
  }
  
  public Transition addTarget(int paramInt) {
    if (paramInt > 0)
      this.mTargetIds.add(Integer.valueOf(paramInt)); 
    return this;
  }
  
  public Transition addTarget(String paramString) {
    if (paramString != null) {
      if (this.mTargetNames == null)
        this.mTargetNames = new ArrayList<>(); 
      this.mTargetNames.add(paramString);
    } 
    return this;
  }
  
  public Transition addTarget(Class paramClass) {
    if (paramClass != null) {
      if (this.mTargetTypes == null)
        this.mTargetTypes = new ArrayList<>(); 
      this.mTargetTypes.add(paramClass);
    } 
    return this;
  }
  
  public Transition removeTarget(int paramInt) {
    if (paramInt > 0)
      this.mTargetIds.remove(Integer.valueOf(paramInt)); 
    return this;
  }
  
  public Transition removeTarget(String paramString) {
    if (paramString != null) {
      ArrayList<String> arrayList = this.mTargetNames;
      if (arrayList != null)
        arrayList.remove(paramString); 
    } 
    return this;
  }
  
  public Transition excludeTarget(int paramInt, boolean paramBoolean) {
    if (paramInt >= 0)
      this.mTargetIdExcludes = excludeObject(this.mTargetIdExcludes, Integer.valueOf(paramInt), paramBoolean); 
    return this;
  }
  
  public Transition excludeTarget(String paramString, boolean paramBoolean) {
    this.mTargetNameExcludes = excludeObject(this.mTargetNameExcludes, paramString, paramBoolean);
    return this;
  }
  
  public Transition excludeChildren(int paramInt, boolean paramBoolean) {
    if (paramInt >= 0)
      this.mTargetIdChildExcludes = excludeObject(this.mTargetIdChildExcludes, Integer.valueOf(paramInt), paramBoolean); 
    return this;
  }
  
  public Transition excludeTarget(View paramView, boolean paramBoolean) {
    this.mTargetExcludes = excludeObject(this.mTargetExcludes, paramView, paramBoolean);
    return this;
  }
  
  public Transition excludeChildren(View paramView, boolean paramBoolean) {
    this.mTargetChildExcludes = excludeObject(this.mTargetChildExcludes, paramView, paramBoolean);
    return this;
  }
  
  private static <T> ArrayList<T> excludeObject(ArrayList<T> paramArrayList, T paramT, boolean paramBoolean) {
    ArrayList<T> arrayList = paramArrayList;
    if (paramT != null)
      if (paramBoolean) {
        arrayList = ArrayListManager.add(paramArrayList, paramT);
      } else {
        arrayList = ArrayListManager.remove(paramArrayList, paramT);
      }  
    return arrayList;
  }
  
  public Transition excludeTarget(Class<?> paramClass, boolean paramBoolean) {
    this.mTargetTypeExcludes = excludeObject(this.mTargetTypeExcludes, paramClass, paramBoolean);
    return this;
  }
  
  public Transition excludeChildren(Class<?> paramClass, boolean paramBoolean) {
    this.mTargetTypeChildExcludes = excludeObject(this.mTargetTypeChildExcludes, paramClass, paramBoolean);
    return this;
  }
  
  public Transition addTarget(View paramView) {
    this.mTargets.add(paramView);
    return this;
  }
  
  public Transition removeTarget(View paramView) {
    if (paramView != null)
      this.mTargets.remove(paramView); 
    return this;
  }
  
  public Transition removeTarget(Class paramClass) {
    if (paramClass != null)
      this.mTargetTypes.remove(paramClass); 
    return this;
  }
  
  public List<Integer> getTargetIds() {
    return this.mTargetIds;
  }
  
  public List<View> getTargets() {
    return this.mTargets;
  }
  
  public List<String> getTargetNames() {
    return this.mTargetNames;
  }
  
  public List<String> getTargetViewNames() {
    return this.mTargetNames;
  }
  
  public List<Class> getTargetTypes() {
    return this.mTargetTypes;
  }
  
  void captureValues(ViewGroup paramViewGroup, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: iload_2
    //   2: invokevirtual clearValues : (Z)V
    //   5: aload_0
    //   6: getfield mTargetIds : Ljava/util/ArrayList;
    //   9: invokevirtual size : ()I
    //   12: ifgt -> 25
    //   15: aload_0
    //   16: getfield mTargets : Ljava/util/ArrayList;
    //   19: invokevirtual size : ()I
    //   22: ifle -> 60
    //   25: aload_0
    //   26: getfield mTargetNames : Ljava/util/ArrayList;
    //   29: astore_3
    //   30: aload_3
    //   31: ifnull -> 41
    //   34: aload_3
    //   35: invokevirtual isEmpty : ()Z
    //   38: ifeq -> 60
    //   41: aload_0
    //   42: getfield mTargetTypes : Ljava/util/ArrayList;
    //   45: astore_3
    //   46: aload_3
    //   47: ifnull -> 69
    //   50: aload_3
    //   51: invokevirtual isEmpty : ()Z
    //   54: ifeq -> 60
    //   57: goto -> 69
    //   60: aload_0
    //   61: aload_1
    //   62: iload_2
    //   63: invokespecial captureHierarchy : (Landroid/view/View;Z)V
    //   66: goto -> 289
    //   69: iconst_0
    //   70: istore #4
    //   72: iload #4
    //   74: aload_0
    //   75: getfield mTargetIds : Ljava/util/ArrayList;
    //   78: invokevirtual size : ()I
    //   81: if_icmpge -> 190
    //   84: aload_0
    //   85: getfield mTargetIds : Ljava/util/ArrayList;
    //   88: iload #4
    //   90: invokevirtual get : (I)Ljava/lang/Object;
    //   93: checkcast java/lang/Integer
    //   96: invokevirtual intValue : ()I
    //   99: istore #5
    //   101: aload_1
    //   102: iload #5
    //   104: invokevirtual findViewById : (I)Landroid/view/View;
    //   107: astore_3
    //   108: aload_3
    //   109: ifnull -> 184
    //   112: new android/transition/TransitionValues
    //   115: dup
    //   116: aload_3
    //   117: invokespecial <init> : (Landroid/view/View;)V
    //   120: astore #6
    //   122: iload_2
    //   123: ifeq -> 135
    //   126: aload_0
    //   127: aload #6
    //   129: invokevirtual captureStartValues : (Landroid/transition/TransitionValues;)V
    //   132: goto -> 141
    //   135: aload_0
    //   136: aload #6
    //   138: invokevirtual captureEndValues : (Landroid/transition/TransitionValues;)V
    //   141: aload #6
    //   143: getfield targetedTransitions : Ljava/util/ArrayList;
    //   146: aload_0
    //   147: invokevirtual add : (Ljava/lang/Object;)Z
    //   150: pop
    //   151: aload_0
    //   152: aload #6
    //   154: invokevirtual capturePropagationValues : (Landroid/transition/TransitionValues;)V
    //   157: iload_2
    //   158: ifeq -> 174
    //   161: aload_0
    //   162: getfield mStartValues : Landroid/transition/TransitionValuesMaps;
    //   165: aload_3
    //   166: aload #6
    //   168: invokestatic addViewValues : (Landroid/transition/TransitionValuesMaps;Landroid/view/View;Landroid/transition/TransitionValues;)V
    //   171: goto -> 184
    //   174: aload_0
    //   175: getfield mEndValues : Landroid/transition/TransitionValuesMaps;
    //   178: aload_3
    //   179: aload #6
    //   181: invokestatic addViewValues : (Landroid/transition/TransitionValuesMaps;Landroid/view/View;Landroid/transition/TransitionValues;)V
    //   184: iinc #4, 1
    //   187: goto -> 72
    //   190: iconst_0
    //   191: istore #4
    //   193: iload #4
    //   195: aload_0
    //   196: getfield mTargets : Ljava/util/ArrayList;
    //   199: invokevirtual size : ()I
    //   202: if_icmpge -> 289
    //   205: aload_0
    //   206: getfield mTargets : Ljava/util/ArrayList;
    //   209: iload #4
    //   211: invokevirtual get : (I)Ljava/lang/Object;
    //   214: checkcast android/view/View
    //   217: astore_3
    //   218: new android/transition/TransitionValues
    //   221: dup
    //   222: aload_3
    //   223: invokespecial <init> : (Landroid/view/View;)V
    //   226: astore_1
    //   227: iload_2
    //   228: ifeq -> 239
    //   231: aload_0
    //   232: aload_1
    //   233: invokevirtual captureStartValues : (Landroid/transition/TransitionValues;)V
    //   236: goto -> 244
    //   239: aload_0
    //   240: aload_1
    //   241: invokevirtual captureEndValues : (Landroid/transition/TransitionValues;)V
    //   244: aload_1
    //   245: getfield targetedTransitions : Ljava/util/ArrayList;
    //   248: aload_0
    //   249: invokevirtual add : (Ljava/lang/Object;)Z
    //   252: pop
    //   253: aload_0
    //   254: aload_1
    //   255: invokevirtual capturePropagationValues : (Landroid/transition/TransitionValues;)V
    //   258: iload_2
    //   259: ifeq -> 274
    //   262: aload_0
    //   263: getfield mStartValues : Landroid/transition/TransitionValuesMaps;
    //   266: aload_3
    //   267: aload_1
    //   268: invokestatic addViewValues : (Landroid/transition/TransitionValuesMaps;Landroid/view/View;Landroid/transition/TransitionValues;)V
    //   271: goto -> 283
    //   274: aload_0
    //   275: getfield mEndValues : Landroid/transition/TransitionValuesMaps;
    //   278: aload_3
    //   279: aload_1
    //   280: invokestatic addViewValues : (Landroid/transition/TransitionValuesMaps;Landroid/view/View;Landroid/transition/TransitionValues;)V
    //   283: iinc #4, 1
    //   286: goto -> 193
    //   289: iload_2
    //   290: ifne -> 425
    //   293: aload_0
    //   294: getfield mNameOverrides : Landroid/util/ArrayMap;
    //   297: astore_1
    //   298: aload_1
    //   299: ifnull -> 425
    //   302: aload_1
    //   303: invokevirtual size : ()I
    //   306: istore #5
    //   308: new java/util/ArrayList
    //   311: dup
    //   312: iload #5
    //   314: invokespecial <init> : (I)V
    //   317: astore_1
    //   318: iconst_0
    //   319: istore #4
    //   321: iload #4
    //   323: iload #5
    //   325: if_icmpge -> 366
    //   328: aload_0
    //   329: getfield mNameOverrides : Landroid/util/ArrayMap;
    //   332: iload #4
    //   334: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   337: checkcast java/lang/String
    //   340: astore_3
    //   341: aload_1
    //   342: aload_0
    //   343: getfield mStartValues : Landroid/transition/TransitionValuesMaps;
    //   346: getfield nameValues : Landroid/util/ArrayMap;
    //   349: aload_3
    //   350: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   353: checkcast android/view/View
    //   356: invokevirtual add : (Ljava/lang/Object;)Z
    //   359: pop
    //   360: iinc #4, 1
    //   363: goto -> 321
    //   366: iconst_0
    //   367: istore #4
    //   369: iload #4
    //   371: iload #5
    //   373: if_icmpge -> 425
    //   376: aload_1
    //   377: iload #4
    //   379: invokevirtual get : (I)Ljava/lang/Object;
    //   382: checkcast android/view/View
    //   385: astore #6
    //   387: aload #6
    //   389: ifnull -> 419
    //   392: aload_0
    //   393: getfield mNameOverrides : Landroid/util/ArrayMap;
    //   396: iload #4
    //   398: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   401: checkcast java/lang/String
    //   404: astore_3
    //   405: aload_0
    //   406: getfield mStartValues : Landroid/transition/TransitionValuesMaps;
    //   409: getfield nameValues : Landroid/util/ArrayMap;
    //   412: aload_3
    //   413: aload #6
    //   415: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   418: pop
    //   419: iinc #4, 1
    //   422: goto -> 369
    //   425: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1425	-> 0
    //   #1426	-> 5
    //   #1427	-> 34
    //   #1428	-> 50
    //   #1465	-> 60
    //   #1429	-> 69
    //   #1430	-> 84
    //   #1431	-> 101
    //   #1432	-> 108
    //   #1433	-> 112
    //   #1434	-> 122
    //   #1435	-> 126
    //   #1437	-> 135
    //   #1439	-> 141
    //   #1440	-> 151
    //   #1441	-> 157
    //   #1442	-> 161
    //   #1444	-> 174
    //   #1429	-> 184
    //   #1448	-> 190
    //   #1449	-> 205
    //   #1450	-> 218
    //   #1451	-> 227
    //   #1452	-> 231
    //   #1454	-> 239
    //   #1456	-> 244
    //   #1457	-> 253
    //   #1458	-> 258
    //   #1459	-> 262
    //   #1461	-> 274
    //   #1448	-> 283
    //   #1467	-> 289
    //   #1468	-> 302
    //   #1469	-> 308
    //   #1470	-> 318
    //   #1471	-> 328
    //   #1472	-> 341
    //   #1470	-> 360
    //   #1474	-> 366
    //   #1475	-> 376
    //   #1476	-> 387
    //   #1477	-> 392
    //   #1478	-> 405
    //   #1474	-> 419
    //   #1482	-> 425
  }
  
  static void addViewValues(TransitionValuesMaps paramTransitionValuesMaps, View paramView, TransitionValues paramTransitionValues) {
    paramTransitionValuesMaps.viewValues.put(paramView, paramTransitionValues);
    int i = paramView.getId();
    if (i >= 0)
      if (paramTransitionValuesMaps.idValues.indexOfKey(i) >= 0) {
        paramTransitionValuesMaps.idValues.put(i, null);
      } else {
        paramTransitionValuesMaps.idValues.put(i, paramView);
      }  
    String str = paramView.getTransitionName();
    if (str != null)
      if (paramTransitionValuesMaps.nameValues.containsKey(str)) {
        paramTransitionValuesMaps.nameValues.put(str, null);
      } else {
        paramTransitionValuesMaps.nameValues.put(str, paramView);
      }  
    if (paramView.getParent() instanceof ListView) {
      ListView listView = (ListView)paramView.getParent();
      if (listView.getAdapter().hasStableIds()) {
        i = listView.getPositionForView(paramView);
        long l = listView.getItemIdAtPosition(i);
        if (paramTransitionValuesMaps.itemIdValues.indexOfKey(l) >= 0) {
          paramView = paramTransitionValuesMaps.itemIdValues.get(l);
          if (paramView != null) {
            paramView.setHasTransientState(false);
            paramTransitionValuesMaps.itemIdValues.put(l, null);
          } 
        } else {
          paramView.setHasTransientState(true);
          paramTransitionValuesMaps.itemIdValues.put(l, paramView);
        } 
      } 
    } 
  }
  
  void clearValues(boolean paramBoolean) {
    if (paramBoolean) {
      this.mStartValues.viewValues.clear();
      this.mStartValues.idValues.clear();
      this.mStartValues.itemIdValues.clear();
      this.mStartValues.nameValues.clear();
      this.mStartValuesList = null;
    } else {
      this.mEndValues.viewValues.clear();
      this.mEndValues.idValues.clear();
      this.mEndValues.itemIdValues.clear();
      this.mEndValues.nameValues.clear();
      this.mEndValuesList = null;
    } 
  }
  
  private void captureHierarchy(View paramView, boolean paramBoolean) {
    if (paramView == null)
      return; 
    int i = paramView.getId();
    ArrayList<Integer> arrayList2 = this.mTargetIdExcludes;
    if (arrayList2 != null && arrayList2.contains(Integer.valueOf(i)))
      return; 
    ArrayList<View> arrayList1 = this.mTargetExcludes;
    if (arrayList1 != null && arrayList1.contains(paramView))
      return; 
    ArrayList<Class> arrayList = this.mTargetTypeExcludes;
    if (arrayList != null && paramView != null) {
      int j = arrayList.size();
      for (byte b = 0; b < j; b++) {
        if (((Class)this.mTargetTypeExcludes.get(b)).isInstance(paramView))
          return; 
      } 
    } 
    if (paramView.getParent() instanceof ViewGroup) {
      TransitionValues transitionValues = new TransitionValues(paramView);
      if (paramBoolean) {
        captureStartValues(transitionValues);
      } else {
        captureEndValues(transitionValues);
      } 
      transitionValues.targetedTransitions.add(this);
      capturePropagationValues(transitionValues);
      if (paramBoolean) {
        addViewValues(this.mStartValues, paramView, transitionValues);
      } else {
        addViewValues(this.mEndValues, paramView, transitionValues);
      } 
    } 
    if (paramView instanceof ViewGroup) {
      ArrayList<Integer> arrayList5 = this.mTargetIdChildExcludes;
      if (arrayList5 != null && arrayList5.contains(Integer.valueOf(i)))
        return; 
      ArrayList<View> arrayList4 = this.mTargetChildExcludes;
      if (arrayList4 != null && arrayList4.contains(paramView))
        return; 
      ArrayList<Class> arrayList3 = this.mTargetTypeChildExcludes;
      if (arrayList3 != null) {
        int j = arrayList3.size();
        for (byte b1 = 0; b1 < j; b1++) {
          if (((Class)this.mTargetTypeChildExcludes.get(b1)).isInstance(paramView))
            return; 
        } 
      } 
      paramView = paramView;
      for (byte b = 0; b < paramView.getChildCount(); b++)
        captureHierarchy(paramView.getChildAt(b), paramBoolean); 
    } 
  }
  
  public TransitionValues getTransitionValues(View paramView, boolean paramBoolean) {
    TransitionValuesMaps transitionValuesMaps;
    TransitionSet transitionSet = this.mParent;
    if (transitionSet != null)
      return transitionSet.getTransitionValues(paramView, paramBoolean); 
    if (paramBoolean) {
      transitionValuesMaps = this.mStartValues;
    } else {
      transitionValuesMaps = this.mEndValues;
    } 
    return transitionValuesMaps.viewValues.get(paramView);
  }
  
  TransitionValues getMatchedTransitionValues(View paramView, boolean paramBoolean) {
    TransitionValues transitionValues;
    ArrayList<TransitionValues> arrayList;
    byte b2;
    TransitionSet transitionSet = this.mParent;
    if (transitionSet != null)
      return transitionSet.getMatchedTransitionValues(paramView, paramBoolean); 
    if (paramBoolean) {
      arrayList = this.mStartValuesList;
    } else {
      arrayList = this.mEndValuesList;
    } 
    if (arrayList == null)
      return null; 
    int i = arrayList.size();
    byte b1 = -1;
    byte b = 0;
    while (true) {
      b2 = b1;
      if (b < i) {
        TransitionValues transitionValues1 = arrayList.get(b);
        if (transitionValues1 == null)
          return null; 
        if (transitionValues1.view == paramView) {
          b2 = b;
          break;
        } 
        b++;
        continue;
      } 
      break;
    } 
    paramView = null;
    if (b2 >= 0) {
      ArrayList<TransitionValues> arrayList1;
      if (paramBoolean) {
        arrayList1 = this.mEndValuesList;
      } else {
        arrayList1 = this.mStartValuesList;
      } 
      transitionValues = arrayList1.get(b2);
    } 
    return transitionValues;
  }
  
  public void pause(View paramView) {
    if (!this.mEnded) {
      ArrayMap<Animator, AnimationInfo> arrayMap = getRunningAnimators();
      int i = arrayMap.size();
      if (paramView != null) {
        WindowId windowId = paramView.getWindowId();
        for (; --i >= 0; i--) {
          AnimationInfo animationInfo = arrayMap.valueAt(i);
          if (animationInfo.view != null && windowId != null && windowId.equals(animationInfo.windowId)) {
            Animator animator = arrayMap.keyAt(i);
            animator.pause();
          } 
        } 
      } 
      ArrayList<TransitionListener> arrayList = this.mListeners;
      if (arrayList != null && arrayList.size() > 0) {
        arrayList = this.mListeners;
        arrayList = (ArrayList<TransitionListener>)arrayList.clone();
        int j = arrayList.size();
        for (i = 0; i < j; i++)
          ((TransitionListener)arrayList.get(i)).onTransitionPause(this); 
      } 
      this.mPaused = true;
    } 
  }
  
  public void resume(View paramView) {
    if (this.mPaused) {
      if (!this.mEnded) {
        ArrayMap<Animator, AnimationInfo> arrayMap = getRunningAnimators();
        int i = arrayMap.size();
        WindowId windowId = paramView.getWindowId();
        for (; --i >= 0; i--) {
          AnimationInfo animationInfo = arrayMap.valueAt(i);
          if (animationInfo.view != null && windowId != null && windowId.equals(animationInfo.windowId)) {
            Animator animator = arrayMap.keyAt(i);
            animator.resume();
          } 
        } 
        ArrayList<TransitionListener> arrayList = this.mListeners;
        if (arrayList != null && arrayList.size() > 0) {
          arrayList = this.mListeners;
          arrayList = (ArrayList<TransitionListener>)arrayList.clone();
          int j = arrayList.size();
          for (i = 0; i < j; i++)
            ((TransitionListener)arrayList.get(i)).onTransitionResume(this); 
        } 
      } 
      this.mPaused = false;
    } 
  }
  
  void playTransition(ViewGroup paramViewGroup) {
    // Byte code:
    //   0: aload_0
    //   1: new java/util/ArrayList
    //   4: dup
    //   5: invokespecial <init> : ()V
    //   8: putfield mStartValuesList : Ljava/util/ArrayList;
    //   11: aload_0
    //   12: new java/util/ArrayList
    //   15: dup
    //   16: invokespecial <init> : ()V
    //   19: putfield mEndValuesList : Ljava/util/ArrayList;
    //   22: aload_0
    //   23: aload_0
    //   24: getfield mStartValues : Landroid/transition/TransitionValuesMaps;
    //   27: aload_0
    //   28: getfield mEndValues : Landroid/transition/TransitionValuesMaps;
    //   31: invokespecial matchStartAndEnd : (Landroid/transition/TransitionValuesMaps;Landroid/transition/TransitionValuesMaps;)V
    //   34: invokestatic getRunningAnimators : ()Landroid/util/ArrayMap;
    //   37: astore_2
    //   38: aload_2
    //   39: invokevirtual size : ()I
    //   42: istore_3
    //   43: aload_1
    //   44: invokevirtual getWindowId : ()Landroid/view/WindowId;
    //   47: astore #4
    //   49: iinc #3, -1
    //   52: iload_3
    //   53: iflt -> 255
    //   56: aload_2
    //   57: iload_3
    //   58: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   61: checkcast android/animation/Animator
    //   64: astore #5
    //   66: aload #5
    //   68: ifnull -> 249
    //   71: aload_2
    //   72: aload #5
    //   74: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   77: checkcast android/transition/Transition$AnimationInfo
    //   80: astore #6
    //   82: aload #6
    //   84: ifnull -> 249
    //   87: aload #6
    //   89: getfield view : Landroid/view/View;
    //   92: ifnull -> 249
    //   95: aload #6
    //   97: getfield windowId : Landroid/view/WindowId;
    //   100: aload #4
    //   102: if_acmpne -> 249
    //   105: aload #6
    //   107: getfield values : Landroid/transition/TransitionValues;
    //   110: astore #7
    //   112: aload #6
    //   114: getfield view : Landroid/view/View;
    //   117: astore #8
    //   119: iconst_1
    //   120: istore #9
    //   122: aload_0
    //   123: aload #8
    //   125: iconst_1
    //   126: invokevirtual getTransitionValues : (Landroid/view/View;Z)Landroid/transition/TransitionValues;
    //   129: astore #10
    //   131: aload_0
    //   132: aload #8
    //   134: iconst_1
    //   135: invokevirtual getMatchedTransitionValues : (Landroid/view/View;Z)Landroid/transition/TransitionValues;
    //   138: astore #11
    //   140: aload #11
    //   142: astore #12
    //   144: aload #10
    //   146: ifnonnull -> 175
    //   149: aload #11
    //   151: astore #12
    //   153: aload #11
    //   155: ifnonnull -> 175
    //   158: aload_0
    //   159: getfield mEndValues : Landroid/transition/TransitionValuesMaps;
    //   162: getfield viewValues : Landroid/util/ArrayMap;
    //   165: aload #8
    //   167: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   170: checkcast android/transition/TransitionValues
    //   173: astore #12
    //   175: aload #10
    //   177: ifnonnull -> 185
    //   180: aload #12
    //   182: ifnull -> 207
    //   185: aload #6
    //   187: getfield transition : Landroid/transition/Transition;
    //   190: astore #11
    //   192: aload #11
    //   194: aload #7
    //   196: aload #12
    //   198: invokevirtual isTransitionRequired : (Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Z
    //   201: ifeq -> 207
    //   204: goto -> 210
    //   207: iconst_0
    //   208: istore #9
    //   210: iload #9
    //   212: ifeq -> 249
    //   215: aload #5
    //   217: invokevirtual isRunning : ()Z
    //   220: ifne -> 244
    //   223: aload #5
    //   225: invokevirtual isStarted : ()Z
    //   228: ifeq -> 234
    //   231: goto -> 244
    //   234: aload_2
    //   235: aload #5
    //   237: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   240: pop
    //   241: goto -> 249
    //   244: aload #5
    //   246: invokevirtual cancel : ()V
    //   249: iinc #3, -1
    //   252: goto -> 52
    //   255: aload_0
    //   256: aload_1
    //   257: aload_0
    //   258: getfield mStartValues : Landroid/transition/TransitionValuesMaps;
    //   261: aload_0
    //   262: getfield mEndValues : Landroid/transition/TransitionValuesMaps;
    //   265: aload_0
    //   266: getfield mStartValuesList : Ljava/util/ArrayList;
    //   269: aload_0
    //   270: getfield mEndValuesList : Ljava/util/ArrayList;
    //   273: invokevirtual createAnimators : (Landroid/view/ViewGroup;Landroid/transition/TransitionValuesMaps;Landroid/transition/TransitionValuesMaps;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    //   276: aload_0
    //   277: invokevirtual runAnimators : ()V
    //   280: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1739	-> 0
    //   #1740	-> 11
    //   #1741	-> 22
    //   #1743	-> 34
    //   #1744	-> 38
    //   #1745	-> 43
    //   #1746	-> 49
    //   #1747	-> 56
    //   #1748	-> 66
    //   #1749	-> 71
    //   #1750	-> 82
    //   #1751	-> 105
    //   #1752	-> 112
    //   #1753	-> 119
    //   #1754	-> 131
    //   #1755	-> 140
    //   #1756	-> 158
    //   #1758	-> 175
    //   #1759	-> 192
    //   #1760	-> 210
    //   #1761	-> 215
    //   #1770	-> 234
    //   #1765	-> 244
    //   #1746	-> 249
    //   #1777	-> 255
    //   #1778	-> 276
    //   #1779	-> 280
  }
  
  public boolean isTransitionRequired(TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    boolean bool1 = false, bool2 = false;
    boolean bool3 = bool1;
    if (paramTransitionValues1 != null) {
      bool3 = bool1;
      if (paramTransitionValues2 != null) {
        String[] arrayOfString = getTransitionProperties();
        if (arrayOfString != null) {
          int i = arrayOfString.length;
          byte b = 0;
          while (true) {
            bool3 = bool2;
            if (b < i) {
              if (isValueChanged(paramTransitionValues1, paramTransitionValues2, arrayOfString[b])) {
                bool3 = true;
                break;
              } 
              b++;
              continue;
            } 
            break;
          } 
        } else {
          Iterator<String> iterator = paramTransitionValues1.values.keySet().iterator();
          while (true) {
            bool3 = bool1;
            if (iterator.hasNext()) {
              String str = iterator.next();
              if (isValueChanged(paramTransitionValues1, paramTransitionValues2, str)) {
                bool3 = true;
                break;
              } 
              continue;
            } 
            break;
          } 
        } 
      } 
    } 
    return bool3;
  }
  
  private static boolean isValueChanged(TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2, String paramString) {
    int i;
    if (paramTransitionValues1.values.containsKey(paramString) != paramTransitionValues2.values.containsKey(paramString))
      return false; 
    paramTransitionValues1 = (TransitionValues)paramTransitionValues1.values.get(paramString);
    paramTransitionValues2 = (TransitionValues)paramTransitionValues2.values.get(paramString);
    if (paramTransitionValues1 == null && paramTransitionValues2 == null) {
      i = 0;
    } else {
      if (paramTransitionValues1 == null || paramTransitionValues2 == null)
        return true; 
      i = paramTransitionValues1.equals(paramTransitionValues2) ^ true;
    } 
    return i;
  }
  
  protected void animate(Animator paramAnimator) {
    if (paramAnimator == null) {
      end();
    } else {
      if (getDuration() >= 0L)
        paramAnimator.setDuration(getDuration()); 
      if (getStartDelay() >= 0L)
        paramAnimator.setStartDelay(getStartDelay() + paramAnimator.getStartDelay()); 
      if (getInterpolator() != null)
        paramAnimator.setInterpolator(getInterpolator()); 
      paramAnimator.addListener((Animator.AnimatorListener)new Object(this));
      paramAnimator.start();
    } 
  }
  
  protected void start() {
    if (this.mNumInstances == 0) {
      ArrayList<TransitionListener> arrayList = this.mListeners;
      if (arrayList != null && arrayList.size() > 0) {
        arrayList = this.mListeners;
        arrayList = (ArrayList<TransitionListener>)arrayList.clone();
        int i = arrayList.size();
        for (byte b = 0; b < i; b++)
          ((TransitionListener)arrayList.get(b)).onTransitionStart(this); 
      } 
      this.mEnded = false;
    } 
    this.mNumInstances++;
  }
  
  protected void end() {
    int i = this.mNumInstances - 1;
    if (i == 0) {
      ArrayList<TransitionListener> arrayList = this.mListeners;
      if (arrayList != null && arrayList.size() > 0) {
        arrayList = this.mListeners;
        arrayList = (ArrayList<TransitionListener>)arrayList.clone();
        int j = arrayList.size();
        for (i = 0; i < j; i++)
          ((TransitionListener)arrayList.get(i)).onTransitionEnd(this); 
      } 
      for (i = 0; i < this.mStartValues.itemIdValues.size(); i++) {
        View view = this.mStartValues.itemIdValues.valueAt(i);
        if (view != null)
          view.setHasTransientState(false); 
      } 
      for (i = 0; i < this.mEndValues.itemIdValues.size(); i++) {
        View view = this.mEndValues.itemIdValues.valueAt(i);
        if (view != null)
          view.setHasTransientState(false); 
      } 
      this.mEnded = true;
    } 
  }
  
  void forceToEnd(ViewGroup paramViewGroup) {
    ArrayMap<Animator, AnimationInfo> arrayMap1 = getRunningAnimators();
    int i = arrayMap1.size();
    if (paramViewGroup == null || i == 0)
      return; 
    WindowId windowId = paramViewGroup.getWindowId();
    ArrayMap<Animator, AnimationInfo> arrayMap2 = new ArrayMap<>(arrayMap1);
    arrayMap1.clear();
    for (; --i >= 0; i--) {
      AnimationInfo animationInfo = arrayMap2.valueAt(i);
      if (animationInfo.view != null && windowId != null && windowId.equals(animationInfo.windowId)) {
        Animator animator = arrayMap2.keyAt(i);
        animator.end();
      } 
    } 
  }
  
  protected void cancel() {
    int i = this.mCurrentAnimators.size();
    for (; --i >= 0; i--) {
      Animator animator = this.mCurrentAnimators.get(i);
      animator.cancel();
    } 
    ArrayList<TransitionListener> arrayList = this.mListeners;
    if (arrayList != null && arrayList.size() > 0) {
      arrayList = this.mListeners;
      arrayList = (ArrayList<TransitionListener>)arrayList.clone();
      int j = arrayList.size();
      for (i = 0; i < j; i++)
        ((TransitionListener)arrayList.get(i)).onTransitionCancel(this); 
    } 
  }
  
  public Transition addListener(TransitionListener paramTransitionListener) {
    if (this.mListeners == null)
      this.mListeners = new ArrayList<>(); 
    this.mListeners.add(paramTransitionListener);
    return this;
  }
  
  public Transition removeListener(TransitionListener paramTransitionListener) {
    ArrayList<TransitionListener> arrayList = this.mListeners;
    if (arrayList == null)
      return this; 
    arrayList.remove(paramTransitionListener);
    if (this.mListeners.size() == 0)
      this.mListeners = null; 
    return this;
  }
  
  public void setEpicenterCallback(EpicenterCallback paramEpicenterCallback) {
    this.mEpicenterCallback = paramEpicenterCallback;
  }
  
  public EpicenterCallback getEpicenterCallback() {
    return this.mEpicenterCallback;
  }
  
  public Rect getEpicenter() {
    EpicenterCallback epicenterCallback = this.mEpicenterCallback;
    if (epicenterCallback == null)
      return null; 
    return epicenterCallback.onGetEpicenter(this);
  }
  
  public void setPathMotion(PathMotion paramPathMotion) {
    if (paramPathMotion == null) {
      this.mPathMotion = STRAIGHT_PATH_MOTION;
    } else {
      this.mPathMotion = paramPathMotion;
    } 
  }
  
  public PathMotion getPathMotion() {
    return this.mPathMotion;
  }
  
  public void setPropagation(TransitionPropagation paramTransitionPropagation) {
    this.mPropagation = paramTransitionPropagation;
  }
  
  public TransitionPropagation getPropagation() {
    return this.mPropagation;
  }
  
  void capturePropagationValues(TransitionValues paramTransitionValues) {
    if (this.mPropagation != null && !paramTransitionValues.values.isEmpty()) {
      boolean bool2;
      String[] arrayOfString = this.mPropagation.getPropagationProperties();
      if (arrayOfString == null)
        return; 
      boolean bool1 = true;
      byte b = 0;
      while (true) {
        bool2 = bool1;
        if (b < arrayOfString.length) {
          if (!paramTransitionValues.values.containsKey(arrayOfString[b])) {
            bool2 = false;
            break;
          } 
          b++;
          continue;
        } 
        break;
      } 
      if (!bool2)
        this.mPropagation.captureValues(paramTransitionValues); 
    } 
  }
  
  Transition setSceneRoot(ViewGroup paramViewGroup) {
    this.mSceneRoot = paramViewGroup;
    return this;
  }
  
  void setCanRemoveViews(boolean paramBoolean) {
    this.mCanRemoveViews = paramBoolean;
  }
  
  public boolean canRemoveViews() {
    return this.mCanRemoveViews;
  }
  
  public void setNameOverrides(ArrayMap<String, String> paramArrayMap) {
    this.mNameOverrides = paramArrayMap;
  }
  
  public ArrayMap<String, String> getNameOverrides() {
    return this.mNameOverrides;
  }
  
  public String toString() {
    return toString("");
  }
  
  public Transition clone() {
    Transition transition = null;
    try {
      Transition transition1 = (Transition)super.clone();
      transition = transition1;
      ArrayList<Animator> arrayList = new ArrayList();
      transition = transition1;
      this();
      transition = transition1;
      transition1.mAnimators = arrayList;
      transition = transition1;
      TransitionValuesMaps transitionValuesMaps = new TransitionValuesMaps();
      transition = transition1;
      this();
      transition = transition1;
      transition1.mStartValues = transitionValuesMaps;
      transition = transition1;
      transitionValuesMaps = new TransitionValuesMaps();
      transition = transition1;
      this();
      transition = transition1;
      transition1.mEndValues = transitionValuesMaps;
      transition = transition1;
      transition1.mStartValuesList = null;
      transition = transition1;
      transition1.mEndValuesList = null;
      transition = transition1;
    } catch (CloneNotSupportedException cloneNotSupportedException) {}
    return transition;
  }
  
  public String getName() {
    return this.mName;
  }
  
  String toString(String paramString) {
    // Byte code:
    //   0: new java/lang/StringBuilder
    //   3: dup
    //   4: invokespecial <init> : ()V
    //   7: astore_2
    //   8: aload_2
    //   9: aload_1
    //   10: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13: pop
    //   14: aload_2
    //   15: aload_0
    //   16: invokevirtual getClass : ()Ljava/lang/Class;
    //   19: invokevirtual getSimpleName : ()Ljava/lang/String;
    //   22: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   25: pop
    //   26: aload_2
    //   27: ldc_w '@'
    //   30: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   33: pop
    //   34: aload_2
    //   35: aload_0
    //   36: invokevirtual hashCode : ()I
    //   39: invokestatic toHexString : (I)Ljava/lang/String;
    //   42: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   45: pop
    //   46: aload_2
    //   47: ldc_w ': '
    //   50: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   53: pop
    //   54: aload_2
    //   55: invokevirtual toString : ()Ljava/lang/String;
    //   58: astore_2
    //   59: aload_2
    //   60: astore_1
    //   61: aload_0
    //   62: getfield mDuration : J
    //   65: ldc2_w -1
    //   68: lcmp
    //   69: ifeq -> 116
    //   72: new java/lang/StringBuilder
    //   75: dup
    //   76: invokespecial <init> : ()V
    //   79: astore_1
    //   80: aload_1
    //   81: aload_2
    //   82: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   85: pop
    //   86: aload_1
    //   87: ldc_w 'dur('
    //   90: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   93: pop
    //   94: aload_1
    //   95: aload_0
    //   96: getfield mDuration : J
    //   99: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   102: pop
    //   103: aload_1
    //   104: ldc_w ') '
    //   107: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   110: pop
    //   111: aload_1
    //   112: invokevirtual toString : ()Ljava/lang/String;
    //   115: astore_1
    //   116: aload_1
    //   117: astore_2
    //   118: aload_0
    //   119: getfield mStartDelay : J
    //   122: ldc2_w -1
    //   125: lcmp
    //   126: ifeq -> 173
    //   129: new java/lang/StringBuilder
    //   132: dup
    //   133: invokespecial <init> : ()V
    //   136: astore_2
    //   137: aload_2
    //   138: aload_1
    //   139: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   142: pop
    //   143: aload_2
    //   144: ldc_w 'dly('
    //   147: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   150: pop
    //   151: aload_2
    //   152: aload_0
    //   153: getfield mStartDelay : J
    //   156: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   159: pop
    //   160: aload_2
    //   161: ldc_w ') '
    //   164: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   167: pop
    //   168: aload_2
    //   169: invokevirtual toString : ()Ljava/lang/String;
    //   172: astore_2
    //   173: aload_2
    //   174: astore_1
    //   175: aload_0
    //   176: getfield mInterpolator : Landroid/animation/TimeInterpolator;
    //   179: ifnull -> 226
    //   182: new java/lang/StringBuilder
    //   185: dup
    //   186: invokespecial <init> : ()V
    //   189: astore_1
    //   190: aload_1
    //   191: aload_2
    //   192: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   195: pop
    //   196: aload_1
    //   197: ldc_w 'interp('
    //   200: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   203: pop
    //   204: aload_1
    //   205: aload_0
    //   206: getfield mInterpolator : Landroid/animation/TimeInterpolator;
    //   209: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   212: pop
    //   213: aload_1
    //   214: ldc_w ') '
    //   217: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   220: pop
    //   221: aload_1
    //   222: invokevirtual toString : ()Ljava/lang/String;
    //   225: astore_1
    //   226: aload_0
    //   227: getfield mTargetIds : Ljava/util/ArrayList;
    //   230: invokevirtual size : ()I
    //   233: ifgt -> 248
    //   236: aload_1
    //   237: astore_2
    //   238: aload_0
    //   239: getfield mTargets : Ljava/util/ArrayList;
    //   242: invokevirtual size : ()I
    //   245: ifle -> 498
    //   248: new java/lang/StringBuilder
    //   251: dup
    //   252: invokespecial <init> : ()V
    //   255: astore_2
    //   256: aload_2
    //   257: aload_1
    //   258: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   261: pop
    //   262: aload_2
    //   263: ldc_w 'tgts('
    //   266: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   269: pop
    //   270: aload_2
    //   271: invokevirtual toString : ()Ljava/lang/String;
    //   274: astore_2
    //   275: aload_2
    //   276: astore_1
    //   277: aload_0
    //   278: getfield mTargetIds : Ljava/util/ArrayList;
    //   281: invokevirtual size : ()I
    //   284: ifle -> 373
    //   287: iconst_0
    //   288: istore_3
    //   289: aload_2
    //   290: astore_1
    //   291: iload_3
    //   292: aload_0
    //   293: getfield mTargetIds : Ljava/util/ArrayList;
    //   296: invokevirtual size : ()I
    //   299: if_icmpge -> 373
    //   302: aload_2
    //   303: astore_1
    //   304: iload_3
    //   305: ifle -> 335
    //   308: new java/lang/StringBuilder
    //   311: dup
    //   312: invokespecial <init> : ()V
    //   315: astore_1
    //   316: aload_1
    //   317: aload_2
    //   318: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   321: pop
    //   322: aload_1
    //   323: ldc_w ', '
    //   326: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   329: pop
    //   330: aload_1
    //   331: invokevirtual toString : ()Ljava/lang/String;
    //   334: astore_1
    //   335: new java/lang/StringBuilder
    //   338: dup
    //   339: invokespecial <init> : ()V
    //   342: astore_2
    //   343: aload_2
    //   344: aload_1
    //   345: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   348: pop
    //   349: aload_2
    //   350: aload_0
    //   351: getfield mTargetIds : Ljava/util/ArrayList;
    //   354: iload_3
    //   355: invokevirtual get : (I)Ljava/lang/Object;
    //   358: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   361: pop
    //   362: aload_2
    //   363: invokevirtual toString : ()Ljava/lang/String;
    //   366: astore_2
    //   367: iinc #3, 1
    //   370: goto -> 289
    //   373: aload_1
    //   374: astore_2
    //   375: aload_0
    //   376: getfield mTargets : Ljava/util/ArrayList;
    //   379: invokevirtual size : ()I
    //   382: ifle -> 471
    //   385: iconst_0
    //   386: istore_3
    //   387: aload_1
    //   388: astore_2
    //   389: iload_3
    //   390: aload_0
    //   391: getfield mTargets : Ljava/util/ArrayList;
    //   394: invokevirtual size : ()I
    //   397: if_icmpge -> 471
    //   400: aload_1
    //   401: astore_2
    //   402: iload_3
    //   403: ifle -> 433
    //   406: new java/lang/StringBuilder
    //   409: dup
    //   410: invokespecial <init> : ()V
    //   413: astore_2
    //   414: aload_2
    //   415: aload_1
    //   416: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   419: pop
    //   420: aload_2
    //   421: ldc_w ', '
    //   424: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   427: pop
    //   428: aload_2
    //   429: invokevirtual toString : ()Ljava/lang/String;
    //   432: astore_2
    //   433: new java/lang/StringBuilder
    //   436: dup
    //   437: invokespecial <init> : ()V
    //   440: astore_1
    //   441: aload_1
    //   442: aload_2
    //   443: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   446: pop
    //   447: aload_1
    //   448: aload_0
    //   449: getfield mTargets : Ljava/util/ArrayList;
    //   452: iload_3
    //   453: invokevirtual get : (I)Ljava/lang/Object;
    //   456: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   459: pop
    //   460: aload_1
    //   461: invokevirtual toString : ()Ljava/lang/String;
    //   464: astore_1
    //   465: iinc #3, 1
    //   468: goto -> 387
    //   471: new java/lang/StringBuilder
    //   474: dup
    //   475: invokespecial <init> : ()V
    //   478: astore_1
    //   479: aload_1
    //   480: aload_2
    //   481: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   484: pop
    //   485: aload_1
    //   486: ldc_w ')'
    //   489: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   492: pop
    //   493: aload_1
    //   494: invokevirtual toString : ()Ljava/lang/String;
    //   497: astore_2
    //   498: aload_2
    //   499: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2258	-> 0
    //   #2259	-> 34
    //   #2260	-> 59
    //   #2261	-> 72
    //   #2263	-> 116
    //   #2264	-> 129
    //   #2266	-> 173
    //   #2267	-> 182
    //   #2269	-> 226
    //   #2270	-> 248
    //   #2271	-> 275
    //   #2272	-> 287
    //   #2273	-> 302
    //   #2274	-> 308
    //   #2276	-> 335
    //   #2272	-> 367
    //   #2279	-> 373
    //   #2280	-> 385
    //   #2281	-> 400
    //   #2282	-> 406
    //   #2284	-> 433
    //   #2280	-> 465
    //   #2287	-> 471
    //   #2289	-> 498
  }
  
  public Transition() {}
  
  public abstract void captureEndValues(TransitionValues paramTransitionValues);
  
  public abstract void captureStartValues(TransitionValues paramTransitionValues);
  
  public static class AnimationInfo {
    String name;
    
    Transition transition;
    
    TransitionValues values;
    
    public View view;
    
    WindowId windowId;
    
    AnimationInfo(View param1View, String param1String, Transition param1Transition, WindowId param1WindowId, TransitionValues param1TransitionValues) {
      this.view = param1View;
      this.name = param1String;
      this.values = param1TransitionValues;
      this.windowId = param1WindowId;
      this.transition = param1Transition;
    }
  }
  
  private static class ArrayListManager {
    static <T> ArrayList<T> add(ArrayList<T> param1ArrayList, T param1T) {
      ArrayList<T> arrayList = param1ArrayList;
      if (param1ArrayList == null)
        arrayList = new ArrayList<>(); 
      if (!arrayList.contains(param1T))
        arrayList.add(param1T); 
      return arrayList;
    }
    
    static <T> ArrayList<T> remove(ArrayList<T> param1ArrayList, T param1T) {
      ArrayList<T> arrayList = param1ArrayList;
      if (param1ArrayList != null) {
        param1ArrayList.remove(param1T);
        arrayList = param1ArrayList;
        if (param1ArrayList.isEmpty())
          arrayList = null; 
      } 
      return arrayList;
    }
  }
  
  public static abstract class EpicenterCallback {
    public abstract Rect onGetEpicenter(Transition param1Transition);
  }
  
  public static interface TransitionListener {
    void onTransitionCancel(Transition param1Transition);
    
    void onTransitionEnd(Transition param1Transition);
    
    void onTransitionPause(Transition param1Transition);
    
    void onTransitionResume(Transition param1Transition);
    
    void onTransitionStart(Transition param1Transition);
  }
}
