package android.transition;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.RectEvaluator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOverlay;
import java.util.Map;

public class Crossfade extends Transition {
  public static final int FADE_BEHAVIOR_CROSSFADE = 0;
  
  public static final int FADE_BEHAVIOR_OUT_IN = 2;
  
  public static final int FADE_BEHAVIOR_REVEAL = 1;
  
  private static final String LOG_TAG = "Crossfade";
  
  private static final String PROPNAME_BITMAP = "android:crossfade:bitmap";
  
  private static final String PROPNAME_BOUNDS = "android:crossfade:bounds";
  
  private static final String PROPNAME_DRAWABLE = "android:crossfade:drawable";
  
  public static final int RESIZE_BEHAVIOR_NONE = 0;
  
  public static final int RESIZE_BEHAVIOR_SCALE = 1;
  
  private static RectEvaluator sRectEvaluator = new RectEvaluator();
  
  private int mFadeBehavior = 1;
  
  private int mResizeBehavior = 1;
  
  public Crossfade setFadeBehavior(int paramInt) {
    if (paramInt >= 0 && paramInt <= 2)
      this.mFadeBehavior = paramInt; 
    return this;
  }
  
  public int getFadeBehavior() {
    return this.mFadeBehavior;
  }
  
  public Crossfade setResizeBehavior(int paramInt) {
    if (paramInt >= 0 && paramInt <= 1)
      this.mResizeBehavior = paramInt; 
    return this;
  }
  
  public int getResizeBehavior() {
    return this.mResizeBehavior;
  }
  
  public Animator createAnimator(ViewGroup paramViewGroup, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    boolean bool;
    if (paramTransitionValues1 == null || paramTransitionValues2 == null)
      return null; 
    if (this.mFadeBehavior != 1) {
      bool = true;
    } else {
      bool = false;
    } 
    final View view = paramTransitionValues2.view;
    Map<String, Object> map1 = paramTransitionValues1.values;
    Map<String, Object> map2 = paramTransitionValues2.values;
    Rect rect1 = (Rect)map1.get("android:crossfade:bounds");
    Rect rect2 = (Rect)map2.get("android:crossfade:bounds");
    Bitmap bitmap1 = (Bitmap)map1.get("android:crossfade:bitmap");
    Bitmap bitmap2 = (Bitmap)map2.get("android:crossfade:bitmap");
    final BitmapDrawable startDrawable = (BitmapDrawable)map1.get("android:crossfade:drawable");
    BitmapDrawable bitmapDrawable2 = (BitmapDrawable)map2.get("android:crossfade:drawable");
    if (bitmapDrawable1 != null && bitmapDrawable2 != null && !bitmap1.sameAs(bitmap2)) {
      ObjectAnimator objectAnimator1, objectAnimator2;
      if (bool) {
        viewOverlay = ((ViewGroup)view.getParent()).getOverlay();
      } else {
        viewOverlay = view.getOverlay();
      } 
      if (this.mFadeBehavior == 1)
        viewOverlay.add((Drawable)bitmapDrawable2); 
      viewOverlay.add((Drawable)bitmapDrawable1);
      if (this.mFadeBehavior == 2) {
        objectAnimator2 = ObjectAnimator.ofInt(bitmapDrawable1, "alpha", new int[] { 255, 0, 0 });
      } else {
        objectAnimator2 = ObjectAnimator.ofInt(bitmapDrawable1, "alpha", new int[] { 0 });
      } 
      objectAnimator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            final Crossfade this$0;
            
            final BitmapDrawable val$startDrawable;
            
            final View val$view;
            
            public void onAnimationUpdate(ValueAnimator param1ValueAnimator) {
              view.invalidate(startDrawable.getBounds());
            }
          });
      ViewOverlay viewOverlay = null;
      int i = this.mFadeBehavior;
      if (i == 2) {
        objectAnimator1 = ObjectAnimator.ofFloat(view, View.ALPHA, new float[] { 0.0F, 0.0F, 1.0F });
      } else if (i == 0) {
        objectAnimator1 = ObjectAnimator.ofFloat(view, View.ALPHA, new float[] { 0.0F, 1.0F });
      } 
      objectAnimator2.addListener((Animator.AnimatorListener)new Object(this, bool, view, bitmapDrawable1, bitmapDrawable2));
      AnimatorSet animatorSet = new AnimatorSet();
      animatorSet.playTogether(new Animator[] { (Animator)objectAnimator2 });
      if (objectAnimator1 != null)
        animatorSet.playTogether(new Animator[] { (Animator)objectAnimator1 }); 
      if (this.mResizeBehavior == 1 && !rect1.equals(rect2)) {
        objectAnimator1 = ObjectAnimator.ofObject(bitmapDrawable1, "bounds", (TypeEvaluator)sRectEvaluator, new Object[] { rect1, rect2 });
        animatorSet.playTogether(new Animator[] { (Animator)objectAnimator1 });
        if (this.mResizeBehavior == 1) {
          objectAnimator1 = ObjectAnimator.ofObject(bitmapDrawable2, "bounds", (TypeEvaluator)sRectEvaluator, new Object[] { rect1, rect2 });
          animatorSet.playTogether(new Animator[] { (Animator)objectAnimator1 });
        } 
      } 
      return (Animator)animatorSet;
    } 
    return null;
  }
  
  private void captureValues(TransitionValues paramTransitionValues) {
    View view = paramTransitionValues.view;
    Rect rect = new Rect(0, 0, view.getWidth(), view.getHeight());
    if (this.mFadeBehavior != 1)
      rect.offset(view.getLeft(), view.getTop()); 
    paramTransitionValues.values.put("android:crossfade:bounds", rect);
    Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
    if (view instanceof TextureView) {
      bitmap = ((TextureView)view).getBitmap();
    } else {
      Canvas canvas = new Canvas(bitmap);
      view.draw(canvas);
    } 
    paramTransitionValues.values.put("android:crossfade:bitmap", bitmap);
    BitmapDrawable bitmapDrawable = new BitmapDrawable(bitmap);
    bitmapDrawable.setBounds(rect);
    paramTransitionValues.values.put("android:crossfade:drawable", bitmapDrawable);
  }
  
  public void captureStartValues(TransitionValues paramTransitionValues) {
    captureValues(paramTransitionValues);
  }
  
  public void captureEndValues(TransitionValues paramTransitionValues) {
    captureValues(paramTransitionValues);
  }
}
