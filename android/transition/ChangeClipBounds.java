package android.transition;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.RectEvaluator;
import android.animation.TypeEvaluator;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import java.util.Map;

public class ChangeClipBounds extends Transition {
  private static final String PROPNAME_BOUNDS = "android:clipBounds:bounds";
  
  private static final String PROPNAME_CLIP = "android:clipBounds:clip";
  
  private static final String TAG = "ChangeTransform";
  
  private static final String[] sTransitionProperties = new String[] { "android:clipBounds:clip" };
  
  public ChangeClipBounds() {}
  
  public ChangeClipBounds(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public String[] getTransitionProperties() {
    return sTransitionProperties;
  }
  
  private void captureValues(TransitionValues paramTransitionValues) {
    View view = paramTransitionValues.view;
    if (view.getVisibility() == 8)
      return; 
    Rect rect = view.getClipBounds();
    paramTransitionValues.values.put("android:clipBounds:clip", rect);
    if (rect == null) {
      Rect rect1 = new Rect(0, 0, view.getWidth(), view.getHeight());
      paramTransitionValues.values.put("android:clipBounds:bounds", rect1);
    } 
  }
  
  public void captureStartValues(TransitionValues paramTransitionValues) {
    captureValues(paramTransitionValues);
  }
  
  public void captureEndValues(TransitionValues paramTransitionValues) {
    captureValues(paramTransitionValues);
  }
  
  public Animator createAnimator(ViewGroup paramViewGroup, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    if (paramTransitionValues1 != null && paramTransitionValues2 != null) {
      Map<String, Object> map = paramTransitionValues1.values;
      if (map.containsKey("android:clipBounds:clip")) {
        map = paramTransitionValues2.values;
        if (map.containsKey("android:clipBounds:clip")) {
          Rect rect1, rect2;
          boolean bool;
          Rect rect3 = (Rect)paramTransitionValues1.values.get("android:clipBounds:clip");
          Rect rect4 = (Rect)paramTransitionValues2.values.get("android:clipBounds:clip");
          if (rect4 == null) {
            bool = true;
          } else {
            bool = false;
          } 
          if (rect3 == null && rect4 == null)
            return null; 
          if (rect3 == null) {
            rect1 = (Rect)paramTransitionValues1.values.get("android:clipBounds:bounds");
            rect2 = rect4;
          } else {
            rect1 = rect3;
            rect2 = rect4;
            if (rect4 == null) {
              rect2 = (Rect)paramTransitionValues2.values.get("android:clipBounds:bounds");
              rect1 = rect3;
            } 
          } 
          if (rect1.equals(rect2))
            return null; 
          paramTransitionValues2.view.setClipBounds(rect1);
          RectEvaluator rectEvaluator = new RectEvaluator(new Rect());
          View view = paramTransitionValues2.view;
          ObjectAnimator objectAnimator = ObjectAnimator.ofObject(view, "clipBounds", (TypeEvaluator)rectEvaluator, new Object[] { rect1, rect2 });
          if (bool) {
            View view1 = paramTransitionValues2.view;
            objectAnimator.addListener((Animator.AnimatorListener)new Object(this, view1));
          } 
          return (Animator)objectAnimator;
        } 
      } 
    } 
    return null;
  }
}
