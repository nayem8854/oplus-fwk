package android.apex;

import android.os.Parcel;
import android.os.Parcelable;

public class ApexInfoList implements Parcelable {
  public static final Parcelable.Creator<ApexInfoList> CREATOR = new Parcelable.Creator<ApexInfoList>() {
      public ApexInfoList createFromParcel(Parcel param1Parcel) {
        ApexInfoList apexInfoList = new ApexInfoList();
        apexInfoList.readFromParcel(param1Parcel);
        return apexInfoList;
      }
      
      public ApexInfoList[] newArray(int param1Int) {
        return new ApexInfoList[param1Int];
      }
    };
  
  public ApexInfo[] apexInfos;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    int i = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeTypedArray((Parcelable[])this.apexInfos, 0);
    paramInt = paramParcel.dataPosition();
    paramParcel.setDataPosition(i);
    paramParcel.writeInt(paramInt - i);
    paramParcel.setDataPosition(paramInt);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      this.apexInfos = (ApexInfo[])paramParcel.createTypedArray(ApexInfo.CREATOR);
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
