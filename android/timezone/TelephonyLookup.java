package android.timezone;

import java.util.Objects;
import libcore.timezone.TelephonyNetworkFinder;

public final class TelephonyLookup {
  private static TelephonyLookup sInstance;
  
  private static final Object sLock = new Object();
  
  private final libcore.timezone.TelephonyLookup mDelegate;
  
  public static TelephonyLookup getInstance() {
    synchronized (sLock) {
      if (sInstance == null) {
        TelephonyLookup telephonyLookup = new TelephonyLookup();
        this(libcore.timezone.TelephonyLookup.getInstance());
        sInstance = telephonyLookup;
      } 
      return sInstance;
    } 
  }
  
  private TelephonyLookup(libcore.timezone.TelephonyLookup paramTelephonyLookup) {
    Objects.requireNonNull(paramTelephonyLookup);
    this.mDelegate = paramTelephonyLookup;
  }
  
  public TelephonyNetworkFinder getTelephonyNetworkFinder() {
    libcore.timezone.TelephonyLookup telephonyLookup = this.mDelegate;
    TelephonyNetworkFinder telephonyNetworkFinder = telephonyLookup.getTelephonyNetworkFinder();
    if (telephonyNetworkFinder != null) {
      TelephonyNetworkFinder telephonyNetworkFinder1 = new TelephonyNetworkFinder(telephonyNetworkFinder);
    } else {
      telephonyNetworkFinder = null;
    } 
    return (TelephonyNetworkFinder)telephonyNetworkFinder;
  }
}
