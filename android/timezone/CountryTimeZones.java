package android.timezone;

import android.icu.util.TimeZone;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class CountryTimeZones {
  private final libcore.timezone.CountryTimeZones mDelegate;
  
  public static final class TimeZoneMapping {
    private libcore.timezone.CountryTimeZones.TimeZoneMapping mDelegate;
    
    TimeZoneMapping(libcore.timezone.CountryTimeZones.TimeZoneMapping param1TimeZoneMapping) {
      Objects.requireNonNull(param1TimeZoneMapping);
      this.mDelegate = param1TimeZoneMapping;
    }
    
    public String getTimeZoneId() {
      return this.mDelegate.getTimeZoneId();
    }
    
    public TimeZone getTimeZone() {
      return this.mDelegate.getTimeZone();
    }
    
    public boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      return this.mDelegate.equals(((TimeZoneMapping)param1Object).mDelegate);
    }
    
    public int hashCode() {
      return this.mDelegate.hashCode();
    }
    
    public String toString() {
      return this.mDelegate.toString();
    }
  }
  
  public static final class OffsetResult {
    private final boolean mIsOnlyMatch;
    
    private final TimeZone mTimeZone;
    
    public OffsetResult(TimeZone param1TimeZone, boolean param1Boolean) {
      Objects.requireNonNull(param1TimeZone);
      this.mTimeZone = param1TimeZone;
      this.mIsOnlyMatch = param1Boolean;
    }
    
    public TimeZone getTimeZone() {
      return this.mTimeZone;
    }
    
    public boolean isOnlyMatch() {
      return this.mIsOnlyMatch;
    }
    
    public boolean equals(Object param1Object) {
      null = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      if (this.mIsOnlyMatch == ((OffsetResult)param1Object).mIsOnlyMatch) {
        TimeZone timeZone = this.mTimeZone;
        if (timeZone.getID().equals(((OffsetResult)param1Object).mTimeZone.getID()))
          return null; 
      } 
      return false;
    }
    
    public int hashCode() {
      return Objects.hash(new Object[] { this.mTimeZone, Boolean.valueOf(this.mIsOnlyMatch) });
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("OffsetResult{mTimeZone(ID)=");
      TimeZone timeZone = this.mTimeZone;
      stringBuilder.append(timeZone.getID());
      stringBuilder.append(", mIsOnlyMatch=");
      stringBuilder.append(this.mIsOnlyMatch);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
  }
  
  CountryTimeZones(libcore.timezone.CountryTimeZones paramCountryTimeZones) {
    this.mDelegate = paramCountryTimeZones;
  }
  
  public boolean matchesCountryCode(String paramString) {
    return this.mDelegate.isForCountryCode(paramString);
  }
  
  public String getDefaultTimeZoneId() {
    return this.mDelegate.getDefaultTimeZoneId();
  }
  
  public TimeZone getDefaultTimeZone() {
    return this.mDelegate.getDefaultTimeZone();
  }
  
  public boolean isDefaultTimeZoneBoosted() {
    return this.mDelegate.isDefaultTimeZoneBoosted();
  }
  
  public boolean hasUtcZone(long paramLong) {
    return this.mDelegate.hasUtcZone(paramLong);
  }
  
  public OffsetResult lookupByOffsetWithBias(long paramLong, TimeZone paramTimeZone, int paramInt, boolean paramBoolean) {
    OffsetResult offsetResult;
    libcore.timezone.CountryTimeZones countryTimeZones = this.mDelegate;
    libcore.timezone.CountryTimeZones.OffsetResult offsetResult1 = countryTimeZones.lookupByOffsetWithBias(paramLong, paramTimeZone, paramInt, paramBoolean);
    if (offsetResult1 == null) {
      offsetResult1 = null;
    } else {
      offsetResult = new OffsetResult(offsetResult1.getTimeZone(), offsetResult1.isOnlyMatch());
    } 
    return offsetResult;
  }
  
  public OffsetResult lookupByOffsetWithBias(long paramLong, TimeZone paramTimeZone, int paramInt) {
    OffsetResult offsetResult;
    libcore.timezone.CountryTimeZones countryTimeZones = this.mDelegate;
    libcore.timezone.CountryTimeZones.OffsetResult offsetResult1 = countryTimeZones.lookupByOffsetWithBias(paramLong, paramTimeZone, paramInt);
    if (offsetResult1 == null) {
      offsetResult1 = null;
    } else {
      offsetResult = new OffsetResult(offsetResult1.getTimeZone(), offsetResult1.isOnlyMatch());
    } 
    return offsetResult;
  }
  
  public List<TimeZoneMapping> getEffectiveTimeZoneMappingsAt(long paramLong) {
    libcore.timezone.CountryTimeZones countryTimeZones = this.mDelegate;
    List list = countryTimeZones.getEffectiveTimeZoneMappingsAt(paramLong);
    ArrayList<TimeZoneMapping> arrayList = new ArrayList(list.size());
    for (libcore.timezone.CountryTimeZones.TimeZoneMapping timeZoneMapping : list)
      arrayList.add(new TimeZoneMapping(timeZoneMapping)); 
    return Collections.unmodifiableList(arrayList);
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    return this.mDelegate.equals(((CountryTimeZones)paramObject).mDelegate);
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mDelegate });
  }
  
  public String toString() {
    return this.mDelegate.toString();
  }
}
