package android.timezone;

import java.util.Objects;
import libcore.timezone.CountryTimeZones;

public final class TimeZoneFinder {
  private static TimeZoneFinder sInstance;
  
  private static final Object sLock = new Object();
  
  private final libcore.timezone.TimeZoneFinder mDelegate;
  
  public static TimeZoneFinder getInstance() {
    synchronized (sLock) {
      if (sInstance == null) {
        TimeZoneFinder timeZoneFinder = new TimeZoneFinder();
        this(libcore.timezone.TimeZoneFinder.getInstance());
        sInstance = timeZoneFinder;
      } 
      return sInstance;
    } 
  }
  
  private TimeZoneFinder(libcore.timezone.TimeZoneFinder paramTimeZoneFinder) {
    Objects.requireNonNull(paramTimeZoneFinder);
    this.mDelegate = paramTimeZoneFinder;
  }
  
  public String getIanaVersion() {
    return this.mDelegate.getIanaVersion();
  }
  
  public CountryTimeZones lookupCountryTimeZones(String paramString) {
    CountryTimeZones countryTimeZones;
    CountryTimeZones countryTimeZones1 = this.mDelegate.lookupCountryTimeZones(paramString);
    if (countryTimeZones1 == null) {
      countryTimeZones1 = null;
    } else {
      countryTimeZones = new CountryTimeZones(countryTimeZones1);
    } 
    return countryTimeZones;
  }
}
