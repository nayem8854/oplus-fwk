package android.timezone;

import java.util.Objects;
import libcore.timezone.TelephonyNetwork;

public final class TelephonyNetworkFinder {
  private final libcore.timezone.TelephonyNetworkFinder mDelegate;
  
  TelephonyNetworkFinder(libcore.timezone.TelephonyNetworkFinder paramTelephonyNetworkFinder) {
    Objects.requireNonNull(paramTelephonyNetworkFinder);
    this.mDelegate = paramTelephonyNetworkFinder;
  }
  
  public TelephonyNetwork findNetworkByMccMnc(String paramString1, String paramString2) {
    Objects.requireNonNull(paramString1);
    Objects.requireNonNull(paramString2);
    libcore.timezone.TelephonyNetworkFinder telephonyNetworkFinder = this.mDelegate;
    TelephonyNetwork telephonyNetwork = telephonyNetworkFinder.findNetworkByMccMnc(paramString1, paramString2);
    if (telephonyNetwork != null) {
      TelephonyNetwork telephonyNetwork1 = new TelephonyNetwork(telephonyNetwork);
    } else {
      telephonyNetwork = null;
    } 
    return (TelephonyNetwork)telephonyNetwork;
  }
}
