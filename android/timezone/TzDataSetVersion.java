package android.timezone;

import java.io.IOException;
import java.util.Objects;

public final class TzDataSetVersion {
  private final libcore.timezone.TzDataSetVersion mDelegate;
  
  public static int currentFormatMajorVersion() {
    return libcore.timezone.TzDataSetVersion.currentFormatMajorVersion();
  }
  
  public static int currentFormatMinorVersion() {
    return libcore.timezone.TzDataSetVersion.currentFormatMinorVersion();
  }
  
  public static boolean isCompatibleWithThisDevice(TzDataSetVersion paramTzDataSetVersion) {
    return libcore.timezone.TzDataSetVersion.isCompatibleWithThisDevice(paramTzDataSetVersion.mDelegate);
  }
  
  public static TzDataSetVersion read() throws IOException, TzDataSetException {
    try {
      return 
        new TzDataSetVersion(libcore.timezone.TzDataSetVersion.readTimeZoneModuleVersion());
    } catch (libcore.timezone.TzDataSetVersion.TzDataSetException tzDataSetException) {
      throw new TzDataSetException(tzDataSetException.getMessage(), tzDataSetException);
    } 
  }
  
  public static final class TzDataSetException extends Exception {
    public TzDataSetException(String param1String) {
      super(param1String);
    }
    
    public TzDataSetException(String param1String, Throwable param1Throwable) {
      super(param1String, param1Throwable);
    }
  }
  
  private TzDataSetVersion(libcore.timezone.TzDataSetVersion paramTzDataSetVersion) {
    Objects.requireNonNull(paramTzDataSetVersion);
    this.mDelegate = paramTzDataSetVersion;
  }
  
  public int getFormatMajorVersion() {
    return this.mDelegate.getFormatMajorVersion();
  }
  
  public int getFormatMinorVersion() {
    return this.mDelegate.getFormatMinorVersion();
  }
  
  public String getRulesVersion() {
    return this.mDelegate.getRulesVersion();
  }
  
  public int getRevision() {
    return this.mDelegate.getRevision();
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    return this.mDelegate.equals(((TzDataSetVersion)paramObject).mDelegate);
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mDelegate });
  }
  
  public String toString() {
    return this.mDelegate.toString();
  }
}
