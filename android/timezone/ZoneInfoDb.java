package android.timezone;

import java.util.Objects;

public final class ZoneInfoDb {
  private static ZoneInfoDb sInstance;
  
  private static final Object sLock = new Object();
  
  private final libcore.timezone.ZoneInfoDb mDelegate;
  
  public static ZoneInfoDb getInstance() {
    synchronized (sLock) {
      if (sInstance == null) {
        ZoneInfoDb zoneInfoDb = new ZoneInfoDb();
        this(libcore.timezone.ZoneInfoDb.getInstance());
        sInstance = zoneInfoDb;
      } 
      return sInstance;
    } 
  }
  
  private ZoneInfoDb(libcore.timezone.ZoneInfoDb paramZoneInfoDb) {
    Objects.requireNonNull(paramZoneInfoDb);
    this.mDelegate = paramZoneInfoDb;
  }
  
  public String getVersion() {
    return this.mDelegate.getVersion();
  }
}
