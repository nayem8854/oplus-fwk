package android.timezone;

import java.util.Objects;

public final class TelephonyNetwork {
  private final libcore.timezone.TelephonyNetwork mDelegate;
  
  TelephonyNetwork(libcore.timezone.TelephonyNetwork paramTelephonyNetwork) {
    Objects.requireNonNull(paramTelephonyNetwork);
    this.mDelegate = paramTelephonyNetwork;
  }
  
  public String getMcc() {
    return this.mDelegate.getMcc();
  }
  
  public String getMnc() {
    return this.mDelegate.getMnc();
  }
  
  public String getCountryIsoCode() {
    return this.mDelegate.getCountryIsoCode();
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    return this.mDelegate.equals(((TelephonyNetwork)paramObject).mDelegate);
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mDelegate });
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("TelephonyNetwork{mDelegate=");
    stringBuilder.append(this.mDelegate);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
}
