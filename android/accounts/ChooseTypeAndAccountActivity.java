package android.accounts;

import android.app.Activity;
import android.app.ActivityTaskManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.UserHandle;
import android.os.UserManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.google.android.collect.Sets;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class ChooseTypeAndAccountActivity extends Activity implements AccountManagerCallback<Bundle> {
  private String mSelectedAccountName = null;
  
  private boolean mSelectedAddNewAccount = false;
  
  private int mPendingRequest = 0;
  
  private Parcelable[] mExistingAccounts = null;
  
  public static final String EXTRA_ADD_ACCOUNT_AUTH_TOKEN_TYPE_STRING = "authTokenType";
  
  public static final String EXTRA_ADD_ACCOUNT_OPTIONS_BUNDLE = "addAccountOptions";
  
  public static final String EXTRA_ADD_ACCOUNT_REQUIRED_FEATURES_STRING_ARRAY = "addAccountRequiredFeatures";
  
  public static final String EXTRA_ALLOWABLE_ACCOUNTS_ARRAYLIST = "allowableAccounts";
  
  public static final String EXTRA_ALLOWABLE_ACCOUNT_TYPES_STRING_ARRAY = "allowableAccountTypes";
  
  @Deprecated
  public static final String EXTRA_ALWAYS_PROMPT_FOR_ACCOUNT = "alwaysPromptForAccount";
  
  public static final String EXTRA_DESCRIPTION_TEXT_OVERRIDE = "descriptionTextOverride";
  
  public static final String EXTRA_SELECTED_ACCOUNT = "selectedAccount";
  
  private static final String KEY_INSTANCE_STATE_ACCOUNTS_LIST = "accountsList";
  
  private static final String KEY_INSTANCE_STATE_EXISTING_ACCOUNTS = "existingAccounts";
  
  private static final String KEY_INSTANCE_STATE_PENDING_REQUEST = "pendingRequest";
  
  private static final String KEY_INSTANCE_STATE_SELECTED_ACCOUNT_NAME = "selectedAccountName";
  
  private static final String KEY_INSTANCE_STATE_SELECTED_ADD_ACCOUNT = "selectedAddAccount";
  
  private static final String KEY_INSTANCE_STATE_VISIBILITY_LIST = "visibilityList";
  
  public static final int REQUEST_ADD_ACCOUNT = 2;
  
  public static final int REQUEST_CHOOSE_TYPE = 1;
  
  public static final int REQUEST_NULL = 0;
  
  private static final int SELECTED_ITEM_NONE = -1;
  
  private static final String TAG = "AccountChooser";
  
  private LinkedHashMap<Account, Integer> mAccounts;
  
  private String mCallingPackage;
  
  private int mCallingUid;
  
  private String mDescriptionOverride;
  
  private boolean mDisallowAddAccounts;
  
  private boolean mDontShowPicker;
  
  private Button mOkButton;
  
  private ArrayList<Account> mPossiblyVisibleAccounts;
  
  private int mSelectedItemIndex;
  
  private Set<Account> mSetOfAllowableAccounts;
  
  private Set<String> mSetOfRelevantAccountTypes;
  
  public void onCreate(Bundle paramBundle) {
    Parcelable[] arrayOfParcelable;
    if (Log.isLoggable("AccountChooser", 2)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ChooseTypeAndAccountActivity.onCreate(savedInstanceState=");
      stringBuilder.append(paramBundle);
      stringBuilder.append(")");
      Log.v("AccountChooser", stringBuilder.toString());
    } 
    getWindow().addSystemFlags(524288);
    boolean bool = false;
    try {
      IBinder iBinder = getActivityToken();
      this.mCallingUid = ActivityTaskManager.getService().getLaunchedFromUid(iBinder);
      String str = ActivityTaskManager.getService().getLaunchedFromPackage(iBinder);
      if (this.mCallingUid != 0 && str != null) {
        UserManager userManager = UserManager.get((Context)this);
        UserHandle userHandle = new UserHandle();
        int i = this.mCallingUid;
        this(UserHandle.getUserId(i));
        Bundle bundle = userManager.getUserRestrictions(userHandle);
        this.mDisallowAddAccounts = bundle.getBoolean("no_modify_accounts", false);
      } 
    } catch (RemoteException remoteException) {
      String str = getClass().getSimpleName();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to get caller identity \n");
      stringBuilder.append(remoteException);
      Log.w(str, stringBuilder.toString());
    } 
    Intent intent = getIntent();
    this.mSetOfAllowableAccounts = getAllowableAccountSet(intent);
    this.mSetOfRelevantAccountTypes = getReleventAccountTypes(intent);
    this.mDescriptionOverride = intent.getStringExtra("descriptionTextOverride");
    if (paramBundle != null) {
      this.mPendingRequest = paramBundle.getInt("pendingRequest");
      this.mExistingAccounts = paramBundle.getParcelableArray("existingAccounts");
      this.mSelectedAccountName = paramBundle.getString("selectedAccountName");
      this.mSelectedAddNewAccount = paramBundle.getBoolean("selectedAddAccount", false);
      arrayOfParcelable = paramBundle.getParcelableArray("accountsList");
      ArrayList<Integer> arrayList = paramBundle.getIntegerArrayList("visibilityList");
      this.mAccounts = new LinkedHashMap<>();
      for (byte b = 0; b < arrayOfParcelable.length; b++)
        this.mAccounts.put((Account)arrayOfParcelable[b], arrayList.get(b)); 
    } else {
      this.mPendingRequest = 0;
      this.mExistingAccounts = null;
      Account account = arrayOfParcelable.<Account>getParcelableExtra("selectedAccount");
      if (account != null)
        this.mSelectedAccountName = account.name; 
      this.mAccounts = getAcceptableAccountChoices(AccountManager.get((Context)this));
    } 
    if (Log.isLoggable("AccountChooser", 2)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("selected account name is ");
      stringBuilder.append(this.mSelectedAccountName);
      Log.v("AccountChooser", stringBuilder.toString());
    } 
    this.mPossiblyVisibleAccounts = new ArrayList<>(this.mAccounts.size());
    for (Map.Entry<Account, Integer> entry : this.mAccounts.entrySet()) {
      if (3 != ((Integer)entry.getValue()).intValue())
        this.mPossiblyVisibleAccounts.add((Account)entry.getKey()); 
    } 
    if (this.mPossiblyVisibleAccounts.isEmpty() && this.mDisallowAddAccounts) {
      requestWindowFeature(1);
      setContentView(17367096);
      this.mDontShowPicker = true;
    } 
    if (this.mDontShowPicker) {
      super.onCreate(paramBundle);
      return;
    } 
    if (this.mPendingRequest == 0)
      if (this.mPossiblyVisibleAccounts.isEmpty()) {
        setNonLabelThemeAndCallSuperCreate(paramBundle);
        if (this.mSetOfRelevantAccountTypes.size() == 1) {
          runAddAccountForAuthenticator(this.mSetOfRelevantAccountTypes.iterator().next());
        } else {
          startChooseAccountTypeActivity();
        } 
      }  
    String[] arrayOfString = getListOfDisplayableOptions(this.mPossiblyVisibleAccounts);
    this.mSelectedItemIndex = getItemIndexToSelect(this.mPossiblyVisibleAccounts, this.mSelectedAccountName, this.mSelectedAddNewAccount);
    super.onCreate(paramBundle);
    setContentView(17367122);
    overrideDescriptionIfSupplied(this.mDescriptionOverride);
    populateUIAccountList(arrayOfString);
    Button button = findViewById(16908314);
    if (this.mSelectedItemIndex != -1)
      bool = true; 
    button.setEnabled(bool);
  }
  
  protected void onDestroy() {
    if (Log.isLoggable("AccountChooser", 2))
      Log.v("AccountChooser", "ChooseTypeAndAccountActivity.onDestroy()"); 
    super.onDestroy();
  }
  
  protected void onSaveInstanceState(Bundle paramBundle) {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putInt("pendingRequest", this.mPendingRequest);
    if (this.mPendingRequest == 2)
      paramBundle.putParcelableArray("existingAccounts", this.mExistingAccounts); 
    int i = this.mSelectedItemIndex;
    if (i != -1)
      if (i == this.mPossiblyVisibleAccounts.size()) {
        paramBundle.putBoolean("selectedAddAccount", true);
      } else {
        paramBundle.putBoolean("selectedAddAccount", false);
        ArrayList<Account> arrayList1 = this.mPossiblyVisibleAccounts;
        i = this.mSelectedItemIndex;
        String str = ((Account)arrayList1.get(i)).name;
        paramBundle.putString("selectedAccountName", str);
      }  
    Parcelable[] arrayOfParcelable = new Parcelable[this.mAccounts.size()];
    ArrayList<Integer> arrayList = new ArrayList(this.mAccounts.size());
    i = 0;
    for (Map.Entry<Account, Integer> entry : this.mAccounts.entrySet()) {
      arrayOfParcelable[i] = (Parcelable)entry.getKey();
      arrayList.add((Integer)entry.getValue());
      i++;
    } 
    paramBundle.putParcelableArray("accountsList", arrayOfParcelable);
    paramBundle.putIntegerArrayList("visibilityList", arrayList);
  }
  
  public void onCancelButtonClicked(View paramView) {
    onBackPressed();
  }
  
  public void onOkButtonClicked(View paramView) {
    if (this.mSelectedItemIndex == this.mPossiblyVisibleAccounts.size()) {
      startChooseAccountTypeActivity();
    } else {
      int i = this.mSelectedItemIndex;
      if (i != -1)
        onAccountSelected(this.mPossiblyVisibleAccounts.get(i)); 
    } 
  }
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent) {
    // Byte code:
    //   0: ldc 'AccountChooser'
    //   2: iconst_2
    //   3: invokestatic isLoggable : (Ljava/lang/String;I)Z
    //   6: ifeq -> 122
    //   9: aload_3
    //   10: ifnull -> 28
    //   13: aload_3
    //   14: invokevirtual getExtras : ()Landroid/os/Bundle;
    //   17: ifnull -> 28
    //   20: aload_3
    //   21: invokevirtual getExtras : ()Landroid/os/Bundle;
    //   24: invokevirtual keySet : ()Ljava/util/Set;
    //   27: pop
    //   28: aload_3
    //   29: ifnull -> 41
    //   32: aload_3
    //   33: invokevirtual getExtras : ()Landroid/os/Bundle;
    //   36: astore #4
    //   38: goto -> 44
    //   41: aconst_null
    //   42: astore #4
    //   44: new java/lang/StringBuilder
    //   47: dup
    //   48: invokespecial <init> : ()V
    //   51: astore #5
    //   53: aload #5
    //   55: ldc_w 'ChooseTypeAndAccountActivity.onActivityResult(reqCode='
    //   58: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   61: pop
    //   62: aload #5
    //   64: iload_1
    //   65: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   68: pop
    //   69: aload #5
    //   71: ldc_w ', resCode='
    //   74: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   77: pop
    //   78: aload #5
    //   80: iload_2
    //   81: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   84: pop
    //   85: aload #5
    //   87: ldc_w ', extras='
    //   90: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   93: pop
    //   94: aload #5
    //   96: aload #4
    //   98: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   101: pop
    //   102: aload #5
    //   104: ldc_w ')'
    //   107: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   110: pop
    //   111: ldc 'AccountChooser'
    //   113: aload #5
    //   115: invokevirtual toString : ()Ljava/lang/String;
    //   118: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   121: pop
    //   122: aload_0
    //   123: iconst_0
    //   124: putfield mPendingRequest : I
    //   127: iload_2
    //   128: ifne -> 151
    //   131: aload_0
    //   132: getfield mPossiblyVisibleAccounts : Ljava/util/ArrayList;
    //   135: invokevirtual isEmpty : ()Z
    //   138: ifeq -> 150
    //   141: aload_0
    //   142: iconst_0
    //   143: invokevirtual setResult : (I)V
    //   146: aload_0
    //   147: invokevirtual finish : ()V
    //   150: return
    //   151: iload_2
    //   152: iconst_m1
    //   153: if_icmpne -> 395
    //   156: iload_1
    //   157: iconst_1
    //   158: if_icmpne -> 195
    //   161: aload_3
    //   162: ifnull -> 183
    //   165: aload_3
    //   166: ldc_w 'accountType'
    //   169: invokevirtual getStringExtra : (Ljava/lang/String;)Ljava/lang/String;
    //   172: astore_3
    //   173: aload_3
    //   174: ifnull -> 183
    //   177: aload_0
    //   178: aload_3
    //   179: invokevirtual runAddAccountForAuthenticator : (Ljava/lang/String;)V
    //   182: return
    //   183: ldc 'AccountChooser'
    //   185: ldc_w 'ChooseTypeAndAccountActivity.onActivityResult: unable to find account type, pretending the request was canceled'
    //   188: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   191: pop
    //   192: goto -> 386
    //   195: iload_1
    //   196: iconst_2
    //   197: if_icmpne -> 386
    //   200: aconst_null
    //   201: astore #4
    //   203: aconst_null
    //   204: astore #5
    //   206: aload_3
    //   207: ifnull -> 228
    //   210: aload_3
    //   211: ldc_w 'authAccount'
    //   214: invokevirtual getStringExtra : (Ljava/lang/String;)Ljava/lang/String;
    //   217: astore #4
    //   219: aload_3
    //   220: ldc_w 'accountType'
    //   223: invokevirtual getStringExtra : (Ljava/lang/String;)Ljava/lang/String;
    //   226: astore #5
    //   228: aload #4
    //   230: ifnull -> 245
    //   233: aload #4
    //   235: astore_3
    //   236: aload #5
    //   238: astore #6
    //   240: aload #5
    //   242: ifnonnull -> 369
    //   245: aload_0
    //   246: invokestatic get : (Landroid/content/Context;)Landroid/accounts/AccountManager;
    //   249: aload_0
    //   250: getfield mCallingPackage : Ljava/lang/String;
    //   253: aload_0
    //   254: getfield mCallingUid : I
    //   257: invokevirtual getAccountsForPackage : (Ljava/lang/String;I)[Landroid/accounts/Account;
    //   260: astore #7
    //   262: new java/util/HashSet
    //   265: dup
    //   266: invokespecial <init> : ()V
    //   269: astore #8
    //   271: aload_0
    //   272: getfield mExistingAccounts : [Landroid/os/Parcelable;
    //   275: astore #6
    //   277: aload #6
    //   279: arraylength
    //   280: istore_2
    //   281: iconst_0
    //   282: istore_1
    //   283: iload_1
    //   284: iload_2
    //   285: if_icmpge -> 311
    //   288: aload #6
    //   290: iload_1
    //   291: aaload
    //   292: astore_3
    //   293: aload #8
    //   295: aload_3
    //   296: checkcast android/accounts/Account
    //   299: invokeinterface add : (Ljava/lang/Object;)Z
    //   304: pop
    //   305: iinc #1, 1
    //   308: goto -> 283
    //   311: aload #7
    //   313: arraylength
    //   314: istore_2
    //   315: iconst_0
    //   316: istore_1
    //   317: aload #4
    //   319: astore_3
    //   320: aload #5
    //   322: astore #6
    //   324: iload_1
    //   325: iload_2
    //   326: if_icmpge -> 369
    //   329: aload #7
    //   331: iload_1
    //   332: aaload
    //   333: astore #6
    //   335: aload #8
    //   337: aload #6
    //   339: invokeinterface contains : (Ljava/lang/Object;)Z
    //   344: ifne -> 363
    //   347: aload #6
    //   349: getfield name : Ljava/lang/String;
    //   352: astore_3
    //   353: aload #6
    //   355: getfield type : Ljava/lang/String;
    //   358: astore #6
    //   360: goto -> 369
    //   363: iinc #1, 1
    //   366: goto -> 317
    //   369: aload_3
    //   370: ifnonnull -> 378
    //   373: aload #6
    //   375: ifnull -> 386
    //   378: aload_0
    //   379: aload_3
    //   380: aload #6
    //   382: invokespecial setResultAndFinish : (Ljava/lang/String;Ljava/lang/String;)V
    //   385: return
    //   386: ldc 'AccountChooser'
    //   388: ldc_w 'ChooseTypeAndAccountActivity.onActivityResult: unable to find added account, pretending the request was canceled'
    //   391: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   394: pop
    //   395: ldc 'AccountChooser'
    //   397: iconst_2
    //   398: invokestatic isLoggable : (Ljava/lang/String;I)Z
    //   401: ifeq -> 413
    //   404: ldc 'AccountChooser'
    //   406: ldc_w 'ChooseTypeAndAccountActivity.onActivityResult: canceled'
    //   409: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   412: pop
    //   413: aload_0
    //   414: iconst_0
    //   415: invokevirtual setResult : (I)V
    //   418: aload_0
    //   419: invokevirtual finish : ()V
    //   422: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #312	-> 0
    //   #313	-> 9
    //   #314	-> 28
    //   #315	-> 44
    //   #320	-> 122
    //   #322	-> 127
    //   #325	-> 131
    //   #326	-> 141
    //   #327	-> 146
    //   #329	-> 150
    //   #332	-> 151
    //   #333	-> 156
    //   #334	-> 161
    //   #335	-> 165
    //   #336	-> 173
    //   #337	-> 177
    //   #338	-> 182
    //   #341	-> 183
    //   #343	-> 195
    //   #344	-> 200
    //   #345	-> 203
    //   #347	-> 206
    //   #348	-> 210
    //   #349	-> 219
    //   #352	-> 228
    //   #354	-> 245
    //   #356	-> 262
    //   #357	-> 271
    //   #358	-> 293
    //   #357	-> 305
    //   #360	-> 311
    //   #362	-> 335
    //   #363	-> 347
    //   #364	-> 353
    //   #365	-> 360
    //   #360	-> 363
    //   #370	-> 369
    //   #371	-> 378
    //   #372	-> 385
    //   #375	-> 386
    //   #378	-> 395
    //   #379	-> 404
    //   #381	-> 413
    //   #382	-> 418
    //   #383	-> 422
  }
  
  protected void runAddAccountForAuthenticator(String paramString) {
    if (Log.isLoggable("AccountChooser", 2)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("runAddAccountForAuthenticator: ");
      stringBuilder.append(paramString);
      Log.v("AccountChooser", stringBuilder.toString());
    } 
    Bundle bundle = getIntent().getBundleExtra("addAccountOptions");
    String[] arrayOfString = getIntent().getStringArrayExtra("addAccountRequiredFeatures");
    String str = getIntent().getStringExtra("authTokenType");
    AccountManager.get((Context)this).addAccount(paramString, str, arrayOfString, bundle, null, this, null);
  }
  
  public void run(AccountManagerFuture<Bundle> paramAccountManagerFuture) {
    try {
      Bundle bundle2 = paramAccountManagerFuture.getResult();
      Intent intent = (Intent)bundle2.getParcelable("intent");
      if (intent != null) {
        this.mPendingRequest = 2;
        this.mExistingAccounts = (Parcelable[])AccountManager.get((Context)this).getAccountsForPackage(this.mCallingPackage, this.mCallingUid);
        intent.setFlags(intent.getFlags() & 0xEFFFFFFF);
        startActivityForResult(intent, 2);
        return;
      } 
      Bundle bundle1 = new Bundle();
      bundle1.putString("errorMessage", "error communicating with server");
      setResult(-1, (new Intent()).putExtras(bundle1));
      finish();
      return;
    } catch (OperationCanceledException operationCanceledException) {
      setResult(0);
      finish();
      return;
    } catch (IOException iOException) {
      Bundle bundle = new Bundle();
      bundle.putString("errorMessage", "error communicating with server");
      setResult(-1, (new Intent()).putExtras(bundle));
      finish();
      return;
    } catch (AuthenticatorException authenticatorException) {
      Bundle bundle = new Bundle();
      bundle.putString("errorMessage", "error communicating with server");
      setResult(-1, (new Intent()).putExtras(bundle));
      finish();
      return;
    } 
  }
  
  private void setNonLabelThemeAndCallSuperCreate(Bundle paramBundle) {
    setTheme(16974132);
    super.onCreate(paramBundle);
  }
  
  private void onAccountSelected(Account paramAccount) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("selected account ");
    stringBuilder.append(paramAccount);
    Log.d("AccountChooser", stringBuilder.toString());
    setResultAndFinish(paramAccount.name, paramAccount.type);
  }
  
  private void setResultAndFinish(String paramString1, String paramString2) {
    Account account = new Account(paramString1, paramString2);
    Integer integer = Integer.valueOf(AccountManager.get((Context)this).getAccountVisibility(account, this.mCallingPackage));
    if (integer != null && 
      integer.intValue() == 4)
      AccountManager.get((Context)this).setAccountVisibility(account, this.mCallingPackage, 2); 
    if (integer != null && integer.intValue() == 3) {
      setResult(0);
      finish();
      return;
    } 
    Bundle bundle = new Bundle();
    bundle.putString("authAccount", paramString1);
    bundle.putString("accountType", paramString2);
    setResult(-1, (new Intent()).putExtras(bundle));
    if (Log.isLoggable("AccountChooser", 2)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ChooseTypeAndAccountActivity.setResultAndFinish: selected account ");
      stringBuilder.append(paramString1);
      stringBuilder.append(", ");
      stringBuilder.append(paramString2);
      Log.v("AccountChooser", stringBuilder.toString());
    } 
    finish();
  }
  
  private void startChooseAccountTypeActivity() {
    if (Log.isLoggable("AccountChooser", 2))
      Log.v("AccountChooser", "ChooseAccountTypeActivity.startChooseAccountTypeActivity()"); 
    Intent intent = new Intent((Context)this, ChooseAccountTypeActivity.class);
    intent.setFlags(524288);
    String[] arrayOfString2 = getIntent().getStringArrayExtra("allowableAccountTypes");
    intent.putExtra("allowableAccountTypes", arrayOfString2);
    Bundle bundle = getIntent().getBundleExtra("addAccountOptions");
    intent.putExtra("addAccountOptions", bundle);
    String[] arrayOfString1 = getIntent().getStringArrayExtra("addAccountRequiredFeatures");
    intent.putExtra("addAccountRequiredFeatures", arrayOfString1);
    String str = getIntent().getStringExtra("authTokenType");
    intent.putExtra("authTokenType", str);
    startActivityForResult(intent, 1);
    this.mPendingRequest = 1;
  }
  
  private int getItemIndexToSelect(ArrayList<Account> paramArrayList, String paramString, boolean paramBoolean) {
    if (paramBoolean)
      return paramArrayList.size(); 
    for (byte b = 0; b < paramArrayList.size(); b++) {
      if (((Account)paramArrayList.get(b)).name.equals(paramString))
        return b; 
    } 
    return -1;
  }
  
  private String[] getListOfDisplayableOptions(ArrayList<Account> paramArrayList) {
    String[] arrayOfString = new String[paramArrayList.size() + (this.mDisallowAddAccounts ^ true)];
    for (byte b = 0; b < paramArrayList.size(); b++)
      arrayOfString[b] = ((Account)paramArrayList.get(b)).name; 
    if (!this.mDisallowAddAccounts)
      arrayOfString[paramArrayList.size()] = getResources().getString(17039610); 
    return arrayOfString;
  }
  
  private LinkedHashMap<Account, Integer> getAcceptableAccountChoices(AccountManager paramAccountManager) {
    String str = this.mCallingPackage;
    Map<Account, Integer> map = paramAccountManager.getAccountsAndVisibilityForPackage(str, null);
    Account[] arrayOfAccount = paramAccountManager.getAccounts();
    LinkedHashMap<Object, Object> linkedHashMap = new LinkedHashMap<>(map.size());
    int i;
    byte b;
    for (i = arrayOfAccount.length, b = 0; b < i; ) {
      Account account = arrayOfAccount[b];
      Set<Account> set1 = this.mSetOfAllowableAccounts;
      if (set1 != null && 
        !set1.contains(account))
        continue; 
      Set<String> set = this.mSetOfRelevantAccountTypes;
      if (set != null) {
        String str1 = account.type;
        if (!set.contains(str1))
          continue; 
      } 
      if (map.get(account) != null)
        linkedHashMap.put(account, map.get(account)); 
      continue;
      b++;
    } 
    return (LinkedHashMap)linkedHashMap;
  }
  
  private Set<String> getReleventAccountTypes(Intent paramIntent) {
    String[] arrayOfString = paramIntent.getStringArrayExtra("allowableAccountTypes");
    AuthenticatorDescription[] arrayOfAuthenticatorDescription = AccountManager.get((Context)this).getAuthenticatorTypes();
    HashSet<String> hashSet = new HashSet(arrayOfAuthenticatorDescription.length);
    int i;
    byte b;
    for (i = arrayOfAuthenticatorDescription.length, b = 0; b < i; ) {
      AuthenticatorDescription authenticatorDescription = arrayOfAuthenticatorDescription[b];
      hashSet.add(authenticatorDescription.type);
      b++;
    } 
    if (arrayOfString != null) {
      HashSet<String> hashSet1 = Sets.newHashSet((Object[])arrayOfString);
      hashSet1.retainAll(hashSet);
      hashSet = hashSet1;
    } 
    return hashSet;
  }
  
  private Set<Account> getAllowableAccountSet(Intent paramIntent) {
    Parcelable parcelable;
    Intent intent = null;
    ArrayList<Parcelable> arrayList = paramIntent.getParcelableArrayListExtra("allowableAccounts");
    paramIntent = intent;
    if (arrayList != null) {
      HashSet<Account> hashSet = new HashSet(arrayList.size());
      Iterator<Parcelable> iterator = arrayList.iterator();
      while (true) {
        HashSet<Account> hashSet1 = hashSet;
        if (iterator.hasNext()) {
          parcelable = iterator.next();
          hashSet.add((Account)parcelable);
          continue;
        } 
        break;
      } 
    } 
    return (Set<Account>)parcelable;
  }
  
  private void overrideDescriptionIfSupplied(String paramString) {
    TextView textView = findViewById(16908926);
    if (!TextUtils.isEmpty(paramString)) {
      textView.setText(paramString);
    } else {
      textView.setVisibility(8);
    } 
  }
  
  private final void populateUIAccountList(String[] paramArrayOfString) {
    ListView listView = findViewById(16908298);
    listView.setAdapter((ListAdapter)new ArrayAdapter((Context)this, 17367055, (Object[])paramArrayOfString));
    listView.setChoiceMode(1);
    listView.setItemsCanFocus(false);
    listView.setOnItemClickListener((AdapterView.OnItemClickListener)new Object(this));
    int i = this.mSelectedItemIndex;
    if (i != -1) {
      listView.setItemChecked(i, true);
      if (Log.isLoggable("AccountChooser", 2)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("List item ");
        stringBuilder.append(this.mSelectedItemIndex);
        stringBuilder.append(" should be selected");
        Log.v("AccountChooser", stringBuilder.toString());
      } 
    } 
  }
}
