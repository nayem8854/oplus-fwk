package android.accounts;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAccountManagerResponse extends IInterface {
  void onError(int paramInt, String paramString) throws RemoteException;
  
  void onResult(Bundle paramBundle) throws RemoteException;
  
  class Default implements IAccountManagerResponse {
    public void onResult(Bundle param1Bundle) throws RemoteException {}
    
    public void onError(int param1Int, String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAccountManagerResponse {
    private static final String DESCRIPTOR = "android.accounts.IAccountManagerResponse";
    
    static final int TRANSACTION_onError = 2;
    
    static final int TRANSACTION_onResult = 1;
    
    public Stub() {
      attachInterface(this, "android.accounts.IAccountManagerResponse");
    }
    
    public static IAccountManagerResponse asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.accounts.IAccountManagerResponse");
      if (iInterface != null && iInterface instanceof IAccountManagerResponse)
        return (IAccountManagerResponse)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onError";
      } 
      return "onResult";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.accounts.IAccountManagerResponse");
          return true;
        } 
        param1Parcel1.enforceInterface("android.accounts.IAccountManagerResponse");
        param1Int1 = param1Parcel1.readInt();
        str = param1Parcel1.readString();
        onError(param1Int1, str);
        return true;
      } 
      str.enforceInterface("android.accounts.IAccountManagerResponse");
      if (str.readInt() != 0) {
        Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str);
      } else {
        str = null;
      } 
      onResult((Bundle)str);
      return true;
    }
    
    private static class Proxy implements IAccountManagerResponse {
      public static IAccountManagerResponse sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.accounts.IAccountManagerResponse";
      }
      
      public void onResult(Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.accounts.IAccountManagerResponse");
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IAccountManagerResponse.Stub.getDefaultImpl() != null) {
            IAccountManagerResponse.Stub.getDefaultImpl().onResult(param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onError(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.accounts.IAccountManagerResponse");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IAccountManagerResponse.Stub.getDefaultImpl() != null) {
            IAccountManagerResponse.Stub.getDefaultImpl().onError(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAccountManagerResponse param1IAccountManagerResponse) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAccountManagerResponse != null) {
          Proxy.sDefaultImpl = param1IAccountManagerResponse;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAccountManagerResponse getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
