package android.accounts;

import android.app.Activity;
import android.os.Bundle;

@Deprecated
public class AccountAuthenticatorActivity extends Activity {
  private AccountAuthenticatorResponse mAccountAuthenticatorResponse = null;
  
  private Bundle mResultBundle = null;
  
  public final void setAccountAuthenticatorResult(Bundle paramBundle) {
    this.mResultBundle = paramBundle;
  }
  
  protected void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    AccountAuthenticatorResponse accountAuthenticatorResponse = getIntent().<AccountAuthenticatorResponse>getParcelableExtra("accountAuthenticatorResponse");
    if (accountAuthenticatorResponse != null)
      accountAuthenticatorResponse.onRequestContinued(); 
  }
  
  public void finish() {
    AccountAuthenticatorResponse accountAuthenticatorResponse = this.mAccountAuthenticatorResponse;
    if (accountAuthenticatorResponse != null) {
      Bundle bundle = this.mResultBundle;
      if (bundle != null) {
        accountAuthenticatorResponse.onResult(bundle);
      } else {
        accountAuthenticatorResponse.onError(4, "canceled");
      } 
      this.mAccountAuthenticatorResponse = null;
    } 
    super.finish();
  }
}
