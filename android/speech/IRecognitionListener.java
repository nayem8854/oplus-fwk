package android.speech;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IRecognitionListener extends IInterface {
  void onBeginningOfSpeech() throws RemoteException;
  
  void onBufferReceived(byte[] paramArrayOfbyte) throws RemoteException;
  
  void onEndOfSpeech() throws RemoteException;
  
  void onError(int paramInt) throws RemoteException;
  
  void onEvent(int paramInt, Bundle paramBundle) throws RemoteException;
  
  void onPartialResults(Bundle paramBundle) throws RemoteException;
  
  void onReadyForSpeech(Bundle paramBundle) throws RemoteException;
  
  void onResults(Bundle paramBundle) throws RemoteException;
  
  void onRmsChanged(float paramFloat) throws RemoteException;
  
  class Default implements IRecognitionListener {
    public void onReadyForSpeech(Bundle param1Bundle) throws RemoteException {}
    
    public void onBeginningOfSpeech() throws RemoteException {}
    
    public void onRmsChanged(float param1Float) throws RemoteException {}
    
    public void onBufferReceived(byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void onEndOfSpeech() throws RemoteException {}
    
    public void onError(int param1Int) throws RemoteException {}
    
    public void onResults(Bundle param1Bundle) throws RemoteException {}
    
    public void onPartialResults(Bundle param1Bundle) throws RemoteException {}
    
    public void onEvent(int param1Int, Bundle param1Bundle) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRecognitionListener {
    private static final String DESCRIPTOR = "android.speech.IRecognitionListener";
    
    static final int TRANSACTION_onBeginningOfSpeech = 2;
    
    static final int TRANSACTION_onBufferReceived = 4;
    
    static final int TRANSACTION_onEndOfSpeech = 5;
    
    static final int TRANSACTION_onError = 6;
    
    static final int TRANSACTION_onEvent = 9;
    
    static final int TRANSACTION_onPartialResults = 8;
    
    static final int TRANSACTION_onReadyForSpeech = 1;
    
    static final int TRANSACTION_onResults = 7;
    
    static final int TRANSACTION_onRmsChanged = 3;
    
    public Stub() {
      attachInterface(this, "android.speech.IRecognitionListener");
    }
    
    public static IRecognitionListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.speech.IRecognitionListener");
      if (iInterface != null && iInterface instanceof IRecognitionListener)
        return (IRecognitionListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 9:
          return "onEvent";
        case 8:
          return "onPartialResults";
        case 7:
          return "onResults";
        case 6:
          return "onError";
        case 5:
          return "onEndOfSpeech";
        case 4:
          return "onBufferReceived";
        case 3:
          return "onRmsChanged";
        case 2:
          return "onBeginningOfSpeech";
        case 1:
          break;
      } 
      return "onReadyForSpeech";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        byte[] arrayOfByte;
        float f;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 9:
            param1Parcel1.enforceInterface("android.speech.IRecognitionListener");
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onEvent(param1Int1, (Bundle)param1Parcel1);
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.speech.IRecognitionListener");
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onPartialResults((Bundle)param1Parcel1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.speech.IRecognitionListener");
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onResults((Bundle)param1Parcel1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.speech.IRecognitionListener");
            param1Int1 = param1Parcel1.readInt();
            onError(param1Int1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.speech.IRecognitionListener");
            onEndOfSpeech();
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.speech.IRecognitionListener");
            arrayOfByte = param1Parcel1.createByteArray();
            onBufferReceived(arrayOfByte);
            return true;
          case 3:
            arrayOfByte.enforceInterface("android.speech.IRecognitionListener");
            f = arrayOfByte.readFloat();
            onRmsChanged(f);
            return true;
          case 2:
            arrayOfByte.enforceInterface("android.speech.IRecognitionListener");
            onBeginningOfSpeech();
            return true;
          case 1:
            break;
        } 
        arrayOfByte.enforceInterface("android.speech.IRecognitionListener");
        if (arrayOfByte.readInt() != 0) {
          Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)arrayOfByte);
        } else {
          arrayOfByte = null;
        } 
        onReadyForSpeech((Bundle)arrayOfByte);
        return true;
      } 
      param1Parcel2.writeString("android.speech.IRecognitionListener");
      return true;
    }
    
    private static class Proxy implements IRecognitionListener {
      public static IRecognitionListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.speech.IRecognitionListener";
      }
      
      public void onReadyForSpeech(Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.speech.IRecognitionListener");
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IRecognitionListener.Stub.getDefaultImpl() != null) {
            IRecognitionListener.Stub.getDefaultImpl().onReadyForSpeech(param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onBeginningOfSpeech() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.speech.IRecognitionListener");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IRecognitionListener.Stub.getDefaultImpl() != null) {
            IRecognitionListener.Stub.getDefaultImpl().onBeginningOfSpeech();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRmsChanged(float param2Float) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.speech.IRecognitionListener");
          parcel.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IRecognitionListener.Stub.getDefaultImpl() != null) {
            IRecognitionListener.Stub.getDefaultImpl().onRmsChanged(param2Float);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onBufferReceived(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.speech.IRecognitionListener");
          parcel.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IRecognitionListener.Stub.getDefaultImpl() != null) {
            IRecognitionListener.Stub.getDefaultImpl().onBufferReceived(param2ArrayOfbyte);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onEndOfSpeech() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.speech.IRecognitionListener");
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IRecognitionListener.Stub.getDefaultImpl() != null) {
            IRecognitionListener.Stub.getDefaultImpl().onEndOfSpeech();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onError(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.speech.IRecognitionListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IRecognitionListener.Stub.getDefaultImpl() != null) {
            IRecognitionListener.Stub.getDefaultImpl().onError(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onResults(Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.speech.IRecognitionListener");
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IRecognitionListener.Stub.getDefaultImpl() != null) {
            IRecognitionListener.Stub.getDefaultImpl().onResults(param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPartialResults(Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.speech.IRecognitionListener");
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IRecognitionListener.Stub.getDefaultImpl() != null) {
            IRecognitionListener.Stub.getDefaultImpl().onPartialResults(param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onEvent(int param2Int, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.speech.IRecognitionListener");
          parcel.writeInt(param2Int);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IRecognitionListener.Stub.getDefaultImpl() != null) {
            IRecognitionListener.Stub.getDefaultImpl().onEvent(param2Int, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRecognitionListener param1IRecognitionListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRecognitionListener != null) {
          Proxy.sDefaultImpl = param1IRecognitionListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRecognitionListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
