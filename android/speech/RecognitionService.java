package android.speech;

import android.app.Service;
import android.content.Intent;
import android.content.PermissionChecker;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.lang.ref.WeakReference;
import java.util.Objects;

public abstract class RecognitionService extends Service {
  private static final boolean DBG = false;
  
  private static final int MSG_CANCEL = 3;
  
  private static final int MSG_RESET = 4;
  
  private static final int MSG_START_LISTENING = 1;
  
  private static final int MSG_STOP_LISTENING = 2;
  
  public static final String SERVICE_INTERFACE = "android.speech.RecognitionService";
  
  public static final String SERVICE_META_DATA = "android.speech";
  
  private static final String TAG = "RecognitionService";
  
  private RecognitionServiceBinder mBinder = new RecognitionServiceBinder(this);
  
  private Callback mCurrentCallback = null;
  
  private final Handler mHandler = (Handler)new Object(this);
  
  private void dispatchStartListening(Intent paramIntent, IRecognitionListener paramIRecognitionListener, int paramInt) {
    Callback callback;
    if (this.mCurrentCallback == null) {
      try {
        IBinder iBinder = paramIRecognitionListener.asBinder();
        Object object = new Object();
        super(this, paramIRecognitionListener);
        iBinder.linkToDeath((IBinder.DeathRecipient)object, 0);
        this.mCurrentCallback = callback = new Callback(paramIRecognitionListener, paramInt);
        onStartListening(paramIntent, callback);
      } catch (RemoteException remoteException) {
        Log.e("RecognitionService", "dead listener on startListening");
        return;
      } 
    } else {
      try {
        callback.onError(8);
      } catch (RemoteException remoteException) {
        Log.d("RecognitionService", "onError call from startListening failed");
      } 
      Log.i("RecognitionService", "concurrent startListening received - ignoring this call");
    } 
  }
  
  private void dispatchStopListening(IRecognitionListener paramIRecognitionListener) {
    try {
      if (this.mCurrentCallback == null) {
        paramIRecognitionListener.onError(5);
        Log.w("RecognitionService", "stopListening called with no preceding startListening - ignoring");
      } else if (this.mCurrentCallback.mListener.asBinder() != paramIRecognitionListener.asBinder()) {
        paramIRecognitionListener.onError(8);
        Log.w("RecognitionService", "stopListening called by other caller than startListening - ignoring");
      } else {
        onStopListening(this.mCurrentCallback);
      } 
    } catch (RemoteException remoteException) {
      Log.d("RecognitionService", "onError call from stopListening failed");
    } 
  }
  
  private void dispatchCancel(IRecognitionListener paramIRecognitionListener) {
    Callback callback = this.mCurrentCallback;
    if (callback != null)
      if (callback.mListener.asBinder() != paramIRecognitionListener.asBinder()) {
        Log.w("RecognitionService", "cancel called by client who did not call startListening - ignoring");
      } else {
        onCancel(this.mCurrentCallback);
        this.mCurrentCallback = null;
      }  
  }
  
  private void dispatchClearCallback() {
    this.mCurrentCallback = null;
  }
  
  class StartListeningArgs {
    public final int mCallingUid;
    
    public final Intent mIntent;
    
    public final IRecognitionListener mListener;
    
    final RecognitionService this$0;
    
    public StartListeningArgs(Intent param1Intent, IRecognitionListener param1IRecognitionListener, int param1Int) {
      this.mIntent = param1Intent;
      this.mListener = param1IRecognitionListener;
      this.mCallingUid = param1Int;
    }
  }
  
  private boolean checkPermissions(IRecognitionListener paramIRecognitionListener, boolean paramBoolean, String paramString1, String paramString2) {
    if (paramBoolean) {
      if (PermissionChecker.checkCallingPermissionForDataDelivery(this, "android.permission.RECORD_AUDIO", paramString1, paramString2, null) == 0)
        return true; 
    } else if (PermissionChecker.checkCallingOrSelfPermissionForPreflight(this, "android.permission.RECORD_AUDIO") == 0) {
      return true;
    } 
    try {
      Log.e("RecognitionService", "call for recognition service without RECORD_AUDIO permissions");
      paramIRecognitionListener.onError(9);
    } catch (RemoteException remoteException) {
      Log.e("RecognitionService", "sending ERROR_INSUFFICIENT_PERMISSIONS message failed", (Throwable)remoteException);
    } 
    return false;
  }
  
  public final IBinder onBind(Intent paramIntent) {
    return (IBinder)this.mBinder;
  }
  
  public void onDestroy() {
    this.mCurrentCallback = null;
    this.mBinder.clearReference();
    super.onDestroy();
  }
  
  protected abstract void onCancel(Callback paramCallback);
  
  protected abstract void onStartListening(Intent paramIntent, Callback paramCallback);
  
  protected abstract void onStopListening(Callback paramCallback);
  
  class Callback {
    private final int mCallingUid;
    
    private final IRecognitionListener mListener;
    
    final RecognitionService this$0;
    
    private Callback(IRecognitionListener param1IRecognitionListener, int param1Int) {
      this.mListener = param1IRecognitionListener;
      this.mCallingUid = param1Int;
    }
    
    public void beginningOfSpeech() throws RemoteException {
      this.mListener.onBeginningOfSpeech();
    }
    
    public void bufferReceived(byte[] param1ArrayOfbyte) throws RemoteException {
      this.mListener.onBufferReceived(param1ArrayOfbyte);
    }
    
    public void endOfSpeech() throws RemoteException {
      this.mListener.onEndOfSpeech();
    }
    
    public void error(int param1Int) throws RemoteException {
      Message.obtain(RecognitionService.this.mHandler, 4).sendToTarget();
      this.mListener.onError(param1Int);
    }
    
    public void partialResults(Bundle param1Bundle) throws RemoteException {
      this.mListener.onPartialResults(param1Bundle);
    }
    
    public void readyForSpeech(Bundle param1Bundle) throws RemoteException {
      this.mListener.onReadyForSpeech(param1Bundle);
    }
    
    public void results(Bundle param1Bundle) throws RemoteException {
      Message.obtain(RecognitionService.this.mHandler, 4).sendToTarget();
      this.mListener.onResults(param1Bundle);
    }
    
    public void rmsChanged(float param1Float) throws RemoteException {
      this.mListener.onRmsChanged(param1Float);
    }
    
    public int getCallingUid() {
      return this.mCallingUid;
    }
  }
  
  private static final class RecognitionServiceBinder extends IRecognitionService.Stub {
    private final WeakReference<RecognitionService> mServiceRef;
    
    public RecognitionServiceBinder(RecognitionService param1RecognitionService) {
      this.mServiceRef = new WeakReference<>(param1RecognitionService);
    }
    
    public void startListening(Intent param1Intent, IRecognitionListener param1IRecognitionListener, String param1String1, String param1String2) {
      Preconditions.checkNotNull(param1String1);
      RecognitionService recognitionService = this.mServiceRef.get();
      if (recognitionService != null && recognitionService.checkPermissions(param1IRecognitionListener, true, param1String1, param1String2)) {
        Handler handler2 = recognitionService.mHandler, handler1 = recognitionService.mHandler;
        Objects.requireNonNull(recognitionService);
        RecognitionService.StartListeningArgs startListeningArgs = new RecognitionService.StartListeningArgs(param1Intent, param1IRecognitionListener, Binder.getCallingUid());
        handler2.sendMessage(Message.obtain(handler1, 1, startListeningArgs));
      } 
    }
    
    public void stopListening(IRecognitionListener param1IRecognitionListener, String param1String1, String param1String2) {
      Preconditions.checkNotNull(param1String1);
      RecognitionService recognitionService = this.mServiceRef.get();
      if (recognitionService != null && recognitionService.checkPermissions(param1IRecognitionListener, false, param1String1, param1String2))
        recognitionService.mHandler.sendMessage(Message.obtain(recognitionService.mHandler, 2, param1IRecognitionListener)); 
    }
    
    public void cancel(IRecognitionListener param1IRecognitionListener, String param1String1, String param1String2) {
      Preconditions.checkNotNull(param1String1);
      RecognitionService recognitionService = this.mServiceRef.get();
      if (recognitionService != null && recognitionService.checkPermissions(param1IRecognitionListener, false, param1String1, param1String2))
        recognitionService.mHandler.sendMessage(Message.obtain(recognitionService.mHandler, 3, param1IRecognitionListener)); 
    }
    
    public void clearReference() {
      this.mServiceRef.clear();
    }
  }
}
