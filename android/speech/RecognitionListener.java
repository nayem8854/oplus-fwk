package android.speech;

import android.os.Bundle;

public interface RecognitionListener {
  void onBeginningOfSpeech();
  
  void onBufferReceived(byte[] paramArrayOfbyte);
  
  void onEndOfSpeech();
  
  void onError(int paramInt);
  
  void onEvent(int paramInt, Bundle paramBundle);
  
  void onPartialResults(Bundle paramBundle);
  
  void onReadyForSpeech(Bundle paramBundle);
  
  void onResults(Bundle paramBundle);
  
  void onRmsChanged(float paramFloat);
}
