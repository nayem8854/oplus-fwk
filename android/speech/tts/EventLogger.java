package android.speech.tts;

import android.text.TextUtils;

class EventLogger extends AbstractEventLogger {
  private final SynthesisRequest mRequest;
  
  EventLogger(SynthesisRequest paramSynthesisRequest, int paramInt1, int paramInt2, String paramString) {
    super(paramInt1, paramInt2, paramString);
    this.mRequest = paramSynthesisRequest;
  }
  
  protected void logFailure(int paramInt) {
    if (paramInt != -2) {
      String str1 = this.mServiceApp;
      int i = this.mCallerUid;
      paramInt = this.mCallerPid;
      int j = getUtteranceLength();
      String str2 = getLocaleString();
      SynthesisRequest synthesisRequest = this.mRequest;
      int k = synthesisRequest.getSpeechRate(), m = this.mRequest.getPitch();
      EventLogTags.writeTtsSpeakFailure(str1, i, paramInt, j, str2, k, m);
    } 
  }
  
  protected void logSuccess(long paramLong1, long paramLong2, long paramLong3) {
    String str1 = this.mServiceApp;
    int i = this.mCallerUid, j = this.mCallerPid;
    int k = getUtteranceLength();
    String str2 = getLocaleString();
    SynthesisRequest synthesisRequest = this.mRequest;
    int m = synthesisRequest.getSpeechRate(), n = this.mRequest.getPitch();
    EventLogTags.writeTtsSpeakSuccess(str1, i, j, k, str2, m, n, paramLong2, paramLong3, paramLong1);
  }
  
  private int getUtteranceLength() {
    int i;
    String str = this.mRequest.getText();
    if (str == null) {
      i = 0;
    } else {
      i = str.length();
    } 
    return i;
  }
  
  private String getLocaleString() {
    StringBuilder stringBuilder = new StringBuilder(this.mRequest.getLanguage());
    if (!TextUtils.isEmpty(this.mRequest.getCountry())) {
      stringBuilder.append('-');
      stringBuilder.append(this.mRequest.getCountry());
      if (!TextUtils.isEmpty(this.mRequest.getVariant())) {
        stringBuilder.append('-');
        stringBuilder.append(this.mRequest.getVariant());
      } 
    } 
    return stringBuilder.toString();
  }
}
