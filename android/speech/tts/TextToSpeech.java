package android.speech.tts;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Set;

public class TextToSpeech {
  private final Map<CharSequence, Uri> mUtterances;
  
  private volatile UtteranceProgressListener mUtteranceProgressListener;
  
  private final boolean mUseFallback;
  
  public class Engine {
    public static final String ACTION_CHECK_TTS_DATA = "android.speech.tts.engine.CHECK_TTS_DATA";
    
    public static final String ACTION_GET_SAMPLE_TEXT = "android.speech.tts.engine.GET_SAMPLE_TEXT";
    
    public static final String ACTION_INSTALL_TTS_DATA = "android.speech.tts.engine.INSTALL_TTS_DATA";
    
    public static final String ACTION_TTS_DATA_INSTALLED = "android.speech.tts.engine.TTS_DATA_INSTALLED";
    
    @Deprecated
    public static final int CHECK_VOICE_DATA_BAD_DATA = -1;
    
    public static final int CHECK_VOICE_DATA_FAIL = 0;
    
    @Deprecated
    public static final int CHECK_VOICE_DATA_MISSING_DATA = -2;
    
    @Deprecated
    public static final int CHECK_VOICE_DATA_MISSING_VOLUME = -3;
    
    public static final int CHECK_VOICE_DATA_PASS = 1;
    
    @Deprecated
    public static final String DEFAULT_ENGINE = "com.svox.pico";
    
    public static final float DEFAULT_PAN = 0.0F;
    
    public static final int DEFAULT_PITCH = 100;
    
    public static final int DEFAULT_RATE = 100;
    
    public static final int DEFAULT_STREAM = 3;
    
    public static final float DEFAULT_VOLUME = 1.0F;
    
    public static final String EXTRA_AVAILABLE_VOICES = "availableVoices";
    
    @Deprecated
    public static final String EXTRA_CHECK_VOICE_DATA_FOR = "checkVoiceDataFor";
    
    public static final String EXTRA_SAMPLE_TEXT = "sampleText";
    
    @Deprecated
    public static final String EXTRA_TTS_DATA_INSTALLED = "dataInstalled";
    
    public static final String EXTRA_UNAVAILABLE_VOICES = "unavailableVoices";
    
    @Deprecated
    public static final String EXTRA_VOICE_DATA_FILES = "dataFiles";
    
    @Deprecated
    public static final String EXTRA_VOICE_DATA_FILES_INFO = "dataFilesInfo";
    
    @Deprecated
    public static final String EXTRA_VOICE_DATA_ROOT_DIRECTORY = "dataRoot";
    
    public static final String INTENT_ACTION_TTS_SERVICE = "android.intent.action.TTS_SERVICE";
    
    @Deprecated
    public static final String KEY_FEATURE_EMBEDDED_SYNTHESIS = "embeddedTts";
    
    public static final String KEY_FEATURE_NETWORK_RETRIES_COUNT = "networkRetriesCount";
    
    @Deprecated
    public static final String KEY_FEATURE_NETWORK_SYNTHESIS = "networkTts";
    
    public static final String KEY_FEATURE_NETWORK_TIMEOUT_MS = "networkTimeoutMs";
    
    public static final String KEY_FEATURE_NOT_INSTALLED = "notInstalled";
    
    public static final String KEY_PARAM_AUDIO_ATTRIBUTES = "audioAttributes";
    
    public static final String KEY_PARAM_COUNTRY = "country";
    
    public static final String KEY_PARAM_ENGINE = "engine";
    
    public static final String KEY_PARAM_LANGUAGE = "language";
    
    public static final String KEY_PARAM_PAN = "pan";
    
    public static final String KEY_PARAM_PITCH = "pitch";
    
    public static final String KEY_PARAM_RATE = "rate";
    
    public static final String KEY_PARAM_SESSION_ID = "sessionId";
    
    public static final String KEY_PARAM_STREAM = "streamType";
    
    public static final String KEY_PARAM_UTTERANCE_ID = "utteranceId";
    
    public static final String KEY_PARAM_VARIANT = "variant";
    
    public static final String KEY_PARAM_VOICE_NAME = "voiceName";
    
    public static final String KEY_PARAM_VOLUME = "volume";
    
    public static final String SERVICE_META_DATA = "android.speech.tts";
    
    public static final int USE_DEFAULTS = 0;
    
    final TextToSpeech this$0;
  }
  
  private final Object mStartLock = new Object();
  
  private Connection mServiceConnection;
  
  private String mRequestedEngine;
  
  private final Bundle mParams = new Bundle();
  
  private OnInitListener mInitListener;
  
  private final TtsEngines mEnginesHelper;
  
  private final Map<String, Uri> mEarcons;
  
  private volatile String mCurrentEngine = null;
  
  private final Context mContext;
  
  private Connection mConnectingServiceConnection;
  
  private static final String TAG = "TextToSpeech";
  
  public static final int SUCCESS = 0;
  
  public static final int STOPPED = -2;
  
  public static final int QUEUE_FLUSH = 0;
  
  static final int QUEUE_DESTROY = 2;
  
  public static final int QUEUE_ADD = 1;
  
  public static final int LANG_NOT_SUPPORTED = -2;
  
  public static final int LANG_MISSING_DATA = -1;
  
  public static final int LANG_COUNTRY_VAR_AVAILABLE = 2;
  
  public static final int LANG_COUNTRY_AVAILABLE = 1;
  
  public static final int LANG_AVAILABLE = 0;
  
  public static final int ERROR_SYNTHESIS = -3;
  
  public static final int ERROR_SERVICE = -4;
  
  public static final int ERROR_OUTPUT = -5;
  
  public static final int ERROR_NOT_INSTALLED_YET = -9;
  
  public static final int ERROR_NETWORK_TIMEOUT = -7;
  
  public static final int ERROR_NETWORK = -6;
  
  public static final int ERROR_INVALID_REQUEST = -8;
  
  public static final int ERROR = -1;
  
  public static final String ACTION_TTS_QUEUE_PROCESSING_COMPLETED = "android.speech.tts.TTS_QUEUE_PROCESSING_COMPLETED";
  
  public TextToSpeech(Context paramContext, OnInitListener paramOnInitListener) {
    this(paramContext, paramOnInitListener, null);
  }
  
  public TextToSpeech(Context paramContext, OnInitListener paramOnInitListener, String paramString) {
    this(paramContext, paramOnInitListener, paramString, null, true);
  }
  
  public TextToSpeech(Context paramContext, OnInitListener paramOnInitListener, String paramString1, String paramString2, boolean paramBoolean) {
    this.mContext = paramContext;
    this.mInitListener = paramOnInitListener;
    this.mRequestedEngine = paramString1;
    this.mUseFallback = paramBoolean;
    this.mEarcons = new HashMap<>();
    this.mUtterances = new HashMap<>();
    this.mUtteranceProgressListener = null;
    this.mEnginesHelper = new TtsEngines(this.mContext);
    initTts();
  }
  
  private <R> R runActionNoReconnect(Action<R> paramAction, R paramR, String paramString, boolean paramBoolean) {
    return runAction(paramAction, paramR, paramString, false, paramBoolean);
  }
  
  private <R> R runAction(Action<R> paramAction, R paramR, String paramString) {
    return runAction(paramAction, paramR, paramString, true, true);
  }
  
  private <R> R runAction(Action<R> paramAction, R paramR, String paramString, boolean paramBoolean1, boolean paramBoolean2) {
    synchronized (this.mStartLock) {
      if (this.mServiceConnection == null) {
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(paramString);
        stringBuilder.append(" failed: not bound to TTS engine");
        Log.w("TextToSpeech", stringBuilder.toString());
        return paramR;
      } 
      StringBuilder stringBuilder = this.mServiceConnection.runAction((Action<StringBuilder>)stringBuilder, (StringBuilder)paramR, paramString, paramBoolean1, paramBoolean2);
      return (R)stringBuilder;
    } 
  }
  
  private int initTts() {
    String str1 = this.mRequestedEngine;
    if (str1 != null)
      if (this.mEnginesHelper.isEngineInstalled(str1)) {
        if (connectToEngine(this.mRequestedEngine)) {
          this.mCurrentEngine = this.mRequestedEngine;
          return 0;
        } 
        if (!this.mUseFallback) {
          this.mCurrentEngine = null;
          dispatchOnInit(-1);
          return -1;
        } 
      } else if (!this.mUseFallback) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Requested engine not installed: ");
        stringBuilder.append(this.mRequestedEngine);
        Log.i("TextToSpeech", stringBuilder.toString());
        this.mCurrentEngine = null;
        dispatchOnInit(-1);
        return -1;
      }  
    String str2 = getDefaultEngine();
    if (str2 != null && !str2.equals(this.mRequestedEngine) && 
      connectToEngine(str2)) {
      this.mCurrentEngine = str2;
      return 0;
    } 
    str1 = this.mEnginesHelper.getHighestRankedEngineName();
    if (str1 != null && !str1.equals(this.mRequestedEngine) && 
      !str1.equals(str2) && 
      connectToEngine(str1)) {
      this.mCurrentEngine = str1;
      return 0;
    } 
    this.mCurrentEngine = null;
    dispatchOnInit(-1);
    return -1;
  }
  
  private boolean connectToEngine(String paramString) {
    StringBuilder stringBuilder1;
    Connection connection = new Connection();
    Intent intent = new Intent("android.intent.action.TTS_SERVICE");
    intent.setPackage(paramString);
    boolean bool = this.mContext.bindService(intent, connection, 1);
    if (!bool) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Failed to bind to ");
      stringBuilder1.append(paramString);
      Log.e("TextToSpeech", stringBuilder1.toString());
      return false;
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("Sucessfully bound to ");
    stringBuilder2.append(paramString);
    Log.i("TextToSpeech", stringBuilder2.toString());
    this.mConnectingServiceConnection = (Connection)stringBuilder1;
    return true;
  }
  
  private void dispatchOnInit(int paramInt) {
    synchronized (this.mStartLock) {
      if (this.mInitListener != null) {
        this.mInitListener.onInit(paramInt);
        this.mInitListener = null;
      } 
      return;
    } 
  }
  
  private IBinder getCallerIdentity() {
    return this.mServiceConnection.getCallerIdentity();
  }
  
  public void shutdown() {
    synchronized (this.mStartLock) {
      if (this.mConnectingServiceConnection != null) {
        this.mContext.unbindService(this.mConnectingServiceConnection);
        this.mConnectingServiceConnection = null;
        return;
      } 
      runActionNoReconnect(new _$$Lambda$TextToSpeech$_uDA9GI4eb2MQUEii8nt8nERxiY(this), null, "shutdown", false);
      return;
    } 
  }
  
  public int addSpeech(String paramString1, String paramString2, int paramInt) {
    synchronized (this.mStartLock) {
      this.mUtterances.put(paramString1, makeResourceUri(paramString2, paramInt));
      return 0;
    } 
  }
  
  public int addSpeech(CharSequence paramCharSequence, String paramString, int paramInt) {
    synchronized (this.mStartLock) {
      this.mUtterances.put(paramCharSequence, makeResourceUri(paramString, paramInt));
      return 0;
    } 
  }
  
  public int addSpeech(String paramString1, String paramString2) {
    synchronized (this.mStartLock) {
      this.mUtterances.put(paramString1, Uri.parse(paramString2));
      return 0;
    } 
  }
  
  public int addSpeech(CharSequence paramCharSequence, File paramFile) {
    synchronized (this.mStartLock) {
      this.mUtterances.put(paramCharSequence, Uri.fromFile(paramFile));
      return 0;
    } 
  }
  
  public int addEarcon(String paramString1, String paramString2, int paramInt) {
    synchronized (this.mStartLock) {
      this.mEarcons.put(paramString1, makeResourceUri(paramString2, paramInt));
      return 0;
    } 
  }
  
  @Deprecated
  public int addEarcon(String paramString1, String paramString2) {
    synchronized (this.mStartLock) {
      this.mEarcons.put(paramString1, Uri.parse(paramString2));
      return 0;
    } 
  }
  
  public int addEarcon(String paramString, File paramFile) {
    synchronized (this.mStartLock) {
      this.mEarcons.put(paramString, Uri.fromFile(paramFile));
      return 0;
    } 
  }
  
  private Uri makeResourceUri(String paramString, int paramInt) {
    Uri.Builder builder2 = new Uri.Builder();
    builder2 = builder2.scheme("android.resource");
    Uri.Builder builder1 = builder2.encodedAuthority(paramString);
    builder1 = builder1.appendEncodedPath(String.valueOf(paramInt));
    return builder1.build();
  }
  
  public int speak(CharSequence paramCharSequence, int paramInt, Bundle paramBundle, String paramString) {
    _$$Lambda$TextToSpeech$0sxRIm64uH_HLqkzl4HmvlrT8_8 _$$Lambda$TextToSpeech$0sxRIm64uH_HLqkzl4HmvlrT8_8 = new _$$Lambda$TextToSpeech$0sxRIm64uH_HLqkzl4HmvlrT8_8(this, paramCharSequence, paramInt, paramBundle, paramString);
    return ((Integer)runAction(_$$Lambda$TextToSpeech$0sxRIm64uH_HLqkzl4HmvlrT8_8, Integer.valueOf(-1), "speak")).intValue();
  }
  
  @Deprecated
  public int speak(String paramString, int paramInt, HashMap<String, String> paramHashMap) {
    String str;
    Bundle bundle = convertParamsHashMaptoBundle(paramHashMap);
    if (paramHashMap == null) {
      paramHashMap = null;
    } else {
      str = paramHashMap.get("utteranceId");
    } 
    return speak(paramString, paramInt, bundle, str);
  }
  
  public int playEarcon(String paramString1, int paramInt, Bundle paramBundle, String paramString2) {
    _$$Lambda$TextToSpeech$S_Nd5U1XLz0kJB_yE5zdE9y8JCQ _$$Lambda$TextToSpeech$S_Nd5U1XLz0kJB_yE5zdE9y8JCQ = new _$$Lambda$TextToSpeech$S_Nd5U1XLz0kJB_yE5zdE9y8JCQ(this, paramString1, paramInt, paramBundle, paramString2);
    return ((Integer)runAction(_$$Lambda$TextToSpeech$S_Nd5U1XLz0kJB_yE5zdE9y8JCQ, Integer.valueOf(-1), "playEarcon")).intValue();
  }
  
  @Deprecated
  public int playEarcon(String paramString, int paramInt, HashMap<String, String> paramHashMap) {
    String str;
    Bundle bundle = convertParamsHashMaptoBundle(paramHashMap);
    if (paramHashMap == null) {
      paramHashMap = null;
    } else {
      str = paramHashMap.get("utteranceId");
    } 
    return playEarcon(paramString, paramInt, bundle, str);
  }
  
  public int playSilentUtterance(long paramLong, int paramInt, String paramString) {
    _$$Lambda$TextToSpeech$gWiZrbosTL_zRtizDp_NJINTIlw _$$Lambda$TextToSpeech$gWiZrbosTL_zRtizDp_NJINTIlw = new _$$Lambda$TextToSpeech$gWiZrbosTL_zRtizDp_NJINTIlw(this, paramLong, paramInt, paramString);
    return ((Integer)runAction(_$$Lambda$TextToSpeech$gWiZrbosTL_zRtizDp_NJINTIlw, Integer.valueOf(-1), "playSilentUtterance")).intValue();
  }
  
  @Deprecated
  public int playSilence(long paramLong, int paramInt, HashMap<String, String> paramHashMap) {
    String str;
    if (paramHashMap == null) {
      paramHashMap = null;
    } else {
      str = paramHashMap.get("utteranceId");
    } 
    return playSilentUtterance(paramLong, paramInt, str);
  }
  
  @Deprecated
  public Set<String> getFeatures(Locale paramLocale) {
    return runAction(new _$$Lambda$TextToSpeech$3k0fKs0_apyWO_AX6PsXCEYJz_E(paramLocale), null, "getFeatures");
  }
  
  public boolean isSpeaking() {
    -$.Lambda.TextToSpeech.X57btyWBAcjYI3iFilFzkbog6o x57btyWBAcjYI3iFilFzkbog6o = _$$Lambda$TextToSpeech$0X57btyWBAcjYI3iFilFzkbog6o.INSTANCE;
    return ((Boolean)runAction((Action<Boolean>)x57btyWBAcjYI3iFilFzkbog6o, Boolean.valueOf(false), "isSpeaking")).booleanValue();
  }
  
  public int stop() {
    _$$Lambda$TextToSpeech$rAlRDxyrlufrWCXLLCrw4EQupZo _$$Lambda$TextToSpeech$rAlRDxyrlufrWCXLLCrw4EQupZo = new _$$Lambda$TextToSpeech$rAlRDxyrlufrWCXLLCrw4EQupZo(this);
    return ((Integer)runAction(_$$Lambda$TextToSpeech$rAlRDxyrlufrWCXLLCrw4EQupZo, Integer.valueOf(-1), "stop")).intValue();
  }
  
  public int setSpeechRate(float paramFloat) {
    if (paramFloat > 0.0F) {
      int i = (int)(100.0F * paramFloat);
      if (i > 0)
        synchronized (this.mStartLock) {
          this.mParams.putInt("rate", i);
          return 0;
        }  
    } 
    return -1;
  }
  
  public int setPitch(float paramFloat) {
    if (paramFloat > 0.0F) {
      int i = (int)(100.0F * paramFloat);
      if (i > 0)
        synchronized (this.mStartLock) {
          this.mParams.putInt("pitch", i);
          return 0;
        }  
    } 
    return -1;
  }
  
  public int setAudioAttributes(AudioAttributes paramAudioAttributes) {
    if (paramAudioAttributes != null)
      synchronized (this.mStartLock) {
        this.mParams.putParcelable("audioAttributes", (Parcelable)paramAudioAttributes);
        return 0;
      }  
    return -1;
  }
  
  public String getCurrentEngine() {
    return this.mCurrentEngine;
  }
  
  @Deprecated
  public Locale getDefaultLanguage() {
    return runAction((Action<Locale>)_$$Lambda$TextToSpeech$xH3gJMX0vD1oEt8piDmZt3rN_I0.INSTANCE, null, "getDefaultLanguage");
  }
  
  public int setLanguage(Locale paramLocale) {
    _$$Lambda$TextToSpeech$RsLPOHbMGtIthv7AyRho2VQZrE0 _$$Lambda$TextToSpeech$RsLPOHbMGtIthv7AyRho2VQZrE0 = new _$$Lambda$TextToSpeech$RsLPOHbMGtIthv7AyRho2VQZrE0(this, paramLocale);
    return ((Integer)runAction(_$$Lambda$TextToSpeech$RsLPOHbMGtIthv7AyRho2VQZrE0, Integer.valueOf(-2), "setLanguage")).intValue();
  }
  
  @Deprecated
  public Locale getLanguage() {
    return runAction(new _$$Lambda$TextToSpeech$SCSPSCsV21MGYtjsy7CFU8T2th8(this), null, "getLanguage");
  }
  
  public Set<Locale> getAvailableLanguages() {
    return runAction((Action<Set<Locale>>)_$$Lambda$TextToSpeech$u9Oi4DXhm78ulJUBVghiKVZQjPU.INSTANCE, null, "getAvailableLanguages");
  }
  
  public Set<Voice> getVoices() {
    return runAction((Action<Set<Voice>>)_$$Lambda$TextToSpeech$XHpwKL0cN9fnzsF2ozBQilRypm4.INSTANCE, null, "getVoices");
  }
  
  public int setVoice(Voice paramVoice) {
    _$$Lambda$TextToSpeech$6gq2_hmHwhnAAw1VHrcD_6tHMw8 _$$Lambda$TextToSpeech$6gq2_hmHwhnAAw1VHrcD_6tHMw8 = new _$$Lambda$TextToSpeech$6gq2_hmHwhnAAw1VHrcD_6tHMw8(this, paramVoice);
    return ((Integer)runAction(_$$Lambda$TextToSpeech$6gq2_hmHwhnAAw1VHrcD_6tHMw8, Integer.valueOf(-2), "setVoice")).intValue();
  }
  
  public Voice getVoice() {
    return runAction(new _$$Lambda$TextToSpeech$uFLQixb7gBl79G3qfDlUfVtjF1s(this), null, "getVoice");
  }
  
  private Voice getVoice(ITextToSpeechService paramITextToSpeechService, String paramString) throws RemoteException {
    List<Voice> list = paramITextToSpeechService.getVoices();
    if (list == null) {
      Log.w("TextToSpeech", "getVoices returned null");
      return null;
    } 
    for (Voice voice : list) {
      if (voice.getName().equals(paramString))
        return voice; 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Could not find voice ");
    stringBuilder.append(paramString);
    stringBuilder.append(" in voice list");
    Log.w("TextToSpeech", stringBuilder.toString());
    return null;
  }
  
  public Voice getDefaultVoice() {
    return runAction((Action<Voice>)_$$Lambda$TextToSpeech$CLvszYuZtoVXMK_ouZ7G19Unb_E.INSTANCE, null, "getDefaultVoice");
  }
  
  public int isLanguageAvailable(Locale paramLocale) {
    _$$Lambda$TextToSpeech$_4bXSG9GsOV2bM_POOTpSFszEZs _$$Lambda$TextToSpeech$_4bXSG9GsOV2bM_POOTpSFszEZs = new _$$Lambda$TextToSpeech$_4bXSG9GsOV2bM_POOTpSFszEZs(paramLocale);
    return ((Integer)runAction(_$$Lambda$TextToSpeech$_4bXSG9GsOV2bM_POOTpSFszEZs, Integer.valueOf(-2), "isLanguageAvailable")).intValue();
  }
  
  public int synthesizeToFile(CharSequence paramCharSequence, Bundle paramBundle, ParcelFileDescriptor paramParcelFileDescriptor, String paramString) {
    _$$Lambda$TextToSpeech$RmMsygQ8Y8_w6j3mi5IAGOmSEIg _$$Lambda$TextToSpeech$RmMsygQ8Y8_w6j3mi5IAGOmSEIg = new _$$Lambda$TextToSpeech$RmMsygQ8Y8_w6j3mi5IAGOmSEIg(this, paramCharSequence, paramParcelFileDescriptor, paramBundle, paramString);
    return ((Integer)runAction(_$$Lambda$TextToSpeech$RmMsygQ8Y8_w6j3mi5IAGOmSEIg, Integer.valueOf(-1), "synthesizeToFile")).intValue();
  }
  
  public int synthesizeToFile(CharSequence paramCharSequence, Bundle paramBundle, File paramFile, String paramString) {
    if (paramFile.exists() && !paramFile.canWrite()) {
      paramCharSequence = new StringBuilder();
      paramCharSequence.append("Can't write to ");
      paramCharSequence.append(paramFile);
      Log.e("TextToSpeech", paramCharSequence.toString());
      return -1;
    } 
    try {
      ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.open(paramFile, 738197504);
      try {
        int i = synthesizeToFile(paramCharSequence, paramBundle, parcelFileDescriptor, paramString);
        parcelFileDescriptor.close();
        return i;
      } finally {
        if (parcelFileDescriptor != null)
          try {
            parcelFileDescriptor.close();
          } finally {
            paramBundle = null;
          }  
      } 
    } catch (FileNotFoundException fileNotFoundException) {
      paramCharSequence = new StringBuilder();
      paramCharSequence.append("Opening file ");
      paramCharSequence.append(paramFile);
      paramCharSequence.append(" failed");
      Log.e("TextToSpeech", paramCharSequence.toString(), fileNotFoundException);
      return -1;
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Closing file ");
      stringBuilder.append(paramFile);
      stringBuilder.append(" failed");
      Log.e("TextToSpeech", stringBuilder.toString(), iOException);
      return -1;
    } 
  }
  
  @Deprecated
  public int synthesizeToFile(String paramString1, HashMap<String, String> paramHashMap, String paramString2) {
    Bundle bundle = convertParamsHashMaptoBundle(paramHashMap);
    File file = new File(paramString2);
    String str = paramHashMap.get("utteranceId");
    return synthesizeToFile(paramString1, bundle, file, str);
  }
  
  private Bundle convertParamsHashMaptoBundle(HashMap<String, String> paramHashMap) {
    if (paramHashMap != null && !paramHashMap.isEmpty()) {
      Bundle bundle = new Bundle();
      copyIntParam(bundle, paramHashMap, "streamType");
      copyIntParam(bundle, paramHashMap, "sessionId");
      copyStringParam(bundle, paramHashMap, "utteranceId");
      copyFloatParam(bundle, paramHashMap, "volume");
      copyFloatParam(bundle, paramHashMap, "pan");
      copyStringParam(bundle, paramHashMap, "networkTts");
      copyStringParam(bundle, paramHashMap, "embeddedTts");
      copyIntParam(bundle, paramHashMap, "networkTimeoutMs");
      copyIntParam(bundle, paramHashMap, "networkRetriesCount");
      if (!TextUtils.isEmpty(this.mCurrentEngine))
        for (Map.Entry<String, String> entry : paramHashMap.entrySet()) {
          String str = (String)entry.getKey();
          if (str != null && str.startsWith(this.mCurrentEngine))
            bundle.putString(str, (String)entry.getValue()); 
        }  
      return bundle;
    } 
    return null;
  }
  
  private Bundle getParams(Bundle paramBundle) {
    if (paramBundle != null && !paramBundle.isEmpty()) {
      Bundle bundle = new Bundle(this.mParams);
      bundle.putAll(paramBundle);
      verifyIntegerBundleParam(bundle, "streamType");
      verifyIntegerBundleParam(bundle, "sessionId");
      verifyStringBundleParam(bundle, "utteranceId");
      verifyFloatBundleParam(bundle, "volume");
      verifyFloatBundleParam(bundle, "pan");
      verifyBooleanBundleParam(bundle, "networkTts");
      verifyBooleanBundleParam(bundle, "embeddedTts");
      verifyIntegerBundleParam(bundle, "networkTimeoutMs");
      verifyIntegerBundleParam(bundle, "networkRetriesCount");
      return bundle;
    } 
    return this.mParams;
  }
  
  private static boolean verifyIntegerBundleParam(Bundle paramBundle, String paramString) {
    if (paramBundle.containsKey(paramString) && 
      !(paramBundle.get(paramString) instanceof Integer) && 
      !(paramBundle.get(paramString) instanceof Long)) {
      paramBundle.remove(paramString);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Synthesis request paramter ");
      stringBuilder.append(paramString);
      stringBuilder.append(" containst value  with invalid type. Should be an Integer or a Long");
      Log.w("TextToSpeech", stringBuilder.toString());
      return false;
    } 
    return true;
  }
  
  private static boolean verifyStringBundleParam(Bundle paramBundle, String paramString) {
    if (paramBundle.containsKey(paramString) && 
      !(paramBundle.get(paramString) instanceof String)) {
      paramBundle.remove(paramString);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Synthesis request paramter ");
      stringBuilder.append(paramString);
      stringBuilder.append(" containst value  with invalid type. Should be a String");
      Log.w("TextToSpeech", stringBuilder.toString());
      return false;
    } 
    return true;
  }
  
  private static boolean verifyBooleanBundleParam(Bundle paramBundle, String paramString) {
    if (paramBundle.containsKey(paramString) && 
      !(paramBundle.get(paramString) instanceof Boolean) && 
      !(paramBundle.get(paramString) instanceof String)) {
      paramBundle.remove(paramString);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Synthesis request paramter ");
      stringBuilder.append(paramString);
      stringBuilder.append(" containst value  with invalid type. Should be a Boolean or String");
      Log.w("TextToSpeech", stringBuilder.toString());
      return false;
    } 
    return true;
  }
  
  private static boolean verifyFloatBundleParam(Bundle paramBundle, String paramString) {
    if (paramBundle.containsKey(paramString) && 
      !(paramBundle.get(paramString) instanceof Float) && 
      !(paramBundle.get(paramString) instanceof Double)) {
      paramBundle.remove(paramString);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Synthesis request paramter ");
      stringBuilder.append(paramString);
      stringBuilder.append(" containst value  with invalid type. Should be a Float or a Double");
      Log.w("TextToSpeech", stringBuilder.toString());
      return false;
    } 
    return true;
  }
  
  private void copyStringParam(Bundle paramBundle, HashMap<String, String> paramHashMap, String paramString) {
    String str = paramHashMap.get(paramString);
    if (str != null)
      paramBundle.putString(paramString, str); 
  }
  
  private void copyIntParam(Bundle paramBundle, HashMap<String, String> paramHashMap, String paramString) {
    String str = paramHashMap.get(paramString);
    if (!TextUtils.isEmpty(str))
      try {
        int i = Integer.parseInt(str);
        paramBundle.putInt(paramString, i);
      } catch (NumberFormatException numberFormatException) {} 
  }
  
  private void copyFloatParam(Bundle paramBundle, HashMap<String, String> paramHashMap, String paramString) {
    String str = paramHashMap.get(paramString);
    if (!TextUtils.isEmpty(str))
      try {
        float f = Float.parseFloat(str);
        paramBundle.putFloat(paramString, f);
      } catch (NumberFormatException numberFormatException) {} 
  }
  
  @Deprecated
  public int setOnUtteranceCompletedListener(OnUtteranceCompletedListener paramOnUtteranceCompletedListener) {
    this.mUtteranceProgressListener = UtteranceProgressListener.from(paramOnUtteranceCompletedListener);
    return 0;
  }
  
  public int setOnUtteranceProgressListener(UtteranceProgressListener paramUtteranceProgressListener) {
    this.mUtteranceProgressListener = paramUtteranceProgressListener;
    return 0;
  }
  
  @Deprecated
  public int setEngineByPackageName(String paramString) {
    this.mRequestedEngine = paramString;
    return initTts();
  }
  
  public String getDefaultEngine() {
    return this.mEnginesHelper.getDefaultEngine();
  }
  
  @Deprecated
  public boolean areDefaultsEnforced() {
    return false;
  }
  
  public List<EngineInfo> getEngines() {
    return this.mEnginesHelper.getEngines();
  }
  
  class Connection implements ServiceConnection {
    private final ITextToSpeechCallback.Stub mCallback = (ITextToSpeechCallback.Stub)new Object(this);
    
    private boolean mEstablished;
    
    private SetupConnectionAsyncTask mOnSetupConnectionAsyncTask;
    
    private ITextToSpeechService mService;
    
    final TextToSpeech this$0;
    
    private class SetupConnectionAsyncTask extends AsyncTask<Void, Void, Integer> {
      private final ComponentName mName;
      
      final TextToSpeech.Connection this$1;
      
      public SetupConnectionAsyncTask(ComponentName param2ComponentName) {
        this.mName = param2ComponentName;
      }
      
      protected Integer doInBackground(Void... param2VarArgs) {
        synchronized (TextToSpeech.this.mStartLock) {
          if (isCancelled())
            return null; 
          try {
            TextToSpeech.Connection.this.mService.setCallback(TextToSpeech.Connection.this.getCallerIdentity(), TextToSpeech.Connection.this.mCallback);
            if (TextToSpeech.this.mParams.getString("language") == null) {
              String[] arrayOfString = TextToSpeech.Connection.this.mService.getClientDefaultLanguage();
              TextToSpeech.this.mParams.putString("language", arrayOfString[0]);
              TextToSpeech.this.mParams.putString("country", arrayOfString[1]);
              TextToSpeech.this.mParams.putString("variant", arrayOfString[2]);
              String str = TextToSpeech.Connection.this.mService.getDefaultVoiceNameFor(arrayOfString[0], arrayOfString[1], arrayOfString[2]);
              TextToSpeech.this.mParams.putString("voiceName", str);
            } 
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Set up connection to ");
            stringBuilder.append(this.mName);
            Log.i("TextToSpeech", stringBuilder.toString());
            return Integer.valueOf(0);
          } catch (RemoteException remoteException) {
            Log.e("TextToSpeech", "Error connecting to service, setCallback() failed");
            return Integer.valueOf(-1);
          } 
        } 
      }
      
      protected void onPostExecute(Integer param2Integer) {
        synchronized (TextToSpeech.this.mStartLock) {
          if (TextToSpeech.Connection.this.mOnSetupConnectionAsyncTask == this)
            TextToSpeech.Connection.access$602(TextToSpeech.Connection.this, null); 
          TextToSpeech.Connection.access$702(TextToSpeech.Connection.this, true);
          TextToSpeech.this.dispatchOnInit(param2Integer.intValue());
          return;
        } 
      }
    }
    
    public void onServiceConnected(ComponentName param1ComponentName, IBinder param1IBinder) {
      synchronized (TextToSpeech.this.mStartLock) {
        TextToSpeech.access$902(TextToSpeech.this, null);
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Connected to ");
        stringBuilder.append(param1ComponentName);
        Log.i("TextToSpeech", stringBuilder.toString());
        if (this.mOnSetupConnectionAsyncTask != null)
          this.mOnSetupConnectionAsyncTask.cancel(false); 
        this.mService = ITextToSpeechService.Stub.asInterface(param1IBinder);
        TextToSpeech.access$1002(TextToSpeech.this, this);
        this.mEstablished = false;
        SetupConnectionAsyncTask setupConnectionAsyncTask = new SetupConnectionAsyncTask();
        this(this, param1ComponentName);
        this.mOnSetupConnectionAsyncTask = setupConnectionAsyncTask;
        setupConnectionAsyncTask.execute((Object[])new Void[0]);
        return;
      } 
    }
    
    public IBinder getCallerIdentity() {
      return (IBinder)this.mCallback;
    }
    
    private boolean clearServiceConnection() {
      // Byte code:
      //   0: aload_0
      //   1: getfield this$0 : Landroid/speech/tts/TextToSpeech;
      //   4: invokestatic access$200 : (Landroid/speech/tts/TextToSpeech;)Ljava/lang/Object;
      //   7: astore_1
      //   8: aload_1
      //   9: monitorenter
      //   10: iconst_0
      //   11: istore_2
      //   12: aload_0
      //   13: getfield mOnSetupConnectionAsyncTask : Landroid/speech/tts/TextToSpeech$Connection$SetupConnectionAsyncTask;
      //   16: ifnull -> 33
      //   19: aload_0
      //   20: getfield mOnSetupConnectionAsyncTask : Landroid/speech/tts/TextToSpeech$Connection$SetupConnectionAsyncTask;
      //   23: iconst_0
      //   24: invokevirtual cancel : (Z)Z
      //   27: istore_2
      //   28: aload_0
      //   29: aconst_null
      //   30: putfield mOnSetupConnectionAsyncTask : Landroid/speech/tts/TextToSpeech$Connection$SetupConnectionAsyncTask;
      //   33: aload_0
      //   34: aconst_null
      //   35: putfield mService : Landroid/speech/tts/ITextToSpeechService;
      //   38: aload_0
      //   39: getfield this$0 : Landroid/speech/tts/TextToSpeech;
      //   42: invokestatic access$1000 : (Landroid/speech/tts/TextToSpeech;)Landroid/speech/tts/TextToSpeech$Connection;
      //   45: aload_0
      //   46: if_acmpne -> 58
      //   49: aload_0
      //   50: getfield this$0 : Landroid/speech/tts/TextToSpeech;
      //   53: aconst_null
      //   54: invokestatic access$1002 : (Landroid/speech/tts/TextToSpeech;Landroid/speech/tts/TextToSpeech$Connection;)Landroid/speech/tts/TextToSpeech$Connection;
      //   57: pop
      //   58: aload_1
      //   59: monitorexit
      //   60: iload_2
      //   61: ireturn
      //   62: astore_3
      //   63: aload_1
      //   64: monitorexit
      //   65: aload_3
      //   66: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2261	-> 0
      //   #2262	-> 10
      //   #2263	-> 12
      //   #2264	-> 19
      //   #2265	-> 28
      //   #2268	-> 33
      //   #2270	-> 38
      //   #2271	-> 49
      //   #2273	-> 58
      //   #2274	-> 62
      // Exception table:
      //   from	to	target	type
      //   12	19	62	finally
      //   19	28	62	finally
      //   28	33	62	finally
      //   33	38	62	finally
      //   38	49	62	finally
      //   49	58	62	finally
      //   58	60	62	finally
      //   63	65	62	finally
    }
    
    public void onServiceDisconnected(ComponentName param1ComponentName) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Asked to disconnect from ");
      stringBuilder.append(param1ComponentName);
      Log.i("TextToSpeech", stringBuilder.toString());
      if (clearServiceConnection())
        TextToSpeech.this.dispatchOnInit(-1); 
    }
    
    public void disconnect() {
      TextToSpeech.this.mContext.unbindService(this);
      clearServiceConnection();
    }
    
    public boolean isEstablished() {
      boolean bool;
      if (this.mService != null && this.mEstablished) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public <R> R runAction(TextToSpeech.Action<R> param1Action, R param1R, String param1String, boolean param1Boolean1, boolean param1Boolean2) {
      Object object = TextToSpeech.this.mStartLock;
      /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      try {
        if (this.mService == null) {
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append(param1String);
          stringBuilder.append(" failed: not connected to TTS engine");
          Log.w("TextToSpeech", stringBuilder.toString());
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          return param1R;
        } 
        if (param1Boolean2 && !isEstablished()) {
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append(param1String);
          stringBuilder.append(" failed: TTS engine connection not fully set up");
          Log.w("TextToSpeech", stringBuilder.toString());
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          return param1R;
        } 
        StringBuilder stringBuilder = stringBuilder.run(this.mService);
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        return (R)stringBuilder;
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(param1String);
        stringBuilder.append(" failed");
        Log.e("TextToSpeech", stringBuilder.toString(), (Throwable)remoteException);
        if (param1Boolean1) {
          disconnect();
          TextToSpeech.this.initTts();
        } 
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        return param1R;
      } finally {}
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      throw param1Action;
    }
    
    private Connection() {}
  }
  
  public static class EngineInfo {
    public int icon;
    
    public String label;
    
    public String name;
    
    public int priority;
    
    public boolean system;
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("EngineInfo{name=");
      stringBuilder.append(this.name);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  public static int getMaxSpeechInputLength() {
    return 4000;
  }
  
  private static interface Action<R> {
    R run(ITextToSpeechService param1ITextToSpeechService) throws RemoteException;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Error {}
  
  public static interface OnInitListener {
    void onInit(int param1Int);
  }
  
  @Deprecated
  public static interface OnUtteranceCompletedListener {
    void onUtteranceCompleted(String param1String);
  }
}
