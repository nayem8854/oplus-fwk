package android.speech.tts;

import android.os.SystemClock;

abstract class AbstractEventLogger {
  protected long mPlaybackStartTime = -1L;
  
  private volatile long mRequestProcessingStartTime = -1L;
  
  private volatile long mEngineStartTime = -1L;
  
  private volatile long mEngineCompleteTime = -1L;
  
  private boolean mLogWritten = false;
  
  protected final int mCallerPid;
  
  protected final int mCallerUid;
  
  protected final long mReceivedTime;
  
  protected final String mServiceApp;
  
  AbstractEventLogger(int paramInt1, int paramInt2, String paramString) {
    this.mCallerUid = paramInt1;
    this.mCallerPid = paramInt2;
    this.mServiceApp = paramString;
    this.mReceivedTime = SystemClock.elapsedRealtime();
  }
  
  public void onRequestProcessingStart() {
    this.mRequestProcessingStartTime = SystemClock.elapsedRealtime();
  }
  
  public void onEngineDataReceived() {
    if (this.mEngineStartTime == -1L)
      this.mEngineStartTime = SystemClock.elapsedRealtime(); 
  }
  
  public void onEngineComplete() {
    this.mEngineCompleteTime = SystemClock.elapsedRealtime();
  }
  
  public void onAudioDataWritten() {
    if (this.mPlaybackStartTime == -1L)
      this.mPlaybackStartTime = SystemClock.elapsedRealtime(); 
  }
  
  public void onCompleted(int paramInt) {
    if (this.mLogWritten)
      return; 
    this.mLogWritten = true;
    SystemClock.elapsedRealtime();
    if (paramInt != 0 || this.mPlaybackStartTime == -1L || this.mEngineCompleteTime == -1L) {
      logFailure(paramInt);
      return;
    } 
    long l1 = this.mPlaybackStartTime, l2 = this.mReceivedTime;
    long l3 = this.mEngineStartTime, l4 = this.mRequestProcessingStartTime;
    long l5 = this.mEngineCompleteTime, l6 = this.mRequestProcessingStartTime;
    logSuccess(l1 - l2, l3 - l4, l5 - l6);
  }
  
  protected abstract void logFailure(int paramInt);
  
  protected abstract void logSuccess(long paramLong1, long paramLong2, long paramLong3);
}
