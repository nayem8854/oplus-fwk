package android.speech.tts;

import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;

class AudioPlaybackHandler {
  private final LinkedBlockingQueue<PlaybackQueueItem> mQueue = new LinkedBlockingQueue<>();
  
  private final Thread mHandlerThread;
  
  private volatile PlaybackQueueItem mCurrentWorkItem = null;
  
  private static final String TAG = "TTS.AudioPlaybackHandler";
  
  private static final boolean DBG = false;
  
  AudioPlaybackHandler() {
    this.mHandlerThread = new Thread(new MessageLoop(), "TTS.AudioPlaybackThread");
  }
  
  public void start() {
    this.mHandlerThread.start();
  }
  
  private void stop(PlaybackQueueItem paramPlaybackQueueItem) {
    if (paramPlaybackQueueItem == null)
      return; 
    paramPlaybackQueueItem.stop(-2);
  }
  
  public void enqueue(PlaybackQueueItem paramPlaybackQueueItem) {
    try {
      this.mQueue.put(paramPlaybackQueueItem);
    } catch (InterruptedException interruptedException) {}
  }
  
  public void stopForApp(Object paramObject) {
    removeWorkItemsFor(paramObject);
    PlaybackQueueItem playbackQueueItem = this.mCurrentWorkItem;
    if (playbackQueueItem != null && playbackQueueItem.getCallerIdentity() == paramObject)
      stop(playbackQueueItem); 
  }
  
  public void stop() {
    removeAllMessages();
    stop(this.mCurrentWorkItem);
  }
  
  public boolean isSpeaking() {
    return (this.mQueue.peek() != null || this.mCurrentWorkItem != null);
  }
  
  public void quit() {
    removeAllMessages();
    stop(this.mCurrentWorkItem);
    this.mHandlerThread.interrupt();
  }
  
  private void removeAllMessages() {
    this.mQueue.clear();
  }
  
  private void removeWorkItemsFor(Object paramObject) {
    Iterator<PlaybackQueueItem> iterator = this.mQueue.iterator();
    while (iterator.hasNext()) {
      PlaybackQueueItem playbackQueueItem = iterator.next();
      if (playbackQueueItem.getCallerIdentity() == paramObject) {
        iterator.remove();
        stop(playbackQueueItem);
      } 
    } 
  }
  
  private final class MessageLoop implements Runnable {
    final AudioPlaybackHandler this$0;
    
    private MessageLoop() {}
    
    public void run() {
      try {
        while (true) {
          PlaybackQueueItem playbackQueueItem = AudioPlaybackHandler.this.mQueue.take();
          AudioPlaybackHandler.access$202(AudioPlaybackHandler.this, playbackQueueItem);
          playbackQueueItem.run();
          AudioPlaybackHandler.access$202(AudioPlaybackHandler.this, null);
        } 
      } catch (InterruptedException interruptedException) {
        return;
      } 
    }
  }
}
