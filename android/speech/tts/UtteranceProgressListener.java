package android.speech.tts;

public abstract class UtteranceProgressListener {
  public void onError(String paramString, int paramInt) {
    onError(paramString);
  }
  
  public void onStop(String paramString, boolean paramBoolean) {}
  
  public void onBeginSynthesis(String paramString, int paramInt1, int paramInt2, int paramInt3) {}
  
  public void onAudioAvailable(String paramString, byte[] paramArrayOfbyte) {}
  
  public void onRangeStart(String paramString, int paramInt1, int paramInt2, int paramInt3) {
    onUtteranceRangeStart(paramString, paramInt1, paramInt2);
  }
  
  @Deprecated
  public void onUtteranceRangeStart(String paramString, int paramInt1, int paramInt2) {}
  
  static UtteranceProgressListener from(TextToSpeech.OnUtteranceCompletedListener paramOnUtteranceCompletedListener) {
    return (UtteranceProgressListener)new Object(paramOnUtteranceCompletedListener);
  }
  
  public abstract void onDone(String paramString);
  
  @Deprecated
  public abstract void onError(String paramString);
  
  public abstract void onStart(String paramString);
}
