package android.speech.tts;

import android.media.AudioFormat;
import android.util.Log;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;

class FileSynthesisCallback extends AbstractSynthesisCallback {
  protected int mStatusCode;
  
  private final Object mStateLock = new Object();
  
  private boolean mStarted = false;
  
  private int mSampleRateInHz;
  
  private FileChannel mFileChannel;
  
  private boolean mDone = false;
  
  private final TextToSpeechService.UtteranceProgressDispatcher mDispatcher;
  
  private int mChannelCount;
  
  private int mAudioFormat;
  
  private static final int WAV_HEADER_LENGTH = 44;
  
  private static final short WAV_FORMAT_PCM = 1;
  
  private static final String TAG = "FileSynthesisRequest";
  
  private static final int MAX_AUDIO_BUFFER_SIZE = 8192;
  
  private static final boolean DBG = false;
  
  FileSynthesisCallback(FileChannel paramFileChannel, TextToSpeechService.UtteranceProgressDispatcher paramUtteranceProgressDispatcher, boolean paramBoolean) {
    super(paramBoolean);
    this.mFileChannel = paramFileChannel;
    this.mDispatcher = paramUtteranceProgressDispatcher;
    this.mStatusCode = 0;
  }
  
  void stop() {
    synchronized (this.mStateLock) {
      if (this.mDone)
        return; 
      if (this.mStatusCode == -2)
        return; 
      this.mStatusCode = -2;
      cleanUp();
      this.mDispatcher.dispatchOnStop();
      return;
    } 
  }
  
  private void cleanUp() {
    closeFile();
  }
  
  private void closeFile() {
    this.mFileChannel = null;
  }
  
  public int getMaxBufferSize() {
    return 8192;
  }
  
  public int start(int paramInt1, int paramInt2, int paramInt3) {
    if (paramInt2 != 3 && paramInt2 != 2 && paramInt2 != 4) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Audio format encoding ");
      stringBuilder.append(paramInt2);
      stringBuilder.append(" not supported. Please use one of AudioFormat.ENCODING_PCM_8BIT, AudioFormat.ENCODING_PCM_16BIT or AudioFormat.ENCODING_PCM_FLOAT");
      Log.e("FileSynthesisRequest", stringBuilder.toString());
    } 
    this.mDispatcher.dispatchOnBeginSynthesis(paramInt1, paramInt2, paramInt3);
    synchronized (this.mStateLock) {
      if (this.mStatusCode == -2) {
        paramInt1 = errorCodeOnStop();
        return paramInt1;
      } 
      if (this.mStatusCode != 0)
        return -1; 
      if (this.mStarted) {
        Log.e("FileSynthesisRequest", "Start called twice");
        return -1;
      } 
      this.mStarted = true;
      this.mSampleRateInHz = paramInt1;
      this.mAudioFormat = paramInt2;
      this.mChannelCount = paramInt3;
      this.mDispatcher.dispatchOnStart();
      FileChannel fileChannel = this.mFileChannel;
      try {
        fileChannel.write(ByteBuffer.allocate(44));
        return 0;
      } catch (IOException iOException) {
        Log.e("FileSynthesisRequest", "Failed to write wav header to output file descriptor", iOException);
        synchronized (this.mStateLock) {
          cleanUp();
          this.mStatusCode = -5;
          return -1;
        } 
      } 
    } 
  }
  
  public int audioAvailable(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    synchronized (this.mStateLock) {
      if (this.mStatusCode == -2) {
        paramInt1 = errorCodeOnStop();
        return paramInt1;
      } 
      if (this.mStatusCode != 0)
        return -1; 
      if (this.mFileChannel == null) {
        Log.e("FileSynthesisRequest", "File not open");
        this.mStatusCode = -5;
        return -1;
      } 
      if (!this.mStarted) {
        Log.e("FileSynthesisRequest", "Start method was not called");
        return -1;
      } 
      FileChannel fileChannel = this.mFileChannel;
      null = new byte[paramInt2];
      System.arraycopy(paramArrayOfbyte, paramInt1, null, 0, paramInt2);
      this.mDispatcher.dispatchOnAudioAvailable((byte[])null);
      try {
        fileChannel.write(ByteBuffer.wrap(paramArrayOfbyte, paramInt1, paramInt2));
        return 0;
      } catch (IOException iOException) {
        Log.e("FileSynthesisRequest", "Failed to write to output file descriptor", iOException);
        synchronized (this.mStateLock) {
          cleanUp();
          this.mStatusCode = -5;
          return -1;
        } 
      } 
    } 
  }
  
  public int done() {
    synchronized (this.mStateLock) {
      if (this.mDone) {
        Log.w("FileSynthesisRequest", "Duplicate call to done()");
        return -1;
      } 
      if (this.mStatusCode == -2)
        return errorCodeOnStop(); 
      if (this.mStatusCode != 0 && this.mStatusCode != -2) {
        this.mDispatcher.dispatchOnError(this.mStatusCode);
        return -1;
      } 
      if (this.mFileChannel == null) {
        Log.e("FileSynthesisRequest", "File not open");
        return -1;
      } 
      this.mDone = true;
      FileChannel fileChannel = this.mFileChannel;
      int i = this.mSampleRateInHz;
      int j = this.mAudioFormat;
      int k = this.mChannelCount;
      try {
        fileChannel.position(0L);
        int m = (int)(fileChannel.size() - 44L);
        null = makeWavHeader(i, j, k, m);
        fileChannel.write((ByteBuffer)null);
        synchronized (this.mStateLock) {
          closeFile();
          this.mDispatcher.dispatchOnSuccess();
          return 0;
        } 
      } catch (IOException iOException) {
        Log.e("FileSynthesisRequest", "Failed to write to output file descriptor", iOException);
        synchronized (this.mStateLock) {
          cleanUp();
          return -1;
        } 
      } 
    } 
  }
  
  public void error() {
    error(-3);
  }
  
  public void error(int paramInt) {
    synchronized (this.mStateLock) {
      if (this.mDone)
        return; 
      cleanUp();
      this.mStatusCode = paramInt;
      return;
    } 
  }
  
  public boolean hasStarted() {
    synchronized (this.mStateLock) {
      return this.mStarted;
    } 
  }
  
  public boolean hasFinished() {
    synchronized (this.mStateLock) {
      return this.mDone;
    } 
  }
  
  private ByteBuffer makeWavHeader(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    paramInt2 = AudioFormat.getBytesPerSample(paramInt2);
    short s1 = (short)(paramInt2 * paramInt3);
    short s2 = (short)(paramInt2 * 8);
    byte[] arrayOfByte = new byte[44];
    ByteBuffer byteBuffer = ByteBuffer.wrap(arrayOfByte);
    byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
    byteBuffer.put(new byte[] { 82, 73, 70, 70 });
    byteBuffer.putInt(paramInt4 + 44 - 8);
    byteBuffer.put(new byte[] { 87, 65, 86, 69 });
    byteBuffer.put(new byte[] { 102, 109, 116, 32 });
    byteBuffer.putInt(16);
    byteBuffer.putShort((short)1);
    byteBuffer.putShort((short)paramInt3);
    byteBuffer.putInt(paramInt1);
    byteBuffer.putInt(paramInt1 * paramInt2 * paramInt3);
    byteBuffer.putShort(s1);
    byteBuffer.putShort(s2);
    byteBuffer.put(new byte[] { 100, 97, 116, 97 });
    byteBuffer.putInt(paramInt4);
    byteBuffer.flip();
    return byteBuffer;
  }
  
  public void rangeStart(int paramInt1, int paramInt2, int paramInt3) {
    this.mDispatcher.dispatchOnRangeStart(paramInt1, paramInt2, paramInt3);
  }
}
