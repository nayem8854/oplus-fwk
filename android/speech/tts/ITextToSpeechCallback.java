package android.speech.tts;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ITextToSpeechCallback extends IInterface {
  void onAudioAvailable(String paramString, byte[] paramArrayOfbyte) throws RemoteException;
  
  void onBeginSynthesis(String paramString, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void onError(String paramString, int paramInt) throws RemoteException;
  
  void onRangeStart(String paramString, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void onStart(String paramString) throws RemoteException;
  
  void onStop(String paramString, boolean paramBoolean) throws RemoteException;
  
  void onSuccess(String paramString) throws RemoteException;
  
  class Default implements ITextToSpeechCallback {
    public void onStart(String param1String) throws RemoteException {}
    
    public void onSuccess(String param1String) throws RemoteException {}
    
    public void onStop(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void onError(String param1String, int param1Int) throws RemoteException {}
    
    public void onBeginSynthesis(String param1String, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void onAudioAvailable(String param1String, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void onRangeStart(String param1String, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITextToSpeechCallback {
    private static final String DESCRIPTOR = "android.speech.tts.ITextToSpeechCallback";
    
    static final int TRANSACTION_onAudioAvailable = 6;
    
    static final int TRANSACTION_onBeginSynthesis = 5;
    
    static final int TRANSACTION_onError = 4;
    
    static final int TRANSACTION_onRangeStart = 7;
    
    static final int TRANSACTION_onStart = 1;
    
    static final int TRANSACTION_onStop = 3;
    
    static final int TRANSACTION_onSuccess = 2;
    
    public Stub() {
      attachInterface(this, "android.speech.tts.ITextToSpeechCallback");
    }
    
    public static ITextToSpeechCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.speech.tts.ITextToSpeechCallback");
      if (iInterface != null && iInterface instanceof ITextToSpeechCallback)
        return (ITextToSpeechCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "onRangeStart";
        case 6:
          return "onAudioAvailable";
        case 5:
          return "onBeginSynthesis";
        case 4:
          return "onError";
        case 3:
          return "onStop";
        case 2:
          return "onSuccess";
        case 1:
          break;
      } 
      return "onStart";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        byte[] arrayOfByte;
        int i;
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("android.speech.tts.ITextToSpeechCallback");
            str = param1Parcel1.readString();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            i = param1Parcel1.readInt();
            onRangeStart(str, param1Int2, param1Int1, i);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.speech.tts.ITextToSpeechCallback");
            str = param1Parcel1.readString();
            arrayOfByte = param1Parcel1.createByteArray();
            onAudioAvailable(str, arrayOfByte);
            return true;
          case 5:
            arrayOfByte.enforceInterface("android.speech.tts.ITextToSpeechCallback");
            str = arrayOfByte.readString();
            param1Int1 = arrayOfByte.readInt();
            i = arrayOfByte.readInt();
            param1Int2 = arrayOfByte.readInt();
            onBeginSynthesis(str, param1Int1, i, param1Int2);
            return true;
          case 4:
            arrayOfByte.enforceInterface("android.speech.tts.ITextToSpeechCallback");
            str = arrayOfByte.readString();
            param1Int1 = arrayOfByte.readInt();
            onError(str, param1Int1);
            return true;
          case 3:
            arrayOfByte.enforceInterface("android.speech.tts.ITextToSpeechCallback");
            str = arrayOfByte.readString();
            if (arrayOfByte.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            onStop(str, bool);
            return true;
          case 2:
            arrayOfByte.enforceInterface("android.speech.tts.ITextToSpeechCallback");
            str1 = arrayOfByte.readString();
            onSuccess(str1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.speech.tts.ITextToSpeechCallback");
        String str1 = str1.readString();
        onStart(str1);
        return true;
      } 
      str.writeString("android.speech.tts.ITextToSpeechCallback");
      return true;
    }
    
    private static class Proxy implements ITextToSpeechCallback {
      public static ITextToSpeechCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.speech.tts.ITextToSpeechCallback";
      }
      
      public void onStart(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.speech.tts.ITextToSpeechCallback");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ITextToSpeechCallback.Stub.getDefaultImpl() != null) {
            ITextToSpeechCallback.Stub.getDefaultImpl().onStart(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSuccess(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.speech.tts.ITextToSpeechCallback");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && ITextToSpeechCallback.Stub.getDefaultImpl() != null) {
            ITextToSpeechCallback.Stub.getDefaultImpl().onSuccess(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onStop(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.speech.tts.ITextToSpeechCallback");
          parcel.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel, null, 1);
          if (!bool1 && ITextToSpeechCallback.Stub.getDefaultImpl() != null) {
            ITextToSpeechCallback.Stub.getDefaultImpl().onStop(param2String, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onError(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.speech.tts.ITextToSpeechCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && ITextToSpeechCallback.Stub.getDefaultImpl() != null) {
            ITextToSpeechCallback.Stub.getDefaultImpl().onError(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onBeginSynthesis(String param2String, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.speech.tts.ITextToSpeechCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && ITextToSpeechCallback.Stub.getDefaultImpl() != null) {
            ITextToSpeechCallback.Stub.getDefaultImpl().onBeginSynthesis(param2String, param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAudioAvailable(String param2String, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.speech.tts.ITextToSpeechCallback");
          parcel.writeString(param2String);
          parcel.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && ITextToSpeechCallback.Stub.getDefaultImpl() != null) {
            ITextToSpeechCallback.Stub.getDefaultImpl().onAudioAvailable(param2String, param2ArrayOfbyte);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRangeStart(String param2String, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.speech.tts.ITextToSpeechCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && ITextToSpeechCallback.Stub.getDefaultImpl() != null) {
            ITextToSpeechCallback.Stub.getDefaultImpl().onRangeStart(param2String, param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITextToSpeechCallback param1ITextToSpeechCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITextToSpeechCallback != null) {
          Proxy.sDefaultImpl = param1ITextToSpeechCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITextToSpeechCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
