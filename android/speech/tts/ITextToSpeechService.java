package android.speech.tts;

import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.text.TextUtils;
import java.util.List;

public interface ITextToSpeechService extends IInterface {
  String[] getClientDefaultLanguage() throws RemoteException;
  
  String getDefaultVoiceNameFor(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  String[] getFeaturesForLanguage(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  String[] getLanguage() throws RemoteException;
  
  List<Voice> getVoices() throws RemoteException;
  
  int isLanguageAvailable(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  boolean isSpeaking() throws RemoteException;
  
  int loadLanguage(IBinder paramIBinder, String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  int loadVoice(IBinder paramIBinder, String paramString) throws RemoteException;
  
  int playAudio(IBinder paramIBinder, Uri paramUri, int paramInt, Bundle paramBundle, String paramString) throws RemoteException;
  
  int playSilence(IBinder paramIBinder, long paramLong, int paramInt, String paramString) throws RemoteException;
  
  void setCallback(IBinder paramIBinder, ITextToSpeechCallback paramITextToSpeechCallback) throws RemoteException;
  
  int speak(IBinder paramIBinder, CharSequence paramCharSequence, int paramInt, Bundle paramBundle, String paramString) throws RemoteException;
  
  int stop(IBinder paramIBinder) throws RemoteException;
  
  int synthesizeToFileDescriptor(IBinder paramIBinder, CharSequence paramCharSequence, ParcelFileDescriptor paramParcelFileDescriptor, Bundle paramBundle, String paramString) throws RemoteException;
  
  class Default implements ITextToSpeechService {
    public int speak(IBinder param1IBinder, CharSequence param1CharSequence, int param1Int, Bundle param1Bundle, String param1String) throws RemoteException {
      return 0;
    }
    
    public int synthesizeToFileDescriptor(IBinder param1IBinder, CharSequence param1CharSequence, ParcelFileDescriptor param1ParcelFileDescriptor, Bundle param1Bundle, String param1String) throws RemoteException {
      return 0;
    }
    
    public int playAudio(IBinder param1IBinder, Uri param1Uri, int param1Int, Bundle param1Bundle, String param1String) throws RemoteException {
      return 0;
    }
    
    public int playSilence(IBinder param1IBinder, long param1Long, int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public boolean isSpeaking() throws RemoteException {
      return false;
    }
    
    public int stop(IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public String[] getLanguage() throws RemoteException {
      return null;
    }
    
    public String[] getClientDefaultLanguage() throws RemoteException {
      return null;
    }
    
    public int isLanguageAvailable(String param1String1, String param1String2, String param1String3) throws RemoteException {
      return 0;
    }
    
    public String[] getFeaturesForLanguage(String param1String1, String param1String2, String param1String3) throws RemoteException {
      return null;
    }
    
    public int loadLanguage(IBinder param1IBinder, String param1String1, String param1String2, String param1String3) throws RemoteException {
      return 0;
    }
    
    public void setCallback(IBinder param1IBinder, ITextToSpeechCallback param1ITextToSpeechCallback) throws RemoteException {}
    
    public List<Voice> getVoices() throws RemoteException {
      return null;
    }
    
    public int loadVoice(IBinder param1IBinder, String param1String) throws RemoteException {
      return 0;
    }
    
    public String getDefaultVoiceNameFor(String param1String1, String param1String2, String param1String3) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITextToSpeechService {
    private static final String DESCRIPTOR = "android.speech.tts.ITextToSpeechService";
    
    static final int TRANSACTION_getClientDefaultLanguage = 8;
    
    static final int TRANSACTION_getDefaultVoiceNameFor = 15;
    
    static final int TRANSACTION_getFeaturesForLanguage = 10;
    
    static final int TRANSACTION_getLanguage = 7;
    
    static final int TRANSACTION_getVoices = 13;
    
    static final int TRANSACTION_isLanguageAvailable = 9;
    
    static final int TRANSACTION_isSpeaking = 5;
    
    static final int TRANSACTION_loadLanguage = 11;
    
    static final int TRANSACTION_loadVoice = 14;
    
    static final int TRANSACTION_playAudio = 3;
    
    static final int TRANSACTION_playSilence = 4;
    
    static final int TRANSACTION_setCallback = 12;
    
    static final int TRANSACTION_speak = 1;
    
    static final int TRANSACTION_stop = 6;
    
    static final int TRANSACTION_synthesizeToFileDescriptor = 2;
    
    public Stub() {
      attachInterface(this, "android.speech.tts.ITextToSpeechService");
    }
    
    public static ITextToSpeechService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.speech.tts.ITextToSpeechService");
      if (iInterface != null && iInterface instanceof ITextToSpeechService)
        return (ITextToSpeechService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 15:
          return "getDefaultVoiceNameFor";
        case 14:
          return "loadVoice";
        case 13:
          return "getVoices";
        case 12:
          return "setCallback";
        case 11:
          return "loadLanguage";
        case 10:
          return "getFeaturesForLanguage";
        case 9:
          return "isLanguageAvailable";
        case 8:
          return "getClientDefaultLanguage";
        case 7:
          return "getLanguage";
        case 6:
          return "stop";
        case 5:
          return "isSpeaking";
        case 4:
          return "playSilence";
        case 3:
          return "playAudio";
        case 2:
          return "synthesizeToFileDescriptor";
        case 1:
          break;
      } 
      return "speak";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        String str4;
        List<Voice> list;
        ITextToSpeechCallback iTextToSpeechCallback;
        String str3, arrayOfString2[], str2, arrayOfString1[];
        IBinder iBinder1;
        String str6;
        IBinder iBinder3;
        String str5;
        IBinder iBinder2;
        String str7;
        long l;
        IBinder iBinder5;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 15:
            param1Parcel1.enforceInterface("android.speech.tts.ITextToSpeechService");
            str6 = param1Parcel1.readString();
            str7 = param1Parcel1.readString();
            str4 = param1Parcel1.readString();
            str4 = getDefaultVoiceNameFor(str6, str7, str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str4);
            return true;
          case 14:
            str4.enforceInterface("android.speech.tts.ITextToSpeechService");
            iBinder3 = str4.readStrongBinder();
            str4 = str4.readString();
            param1Int1 = loadVoice(iBinder3, str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 13:
            str4.enforceInterface("android.speech.tts.ITextToSpeechService");
            list = getVoices();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 12:
            list.enforceInterface("android.speech.tts.ITextToSpeechService");
            iBinder3 = list.readStrongBinder();
            iTextToSpeechCallback = ITextToSpeechCallback.Stub.asInterface(list.readStrongBinder());
            setCallback(iBinder3, iTextToSpeechCallback);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            iTextToSpeechCallback.enforceInterface("android.speech.tts.ITextToSpeechService");
            iBinder4 = iTextToSpeechCallback.readStrongBinder();
            str7 = iTextToSpeechCallback.readString();
            str5 = iTextToSpeechCallback.readString();
            str3 = iTextToSpeechCallback.readString();
            param1Int1 = loadLanguage(iBinder4, str7, str5, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 10:
            str3.enforceInterface("android.speech.tts.ITextToSpeechService");
            str7 = str3.readString();
            str5 = str3.readString();
            str3 = str3.readString();
            arrayOfString2 = getFeaturesForLanguage(str7, str5, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString2);
            return true;
          case 9:
            arrayOfString2.enforceInterface("android.speech.tts.ITextToSpeechService");
            str5 = arrayOfString2.readString();
            str7 = arrayOfString2.readString();
            str2 = arrayOfString2.readString();
            param1Int1 = isLanguageAvailable(str5, str7, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 8:
            str2.enforceInterface("android.speech.tts.ITextToSpeechService");
            arrayOfString1 = getClientDefaultLanguage();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString1);
            return true;
          case 7:
            arrayOfString1.enforceInterface("android.speech.tts.ITextToSpeechService");
            arrayOfString1 = getLanguage();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString1);
            return true;
          case 6:
            arrayOfString1.enforceInterface("android.speech.tts.ITextToSpeechService");
            iBinder1 = arrayOfString1.readStrongBinder();
            param1Int1 = stop(iBinder1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 5:
            iBinder1.enforceInterface("android.speech.tts.ITextToSpeechService");
            bool = isSpeaking();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 4:
            iBinder1.enforceInterface("android.speech.tts.ITextToSpeechService");
            iBinder2 = iBinder1.readStrongBinder();
            l = iBinder1.readLong();
            i = iBinder1.readInt();
            str1 = iBinder1.readString();
            i = playSilence(iBinder2, l, i, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 3:
            str1.enforceInterface("android.speech.tts.ITextToSpeechService");
            iBinder4 = str1.readStrongBinder();
            if (str1.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)str1);
            } else {
              iBinder2 = null;
            } 
            i = str1.readInt();
            if (str1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str7 = null;
            } 
            str1 = str1.readString();
            i = playAudio(iBinder4, (Uri)iBinder2, i, (Bundle)str7, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 2:
            str1.enforceInterface("android.speech.tts.ITextToSpeechService");
            iBinder5 = str1.readStrongBinder();
            if (str1.readInt() != 0) {
              CharSequence charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)str1);
            } else {
              iBinder2 = null;
            } 
            if (str1.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str7 = null;
            } 
            if (str1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str1);
            } else {
              iBinder4 = null;
            } 
            str1 = str1.readString();
            i = synthesizeToFileDescriptor(iBinder5, (CharSequence)iBinder2, (ParcelFileDescriptor)str7, (Bundle)iBinder4, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.speech.tts.ITextToSpeechService");
        IBinder iBinder4 = str1.readStrongBinder();
        if (str1.readInt() != 0) {
          CharSequence charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)str1);
        } else {
          iBinder2 = null;
        } 
        int i = str1.readInt();
        if (str1.readInt() != 0) {
          Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str1);
        } else {
          str7 = null;
        } 
        String str1 = str1.readString();
        i = speak(iBinder4, (CharSequence)iBinder2, i, (Bundle)str7, str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(i);
        return true;
      } 
      param1Parcel2.writeString("android.speech.tts.ITextToSpeechService");
      return true;
    }
    
    private static class Proxy implements ITextToSpeechService {
      public static ITextToSpeechService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.speech.tts.ITextToSpeechService";
      }
      
      public int speak(IBinder param2IBinder, CharSequence param2CharSequence, int param2Int, Bundle param2Bundle, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2CharSequence != null) {
            parcel1.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ITextToSpeechService.Stub.getDefaultImpl() != null) {
            param2Int = ITextToSpeechService.Stub.getDefaultImpl().speak(param2IBinder, param2CharSequence, param2Int, param2Bundle, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int synthesizeToFileDescriptor(IBinder param2IBinder, CharSequence param2CharSequence, ParcelFileDescriptor param2ParcelFileDescriptor, Bundle param2Bundle, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2CharSequence != null) {
            parcel1.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2ParcelFileDescriptor != null) {
            parcel1.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ITextToSpeechService.Stub.getDefaultImpl() != null)
            return ITextToSpeechService.Stub.getDefaultImpl().synthesizeToFileDescriptor(param2IBinder, param2CharSequence, param2ParcelFileDescriptor, param2Bundle, param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int playAudio(IBinder param2IBinder, Uri param2Uri, int param2Int, Bundle param2Bundle, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ITextToSpeechService.Stub.getDefaultImpl() != null) {
            param2Int = ITextToSpeechService.Stub.getDefaultImpl().playAudio(param2IBinder, param2Uri, param2Int, param2Bundle, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int playSilence(IBinder param2IBinder, long param2Long, int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ITextToSpeechService.Stub.getDefaultImpl() != null) {
            param2Int = ITextToSpeechService.Stub.getDefaultImpl().playSilence(param2IBinder, param2Long, param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSpeaking() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && ITextToSpeechService.Stub.getDefaultImpl() != null) {
            bool1 = ITextToSpeechService.Stub.getDefaultImpl().isSpeaking();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int stop(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && ITextToSpeechService.Stub.getDefaultImpl() != null)
            return ITextToSpeechService.Stub.getDefaultImpl().stop(param2IBinder); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getLanguage() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && ITextToSpeechService.Stub.getDefaultImpl() != null)
            return ITextToSpeechService.Stub.getDefaultImpl().getLanguage(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getClientDefaultLanguage() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && ITextToSpeechService.Stub.getDefaultImpl() != null)
            return ITextToSpeechService.Stub.getDefaultImpl().getClientDefaultLanguage(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int isLanguageAvailable(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && ITextToSpeechService.Stub.getDefaultImpl() != null)
            return ITextToSpeechService.Stub.getDefaultImpl().isLanguageAvailable(param2String1, param2String2, param2String3); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getFeaturesForLanguage(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && ITextToSpeechService.Stub.getDefaultImpl() != null)
            return ITextToSpeechService.Stub.getDefaultImpl().getFeaturesForLanguage(param2String1, param2String2, param2String3); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int loadLanguage(IBinder param2IBinder, String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && ITextToSpeechService.Stub.getDefaultImpl() != null)
            return ITextToSpeechService.Stub.getDefaultImpl().loadLanguage(param2IBinder, param2String1, param2String2, param2String3); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setCallback(IBinder param2IBinder, ITextToSpeechCallback param2ITextToSpeechCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2ITextToSpeechCallback != null) {
            iBinder = param2ITextToSpeechCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && ITextToSpeechService.Stub.getDefaultImpl() != null) {
            ITextToSpeechService.Stub.getDefaultImpl().setCallback(param2IBinder, param2ITextToSpeechCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<Voice> getVoices() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && ITextToSpeechService.Stub.getDefaultImpl() != null)
            return ITextToSpeechService.Stub.getDefaultImpl().getVoices(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(Voice.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int loadVoice(IBinder param2IBinder, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && ITextToSpeechService.Stub.getDefaultImpl() != null)
            return ITextToSpeechService.Stub.getDefaultImpl().loadVoice(param2IBinder, param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDefaultVoiceNameFor(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && ITextToSpeechService.Stub.getDefaultImpl() != null) {
            param2String1 = ITextToSpeechService.Stub.getDefaultImpl().getDefaultVoiceNameFor(param2String1, param2String2, param2String3);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITextToSpeechService param1ITextToSpeechService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITextToSpeechService != null) {
          Proxy.sDefaultImpl = param1ITextToSpeechService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITextToSpeechService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
