package android.speech.tts;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import com.android.internal.R;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import org.xmlpull.v1.XmlPullParserException;

public class TtsEngines {
  private static final boolean DBG = false;
  
  private static final String LOCALE_DELIMITER_NEW = "_";
  
  private static final String LOCALE_DELIMITER_OLD = "-";
  
  private static final String TAG = "TtsEngines";
  
  private static final String XML_TAG_NAME = "tts-engine";
  
  private static final Map<String, String> sNormalizeCountry;
  
  private static final Map<String, String> sNormalizeLanguage;
  
  private final Context mContext;
  
  static {
    HashMap<Object, Object> hashMap = new HashMap<>();
    String[] arrayOfString;
    int i;
    boolean bool;
    byte b;
    for (arrayOfString = Locale.getISOLanguages(), i = arrayOfString.length, bool = false, b = 0; b < i; ) {
      String str = arrayOfString[b];
      try {
        Locale locale = new Locale();
        this(str);
        hashMap.put(locale.getISO3Language(), str);
      } catch (MissingResourceException missingResourceException) {}
      b++;
    } 
    sNormalizeLanguage = Collections.unmodifiableMap(hashMap);
    hashMap = new HashMap<>();
    for (arrayOfString = Locale.getISOCountries(), i = arrayOfString.length, b = bool; b < i; ) {
      String str = arrayOfString[b];
      try {
        Locale locale = new Locale();
        this("", str);
        hashMap.put(locale.getISO3Country(), str);
      } catch (MissingResourceException missingResourceException) {}
      b++;
    } 
    sNormalizeCountry = Collections.unmodifiableMap(hashMap);
  }
  
  public TtsEngines(Context paramContext) {
    this.mContext = paramContext;
  }
  
  public String getDefaultEngine() {
    String str = Settings.Secure.getString(this.mContext.getContentResolver(), "tts_default_synth");
    if (!isEngineInstalled(str))
      str = getHighestRankedEngineName(); 
    return str;
  }
  
  public String getHighestRankedEngineName() {
    List<TextToSpeech.EngineInfo> list = getEngines();
    if (list.size() > 0 && ((TextToSpeech.EngineInfo)list.get(0)).system)
      return ((TextToSpeech.EngineInfo)list.get(0)).name; 
    return null;
  }
  
  public TextToSpeech.EngineInfo getEngineInfo(String paramString) {
    PackageManager packageManager = this.mContext.getPackageManager();
    Intent intent = new Intent("android.intent.action.TTS_SERVICE");
    intent.setPackage(paramString);
    List<ResolveInfo> list = packageManager.queryIntentServices(intent, 65536);
    if (list != null && list.size() == 1)
      return getEngineInfo(list.get(0), packageManager); 
    return null;
  }
  
  public List<TextToSpeech.EngineInfo> getEngines() {
    PackageManager packageManager = this.mContext.getPackageManager();
    Intent intent = new Intent("android.intent.action.TTS_SERVICE");
    List<ResolveInfo> list = packageManager.queryIntentServices(intent, 65536);
    if (list == null)
      return Collections.emptyList(); 
    ArrayList<TextToSpeech.EngineInfo> arrayList = new ArrayList(list.size());
    for (ResolveInfo resolveInfo : list) {
      TextToSpeech.EngineInfo engineInfo = getEngineInfo(resolveInfo, packageManager);
      if (engineInfo != null)
        arrayList.add(engineInfo); 
    } 
    Collections.sort(arrayList, EngineInfoComparator.INSTANCE);
    return arrayList;
  }
  
  private boolean isSystemEngine(ServiceInfo paramServiceInfo) {
    ApplicationInfo applicationInfo = paramServiceInfo.applicationInfo;
    boolean bool = true;
    if (applicationInfo == null || (applicationInfo.flags & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public boolean isEngineInstalled(String paramString) {
    boolean bool = false;
    if (paramString == null)
      return false; 
    if (getEngineInfo(paramString) != null)
      bool = true; 
    return bool;
  }
  
  public Intent getSettingsIntent(String paramString) {
    PackageManager packageManager = this.mContext.getPackageManager();
    Intent intent = new Intent("android.intent.action.TTS_SERVICE");
    intent.setPackage(paramString);
    List<ResolveInfo> list = packageManager.queryIntentServices(intent, 65664);
    if (list != null && list.size() == 1) {
      ServiceInfo serviceInfo = ((ResolveInfo)list.get(0)).serviceInfo;
      if (serviceInfo != null) {
        String str = settingsActivityFromServiceInfo(serviceInfo, packageManager);
        if (str != null) {
          Intent intent1 = new Intent();
          intent1.setClassName(paramString, str);
          return intent1;
        } 
      } 
    } 
    return null;
  }
  
  private String settingsActivityFromServiceInfo(ServiceInfo paramServiceInfo, PackageManager paramPackageManager) {
    XmlResourceParser xmlResourceParser1 = null, xmlResourceParser2 = null, xmlResourceParser3 = null, xmlResourceParser4 = null;
    try {
      StringBuilder stringBuilder;
      XmlResourceParser xmlResourceParser = paramServiceInfo.loadXmlMetaData(paramPackageManager, "android.speech.tts");
      if (xmlResourceParser == null) {
        xmlResourceParser4 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser3 = xmlResourceParser;
        stringBuilder = new StringBuilder();
        xmlResourceParser4 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser3 = xmlResourceParser;
        this();
        xmlResourceParser4 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser3 = xmlResourceParser;
        stringBuilder.append("No meta-data found for :");
        xmlResourceParser4 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser3 = xmlResourceParser;
        stringBuilder.append(paramServiceInfo);
        xmlResourceParser4 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser3 = xmlResourceParser;
        Log.w("TtsEngines", stringBuilder.toString());
        if (xmlResourceParser != null)
          xmlResourceParser.close(); 
        return null;
      } 
      xmlResourceParser4 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser3 = xmlResourceParser;
      Resources resources = stringBuilder.getResourcesForApplication(paramServiceInfo.applicationInfo);
      while (true) {
        xmlResourceParser4 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser3 = xmlResourceParser;
        int i = xmlResourceParser.next();
        if (i != 1) {
          if (i == 2) {
            xmlResourceParser4 = xmlResourceParser;
            xmlResourceParser1 = xmlResourceParser;
            xmlResourceParser2 = xmlResourceParser;
            xmlResourceParser3 = xmlResourceParser;
            if (!"tts-engine".equals(xmlResourceParser.getName())) {
              xmlResourceParser4 = xmlResourceParser;
              xmlResourceParser1 = xmlResourceParser;
              xmlResourceParser2 = xmlResourceParser;
              xmlResourceParser3 = xmlResourceParser;
              StringBuilder stringBuilder1 = new StringBuilder();
              xmlResourceParser4 = xmlResourceParser;
              xmlResourceParser1 = xmlResourceParser;
              xmlResourceParser2 = xmlResourceParser;
              xmlResourceParser3 = xmlResourceParser;
              this();
              xmlResourceParser4 = xmlResourceParser;
              xmlResourceParser1 = xmlResourceParser;
              xmlResourceParser2 = xmlResourceParser;
              xmlResourceParser3 = xmlResourceParser;
              stringBuilder1.append("Package ");
              xmlResourceParser4 = xmlResourceParser;
              xmlResourceParser1 = xmlResourceParser;
              xmlResourceParser2 = xmlResourceParser;
              xmlResourceParser3 = xmlResourceParser;
              stringBuilder1.append(paramServiceInfo);
              xmlResourceParser4 = xmlResourceParser;
              xmlResourceParser1 = xmlResourceParser;
              xmlResourceParser2 = xmlResourceParser;
              xmlResourceParser3 = xmlResourceParser;
              stringBuilder1.append(" uses unknown tag :");
              xmlResourceParser4 = xmlResourceParser;
              xmlResourceParser1 = xmlResourceParser;
              xmlResourceParser2 = xmlResourceParser;
              xmlResourceParser3 = xmlResourceParser;
              stringBuilder1.append(xmlResourceParser.getName());
              xmlResourceParser4 = xmlResourceParser;
              xmlResourceParser1 = xmlResourceParser;
              xmlResourceParser2 = xmlResourceParser;
              xmlResourceParser3 = xmlResourceParser;
              str = stringBuilder1.toString();
              xmlResourceParser4 = xmlResourceParser;
              xmlResourceParser1 = xmlResourceParser;
              xmlResourceParser2 = xmlResourceParser;
              xmlResourceParser3 = xmlResourceParser;
              Log.w("TtsEngines", str);
              if (xmlResourceParser != null)
                xmlResourceParser.close(); 
              return null;
            } 
            xmlResourceParser4 = xmlResourceParser;
            xmlResourceParser1 = xmlResourceParser;
            xmlResourceParser2 = xmlResourceParser;
            xmlResourceParser3 = xmlResourceParser;
            AttributeSet attributeSet = Xml.asAttributeSet(xmlResourceParser);
            xmlResourceParser4 = xmlResourceParser;
            xmlResourceParser1 = xmlResourceParser;
            xmlResourceParser2 = xmlResourceParser;
            xmlResourceParser3 = xmlResourceParser;
            TypedArray typedArray = str.obtainAttributes(attributeSet, R.styleable.TextToSpeechEngine);
            xmlResourceParser4 = xmlResourceParser;
            xmlResourceParser1 = xmlResourceParser;
            xmlResourceParser2 = xmlResourceParser;
            xmlResourceParser3 = xmlResourceParser;
            String str = typedArray.getString(0);
            xmlResourceParser4 = xmlResourceParser;
            xmlResourceParser1 = xmlResourceParser;
            xmlResourceParser2 = xmlResourceParser;
            xmlResourceParser3 = xmlResourceParser;
            typedArray.recycle();
            if (xmlResourceParser != null)
              xmlResourceParser.close(); 
            return str;
          } 
          continue;
        } 
        break;
      } 
      if (xmlResourceParser != null)
        xmlResourceParser.close(); 
      return null;
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      xmlResourceParser4 = xmlResourceParser3;
      StringBuilder stringBuilder = new StringBuilder();
      xmlResourceParser4 = xmlResourceParser3;
      this();
      xmlResourceParser4 = xmlResourceParser3;
      stringBuilder.append("Could not load resources for : ");
      xmlResourceParser4 = xmlResourceParser3;
      stringBuilder.append(paramServiceInfo);
      xmlResourceParser4 = xmlResourceParser3;
      Log.w("TtsEngines", stringBuilder.toString());
      if (xmlResourceParser3 != null)
        xmlResourceParser3.close(); 
      return null;
    } catch (XmlPullParserException xmlPullParserException) {
      xmlResourceParser4 = xmlResourceParser2;
      StringBuilder stringBuilder = new StringBuilder();
      xmlResourceParser4 = xmlResourceParser2;
      this();
      xmlResourceParser4 = xmlResourceParser2;
      stringBuilder.append("Error parsing metadata for ");
      xmlResourceParser4 = xmlResourceParser2;
      stringBuilder.append(paramServiceInfo);
      xmlResourceParser4 = xmlResourceParser2;
      stringBuilder.append(":");
      xmlResourceParser4 = xmlResourceParser2;
      stringBuilder.append(xmlPullParserException);
      xmlResourceParser4 = xmlResourceParser2;
      Log.w("TtsEngines", stringBuilder.toString());
      if (xmlResourceParser2 != null)
        xmlResourceParser2.close(); 
      return null;
    } catch (IOException iOException) {
      xmlResourceParser4 = xmlResourceParser1;
      StringBuilder stringBuilder = new StringBuilder();
      xmlResourceParser4 = xmlResourceParser1;
      this();
      xmlResourceParser4 = xmlResourceParser1;
      stringBuilder.append("Error parsing metadata for ");
      xmlResourceParser4 = xmlResourceParser1;
      stringBuilder.append(paramServiceInfo);
      xmlResourceParser4 = xmlResourceParser1;
      stringBuilder.append(":");
      xmlResourceParser4 = xmlResourceParser1;
      stringBuilder.append(iOException);
      xmlResourceParser4 = xmlResourceParser1;
      Log.w("TtsEngines", stringBuilder.toString());
      if (xmlResourceParser1 != null)
        xmlResourceParser1.close(); 
      return null;
    } finally {}
    if (xmlResourceParser4 != null)
      xmlResourceParser4.close(); 
    throw paramServiceInfo;
  }
  
  private TextToSpeech.EngineInfo getEngineInfo(ResolveInfo paramResolveInfo, PackageManager paramPackageManager) {
    ServiceInfo serviceInfo = paramResolveInfo.serviceInfo;
    if (serviceInfo != null) {
      TextToSpeech.EngineInfo engineInfo = new TextToSpeech.EngineInfo();
      engineInfo.name = serviceInfo.packageName;
      CharSequence charSequence = serviceInfo.loadLabel(paramPackageManager);
      if (TextUtils.isEmpty(charSequence)) {
        charSequence = engineInfo.name;
      } else {
        charSequence = charSequence.toString();
      } 
      engineInfo.label = (String)charSequence;
      engineInfo.icon = serviceInfo.getIconResource();
      engineInfo.priority = paramResolveInfo.priority;
      engineInfo.system = isSystemEngine(serviceInfo);
      return engineInfo;
    } 
    return null;
  }
  
  private static class EngineInfoComparator implements Comparator<TextToSpeech.EngineInfo> {
    static EngineInfoComparator INSTANCE = new EngineInfoComparator();
    
    public int compare(TextToSpeech.EngineInfo param1EngineInfo1, TextToSpeech.EngineInfo param1EngineInfo2) {
      if (param1EngineInfo1.system && !param1EngineInfo2.system)
        return -1; 
      if (param1EngineInfo2.system && !param1EngineInfo1.system)
        return 1; 
      return param1EngineInfo2.priority - param1EngineInfo1.priority;
    }
  }
  
  public Locale getLocalePrefForEngine(String paramString) {
    Context context = this.mContext;
    String str = Settings.Secure.getString(context.getContentResolver(), "tts_default_locale");
    return getLocalePrefForEngine(paramString, str);
  }
  
  public Locale getLocalePrefForEngine(String paramString1, String paramString2) {
    String str = parseEnginePrefFromList(paramString2, paramString1);
    if (TextUtils.isEmpty(str))
      return Locale.getDefault(); 
    Locale locale2 = parseLocaleString(str);
    Locale locale1 = locale2;
    if (locale2 == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to parse locale ");
      stringBuilder.append(str);
      stringBuilder.append(", returning en_US instead");
      Log.w("TtsEngines", stringBuilder.toString());
      locale1 = Locale.US;
    } 
    return locale1;
  }
  
  public boolean isLocaleSetToDefaultForEngine(String paramString) {
    Context context = this.mContext;
    String str = Settings.Secure.getString(context.getContentResolver(), "tts_default_locale");
    return TextUtils.isEmpty(parseEnginePrefFromList(str, paramString));
  }
  
  public Locale parseLocaleString(String paramString) {
    String str1 = "", str2 = "", str3 = "";
    String str4 = str2, str5 = str3;
    if (!TextUtils.isEmpty(paramString)) {
      String[] arrayOfString = paramString.split("[-_]");
      String str = arrayOfString[0].toLowerCase();
      if (arrayOfString.length == 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to convert ");
        stringBuilder.append(paramString);
        stringBuilder.append(" to a valid Locale object. Only separators");
        Log.w("TtsEngines", stringBuilder.toString());
        return null;
      } 
      if (arrayOfString.length > 3) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to convert ");
        stringBuilder.append(paramString);
        stringBuilder.append(" to a valid Locale object. Too many separators");
        Log.w("TtsEngines", stringBuilder.toString());
        return null;
      } 
      if (arrayOfString.length >= 2)
        str2 = arrayOfString[1].toUpperCase(); 
      str1 = str;
      str4 = str2;
      str5 = str3;
      if (arrayOfString.length >= 3) {
        str5 = arrayOfString[2];
        str4 = str2;
        str1 = str;
      } 
    } 
    str2 = sNormalizeLanguage.get(str1);
    if (str2 != null)
      str1 = str2; 
    str2 = sNormalizeCountry.get(str4);
    if (str2 != null)
      str4 = str2; 
    Locale locale = new Locale(str1, str4, str5);
    try {
      locale.getISO3Language();
      locale.getISO3Country();
      return locale;
    } catch (MissingResourceException missingResourceException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to convert ");
      stringBuilder.append(paramString);
      stringBuilder.append(" to a valid Locale object.");
      Log.w("TtsEngines", stringBuilder.toString());
      return null;
    } 
  }
  
  public static Locale normalizeTTSLocale(Locale paramLocale) {
    String str1 = paramLocale.getLanguage();
    String str2 = str1;
    if (!TextUtils.isEmpty(str1)) {
      String str = sNormalizeLanguage.get(str1);
      str2 = str1;
      if (str != null)
        str2 = str; 
    } 
    String str3 = paramLocale.getCountry();
    str1 = str3;
    if (!TextUtils.isEmpty(str3)) {
      String str = sNormalizeCountry.get(str3);
      str1 = str3;
      if (str != null)
        str1 = str; 
    } 
    return new Locale(str2, str1, paramLocale.getVariant());
  }
  
  public static String[] toOldLocaleStringFormat(Locale paramLocale) {
    String[] arrayOfString = new String[3];
    arrayOfString[0] = "";
    arrayOfString[1] = "";
    arrayOfString[2] = "";
    try {
      arrayOfString[0] = paramLocale.getISO3Language();
      arrayOfString[1] = paramLocale.getISO3Country();
      arrayOfString[2] = paramLocale.getVariant();
      return arrayOfString;
    } catch (MissingResourceException missingResourceException) {
      return new String[] { "eng", "USA", "" };
    } 
  }
  
  private static String parseEnginePrefFromList(String paramString1, String paramString2) {
    if (TextUtils.isEmpty(paramString1))
      return null; 
    for (String paramString1 : paramString1.split(",")) {
      int i = paramString1.indexOf(':');
      if (i > 0 && 
        paramString2.equals(paramString1.substring(0, i)))
        return paramString1.substring(i + 1); 
    } 
    return null;
  }
  
  public void updateLocalePrefForEngine(String paramString, Locale paramLocale) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mContext : Landroid/content/Context;
    //   6: invokevirtual getContentResolver : ()Landroid/content/ContentResolver;
    //   9: ldc_w 'tts_default_locale'
    //   12: invokestatic getString : (Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    //   15: astore_3
    //   16: aload_2
    //   17: ifnull -> 28
    //   20: aload_2
    //   21: invokevirtual toString : ()Ljava/lang/String;
    //   24: astore_2
    //   25: goto -> 31
    //   28: ldc ''
    //   30: astore_2
    //   31: aload_0
    //   32: aload_3
    //   33: aload_1
    //   34: aload_2
    //   35: invokespecial updateValueInCommaSeparatedList : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   38: astore_2
    //   39: aload_0
    //   40: getfield mContext : Landroid/content/Context;
    //   43: invokevirtual getContentResolver : ()Landroid/content/ContentResolver;
    //   46: astore_1
    //   47: aload_2
    //   48: invokevirtual toString : ()Ljava/lang/String;
    //   51: astore_2
    //   52: aload_1
    //   53: ldc_w 'tts_default_locale'
    //   56: aload_2
    //   57: invokestatic putString : (Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    //   60: pop
    //   61: aload_0
    //   62: monitorexit
    //   63: return
    //   64: astore_1
    //   65: aload_0
    //   66: monitorexit
    //   67: aload_1
    //   68: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #527	-> 2
    //   #534	-> 16
    //   #535	-> 16
    //   #534	-> 31
    //   #539	-> 39
    //   #540	-> 47
    //   #539	-> 52
    //   #541	-> 61
    //   #526	-> 64
    // Exception table:
    //   from	to	target	type
    //   2	16	64	finally
    //   20	25	64	finally
    //   31	39	64	finally
    //   39	47	64	finally
    //   47	52	64	finally
    //   52	61	64	finally
  }
  
  private String updateValueInCommaSeparatedList(String paramString1, String paramString2, String paramString3) {
    StringBuilder stringBuilder = new StringBuilder();
    if (TextUtils.isEmpty(paramString1)) {
      stringBuilder.append(paramString2);
      stringBuilder.append(':');
      stringBuilder.append(paramString3);
    } else {
      String[] arrayOfString = paramString1.split(",");
      boolean bool1 = true;
      boolean bool2 = false;
      int i;
      byte b;
      for (i = arrayOfString.length, b = 0; b < i; ) {
        String str = arrayOfString[b];
        int j = str.indexOf(':');
        boolean bool3 = bool1, bool4 = bool2;
        if (j > 0)
          if (paramString2.equals(str.substring(0, j))) {
            if (bool1) {
              bool1 = false;
            } else {
              stringBuilder.append(',');
            } 
            bool4 = true;
            stringBuilder.append(paramString2);
            stringBuilder.append(':');
            stringBuilder.append(paramString3);
            bool3 = bool1;
          } else {
            if (bool1) {
              bool1 = false;
            } else {
              stringBuilder.append(',');
            } 
            stringBuilder.append(str);
            bool4 = bool2;
            bool3 = bool1;
          }  
        b++;
        bool1 = bool3;
        bool2 = bool4;
      } 
      if (!bool2) {
        stringBuilder.append(',');
        stringBuilder.append(paramString2);
        stringBuilder.append(':');
        stringBuilder.append(paramString3);
      } 
    } 
    return stringBuilder.toString();
  }
}
