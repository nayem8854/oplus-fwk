package android.speech.tts;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public interface SynthesisCallback {
  int audioAvailable(byte[] paramArrayOfbyte, int paramInt1, int paramInt2);
  
  int done();
  
  void error();
  
  void error(int paramInt);
  
  int getMaxBufferSize();
  
  boolean hasFinished();
  
  boolean hasStarted();
  
  default void rangeStart(int paramInt1, int paramInt2, int paramInt3) {}
  
  int start(int paramInt1, int paramInt2, int paramInt3);
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SupportedAudioFormat {}
}
