package android.speech.tts;

import android.media.AudioFormat;
import android.media.AudioTrack;
import android.util.Log;

class BlockingAudioTrack {
  private volatile boolean mStopped;
  
  private int mSessionId;
  
  private final int mSampleRateInHz;
  
  private boolean mIsShortUtterance;
  
  private final int mChannelCount;
  
  private int mBytesWritten = 0;
  
  private final int mBytesPerFrame;
  
  private Object mAudioTrackLock = new Object();
  
  private AudioTrack mAudioTrack;
  
  private final TextToSpeechService.AudioOutputParams mAudioParams;
  
  private final int mAudioFormat;
  
  private int mAudioBufferSize;
  
  private static final String TAG = "TTS.BlockingAudioTrack";
  
  private static final long MIN_SLEEP_TIME_MS = 20L;
  
  private static final int MIN_AUDIO_BUFFER_SIZE = 8192;
  
  private static final long MAX_SLEEP_TIME_MS = 2500L;
  
  private static final long MAX_PROGRESS_WAIT_MS = 2500L;
  
  private static final boolean DBG = false;
  
  BlockingAudioTrack(TextToSpeechService.AudioOutputParams paramAudioOutputParams, int paramInt1, int paramInt2, int paramInt3) {
    this.mAudioParams = paramAudioOutputParams;
    this.mSampleRateInHz = paramInt1;
    this.mAudioFormat = paramInt2;
    this.mChannelCount = paramInt3;
    this.mBytesPerFrame = AudioFormat.getBytesPerSample(paramInt2) * this.mChannelCount;
    this.mIsShortUtterance = false;
    this.mAudioBufferSize = 0;
    this.mBytesWritten = 0;
    this.mAudioTrack = null;
    this.mStopped = false;
  }
  
  public boolean init() {
    null = createStreamingAudioTrack();
    synchronized (this.mAudioTrackLock) {
      this.mAudioTrack = null;
      if (null == null)
        return false; 
      return true;
    } 
  }
  
  public void stop() {
    synchronized (this.mAudioTrackLock) {
      if (this.mAudioTrack != null)
        this.mAudioTrack.stop(); 
      this.mStopped = true;
      return;
    } 
  }
  
  public int write(byte[] paramArrayOfbyte) {
    synchronized (this.mAudioTrackLock) {
      AudioTrack audioTrack = this.mAudioTrack;
      if (audioTrack == null || this.mStopped)
        return -1; 
      int i = writeToAudioTrack(audioTrack, paramArrayOfbyte);
      this.mBytesWritten += i;
      return i;
    } 
  }
  
  public void waitAndRelease() {
    synchronized (this.mAudioTrackLock) {
      null = this.mAudioTrack;
      if (null == null)
        return; 
      if (this.mBytesWritten < this.mAudioBufferSize && !this.mStopped) {
        this.mIsShortUtterance = true;
        null.stop();
      } 
      if (!this.mStopped)
        blockUntilDone(this.mAudioTrack); 
      synchronized (this.mAudioTrackLock) {
        this.mAudioTrack = null;
        null.release();
        return;
      } 
    } 
  }
  
  static int getChannelConfig(int paramInt) {
    if (paramInt == 1)
      return 4; 
    if (paramInt == 2)
      return 12; 
    return 0;
  }
  
  long getAudioLengthMs(int paramInt) {
    paramInt /= this.mBytesPerFrame;
    return (paramInt * 1000 / this.mSampleRateInHz);
  }
  
  private static int writeToAudioTrack(AudioTrack paramAudioTrack, byte[] paramArrayOfbyte) {
    if (paramAudioTrack.getPlayState() != 3)
      paramAudioTrack.play(); 
    int i = 0;
    while (i < paramArrayOfbyte.length) {
      int j = paramAudioTrack.write(paramArrayOfbyte, i, paramArrayOfbyte.length);
      if (j <= 0)
        break; 
      i += j;
    } 
    return i;
  }
  
  private AudioTrack createStreamingAudioTrack() {
    int i = getChannelConfig(this.mChannelCount);
    int j = this.mSampleRateInHz, k = this.mAudioFormat;
    j = AudioTrack.getMinBufferSize(j, i, k);
    j = Math.max(8192, j);
    AudioFormat.Builder builder = new AudioFormat.Builder();
    builder = builder.setChannelMask(i);
    i = this.mAudioFormat;
    builder = builder.setEncoding(i);
    i = this.mSampleRateInHz;
    AudioFormat audioFormat = builder.setSampleRate(i).build();
    AudioTrack audioTrack = new AudioTrack(this.mAudioParams.mAudioAttributes, audioFormat, j, 1, this.mAudioParams.mSessionId);
    if (audioTrack.getState() != 1) {
      Log.w("TTS.BlockingAudioTrack", "Unable to create audio track.");
      audioTrack.release();
      return null;
    } 
    this.mAudioBufferSize = j;
    setupVolume(audioTrack, this.mAudioParams.mVolume, this.mAudioParams.mPan);
    return audioTrack;
  }
  
  private void blockUntilDone(AudioTrack paramAudioTrack) {
    if (this.mBytesWritten <= 0)
      return; 
    if (this.mIsShortUtterance) {
      blockUntilEstimatedCompletion();
    } else {
      blockUntilCompletion(paramAudioTrack);
    } 
  }
  
  private void blockUntilEstimatedCompletion() {
    int i = this.mBytesWritten / this.mBytesPerFrame;
    long l = (i * 1000 / this.mSampleRateInHz);
    try {
      Thread.sleep(l);
    } catch (InterruptedException interruptedException) {}
  }
  
  private void blockUntilCompletion(AudioTrack paramAudioTrack) {
    int i = this.mBytesWritten / this.mBytesPerFrame;
    int j = -1;
    long l = 0L;
    while (true) {
      int k = paramAudioTrack.getPlaybackHeadPosition();
      if (k < i && 
        paramAudioTrack.getPlayState() == 3 && !this.mStopped) {
        long l1 = ((i - k) * 1000 / paramAudioTrack.getSampleRate());
        long l2 = clip(l1, 20L, 2500L);
        if (k == j) {
          l1 = l + l2;
          l = l1;
          if (l1 > 2500L) {
            Log.w("TTS.BlockingAudioTrack", "Waited unsuccessfully for 2500ms for AudioTrack to make progress, Aborting");
            break;
          } 
        } else {
          l = 0L;
        } 
        j = k;
        try {
          Thread.sleep(l2);
          continue;
        } catch (InterruptedException interruptedException) {
          break;
        } 
      } 
      break;
    } 
  }
  
  private static void setupVolume(AudioTrack paramAudioTrack, float paramFloat1, float paramFloat2) {
    float f3, f1 = clip(paramFloat1, 0.0F, 1.0F);
    float f2 = clip(paramFloat2, -1.0F, 1.0F);
    paramFloat1 = f1;
    paramFloat2 = f1;
    if (f2 > 0.0F) {
      f1 = paramFloat1 * (1.0F - f2);
      f3 = paramFloat2;
    } else {
      f1 = paramFloat1;
      f3 = paramFloat2;
      if (f2 < 0.0F) {
        f3 = paramFloat2 * (1.0F + f2);
        f1 = paramFloat1;
      } 
    } 
    if (paramAudioTrack.setStereoVolume(f1, f3) != 0)
      Log.e("TTS.BlockingAudioTrack", "Failed to set volume"); 
  }
  
  private static final long clip(long paramLong1, long paramLong2, long paramLong3) {
    if (paramLong1 < paramLong2) {
      paramLong1 = paramLong2;
    } else if (paramLong1 >= paramLong3) {
      paramLong1 = paramLong3;
    } 
    return paramLong1;
  }
  
  private static final float clip(float paramFloat1, float paramFloat2, float paramFloat3) {
    if (paramFloat1 < paramFloat2) {
      paramFloat1 = paramFloat2;
    } else if (paramFloat1 >= paramFloat3) {
      paramFloat1 = paramFloat3;
    } 
    return paramFloat1;
  }
  
  public void setPlaybackPositionUpdateListener(AudioTrack.OnPlaybackPositionUpdateListener paramOnPlaybackPositionUpdateListener) {
    synchronized (this.mAudioTrackLock) {
      if (this.mAudioTrack != null)
        this.mAudioTrack.setPlaybackPositionUpdateListener(paramOnPlaybackPositionUpdateListener); 
      return;
    } 
  }
  
  public void setNotificationMarkerPosition(int paramInt) {
    synchronized (this.mAudioTrackLock) {
      if (this.mAudioTrack != null)
        this.mAudioTrack.setNotificationMarkerPosition(paramInt); 
      return;
    } 
  }
}
