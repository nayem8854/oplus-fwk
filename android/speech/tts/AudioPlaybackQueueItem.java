package android.speech.tts;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.ConditionVariable;
import android.util.Log;

class AudioPlaybackQueueItem extends PlaybackQueueItem {
  private static final String TAG = "TTS.AudioQueueItem";
  
  private final TextToSpeechService.AudioOutputParams mAudioParams;
  
  private final Context mContext;
  
  private final ConditionVariable mDone;
  
  private volatile boolean mFinished;
  
  private MediaPlayer mPlayer;
  
  private final Uri mUri;
  
  AudioPlaybackQueueItem(TextToSpeechService.UtteranceProgressDispatcher paramUtteranceProgressDispatcher, Object paramObject, Context paramContext, Uri paramUri, TextToSpeechService.AudioOutputParams paramAudioOutputParams) {
    super(paramUtteranceProgressDispatcher, paramObject);
    this.mContext = paramContext;
    this.mUri = paramUri;
    this.mAudioParams = paramAudioOutputParams;
    this.mDone = new ConditionVariable();
    this.mPlayer = null;
    this.mFinished = false;
  }
  
  public void run() {
    TextToSpeechService.UtteranceProgressDispatcher utteranceProgressDispatcher = getDispatcher();
    utteranceProgressDispatcher.dispatchOnStart();
    int i = this.mAudioParams.mSessionId;
    Context context = this.mContext;
    Uri uri = this.mUri;
    AudioAttributes audioAttributes = this.mAudioParams.mAudioAttributes;
    if (i <= 0)
      i = 0; 
    MediaPlayer mediaPlayer = MediaPlayer.create(context, uri, null, audioAttributes, i);
    if (mediaPlayer == null) {
      utteranceProgressDispatcher.dispatchOnError(-5);
      return;
    } 
    try {
      MediaPlayer.OnErrorListener onErrorListener = new MediaPlayer.OnErrorListener() {
          final AudioPlaybackQueueItem this$0;
          
          public boolean onError(MediaPlayer param1MediaPlayer, int param1Int1, int param1Int2) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Audio playback error: ");
            stringBuilder.append(param1Int1);
            stringBuilder.append(", ");
            stringBuilder.append(param1Int2);
            Log.w("TTS.AudioQueueItem", stringBuilder.toString());
            AudioPlaybackQueueItem.this.mDone.open();
            return true;
          }
        };
      super(this);
      mediaPlayer.setOnErrorListener(onErrorListener);
      mediaPlayer = this.mPlayer;
      MediaPlayer.OnCompletionListener onCompletionListener = new MediaPlayer.OnCompletionListener() {
          final AudioPlaybackQueueItem this$0;
          
          public void onCompletion(MediaPlayer param1MediaPlayer) {
            AudioPlaybackQueueItem.access$102(AudioPlaybackQueueItem.this, true);
            AudioPlaybackQueueItem.this.mDone.open();
          }
        };
      super(this);
      mediaPlayer.setOnCompletionListener(onCompletionListener);
      setupVolume(this.mPlayer, this.mAudioParams.mVolume, this.mAudioParams.mPan);
      this.mPlayer.start();
      this.mDone.block();
      finish();
    } catch (IllegalArgumentException illegalArgumentException) {
      Log.w("TTS.AudioQueueItem", "MediaPlayer failed", illegalArgumentException);
      this.mDone.open();
    } 
    if (this.mFinished) {
      utteranceProgressDispatcher.dispatchOnSuccess();
    } else {
      utteranceProgressDispatcher.dispatchOnStop();
    } 
  }
  
  private static void setupVolume(MediaPlayer paramMediaPlayer, float paramFloat1, float paramFloat2) {
    float f3, f1 = clip(paramFloat1, 0.0F, 1.0F);
    float f2 = clip(paramFloat2, -1.0F, 1.0F);
    paramFloat1 = f1;
    paramFloat2 = f1;
    if (f2 > 0.0F) {
      f3 = paramFloat1 * (1.0F - f2);
      f1 = paramFloat2;
    } else {
      f3 = paramFloat1;
      f1 = paramFloat2;
      if (f2 < 0.0F) {
        f1 = paramFloat2 * (1.0F + f2);
        f3 = paramFloat1;
      } 
    } 
    paramMediaPlayer.setVolume(f3, f1);
  }
  
  private static final float clip(float paramFloat1, float paramFloat2, float paramFloat3) {
    if (paramFloat1 < paramFloat2) {
      paramFloat1 = paramFloat2;
    } else if (paramFloat1 >= paramFloat3) {
      paramFloat1 = paramFloat3;
    } 
    return paramFloat1;
  }
  
  private void finish() {
    try {
      this.mPlayer.stop();
    } catch (IllegalStateException illegalStateException) {}
    this.mPlayer.release();
  }
  
  void stop(int paramInt) {
    this.mDone.open();
  }
}
