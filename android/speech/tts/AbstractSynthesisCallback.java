package android.speech.tts;

abstract class AbstractSynthesisCallback implements SynthesisCallback {
  protected final boolean mClientIsUsingV2;
  
  AbstractSynthesisCallback(boolean paramBoolean) {
    this.mClientIsUsingV2 = paramBoolean;
  }
  
  int errorCodeOnStop() {
    byte b;
    if (this.mClientIsUsingV2) {
      b = -2;
    } else {
      b = -1;
    } 
    return b;
  }
  
  abstract void stop();
}
