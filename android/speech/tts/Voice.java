package android.speech.tts;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class Voice implements Parcelable {
  public Voice(String paramString, Locale paramLocale, int paramInt1, int paramInt2, boolean paramBoolean, Set<String> paramSet) {
    this.mName = paramString;
    this.mLocale = paramLocale;
    this.mQuality = paramInt1;
    this.mLatency = paramInt2;
    this.mRequiresNetworkConnection = paramBoolean;
    this.mFeatures = paramSet;
  }
  
  private Voice(Parcel paramParcel) {
    this.mName = paramParcel.readString();
    this.mLocale = (Locale)paramParcel.readSerializable();
    this.mQuality = paramParcel.readInt();
    this.mLatency = paramParcel.readInt();
    byte b = paramParcel.readByte();
    boolean bool = true;
    if (b != 1)
      bool = false; 
    this.mRequiresNetworkConnection = bool;
    HashSet<String> hashSet = new HashSet();
    Collections.addAll(hashSet, paramParcel.readStringArray());
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mName);
    paramParcel.writeSerializable(this.mLocale);
    paramParcel.writeInt(this.mQuality);
    paramParcel.writeInt(this.mLatency);
    paramParcel.writeByte((byte)this.mRequiresNetworkConnection);
    paramParcel.writeStringList(new ArrayList<>(this.mFeatures));
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<Voice> CREATOR = new Parcelable.Creator<Voice>() {
      public Voice createFromParcel(Parcel param1Parcel) {
        return new Voice(param1Parcel);
      }
      
      public Voice[] newArray(int param1Int) {
        return new Voice[param1Int];
      }
    };
  
  public static final int LATENCY_HIGH = 400;
  
  public static final int LATENCY_LOW = 200;
  
  public static final int LATENCY_NORMAL = 300;
  
  public static final int LATENCY_VERY_HIGH = 500;
  
  public static final int LATENCY_VERY_LOW = 100;
  
  public static final int QUALITY_HIGH = 400;
  
  public static final int QUALITY_LOW = 200;
  
  public static final int QUALITY_NORMAL = 300;
  
  public static final int QUALITY_VERY_HIGH = 500;
  
  public static final int QUALITY_VERY_LOW = 100;
  
  private final Set<String> mFeatures;
  
  private final int mLatency;
  
  private final Locale mLocale;
  
  private final String mName;
  
  private final int mQuality;
  
  private final boolean mRequiresNetworkConnection;
  
  public Locale getLocale() {
    return this.mLocale;
  }
  
  public int getQuality() {
    return this.mQuality;
  }
  
  public int getLatency() {
    return this.mLatency;
  }
  
  public boolean isNetworkConnectionRequired() {
    return this.mRequiresNetworkConnection;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public Set<String> getFeatures() {
    return this.mFeatures;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(64);
    stringBuilder.append("Voice[Name: ");
    stringBuilder.append(this.mName);
    stringBuilder.append(", locale: ");
    stringBuilder.append(this.mLocale);
    stringBuilder.append(", quality: ");
    stringBuilder.append(this.mQuality);
    stringBuilder.append(", latency: ");
    stringBuilder.append(this.mLatency);
    stringBuilder.append(", requiresNetwork: ");
    stringBuilder.append(this.mRequiresNetworkConnection);
    stringBuilder.append(", features: ");
    stringBuilder.append(this.mFeatures.toString());
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int hashCode() {
    int j, m;
    char c;
    Set<String> set = this.mFeatures;
    int i = 0;
    if (set == null) {
      j = 0;
    } else {
      j = set.hashCode();
    } 
    int k = this.mLatency;
    Locale locale = this.mLocale;
    if (locale == null) {
      m = 0;
    } else {
      m = locale.hashCode();
    } 
    String str = this.mName;
    if (str != null)
      i = str.hashCode(); 
    int n = this.mQuality;
    if (this.mRequiresNetworkConnection) {
      c = 'ӏ';
    } else {
      c = 'ӕ';
    } 
    return (((((1 * 31 + j) * 31 + k) * 31 + m) * 31 + i) * 31 + n) * 31 + c;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    Set<String> set = this.mFeatures;
    if (set == null) {
      if (((Voice)paramObject).mFeatures != null)
        return false; 
    } else if (!set.equals(((Voice)paramObject).mFeatures)) {
      return false;
    } 
    if (this.mLatency != ((Voice)paramObject).mLatency)
      return false; 
    Locale locale = this.mLocale;
    if (locale == null) {
      if (((Voice)paramObject).mLocale != null)
        return false; 
    } else if (!locale.equals(((Voice)paramObject).mLocale)) {
      return false;
    } 
    String str = this.mName;
    if (str == null) {
      if (((Voice)paramObject).mName != null)
        return false; 
    } else if (!str.equals(((Voice)paramObject).mName)) {
      return false;
    } 
    if (this.mQuality != ((Voice)paramObject).mQuality)
      return false; 
    if (this.mRequiresNetworkConnection != ((Voice)paramObject).mRequiresNetworkConnection)
      return false; 
    return true;
  }
}
